/* Generated from c-platform.scm by the CHICKEN compiler
   http://www.call-cc.org
   Version 5.2.0 (rev 317468e4)
   linux-unix-gnu-x86-64 [ 64bit dload ptables ]
   command line: c-platform.scm -optimize-level 2 -include-path . -include-path ./ -inline -ignore-repository -feature chicken-bootstrap -no-warnings -specialize -consult-types-file ./types.db -no-lambda-info -no-trace -emit-import-library chicken.compiler.c-platform -output-file c-platform.c
   unit: c-platform
   uses: library eval expand internal optimizer support compiler
*/
#include "chicken.h"

static C_PTABLE_ENTRY *create_ptable(void);
C_noret_decl(C_library_toplevel)
C_externimport void C_ccall C_library_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_eval_toplevel)
C_externimport void C_ccall C_eval_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_expand_toplevel)
C_externimport void C_ccall C_expand_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_internal_toplevel)
C_externimport void C_ccall C_internal_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_optimizer_toplevel)
C_externimport void C_ccall C_optimizer_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_support_toplevel)
C_externimport void C_ccall C_support_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_compiler_toplevel)
C_externimport void C_ccall C_compiler_toplevel(C_word c,C_word *av) C_noret;

static C_TLS C_word lf[941];
static double C_possibly_force_alignment;


C_noret_decl(f_1664)
static void C_ccall f_1664(C_word c,C_word *av) C_noret;
C_noret_decl(f_1667)
static void C_ccall f_1667(C_word c,C_word *av) C_noret;
C_noret_decl(f_1670)
static void C_ccall f_1670(C_word c,C_word *av) C_noret;
C_noret_decl(f_1673)
static void C_ccall f_1673(C_word c,C_word *av) C_noret;
C_noret_decl(f_1676)
static void C_ccall f_1676(C_word c,C_word *av) C_noret;
C_noret_decl(f_1679)
static void C_ccall f_1679(C_word c,C_word *av) C_noret;
C_noret_decl(f_1682)
static void C_ccall f_1682(C_word c,C_word *av) C_noret;
C_noret_decl(f_2117)
static void C_fcall f_2117(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_2123)
static void C_fcall f_2123(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_2137)
static void C_ccall f_2137(C_word c,C_word *av) C_noret;
C_noret_decl(f_2293)
static void C_fcall f_2293(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_2302)
static void C_fcall f_2302(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_2310)
static void C_fcall f_2310(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_2317)
static void C_ccall f_2317(C_word c,C_word *av) C_noret;
C_noret_decl(f_2331)
static void C_ccall f_2331(C_word c,C_word *av) C_noret;
C_noret_decl(f_2459)
static C_word C_fcall f_2459(C_word t0);
C_noret_decl(f_2701)
static void C_fcall f_2701(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_2715)
static void C_ccall f_2715(C_word c,C_word *av) C_noret;
C_noret_decl(f_2719)
static void C_ccall f_2719(C_word c,C_word *av) C_noret;
C_noret_decl(f_2963)
static void C_ccall f_2963(C_word c,C_word *av) C_noret;
C_noret_decl(f_2971)
static void C_ccall f_2971(C_word c,C_word *av) C_noret;
C_noret_decl(f_2974)
static void C_ccall f_2974(C_word c,C_word *av) C_noret;
C_noret_decl(f_2977)
static void C_ccall f_2977(C_word c,C_word *av) C_noret;
C_noret_decl(f_2992)
static void C_ccall f_2992(C_word c,C_word *av) C_noret;
C_noret_decl(f_2999)
static void C_ccall f_2999(C_word c,C_word *av) C_noret;
C_noret_decl(f_3008)
static void C_ccall f_3008(C_word c,C_word *av) C_noret;
C_noret_decl(f_3010)
static void C_fcall f_3010(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_3012)
static void C_ccall f_3012(C_word c,C_word *av) C_noret;
C_noret_decl(f_3034)
static void C_ccall f_3034(C_word c,C_word *av) C_noret;
C_noret_decl(f_3067)
static void C_ccall f_3067(C_word c,C_word *av) C_noret;
C_noret_decl(f_3075)
static void C_ccall f_3075(C_word c,C_word *av) C_noret;
C_noret_decl(f_3078)
static void C_ccall f_3078(C_word c,C_word *av) C_noret;
C_noret_decl(f_3080)
static void C_ccall f_3080(C_word c,C_word *av) C_noret;
C_noret_decl(f_3096)
static void C_ccall f_3096(C_word c,C_word *av) C_noret;
C_noret_decl(f_3105)
static void C_fcall f_3105(C_word t0,C_word t1) C_noret;
C_noret_decl(f_3108)
static void C_fcall f_3108(C_word t0,C_word t1) C_noret;
C_noret_decl(f_3123)
static void C_ccall f_3123(C_word c,C_word *av) C_noret;
C_noret_decl(f_3135)
static void C_ccall f_3135(C_word c,C_word *av) C_noret;
C_noret_decl(f_3149)
static void C_ccall f_3149(C_word c,C_word *av) C_noret;
C_noret_decl(f_3153)
static void C_ccall f_3153(C_word c,C_word *av) C_noret;
C_noret_decl(f_3162)
static void C_ccall f_3162(C_word c,C_word *av) C_noret;
C_noret_decl(f_3176)
static void C_ccall f_3176(C_word c,C_word *av) C_noret;
C_noret_decl(f_3180)
static void C_ccall f_3180(C_word c,C_word *av) C_noret;
C_noret_decl(f_3210)
static void C_ccall f_3210(C_word c,C_word *av) C_noret;
C_noret_decl(f_3214)
static void C_ccall f_3214(C_word c,C_word *av) C_noret;
C_noret_decl(f_3218)
static void C_ccall f_3218(C_word c,C_word *av) C_noret;
C_noret_decl(f_3222)
static void C_ccall f_3222(C_word c,C_word *av) C_noret;
C_noret_decl(f_3226)
static void C_ccall f_3226(C_word c,C_word *av) C_noret;
C_noret_decl(f_3234)
static void C_ccall f_3234(C_word c,C_word *av) C_noret;
C_noret_decl(f_3237)
static void C_ccall f_3237(C_word c,C_word *av) C_noret;
C_noret_decl(f_3240)
static void C_ccall f_3240(C_word c,C_word *av) C_noret;
C_noret_decl(f_3242)
static void C_ccall f_3242(C_word c,C_word *av) C_noret;
C_noret_decl(f_3270)
static void C_ccall f_3270(C_word c,C_word *av) C_noret;
C_noret_decl(f_3278)
static void C_ccall f_3278(C_word c,C_word *av) C_noret;
C_noret_decl(f_3295)
static void C_ccall f_3295(C_word c,C_word *av) C_noret;
C_noret_decl(f_3297)
static void C_fcall f_3297(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3322)
static void C_ccall f_3322(C_word c,C_word *av) C_noret;
C_noret_decl(f_3333)
static void C_ccall f_3333(C_word c,C_word *av) C_noret;
C_noret_decl(f_3337)
static void C_ccall f_3337(C_word c,C_word *av) C_noret;
C_noret_decl(f_3340)
static void C_ccall f_3340(C_word c,C_word *av) C_noret;
C_noret_decl(f_3354)
static void C_ccall f_3354(C_word c,C_word *av) C_noret;
C_noret_decl(f_3358)
static void C_ccall f_3358(C_word c,C_word *av) C_noret;
C_noret_decl(f_3381)
static void C_ccall f_3381(C_word c,C_word *av) C_noret;
C_noret_decl(f_3396)
static void C_ccall f_3396(C_word c,C_word *av) C_noret;
C_noret_decl(f_3404)
static void C_ccall f_3404(C_word c,C_word *av) C_noret;
C_noret_decl(f_3413)
static void C_ccall f_3413(C_word c,C_word *av) C_noret;
C_noret_decl(f_3417)
static void C_ccall f_3417(C_word c,C_word *av) C_noret;
C_noret_decl(f_3420)
static void C_ccall f_3420(C_word c,C_word *av) C_noret;
C_noret_decl(f_3423)
static void C_ccall f_3423(C_word c,C_word *av) C_noret;
C_noret_decl(f_3425)
static void C_fcall f_3425(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_3431)
static void C_ccall f_3431(C_word c,C_word *av) C_noret;
C_noret_decl(f_3443)
static void C_ccall f_3443(C_word c,C_word *av) C_noret;
C_noret_decl(f_3462)
static void C_ccall f_3462(C_word c,C_word *av) C_noret;
C_noret_decl(f_3493)
static void C_ccall f_3493(C_word c,C_word *av) C_noret;
C_noret_decl(f_3496)
static void C_ccall f_3496(C_word c,C_word *av) C_noret;
C_noret_decl(f_3499)
static void C_ccall f_3499(C_word c,C_word *av) C_noret;
C_noret_decl(f_3502)
static void C_ccall f_3502(C_word c,C_word *av) C_noret;
C_noret_decl(f_3505)
static void C_ccall f_3505(C_word c,C_word *av) C_noret;
C_noret_decl(f_3508)
static void C_ccall f_3508(C_word c,C_word *av) C_noret;
C_noret_decl(f_3509)
static void C_ccall f_3509(C_word c,C_word *av) C_noret;
C_noret_decl(f_3535)
static void C_ccall f_3535(C_word c,C_word *av) C_noret;
C_noret_decl(f_3538)
static void C_ccall f_3538(C_word c,C_word *av) C_noret;
C_noret_decl(f_3540)
static void C_ccall f_3540(C_word c,C_word *av) C_noret;
C_noret_decl(f_3574)
static void C_ccall f_3574(C_word c,C_word *av) C_noret;
C_noret_decl(f_3601)
static void C_ccall f_3601(C_word c,C_word *av) C_noret;
C_noret_decl(f_3604)
static void C_ccall f_3604(C_word c,C_word *av) C_noret;
C_noret_decl(f_3607)
static void C_ccall f_3607(C_word c,C_word *av) C_noret;
C_noret_decl(f_3622)
static void C_ccall f_3622(C_word c,C_word *av) C_noret;
C_noret_decl(f_3626)
static void C_ccall f_3626(C_word c,C_word *av) C_noret;
C_noret_decl(f_3638)
static void C_ccall f_3638(C_word c,C_word *av) C_noret;
C_noret_decl(f_3650)
static void C_ccall f_3650(C_word c,C_word *av) C_noret;
C_noret_decl(f_3662)
static void C_ccall f_3662(C_word c,C_word *av) C_noret;
C_noret_decl(f_3666)
static void C_ccall f_3666(C_word c,C_word *av) C_noret;
C_noret_decl(f_3674)
static void C_ccall f_3674(C_word c,C_word *av) C_noret;
C_noret_decl(f_3681)
static void C_ccall f_3681(C_word c,C_word *av) C_noret;
C_noret_decl(f_3685)
static void C_ccall f_3685(C_word c,C_word *av) C_noret;
C_noret_decl(f_3689)
static void C_ccall f_3689(C_word c,C_word *av) C_noret;
C_noret_decl(f_3693)
static void C_ccall f_3693(C_word c,C_word *av) C_noret;
C_noret_decl(f_3697)
static void C_ccall f_3697(C_word c,C_word *av) C_noret;
C_noret_decl(f_3705)
static void C_ccall f_3705(C_word c,C_word *av) C_noret;
C_noret_decl(f_3708)
static void C_ccall f_3708(C_word c,C_word *av) C_noret;
C_noret_decl(f_3711)
static void C_ccall f_3711(C_word c,C_word *av) C_noret;
C_noret_decl(f_3714)
static void C_ccall f_3714(C_word c,C_word *av) C_noret;
C_noret_decl(f_3717)
static void C_ccall f_3717(C_word c,C_word *av) C_noret;
C_noret_decl(f_3720)
static void C_ccall f_3720(C_word c,C_word *av) C_noret;
C_noret_decl(f_3723)
static void C_ccall f_3723(C_word c,C_word *av) C_noret;
C_noret_decl(f_3726)
static void C_ccall f_3726(C_word c,C_word *av) C_noret;
C_noret_decl(f_3729)
static void C_ccall f_3729(C_word c,C_word *av) C_noret;
C_noret_decl(f_3732)
static void C_ccall f_3732(C_word c,C_word *av) C_noret;
C_noret_decl(f_3735)
static void C_ccall f_3735(C_word c,C_word *av) C_noret;
C_noret_decl(f_3738)
static void C_ccall f_3738(C_word c,C_word *av) C_noret;
C_noret_decl(f_3741)
static void C_ccall f_3741(C_word c,C_word *av) C_noret;
C_noret_decl(f_3744)
static void C_ccall f_3744(C_word c,C_word *av) C_noret;
C_noret_decl(f_3747)
static void C_ccall f_3747(C_word c,C_word *av) C_noret;
C_noret_decl(f_3750)
static void C_ccall f_3750(C_word c,C_word *av) C_noret;
C_noret_decl(f_3753)
static void C_ccall f_3753(C_word c,C_word *av) C_noret;
C_noret_decl(f_3756)
static void C_ccall f_3756(C_word c,C_word *av) C_noret;
C_noret_decl(f_3759)
static void C_ccall f_3759(C_word c,C_word *av) C_noret;
C_noret_decl(f_3762)
static void C_ccall f_3762(C_word c,C_word *av) C_noret;
C_noret_decl(f_3765)
static void C_ccall f_3765(C_word c,C_word *av) C_noret;
C_noret_decl(f_3768)
static void C_ccall f_3768(C_word c,C_word *av) C_noret;
C_noret_decl(f_3771)
static void C_ccall f_3771(C_word c,C_word *av) C_noret;
C_noret_decl(f_3774)
static void C_ccall f_3774(C_word c,C_word *av) C_noret;
C_noret_decl(f_3777)
static void C_ccall f_3777(C_word c,C_word *av) C_noret;
C_noret_decl(f_3780)
static void C_ccall f_3780(C_word c,C_word *av) C_noret;
C_noret_decl(f_3783)
static void C_ccall f_3783(C_word c,C_word *av) C_noret;
C_noret_decl(f_3786)
static void C_ccall f_3786(C_word c,C_word *av) C_noret;
C_noret_decl(f_3789)
static void C_ccall f_3789(C_word c,C_word *av) C_noret;
C_noret_decl(f_3792)
static void C_ccall f_3792(C_word c,C_word *av) C_noret;
C_noret_decl(f_3795)
static void C_ccall f_3795(C_word c,C_word *av) C_noret;
C_noret_decl(f_3798)
static void C_ccall f_3798(C_word c,C_word *av) C_noret;
C_noret_decl(f_3801)
static void C_ccall f_3801(C_word c,C_word *av) C_noret;
C_noret_decl(f_3804)
static void C_ccall f_3804(C_word c,C_word *av) C_noret;
C_noret_decl(f_3807)
static void C_ccall f_3807(C_word c,C_word *av) C_noret;
C_noret_decl(f_3810)
static void C_ccall f_3810(C_word c,C_word *av) C_noret;
C_noret_decl(f_3813)
static void C_ccall f_3813(C_word c,C_word *av) C_noret;
C_noret_decl(f_3816)
static void C_ccall f_3816(C_word c,C_word *av) C_noret;
C_noret_decl(f_3819)
static void C_ccall f_3819(C_word c,C_word *av) C_noret;
C_noret_decl(f_3822)
static void C_ccall f_3822(C_word c,C_word *av) C_noret;
C_noret_decl(f_3825)
static void C_ccall f_3825(C_word c,C_word *av) C_noret;
C_noret_decl(f_3828)
static void C_ccall f_3828(C_word c,C_word *av) C_noret;
C_noret_decl(f_3831)
static void C_ccall f_3831(C_word c,C_word *av) C_noret;
C_noret_decl(f_3834)
static void C_ccall f_3834(C_word c,C_word *av) C_noret;
C_noret_decl(f_3837)
static void C_ccall f_3837(C_word c,C_word *av) C_noret;
C_noret_decl(f_3840)
static void C_ccall f_3840(C_word c,C_word *av) C_noret;
C_noret_decl(f_3843)
static void C_ccall f_3843(C_word c,C_word *av) C_noret;
C_noret_decl(f_3846)
static void C_ccall f_3846(C_word c,C_word *av) C_noret;
C_noret_decl(f_3849)
static void C_ccall f_3849(C_word c,C_word *av) C_noret;
C_noret_decl(f_3852)
static void C_ccall f_3852(C_word c,C_word *av) C_noret;
C_noret_decl(f_3855)
static void C_ccall f_3855(C_word c,C_word *av) C_noret;
C_noret_decl(f_3858)
static void C_ccall f_3858(C_word c,C_word *av) C_noret;
C_noret_decl(f_3861)
static void C_ccall f_3861(C_word c,C_word *av) C_noret;
C_noret_decl(f_3864)
static void C_ccall f_3864(C_word c,C_word *av) C_noret;
C_noret_decl(f_3867)
static void C_ccall f_3867(C_word c,C_word *av) C_noret;
C_noret_decl(f_3870)
static void C_ccall f_3870(C_word c,C_word *av) C_noret;
C_noret_decl(f_3873)
static void C_ccall f_3873(C_word c,C_word *av) C_noret;
C_noret_decl(f_3876)
static void C_ccall f_3876(C_word c,C_word *av) C_noret;
C_noret_decl(f_3879)
static void C_ccall f_3879(C_word c,C_word *av) C_noret;
C_noret_decl(f_3882)
static void C_ccall f_3882(C_word c,C_word *av) C_noret;
C_noret_decl(f_3885)
static void C_ccall f_3885(C_word c,C_word *av) C_noret;
C_noret_decl(f_3888)
static void C_ccall f_3888(C_word c,C_word *av) C_noret;
C_noret_decl(f_3891)
static void C_ccall f_3891(C_word c,C_word *av) C_noret;
C_noret_decl(f_3894)
static void C_ccall f_3894(C_word c,C_word *av) C_noret;
C_noret_decl(f_3897)
static void C_ccall f_3897(C_word c,C_word *av) C_noret;
C_noret_decl(f_3900)
static void C_ccall f_3900(C_word c,C_word *av) C_noret;
C_noret_decl(f_3903)
static void C_ccall f_3903(C_word c,C_word *av) C_noret;
C_noret_decl(f_3906)
static void C_ccall f_3906(C_word c,C_word *av) C_noret;
C_noret_decl(f_3909)
static void C_ccall f_3909(C_word c,C_word *av) C_noret;
C_noret_decl(f_3912)
static void C_ccall f_3912(C_word c,C_word *av) C_noret;
C_noret_decl(f_3915)
static void C_ccall f_3915(C_word c,C_word *av) C_noret;
C_noret_decl(f_3918)
static void C_ccall f_3918(C_word c,C_word *av) C_noret;
C_noret_decl(f_3921)
static void C_ccall f_3921(C_word c,C_word *av) C_noret;
C_noret_decl(f_3924)
static void C_ccall f_3924(C_word c,C_word *av) C_noret;
C_noret_decl(f_3927)
static void C_ccall f_3927(C_word c,C_word *av) C_noret;
C_noret_decl(f_3930)
static void C_ccall f_3930(C_word c,C_word *av) C_noret;
C_noret_decl(f_3933)
static void C_ccall f_3933(C_word c,C_word *av) C_noret;
C_noret_decl(f_3936)
static void C_ccall f_3936(C_word c,C_word *av) C_noret;
C_noret_decl(f_3939)
static void C_ccall f_3939(C_word c,C_word *av) C_noret;
C_noret_decl(f_3942)
static void C_ccall f_3942(C_word c,C_word *av) C_noret;
C_noret_decl(f_3945)
static void C_ccall f_3945(C_word c,C_word *av) C_noret;
C_noret_decl(f_3948)
static void C_ccall f_3948(C_word c,C_word *av) C_noret;
C_noret_decl(f_3951)
static void C_ccall f_3951(C_word c,C_word *av) C_noret;
C_noret_decl(f_3954)
static void C_ccall f_3954(C_word c,C_word *av) C_noret;
C_noret_decl(f_3957)
static void C_ccall f_3957(C_word c,C_word *av) C_noret;
C_noret_decl(f_3960)
static void C_ccall f_3960(C_word c,C_word *av) C_noret;
C_noret_decl(f_3963)
static void C_ccall f_3963(C_word c,C_word *av) C_noret;
C_noret_decl(f_3966)
static void C_ccall f_3966(C_word c,C_word *av) C_noret;
C_noret_decl(f_3969)
static void C_ccall f_3969(C_word c,C_word *av) C_noret;
C_noret_decl(f_3972)
static void C_ccall f_3972(C_word c,C_word *av) C_noret;
C_noret_decl(f_3975)
static void C_ccall f_3975(C_word c,C_word *av) C_noret;
C_noret_decl(f_3978)
static void C_ccall f_3978(C_word c,C_word *av) C_noret;
C_noret_decl(f_3981)
static void C_ccall f_3981(C_word c,C_word *av) C_noret;
C_noret_decl(f_3984)
static void C_ccall f_3984(C_word c,C_word *av) C_noret;
C_noret_decl(f_3987)
static void C_ccall f_3987(C_word c,C_word *av) C_noret;
C_noret_decl(f_3990)
static void C_ccall f_3990(C_word c,C_word *av) C_noret;
C_noret_decl(f_3993)
static void C_ccall f_3993(C_word c,C_word *av) C_noret;
C_noret_decl(f_3996)
static void C_ccall f_3996(C_word c,C_word *av) C_noret;
C_noret_decl(f_3999)
static void C_ccall f_3999(C_word c,C_word *av) C_noret;
C_noret_decl(f_4002)
static void C_ccall f_4002(C_word c,C_word *av) C_noret;
C_noret_decl(f_4005)
static void C_ccall f_4005(C_word c,C_word *av) C_noret;
C_noret_decl(f_4008)
static void C_ccall f_4008(C_word c,C_word *av) C_noret;
C_noret_decl(f_4011)
static void C_ccall f_4011(C_word c,C_word *av) C_noret;
C_noret_decl(f_4014)
static void C_ccall f_4014(C_word c,C_word *av) C_noret;
C_noret_decl(f_4017)
static void C_ccall f_4017(C_word c,C_word *av) C_noret;
C_noret_decl(f_4020)
static void C_ccall f_4020(C_word c,C_word *av) C_noret;
C_noret_decl(f_4023)
static void C_ccall f_4023(C_word c,C_word *av) C_noret;
C_noret_decl(f_4026)
static void C_ccall f_4026(C_word c,C_word *av) C_noret;
C_noret_decl(f_4029)
static void C_ccall f_4029(C_word c,C_word *av) C_noret;
C_noret_decl(f_4032)
static void C_ccall f_4032(C_word c,C_word *av) C_noret;
C_noret_decl(f_4035)
static void C_ccall f_4035(C_word c,C_word *av) C_noret;
C_noret_decl(f_4038)
static void C_ccall f_4038(C_word c,C_word *av) C_noret;
C_noret_decl(f_4041)
static void C_ccall f_4041(C_word c,C_word *av) C_noret;
C_noret_decl(f_4044)
static void C_ccall f_4044(C_word c,C_word *av) C_noret;
C_noret_decl(f_4047)
static void C_ccall f_4047(C_word c,C_word *av) C_noret;
C_noret_decl(f_4050)
static void C_ccall f_4050(C_word c,C_word *av) C_noret;
C_noret_decl(f_4053)
static void C_ccall f_4053(C_word c,C_word *av) C_noret;
C_noret_decl(f_4056)
static void C_ccall f_4056(C_word c,C_word *av) C_noret;
C_noret_decl(f_4059)
static void C_ccall f_4059(C_word c,C_word *av) C_noret;
C_noret_decl(f_4062)
static void C_ccall f_4062(C_word c,C_word *av) C_noret;
C_noret_decl(f_4065)
static void C_ccall f_4065(C_word c,C_word *av) C_noret;
C_noret_decl(f_4068)
static void C_ccall f_4068(C_word c,C_word *av) C_noret;
C_noret_decl(f_4071)
static void C_ccall f_4071(C_word c,C_word *av) C_noret;
C_noret_decl(f_4074)
static void C_ccall f_4074(C_word c,C_word *av) C_noret;
C_noret_decl(f_4077)
static void C_ccall f_4077(C_word c,C_word *av) C_noret;
C_noret_decl(f_4080)
static void C_ccall f_4080(C_word c,C_word *av) C_noret;
C_noret_decl(f_4083)
static void C_ccall f_4083(C_word c,C_word *av) C_noret;
C_noret_decl(f_4086)
static void C_ccall f_4086(C_word c,C_word *av) C_noret;
C_noret_decl(f_4089)
static void C_ccall f_4089(C_word c,C_word *av) C_noret;
C_noret_decl(f_4092)
static void C_ccall f_4092(C_word c,C_word *av) C_noret;
C_noret_decl(f_4095)
static void C_ccall f_4095(C_word c,C_word *av) C_noret;
C_noret_decl(f_4098)
static void C_ccall f_4098(C_word c,C_word *av) C_noret;
C_noret_decl(f_4101)
static void C_ccall f_4101(C_word c,C_word *av) C_noret;
C_noret_decl(f_4104)
static void C_ccall f_4104(C_word c,C_word *av) C_noret;
C_noret_decl(f_4107)
static void C_ccall f_4107(C_word c,C_word *av) C_noret;
C_noret_decl(f_4110)
static void C_ccall f_4110(C_word c,C_word *av) C_noret;
C_noret_decl(f_4113)
static void C_ccall f_4113(C_word c,C_word *av) C_noret;
C_noret_decl(f_4116)
static void C_ccall f_4116(C_word c,C_word *av) C_noret;
C_noret_decl(f_4119)
static void C_ccall f_4119(C_word c,C_word *av) C_noret;
C_noret_decl(f_4122)
static void C_ccall f_4122(C_word c,C_word *av) C_noret;
C_noret_decl(f_4125)
static void C_ccall f_4125(C_word c,C_word *av) C_noret;
C_noret_decl(f_4128)
static void C_ccall f_4128(C_word c,C_word *av) C_noret;
C_noret_decl(f_4131)
static void C_ccall f_4131(C_word c,C_word *av) C_noret;
C_noret_decl(f_4134)
static void C_ccall f_4134(C_word c,C_word *av) C_noret;
C_noret_decl(f_4137)
static void C_ccall f_4137(C_word c,C_word *av) C_noret;
C_noret_decl(f_4140)
static void C_ccall f_4140(C_word c,C_word *av) C_noret;
C_noret_decl(f_4143)
static void C_ccall f_4143(C_word c,C_word *av) C_noret;
C_noret_decl(f_4146)
static void C_ccall f_4146(C_word c,C_word *av) C_noret;
C_noret_decl(f_4149)
static void C_ccall f_4149(C_word c,C_word *av) C_noret;
C_noret_decl(f_4152)
static void C_ccall f_4152(C_word c,C_word *av) C_noret;
C_noret_decl(f_4155)
static void C_ccall f_4155(C_word c,C_word *av) C_noret;
C_noret_decl(f_4158)
static void C_ccall f_4158(C_word c,C_word *av) C_noret;
C_noret_decl(f_4161)
static void C_ccall f_4161(C_word c,C_word *av) C_noret;
C_noret_decl(f_4164)
static void C_ccall f_4164(C_word c,C_word *av) C_noret;
C_noret_decl(f_4167)
static void C_ccall f_4167(C_word c,C_word *av) C_noret;
C_noret_decl(f_4170)
static void C_ccall f_4170(C_word c,C_word *av) C_noret;
C_noret_decl(f_4173)
static void C_ccall f_4173(C_word c,C_word *av) C_noret;
C_noret_decl(f_4176)
static void C_ccall f_4176(C_word c,C_word *av) C_noret;
C_noret_decl(f_4179)
static void C_ccall f_4179(C_word c,C_word *av) C_noret;
C_noret_decl(f_4182)
static void C_ccall f_4182(C_word c,C_word *av) C_noret;
C_noret_decl(f_4185)
static void C_ccall f_4185(C_word c,C_word *av) C_noret;
C_noret_decl(f_4188)
static void C_ccall f_4188(C_word c,C_word *av) C_noret;
C_noret_decl(f_4191)
static void C_ccall f_4191(C_word c,C_word *av) C_noret;
C_noret_decl(f_4194)
static void C_ccall f_4194(C_word c,C_word *av) C_noret;
C_noret_decl(f_4197)
static void C_ccall f_4197(C_word c,C_word *av) C_noret;
C_noret_decl(f_4200)
static void C_ccall f_4200(C_word c,C_word *av) C_noret;
C_noret_decl(f_4203)
static void C_ccall f_4203(C_word c,C_word *av) C_noret;
C_noret_decl(f_4206)
static void C_ccall f_4206(C_word c,C_word *av) C_noret;
C_noret_decl(f_4209)
static void C_ccall f_4209(C_word c,C_word *av) C_noret;
C_noret_decl(f_4212)
static void C_ccall f_4212(C_word c,C_word *av) C_noret;
C_noret_decl(f_4215)
static void C_ccall f_4215(C_word c,C_word *av) C_noret;
C_noret_decl(f_4218)
static void C_ccall f_4218(C_word c,C_word *av) C_noret;
C_noret_decl(f_4221)
static void C_ccall f_4221(C_word c,C_word *av) C_noret;
C_noret_decl(f_4224)
static void C_ccall f_4224(C_word c,C_word *av) C_noret;
C_noret_decl(f_4227)
static void C_ccall f_4227(C_word c,C_word *av) C_noret;
C_noret_decl(f_4230)
static void C_ccall f_4230(C_word c,C_word *av) C_noret;
C_noret_decl(f_4233)
static void C_ccall f_4233(C_word c,C_word *av) C_noret;
C_noret_decl(f_4236)
static void C_ccall f_4236(C_word c,C_word *av) C_noret;
C_noret_decl(f_4239)
static void C_ccall f_4239(C_word c,C_word *av) C_noret;
C_noret_decl(f_4242)
static void C_ccall f_4242(C_word c,C_word *av) C_noret;
C_noret_decl(f_4245)
static void C_ccall f_4245(C_word c,C_word *av) C_noret;
C_noret_decl(f_4248)
static void C_ccall f_4248(C_word c,C_word *av) C_noret;
C_noret_decl(f_4251)
static void C_ccall f_4251(C_word c,C_word *av) C_noret;
C_noret_decl(f_4254)
static void C_ccall f_4254(C_word c,C_word *av) C_noret;
C_noret_decl(f_4257)
static void C_ccall f_4257(C_word c,C_word *av) C_noret;
C_noret_decl(f_4260)
static void C_ccall f_4260(C_word c,C_word *av) C_noret;
C_noret_decl(f_4263)
static void C_ccall f_4263(C_word c,C_word *av) C_noret;
C_noret_decl(f_4266)
static void C_ccall f_4266(C_word c,C_word *av) C_noret;
C_noret_decl(f_4269)
static void C_ccall f_4269(C_word c,C_word *av) C_noret;
C_noret_decl(f_4272)
static void C_ccall f_4272(C_word c,C_word *av) C_noret;
C_noret_decl(f_4275)
static void C_ccall f_4275(C_word c,C_word *av) C_noret;
C_noret_decl(f_4278)
static void C_ccall f_4278(C_word c,C_word *av) C_noret;
C_noret_decl(f_4281)
static void C_ccall f_4281(C_word c,C_word *av) C_noret;
C_noret_decl(f_4284)
static void C_ccall f_4284(C_word c,C_word *av) C_noret;
C_noret_decl(f_4287)
static void C_ccall f_4287(C_word c,C_word *av) C_noret;
C_noret_decl(f_4290)
static void C_ccall f_4290(C_word c,C_word *av) C_noret;
C_noret_decl(f_4293)
static void C_ccall f_4293(C_word c,C_word *av) C_noret;
C_noret_decl(f_4296)
static void C_ccall f_4296(C_word c,C_word *av) C_noret;
C_noret_decl(f_4299)
static void C_ccall f_4299(C_word c,C_word *av) C_noret;
C_noret_decl(f_4302)
static void C_ccall f_4302(C_word c,C_word *av) C_noret;
C_noret_decl(f_4305)
static void C_ccall f_4305(C_word c,C_word *av) C_noret;
C_noret_decl(f_4308)
static void C_ccall f_4308(C_word c,C_word *av) C_noret;
C_noret_decl(f_4311)
static void C_ccall f_4311(C_word c,C_word *av) C_noret;
C_noret_decl(f_4314)
static void C_ccall f_4314(C_word c,C_word *av) C_noret;
C_noret_decl(f_4317)
static void C_ccall f_4317(C_word c,C_word *av) C_noret;
C_noret_decl(f_4320)
static void C_ccall f_4320(C_word c,C_word *av) C_noret;
C_noret_decl(f_4323)
static void C_ccall f_4323(C_word c,C_word *av) C_noret;
C_noret_decl(f_4326)
static void C_ccall f_4326(C_word c,C_word *av) C_noret;
C_noret_decl(f_4329)
static void C_ccall f_4329(C_word c,C_word *av) C_noret;
C_noret_decl(f_4332)
static void C_ccall f_4332(C_word c,C_word *av) C_noret;
C_noret_decl(f_4335)
static void C_ccall f_4335(C_word c,C_word *av) C_noret;
C_noret_decl(f_4338)
static void C_ccall f_4338(C_word c,C_word *av) C_noret;
C_noret_decl(f_4341)
static void C_ccall f_4341(C_word c,C_word *av) C_noret;
C_noret_decl(f_4344)
static void C_ccall f_4344(C_word c,C_word *av) C_noret;
C_noret_decl(f_4347)
static void C_ccall f_4347(C_word c,C_word *av) C_noret;
C_noret_decl(f_4350)
static void C_ccall f_4350(C_word c,C_word *av) C_noret;
C_noret_decl(f_4353)
static void C_ccall f_4353(C_word c,C_word *av) C_noret;
C_noret_decl(f_4356)
static void C_ccall f_4356(C_word c,C_word *av) C_noret;
C_noret_decl(f_4359)
static void C_ccall f_4359(C_word c,C_word *av) C_noret;
C_noret_decl(f_4362)
static void C_ccall f_4362(C_word c,C_word *av) C_noret;
C_noret_decl(f_4365)
static void C_ccall f_4365(C_word c,C_word *av) C_noret;
C_noret_decl(f_4368)
static void C_ccall f_4368(C_word c,C_word *av) C_noret;
C_noret_decl(f_4371)
static void C_ccall f_4371(C_word c,C_word *av) C_noret;
C_noret_decl(f_4374)
static void C_ccall f_4374(C_word c,C_word *av) C_noret;
C_noret_decl(f_4377)
static void C_ccall f_4377(C_word c,C_word *av) C_noret;
C_noret_decl(f_4380)
static void C_ccall f_4380(C_word c,C_word *av) C_noret;
C_noret_decl(f_4383)
static void C_ccall f_4383(C_word c,C_word *av) C_noret;
C_noret_decl(f_4386)
static void C_ccall f_4386(C_word c,C_word *av) C_noret;
C_noret_decl(f_4389)
static void C_ccall f_4389(C_word c,C_word *av) C_noret;
C_noret_decl(f_4392)
static void C_ccall f_4392(C_word c,C_word *av) C_noret;
C_noret_decl(f_4395)
static void C_ccall f_4395(C_word c,C_word *av) C_noret;
C_noret_decl(f_4398)
static void C_ccall f_4398(C_word c,C_word *av) C_noret;
C_noret_decl(f_4401)
static void C_ccall f_4401(C_word c,C_word *av) C_noret;
C_noret_decl(f_4404)
static void C_ccall f_4404(C_word c,C_word *av) C_noret;
C_noret_decl(f_4407)
static void C_ccall f_4407(C_word c,C_word *av) C_noret;
C_noret_decl(f_4410)
static void C_ccall f_4410(C_word c,C_word *av) C_noret;
C_noret_decl(f_4413)
static void C_ccall f_4413(C_word c,C_word *av) C_noret;
C_noret_decl(f_4416)
static void C_ccall f_4416(C_word c,C_word *av) C_noret;
C_noret_decl(f_4419)
static void C_ccall f_4419(C_word c,C_word *av) C_noret;
C_noret_decl(f_4422)
static void C_ccall f_4422(C_word c,C_word *av) C_noret;
C_noret_decl(f_4425)
static void C_ccall f_4425(C_word c,C_word *av) C_noret;
C_noret_decl(f_4428)
static void C_ccall f_4428(C_word c,C_word *av) C_noret;
C_noret_decl(f_4431)
static void C_ccall f_4431(C_word c,C_word *av) C_noret;
C_noret_decl(f_4434)
static void C_ccall f_4434(C_word c,C_word *av) C_noret;
C_noret_decl(f_4437)
static void C_ccall f_4437(C_word c,C_word *av) C_noret;
C_noret_decl(f_4439)
static void C_ccall f_4439(C_word c,C_word *av) C_noret;
C_noret_decl(f_4476)
static void C_ccall f_4476(C_word c,C_word *av) C_noret;
C_noret_decl(f_4478)
static void C_ccall f_4478(C_word c,C_word *av) C_noret;
C_noret_decl(f_4485)
static void C_fcall f_4485(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4496)
static void C_ccall f_4496(C_word c,C_word *av) C_noret;
C_noret_decl(f_4517)
static void C_ccall f_4517(C_word c,C_word *av) C_noret;
C_noret_decl(f_4521)
static void C_ccall f_4521(C_word c,C_word *av) C_noret;
C_noret_decl(f_4534)
static void C_ccall f_4534(C_word c,C_word *av) C_noret;
C_noret_decl(f_4536)
static void C_ccall f_4536(C_word c,C_word *av) C_noret;
C_noret_decl(f_4558)
static void C_ccall f_4558(C_word c,C_word *av) C_noret;
C_noret_decl(f_4562)
static void C_ccall f_4562(C_word c,C_word *av) C_noret;
C_noret_decl(f_4572)
static void C_ccall f_4572(C_word c,C_word *av) C_noret;
C_noret_decl(f_4575)
static void C_ccall f_4575(C_word c,C_word *av) C_noret;
C_noret_decl(f_4578)
static void C_ccall f_4578(C_word c,C_word *av) C_noret;
C_noret_decl(f_4581)
static void C_ccall f_4581(C_word c,C_word *av) C_noret;
C_noret_decl(f_4584)
static void C_ccall f_4584(C_word c,C_word *av) C_noret;
C_noret_decl(f_4587)
static void C_ccall f_4587(C_word c,C_word *av) C_noret;
C_noret_decl(f_4590)
static void C_ccall f_4590(C_word c,C_word *av) C_noret;
C_noret_decl(f_4593)
static void C_ccall f_4593(C_word c,C_word *av) C_noret;
C_noret_decl(f_4596)
static void C_ccall f_4596(C_word c,C_word *av) C_noret;
C_noret_decl(f_4599)
static void C_ccall f_4599(C_word c,C_word *av) C_noret;
C_noret_decl(f_4602)
static void C_ccall f_4602(C_word c,C_word *av) C_noret;
C_noret_decl(f_4605)
static void C_ccall f_4605(C_word c,C_word *av) C_noret;
C_noret_decl(f_4608)
static void C_ccall f_4608(C_word c,C_word *av) C_noret;
C_noret_decl(f_4611)
static void C_ccall f_4611(C_word c,C_word *av) C_noret;
C_noret_decl(f_4614)
static void C_ccall f_4614(C_word c,C_word *av) C_noret;
C_noret_decl(f_4617)
static void C_ccall f_4617(C_word c,C_word *av) C_noret;
C_noret_decl(f_4620)
static void C_ccall f_4620(C_word c,C_word *av) C_noret;
C_noret_decl(f_4623)
static void C_ccall f_4623(C_word c,C_word *av) C_noret;
C_noret_decl(f_4626)
static void C_ccall f_4626(C_word c,C_word *av) C_noret;
C_noret_decl(f_4629)
static void C_ccall f_4629(C_word c,C_word *av) C_noret;
C_noret_decl(f_4632)
static void C_ccall f_4632(C_word c,C_word *av) C_noret;
C_noret_decl(f_4635)
static void C_ccall f_4635(C_word c,C_word *av) C_noret;
C_noret_decl(f_4638)
static void C_ccall f_4638(C_word c,C_word *av) C_noret;
C_noret_decl(f_4641)
static void C_ccall f_4641(C_word c,C_word *av) C_noret;
C_noret_decl(f_4644)
static void C_ccall f_4644(C_word c,C_word *av) C_noret;
C_noret_decl(f_4647)
static void C_ccall f_4647(C_word c,C_word *av) C_noret;
C_noret_decl(f_4650)
static void C_ccall f_4650(C_word c,C_word *av) C_noret;
C_noret_decl(f_4653)
static void C_ccall f_4653(C_word c,C_word *av) C_noret;
C_noret_decl(f_4656)
static void C_ccall f_4656(C_word c,C_word *av) C_noret;
C_noret_decl(f_4659)
static void C_ccall f_4659(C_word c,C_word *av) C_noret;
C_noret_decl(f_4662)
static void C_ccall f_4662(C_word c,C_word *av) C_noret;
C_noret_decl(f_4665)
static void C_ccall f_4665(C_word c,C_word *av) C_noret;
C_noret_decl(f_4668)
static void C_ccall f_4668(C_word c,C_word *av) C_noret;
C_noret_decl(f_4671)
static void C_ccall f_4671(C_word c,C_word *av) C_noret;
C_noret_decl(f_4674)
static void C_ccall f_4674(C_word c,C_word *av) C_noret;
C_noret_decl(f_4677)
static void C_ccall f_4677(C_word c,C_word *av) C_noret;
C_noret_decl(f_4680)
static void C_ccall f_4680(C_word c,C_word *av) C_noret;
C_noret_decl(f_4683)
static void C_ccall f_4683(C_word c,C_word *av) C_noret;
C_noret_decl(f_4686)
static void C_ccall f_4686(C_word c,C_word *av) C_noret;
C_noret_decl(f_4689)
static void C_ccall f_4689(C_word c,C_word *av) C_noret;
C_noret_decl(f_4692)
static void C_ccall f_4692(C_word c,C_word *av) C_noret;
C_noret_decl(f_4695)
static void C_ccall f_4695(C_word c,C_word *av) C_noret;
C_noret_decl(f_4698)
static void C_ccall f_4698(C_word c,C_word *av) C_noret;
C_noret_decl(f_4701)
static void C_ccall f_4701(C_word c,C_word *av) C_noret;
C_noret_decl(f_4704)
static void C_ccall f_4704(C_word c,C_word *av) C_noret;
C_noret_decl(f_4707)
static void C_ccall f_4707(C_word c,C_word *av) C_noret;
C_noret_decl(f_4710)
static void C_ccall f_4710(C_word c,C_word *av) C_noret;
C_noret_decl(f_4713)
static void C_ccall f_4713(C_word c,C_word *av) C_noret;
C_noret_decl(f_4716)
static void C_ccall f_4716(C_word c,C_word *av) C_noret;
C_noret_decl(f_4719)
static void C_ccall f_4719(C_word c,C_word *av) C_noret;
C_noret_decl(f_4722)
static void C_ccall f_4722(C_word c,C_word *av) C_noret;
C_noret_decl(f_4725)
static void C_ccall f_4725(C_word c,C_word *av) C_noret;
C_noret_decl(f_4728)
static void C_ccall f_4728(C_word c,C_word *av) C_noret;
C_noret_decl(f_4731)
static void C_ccall f_4731(C_word c,C_word *av) C_noret;
C_noret_decl(f_4734)
static void C_ccall f_4734(C_word c,C_word *av) C_noret;
C_noret_decl(f_4737)
static void C_ccall f_4737(C_word c,C_word *av) C_noret;
C_noret_decl(f_4740)
static void C_ccall f_4740(C_word c,C_word *av) C_noret;
C_noret_decl(f_4743)
static void C_ccall f_4743(C_word c,C_word *av) C_noret;
C_noret_decl(f_4746)
static void C_ccall f_4746(C_word c,C_word *av) C_noret;
C_noret_decl(f_4749)
static void C_ccall f_4749(C_word c,C_word *av) C_noret;
C_noret_decl(f_4752)
static void C_ccall f_4752(C_word c,C_word *av) C_noret;
C_noret_decl(f_4755)
static void C_ccall f_4755(C_word c,C_word *av) C_noret;
C_noret_decl(f_4758)
static void C_ccall f_4758(C_word c,C_word *av) C_noret;
C_noret_decl(f_4761)
static void C_ccall f_4761(C_word c,C_word *av) C_noret;
C_noret_decl(f_4764)
static void C_ccall f_4764(C_word c,C_word *av) C_noret;
C_noret_decl(f_4767)
static void C_ccall f_4767(C_word c,C_word *av) C_noret;
C_noret_decl(f_4770)
static void C_ccall f_4770(C_word c,C_word *av) C_noret;
C_noret_decl(f_4773)
static void C_ccall f_4773(C_word c,C_word *av) C_noret;
C_noret_decl(f_4776)
static void C_ccall f_4776(C_word c,C_word *av) C_noret;
C_noret_decl(f_4779)
static void C_ccall f_4779(C_word c,C_word *av) C_noret;
C_noret_decl(f_4782)
static void C_ccall f_4782(C_word c,C_word *av) C_noret;
C_noret_decl(f_4785)
static void C_ccall f_4785(C_word c,C_word *av) C_noret;
C_noret_decl(f_4788)
static void C_ccall f_4788(C_word c,C_word *av) C_noret;
C_noret_decl(f_4791)
static void C_ccall f_4791(C_word c,C_word *av) C_noret;
C_noret_decl(f_4794)
static void C_ccall f_4794(C_word c,C_word *av) C_noret;
C_noret_decl(f_4797)
static void C_ccall f_4797(C_word c,C_word *av) C_noret;
C_noret_decl(f_4800)
static void C_ccall f_4800(C_word c,C_word *av) C_noret;
C_noret_decl(f_4803)
static void C_ccall f_4803(C_word c,C_word *av) C_noret;
C_noret_decl(f_4806)
static void C_ccall f_4806(C_word c,C_word *av) C_noret;
C_noret_decl(f_4809)
static void C_ccall f_4809(C_word c,C_word *av) C_noret;
C_noret_decl(f_4812)
static void C_ccall f_4812(C_word c,C_word *av) C_noret;
C_noret_decl(f_4815)
static void C_ccall f_4815(C_word c,C_word *av) C_noret;
C_noret_decl(f_4818)
static void C_ccall f_4818(C_word c,C_word *av) C_noret;
C_noret_decl(f_4821)
static void C_ccall f_4821(C_word c,C_word *av) C_noret;
C_noret_decl(f_4824)
static void C_ccall f_4824(C_word c,C_word *av) C_noret;
C_noret_decl(f_4827)
static void C_ccall f_4827(C_word c,C_word *av) C_noret;
C_noret_decl(f_4830)
static void C_ccall f_4830(C_word c,C_word *av) C_noret;
C_noret_decl(f_4833)
static void C_ccall f_4833(C_word c,C_word *av) C_noret;
C_noret_decl(f_4836)
static void C_ccall f_4836(C_word c,C_word *av) C_noret;
C_noret_decl(f_4839)
static void C_ccall f_4839(C_word c,C_word *av) C_noret;
C_noret_decl(f_4842)
static void C_ccall f_4842(C_word c,C_word *av) C_noret;
C_noret_decl(f_4845)
static void C_ccall f_4845(C_word c,C_word *av) C_noret;
C_noret_decl(f_4848)
static void C_ccall f_4848(C_word c,C_word *av) C_noret;
C_noret_decl(f_4851)
static void C_ccall f_4851(C_word c,C_word *av) C_noret;
C_noret_decl(f_4854)
static void C_ccall f_4854(C_word c,C_word *av) C_noret;
C_noret_decl(f_4857)
static void C_ccall f_4857(C_word c,C_word *av) C_noret;
C_noret_decl(f_4860)
static void C_ccall f_4860(C_word c,C_word *av) C_noret;
C_noret_decl(f_4863)
static void C_ccall f_4863(C_word c,C_word *av) C_noret;
C_noret_decl(f_4866)
static void C_ccall f_4866(C_word c,C_word *av) C_noret;
C_noret_decl(f_4869)
static void C_ccall f_4869(C_word c,C_word *av) C_noret;
C_noret_decl(f_4872)
static void C_ccall f_4872(C_word c,C_word *av) C_noret;
C_noret_decl(f_4875)
static void C_ccall f_4875(C_word c,C_word *av) C_noret;
C_noret_decl(f_4878)
static void C_ccall f_4878(C_word c,C_word *av) C_noret;
C_noret_decl(f_4881)
static void C_ccall f_4881(C_word c,C_word *av) C_noret;
C_noret_decl(f_4884)
static void C_ccall f_4884(C_word c,C_word *av) C_noret;
C_noret_decl(f_4887)
static void C_ccall f_4887(C_word c,C_word *av) C_noret;
C_noret_decl(f_4890)
static void C_ccall f_4890(C_word c,C_word *av) C_noret;
C_noret_decl(f_4893)
static void C_ccall f_4893(C_word c,C_word *av) C_noret;
C_noret_decl(f_4896)
static void C_ccall f_4896(C_word c,C_word *av) C_noret;
C_noret_decl(f_4899)
static void C_ccall f_4899(C_word c,C_word *av) C_noret;
C_noret_decl(f_4902)
static void C_ccall f_4902(C_word c,C_word *av) C_noret;
C_noret_decl(f_4905)
static void C_ccall f_4905(C_word c,C_word *av) C_noret;
C_noret_decl(f_4908)
static void C_ccall f_4908(C_word c,C_word *av) C_noret;
C_noret_decl(f_4911)
static void C_ccall f_4911(C_word c,C_word *av) C_noret;
C_noret_decl(f_4914)
static void C_ccall f_4914(C_word c,C_word *av) C_noret;
C_noret_decl(f_4917)
static void C_ccall f_4917(C_word c,C_word *av) C_noret;
C_noret_decl(f_4920)
static void C_ccall f_4920(C_word c,C_word *av) C_noret;
C_noret_decl(f_4923)
static void C_ccall f_4923(C_word c,C_word *av) C_noret;
C_noret_decl(f_4926)
static void C_ccall f_4926(C_word c,C_word *av) C_noret;
C_noret_decl(f_4929)
static void C_ccall f_4929(C_word c,C_word *av) C_noret;
C_noret_decl(f_4932)
static void C_ccall f_4932(C_word c,C_word *av) C_noret;
C_noret_decl(f_4935)
static void C_ccall f_4935(C_word c,C_word *av) C_noret;
C_noret_decl(f_4938)
static void C_ccall f_4938(C_word c,C_word *av) C_noret;
C_noret_decl(f_4941)
static void C_ccall f_4941(C_word c,C_word *av) C_noret;
C_noret_decl(f_4944)
static void C_ccall f_4944(C_word c,C_word *av) C_noret;
C_noret_decl(f_4947)
static void C_ccall f_4947(C_word c,C_word *av) C_noret;
C_noret_decl(f_4950)
static void C_ccall f_4950(C_word c,C_word *av) C_noret;
C_noret_decl(f_4953)
static void C_ccall f_4953(C_word c,C_word *av) C_noret;
C_noret_decl(f_4956)
static void C_ccall f_4956(C_word c,C_word *av) C_noret;
C_noret_decl(f_4959)
static void C_ccall f_4959(C_word c,C_word *av) C_noret;
C_noret_decl(f_4962)
static void C_ccall f_4962(C_word c,C_word *av) C_noret;
C_noret_decl(f_4965)
static void C_ccall f_4965(C_word c,C_word *av) C_noret;
C_noret_decl(f_4968)
static void C_ccall f_4968(C_word c,C_word *av) C_noret;
C_noret_decl(f_4971)
static void C_ccall f_4971(C_word c,C_word *av) C_noret;
C_noret_decl(f_4974)
static void C_ccall f_4974(C_word c,C_word *av) C_noret;
C_noret_decl(f_4977)
static void C_ccall f_4977(C_word c,C_word *av) C_noret;
C_noret_decl(f_4980)
static void C_ccall f_4980(C_word c,C_word *av) C_noret;
C_noret_decl(f_4983)
static void C_ccall f_4983(C_word c,C_word *av) C_noret;
C_noret_decl(f_4986)
static void C_ccall f_4986(C_word c,C_word *av) C_noret;
C_noret_decl(f_4989)
static void C_ccall f_4989(C_word c,C_word *av) C_noret;
C_noret_decl(f_4992)
static void C_ccall f_4992(C_word c,C_word *av) C_noret;
C_noret_decl(f_4995)
static void C_ccall f_4995(C_word c,C_word *av) C_noret;
C_noret_decl(f_4998)
static void C_ccall f_4998(C_word c,C_word *av) C_noret;
C_noret_decl(f_5001)
static void C_ccall f_5001(C_word c,C_word *av) C_noret;
C_noret_decl(f_5004)
static void C_ccall f_5004(C_word c,C_word *av) C_noret;
C_noret_decl(f_5007)
static void C_ccall f_5007(C_word c,C_word *av) C_noret;
C_noret_decl(f_5010)
static void C_ccall f_5010(C_word c,C_word *av) C_noret;
C_noret_decl(f_5013)
static void C_ccall f_5013(C_word c,C_word *av) C_noret;
C_noret_decl(f_5016)
static void C_ccall f_5016(C_word c,C_word *av) C_noret;
C_noret_decl(f_5019)
static void C_ccall f_5019(C_word c,C_word *av) C_noret;
C_noret_decl(f_5022)
static void C_ccall f_5022(C_word c,C_word *av) C_noret;
C_noret_decl(f_5025)
static void C_ccall f_5025(C_word c,C_word *av) C_noret;
C_noret_decl(f_5028)
static void C_ccall f_5028(C_word c,C_word *av) C_noret;
C_noret_decl(f_5031)
static void C_ccall f_5031(C_word c,C_word *av) C_noret;
C_noret_decl(f_5034)
static void C_ccall f_5034(C_word c,C_word *av) C_noret;
C_noret_decl(f_5037)
static void C_ccall f_5037(C_word c,C_word *av) C_noret;
C_noret_decl(f_5040)
static void C_ccall f_5040(C_word c,C_word *av) C_noret;
C_noret_decl(f_5043)
static void C_ccall f_5043(C_word c,C_word *av) C_noret;
C_noret_decl(f_5046)
static void C_ccall f_5046(C_word c,C_word *av) C_noret;
C_noret_decl(f_5049)
static void C_ccall f_5049(C_word c,C_word *av) C_noret;
C_noret_decl(f_5052)
static void C_ccall f_5052(C_word c,C_word *av) C_noret;
C_noret_decl(f_5055)
static void C_ccall f_5055(C_word c,C_word *av) C_noret;
C_noret_decl(f_5058)
static void C_ccall f_5058(C_word c,C_word *av) C_noret;
C_noret_decl(f_5061)
static void C_ccall f_5061(C_word c,C_word *av) C_noret;
C_noret_decl(f_5064)
static void C_ccall f_5064(C_word c,C_word *av) C_noret;
C_noret_decl(f_5067)
static void C_ccall f_5067(C_word c,C_word *av) C_noret;
C_noret_decl(f_5070)
static void C_ccall f_5070(C_word c,C_word *av) C_noret;
C_noret_decl(f_5073)
static void C_ccall f_5073(C_word c,C_word *av) C_noret;
C_noret_decl(f_5076)
static void C_ccall f_5076(C_word c,C_word *av) C_noret;
C_noret_decl(f_5079)
static void C_ccall f_5079(C_word c,C_word *av) C_noret;
C_noret_decl(f_5082)
static void C_ccall f_5082(C_word c,C_word *av) C_noret;
C_noret_decl(f_5085)
static void C_ccall f_5085(C_word c,C_word *av) C_noret;
C_noret_decl(f_5088)
static void C_ccall f_5088(C_word c,C_word *av) C_noret;
C_noret_decl(f_5091)
static void C_ccall f_5091(C_word c,C_word *av) C_noret;
C_noret_decl(f_5094)
static void C_ccall f_5094(C_word c,C_word *av) C_noret;
C_noret_decl(f_5097)
static void C_ccall f_5097(C_word c,C_word *av) C_noret;
C_noret_decl(f_5100)
static void C_ccall f_5100(C_word c,C_word *av) C_noret;
C_noret_decl(f_5103)
static void C_ccall f_5103(C_word c,C_word *av) C_noret;
C_noret_decl(f_5106)
static void C_ccall f_5106(C_word c,C_word *av) C_noret;
C_noret_decl(f_5109)
static void C_ccall f_5109(C_word c,C_word *av) C_noret;
C_noret_decl(f_5112)
static void C_ccall f_5112(C_word c,C_word *av) C_noret;
C_noret_decl(f_5115)
static void C_ccall f_5115(C_word c,C_word *av) C_noret;
C_noret_decl(f_5118)
static void C_ccall f_5118(C_word c,C_word *av) C_noret;
C_noret_decl(f_5121)
static void C_ccall f_5121(C_word c,C_word *av) C_noret;
C_noret_decl(f_5124)
static void C_ccall f_5124(C_word c,C_word *av) C_noret;
C_noret_decl(f_5127)
static void C_ccall f_5127(C_word c,C_word *av) C_noret;
C_noret_decl(f_5130)
static void C_ccall f_5130(C_word c,C_word *av) C_noret;
C_noret_decl(f_5133)
static void C_ccall f_5133(C_word c,C_word *av) C_noret;
C_noret_decl(f_5136)
static void C_ccall f_5136(C_word c,C_word *av) C_noret;
C_noret_decl(f_5139)
static void C_ccall f_5139(C_word c,C_word *av) C_noret;
C_noret_decl(f_5142)
static void C_ccall f_5142(C_word c,C_word *av) C_noret;
C_noret_decl(f_5144)
static void C_ccall f_5144(C_word c,C_word *av) C_noret;
C_noret_decl(f_5166)
static void C_ccall f_5166(C_word c,C_word *av) C_noret;
C_noret_decl(f_5181)
static void C_ccall f_5181(C_word c,C_word *av) C_noret;
C_noret_decl(f_5184)
static void C_ccall f_5184(C_word c,C_word *av) C_noret;
C_noret_decl(f_5199)
static void C_ccall f_5199(C_word c,C_word *av) C_noret;
C_noret_decl(f_5211)
static void C_ccall f_5211(C_word c,C_word *av) C_noret;
C_noret_decl(f_5219)
static void C_ccall f_5219(C_word c,C_word *av) C_noret;
C_noret_decl(f_5221)
static void C_fcall f_5221(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5242)
static void C_ccall f_5242(C_word c,C_word *av) C_noret;
C_noret_decl(f_5246)
static void C_ccall f_5246(C_word c,C_word *av) C_noret;
C_noret_decl(f_5249)
static void C_ccall f_5249(C_word c,C_word *av) C_noret;
C_noret_decl(f_5252)
static void C_ccall f_5252(C_word c,C_word *av) C_noret;
C_noret_decl(f_5254)
static void C_ccall f_5254(C_word c,C_word *av) C_noret;
C_noret_decl(f_5273)
static void C_ccall f_5273(C_word c,C_word *av) C_noret;
C_noret_decl(f_5290)
static void C_ccall f_5290(C_word c,C_word *av) C_noret;
C_noret_decl(f_5333)
static void C_ccall f_5333(C_word c,C_word *av) C_noret;
C_noret_decl(f_5337)
static void C_ccall f_5337(C_word c,C_word *av) C_noret;
C_noret_decl(f_5341)
static void C_ccall f_5341(C_word c,C_word *av) C_noret;
C_noret_decl(f_5345)
static void C_ccall f_5345(C_word c,C_word *av) C_noret;
C_noret_decl(f_5352)
static void C_ccall f_5352(C_word c,C_word *av) C_noret;
C_noret_decl(f_5356)
static void C_ccall f_5356(C_word c,C_word *av) C_noret;
C_noret_decl(f_5364)
static void C_ccall f_5364(C_word c,C_word *av) C_noret;
C_noret_decl(f_5368)
static void C_ccall f_5368(C_word c,C_word *av) C_noret;
C_noret_decl(f_5376)
static void C_ccall f_5376(C_word c,C_word *av) C_noret;
C_noret_decl(f_5379)
static void C_ccall f_5379(C_word c,C_word *av) C_noret;
C_noret_decl(f_5383)
static void C_ccall f_5383(C_word c,C_word *av) C_noret;
C_noret_decl(f_5386)
static void C_ccall f_5386(C_word c,C_word *av) C_noret;
C_noret_decl(f_5389)
static void C_ccall f_5389(C_word c,C_word *av) C_noret;
C_noret_decl(f_5392)
static void C_ccall f_5392(C_word c,C_word *av) C_noret;
C_noret_decl(f_5395)
static void C_ccall f_5395(C_word c,C_word *av) C_noret;
C_noret_decl(f_5398)
static void C_ccall f_5398(C_word c,C_word *av) C_noret;
C_noret_decl(f_5401)
static void C_ccall f_5401(C_word c,C_word *av) C_noret;
C_noret_decl(f_5404)
static void C_ccall f_5404(C_word c,C_word *av) C_noret;
C_noret_decl(f_5407)
static void C_ccall f_5407(C_word c,C_word *av) C_noret;
C_noret_decl(f_5410)
static void C_ccall f_5410(C_word c,C_word *av) C_noret;
C_noret_decl(f_5413)
static void C_ccall f_5413(C_word c,C_word *av) C_noret;
C_noret_decl(f_5416)
static void C_ccall f_5416(C_word c,C_word *av) C_noret;
C_noret_decl(f_5419)
static void C_ccall f_5419(C_word c,C_word *av) C_noret;
C_noret_decl(f_5422)
static void C_ccall f_5422(C_word c,C_word *av) C_noret;
C_noret_decl(f_5425)
static void C_ccall f_5425(C_word c,C_word *av) C_noret;
C_noret_decl(f_5428)
static void C_ccall f_5428(C_word c,C_word *av) C_noret;
C_noret_decl(f_5430)
static void C_ccall f_5430(C_word c,C_word *av) C_noret;
C_noret_decl(f_5452)
static void C_ccall f_5452(C_word c,C_word *av) C_noret;
C_noret_decl(f_5470)
static void C_ccall f_5470(C_word c,C_word *av) C_noret;
C_noret_decl(f_5492)
static void C_ccall f_5492(C_word c,C_word *av) C_noret;
C_noret_decl(f_5510)
static void C_ccall f_5510(C_word c,C_word *av) C_noret;
C_noret_decl(f_5535)
static void C_ccall f_5535(C_word c,C_word *av) C_noret;
C_noret_decl(f_5556)
static void C_ccall f_5556(C_word c,C_word *av) C_noret;
C_noret_decl(f_5564)
static void C_ccall f_5564(C_word c,C_word *av) C_noret;
C_noret_decl(f_5568)
static void C_ccall f_5568(C_word c,C_word *av) C_noret;
C_noret_decl(f_5575)
static void C_ccall f_5575(C_word c,C_word *av) C_noret;
C_noret_decl(f_5603)
static void C_ccall f_5603(C_word c,C_word *av) C_noret;
C_noret_decl(f_5606)
static void C_ccall f_5606(C_word c,C_word *av) C_noret;
C_noret_decl(f_5637)
static void C_fcall f_5637(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5659)
static void C_ccall f_5659(C_word c,C_word *av) C_noret;
C_noret_decl(f_5682)
static void C_ccall f_5682(C_word c,C_word *av) C_noret;
C_noret_decl(f_5686)
static void C_ccall f_5686(C_word c,C_word *av) C_noret;
C_noret_decl(f_5690)
static void C_ccall f_5690(C_word c,C_word *av) C_noret;
C_noret_decl(f_5697)
static void C_ccall f_5697(C_word c,C_word *av) C_noret;
C_noret_decl(f_5719)
static void C_ccall f_5719(C_word c,C_word *av) C_noret;
C_noret_decl(f_5729)
static void C_ccall f_5729(C_word c,C_word *av) C_noret;
C_noret_decl(f_5743)
static void C_ccall f_5743(C_word c,C_word *av) C_noret;
C_noret_decl(f_5747)
static void C_ccall f_5747(C_word c,C_word *av) C_noret;
C_noret_decl(f_5754)
static void C_ccall f_5754(C_word c,C_word *av) C_noret;
C_noret_decl(f_5785)
static void C_ccall f_5785(C_word c,C_word *av) C_noret;
C_noret_decl(f_5788)
static void C_fcall f_5788(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5803)
static void C_ccall f_5803(C_word c,C_word *av) C_noret;
C_noret_decl(f_5820)
static void C_ccall f_5820(C_word c,C_word *av) C_noret;
C_noret_decl(f_5824)
static void C_ccall f_5824(C_word c,C_word *av) C_noret;
C_noret_decl(f_5831)
static void C_ccall f_5831(C_word c,C_word *av) C_noret;
C_noret_decl(f_5862)
static void C_ccall f_5862(C_word c,C_word *av) C_noret;
C_noret_decl(f_5890)
static void C_ccall f_5890(C_word c,C_word *av) C_noret;
C_noret_decl(f_5892)
static void C_ccall f_5892(C_word c,C_word *av) C_noret;
C_noret_decl(f_5915)
static void C_ccall f_5915(C_word c,C_word *av) C_noret;
C_noret_decl(f_5917)
static void C_ccall f_5917(C_word c,C_word *av) C_noret;
C_noret_decl(f_5936)
static void C_ccall f_5936(C_word c,C_word *av) C_noret;
C_noret_decl(f_5940)
static void C_ccall f_5940(C_word c,C_word *av) C_noret;
C_noret_decl(f_5955)
static void C_ccall f_5955(C_word c,C_word *av) C_noret;
C_noret_decl(f_5986)
static void C_ccall f_5986(C_word c,C_word *av) C_noret;
C_noret_decl(f_6014)
static void C_ccall f_6014(C_word c,C_word *av) C_noret;
C_noret_decl(f_6016)
static void C_ccall f_6016(C_word c,C_word *av) C_noret;
C_noret_decl(f_6039)
static void C_ccall f_6039(C_word c,C_word *av) C_noret;
C_noret_decl(f_6041)
static void C_ccall f_6041(C_word c,C_word *av) C_noret;
C_noret_decl(f_6060)
static void C_ccall f_6060(C_word c,C_word *av) C_noret;
C_noret_decl(f_6064)
static void C_ccall f_6064(C_word c,C_word *av) C_noret;
C_noret_decl(f_6079)
static void C_ccall f_6079(C_word c,C_word *av) C_noret;
C_noret_decl(f_6083)
static void C_ccall f_6083(C_word c,C_word *av) C_noret;
C_noret_decl(f_6104)
static void C_ccall f_6104(C_word c,C_word *av) C_noret;
C_noret_decl(f_6146)
static void C_ccall f_6146(C_word c,C_word *av) C_noret;
C_noret_decl(f_6148)
static void C_ccall f_6148(C_word c,C_word *av) C_noret;
C_noret_decl(f_6155)
static void C_fcall f_6155(C_word t0,C_word t1) C_noret;
C_noret_decl(f_6166)
static void C_ccall f_6166(C_word c,C_word *av) C_noret;
C_noret_decl(f_6187)
static void C_ccall f_6187(C_word c,C_word *av) C_noret;
C_noret_decl(f_6191)
static void C_ccall f_6191(C_word c,C_word *av) C_noret;
C_noret_decl(f_6197)
static void C_ccall f_6197(C_word c,C_word *av) C_noret;
C_noret_decl(f_6219)
static void C_ccall f_6219(C_word c,C_word *av) C_noret;
C_noret_decl(f_6223)
static void C_ccall f_6223(C_word c,C_word *av) C_noret;
C_noret_decl(f_6225)
static void C_ccall f_6225(C_word c,C_word *av) C_noret;
C_noret_decl(f_6241)
static void C_ccall f_6241(C_word c,C_word *av) C_noret;
C_noret_decl(f_6247)
static void C_ccall f_6247(C_word c,C_word *av) C_noret;
C_noret_decl(f_6265)
static void C_ccall f_6265(C_word c,C_word *av) C_noret;
C_noret_decl(f_6268)
static void C_fcall f_6268(C_word t0,C_word t1) C_noret;
C_noret_decl(f_6271)
static void C_fcall f_6271(C_word t0,C_word t1) C_noret;
C_noret_decl(f_6286)
static void C_ccall f_6286(C_word c,C_word *av) C_noret;
C_noret_decl(f_6298)
static void C_ccall f_6298(C_word c,C_word *av) C_noret;
C_noret_decl(f_6308)
static void C_ccall f_6308(C_word c,C_word *av) C_noret;
C_noret_decl(f_6312)
static void C_ccall f_6312(C_word c,C_word *av) C_noret;
C_noret_decl(f_6321)
static void C_ccall f_6321(C_word c,C_word *av) C_noret;
C_noret_decl(f_6331)
static void C_ccall f_6331(C_word c,C_word *av) C_noret;
C_noret_decl(f_6335)
static void C_ccall f_6335(C_word c,C_word *av) C_noret;
C_noret_decl(f_6365)
static void C_ccall f_6365(C_word c,C_word *av) C_noret;
C_noret_decl(f_6369)
static void C_ccall f_6369(C_word c,C_word *av) C_noret;
C_noret_decl(f_6373)
static void C_ccall f_6373(C_word c,C_word *av) C_noret;
C_noret_decl(f_6377)
static void C_ccall f_6377(C_word c,C_word *av) C_noret;
C_noret_decl(f_6381)
static void C_ccall f_6381(C_word c,C_word *av) C_noret;
C_noret_decl(f_6390)
static void C_ccall f_6390(C_word c,C_word *av) C_noret;
C_noret_decl(f_6394)
static void C_ccall f_6394(C_word c,C_word *av) C_noret;
C_noret_decl(f_6396)
static void C_fcall f_6396(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6406)
static void C_ccall f_6406(C_word c,C_word *av) C_noret;
C_noret_decl(f_6419)
static void C_fcall f_6419(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6444)
static void C_ccall f_6444(C_word c,C_word *av) C_noret;
C_noret_decl(C_c_2dplatform_toplevel)
C_externexport void C_ccall C_c_2dplatform_toplevel(C_word c,C_word *av) C_noret;

C_noret_decl(trf_2117)
static void C_ccall trf_2117(C_word c,C_word *av) C_noret;
static void C_ccall trf_2117(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_2117(t0,t1,t2);}

C_noret_decl(trf_2123)
static void C_ccall trf_2123(C_word c,C_word *av) C_noret;
static void C_ccall trf_2123(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_2123(t0,t1,t2,t3);}

C_noret_decl(trf_2293)
static void C_ccall trf_2293(C_word c,C_word *av) C_noret;
static void C_ccall trf_2293(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_2293(t0,t1,t2);}

C_noret_decl(trf_2302)
static void C_ccall trf_2302(C_word c,C_word *av) C_noret;
static void C_ccall trf_2302(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_2302(t0,t1,t2);}

C_noret_decl(trf_2310)
static void C_ccall trf_2310(C_word c,C_word *av) C_noret;
static void C_ccall trf_2310(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_2310(t0,t1,t2,t3);}

C_noret_decl(trf_2701)
static void C_ccall trf_2701(C_word c,C_word *av) C_noret;
static void C_ccall trf_2701(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_2701(t0,t1,t2);}

C_noret_decl(trf_3010)
static void C_ccall trf_3010(C_word c,C_word *av) C_noret;
static void C_ccall trf_3010(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_3010(t0,t1,t2,t3);}

C_noret_decl(trf_3105)
static void C_ccall trf_3105(C_word c,C_word *av) C_noret;
static void C_ccall trf_3105(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_3105(t0,t1);}

C_noret_decl(trf_3108)
static void C_ccall trf_3108(C_word c,C_word *av) C_noret;
static void C_ccall trf_3108(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_3108(t0,t1);}

C_noret_decl(trf_3297)
static void C_ccall trf_3297(C_word c,C_word *av) C_noret;
static void C_ccall trf_3297(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3297(t0,t1,t2);}

C_noret_decl(trf_3425)
static void C_ccall trf_3425(C_word c,C_word *av) C_noret;
static void C_ccall trf_3425(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_3425(t0,t1,t2,t3);}

C_noret_decl(trf_4485)
static void C_ccall trf_4485(C_word c,C_word *av) C_noret;
static void C_ccall trf_4485(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4485(t0,t1);}

C_noret_decl(trf_5221)
static void C_ccall trf_5221(C_word c,C_word *av) C_noret;
static void C_ccall trf_5221(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5221(t0,t1);}

C_noret_decl(trf_5637)
static void C_ccall trf_5637(C_word c,C_word *av) C_noret;
static void C_ccall trf_5637(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5637(t0,t1);}

C_noret_decl(trf_5788)
static void C_ccall trf_5788(C_word c,C_word *av) C_noret;
static void C_ccall trf_5788(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5788(t0,t1);}

C_noret_decl(trf_6155)
static void C_ccall trf_6155(C_word c,C_word *av) C_noret;
static void C_ccall trf_6155(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_6155(t0,t1);}

C_noret_decl(trf_6268)
static void C_ccall trf_6268(C_word c,C_word *av) C_noret;
static void C_ccall trf_6268(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_6268(t0,t1);}

C_noret_decl(trf_6271)
static void C_ccall trf_6271(C_word c,C_word *av) C_noret;
static void C_ccall trf_6271(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_6271(t0,t1);}

C_noret_decl(trf_6396)
static void C_ccall trf_6396(C_word c,C_word *av) C_noret;
static void C_ccall trf_6396(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6396(t0,t1,t2);}

C_noret_decl(trf_6419)
static void C_ccall trf_6419(C_word c,C_word *av) C_noret;
static void C_ccall trf_6419(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6419(t0,t1,t2);}

/* k1662 */
static void C_ccall f_1664(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_1664,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1667,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_eval_toplevel(2,av2);}}

/* k1665 in k1662 */
static void C_ccall f_1667(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_1667,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1670,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_expand_toplevel(2,av2);}}

/* k1668 in k1665 in k1662 */
static void C_ccall f_1670(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_1670,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1673,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_internal_toplevel(2,av2);}}

/* k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_1673(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_1673,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1676,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_optimizer_toplevel(2,av2);}}

/* k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_1676(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_1676,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1679,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_support_toplevel(2,av2);}}

/* k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_1679(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_1679,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1682,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_compiler_toplevel(2,av2);}}

/* k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_1682(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(23,c,4)))){
C_save_and_reclaim((void *)f_1682,c,av);}
a=C_alloc(23);
t2=C_a_i_provide(&a,1,lf[0]);
t3=C_a_i_provide(&a,1,lf[1]);
t4=C_mutate(&lf[2] /* (set! chicken.compiler.c-platform#cons* ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_2117,tmp=(C_word)a,a+=2,tmp));
t5=C_mutate(&lf[3] /* (set! chicken.compiler.c-platform#filter ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_2293,tmp=(C_word)a,a+=2,tmp));
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2963,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:55: chicken.compiler.optimizer#default-optimization-passes */
t7=*((C_word*)lf[940]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t7;
av2[1]=t6;
av2[2]=C_fix(3);
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}

/* chicken.compiler.c-platform#cons* in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_fcall f_2117(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,4)))){
C_save_and_reclaim_args((void *)trf_2117,3,t1,t2,t3);}
a=C_alloc(5);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2123,a[2]=t5,tmp=(C_word)a,a+=3,tmp));
t7=((C_word*)t5)[1];
f_2123(t7,t1,t2,t3);}

/* loop in chicken.compiler.c-platform#cons* in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_fcall f_2123(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(4,0,3)))){
C_save_and_reclaim_args((void *)trf_2123,4,t0,t1,t2,t3);}
a=C_alloc(4);
if(C_truep(C_i_nullp(t3))){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2137,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* mini-srfi-1.scm:95: loop */
t6=t4;
t7=C_i_car(t3);
t8=C_u_i_cdr(t3);
t1=t6;
t2=t7;
t3=t8;
goto loop;}}

/* k2135 in loop in chicken.compiler.c-platform#cons* in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_2137(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_2137,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.compiler.c-platform#filter in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_fcall f_2293(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_2293,3,t1,t2,t3);}
a=C_alloc(6);
t4=C_i_check_list_2(t3,lf[4]);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2302,a[2]=t2,a[3]=t6,tmp=(C_word)a,a+=4,tmp));
t8=((C_word*)t6)[1];
f_2302(t8,t1,t3);}

/* foldr254 in chicken.compiler.c-platform#filter in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_fcall f_2302(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(8,0,4)))){
C_save_and_reclaim_args((void *)trf_2302,3,t0,t1,t2);}
a=C_alloc(8);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2310,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=C_slot(t2,C_fix(0));
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2331,a[2]=t3,a[3]=t1,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
t7=t5;
t8=C_slot(t2,C_fix(1));
t1=t7;
t2=t8;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* g259 in foldr254 in chicken.compiler.c-platform#filter in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_fcall f_2310(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_2310,4,t0,t1,t2,t3);}
a=C_alloc(5);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2317,a[2]=t1,a[3]=t2,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* mini-srfi-1.scm:131: pred */
t5=((C_word*)t0)[2];{
C_word av2[3];
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k2315 in g259 in foldr254 in chicken.compiler.c-platform#filter in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_2317(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_2317,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(C_truep(t1)?C_a_i_cons(&a,2,((C_word*)t0)[3],((C_word*)t0)[4]):((C_word*)t0)[4]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k2329 in foldr254 in chicken.compiler.c-platform#filter in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_2331(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2331,c,av);}
/* mini-srfi-1.scm:131: g259 */
t2=((C_word*)t0)[2];
f_2310(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* loop in rewrite-apply in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static C_word C_fcall f_2459(C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_stack_overflow_check;
loop:{}
t2=C_i_cdr(t1);
if(C_truep(C_i_nullp(t2))){
return(C_u_i_car(t1));}
else{
t4=C_u_i_cdr(t1);
t1=t4;
goto loop;}}

/* loop in k5182 in k5179 in k5240 in k5164 in k5244 in rewrite-make-vector in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in ... */
static void C_fcall f_2701(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_2701,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_fixnum_greater_or_equal_p(t2,((C_word*)t0)[2]))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2715,a[2]=t1,a[3]=((C_word*)t0)[3],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* mini-srfi-1.scm:190: proc */
t4=((C_word*)t0)[4];
f_5221(t4,t3);}}

/* k2713 in loop in k5182 in k5179 in k5240 in k5164 in k5244 in rewrite-make-vector in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in ... */
static void C_ccall f_2715(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_2715,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2719,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* mini-srfi-1.scm:190: loop */
t3=((C_word*)((C_word*)t0)[3])[1];
f_2701(t3,t2,C_fixnum_plus(((C_word*)t0)[4],C_fix(1)));}

/* k2717 in k2713 in loop in k5182 in k5179 in k5240 in k5164 in k5244 in rewrite-make-vector in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in ... */
static void C_ccall f_2719(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_2719,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_2963(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2963,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[5]+1 /* (set! chicken.compiler.c-platform#default-declarations ...) */,lf[6]);
t3=C_mutate((C_word*)lf[7]+1 /* (set! chicken.compiler.c-platform#default-profiling-declarations ...) */,lf[8]);
t4=C_mutate((C_word*)lf[9]+1 /* (set! chicken.compiler.c-platform#default-units ...) */,lf[10]);
t5=C_set_block_item(lf[11] /* chicken.compiler.c-platform#words-per-flonum */,0,C_fix(4));
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2971,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:83: chicken.compiler.optimizer#eq-inline-operator */
t7=*((C_word*)lf[938]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t7;
av2[1]=t6;
av2[2]=lf[939];
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}

/* k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_2971(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2971,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2974,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:84: chicken.compiler.optimizer#membership-test-operators */
t3=*((C_word*)lf[936]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[937];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_2974(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2974,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2977,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:87: chicken.compiler.optimizer#membership-unfold-limit */
t3=*((C_word*)lf[935]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_fix(20);
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_2977(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,3)))){
C_save_and_reclaim((void *)f_2977,c,av);}
a=C_alloc(15);
t2=C_mutate((C_word*)lf[12]+1 /* (set! chicken.compiler.c-platform#target-include-file ...) */,lf[13]);
t3=C_mutate((C_word*)lf[14]+1 /* (set! chicken.compiler.c-platform#valid-compiler-options ...) */,lf[15]);
t4=C_mutate((C_word*)lf[16]+1 /* (set! chicken.compiler.c-platform#valid-compiler-options-with-argument ...) */,lf[17]);
t5=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t6=t5;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=((C_word*)t7)[1];
t9=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2992,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t10=C_SCHEME_UNDEFINED;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_set_block_item(t11,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6419,a[2]=t7,a[3]=t11,a[4]=t8,tmp=(C_word)a,a+=5,tmp));
t13=((C_word*)t11)[1];
f_6419(t13,t9,lf[934]);}

/* k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_2992(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_2992,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[18]+1 /* (set! chicken.compiler.core#default-standard-bindings ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2999,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:261: scheme#append */
t4=*((C_word*)lf[37]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[929];
av2[3]=lf[930];
av2[4]=lf[931];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_2999(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_2999,c,av);}
a=C_alloc(8);
t2=C_mutate((C_word*)lf[19]+1 /* (set! chicken.compiler.core#default-extended-bindings ...) */,t1);
t3=C_mutate((C_word*)lf[20]+1 /* (set! chicken.compiler.core#internal-bindings ...) */,lf[21]);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3008,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6396,a[2]=t6,tmp=(C_word)a,a+=3,tmp));
t8=((C_word*)t6)[1];
f_6396(t8,t4,lf[928]);}

/* k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_3008(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,5)))){
C_save_and_reclaim((void *)f_3008,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_3010,tmp=(C_word)a,a+=2,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3075,a[2]=((C_word*)t0)[2],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6394,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:315: op1 */
f_3010(t4,lf[923],lf[924],lf[925]);}

/* op1 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_fcall f_3010(C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,6)))){
C_save_and_reclaim_args((void *)trf_3010,4,t1,t2,t3,t4);}
a=C_alloc(5);
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3012,a[2]=t3,a[3]=t2,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* f_3012 in op1 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_3012(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(19,c,4)))){
C_save_and_reclaim((void *)f_3012,c,av);}
a=C_alloc(19);
t6=C_i_length(t5);
t7=C_eqp(t6,C_fix(1));
if(C_truep(t7)){
t8=C_a_i_list1(&a,1,C_SCHEME_TRUE);
t9=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3034,a[2]=t4,a[3]=t1,a[4]=t8,tmp=(C_word)a,a+=5,tmp);
t10=C_eqp(lf[24],*((C_word*)lf[25]+1));
if(C_truep(t10)){
t11=(C_truep(*((C_word*)lf[26]+1))?C_a_i_list1(&a,1,((C_word*)t0)[2]):C_a_i_list1(&a,1,((C_word*)t0)[3]));
/* c-platform.scm:311: chicken.compiler.support#make-node */
t12=*((C_word*)lf[22]+1);{
C_word *av2=av;
av2[0]=t12;
av2[1]=t9;
av2[2]=lf[27];
av2[3]=t11;
av2[4]=t5;
((C_proc)(void*)(*((C_word*)t12+1)))(5,av2);}}
else{
t11=C_a_i_list2(&a,2,((C_word*)t0)[4],C_fix(36));
t12=C_i_car(t5);
t13=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3067,a[2]=t12,a[3]=t9,a[4]=t11,tmp=(C_word)a,a+=5,tmp);
/* c-platform.scm:314: chicken.compiler.support#qnode */
t14=*((C_word*)lf[29]+1);{
C_word *av2=av;
av2[0]=t14;
av2[1]=t13;
av2[2]=C_fix(1);
((C_proc)(void*)(*((C_word*)t14+1)))(3,av2);}}}
else{
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}}

/* k3032 */
static void C_ccall f_3034(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_3034,c,av);}
a=C_alloc(6);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
/* c-platform.scm:306: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[23];
av2[3]=((C_word*)t0)[4];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k3065 */
static void C_ccall f_3067(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_3067,c,av);}
a=C_alloc(6);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
/* c-platform.scm:312: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[28];
av2[3]=((C_word*)t0)[4];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_3075(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_3075,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3078,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6390,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:316: op1 */
f_3010(t3,lf[919],lf[920],lf[921]);}

/* k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_3078(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,6)))){
C_save_and_reclaim((void *)f_3078,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_3080,tmp=(C_word)a,a+=2,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3234,a[2]=((C_word*)t0)[2],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:339: chicken.compiler.optimizer#rewrite */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[822];
av2[3]=C_fix(8);
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* eqv?-id in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_3080(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_3080,c,av);}
a=C_alloc(13);
t6=C_i_length(t5);
t7=C_eqp(t6,C_fix(2));
if(C_truep(t7)){
t8=C_i_car(t5);
t9=C_i_cadr(t5);
t10=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3096,a[2]=t1,a[3]=t4,a[4]=t5,a[5]=t9,a[6]=t8,tmp=(C_word)a,a+=7,tmp);
t11=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3226,a[2]=t4,a[3]=t10,a[4]=t9,a[5]=t8,tmp=(C_word)a,a+=6,tmp);
/* c-platform.scm:326: chicken.compiler.support#node-class */
t12=*((C_word*)lf[34]+1);{
C_word *av2=av;
av2[0]=t12;
av2[1]=t11;
av2[2]=t8;
((C_proc)(void*)(*((C_word*)t12+1)))(3,av2);}}
else{
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}}

/* k3094 in eqv?-id in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_3096(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_3096,c,av);}
a=C_alloc(10);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3105,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3180,a[2]=t2,a[3]=((C_word*)t0)[6],tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:330: chicken.compiler.support#node-class */
t4=*((C_word*)lf[34]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}}

/* k3103 in k3094 in eqv?-id in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_fcall f_3105(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,2)))){
C_save_and_reclaim_args((void *)trf_3105,2,t0,t1);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3108,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
if(C_truep(t1)){
t3=t2;
f_3108(t3,t1);}
else{
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3153,a[2]=t2,a[3]=((C_word*)t0)[5],tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:333: chicken.compiler.support#node-class */
t4=*((C_word*)lf[34]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}}

/* k3106 in k3103 in k3094 in eqv?-id in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_fcall f_3108(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,4)))){
C_save_and_reclaim_args((void *)trf_3108,2,t0,t1);}
a=C_alloc(8);
if(C_truep(t1)){
t2=C_a_i_list1(&a,1,C_SCHEME_TRUE);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3123,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* c-platform.scm:338: chicken.compiler.support#make-node */
t4=*((C_word*)lf[22]+1);{
C_word av2[5];
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[27];
av2[3]=lf[30];
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word av2[2];
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k3121 in k3106 in k3103 in k3094 in eqv?-id in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_3123(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_3123,c,av);}
a=C_alloc(6);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
/* c-platform.scm:336: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[23];
av2[3]=((C_word*)t0)[4];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k3133 in k3147 in k3151 in k3103 in k3094 in eqv?-id in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 in ... */
static void C_ccall f_3135(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3135,c,av);}
t2=((C_word*)t0)[2];
f_3108(t2,(C_truep(t1)?t1:C_i_not(C_i_numberp(((C_word*)t0)[3]))));}

/* k3147 in k3151 in k3103 in k3094 in eqv?-id in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_3149(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_3149,c,av);}
a=C_alloc(4);
t2=C_i_car(t1);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3135,a[2]=((C_word*)t0)[2],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:335: chicken.compiler.support#immediate? */
t4=*((C_word*)lf[32]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k3151 in k3103 in k3094 in eqv?-id in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_3153(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_3153,c,av);}
a=C_alloc(3);
t2=C_eqp(lf[31],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3149,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:334: chicken.compiler.support#node-parameters */
t4=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=((C_word*)t0)[2];
f_3108(t3,C_SCHEME_FALSE);}}

/* k3160 in k3174 in k3178 in k3094 in eqv?-id in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_3162(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3162,c,av);}
t2=((C_word*)t0)[2];
f_3105(t2,(C_truep(t1)?t1:C_i_not(C_i_numberp(((C_word*)t0)[3]))));}

/* k3174 in k3178 in k3094 in eqv?-id in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_3176(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_3176,c,av);}
a=C_alloc(4);
t2=C_i_car(t1);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3162,a[2]=((C_word*)t0)[2],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:332: chicken.compiler.support#immediate? */
t4=*((C_word*)lf[32]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k3178 in k3094 in eqv?-id in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_3180(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_3180,c,av);}
a=C_alloc(3);
t2=C_eqp(lf[31],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3176,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:331: chicken.compiler.support#node-parameters */
t4=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=((C_word*)t0)[2];
f_3105(t3,C_SCHEME_FALSE);}}

/* k3208 in k3216 in k3212 in k3220 in k3224 in eqv?-id in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 in ... */
static void C_ccall f_3210(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_3210,c,av);}
a=C_alloc(6);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
/* c-platform.scm:329: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[23];
av2[3]=((C_word*)t0)[4];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k3212 in k3220 in k3224 in eqv?-id in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_3214(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_3214,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3218,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* c-platform.scm:328: chicken.compiler.support#node-parameters */
t3=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k3216 in k3212 in k3220 in k3224 in eqv?-id in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_3218(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_3218,c,av);}
a=C_alloc(8);
if(C_truep(C_i_equalp(((C_word*)t0)[2],t1))){
t2=C_a_i_list1(&a,1,C_SCHEME_TRUE);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3210,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* c-platform.scm:329: chicken.compiler.support#qnode */
t4=*((C_word*)lf[29]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t2=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
f_3096(2,av2);}}}

/* k3220 in k3224 in eqv?-id in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_3222(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_3222,c,av);}
a=C_alloc(5);
t2=C_eqp(lf[35],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3214,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* c-platform.scm:328: chicken.compiler.support#node-parameters */
t4=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
f_3096(2,av2);}}}

/* k3224 in eqv?-id in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_3226(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_3226,c,av);}
a=C_alloc(6);
t2=C_eqp(lf[35],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3222,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* c-platform.scm:327: chicken.compiler.support#node-class */
t4=*((C_word*)lf[34]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
f_3096(2,av2);}}}

/* k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_3234(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_3234,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3237,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:340: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[820];
av2[3]=C_fix(8);
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_3237(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,6)))){
C_save_and_reclaim((void *)f_3237,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3240,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_6225,tmp=(C_word)a,a+=2,tmp);
/* c-platform.scm:342: chicken.compiler.optimizer#rewrite */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=lf[917];
av2[3]=C_fix(8);
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_3240(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,6)))){
C_save_and_reclaim((void *)f_3240,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_3242,tmp=(C_word)a,a+=2,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3420,a[2]=((C_word*)t0)[2],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:397: chicken.compiler.optimizer#rewrite */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[914];
av2[3]=C_fix(8);
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* rewrite-apply in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_3242(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_3242,c,av);}
a=C_alloc(9);
if(C_truep(C_i_pairp(t5))){
t6=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_2459,tmp=(C_word)a,a+=2,tmp);
t7=(
  f_2459(t5)
);
t8=C_u_i_car(t5);
t9=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3417,a[2]=t1,a[3]=t5,a[4]=t4,a[5]=t7,a[6]=t8,tmp=(C_word)a,a+=7,tmp);
/* c-platform.scm:377: chicken.compiler.support#node-class */
t10=*((C_word*)lf[34]+1);{
C_word *av2=av;
av2[0]=t10;
av2[1]=t9;
av2[2]=t7;
((C_proc)(void*)(*((C_word*)t10+1)))(3,av2);}}
else{
t6=t1;{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* k3268 in k3415 in rewrite-apply in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 in ... */
static void C_ccall f_3270(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_3270,c,av);}
/* c-platform.scm:378: chicken.compiler.support#make-node */
t2=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[23];
av2[3]=((C_word*)t0)[3];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k3276 in k3415 in rewrite-apply in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 in ... */
static void C_ccall f_3278(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_3278,c,av);}
a=C_alloc(6);
/* c-platform.scm:380: cons* */
f_2117(((C_word*)t0)[2],((C_word*)t0)[3],C_a_i_list(&a,2,((C_word*)t0)[4],t1));}

/* k3293 in k3331 in k3335 in k3415 in rewrite-apply in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in ... */
static void C_ccall f_3295(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3295,c,av);}
/* c-platform.scm:382: scheme#append */
t2=*((C_word*)lf[37]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* map-loop694 in k3331 in k3335 in k3415 in rewrite-apply in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in ... */
static void C_fcall f_3297(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_3297,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3322,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* c-platform.scm:382: g700 */
t4=((C_word*)t0)[4];{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k3320 in map-loop694 in k3331 in k3335 in k3415 in rewrite-apply in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in ... */
static void C_ccall f_3322(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_3322,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_3297(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k3331 in k3335 in k3415 in rewrite-apply in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in ... */
static void C_ccall f_3333(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_3333,c,av);}
a=C_alloc(12);
t2=C_i_car(t1);
t3=C_i_check_list_2(t2,lf[36]);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3295,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3297,a[2]=((C_word*)t0)[4],a[3]=t6,a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],tmp=(C_word)a,a+=6,tmp));
t8=((C_word*)t6)[1];
f_3297(t8,t4,t2);}

/* k3335 in k3415 in rewrite-apply in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 in ... */
static void C_ccall f_3337(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_3337,c,av);}
a=C_alloc(12);
t2=C_i_cdr(t1);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=*((C_word*)lf[29]+1);
t8=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3333,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=t5,a[5]=t7,a[6]=t6,tmp=(C_word)a,a+=7,tmp);
/* c-platform.scm:382: chicken.compiler.support#node-parameters */
t9=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t9;
av2[1]=t8;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}

/* k3338 in k3415 in rewrite-apply in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 in ... */
static void C_ccall f_3340(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,4)))){
C_save_and_reclaim((void *)f_3340,c,av);}
a=C_alloc(12);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_a_i_list1(&a,1,C_SCHEME_TRUE);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3354,a[2]=((C_word*)t0)[2],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3358,a[2]=t3,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* c-platform.scm:395: chicken.compiler.support#make-node */
t5=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=lf[39];
av2[3]=lf[40];
av2[4]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}}

/* k3352 in k3338 in k3415 in rewrite-apply in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in ... */
static void C_ccall f_3354(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_3354,c,av);}
/* c-platform.scm:393: chicken.compiler.support#make-node */
t2=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[23];
av2[3]=((C_word*)t0)[3];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k3356 in k3338 in k3415 in rewrite-apply in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in ... */
static void C_ccall f_3358(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_3358,c,av);}
a=C_alloc(6);
/* c-platform.scm:395: cons* */
f_2117(((C_word*)t0)[2],t1,C_a_i_list(&a,2,((C_word*)t0)[3],((C_word*)t0)[4]));}

/* k3379 in k3402 in k3411 in k3415 in rewrite-apply in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in ... */
static void C_ccall f_3381(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_3381,c,av);}
a=C_alloc(9);
if(C_truep(t1)){
t2=C_a_i_list1(&a,1,C_SCHEME_TRUE);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3396,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* c-platform.scm:390: chicken.compiler.support#make-node */
t4=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[39];
av2[3]=lf[43];
av2[4]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}
else{
t2=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
f_3340(2,av2);}}}

/* k3394 in k3379 in k3402 in k3411 in k3415 in rewrite-apply in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in ... */
static void C_ccall f_3396(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_3396,c,av);}
a=C_alloc(9);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=C_a_i_list3(&a,3,t1,((C_word*)t0)[3],t2);
/* c-platform.scm:388: chicken.compiler.support#make-node */
t4=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[4];
av2[2]=lf[23];
av2[3]=((C_word*)t0)[5];
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k3402 in k3411 in k3415 in rewrite-apply in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in ... */
static void C_ccall f_3404(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_3404,c,av);}
a=C_alloc(5);
t2=C_i_car(t1);
if(C_truep((C_truep(C_eqp(t2,lf[41]))?C_SCHEME_TRUE:(C_truep(C_eqp(t2,lf[42]))?C_SCHEME_TRUE:C_SCHEME_FALSE)))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3381,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* c-platform.scm:387: chicken.compiler.support#intrinsic? */
t4=*((C_word*)lf[44]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
f_3340(2,av2);}}}

/* k3411 in k3415 in rewrite-apply in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 in ... */
static void C_ccall f_3413(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_3413,c,av);}
a=C_alloc(5);
t2=C_eqp(lf[35],t1);
if(C_truep(t2)){
t3=C_i_length(((C_word*)t0)[2]);
t4=C_eqp(C_fix(2),t3);
if(C_truep(t4)){
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3404,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* c-platform.scm:385: chicken.compiler.support#node-parameters */
t6=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}
else{
t5=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_FALSE;
f_3340(2,av2);}}}
else{
t3=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
f_3340(2,av2);}}}

/* k3415 in rewrite-apply in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_3417(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,2)))){
C_save_and_reclaim((void *)f_3417,c,av);}
a=C_alloc(16);
t2=C_eqp(lf[31],t1);
if(C_truep(t2)){
t3=C_a_i_list1(&a,1,C_SCHEME_FALSE);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3270,a[2]=((C_word*)t0)[2],a[3]=t3,tmp=(C_word)a,a+=4,tmp);
t5=C_i_car(((C_word*)t0)[3]);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3278,a[2]=t4,a[3]=t5,a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3337,a[2]=t6,a[3]=((C_word*)t0)[5],tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:382: chicken.base#butlast */
t8=*((C_word*)lf[38]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t8;
av2[1]=t7;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t8+1)))(3,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3340,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3413,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t3,a[5]=((C_word*)t0)[6],tmp=(C_word)a,a+=6,tmp);
/* c-platform.scm:383: chicken.compiler.support#node-class */
t5=*((C_word*)lf[34]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}}

/* k3418 in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_3420(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_3420,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3423,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:398: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[913];
av2[3]=C_fix(8);
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k3421 in k3418 in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_3423(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,5)))){
C_save_and_reclaim((void *)f_3423,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_3425,tmp=(C_word)a,a+=2,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3493,a[2]=((C_word*)t0)[2],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:419: rewrite-c..r */
f_3425(t3,lf[910],lf[911],lf[912]);}

/* rewrite-c..r in k3421 in k3418 in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 in ... */
static void C_fcall f_3425(C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,6)))){
C_save_and_reclaim_args((void *)trf_3425,4,t1,t2,t3,t4);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3431,a[2]=t4,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:402: chicken.compiler.optimizer#rewrite */
t6=*((C_word*)lf[46]+1);{
C_word av2[5];
av2[0]=t6;
av2[1]=t1;
av2[2]=t2;
av2[3]=C_fix(8);
av2[4]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* a3430 in rewrite-c..r in k3421 in k3418 in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in ... */
static void C_ccall f_3431(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_3431,c,av);}
a=C_alloc(6);
t6=C_i_length(t5);
t7=C_eqp(t6,C_fix(1));
if(C_truep(t7)){
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3443,a[2]=t5,a[3]=t4,a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
/* c-platform.scm:408: scheme#call-with-current-continuation */
t9=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t9;
av2[1]=t1;
av2[2]=t8;
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}
else{
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}}

/* a3442 in a3430 in rewrite-c..r in k3421 in k3418 in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in ... */
static void C_ccall f_3443(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,4)))){
C_save_and_reclaim((void *)f_3443,c,av);}
a=C_alloc(11);
t3=C_i_car(((C_word*)t0)[2]);
t4=C_a_i_list1(&a,1,C_SCHEME_TRUE);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3462,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
t6=(C_truep(*((C_word*)lf[26]+1))?((C_word*)t0)[4]:C_SCHEME_FALSE);
if(C_truep(t6)){
t7=C_a_i_list1(&a,1,((C_word*)t0)[4]);
/* c-platform.scm:415: chicken.compiler.support#make-node */
t8=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t8;
av2[1]=t5;
av2[2]=lf[27];
av2[3]=t7;
av2[4]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t8+1)))(5,av2);}}
else{
if(C_truep(((C_word*)t0)[5])){
t7=C_a_i_list1(&a,1,((C_word*)t0)[5]);
/* c-platform.scm:416: chicken.compiler.support#make-node */
t8=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t8;
av2[1]=t5;
av2[2]=lf[27];
av2[3]=t7;
av2[4]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t8+1)))(5,av2);}}
else{
/* c-platform.scm:417: return */
t7=t2;{
C_word *av2=av;
av2[0]=t7;
av2[1]=t5;
av2[2]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}}}

/* k3460 in a3442 in a3430 in rewrite-c..r in k3421 in k3418 in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in ... */
static void C_ccall f_3462(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_3462,c,av);}
a=C_alloc(6);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
/* c-platform.scm:411: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[23];
av2[3]=((C_word*)t0)[4];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k3491 in k3421 in k3418 in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 in ... */
static void C_ccall f_3493(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_3493,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3496,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:420: rewrite-c..r */
f_3425(t2,lf[907],lf[908],lf[909]);}

/* k3494 in k3491 in k3421 in k3418 in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in ... */
static void C_ccall f_3496(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_3496,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3499,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:421: rewrite-c..r */
f_3425(t2,lf[904],lf[905],lf[906]);}

/* k3497 in k3494 in k3491 in k3421 in k3418 in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in ... */
static void C_ccall f_3499(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_3499,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3502,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:422: rewrite-c..r */
f_3425(t2,lf[901],lf[902],lf[903]);}

/* k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in ... */
static void C_ccall f_3502(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_3502,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3505,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:423: rewrite-c..r */
f_3425(t2,lf[877],lf[899],lf[900]);}

/* k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in ... */
static void C_ccall f_3505(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_3505,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3508,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:424: rewrite-c..r */
f_3425(t2,lf[856],lf[897],lf[898]);}

/* k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in ... */
static void C_ccall f_3508(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,6)))){
C_save_and_reclaim((void *)f_3508,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_3509,tmp=(C_word)a,a+=2,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3535,a[2]=((C_word*)t0)[2],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:431: chicken.compiler.optimizer#rewrite */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[895];
av2[3]=C_fix(8);
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* rvalues in k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in ... */
static void C_ccall f_3509(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_3509,c,av);}
a=C_alloc(6);
t6=C_i_length(t5);
t7=C_eqp(t6,C_fix(1));
if(C_truep(t7)){
t8=C_a_i_list1(&a,1,C_SCHEME_TRUE);
t9=C_a_i_cons(&a,2,t4,t5);
/* c-platform.scm:430: chicken.compiler.support#make-node */
t10=*((C_word*)lf[22]+1);{
C_word *av2=av;
av2[0]=t10;
av2[1]=t1;
av2[2]=lf[23];
av2[3]=t8;
av2[4]=t9;
((C_proc)(void*)(*((C_word*)t10+1)))(5,av2);}}
else{
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}}

/* k3533 in k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in ... */
static void C_ccall f_3535(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_3535,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3538,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:432: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[42];
av2[3]=C_fix(8);
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k3536 in k3533 in k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in ... */
static void C_ccall f_3538(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,6)))){
C_save_and_reclaim((void *)f_3538,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_3540,tmp=(C_word)a,a+=2,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3705,a[2]=((C_word*)t0)[2],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:463: chicken.compiler.optimizer#rewrite */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[891];
av2[3]=C_fix(8);
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* rewrite-c-w-v in k3536 in k3533 in k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in ... */
static void C_ccall f_3540(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_3540,c,av);}
a=C_alloc(7);
t6=C_i_length(t5);
t7=C_eqp(C_fix(2),t6);
if(C_truep(t7)){
t8=C_i_car(t5);
t9=C_i_cadr(t5);
t10=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3697,a[2]=t1,a[3]=t8,a[4]=t9,a[5]=t4,a[6]=t2,tmp=(C_word)a,a+=7,tmp);
/* c-platform.scm:441: chicken.compiler.support#node-class */
t11=*((C_word*)lf[34]+1);{
C_word *av2=av;
av2[0]=t11;
av2[1]=t10;
av2[2]=t8;
((C_proc)(void*)(*((C_word*)t11+1)))(3,av2);}}
else{
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}}

/* k3572 in k3687 in k3691 in k3695 in rewrite-c-w-v in k3536 in k3533 in k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in ... */
static void C_ccall f_3574(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_3574,c,av);}
a=C_alloc(7);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3685,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,tmp=(C_word)a,a+=7,tmp);
/* c-platform.scm:445: chicken.compiler.support#node-class */
t3=*((C_word*)lf[34]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k3599 in k3679 in k3683 in k3572 in k3687 in k3691 in k3695 in rewrite-c-w-v in k3536 in k3533 in k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in k3238 in k3235 in k3232 in k3076 in ... */
static void C_ccall f_3601(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_3601,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3604,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* c-platform.scm:450: chicken.base#gensym */
t3=*((C_word*)lf[50]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[55];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k3602 in k3599 in k3679 in k3683 in k3572 in k3687 in k3691 in k3695 in rewrite-c-w-v in k3536 in k3533 in k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in k3238 in k3235 in k3232 in ... */
static void C_ccall f_3604(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_3604,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3607,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3674,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:451: chicken.compiler.support#node-parameters */
t4=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k3605 in k3602 in k3599 in k3679 in k3683 in k3572 in k3687 in k3691 in k3695 in rewrite-c-w-v in k3536 in k3533 in k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in k3238 in k3235 in ... */
static void C_ccall f_3607(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,2)))){
C_save_and_reclaim((void *)f_3607,c,av);}
a=C_alloc(15);
t2=C_a_i_list1(&a,1,((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3622,a[2]=((C_word*)t0)[3],a[3]=t2,a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[2],tmp=(C_word)a,a+=6,tmp);
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3666,a[2]=((C_word*)t0)[5],a[3]=t3,a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[7],tmp=(C_word)a,a+=6,tmp);
/* c-platform.scm:456: chicken.base#gensym */
t5=*((C_word*)lf[50]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=lf[51];
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k3620 in k3605 in k3602 in k3599 in k3679 in k3683 in k3572 in k3687 in k3691 in k3695 in rewrite-c-w-v in k3536 in k3533 in k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in k3238 in ... */
static void C_ccall f_3622(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_3622,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3626,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
t3=C_a_i_list1(&a,1,C_SCHEME_TRUE);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3638,a[2]=((C_word*)t0)[4],a[3]=t2,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* c-platform.scm:462: chicken.compiler.support#varnode */
t5=*((C_word*)lf[49]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k3624 in k3620 in k3605 in k3602 in k3599 in k3679 in k3683 in k3572 in k3687 in k3691 in k3695 in rewrite-c-w-v in k3536 in k3533 in k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in ... */
static void C_ccall f_3626(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_3626,c,av);}
a=C_alloc(6);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
/* c-platform.scm:452: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[48];
av2[3]=((C_word*)t0)[4];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k3636 in k3620 in k3605 in k3602 in k3599 in k3679 in k3683 in k3572 in k3687 in k3691 in k3695 in rewrite-c-w-v in k3536 in k3533 in k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in ... */
static void C_ccall f_3638(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_3638,c,av);}
a=C_alloc(6);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
/* c-platform.scm:460: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[23];
av2[3]=((C_word*)t0)[4];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k3648 in k3664 in k3605 in k3602 in k3599 in k3679 in k3683 in k3572 in k3687 in k3691 in k3695 in rewrite-c-w-v in k3536 in k3533 in k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in ... */
static void C_ccall f_3650(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_3650,c,av);}
a=C_alloc(3);
t2=C_a_i_list1(&a,1,t1);
/* c-platform.scm:454: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[47];
av2[3]=((C_word*)t0)[3];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k3660 in k3664 in k3605 in k3602 in k3599 in k3679 in k3683 in k3572 in k3687 in k3691 in k3695 in rewrite-c-w-v in k3536 in k3533 in k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in ... */
static void C_ccall f_3662(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_3662,c,av);}
a=C_alloc(9);
t2=C_a_i_list3(&a,3,((C_word*)t0)[2],((C_word*)t0)[3],t1);
/* c-platform.scm:457: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[4];
av2[2]=lf[23];
av2[3]=((C_word*)t0)[5];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k3664 in k3605 in k3602 in k3599 in k3679 in k3683 in k3572 in k3687 in k3691 in k3695 in rewrite-c-w-v in k3536 in k3533 in k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in k3238 in ... */
static void C_ccall f_3666(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(28,c,2)))){
C_save_and_reclaim((void *)f_3666,c,av);}
a=C_alloc(28);
t2=C_a_i_list1(&a,1,((C_word*)t0)[2]);
t3=C_a_i_list4(&a,4,t1,C_SCHEME_FALSE,t2,C_fix(0));
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3650,a[2]=((C_word*)t0)[3],a[3]=t3,tmp=(C_word)a,a+=4,tmp);
t5=C_a_i_list1(&a,1,C_SCHEME_TRUE);
t6=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3662,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=t4,a[5]=t5,tmp=(C_word)a,a+=6,tmp);
/* c-platform.scm:459: chicken.compiler.support#varnode */
t7=*((C_word*)lf[49]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t7;
av2[1]=t6;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}

/* k3672 in k3602 in k3599 in k3679 in k3683 in k3572 in k3687 in k3691 in k3695 in rewrite-c-w-v in k3536 in k3533 in k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in k3238 in k3235 in ... */
static void C_ccall f_3674(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_3674,c,av);}
/* c-platform.scm:451: chicken.compiler.support#debugging */
t2=*((C_word*)lf[52]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[53];
av2[3]=lf[54];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k3679 in k3683 in k3572 in k3687 in k3691 in k3695 in rewrite-c-w-v in k3536 in k3533 in k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in k3238 in k3235 in k3232 in k3076 in k3073 in ... */
static void C_ccall f_3681(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_3681,c,av);}
a=C_alloc(7);
t2=C_i_caddr(t1);
if(C_truep(C_i_listp(t2))){
t3=C_eqp(C_fix(2),C_u_i_length(t2));
if(C_truep(t3)){
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3601,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* c-platform.scm:449: chicken.base#gensym */
t5=*((C_word*)lf[50]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t4=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}
else{
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k3683 in k3572 in k3687 in k3691 in k3695 in rewrite-c-w-v in k3536 in k3533 in k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in ... */
static void C_ccall f_3685(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_3685,c,av);}
a=C_alloc(7);
t2=C_eqp(lf[47],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3681,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* c-platform.scm:446: chicken.compiler.support#node-parameters */
t4=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k3687 in k3691 in k3695 in rewrite-c-w-v in k3536 in k3533 in k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in ... */
static void C_ccall f_3689(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_3689,c,av);}
a=C_alloc(6);
t2=C_i_car(t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3574,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* c-platform.scm:444: chicken.compiler.support#db-get */
t4=*((C_word*)lf[56]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[6];
av2[3]=t2;
av2[4]=lf[57];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}
else{
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k3691 in k3695 in rewrite-c-w-v in k3536 in k3533 in k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in ... */
static void C_ccall f_3693(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_3693,c,av);}
a=C_alloc(7);
t2=C_eqp(lf[35],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3689,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* c-platform.scm:443: chicken.compiler.support#node-parameters */
t4=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k3695 in rewrite-c-w-v in k3536 in k3533 in k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in ... */
static void C_ccall f_3697(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_3697,c,av);}
a=C_alloc(7);
t2=C_eqp(lf[35],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3693,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* c-platform.scm:442: chicken.compiler.support#node-class */
t4=*((C_word*)lf[34]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k3703 in k3536 in k3533 in k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in ... */
static void C_ccall f_3705(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_3705,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3708,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:464: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[888];
av2[3]=C_fix(8);
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k3706 in k3703 in k3536 in k3533 in k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in ... */
static void C_ccall f_3708(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3708,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3711,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:466: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[895];
av2[3]=C_fix(13);
av2[4]=C_SCHEME_FALSE;
av2[5]=lf[896];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3709 in k3706 in k3703 in k3536 in k3533 in k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in ... */
static void C_ccall f_3711(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3711,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3714,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:467: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[42];
av2[3]=C_fix(13);
av2[4]=C_SCHEME_FALSE;
av2[5]=lf[894];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3712 in k3709 in k3706 in k3703 in k3536 in k3533 in k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in ... */
static void C_ccall f_3714(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3714,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3717,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:468: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[891];
av2[3]=C_fix(13);
av2[4]=C_fix(2);
av2[5]=lf[893];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3715 in k3712 in k3709 in k3706 in k3703 in k3536 in k3533 in k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in ... */
static void C_ccall f_3717(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3717,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3720,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:469: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[891];
av2[3]=C_fix(13);
av2[4]=C_fix(2);
av2[5]=lf[892];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3718 in k3715 in k3712 in k3709 in k3706 in k3703 in k3536 in k3533 in k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in k3238 in k3235 in k3232 in k3076 in k3073 in k3006 in ... */
static void C_ccall f_3720(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3720,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3723,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:470: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[888];
av2[3]=C_fix(13);
av2[4]=C_fix(2);
av2[5]=lf[890];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3721 in k3718 in k3715 in k3712 in k3709 in k3706 in k3703 in k3536 in k3533 in k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in k3238 in k3235 in k3232 in k3076 in k3073 in ... */
static void C_ccall f_3723(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3723,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3726,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:471: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[888];
av2[3]=C_fix(13);
av2[4]=C_fix(2);
av2[5]=lf[889];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3724 in k3721 in k3718 in k3715 in k3712 in k3709 in k3706 in k3703 in k3536 in k3533 in k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in k3238 in k3235 in k3232 in k3076 in ... */
static void C_ccall f_3726(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3726,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3729,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:472: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[886];
av2[3]=C_fix(13);
av2[4]=C_fix(2);
av2[5]=lf[887];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3727 in k3724 in k3721 in k3718 in k3715 in k3712 in k3709 in k3706 in k3703 in k3536 in k3533 in k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in k3238 in k3235 in k3232 in ... */
static void C_ccall f_3729(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3729,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3732,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:474: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[839];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[885];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3730 in k3727 in k3724 in k3721 in k3718 in k3715 in k3712 in k3709 in k3706 in k3703 in k3536 in k3533 in k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in k3238 in k3235 in ... */
static void C_ccall f_3732(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3732,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3735,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:475: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[837];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[884];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3733 in k3730 in k3727 in k3724 in k3721 in k3718 in k3715 in k3712 in k3709 in k3706 in k3703 in k3536 in k3533 in k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in k3238 in ... */
static void C_ccall f_3735(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3735,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3738,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:476: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[835];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[883];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3736 in k3733 in k3730 in k3727 in k3724 in k3721 in k3718 in k3715 in k3712 in k3709 in k3706 in k3703 in k3536 in k3533 in k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in k3418 in ... */
static void C_ccall f_3738(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3738,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3741,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:477: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[881];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[882];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3739 in k3736 in k3733 in k3730 in k3727 in k3724 in k3721 in k3718 in k3715 in k3712 in k3709 in k3706 in k3703 in k3536 in k3533 in k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in k3421 in ... */
static void C_ccall f_3741(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3741,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3744,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:478: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[879];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[880];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3742 in k3739 in k3736 in k3733 in k3730 in k3727 in k3724 in k3721 in k3718 in k3715 in k3712 in k3709 in k3706 in k3703 in k3536 in k3533 in k3506 in k3503 in k3500 in k3497 in k3494 in k3491 in ... */
static void C_ccall f_3744(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3744,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3747,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:479: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[877];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[878];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3745 in k3742 in k3739 in k3736 in k3733 in k3730 in k3727 in k3724 in k3721 in k3718 in k3715 in k3712 in k3709 in k3706 in k3703 in k3536 in k3533 in k3506 in k3503 in k3500 in k3497 in k3494 in ... */
static void C_ccall f_3747(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3747,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3750,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:480: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[875];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[876];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3748 in k3745 in k3742 in k3739 in k3736 in k3733 in k3730 in k3727 in k3724 in k3721 in k3718 in k3715 in k3712 in k3709 in k3706 in k3703 in k3536 in k3533 in k3506 in k3503 in k3500 in k3497 in ... */
static void C_ccall f_3750(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3750,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3753,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:481: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[873];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[874];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3751 in k3748 in k3745 in k3742 in k3739 in k3736 in k3733 in k3730 in k3727 in k3724 in k3721 in k3718 in k3715 in k3712 in k3709 in k3706 in k3703 in k3536 in k3533 in k3506 in k3503 in k3500 in ... */
static void C_ccall f_3753(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3753,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3756,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:482: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[871];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[872];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3754 in k3751 in k3748 in k3745 in k3742 in k3739 in k3736 in k3733 in k3730 in k3727 in k3724 in k3721 in k3718 in k3715 in k3712 in k3709 in k3706 in k3703 in k3536 in k3533 in k3506 in k3503 in ... */
static void C_ccall f_3756(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3756,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3759,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:483: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[833];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[870];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3757 in k3754 in k3751 in k3748 in k3745 in k3742 in k3739 in k3736 in k3733 in k3730 in k3727 in k3724 in k3721 in k3718 in k3715 in k3712 in k3709 in k3706 in k3703 in k3536 in k3533 in k3506 in ... */
static void C_ccall f_3759(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3759,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3762,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:484: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[868];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[869];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3760 in k3757 in k3754 in k3751 in k3748 in k3745 in k3742 in k3739 in k3736 in k3733 in k3730 in k3727 in k3724 in k3721 in k3718 in k3715 in k3712 in k3709 in k3706 in k3703 in k3536 in k3533 in ... */
static void C_ccall f_3762(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3762,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3765,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:485: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[866];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[867];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3763 in k3760 in k3757 in k3754 in k3751 in k3748 in k3745 in k3742 in k3739 in k3736 in k3733 in k3730 in k3727 in k3724 in k3721 in k3718 in k3715 in k3712 in k3709 in k3706 in k3703 in k3536 in ... */
static void C_ccall f_3765(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3765,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3768,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:486: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[864];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[865];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3766 in k3763 in k3760 in k3757 in k3754 in k3751 in k3748 in k3745 in k3742 in k3739 in k3736 in k3733 in k3730 in k3727 in k3724 in k3721 in k3718 in k3715 in k3712 in k3709 in k3706 in k3703 in ... */
static void C_ccall f_3768(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3768,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3771,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:487: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[862];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[863];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3769 in k3766 in k3763 in k3760 in k3757 in k3754 in k3751 in k3748 in k3745 in k3742 in k3739 in k3736 in k3733 in k3730 in k3727 in k3724 in k3721 in k3718 in k3715 in k3712 in k3709 in k3706 in ... */
static void C_ccall f_3771(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3771,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3774,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:488: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[860];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[861];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3772 in k3769 in k3766 in k3763 in k3760 in k3757 in k3754 in k3751 in k3748 in k3745 in k3742 in k3739 in k3736 in k3733 in k3730 in k3727 in k3724 in k3721 in k3718 in k3715 in k3712 in k3709 in ... */
static void C_ccall f_3774(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3774,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3777,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:489: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[858];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[859];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3775 in k3772 in k3769 in k3766 in k3763 in k3760 in k3757 in k3754 in k3751 in k3748 in k3745 in k3742 in k3739 in k3736 in k3733 in k3730 in k3727 in k3724 in k3721 in k3718 in k3715 in k3712 in ... */
static void C_ccall f_3777(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3777,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3780,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:490: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[856];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[857];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3778 in k3775 in k3772 in k3769 in k3766 in k3763 in k3760 in k3757 in k3754 in k3751 in k3748 in k3745 in k3742 in k3739 in k3736 in k3733 in k3730 in k3727 in k3724 in k3721 in k3718 in k3715 in ... */
static void C_ccall f_3780(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3780,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3783,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:491: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[854];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[855];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3781 in k3778 in k3775 in k3772 in k3769 in k3766 in k3763 in k3760 in k3757 in k3754 in k3751 in k3748 in k3745 in k3742 in k3739 in k3736 in k3733 in k3730 in k3727 in k3724 in k3721 in k3718 in ... */
static void C_ccall f_3783(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3783,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3786,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:492: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[852];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[853];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3784 in k3781 in k3778 in k3775 in k3772 in k3769 in k3766 in k3763 in k3760 in k3757 in k3754 in k3751 in k3748 in k3745 in k3742 in k3739 in k3736 in k3733 in k3730 in k3727 in k3724 in k3721 in ... */
static void C_ccall f_3786(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3786,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3789,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:493: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[850];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[851];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3787 in k3784 in k3781 in k3778 in k3775 in k3772 in k3769 in k3766 in k3763 in k3760 in k3757 in k3754 in k3751 in k3748 in k3745 in k3742 in k3739 in k3736 in k3733 in k3730 in k3727 in k3724 in ... */
static void C_ccall f_3789(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3789,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3792,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:494: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[848];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[849];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3790 in k3787 in k3784 in k3781 in k3778 in k3775 in k3772 in k3769 in k3766 in k3763 in k3760 in k3757 in k3754 in k3751 in k3748 in k3745 in k3742 in k3739 in k3736 in k3733 in k3730 in k3727 in ... */
static void C_ccall f_3792(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3792,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3795,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:495: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[846];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[847];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3793 in k3790 in k3787 in k3784 in k3781 in k3778 in k3775 in k3772 in k3769 in k3766 in k3763 in k3760 in k3757 in k3754 in k3751 in k3748 in k3745 in k3742 in k3739 in k3736 in k3733 in k3730 in ... */
static void C_ccall f_3795(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3795,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3798,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:496: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[844];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[845];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3796 in k3793 in k3790 in k3787 in k3784 in k3781 in k3778 in k3775 in k3772 in k3769 in k3766 in k3763 in k3760 in k3757 in k3754 in k3751 in k3748 in k3745 in k3742 in k3739 in k3736 in k3733 in ... */
static void C_ccall f_3798(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3798,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3801,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:497: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[842];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[843];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3799 in k3796 in k3793 in k3790 in k3787 in k3784 in k3781 in k3778 in k3775 in k3772 in k3769 in k3766 in k3763 in k3760 in k3757 in k3754 in k3751 in k3748 in k3745 in k3742 in k3739 in k3736 in ... */
static void C_ccall f_3801(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3801,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3804,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:498: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[831];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[841];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3802 in k3799 in k3796 in k3793 in k3790 in k3787 in k3784 in k3781 in k3778 in k3775 in k3772 in k3769 in k3766 in k3763 in k3760 in k3757 in k3754 in k3751 in k3748 in k3745 in k3742 in k3739 in ... */
static void C_ccall f_3804(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3804,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3807,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:500: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[839];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[840];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3805 in k3802 in k3799 in k3796 in k3793 in k3790 in k3787 in k3784 in k3781 in k3778 in k3775 in k3772 in k3769 in k3766 in k3763 in k3760 in k3757 in k3754 in k3751 in k3748 in k3745 in k3742 in ... */
static void C_ccall f_3807(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3807,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3810,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:501: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[837];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[838];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3808 in k3805 in k3802 in k3799 in k3796 in k3793 in k3790 in k3787 in k3784 in k3781 in k3778 in k3775 in k3772 in k3769 in k3766 in k3763 in k3760 in k3757 in k3754 in k3751 in k3748 in k3745 in ... */
static void C_ccall f_3810(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3810,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3813,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:502: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[835];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[836];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3811 in k3808 in k3805 in k3802 in k3799 in k3796 in k3793 in k3790 in k3787 in k3784 in k3781 in k3778 in k3775 in k3772 in k3769 in k3766 in k3763 in k3760 in k3757 in k3754 in k3751 in k3748 in ... */
static void C_ccall f_3813(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3813,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3816,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:503: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[833];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[834];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3814 in k3811 in k3808 in k3805 in k3802 in k3799 in k3796 in k3793 in k3790 in k3787 in k3784 in k3781 in k3778 in k3775 in k3772 in k3769 in k3766 in k3763 in k3760 in k3757 in k3754 in k3751 in ... */
static void C_ccall f_3816(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3816,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3819,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:504: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[831];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[832];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3817 in k3814 in k3811 in k3808 in k3805 in k3802 in k3799 in k3796 in k3793 in k3790 in k3787 in k3784 in k3781 in k3778 in k3775 in k3772 in k3769 in k3766 in k3763 in k3760 in k3757 in k3754 in ... */
static void C_ccall f_3819(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3819,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3822,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:506: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[828];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[830];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3820 in k3817 in k3814 in k3811 in k3808 in k3805 in k3802 in k3799 in k3796 in k3793 in k3790 in k3787 in k3784 in k3781 in k3778 in k3775 in k3772 in k3769 in k3766 in k3763 in k3760 in k3757 in ... */
static void C_ccall f_3822(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3822,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3825,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:507: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[828];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[829];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3823 in k3820 in k3817 in k3814 in k3811 in k3808 in k3805 in k3802 in k3799 in k3796 in k3793 in k3790 in k3787 in k3784 in k3781 in k3778 in k3775 in k3772 in k3769 in k3766 in k3763 in k3760 in ... */
static void C_ccall f_3825(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_3825,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3828,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:509: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[826];
av2[3]=C_fix(1);
av2[4]=C_fix(2);
av2[5]=lf[827];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k3826 in k3823 in k3820 in k3817 in k3814 in k3811 in k3808 in k3805 in k3802 in k3799 in k3796 in k3793 in k3790 in k3787 in k3784 in k3781 in k3778 in k3775 in k3772 in k3769 in k3766 in k3763 in ... */
static void C_ccall f_3828(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_3828,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3831,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:510: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[824];
av2[3]=C_fix(1);
av2[4]=C_fix(2);
av2[5]=lf[825];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k3829 in k3826 in k3823 in k3820 in k3817 in k3814 in k3811 in k3808 in k3805 in k3802 in k3799 in k3796 in k3793 in k3790 in k3787 in k3784 in k3781 in k3778 in k3775 in k3772 in k3769 in k3766 in ... */
static void C_ccall f_3831(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_3831,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3834,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:511: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[822];
av2[3]=C_fix(1);
av2[4]=C_fix(2);
av2[5]=lf[823];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k3832 in k3829 in k3826 in k3823 in k3820 in k3817 in k3814 in k3811 in k3808 in k3805 in k3802 in k3799 in k3796 in k3793 in k3790 in k3787 in k3784 in k3781 in k3778 in k3775 in k3772 in k3769 in ... */
static void C_ccall f_3834(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_3834,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3837,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:512: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[820];
av2[3]=C_fix(1);
av2[4]=C_fix(2);
av2[5]=lf[821];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k3835 in k3832 in k3829 in k3826 in k3823 in k3820 in k3817 in k3814 in k3811 in k3808 in k3805 in k3802 in k3799 in k3796 in k3793 in k3790 in k3787 in k3784 in k3781 in k3778 in k3775 in k3772 in ... */
static void C_ccall f_3837(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3837,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3840,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:514: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[817];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[819];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3838 in k3835 in k3832 in k3829 in k3826 in k3823 in k3820 in k3817 in k3814 in k3811 in k3808 in k3805 in k3802 in k3799 in k3796 in k3793 in k3790 in k3787 in k3784 in k3781 in k3778 in k3775 in ... */
static void C_ccall f_3840(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3840,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3843,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:515: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[817];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[818];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3841 in k3838 in k3835 in k3832 in k3829 in k3826 in k3823 in k3820 in k3817 in k3814 in k3811 in k3808 in k3805 in k3802 in k3799 in k3796 in k3793 in k3790 in k3787 in k3784 in k3781 in k3778 in ... */
static void C_ccall f_3843(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3843,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3846,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:516: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[815];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[816];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3844 in k3841 in k3838 in k3835 in k3832 in k3829 in k3826 in k3823 in k3820 in k3817 in k3814 in k3811 in k3808 in k3805 in k3802 in k3799 in k3796 in k3793 in k3790 in k3787 in k3784 in k3781 in ... */
static void C_ccall f_3846(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3846,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3849,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:517: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[813];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[814];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3847 in k3844 in k3841 in k3838 in k3835 in k3832 in k3829 in k3826 in k3823 in k3820 in k3817 in k3814 in k3811 in k3808 in k3805 in k3802 in k3799 in k3796 in k3793 in k3790 in k3787 in k3784 in ... */
static void C_ccall f_3849(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3849,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3852,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:518: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[811];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[812];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3850 in k3847 in k3844 in k3841 in k3838 in k3835 in k3832 in k3829 in k3826 in k3823 in k3820 in k3817 in k3814 in k3811 in k3808 in k3805 in k3802 in k3799 in k3796 in k3793 in k3790 in k3787 in ... */
static void C_ccall f_3852(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3852,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3855,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:519: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[809];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[810];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3853 in k3850 in k3847 in k3844 in k3841 in k3838 in k3835 in k3832 in k3829 in k3826 in k3823 in k3820 in k3817 in k3814 in k3811 in k3808 in k3805 in k3802 in k3799 in k3796 in k3793 in k3790 in ... */
static void C_ccall f_3855(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3855,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3858,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:520: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[807];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[808];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3856 in k3853 in k3850 in k3847 in k3844 in k3841 in k3838 in k3835 in k3832 in k3829 in k3826 in k3823 in k3820 in k3817 in k3814 in k3811 in k3808 in k3805 in k3802 in k3799 in k3796 in k3793 in ... */
static void C_ccall f_3858(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3858,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3861,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:521: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[805];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[806];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3859 in k3856 in k3853 in k3850 in k3847 in k3844 in k3841 in k3838 in k3835 in k3832 in k3829 in k3826 in k3823 in k3820 in k3817 in k3814 in k3811 in k3808 in k3805 in k3802 in k3799 in k3796 in ... */
static void C_ccall f_3861(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3861,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3864,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:522: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[803];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[804];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3862 in k3859 in k3856 in k3853 in k3850 in k3847 in k3844 in k3841 in k3838 in k3835 in k3832 in k3829 in k3826 in k3823 in k3820 in k3817 in k3814 in k3811 in k3808 in k3805 in k3802 in k3799 in ... */
static void C_ccall f_3864(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3864,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3867,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:523: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[801];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[802];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3865 in k3862 in k3859 in k3856 in k3853 in k3850 in k3847 in k3844 in k3841 in k3838 in k3835 in k3832 in k3829 in k3826 in k3823 in k3820 in k3817 in k3814 in k3811 in k3808 in k3805 in k3802 in ... */
static void C_ccall f_3867(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3867,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3870,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:524: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[799];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[800];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3868 in k3865 in k3862 in k3859 in k3856 in k3853 in k3850 in k3847 in k3844 in k3841 in k3838 in k3835 in k3832 in k3829 in k3826 in k3823 in k3820 in k3817 in k3814 in k3811 in k3808 in k3805 in ... */
static void C_ccall f_3870(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3870,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3873,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:525: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[797];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[798];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3871 in k3868 in k3865 in k3862 in k3859 in k3856 in k3853 in k3850 in k3847 in k3844 in k3841 in k3838 in k3835 in k3832 in k3829 in k3826 in k3823 in k3820 in k3817 in k3814 in k3811 in k3808 in ... */
static void C_ccall f_3873(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3873,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3876,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:526: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[795];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[796];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3874 in k3871 in k3868 in k3865 in k3862 in k3859 in k3856 in k3853 in k3850 in k3847 in k3844 in k3841 in k3838 in k3835 in k3832 in k3829 in k3826 in k3823 in k3820 in k3817 in k3814 in k3811 in ... */
static void C_ccall f_3876(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3876,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3879,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:527: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[793];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[794];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3877 in k3874 in k3871 in k3868 in k3865 in k3862 in k3859 in k3856 in k3853 in k3850 in k3847 in k3844 in k3841 in k3838 in k3835 in k3832 in k3829 in k3826 in k3823 in k3820 in k3817 in k3814 in ... */
static void C_ccall f_3879(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3879,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3882,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:528: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[791];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[792];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3880 in k3877 in k3874 in k3871 in k3868 in k3865 in k3862 in k3859 in k3856 in k3853 in k3850 in k3847 in k3844 in k3841 in k3838 in k3835 in k3832 in k3829 in k3826 in k3823 in k3820 in k3817 in ... */
static void C_ccall f_3882(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3882,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3885,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:529: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[789];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[790];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3883 in k3880 in k3877 in k3874 in k3871 in k3868 in k3865 in k3862 in k3859 in k3856 in k3853 in k3850 in k3847 in k3844 in k3841 in k3838 in k3835 in k3832 in k3829 in k3826 in k3823 in k3820 in ... */
static void C_ccall f_3885(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3885,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3888,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:530: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[787];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[788];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3886 in k3883 in k3880 in k3877 in k3874 in k3871 in k3868 in k3865 in k3862 in k3859 in k3856 in k3853 in k3850 in k3847 in k3844 in k3841 in k3838 in k3835 in k3832 in k3829 in k3826 in k3823 in ... */
static void C_ccall f_3888(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3888,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3891,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:531: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[785];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[786];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3889 in k3886 in k3883 in k3880 in k3877 in k3874 in k3871 in k3868 in k3865 in k3862 in k3859 in k3856 in k3853 in k3850 in k3847 in k3844 in k3841 in k3838 in k3835 in k3832 in k3829 in k3826 in ... */
static void C_ccall f_3891(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3891,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3894,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:532: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[783];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[784];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3892 in k3889 in k3886 in k3883 in k3880 in k3877 in k3874 in k3871 in k3868 in k3865 in k3862 in k3859 in k3856 in k3853 in k3850 in k3847 in k3844 in k3841 in k3838 in k3835 in k3832 in k3829 in ... */
static void C_ccall f_3894(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3894,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3897,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:533: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[781];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[782];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3895 in k3892 in k3889 in k3886 in k3883 in k3880 in k3877 in k3874 in k3871 in k3868 in k3865 in k3862 in k3859 in k3856 in k3853 in k3850 in k3847 in k3844 in k3841 in k3838 in k3835 in k3832 in ... */
static void C_ccall f_3897(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3897,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3900,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:534: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[779];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[780];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3898 in k3895 in k3892 in k3889 in k3886 in k3883 in k3880 in k3877 in k3874 in k3871 in k3868 in k3865 in k3862 in k3859 in k3856 in k3853 in k3850 in k3847 in k3844 in k3841 in k3838 in k3835 in ... */
static void C_ccall f_3900(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3900,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3903,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:535: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[777];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[778];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3901 in k3898 in k3895 in k3892 in k3889 in k3886 in k3883 in k3880 in k3877 in k3874 in k3871 in k3868 in k3865 in k3862 in k3859 in k3856 in k3853 in k3850 in k3847 in k3844 in k3841 in k3838 in ... */
static void C_ccall f_3903(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3903,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3906,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:536: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[775];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[776];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3904 in k3901 in k3898 in k3895 in k3892 in k3889 in k3886 in k3883 in k3880 in k3877 in k3874 in k3871 in k3868 in k3865 in k3862 in k3859 in k3856 in k3853 in k3850 in k3847 in k3844 in k3841 in ... */
static void C_ccall f_3906(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3906,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3909,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:537: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[773];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[774];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3907 in k3904 in k3901 in k3898 in k3895 in k3892 in k3889 in k3886 in k3883 in k3880 in k3877 in k3874 in k3871 in k3868 in k3865 in k3862 in k3859 in k3856 in k3853 in k3850 in k3847 in k3844 in ... */
static void C_ccall f_3909(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3909,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3912,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:538: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[771];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[772];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3910 in k3907 in k3904 in k3901 in k3898 in k3895 in k3892 in k3889 in k3886 in k3883 in k3880 in k3877 in k3874 in k3871 in k3868 in k3865 in k3862 in k3859 in k3856 in k3853 in k3850 in k3847 in ... */
static void C_ccall f_3912(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3912,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3915,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:539: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[769];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[770];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3913 in k3910 in k3907 in k3904 in k3901 in k3898 in k3895 in k3892 in k3889 in k3886 in k3883 in k3880 in k3877 in k3874 in k3871 in k3868 in k3865 in k3862 in k3859 in k3856 in k3853 in k3850 in ... */
static void C_ccall f_3915(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3915,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3918,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:540: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[767];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[768];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3916 in k3913 in k3910 in k3907 in k3904 in k3901 in k3898 in k3895 in k3892 in k3889 in k3886 in k3883 in k3880 in k3877 in k3874 in k3871 in k3868 in k3865 in k3862 in k3859 in k3856 in k3853 in ... */
static void C_ccall f_3918(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3918,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3921,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:541: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[765];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[766];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3919 in k3916 in k3913 in k3910 in k3907 in k3904 in k3901 in k3898 in k3895 in k3892 in k3889 in k3886 in k3883 in k3880 in k3877 in k3874 in k3871 in k3868 in k3865 in k3862 in k3859 in k3856 in ... */
static void C_ccall f_3921(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3921,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3924,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:542: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[763];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[764];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3922 in k3919 in k3916 in k3913 in k3910 in k3907 in k3904 in k3901 in k3898 in k3895 in k3892 in k3889 in k3886 in k3883 in k3880 in k3877 in k3874 in k3871 in k3868 in k3865 in k3862 in k3859 in ... */
static void C_ccall f_3924(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3924,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3927,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:543: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[761];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[762];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3925 in k3922 in k3919 in k3916 in k3913 in k3910 in k3907 in k3904 in k3901 in k3898 in k3895 in k3892 in k3889 in k3886 in k3883 in k3880 in k3877 in k3874 in k3871 in k3868 in k3865 in k3862 in ... */
static void C_ccall f_3927(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3927,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3930,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:544: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[759];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[760];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3928 in k3925 in k3922 in k3919 in k3916 in k3913 in k3910 in k3907 in k3904 in k3901 in k3898 in k3895 in k3892 in k3889 in k3886 in k3883 in k3880 in k3877 in k3874 in k3871 in k3868 in k3865 in ... */
static void C_ccall f_3930(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3930,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3933,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:545: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[757];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[758];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3931 in k3928 in k3925 in k3922 in k3919 in k3916 in k3913 in k3910 in k3907 in k3904 in k3901 in k3898 in k3895 in k3892 in k3889 in k3886 in k3883 in k3880 in k3877 in k3874 in k3871 in k3868 in ... */
static void C_ccall f_3933(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3933,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3936,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:546: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[755];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[756];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3934 in k3931 in k3928 in k3925 in k3922 in k3919 in k3916 in k3913 in k3910 in k3907 in k3904 in k3901 in k3898 in k3895 in k3892 in k3889 in k3886 in k3883 in k3880 in k3877 in k3874 in k3871 in ... */
static void C_ccall f_3936(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3936,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3939,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:547: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[753];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[754];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3937 in k3934 in k3931 in k3928 in k3925 in k3922 in k3919 in k3916 in k3913 in k3910 in k3907 in k3904 in k3901 in k3898 in k3895 in k3892 in k3889 in k3886 in k3883 in k3880 in k3877 in k3874 in ... */
static void C_ccall f_3939(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3939,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3942,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:548: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[751];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[752];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3940 in k3937 in k3934 in k3931 in k3928 in k3925 in k3922 in k3919 in k3916 in k3913 in k3910 in k3907 in k3904 in k3901 in k3898 in k3895 in k3892 in k3889 in k3886 in k3883 in k3880 in k3877 in ... */
static void C_ccall f_3942(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3942,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3945,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:549: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[749];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[750];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3943 in k3940 in k3937 in k3934 in k3931 in k3928 in k3925 in k3922 in k3919 in k3916 in k3913 in k3910 in k3907 in k3904 in k3901 in k3898 in k3895 in k3892 in k3889 in k3886 in k3883 in k3880 in ... */
static void C_ccall f_3945(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3945,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3948,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:550: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[747];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[748];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3946 in k3943 in k3940 in k3937 in k3934 in k3931 in k3928 in k3925 in k3922 in k3919 in k3916 in k3913 in k3910 in k3907 in k3904 in k3901 in k3898 in k3895 in k3892 in k3889 in k3886 in k3883 in ... */
static void C_ccall f_3948(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3948,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3951,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:551: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[745];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[746];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3949 in k3946 in k3943 in k3940 in k3937 in k3934 in k3931 in k3928 in k3925 in k3922 in k3919 in k3916 in k3913 in k3910 in k3907 in k3904 in k3901 in k3898 in k3895 in k3892 in k3889 in k3886 in ... */
static void C_ccall f_3951(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3951,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3954,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:552: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[743];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[744];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3952 in k3949 in k3946 in k3943 in k3940 in k3937 in k3934 in k3931 in k3928 in k3925 in k3922 in k3919 in k3916 in k3913 in k3910 in k3907 in k3904 in k3901 in k3898 in k3895 in k3892 in k3889 in ... */
static void C_ccall f_3954(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3954,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3957,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:553: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[741];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[742];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3955 in k3952 in k3949 in k3946 in k3943 in k3940 in k3937 in k3934 in k3931 in k3928 in k3925 in k3922 in k3919 in k3916 in k3913 in k3910 in k3907 in k3904 in k3901 in k3898 in k3895 in k3892 in ... */
static void C_ccall f_3957(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3957,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3960,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:554: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[739];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[740];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3958 in k3955 in k3952 in k3949 in k3946 in k3943 in k3940 in k3937 in k3934 in k3931 in k3928 in k3925 in k3922 in k3919 in k3916 in k3913 in k3910 in k3907 in k3904 in k3901 in k3898 in k3895 in ... */
static void C_ccall f_3960(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3960,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3963,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:555: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[737];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[738];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3961 in k3958 in k3955 in k3952 in k3949 in k3946 in k3943 in k3940 in k3937 in k3934 in k3931 in k3928 in k3925 in k3922 in k3919 in k3916 in k3913 in k3910 in k3907 in k3904 in k3901 in k3898 in ... */
static void C_ccall f_3963(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3963,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3966,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:556: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[735];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[736];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3964 in k3961 in k3958 in k3955 in k3952 in k3949 in k3946 in k3943 in k3940 in k3937 in k3934 in k3931 in k3928 in k3925 in k3922 in k3919 in k3916 in k3913 in k3910 in k3907 in k3904 in k3901 in ... */
static void C_ccall f_3966(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3966,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3969,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:557: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[733];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[734];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3967 in k3964 in k3961 in k3958 in k3955 in k3952 in k3949 in k3946 in k3943 in k3940 in k3937 in k3934 in k3931 in k3928 in k3925 in k3922 in k3919 in k3916 in k3913 in k3910 in k3907 in k3904 in ... */
static void C_ccall f_3969(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3969,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3972,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:558: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[731];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[732];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3970 in k3967 in k3964 in k3961 in k3958 in k3955 in k3952 in k3949 in k3946 in k3943 in k3940 in k3937 in k3934 in k3931 in k3928 in k3925 in k3922 in k3919 in k3916 in k3913 in k3910 in k3907 in ... */
static void C_ccall f_3972(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3972,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3975,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:559: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[729];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[730];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3973 in k3970 in k3967 in k3964 in k3961 in k3958 in k3955 in k3952 in k3949 in k3946 in k3943 in k3940 in k3937 in k3934 in k3931 in k3928 in k3925 in k3922 in k3919 in k3916 in k3913 in k3910 in ... */
static void C_ccall f_3975(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3975,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3978,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:560: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[726];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[728];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3976 in k3973 in k3970 in k3967 in k3964 in k3961 in k3958 in k3955 in k3952 in k3949 in k3946 in k3943 in k3940 in k3937 in k3934 in k3931 in k3928 in k3925 in k3922 in k3919 in k3916 in k3913 in ... */
static void C_ccall f_3978(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3978,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3981,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:561: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[726];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[727];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3979 in k3976 in k3973 in k3970 in k3967 in k3964 in k3961 in k3958 in k3955 in k3952 in k3949 in k3946 in k3943 in k3940 in k3937 in k3934 in k3931 in k3928 in k3925 in k3922 in k3919 in k3916 in ... */
static void C_ccall f_3981(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3981,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3984,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:562: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[723];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[725];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3982 in k3979 in k3976 in k3973 in k3970 in k3967 in k3964 in k3961 in k3958 in k3955 in k3952 in k3949 in k3946 in k3943 in k3940 in k3937 in k3934 in k3931 in k3928 in k3925 in k3922 in k3919 in ... */
static void C_ccall f_3984(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3984,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3987,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:563: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[723];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[724];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3985 in k3982 in k3979 in k3976 in k3973 in k3970 in k3967 in k3964 in k3961 in k3958 in k3955 in k3952 in k3949 in k3946 in k3943 in k3940 in k3937 in k3934 in k3931 in k3928 in k3925 in k3922 in ... */
static void C_ccall f_3987(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3987,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3990,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:564: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[721];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[722];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3988 in k3985 in k3982 in k3979 in k3976 in k3973 in k3970 in k3967 in k3964 in k3961 in k3958 in k3955 in k3952 in k3949 in k3946 in k3943 in k3940 in k3937 in k3934 in k3931 in k3928 in k3925 in ... */
static void C_ccall f_3990(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3990,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3993,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:565: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[719];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[720];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3991 in k3988 in k3985 in k3982 in k3979 in k3976 in k3973 in k3970 in k3967 in k3964 in k3961 in k3958 in k3955 in k3952 in k3949 in k3946 in k3943 in k3940 in k3937 in k3934 in k3931 in k3928 in ... */
static void C_ccall f_3993(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3993,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3996,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:566: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[716];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[718];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3994 in k3991 in k3988 in k3985 in k3982 in k3979 in k3976 in k3973 in k3970 in k3967 in k3964 in k3961 in k3958 in k3955 in k3952 in k3949 in k3946 in k3943 in k3940 in k3937 in k3934 in k3931 in ... */
static void C_ccall f_3996(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3996,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3999,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:567: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[716];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[717];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k3997 in k3994 in k3991 in k3988 in k3985 in k3982 in k3979 in k3976 in k3973 in k3970 in k3967 in k3964 in k3961 in k3958 in k3955 in k3952 in k3949 in k3946 in k3943 in k3940 in k3937 in k3934 in ... */
static void C_ccall f_3999(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3999,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4002,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:568: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[713];
av2[3]=C_fix(2);
av2[4]=C_fix(3);
av2[5]=lf[715];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4000 in k3997 in k3994 in k3991 in k3988 in k3985 in k3982 in k3979 in k3976 in k3973 in k3970 in k3967 in k3964 in k3961 in k3958 in k3955 in k3952 in k3949 in k3946 in k3943 in k3940 in k3937 in ... */
static void C_ccall f_4002(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4002,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4005,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:569: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[713];
av2[3]=C_fix(2);
av2[4]=C_fix(3);
av2[5]=lf[714];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4003 in k4000 in k3997 in k3994 in k3991 in k3988 in k3985 in k3982 in k3979 in k3976 in k3973 in k3970 in k3967 in k3964 in k3961 in k3958 in k3955 in k3952 in k3949 in k3946 in k3943 in k3940 in ... */
static void C_ccall f_4005(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4005,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4008,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:570: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[710];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[712];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4006 in k4003 in k4000 in k3997 in k3994 in k3991 in k3988 in k3985 in k3982 in k3979 in k3976 in k3973 in k3970 in k3967 in k3964 in k3961 in k3958 in k3955 in k3952 in k3949 in k3946 in k3943 in ... */
static void C_ccall f_4008(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4008,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4011,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:571: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[710];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[711];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4009 in k4006 in k4003 in k4000 in k3997 in k3994 in k3991 in k3988 in k3985 in k3982 in k3979 in k3976 in k3973 in k3970 in k3967 in k3964 in k3961 in k3958 in k3955 in k3952 in k3949 in k3946 in ... */
static void C_ccall f_4011(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4011,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4014,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:572: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[707];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[709];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4012 in k4009 in k4006 in k4003 in k4000 in k3997 in k3994 in k3991 in k3988 in k3985 in k3982 in k3979 in k3976 in k3973 in k3970 in k3967 in k3964 in k3961 in k3958 in k3955 in k3952 in k3949 in ... */
static void C_ccall f_4014(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4014,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4017,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:573: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[707];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[708];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4015 in k4012 in k4009 in k4006 in k4003 in k4000 in k3997 in k3994 in k3991 in k3988 in k3985 in k3982 in k3979 in k3976 in k3973 in k3970 in k3967 in k3964 in k3961 in k3958 in k3955 in k3952 in ... */
static void C_ccall f_4017(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4017,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4020,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:574: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[704];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[706];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4018 in k4015 in k4012 in k4009 in k4006 in k4003 in k4000 in k3997 in k3994 in k3991 in k3988 in k3985 in k3982 in k3979 in k3976 in k3973 in k3970 in k3967 in k3964 in k3961 in k3958 in k3955 in ... */
static void C_ccall f_4020(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4020,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4023,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:575: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[704];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[705];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4021 in k4018 in k4015 in k4012 in k4009 in k4006 in k4003 in k4000 in k3997 in k3994 in k3991 in k3988 in k3985 in k3982 in k3979 in k3976 in k3973 in k3970 in k3967 in k3964 in k3961 in k3958 in ... */
static void C_ccall f_4023(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4023,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4026,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:576: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[701];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[703];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4024 in k4021 in k4018 in k4015 in k4012 in k4009 in k4006 in k4003 in k4000 in k3997 in k3994 in k3991 in k3988 in k3985 in k3982 in k3979 in k3976 in k3973 in k3970 in k3967 in k3964 in k3961 in ... */
static void C_ccall f_4026(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4026,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4029,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:577: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[701];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[702];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4027 in k4024 in k4021 in k4018 in k4015 in k4012 in k4009 in k4006 in k4003 in k4000 in k3997 in k3994 in k3991 in k3988 in k3985 in k3982 in k3979 in k3976 in k3973 in k3970 in k3967 in k3964 in ... */
static void C_ccall f_4029(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4029,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4032,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:578: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[698];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[700];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4030 in k4027 in k4024 in k4021 in k4018 in k4015 in k4012 in k4009 in k4006 in k4003 in k4000 in k3997 in k3994 in k3991 in k3988 in k3985 in k3982 in k3979 in k3976 in k3973 in k3970 in k3967 in ... */
static void C_ccall f_4032(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4032,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4035,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:579: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[698];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[699];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4033 in k4030 in k4027 in k4024 in k4021 in k4018 in k4015 in k4012 in k4009 in k4006 in k4003 in k4000 in k3997 in k3994 in k3991 in k3988 in k3985 in k3982 in k3979 in k3976 in k3973 in k3970 in ... */
static void C_ccall f_4035(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4035,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4038,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:580: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[695];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[697];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4036 in k4033 in k4030 in k4027 in k4024 in k4021 in k4018 in k4015 in k4012 in k4009 in k4006 in k4003 in k4000 in k3997 in k3994 in k3991 in k3988 in k3985 in k3982 in k3979 in k3976 in k3973 in ... */
static void C_ccall f_4038(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4038,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4041,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:581: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[695];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[696];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4039 in k4036 in k4033 in k4030 in k4027 in k4024 in k4021 in k4018 in k4015 in k4012 in k4009 in k4006 in k4003 in k4000 in k3997 in k3994 in k3991 in k3988 in k3985 in k3982 in k3979 in k3976 in ... */
static void C_ccall f_4041(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4041,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4044,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:582: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[693];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[694];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4042 in k4039 in k4036 in k4033 in k4030 in k4027 in k4024 in k4021 in k4018 in k4015 in k4012 in k4009 in k4006 in k4003 in k4000 in k3997 in k3994 in k3991 in k3988 in k3985 in k3982 in k3979 in ... */
static void C_ccall f_4044(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4044,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4047,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:583: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[691];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[692];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4045 in k4042 in k4039 in k4036 in k4033 in k4030 in k4027 in k4024 in k4021 in k4018 in k4015 in k4012 in k4009 in k4006 in k4003 in k4000 in k3997 in k3994 in k3991 in k3988 in k3985 in k3982 in ... */
static void C_ccall f_4047(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4047,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4050,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:584: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[689];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[690];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4048 in k4045 in k4042 in k4039 in k4036 in k4033 in k4030 in k4027 in k4024 in k4021 in k4018 in k4015 in k4012 in k4009 in k4006 in k4003 in k4000 in k3997 in k3994 in k3991 in k3988 in k3985 in ... */
static void C_ccall f_4050(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4050,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4053,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:585: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[687];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[688];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4051 in k4048 in k4045 in k4042 in k4039 in k4036 in k4033 in k4030 in k4027 in k4024 in k4021 in k4018 in k4015 in k4012 in k4009 in k4006 in k4003 in k4000 in k3997 in k3994 in k3991 in k3988 in ... */
static void C_ccall f_4053(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4053,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4056,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:586: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[685];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[686];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4054 in k4051 in k4048 in k4045 in k4042 in k4039 in k4036 in k4033 in k4030 in k4027 in k4024 in k4021 in k4018 in k4015 in k4012 in k4009 in k4006 in k4003 in k4000 in k3997 in k3994 in k3991 in ... */
static void C_ccall f_4056(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4056,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4059,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:587: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[683];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[684];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4057 in k4054 in k4051 in k4048 in k4045 in k4042 in k4039 in k4036 in k4033 in k4030 in k4027 in k4024 in k4021 in k4018 in k4015 in k4012 in k4009 in k4006 in k4003 in k4000 in k3997 in k3994 in ... */
static void C_ccall f_4059(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4059,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4062,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:588: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[681];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[682];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4060 in k4057 in k4054 in k4051 in k4048 in k4045 in k4042 in k4039 in k4036 in k4033 in k4030 in k4027 in k4024 in k4021 in k4018 in k4015 in k4012 in k4009 in k4006 in k4003 in k4000 in k3997 in ... */
static void C_ccall f_4062(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4062,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4065,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:589: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[679];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[680];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4063 in k4060 in k4057 in k4054 in k4051 in k4048 in k4045 in k4042 in k4039 in k4036 in k4033 in k4030 in k4027 in k4024 in k4021 in k4018 in k4015 in k4012 in k4009 in k4006 in k4003 in k4000 in ... */
static void C_ccall f_4065(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4065,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4068,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:590: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[677];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[678];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4066 in k4063 in k4060 in k4057 in k4054 in k4051 in k4048 in k4045 in k4042 in k4039 in k4036 in k4033 in k4030 in k4027 in k4024 in k4021 in k4018 in k4015 in k4012 in k4009 in k4006 in k4003 in ... */
static void C_ccall f_4068(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4068,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4071,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:591: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[675];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[676];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4069 in k4066 in k4063 in k4060 in k4057 in k4054 in k4051 in k4048 in k4045 in k4042 in k4039 in k4036 in k4033 in k4030 in k4027 in k4024 in k4021 in k4018 in k4015 in k4012 in k4009 in k4006 in ... */
static void C_ccall f_4071(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4071,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4074,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:592: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[673];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[674];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4072 in k4069 in k4066 in k4063 in k4060 in k4057 in k4054 in k4051 in k4048 in k4045 in k4042 in k4039 in k4036 in k4033 in k4030 in k4027 in k4024 in k4021 in k4018 in k4015 in k4012 in k4009 in ... */
static void C_ccall f_4074(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4074,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4077,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:593: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[671];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[672];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4075 in k4072 in k4069 in k4066 in k4063 in k4060 in k4057 in k4054 in k4051 in k4048 in k4045 in k4042 in k4039 in k4036 in k4033 in k4030 in k4027 in k4024 in k4021 in k4018 in k4015 in k4012 in ... */
static void C_ccall f_4077(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4077,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4080,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:594: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[669];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[670];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4078 in k4075 in k4072 in k4069 in k4066 in k4063 in k4060 in k4057 in k4054 in k4051 in k4048 in k4045 in k4042 in k4039 in k4036 in k4033 in k4030 in k4027 in k4024 in k4021 in k4018 in k4015 in ... */
static void C_ccall f_4080(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4080,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4083,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:595: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[667];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[668];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4081 in k4078 in k4075 in k4072 in k4069 in k4066 in k4063 in k4060 in k4057 in k4054 in k4051 in k4048 in k4045 in k4042 in k4039 in k4036 in k4033 in k4030 in k4027 in k4024 in k4021 in k4018 in ... */
static void C_ccall f_4083(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4083,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4086,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:596: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[665];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[666];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4084 in k4081 in k4078 in k4075 in k4072 in k4069 in k4066 in k4063 in k4060 in k4057 in k4054 in k4051 in k4048 in k4045 in k4042 in k4039 in k4036 in k4033 in k4030 in k4027 in k4024 in k4021 in ... */
static void C_ccall f_4086(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4086,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4089,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:597: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[663];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[664];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4087 in k4084 in k4081 in k4078 in k4075 in k4072 in k4069 in k4066 in k4063 in k4060 in k4057 in k4054 in k4051 in k4048 in k4045 in k4042 in k4039 in k4036 in k4033 in k4030 in k4027 in k4024 in ... */
static void C_ccall f_4089(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4089,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4092,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:598: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[661];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[662];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4090 in k4087 in k4084 in k4081 in k4078 in k4075 in k4072 in k4069 in k4066 in k4063 in k4060 in k4057 in k4054 in k4051 in k4048 in k4045 in k4042 in k4039 in k4036 in k4033 in k4030 in k4027 in ... */
static void C_ccall f_4092(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4092,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4095,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:599: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[659];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[660];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4093 in k4090 in k4087 in k4084 in k4081 in k4078 in k4075 in k4072 in k4069 in k4066 in k4063 in k4060 in k4057 in k4054 in k4051 in k4048 in k4045 in k4042 in k4039 in k4036 in k4033 in k4030 in ... */
static void C_ccall f_4095(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4095,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4098,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:600: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[657];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[658];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4096 in k4093 in k4090 in k4087 in k4084 in k4081 in k4078 in k4075 in k4072 in k4069 in k4066 in k4063 in k4060 in k4057 in k4054 in k4051 in k4048 in k4045 in k4042 in k4039 in k4036 in k4033 in ... */
static void C_ccall f_4098(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4098,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4101,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:601: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[655];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[656];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4099 in k4096 in k4093 in k4090 in k4087 in k4084 in k4081 in k4078 in k4075 in k4072 in k4069 in k4066 in k4063 in k4060 in k4057 in k4054 in k4051 in k4048 in k4045 in k4042 in k4039 in k4036 in ... */
static void C_ccall f_4101(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4101,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4104,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:602: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[653];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[654];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4102 in k4099 in k4096 in k4093 in k4090 in k4087 in k4084 in k4081 in k4078 in k4075 in k4072 in k4069 in k4066 in k4063 in k4060 in k4057 in k4054 in k4051 in k4048 in k4045 in k4042 in k4039 in ... */
static void C_ccall f_4104(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4104,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4107,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:603: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[651];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[652];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4105 in k4102 in k4099 in k4096 in k4093 in k4090 in k4087 in k4084 in k4081 in k4078 in k4075 in k4072 in k4069 in k4066 in k4063 in k4060 in k4057 in k4054 in k4051 in k4048 in k4045 in k4042 in ... */
static void C_ccall f_4107(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4107,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4110,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:604: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[649];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[650];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4108 in k4105 in k4102 in k4099 in k4096 in k4093 in k4090 in k4087 in k4084 in k4081 in k4078 in k4075 in k4072 in k4069 in k4066 in k4063 in k4060 in k4057 in k4054 in k4051 in k4048 in k4045 in ... */
static void C_ccall f_4110(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4110,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4113,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:605: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[647];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[648];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4111 in k4108 in k4105 in k4102 in k4099 in k4096 in k4093 in k4090 in k4087 in k4084 in k4081 in k4078 in k4075 in k4072 in k4069 in k4066 in k4063 in k4060 in k4057 in k4054 in k4051 in k4048 in ... */
static void C_ccall f_4113(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4113,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4116,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:606: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[645];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[646];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4114 in k4111 in k4108 in k4105 in k4102 in k4099 in k4096 in k4093 in k4090 in k4087 in k4084 in k4081 in k4078 in k4075 in k4072 in k4069 in k4066 in k4063 in k4060 in k4057 in k4054 in k4051 in ... */
static void C_ccall f_4116(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4116,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4119,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:607: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[643];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[644];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4117 in k4114 in k4111 in k4108 in k4105 in k4102 in k4099 in k4096 in k4093 in k4090 in k4087 in k4084 in k4081 in k4078 in k4075 in k4072 in k4069 in k4066 in k4063 in k4060 in k4057 in k4054 in ... */
static void C_ccall f_4119(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4119,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4122,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:608: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[641];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[642];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4120 in k4117 in k4114 in k4111 in k4108 in k4105 in k4102 in k4099 in k4096 in k4093 in k4090 in k4087 in k4084 in k4081 in k4078 in k4075 in k4072 in k4069 in k4066 in k4063 in k4060 in k4057 in ... */
static void C_ccall f_4122(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4122,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4125,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:609: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[639];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[640];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4123 in k4120 in k4117 in k4114 in k4111 in k4108 in k4105 in k4102 in k4099 in k4096 in k4093 in k4090 in k4087 in k4084 in k4081 in k4078 in k4075 in k4072 in k4069 in k4066 in k4063 in k4060 in ... */
static void C_ccall f_4125(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4125,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4128,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:610: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[637];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[638];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4126 in k4123 in k4120 in k4117 in k4114 in k4111 in k4108 in k4105 in k4102 in k4099 in k4096 in k4093 in k4090 in k4087 in k4084 in k4081 in k4078 in k4075 in k4072 in k4069 in k4066 in k4063 in ... */
static void C_ccall f_4128(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4128,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4131,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:611: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[635];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[636];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4129 in k4126 in k4123 in k4120 in k4117 in k4114 in k4111 in k4108 in k4105 in k4102 in k4099 in k4096 in k4093 in k4090 in k4087 in k4084 in k4081 in k4078 in k4075 in k4072 in k4069 in k4066 in ... */
static void C_ccall f_4131(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4131,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4134,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:612: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[633];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[634];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4132 in k4129 in k4126 in k4123 in k4120 in k4117 in k4114 in k4111 in k4108 in k4105 in k4102 in k4099 in k4096 in k4093 in k4090 in k4087 in k4084 in k4081 in k4078 in k4075 in k4072 in k4069 in ... */
static void C_ccall f_4134(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4134,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4137,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:613: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[631];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[632];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4135 in k4132 in k4129 in k4126 in k4123 in k4120 in k4117 in k4114 in k4111 in k4108 in k4105 in k4102 in k4099 in k4096 in k4093 in k4090 in k4087 in k4084 in k4081 in k4078 in k4075 in k4072 in ... */
static void C_ccall f_4137(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4137,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4140,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:614: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[629];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[630];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4138 in k4135 in k4132 in k4129 in k4126 in k4123 in k4120 in k4117 in k4114 in k4111 in k4108 in k4105 in k4102 in k4099 in k4096 in k4093 in k4090 in k4087 in k4084 in k4081 in k4078 in k4075 in ... */
static void C_ccall f_4140(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4140,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4143,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:615: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[627];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[628];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4141 in k4138 in k4135 in k4132 in k4129 in k4126 in k4123 in k4120 in k4117 in k4114 in k4111 in k4108 in k4105 in k4102 in k4099 in k4096 in k4093 in k4090 in k4087 in k4084 in k4081 in k4078 in ... */
static void C_ccall f_4143(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4143,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4146,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:616: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[625];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[626];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4144 in k4141 in k4138 in k4135 in k4132 in k4129 in k4126 in k4123 in k4120 in k4117 in k4114 in k4111 in k4108 in k4105 in k4102 in k4099 in k4096 in k4093 in k4090 in k4087 in k4084 in k4081 in ... */
static void C_ccall f_4146(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4146,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4149,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:617: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[623];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[624];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4147 in k4144 in k4141 in k4138 in k4135 in k4132 in k4129 in k4126 in k4123 in k4120 in k4117 in k4114 in k4111 in k4108 in k4105 in k4102 in k4099 in k4096 in k4093 in k4090 in k4087 in k4084 in ... */
static void C_ccall f_4149(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4149,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4152,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:618: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[621];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[622];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4150 in k4147 in k4144 in k4141 in k4138 in k4135 in k4132 in k4129 in k4126 in k4123 in k4120 in k4117 in k4114 in k4111 in k4108 in k4105 in k4102 in k4099 in k4096 in k4093 in k4090 in k4087 in ... */
static void C_ccall f_4152(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4152,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4155,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:620: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[617];
av2[3]=C_fix(14);
av2[4]=lf[24];
av2[5]=C_fix(2);
av2[6]=lf[619];
av2[7]=lf[620];
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4153 in k4150 in k4147 in k4144 in k4141 in k4138 in k4135 in k4132 in k4129 in k4126 in k4123 in k4120 in k4117 in k4114 in k4111 in k4108 in k4105 in k4102 in k4099 in k4096 in k4093 in k4090 in ... */
static void C_ccall f_4155(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4155,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4158,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:621: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[617];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[618];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4156 in k4153 in k4150 in k4147 in k4144 in k4141 in k4138 in k4135 in k4132 in k4129 in k4126 in k4123 in k4120 in k4117 in k4114 in k4111 in k4108 in k4105 in k4102 in k4099 in k4096 in k4093 in ... */
static void C_ccall f_4158(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4158,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4161,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:622: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[613];
av2[3]=C_fix(14);
av2[4]=lf[24];
av2[5]=C_fix(2);
av2[6]=lf[615];
av2[7]=lf[616];
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4159 in k4156 in k4153 in k4150 in k4147 in k4144 in k4141 in k4138 in k4135 in k4132 in k4129 in k4126 in k4123 in k4120 in k4117 in k4114 in k4111 in k4108 in k4105 in k4102 in k4099 in k4096 in ... */
static void C_ccall f_4161(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4161,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4164,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:623: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[613];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[614];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4162 in k4159 in k4156 in k4153 in k4150 in k4147 in k4144 in k4141 in k4138 in k4135 in k4132 in k4129 in k4126 in k4123 in k4120 in k4117 in k4114 in k4111 in k4108 in k4105 in k4102 in k4099 in ... */
static void C_ccall f_4164(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4164,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4167,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:624: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[610];
av2[3]=C_fix(17);
av2[4]=C_fix(2);
av2[5]=lf[611];
av2[6]=lf[612];
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4165 in k4162 in k4159 in k4156 in k4153 in k4150 in k4147 in k4144 in k4141 in k4138 in k4135 in k4132 in k4129 in k4126 in k4123 in k4120 in k4117 in k4114 in k4111 in k4108 in k4105 in k4102 in ... */
static void C_ccall f_4167(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4167,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4170,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:625: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[607];
av2[3]=C_fix(17);
av2[4]=C_fix(2);
av2[5]=lf[608];
av2[6]=lf[609];
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4168 in k4165 in k4162 in k4159 in k4156 in k4153 in k4150 in k4147 in k4144 in k4141 in k4138 in k4135 in k4132 in k4129 in k4126 in k4123 in k4120 in k4117 in k4114 in k4111 in k4108 in k4105 in ... */
static void C_ccall f_4170(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4170,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4173,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:626: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[605];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[606];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4171 in k4168 in k4165 in k4162 in k4159 in k4156 in k4153 in k4150 in k4147 in k4144 in k4141 in k4138 in k4135 in k4132 in k4129 in k4126 in k4123 in k4120 in k4117 in k4114 in k4111 in k4108 in ... */
static void C_ccall f_4173(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4173,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4176,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:627: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[603];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[604];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4174 in k4171 in k4168 in k4165 in k4162 in k4159 in k4156 in k4153 in k4150 in k4147 in k4144 in k4141 in k4138 in k4135 in k4132 in k4129 in k4126 in k4123 in k4120 in k4117 in k4114 in k4111 in ... */
static void C_ccall f_4176(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_4176,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4179,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:629: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[600];
av2[3]=C_fix(4);
av2[4]=lf[297];
av2[5]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4177 in k4174 in k4171 in k4168 in k4165 in k4162 in k4159 in k4156 in k4153 in k4150 in k4147 in k4144 in k4141 in k4138 in k4135 in k4132 in k4129 in k4126 in k4123 in k4120 in k4117 in k4114 in ... */
static void C_ccall f_4179(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_4179,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4182,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:630: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[597];
av2[3]=C_fix(4);
av2[4]=lf[297];
av2[5]=C_fix(1);
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4180 in k4177 in k4174 in k4171 in k4168 in k4165 in k4162 in k4159 in k4156 in k4153 in k4150 in k4147 in k4144 in k4141 in k4138 in k4135 in k4132 in k4129 in k4126 in k4123 in k4120 in k4117 in ... */
static void C_ccall f_4182(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4182,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4185,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:631: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[600];
av2[3]=C_fix(17);
av2[4]=C_fix(2);
av2[5]=lf[601];
av2[6]=lf[602];
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4183 in k4180 in k4177 in k4174 in k4171 in k4168 in k4165 in k4162 in k4159 in k4156 in k4153 in k4150 in k4147 in k4144 in k4141 in k4138 in k4135 in k4132 in k4129 in k4126 in k4123 in k4120 in ... */
static void C_ccall f_4185(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4185,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4188,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:632: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[597];
av2[3]=C_fix(17);
av2[4]=C_fix(2);
av2[5]=lf[598];
av2[6]=lf[599];
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4186 in k4183 in k4180 in k4177 in k4174 in k4171 in k4168 in k4165 in k4162 in k4159 in k4156 in k4153 in k4150 in k4147 in k4144 in k4141 in k4138 in k4135 in k4132 in k4129 in k4126 in k4123 in ... */
static void C_ccall f_4188(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4188,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4191,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:634: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[594];
av2[3]=C_fix(14);
av2[4]=lf[24];
av2[5]=C_fix(1);
av2[6]=lf[595];
av2[7]=lf[596];
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4189 in k4186 in k4183 in k4180 in k4177 in k4174 in k4171 in k4168 in k4165 in k4162 in k4159 in k4156 in k4153 in k4150 in k4147 in k4144 in k4141 in k4138 in k4135 in k4132 in k4129 in k4126 in ... */
static void C_ccall f_4191(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_4191,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4194,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:636: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[590];
av2[3]=C_fix(19);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k4192 in k4189 in k4186 in k4183 in k4180 in k4177 in k4174 in k4171 in k4168 in k4165 in k4162 in k4159 in k4156 in k4153 in k4150 in k4147 in k4144 in k4141 in k4138 in k4135 in k4132 in k4129 in ... */
static void C_ccall f_4194(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_4194,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4197,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:637: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[586];
av2[3]=C_fix(19);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k4195 in k4192 in k4189 in k4186 in k4183 in k4180 in k4177 in k4174 in k4171 in k4168 in k4165 in k4162 in k4159 in k4156 in k4153 in k4150 in k4147 in k4144 in k4141 in k4138 in k4135 in k4132 in ... */
static void C_ccall f_4197(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_4197,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4200,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:638: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[582];
av2[3]=C_fix(19);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k4198 in k4195 in k4192 in k4189 in k4186 in k4183 in k4180 in k4177 in k4174 in k4171 in k4168 in k4165 in k4162 in k4159 in k4156 in k4153 in k4150 in k4147 in k4144 in k4141 in k4138 in k4135 in ... */
static void C_ccall f_4200(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,8)))){
C_save_and_reclaim((void *)f_4200,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4203,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:640: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 9) {
  av2=av;
} else {
  av2=C_alloc(9);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[590];
av2[3]=C_fix(21);
av2[4]=C_fix(-1);
av2[5]=lf[591];
av2[6]=lf[592];
av2[7]=lf[593];
av2[8]=C_fix(5);
((C_proc)(void*)(*((C_word*)t3+1)))(9,av2);}}

/* k4201 in k4198 in k4195 in k4192 in k4189 in k4186 in k4183 in k4180 in k4177 in k4174 in k4171 in k4168 in k4165 in k4162 in k4159 in k4156 in k4153 in k4150 in k4147 in k4144 in k4141 in k4138 in ... */
static void C_ccall f_4203(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,8)))){
C_save_and_reclaim((void *)f_4203,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4206,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:641: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 9) {
  av2=av;
} else {
  av2=C_alloc(9);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[586];
av2[3]=C_fix(21);
av2[4]=C_fix(0);
av2[5]=lf[587];
av2[6]=lf[588];
av2[7]=lf[589];
av2[8]=C_fix(5);
((C_proc)(void*)(*((C_word*)t3+1)))(9,av2);}}

/* k4204 in k4201 in k4198 in k4195 in k4192 in k4189 in k4186 in k4183 in k4180 in k4177 in k4174 in k4171 in k4168 in k4165 in k4162 in k4159 in k4156 in k4153 in k4150 in k4147 in k4144 in k4141 in ... */
static void C_ccall f_4206(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,8)))){
C_save_and_reclaim((void *)f_4206,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4209,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:642: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 9) {
  av2=av;
} else {
  av2=C_alloc(9);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[582];
av2[3]=C_fix(21);
av2[4]=C_fix(0);
av2[5]=lf[583];
av2[6]=lf[584];
av2[7]=lf[585];
av2[8]=C_fix(5);
((C_proc)(void*)(*((C_word*)t3+1)))(9,av2);}}

/* k4207 in k4204 in k4201 in k4198 in k4195 in k4192 in k4189 in k4186 in k4183 in k4180 in k4177 in k4174 in k4171 in k4168 in k4165 in k4162 in k4159 in k4156 in k4153 in k4150 in k4147 in k4144 in ... */
static void C_ccall f_4209(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,8)))){
C_save_and_reclaim((void *)f_4209,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4212,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:644: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 9) {
  av2=av;
} else {
  av2=C_alloc(9);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[579];
av2[3]=C_fix(22);
av2[4]=C_fix(1);
av2[5]=lf[580];
av2[6]=C_SCHEME_TRUE;
av2[7]=C_fix(5);
av2[8]=lf[581];
((C_proc)(void*)(*((C_word*)t3+1)))(9,av2);}}

/* k4210 in k4207 in k4204 in k4201 in k4198 in k4195 in k4192 in k4189 in k4186 in k4183 in k4180 in k4177 in k4174 in k4171 in k4168 in k4165 in k4162 in k4159 in k4156 in k4153 in k4150 in k4147 in ... */
static void C_ccall f_4212(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4212,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4215,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:646: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[577];
av2[3]=C_fix(16);
av2[4]=C_fix(2);
av2[5]=lf[578];
av2[6]=C_SCHEME_FALSE;
av2[7]=*((C_word*)lf[11]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4213 in k4210 in k4207 in k4204 in k4201 in k4198 in k4195 in k4192 in k4189 in k4186 in k4183 in k4180 in k4177 in k4174 in k4171 in k4168 in k4165 in k4162 in k4159 in k4156 in k4153 in k4150 in ... */
static void C_ccall f_4215(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4215,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4218,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:647: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[575];
av2[3]=C_fix(16);
av2[4]=C_fix(2);
av2[5]=lf[576];
av2[6]=C_SCHEME_FALSE;
av2[7]=*((C_word*)lf[11]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4216 in k4213 in k4210 in k4207 in k4204 in k4201 in k4198 in k4195 in k4192 in k4189 in k4186 in k4183 in k4180 in k4177 in k4174 in k4171 in k4168 in k4165 in k4162 in k4159 in k4156 in k4153 in ... */
static void C_ccall f_4218(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4218,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4221,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:648: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[573];
av2[3]=C_fix(16);
av2[4]=C_fix(2);
av2[5]=lf[574];
av2[6]=C_SCHEME_FALSE;
av2[7]=*((C_word*)lf[11]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4219 in k4216 in k4213 in k4210 in k4207 in k4204 in k4201 in k4198 in k4195 in k4192 in k4189 in k4186 in k4183 in k4180 in k4177 in k4174 in k4171 in k4168 in k4165 in k4162 in k4159 in k4156 in ... */
static void C_ccall f_4221(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4221,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4224,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:649: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[571];
av2[3]=C_fix(16);
av2[4]=C_fix(2);
av2[5]=lf[572];
av2[6]=C_SCHEME_FALSE;
av2[7]=*((C_word*)lf[11]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4222 in k4219 in k4216 in k4213 in k4210 in k4207 in k4204 in k4201 in k4198 in k4195 in k4192 in k4189 in k4186 in k4183 in k4180 in k4177 in k4174 in k4171 in k4168 in k4165 in k4162 in k4159 in ... */
static void C_ccall f_4224(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4224,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4227,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:650: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[569];
av2[3]=C_fix(16);
av2[4]=C_fix(2);
av2[5]=lf[570];
av2[6]=C_SCHEME_FALSE;
av2[7]=*((C_word*)lf[11]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4225 in k4222 in k4219 in k4216 in k4213 in k4210 in k4207 in k4204 in k4201 in k4198 in k4195 in k4192 in k4189 in k4186 in k4183 in k4180 in k4177 in k4174 in k4171 in k4168 in k4165 in k4162 in ... */
static void C_ccall f_4227(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4227,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4230,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:651: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[567];
av2[3]=C_fix(16);
av2[4]=C_fix(1);
av2[5]=lf[568];
av2[6]=C_SCHEME_FALSE;
av2[7]=*((C_word*)lf[11]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4228 in k4225 in k4222 in k4219 in k4216 in k4213 in k4210 in k4207 in k4204 in k4201 in k4198 in k4195 in k4192 in k4189 in k4186 in k4183 in k4180 in k4177 in k4174 in k4171 in k4168 in k4165 in ... */
static void C_ccall f_4230(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4230,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4233,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:652: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[565];
av2[3]=C_fix(16);
av2[4]=C_fix(2);
av2[5]=lf[566];
av2[6]=C_SCHEME_FALSE;
av2[7]=*((C_word*)lf[11]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4231 in k4228 in k4225 in k4222 in k4219 in k4216 in k4213 in k4210 in k4207 in k4204 in k4201 in k4198 in k4195 in k4192 in k4189 in k4186 in k4183 in k4180 in k4177 in k4174 in k4171 in k4168 in ... */
static void C_ccall f_4233(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4233,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4236,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:654: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[561];
av2[3]=C_fix(5);
av2[4]=lf[564];
av2[5]=C_fix(0);
av2[6]=lf[24];
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4234 in k4231 in k4228 in k4225 in k4222 in k4219 in k4216 in k4213 in k4210 in k4207 in k4204 in k4201 in k4198 in k4195 in k4192 in k4189 in k4186 in k4183 in k4180 in k4177 in k4174 in k4171 in ... */
static void C_ccall f_4236(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4236,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4239,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:655: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[561];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[563];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4237 in k4234 in k4231 in k4228 in k4225 in k4222 in k4219 in k4216 in k4213 in k4210 in k4207 in k4204 in k4201 in k4198 in k4195 in k4192 in k4189 in k4186 in k4183 in k4180 in k4177 in k4174 in ... */
static void C_ccall f_4239(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4239,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4242,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:656: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[561];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[562];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4240 in k4237 in k4234 in k4231 in k4228 in k4225 in k4222 in k4219 in k4216 in k4213 in k4210 in k4207 in k4204 in k4201 in k4198 in k4195 in k4192 in k4189 in k4186 in k4183 in k4180 in k4177 in ... */
static void C_ccall f_4242(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4242,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4245,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:657: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[557];
av2[3]=C_fix(5);
av2[4]=lf[560];
av2[5]=C_fix(0);
av2[6]=lf[24];
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4243 in k4240 in k4237 in k4234 in k4231 in k4228 in k4225 in k4222 in k4219 in k4216 in k4213 in k4210 in k4207 in k4204 in k4201 in k4198 in k4195 in k4192 in k4189 in k4186 in k4183 in k4180 in ... */
static void C_ccall f_4245(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4245,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4248,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:658: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[557];
av2[3]=C_fix(5);
av2[4]=lf[559];
av2[5]=C_fix(0);
av2[6]=lf[388];
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4246 in k4243 in k4240 in k4237 in k4234 in k4231 in k4228 in k4225 in k4222 in k4219 in k4216 in k4213 in k4210 in k4207 in k4204 in k4201 in k4198 in k4195 in k4192 in k4189 in k4186 in k4183 in ... */
static void C_ccall f_4248(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4248,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4251,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:659: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[557];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[558];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4249 in k4246 in k4243 in k4240 in k4237 in k4234 in k4231 in k4228 in k4225 in k4222 in k4219 in k4216 in k4213 in k4210 in k4207 in k4204 in k4201 in k4198 in k4195 in k4192 in k4189 in k4186 in ... */
static void C_ccall f_4251(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4251,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4254,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:660: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[553];
av2[3]=C_fix(5);
av2[4]=lf[556];
av2[5]=C_fix(0);
av2[6]=lf[24];
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4252 in k4249 in k4246 in k4243 in k4240 in k4237 in k4234 in k4231 in k4228 in k4225 in k4222 in k4219 in k4216 in k4213 in k4210 in k4207 in k4204 in k4201 in k4198 in k4195 in k4192 in k4189 in ... */
static void C_ccall f_4254(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4254,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4257,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:661: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[553];
av2[3]=C_fix(5);
av2[4]=lf[555];
av2[5]=C_fix(0);
av2[6]=lf[388];
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4255 in k4252 in k4249 in k4246 in k4243 in k4240 in k4237 in k4234 in k4231 in k4228 in k4225 in k4222 in k4219 in k4216 in k4213 in k4210 in k4207 in k4204 in k4201 in k4198 in k4195 in k4192 in ... */
static void C_ccall f_4257(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4257,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4260,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:662: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[553];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[554];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4258 in k4255 in k4252 in k4249 in k4246 in k4243 in k4240 in k4237 in k4234 in k4231 in k4228 in k4225 in k4222 in k4219 in k4216 in k4213 in k4210 in k4207 in k4204 in k4201 in k4198 in k4195 in ... */
static void C_ccall f_4260(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4260,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4263,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:664: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[541];
av2[3]=C_fix(6);
av2[4]=lf[551];
av2[5]=lf[552];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4261 in k4258 in k4255 in k4252 in k4249 in k4246 in k4243 in k4240 in k4237 in k4234 in k4231 in k4228 in k4225 in k4222 in k4219 in k4216 in k4213 in k4210 in k4207 in k4204 in k4201 in k4198 in ... */
static void C_ccall f_4263(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4263,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4266,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:665: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[537];
av2[3]=C_fix(6);
av2[4]=lf[549];
av2[5]=lf[550];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4264 in k4261 in k4258 in k4255 in k4252 in k4249 in k4246 in k4243 in k4240 in k4237 in k4234 in k4231 in k4228 in k4225 in k4222 in k4219 in k4216 in k4213 in k4210 in k4207 in k4204 in k4201 in ... */
static void C_ccall f_4266(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4266,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4269,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:666: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[546];
av2[3]=C_fix(6);
av2[4]=lf[547];
av2[5]=lf[548];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4267 in k4264 in k4261 in k4258 in k4255 in k4252 in k4249 in k4246 in k4243 in k4240 in k4237 in k4234 in k4231 in k4228 in k4225 in k4222 in k4219 in k4216 in k4213 in k4210 in k4207 in k4204 in ... */
static void C_ccall f_4269(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4269,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4272,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:667: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[543];
av2[3]=C_fix(6);
av2[4]=lf[544];
av2[5]=lf[545];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4270 in k4267 in k4264 in k4261 in k4258 in k4255 in k4252 in k4249 in k4246 in k4243 in k4240 in k4237 in k4234 in k4231 in k4228 in k4225 in k4222 in k4219 in k4216 in k4213 in k4210 in k4207 in ... */
static void C_ccall f_4272(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4272,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4275,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:669: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[541];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[542];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4273 in k4270 in k4267 in k4264 in k4261 in k4258 in k4255 in k4252 in k4249 in k4246 in k4243 in k4240 in k4237 in k4234 in k4231 in k4228 in k4225 in k4222 in k4219 in k4216 in k4213 in k4210 in ... */
static void C_ccall f_4275(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4275,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4278,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:670: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[539];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[540];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4276 in k4273 in k4270 in k4267 in k4264 in k4261 in k4258 in k4255 in k4252 in k4249 in k4246 in k4243 in k4240 in k4237 in k4234 in k4231 in k4228 in k4225 in k4222 in k4219 in k4216 in k4213 in ... */
static void C_ccall f_4278(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4278,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4281,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:671: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[537];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[538];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4279 in k4276 in k4273 in k4270 in k4267 in k4264 in k4261 in k4258 in k4255 in k4252 in k4249 in k4246 in k4243 in k4240 in k4237 in k4234 in k4231 in k4228 in k4225 in k4222 in k4219 in k4216 in ... */
static void C_ccall f_4281(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4281,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4284,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:673: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[523];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[536];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4282 in k4279 in k4276 in k4273 in k4270 in k4267 in k4264 in k4261 in k4258 in k4255 in k4252 in k4249 in k4246 in k4243 in k4240 in k4237 in k4234 in k4231 in k4228 in k4225 in k4222 in k4219 in ... */
static void C_ccall f_4284(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4284,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4287,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:674: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[521];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[535];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4285 in k4282 in k4279 in k4276 in k4273 in k4270 in k4267 in k4264 in k4261 in k4258 in k4255 in k4252 in k4249 in k4246 in k4243 in k4240 in k4237 in k4234 in k4231 in k4228 in k4225 in k4222 in ... */
static void C_ccall f_4287(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4287,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4290,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:675: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[519];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[534];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4288 in k4285 in k4282 in k4279 in k4276 in k4273 in k4270 in k4267 in k4264 in k4261 in k4258 in k4255 in k4252 in k4249 in k4246 in k4243 in k4240 in k4237 in k4234 in k4231 in k4228 in k4225 in ... */
static void C_ccall f_4290(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4290,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4293,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:676: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[517];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[533];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4291 in k4288 in k4285 in k4282 in k4279 in k4276 in k4273 in k4270 in k4267 in k4264 in k4261 in k4258 in k4255 in k4252 in k4249 in k4246 in k4243 in k4240 in k4237 in k4234 in k4231 in k4228 in ... */
static void C_ccall f_4293(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4293,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4296,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:677: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[515];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[532];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4294 in k4291 in k4288 in k4285 in k4282 in k4279 in k4276 in k4273 in k4270 in k4267 in k4264 in k4261 in k4258 in k4255 in k4252 in k4249 in k4246 in k4243 in k4240 in k4237 in k4234 in k4231 in ... */
static void C_ccall f_4296(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4296,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4299,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:678: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[513];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[531];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4297 in k4294 in k4291 in k4288 in k4285 in k4282 in k4279 in k4276 in k4273 in k4270 in k4267 in k4264 in k4261 in k4258 in k4255 in k4252 in k4249 in k4246 in k4243 in k4240 in k4237 in k4234 in ... */
static void C_ccall f_4299(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4299,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4302,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:679: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[511];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[530];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4300 in k4297 in k4294 in k4291 in k4288 in k4285 in k4282 in k4279 in k4276 in k4273 in k4270 in k4267 in k4264 in k4261 in k4258 in k4255 in k4252 in k4249 in k4246 in k4243 in k4240 in k4237 in ... */
static void C_ccall f_4302(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4302,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4305,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:680: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[509];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[529];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4303 in k4300 in k4297 in k4294 in k4291 in k4288 in k4285 in k4282 in k4279 in k4276 in k4273 in k4270 in k4267 in k4264 in k4261 in k4258 in k4255 in k4252 in k4249 in k4246 in k4243 in k4240 in ... */
static void C_ccall f_4305(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4305,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4308,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:681: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[507];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[528];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4306 in k4303 in k4300 in k4297 in k4294 in k4291 in k4288 in k4285 in k4282 in k4279 in k4276 in k4273 in k4270 in k4267 in k4264 in k4261 in k4258 in k4255 in k4252 in k4249 in k4246 in k4243 in ... */
static void C_ccall f_4308(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4308,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4311,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:682: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[505];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[527];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4309 in k4306 in k4303 in k4300 in k4297 in k4294 in k4291 in k4288 in k4285 in k4282 in k4279 in k4276 in k4273 in k4270 in k4267 in k4264 in k4261 in k4258 in k4255 in k4252 in k4249 in k4246 in ... */
static void C_ccall f_4311(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4311,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4314,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:683: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[503];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[526];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4312 in k4309 in k4306 in k4303 in k4300 in k4297 in k4294 in k4291 in k4288 in k4285 in k4282 in k4279 in k4276 in k4273 in k4270 in k4267 in k4264 in k4261 in k4258 in k4255 in k4252 in k4249 in ... */
static void C_ccall f_4314(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4314,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4317,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:684: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[501];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[525];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4315 in k4312 in k4309 in k4306 in k4303 in k4300 in k4297 in k4294 in k4291 in k4288 in k4285 in k4282 in k4279 in k4276 in k4273 in k4270 in k4267 in k4264 in k4261 in k4258 in k4255 in k4252 in ... */
static void C_ccall f_4317(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4317,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4320,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:685: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[523];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[524];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4318 in k4315 in k4312 in k4309 in k4306 in k4303 in k4300 in k4297 in k4294 in k4291 in k4288 in k4285 in k4282 in k4279 in k4276 in k4273 in k4270 in k4267 in k4264 in k4261 in k4258 in k4255 in ... */
static void C_ccall f_4320(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4320,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4323,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:686: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[521];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[522];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4321 in k4318 in k4315 in k4312 in k4309 in k4306 in k4303 in k4300 in k4297 in k4294 in k4291 in k4288 in k4285 in k4282 in k4279 in k4276 in k4273 in k4270 in k4267 in k4264 in k4261 in k4258 in ... */
static void C_ccall f_4323(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4323,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4326,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:687: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[519];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[520];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4324 in k4321 in k4318 in k4315 in k4312 in k4309 in k4306 in k4303 in k4300 in k4297 in k4294 in k4291 in k4288 in k4285 in k4282 in k4279 in k4276 in k4273 in k4270 in k4267 in k4264 in k4261 in ... */
static void C_ccall f_4326(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4326,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4329,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:688: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[517];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[518];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4327 in k4324 in k4321 in k4318 in k4315 in k4312 in k4309 in k4306 in k4303 in k4300 in k4297 in k4294 in k4291 in k4288 in k4285 in k4282 in k4279 in k4276 in k4273 in k4270 in k4267 in k4264 in ... */
static void C_ccall f_4329(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4329,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4332,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:689: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[515];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[516];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4330 in k4327 in k4324 in k4321 in k4318 in k4315 in k4312 in k4309 in k4306 in k4303 in k4300 in k4297 in k4294 in k4291 in k4288 in k4285 in k4282 in k4279 in k4276 in k4273 in k4270 in k4267 in ... */
static void C_ccall f_4332(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4332,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4335,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:690: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[513];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[514];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4333 in k4330 in k4327 in k4324 in k4321 in k4318 in k4315 in k4312 in k4309 in k4306 in k4303 in k4300 in k4297 in k4294 in k4291 in k4288 in k4285 in k4282 in k4279 in k4276 in k4273 in k4270 in ... */
static void C_ccall f_4335(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4335,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4338,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:691: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[511];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[512];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4336 in k4333 in k4330 in k4327 in k4324 in k4321 in k4318 in k4315 in k4312 in k4309 in k4306 in k4303 in k4300 in k4297 in k4294 in k4291 in k4288 in k4285 in k4282 in k4279 in k4276 in k4273 in ... */
static void C_ccall f_4338(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4338,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4341,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:692: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[509];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[510];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4339 in k4336 in k4333 in k4330 in k4327 in k4324 in k4321 in k4318 in k4315 in k4312 in k4309 in k4306 in k4303 in k4300 in k4297 in k4294 in k4291 in k4288 in k4285 in k4282 in k4279 in k4276 in ... */
static void C_ccall f_4341(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4341,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4344,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:693: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[507];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[508];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4342 in k4339 in k4336 in k4333 in k4330 in k4327 in k4324 in k4321 in k4318 in k4315 in k4312 in k4309 in k4306 in k4303 in k4300 in k4297 in k4294 in k4291 in k4288 in k4285 in k4282 in k4279 in ... */
static void C_ccall f_4344(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4344,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4347,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:694: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[505];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[506];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4345 in k4342 in k4339 in k4336 in k4333 in k4330 in k4327 in k4324 in k4321 in k4318 in k4315 in k4312 in k4309 in k4306 in k4303 in k4300 in k4297 in k4294 in k4291 in k4288 in k4285 in k4282 in ... */
static void C_ccall f_4347(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4347,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4350,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:695: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[503];
av2[3]=C_fix(2);
av2[4]=C_fix(3);
av2[5]=lf[504];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4348 in k4345 in k4342 in k4339 in k4336 in k4333 in k4330 in k4327 in k4324 in k4321 in k4318 in k4315 in k4312 in k4309 in k4306 in k4303 in k4300 in k4297 in k4294 in k4291 in k4288 in k4285 in ... */
static void C_ccall f_4350(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4350,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4353,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:696: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[501];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[502];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4351 in k4348 in k4345 in k4342 in k4339 in k4336 in k4333 in k4330 in k4327 in k4324 in k4321 in k4318 in k4315 in k4312 in k4309 in k4306 in k4303 in k4300 in k4297 in k4294 in k4291 in k4288 in ... */
static void C_ccall f_4353(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4353,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4356,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:698: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[439];
av2[3]=C_fix(9);
av2[4]=lf[499];
av2[5]=lf[500];
av2[6]=C_SCHEME_TRUE;
av2[7]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4354 in k4351 in k4348 in k4345 in k4342 in k4339 in k4336 in k4333 in k4330 in k4327 in k4324 in k4321 in k4318 in k4315 in k4312 in k4309 in k4306 in k4303 in k4300 in k4297 in k4294 in k4291 in ... */
static void C_ccall f_4356(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4356,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4359,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:699: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[437];
av2[3]=C_fix(9);
av2[4]=lf[497];
av2[5]=lf[498];
av2[6]=C_SCHEME_TRUE;
av2[7]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4357 in k4354 in k4351 in k4348 in k4345 in k4342 in k4339 in k4336 in k4333 in k4330 in k4327 in k4324 in k4321 in k4318 in k4315 in k4312 in k4309 in k4306 in k4303 in k4300 in k4297 in k4294 in ... */
static void C_ccall f_4359(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4359,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4362,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:700: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[435];
av2[3]=C_fix(9);
av2[4]=lf[495];
av2[5]=lf[496];
av2[6]=C_SCHEME_TRUE;
av2[7]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4360 in k4357 in k4354 in k4351 in k4348 in k4345 in k4342 in k4339 in k4336 in k4333 in k4330 in k4327 in k4324 in k4321 in k4318 in k4315 in k4312 in k4309 in k4306 in k4303 in k4300 in k4297 in ... */
static void C_ccall f_4362(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4362,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4365,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:701: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[433];
av2[3]=C_fix(9);
av2[4]=lf[493];
av2[5]=lf[494];
av2[6]=C_SCHEME_TRUE;
av2[7]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4363 in k4360 in k4357 in k4354 in k4351 in k4348 in k4345 in k4342 in k4339 in k4336 in k4333 in k4330 in k4327 in k4324 in k4321 in k4318 in k4315 in k4312 in k4309 in k4306 in k4303 in k4300 in ... */
static void C_ccall f_4365(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4365,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4368,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:702: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[431];
av2[3]=C_fix(9);
av2[4]=lf[491];
av2[5]=lf[492];
av2[6]=C_SCHEME_TRUE;
av2[7]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4366 in k4363 in k4360 in k4357 in k4354 in k4351 in k4348 in k4345 in k4342 in k4339 in k4336 in k4333 in k4330 in k4327 in k4324 in k4321 in k4318 in k4315 in k4312 in k4309 in k4306 in k4303 in ... */
static void C_ccall f_4368(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4368,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4371,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:704: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[490];
av2[3]=C_fix(11);
av2[4]=C_fix(1);
av2[5]=lf[99];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4369 in k4366 in k4363 in k4360 in k4357 in k4354 in k4351 in k4348 in k4345 in k4342 in k4339 in k4336 in k4333 in k4330 in k4327 in k4324 in k4321 in k4318 in k4315 in k4312 in k4309 in k4306 in ... */
static void C_ccall f_4371(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4371,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4374,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:705: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[488];
av2[3]=C_fix(11);
av2[4]=C_fix(2);
av2[5]=lf[489];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4372 in k4369 in k4366 in k4363 in k4360 in k4357 in k4354 in k4351 in k4348 in k4345 in k4342 in k4339 in k4336 in k4333 in k4330 in k4327 in k4324 in k4321 in k4318 in k4315 in k4312 in k4309 in ... */
static void C_ccall f_4374(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4374,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4377,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:706: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[486];
av2[3]=C_fix(11);
av2[4]=C_fix(2);
av2[5]=lf[487];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4375 in k4372 in k4369 in k4366 in k4363 in k4360 in k4357 in k4354 in k4351 in k4348 in k4345 in k4342 in k4339 in k4336 in k4333 in k4330 in k4327 in k4324 in k4321 in k4318 in k4315 in k4312 in ... */
static void C_ccall f_4377(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4377,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4380,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:707: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[485];
av2[3]=C_fix(11);
av2[4]=C_fix(3);
av2[5]=lf[297];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4378 in k4375 in k4372 in k4369 in k4366 in k4363 in k4360 in k4357 in k4354 in k4351 in k4348 in k4345 in k4342 in k4339 in k4336 in k4333 in k4330 in k4327 in k4324 in k4321 in k4318 in k4315 in ... */
static void C_ccall f_4380(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4380,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4383,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:708: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[484];
av2[3]=C_fix(11);
av2[4]=C_fix(3);
av2[5]=lf[297];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4381 in k4378 in k4375 in k4372 in k4369 in k4366 in k4363 in k4360 in k4357 in k4354 in k4351 in k4348 in k4345 in k4342 in k4339 in k4336 in k4333 in k4330 in k4327 in k4324 in k4321 in k4318 in ... */
static void C_ccall f_4383(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4383,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4386,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:709: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[483];
av2[3]=C_fix(11);
av2[4]=C_SCHEME_FALSE;
av2[5]=lf[340];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4384 in k4381 in k4378 in k4375 in k4372 in k4369 in k4366 in k4363 in k4360 in k4357 in k4354 in k4351 in k4348 in k4345 in k4342 in k4339 in k4336 in k4333 in k4330 in k4327 in k4324 in k4321 in ... */
static void C_ccall f_4386(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4386,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4389,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:710: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[481];
av2[3]=C_fix(11);
av2[4]=C_fix(3);
av2[5]=lf[482];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4387 in k4384 in k4381 in k4378 in k4375 in k4372 in k4369 in k4366 in k4363 in k4360 in k4357 in k4354 in k4351 in k4348 in k4345 in k4342 in k4339 in k4336 in k4333 in k4330 in k4327 in k4324 in ... */
static void C_ccall f_4389(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4389,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4392,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:711: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[479];
av2[3]=C_fix(11);
av2[4]=C_fix(2);
av2[5]=lf[480];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in k4372 in k4369 in k4366 in k4363 in k4360 in k4357 in k4354 in k4351 in k4348 in k4345 in k4342 in k4339 in k4336 in k4333 in k4330 in k4327 in ... */
static void C_ccall f_4392(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4392,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4395,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:712: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[477];
av2[3]=C_fix(11);
av2[4]=C_fix(1);
av2[5]=lf[478];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in k4372 in k4369 in k4366 in k4363 in k4360 in k4357 in k4354 in k4351 in k4348 in k4345 in k4342 in k4339 in k4336 in k4333 in k4330 in ... */
static void C_ccall f_4395(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4395,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4398,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:713: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[475];
av2[3]=C_fix(11);
av2[4]=C_fix(1);
av2[5]=lf[476];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in k4372 in k4369 in k4366 in k4363 in k4360 in k4357 in k4354 in k4351 in k4348 in k4345 in k4342 in k4339 in k4336 in k4333 in ... */
static void C_ccall f_4398(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4398,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4401,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:715: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[473];
av2[3]=C_fix(11);
av2[4]=C_fix(3);
av2[5]=lf[297];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in k4372 in k4369 in k4366 in k4363 in k4360 in k4357 in k4354 in k4351 in k4348 in k4345 in k4342 in k4339 in k4336 in ... */
static void C_ccall f_4401(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4401,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4404,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:716: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[473];
av2[3]=C_fix(2);
av2[4]=C_fix(3);
av2[5]=lf[474];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in k4372 in k4369 in k4366 in k4363 in k4360 in k4357 in k4354 in k4351 in k4348 in k4345 in k4342 in k4339 in ... */
static void C_ccall f_4404(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4404,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4407,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:718: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[469];
av2[3]=C_fix(12);
av2[4]=lf[472];
av2[5]=C_SCHEME_TRUE;
av2[6]=C_fix(2);
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in k4372 in k4369 in k4366 in k4363 in k4360 in k4357 in k4354 in k4351 in k4348 in k4345 in k4342 in ... */
static void C_ccall f_4407(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4407,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4410,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:719: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[468];
av2[3]=C_fix(12);
av2[4]=lf[471];
av2[5]=C_SCHEME_TRUE;
av2[6]=C_fix(2);
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in k4372 in k4369 in k4366 in k4363 in k4360 in k4357 in k4354 in k4351 in k4348 in k4345 in ... */
static void C_ccall f_4410(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4410,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4413,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:720: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[470];
av2[3]=C_fix(12);
av2[4]=C_SCHEME_FALSE;
av2[5]=C_SCHEME_TRUE;
av2[6]=C_fix(1);
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in k4372 in k4369 in k4366 in k4363 in k4360 in k4357 in k4354 in k4351 in k4348 in ... */
static void C_ccall f_4413(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_4413,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4416,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:722: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[469];
av2[3]=C_fix(19);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in k4372 in k4369 in k4366 in k4363 in k4360 in k4357 in k4354 in k4351 in ... */
static void C_ccall f_4416(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_4416,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4419,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:723: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[468];
av2[3]=C_fix(19);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in k4372 in k4369 in k4366 in k4363 in k4360 in k4357 in k4354 in ... */
static void C_ccall f_4419(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_4419,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4422,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:725: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[469];
av2[3]=C_fix(18);
av2[4]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in k4372 in k4369 in k4366 in k4363 in k4360 in k4357 in ... */
static void C_ccall f_4422(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_4422,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4425,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:726: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[468];
av2[3]=C_fix(18);
av2[4]=C_fix(1);
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in k4372 in k4369 in k4366 in k4363 in k4360 in ... */
static void C_ccall f_4425(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_4425,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4428,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:727: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[349];
av2[3]=C_fix(18);
av2[4]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in k4372 in k4369 in k4366 in k4363 in ... */
static void C_ccall f_4428(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,6)))){
C_save_and_reclaim((void *)f_4428,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4431,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_6079,tmp=(C_word)a,a+=2,tmp);
/* c-platform.scm:729: chicken.compiler.optimizer#rewrite */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=lf[429];
av2[3]=C_fix(8);
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in k4372 in k4369 in k4366 in ... */
static void C_ccall f_4431(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,6)))){
C_save_and_reclaim((void *)f_4431,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4434,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_5955,tmp=(C_word)a,a+=2,tmp);
/* c-platform.scm:759: chicken.compiler.optimizer#rewrite */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=lf[427];
av2[3]=C_fix(8);
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in k4372 in k4369 in ... */
static void C_ccall f_4434(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,6)))){
C_save_and_reclaim((void *)f_4434,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4437,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_5831,tmp=(C_word)a,a+=2,tmp);
/* c-platform.scm:795: chicken.compiler.optimizer#rewrite */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=lf[424];
av2[3]=C_fix(8);
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k4435 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in k4372 in ... */
static void C_ccall f_4437(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,6)))){
C_save_and_reclaim((void *)f_4437,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_4439,tmp=(C_word)a,a+=2,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4572,a[2]=((C_word*)t0)[2],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:857: chicken.compiler.optimizer#rewrite */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[454];
av2[3]=C_fix(8);
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* rewrite-div in k4435 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in ... */
static void C_ccall f_4439(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_4439,c,av);}
a=C_alloc(7);
t6=C_eqp(*((C_word*)lf[25]+1),lf[24]);
if(C_truep(t6)){
t7=C_i_length(t5);
if(C_truep(C_fixnum_greater_or_equal_p(t7,C_fix(2)))){
t8=C_i_car(t5);
t9=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4534,a[2]=t8,a[3]=t4,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t10=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_4536,tmp=(C_word)a,a+=2,tmp);
/* c-platform.scm:841: filter */
f_2293(t9,t10,C_u_i_cdr(t5));}
else{
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}}
else{
t7=t1;{
C_word *av2=av;
av2[0]=t7;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}}

/* k4474 in k4532 in rewrite-div in k4435 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in ... */
static void C_ccall f_4476(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_4476,c,av);}
a=C_alloc(6);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
/* c-platform.scm:847: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[23];
av2[3]=((C_word*)t0)[4];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* a4477 in k4532 in rewrite-div in k4435 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in ... */
static void C_ccall f_4478(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_4478,c,av);}
a=C_alloc(9);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4485,a[2]=t2,a[3]=t1,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4521,a[2]=t4,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:853: chicken.compiler.support#node-class */
t6=*((C_word*)lf[34]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k4483 in a4477 in k4532 in rewrite-div in k4435 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in ... */
static void C_fcall f_4485(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,4)))){
C_save_and_reclaim_args((void *)trf_4485,2,t0,t1);}
a=C_alloc(6);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4496,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:854: chicken.compiler.support#qnode */
t3=*((C_word*)lf[29]+1);{
C_word av2[3];
av2[0]=t3;
av2[1]=t2;
av2[2]=C_fix(1);
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],((C_word*)t0)[4]);
/* c-platform.scm:855: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word av2[5];
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[27];
av2[3]=lf[59];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}}

/* k4494 in k4483 in a4477 in k4532 in rewrite-div in k4435 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in ... */
static void C_ccall f_4496(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_4496,c,av);}
a=C_alloc(6);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
/* c-platform.scm:854: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[27];
av2[3]=lf[58];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4515 in k4519 in a4477 in k4532 in rewrite-div in k4435 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in ... */
static void C_ccall f_4517(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4517,c,av);}
t2=((C_word*)t0)[2];
f_4485(t2,C_eqp(C_fix(2),C_i_car(t1)));}

/* k4519 in a4477 in k4532 in rewrite-div in k4435 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in ... */
static void C_ccall f_4521(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4521,c,av);}
a=C_alloc(3);
t2=C_eqp(lf[31],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4517,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:853: chicken.compiler.support#node-parameters */
t4=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=((C_word*)t0)[2];
f_4485(t3,C_SCHEME_FALSE);}}

/* k4532 in rewrite-div in k4435 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in ... */
static void C_ccall f_4534(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,4)))){
C_save_and_reclaim((void *)f_4534,c,av);}
a=C_alloc(13);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_i_length(t2);
if(C_truep(C_fixnum_greater_or_equal_p(t3,C_fix(2)))){
t4=C_a_i_list1(&a,1,C_SCHEME_TRUE);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4476,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t4,tmp=(C_word)a,a+=5,tmp);
t6=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_4478,tmp=(C_word)a,a+=2,tmp);
/* c-platform.scm:851: chicken.compiler.support#fold-inner */
t7=*((C_word*)lf[60]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t7;
av2[1]=t5;
av2[2]=t6;
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}
else{
t4=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* a4535 in rewrite-div in k4435 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in ... */
static void C_ccall f_4536(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_4536,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4562,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:843: chicken.compiler.support#node-class */
t4=*((C_word*)lf[34]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k4556 in k4560 in a4535 in rewrite-div in k4435 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in ... */
static void C_ccall f_4558(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4558,c,av);}
t2=C_i_car(t1);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_not(C_eqp(C_fix(1),t2));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k4560 in a4535 in rewrite-div in k4435 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in ... */
static void C_ccall f_4562(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4562,c,av);}
a=C_alloc(3);
t2=C_eqp(lf[31],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4558,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:844: chicken.compiler.support#node-parameters */
t4=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k4570 in k4435 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in ... */
static void C_ccall f_4572(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_4572,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4575,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:858: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[457];
av2[3]=C_fix(8);
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4573 in k4570 in k4435 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in ... */
static void C_ccall f_4575(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,6)))){
C_save_and_reclaim((void *)f_4575,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4578,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_5754,tmp=(C_word)a,a+=2,tmp);
/* c-platform.scm:860: chicken.compiler.optimizer#rewrite */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=lf[449];
av2[3]=C_fix(8);
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k4576 in k4573 in k4570 in k4435 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in ... */
static void C_ccall f_4578(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_4578,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4581,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:878: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[427];
av2[3]=C_fix(19);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k4579 in k4576 in k4573 in k4570 in k4435 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in ... */
static void C_ccall f_4581(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_4581,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4584,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:879: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[424];
av2[3]=C_fix(19);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k4582 in k4579 in k4576 in k4573 in k4570 in k4435 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in ... */
static void C_ccall f_4584(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_4584,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4587,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:880: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[429];
av2[3]=C_fix(19);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k4585 in k4582 in k4579 in k4576 in k4573 in k4570 in k4435 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in ... */
static void C_ccall f_4587(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_4587,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4590,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:881: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[454];
av2[3]=C_fix(19);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k4588 in k4585 in k4582 in k4579 in k4576 in k4573 in k4570 in k4435 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in ... */
static void C_ccall f_4590(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4590,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4593,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:883: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[427];
av2[3]=C_fix(16);
av2[4]=C_fix(2);
av2[5]=lf[453];
av2[6]=C_SCHEME_TRUE;
av2[7]=C_fix(29);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4591 in k4588 in k4585 in k4582 in k4579 in k4576 in k4573 in k4570 in k4435 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in ... */
static void C_ccall f_4593(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4593,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4596,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:884: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[424];
av2[3]=C_fix(16);
av2[4]=C_fix(2);
av2[5]=lf[452];
av2[6]=C_SCHEME_TRUE;
av2[7]=C_fix(29);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4594 in k4591 in k4588 in k4585 in k4582 in k4579 in k4576 in k4573 in k4570 in k4435 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in ... */
static void C_ccall f_4596(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4596,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4599,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:885: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[429];
av2[3]=C_fix(16);
av2[4]=C_fix(2);
av2[5]=lf[451];
av2[6]=C_SCHEME_TRUE;
av2[7]=C_fix(33);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4597 in k4594 in k4591 in k4588 in k4585 in k4582 in k4579 in k4576 in k4573 in k4570 in k4435 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in ... */
static void C_ccall f_4599(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4599,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4602,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:886: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[449];
av2[3]=C_fix(16);
av2[4]=C_fix(2);
av2[5]=lf[450];
av2[6]=C_SCHEME_TRUE;
av2[7]=C_fix(5);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4600 in k4597 in k4594 in k4591 in k4588 in k4585 in k4582 in k4579 in k4576 in k4573 in k4570 in k4435 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in ... */
static void C_ccall f_4602(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4602,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4605,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:887: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[400];
av2[3]=C_fix(16);
av2[4]=C_fix(2);
av2[5]=lf[448];
av2[6]=C_SCHEME_TRUE;
av2[7]=C_fix(5);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4603 in k4600 in k4597 in k4594 in k4591 in k4588 in k4585 in k4582 in k4579 in k4576 in k4573 in k4570 in k4435 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in ... */
static void C_ccall f_4605(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4605,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4608,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:888: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[446];
av2[3]=C_fix(16);
av2[4]=C_fix(2);
av2[5]=lf[447];
av2[6]=C_SCHEME_TRUE;
av2[7]=C_fix(5);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4606 in k4603 in k4600 in k4597 in k4594 in k4591 in k4588 in k4585 in k4582 in k4579 in k4576 in k4573 in k4570 in k4435 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in ... */
static void C_ccall f_4608(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_4608,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4611,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:890: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[439];
av2[3]=C_fix(17);
av2[4]=C_fix(2);
av2[5]=lf[445];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4609 in k4606 in k4603 in k4600 in k4597 in k4594 in k4591 in k4588 in k4585 in k4582 in k4579 in k4576 in k4573 in k4570 in k4435 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in ... */
static void C_ccall f_4611(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_4611,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4614,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:891: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[437];
av2[3]=C_fix(17);
av2[4]=C_fix(2);
av2[5]=lf[444];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4612 in k4609 in k4606 in k4603 in k4600 in k4597 in k4594 in k4591 in k4588 in k4585 in k4582 in k4579 in k4576 in k4573 in k4570 in k4435 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in ... */
static void C_ccall f_4614(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_4614,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4617,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:892: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[435];
av2[3]=C_fix(17);
av2[4]=C_fix(2);
av2[5]=lf[443];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4615 in k4612 in k4609 in k4606 in k4603 in k4600 in k4597 in k4594 in k4591 in k4588 in k4585 in k4582 in k4579 in k4576 in k4573 in k4570 in k4435 in k4432 in k4429 in k4426 in k4423 in k4420 in ... */
static void C_ccall f_4617(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_4617,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4620,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:893: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[433];
av2[3]=C_fix(17);
av2[4]=C_fix(2);
av2[5]=lf[442];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4618 in k4615 in k4612 in k4609 in k4606 in k4603 in k4600 in k4597 in k4594 in k4591 in k4588 in k4585 in k4582 in k4579 in k4576 in k4573 in k4570 in k4435 in k4432 in k4429 in k4426 in k4423 in ... */
static void C_ccall f_4620(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_4620,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4623,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:894: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[431];
av2[3]=C_fix(17);
av2[4]=C_fix(2);
av2[5]=lf[441];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4621 in k4618 in k4615 in k4612 in k4609 in k4606 in k4603 in k4600 in k4597 in k4594 in k4591 in k4588 in k4585 in k4582 in k4579 in k4576 in k4573 in k4570 in k4435 in k4432 in k4429 in k4426 in ... */
static void C_ccall f_4623(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4623,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4626,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:896: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[439];
av2[3]=C_fix(13);
av2[4]=C_SCHEME_FALSE;
av2[5]=lf[440];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4624 in k4621 in k4618 in k4615 in k4612 in k4609 in k4606 in k4603 in k4600 in k4597 in k4594 in k4591 in k4588 in k4585 in k4582 in k4579 in k4576 in k4573 in k4570 in k4435 in k4432 in k4429 in ... */
static void C_ccall f_4626(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4626,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4629,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:897: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[437];
av2[3]=C_fix(13);
av2[4]=C_SCHEME_FALSE;
av2[5]=lf[438];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4627 in k4624 in k4621 in k4618 in k4615 in k4612 in k4609 in k4606 in k4603 in k4600 in k4597 in k4594 in k4591 in k4588 in k4585 in k4582 in k4579 in k4576 in k4573 in k4570 in k4435 in k4432 in ... */
static void C_ccall f_4629(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4629,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4632,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:898: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[435];
av2[3]=C_fix(13);
av2[4]=C_SCHEME_FALSE;
av2[5]=lf[436];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4630 in k4627 in k4624 in k4621 in k4618 in k4615 in k4612 in k4609 in k4606 in k4603 in k4600 in k4597 in k4594 in k4591 in k4588 in k4585 in k4582 in k4579 in k4576 in k4573 in k4570 in k4435 in ... */
static void C_ccall f_4632(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4632,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4635,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:899: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[433];
av2[3]=C_fix(13);
av2[4]=C_SCHEME_FALSE;
av2[5]=lf[434];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4633 in k4630 in k4627 in k4624 in k4621 in k4618 in k4615 in k4612 in k4609 in k4606 in k4603 in k4600 in k4597 in k4594 in k4591 in k4588 in k4585 in k4582 in k4579 in k4576 in k4573 in k4570 in ... */
static void C_ccall f_4635(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4635,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4638,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:900: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[431];
av2[3]=C_fix(13);
av2[4]=C_SCHEME_FALSE;
av2[5]=lf[432];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4636 in k4633 in k4630 in k4627 in k4624 in k4621 in k4618 in k4615 in k4612 in k4609 in k4606 in k4603 in k4600 in k4597 in k4594 in k4591 in k4588 in k4585 in k4582 in k4579 in k4576 in k4573 in ... */
static void C_ccall f_4638(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4638,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4641,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:902: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[429];
av2[3]=C_fix(13);
av2[4]=C_SCHEME_FALSE;
av2[5]=lf[430];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4639 in k4636 in k4633 in k4630 in k4627 in k4624 in k4621 in k4618 in k4615 in k4612 in k4609 in k4606 in k4603 in k4600 in k4597 in k4594 in k4591 in k4588 in k4585 in k4582 in k4579 in k4576 in ... */
static void C_ccall f_4641(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4641,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4644,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:903: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[427];
av2[3]=C_fix(13);
av2[4]=C_SCHEME_FALSE;
av2[5]=lf[428];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4642 in k4639 in k4636 in k4633 in k4630 in k4627 in k4624 in k4621 in k4618 in k4615 in k4612 in k4609 in k4606 in k4603 in k4600 in k4597 in k4594 in k4591 in k4588 in k4585 in k4582 in k4579 in ... */
static void C_ccall f_4644(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4644,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4647,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:904: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[424];
av2[3]=C_fix(13);
av2[4]=lf[425];
av2[5]=lf[426];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4645 in k4642 in k4639 in k4636 in k4633 in k4630 in k4627 in k4624 in k4621 in k4618 in k4615 in k4612 in k4609 in k4606 in k4603 in k4600 in k4597 in k4594 in k4591 in k4588 in k4585 in k4582 in ... */
static void C_ccall f_4647(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4647,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4650,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:906: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[421];
av2[3]=C_fix(13);
av2[4]=lf[422];
av2[5]=lf[423];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4648 in k4645 in k4642 in k4639 in k4636 in k4633 in k4630 in k4627 in k4624 in k4621 in k4618 in k4615 in k4612 in k4609 in k4606 in k4603 in k4600 in k4597 in k4594 in k4591 in k4588 in k4585 in ... */
static void C_ccall f_4650(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4650,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4653,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:907: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[419];
av2[3]=C_fix(13);
av2[4]=C_fix(1);
av2[5]=lf[420];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4651 in k4648 in k4645 in k4642 in k4639 in k4636 in k4633 in k4630 in k4627 in k4624 in k4621 in k4618 in k4615 in k4612 in k4609 in k4606 in k4603 in k4600 in k4597 in k4594 in k4591 in k4588 in ... */
static void C_ccall f_4653(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4653,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4656,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:908: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[417];
av2[3]=C_fix(13);
av2[4]=C_fix(4);
av2[5]=lf[418];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4654 in k4651 in k4648 in k4645 in k4642 in k4639 in k4636 in k4633 in k4630 in k4627 in k4624 in k4621 in k4618 in k4615 in k4612 in k4609 in k4606 in k4603 in k4600 in k4597 in k4594 in k4591 in ... */
static void C_ccall f_4656(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4656,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4659,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:909: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[415];
av2[3]=C_fix(13);
av2[4]=C_fix(1);
av2[5]=lf[416];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4657 in k4654 in k4651 in k4648 in k4645 in k4642 in k4639 in k4636 in k4633 in k4630 in k4627 in k4624 in k4621 in k4618 in k4615 in k4612 in k4609 in k4606 in k4603 in k4600 in k4597 in k4594 in ... */
static void C_ccall f_4659(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4659,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4662,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:910: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[413];
av2[3]=C_fix(13);
av2[4]=C_fix(0);
av2[5]=lf[414];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4660 in k4657 in k4654 in k4651 in k4648 in k4645 in k4642 in k4639 in k4636 in k4633 in k4630 in k4627 in k4624 in k4621 in k4618 in k4615 in k4612 in k4609 in k4606 in k4603 in k4600 in k4597 in ... */
static void C_ccall f_4662(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4662,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4665,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:911: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[411];
av2[3]=C_fix(13);
av2[4]=C_fix(1);
av2[5]=lf[412];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4663 in k4660 in k4657 in k4654 in k4651 in k4648 in k4645 in k4642 in k4639 in k4636 in k4633 in k4630 in k4627 in k4624 in k4621 in k4618 in k4615 in k4612 in k4609 in k4606 in k4603 in k4600 in ... */
static void C_ccall f_4665(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4665,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4668,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:912: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[409];
av2[3]=C_fix(13);
av2[4]=C_fix(1);
av2[5]=lf[410];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4666 in k4663 in k4660 in k4657 in k4654 in k4651 in k4648 in k4645 in k4642 in k4639 in k4636 in k4633 in k4630 in k4627 in k4624 in k4621 in k4618 in k4615 in k4612 in k4609 in k4606 in k4603 in ... */
static void C_ccall f_4668(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4668,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4671,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:913: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[407];
av2[3]=C_fix(13);
av2[4]=C_fix(1);
av2[5]=lf[408];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4669 in k4666 in k4663 in k4660 in k4657 in k4654 in k4651 in k4648 in k4645 in k4642 in k4639 in k4636 in k4633 in k4630 in k4627 in k4624 in k4621 in k4618 in k4615 in k4612 in k4609 in k4606 in ... */
static void C_ccall f_4671(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4671,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4674,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:915: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[398];
av2[3]=C_fix(14);
av2[4]=lf[24];
av2[5]=C_fix(1);
av2[6]=lf[405];
av2[7]=lf[406];
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4672 in k4669 in k4666 in k4663 in k4660 in k4657 in k4654 in k4651 in k4648 in k4645 in k4642 in k4639 in k4636 in k4633 in k4630 in k4627 in k4624 in k4621 in k4618 in k4615 in k4612 in k4609 in ... */
static void C_ccall f_4674(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4674,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4677,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:916: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[396];
av2[3]=C_fix(14);
av2[4]=lf[24];
av2[5]=C_fix(1);
av2[6]=lf[403];
av2[7]=lf[404];
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4675 in k4672 in k4669 in k4666 in k4663 in k4660 in k4657 in k4654 in k4651 in k4648 in k4645 in k4642 in k4639 in k4636 in k4633 in k4630 in k4627 in k4624 in k4621 in k4618 in k4615 in k4612 in ... */
static void C_ccall f_4677(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4677,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4680,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:917: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[400];
av2[3]=C_fix(14);
av2[4]=lf[24];
av2[5]=C_fix(2);
av2[6]=lf[401];
av2[7]=lf[402];
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4678 in k4675 in k4672 in k4669 in k4666 in k4663 in k4660 in k4657 in k4654 in k4651 in k4648 in k4645 in k4642 in k4639 in k4636 in k4633 in k4630 in k4627 in k4624 in k4621 in k4618 in k4615 in ... */
static void C_ccall f_4680(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_4680,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4683,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:919: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[398];
av2[3]=C_fix(17);
av2[4]=C_fix(1);
av2[5]=lf[399];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4681 in k4678 in k4675 in k4672 in k4669 in k4666 in k4663 in k4660 in k4657 in k4654 in k4651 in k4648 in k4645 in k4642 in k4639 in k4636 in k4633 in k4630 in k4627 in k4624 in k4621 in k4618 in ... */
static void C_ccall f_4683(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_4683,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4686,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:920: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[396];
av2[3]=C_fix(17);
av2[4]=C_fix(1);
av2[5]=lf[397];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4684 in k4681 in k4678 in k4675 in k4672 in k4669 in k4666 in k4663 in k4660 in k4657 in k4654 in k4651 in k4648 in k4645 in k4642 in k4639 in k4636 in k4633 in k4630 in k4627 in k4624 in k4621 in ... */
static void C_ccall f_4686(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4686,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4689,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:922: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[394];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[395];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4687 in k4684 in k4681 in k4678 in k4675 in k4672 in k4669 in k4666 in k4663 in k4660 in k4657 in k4654 in k4651 in k4648 in k4645 in k4642 in k4639 in k4636 in k4633 in k4630 in k4627 in k4624 in ... */
static void C_ccall f_4689(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4689,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4692,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:923: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[392];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[393];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4690 in k4687 in k4684 in k4681 in k4678 in k4675 in k4672 in k4669 in k4666 in k4663 in k4660 in k4657 in k4654 in k4651 in k4648 in k4645 in k4642 in k4639 in k4636 in k4633 in k4630 in k4627 in ... */
static void C_ccall f_4692(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4692,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4695,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:925: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[390];
av2[3]=C_fix(15);
av2[4]=lf[388];
av2[5]=lf[24];
av2[6]=lf[391];
av2[7]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4693 in k4690 in k4687 in k4684 in k4681 in k4678 in k4675 in k4672 in k4669 in k4666 in k4663 in k4660 in k4657 in k4654 in k4651 in k4648 in k4645 in k4642 in k4639 in k4636 in k4633 in k4630 in ... */
static void C_ccall f_4695(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4695,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4698,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:926: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[389];
av2[3]=C_fix(15);
av2[4]=lf[388];
av2[5]=lf[24];
av2[6]=lf[358];
av2[7]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4696 in k4693 in k4690 in k4687 in k4684 in k4681 in k4678 in k4675 in k4672 in k4669 in k4666 in k4663 in k4660 in k4657 in k4654 in k4651 in k4648 in k4645 in k4642 in k4639 in k4636 in k4633 in ... */
static void C_ccall f_4698(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4698,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4701,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:927: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[387];
av2[3]=C_fix(15);
av2[4]=lf[388];
av2[5]=lf[24];
av2[6]=lf[361];
av2[7]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4699 in k4696 in k4693 in k4690 in k4687 in k4684 in k4681 in k4678 in k4675 in k4672 in k4669 in k4666 in k4663 in k4660 in k4657 in k4654 in k4651 in k4648 in k4645 in k4642 in k4639 in k4636 in ... */
static void C_ccall f_4701(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4701,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4704,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:929: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[385];
av2[3]=C_fix(16);
av2[4]=C_fix(1);
av2[5]=lf[386];
av2[6]=C_SCHEME_FALSE;
av2[7]=*((C_word*)lf[11]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4702 in k4699 in k4696 in k4693 in k4690 in k4687 in k4684 in k4681 in k4678 in k4675 in k4672 in k4669 in k4666 in k4663 in k4660 in k4657 in k4654 in k4651 in k4648 in k4645 in k4642 in k4639 in ... */
static void C_ccall f_4704(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4704,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4707,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:930: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[383];
av2[3]=C_fix(16);
av2[4]=C_fix(1);
av2[5]=lf[384];
av2[6]=C_SCHEME_FALSE;
av2[7]=*((C_word*)lf[11]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4705 in k4702 in k4699 in k4696 in k4693 in k4690 in k4687 in k4684 in k4681 in k4678 in k4675 in k4672 in k4669 in k4666 in k4663 in k4660 in k4657 in k4654 in k4651 in k4648 in k4645 in k4642 in ... */
static void C_ccall f_4707(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4707,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4710,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:931: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[381];
av2[3]=C_fix(16);
av2[4]=C_fix(1);
av2[5]=lf[382];
av2[6]=C_SCHEME_FALSE;
av2[7]=*((C_word*)lf[11]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4708 in k4705 in k4702 in k4699 in k4696 in k4693 in k4690 in k4687 in k4684 in k4681 in k4678 in k4675 in k4672 in k4669 in k4666 in k4663 in k4660 in k4657 in k4654 in k4651 in k4648 in k4645 in ... */
static void C_ccall f_4710(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4710,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4713,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:932: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[379];
av2[3]=C_fix(16);
av2[4]=C_fix(1);
av2[5]=lf[380];
av2[6]=C_SCHEME_FALSE;
av2[7]=*((C_word*)lf[11]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4711 in k4708 in k4705 in k4702 in k4699 in k4696 in k4693 in k4690 in k4687 in k4684 in k4681 in k4678 in k4675 in k4672 in k4669 in k4666 in k4663 in k4660 in k4657 in k4654 in k4651 in k4648 in ... */
static void C_ccall f_4713(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4713,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4716,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:933: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[377];
av2[3]=C_fix(16);
av2[4]=C_fix(1);
av2[5]=lf[378];
av2[6]=C_SCHEME_FALSE;
av2[7]=*((C_word*)lf[11]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4714 in k4711 in k4708 in k4705 in k4702 in k4699 in k4696 in k4693 in k4690 in k4687 in k4684 in k4681 in k4678 in k4675 in k4672 in k4669 in k4666 in k4663 in k4660 in k4657 in k4654 in k4651 in ... */
static void C_ccall f_4716(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4716,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4719,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:934: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[375];
av2[3]=C_fix(16);
av2[4]=C_fix(1);
av2[5]=lf[376];
av2[6]=C_SCHEME_FALSE;
av2[7]=*((C_word*)lf[11]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4717 in k4714 in k4711 in k4708 in k4705 in k4702 in k4699 in k4696 in k4693 in k4690 in k4687 in k4684 in k4681 in k4678 in k4675 in k4672 in k4669 in k4666 in k4663 in k4660 in k4657 in k4654 in ... */
static void C_ccall f_4719(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4719,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4722,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:935: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[373];
av2[3]=C_fix(16);
av2[4]=C_fix(2);
av2[5]=lf[374];
av2[6]=C_SCHEME_FALSE;
av2[7]=*((C_word*)lf[11]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4720 in k4717 in k4714 in k4711 in k4708 in k4705 in k4702 in k4699 in k4696 in k4693 in k4690 in k4687 in k4684 in k4681 in k4678 in k4675 in k4672 in k4669 in k4666 in k4663 in k4660 in k4657 in ... */
static void C_ccall f_4722(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4722,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4725,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:936: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[371];
av2[3]=C_fix(16);
av2[4]=C_fix(1);
av2[5]=lf[372];
av2[6]=C_SCHEME_FALSE;
av2[7]=*((C_word*)lf[11]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4723 in k4720 in k4717 in k4714 in k4711 in k4708 in k4705 in k4702 in k4699 in k4696 in k4693 in k4690 in k4687 in k4684 in k4681 in k4678 in k4675 in k4672 in k4669 in k4666 in k4663 in k4660 in ... */
static void C_ccall f_4725(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4725,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4728,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:937: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[369];
av2[3]=C_fix(16);
av2[4]=C_fix(2);
av2[5]=lf[370];
av2[6]=C_SCHEME_FALSE;
av2[7]=*((C_word*)lf[11]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4726 in k4723 in k4720 in k4717 in k4714 in k4711 in k4708 in k4705 in k4702 in k4699 in k4696 in k4693 in k4690 in k4687 in k4684 in k4681 in k4678 in k4675 in k4672 in k4669 in k4666 in k4663 in ... */
static void C_ccall f_4728(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4728,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4731,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:938: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[367];
av2[3]=C_fix(16);
av2[4]=C_fix(1);
av2[5]=lf[368];
av2[6]=C_SCHEME_FALSE;
av2[7]=*((C_word*)lf[11]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4729 in k4726 in k4723 in k4720 in k4717 in k4714 in k4711 in k4708 in k4705 in k4702 in k4699 in k4696 in k4693 in k4690 in k4687 in k4684 in k4681 in k4678 in k4675 in k4672 in k4669 in k4666 in ... */
static void C_ccall f_4731(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4731,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4734,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:939: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[365];
av2[3]=C_fix(16);
av2[4]=C_fix(1);
av2[5]=lf[366];
av2[6]=C_SCHEME_FALSE;
av2[7]=*((C_word*)lf[11]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4732 in k4729 in k4726 in k4723 in k4720 in k4717 in k4714 in k4711 in k4708 in k4705 in k4702 in k4699 in k4696 in k4693 in k4690 in k4687 in k4684 in k4681 in k4678 in k4675 in k4672 in k4669 in ... */
static void C_ccall f_4734(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4734,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4737,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:940: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[363];
av2[3]=C_fix(16);
av2[4]=C_fix(1);
av2[5]=lf[364];
av2[6]=C_SCHEME_FALSE;
av2[7]=*((C_word*)lf[11]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4735 in k4732 in k4729 in k4726 in k4723 in k4720 in k4717 in k4714 in k4711 in k4708 in k4705 in k4702 in k4699 in k4696 in k4693 in k4690 in k4687 in k4684 in k4681 in k4678 in k4675 in k4672 in ... */
static void C_ccall f_4737(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4737,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4740,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:941: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[361];
av2[3]=C_fix(16);
av2[4]=C_fix(1);
av2[5]=lf[362];
av2[6]=C_SCHEME_FALSE;
av2[7]=*((C_word*)lf[11]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4738 in k4735 in k4732 in k4729 in k4726 in k4723 in k4720 in k4717 in k4714 in k4711 in k4708 in k4705 in k4702 in k4699 in k4696 in k4693 in k4690 in k4687 in k4684 in k4681 in k4678 in k4675 in ... */
static void C_ccall f_4740(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4740,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4743,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:942: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[356];
av2[3]=C_fix(16);
av2[4]=C_fix(1);
av2[5]=lf[360];
av2[6]=C_SCHEME_FALSE;
av2[7]=*((C_word*)lf[11]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4741 in k4738 in k4735 in k4732 in k4729 in k4726 in k4723 in k4720 in k4717 in k4714 in k4711 in k4708 in k4705 in k4702 in k4699 in k4696 in k4693 in k4690 in k4687 in k4684 in k4681 in k4678 in ... */
static void C_ccall f_4743(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4743,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4746,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:943: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[358];
av2[3]=C_fix(16);
av2[4]=C_fix(1);
av2[5]=lf[359];
av2[6]=C_SCHEME_FALSE;
av2[7]=*((C_word*)lf[11]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4744 in k4741 in k4738 in k4735 in k4732 in k4729 in k4726 in k4723 in k4720 in k4717 in k4714 in k4711 in k4708 in k4705 in k4702 in k4699 in k4696 in k4693 in k4690 in k4687 in k4684 in k4681 in ... */
static void C_ccall f_4746(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4746,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4749,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:944: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[356];
av2[3]=C_fix(16);
av2[4]=C_fix(1);
av2[5]=lf[357];
av2[6]=C_SCHEME_FALSE;
av2[7]=*((C_word*)lf[11]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4747 in k4744 in k4741 in k4738 in k4735 in k4732 in k4729 in k4726 in k4723 in k4720 in k4717 in k4714 in k4711 in k4708 in k4705 in k4702 in k4699 in k4696 in k4693 in k4690 in k4687 in k4684 in ... */
static void C_ccall f_4749(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4749,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4752,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:946: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[354];
av2[3]=C_fix(16);
av2[4]=C_fix(2);
av2[5]=lf[355];
av2[6]=C_SCHEME_TRUE;
av2[7]=C_fix(3);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4750 in k4747 in k4744 in k4741 in k4738 in k4735 in k4732 in k4729 in k4726 in k4723 in k4720 in k4717 in k4714 in k4711 in k4708 in k4705 in k4702 in k4699 in k4696 in k4693 in k4690 in k4687 in ... */
static void C_ccall f_4752(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4752,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4755,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:947: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[352];
av2[3]=C_fix(16);
av2[4]=C_fix(2);
av2[5]=lf[353];
av2[6]=C_SCHEME_TRUE;
av2[7]=C_fix(3);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4753 in k4750 in k4747 in k4744 in k4741 in k4738 in k4735 in k4732 in k4729 in k4726 in k4723 in k4720 in k4717 in k4714 in k4711 in k4708 in k4705 in k4702 in k4699 in k4696 in k4693 in k4690 in ... */
static void C_ccall f_4755(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,8)))){
C_save_and_reclaim((void *)f_4755,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4758,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:948: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 9) {
  av2=av;
} else {
  av2=C_alloc(9);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[349];
av2[3]=C_fix(16);
av2[4]=C_SCHEME_FALSE;
av2[5]=lf[350];
av2[6]=C_SCHEME_TRUE;
av2[7]=lf[351];
av2[8]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(9,av2);}}

/* k4756 in k4753 in k4750 in k4747 in k4744 in k4741 in k4738 in k4735 in k4732 in k4729 in k4726 in k4723 in k4720 in k4717 in k4714 in k4711 in k4708 in k4705 in k4702 in k4699 in k4696 in k4693 in ... */
static void C_ccall f_4758(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4758,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4761,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:949: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[346];
av2[3]=C_fix(16);
av2[4]=C_SCHEME_FALSE;
av2[5]=lf[347];
av2[6]=C_SCHEME_TRUE;
av2[7]=lf[348];
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4759 in k4756 in k4753 in k4750 in k4747 in k4744 in k4741 in k4738 in k4735 in k4732 in k4729 in k4726 in k4723 in k4720 in k4717 in k4714 in k4711 in k4708 in k4705 in k4702 in k4699 in k4696 in ... */
static void C_ccall f_4761(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,8)))){
C_save_and_reclaim((void *)f_4761,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4764,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:950: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 9) {
  av2=av;
} else {
  av2=C_alloc(9);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[344];
av2[3]=C_fix(16);
av2[4]=C_SCHEME_FALSE;
av2[5]=lf[345];
av2[6]=C_SCHEME_TRUE;
av2[7]=C_SCHEME_TRUE;
av2[8]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(9,av2);}}

/* k4762 in k4759 in k4756 in k4753 in k4750 in k4747 in k4744 in k4741 in k4738 in k4735 in k4732 in k4729 in k4726 in k4723 in k4720 in k4717 in k4714 in k4711 in k4708 in k4705 in k4702 in k4699 in ... */
static void C_ccall f_4764(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4764,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4767,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:951: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[342];
av2[3]=C_fix(16);
av2[4]=C_SCHEME_FALSE;
av2[5]=lf[343];
av2[6]=C_SCHEME_TRUE;
av2[7]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4765 in k4762 in k4759 in k4756 in k4753 in k4750 in k4747 in k4744 in k4741 in k4738 in k4735 in k4732 in k4729 in k4726 in k4723 in k4720 in k4717 in k4714 in k4711 in k4708 in k4705 in k4702 in ... */
static void C_ccall f_4767(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,8)))){
C_save_and_reclaim((void *)f_4767,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4770,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:952: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 9) {
  av2=av;
} else {
  av2=C_alloc(9);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[340];
av2[3]=C_fix(16);
av2[4]=C_SCHEME_FALSE;
av2[5]=lf[341];
av2[6]=C_SCHEME_TRUE;
av2[7]=C_SCHEME_TRUE;
av2[8]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(9,av2);}}

/* k4768 in k4765 in k4762 in k4759 in k4756 in k4753 in k4750 in k4747 in k4744 in k4741 in k4738 in k4735 in k4732 in k4729 in k4726 in k4723 in k4720 in k4717 in k4714 in k4711 in k4708 in k4705 in ... */
static void C_ccall f_4770(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4770,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4773,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:953: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[338];
av2[3]=C_fix(16);
av2[4]=C_SCHEME_FALSE;
av2[5]=lf[339];
av2[6]=C_SCHEME_TRUE;
av2[7]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4771 in k4768 in k4765 in k4762 in k4759 in k4756 in k4753 in k4750 in k4747 in k4744 in k4741 in k4738 in k4735 in k4732 in k4729 in k4726 in k4723 in k4720 in k4717 in k4714 in k4711 in k4708 in ... */
static void C_ccall f_4773(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4773,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4776,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:954: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[336];
av2[3]=C_fix(16);
av2[4]=C_fix(1);
av2[5]=lf[337];
av2[6]=C_SCHEME_FALSE;
av2[7]=C_fix(2);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4774 in k4771 in k4768 in k4765 in k4762 in k4759 in k4756 in k4753 in k4750 in k4747 in k4744 in k4741 in k4738 in k4735 in k4732 in k4729 in k4726 in k4723 in k4720 in k4717 in k4714 in k4711 in ... */
static void C_ccall f_4776(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4776,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4779,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:955: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[334];
av2[3]=C_fix(16);
av2[4]=C_fix(1);
av2[5]=lf[335];
av2[6]=C_SCHEME_FALSE;
av2[7]=*((C_word*)lf[11]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4777 in k4774 in k4771 in k4768 in k4765 in k4762 in k4759 in k4756 in k4753 in k4750 in k4747 in k4744 in k4741 in k4738 in k4735 in k4732 in k4729 in k4726 in k4723 in k4720 in k4717 in k4714 in ... */
static void C_ccall f_4779(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4779,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4782,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:956: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[332];
av2[3]=C_fix(16);
av2[4]=C_fix(2);
av2[5]=lf[333];
av2[6]=C_SCHEME_FALSE;
av2[7]=C_fix(2);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4780 in k4777 in k4774 in k4771 in k4768 in k4765 in k4762 in k4759 in k4756 in k4753 in k4750 in k4747 in k4744 in k4741 in k4738 in k4735 in k4732 in k4729 in k4726 in k4723 in k4720 in k4717 in ... */
static void C_ccall f_4782(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4782,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4785,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:957: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[330];
av2[3]=C_fix(16);
av2[4]=C_fix(1);
av2[5]=lf[331];
av2[6]=C_SCHEME_TRUE;
av2[7]=C_fix(6);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4783 in k4780 in k4777 in k4774 in k4771 in k4768 in k4765 in k4762 in k4759 in k4756 in k4753 in k4750 in k4747 in k4744 in k4741 in k4738 in k4735 in k4732 in k4729 in k4726 in k4723 in k4720 in ... */
static void C_ccall f_4785(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4785,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4788,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:959: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[328];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[329];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4786 in k4783 in k4780 in k4777 in k4774 in k4771 in k4768 in k4765 in k4762 in k4759 in k4756 in k4753 in k4750 in k4747 in k4744 in k4741 in k4738 in k4735 in k4732 in k4729 in k4726 in k4723 in ... */
static void C_ccall f_4788(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4788,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4791,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:960: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[326];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[327];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4789 in k4786 in k4783 in k4780 in k4777 in k4774 in k4771 in k4768 in k4765 in k4762 in k4759 in k4756 in k4753 in k4750 in k4747 in k4744 in k4741 in k4738 in k4735 in k4732 in k4729 in k4726 in ... */
static void C_ccall f_4791(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4791,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4794,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:961: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[324];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[325];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4792 in k4789 in k4786 in k4783 in k4780 in k4777 in k4774 in k4771 in k4768 in k4765 in k4762 in k4759 in k4756 in k4753 in k4750 in k4747 in k4744 in k4741 in k4738 in k4735 in k4732 in k4729 in ... */
static void C_ccall f_4794(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4794,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4797,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:962: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[322];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[323];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4795 in k4792 in k4789 in k4786 in k4783 in k4780 in k4777 in k4774 in k4771 in k4768 in k4765 in k4762 in k4759 in k4756 in k4753 in k4750 in k4747 in k4744 in k4741 in k4738 in k4735 in k4732 in ... */
static void C_ccall f_4797(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4797,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4800,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:963: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[320];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[321];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4798 in k4795 in k4792 in k4789 in k4786 in k4783 in k4780 in k4777 in k4774 in k4771 in k4768 in k4765 in k4762 in k4759 in k4756 in k4753 in k4750 in k4747 in k4744 in k4741 in k4738 in k4735 in ... */
static void C_ccall f_4800(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4800,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4803,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:964: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[318];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[319];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4801 in k4798 in k4795 in k4792 in k4789 in k4786 in k4783 in k4780 in k4777 in k4774 in k4771 in k4768 in k4765 in k4762 in k4759 in k4756 in k4753 in k4750 in k4747 in k4744 in k4741 in k4738 in ... */
static void C_ccall f_4803(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4803,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4806,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:965: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[316];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[317];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4804 in k4801 in k4798 in k4795 in k4792 in k4789 in k4786 in k4783 in k4780 in k4777 in k4774 in k4771 in k4768 in k4765 in k4762 in k4759 in k4756 in k4753 in k4750 in k4747 in k4744 in k4741 in ... */
static void C_ccall f_4806(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4806,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4809,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:966: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[314];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[315];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4807 in k4804 in k4801 in k4798 in k4795 in k4792 in k4789 in k4786 in k4783 in k4780 in k4777 in k4774 in k4771 in k4768 in k4765 in k4762 in k4759 in k4756 in k4753 in k4750 in k4747 in k4744 in ... */
static void C_ccall f_4809(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4809,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4812,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:967: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[312];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[313];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4810 in k4807 in k4804 in k4801 in k4798 in k4795 in k4792 in k4789 in k4786 in k4783 in k4780 in k4777 in k4774 in k4771 in k4768 in k4765 in k4762 in k4759 in k4756 in k4753 in k4750 in k4747 in ... */
static void C_ccall f_4812(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4812,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4815,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:968: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[310];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[311];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4813 in k4810 in k4807 in k4804 in k4801 in k4798 in k4795 in k4792 in k4789 in k4786 in k4783 in k4780 in k4777 in k4774 in k4771 in k4768 in k4765 in k4762 in k4759 in k4756 in k4753 in k4750 in ... */
static void C_ccall f_4815(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4815,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4818,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:969: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[308];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[309];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4816 in k4813 in k4810 in k4807 in k4804 in k4801 in k4798 in k4795 in k4792 in k4789 in k4786 in k4783 in k4780 in k4777 in k4774 in k4771 in k4768 in k4765 in k4762 in k4759 in k4756 in k4753 in ... */
static void C_ccall f_4818(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4818,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4821,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:970: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[306];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[307];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4819 in k4816 in k4813 in k4810 in k4807 in k4804 in k4801 in k4798 in k4795 in k4792 in k4789 in k4786 in k4783 in k4780 in k4777 in k4774 in k4771 in k4768 in k4765 in k4762 in k4759 in k4756 in ... */
static void C_ccall f_4821(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4821,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4824,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:974: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[304];
av2[3]=C_fix(16);
av2[4]=C_fix(1);
av2[5]=lf[305];
av2[6]=C_SCHEME_FALSE;
av2[7]=C_fix(5);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4822 in k4819 in k4816 in k4813 in k4810 in k4807 in k4804 in k4801 in k4798 in k4795 in k4792 in k4789 in k4786 in k4783 in k4780 in k4777 in k4774 in k4771 in k4768 in k4765 in k4762 in k4759 in ... */
static void C_ccall f_4824(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4824,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4827,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:975: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[302];
av2[3]=C_fix(16);
av2[4]=C_fix(1);
av2[5]=lf[303];
av2[6]=C_SCHEME_FALSE;
av2[7]=C_fix(5);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4825 in k4822 in k4819 in k4816 in k4813 in k4810 in k4807 in k4804 in k4801 in k4798 in k4795 in k4792 in k4789 in k4786 in k4783 in k4780 in k4777 in k4774 in k4771 in k4768 in k4765 in k4762 in ... */
static void C_ccall f_4827(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4827,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4830,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:977: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[300];
av2[3]=C_fix(16);
av2[4]=C_fix(1);
av2[5]=lf[301];
av2[6]=C_SCHEME_FALSE;
av2[7]=*((C_word*)lf[11]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4828 in k4825 in k4822 in k4819 in k4816 in k4813 in k4810 in k4807 in k4804 in k4801 in k4798 in k4795 in k4792 in k4789 in k4786 in k4783 in k4780 in k4777 in k4774 in k4771 in k4768 in k4765 in ... */
static void C_ccall f_4830(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4830,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4833,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:978: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[298];
av2[3]=C_fix(16);
av2[4]=C_fix(1);
av2[5]=lf[299];
av2[6]=C_SCHEME_FALSE;
av2[7]=*((C_word*)lf[11]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4831 in k4828 in k4825 in k4822 in k4819 in k4816 in k4813 in k4810 in k4807 in k4804 in k4801 in k4798 in k4795 in k4792 in k4789 in k4786 in k4783 in k4780 in k4777 in k4774 in k4771 in k4768 in ... */
static void C_ccall f_4833(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,6)))){
C_save_and_reclaim((void *)f_4833,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4836,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_5697,tmp=(C_word)a,a+=2,tmp);
/* c-platform.scm:980: chicken.compiler.optimizer#rewrite */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=lf[297];
av2[3]=C_fix(8);
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k4834 in k4831 in k4828 in k4825 in k4822 in k4819 in k4816 in k4813 in k4810 in k4807 in k4804 in k4801 in k4798 in k4795 in k4792 in k4789 in k4786 in k4783 in k4780 in k4777 in k4774 in k4771 in ... */
static void C_ccall f_4836(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4836,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4839,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:998: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[292];
av2[3]=C_fix(17);
av2[4]=C_fix(2);
av2[5]=lf[293];
av2[6]=lf[294];
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4837 in k4834 in k4831 in k4828 in k4825 in k4822 in k4819 in k4816 in k4813 in k4810 in k4807 in k4804 in k4801 in k4798 in k4795 in k4792 in k4789 in k4786 in k4783 in k4780 in k4777 in k4774 in ... */
static void C_ccall f_4839(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4839,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4842,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:999: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[289];
av2[3]=C_fix(17);
av2[4]=C_fix(2);
av2[5]=lf[290];
av2[6]=lf[291];
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4840 in k4837 in k4834 in k4831 in k4828 in k4825 in k4822 in k4819 in k4816 in k4813 in k4810 in k4807 in k4804 in k4801 in k4798 in k4795 in k4792 in k4789 in k4786 in k4783 in k4780 in k4777 in ... */
static void C_ccall f_4842(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_4842,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4845,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1000: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[287];
av2[3]=C_fix(17);
av2[4]=C_fix(2);
av2[5]=lf[288];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4843 in k4840 in k4837 in k4834 in k4831 in k4828 in k4825 in k4822 in k4819 in k4816 in k4813 in k4810 in k4807 in k4804 in k4801 in k4798 in k4795 in k4792 in k4789 in k4786 in k4783 in k4780 in ... */
static void C_ccall f_4845(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_4845,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4848,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1001: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[285];
av2[3]=C_fix(17);
av2[4]=C_fix(2);
av2[5]=lf[286];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4846 in k4843 in k4840 in k4837 in k4834 in k4831 in k4828 in k4825 in k4822 in k4819 in k4816 in k4813 in k4810 in k4807 in k4804 in k4801 in k4798 in k4795 in k4792 in k4789 in k4786 in k4783 in ... */
static void C_ccall f_4848(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4848,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4851,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1002: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[282];
av2[3]=C_fix(17);
av2[4]=C_fix(1);
av2[5]=lf[283];
av2[6]=lf[284];
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4849 in k4846 in k4843 in k4840 in k4837 in k4834 in k4831 in k4828 in k4825 in k4822 in k4819 in k4816 in k4813 in k4810 in k4807 in k4804 in k4801 in k4798 in k4795 in k4792 in k4789 in k4786 in ... */
static void C_ccall f_4851(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4851,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4854,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1003: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[279];
av2[3]=C_fix(17);
av2[4]=C_fix(2);
av2[5]=lf[280];
av2[6]=lf[281];
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4852 in k4849 in k4846 in k4843 in k4840 in k4837 in k4834 in k4831 in k4828 in k4825 in k4822 in k4819 in k4816 in k4813 in k4810 in k4807 in k4804 in k4801 in k4798 in k4795 in k4792 in k4789 in ... */
static void C_ccall f_4854(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4854,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4857,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1004: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[276];
av2[3]=C_fix(17);
av2[4]=C_fix(2);
av2[5]=lf[277];
av2[6]=lf[278];
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4855 in k4852 in k4849 in k4846 in k4843 in k4840 in k4837 in k4834 in k4831 in k4828 in k4825 in k4822 in k4819 in k4816 in k4813 in k4810 in k4807 in k4804 in k4801 in k4798 in k4795 in k4792 in ... */
static void C_ccall f_4857(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4857,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4860,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1005: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[273];
av2[3]=C_fix(17);
av2[4]=C_fix(2);
av2[5]=lf[274];
av2[6]=lf[275];
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4858 in k4855 in k4852 in k4849 in k4846 in k4843 in k4840 in k4837 in k4834 in k4831 in k4828 in k4825 in k4822 in k4819 in k4816 in k4813 in k4810 in k4807 in k4804 in k4801 in k4798 in k4795 in ... */
static void C_ccall f_4860(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4860,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4863,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1006: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[270];
av2[3]=C_fix(17);
av2[4]=C_fix(2);
av2[5]=lf[271];
av2[6]=lf[272];
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4861 in k4858 in k4855 in k4852 in k4849 in k4846 in k4843 in k4840 in k4837 in k4834 in k4831 in k4828 in k4825 in k4822 in k4819 in k4816 in k4813 in k4810 in k4807 in k4804 in k4801 in k4798 in ... */
static void C_ccall f_4863(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4863,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4866,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1007: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[267];
av2[3]=C_fix(17);
av2[4]=C_fix(2);
av2[5]=lf[268];
av2[6]=lf[269];
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4864 in k4861 in k4858 in k4855 in k4852 in k4849 in k4846 in k4843 in k4840 in k4837 in k4834 in k4831 in k4828 in k4825 in k4822 in k4819 in k4816 in k4813 in k4810 in k4807 in k4804 in k4801 in ... */
static void C_ccall f_4866(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_4866,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4869,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1008: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[265];
av2[3]=C_fix(17);
av2[4]=C_fix(2);
av2[5]=lf[266];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4867 in k4864 in k4861 in k4858 in k4855 in k4852 in k4849 in k4846 in k4843 in k4840 in k4837 in k4834 in k4831 in k4828 in k4825 in k4822 in k4819 in k4816 in k4813 in k4810 in k4807 in k4804 in ... */
static void C_ccall f_4869(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,6)))){
C_save_and_reclaim((void *)f_4869,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4872,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_5575,tmp=(C_word)a,a+=2,tmp);
/* c-platform.scm:1010: chicken.compiler.optimizer#rewrite */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=lf[264];
av2[3]=C_fix(8);
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k4870 in k4867 in k4864 in k4861 in k4858 in k4855 in k4852 in k4849 in k4846 in k4843 in k4840 in k4837 in k4834 in k4831 in k4828 in k4825 in k4822 in k4819 in k4816 in k4813 in k4810 in k4807 in ... */
static void C_ccall f_4872(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_4872,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4875,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1044: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[257];
av2[3]=C_fix(17);
av2[4]=C_fix(2);
av2[5]=lf[258];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4873 in k4870 in k4867 in k4864 in k4861 in k4858 in k4855 in k4852 in k4849 in k4846 in k4843 in k4840 in k4837 in k4834 in k4831 in k4828 in k4825 in k4822 in k4819 in k4816 in k4813 in k4810 in ... */
static void C_ccall f_4875(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_4875,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4878,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1045: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[255];
av2[3]=C_fix(17);
av2[4]=C_fix(3);
av2[5]=lf[256];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4876 in k4873 in k4870 in k4867 in k4864 in k4861 in k4858 in k4855 in k4852 in k4849 in k4846 in k4843 in k4840 in k4837 in k4834 in k4831 in k4828 in k4825 in k4822 in k4819 in k4816 in k4813 in ... */
static void C_ccall f_4878(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_4878,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4881,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1046: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[253];
av2[3]=C_fix(17);
av2[4]=C_fix(2);
av2[5]=lf[254];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4879 in k4876 in k4873 in k4870 in k4867 in k4864 in k4861 in k4858 in k4855 in k4852 in k4849 in k4846 in k4843 in k4840 in k4837 in k4834 in k4831 in k4828 in k4825 in k4822 in k4819 in k4816 in ... */
static void C_ccall f_4881(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_4881,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4884,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1047: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[251];
av2[3]=C_fix(17);
av2[4]=C_fix(2);
av2[5]=lf[252];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4882 in k4879 in k4876 in k4873 in k4870 in k4867 in k4864 in k4861 in k4858 in k4855 in k4852 in k4849 in k4846 in k4843 in k4840 in k4837 in k4834 in k4831 in k4828 in k4825 in k4822 in k4819 in ... */
static void C_ccall f_4884(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_4884,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4887,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1048: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[249];
av2[3]=C_fix(17);
av2[4]=C_fix(2);
av2[5]=lf[250];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4885 in k4882 in k4879 in k4876 in k4873 in k4870 in k4867 in k4864 in k4861 in k4858 in k4855 in k4852 in k4849 in k4846 in k4843 in k4840 in k4837 in k4834 in k4831 in k4828 in k4825 in k4822 in ... */
static void C_ccall f_4887(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_4887,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4890,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1049: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[247];
av2[3]=C_fix(17);
av2[4]=C_fix(3);
av2[5]=lf[248];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4888 in k4885 in k4882 in k4879 in k4876 in k4873 in k4870 in k4867 in k4864 in k4861 in k4858 in k4855 in k4852 in k4849 in k4846 in k4843 in k4840 in k4837 in k4834 in k4831 in k4828 in k4825 in ... */
static void C_ccall f_4890(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_4890,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4893,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1050: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[245];
av2[3]=C_fix(17);
av2[4]=C_fix(3);
av2[5]=lf[246];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4891 in k4888 in k4885 in k4882 in k4879 in k4876 in k4873 in k4870 in k4867 in k4864 in k4861 in k4858 in k4855 in k4852 in k4849 in k4846 in k4843 in k4840 in k4837 in k4834 in k4831 in k4828 in ... */
static void C_ccall f_4893(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_4893,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4896,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1051: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[243];
av2[3]=C_fix(17);
av2[4]=C_fix(3);
av2[5]=lf[244];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4894 in k4891 in k4888 in k4885 in k4882 in k4879 in k4876 in k4873 in k4870 in k4867 in k4864 in k4861 in k4858 in k4855 in k4852 in k4849 in k4846 in k4843 in k4840 in k4837 in k4834 in k4831 in ... */
static void C_ccall f_4896(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4896,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4899,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1052: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[240];
av2[3]=C_fix(17);
av2[4]=C_fix(2);
av2[5]=lf[241];
av2[6]=lf[242];
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4897 in k4894 in k4891 in k4888 in k4885 in k4882 in k4879 in k4876 in k4873 in k4870 in k4867 in k4864 in k4861 in k4858 in k4855 in k4852 in k4849 in k4846 in k4843 in k4840 in k4837 in k4834 in ... */
static void C_ccall f_4899(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_4899,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4902,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1053: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[238];
av2[3]=C_fix(17);
av2[4]=C_fix(2);
av2[5]=lf[239];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4900 in k4897 in k4894 in k4891 in k4888 in k4885 in k4882 in k4879 in k4876 in k4873 in k4870 in k4867 in k4864 in k4861 in k4858 in k4855 in k4852 in k4849 in k4846 in k4843 in k4840 in k4837 in ... */
static void C_ccall f_4902(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_4902,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4905,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1054: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[236];
av2[3]=C_fix(17);
av2[4]=C_fix(1);
av2[5]=lf[237];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4903 in k4900 in k4897 in k4894 in k4891 in k4888 in k4885 in k4882 in k4879 in k4876 in k4873 in k4870 in k4867 in k4864 in k4861 in k4858 in k4855 in k4852 in k4849 in k4846 in k4843 in k4840 in ... */
static void C_ccall f_4905(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4905,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4908,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1055: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[233];
av2[3]=C_fix(17);
av2[4]=C_fix(1);
av2[5]=lf[234];
av2[6]=lf[235];
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4906 in k4903 in k4900 in k4897 in k4894 in k4891 in k4888 in k4885 in k4882 in k4879 in k4876 in k4873 in k4870 in k4867 in k4864 in k4861 in k4858 in k4855 in k4852 in k4849 in k4846 in k4843 in ... */
static void C_ccall f_4908(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_4908,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4911,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1056: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[231];
av2[3]=C_fix(17);
av2[4]=C_fix(1);
av2[5]=lf[232];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4909 in k4906 in k4903 in k4900 in k4897 in k4894 in k4891 in k4888 in k4885 in k4882 in k4879 in k4876 in k4873 in k4870 in k4867 in k4864 in k4861 in k4858 in k4855 in k4852 in k4849 in k4846 in ... */
static void C_ccall f_4911(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_4911,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4914,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1057: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[229];
av2[3]=C_fix(17);
av2[4]=C_fix(1);
av2[5]=lf[230];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4912 in k4909 in k4906 in k4903 in k4900 in k4897 in k4894 in k4891 in k4888 in k4885 in k4882 in k4879 in k4876 in k4873 in k4870 in k4867 in k4864 in k4861 in k4858 in k4855 in k4852 in k4849 in ... */
static void C_ccall f_4914(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_4914,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4917,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1058: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[227];
av2[3]=C_fix(17);
av2[4]=C_fix(2);
av2[5]=lf[228];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4915 in k4912 in k4909 in k4906 in k4903 in k4900 in k4897 in k4894 in k4891 in k4888 in k4885 in k4882 in k4879 in k4876 in k4873 in k4870 in k4867 in k4864 in k4861 in k4858 in k4855 in k4852 in ... */
static void C_ccall f_4917(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_4917,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4920,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1059: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[225];
av2[3]=C_fix(17);
av2[4]=C_fix(1);
av2[5]=lf[226];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4918 in k4915 in k4912 in k4909 in k4906 in k4903 in k4900 in k4897 in k4894 in k4891 in k4888 in k4885 in k4882 in k4879 in k4876 in k4873 in k4870 in k4867 in k4864 in k4861 in k4858 in k4855 in ... */
static void C_ccall f_4920(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_4920,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4923,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1060: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[223];
av2[3]=C_fix(17);
av2[4]=C_fix(1);
av2[5]=lf[224];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4921 in k4918 in k4915 in k4912 in k4909 in k4906 in k4903 in k4900 in k4897 in k4894 in k4891 in k4888 in k4885 in k4882 in k4879 in k4876 in k4873 in k4870 in k4867 in k4864 in k4861 in k4858 in ... */
static void C_ccall f_4923(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_4923,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4926,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1061: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[221];
av2[3]=C_fix(17);
av2[4]=C_fix(1);
av2[5]=lf[222];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4924 in k4921 in k4918 in k4915 in k4912 in k4909 in k4906 in k4903 in k4900 in k4897 in k4894 in k4891 in k4888 in k4885 in k4882 in k4879 in k4876 in k4873 in k4870 in k4867 in k4864 in k4861 in ... */
static void C_ccall f_4926(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_4926,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4929,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1062: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[219];
av2[3]=C_fix(17);
av2[4]=C_fix(1);
av2[5]=lf[220];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4927 in k4924 in k4921 in k4918 in k4915 in k4912 in k4909 in k4906 in k4903 in k4900 in k4897 in k4894 in k4891 in k4888 in k4885 in k4882 in k4879 in k4876 in k4873 in k4870 in k4867 in k4864 in ... */
static void C_ccall f_4929(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_4929,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4932,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1063: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[217];
av2[3]=C_fix(17);
av2[4]=C_fix(2);
av2[5]=lf[218];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4930 in k4927 in k4924 in k4921 in k4918 in k4915 in k4912 in k4909 in k4906 in k4903 in k4900 in k4897 in k4894 in k4891 in k4888 in k4885 in k4882 in k4879 in k4876 in k4873 in k4870 in k4867 in ... */
static void C_ccall f_4932(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_4932,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4935,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1064: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[215];
av2[3]=C_fix(17);
av2[4]=C_fix(1);
av2[5]=lf[216];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4933 in k4930 in k4927 in k4924 in k4921 in k4918 in k4915 in k4912 in k4909 in k4906 in k4903 in k4900 in k4897 in k4894 in k4891 in k4888 in k4885 in k4882 in k4879 in k4876 in k4873 in k4870 in ... */
static void C_ccall f_4935(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_4935,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4938,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1065: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[213];
av2[3]=C_fix(17);
av2[4]=C_fix(1);
av2[5]=lf[214];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4936 in k4933 in k4930 in k4927 in k4924 in k4921 in k4918 in k4915 in k4912 in k4909 in k4906 in k4903 in k4900 in k4897 in k4894 in k4891 in k4888 in k4885 in k4882 in k4879 in k4876 in k4873 in ... */
static void C_ccall f_4938(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_4938,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4941,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1066: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[211];
av2[3]=C_fix(17);
av2[4]=C_fix(2);
av2[5]=lf[212];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4939 in k4936 in k4933 in k4930 in k4927 in k4924 in k4921 in k4918 in k4915 in k4912 in k4909 in k4906 in k4903 in k4900 in k4897 in k4894 in k4891 in k4888 in k4885 in k4882 in k4879 in k4876 in ... */
static void C_ccall f_4941(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_4941,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4944,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1067: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[209];
av2[3]=C_fix(17);
av2[4]=C_fix(2);
av2[5]=lf[210];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4942 in k4939 in k4936 in k4933 in k4930 in k4927 in k4924 in k4921 in k4918 in k4915 in k4912 in k4909 in k4906 in k4903 in k4900 in k4897 in k4894 in k4891 in k4888 in k4885 in k4882 in k4879 in ... */
static void C_ccall f_4944(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4944,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4947,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1069: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[207];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[208];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4945 in k4942 in k4939 in k4936 in k4933 in k4930 in k4927 in k4924 in k4921 in k4918 in k4915 in k4912 in k4909 in k4906 in k4903 in k4900 in k4897 in k4894 in k4891 in k4888 in k4885 in k4882 in ... */
static void C_ccall f_4947(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4947,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4950,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1072: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[204];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[206];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4948 in k4945 in k4942 in k4939 in k4936 in k4933 in k4930 in k4927 in k4924 in k4921 in k4918 in k4915 in k4912 in k4909 in k4906 in k4903 in k4900 in k4897 in k4894 in k4891 in k4888 in k4885 in ... */
static void C_ccall f_4950(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4950,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4953,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1073: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[204];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[205];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4951 in k4948 in k4945 in k4942 in k4939 in k4936 in k4933 in k4930 in k4927 in k4924 in k4921 in k4918 in k4915 in k4912 in k4909 in k4906 in k4903 in k4900 in k4897 in k4894 in k4891 in k4888 in ... */
static void C_ccall f_4953(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4953,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4956,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1074: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[201];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[203];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4954 in k4951 in k4948 in k4945 in k4942 in k4939 in k4936 in k4933 in k4930 in k4927 in k4924 in k4921 in k4918 in k4915 in k4912 in k4909 in k4906 in k4903 in k4900 in k4897 in k4894 in k4891 in ... */
static void C_ccall f_4956(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4956,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4959,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1075: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[201];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[202];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4957 in k4954 in k4951 in k4948 in k4945 in k4942 in k4939 in k4936 in k4933 in k4930 in k4927 in k4924 in k4921 in k4918 in k4915 in k4912 in k4909 in k4906 in k4903 in k4900 in k4897 in k4894 in ... */
static void C_ccall f_4959(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4959,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4962,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1076: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[198];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[200];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4960 in k4957 in k4954 in k4951 in k4948 in k4945 in k4942 in k4939 in k4936 in k4933 in k4930 in k4927 in k4924 in k4921 in k4918 in k4915 in k4912 in k4909 in k4906 in k4903 in k4900 in k4897 in ... */
static void C_ccall f_4962(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4962,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4965,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1077: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[198];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[199];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4963 in k4960 in k4957 in k4954 in k4951 in k4948 in k4945 in k4942 in k4939 in k4936 in k4933 in k4930 in k4927 in k4924 in k4921 in k4918 in k4915 in k4912 in k4909 in k4906 in k4903 in k4900 in ... */
static void C_ccall f_4965(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4965,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4968,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1078: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[195];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[197];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4966 in k4963 in k4960 in k4957 in k4954 in k4951 in k4948 in k4945 in k4942 in k4939 in k4936 in k4933 in k4930 in k4927 in k4924 in k4921 in k4918 in k4915 in k4912 in k4909 in k4906 in k4903 in ... */
static void C_ccall f_4968(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4968,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4971,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1079: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[195];
av2[3]=C_fix(2);
av2[4]=C_fix(2);
av2[5]=lf[196];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4969 in k4966 in k4963 in k4960 in k4957 in k4954 in k4951 in k4948 in k4945 in k4942 in k4939 in k4936 in k4933 in k4930 in k4927 in k4924 in k4921 in k4918 in k4915 in k4912 in k4909 in k4906 in ... */
static void C_ccall f_4971(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4971,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4974,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1081: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[193];
av2[3]=C_fix(16);
av2[4]=C_fix(2);
av2[5]=lf[194];
av2[6]=C_SCHEME_TRUE;
av2[7]=C_fix(5);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4972 in k4969 in k4966 in k4963 in k4960 in k4957 in k4954 in k4951 in k4948 in k4945 in k4942 in k4939 in k4936 in k4933 in k4930 in k4927 in k4924 in k4921 in k4918 in k4915 in k4912 in k4909 in ... */
static void C_ccall f_4974(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4974,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4977,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1082: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[191];
av2[3]=C_fix(16);
av2[4]=C_fix(2);
av2[5]=lf[192];
av2[6]=C_SCHEME_TRUE;
av2[7]=C_fix(5);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4975 in k4972 in k4969 in k4966 in k4963 in k4960 in k4957 in k4954 in k4951 in k4948 in k4945 in k4942 in k4939 in k4936 in k4933 in k4930 in k4927 in k4924 in k4921 in k4918 in k4915 in k4912 in ... */
static void C_ccall f_4977(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4977,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4980,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1084: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[188];
av2[3]=C_fix(16);
av2[4]=C_fix(2);
av2[5]=lf[190];
av2[6]=C_SCHEME_FALSE;
av2[7]=*((C_word*)lf[11]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4978 in k4975 in k4972 in k4969 in k4966 in k4963 in k4960 in k4957 in k4954 in k4951 in k4948 in k4945 in k4942 in k4939 in k4936 in k4933 in k4930 in k4927 in k4924 in k4921 in k4918 in k4915 in ... */
static void C_ccall f_4980(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4980,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4983,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1085: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[188];
av2[3]=C_fix(16);
av2[4]=C_fix(2);
av2[5]=lf[189];
av2[6]=C_SCHEME_TRUE;
av2[7]=*((C_word*)lf[11]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4981 in k4978 in k4975 in k4972 in k4969 in k4966 in k4963 in k4960 in k4957 in k4954 in k4951 in k4948 in k4945 in k4942 in k4939 in k4936 in k4933 in k4930 in k4927 in k4924 in k4921 in k4918 in ... */
static void C_ccall f_4983(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4983,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4986,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1086: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[185];
av2[3]=C_fix(16);
av2[4]=C_fix(2);
av2[5]=lf[187];
av2[6]=C_SCHEME_FALSE;
av2[7]=*((C_word*)lf[11]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4984 in k4981 in k4978 in k4975 in k4972 in k4969 in k4966 in k4963 in k4960 in k4957 in k4954 in k4951 in k4948 in k4945 in k4942 in k4939 in k4936 in k4933 in k4930 in k4927 in k4924 in k4921 in ... */
static void C_ccall f_4986(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_4986,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4989,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1087: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[185];
av2[3]=C_fix(16);
av2[4]=C_fix(2);
av2[5]=lf[186];
av2[6]=C_SCHEME_TRUE;
av2[7]=*((C_word*)lf[11]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k4987 in k4984 in k4981 in k4978 in k4975 in k4972 in k4969 in k4966 in k4963 in k4960 in k4957 in k4954 in k4951 in k4948 in k4945 in k4942 in k4939 in k4936 in k4933 in k4930 in k4927 in k4924 in ... */
static void C_ccall f_4989(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4989,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4992,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1089: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[182];
av2[3]=C_fix(2);
av2[4]=C_fix(3);
av2[5]=lf[184];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4990 in k4987 in k4984 in k4981 in k4978 in k4975 in k4972 in k4969 in k4966 in k4963 in k4960 in k4957 in k4954 in k4951 in k4948 in k4945 in k4942 in k4939 in k4936 in k4933 in k4930 in k4927 in ... */
static void C_ccall f_4992(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4992,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4995,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1090: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[182];
av2[3]=C_fix(2);
av2[4]=C_fix(3);
av2[5]=lf[183];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4993 in k4990 in k4987 in k4984 in k4981 in k4978 in k4975 in k4972 in k4969 in k4966 in k4963 in k4960 in k4957 in k4954 in k4951 in k4948 in k4945 in k4942 in k4939 in k4936 in k4933 in k4930 in ... */
static void C_ccall f_4995(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4995,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4998,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1091: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[179];
av2[3]=C_fix(2);
av2[4]=C_fix(3);
av2[5]=lf[181];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4996 in k4993 in k4990 in k4987 in k4984 in k4981 in k4978 in k4975 in k4972 in k4969 in k4966 in k4963 in k4960 in k4957 in k4954 in k4951 in k4948 in k4945 in k4942 in k4939 in k4936 in k4933 in ... */
static void C_ccall f_4998(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_4998,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5001,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1092: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[179];
av2[3]=C_fix(2);
av2[4]=C_fix(3);
av2[5]=lf[180];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k4999 in k4996 in k4993 in k4990 in k4987 in k4984 in k4981 in k4978 in k4975 in k4972 in k4969 in k4966 in k4963 in k4960 in k4957 in k4954 in k4951 in k4948 in k4945 in k4942 in k4939 in k4936 in ... */
static void C_ccall f_5001(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5001,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5004,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1093: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[176];
av2[3]=C_fix(2);
av2[4]=C_fix(3);
av2[5]=lf[178];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5002 in k4999 in k4996 in k4993 in k4990 in k4987 in k4984 in k4981 in k4978 in k4975 in k4972 in k4969 in k4966 in k4963 in k4960 in k4957 in k4954 in k4951 in k4948 in k4945 in k4942 in k4939 in ... */
static void C_ccall f_5004(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5004,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5007,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1094: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[176];
av2[3]=C_fix(2);
av2[4]=C_fix(3);
av2[5]=lf[177];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5005 in k5002 in k4999 in k4996 in k4993 in k4990 in k4987 in k4984 in k4981 in k4978 in k4975 in k4972 in k4969 in k4966 in k4963 in k4960 in k4957 in k4954 in k4951 in k4948 in k4945 in k4942 in ... */
static void C_ccall f_5007(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5007,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5010,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1095: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[173];
av2[3]=C_fix(2);
av2[4]=C_fix(3);
av2[5]=lf[175];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5008 in k5005 in k5002 in k4999 in k4996 in k4993 in k4990 in k4987 in k4984 in k4981 in k4978 in k4975 in k4972 in k4969 in k4966 in k4963 in k4960 in k4957 in k4954 in k4951 in k4948 in k4945 in ... */
static void C_ccall f_5010(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5010,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5013,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1096: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[173];
av2[3]=C_fix(2);
av2[4]=C_fix(3);
av2[5]=lf[174];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5011 in k5008 in k5005 in k5002 in k4999 in k4996 in k4993 in k4990 in k4987 in k4984 in k4981 in k4978 in k4975 in k4972 in k4969 in k4966 in k4963 in k4960 in k4957 in k4954 in k4951 in k4948 in ... */
static void C_ccall f_5013(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5013,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5016,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1097: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[170];
av2[3]=C_fix(2);
av2[4]=C_fix(3);
av2[5]=lf[172];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5014 in k5011 in k5008 in k5005 in k5002 in k4999 in k4996 in k4993 in k4990 in k4987 in k4984 in k4981 in k4978 in k4975 in k4972 in k4969 in k4966 in k4963 in k4960 in k4957 in k4954 in k4951 in ... */
static void C_ccall f_5016(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5016,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5019,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1098: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[170];
av2[3]=C_fix(2);
av2[4]=C_fix(3);
av2[5]=lf[171];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5017 in k5014 in k5011 in k5008 in k5005 in k5002 in k4999 in k4996 in k4993 in k4990 in k4987 in k4984 in k4981 in k4978 in k4975 in k4972 in k4969 in k4966 in k4963 in k4960 in k4957 in k4954 in ... */
static void C_ccall f_5019(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5019,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5022,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1099: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[167];
av2[3]=C_fix(2);
av2[4]=C_fix(3);
av2[5]=lf[169];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5020 in k5017 in k5014 in k5011 in k5008 in k5005 in k5002 in k4999 in k4996 in k4993 in k4990 in k4987 in k4984 in k4981 in k4978 in k4975 in k4972 in k4969 in k4966 in k4963 in k4960 in k4957 in ... */
static void C_ccall f_5022(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5022,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5025,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1100: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[167];
av2[3]=C_fix(2);
av2[4]=C_fix(3);
av2[5]=lf[168];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5023 in k5020 in k5017 in k5014 in k5011 in k5008 in k5005 in k5002 in k4999 in k4996 in k4993 in k4990 in k4987 in k4984 in k4981 in k4978 in k4975 in k4972 in k4969 in k4966 in k4963 in k4960 in ... */
static void C_ccall f_5025(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5025,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5028,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1101: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[164];
av2[3]=C_fix(2);
av2[4]=C_fix(3);
av2[5]=lf[166];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5026 in k5023 in k5020 in k5017 in k5014 in k5011 in k5008 in k5005 in k5002 in k4999 in k4996 in k4993 in k4990 in k4987 in k4984 in k4981 in k4978 in k4975 in k4972 in k4969 in k4966 in k4963 in ... */
static void C_ccall f_5028(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5028,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5031,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1102: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[164];
av2[3]=C_fix(2);
av2[4]=C_fix(3);
av2[5]=lf[165];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5029 in k5026 in k5023 in k5020 in k5017 in k5014 in k5011 in k5008 in k5005 in k5002 in k4999 in k4996 in k4993 in k4990 in k4987 in k4984 in k4981 in k4978 in k4975 in k4972 in k4969 in k4966 in ... */
static void C_ccall f_5031(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5031,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5034,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1103: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[161];
av2[3]=C_fix(2);
av2[4]=C_fix(3);
av2[5]=lf[163];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5032 in k5029 in k5026 in k5023 in k5020 in k5017 in k5014 in k5011 in k5008 in k5005 in k5002 in k4999 in k4996 in k4993 in k4990 in k4987 in k4984 in k4981 in k4978 in k4975 in k4972 in k4969 in ... */
static void C_ccall f_5034(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5034,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5037,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1104: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[161];
av2[3]=C_fix(2);
av2[4]=C_fix(3);
av2[5]=lf[162];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5035 in k5032 in k5029 in k5026 in k5023 in k5020 in k5017 in k5014 in k5011 in k5008 in k5005 in k5002 in k4999 in k4996 in k4993 in k4990 in k4987 in k4984 in k4981 in k4978 in k4975 in k4972 in ... */
static void C_ccall f_5037(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5037,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5040,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1105: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[158];
av2[3]=C_fix(2);
av2[4]=C_fix(3);
av2[5]=lf[160];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5038 in k5035 in k5032 in k5029 in k5026 in k5023 in k5020 in k5017 in k5014 in k5011 in k5008 in k5005 in k5002 in k4999 in k4996 in k4993 in k4990 in k4987 in k4984 in k4981 in k4978 in k4975 in ... */
static void C_ccall f_5040(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5040,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5043,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1106: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[158];
av2[3]=C_fix(2);
av2[4]=C_fix(3);
av2[5]=lf[159];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5041 in k5038 in k5035 in k5032 in k5029 in k5026 in k5023 in k5020 in k5017 in k5014 in k5011 in k5008 in k5005 in k5002 in k4999 in k4996 in k4993 in k4990 in k4987 in k4984 in k4981 in k4978 in ... */
static void C_ccall f_5043(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5043,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5046,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1107: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[155];
av2[3]=C_fix(2);
av2[4]=C_fix(3);
av2[5]=lf[157];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5044 in k5041 in k5038 in k5035 in k5032 in k5029 in k5026 in k5023 in k5020 in k5017 in k5014 in k5011 in k5008 in k5005 in k5002 in k4999 in k4996 in k4993 in k4990 in k4987 in k4984 in k4981 in ... */
static void C_ccall f_5046(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5046,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5049,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1108: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[155];
av2[3]=C_fix(2);
av2[4]=C_fix(3);
av2[5]=lf[156];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5047 in k5044 in k5041 in k5038 in k5035 in k5032 in k5029 in k5026 in k5023 in k5020 in k5017 in k5014 in k5011 in k5008 in k5005 in k5002 in k4999 in k4996 in k4993 in k4990 in k4987 in k4984 in ... */
static void C_ccall f_5049(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5049,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5052,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1110: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[152];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[154];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5050 in k5047 in k5044 in k5041 in k5038 in k5035 in k5032 in k5029 in k5026 in k5023 in k5020 in k5017 in k5014 in k5011 in k5008 in k5005 in k5002 in k4999 in k4996 in k4993 in k4990 in k4987 in ... */
static void C_ccall f_5052(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5052,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5055,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1111: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[152];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[153];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5053 in k5050 in k5047 in k5044 in k5041 in k5038 in k5035 in k5032 in k5029 in k5026 in k5023 in k5020 in k5017 in k5014 in k5011 in k5008 in k5005 in k5002 in k4999 in k4996 in k4993 in k4990 in ... */
static void C_ccall f_5055(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5055,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5058,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1112: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[149];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[151];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5056 in k5053 in k5050 in k5047 in k5044 in k5041 in k5038 in k5035 in k5032 in k5029 in k5026 in k5023 in k5020 in k5017 in k5014 in k5011 in k5008 in k5005 in k5002 in k4999 in k4996 in k4993 in ... */
static void C_ccall f_5058(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5058,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5061,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1113: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[149];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[150];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5059 in k5056 in k5053 in k5050 in k5047 in k5044 in k5041 in k5038 in k5035 in k5032 in k5029 in k5026 in k5023 in k5020 in k5017 in k5014 in k5011 in k5008 in k5005 in k5002 in k4999 in k4996 in ... */
static void C_ccall f_5061(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5061,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5064,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1114: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[146];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[148];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5062 in k5059 in k5056 in k5053 in k5050 in k5047 in k5044 in k5041 in k5038 in k5035 in k5032 in k5029 in k5026 in k5023 in k5020 in k5017 in k5014 in k5011 in k5008 in k5005 in k5002 in k4999 in ... */
static void C_ccall f_5064(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5064,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5067,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1115: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[146];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[147];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5065 in k5062 in k5059 in k5056 in k5053 in k5050 in k5047 in k5044 in k5041 in k5038 in k5035 in k5032 in k5029 in k5026 in k5023 in k5020 in k5017 in k5014 in k5011 in k5008 in k5005 in k5002 in ... */
static void C_ccall f_5067(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5067,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5070,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1116: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[143];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[145];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5068 in k5065 in k5062 in k5059 in k5056 in k5053 in k5050 in k5047 in k5044 in k5041 in k5038 in k5035 in k5032 in k5029 in k5026 in k5023 in k5020 in k5017 in k5014 in k5011 in k5008 in k5005 in ... */
static void C_ccall f_5070(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5070,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5073,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1117: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[143];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[144];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5071 in k5068 in k5065 in k5062 in k5059 in k5056 in k5053 in k5050 in k5047 in k5044 in k5041 in k5038 in k5035 in k5032 in k5029 in k5026 in k5023 in k5020 in k5017 in k5014 in k5011 in k5008 in ... */
static void C_ccall f_5073(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5073,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5076,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1118: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[140];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[142];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5074 in k5071 in k5068 in k5065 in k5062 in k5059 in k5056 in k5053 in k5050 in k5047 in k5044 in k5041 in k5038 in k5035 in k5032 in k5029 in k5026 in k5023 in k5020 in k5017 in k5014 in k5011 in ... */
static void C_ccall f_5076(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5076,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5079,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1119: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[140];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[141];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5077 in k5074 in k5071 in k5068 in k5065 in k5062 in k5059 in k5056 in k5053 in k5050 in k5047 in k5044 in k5041 in k5038 in k5035 in k5032 in k5029 in k5026 in k5023 in k5020 in k5017 in k5014 in ... */
static void C_ccall f_5079(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5079,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5082,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1120: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[137];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[139];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5080 in k5077 in k5074 in k5071 in k5068 in k5065 in k5062 in k5059 in k5056 in k5053 in k5050 in k5047 in k5044 in k5041 in k5038 in k5035 in k5032 in k5029 in k5026 in k5023 in k5020 in k5017 in ... */
static void C_ccall f_5082(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5082,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5085,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1121: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[137];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[138];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5083 in k5080 in k5077 in k5074 in k5071 in k5068 in k5065 in k5062 in k5059 in k5056 in k5053 in k5050 in k5047 in k5044 in k5041 in k5038 in k5035 in k5032 in k5029 in k5026 in k5023 in k5020 in ... */
static void C_ccall f_5085(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5085,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5088,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1122: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[134];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[136];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5086 in k5083 in k5080 in k5077 in k5074 in k5071 in k5068 in k5065 in k5062 in k5059 in k5056 in k5053 in k5050 in k5047 in k5044 in k5041 in k5038 in k5035 in k5032 in k5029 in k5026 in k5023 in ... */
static void C_ccall f_5088(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5088,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5091,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1123: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[134];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[135];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5089 in k5086 in k5083 in k5080 in k5077 in k5074 in k5071 in k5068 in k5065 in k5062 in k5059 in k5056 in k5053 in k5050 in k5047 in k5044 in k5041 in k5038 in k5035 in k5032 in k5029 in k5026 in ... */
static void C_ccall f_5091(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5091,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5094,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1124: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[131];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[133];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5092 in k5089 in k5086 in k5083 in k5080 in k5077 in k5074 in k5071 in k5068 in k5065 in k5062 in k5059 in k5056 in k5053 in k5050 in k5047 in k5044 in k5041 in k5038 in k5035 in k5032 in k5029 in ... */
static void C_ccall f_5094(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5094,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5097,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1125: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[131];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[132];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5095 in k5092 in k5089 in k5086 in k5083 in k5080 in k5077 in k5074 in k5071 in k5068 in k5065 in k5062 in k5059 in k5056 in k5053 in k5050 in k5047 in k5044 in k5041 in k5038 in k5035 in k5032 in ... */
static void C_ccall f_5097(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5097,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5100,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1126: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[128];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[130];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5098 in k5095 in k5092 in k5089 in k5086 in k5083 in k5080 in k5077 in k5074 in k5071 in k5068 in k5065 in k5062 in k5059 in k5056 in k5053 in k5050 in k5047 in k5044 in k5041 in k5038 in k5035 in ... */
static void C_ccall f_5100(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5100,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5103,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1127: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[128];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[129];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5101 in k5098 in k5095 in k5092 in k5089 in k5086 in k5083 in k5080 in k5077 in k5074 in k5071 in k5068 in k5065 in k5062 in k5059 in k5056 in k5053 in k5050 in k5047 in k5044 in k5041 in k5038 in ... */
static void C_ccall f_5103(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5103,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5106,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1128: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[125];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[127];
av2[6]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5104 in k5101 in k5098 in k5095 in k5092 in k5089 in k5086 in k5083 in k5080 in k5077 in k5074 in k5071 in k5068 in k5065 in k5062 in k5059 in k5056 in k5053 in k5050 in k5047 in k5044 in k5041 in ... */
static void C_ccall f_5106(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5106,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5109,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1129: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[125];
av2[3]=C_fix(2);
av2[4]=C_fix(1);
av2[5]=lf[126];
av2[6]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5107 in k5104 in k5101 in k5098 in k5095 in k5092 in k5089 in k5086 in k5083 in k5080 in k5077 in k5074 in k5071 in k5068 in k5065 in k5062 in k5059 in k5056 in k5053 in k5050 in k5047 in k5044 in ... */
static void C_ccall f_5109(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_5109,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5112,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1131: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[123];
av2[3]=C_fix(17);
av2[4]=C_fix(1);
av2[5]=lf[124];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k5110 in k5107 in k5104 in k5101 in k5098 in k5095 in k5092 in k5089 in k5086 in k5083 in k5080 in k5077 in k5074 in k5071 in k5068 in k5065 in k5062 in k5059 in k5056 in k5053 in k5050 in k5047 in ... */
static void C_ccall f_5112(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_5112,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5115,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1133: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[121];
av2[3]=C_fix(7);
av2[4]=C_fix(1);
av2[5]=lf[122];
av2[6]=C_fix(1);
av2[7]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in k5095 in k5092 in k5089 in k5086 in k5083 in k5080 in k5077 in k5074 in k5071 in k5068 in k5065 in k5062 in k5059 in k5056 in k5053 in k5050 in ... */
static void C_ccall f_5115(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_5115,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5118,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1134: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[119];
av2[3]=C_fix(7);
av2[4]=C_fix(1);
av2[5]=lf[120];
av2[6]=C_fix(1);
av2[7]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in k5095 in k5092 in k5089 in k5086 in k5083 in k5080 in k5077 in k5074 in k5071 in k5068 in k5065 in k5062 in k5059 in k5056 in k5053 in ... */
static void C_ccall f_5118(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_5118,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5121,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1135: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[117];
av2[3]=C_fix(7);
av2[4]=C_fix(1);
av2[5]=lf[118];
av2[6]=C_fix(1);
av2[7]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in k5095 in k5092 in k5089 in k5086 in k5083 in k5080 in k5077 in k5074 in k5071 in k5068 in k5065 in k5062 in k5059 in k5056 in ... */
static void C_ccall f_5121(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_5121,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5124,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1136: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[115];
av2[3]=C_fix(7);
av2[4]=C_fix(1);
av2[5]=lf[116];
av2[6]=C_fix(1);
av2[7]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in k5095 in k5092 in k5089 in k5086 in k5083 in k5080 in k5077 in k5074 in k5071 in k5068 in k5065 in k5062 in k5059 in ... */
static void C_ccall f_5124(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_5124,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5127,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1137: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[113];
av2[3]=C_fix(7);
av2[4]=C_fix(1);
av2[5]=lf[114];
av2[6]=C_fix(1);
av2[7]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in k5095 in k5092 in k5089 in k5086 in k5083 in k5080 in k5077 in k5074 in k5071 in k5068 in k5065 in k5062 in ... */
static void C_ccall f_5127(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_5127,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5130,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1138: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[111];
av2[3]=C_fix(7);
av2[4]=C_fix(1);
av2[5]=lf[112];
av2[6]=C_fix(1);
av2[7]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in k5095 in k5092 in k5089 in k5086 in k5083 in k5080 in k5077 in k5074 in k5071 in k5068 in k5065 in ... */
static void C_ccall f_5130(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_5130,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5133,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1139: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[109];
av2[3]=C_fix(7);
av2[4]=C_fix(1);
av2[5]=lf[110];
av2[6]=C_fix(1);
av2[7]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in k5095 in k5092 in k5089 in k5086 in k5083 in k5080 in k5077 in k5074 in k5071 in k5068 in ... */
static void C_ccall f_5133(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_5133,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5136,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1140: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[107];
av2[3]=C_fix(7);
av2[4]=C_fix(1);
av2[5]=lf[108];
av2[6]=C_fix(1);
av2[7]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in k5095 in k5092 in k5089 in k5086 in k5083 in k5080 in k5077 in k5074 in k5071 in ... */
static void C_ccall f_5136(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_5136,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5139,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1141: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[105];
av2[3]=C_fix(7);
av2[4]=C_fix(1);
av2[5]=lf[106];
av2[6]=C_fix(1);
av2[7]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in k5095 in k5092 in k5089 in k5086 in k5083 in k5080 in k5077 in k5074 in ... */
static void C_ccall f_5139(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_5139,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5142,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1142: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[103];
av2[3]=C_fix(7);
av2[4]=C_fix(1);
av2[5]=lf[104];
av2[6]=C_fix(1);
av2[7]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in k5095 in k5092 in k5089 in k5086 in k5083 in k5080 in k5077 in ... */
static void C_ccall f_5142(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,6)))){
C_save_and_reclaim((void *)f_5142,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_5144,tmp=(C_word)a,a+=2,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5249,a[2]=((C_word*)t0)[2],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:1170: chicken.compiler.optimizer#rewrite */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[102];
av2[3]=C_fix(8);
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* rewrite-make-vector in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in k5095 in k5092 in k5089 in k5086 in k5083 in k5080 in ... */
static void C_ccall f_5144(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_5144,c,av);}
a=C_alloc(6);
t6=C_i_length(t5);
if(C_truep(C_i_pairp(t5))){
t7=C_i_car(t5);
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5246,a[2]=t1,a[3]=t4,a[4]=t5,a[5]=t7,tmp=(C_word)a,a+=6,tmp);
/* c-platform.scm:1151: chicken.compiler.support#node-class */
t9=*((C_word*)lf[34]+1);{
C_word *av2=av;
av2[0]=t9;
av2[1]=t8;
av2[2]=t7;
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}
else{
t7=t1;{
C_word *av2=av;
av2[0]=t7;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}}

/* k5164 in k5244 in rewrite-make-vector in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in k5095 in k5092 in k5089 in k5086 in ... */
static void C_ccall f_5166(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_5166,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5242,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* c-platform.scm:1153: chicken.compiler.support#node-parameters */
t3=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k5179 in k5240 in k5164 in k5244 in rewrite-make-vector in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in k5095 in k5092 in ... */
static void C_ccall f_5181(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_5181,c,av);}
a=C_alloc(6);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5184,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_i_pairp(C_u_i_cdr(((C_word*)t0)[6])))){
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_cadr(((C_word*)t0)[6]);
f_5184(2,av2);}}
else{
/* c-platform.scm:1158: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[62];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k5182 in k5179 in k5240 in k5164 in k5244 in rewrite-make-vector in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in k5095 in ... */
static void C_ccall f_5184(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(41,c,3)))){
C_save_and_reclaim((void *)f_5184,c,av);}
a=C_alloc(41);
t2=C_a_i_list1(&a,1,((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5199,a[2]=t1,a[3]=((C_word*)t0)[3],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t4=C_a_i_list1(&a,1,C_SCHEME_TRUE);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5211,a[2]=((C_word*)t0)[4],a[3]=t3,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
t6=C_a_i_fixnum_plus(&a,2,((C_word*)t0)[5],C_fix(1));
t7=C_a_i_list2(&a,2,lf[61],t6);
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5219,a[2]=t5,a[3]=t7,tmp=(C_word)a,a+=4,tmp);
t9=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5221,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t10=C_SCHEME_UNDEFINED;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_set_block_item(t11,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2701,a[2]=((C_word*)t0)[5],a[3]=t11,a[4]=t9,tmp=(C_word)a,a+=5,tmp));
t13=((C_word*)t11)[1];
f_2701(t13,t8,C_fix(0));}

/* k5197 in k5182 in k5179 in k5240 in k5164 in k5244 in rewrite-make-vector in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in ... */
static void C_ccall f_5199(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_5199,c,av);}
a=C_alloc(6);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
/* c-platform.scm:1159: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[48];
av2[3]=((C_word*)t0)[4];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k5209 in k5182 in k5179 in k5240 in k5164 in k5244 in rewrite-make-vector in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in ... */
static void C_ccall f_5211(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_5211,c,av);}
a=C_alloc(6);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
/* c-platform.scm:1163: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[23];
av2[3]=((C_word*)t0)[4];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k5217 in k5182 in k5179 in k5240 in k5164 in k5244 in rewrite-make-vector in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in ... */
static void C_ccall f_5219(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_5219,c,av);}
/* c-platform.scm:1166: chicken.compiler.support#make-node */
t2=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[28];
av2[3]=((C_word*)t0)[3];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a5220 in k5182 in k5179 in k5240 in k5164 in k5244 in rewrite-make-vector in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in ... */
static void C_fcall f_5221(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,2)))){
C_save_and_reclaim_args((void *)trf_5221,2,t0,t1);}
/* c-platform.scm:1169: chicken.compiler.support#varnode */
t2=*((C_word*)lf[49]+1);{
C_word av2[3];
av2[0]=t2;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k5240 in k5164 in k5244 in rewrite-make-vector in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in k5095 in k5092 in k5089 in ... */
static void C_ccall f_5242(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,4)))){
C_save_and_reclaim((void *)f_5242,c,av);}
a=C_alloc(7);
t2=C_i_car(t1);
if(C_truep(C_fixnump(t2))){
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5181,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t2,a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* c-platform.scm:1155: scheme#<= */{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=t3;
av2[2]=C_fix(0);
av2[3]=t2;
av2[4]=C_fix(32);
C_less_or_equal_p(5,av2);}}
else{
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k5244 in rewrite-make-vector in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in k5095 in k5092 in k5089 in k5086 in k5083 in ... */
static void C_ccall f_5246(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_5246,c,av);}
a=C_alloc(6);
t2=C_eqp(lf[31],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5166,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* c-platform.scm:1152: chicken.base#gensym */
t4=*((C_word*)lf[50]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in k5095 in k5092 in k5089 in k5086 in k5083 in k5080 in ... */
static void C_ccall f_5249(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_5249,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5252,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1171: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[101];
av2[3]=C_fix(8);
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in k5095 in k5092 in k5089 in k5086 in k5083 in ... */
static void C_ccall f_5252(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,6)))){
C_save_and_reclaim((void *)f_5252,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_5254,tmp=(C_word)a,a+=2,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5376,a[2]=((C_word*)t0)[2],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:1193: chicken.compiler.optimizer#rewrite */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[45];
av2[3]=C_fix(8);
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* rewrite-call/cc in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in k5095 in k5092 in k5089 in k5086 in ... */
static void C_ccall f_5254(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_5254,c,av);}
a=C_alloc(6);
t6=C_i_length(t5);
t7=C_eqp(C_fix(1),t6);
if(C_truep(t7)){
t8=C_i_car(t5);
t9=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5368,a[2]=t8,a[3]=t4,a[4]=t2,a[5]=t1,tmp=(C_word)a,a+=6,tmp);
/* c-platform.scm:1178: chicken.compiler.support#node-class */
t10=*((C_word*)lf[34]+1);{
C_word *av2=av;
av2[0]=t10;
av2[1]=t9;
av2[2]=t8;
((C_proc)(void*)(*((C_word*)t10+1)))(3,av2);}}
else{
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}}

/* k5271 in k5366 in rewrite-call/cc in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in k5095 in k5092 in ... */
static void C_ccall f_5273(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_5273,c,av);}
a=C_alloc(7);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5356,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,tmp=(C_word)a,a+=7,tmp);
/* c-platform.scm:1180: chicken.compiler.support#node-class */
t3=*((C_word*)lf[34]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t2=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* a5289 in k5350 in k5354 in k5271 in k5366 in rewrite-call/cc in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in ... */
static void C_ccall f_5290(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,4)))){
C_save_and_reclaim((void *)f_5290,c,av);}
a=C_alloc(7);
if(C_truep(C_i_nequalp(t3,C_fix(2)))){
t5=(C_truep(t4)?t4:C_i_cadr(((C_word*)t0)[2]));
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5345,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t1,a[5]=((C_word*)t0)[5],a[6]=t5,tmp=(C_word)a,a+=7,tmp);
/* c-platform.scm:1187: chicken.compiler.support#db-get */
t7=*((C_word*)lf[56]+1);{
C_word *av2=av;
av2[0]=t7;
av2[1]=t6;
av2[2]=((C_word*)t0)[5];
av2[3]=t5;
av2[4]=lf[65];
((C_proc)(void*)(*((C_word*)t7+1)))(5,av2);}}
else{
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* k5331 in k5335 in k5339 in k5343 in a5289 in k5350 in k5354 in k5271 in k5366 in rewrite-call/cc in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in ... */
static void C_ccall f_5333(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_5333,c,av);}
a=C_alloc(9);
t2=C_a_i_list3(&a,3,((C_word*)t0)[2],((C_word*)t0)[3],t1);
/* c-platform.scm:1190: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[4];
av2[2]=lf[23];
av2[3]=((C_word*)t0)[5];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k5335 in k5339 in k5343 in a5289 in k5350 in k5354 in k5271 in k5366 in rewrite-call/cc in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in ... */
static void C_ccall f_5337(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_5337,c,av);}
a=C_alloc(9);
if(C_truep(C_i_not(t1))){
t2=C_a_i_list1(&a,1,C_SCHEME_TRUE);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5333,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* c-platform.scm:1192: chicken.compiler.support#qnode */
t4=*((C_word*)lf[29]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t2=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k5339 in k5343 in a5289 in k5350 in k5354 in k5271 in k5366 in rewrite-call/cc in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in ... */
static void C_ccall f_5341(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_5341,c,av);}
a=C_alloc(5);
if(C_truep(C_i_not(t1))){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5337,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* c-platform.scm:1189: chicken.compiler.support#db-get */
t3=*((C_word*)lf[56]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=((C_word*)t0)[6];
av2[4]=lf[63];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}
else{
t2=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k5343 in a5289 in k5350 in k5354 in k5271 in k5366 in rewrite-call/cc in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in ... */
static void C_ccall f_5345(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,4)))){
C_save_and_reclaim((void *)f_5345,c,av);}
a=C_alloc(7);
if(C_truep(C_i_not(t1))){
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5341,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* c-platform.scm:1188: chicken.compiler.support#db-get */
t3=*((C_word*)lf[56]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=((C_word*)t0)[6];
av2[4]=lf[64];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}
else{
t2=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k5350 in k5354 in k5271 in k5366 in rewrite-call/cc in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in ... */
static void C_ccall f_5352(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,5)))){
C_save_and_reclaim((void *)f_5352,c,av);}
a=C_alloc(6);
t2=C_i_caddr(t1);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5290,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* c-platform.scm:1182: ##sys#decompose-lambda-list */
t4=*((C_word*)lf[66]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[5];
av2[2]=t2;
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k5354 in k5271 in k5366 in rewrite-call/cc in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in k5095 in ... */
static void C_ccall f_5356(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_5356,c,av);}
a=C_alloc(6);
t2=C_eqp(lf[47],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5352,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* c-platform.scm:1181: chicken.compiler.support#node-parameters */
t4=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k5362 in k5366 in rewrite-call/cc in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in k5095 in k5092 in ... */
static void C_ccall f_5364(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_5364,c,av);}
/* c-platform.scm:1179: chicken.compiler.support#db-get */
t2=*((C_word*)lf[56]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=C_i_car(t1);
av2[4]=lf[57];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k5366 in rewrite-call/cc in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in k5095 in k5092 in k5089 in ... */
static void C_ccall f_5368(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_5368,c,av);}
a=C_alloc(10);
t2=C_eqp(lf[35],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5273,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5364,a[2]=t3,a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:1179: chicken.compiler.support#node-parameters */
t5=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}
else{
t3=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k5374 in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in k5095 in k5092 in k5089 in k5086 in ... */
static void C_ccall f_5376(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_5376,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5379,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1194: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[100];
av2[3]=C_fix(8);
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k5377 in k5374 in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in k5095 in k5092 in k5089 in ... */
static void C_ccall f_5379(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,6)))){
C_save_and_reclaim((void *)f_5379,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5383,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_5510,tmp=(C_word)a,a+=2,tmp);
/* c-platform.scm:1222: chicken.compiler.optimizer#rewrite */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=lf[99];
av2[3]=C_fix(8);
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k5381 in k5377 in k5374 in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in k5095 in k5092 in ... */
static void C_ccall f_5383(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_5383,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5386,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1236: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[97];
av2[3]=C_fix(3);
av2[4]=lf[96];
av2[5]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k5384 in k5381 in k5377 in k5374 in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in k5095 in ... */
static void C_ccall f_5386(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_5386,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5389,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1237: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[95];
av2[3]=C_fix(3);
av2[4]=lf[96];
av2[5]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k5387 in k5384 in k5381 in k5377 in k5374 in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in ... */
static void C_ccall f_5389(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_5389,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5392,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1238: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[94];
av2[3]=C_fix(3);
av2[4]=lf[84];
av2[5]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k5390 in k5387 in k5384 in k5381 in k5377 in k5374 in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in ... */
static void C_ccall f_5392(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_5392,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5395,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1239: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[93];
av2[3]=C_fix(3);
av2[4]=lf[81];
av2[5]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k5393 in k5390 in k5387 in k5384 in k5381 in k5377 in k5374 in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in ... */
static void C_ccall f_5395(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_5395,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5398,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1240: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[91];
av2[3]=C_fix(3);
av2[4]=lf[92];
av2[5]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k5396 in k5393 in k5390 in k5387 in k5384 in k5381 in k5377 in k5374 in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in ... */
static void C_ccall f_5398(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,6)))){
C_save_and_reclaim((void *)f_5398,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5401,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_5470,tmp=(C_word)a,a+=2,tmp);
/* c-platform.scm:1242: chicken.compiler.optimizer#rewrite */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=lf[90];
av2[3]=C_fix(8);
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k5399 in k5396 in k5393 in k5390 in k5387 in k5384 in k5381 in k5377 in k5374 in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in ... */
static void C_ccall f_5401(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,6)))){
C_save_and_reclaim((void *)f_5401,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5404,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_5430,tmp=(C_word)a,a+=2,tmp);
/* c-platform.scm:1254: chicken.compiler.optimizer#rewrite */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=lf[87];
av2[3]=C_fix(8);
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k5402 in k5399 in k5396 in k5393 in k5390 in k5387 in k5384 in k5381 in k5377 in k5374 in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in ... */
static void C_ccall f_5404(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5404,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5407,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1266: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[82];
av2[3]=C_fix(23);
av2[4]=C_fix(0);
av2[5]=lf[83];
av2[6]=lf[84];
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5405 in k5402 in k5399 in k5396 in k5393 in k5390 in k5387 in k5384 in k5381 in k5377 in k5374 in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in ... */
static void C_ccall f_5407(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5407,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5410,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1267: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[79];
av2[3]=C_fix(23);
av2[4]=C_fix(1);
av2[5]=lf[80];
av2[6]=lf[81];
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5408 in k5405 in k5402 in k5399 in k5396 in k5393 in k5390 in k5387 in k5384 in k5381 in k5377 in k5374 in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in ... */
static void C_ccall f_5410(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,8)))){
C_save_and_reclaim((void *)f_5410,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5413,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1268: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 9) {
  av2=av;
} else {
  av2=C_alloc(9);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[77];
av2[3]=C_fix(23);
av2[4]=C_fix(2);
av2[5]=lf[78];
av2[6]=C_fix(0);
av2[7]=C_fix(0);
av2[8]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(9,av2);}}

/* k5411 in k5408 in k5405 in k5402 in k5399 in k5396 in k5393 in k5390 in k5387 in k5384 in k5381 in k5377 in k5374 in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in ... */
static void C_ccall f_5413(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,8)))){
C_save_and_reclaim((void *)f_5413,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5416,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1269: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 9) {
  av2=av;
} else {
  av2=C_alloc(9);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[75];
av2[3]=C_fix(23);
av2[4]=C_fix(2);
av2[5]=lf[76];
av2[6]=C_fix(0);
av2[7]=C_fix(0);
av2[8]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(9,av2);}}

/* k5414 in k5411 in k5408 in k5405 in k5402 in k5399 in k5396 in k5393 in k5390 in k5387 in k5384 in k5381 in k5377 in k5374 in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in ... */
static void C_ccall f_5416(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5416,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5419,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1270: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[73];
av2[3]=C_fix(23);
av2[4]=C_fix(2);
av2[5]=lf[74];
av2[6]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5417 in k5414 in k5411 in k5408 in k5405 in k5402 in k5399 in k5396 in k5393 in k5390 in k5387 in k5384 in k5381 in k5377 in k5374 in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in ... */
static void C_ccall f_5419(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_5419,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5422,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1271: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[71];
av2[3]=C_fix(23);
av2[4]=C_fix(2);
av2[5]=lf[72];
av2[6]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}

/* k5420 in k5417 in k5414 in k5411 in k5408 in k5405 in k5402 in k5399 in k5396 in k5393 in k5390 in k5387 in k5384 in k5381 in k5377 in k5374 in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in ... */
static void C_ccall f_5422(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_5422,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5425,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1273: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[69];
av2[3]=C_fix(7);
av2[4]=C_fix(2);
av2[5]=lf[70];
av2[6]=C_SCHEME_FALSE;
av2[7]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k5423 in k5420 in k5417 in k5414 in k5411 in k5408 in k5405 in k5402 in k5399 in k5396 in k5393 in k5390 in k5387 in k5384 in k5381 in k5377 in k5374 in k5250 in k5247 in k5140 in k5137 in k5134 in ... */
static void C_ccall f_5425(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_5425,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5428,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1274: chicken.compiler.optimizer#rewrite */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[67];
av2[3]=C_fix(7);
av2[4]=C_fix(2);
av2[5]=lf[68];
av2[6]=C_SCHEME_FALSE;
av2[7]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k5426 in k5423 in k5420 in k5417 in k5414 in k5411 in k5408 in k5405 in k5402 in k5399 in k5396 in k5393 in k5390 in k5387 in k5384 in k5381 in k5377 in k5374 in k5250 in k5247 in k5140 in k5137 in ... */
static void C_ccall f_5428(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5428,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a5429 in k5399 in k5396 in k5393 in k5390 in k5387 in k5384 in k5381 in k5377 in k5374 in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in ... */
static void C_ccall f_5430(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,4)))){
C_save_and_reclaim((void *)f_5430,c,av);}
a=C_alloc(11);
t6=C_i_length(t5);
t7=C_eqp(C_fix(1),t6);
if(C_truep(t7)){
t8=C_a_i_list1(&a,1,C_SCHEME_TRUE);
t9=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5452,a[2]=t4,a[3]=t1,a[4]=t8,tmp=(C_word)a,a+=5,tmp);
t10=C_eqp(*((C_word*)lf[25]+1),lf[24]);
t11=(C_truep(t10)?C_a_i_list1(&a,1,lf[85]):C_a_i_list1(&a,1,lf[86]));
/* c-platform.scm:1261: chicken.compiler.support#make-node */
t12=*((C_word*)lf[22]+1);{
C_word *av2=av;
av2[0]=t12;
av2[1]=t9;
av2[2]=lf[27];
av2[3]=t11;
av2[4]=t5;
((C_proc)(void*)(*((C_word*)t12+1)))(5,av2);}}
else{
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}}

/* k5450 in a5429 in k5399 in k5396 in k5393 in k5390 in k5387 in k5384 in k5381 in k5377 in k5374 in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in ... */
static void C_ccall f_5452(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_5452,c,av);}
a=C_alloc(6);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
/* c-platform.scm:1258: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[23];
av2[3]=((C_word*)t0)[4];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* a5469 in k5396 in k5393 in k5390 in k5387 in k5384 in k5381 in k5377 in k5374 in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in ... */
static void C_ccall f_5470(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,4)))){
C_save_and_reclaim((void *)f_5470,c,av);}
a=C_alloc(11);
t6=C_i_length(t5);
t7=C_eqp(C_fix(2),t6);
if(C_truep(t7)){
t8=C_a_i_list1(&a,1,C_SCHEME_TRUE);
t9=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5492,a[2]=t4,a[3]=t1,a[4]=t8,tmp=(C_word)a,a+=5,tmp);
t10=C_eqp(*((C_word*)lf[25]+1),lf[24]);
t11=(C_truep(t10)?C_a_i_list1(&a,1,lf[88]):C_a_i_list1(&a,1,lf[89]));
/* c-platform.scm:1249: chicken.compiler.support#make-node */
t12=*((C_word*)lf[22]+1);{
C_word *av2=av;
av2[0]=t12;
av2[1]=t9;
av2[2]=lf[27];
av2[3]=t11;
av2[4]=t5;
((C_proc)(void*)(*((C_word*)t12+1)))(5,av2);}}
else{
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}}

/* k5490 in a5469 in k5396 in k5393 in k5390 in k5387 in k5384 in k5381 in k5377 in k5374 in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in ... */
static void C_ccall f_5492(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_5492,c,av);}
a=C_alloc(6);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
/* c-platform.scm:1246: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[23];
av2[3]=((C_word*)t0)[4];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* a5509 in k5377 in k5374 in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in k5095 in k5092 in ... */
static void C_ccall f_5510(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_5510,c,av);}
a=C_alloc(5);
t6=C_i_length(t5);
t7=C_eqp(C_fix(1),t6);
if(C_truep(t7)){
t8=C_i_car(t5);
t9=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5568,a[2]=t4,a[3]=t1,a[4]=t8,tmp=(C_word)a,a+=5,tmp);
/* c-platform.scm:1228: chicken.compiler.support#node-class */
t10=*((C_word*)lf[34]+1);{
C_word *av2=av;
av2[0]=t10;
av2[1]=t9;
av2[2]=t8;
((C_proc)(void*)(*((C_word*)t10+1)))(3,av2);}}
else{
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}}

/* k5533 in k5562 in k5566 in a5509 in k5377 in k5374 in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in ... */
static void C_ccall f_5535(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_5535,c,av);}
a=C_alloc(8);
if(C_truep(t1)){
t2=C_i_assq(((C_word*)t0)[2],lf[98]);
if(C_truep(t2)){
t3=C_a_i_list1(&a,1,C_SCHEME_TRUE);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5556,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* c-platform.scm:1234: chicken.compiler.support#varnode */
t5=*((C_word*)lf[49]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=C_i_cdr(t2);
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}
else{
t3=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}
else{
t2=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k5554 in k5533 in k5562 in k5566 in a5509 in k5377 in k5374 in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in ... */
static void C_ccall f_5556(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_5556,c,av);}
a=C_alloc(6);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
/* c-platform.scm:1232: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[23];
av2[3]=((C_word*)t0)[4];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k5562 in k5566 in a5509 in k5377 in k5374 in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in ... */
static void C_ccall f_5564(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_5564,c,av);}
a=C_alloc(5);
t2=C_i_car(t1);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5535,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* c-platform.scm:1230: chicken.compiler.support#intrinsic? */
t4=*((C_word*)lf[44]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k5566 in a5509 in k5377 in k5374 in k5250 in k5247 in k5140 in k5137 in k5134 in k5131 in k5128 in k5125 in k5122 in k5119 in k5116 in k5113 in k5110 in k5107 in k5104 in k5101 in k5098 in k5095 in ... */
static void C_ccall f_5568(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_5568,c,av);}
a=C_alloc(4);
t2=C_eqp(lf[35],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5564,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:1229: chicken.compiler.support#node-parameters */
t4=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* a5574 in k4867 in k4864 in k4861 in k4858 in k4855 in k4852 in k4849 in k4846 in k4843 in k4840 in k4837 in k4834 in k4831 in k4828 in k4825 in k4822 in k4819 in k4816 in k4813 in k4810 in k4807 in ... */
static void C_ccall f_5575(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_5575,c,av);}
a=C_alloc(10);
t6=C_i_length(t5);
t7=C_eqp(C_fix(2),t6);
if(C_truep(t7)){
t8=C_i_cadr(t5);
t9=C_a_i_list1(&a,1,C_SCHEME_TRUE);
t10=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5690,a[2]=t4,a[3]=t1,a[4]=t9,a[5]=t5,a[6]=t8,tmp=(C_word)a,a+=7,tmp);
/* c-platform.scm:1026: chicken.compiler.support#node-class */
t11=*((C_word*)lf[34]+1);{
C_word *av2=av;
av2[0]=t11;
av2[1]=t10;
av2[2]=t8;
((C_proc)(void*)(*((C_word*)t11+1)))(3,av2);}}
else{
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}}

/* k5601 in k5688 in a5574 in k4867 in k4864 in k4861 in k4858 in k4855 in k4852 in k4849 in k4846 in k4843 in k4840 in k4837 in k4834 in k4831 in k4828 in k4825 in k4822 in k4819 in k4816 in k4813 in ... */
static void C_ccall f_5603(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,4)))){
C_save_and_reclaim((void *)f_5603,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5606,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
if(C_truep(t1)){
t3=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
/* c-platform.scm:1023: chicken.compiler.support#make-node */
t4=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[23];
av2[3]=((C_word*)t0)[4];
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}
else{
t3=C_eqp(*((C_word*)lf[25]+1),lf[24]);
if(C_truep(t3)){
/* c-platform.scm:1038: chicken.compiler.support#make-node */
t4=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=lf[27];
av2[3]=lf[259];
av2[4]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}
else{
t4=C_a_i_list2(&a,2,lf[260],C_fix(5));
/* c-platform.scm:1040: chicken.compiler.support#make-node */
t5=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=lf[28];
av2[3]=t4;
av2[4]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}}}

/* k5604 in k5601 in k5688 in a5574 in k4867 in k4864 in k4861 in k4858 in k4855 in k4852 in k4849 in k4846 in k4843 in k4840 in k4837 in k4834 in k4831 in k4828 in k4825 in k4822 in k4819 in k4816 in ... */
static void C_ccall f_5606(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_5606,c,av);}
a=C_alloc(6);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
/* c-platform.scm:1023: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[23];
av2[3]=((C_word*)t0)[4];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k5635 in k5684 in k5688 in a5574 in k4867 in k4864 in k4861 in k4858 in k4855 in k4852 in k4849 in k4846 in k4843 in k4840 in k4837 in k4834 in k4831 in k4828 in k4825 in k4822 in k4819 in k4816 in ... */
static void C_fcall f_5637(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(33,0,4)))){
C_save_and_reclaim_args((void *)trf_5637,2,t0,t1);}
a=C_alloc(33);
if(C_truep(t1)){
if(C_truep(C_i_negativep(((C_word*)t0)[2]))){
t2=C_i_car(((C_word*)t0)[3]);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5659,a[2]=t2,a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
t4=C_s_a_i_negate(&a,1,((C_word*)t0)[2]);
/* c-platform.scm:1033: chicken.compiler.support#qnode */
t5=*((C_word*)lf[29]+1);{
C_word av2[3];
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}
else{
t2=C_i_car(((C_word*)t0)[3]);
t3=C_a_i_list2(&a,2,t2,((C_word*)t0)[5]);
/* c-platform.scm:1034: chicken.compiler.support#make-node */
t4=*((C_word*)lf[22]+1);{
C_word av2[5];
av2[0]=t4;
av2[1]=((C_word*)t0)[4];
av2[2]=lf[27];
av2[3]=lf[262];
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}}
else{
t2=((C_word*)t0)[4];{
C_word av2[2];
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
f_5603(2,av2);}}}

/* k5657 in k5635 in k5684 in k5688 in a5574 in k4867 in k4864 in k4861 in k4858 in k4855 in k4852 in k4849 in k4846 in k4843 in k4840 in k4837 in k4834 in k4831 in k4828 in k4825 in k4822 in k4819 in ... */
static void C_ccall f_5659(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_5659,c,av);}
a=C_alloc(6);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
/* c-platform.scm:1031: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[27];
av2[3]=lf[261];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k5680 in k5684 in k5688 in a5574 in k4867 in k4864 in k4861 in k4858 in k4855 in k4852 in k4849 in k4846 in k4843 in k4840 in k4837 in k4834 in k4831 in k4828 in k4825 in k4822 in k4819 in k4816 in ... */
static void C_ccall f_5682(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5682,c,av);}
t2=((C_word*)t0)[2];
f_5637(t2,C_i_not(t1));}

/* k5684 in k5688 in a5574 in k4867 in k4864 in k4861 in k4858 in k4855 in k4852 in k4849 in k4846 in k4843 in k4840 in k4837 in k4834 in k4831 in k4828 in k4825 in k4822 in k4819 in k4816 in k4813 in ... */
static void C_ccall f_5686(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_5686,c,av);}
a=C_alloc(9);
t2=C_i_car(t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5637,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_fixnump(t2))){
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5682,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:1029: chicken.compiler.support#big-fixnum? */
t5=*((C_word*)lf[263]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}
else{
t4=t3;
f_5637(t4,C_SCHEME_FALSE);}}
else{
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
f_5603(2,av2);}}}

/* k5688 in a5574 in k4867 in k4864 in k4861 in k4858 in k4855 in k4852 in k4849 in k4846 in k4843 in k4840 in k4837 in k4834 in k4831 in k4828 in k4825 in k4822 in k4819 in k4816 in k4813 in k4810 in ... */
static void C_ccall f_5690(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_5690,c,av);}
a=C_alloc(11);
t2=C_eqp(lf[31],t1);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5603,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
if(C_truep(t2)){
t4=C_eqp(*((C_word*)lf[25]+1),lf[24]);
if(C_truep(t4)){
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5686,a[2]=((C_word*)t0)[5],a[3]=t3,a[4]=((C_word*)t0)[6],tmp=(C_word)a,a+=5,tmp);
/* c-platform.scm:1028: chicken.compiler.support#node-parameters */
t6=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}
else{
t5=t3;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_FALSE;
f_5603(2,av2);}}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
f_5603(2,av2);}}}

/* a5696 in k4831 in k4828 in k4825 in k4822 in k4819 in k4816 in k4813 in k4810 in k4807 in k4804 in k4801 in k4798 in k4795 in k4792 in k4789 in k4786 in k4783 in k4780 in k4777 in k4774 in k4771 in ... */
static void C_ccall f_5697(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,2)))){
C_save_and_reclaim((void *)f_5697,c,av);}
a=C_alloc(16);
t6=C_i_length(t5);
t7=C_eqp(t6,C_fix(3));
if(C_truep(t7)){
t8=C_a_i_list1(&a,1,C_SCHEME_TRUE);
t9=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5719,a[2]=t4,a[3]=t1,a[4]=t8,tmp=(C_word)a,a+=5,tmp);
t10=C_i_caddr(t5);
t11=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5729,a[2]=t9,a[3]=t5,tmp=(C_word)a,a+=4,tmp);
t12=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5747,a[2]=t11,a[3]=t10,tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:992: chicken.compiler.support#node-class */
t13=*((C_word*)lf[34]+1);{
C_word *av2=av;
av2[0]=t13;
av2[1]=t12;
av2[2]=t10;
((C_proc)(void*)(*((C_word*)t13+1)))(3,av2);}}
else{
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}}

/* k5717 in a5696 in k4831 in k4828 in k4825 in k4822 in k4819 in k4816 in k4813 in k4810 in k4807 in k4804 in k4801 in k4798 in k4795 in k4792 in k4789 in k4786 in k4783 in k4780 in k4777 in k4774 in ... */
static void C_ccall f_5719(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_5719,c,av);}
a=C_alloc(6);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
/* c-platform.scm:986: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[23];
av2[3]=((C_word*)t0)[4];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k5727 in a5696 in k4831 in k4828 in k4825 in k4822 in k4819 in k4816 in k4813 in k4810 in k4807 in k4804 in k4801 in k4798 in k4795 in k4792 in k4789 in k4786 in k4783 in k4780 in k4777 in k4774 in ... */
static void C_ccall f_5729(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_5729,c,av);}
if(C_truep(t1)){
/* c-platform.scm:989: chicken.compiler.support#make-node */
t2=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[27];
av2[3]=lf[295];
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}
else{
/* c-platform.scm:989: chicken.compiler.support#make-node */
t2=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[27];
av2[3]=lf[296];
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}}

/* k5741 in k5745 in a5696 in k4831 in k4828 in k4825 in k4822 in k4819 in k4816 in k4813 in k4810 in k4807 in k4804 in k4801 in k4798 in k4795 in k4792 in k4789 in k4786 in k4783 in k4780 in k4777 in ... */
static void C_ccall f_5743(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5743,c,av);}
/* c-platform.scm:993: chicken.compiler.support#immediate? */
t2=*((C_word*)lf[32]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_i_car(t1);
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k5745 in a5696 in k4831 in k4828 in k4825 in k4822 in k4819 in k4816 in k4813 in k4810 in k4807 in k4804 in k4801 in k4798 in k4795 in k4792 in k4789 in k4786 in k4783 in k4780 in k4777 in k4774 in ... */
static void C_ccall f_5747(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_5747,c,av);}
a=C_alloc(3);
t2=C_eqp(lf[31],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5743,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:993: chicken.compiler.support#node-parameters */
t4=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
f_5729(2,av2);}}}

/* a5753 in k4573 in k4570 in k4435 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in ... */
static void C_ccall f_5754(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,2)))){
C_save_and_reclaim((void *)f_5754,c,av);}
a=C_alloc(16);
t6=C_eqp(lf[24],*((C_word*)lf[25]+1));
if(C_truep(t6)){
t7=C_i_length(t5);
t8=C_eqp(t7,C_fix(2));
if(C_truep(t8)){
t9=C_a_i_list1(&a,1,C_SCHEME_TRUE);
t10=C_i_cadr(t5);
t11=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5785,a[2]=t4,a[3]=t1,a[4]=t9,tmp=(C_word)a,a+=5,tmp);
t12=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5788,a[2]=t5,a[3]=t11,tmp=(C_word)a,a+=4,tmp);
t13=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5824,a[2]=t12,a[3]=t10,tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:871: chicken.compiler.support#node-class */
t14=*((C_word*)lf[34]+1);{
C_word *av2=av;
av2[0]=t14;
av2[1]=t13;
av2[2]=t10;
((C_proc)(void*)(*((C_word*)t14+1)))(3,av2);}}
else{
t9=t1;{
C_word *av2=av;
av2[0]=t9;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t9+1)))(2,av2);}}}
else{
t7=t1;{
C_word *av2=av;
av2[0]=t7;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}}

/* k5783 in a5753 in k4573 in k4570 in k4435 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in ... */
static void C_ccall f_5785(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_5785,c,av);}
a=C_alloc(6);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
/* c-platform.scm:867: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[23];
av2[3]=((C_word*)t0)[4];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k5786 in a5753 in k4573 in k4570 in k4435 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in ... */
static void C_fcall f_5788(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,4)))){
C_save_and_reclaim_args((void *)trf_5788,2,t0,t1);}
a=C_alloc(4);
if(C_truep(t1)){
t2=C_i_car(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5803,a[2]=t2,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:875: chicken.compiler.support#qnode */
t4=*((C_word*)lf[29]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_fix(1);
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
/* c-platform.scm:876: chicken.compiler.support#make-node */
t2=*((C_word*)lf[22]+1);{
C_word av2[5];
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[27];
av2[3]=lf[456];
av2[4]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}}

/* k5801 in k5786 in a5753 in k4573 in k4570 in k4435 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in ... */
static void C_ccall f_5803(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_5803,c,av);}
a=C_alloc(6);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
/* c-platform.scm:873: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[27];
av2[3]=lf[455];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k5818 in k5822 in a5753 in k4573 in k4570 in k4435 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in ... */
static void C_ccall f_5820(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5820,c,av);}
t2=((C_word*)t0)[2];
f_5788(t2,C_eqp(C_fix(2),C_i_car(t1)));}

/* k5822 in a5753 in k4573 in k4570 in k4435 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in ... */
static void C_ccall f_5824(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_5824,c,av);}
a=C_alloc(3);
t2=C_eqp(lf[31],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5820,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:872: chicken.compiler.support#node-parameters */
t4=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=((C_word*)t0)[2];
f_5788(t3,C_SCHEME_FALSE);}}

/* a5830 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in k4372 in ... */
static void C_ccall f_5831(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_5831,c,av);}
a=C_alloc(8);
t6=C_i_nullp(t5);
t7=(C_truep(t6)?t6:C_i_not(C_eqp(*((C_word*)lf[25]+1),lf[24])));
if(C_truep(t7)){
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}
else{
t8=C_i_cdr(t5);
if(C_truep(C_i_nullp(t8))){
t9=C_a_i_list1(&a,1,C_SCHEME_TRUE);
t10=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5862,a[2]=t4,a[3]=t1,a[4]=t9,tmp=(C_word)a,a+=5,tmp);
if(C_truep(*((C_word*)lf[26]+1))){
/* c-platform.scm:809: chicken.compiler.support#make-node */
t11=*((C_word*)lf[22]+1);{
C_word *av2=av;
av2[0]=t11;
av2[1]=t10;
av2[2]=lf[27];
av2[3]=lf[458];
av2[4]=t5;
((C_proc)(void*)(*((C_word*)t11+1)))(5,av2);}}
else{
/* c-platform.scm:809: chicken.compiler.support#make-node */
t11=*((C_word*)lf[22]+1);{
C_word *av2=av;
av2[0]=t11;
av2[1]=t10;
av2[2]=lf[27];
av2[3]=lf[459];
av2[4]=t5;
((C_proc)(void*)(*((C_word*)t11+1)))(5,av2);}}}
else{
t9=C_u_i_car(t5);
t10=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5915,a[2]=t9,a[3]=t4,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t11=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_5917,tmp=(C_word)a,a+=2,tmp);
/* c-platform.scm:815: filter */
f_2293(t10,t11,C_u_i_cdr(t5));}}}

/* k5860 in a5830 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in ... */
static void C_ccall f_5862(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_5862,c,av);}
a=C_alloc(6);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
/* c-platform.scm:806: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[23];
av2[3]=((C_word*)t0)[4];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k5888 in k5913 in a5830 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in ... */
static void C_ccall f_5890(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_5890,c,av);}
a=C_alloc(6);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
/* c-platform.scm:821: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[23];
av2[3]=((C_word*)t0)[4];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* a5891 in k5913 in a5830 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in ... */
static void C_ccall f_5892(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_5892,c,av);}
a=C_alloc(6);
t4=(C_truep(*((C_word*)lf[26]+1))?lf[460]:lf[461]);
t5=C_a_i_list2(&a,2,t2,t3);
/* c-platform.scm:827: chicken.compiler.support#make-node */
t6=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t6;
av2[1]=t1;
av2[2]=lf[27];
av2[3]=t4;
av2[4]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k5913 in a5830 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in ... */
static void C_ccall f_5915(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,4)))){
C_save_and_reclaim((void *)f_5915,c,av);}
a=C_alloc(13);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_i_length(t2);
if(C_truep(C_fixnum_greater_or_equal_p(t3,C_fix(2)))){
t4=C_a_i_list1(&a,1,C_SCHEME_TRUE);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5890,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t4,tmp=(C_word)a,a+=5,tmp);
t6=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_5892,tmp=(C_word)a,a+=2,tmp);
/* c-platform.scm:825: chicken.compiler.support#fold-inner */
t7=*((C_word*)lf[60]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t7;
av2[1]=t5;
av2[2]=t6;
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}
else{
t4=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* a5916 in a5830 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in ... */
static void C_ccall f_5917(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_5917,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5940,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:817: chicken.compiler.support#node-class */
t4=*((C_word*)lf[34]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k5934 in k5938 in a5916 in a5830 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in ... */
static void C_ccall f_5936(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5936,c,av);}
t2=C_i_car(t1);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_not(C_i_zerop(t2));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k5938 in a5916 in a5830 in k4432 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in ... */
static void C_ccall f_5940(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_5940,c,av);}
a=C_alloc(3);
t2=C_eqp(lf[31],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5936,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:818: chicken.compiler.support#node-parameters */
t4=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* a5954 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in k4372 in k4369 in ... */
static void C_ccall f_5955(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_5955,c,av);}
a=C_alloc(8);
t6=C_i_nullp(t5);
t7=(C_truep(t6)?t6:C_i_not(C_eqp(*((C_word*)lf[25]+1),lf[24])));
if(C_truep(t7)){
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}
else{
t8=C_i_cdr(t5);
if(C_truep(C_i_nullp(t8))){
t9=C_a_i_list1(&a,1,C_SCHEME_TRUE);
t10=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5986,a[2]=t4,a[3]=t1,a[4]=t9,tmp=(C_word)a,a+=5,tmp);
if(C_truep(*((C_word*)lf[26]+1))){
/* c-platform.scm:772: chicken.compiler.support#make-node */
t11=*((C_word*)lf[22]+1);{
C_word *av2=av;
av2[0]=t11;
av2[1]=t10;
av2[2]=lf[27];
av2[3]=lf[462];
av2[4]=t5;
((C_proc)(void*)(*((C_word*)t11+1)))(5,av2);}}
else{
/* c-platform.scm:772: chicken.compiler.support#make-node */
t11=*((C_word*)lf[22]+1);{
C_word *av2=av;
av2[0]=t11;
av2[1]=t10;
av2[2]=lf[27];
av2[3]=lf[463];
av2[4]=t5;
((C_proc)(void*)(*((C_word*)t11+1)))(5,av2);}}}
else{
t9=C_u_i_car(t5);
t10=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6039,a[2]=t9,a[3]=t4,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t11=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_6041,tmp=(C_word)a,a+=2,tmp);
/* c-platform.scm:778: filter */
f_2293(t10,t11,C_u_i_cdr(t5));}}}

/* k5984 in a5954 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in k4372 in ... */
static void C_ccall f_5986(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_5986,c,av);}
a=C_alloc(6);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
/* c-platform.scm:769: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[23];
av2[3]=((C_word*)t0)[4];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k6012 in k6037 in a5954 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in ... */
static void C_ccall f_6014(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_6014,c,av);}
a=C_alloc(6);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
/* c-platform.scm:784: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[23];
av2[3]=((C_word*)t0)[4];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* a6015 in k6037 in a5954 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in ... */
static void C_ccall f_6016(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_6016,c,av);}
a=C_alloc(6);
t4=(C_truep(*((C_word*)lf[26]+1))?lf[464]:lf[465]);
t5=C_a_i_list2(&a,2,t2,t3);
/* c-platform.scm:790: chicken.compiler.support#make-node */
t6=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t6;
av2[1]=t1;
av2[2]=lf[27];
av2[3]=t4;
av2[4]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k6037 in a5954 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in k4372 in ... */
static void C_ccall f_6039(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,4)))){
C_save_and_reclaim((void *)f_6039,c,av);}
a=C_alloc(13);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_i_length(t2);
if(C_truep(C_fixnum_greater_or_equal_p(t3,C_fix(2)))){
t4=C_a_i_list1(&a,1,C_SCHEME_TRUE);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6014,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t4,tmp=(C_word)a,a+=5,tmp);
t6=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_6016,tmp=(C_word)a,a+=2,tmp);
/* c-platform.scm:788: chicken.compiler.support#fold-inner */
t7=*((C_word*)lf[60]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t7;
av2[1]=t5;
av2[2]=t6;
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}
else{
t4=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* a6040 in a5954 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in k4372 in ... */
static void C_ccall f_6041(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_6041,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6064,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:780: chicken.compiler.support#node-class */
t4=*((C_word*)lf[34]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k6058 in k6062 in a6040 in a5954 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in ... */
static void C_ccall f_6060(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6060,c,av);}
t2=C_i_car(t1);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_not(C_i_zerop(t2));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k6062 in a6040 in a5954 in k4429 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in ... */
static void C_ccall f_6064(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_6064,c,av);}
a=C_alloc(3);
t2=C_eqp(lf[31],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6060,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:781: chicken.compiler.support#node-parameters */
t4=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* a6078 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in k4372 in k4369 in k4366 in ... */
static void C_ccall f_6079(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_6079,c,av);}
a=C_alloc(6);
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6083,a[2]=t4,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t7=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_6197,tmp=(C_word)a,a+=2,tmp);
/* c-platform.scm:738: filter */
f_2293(t6,t7,t5);}

/* k6081 in a6078 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in k4372 in k4369 in ... */
static void C_ccall f_6083(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,4)))){
C_save_and_reclaim((void *)f_6083,c,av);}
a=C_alloc(10);
if(C_truep(C_i_nullp(t1))){
t2=C_a_i_list1(&a,1,C_SCHEME_TRUE);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6104,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* c-platform.scm:743: chicken.compiler.support#qnode */
t4=*((C_word*)lf[29]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_fix(0);
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t2=C_i_cdr(t1);
if(C_truep(C_i_nullp(t2))){
t3=C_a_i_list1(&a,1,C_SCHEME_TRUE);
t4=C_i_car(t1);
t5=C_a_i_list2(&a,2,((C_word*)t0)[2],t4);
/* c-platform.scm:745: chicken.compiler.support#make-node */
t6=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t6;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[23];
av2[3]=t3;
av2[4]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}
else{
t3=C_eqp(*((C_word*)lf[25]+1),lf[24]);
if(C_truep(t3)){
t4=C_a_i_list1(&a,1,C_SCHEME_TRUE);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6146,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t4,tmp=(C_word)a,a+=5,tmp);
t6=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_6148,tmp=(C_word)a,a+=2,tmp);
/* c-platform.scm:751: chicken.compiler.support#fold-inner */
t7=*((C_word*)lf[60]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t7;
av2[1]=t5;
av2[2]=t6;
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}
else{
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}}}

/* k6102 in k6081 in a6078 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in k4372 in ... */
static void C_ccall f_6104(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_6104,c,av);}
a=C_alloc(6);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
/* c-platform.scm:743: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[23];
av2[3]=((C_word*)t0)[4];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k6144 in k6081 in a6078 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in k4372 in ... */
static void C_ccall f_6146(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_6146,c,av);}
a=C_alloc(6);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
/* c-platform.scm:747: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[23];
av2[3]=((C_word*)t0)[4];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* a6147 in k6081 in a6078 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in k4372 in ... */
static void C_ccall f_6148(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_6148,c,av);}
a=C_alloc(9);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6155,a[2]=t2,a[3]=t1,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6191,a[2]=t4,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:753: chicken.compiler.support#node-class */
t6=*((C_word*)lf[34]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k6153 in a6147 in k6081 in a6078 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in ... */
static void C_fcall f_6155(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,4)))){
C_save_and_reclaim_args((void *)trf_6155,2,t0,t1);}
a=C_alloc(6);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6166,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:754: chicken.compiler.support#qnode */
t3=*((C_word*)lf[29]+1);{
C_word av2[3];
av2[0]=t3;
av2[1]=t2;
av2[2]=C_fix(1);
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],((C_word*)t0)[4]);
/* c-platform.scm:755: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word av2[5];
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[27];
av2[3]=lf[467];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}}

/* k6164 in k6153 in a6147 in k6081 in a6078 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in ... */
static void C_ccall f_6166(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_6166,c,av);}
a=C_alloc(6);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
/* c-platform.scm:754: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[27];
av2[3]=lf[466];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k6185 in k6189 in a6147 in k6081 in a6078 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in ... */
static void C_ccall f_6187(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6187,c,av);}
t2=((C_word*)t0)[2];
f_6155(t2,C_eqp(C_fix(2),C_i_car(t1)));}

/* k6189 in a6147 in k6081 in a6078 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in ... */
static void C_ccall f_6191(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_6191,c,av);}
a=C_alloc(3);
t2=C_eqp(lf[31],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6187,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:753: chicken.compiler.support#node-parameters */
t4=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=((C_word*)t0)[2];
f_6155(t3,C_SCHEME_FALSE);}}

/* a6196 in a6078 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in k4372 in k4369 in ... */
static void C_ccall f_6197(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_6197,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6223,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:740: chicken.compiler.support#node-class */
t4=*((C_word*)lf[34]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k6217 in k6221 in a6196 in a6078 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in ... */
static void C_ccall f_6219(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6219,c,av);}
t2=C_i_car(t1);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_not(C_eqp(C_fix(1),t2));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k6221 in a6196 in a6078 in k4426 in k4423 in k4420 in k4417 in k4414 in k4411 in k4408 in k4405 in k4402 in k4399 in k4396 in k4393 in k4390 in k4387 in k4384 in k4381 in k4378 in k4375 in k4372 in ... */
static void C_ccall f_6223(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_6223,c,av);}
a=C_alloc(3);
t2=C_eqp(lf[31],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6219,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:741: chicken.compiler.support#node-parameters */
t4=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* a6224 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_6225(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_6225,c,av);}
a=C_alloc(13);
t6=C_i_length(t5);
t7=C_eqp(t6,C_fix(2));
if(C_truep(t7)){
t8=C_i_car(t5);
t9=C_i_cadr(t5);
t10=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6241,a[2]=t1,a[3]=t4,a[4]=t5,a[5]=t9,a[6]=t8,tmp=(C_word)a,a+=7,tmp);
t11=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6381,a[2]=t4,a[3]=t10,a[4]=t9,a[5]=t8,tmp=(C_word)a,a+=6,tmp);
/* c-platform.scm:351: chicken.compiler.support#node-class */
t12=*((C_word*)lf[34]+1);{
C_word *av2=av;
av2[0]=t12;
av2[1]=t11;
av2[2]=t8;
((C_proc)(void*)(*((C_word*)t12+1)))(3,av2);}}
else{
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}}

/* k6239 in a6224 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_6241(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,2)))){
C_save_and_reclaim((void *)f_6241,c,av);}
a=C_alloc(15);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6247,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6268,a[2]=((C_word*)t0)[3],a[3]=t2,a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6335,a[2]=t3,a[3]=((C_word*)t0)[6],tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:355: chicken.compiler.support#node-class */
t5=*((C_word*)lf[34]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}}

/* k6245 in k6239 in a6224 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_6247(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_6247,c,av);}
a=C_alloc(8);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_a_i_list1(&a,1,C_SCHEME_TRUE);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6265,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* c-platform.scm:366: chicken.compiler.support#make-node */
t4=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[27];
av2[3]=lf[915];
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}}

/* k6263 in k6245 in k6239 in a6224 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 in ... */
static void C_ccall f_6265(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_6265,c,av);}
a=C_alloc(6);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
/* c-platform.scm:364: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[23];
av2[3]=((C_word*)t0)[4];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k6266 in k6239 in a6224 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_fcall f_6268(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,2)))){
C_save_and_reclaim_args((void *)trf_6268,2,t0,t1);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6271,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
if(C_truep(t1)){
t3=t2;
f_6271(t3,t1);}
else{
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6312,a[2]=t2,a[3]=((C_word*)t0)[5],tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:358: chicken.compiler.support#node-class */
t4=*((C_word*)lf[34]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}}

/* k6269 in k6266 in k6239 in a6224 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 in ... */
static void C_fcall f_6271(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,4)))){
C_save_and_reclaim_args((void *)trf_6271,2,t0,t1);}
a=C_alloc(8);
if(C_truep(t1)){
t2=C_a_i_list1(&a,1,C_SCHEME_TRUE);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6286,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* c-platform.scm:363: chicken.compiler.support#make-node */
t4=*((C_word*)lf[22]+1);{
C_word av2[5];
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[27];
av2[3]=lf[916];
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word av2[2];
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
f_6247(2,av2);}}}

/* k6284 in k6269 in k6266 in k6239 in a6224 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in ... */
static void C_ccall f_6286(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_6286,c,av);}
a=C_alloc(6);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
/* c-platform.scm:361: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[23];
av2[3]=((C_word*)t0)[4];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k6296 in k6306 in k6310 in k6266 in k6239 in a6224 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in ... */
static void C_ccall f_6298(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6298,c,av);}
t2=((C_word*)t0)[2];
f_6271(t2,(C_truep(t1)?t1:C_i_symbolp(((C_word*)t0)[3])));}

/* k6306 in k6310 in k6266 in k6239 in a6224 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in ... */
static void C_ccall f_6308(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_6308,c,av);}
a=C_alloc(4);
t2=C_i_car(t1);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6298,a[2]=((C_word*)t0)[2],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:360: chicken.compiler.support#immediate? */
t4=*((C_word*)lf[32]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k6310 in k6266 in k6239 in a6224 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 in ... */
static void C_ccall f_6312(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_6312,c,av);}
a=C_alloc(3);
t2=C_eqp(lf[31],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6308,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:359: chicken.compiler.support#node-parameters */
t4=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=((C_word*)t0)[2];
f_6271(t3,C_SCHEME_FALSE);}}

/* k6319 in k6329 in k6333 in k6239 in a6224 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in ... */
static void C_ccall f_6321(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6321,c,av);}
t2=((C_word*)t0)[2];
f_6268(t2,(C_truep(t1)?t1:C_i_symbolp(((C_word*)t0)[3])));}

/* k6329 in k6333 in k6239 in a6224 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 in ... */
static void C_ccall f_6331(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_6331,c,av);}
a=C_alloc(4);
t2=C_i_car(t1);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6321,a[2]=((C_word*)t0)[2],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* c-platform.scm:357: chicken.compiler.support#immediate? */
t4=*((C_word*)lf[32]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k6333 in k6239 in a6224 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_6335(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_6335,c,av);}
a=C_alloc(3);
t2=C_eqp(lf[31],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6331,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* c-platform.scm:356: chicken.compiler.support#node-parameters */
t4=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=((C_word*)t0)[2];
f_6268(t3,C_SCHEME_FALSE);}}

/* k6363 in k6371 in k6367 in k6375 in k6379 in a6224 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in ... */
static void C_ccall f_6365(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_6365,c,av);}
a=C_alloc(6);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
/* c-platform.scm:354: chicken.compiler.support#make-node */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[23];
av2[3]=((C_word*)t0)[4];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k6367 in k6375 in k6379 in a6224 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 in ... */
static void C_ccall f_6369(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_6369,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6373,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* c-platform.scm:353: chicken.compiler.support#node-parameters */
t3=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k6371 in k6367 in k6375 in k6379 in a6224 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in ... */
static void C_ccall f_6373(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_6373,c,av);}
a=C_alloc(8);
if(C_truep(C_i_equalp(((C_word*)t0)[2],t1))){
t2=C_a_i_list1(&a,1,C_SCHEME_TRUE);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6365,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* c-platform.scm:354: chicken.compiler.support#qnode */
t4=*((C_word*)lf[29]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t2=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
f_6241(2,av2);}}}

/* k6375 in k6379 in a6224 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_6377(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_6377,c,av);}
a=C_alloc(5);
t2=C_eqp(lf[35],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6369,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* c-platform.scm:353: chicken.compiler.support#node-parameters */
t4=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
f_6241(2,av2);}}}

/* k6379 in a6224 in k3235 in k3232 in k3076 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_6381(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_6381,c,av);}
a=C_alloc(6);
t2=C_eqp(lf[35],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6377,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* c-platform.scm:352: chicken.compiler.support#node-class */
t4=*((C_word*)lf[34]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
f_6241(2,av2);}}}

/* k6388 in k3073 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_6390(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6390,c,av);}
/* c-platform.scm:316: chicken.compiler.optimizer#rewrite */
t2=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[918];
av2[3]=C_fix(8);
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k6392 in k3006 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_6394(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6394,c,av);}
/* c-platform.scm:315: chicken.compiler.optimizer#rewrite */
t2=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[922];
av2[3]=C_fix(8);
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* for-each-loop590 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_fcall f_6396(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,4)))){
C_save_and_reclaim_args((void *)trf_6396,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6406,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t4=C_slot(t2,C_fix(0));
t5=*((C_word*)lf[926]+1);
/* c-platform.scm:287: g606 */
t6=*((C_word*)lf[926]+1);{
C_word av2[5];
av2[0]=t6;
av2[1]=t3;
av2[2]=t4;
av2[3]=lf[927];
av2[4]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k6404 in for-each-loop590 in k2997 in k2990 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_6406(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6406,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_6396(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* map-loop552 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_fcall f_6419(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_6419,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6444,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* c-platform.scm:123: chicken.base#symbol-append */
t4=*((C_word*)lf[932]+1);{
C_word av2[4];
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[933];
av2[3]=C_slot(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k6442 in map-loop552 in k2975 in k2972 in k2969 in k2961 in k1680 in k1677 in k1674 in k1671 in k1668 in k1665 in k1662 */
static void C_ccall f_6444(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_6444,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_6419(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* toplevel */
static C_TLS int toplevel_initialized=0;

void C_ccall C_c_2dplatform_toplevel(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(toplevel_initialized) {C_kontinue(t1,C_SCHEME_UNDEFINED);}
else C_toplevel_entry(C_text("c-platform"));
C_check_nursery_minimum(C_calculate_demand(3,c,2));
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void*)C_c_2dplatform_toplevel,c,av);}
toplevel_initialized=1;
if(C_unlikely(!C_demand_2(9961))){
C_save(t1);
C_rereclaim2(9961*sizeof(C_word),1);
t1=C_restore;}
a=C_alloc(3);
C_initialize_lf(lf,941);
lf[0]=C_h_intern(&lf[0],10, C_text("c-platform"));
lf[1]=C_h_intern(&lf[1],28, C_text("chicken.compiler.c-platform#"));
lf[4]=C_h_intern(&lf[4],5, C_text("foldr"));
lf[5]=C_h_intern(&lf[5],48, C_text("chicken.compiler.c-platform#default-declarations"));
lf[6]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\014\001always-bound\376\003\000\000\002\376\001\000\000\024\001##sys#standard-input\376\003\000\000\002\376\001\000\000\025\001##sys#stan"
"dard-output\376\003\000\000\002\376\001\000\000\024\001##sys#standard-error\376\003\000\000\002\376\001\000\000\025\001##sys#undefined-value\376\377\016\376\003\000"
"\000\002\376\003\000\000\002\376\001\000\000\022\001bound-to-procedure\376\003\000\000\002\376\001\000\000\016\001##sys#for-each\376\003\000\000\002\376\001\000\000\011\001##sys#map\376\003\000\000"
"\002\376\001\000\000\013\001##sys#print\376\003\000\000\002\376\001\000\000\014\001##sys#setter\376\003\000\000\002\376\001\000\000\015\001##sys#setslot\376\003\000\000\002\376\001\000\000\022\001##sy"
"s#dynamic-wind\376\003\000\000\002\376\001\000\000\026\001##sys#call-with-values\376\003\000\000\002\376\001\000\000\021\001##sys#start-timer\376\003\000\000\002"
"\376\001\000\000\020\001##sys#stop-timer\376\003\000\000\002\376\001\000\000\011\001##sys#gcd\376\003\000\000\002\376\001\000\000\011\001##sys#lcm\376\003\000\000\002\376\001\000\000\020\001##sys#s"
"tructure\077\376\003\000\000\002\376\001\000\000\012\001##sys#slot\376\003\000\000\002\376\001\000\000\025\001##sys#allocate-vector\376\003\000\000\002\376\001\000\000\022\001##sys#l"
"ist->vector\376\003\000\000\002\376\001\000\000\017\001##sys#block-ref\376\003\000\000\002\376\001\000\000\020\001##sys#block-set!\376\003\000\000\002\376\001\000\000\012\001##sys"
"#list\376\003\000\000\002\376\001\000\000\012\001##sys#cons\376\003\000\000\002\376\001\000\000\014\001##sys#append\376\003\000\000\002\376\001\000\000\014\001##sys#vector\376\003\000\000\002\376\001\000"
"\000\033\001##sys#foreign-char-argument\376\003\000\000\002\376\001\000\000\035\001##sys#foreign-fixnum-argument\376\003\000\000\002\376\001\000\000\035"
"\001##sys#foreign-flonum-argument\376\003\000\000\002\376\001\000\000\013\001##sys#error\376\003\000\000\002\376\001\000\000\023\001##sys#peek-c-stri"
"ng\376\003\000\000\002\376\001\000\000\033\001##sys#peek-nonnull-c-string\376\003\000\000\002\376\001\000\000\034\001##sys#peek-and-free-c-string\376"
"\003\000\000\002\376\001\000\000$\001##sys#peek-and-free-nonnull-c-string\376\003\000\000\002\376\001\000\000\034\001##sys#foreign-block-arg"
"ument\376\003\000\000\002\376\001\000\000\035\001##sys#foreign-string-argument\376\003\000\000\002\376\001\000\000\036\001##sys#foreign-pointer-ar"
"gument\376\003\000\000\002\376\001\000\000$\001##sys#call-with-current-continuation\376\377\016\376\377\016"));
lf[7]=C_h_intern(&lf[7],58, C_text("chicken.compiler.c-platform#default-profiling-declarations"));
lf[8]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\016\001##core#declare\376\003\000\000\002\376\003\000\000\002\376\001\000\000\004\001uses\376\003\000\000\002\376\001\000\000\010\001profiler\376\377\016\376\003\000\000\002\376\003\000"
"\000\002\376\001\000\000\022\001bound-to-procedure\376\003\000\000\002\376\001\000\000\023\001##sys#profile-entry\376\003\000\000\002\376\001\000\000\022\001##sys#profile"
"-exit\376\377\016\376\377\016\376\377\016"));
lf[9]=C_h_intern(&lf[9],41, C_text("chicken.compiler.c-platform#default-units"));
lf[10]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\007\001library\376\003\000\000\002\376\001\000\000\004\001eval\376\377\016"));
lf[11]=C_h_intern(&lf[11],44, C_text("chicken.compiler.c-platform#words-per-flonum"));
lf[12]=C_h_intern(&lf[12],47, C_text("chicken.compiler.c-platform#target-include-file"));
lf[13]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011chicken.h"));
lf[14]=C_h_intern(&lf[14],50, C_text("chicken.compiler.c-platform#valid-compiler-options"));
lf[15]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\005\001-help\376\003\000\000\002\376\001\000\000\001\001h\376\003\000\000\002\376\001\000\000\004\001help\376\003\000\000\002\376\001\000\000\007\001version\376\003\000\000\002\376\001\000\000\007\001verbose\376"
"\003\000\000\002\376\001\000\000\014\001explicit-use\376\003\000\000\002\376\001\000\000\010\001no-trace\376\003\000\000\002\376\001\000\000\013\001no-warnings\376\003\000\000\002\376\001\000\000\006\001unsafe"
"\376\003\000\000\002\376\001\000\000\005\001block\376\003\000\000\002\376\001\000\000\014\001check-syntax\376\003\000\000\002\376\001\000\000\011\001to-stdout\376\003\000\000\002\376\001\000\000\025\001no-usual-i"
"ntegrations\376\003\000\000\002\376\001\000\000\020\001case-insensitive\376\003\000\000\002\376\001\000\000\016\001no-lambda-info\376\003\000\000\002\376\001\000\000\007\001profil"
"e\376\003\000\000\002\376\001\000\000\006\001inline\376\003\000\000\002\376\001\000\000\024\001keep-shadowed-macros\376\003\000\000\002\376\001\000\000\021\001ignore-repository\376\003\000"
"\000\002\376\001\000\000\021\001fixnum-arithmetic\376\003\000\000\002\376\001\000\000\022\001disable-interrupts\376\003\000\000\002\376\001\000\000\026\001optimize-leaf-r"
"outines\376\003\000\000\002\376\001\000\000\016\001compile-syntax\376\003\000\000\002\376\001\000\000\014\001tag-pointers\376\003\000\000\002\376\001\000\000\022\001accumulate-pro"
"file\376\003\000\000\002\376\001\000\000\035\001disable-stack-overflow-checks\376\003\000\000\002\376\001\000\000\003\001raw\376\003\000\000\002\376\001\000\000\012\001specialize\376"
"\003\000\000\002\376\001\000\000\036\001emit-external-prototypes-first\376\003\000\000\002\376\001\000\000\007\001release\376\003\000\000\002\376\001\000\000\005\001local\376\003\000\000\002\376"
"\001\000\000\015\001inline-global\376\003\000\000\002\376\001\000\000\014\001analyze-only\376\003\000\000\002\376\001\000\000\007\001dynamic\376\003\000\000\002\376\001\000\000\006\001static\376\003\000\000"
"\002\376\001\000\000\016\001no-argc-checks\376\003\000\000\002\376\001\000\000\023\001no-procedure-checks\376\003\000\000\002\376\001\000\000\027\001no-parentheses-syn"
"onyms\376\003\000\000\002\376\001\000\000)\001no-procedure-checks-for-toplevel-bindings\376\003\000\000\002\376\001\000\000\017\001no-bound-che"
"cks\376\003\000\000\002\376\001\000\000&\001no-procedure-checks-for-usual-bindings\376\003\000\000\002\376\001\000\000\022\001no-compiler-synta"
"x\376\003\000\000\002\376\001\000\000\027\001no-parentheses-synonyms\376\003\000\000\002\376\001\000\000\020\001no-symbol-escape\376\003\000\000\002\376\001\000\000\013\001r5rs-sy"
"ntax\376\003\000\000\002\376\001\000\000\031\001emit-all-import-libraries\376\003\000\000\002\376\001\000\000\014\001strict-types\376\003\000\000\002\376\001\000\000\012\001cluste"
"ring\376\003\000\000\002\376\001\000\000\004\001lfa2\376\003\000\000\002\376\001\000\000\012\001debug-info\376\003\000\000\002\376\001\000\000\033\001regenerate-import-libraries\376\003"
"\000\000\002\376\001\000\000\012\001setup-mode\376\003\000\000\002\376\001\000\000\023\001module-registration\376\003\000\000\002\376\001\000\000\026\001no-module-registrati"
"on\376\377\016"));
lf[16]=C_h_intern(&lf[16],64, C_text("chicken.compiler.c-platform#valid-compiler-options-with-argument"));
lf[17]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\005\001debug\376\003\000\000\002\376\001\000\000\016\001emit-link-file\376\003\000\000\002\376\001\000\000\013\001output-file\376\003\000\000\002\376\001\000\000\014\001includ"
"e-path\376\003\000\000\002\376\001\000\000\011\001heap-size\376\003\000\000\002\376\001\000\000\012\001stack-size\376\003\000\000\002\376\001\000\000\004\001unit\376\003\000\000\002\376\001\000\000\004\001uses\376\003\000"
"\000\002\376\001\000\000\006\001module\376\003\000\000\002\376\001\000\000\015\001keyword-style\376\003\000\000\002\376\001\000\000\021\001require-extension\376\003\000\000\002\376\001\000\000\014\001inl"
"ine-limit\376\003\000\000\002\376\001\000\000\014\001profile-name\376\003\000\000\002\376\001\000\000\007\001prelude\376\003\000\000\002\376\001\000\000\010\001postlude\376\003\000\000\002\376\001\000\000\010\001"
"prologue\376\003\000\000\002\376\001\000\000\010\001epilogue\376\003\000\000\002\376\001\000\000\007\001nursery\376\003\000\000\002\376\001\000\000\006\001extend\376\003\000\000\002\376\001\000\000\007\001feature"
"\376\003\000\000\002\376\001\000\000\012\001no-feature\376\003\000\000\002\376\001\000\000\014\001unroll-limit\376\003\000\000\002\376\001\000\000\020\001emit-inline-file\376\003\000\000\002\376\001\000\000"
"\023\001consult-inline-file\376\003\000\000\002\376\001\000\000\017\001emit-types-file\376\003\000\000\002\376\001\000\000\022\001consult-types-file\376\003\000\000"
"\002\376\001\000\000\023\001emit-import-library\376\377\016"));
lf[18]=C_h_intern(&lf[18],47, C_text("chicken.compiler.core#default-standard-bindings"));
lf[19]=C_h_intern(&lf[19],47, C_text("chicken.compiler.core#default-extended-bindings"));
lf[20]=C_h_intern(&lf[20],39, C_text("chicken.compiler.core#internal-bindings"));
lf[21]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\012\001##sys#slot\376\003\000\000\002\376\001\000\000\015\001##sys#setslot\376\003\000\000\002\376\001\000\000\017\001##sys#block-ref\376\003\000\000\002\376\001\000\000"
"\020\001##sys#block-set!\376\003\000\000\002\376\001\000\000\011\001##sys#/-2\376\003\000\000\002\376\001\000\000$\001##sys#call-with-current-continu"
"ation\376\003\000\000\002\376\001\000\000\012\001##sys#size\376\003\000\000\002\376\001\000\000\012\001##sys#byte\376\003\000\000\002\376\001\000\000\015\001##sys#setbyte\376\003\000\000\002\376\001\000\000"
"\016\001##sys#pointer\077\376\003\000\000\002\376\001\000\000\030\001##sys#generic-structure\077\376\003\000\000\002\376\001\000\000\020\001##sys#structure\077\376\003"
"\000\000\002\376\001\000\000\025\001##sys#check-structure\376\003\000\000\002\376\001\000\000\022\001##sys#check-number\376\003\000\000\002\376\001\000\000\020\001##sys#chec"
"k-list\376\003\000\000\002\376\001\000\000\020\001##sys#check-pair\376\003\000\000\002\376\001\000\000\022\001##sys#check-string\376\003\000\000\002\376\001\000\000\022\001##sys#c"
"heck-symbol\376\003\000\000\002\376\001\000\000\023\001##sys#check-boolean\376\003\000\000\002\376\001\000\000\024\001##sys#check-locative\376\003\000\000\002\376\001\000"
"\000\020\001##sys#check-port\376\003\000\000\002\376\001\000\000\026\001##sys#check-input-port\376\003\000\000\002\376\001\000\000\027\001##sys#check-outpu"
"t-port\376\003\000\000\002\376\001\000\000\025\001##sys#check-open-port\376\003\000\000\002\376\001\000\000\020\001##sys#check-char\376\003\000\000\002\376\001\000\000\022\001##sy"
"s#check-vector\376\003\000\000\002\376\001\000\000\027\001##sys#check-byte-vector\376\003\000\000\002\376\001\000\000\012\001##sys#list\376\003\000\000\002\376\001\000\000\012\001"
"##sys#cons\376\003\000\000\002\376\001\000\000\026\001##sys#call-with-values\376\003\000\000\002\376\001\000\000\035\001##sys#flonum-in-fixnum-ran"
"ge\077\376\003\000\000\002\376\001\000\000\020\001##sys#immediate\077\376\003\000\000\002\376\001\000\000\024\001##sys#context-switch\376\003\000\000\002\376\001\000\000\024\001##sys#ma"
"ke-structure\376\003\000\000\002\376\001\000\000\013\001##sys#apply\376\003\000\000\002\376\001\000\000\022\001##sys#apply-values\376\003\000\000\002\376\001\000\000\047\001chicke"
"n.continuation#continuation-graft\376\003\000\000\002\376\001\000\000\021\001##sys#bytevector\077\376\003\000\000\002\376\001\000\000\021\001##sys#ma"
"ke-vector\376\003\000\000\002\376\001\000\000\014\001##sys#setter\376\003\000\000\002\376\001\000\000\011\001##sys#car\376\003\000\000\002\376\001\000\000\011\001##sys#cdr\376\003\000\000\002\376\001\000"
"\000\013\001##sys#pair\077\376\003\000\000\002\376\001\000\000\011\001##sys#eq\077\376\003\000\000\002\376\001\000\000\013\001##sys#list\077\376\003\000\000\002\376\001\000\000\015\001##sys#vector\077"
"\376\003\000\000\002\376\001\000\000\012\001##sys#eqv\077\376\003\000\000\002\376\001\000\000\021\001##sys#get-keyword\376\003\000\000\002\376\001\000\000\033\001##sys#foreign-char-a"
"rgument\376\003\000\000\002\376\001\000\000\035\001##sys#foreign-fixnum-argument\376\003\000\000\002\376\001\000\000\035\001##sys#foreign-flonum-a"
"rgument\376\003\000\000\002\376\001\000\000\034\001##sys#foreign-block-argument\376\003\000\000\002\376\001\000\000%\001##sys#foreign-struct-wr"
"apper-argument\376\003\000\000\002\376\001\000\000\035\001##sys#foreign-string-argument\376\003\000\000\002\376\001\000\000\036\001##sys#foreign-p"
"ointer-argument\376\003\000\000\002\376\001\000\000\012\001##sys#void\376\003\000\000\002\376\001\000\000%\001##sys#foreign-ranged-integer-argu"
"ment\376\003\000\000\002\376\001\000\000.\001##sys#foreign-unsigned-ranged-integer-argument\376\003\000\000\002\376\001\000\000\021\001##sys#pe"
"ek-fixnum\376\003\000\000\002\376\001\000\000\016\001##sys#setislot\376\003\000\000\002\376\001\000\000\022\001##sys#poke-integer\376\003\000\000\002\376\001\000\000\020\001##sys#"
"permanent\077\376\003\000\000\002\376\001\000\000\014\001##sys#values\376\003\000\000\002\376\001\000\000\021\001##sys#poke-double\376\003\000\000\002\376\001\000\000\023\001##sys#in"
"tern-symbol\376\003\000\000\002\376\001\000\000\023\001##sys#null-pointer\077\376\003\000\000\002\376\001\000\000\017\001##sys#peek-byte\376\003\000\000\002\376\001\000\000\022\001##"
"sys#file-exists\077\376\003\000\000\002\376\001\000\000\025\001##sys#substring-index\376\003\000\000\002\376\001\000\000\030\001##sys#substring-index"
"-ci\376\003\000\000\002\376\001\000\000\011\001##sys#lcm\376\003\000\000\002\376\001\000\000\011\001##sys#gcd\376\377\016"));
lf[22]=C_h_intern(&lf[22],34, C_text("chicken.compiler.support#make-node"));
lf[23]=C_h_intern(&lf[23],11, C_text("##core#call"));
lf[24]=C_h_intern(&lf[24],6, C_text("fixnum"));
lf[25]=C_h_intern(&lf[25],36, C_text("chicken.compiler.support#number-type"));
lf[26]=C_h_intern(&lf[26],31, C_text("chicken.compiler.support#unsafe"));
lf[27]=C_h_intern(&lf[27],13, C_text("##core#inline"));
lf[28]=C_h_intern(&lf[28],22, C_text("##core#inline_allocate"));
lf[29]=C_h_intern(&lf[29],30, C_text("chicken.compiler.support#qnode"));
lf[30]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\005C_eqp\376\377\016"));
lf[31]=C_h_intern(&lf[31],5, C_text("quote"));
lf[32]=C_h_intern(&lf[32],35, C_text("chicken.compiler.support#immediate\077"));
lf[33]=C_h_intern(&lf[33],40, C_text("chicken.compiler.support#node-parameters"));
lf[34]=C_h_intern(&lf[34],35, C_text("chicken.compiler.support#node-class"));
lf[35]=C_h_intern(&lf[35],15, C_text("##core#variable"));
lf[36]=C_h_intern(&lf[36],3, C_text("map"));
lf[37]=C_h_intern(&lf[37],13, C_text("scheme#append"));
lf[38]=C_h_intern(&lf[38],20, C_text("chicken.base#butlast"));
lf[39]=C_h_intern(&lf[39],11, C_text("##core#proc"));
lf[40]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\007C_apply\376\003\000\000\002\376\377\006\001\376\377\016"));
lf[41]=C_h_intern(&lf[41],6, C_text("values"));
lf[42]=C_h_intern(&lf[42],12, C_text("##sys#values"));
lf[43]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\016C_apply_values\376\003\000\000\002\376\377\006\001\376\377\016"));
lf[44]=C_h_intern(&lf[44],35, C_text("chicken.compiler.support#intrinsic\077"));
lf[45]=C_h_intern(&lf[45],37, C_text("scheme#call-with-current-continuation"));
lf[46]=C_h_intern(&lf[46],34, C_text("chicken.compiler.optimizer#rewrite"));
lf[47]=C_h_intern(&lf[47],13, C_text("##core#lambda"));
lf[48]=C_h_intern(&lf[48],3, C_text("let"));
lf[49]=C_h_intern(&lf[49],32, C_text("chicken.compiler.support#varnode"));
lf[50]=C_h_intern(&lf[50],19, C_text("chicken.base#gensym"));
lf[51]=C_h_intern(&lf[51],2, C_text("f_"));
lf[52]=C_h_intern(&lf[52],34, C_text("chicken.compiler.support#debugging"));
lf[53]=C_h_intern(&lf[53],1, C_text("o"));
lf[54]=C_decode_literal(C_heaptop,C_text("\376B\000\000)removing single-valued `call-with-values\047"));
lf[55]=C_h_intern(&lf[55],1, C_text("r"));
lf[56]=C_h_intern(&lf[56],31, C_text("chicken.compiler.support#db-get"));
lf[57]=C_h_intern(&lf[57],5, C_text("value"));
lf[58]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\024C_fixnum_shift_right\376\377\016"));
lf[59]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\017C_fixnum_divide\376\377\016"));
lf[60]=C_h_intern(&lf[60],35, C_text("chicken.compiler.support#fold-inner"));
lf[61]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_a_i_vector"));
lf[62]=C_h_intern(&lf[62],16, C_text("##core#undefined"));
lf[63]=C_h_intern(&lf[63],16, C_text("inline-transient"));
lf[64]=C_h_intern(&lf[64],8, C_text("assigned"));
lf[65]=C_h_intern(&lf[65],10, C_text("references"));
lf[66]=C_h_intern(&lf[66],27, C_text("##sys#decompose-lambda-list"));
lf[67]=C_h_intern(&lf[67],17, C_text("##sys#get-keyword"));
lf[68]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017C_i_get_keyword"));
lf[69]=C_h_intern(&lf[69],27, C_text("chicken.keyword#get-keyword"));
lf[70]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017C_i_get_keyword"));
lf[71]=C_h_intern(&lf[71],33, C_text("chicken.string#substring-index-ci"));
lf[72]=C_h_intern(&lf[72],24, C_text("##sys#substring-index-ci"));
lf[73]=C_h_intern(&lf[73],30, C_text("chicken.string#substring-index"));
lf[74]=C_h_intern(&lf[74],21, C_text("##sys#substring-index"));
lf[75]=C_h_intern(&lf[75],29, C_text("chicken.string#substring-ci=\077"));
lf[76]=C_h_intern(&lf[76],20, C_text("##sys#substring-ci=\077"));
lf[77]=C_h_intern(&lf[77],26, C_text("chicken.string#substring=\077"));
lf[78]=C_h_intern(&lf[78],17, C_text("##sys#substring=\077"));
lf[79]=C_h_intern(&lf[79],17, C_text("scheme#write-char"));
lf[80]=C_h_intern(&lf[80],21, C_text("##sys#write-char/port"));
lf[81]=C_h_intern(&lf[81],21, C_text("##sys#standard-output"));
lf[82]=C_h_intern(&lf[82],16, C_text("scheme#read-char"));
lf[83]=C_h_intern(&lf[83],20, C_text("##sys#read-char/port"));
lf[84]=C_h_intern(&lf[84],20, C_text("##sys#standard-input"));
lf[85]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_i_fixnum_length"));
lf[86]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022C_i_integer_length"));
lf[87]=C_h_intern(&lf[87],30, C_text("chicken.bitwise#integer-length"));
lf[88]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_u_i_bit_to_bool"));
lf[89]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017C_i_bit_to_bool"));
lf[90]=C_h_intern(&lf[90],28, C_text("chicken.bitwise#bit->boolean"));
lf[91]=C_h_intern(&lf[91],31, C_text("chicken.base#current-error-port"));
lf[92]=C_h_intern(&lf[92],20, C_text("##sys#standard-error"));
lf[93]=C_h_intern(&lf[93],26, C_text("scheme#current-output-port"));
lf[94]=C_h_intern(&lf[94],25, C_text("scheme#current-input-port"));
lf[95]=C_h_intern(&lf[95],10, C_text("##sys#void"));
lf[96]=C_h_intern(&lf[96],21, C_text("##sys#undefined-value"));
lf[97]=C_h_intern(&lf[97],17, C_text("chicken.base#void"));
lf[98]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\012\001scheme#car\376\001\000\000\017\001scheme#set-car!\376\003\000\000\002\376\003\000\000\002\376\001\000\000\012\001scheme#cdr\376\001\000\000\017\001s"
"cheme#set-cdr!\376\003\000\000\002\376\003\000\000\002\376\001\000\000\021\001scheme#string-ref\376\001\000\000\022\001scheme#string-set!\376\003\000\000\002\376\003\000\000"
"\002\376\001\000\000\021\001scheme#vector-ref\376\001\000\000\022\001scheme#vector-set!\376\003\000\000\002\376\003\000\000\002\376\001\000\000\023\001srfi-4#u8vector-"
"ref\376\001\000\000\024\001srfi-4#u8vector-set!\376\003\000\000\002\376\003\000\000\002\376\001\000\000\023\001srfi-4#s8vector-ref\376\001\000\000\024\001srfi-4#s8v"
"ector-set!\376\003\000\000\002\376\003\000\000\002\376\001\000\000\024\001srfi-4#u16vector-ref\376\001\000\000\025\001srfi-4#u16vector-set!\376\003\000\000\002\376\003"
"\000\000\002\376\001\000\000\024\001srfi-4#s16vector-ref\376\001\000\000\025\001srfi-4#s16vector-set!\376\003\000\000\002\376\003\000\000\002\376\001\000\000\024\001srfi-4#u"
"32vector-ref\376\001\000\000\025\001srfi-4#u32vector-set!\376\003\000\000\002\376\003\000\000\002\376\001\000\000\024\001srfi-4#s32vector-ref\376\001\000\000\025"
"\001srfi-4#s32vector-set!\376\003\000\000\002\376\003\000\000\002\376\001\000\000\024\001srfi-4#u64vector-ref\376\001\000\000\025\001srfi-4#u64vector"
"-set!\376\003\000\000\002\376\003\000\000\002\376\001\000\000\024\001srfi-4#s64vector-ref\376\001\000\000\025\001srfi-4#s64vector-set!\376\003\000\000\002\376\003\000\000\002\376\001"
"\000\000\024\001srfi-4#f32vector-ref\376\001\000\000\025\001srfi-4#f32vector-set!\376\003\000\000\002\376\003\000\000\002\376\001\000\000\024\001srfi-4#f64vec"
"tor-ref\376\001\000\000\025\001srfi-4#f64vector-set!\376\003\000\000\002\376\003\000\000\002\376\001\000\000\035\001chicken.locative#locative-ref\376"
"\001\000\000\036\001chicken.locative#locative-set!\376\003\000\000\002\376\003\000\000\002\376\001\000\000\035\001chicken.memory#pointer-u8-ref"
"\376\001\000\000\036\001chicken.memory#pointer-u8-set!\376\003\000\000\002\376\003\000\000\002\376\001\000\000\035\001chicken.memory#pointer-s8-re"
"f\376\001\000\000\036\001chicken.memory#pointer-s8-set!\376\003\000\000\002\376\003\000\000\002\376\001\000\000\036\001chicken.memory#pointer-u16-"
"ref\376\001\000\000\037\001chicken.memory#pointer-u16-set!\376\003\000\000\002\376\003\000\000\002\376\001\000\000\036\001chicken.memory#pointer-s"
"16-ref\376\001\000\000\037\001chicken.memory#pointer-s16-set!\376\003\000\000\002\376\003\000\000\002\376\001\000\000\036\001chicken.memory#pointe"
"r-u32-ref\376\001\000\000\037\001chicken.memory#pointer-u32-set!\376\003\000\000\002\376\003\000\000\002\376\001\000\000\036\001chicken.memory#poi"
"nter-s32-ref\376\001\000\000\037\001chicken.memory#pointer-s32-set!\376\003\000\000\002\376\003\000\000\002\376\001\000\000\036\001chicken.memory#"
"pointer-f32-ref\376\001\000\000\037\001chicken.memory#pointer-f32-set!\376\003\000\000\002\376\003\000\000\002\376\001\000\000\036\001chicken.memo"
"ry#pointer-f64-ref\376\001\000\000\037\001chicken.memory#pointer-f64-set!\376\003\000\000\002\376\003\000\000\002\376\001\000\000\047\001chicken.m"
"emory.representation#block-ref\376\001\000\000(\001chicken.memory.representation#block-set!\376\377\016"));
lf[99]=C_h_intern(&lf[99],12, C_text("##sys#setter"));
lf[100]=C_h_intern(&lf[100],20, C_text("chicken.base#call/cc"));
lf[101]=C_h_intern(&lf[101],17, C_text("##sys#make-vector"));
lf[102]=C_h_intern(&lf[102],18, C_text("scheme#make-vector"));
lf[103]=C_h_intern(&lf[103],29, C_text("srfi-4#f64vector->blob/shared"));
lf[104]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006C_slot"));
lf[105]=C_h_intern(&lf[105],29, C_text("srfi-4#f32vector->blob/shared"));
lf[106]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006C_slot"));
lf[107]=C_h_intern(&lf[107],29, C_text("srfi-4#s64vector->blob/shared"));
lf[108]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006C_slot"));
lf[109]=C_h_intern(&lf[109],29, C_text("srfi-4#u64vector->blob/shared"));
lf[110]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006C_slot"));
lf[111]=C_h_intern(&lf[111],29, C_text("srfi-4#s32vector->blob/shared"));
lf[112]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006C_slot"));
lf[113]=C_h_intern(&lf[113],29, C_text("srfi-4#u32vector->blob/shared"));
lf[114]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006C_slot"));
lf[115]=C_h_intern(&lf[115],29, C_text("srfi-4#s16vector->blob/shared"));
lf[116]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006C_slot"));
lf[117]=C_h_intern(&lf[117],29, C_text("srfi-4#u16vector->blob/shared"));
lf[118]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006C_slot"));
lf[119]=C_h_intern(&lf[119],28, C_text("srfi-4#s8vector->blob/shared"));
lf[120]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006C_slot"));
lf[121]=C_h_intern(&lf[121],28, C_text("srfi-4#u8vector->blob/shared"));
lf[122]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006C_slot"));
lf[123]=C_h_intern(&lf[123],18, C_text("chicken.base#atom\077"));
lf[124]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_i_not_pair_p"));
lf[125]=C_h_intern(&lf[125],23, C_text("srfi-4#f64vector-length"));
lf[126]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024C_i_f64vector_length"));
lf[127]=C_decode_literal(C_heaptop,C_text("\376B\000\000\026C_u_i_f64vector_length"));
lf[128]=C_h_intern(&lf[128],23, C_text("srfi-4#f32vector-length"));
lf[129]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024C_i_f32vector_length"));
lf[130]=C_decode_literal(C_heaptop,C_text("\376B\000\000\026C_u_i_f32vector_length"));
lf[131]=C_h_intern(&lf[131],23, C_text("srfi-4#s64vector-length"));
lf[132]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024C_i_s64vector_length"));
lf[133]=C_decode_literal(C_heaptop,C_text("\376B\000\000\026C_u_i_s64vector_length"));
lf[134]=C_h_intern(&lf[134],23, C_text("srfi-4#u64vector-length"));
lf[135]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024C_i_u64vector_length"));
lf[136]=C_decode_literal(C_heaptop,C_text("\376B\000\000\026C_u_i_u64vector_length"));
lf[137]=C_h_intern(&lf[137],23, C_text("srfi-4#s32vector-length"));
lf[138]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024C_i_s32vector_length"));
lf[139]=C_decode_literal(C_heaptop,C_text("\376B\000\000\026C_u_i_s32vector_length"));
lf[140]=C_h_intern(&lf[140],23, C_text("srfi-4#u32vector-length"));
lf[141]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024C_i_u32vector_length"));
lf[142]=C_decode_literal(C_heaptop,C_text("\376B\000\000\026C_u_i_u32vector_length"));
lf[143]=C_h_intern(&lf[143],23, C_text("srfi-4#s16vector-length"));
lf[144]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024C_i_s16vector_length"));
lf[145]=C_decode_literal(C_heaptop,C_text("\376B\000\000\026C_u_i_s16vector_length"));
lf[146]=C_h_intern(&lf[146],23, C_text("srfi-4#u16vector-length"));
lf[147]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024C_i_u16vector_length"));
lf[148]=C_decode_literal(C_heaptop,C_text("\376B\000\000\026C_u_i_u16vector_length"));
lf[149]=C_h_intern(&lf[149],22, C_text("srfi-4#s8vector-length"));
lf[150]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023C_i_s8vector_length"));
lf[151]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025C_u_i_s8vector_length"));
lf[152]=C_h_intern(&lf[152],22, C_text("srfi-4#u8vector-length"));
lf[153]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023C_i_u8vector_length"));
lf[154]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025C_u_i_u8vector_length"));
lf[155]=C_h_intern(&lf[155],21, C_text("srfi-4#f64vector-set!"));
lf[156]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_i_f64vector_set"));
lf[157]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023C_u_i_f64vector_set"));
lf[158]=C_h_intern(&lf[158],21, C_text("srfi-4#f32vector-set!"));
lf[159]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_i_f32vector_set"));
lf[160]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023C_u_i_f32vector_set"));
lf[161]=C_h_intern(&lf[161],21, C_text("srfi-4#s64vector-set!"));
lf[162]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_i_s64vector_set"));
lf[163]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023C_u_i_s64vector_set"));
lf[164]=C_h_intern(&lf[164],21, C_text("srfi-4#u64vector-set!"));
lf[165]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_i_u64vector_set"));
lf[166]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023C_u_i_u64vector_set"));
lf[167]=C_h_intern(&lf[167],21, C_text("srfi-4#s32vector-set!"));
lf[168]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_i_s32vector_set"));
lf[169]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023C_u_i_s32vector_set"));
lf[170]=C_h_intern(&lf[170],21, C_text("srfi-4#u32vector-set!"));
lf[171]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_i_u32vector_set"));
lf[172]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023C_u_i_u32vector_set"));
lf[173]=C_h_intern(&lf[173],21, C_text("srfi-4#s16vector-set!"));
lf[174]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_i_s16vector_set"));
lf[175]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023C_u_i_s16vector_set"));
lf[176]=C_h_intern(&lf[176],21, C_text("srfi-4#u16vector-set!"));
lf[177]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_i_u16vector_set"));
lf[178]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023C_u_i_u16vector_set"));
lf[179]=C_h_intern(&lf[179],20, C_text("srfi-4#s8vector-set!"));
lf[180]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020C_i_s8vector_set"));
lf[181]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022C_u_i_s8vector_set"));
lf[182]=C_h_intern(&lf[182],20, C_text("srfi-4#u8vector-set!"));
lf[183]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020C_i_u8vector_set"));
lf[184]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022C_u_i_u8vector_set"));
lf[185]=C_h_intern(&lf[185],20, C_text("srfi-4#f64vector-ref"));
lf[186]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023C_a_i_f64vector_ref"));
lf[187]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025C_a_u_i_f64vector_ref"));
lf[188]=C_h_intern(&lf[188],20, C_text("srfi-4#f32vector-ref"));
lf[189]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023C_a_i_f32vector_ref"));
lf[190]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025C_a_u_i_f32vector_ref"));
lf[191]=C_h_intern(&lf[191],20, C_text("srfi-4#s32vector-ref"));
lf[192]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023C_a_i_s32vector_ref"));
lf[193]=C_h_intern(&lf[193],20, C_text("srfi-4#u32vector-ref"));
lf[194]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023C_a_i_u32vector_ref"));
lf[195]=C_h_intern(&lf[195],20, C_text("srfi-4#s16vector-ref"));
lf[196]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_i_s16vector_ref"));
lf[197]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023C_u_i_s16vector_ref"));
lf[198]=C_h_intern(&lf[198],20, C_text("srfi-4#u16vector-ref"));
lf[199]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_i_u16vector_ref"));
lf[200]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023C_u_i_u16vector_ref"));
lf[201]=C_h_intern(&lf[201],19, C_text("srfi-4#s8vector-ref"));
lf[202]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020C_i_s8vector_ref"));
lf[203]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022C_u_i_s8vector_ref"));
lf[204]=C_h_intern(&lf[204],19, C_text("srfi-4#u8vector-ref"));
lf[205]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020C_i_u8vector_ref"));
lf[206]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022C_u_i_u8vector_ref"));
lf[207]=C_h_intern(&lf[207],22, C_text("chicken.blob#blob-size"));
lf[208]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_block_size"));
lf[209]=C_h_intern(&lf[209],46, C_text("##sys#foreign-unsigned-ranged-integer-argument"));
lf[210]=C_decode_literal(C_heaptop,C_text("\376B\000\000-C_i_foreign_unsigned_ranged_integer_argumentp"));
lf[211]=C_h_intern(&lf[211],37, C_text("##sys#foreign-ranged-integer-argument"));
lf[212]=C_decode_literal(C_heaptop,C_text("\376B\000\000$C_i_foreign_ranged_integer_argumentp"));
lf[213]=C_h_intern(&lf[213],30, C_text("##sys#foreign-pointer-argument"));
lf[214]=C_decode_literal(C_heaptop,C_text("\376B\000\000\035C_i_foreign_pointer_argumentp"));
lf[215]=C_h_intern(&lf[215],29, C_text("##sys#foreign-string-argument"));
lf[216]=C_decode_literal(C_heaptop,C_text("\376B\000\000\034C_i_foreign_string_argumentp"));
lf[217]=C_h_intern(&lf[217],37, C_text("##sys#foreign-struct-wrapper-argument"));
lf[218]=C_decode_literal(C_heaptop,C_text("\376B\000\000$C_i_foreign_struct_wrapper_argumentp"));
lf[219]=C_h_intern(&lf[219],28, C_text("##sys#foreign-block-argument"));
lf[220]=C_decode_literal(C_heaptop,C_text("\376B\000\000\033C_i_foreign_block_argumentp"));
lf[221]=C_h_intern(&lf[221],29, C_text("##sys#foreign-flonum-argument"));
lf[222]=C_decode_literal(C_heaptop,C_text("\376B\000\000\034C_i_foreign_flonum_argumentp"));
lf[223]=C_h_intern(&lf[223],27, C_text("##sys#foreign-char-argument"));
lf[224]=C_decode_literal(C_heaptop,C_text("\376B\000\000\032C_i_foreign_char_argumentp"));
lf[225]=C_h_intern(&lf[225],29, C_text("##sys#foreign-fixnum-argument"));
lf[226]=C_decode_literal(C_heaptop,C_text("\376B\000\000\034C_i_foreign_fixnum_argumentp"));
lf[227]=C_h_intern(&lf[227],30, C_text("chicken.locative#locative-set!"));
lf[228]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020C_i_locative_set"));
lf[229]=C_h_intern(&lf[229],33, C_text("chicken.locative#locative->object"));
lf[230]=C_decode_literal(C_heaptop,C_text("\376B\000\000\026C_i_locative_to_object"));
lf[231]=C_h_intern(&lf[231],16, C_text("##sys#immediate\077"));
lf[232]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006C_immp"));
lf[233]=C_h_intern(&lf[233],19, C_text("##sys#null-pointer\077"));
lf[234]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017C_null_pointerp"));
lf[235]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017C_null_pointerp"));
lf[236]=C_h_intern(&lf[236],16, C_text("##sys#permanent\077"));
lf[237]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_permanentp"));
lf[238]=C_h_intern(&lf[238],18, C_text("scheme#string-ci=\077"));
lf[239]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025C_i_string_ci_equal_p"));
lf[240]=C_h_intern(&lf[240],15, C_text("scheme#string=\077"));
lf[241]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022C_i_string_equal_p"));
lf[242]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024C_u_i_string_equal_p"));
lf[243]=C_h_intern(&lf[243],17, C_text("##sys#poke-double"));
lf[244]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015C_poke_double"));
lf[245]=C_h_intern(&lf[245],18, C_text("##sys#poke-integer"));
lf[246]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_poke_integer"));
lf[247]=C_h_intern(&lf[247],14, C_text("##sys#setislot"));
lf[248]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_i_set_i_slot"));
lf[249]=C_h_intern(&lf[249],30, C_text("chicken.memory#pointer->object"));
lf[250]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023C_pointer_to_object"));
lf[251]=C_h_intern(&lf[251],15, C_text("##sys#peek-byte"));
lf[252]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013C_peek_byte"));
lf[253]=C_h_intern(&lf[253],17, C_text("##sys#peek-fixnum"));
lf[254]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015C_peek_fixnum"));
lf[255]=C_h_intern(&lf[255],13, C_text("##sys#setbyte"));
lf[256]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011C_setbyte"));
lf[257]=C_h_intern(&lf[257],10, C_text("##sys#byte"));
lf[258]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011C_subbyte"));
lf[259]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\033C_i_fixnum_arithmetic_shift\376\377\016"));
lf[260]=C_decode_literal(C_heaptop,C_text("\376B\000\000\030C_s_a_i_arithmetic_shift"));
lf[261]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\024C_fixnum_shift_right\376\377\016"));
lf[262]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\023C_fixnum_shift_left\376\377\016"));
lf[263]=C_h_intern(&lf[263],36, C_text("chicken.compiler.support#big-fixnum\077"));
lf[264]=C_h_intern(&lf[264],32, C_text("chicken.bitwise#arithmetic-shift"));
lf[265]=C_h_intern(&lf[265],20, C_text("chicken.fixnum#fxrem"));
lf[266]=C_decode_literal(C_heaptop,C_text("\376B\000\000\034C_i_fixnum_remainder_checked"));
lf[267]=C_h_intern(&lf[267],20, C_text("chicken.fixnum#fxmod"));
lf[268]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017C_fixnum_modulo"));
lf[269]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_u_fixnum_modulo"));
lf[270]=C_h_intern(&lf[270],18, C_text("chicken.fixnum#fx/"));
lf[271]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017C_fixnum_divide"));
lf[272]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_u_fixnum_divide"));
lf[273]=C_h_intern(&lf[273],20, C_text("chicken.fixnum#fxior"));
lf[274]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013C_fixnum_or"));
lf[275]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015C_u_fixnum_or"));
lf[276]=C_h_intern(&lf[276],20, C_text("chicken.fixnum#fxand"));
lf[277]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_fixnum_and"));
lf[278]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_u_fixnum_and"));
lf[279]=C_h_intern(&lf[279],20, C_text("chicken.fixnum#fxxor"));
lf[280]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_fixnum_xor"));
lf[281]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_fixnum_xor"));
lf[282]=C_h_intern(&lf[282],20, C_text("chicken.fixnum#fxneg"));
lf[283]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017C_fixnum_negate"));
lf[284]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_u_fixnum_negate"));
lf[285]=C_h_intern(&lf[285],20, C_text("chicken.fixnum#fxshr"));
lf[286]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024C_fixnum_shift_right"));
lf[287]=C_h_intern(&lf[287],20, C_text("chicken.fixnum#fxshl"));
lf[288]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023C_fixnum_shift_left"));
lf[289]=C_h_intern(&lf[289],18, C_text("chicken.fixnum#fx-"));
lf[290]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023C_fixnum_difference"));
lf[291]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025C_u_fixnum_difference"));
lf[292]=C_h_intern(&lf[292],18, C_text("chicken.fixnum#fx+"));
lf[293]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015C_fixnum_plus"));
lf[294]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017C_u_fixnum_plus"));
lf[295]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\016C_i_set_i_slot\376\377\016"));
lf[296]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\013C_i_setslot\376\377\016"));
lf[297]=C_h_intern(&lf[297],13, C_text("##sys#setslot"));
lf[298]=C_h_intern(&lf[298],30, C_text("chicken.memory#pointer-f64-ref"));
lf[299]=C_decode_literal(C_heaptop,C_text("\376B\000\000\027C_a_u_i_pointer_f64_ref"));
lf[300]=C_h_intern(&lf[300],30, C_text("chicken.memory#pointer-f32-ref"));
lf[301]=C_decode_literal(C_heaptop,C_text("\376B\000\000\027C_a_u_i_pointer_f32_ref"));
lf[302]=C_h_intern(&lf[302],30, C_text("chicken.memory#pointer-s32-ref"));
lf[303]=C_decode_literal(C_heaptop,C_text("\376B\000\000\027C_a_u_i_pointer_s32_ref"));
lf[304]=C_h_intern(&lf[304],30, C_text("chicken.memory#pointer-u32-ref"));
lf[305]=C_decode_literal(C_heaptop,C_text("\376B\000\000\027C_a_u_i_pointer_u32_ref"));
lf[306]=C_h_intern(&lf[306],31, C_text("chicken.memory#pointer-f64-set!"));
lf[307]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025C_u_i_pointer_f64_set"));
lf[308]=C_h_intern(&lf[308],31, C_text("chicken.memory#pointer-f32-set!"));
lf[309]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025C_u_i_pointer_f32_set"));
lf[310]=C_h_intern(&lf[310],31, C_text("chicken.memory#pointer-s32-set!"));
lf[311]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025C_u_i_pointer_s32_set"));
lf[312]=C_h_intern(&lf[312],31, C_text("chicken.memory#pointer-u32-set!"));
lf[313]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025C_u_i_pointer_u32_set"));
lf[314]=C_h_intern(&lf[314],31, C_text("chicken.memory#pointer-s16-set!"));
lf[315]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025C_u_i_pointer_s16_set"));
lf[316]=C_h_intern(&lf[316],31, C_text("chicken.memory#pointer-u16-set!"));
lf[317]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025C_u_i_pointer_u16_set"));
lf[318]=C_h_intern(&lf[318],30, C_text("chicken.memory#pointer-s8-set!"));
lf[319]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024C_u_i_pointer_s8_set"));
lf[320]=C_h_intern(&lf[320],30, C_text("chicken.memory#pointer-u8-set!"));
lf[321]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024C_u_i_pointer_u8_set"));
lf[322]=C_h_intern(&lf[322],30, C_text("chicken.memory#pointer-s16-ref"));
lf[323]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025C_u_i_pointer_s16_ref"));
lf[324]=C_h_intern(&lf[324],30, C_text("chicken.memory#pointer-u16-ref"));
lf[325]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025C_u_i_pointer_u16_ref"));
lf[326]=C_h_intern(&lf[326],29, C_text("chicken.memory#pointer-s8-ref"));
lf[327]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024C_u_i_pointer_s8_ref"));
lf[328]=C_h_intern(&lf[328],29, C_text("chicken.memory#pointer-u8-ref"));
lf[329]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024C_u_i_pointer_u8_ref"));
lf[330]=C_h_intern(&lf[330],29, C_text("chicken.locative#locative-ref"));
lf[331]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022C_a_i_locative_ref"));
lf[332]=C_h_intern(&lf[332],23, C_text("chicken.memory#pointer+"));
lf[333]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023C_a_u_i_pointer_inc"));
lf[334]=C_h_intern(&lf[334],31, C_text("chicken.memory#pointer->address"));
lf[335]=C_decode_literal(C_heaptop,C_text("\376B\000\000\030C_a_i_pointer_to_address"));
lf[336]=C_h_intern(&lf[336],31, C_text("chicken.memory#address->pointer"));
lf[337]=C_decode_literal(C_heaptop,C_text("\376B\000\000\030C_a_i_address_to_pointer"));
lf[338]=C_h_intern(&lf[338],13, C_text("scheme#string"));
lf[339]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_a_i_string"));
lf[340]=C_h_intern(&lf[340],20, C_text("##sys#make-structure"));
lf[341]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_a_i_record"));
lf[342]=C_h_intern(&lf[342],12, C_text("##sys#vector"));
lf[343]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_a_i_vector"));
lf[344]=C_h_intern(&lf[344],13, C_text("scheme#vector"));
lf[345]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_a_i_vector"));
lf[346]=C_h_intern(&lf[346],10, C_text("##sys#list"));
lf[347]=C_decode_literal(C_heaptop,C_text("\376B\000\000\012C_a_i_list"));
lf[348]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\377\001\000\000\000\000\376\003\000\000\002\376\377\001\000\000\000\003\376\377\016"));
lf[349]=C_h_intern(&lf[349],11, C_text("scheme#list"));
lf[350]=C_decode_literal(C_heaptop,C_text("\376B\000\000\012C_a_i_list"));
lf[351]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\377\001\000\000\000\000\376\003\000\000\002\376\377\001\000\000\000\003\376\377\016"));
lf[352]=C_h_intern(&lf[352],10, C_text("##sys#cons"));
lf[353]=C_decode_literal(C_heaptop,C_text("\376B\000\000\012C_a_i_cons"));
lf[354]=C_h_intern(&lf[354],11, C_text("scheme#cons"));
lf[355]=C_decode_literal(C_heaptop,C_text("\376B\000\000\012C_a_i_cons"));
lf[356]=C_h_intern(&lf[356],22, C_text("chicken.flonum#fpround"));
lf[357]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022C_a_i_flonum_floor"));
lf[358]=C_h_intern(&lf[358],24, C_text("chicken.flonum#fpceiling"));
lf[359]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024C_a_i_flonum_ceiling"));
lf[360]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022C_a_i_flonum_round"));
lf[361]=C_h_intern(&lf[361],25, C_text("chicken.flonum#fptruncate"));
lf[362]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025C_a_i_flonum_truncate"));
lf[363]=C_h_intern(&lf[363],20, C_text("chicken.flonum#fpabs"));
lf[364]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020C_a_i_flonum_abs"));
lf[365]=C_h_intern(&lf[365],21, C_text("chicken.flonum#fpsqrt"));
lf[366]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_a_i_flonum_sqrt"));
lf[367]=C_h_intern(&lf[367],20, C_text("chicken.flonum#fplog"));
lf[368]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020C_a_i_flonum_log"));
lf[369]=C_h_intern(&lf[369],21, C_text("chicken.flonum#fpexpt"));
lf[370]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_a_i_flonum_expt"));
lf[371]=C_h_intern(&lf[371],20, C_text("chicken.flonum#fpexp"));
lf[372]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020C_a_i_flonum_exp"));
lf[373]=C_h_intern(&lf[373],22, C_text("chicken.flonum#fpatan2"));
lf[374]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022C_a_i_flonum_atan2"));
lf[375]=C_h_intern(&lf[375],21, C_text("chicken.flonum#fpatan"));
lf[376]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_a_i_flonum_atan"));
lf[377]=C_h_intern(&lf[377],21, C_text("chicken.flonum#fpacos"));
lf[378]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_a_i_flonum_acos"));
lf[379]=C_h_intern(&lf[379],21, C_text("chicken.flonum#fpasin"));
lf[380]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_a_i_flonum_asin"));
lf[381]=C_h_intern(&lf[381],20, C_text("chicken.flonum#fptan"));
lf[382]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020C_a_i_flonum_tan"));
lf[383]=C_h_intern(&lf[383],20, C_text("chicken.flonum#fpcos"));
lf[384]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020C_a_i_flonum_cos"));
lf[385]=C_h_intern(&lf[385],20, C_text("chicken.flonum#fpsin"));
lf[386]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020C_a_i_flonum_sin"));
lf[387]=C_h_intern(&lf[387],15, C_text("scheme#truncate"));
lf[388]=C_h_intern(&lf[388],6, C_text("flonum"));
lf[389]=C_h_intern(&lf[389],14, C_text("scheme#ceiling"));
lf[390]=C_h_intern(&lf[390],12, C_text("scheme#floor"));
lf[391]=C_h_intern(&lf[391],22, C_text("chicken.flonum#fpfloor"));
lf[392]=C_h_intern(&lf[392],22, C_text("chicken.fixnum#fxeven\077"));
lf[393]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017C_i_fixnumevenp"));
lf[394]=C_h_intern(&lf[394],21, C_text("chicken.fixnum#fxodd\077"));
lf[395]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_i_fixnumoddp"));
lf[396]=C_h_intern(&lf[396],11, C_text("scheme#odd\077"));
lf[397]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010C_i_oddp"));
lf[398]=C_h_intern(&lf[398],12, C_text("scheme#even\077"));
lf[399]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011C_i_evenp"));
lf[400]=C_h_intern(&lf[400],16, C_text("scheme#remainder"));
lf[401]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017C_fixnum_modulo"));
lf[402]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017C_fixnum_modulo"));
lf[403]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_i_fixnumoddp"));
lf[404]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_i_fixnumoddp"));
lf[405]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017C_i_fixnumevenp"));
lf[406]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017C_i_fixnumevenp"));
lf[407]=C_h_intern(&lf[407],17, C_text("##sys#make-symbol"));
lf[408]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015C_make_symbol"));
lf[409]=C_h_intern(&lf[409],19, C_text("##sys#intern-symbol"));
lf[410]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022C_string_to_symbol"));
lf[411]=C_h_intern(&lf[411],20, C_text("##sys#context-switch"));
lf[412]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020C_context_switch"));
lf[413]=C_h_intern(&lf[413],31, C_text("chicken.platform#return-to-host"));
lf[414]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020C_return_to_host"));
lf[415]=C_h_intern(&lf[415],25, C_text("##sys#ensure-heap-reserve"));
lf[416]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025C_ensure_heap_reserve"));
lf[417]=C_h_intern(&lf[417],21, C_text("##sys#allocate-vector"));
lf[418]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_allocate_vector"));
lf[419]=C_h_intern(&lf[419],36, C_text("##sys#call-with-current-continuation"));
lf[420]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011C_call_cc"));
lf[421]=C_h_intern(&lf[421],21, C_text("scheme#number->string"));
lf[422]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\377\001\000\000\000\001\376\377\001\000\000\000\002"));
lf[423]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022C_number_to_string"));
lf[424]=C_h_intern(&lf[424],8, C_text("scheme#-"));
lf[425]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\377\001\000\000\000\001\376\377\006\000"));
lf[426]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007C_minus"));
lf[427]=C_h_intern(&lf[427],8, C_text("scheme#+"));
lf[428]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006C_plus"));
lf[429]=C_h_intern(&lf[429],8, C_text("scheme#\052"));
lf[430]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007C_times"));
lf[431]=C_h_intern(&lf[431],9, C_text("scheme#<="));
lf[432]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_less_or_equal_p"));
lf[433]=C_h_intern(&lf[433],9, C_text("scheme#>="));
lf[434]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024C_greater_or_equal_p"));
lf[435]=C_h_intern(&lf[435],8, C_text("scheme#<"));
lf[436]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007C_lessp"));
lf[437]=C_h_intern(&lf[437],8, C_text("scheme#>"));
lf[438]=C_decode_literal(C_heaptop,C_text("\376B\000\000\012C_greaterp"));
lf[439]=C_h_intern(&lf[439],8, C_text("scheme#="));
lf[440]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011C_nequalp"));
lf[441]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022C_i_less_or_equalp"));
lf[442]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025C_i_greater_or_equalp"));
lf[443]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011C_i_lessp"));
lf[444]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_i_greaterp"));
lf[445]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013C_i_nequalp"));
lf[446]=C_h_intern(&lf[446],13, C_text("scheme#modulo"));
lf[447]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_s_a_i_modulo"));
lf[448]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_s_a_i_remainder"));
lf[449]=C_h_intern(&lf[449],15, C_text("scheme#quotient"));
lf[450]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020C_s_a_i_quotient"));
lf[451]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015C_s_a_i_times"));
lf[452]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015C_s_a_i_minus"));
lf[453]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_s_a_i_plus"));
lf[454]=C_h_intern(&lf[454],8, C_text("scheme#/"));
lf[455]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\024C_fixnum_shift_right\376\377\016"));
lf[456]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\017C_fixnum_divide\376\377\016"));
lf[457]=C_h_intern(&lf[457],9, C_text("##sys#/-2"));
lf[458]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\021C_u_fixnum_negate\376\377\016"));
lf[459]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\017C_fixnum_negate\376\377\016"));
lf[460]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\025C_u_fixnum_difference\376\377\016"));
lf[461]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\023C_fixnum_difference\376\377\016"));
lf[462]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\017C_u_fixnum_plus\376\377\016"));
lf[463]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\015C_fixnum_plus\376\377\016"));
lf[464]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\017C_u_fixnum_plus\376\377\016"));
lf[465]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\015C_fixnum_plus\376\377\016"));
lf[466]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\023C_fixnum_shift_left\376\377\016"));
lf[467]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\016C_fixnum_times\376\377\016"));
lf[468]=C_h_intern(&lf[468],10, C_text("scheme#lcm"));
lf[469]=C_h_intern(&lf[469],10, C_text("scheme#gcd"));
lf[470]=C_h_intern(&lf[470],21, C_text("chicken.base#identity"));
lf[471]=C_h_intern(&lf[471],9, C_text("##sys#lcm"));
lf[472]=C_h_intern(&lf[472],9, C_text("##sys#gcd"));
lf[473]=C_h_intern(&lf[473],18, C_text("scheme#vector-set!"));
lf[474]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_i_vector_set"));
lf[475]=C_h_intern(&lf[475],19, C_text("scheme#list->string"));
lf[476]=C_h_intern(&lf[476],18, C_text("##sys#list->string"));
lf[477]=C_h_intern(&lf[477],19, C_text("scheme#string->list"));
lf[478]=C_h_intern(&lf[478],18, C_text("##sys#string->list"));
lf[479]=C_h_intern(&lf[479],20, C_text("scheme#string-append"));
lf[480]=C_h_intern(&lf[480],19, C_text("##sys#string-append"));
lf[481]=C_h_intern(&lf[481],16, C_text("scheme#substring"));
lf[482]=C_h_intern(&lf[482],15, C_text("##sys#substring"));
lf[483]=C_h_intern(&lf[483],50, C_text("chicken.memory.representation#make-record-instance"));
lf[484]=C_h_intern(&lf[484],16, C_text("##sys#block-set!"));
lf[485]=C_h_intern(&lf[485],40, C_text("chicken.memory.representation#block-set!"));
lf[486]=C_h_intern(&lf[486],10, C_text("scheme#map"));
lf[487]=C_h_intern(&lf[487],9, C_text("##sys#map"));
lf[488]=C_h_intern(&lf[488],15, C_text("scheme#for-each"));
lf[489]=C_h_intern(&lf[489],14, C_text("##sys#for-each"));
lf[490]=C_h_intern(&lf[490],6, C_text("setter"));
lf[491]=C_decode_literal(C_heaptop,C_text("\376B\000\000\030C_fixnum_less_or_equal_p"));
lf[492]=C_decode_literal(C_heaptop,C_text("\376B\000\000\030C_flonum_less_or_equal_p"));
lf[493]=C_decode_literal(C_heaptop,C_text("\376B\000\000\033C_fixnum_greater_or_equal_p"));
lf[494]=C_decode_literal(C_heaptop,C_text("\376B\000\000\033C_flonum_greater_or_equal_p"));
lf[495]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_fixnum_lessp"));
lf[496]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_flonum_lessp"));
lf[497]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_fixnum_greaterp"));
lf[498]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_flonum_greaterp"));
lf[499]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005C_eqp"));
lf[500]=C_decode_literal(C_heaptop,C_text("\376B\000\000\012C_i_equalp"));
lf[501]=C_h_intern(&lf[501],16, C_text("##sys#check-char"));
lf[502]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020C_i_check_char_2"));
lf[503]=C_h_intern(&lf[503],21, C_text("##sys#check-structure"));
lf[504]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025C_i_check_structure_2"));
lf[505]=C_h_intern(&lf[505],18, C_text("##sys#check-vector"));
lf[506]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022C_i_check_vector_2"));
lf[507]=C_h_intern(&lf[507],23, C_text("##sys#check-byte-vector"));
lf[508]=C_decode_literal(C_heaptop,C_text("\376B\000\000\026C_i_check_bytevector_2"));
lf[509]=C_h_intern(&lf[509],18, C_text("##sys#check-string"));
lf[510]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022C_i_check_string_2"));
lf[511]=C_h_intern(&lf[511],18, C_text("##sys#check-symbol"));
lf[512]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022C_i_check_symbol_2"));
lf[513]=C_h_intern(&lf[513],20, C_text("##sys#check-locative"));
lf[514]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024C_i_check_locative_2"));
lf[515]=C_h_intern(&lf[515],19, C_text("##sys#check-boolean"));
lf[516]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023C_i_check_boolean_2"));
lf[517]=C_h_intern(&lf[517],16, C_text("##sys#check-pair"));
lf[518]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020C_i_check_pair_2"));
lf[519]=C_h_intern(&lf[519],16, C_text("##sys#check-list"));
lf[520]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020C_i_check_list_2"));
lf[521]=C_h_intern(&lf[521],18, C_text("##sys#check-number"));
lf[522]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022C_i_check_number_2"));
lf[523]=C_h_intern(&lf[523],18, C_text("##sys#check-fixnum"));
lf[524]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022C_i_check_fixnum_2"));
lf[525]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_i_check_char"));
lf[526]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023C_i_check_structure"));
lf[527]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020C_i_check_vector"));
lf[528]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024C_i_check_bytevector"));
lf[529]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020C_i_check_string"));
lf[530]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020C_i_check_symbol"));
lf[531]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022C_i_check_locative"));
lf[532]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_i_check_boolean"));
lf[533]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_i_check_pair"));
lf[534]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_i_check_list"));
lf[535]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020C_i_check_number"));
lf[536]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020C_i_check_fixnum"));
lf[537]=C_h_intern(&lf[537],20, C_text("scheme#string-length"));
lf[538]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_i_string_length"));
lf[539]=C_h_intern(&lf[539],19, C_text("##sys#vector-length"));
lf[540]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_i_vector_length"));
lf[541]=C_h_intern(&lf[541],20, C_text("scheme#vector-length"));
lf[542]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_i_vector_length"));
lf[543]=C_h_intern(&lf[543],20, C_text("scheme#integer->char"));
lf[544]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020C_make_character"));
lf[545]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007C_unfix"));
lf[546]=C_h_intern(&lf[546],20, C_text("scheme#char->integer"));
lf[547]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005C_fix"));
lf[548]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020C_character_code"));
lf[549]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005C_fix"));
lf[550]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015C_header_size"));
lf[551]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005C_fix"));
lf[552]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015C_header_size"));
lf[553]=C_h_intern(&lf[553],16, C_text("scheme#negative\077"));
lf[554]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015C_i_negativep"));
lf[555]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_flonum_lessp"));
lf[556]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_fixnum_lessp"));
lf[557]=C_h_intern(&lf[557],16, C_text("scheme#positive\077"));
lf[558]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015C_i_positivep"));
lf[559]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_flonum_greaterp"));
lf[560]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_fixnum_greaterp"));
lf[561]=C_h_intern(&lf[561],12, C_text("scheme#zero\077"));
lf[562]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011C_i_zerop"));
lf[563]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_u_i_zerop2"));
lf[564]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005C_eqp"));
lf[565]=C_h_intern(&lf[565],20, C_text("chicken.flonum#fpgcd"));
lf[566]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020C_a_i_flonum_gcd"));
lf[567]=C_h_intern(&lf[567],20, C_text("chicken.flonum#fpneg"));
lf[568]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023C_a_i_flonum_negate"));
lf[569]=C_h_intern(&lf[569],19, C_text("chicken.flonum#fp/\077"));
lf[570]=C_decode_literal(C_heaptop,C_text("\376B\000\000\035C_a_i_flonum_quotient_checked"));
lf[571]=C_h_intern(&lf[571],18, C_text("chicken.flonum#fp/"));
lf[572]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025C_a_i_flonum_quotient"));
lf[573]=C_h_intern(&lf[573],18, C_text("chicken.flonum#fp\052"));
lf[574]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022C_a_i_flonum_times"));
lf[575]=C_h_intern(&lf[575],18, C_text("chicken.flonum#fp-"));
lf[576]=C_decode_literal(C_heaptop,C_text("\376B\000\000\027C_a_i_flonum_difference"));
lf[577]=C_h_intern(&lf[577],18, C_text("chicken.flonum#fp+"));
lf[578]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_a_i_flonum_plus"));
lf[579]=C_h_intern(&lf[579],27, C_text("chicken.bitwise#bitwise-not"));
lf[580]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023C_s_a_i_bitwise_not"));
lf[581]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_fixnum_not"));
lf[582]=C_h_intern(&lf[582],27, C_text("chicken.bitwise#bitwise-ior"));
lf[583]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013C_fixnum_or"));
lf[584]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015C_u_fixnum_or"));
lf[585]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023C_s_a_i_bitwise_ior"));
lf[586]=C_h_intern(&lf[586],27, C_text("chicken.bitwise#bitwise-xor"));
lf[587]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_fixnum_xor"));
lf[588]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_fixnum_xor"));
lf[589]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023C_s_a_i_bitwise_xor"));
lf[590]=C_h_intern(&lf[590],27, C_text("chicken.bitwise#bitwise-and"));
lf[591]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_fixnum_and"));
lf[592]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_u_fixnum_and"));
lf[593]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023C_s_a_i_bitwise_and"));
lf[594]=C_h_intern(&lf[594],10, C_text("scheme#abs"));
lf[595]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_fixnum_abs"));
lf[596]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_fixnum_abs"));
lf[597]=C_h_intern(&lf[597],15, C_text("scheme#set-cdr!"));
lf[598]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013C_i_set_cdr"));
lf[599]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015C_u_i_set_cdr"));
lf[600]=C_h_intern(&lf[600],15, C_text("scheme#set-car!"));
lf[601]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013C_i_set_car"));
lf[602]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015C_u_i_set_car"));
lf[603]=C_h_intern(&lf[603],13, C_text("scheme#member"));
lf[604]=C_decode_literal(C_heaptop,C_text("\376B\000\000\012C_i_member"));
lf[605]=C_h_intern(&lf[605],12, C_text("scheme#assoc"));
lf[606]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011C_i_assoc"));
lf[607]=C_h_intern(&lf[607],11, C_text("scheme#memq"));
lf[608]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010C_i_memq"));
lf[609]=C_decode_literal(C_heaptop,C_text("\376B\000\000\012C_u_i_memq"));
lf[610]=C_h_intern(&lf[610],11, C_text("scheme#assq"));
lf[611]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010C_i_assq"));
lf[612]=C_decode_literal(C_heaptop,C_text("\376B\000\000\012C_u_i_assq"));
lf[613]=C_h_intern(&lf[613],11, C_text("scheme#memv"));
lf[614]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010C_i_memv"));
lf[615]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010C_i_memq"));
lf[616]=C_decode_literal(C_heaptop,C_text("\376B\000\000\012C_u_i_memq"));
lf[617]=C_h_intern(&lf[617],11, C_text("scheme#assv"));
lf[618]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010C_i_assv"));
lf[619]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010C_i_assq"));
lf[620]=C_decode_literal(C_heaptop,C_text("\376B\000\000\012C_u_i_assq"));
lf[621]=C_h_intern(&lf[621],45, C_text("chicken.memory.representation#number-of-slots"));
lf[622]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_block_size"));
lf[623]=C_h_intern(&lf[623],39, C_text("chicken.memory.representation#block-ref"));
lf[624]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006C_slot"));
lf[625]=C_h_intern(&lf[625],17, C_text("##sys#bytevector\077"));
lf[626]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015C_bytevectorp"));
lf[627]=C_h_intern(&lf[627],16, C_text("##sys#structure\077"));
lf[628]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_i_structurep"));
lf[629]=C_h_intern(&lf[629],16, C_text("scheme#list-tail"));
lf[630]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015C_i_list_tail"));
lf[631]=C_h_intern(&lf[631],20, C_text("scheme#char-downcase"));
lf[632]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023C_u_i_char_downcase"));
lf[633]=C_h_intern(&lf[633],18, C_text("scheme#char-upcase"));
lf[634]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_u_i_char_upcase"));
lf[635]=C_h_intern(&lf[635],23, C_text("scheme#char-lower-case\077"));
lf[636]=C_decode_literal(C_heaptop,C_text("\376B\000\000\026C_u_i_char_lower_casep"));
lf[637]=C_h_intern(&lf[637],23, C_text("scheme#char-upper-case\077"));
lf[638]=C_decode_literal(C_heaptop,C_text("\376B\000\000\026C_u_i_char_upper_casep"));
lf[639]=C_h_intern(&lf[639],23, C_text("scheme#char-whitespace\077"));
lf[640]=C_decode_literal(C_heaptop,C_text("\376B\000\000\026C_u_i_char_whitespacep"));
lf[641]=C_h_intern(&lf[641],23, C_text("scheme#char-alphabetic\077"));
lf[642]=C_decode_literal(C_heaptop,C_text("\376B\000\000\026C_u_i_char_alphabeticp"));
lf[643]=C_h_intern(&lf[643],20, C_text("scheme#char-numeric\077"));
lf[644]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023C_u_i_char_numericp"));
lf[645]=C_h_intern(&lf[645],20, C_text("chicken.fixnum#fxlen"));
lf[646]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_i_fixnum_length"));
lf[647]=C_h_intern(&lf[647],20, C_text("chicken.fixnum#fxgcd"));
lf[648]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_i_fixnum_gcd"));
lf[649]=C_h_intern(&lf[649],20, C_text("chicken.flonum#fpmin"));
lf[650]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_i_flonum_min"));
lf[651]=C_h_intern(&lf[651],20, C_text("chicken.flonum#fpmax"));
lf[652]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_i_flonum_max"));
lf[653]=C_h_intern(&lf[653],20, C_text("chicken.fixnum#fxmin"));
lf[654]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_i_fixnum_min"));
lf[655]=C_h_intern(&lf[655],20, C_text("chicken.fixnum#fxmax"));
lf[656]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_i_fixnum_max"));
lf[657]=C_h_intern(&lf[657],19, C_text("chicken.flonum#fp<="));
lf[658]=C_decode_literal(C_heaptop,C_text("\376B\000\000\030C_flonum_less_or_equal_p"));
lf[659]=C_h_intern(&lf[659],19, C_text("chicken.flonum#fp>="));
lf[660]=C_decode_literal(C_heaptop,C_text("\376B\000\000\033C_flonum_greater_or_equal_p"));
lf[661]=C_h_intern(&lf[661],18, C_text("chicken.flonum#fp<"));
lf[662]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_flonum_lessp"));
lf[663]=C_h_intern(&lf[663],18, C_text("chicken.flonum#fp>"));
lf[664]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_flonum_greaterp"));
lf[665]=C_h_intern(&lf[665],18, C_text("chicken.flonum#fp="));
lf[666]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017C_flonum_equalp"));
lf[667]=C_h_intern(&lf[667],19, C_text("chicken.fixnum#fx<="));
lf[668]=C_decode_literal(C_heaptop,C_text("\376B\000\000\030C_fixnum_less_or_equal_p"));
lf[669]=C_h_intern(&lf[669],19, C_text("chicken.fixnum#fx>="));
lf[670]=C_decode_literal(C_heaptop,C_text("\376B\000\000\033C_fixnum_greater_or_equal_p"));
lf[671]=C_h_intern(&lf[671],18, C_text("chicken.fixnum#fx<"));
lf[672]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_fixnum_lessp"));
lf[673]=C_h_intern(&lf[673],18, C_text("chicken.fixnum#fx>"));
lf[674]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_fixnum_greaterp"));
lf[675]=C_h_intern(&lf[675],18, C_text("chicken.fixnum#fx="));
lf[676]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005C_eqp"));
lf[677]=C_h_intern(&lf[677],19, C_text("chicken.fixnum#fx/\077"));
lf[678]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025C_i_o_fixnum_quotient"));
lf[679]=C_h_intern(&lf[679],19, C_text("chicken.fixnum#fx\052\077"));
lf[680]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022C_i_o_fixnum_times"));
lf[681]=C_h_intern(&lf[681],19, C_text("chicken.fixnum#fx-\077"));
lf[682]=C_decode_literal(C_heaptop,C_text("\376B\000\000\027C_i_o_fixnum_difference"));
lf[683]=C_h_intern(&lf[683],19, C_text("chicken.fixnum#fx+\077"));
lf[684]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_i_o_fixnum_plus"));
lf[685]=C_h_intern(&lf[685],18, C_text("chicken.fixnum#fx\052"));
lf[686]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_fixnum_times"));
lf[687]=C_h_intern(&lf[687],20, C_text("chicken.fixnum#fxnot"));
lf[688]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_fixnum_not"));
lf[689]=C_h_intern(&lf[689],10, C_text("##sys#size"));
lf[690]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_block_size"));
lf[691]=C_h_intern(&lf[691],15, C_text("##sys#block-ref"));
lf[692]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015C_i_block_ref"));
lf[693]=C_h_intern(&lf[693],10, C_text("##sys#slot"));
lf[694]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006C_slot"));
lf[695]=C_h_intern(&lf[695],14, C_text("scheme#char<=\077"));
lf[696]=C_decode_literal(C_heaptop,C_text("\376B\000\000\030C_i_char_less_or_equal_p"));
lf[697]=C_decode_literal(C_heaptop,C_text("\376B\000\000\032C_u_i_char_less_or_equal_p"));
lf[698]=C_h_intern(&lf[698],14, C_text("scheme#char>=\077"));
lf[699]=C_decode_literal(C_heaptop,C_text("\376B\000\000\033C_i_char_greater_or_equal_p"));
lf[700]=C_decode_literal(C_heaptop,C_text("\376B\000\000\035C_u_i_char_greater_or_equal_p"));
lf[701]=C_h_intern(&lf[701],13, C_text("scheme#char<\077"));
lf[702]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_i_char_lessp"));
lf[703]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020C_u_i_char_lessp"));
lf[704]=C_h_intern(&lf[704],13, C_text("scheme#char>\077"));
lf[705]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_i_char_greaterp"));
lf[706]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023C_u_i_char_greaterp"));
lf[707]=C_h_intern(&lf[707],13, C_text("scheme#char=\077"));
lf[708]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017C_i_char_equalp"));
lf[709]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_u_i_char_equalp"));
lf[710]=C_h_intern(&lf[710],17, C_text("scheme#vector-ref"));
lf[711]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_i_vector_ref"));
lf[712]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006C_slot"));
lf[713]=C_h_intern(&lf[713],18, C_text("scheme#string-set!"));
lf[714]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_i_string_set"));
lf[715]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_setsubchar"));
lf[716]=C_h_intern(&lf[716],17, C_text("scheme#string-ref"));
lf[717]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_i_string_ref"));
lf[718]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011C_subchar"));
lf[719]=C_h_intern(&lf[719],18, C_text("scheme#eof-object\077"));
lf[720]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006C_eofp"));
lf[721]=C_h_intern(&lf[721],12, C_text("scheme#list\077"));
lf[722]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011C_i_listp"));
lf[723]=C_h_intern(&lf[723],15, C_text("scheme#inexact\077"));
lf[724]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_u_i_inexactp"));
lf[725]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_i_inexactp"));
lf[726]=C_h_intern(&lf[726],13, C_text("scheme#exact\077"));
lf[727]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_u_i_exactp"));
lf[728]=C_decode_literal(C_heaptop,C_text("\376B\000\000\012C_i_exactp"));
lf[729]=C_h_intern(&lf[729],24, C_text("##sys#generic-structure\077"));
lf[730]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_structurep"));
lf[731]=C_h_intern(&lf[731],8, C_text("pointer\077"));
lf[732]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_i_safe_pointerp"));
lf[733]=C_h_intern(&lf[733],14, C_text("##sys#pointer\077"));
lf[734]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015C_anypointerp"));
lf[735]=C_h_intern(&lf[735],25, C_text("chicken.flonum#fpinteger\077"));
lf[736]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020C_u_i_fpintegerp"));
lf[737]=C_h_intern(&lf[737],22, C_text("chicken.base#infinite\077"));
lf[738]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015C_i_infinitep"));
lf[739]=C_h_intern(&lf[739],20, C_text("chicken.base#finite\077"));
lf[740]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013C_i_finitep"));
lf[741]=C_h_intern(&lf[741],17, C_text("chicken.base#nan\077"));
lf[742]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010C_i_nanp"));
lf[743]=C_h_intern(&lf[743],20, C_text("chicken.base#ratnum\077"));
lf[744]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013C_i_ratnump"));
lf[745]=C_h_intern(&lf[745],21, C_text("chicken.base#cplxnum\077"));
lf[746]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_i_cplxnump"));
lf[747]=C_h_intern(&lf[747],20, C_text("chicken.base#bignum\077"));
lf[748]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013C_i_bignump"));
lf[749]=C_h_intern(&lf[749],20, C_text("chicken.base#fixnum\077"));
lf[750]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011C_fixnump"));
lf[751]=C_h_intern(&lf[751],20, C_text("chicken.base#flonum\077"));
lf[752]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013C_i_flonump"));
lf[753]=C_h_intern(&lf[753],27, C_text("chicken.base#exact-integer\077"));
lf[754]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022C_i_exact_integerp"));
lf[755]=C_h_intern(&lf[755],15, C_text("scheme#integer\077"));
lf[756]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_i_integerp"));
lf[757]=C_h_intern(&lf[757],12, C_text("scheme#real\077"));
lf[758]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011C_i_realp"));
lf[759]=C_h_intern(&lf[759],16, C_text("scheme#rational\077"));
lf[760]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015C_i_rationalp"));
lf[761]=C_h_intern(&lf[761],15, C_text("scheme#complex\077"));
lf[762]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013C_i_numberp"));
lf[763]=C_h_intern(&lf[763],14, C_text("scheme#number\077"));
lf[764]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013C_i_numberp"));
lf[765]=C_h_intern(&lf[765],15, C_text("scheme#boolean\077"));
lf[766]=C_decode_literal(C_heaptop,C_text("\376B\000\000\012C_booleanp"));
lf[767]=C_h_intern(&lf[767],18, C_text("chicken.base#port\077"));
lf[768]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011C_i_portp"));
lf[769]=C_h_intern(&lf[769],17, C_text("scheme#procedure\077"));
lf[770]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_i_closurep"));
lf[771]=C_h_intern(&lf[771],11, C_text("##sys#pair\077"));
lf[772]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011C_i_pairp"));
lf[773]=C_h_intern(&lf[773],12, C_text("scheme#pair\077"));
lf[774]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011C_i_pairp"));
lf[775]=C_h_intern(&lf[775],17, C_text("srfi-4#f64vector\077"));
lf[776]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_i_f64vectorp"));
lf[777]=C_h_intern(&lf[777],17, C_text("srfi-4#f32vector\077"));
lf[778]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_i_f32vectorp"));
lf[779]=C_h_intern(&lf[779],17, C_text("srfi-4#s64vector\077"));
lf[780]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_i_s64vectorp"));
lf[781]=C_h_intern(&lf[781],17, C_text("srfi-4#u64vector\077"));
lf[782]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_i_u64vectorp"));
lf[783]=C_h_intern(&lf[783],17, C_text("srfi-4#s32vector\077"));
lf[784]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_i_s32vectorp"));
lf[785]=C_h_intern(&lf[785],17, C_text("srfi-4#u32vector\077"));
lf[786]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_i_u32vectorp"));
lf[787]=C_h_intern(&lf[787],17, C_text("srfi-4#s16vector\077"));
lf[788]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_i_s16vectorp"));
lf[789]=C_h_intern(&lf[789],17, C_text("srfi-4#u16vector\077"));
lf[790]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_i_u16vectorp"));
lf[791]=C_h_intern(&lf[791],16, C_text("srfi-4#s8vector\077"));
lf[792]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015C_i_s8vectorp"));
lf[793]=C_h_intern(&lf[793],16, C_text("srfi-4#u8vector\077"));
lf[794]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015C_i_u8vectorp"));
lf[795]=C_h_intern(&lf[795],20, C_text("##sys#srfi-4-vector\077"));
lf[796]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022C_i_srfi_4_vectorp"));
lf[797]=C_h_intern(&lf[797],13, C_text("##sys#vector\077"));
lf[798]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013C_i_vectorp"));
lf[799]=C_h_intern(&lf[799],14, C_text("scheme#vector\077"));
lf[800]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013C_i_vectorp"));
lf[801]=C_h_intern(&lf[801],14, C_text("scheme#symbol\077"));
lf[802]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013C_i_symbolp"));
lf[803]=C_h_intern(&lf[803],26, C_text("chicken.locative#locative\077"));
lf[804]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015C_i_locativep"));
lf[805]=C_h_intern(&lf[805],14, C_text("scheme#string\077"));
lf[806]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013C_i_stringp"));
lf[807]=C_h_intern(&lf[807],12, C_text("scheme#char\077"));
lf[808]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007C_charp"));
lf[809]=C_h_intern(&lf[809],10, C_text("scheme#not"));
lf[810]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007C_i_not"));
lf[811]=C_h_intern(&lf[811],13, C_text("scheme#length"));
lf[812]=C_decode_literal(C_heaptop,C_text("\376B\000\000\012C_i_length"));
lf[813]=C_h_intern(&lf[813],11, C_text("##sys#null\077"));
lf[814]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011C_i_nullp"));
lf[815]=C_h_intern(&lf[815],12, C_text("scheme#null\077"));
lf[816]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011C_i_nullp"));
lf[817]=C_h_intern(&lf[817],15, C_text("scheme#list-ref"));
lf[818]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_i_list_ref"));
lf[819]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016C_u_i_list_ref"));
lf[820]=C_h_intern(&lf[820],10, C_text("##sys#eqv\077"));
lf[821]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010C_i_eqvp"));
lf[822]=C_h_intern(&lf[822],11, C_text("scheme#eqv\077"));
lf[823]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010C_i_eqvp"));
lf[824]=C_h_intern(&lf[824],9, C_text("##sys#eq\077"));
lf[825]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005C_eqp"));
lf[826]=C_h_intern(&lf[826],10, C_text("scheme#eq\077"));
lf[827]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005C_eqp"));
lf[828]=C_h_intern(&lf[828],10, C_text("scheme#cdr"));
lf[829]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007C_i_cdr"));
lf[830]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011C_u_i_cdr"));
lf[831]=C_h_intern(&lf[831],13, C_text("scheme#cddddr"));
lf[832]=C_decode_literal(C_heaptop,C_text("\376B\000\000\012C_i_cddddr"));
lf[833]=C_h_intern(&lf[833],12, C_text("scheme#cdddr"));
lf[834]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011C_i_cdddr"));
lf[835]=C_h_intern(&lf[835],11, C_text("scheme#cddr"));
lf[836]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010C_i_cddr"));
lf[837]=C_h_intern(&lf[837],11, C_text("scheme#cdar"));
lf[838]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010C_i_cdar"));
lf[839]=C_h_intern(&lf[839],11, C_text("scheme#caar"));
lf[840]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010C_i_caar"));
lf[841]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_u_i_cddddr"));
lf[842]=C_h_intern(&lf[842],13, C_text("scheme#cdddar"));
lf[843]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_u_i_cdddar"));
lf[844]=C_h_intern(&lf[844],13, C_text("scheme#cddadr"));
lf[845]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_u_i_cddadr"));
lf[846]=C_h_intern(&lf[846],13, C_text("scheme#cddaar"));
lf[847]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_u_i_cddaar"));
lf[848]=C_h_intern(&lf[848],13, C_text("scheme#cdaddr"));
lf[849]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_u_i_cdaddr"));
lf[850]=C_h_intern(&lf[850],13, C_text("scheme#cdadar"));
lf[851]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_u_i_cdadar"));
lf[852]=C_h_intern(&lf[852],13, C_text("scheme#cdaadr"));
lf[853]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_u_i_cdaadr"));
lf[854]=C_h_intern(&lf[854],13, C_text("scheme#cdaaar"));
lf[855]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_u_i_cdaaar"));
lf[856]=C_h_intern(&lf[856],13, C_text("scheme#cadddr"));
lf[857]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_u_i_cadddr"));
lf[858]=C_h_intern(&lf[858],13, C_text("scheme#caddar"));
lf[859]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_u_i_caddar"));
lf[860]=C_h_intern(&lf[860],13, C_text("scheme#cadadr"));
lf[861]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_u_i_cadadr"));
lf[862]=C_h_intern(&lf[862],13, C_text("scheme#cadaar"));
lf[863]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_u_i_cadaar"));
lf[864]=C_h_intern(&lf[864],13, C_text("scheme#caaddr"));
lf[865]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_u_i_caaddr"));
lf[866]=C_h_intern(&lf[866],13, C_text("scheme#caadar"));
lf[867]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_u_i_caadar"));
lf[868]=C_h_intern(&lf[868],13, C_text("scheme#caaaar"));
lf[869]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_u_i_caaaar"));
lf[870]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013C_u_i_cdddr"));
lf[871]=C_h_intern(&lf[871],12, C_text("scheme#cddar"));
lf[872]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013C_u_i_cddar"));
lf[873]=C_h_intern(&lf[873],12, C_text("scheme#cdadr"));
lf[874]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013C_u_i_cdadr"));
lf[875]=C_h_intern(&lf[875],12, C_text("scheme#cdaar"));
lf[876]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013C_u_i_cdaar"));
lf[877]=C_h_intern(&lf[877],12, C_text("scheme#caddr"));
lf[878]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013C_u_i_caddr"));
lf[879]=C_h_intern(&lf[879],12, C_text("scheme#cadar"));
lf[880]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013C_u_i_cadar"));
lf[881]=C_h_intern(&lf[881],12, C_text("scheme#caaar"));
lf[882]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013C_u_i_caaar"));
lf[883]=C_decode_literal(C_heaptop,C_text("\376B\000\000\012C_u_i_cddr"));
lf[884]=C_decode_literal(C_heaptop,C_text("\376B\000\000\012C_u_i_cdar"));
lf[885]=C_decode_literal(C_heaptop,C_text("\376B\000\000\012C_u_i_caar"));
lf[886]=C_h_intern(&lf[886],39, C_text("chicken.continuation#continuation-graft"));
lf[887]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024C_continuation_graft"));
lf[888]=C_h_intern(&lf[888],22, C_text("##sys#call-with-values"));
lf[889]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022C_call_with_values"));
lf[890]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024C_u_call_with_values"));
lf[891]=C_h_intern(&lf[891],23, C_text("scheme#call-with-values"));
lf[892]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022C_call_with_values"));
lf[893]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024C_u_call_with_values"));
lf[894]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010C_values"));
lf[895]=C_h_intern(&lf[895],13, C_text("scheme#values"));
lf[896]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010C_values"));
lf[897]=C_decode_literal(C_heaptop,C_text("\376B\000\000\012C_i_cadddr"));
lf[898]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_u_i_cadddr"));
lf[899]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011C_i_caddr"));
lf[900]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013C_u_i_caddr"));
lf[901]=C_h_intern(&lf[901],11, C_text("scheme#cadr"));
lf[902]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010C_i_cadr"));
lf[903]=C_decode_literal(C_heaptop,C_text("\376B\000\000\012C_u_i_cadr"));
lf[904]=C_h_intern(&lf[904],9, C_text("##sys#cdr"));
lf[905]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007C_i_cdr"));
lf[906]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011C_u_i_cdr"));
lf[907]=C_h_intern(&lf[907],9, C_text("##sys#car"));
lf[908]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007C_i_car"));
lf[909]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011C_u_i_car"));
lf[910]=C_h_intern(&lf[910],10, C_text("scheme#car"));
lf[911]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007C_i_car"));
lf[912]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011C_u_i_car"));
lf[913]=C_h_intern(&lf[913],11, C_text("##sys#apply"));
lf[914]=C_h_intern(&lf[914],12, C_text("scheme#apply"));
lf[915]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\012C_i_equalp\376\377\016"));
lf[916]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\005C_eqp\376\377\016"));
lf[917]=C_h_intern(&lf[917],13, C_text("scheme#equal\077"));
lf[918]=C_h_intern(&lf[918],17, C_text("chicken.base#sub1"));
lf[919]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_fixnum_decrease"));
lf[920]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023C_u_fixnum_decrease"));
lf[921]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015C_s_a_i_minus"));
lf[922]=C_h_intern(&lf[922],17, C_text("chicken.base#add1"));
lf[923]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021C_fixnum_increase"));
lf[924]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023C_u_fixnum_increase"));
lf[925]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014C_s_a_i_plus"));
lf[926]=C_h_intern(&lf[926],38, C_text("chicken.compiler.support#mark-variable"));
lf[927]=C_h_intern(&lf[927],15, C_text("##compiler#pure"));
lf[928]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\012\001##sys#slot\376\003\000\000\002\376\001\000\000\017\001##sys#block-ref\376\003\000\000\002\376\001\000\000\012\001##sys#size\376\003\000\000\002\376\001\000\000\012\001#"
"#sys#byte\376\003\000\000\002\376\001\000\000\016\001##sys#pointer\077\376\003\000\000\002\376\001\000\000\030\001##sys#generic-structure\077\376\003\000\000\002\376\001\000\000\020\001"
"##sys#immediate\077\376\003\000\000\002\376\001\000\000\021\001##sys#bytevector\077\376\003\000\000\002\376\001\000\000\013\001##sys#pair\077\376\003\000\000\002\376\001\000\000\011\001##s"
"ys#eq\077\376\003\000\000\002\376\001\000\000\013\001##sys#list\077\376\003\000\000\002\376\001\000\000\015\001##sys#vector\077\376\003\000\000\002\376\001\000\000\012\001##sys#eqv\077\376\003\000\000\002\376\001"
"\000\000\021\001##sys#get-keyword\376\003\000\000\002\376\001\000\000\012\001##sys#void\376\003\000\000\002\376\001\000\000\020\001##sys#permanent\077\376\377\016"));
lf[929]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\022\001chicken.fixnum#fx\052\376\003\000\000\002\376\001\000\000\023\001chicken.fixnum#fx\052\077\376\003\000\000\002\376\001\000\000\022\001chicken.fi"
"xnum#fx+\376\003\000\000\002\376\001\000\000\023\001chicken.fixnum#fx+\077\376\003\000\000\002\376\001\000\000\022\001chicken.fixnum#fx-\376\003\000\000\002\376\001\000\000\023\001ch"
"icken.fixnum#fx-\077\376\003\000\000\002\376\001\000\000\022\001chicken.fixnum#fx/\376\003\000\000\002\376\001\000\000\023\001chicken.fixnum#fx/\077\376\003\000\000"
"\002\376\001\000\000\022\001chicken.fixnum#fx<\376\003\000\000\002\376\001\000\000\023\001chicken.fixnum#fx<=\376\003\000\000\002\376\001\000\000\022\001chicken.fixnum"
"#fx=\376\003\000\000\002\376\001\000\000\022\001chicken.fixnum#fx>\376\003\000\000\002\376\001\000\000\023\001chicken.fixnum#fx>=\376\003\000\000\002\376\001\000\000\024\001chicke"
"n.fixnum#fxand\376\003\000\000\002\376\001\000\000\026\001chicken.fixnum#fxeven\077\376\003\000\000\002\376\001\000\000\024\001chicken.fixnum#fxgcd\376\003"
"\000\000\002\376\001\000\000\024\001chicken.fixnum#fxior\376\003\000\000\002\376\001\000\000\024\001chicken.fixnum#fxlen\376\003\000\000\002\376\001\000\000\024\001chicken.f"
"ixnum#fxmax\376\003\000\000\002\376\001\000\000\024\001chicken.fixnum#fxmin\376\003\000\000\002\376\001\000\000\024\001chicken.fixnum#fxmod\376\003\000\000\002\376\001"
"\000\000\024\001chicken.fixnum#fxneg\376\003\000\000\002\376\001\000\000\024\001chicken.fixnum#fxnot\376\003\000\000\002\376\001\000\000\025\001chicken.fixnum"
"#fxodd\077\376\003\000\000\002\376\001\000\000\024\001chicken.fixnum#fxrem\376\003\000\000\002\376\001\000\000\024\001chicken.fixnum#fxshl\376\003\000\000\002\376\001\000\000\024\001"
"chicken.fixnum#fxshr\376\003\000\000\002\376\001\000\000\024\001chicken.fixnum#fxxor\376\377\016"));
lf[930]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\023\001chicken.flonum#fp/\077\376\003\000\000\002\376\001\000\000\022\001chicken.flonum#fp+\376\003\000\000\002\376\001\000\000\022\001chicken.fl"
"onum#fp-\376\003\000\000\002\376\001\000\000\022\001chicken.flonum#fp\052\376\003\000\000\002\376\001\000\000\022\001chicken.flonum#fp/\376\003\000\000\002\376\001\000\000\022\001chi"
"cken.flonum#fp>\376\003\000\000\002\376\001\000\000\022\001chicken.flonum#fp<\376\003\000\000\002\376\001\000\000\022\001chicken.flonum#fp=\376\003\000\000\002\376\001"
"\000\000\023\001chicken.flonum#fp>=\376\003\000\000\002\376\001\000\000\023\001chicken.flonum#fp<=\376\003\000\000\002\376\001\000\000\024\001chicken.flonum#f"
"pmin\376\003\000\000\002\376\001\000\000\024\001chicken.flonum#fpmax\376\003\000\000\002\376\001\000\000\024\001chicken.flonum#fpneg\376\003\000\000\002\376\001\000\000\024\001chi"
"cken.flonum#fpgcd\376\003\000\000\002\376\001\000\000\026\001chicken.flonum#fpfloor\376\003\000\000\002\376\001\000\000\030\001chicken.flonum#fpce"
"iling\376\003\000\000\002\376\001\000\000\031\001chicken.flonum#fptruncate\376\003\000\000\002\376\001\000\000\026\001chicken.flonum#fpround\376\003\000\000\002\376"
"\001\000\000\024\001chicken.flonum#fpsin\376\003\000\000\002\376\001\000\000\024\001chicken.flonum#fpcos\376\003\000\000\002\376\001\000\000\024\001chicken.flonu"
"m#fptan\376\003\000\000\002\376\001\000\000\025\001chicken.flonum#fpasin\376\003\000\000\002\376\001\000\000\025\001chicken.flonum#fpacos\376\003\000\000\002\376\001\000\000"
"\025\001chicken.flonum#fpatan\376\003\000\000\002\376\001\000\000\026\001chicken.flonum#fpatan2\376\003\000\000\002\376\001\000\000\024\001chicken.flonu"
"m#fpexp\376\003\000\000\002\376\001\000\000\025\001chicken.flonum#fpexpt\376\003\000\000\002\376\001\000\000\024\001chicken.flonum#fplog\376\003\000\000\002\376\001\000\000\025"
"\001chicken.flonum#fpsqrt\376\003\000\000\002\376\001\000\000\024\001chicken.flonum#fpabs\376\003\000\000\002\376\001\000\000\031\001chicken.flonum#f"
"pinteger\077\376\377\016"));
lf[931]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\024\001chicken.base#bignum\077\376\003\000\000\002\376\001\000\000\025\001chicken.base#cplxnum\077\376\003\000\000\002\376\001\000\000\024\001chicke"
"n.base#fixnum\077\376\003\000\000\002\376\001\000\000\024\001chicken.base#flonum\077\376\003\000\000\002\376\001\000\000\024\001chicken.base#ratnum\077\376\003\000\000"
"\002\376\001\000\000\021\001chicken.base#add1\376\003\000\000\002\376\001\000\000\021\001chicken.base#sub1\376\003\000\000\002\376\001\000\000\021\001chicken.base#nan\077"
"\376\003\000\000\002\376\001\000\000\024\001chicken.base#finite\077\376\003\000\000\002\376\001\000\000\026\001chicken.base#infinite\077\376\003\000\000\002\376\001\000\000\023\001chick"
"en.base#gensym\376\003\000\000\002\376\001\000\000\021\001chicken.base#void\376\003\000\000\002\376\001\000\000\022\001chicken.base#print\376\003\000\000\002\376\001\000\000"
"\023\001chicken.base#print\052\376\003\000\000\002\376\001\000\000\022\001chicken.base#error\376\003\000\000\002\376\001\000\000\024\001chicken.base#call/c"
"c\376\003\000\000\002\376\001\000\000\026\001chicken.base#char-name\376\003\000\000\002\376\001\000\000\037\001chicken.base#current-error-port\376\003\000\000"
"\002\376\001\000\000\032\001chicken.base#symbol-append\376\003\000\000\002\376\001\000\000\022\001chicken.base#foldl\376\003\000\000\002\376\001\000\000\022\001chicken"
".base#foldr\376\003\000\000\002\376\001\000\000\023\001chicken.base#setter\376\003\000\000\002\376\001\000\000\037\001chicken.base#getter-with-set"
"ter\376\003\000\000\002\376\001\000\000\024\001chicken.base#equal=\077\376\003\000\000\002\376\001\000\000\033\001chicken.base#exact-integer\077\376\003\000\000\002\376\001\000"
"\000\031\001chicken.base#flush-output\376\003\000\000\002\376\001\000\000\025\001chicken.base#identity\376\003\000\000\002\376\001\000\000\016\001chicken.b"
"ase#o\376\003\000\000\002\376\001\000\000\022\001chicken.base#atom\077\376\003\000\000\002\376\001\000\000\026\001chicken.base#alist-ref\376\003\000\000\002\376\001\000\000\023\001ch"
"icken.base#rassoc\376\003\000\000\002\376\001\000\000\036\001chicken.bitwise#integer-length\376\003\000\000\002\376\001\000\000\033\001chicken.bit"
"wise#bitwise-and\376\003\000\000\002\376\001\000\000\033\001chicken.bitwise#bitwise-not\376\003\000\000\002\376\001\000\000\033\001chicken.bitwise"
"#bitwise-ior\376\003\000\000\002\376\001\000\000\033\001chicken.bitwise#bitwise-xor\376\003\000\000\002\376\001\000\000 \001chicken.bitwise#ari"
"thmetic-shift\376\003\000\000\002\376\001\000\000\034\001chicken.bitwise#bit->boolean\376\003\000\000\002\376\001\000\000\026\001chicken.blob#blob"
"-size\376\003\000\000\002\376\001\000\000\023\001chicken.blob#blob=\077\376\003\000\000\002\376\001\000\000\033\001chicken.keyword#get-keyword\376\003\000\000\002\376\001"
"\000\000\020\001srfi-4#u8vector\077\376\003\000\000\002\376\001\000\000\020\001srfi-4#s8vector\077\376\003\000\000\002\376\001\000\000\021\001srfi-4#u16vector\077\376\003\000\000\002"
"\376\001\000\000\021\001srfi-4#s16vector\077\376\003\000\000\002\376\001\000\000\021\001srfi-4#u32vector\077\376\003\000\000\002\376\001\000\000\021\001srfi-4#u64vector\077\376"
"\003\000\000\002\376\001\000\000\021\001srfi-4#s32vector\077\376\003\000\000\002\376\001\000\000\021\001srfi-4#s64vector\077\376\003\000\000\002\376\001\000\000\021\001srfi-4#f32vect"
"or\077\376\003\000\000\002\376\001\000\000\021\001srfi-4#f64vector\077\376\003\000\000\002\376\001\000\000\026\001srfi-4#u8vector-length\376\003\000\000\002\376\001\000\000\026\001srfi-"
"4#s8vector-length\376\003\000\000\002\376\001\000\000\027\001srfi-4#u16vector-length\376\003\000\000\002\376\001\000\000\027\001srfi-4#s16vector-l"
"ength\376\003\000\000\002\376\001\000\000\027\001srfi-4#u32vector-length\376\003\000\000\002\376\001\000\000\027\001srfi-4#u64vector-length\376\003\000\000\002\376\001"
"\000\000\027\001srfi-4#s32vector-length\376\003\000\000\002\376\001\000\000\027\001srfi-4#s64vector-length\376\003\000\000\002\376\001\000\000\027\001srfi-4#f"
"32vector-length\376\003\000\000\002\376\001\000\000\027\001srfi-4#f64vector-length\376\003\000\000\002\376\001\000\000\023\001srfi-4#u8vector-ref\376"
"\003\000\000\002\376\001\000\000\023\001srfi-4#s8vector-ref\376\003\000\000\002\376\001\000\000\024\001srfi-4#u16vector-ref\376\003\000\000\002\376\001\000\000\024\001srfi-4#s1"
"6vector-ref\376\003\000\000\002\376\001\000\000\024\001srfi-4#u32vector-ref\376\003\000\000\002\376\001\000\000\024\001srfi-4#u64vector-ref\376\003\000\000\002\376\001"
"\000\000\024\001srfi-4#s32vector-ref\376\003\000\000\002\376\001\000\000\024\001srfi-4#s64vector-ref\376\003\000\000\002\376\001\000\000\024\001srfi-4#f32vect"
"or-ref\376\003\000\000\002\376\001\000\000\024\001srfi-4#f64vector-ref\376\003\000\000\002\376\001\000\000\024\001srfi-4#u8vector-set!\376\003\000\000\002\376\001\000\000\024\001s"
"rfi-4#s8vector-set!\376\003\000\000\002\376\001\000\000\025\001srfi-4#u16vector-set!\376\003\000\000\002\376\001\000\000\025\001srfi-4#s16vector-s"
"et!\376\003\000\000\002\376\001\000\000\025\001srfi-4#u32vector-set!\376\003\000\000\002\376\001\000\000\025\001srfi-4#u64vector-set!\376\003\000\000\002\376\001\000\000\025\001sr"
"fi-4#s32vector-set!\376\003\000\000\002\376\001\000\000\025\001srfi-4#s64vector-set!\376\003\000\000\002\376\001\000\000\025\001srfi-4#f32vector-s"
"et!\376\003\000\000\002\376\001\000\000\025\001srfi-4#f64vector-set!\376\003\000\000\002\376\001\000\000\034\001srfi-4#u8vector->blob/shared\376\003\000\000\002\376"
"\001\000\000\034\001srfi-4#s8vector->blob/shared\376\003\000\000\002\376\001\000\000\035\001srfi-4#u16vector->blob/shared\376\003\000\000\002\376\001"
"\000\000\035\001srfi-4#s16vector->blob/shared\376\003\000\000\002\376\001\000\000\035\001srfi-4#u32vector->blob/shared\376\003\000\000\002\376\001"
"\000\000\035\001srfi-4#s32vector->blob/shared\376\003\000\000\002\376\001\000\000\035\001srfi-4#u64vector->blob/shared\376\003\000\000\002\376\001"
"\000\000\035\001srfi-4#s64vector->blob/shared\376\003\000\000\002\376\001\000\000\035\001srfi-4#f32vector->blob/shared\376\003\000\000\002\376\001"
"\000\000\035\001srfi-4#f64vector->blob/shared\376\003\000\000\002\376\001\000\000\034\001srfi-4#blob->u8vector/shared\376\003\000\000\002\376\001\000"
"\000\034\001srfi-4#blob->s8vector/shared\376\003\000\000\002\376\001\000\000\035\001srfi-4#blob->u16vector/shared\376\003\000\000\002\376\001\000\000"
"\035\001srfi-4#blob->s16vector/shared\376\003\000\000\002\376\001\000\000\035\001srfi-4#blob->u32vector/shared\376\003\000\000\002\376\001\000\000"
"\035\001srfi-4#blob->s32vector/shared\376\003\000\000\002\376\001\000\000\035\001srfi-4#blob->u64vector/shared\376\003\000\000\002\376\001\000\000"
"\035\001srfi-4#blob->s64vector/shared\376\003\000\000\002\376\001\000\000\035\001srfi-4#blob->f32vector/shared\376\003\000\000\002\376\001\000\000"
"\035\001srfi-4#blob->f64vector/shared\376\003\000\000\002\376\001\000\000\033\001chicken.memory#u8vector-ref\376\003\000\000\002\376\001\000\000\033\001"
"chicken.memory#s8vector-ref\376\003\000\000\002\376\001\000\000\034\001chicken.memory#u16vector-ref\376\003\000\000\002\376\001\000\000\034\001chi"
"cken.memory#s16vector-ref\376\003\000\000\002\376\001\000\000\034\001chicken.memory#u32vector-ref\376\003\000\000\002\376\001\000\000\034\001chick"
"en.memory#s32vector-ref\376\003\000\000\002\376\001\000\000\034\001chicken.memory#u64vector-ref\376\003\000\000\002\376\001\000\000\034\001chicken"
".memory#s64vector-ref\376\003\000\000\002\376\001\000\000\034\001chicken.memory#f32vector-ref\376\003\000\000\002\376\001\000\000\034\001chicken.m"
"emory#f64vector-ref\376\003\000\000\002\376\001\000\000\035\001chicken.memory#f32vector-set!\376\003\000\000\002\376\001\000\000\035\001chicken.me"
"mory#f64vector-set!\376\003\000\000\002\376\001\000\000\034\001chicken.memory#u8vector-set!\376\003\000\000\002\376\001\000\000\034\001chicken.mem"
"ory#s8vector-set!\376\003\000\000\002\376\001\000\000\035\001chicken.memory#u16vector-set!\376\003\000\000\002\376\001\000\000\035\001chicken.memo"
"ry#s16vector-set!\376\003\000\000\002\376\001\000\000\035\001chicken.memory#u32vector-set!\376\003\000\000\002\376\001\000\000\035\001chicken.memo"
"ry#s32vector-set!\376\003\000\000\002\376\001\000\000\035\001chicken.memory#u64vector-set!\376\003\000\000\002\376\001\000\000\035\001chicken.memo"
"ry#s64vector-set!\376\003\000\000\002\376\001\000\000-\001chicken.memory.representation#number-of-slots\376\003\000\000\002\376\001"
"\000\0002\001chicken.memory.representation#make-record-instance\376\003\000\000\002\376\001\000\000\047\001chicken.memory."
"representation#block-ref\376\003\000\000\002\376\001\000\000(\001chicken.memory.representation#block-set!\376\003\000\000\002"
"\376\001\000\000\035\001chicken.locative#locative-ref\376\003\000\000\002\376\001\000\000\036\001chicken.locative#locative-set!\376\003\000\000"
"\002\376\001\000\000!\001chicken.locative#locative->object\376\003\000\000\002\376\001\000\000\032\001chicken.locative#locative\077\376\003\000"
"\000\002\376\001\000\000\027\001chicken.memory#pointer+\376\003\000\000\002\376\001\000\000\030\001chicken.memory#pointer=\077\376\003\000\000\002\376\001\000\000\037\001chi"
"cken.memory#address->pointer\376\003\000\000\002\376\001\000\000\037\001chicken.memory#pointer->address\376\003\000\000\002\376\001\000\000\036"
"\001chicken.memory#pointer->object\376\003\000\000\002\376\001\000\000\036\001chicken.memory#object->pointer\376\003\000\000\002\376\001\000"
"\000\035\001chicken.memory#pointer-u8-ref\376\003\000\000\002\376\001\000\000\035\001chicken.memory#pointer-s8-ref\376\003\000\000\002\376\001\000"
"\000\036\001chicken.memory#pointer-u16-ref\376\003\000\000\002\376\001\000\000\036\001chicken.memory#pointer-s16-ref\376\003\000\000\002\376"
"\001\000\000\036\001chicken.memory#pointer-u32-ref\376\003\000\000\002\376\001\000\000\036\001chicken.memory#pointer-s32-ref\376\003\000\000"
"\002\376\001\000\000\036\001chicken.memory#pointer-f32-ref\376\003\000\000\002\376\001\000\000\036\001chicken.memory#pointer-f64-ref\376\003"
"\000\000\002\376\001\000\000\036\001chicken.memory#pointer-u8-set!\376\003\000\000\002\376\001\000\000\036\001chicken.memory#pointer-s8-set!"
"\376\003\000\000\002\376\001\000\000\037\001chicken.memory#pointer-u16-set!\376\003\000\000\002\376\001\000\000\037\001chicken.memory#pointer-s16-"
"set!\376\003\000\000\002\376\001\000\000\037\001chicken.memory#pointer-u32-set!\376\003\000\000\002\376\001\000\000\037\001chicken.memory#pointer-"
"s32-set!\376\003\000\000\002\376\001\000\000\037\001chicken.memory#pointer-f32-set!\376\003\000\000\002\376\001\000\000\037\001chicken.memory#poin"
"ter-f64-set!\376\003\000\000\002\376\001\000\000\036\001chicken.string#substring-index\376\003\000\000\002\376\001\000\000!\001chicken.string#s"
"ubstring-index-ci\376\003\000\000\002\376\001\000\000\032\001chicken.string#substring=\077\376\003\000\000\002\376\001\000\000\035\001chicken.string#"
"substring-ci=\077\376\003\000\000\002\376\001\000\000\026\001chicken.io#read-string\376\003\000\000\002\376\001\000\000\025\001chicken.format#format\376"
"\003\000\000\002\376\001\000\000\025\001chicken.format#printf\376\003\000\000\002\376\001\000\000\026\001chicken.format#sprintf\376\003\000\000\002\376\001\000\000\026\001chick"
"en.format#fprintf\376\377\016"));
lf[932]=C_h_intern(&lf[932],26, C_text("chicken.base#symbol-append"));
lf[933]=C_h_intern(&lf[933],7, C_text("scheme#"));
lf[934]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\003\001not\376\003\000\000\002\376\001\000\000\010\001boolean\077\376\003\000\000\002\376\001\000\000\005\001apply\376\003\000\000\002\376\001\000\000\036\001call-with-current-co"
"ntinuation\376\003\000\000\002\376\001\000\000\003\001eq\077\376\003\000\000\002\376\001\000\000\004\001eqv\077\376\003\000\000\002\376\001\000\000\006\001equal\077\376\003\000\000\002\376\001\000\000\005\001pair\077\376\003\000\000\002\376\001\000"
"\000\004\001cons\376\003\000\000\002\376\001\000\000\003\001car\376\003\000\000\002\376\001\000\000\003\001cdr\376\003\000\000\002\376\001\000\000\004\001caar\376\003\000\000\002\376\001\000\000\004\001cadr\376\003\000\000\002\376\001\000\000\004\001cdar"
"\376\003\000\000\002\376\001\000\000\004\001cddr\376\003\000\000\002\376\001\000\000\005\001caaar\376\003\000\000\002\376\001\000\000\005\001caadr\376\003\000\000\002\376\001\000\000\005\001cadar\376\003\000\000\002\376\001\000\000\005\001caddr\376"
"\003\000\000\002\376\001\000\000\005\001cdaar\376\003\000\000\002\376\001\000\000\005\001cdadr\376\003\000\000\002\376\001\000\000\005\001cddar\376\003\000\000\002\376\001\000\000\005\001cdddr\376\003\000\000\002\376\001\000\000\006\001caaaar"
"\376\003\000\000\002\376\001\000\000\006\001caaadr\376\003\000\000\002\376\001\000\000\006\001caadar\376\003\000\000\002\376\001\000\000\006\001caaddr\376\003\000\000\002\376\001\000\000\006\001cadaar\376\003\000\000\002\376\001\000\000\006\001c"
"adadr\376\003\000\000\002\376\001\000\000\006\001caddar\376\003\000\000\002\376\001\000\000\006\001cadddr\376\003\000\000\002\376\001\000\000\006\001cdaaar\376\003\000\000\002\376\001\000\000\006\001cdaadr\376\003\000\000\002\376\001"
"\000\000\006\001cdadar\376\003\000\000\002\376\001\000\000\006\001cdaddr\376\003\000\000\002\376\001\000\000\006\001cddaar\376\003\000\000\002\376\001\000\000\006\001cddadr\376\003\000\000\002\376\001\000\000\006\001cdddar\376\003"
"\000\000\002\376\001\000\000\006\001cddddr\376\003\000\000\002\376\001\000\000\010\001set-car!\376\003\000\000\002\376\001\000\000\010\001set-cdr!\376\003\000\000\002\376\001\000\000\005\001null\077\376\003\000\000\002\376\001\000\000\004\001"
"list\376\003\000\000\002\376\001\000\000\005\001list\077\376\003\000\000\002\376\001\000\000\006\001length\376\003\000\000\002\376\001\000\000\005\001zero\077\376\003\000\000\002\376\001\000\000\001\001\052\376\003\000\000\002\376\001\000\000\001\001-\376\003\000"
"\000\002\376\001\000\000\001\001+\376\003\000\000\002\376\001\000\000\001\001/\376\003\000\000\002\376\001\000\000\001\001-\376\003\000\000\002\376\001\000\000\001\001>\376\003\000\000\002\376\001\000\000\001\001<\376\003\000\000\002\376\001\000\000\002\001>=\376\003\000\000\002\376\001\000\000\002"
"\001<=\376\003\000\000\002\376\001\000\000\001\001=\376\003\000\000\002\376\001\000\000\023\001current-output-port\376\003\000\000\002\376\001\000\000\022\001current-input-port\376\003\000\000\002\376"
"\001\000\000\012\001write-char\376\003\000\000\002\376\001\000\000\007\001newline\376\003\000\000\002\376\001\000\000\005\001write\376\003\000\000\002\376\001\000\000\007\001display\376\003\000\000\002\376\001\000\000\006\001ap"
"pend\376\003\000\000\002\376\001\000\000\016\001symbol->string\376\003\000\000\002\376\001\000\000\010\001for-each\376\003\000\000\002\376\001\000\000\003\001map\376\003\000\000\002\376\001\000\000\005\001char\077\376\003"
"\000\000\002\376\001\000\000\015\001char->integer\376\003\000\000\002\376\001\000\000\015\001integer->char\376\003\000\000\002\376\001\000\000\013\001eof-object\077\376\003\000\000\002\376\001\000\000\015\001v"
"ector-length\376\003\000\000\002\376\001\000\000\015\001string-length\376\003\000\000\002\376\001\000\000\012\001string-ref\376\003\000\000\002\376\001\000\000\013\001string-set!\376"
"\003\000\000\002\376\001\000\000\012\001vector-ref\376\003\000\000\002\376\001\000\000\013\001vector-set!\376\003\000\000\002\376\001\000\000\006\001char=\077\376\003\000\000\002\376\001\000\000\006\001char<\077\376\003\000\000"
"\002\376\001\000\000\006\001char>\077\376\003\000\000\002\376\001\000\000\007\001char>=\077\376\003\000\000\002\376\001\000\000\007\001char<=\077\376\003\000\000\002\376\001\000\000\003\001gcd\376\003\000\000\002\376\001\000\000\003\001lcm\376\003\000"
"\000\002\376\001\000\000\007\001reverse\376\003\000\000\002\376\001\000\000\007\001symbol\077\376\003\000\000\002\376\001\000\000\016\001string->symbol\376\003\000\000\002\376\001\000\000\007\001number\077\376\003\000\000"
"\002\376\001\000\000\010\001complex\077\376\003\000\000\002\376\001\000\000\005\001real\077\376\003\000\000\002\376\001\000\000\010\001integer\077\376\003\000\000\002\376\001\000\000\011\001rational\077\376\003\000\000\002\376\001\000\000\004"
"\001odd\077\376\003\000\000\002\376\001\000\000\005\001even\077\376\003\000\000\002\376\001\000\000\011\001positive\077\376\003\000\000\002\376\001\000\000\011\001negative\077\376\003\000\000\002\376\001\000\000\006\001exact\077\376\003"
"\000\000\002\376\001\000\000\010\001inexact\077\376\003\000\000\002\376\001\000\000\003\001max\376\003\000\000\002\376\001\000\000\003\001min\376\003\000\000\002\376\001\000\000\010\001quotient\376\003\000\000\002\376\001\000\000\011\001remai"
"nder\376\003\000\000\002\376\001\000\000\006\001modulo\376\003\000\000\002\376\001\000\000\005\001floor\376\003\000\000\002\376\001\000\000\007\001ceiling\376\003\000\000\002\376\001\000\000\010\001truncate\376\003\000\000\002\376"
"\001\000\000\005\001round\376\003\000\000\002\376\001\000\000\013\001rationalize\376\003\000\000\002\376\001\000\000\016\001exact->inexact\376\003\000\000\002\376\001\000\000\016\001inexact->exa"
"ct\376\003\000\000\002\376\001\000\000\003\001exp\376\003\000\000\002\376\001\000\000\003\001log\376\003\000\000\002\376\001\000\000\003\001sin\376\003\000\000\002\376\001\000\000\004\001expt\376\003\000\000\002\376\001\000\000\004\001sqrt\376\003\000\000\002\376"
"\001\000\000\003\001cos\376\003\000\000\002\376\001\000\000\003\001tan\376\003\000\000\002\376\001\000\000\004\001asin\376\003\000\000\002\376\001\000\000\004\001acos\376\003\000\000\002\376\001\000\000\004\001atan\376\003\000\000\002\376\001\000\000\016\001nu"
"mber->string\376\003\000\000\002\376\001\000\000\016\001string->number\376\003\000\000\002\376\001\000\000\011\001char-ci=\077\376\003\000\000\002\376\001\000\000\011\001char-ci<\077\376\003\000"
"\000\002\376\001\000\000\011\001char-ci>\077\376\003\000\000\002\376\001\000\000\012\001char-ci>=\077\376\003\000\000\002\376\001\000\000\012\001char-ci<=\077\376\003\000\000\002\376\001\000\000\020\001char-alpha"
"betic\077\376\003\000\000\002\376\001\000\000\020\001char-whitespace\077\376\003\000\000\002\376\001\000\000\015\001char-numeric\077\376\003\000\000\002\376\001\000\000\020\001char-lower-c"
"ase\077\376\003\000\000\002\376\001\000\000\020\001char-upper-case\077\376\003\000\000\002\376\001\000\000\013\001char-upcase\376\003\000\000\002\376\001\000\000\015\001char-downcase\376\003\000"
"\000\002\376\001\000\000\007\001string\077\376\003\000\000\002\376\001\000\000\010\001string=\077\376\003\000\000\002\376\001\000\000\010\001string>\077\376\003\000\000\002\376\001\000\000\010\001string<\077\376\003\000\000\002\376\001\000"
"\000\011\001string>=\077\376\003\000\000\002\376\001\000\000\011\001string<=\077\376\003\000\000\002\376\001\000\000\013\001string-ci=\077\376\003\000\000\002\376\001\000\000\013\001string-ci<\077\376\003\000\000"
"\002\376\001\000\000\013\001string-ci>\077\376\003\000\000\002\376\001\000\000\014\001string-ci<=\077\376\003\000\000\002\376\001\000\000\014\001string-ci>=\077\376\003\000\000\002\376\001\000\000\015\001strin"
"g-append\376\003\000\000\002\376\001\000\000\014\001string->list\376\003\000\000\002\376\001\000\000\014\001list->string\376\003\000\000\002\376\001\000\000\007\001vector\077\376\003\000\000\002\376\001\000"
"\000\014\001vector->list\376\003\000\000\002\376\001\000\000\014\001list->vector\376\003\000\000\002\376\001\000\000\006\001string\376\003\000\000\002\376\001\000\000\004\001read\376\003\000\000\002\376\001\000\000\011"
"\001read-char\376\003\000\000\002\376\001\000\000\011\001substring\376\003\000\000\002\376\001\000\000\014\001string-fill!\376\003\000\000\002\376\001\000\000\014\001vector-copy!\376\003\000\000"
"\002\376\001\000\000\014\001vector-fill!\376\003\000\000\002\376\001\000\000\013\001make-string\376\003\000\000\002\376\001\000\000\013\001make-vector\376\003\000\000\002\376\001\000\000\017\001open-i"
"nput-file\376\003\000\000\002\376\001\000\000\020\001open-output-file\376\003\000\000\002\376\001\000\000\024\001call-with-input-file\376\003\000\000\002\376\001\000\000\025\001ca"
"ll-with-output-file\376\003\000\000\002\376\001\000\000\020\001close-input-port\376\003\000\000\002\376\001\000\000\021\001close-output-port\376\003\000\000\002\376"
"\001\000\000\006\001values\376\003\000\000\002\376\001\000\000\020\001call-with-values\376\003\000\000\002\376\001\000\000\006\001vector\376\003\000\000\002\376\001\000\000\012\001procedure\077\376\003\000\000"
"\002\376\001\000\000\004\001memq\376\003\000\000\002\376\001\000\000\004\001memv\376\003\000\000\002\376\001\000\000\006\001member\376\003\000\000\002\376\001\000\000\004\001assq\376\003\000\000\002\376\001\000\000\004\001assv\376\003\000\000\002\376\001"
"\000\000\005\001assoc\376\003\000\000\002\376\001\000\000\011\001list-tail\376\003\000\000\002\376\001\000\000\010\001list-ref\376\003\000\000\002\376\001\000\000\003\001abs\376\003\000\000\002\376\001\000\000\013\001char-re"
"ady\077\376\003\000\000\002\376\001\000\000\011\001peek-char\376\003\000\000\002\376\001\000\000\014\001list->string\376\003\000\000\002\376\001\000\000\014\001string->list\376\003\000\000\002\376\001\000\000\022"
"\001current-input-port\376\003\000\000\002\376\001\000\000\023\001current-output-port\376\003\000\000\002\376\001\000\000\012\001make-polar\376\003\000\000\002\376\001\000\000\020"
"\001make-rectangular\376\003\000\000\002\376\001\000\000\011\001real-part\376\003\000\000\002\376\001\000\000\011\001imag-part\376\003\000\000\002\376\001\000\000\004\001load\376\003\000\000\002\376\001\000"
"\000\004\001eval\376\003\000\000\002\376\001\000\000\027\001interaction-environment\376\003\000\000\002\376\001\000\000\020\001null-environment\376\003\000\000\002\376\001\000\000\031\001s"
"cheme-report-environment\376\377\016"));
lf[935]=C_h_intern(&lf[935],50, C_text("chicken.compiler.optimizer#membership-unfold-limit"));
lf[936]=C_h_intern(&lf[936],52, C_text("chicken.compiler.optimizer#membership-test-operators"));
lf[937]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376B\000\000\010C_i_memq\376B\000\000\005C_eqp\376\003\000\000\002\376\003\000\000\002\376B\000\000\012C_u_i_memq\376B\000\000\005C_eqp\376\003\000\000\002\376\003\000\000\002\376B"
"\000\000\012C_i_member\376B\000\000\012C_i_equalp\376\003\000\000\002\376\003\000\000\002\376B\000\000\010C_i_memv\376B\000\000\010C_i_eqvp\376\377\016"));
lf[938]=C_h_intern(&lf[938],45, C_text("chicken.compiler.optimizer#eq-inline-operator"));
lf[939]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005C_eqp"));
lf[940]=C_h_intern(&lf[940],54, C_text("chicken.compiler.optimizer#default-optimization-passes"));
C_register_lf2(lf,941,create_ptable());{}
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1664,a[2]=t1,tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_library_toplevel(2,av2);}}

#ifdef C_ENABLE_PTABLES
static C_PTABLE_ENTRY ptable[669] = {
{C_text("f_1664:c_2dplatform_2escm"),(void*)f_1664},
{C_text("f_1667:c_2dplatform_2escm"),(void*)f_1667},
{C_text("f_1670:c_2dplatform_2escm"),(void*)f_1670},
{C_text("f_1673:c_2dplatform_2escm"),(void*)f_1673},
{C_text("f_1676:c_2dplatform_2escm"),(void*)f_1676},
{C_text("f_1679:c_2dplatform_2escm"),(void*)f_1679},
{C_text("f_1682:c_2dplatform_2escm"),(void*)f_1682},
{C_text("f_2117:c_2dplatform_2escm"),(void*)f_2117},
{C_text("f_2123:c_2dplatform_2escm"),(void*)f_2123},
{C_text("f_2137:c_2dplatform_2escm"),(void*)f_2137},
{C_text("f_2293:c_2dplatform_2escm"),(void*)f_2293},
{C_text("f_2302:c_2dplatform_2escm"),(void*)f_2302},
{C_text("f_2310:c_2dplatform_2escm"),(void*)f_2310},
{C_text("f_2317:c_2dplatform_2escm"),(void*)f_2317},
{C_text("f_2331:c_2dplatform_2escm"),(void*)f_2331},
{C_text("f_2459:c_2dplatform_2escm"),(void*)f_2459},
{C_text("f_2701:c_2dplatform_2escm"),(void*)f_2701},
{C_text("f_2715:c_2dplatform_2escm"),(void*)f_2715},
{C_text("f_2719:c_2dplatform_2escm"),(void*)f_2719},
{C_text("f_2963:c_2dplatform_2escm"),(void*)f_2963},
{C_text("f_2971:c_2dplatform_2escm"),(void*)f_2971},
{C_text("f_2974:c_2dplatform_2escm"),(void*)f_2974},
{C_text("f_2977:c_2dplatform_2escm"),(void*)f_2977},
{C_text("f_2992:c_2dplatform_2escm"),(void*)f_2992},
{C_text("f_2999:c_2dplatform_2escm"),(void*)f_2999},
{C_text("f_3008:c_2dplatform_2escm"),(void*)f_3008},
{C_text("f_3010:c_2dplatform_2escm"),(void*)f_3010},
{C_text("f_3012:c_2dplatform_2escm"),(void*)f_3012},
{C_text("f_3034:c_2dplatform_2escm"),(void*)f_3034},
{C_text("f_3067:c_2dplatform_2escm"),(void*)f_3067},
{C_text("f_3075:c_2dplatform_2escm"),(void*)f_3075},
{C_text("f_3078:c_2dplatform_2escm"),(void*)f_3078},
{C_text("f_3080:c_2dplatform_2escm"),(void*)f_3080},
{C_text("f_3096:c_2dplatform_2escm"),(void*)f_3096},
{C_text("f_3105:c_2dplatform_2escm"),(void*)f_3105},
{C_text("f_3108:c_2dplatform_2escm"),(void*)f_3108},
{C_text("f_3123:c_2dplatform_2escm"),(void*)f_3123},
{C_text("f_3135:c_2dplatform_2escm"),(void*)f_3135},
{C_text("f_3149:c_2dplatform_2escm"),(void*)f_3149},
{C_text("f_3153:c_2dplatform_2escm"),(void*)f_3153},
{C_text("f_3162:c_2dplatform_2escm"),(void*)f_3162},
{C_text("f_3176:c_2dplatform_2escm"),(void*)f_3176},
{C_text("f_3180:c_2dplatform_2escm"),(void*)f_3180},
{C_text("f_3210:c_2dplatform_2escm"),(void*)f_3210},
{C_text("f_3214:c_2dplatform_2escm"),(void*)f_3214},
{C_text("f_3218:c_2dplatform_2escm"),(void*)f_3218},
{C_text("f_3222:c_2dplatform_2escm"),(void*)f_3222},
{C_text("f_3226:c_2dplatform_2escm"),(void*)f_3226},
{C_text("f_3234:c_2dplatform_2escm"),(void*)f_3234},
{C_text("f_3237:c_2dplatform_2escm"),(void*)f_3237},
{C_text("f_3240:c_2dplatform_2escm"),(void*)f_3240},
{C_text("f_3242:c_2dplatform_2escm"),(void*)f_3242},
{C_text("f_3270:c_2dplatform_2escm"),(void*)f_3270},
{C_text("f_3278:c_2dplatform_2escm"),(void*)f_3278},
{C_text("f_3295:c_2dplatform_2escm"),(void*)f_3295},
{C_text("f_3297:c_2dplatform_2escm"),(void*)f_3297},
{C_text("f_3322:c_2dplatform_2escm"),(void*)f_3322},
{C_text("f_3333:c_2dplatform_2escm"),(void*)f_3333},
{C_text("f_3337:c_2dplatform_2escm"),(void*)f_3337},
{C_text("f_3340:c_2dplatform_2escm"),(void*)f_3340},
{C_text("f_3354:c_2dplatform_2escm"),(void*)f_3354},
{C_text("f_3358:c_2dplatform_2escm"),(void*)f_3358},
{C_text("f_3381:c_2dplatform_2escm"),(void*)f_3381},
{C_text("f_3396:c_2dplatform_2escm"),(void*)f_3396},
{C_text("f_3404:c_2dplatform_2escm"),(void*)f_3404},
{C_text("f_3413:c_2dplatform_2escm"),(void*)f_3413},
{C_text("f_3417:c_2dplatform_2escm"),(void*)f_3417},
{C_text("f_3420:c_2dplatform_2escm"),(void*)f_3420},
{C_text("f_3423:c_2dplatform_2escm"),(void*)f_3423},
{C_text("f_3425:c_2dplatform_2escm"),(void*)f_3425},
{C_text("f_3431:c_2dplatform_2escm"),(void*)f_3431},
{C_text("f_3443:c_2dplatform_2escm"),(void*)f_3443},
{C_text("f_3462:c_2dplatform_2escm"),(void*)f_3462},
{C_text("f_3493:c_2dplatform_2escm"),(void*)f_3493},
{C_text("f_3496:c_2dplatform_2escm"),(void*)f_3496},
{C_text("f_3499:c_2dplatform_2escm"),(void*)f_3499},
{C_text("f_3502:c_2dplatform_2escm"),(void*)f_3502},
{C_text("f_3505:c_2dplatform_2escm"),(void*)f_3505},
{C_text("f_3508:c_2dplatform_2escm"),(void*)f_3508},
{C_text("f_3509:c_2dplatform_2escm"),(void*)f_3509},
{C_text("f_3535:c_2dplatform_2escm"),(void*)f_3535},
{C_text("f_3538:c_2dplatform_2escm"),(void*)f_3538},
{C_text("f_3540:c_2dplatform_2escm"),(void*)f_3540},
{C_text("f_3574:c_2dplatform_2escm"),(void*)f_3574},
{C_text("f_3601:c_2dplatform_2escm"),(void*)f_3601},
{C_text("f_3604:c_2dplatform_2escm"),(void*)f_3604},
{C_text("f_3607:c_2dplatform_2escm"),(void*)f_3607},
{C_text("f_3622:c_2dplatform_2escm"),(void*)f_3622},
{C_text("f_3626:c_2dplatform_2escm"),(void*)f_3626},
{C_text("f_3638:c_2dplatform_2escm"),(void*)f_3638},
{C_text("f_3650:c_2dplatform_2escm"),(void*)f_3650},
{C_text("f_3662:c_2dplatform_2escm"),(void*)f_3662},
{C_text("f_3666:c_2dplatform_2escm"),(void*)f_3666},
{C_text("f_3674:c_2dplatform_2escm"),(void*)f_3674},
{C_text("f_3681:c_2dplatform_2escm"),(void*)f_3681},
{C_text("f_3685:c_2dplatform_2escm"),(void*)f_3685},
{C_text("f_3689:c_2dplatform_2escm"),(void*)f_3689},
{C_text("f_3693:c_2dplatform_2escm"),(void*)f_3693},
{C_text("f_3697:c_2dplatform_2escm"),(void*)f_3697},
{C_text("f_3705:c_2dplatform_2escm"),(void*)f_3705},
{C_text("f_3708:c_2dplatform_2escm"),(void*)f_3708},
{C_text("f_3711:c_2dplatform_2escm"),(void*)f_3711},
{C_text("f_3714:c_2dplatform_2escm"),(void*)f_3714},
{C_text("f_3717:c_2dplatform_2escm"),(void*)f_3717},
{C_text("f_3720:c_2dplatform_2escm"),(void*)f_3720},
{C_text("f_3723:c_2dplatform_2escm"),(void*)f_3723},
{C_text("f_3726:c_2dplatform_2escm"),(void*)f_3726},
{C_text("f_3729:c_2dplatform_2escm"),(void*)f_3729},
{C_text("f_3732:c_2dplatform_2escm"),(void*)f_3732},
{C_text("f_3735:c_2dplatform_2escm"),(void*)f_3735},
{C_text("f_3738:c_2dplatform_2escm"),(void*)f_3738},
{C_text("f_3741:c_2dplatform_2escm"),(void*)f_3741},
{C_text("f_3744:c_2dplatform_2escm"),(void*)f_3744},
{C_text("f_3747:c_2dplatform_2escm"),(void*)f_3747},
{C_text("f_3750:c_2dplatform_2escm"),(void*)f_3750},
{C_text("f_3753:c_2dplatform_2escm"),(void*)f_3753},
{C_text("f_3756:c_2dplatform_2escm"),(void*)f_3756},
{C_text("f_3759:c_2dplatform_2escm"),(void*)f_3759},
{C_text("f_3762:c_2dplatform_2escm"),(void*)f_3762},
{C_text("f_3765:c_2dplatform_2escm"),(void*)f_3765},
{C_text("f_3768:c_2dplatform_2escm"),(void*)f_3768},
{C_text("f_3771:c_2dplatform_2escm"),(void*)f_3771},
{C_text("f_3774:c_2dplatform_2escm"),(void*)f_3774},
{C_text("f_3777:c_2dplatform_2escm"),(void*)f_3777},
{C_text("f_3780:c_2dplatform_2escm"),(void*)f_3780},
{C_text("f_3783:c_2dplatform_2escm"),(void*)f_3783},
{C_text("f_3786:c_2dplatform_2escm"),(void*)f_3786},
{C_text("f_3789:c_2dplatform_2escm"),(void*)f_3789},
{C_text("f_3792:c_2dplatform_2escm"),(void*)f_3792},
{C_text("f_3795:c_2dplatform_2escm"),(void*)f_3795},
{C_text("f_3798:c_2dplatform_2escm"),(void*)f_3798},
{C_text("f_3801:c_2dplatform_2escm"),(void*)f_3801},
{C_text("f_3804:c_2dplatform_2escm"),(void*)f_3804},
{C_text("f_3807:c_2dplatform_2escm"),(void*)f_3807},
{C_text("f_3810:c_2dplatform_2escm"),(void*)f_3810},
{C_text("f_3813:c_2dplatform_2escm"),(void*)f_3813},
{C_text("f_3816:c_2dplatform_2escm"),(void*)f_3816},
{C_text("f_3819:c_2dplatform_2escm"),(void*)f_3819},
{C_text("f_3822:c_2dplatform_2escm"),(void*)f_3822},
{C_text("f_3825:c_2dplatform_2escm"),(void*)f_3825},
{C_text("f_3828:c_2dplatform_2escm"),(void*)f_3828},
{C_text("f_3831:c_2dplatform_2escm"),(void*)f_3831},
{C_text("f_3834:c_2dplatform_2escm"),(void*)f_3834},
{C_text("f_3837:c_2dplatform_2escm"),(void*)f_3837},
{C_text("f_3840:c_2dplatform_2escm"),(void*)f_3840},
{C_text("f_3843:c_2dplatform_2escm"),(void*)f_3843},
{C_text("f_3846:c_2dplatform_2escm"),(void*)f_3846},
{C_text("f_3849:c_2dplatform_2escm"),(void*)f_3849},
{C_text("f_3852:c_2dplatform_2escm"),(void*)f_3852},
{C_text("f_3855:c_2dplatform_2escm"),(void*)f_3855},
{C_text("f_3858:c_2dplatform_2escm"),(void*)f_3858},
{C_text("f_3861:c_2dplatform_2escm"),(void*)f_3861},
{C_text("f_3864:c_2dplatform_2escm"),(void*)f_3864},
{C_text("f_3867:c_2dplatform_2escm"),(void*)f_3867},
{C_text("f_3870:c_2dplatform_2escm"),(void*)f_3870},
{C_text("f_3873:c_2dplatform_2escm"),(void*)f_3873},
{C_text("f_3876:c_2dplatform_2escm"),(void*)f_3876},
{C_text("f_3879:c_2dplatform_2escm"),(void*)f_3879},
{C_text("f_3882:c_2dplatform_2escm"),(void*)f_3882},
{C_text("f_3885:c_2dplatform_2escm"),(void*)f_3885},
{C_text("f_3888:c_2dplatform_2escm"),(void*)f_3888},
{C_text("f_3891:c_2dplatform_2escm"),(void*)f_3891},
{C_text("f_3894:c_2dplatform_2escm"),(void*)f_3894},
{C_text("f_3897:c_2dplatform_2escm"),(void*)f_3897},
{C_text("f_3900:c_2dplatform_2escm"),(void*)f_3900},
{C_text("f_3903:c_2dplatform_2escm"),(void*)f_3903},
{C_text("f_3906:c_2dplatform_2escm"),(void*)f_3906},
{C_text("f_3909:c_2dplatform_2escm"),(void*)f_3909},
{C_text("f_3912:c_2dplatform_2escm"),(void*)f_3912},
{C_text("f_3915:c_2dplatform_2escm"),(void*)f_3915},
{C_text("f_3918:c_2dplatform_2escm"),(void*)f_3918},
{C_text("f_3921:c_2dplatform_2escm"),(void*)f_3921},
{C_text("f_3924:c_2dplatform_2escm"),(void*)f_3924},
{C_text("f_3927:c_2dplatform_2escm"),(void*)f_3927},
{C_text("f_3930:c_2dplatform_2escm"),(void*)f_3930},
{C_text("f_3933:c_2dplatform_2escm"),(void*)f_3933},
{C_text("f_3936:c_2dplatform_2escm"),(void*)f_3936},
{C_text("f_3939:c_2dplatform_2escm"),(void*)f_3939},
{C_text("f_3942:c_2dplatform_2escm"),(void*)f_3942},
{C_text("f_3945:c_2dplatform_2escm"),(void*)f_3945},
{C_text("f_3948:c_2dplatform_2escm"),(void*)f_3948},
{C_text("f_3951:c_2dplatform_2escm"),(void*)f_3951},
{C_text("f_3954:c_2dplatform_2escm"),(void*)f_3954},
{C_text("f_3957:c_2dplatform_2escm"),(void*)f_3957},
{C_text("f_3960:c_2dplatform_2escm"),(void*)f_3960},
{C_text("f_3963:c_2dplatform_2escm"),(void*)f_3963},
{C_text("f_3966:c_2dplatform_2escm"),(void*)f_3966},
{C_text("f_3969:c_2dplatform_2escm"),(void*)f_3969},
{C_text("f_3972:c_2dplatform_2escm"),(void*)f_3972},
{C_text("f_3975:c_2dplatform_2escm"),(void*)f_3975},
{C_text("f_3978:c_2dplatform_2escm"),(void*)f_3978},
{C_text("f_3981:c_2dplatform_2escm"),(void*)f_3981},
{C_text("f_3984:c_2dplatform_2escm"),(void*)f_3984},
{C_text("f_3987:c_2dplatform_2escm"),(void*)f_3987},
{C_text("f_3990:c_2dplatform_2escm"),(void*)f_3990},
{C_text("f_3993:c_2dplatform_2escm"),(void*)f_3993},
{C_text("f_3996:c_2dplatform_2escm"),(void*)f_3996},
{C_text("f_3999:c_2dplatform_2escm"),(void*)f_3999},
{C_text("f_4002:c_2dplatform_2escm"),(void*)f_4002},
{C_text("f_4005:c_2dplatform_2escm"),(void*)f_4005},
{C_text("f_4008:c_2dplatform_2escm"),(void*)f_4008},
{C_text("f_4011:c_2dplatform_2escm"),(void*)f_4011},
{C_text("f_4014:c_2dplatform_2escm"),(void*)f_4014},
{C_text("f_4017:c_2dplatform_2escm"),(void*)f_4017},
{C_text("f_4020:c_2dplatform_2escm"),(void*)f_4020},
{C_text("f_4023:c_2dplatform_2escm"),(void*)f_4023},
{C_text("f_4026:c_2dplatform_2escm"),(void*)f_4026},
{C_text("f_4029:c_2dplatform_2escm"),(void*)f_4029},
{C_text("f_4032:c_2dplatform_2escm"),(void*)f_4032},
{C_text("f_4035:c_2dplatform_2escm"),(void*)f_4035},
{C_text("f_4038:c_2dplatform_2escm"),(void*)f_4038},
{C_text("f_4041:c_2dplatform_2escm"),(void*)f_4041},
{C_text("f_4044:c_2dplatform_2escm"),(void*)f_4044},
{C_text("f_4047:c_2dplatform_2escm"),(void*)f_4047},
{C_text("f_4050:c_2dplatform_2escm"),(void*)f_4050},
{C_text("f_4053:c_2dplatform_2escm"),(void*)f_4053},
{C_text("f_4056:c_2dplatform_2escm"),(void*)f_4056},
{C_text("f_4059:c_2dplatform_2escm"),(void*)f_4059},
{C_text("f_4062:c_2dplatform_2escm"),(void*)f_4062},
{C_text("f_4065:c_2dplatform_2escm"),(void*)f_4065},
{C_text("f_4068:c_2dplatform_2escm"),(void*)f_4068},
{C_text("f_4071:c_2dplatform_2escm"),(void*)f_4071},
{C_text("f_4074:c_2dplatform_2escm"),(void*)f_4074},
{C_text("f_4077:c_2dplatform_2escm"),(void*)f_4077},
{C_text("f_4080:c_2dplatform_2escm"),(void*)f_4080},
{C_text("f_4083:c_2dplatform_2escm"),(void*)f_4083},
{C_text("f_4086:c_2dplatform_2escm"),(void*)f_4086},
{C_text("f_4089:c_2dplatform_2escm"),(void*)f_4089},
{C_text("f_4092:c_2dplatform_2escm"),(void*)f_4092},
{C_text("f_4095:c_2dplatform_2escm"),(void*)f_4095},
{C_text("f_4098:c_2dplatform_2escm"),(void*)f_4098},
{C_text("f_4101:c_2dplatform_2escm"),(void*)f_4101},
{C_text("f_4104:c_2dplatform_2escm"),(void*)f_4104},
{C_text("f_4107:c_2dplatform_2escm"),(void*)f_4107},
{C_text("f_4110:c_2dplatform_2escm"),(void*)f_4110},
{C_text("f_4113:c_2dplatform_2escm"),(void*)f_4113},
{C_text("f_4116:c_2dplatform_2escm"),(void*)f_4116},
{C_text("f_4119:c_2dplatform_2escm"),(void*)f_4119},
{C_text("f_4122:c_2dplatform_2escm"),(void*)f_4122},
{C_text("f_4125:c_2dplatform_2escm"),(void*)f_4125},
{C_text("f_4128:c_2dplatform_2escm"),(void*)f_4128},
{C_text("f_4131:c_2dplatform_2escm"),(void*)f_4131},
{C_text("f_4134:c_2dplatform_2escm"),(void*)f_4134},
{C_text("f_4137:c_2dplatform_2escm"),(void*)f_4137},
{C_text("f_4140:c_2dplatform_2escm"),(void*)f_4140},
{C_text("f_4143:c_2dplatform_2escm"),(void*)f_4143},
{C_text("f_4146:c_2dplatform_2escm"),(void*)f_4146},
{C_text("f_4149:c_2dplatform_2escm"),(void*)f_4149},
{C_text("f_4152:c_2dplatform_2escm"),(void*)f_4152},
{C_text("f_4155:c_2dplatform_2escm"),(void*)f_4155},
{C_text("f_4158:c_2dplatform_2escm"),(void*)f_4158},
{C_text("f_4161:c_2dplatform_2escm"),(void*)f_4161},
{C_text("f_4164:c_2dplatform_2escm"),(void*)f_4164},
{C_text("f_4167:c_2dplatform_2escm"),(void*)f_4167},
{C_text("f_4170:c_2dplatform_2escm"),(void*)f_4170},
{C_text("f_4173:c_2dplatform_2escm"),(void*)f_4173},
{C_text("f_4176:c_2dplatform_2escm"),(void*)f_4176},
{C_text("f_4179:c_2dplatform_2escm"),(void*)f_4179},
{C_text("f_4182:c_2dplatform_2escm"),(void*)f_4182},
{C_text("f_4185:c_2dplatform_2escm"),(void*)f_4185},
{C_text("f_4188:c_2dplatform_2escm"),(void*)f_4188},
{C_text("f_4191:c_2dplatform_2escm"),(void*)f_4191},
{C_text("f_4194:c_2dplatform_2escm"),(void*)f_4194},
{C_text("f_4197:c_2dplatform_2escm"),(void*)f_4197},
{C_text("f_4200:c_2dplatform_2escm"),(void*)f_4200},
{C_text("f_4203:c_2dplatform_2escm"),(void*)f_4203},
{C_text("f_4206:c_2dplatform_2escm"),(void*)f_4206},
{C_text("f_4209:c_2dplatform_2escm"),(void*)f_4209},
{C_text("f_4212:c_2dplatform_2escm"),(void*)f_4212},
{C_text("f_4215:c_2dplatform_2escm"),(void*)f_4215},
{C_text("f_4218:c_2dplatform_2escm"),(void*)f_4218},
{C_text("f_4221:c_2dplatform_2escm"),(void*)f_4221},
{C_text("f_4224:c_2dplatform_2escm"),(void*)f_4224},
{C_text("f_4227:c_2dplatform_2escm"),(void*)f_4227},
{C_text("f_4230:c_2dplatform_2escm"),(void*)f_4230},
{C_text("f_4233:c_2dplatform_2escm"),(void*)f_4233},
{C_text("f_4236:c_2dplatform_2escm"),(void*)f_4236},
{C_text("f_4239:c_2dplatform_2escm"),(void*)f_4239},
{C_text("f_4242:c_2dplatform_2escm"),(void*)f_4242},
{C_text("f_4245:c_2dplatform_2escm"),(void*)f_4245},
{C_text("f_4248:c_2dplatform_2escm"),(void*)f_4248},
{C_text("f_4251:c_2dplatform_2escm"),(void*)f_4251},
{C_text("f_4254:c_2dplatform_2escm"),(void*)f_4254},
{C_text("f_4257:c_2dplatform_2escm"),(void*)f_4257},
{C_text("f_4260:c_2dplatform_2escm"),(void*)f_4260},
{C_text("f_4263:c_2dplatform_2escm"),(void*)f_4263},
{C_text("f_4266:c_2dplatform_2escm"),(void*)f_4266},
{C_text("f_4269:c_2dplatform_2escm"),(void*)f_4269},
{C_text("f_4272:c_2dplatform_2escm"),(void*)f_4272},
{C_text("f_4275:c_2dplatform_2escm"),(void*)f_4275},
{C_text("f_4278:c_2dplatform_2escm"),(void*)f_4278},
{C_text("f_4281:c_2dplatform_2escm"),(void*)f_4281},
{C_text("f_4284:c_2dplatform_2escm"),(void*)f_4284},
{C_text("f_4287:c_2dplatform_2escm"),(void*)f_4287},
{C_text("f_4290:c_2dplatform_2escm"),(void*)f_4290},
{C_text("f_4293:c_2dplatform_2escm"),(void*)f_4293},
{C_text("f_4296:c_2dplatform_2escm"),(void*)f_4296},
{C_text("f_4299:c_2dplatform_2escm"),(void*)f_4299},
{C_text("f_4302:c_2dplatform_2escm"),(void*)f_4302},
{C_text("f_4305:c_2dplatform_2escm"),(void*)f_4305},
{C_text("f_4308:c_2dplatform_2escm"),(void*)f_4308},
{C_text("f_4311:c_2dplatform_2escm"),(void*)f_4311},
{C_text("f_4314:c_2dplatform_2escm"),(void*)f_4314},
{C_text("f_4317:c_2dplatform_2escm"),(void*)f_4317},
{C_text("f_4320:c_2dplatform_2escm"),(void*)f_4320},
{C_text("f_4323:c_2dplatform_2escm"),(void*)f_4323},
{C_text("f_4326:c_2dplatform_2escm"),(void*)f_4326},
{C_text("f_4329:c_2dplatform_2escm"),(void*)f_4329},
{C_text("f_4332:c_2dplatform_2escm"),(void*)f_4332},
{C_text("f_4335:c_2dplatform_2escm"),(void*)f_4335},
{C_text("f_4338:c_2dplatform_2escm"),(void*)f_4338},
{C_text("f_4341:c_2dplatform_2escm"),(void*)f_4341},
{C_text("f_4344:c_2dplatform_2escm"),(void*)f_4344},
{C_text("f_4347:c_2dplatform_2escm"),(void*)f_4347},
{C_text("f_4350:c_2dplatform_2escm"),(void*)f_4350},
{C_text("f_4353:c_2dplatform_2escm"),(void*)f_4353},
{C_text("f_4356:c_2dplatform_2escm"),(void*)f_4356},
{C_text("f_4359:c_2dplatform_2escm"),(void*)f_4359},
{C_text("f_4362:c_2dplatform_2escm"),(void*)f_4362},
{C_text("f_4365:c_2dplatform_2escm"),(void*)f_4365},
{C_text("f_4368:c_2dplatform_2escm"),(void*)f_4368},
{C_text("f_4371:c_2dplatform_2escm"),(void*)f_4371},
{C_text("f_4374:c_2dplatform_2escm"),(void*)f_4374},
{C_text("f_4377:c_2dplatform_2escm"),(void*)f_4377},
{C_text("f_4380:c_2dplatform_2escm"),(void*)f_4380},
{C_text("f_4383:c_2dplatform_2escm"),(void*)f_4383},
{C_text("f_4386:c_2dplatform_2escm"),(void*)f_4386},
{C_text("f_4389:c_2dplatform_2escm"),(void*)f_4389},
{C_text("f_4392:c_2dplatform_2escm"),(void*)f_4392},
{C_text("f_4395:c_2dplatform_2escm"),(void*)f_4395},
{C_text("f_4398:c_2dplatform_2escm"),(void*)f_4398},
{C_text("f_4401:c_2dplatform_2escm"),(void*)f_4401},
{C_text("f_4404:c_2dplatform_2escm"),(void*)f_4404},
{C_text("f_4407:c_2dplatform_2escm"),(void*)f_4407},
{C_text("f_4410:c_2dplatform_2escm"),(void*)f_4410},
{C_text("f_4413:c_2dplatform_2escm"),(void*)f_4413},
{C_text("f_4416:c_2dplatform_2escm"),(void*)f_4416},
{C_text("f_4419:c_2dplatform_2escm"),(void*)f_4419},
{C_text("f_4422:c_2dplatform_2escm"),(void*)f_4422},
{C_text("f_4425:c_2dplatform_2escm"),(void*)f_4425},
{C_text("f_4428:c_2dplatform_2escm"),(void*)f_4428},
{C_text("f_4431:c_2dplatform_2escm"),(void*)f_4431},
{C_text("f_4434:c_2dplatform_2escm"),(void*)f_4434},
{C_text("f_4437:c_2dplatform_2escm"),(void*)f_4437},
{C_text("f_4439:c_2dplatform_2escm"),(void*)f_4439},
{C_text("f_4476:c_2dplatform_2escm"),(void*)f_4476},
{C_text("f_4478:c_2dplatform_2escm"),(void*)f_4478},
{C_text("f_4485:c_2dplatform_2escm"),(void*)f_4485},
{C_text("f_4496:c_2dplatform_2escm"),(void*)f_4496},
{C_text("f_4517:c_2dplatform_2escm"),(void*)f_4517},
{C_text("f_4521:c_2dplatform_2escm"),(void*)f_4521},
{C_text("f_4534:c_2dplatform_2escm"),(void*)f_4534},
{C_text("f_4536:c_2dplatform_2escm"),(void*)f_4536},
{C_text("f_4558:c_2dplatform_2escm"),(void*)f_4558},
{C_text("f_4562:c_2dplatform_2escm"),(void*)f_4562},
{C_text("f_4572:c_2dplatform_2escm"),(void*)f_4572},
{C_text("f_4575:c_2dplatform_2escm"),(void*)f_4575},
{C_text("f_4578:c_2dplatform_2escm"),(void*)f_4578},
{C_text("f_4581:c_2dplatform_2escm"),(void*)f_4581},
{C_text("f_4584:c_2dplatform_2escm"),(void*)f_4584},
{C_text("f_4587:c_2dplatform_2escm"),(void*)f_4587},
{C_text("f_4590:c_2dplatform_2escm"),(void*)f_4590},
{C_text("f_4593:c_2dplatform_2escm"),(void*)f_4593},
{C_text("f_4596:c_2dplatform_2escm"),(void*)f_4596},
{C_text("f_4599:c_2dplatform_2escm"),(void*)f_4599},
{C_text("f_4602:c_2dplatform_2escm"),(void*)f_4602},
{C_text("f_4605:c_2dplatform_2escm"),(void*)f_4605},
{C_text("f_4608:c_2dplatform_2escm"),(void*)f_4608},
{C_text("f_4611:c_2dplatform_2escm"),(void*)f_4611},
{C_text("f_4614:c_2dplatform_2escm"),(void*)f_4614},
{C_text("f_4617:c_2dplatform_2escm"),(void*)f_4617},
{C_text("f_4620:c_2dplatform_2escm"),(void*)f_4620},
{C_text("f_4623:c_2dplatform_2escm"),(void*)f_4623},
{C_text("f_4626:c_2dplatform_2escm"),(void*)f_4626},
{C_text("f_4629:c_2dplatform_2escm"),(void*)f_4629},
{C_text("f_4632:c_2dplatform_2escm"),(void*)f_4632},
{C_text("f_4635:c_2dplatform_2escm"),(void*)f_4635},
{C_text("f_4638:c_2dplatform_2escm"),(void*)f_4638},
{C_text("f_4641:c_2dplatform_2escm"),(void*)f_4641},
{C_text("f_4644:c_2dplatform_2escm"),(void*)f_4644},
{C_text("f_4647:c_2dplatform_2escm"),(void*)f_4647},
{C_text("f_4650:c_2dplatform_2escm"),(void*)f_4650},
{C_text("f_4653:c_2dplatform_2escm"),(void*)f_4653},
{C_text("f_4656:c_2dplatform_2escm"),(void*)f_4656},
{C_text("f_4659:c_2dplatform_2escm"),(void*)f_4659},
{C_text("f_4662:c_2dplatform_2escm"),(void*)f_4662},
{C_text("f_4665:c_2dplatform_2escm"),(void*)f_4665},
{C_text("f_4668:c_2dplatform_2escm"),(void*)f_4668},
{C_text("f_4671:c_2dplatform_2escm"),(void*)f_4671},
{C_text("f_4674:c_2dplatform_2escm"),(void*)f_4674},
{C_text("f_4677:c_2dplatform_2escm"),(void*)f_4677},
{C_text("f_4680:c_2dplatform_2escm"),(void*)f_4680},
{C_text("f_4683:c_2dplatform_2escm"),(void*)f_4683},
{C_text("f_4686:c_2dplatform_2escm"),(void*)f_4686},
{C_text("f_4689:c_2dplatform_2escm"),(void*)f_4689},
{C_text("f_4692:c_2dplatform_2escm"),(void*)f_4692},
{C_text("f_4695:c_2dplatform_2escm"),(void*)f_4695},
{C_text("f_4698:c_2dplatform_2escm"),(void*)f_4698},
{C_text("f_4701:c_2dplatform_2escm"),(void*)f_4701},
{C_text("f_4704:c_2dplatform_2escm"),(void*)f_4704},
{C_text("f_4707:c_2dplatform_2escm"),(void*)f_4707},
{C_text("f_4710:c_2dplatform_2escm"),(void*)f_4710},
{C_text("f_4713:c_2dplatform_2escm"),(void*)f_4713},
{C_text("f_4716:c_2dplatform_2escm"),(void*)f_4716},
{C_text("f_4719:c_2dplatform_2escm"),(void*)f_4719},
{C_text("f_4722:c_2dplatform_2escm"),(void*)f_4722},
{C_text("f_4725:c_2dplatform_2escm"),(void*)f_4725},
{C_text("f_4728:c_2dplatform_2escm"),(void*)f_4728},
{C_text("f_4731:c_2dplatform_2escm"),(void*)f_4731},
{C_text("f_4734:c_2dplatform_2escm"),(void*)f_4734},
{C_text("f_4737:c_2dplatform_2escm"),(void*)f_4737},
{C_text("f_4740:c_2dplatform_2escm"),(void*)f_4740},
{C_text("f_4743:c_2dplatform_2escm"),(void*)f_4743},
{C_text("f_4746:c_2dplatform_2escm"),(void*)f_4746},
{C_text("f_4749:c_2dplatform_2escm"),(void*)f_4749},
{C_text("f_4752:c_2dplatform_2escm"),(void*)f_4752},
{C_text("f_4755:c_2dplatform_2escm"),(void*)f_4755},
{C_text("f_4758:c_2dplatform_2escm"),(void*)f_4758},
{C_text("f_4761:c_2dplatform_2escm"),(void*)f_4761},
{C_text("f_4764:c_2dplatform_2escm"),(void*)f_4764},
{C_text("f_4767:c_2dplatform_2escm"),(void*)f_4767},
{C_text("f_4770:c_2dplatform_2escm"),(void*)f_4770},
{C_text("f_4773:c_2dplatform_2escm"),(void*)f_4773},
{C_text("f_4776:c_2dplatform_2escm"),(void*)f_4776},
{C_text("f_4779:c_2dplatform_2escm"),(void*)f_4779},
{C_text("f_4782:c_2dplatform_2escm"),(void*)f_4782},
{C_text("f_4785:c_2dplatform_2escm"),(void*)f_4785},
{C_text("f_4788:c_2dplatform_2escm"),(void*)f_4788},
{C_text("f_4791:c_2dplatform_2escm"),(void*)f_4791},
{C_text("f_4794:c_2dplatform_2escm"),(void*)f_4794},
{C_text("f_4797:c_2dplatform_2escm"),(void*)f_4797},
{C_text("f_4800:c_2dplatform_2escm"),(void*)f_4800},
{C_text("f_4803:c_2dplatform_2escm"),(void*)f_4803},
{C_text("f_4806:c_2dplatform_2escm"),(void*)f_4806},
{C_text("f_4809:c_2dplatform_2escm"),(void*)f_4809},
{C_text("f_4812:c_2dplatform_2escm"),(void*)f_4812},
{C_text("f_4815:c_2dplatform_2escm"),(void*)f_4815},
{C_text("f_4818:c_2dplatform_2escm"),(void*)f_4818},
{C_text("f_4821:c_2dplatform_2escm"),(void*)f_4821},
{C_text("f_4824:c_2dplatform_2escm"),(void*)f_4824},
{C_text("f_4827:c_2dplatform_2escm"),(void*)f_4827},
{C_text("f_4830:c_2dplatform_2escm"),(void*)f_4830},
{C_text("f_4833:c_2dplatform_2escm"),(void*)f_4833},
{C_text("f_4836:c_2dplatform_2escm"),(void*)f_4836},
{C_text("f_4839:c_2dplatform_2escm"),(void*)f_4839},
{C_text("f_4842:c_2dplatform_2escm"),(void*)f_4842},
{C_text("f_4845:c_2dplatform_2escm"),(void*)f_4845},
{C_text("f_4848:c_2dplatform_2escm"),(void*)f_4848},
{C_text("f_4851:c_2dplatform_2escm"),(void*)f_4851},
{C_text("f_4854:c_2dplatform_2escm"),(void*)f_4854},
{C_text("f_4857:c_2dplatform_2escm"),(void*)f_4857},
{C_text("f_4860:c_2dplatform_2escm"),(void*)f_4860},
{C_text("f_4863:c_2dplatform_2escm"),(void*)f_4863},
{C_text("f_4866:c_2dplatform_2escm"),(void*)f_4866},
{C_text("f_4869:c_2dplatform_2escm"),(void*)f_4869},
{C_text("f_4872:c_2dplatform_2escm"),(void*)f_4872},
{C_text("f_4875:c_2dplatform_2escm"),(void*)f_4875},
{C_text("f_4878:c_2dplatform_2escm"),(void*)f_4878},
{C_text("f_4881:c_2dplatform_2escm"),(void*)f_4881},
{C_text("f_4884:c_2dplatform_2escm"),(void*)f_4884},
{C_text("f_4887:c_2dplatform_2escm"),(void*)f_4887},
{C_text("f_4890:c_2dplatform_2escm"),(void*)f_4890},
{C_text("f_4893:c_2dplatform_2escm"),(void*)f_4893},
{C_text("f_4896:c_2dplatform_2escm"),(void*)f_4896},
{C_text("f_4899:c_2dplatform_2escm"),(void*)f_4899},
{C_text("f_4902:c_2dplatform_2escm"),(void*)f_4902},
{C_text("f_4905:c_2dplatform_2escm"),(void*)f_4905},
{C_text("f_4908:c_2dplatform_2escm"),(void*)f_4908},
{C_text("f_4911:c_2dplatform_2escm"),(void*)f_4911},
{C_text("f_4914:c_2dplatform_2escm"),(void*)f_4914},
{C_text("f_4917:c_2dplatform_2escm"),(void*)f_4917},
{C_text("f_4920:c_2dplatform_2escm"),(void*)f_4920},
{C_text("f_4923:c_2dplatform_2escm"),(void*)f_4923},
{C_text("f_4926:c_2dplatform_2escm"),(void*)f_4926},
{C_text("f_4929:c_2dplatform_2escm"),(void*)f_4929},
{C_text("f_4932:c_2dplatform_2escm"),(void*)f_4932},
{C_text("f_4935:c_2dplatform_2escm"),(void*)f_4935},
{C_text("f_4938:c_2dplatform_2escm"),(void*)f_4938},
{C_text("f_4941:c_2dplatform_2escm"),(void*)f_4941},
{C_text("f_4944:c_2dplatform_2escm"),(void*)f_4944},
{C_text("f_4947:c_2dplatform_2escm"),(void*)f_4947},
{C_text("f_4950:c_2dplatform_2escm"),(void*)f_4950},
{C_text("f_4953:c_2dplatform_2escm"),(void*)f_4953},
{C_text("f_4956:c_2dplatform_2escm"),(void*)f_4956},
{C_text("f_4959:c_2dplatform_2escm"),(void*)f_4959},
{C_text("f_4962:c_2dplatform_2escm"),(void*)f_4962},
{C_text("f_4965:c_2dplatform_2escm"),(void*)f_4965},
{C_text("f_4968:c_2dplatform_2escm"),(void*)f_4968},
{C_text("f_4971:c_2dplatform_2escm"),(void*)f_4971},
{C_text("f_4974:c_2dplatform_2escm"),(void*)f_4974},
{C_text("f_4977:c_2dplatform_2escm"),(void*)f_4977},
{C_text("f_4980:c_2dplatform_2escm"),(void*)f_4980},
{C_text("f_4983:c_2dplatform_2escm"),(void*)f_4983},
{C_text("f_4986:c_2dplatform_2escm"),(void*)f_4986},
{C_text("f_4989:c_2dplatform_2escm"),(void*)f_4989},
{C_text("f_4992:c_2dplatform_2escm"),(void*)f_4992},
{C_text("f_4995:c_2dplatform_2escm"),(void*)f_4995},
{C_text("f_4998:c_2dplatform_2escm"),(void*)f_4998},
{C_text("f_5001:c_2dplatform_2escm"),(void*)f_5001},
{C_text("f_5004:c_2dplatform_2escm"),(void*)f_5004},
{C_text("f_5007:c_2dplatform_2escm"),(void*)f_5007},
{C_text("f_5010:c_2dplatform_2escm"),(void*)f_5010},
{C_text("f_5013:c_2dplatform_2escm"),(void*)f_5013},
{C_text("f_5016:c_2dplatform_2escm"),(void*)f_5016},
{C_text("f_5019:c_2dplatform_2escm"),(void*)f_5019},
{C_text("f_5022:c_2dplatform_2escm"),(void*)f_5022},
{C_text("f_5025:c_2dplatform_2escm"),(void*)f_5025},
{C_text("f_5028:c_2dplatform_2escm"),(void*)f_5028},
{C_text("f_5031:c_2dplatform_2escm"),(void*)f_5031},
{C_text("f_5034:c_2dplatform_2escm"),(void*)f_5034},
{C_text("f_5037:c_2dplatform_2escm"),(void*)f_5037},
{C_text("f_5040:c_2dplatform_2escm"),(void*)f_5040},
{C_text("f_5043:c_2dplatform_2escm"),(void*)f_5043},
{C_text("f_5046:c_2dplatform_2escm"),(void*)f_5046},
{C_text("f_5049:c_2dplatform_2escm"),(void*)f_5049},
{C_text("f_5052:c_2dplatform_2escm"),(void*)f_5052},
{C_text("f_5055:c_2dplatform_2escm"),(void*)f_5055},
{C_text("f_5058:c_2dplatform_2escm"),(void*)f_5058},
{C_text("f_5061:c_2dplatform_2escm"),(void*)f_5061},
{C_text("f_5064:c_2dplatform_2escm"),(void*)f_5064},
{C_text("f_5067:c_2dplatform_2escm"),(void*)f_5067},
{C_text("f_5070:c_2dplatform_2escm"),(void*)f_5070},
{C_text("f_5073:c_2dplatform_2escm"),(void*)f_5073},
{C_text("f_5076:c_2dplatform_2escm"),(void*)f_5076},
{C_text("f_5079:c_2dplatform_2escm"),(void*)f_5079},
{C_text("f_5082:c_2dplatform_2escm"),(void*)f_5082},
{C_text("f_5085:c_2dplatform_2escm"),(void*)f_5085},
{C_text("f_5088:c_2dplatform_2escm"),(void*)f_5088},
{C_text("f_5091:c_2dplatform_2escm"),(void*)f_5091},
{C_text("f_5094:c_2dplatform_2escm"),(void*)f_5094},
{C_text("f_5097:c_2dplatform_2escm"),(void*)f_5097},
{C_text("f_5100:c_2dplatform_2escm"),(void*)f_5100},
{C_text("f_5103:c_2dplatform_2escm"),(void*)f_5103},
{C_text("f_5106:c_2dplatform_2escm"),(void*)f_5106},
{C_text("f_5109:c_2dplatform_2escm"),(void*)f_5109},
{C_text("f_5112:c_2dplatform_2escm"),(void*)f_5112},
{C_text("f_5115:c_2dplatform_2escm"),(void*)f_5115},
{C_text("f_5118:c_2dplatform_2escm"),(void*)f_5118},
{C_text("f_5121:c_2dplatform_2escm"),(void*)f_5121},
{C_text("f_5124:c_2dplatform_2escm"),(void*)f_5124},
{C_text("f_5127:c_2dplatform_2escm"),(void*)f_5127},
{C_text("f_5130:c_2dplatform_2escm"),(void*)f_5130},
{C_text("f_5133:c_2dplatform_2escm"),(void*)f_5133},
{C_text("f_5136:c_2dplatform_2escm"),(void*)f_5136},
{C_text("f_5139:c_2dplatform_2escm"),(void*)f_5139},
{C_text("f_5142:c_2dplatform_2escm"),(void*)f_5142},
{C_text("f_5144:c_2dplatform_2escm"),(void*)f_5144},
{C_text("f_5166:c_2dplatform_2escm"),(void*)f_5166},
{C_text("f_5181:c_2dplatform_2escm"),(void*)f_5181},
{C_text("f_5184:c_2dplatform_2escm"),(void*)f_5184},
{C_text("f_5199:c_2dplatform_2escm"),(void*)f_5199},
{C_text("f_5211:c_2dplatform_2escm"),(void*)f_5211},
{C_text("f_5219:c_2dplatform_2escm"),(void*)f_5219},
{C_text("f_5221:c_2dplatform_2escm"),(void*)f_5221},
{C_text("f_5242:c_2dplatform_2escm"),(void*)f_5242},
{C_text("f_5246:c_2dplatform_2escm"),(void*)f_5246},
{C_text("f_5249:c_2dplatform_2escm"),(void*)f_5249},
{C_text("f_5252:c_2dplatform_2escm"),(void*)f_5252},
{C_text("f_5254:c_2dplatform_2escm"),(void*)f_5254},
{C_text("f_5273:c_2dplatform_2escm"),(void*)f_5273},
{C_text("f_5290:c_2dplatform_2escm"),(void*)f_5290},
{C_text("f_5333:c_2dplatform_2escm"),(void*)f_5333},
{C_text("f_5337:c_2dplatform_2escm"),(void*)f_5337},
{C_text("f_5341:c_2dplatform_2escm"),(void*)f_5341},
{C_text("f_5345:c_2dplatform_2escm"),(void*)f_5345},
{C_text("f_5352:c_2dplatform_2escm"),(void*)f_5352},
{C_text("f_5356:c_2dplatform_2escm"),(void*)f_5356},
{C_text("f_5364:c_2dplatform_2escm"),(void*)f_5364},
{C_text("f_5368:c_2dplatform_2escm"),(void*)f_5368},
{C_text("f_5376:c_2dplatform_2escm"),(void*)f_5376},
{C_text("f_5379:c_2dplatform_2escm"),(void*)f_5379},
{C_text("f_5383:c_2dplatform_2escm"),(void*)f_5383},
{C_text("f_5386:c_2dplatform_2escm"),(void*)f_5386},
{C_text("f_5389:c_2dplatform_2escm"),(void*)f_5389},
{C_text("f_5392:c_2dplatform_2escm"),(void*)f_5392},
{C_text("f_5395:c_2dplatform_2escm"),(void*)f_5395},
{C_text("f_5398:c_2dplatform_2escm"),(void*)f_5398},
{C_text("f_5401:c_2dplatform_2escm"),(void*)f_5401},
{C_text("f_5404:c_2dplatform_2escm"),(void*)f_5404},
{C_text("f_5407:c_2dplatform_2escm"),(void*)f_5407},
{C_text("f_5410:c_2dplatform_2escm"),(void*)f_5410},
{C_text("f_5413:c_2dplatform_2escm"),(void*)f_5413},
{C_text("f_5416:c_2dplatform_2escm"),(void*)f_5416},
{C_text("f_5419:c_2dplatform_2escm"),(void*)f_5419},
{C_text("f_5422:c_2dplatform_2escm"),(void*)f_5422},
{C_text("f_5425:c_2dplatform_2escm"),(void*)f_5425},
{C_text("f_5428:c_2dplatform_2escm"),(void*)f_5428},
{C_text("f_5430:c_2dplatform_2escm"),(void*)f_5430},
{C_text("f_5452:c_2dplatform_2escm"),(void*)f_5452},
{C_text("f_5470:c_2dplatform_2escm"),(void*)f_5470},
{C_text("f_5492:c_2dplatform_2escm"),(void*)f_5492},
{C_text("f_5510:c_2dplatform_2escm"),(void*)f_5510},
{C_text("f_5535:c_2dplatform_2escm"),(void*)f_5535},
{C_text("f_5556:c_2dplatform_2escm"),(void*)f_5556},
{C_text("f_5564:c_2dplatform_2escm"),(void*)f_5564},
{C_text("f_5568:c_2dplatform_2escm"),(void*)f_5568},
{C_text("f_5575:c_2dplatform_2escm"),(void*)f_5575},
{C_text("f_5603:c_2dplatform_2escm"),(void*)f_5603},
{C_text("f_5606:c_2dplatform_2escm"),(void*)f_5606},
{C_text("f_5637:c_2dplatform_2escm"),(void*)f_5637},
{C_text("f_5659:c_2dplatform_2escm"),(void*)f_5659},
{C_text("f_5682:c_2dplatform_2escm"),(void*)f_5682},
{C_text("f_5686:c_2dplatform_2escm"),(void*)f_5686},
{C_text("f_5690:c_2dplatform_2escm"),(void*)f_5690},
{C_text("f_5697:c_2dplatform_2escm"),(void*)f_5697},
{C_text("f_5719:c_2dplatform_2escm"),(void*)f_5719},
{C_text("f_5729:c_2dplatform_2escm"),(void*)f_5729},
{C_text("f_5743:c_2dplatform_2escm"),(void*)f_5743},
{C_text("f_5747:c_2dplatform_2escm"),(void*)f_5747},
{C_text("f_5754:c_2dplatform_2escm"),(void*)f_5754},
{C_text("f_5785:c_2dplatform_2escm"),(void*)f_5785},
{C_text("f_5788:c_2dplatform_2escm"),(void*)f_5788},
{C_text("f_5803:c_2dplatform_2escm"),(void*)f_5803},
{C_text("f_5820:c_2dplatform_2escm"),(void*)f_5820},
{C_text("f_5824:c_2dplatform_2escm"),(void*)f_5824},
{C_text("f_5831:c_2dplatform_2escm"),(void*)f_5831},
{C_text("f_5862:c_2dplatform_2escm"),(void*)f_5862},
{C_text("f_5890:c_2dplatform_2escm"),(void*)f_5890},
{C_text("f_5892:c_2dplatform_2escm"),(void*)f_5892},
{C_text("f_5915:c_2dplatform_2escm"),(void*)f_5915},
{C_text("f_5917:c_2dplatform_2escm"),(void*)f_5917},
{C_text("f_5936:c_2dplatform_2escm"),(void*)f_5936},
{C_text("f_5940:c_2dplatform_2escm"),(void*)f_5940},
{C_text("f_5955:c_2dplatform_2escm"),(void*)f_5955},
{C_text("f_5986:c_2dplatform_2escm"),(void*)f_5986},
{C_text("f_6014:c_2dplatform_2escm"),(void*)f_6014},
{C_text("f_6016:c_2dplatform_2escm"),(void*)f_6016},
{C_text("f_6039:c_2dplatform_2escm"),(void*)f_6039},
{C_text("f_6041:c_2dplatform_2escm"),(void*)f_6041},
{C_text("f_6060:c_2dplatform_2escm"),(void*)f_6060},
{C_text("f_6064:c_2dplatform_2escm"),(void*)f_6064},
{C_text("f_6079:c_2dplatform_2escm"),(void*)f_6079},
{C_text("f_6083:c_2dplatform_2escm"),(void*)f_6083},
{C_text("f_6104:c_2dplatform_2escm"),(void*)f_6104},
{C_text("f_6146:c_2dplatform_2escm"),(void*)f_6146},
{C_text("f_6148:c_2dplatform_2escm"),(void*)f_6148},
{C_text("f_6155:c_2dplatform_2escm"),(void*)f_6155},
{C_text("f_6166:c_2dplatform_2escm"),(void*)f_6166},
{C_text("f_6187:c_2dplatform_2escm"),(void*)f_6187},
{C_text("f_6191:c_2dplatform_2escm"),(void*)f_6191},
{C_text("f_6197:c_2dplatform_2escm"),(void*)f_6197},
{C_text("f_6219:c_2dplatform_2escm"),(void*)f_6219},
{C_text("f_6223:c_2dplatform_2escm"),(void*)f_6223},
{C_text("f_6225:c_2dplatform_2escm"),(void*)f_6225},
{C_text("f_6241:c_2dplatform_2escm"),(void*)f_6241},
{C_text("f_6247:c_2dplatform_2escm"),(void*)f_6247},
{C_text("f_6265:c_2dplatform_2escm"),(void*)f_6265},
{C_text("f_6268:c_2dplatform_2escm"),(void*)f_6268},
{C_text("f_6271:c_2dplatform_2escm"),(void*)f_6271},
{C_text("f_6286:c_2dplatform_2escm"),(void*)f_6286},
{C_text("f_6298:c_2dplatform_2escm"),(void*)f_6298},
{C_text("f_6308:c_2dplatform_2escm"),(void*)f_6308},
{C_text("f_6312:c_2dplatform_2escm"),(void*)f_6312},
{C_text("f_6321:c_2dplatform_2escm"),(void*)f_6321},
{C_text("f_6331:c_2dplatform_2escm"),(void*)f_6331},
{C_text("f_6335:c_2dplatform_2escm"),(void*)f_6335},
{C_text("f_6365:c_2dplatform_2escm"),(void*)f_6365},
{C_text("f_6369:c_2dplatform_2escm"),(void*)f_6369},
{C_text("f_6373:c_2dplatform_2escm"),(void*)f_6373},
{C_text("f_6377:c_2dplatform_2escm"),(void*)f_6377},
{C_text("f_6381:c_2dplatform_2escm"),(void*)f_6381},
{C_text("f_6390:c_2dplatform_2escm"),(void*)f_6390},
{C_text("f_6394:c_2dplatform_2escm"),(void*)f_6394},
{C_text("f_6396:c_2dplatform_2escm"),(void*)f_6396},
{C_text("f_6406:c_2dplatform_2escm"),(void*)f_6406},
{C_text("f_6419:c_2dplatform_2escm"),(void*)f_6419},
{C_text("f_6444:c_2dplatform_2escm"),(void*)f_6444},
{C_text("toplevel:c_2dplatform_2escm"),(void*)C_c_2dplatform_toplevel},
{NULL,NULL}};
#endif

static C_PTABLE_ENTRY *create_ptable(void){
#ifdef C_ENABLE_PTABLES
return ptable;
#else
return NULL;
#endif
}

/*
o|hiding unexported module binding: chicken.compiler.c-platform#partition 
o|hiding unexported module binding: chicken.compiler.c-platform#span 
o|hiding unexported module binding: chicken.compiler.c-platform#take 
o|hiding unexported module binding: chicken.compiler.c-platform#drop 
o|hiding unexported module binding: chicken.compiler.c-platform#split-at 
o|hiding unexported module binding: chicken.compiler.c-platform#append-map 
o|hiding unexported module binding: chicken.compiler.c-platform#every 
o|hiding unexported module binding: chicken.compiler.c-platform#any 
o|hiding unexported module binding: chicken.compiler.c-platform#cons* 
o|hiding unexported module binding: chicken.compiler.c-platform#concatenate 
o|hiding unexported module binding: chicken.compiler.c-platform#delete 
o|hiding unexported module binding: chicken.compiler.c-platform#first 
o|hiding unexported module binding: chicken.compiler.c-platform#second 
o|hiding unexported module binding: chicken.compiler.c-platform#third 
o|hiding unexported module binding: chicken.compiler.c-platform#fourth 
o|hiding unexported module binding: chicken.compiler.c-platform#fifth 
o|hiding unexported module binding: chicken.compiler.c-platform#delete-duplicates 
o|hiding unexported module binding: chicken.compiler.c-platform#alist-cons 
o|hiding unexported module binding: chicken.compiler.c-platform#filter 
o|hiding unexported module binding: chicken.compiler.c-platform#filter-map 
o|hiding unexported module binding: chicken.compiler.c-platform#remove 
o|hiding unexported module binding: chicken.compiler.c-platform#unzip1 
o|hiding unexported module binding: chicken.compiler.c-platform#last 
o|hiding unexported module binding: chicken.compiler.c-platform#list-index 
o|hiding unexported module binding: chicken.compiler.c-platform#lset-adjoin/eq? 
o|hiding unexported module binding: chicken.compiler.c-platform#lset-difference/eq? 
o|hiding unexported module binding: chicken.compiler.c-platform#lset-union/eq? 
o|hiding unexported module binding: chicken.compiler.c-platform#lset-intersection/eq? 
o|hiding unexported module binding: chicken.compiler.c-platform#list-tabulate 
o|hiding unexported module binding: chicken.compiler.c-platform#lset<=/eq? 
o|hiding unexported module binding: chicken.compiler.c-platform#lset=/eq? 
o|hiding unexported module binding: chicken.compiler.c-platform#length+ 
o|hiding unexported module binding: chicken.compiler.c-platform#find 
o|hiding unexported module binding: chicken.compiler.c-platform#find-tail 
o|hiding unexported module binding: chicken.compiler.c-platform#iota 
o|hiding unexported module binding: chicken.compiler.c-platform#make-list 
o|hiding unexported module binding: chicken.compiler.c-platform#posq 
o|hiding unexported module binding: chicken.compiler.c-platform#posv 
o|hiding unexported module binding: chicken.compiler.c-platform#min-words-per-bignum 
o|hiding unexported module binding: chicken.compiler.c-platform#constant578 
o|hiding unexported module binding: chicken.compiler.c-platform#constant582 
o|hiding unexported module binding: chicken.compiler.c-platform#constant586 
o|hiding unexported module binding: chicken.compiler.c-platform#setter-map 
S|applied compiler syntax:
S|  scheme#for-each		1
S|  chicken.base#foldl		3
S|  scheme#map		5
S|  chicken.base#foldr		3
o|eliminated procedure checks: 34 
o|specializations:
o|  1 (chicken.base#add1 fixnum)
o|  1 (scheme#- *)
o|  1 (scheme#negative? *)
o|  4 (scheme#>= fixnum fixnum)
o|  2 (scheme#zero? *)
o|  1 (scheme#length list)
o|  1 (scheme#memq * list)
o|  15 (scheme#= fixnum fixnum)
o|  1 (scheme#eqv? * *)
o|  5 (##sys#check-list (or pair list) *)
o|  26 (scheme#cdr pair)
o|  10 (scheme#car pair)
(o e)|safe calls: 461 
(o e)|assignments to immediate values: 2 
o|safe globals: (chicken.compiler.c-platform#posv chicken.compiler.c-platform#posq chicken.compiler.c-platform#make-list chicken.compiler.c-platform#iota chicken.compiler.c-platform#find-tail chicken.compiler.c-platform#find chicken.compiler.c-platform#length+ chicken.compiler.c-platform#lset=/eq? chicken.compiler.c-platform#lset<=/eq? chicken.compiler.c-platform#list-tabulate chicken.compiler.c-platform#lset-intersection/eq? chicken.compiler.c-platform#lset-union/eq? chicken.compiler.c-platform#lset-difference/eq? chicken.compiler.c-platform#lset-adjoin/eq? chicken.compiler.c-platform#list-index chicken.compiler.c-platform#last chicken.compiler.c-platform#unzip1 chicken.compiler.c-platform#remove chicken.compiler.c-platform#filter-map chicken.compiler.c-platform#filter chicken.compiler.c-platform#alist-cons chicken.compiler.c-platform#delete-duplicates chicken.compiler.c-platform#fifth chicken.compiler.c-platform#fourth chicken.compiler.c-platform#third chicken.compiler.c-platform#second chicken.compiler.c-platform#first chicken.compiler.c-platform#delete chicken.compiler.c-platform#concatenate chicken.compiler.c-platform#cons* chicken.compiler.c-platform#any chicken.compiler.c-platform#every chicken.compiler.c-platform#append-map chicken.compiler.c-platform#split-at chicken.compiler.c-platform#drop chicken.compiler.c-platform#take chicken.compiler.c-platform#span chicken.compiler.c-platform#partition) 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#partition 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#span 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#drop 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#split-at 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#append-map 
o|inlining procedure: k2064 
o|inlining procedure: k2064 
o|inlining procedure: k2095 
o|inlining procedure: k2095 
o|merged explicitly consed rest parameter: xs203 
o|inlining procedure: k2125 
o|inlining procedure: k2125 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#concatenate 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#fourth 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#fifth 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#delete-duplicates 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#alist-cons 
o|inlining procedure: k2312 
o|inlining procedure: k2312 
o|inlining procedure: k2304 
o|inlining procedure: k2304 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#filter-map 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#remove 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#unzip1 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#list-index 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#lset-adjoin/eq? 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#lset-difference/eq? 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#lset-union/eq? 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#lset-intersection/eq? 
o|inlining procedure: k2703 
o|inlining procedure: k2703 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#lset<=/eq? 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#lset=/eq? 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#length+ 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#find 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#find-tail 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#iota 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#make-list 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#posq 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#posv 
o|inlining procedure: k3014 
o|inlining procedure: k3032 
o|inlining procedure: k3046 
o|inlining procedure: k3046 
o|inlining procedure: k3032 
o|inlining procedure: k3014 
o|substituted constant variable: a3072 
o|inlining procedure: k3082 
o|inlining procedure: k3100 
o|inlining procedure: k3100 
o|inlining procedure: k3124 
o|inlining procedure: "(c-platform.scm:334) chicken.compiler.c-platform#first" 
o|inlining procedure: k3124 
o|inlining procedure: k3163 
o|inlining procedure: k3163 
o|inlining procedure: "(c-platform.scm:331) chicken.compiler.c-platform#first" 
o|inlining procedure: k3184 
o|inlining procedure: k3184 
o|inlining procedure: "(c-platform.scm:325) chicken.compiler.c-platform#second" 
o|inlining procedure: "(c-platform.scm:324) chicken.compiler.c-platform#first" 
o|inlining procedure: k3082 
o|substituted constant variable: a3231 
o|inlining procedure: k3244 
o|consed rest parameter at call site: "(c-platform.scm:380) chicken.compiler.c-platform#cons*" 2 
o|inlining procedure: k3299 
o|inlining procedure: k3299 
o|inlining procedure: "(c-platform.scm:382) chicken.compiler.c-platform#first" 
o|inlining procedure: "(c-platform.scm:380) chicken.compiler.c-platform#first" 
o|inlining procedure: k3341 
o|inlining procedure: k3341 
o|consed rest parameter at call site: "(c-platform.scm:395) chicken.compiler.c-platform#cons*" 2 
o|inlining procedure: k3362 
o|substituted constant variable: a3375 
o|inlining procedure: k3376 
o|inlining procedure: k3376 
o|inlining procedure: k3362 
o|substituted constant variable: a3405 
o|contracted procedure: "(c-platform.scm:375) chicken.compiler.c-platform#last" 
o|inlining procedure: k2461 
o|inlining procedure: k2461 
o|inlining procedure: k3244 
o|inlining procedure: k3433 
o|inlining procedure: k3460 
o|inlining procedure: k3460 
o|inlining procedure: "(c-platform.scm:410) chicken.compiler.c-platform#first" 
o|inlining procedure: k3433 
o|substituted constant variable: a3490 
o|inlining procedure: k3511 
o|inlining procedure: k3511 
o|substituted constant variable: a3532 
o|inlining procedure: k3542 
o|inlining procedure: k3560 
o|inlining procedure: k3575 
o|inlining procedure: k3587 
o|substituted constant variable: a3675 
o|inlining procedure: k3587 
o|inlining procedure: "(c-platform.scm:446) chicken.compiler.c-platform#third" 
o|inlining procedure: k3575 
o|inlining procedure: k3560 
o|inlining procedure: k3542 
o|substituted constant variable: a3698 
o|inlining procedure: k4441 
o|inlining procedure: k4456 
o|inlining procedure: k4480 
o|inlining procedure: k4480 
o|inlining procedure: "(c-platform.scm:853) chicken.compiler.c-platform#first" 
o|inlining procedure: k4456 
o|substituted constant variable: a4526 
o|inlining procedure: k4542 
o|inlining procedure: "(c-platform.scm:844) chicken.compiler.c-platform#first" 
o|inlining procedure: k4542 
o|substituted constant variable: a4569 
o|inlining procedure: k4441 
o|inlining procedure: k5149 
o|inlining procedure: k5170 
o|inlining procedure: "(c-platform.scm:1157) chicken.compiler.c-platform#second" 
o|inlining procedure: k5170 
o|inlining procedure: "(c-platform.scm:1153) chicken.compiler.c-platform#first" 
o|inlining procedure: "(c-platform.scm:1150) chicken.compiler.c-platform#first" 
o|inlining procedure: k5149 
o|inlining procedure: k5256 
o|inlining procedure: k5274 
o|inlining procedure: k5292 
o|inlining procedure: k5307 
o|inlining procedure: k5307 
o|inlining procedure: "(c-platform.scm:1186) chicken.compiler.c-platform#second" 
o|inlining procedure: k5292 
o|inlining procedure: "(c-platform.scm:1181) chicken.compiler.c-platform#third" 
o|inlining procedure: k5274 
o|inlining procedure: "(c-platform.scm:1179) chicken.compiler.c-platform#first" 
o|inlining procedure: "(c-platform.scm:1177) chicken.compiler.c-platform#first" 
o|inlining procedure: k5256 
o|substituted constant variable: a5369 
o|inlining procedure: k5432 
o|inlining procedure: k5458 
o|inlining procedure: k5458 
o|inlining procedure: k5432 
o|substituted constant variable: a5464 
o|inlining procedure: k5472 
o|inlining procedure: k5498 
o|inlining procedure: k5498 
o|inlining procedure: k5472 
o|substituted constant variable: a5504 
o|inlining procedure: k5512 
o|inlining procedure: k5530 
o|substituted constant variable: chicken.compiler.c-platform#setter-map 
o|inlining procedure: k5530 
o|inlining procedure: k5512 
o|substituted constant variable: a5569 
o|substituted constant variable: chicken.compiler.c-platform#min-words-per-bignum 
o|substituted constant variable: chicken.compiler.c-platform#min-words-per-bignum 
o|inlining procedure: k5577 
o|inlining procedure: k5604 
o|inlining procedure: k5604 
o|inlining procedure: k5626 
o|inlining procedure: k5638 
o|inlining procedure: "(c-platform.scm:1033) chicken.compiler.c-platform#first" 
o|inlining procedure: "(c-platform.scm:1036) chicken.compiler.c-platform#first" 
o|inlining procedure: k5638 
o|inlining procedure: "(c-platform.scm:1028) chicken.compiler.c-platform#first" 
o|inlining procedure: k5626 
o|inlining procedure: "(c-platform.scm:1022) chicken.compiler.c-platform#second" 
o|inlining procedure: k5577 
o|substituted constant variable: a5691 
o|inlining procedure: k5699 
o|inlining procedure: k5724 
o|inlining procedure: k5724 
o|inlining procedure: "(c-platform.scm:993) chicken.compiler.c-platform#first" 
o|inlining procedure: "(c-platform.scm:991) chicken.compiler.c-platform#third" 
o|inlining procedure: k5699 
o|substituted constant variable: a5752 
o|substituted constant variable: chicken.compiler.c-platform#min-words-per-bignum 
o|substituted constant variable: chicken.compiler.c-platform#min-words-per-bignum 
o|inlining procedure: k5756 
o|inlining procedure: k5783 
o|inlining procedure: "(c-platform.scm:875) chicken.compiler.c-platform#first" 
o|inlining procedure: k5783 
o|inlining procedure: "(c-platform.scm:872) chicken.compiler.c-platform#first" 
o|inlining procedure: "(c-platform.scm:869) chicken.compiler.c-platform#second" 
o|substituted constant variable: a5829 
o|inlining procedure: k5756 
o|inlining procedure: k5833 
o|inlining procedure: k5833 
o|inlining procedure: k5864 
o|inlining procedure: k5864 
o|inlining procedure: k5870 
o|inlining procedure: k5870 
o|substituted constant variable: a5909 
o|inlining procedure: k5923 
o|inlining procedure: "(c-platform.scm:818) chicken.compiler.c-platform#first" 
o|inlining procedure: k5923 
o|inlining procedure: k5957 
o|inlining procedure: k5957 
o|inlining procedure: k5988 
o|inlining procedure: k5988 
o|inlining procedure: k5994 
o|inlining procedure: k5994 
o|substituted constant variable: a6033 
o|inlining procedure: k6047 
o|inlining procedure: "(c-platform.scm:781) chicken.compiler.c-platform#first" 
o|inlining procedure: k6047 
o|inlining procedure: k6084 
o|inlining procedure: k6084 
o|inlining procedure: "(c-platform.scm:745) chicken.compiler.c-platform#first" 
o|inlining procedure: k6126 
o|inlining procedure: k6150 
o|inlining procedure: k6150 
o|inlining procedure: "(c-platform.scm:753) chicken.compiler.c-platform#first" 
o|inlining procedure: k6126 
o|inlining procedure: k6203 
o|inlining procedure: "(c-platform.scm:741) chicken.compiler.c-platform#first" 
o|inlining procedure: k6203 
o|inlining procedure: k6227 
o|inlining procedure: k6248 
o|inlining procedure: k6248 
o|inlining procedure: k6287 
o|inlining procedure: "(c-platform.scm:359) chicken.compiler.c-platform#first" 
o|inlining procedure: k6287 
o|inlining procedure: k6322 
o|inlining procedure: k6322 
o|inlining procedure: "(c-platform.scm:356) chicken.compiler.c-platform#first" 
o|inlining procedure: k6339 
o|inlining procedure: k6339 
o|inlining procedure: "(c-platform.scm:350) chicken.compiler.c-platform#second" 
o|inlining procedure: "(c-platform.scm:349) chicken.compiler.c-platform#first" 
o|inlining procedure: k6227 
o|substituted constant variable: a6386 
o|inlining procedure: k6398 
o|contracted procedure: "(c-platform.scm:286) g591598" 
o|propagated global variable: g606607 chicken.compiler.support#mark-variable 
o|inlining procedure: k6398 
o|substituted constant variable: chicken.compiler.c-platform#constant582 
o|substituted constant variable: chicken.compiler.c-platform#constant578 
o|substituted constant variable: chicken.compiler.c-platform#constant586 
o|inlining procedure: k6421 
o|contracted procedure: "(c-platform.scm:123) g558567" 
o|inlining procedure: k6421 
o|simplifications: ((if . 1)) 
o|replaced variables: 608 
o|removed binding forms: 573 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#every 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#any 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#first 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#second 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#third 
o|substituted constant variable: r23056461 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#min-words-per-bignum 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#constant578 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#constant582 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#constant586 
o|substituted constant variable: r30156473 
o|substituted constant variable: r31016476 
o|substituted constant variable: r31256483 
o|substituted constant variable: r31856492 
o|substituted constant variable: r30836503 
o|substituted constant variable: r33776521 
o|substituted constant variable: r33636523 
o|substituted constant variable: r32456526 
o|substituted constant variable: r34346537 
o|substituted constant variable: r35126539 
o|substituted constant variable: r35886544 
o|substituted constant variable: r35766550 
o|substituted constant variable: r35616551 
o|substituted constant variable: r35436552 
o|substituted constant variable: r44576562 
o|substituted constant variable: r45436570 
o|substituted constant variable: r45436570 
o|folded constant expression: (scheme#not (quote #f)) 
o|substituted constant variable: r44426572 
o|contracted procedure: "(c-platform.scm:1169) chicken.compiler.c-platform#list-tabulate" 
o|substituted constant variable: r27046462 
o|substituted constant variable: r51716580 
o|substituted constant variable: r51506591 
o|substituted constant variable: r53086596 
o|substituted constant variable: r52936602 
o|substituted constant variable: r52756608 
o|substituted constant variable: r52576619 
o|removed side-effect free assignment to unused variable: chicken.compiler.c-platform#setter-map 
o|substituted constant variable: r54596621 
o|substituted constant variable: r54596621 
o|substituted constant variable: r54596623 
o|substituted constant variable: r54596623 
o|substituted constant variable: r54336625 
o|substituted constant variable: r54996627 
o|substituted constant variable: r54996627 
o|substituted constant variable: r54996629 
o|substituted constant variable: r54996629 
o|substituted constant variable: r54736631 
o|substituted constant variable: r55316634 
o|substituted constant variable: r55136635 
o|converted assignments to bindings: (rewrite-call/cc896) 
o|converted assignments to bindings: (rewrite-make-vector879) 
o|substituted constant variable: r56396653 
o|substituted constant variable: r56276659 
o|substituted constant variable: r55786665 
o|substituted constant variable: r57256667 
o|substituted constant variable: r57256667 
o|substituted constant variable: r57256669 
o|substituted constant variable: r57256669 
o|substituted constant variable: r57006681 
o|substituted constant variable: r57576702 
o|converted assignments to bindings: (rewrite-div827) 
o|substituted constant variable: r58346703 
o|substituted constant variable: r58656705 
o|substituted constant variable: r58656705 
o|substituted constant variable: r58656707 
o|substituted constant variable: r58656707 
o|substituted constant variable: r58716710 
o|substituted constant variable: r59246718 
o|substituted constant variable: r59246718 
o|folded constant expression: (scheme#not (quote #f)) 
o|substituted constant variable: r59586720 
o|substituted constant variable: r59896722 
o|substituted constant variable: r59896722 
o|substituted constant variable: r59896724 
o|substituted constant variable: r59896724 
o|substituted constant variable: r59956727 
o|substituted constant variable: r60486735 
o|substituted constant variable: r60486735 
o|folded constant expression: (scheme#not (quote #f)) 
o|substituted constant variable: r61276752 
o|substituted constant variable: r62046760 
o|substituted constant variable: r62046760 
o|folded constant expression: (scheme#not (quote #f)) 
o|converted assignments to bindings: (rewrite-c-w-v757) 
o|converted assignments to bindings: (rewrite-c..r728) 
o|converted assignments to bindings: (rewrite-apply684) 
o|substituted constant variable: r62886771 
o|substituted constant variable: r63406780 
o|substituted constant variable: r62286791 
o|converted assignments to bindings: (eqv?-id624) 
o|converted assignments to bindings: (op1613) 
o|substituted constant variable: g597599 
o|substituted constant variable: g564568 
o|simplifications: ((let . 8)) 
o|replaced variables: 127 
o|removed binding forms: 548 
o|replaced variables: 3 
o|removed binding forms: 208 
o|removed binding forms: 3 
o|removed unused formal parameters: (i893) 
o|removed unused parameter to known procedure: i893 "(mini-srfi-1.scm:190) a5220" 
o|simplifications: ((if . 10) (let . 10) (##core#call . 280)) 
o|  call simplifications:
o|    scheme#symbol?	2
o|    scheme#assq
o|    scheme#=
o|    chicken.base#fixnum?	2
o|    scheme#<=
o|    chicken.fixnum#fx+
o|    chicken.fixnum#fx>=	5
o|    scheme#caddr	3
o|    scheme#list?
o|    scheme#cdr	6
o|    ##sys#setslot	2
o|    scheme#cadr	8
o|    scheme#equal?	2
o|    scheme#number?	2
o|    scheme#not	12
o|    scheme#length	19
o|    scheme#eq?	56
o|    scheme#list	84
o|    ##sys#check-list	2
o|    scheme#pair?	7
o|    ##sys#slot	10
o|    scheme#null?	8
o|    scheme#car	34
o|    scheme#cons	11
o|contracted procedure: k2128 
o|contracted procedure: k2139 
o|contracted procedure: k2295 
o|contracted procedure: k2307 
o|contracted procedure: k2325 
o|contracted procedure: k2333 
o|contracted procedure: k2982 
o|contracted procedure: k3069 
o|contracted procedure: k3017 
o|contracted procedure: k3024 
o|contracted procedure: k3028 
o|contracted procedure: k3035 
o|contracted procedure: k3042 
o|contracted procedure: k3053 
o|contracted procedure: k3061 
o|contracted procedure: k3057 
o|contracted procedure: k3228 
o|contracted procedure: k3085 
o|contracted procedure: k3088 
o|contracted procedure: k3091 
o|contracted procedure: k3113 
o|contracted procedure: k3117 
o|contracted procedure: k3127 
o|contracted procedure: k3130 
o|contracted procedure: k3143 
o|contracted procedure: k3154 
o|contracted procedure: k3157 
o|contracted procedure: k3170 
o|contracted procedure: k3181 
o|contracted procedure: k3187 
o|contracted procedure: k3193 
o|contracted procedure: k3200 
o|contracted procedure: k3204 
o|contracted procedure: k3247 
o|contracted procedure: k3257 
o|contracted procedure: k3264 
o|contracted procedure: k3272 
o|contracted procedure: k3280 
o|contracted procedure: k3284 
o|contracted procedure: k3287 
o|contracted procedure: k3290 
o|contracted procedure: k3302 
o|contracted procedure: k3305 
o|contracted procedure: k3308 
o|contracted procedure: k3316 
o|contracted procedure: k3324 
o|contracted procedure: k3348 
o|contracted procedure: k3359 
o|contracted procedure: k3407 
o|contracted procedure: k3365 
o|contracted procedure: k3368 
o|contracted procedure: k3386 
o|contracted procedure: k3398 
o|contracted procedure: k3390 
o|contracted procedure: k2474 
o|contracted procedure: k2464 
o|contracted procedure: k3487 
o|contracted procedure: k3436 
o|contracted procedure: k3445 
o|contracted procedure: k3452 
o|contracted procedure: k3456 
o|contracted procedure: k3463 
o|contracted procedure: k3470 
o|contracted procedure: k3480 
o|contracted procedure: k3529 
o|contracted procedure: k3514 
o|contracted procedure: k3521 
o|contracted procedure: k3525 
o|contracted procedure: k3700 
o|contracted procedure: k3545 
o|contracted procedure: k3548 
o|contracted procedure: k3551 
o|contracted procedure: k3557 
o|contracted procedure: k3563 
o|contracted procedure: k3566 
o|contracted procedure: k3581 
o|contracted procedure: k3584 
o|contracted procedure: k3590 
o|contracted procedure: k3596 
o|contracted procedure: k3612 
o|contracted procedure: k3616 
o|contracted procedure: k3628 
o|contracted procedure: k3632 
o|contracted procedure: k3668 
o|contracted procedure: k3640 
o|contracted procedure: k3644 
o|contracted procedure: k3652 
o|contracted procedure: k3656 
o|contracted procedure: k4444 
o|contracted procedure: k4566 
o|contracted procedure: k4450 
o|contracted procedure: k4528 
o|contracted procedure: k4453 
o|contracted procedure: k4523 
o|contracted procedure: k4459 
o|contracted procedure: k4466 
o|contracted procedure: k4470 
o|contracted procedure: k4490 
o|contracted procedure: k4501 
o|contracted procedure: k4504 
o|contracted procedure: k4511 
o|contracted procedure: k4545 
o|contracted procedure: k4552 
o|contracted procedure: k4542 
o|contracted procedure: k5146 
o|contracted procedure: k5152 
o|contracted procedure: k5155 
o|contracted procedure: k5161 
o|contracted procedure: k5167 
o|contracted procedure: k5173 
o|contracted procedure: k5189 
o|contracted procedure: k5193 
o|contracted procedure: k5201 
o|contracted procedure: k5205 
o|contracted procedure: k5213 
o|contracted procedure: k2706 
o|contracted procedure: k2721 
o|contracted procedure: k5228 
o|contracted procedure: k5371 
o|contracted procedure: k5259 
o|contracted procedure: k5262 
o|contracted procedure: k5268 
o|contracted procedure: k5277 
o|contracted procedure: k5283 
o|contracted procedure: k5295 
o|contracted procedure: k5298 
o|contracted procedure: k5304 
o|contracted procedure: k5310 
o|contracted procedure: k5316 
o|contracted procedure: k5323 
o|contracted procedure: k5327 
o|contracted procedure: k5358 
o|contracted procedure: k5466 
o|contracted procedure: k5435 
o|contracted procedure: k5442 
o|contracted procedure: k5446 
o|contracted procedure: k5461 
o|contracted procedure: k5454 
o|contracted procedure: k5506 
o|contracted procedure: k5475 
o|contracted procedure: k5482 
o|contracted procedure: k5486 
o|contracted procedure: k5501 
o|contracted procedure: k5494 
o|contracted procedure: k5571 
o|contracted procedure: k5515 
o|contracted procedure: k5518 
o|contracted procedure: k5524 
o|contracted procedure: k5527 
o|contracted procedure: k5536 
o|contracted procedure: k5546 
o|contracted procedure: k5550 
o|contracted procedure: k5558 
o|contracted procedure: k5693 
o|contracted procedure: k5580 
o|contracted procedure: k5583 
o|contracted procedure: k5590 
o|contracted procedure: k5598 
o|inlining procedure: k5594 
o|inlining procedure: k5594 
o|contracted procedure: k5610 
o|contracted procedure: k5620 
o|contracted procedure: k5623 
o|contracted procedure: k5629 
o|contracted procedure: k5653 
o|contracted procedure: k5649 
o|contracted procedure: k5670 
o|contracted procedure: k5666 
o|contracted procedure: k5673 
o|contracted procedure: k5749 
o|contracted procedure: k5702 
o|contracted procedure: k5709 
o|contracted procedure: k5713 
o|contracted procedure: k5721 
o|contracted procedure: k5730 
o|contracted procedure: k5737 
o|contracted procedure: k5759 
o|contracted procedure: k5826 
o|contracted procedure: k5765 
o|contracted procedure: k5772 
o|contracted procedure: k5776 
o|contracted procedure: k5779 
o|contracted procedure: k5797 
o|contracted procedure: k5793 
o|contracted procedure: k5807 
o|contracted procedure: k5814 
o|contracted procedure: k5836 
o|contracted procedure: k5944 
o|contracted procedure: k5845 
o|contracted procedure: k5852 
o|contracted procedure: k5856 
o|contracted procedure: k5867 
o|contracted procedure: k5906 
o|contracted procedure: k5873 
o|contracted procedure: k5880 
o|contracted procedure: k5884 
o|contracted procedure: k5898 
o|contracted procedure: k5902 
o|contracted procedure: k5926 
o|contracted procedure: k5930 
o|contracted procedure: k5951 
o|contracted procedure: k5960 
o|contracted procedure: k6068 
o|contracted procedure: k5969 
o|contracted procedure: k5976 
o|contracted procedure: k5980 
o|contracted procedure: k5991 
o|contracted procedure: k6030 
o|contracted procedure: k5997 
o|contracted procedure: k6004 
o|contracted procedure: k6008 
o|contracted procedure: k6022 
o|contracted procedure: k6026 
o|contracted procedure: k6050 
o|contracted procedure: k6054 
o|contracted procedure: k6075 
o|contracted procedure: k6087 
o|contracted procedure: k6094 
o|contracted procedure: k6098 
o|contracted procedure: k6193 
o|contracted procedure: k6108 
o|contracted procedure: k6115 
o|contracted procedure: k6123 
o|contracted procedure: k6119 
o|contracted procedure: k6129 
o|contracted procedure: k6136 
o|contracted procedure: k6140 
o|contracted procedure: k6160 
o|contracted procedure: k6171 
o|contracted procedure: k6174 
o|contracted procedure: k6181 
o|contracted procedure: k6206 
o|contracted procedure: k6213 
o|contracted procedure: k6203 
o|contracted procedure: k6383 
o|contracted procedure: k6230 
o|contracted procedure: k6233 
o|contracted procedure: k6236 
o|contracted procedure: k6255 
o|contracted procedure: k6259 
o|contracted procedure: k6276 
o|contracted procedure: k6280 
o|contracted procedure: k6290 
o|contracted procedure: k6293 
o|contracted procedure: k6313 
o|contracted procedure: k6316 
o|contracted procedure: k6336 
o|contracted procedure: k6342 
o|contracted procedure: k6348 
o|contracted procedure: k6355 
o|contracted procedure: k6359 
o|contracted procedure: k6401 
o|contracted procedure: k6411 
o|contracted procedure: k6415 
o|contracted procedure: k6424 
o|contracted procedure: k6427 
o|contracted procedure: k6430 
o|contracted procedure: k6438 
o|contracted procedure: k6446 
o|simplifications: ((if . 4) (let . 47)) 
o|removed binding forms: 257 
o|contracted procedure: k5839 
o|contracted procedure: k5963 
o|removed binding forms: 3 
o|direct leaf routine/allocation: loop327 0 
o|contracted procedure: k3250 
o|converted assignments to bindings: (loop327) 
o|simplifications: ((let . 1)) 
o|removed binding forms: 1 
o|customizable procedures: (map-loop552570 for-each-loop590608 op1613 k6266 k6269 rewrite-c..r728 k6153 k5786 k5635 a5220 loop422 chicken.compiler.c-platform#filter k4483 map-loop694711 chicken.compiler.c-platform#cons* k3103 k3106 foldr254257 g259260 loop204) 
o|calls to known targets: 63 
o|identified direct recursive calls: f_2123 1 
o|identified direct recursive calls: f_2302 1 
o|identified direct recursive calls: f_2459 1 
o|fast box initializations: 6 
o|fast global references: 6 
o|fast global assignments: 2 
o|dropping unused closure argument: f_2117 
o|dropping unused closure argument: f_2293 
o|dropping unused closure argument: f_2459 
o|dropping unused closure argument: f_3010 
o|dropping unused closure argument: f_3425 
*/
/* end of file */
