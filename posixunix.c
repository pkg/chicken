/* Generated from posix.scm by the CHICKEN compiler
   http://www.call-cc.org
   Version 5.2.0 (rev 317468e4)
   linux-unix-gnu-x86-64 [ 64bit dload ptables ]
   command line: posix.scm -optimize-level 2 -include-path . -include-path ./ -inline -ignore-repository -feature chicken-bootstrap -no-warnings -specialize -consult-types-file ./types.db -explicit-use -no-trace -output-file posixunix.c -feature platform-unix -emit-import-library chicken.errno -emit-import-library chicken.file.posix -emit-import-library chicken.time.posix -emit-import-library chicken.process -emit-import-library chicken.process.signal -emit-import-library chicken.process-context.posix -no-module-registration
   unit: posix
   uses: scheduler pathname extras port lolevel library
*/
#include "chicken.h"


#include <signal.h>

static int C_not_implemented(void);
int C_not_implemented() { return -1; }

static C_TLS struct stat C_statbuf;

#define C_stat_type         (C_statbuf.st_mode & S_IFMT)
#define C_stat_perm         (C_statbuf.st_mode & ~S_IFMT)

#define C_u_i_stat(fn)      C_fix(C_stat(C_c_string(fn), &C_statbuf))
#define C_u_i_fstat(fd)     C_fix(fstat(C_unfix(fd), &C_statbuf))

#ifndef S_IFSOCK
# define S_IFSOCK           0140000
#endif

#ifndef S_IRUSR
# define S_IRUSR  S_IREAD
#endif
#ifndef S_IWUSR
# define S_IWUSR  S_IWRITE
#endif
#ifndef S_IXUSR
# define S_IXUSR  S_IEXEC
#endif

#ifndef S_IRGRP
# define S_IRGRP  S_IREAD
#endif
#ifndef S_IWGRP
# define S_IWGRP  S_IWRITE
#endif
#ifndef S_IXGRP
# define S_IXGRP  S_IEXEC
#endif

#ifndef S_IROTH
# define S_IROTH  S_IREAD
#endif
#ifndef S_IWOTH
# define S_IWOTH  S_IWRITE
#endif
#ifndef S_IXOTH
# define S_IXOTH  S_IEXEC
#endif

#define cpy_tmvec_to_tmstc08(ptm, v) \
    ((ptm)->tm_sec = C_unfix(C_block_item((v), 0)), \
    (ptm)->tm_min = C_unfix(C_block_item((v), 1)), \
    (ptm)->tm_hour = C_unfix(C_block_item((v), 2)), \
    (ptm)->tm_mday = C_unfix(C_block_item((v), 3)), \
    (ptm)->tm_mon = C_unfix(C_block_item((v), 4)), \
    (ptm)->tm_year = C_unfix(C_block_item((v), 5)), \
    (ptm)->tm_wday = C_unfix(C_block_item((v), 6)), \
    (ptm)->tm_yday = C_unfix(C_block_item((v), 7)), \
    (ptm)->tm_isdst = (C_block_item((v), 8) != C_SCHEME_FALSE))

#define cpy_tmvec_to_tmstc9(ptm, v) \
    (((struct tm *)ptm)->tm_gmtoff = -C_unfix(C_block_item((v), 9)))

#define C_tm_set_08(v, tm)  cpy_tmvec_to_tmstc08( (tm), (v) )
#define C_tm_set_9(v, tm)   cpy_tmvec_to_tmstc9( (tm), (v) )

static struct tm *
C_tm_set( C_word v, void *tm )
{
  C_tm_set_08( v, (struct tm *)tm );
#if defined(C_GNU_ENV) && !defined(__CYGWIN__) && !defined(__uClinux__)
  C_tm_set_9( v, (struct tm *)tm );
#endif
  return tm;
}

#define TIME_STRING_MAXLENGTH 255
static char C_time_string [TIME_STRING_MAXLENGTH + 1];
#undef TIME_STRING_MAXLENGTH

#define C_strftime(v, f, tm) \
        (strftime(C_time_string, sizeof(C_time_string), C_c_string(f), C_tm_set((v), (tm))) ? C_time_string : NULL)
#define C_a_mktime(ptr, c, v, tm)  C_int64_to_num(ptr, mktime(C_tm_set((v), C_data_pointer(tm))))
#define C_asctime(v, tm)    (asctime(C_tm_set((v), (tm))))

#define C_fdopen(a, n, fd, m) C_mpointer(a, fdopen(C_unfix(fd), C_c_string(m)))
#define C_dup(x)            C_fix(dup(C_unfix(x)))
#define C_dup2(x, y)        C_fix(dup2(C_unfix(x), C_unfix(y)))

#define C_set_file_ptr(port, ptr)  (C_set_block_item(port, 0, (C_block_item(ptr, 0))), C_SCHEME_UNDEFINED)

/* It is assumed that 'int' is-a 'long' */
#define C_ftell(a, n, p)    C_int64_to_num(a, ftell(C_port_file(p)))
#define C_fseek(p, n, w)    C_mk_nbool(fseek(C_port_file(p), C_num_to_int64(n), C_unfix(w)))
#define C_lseek(fd, o, w)     C_fix(lseek(C_unfix(fd), C_num_to_int64(o), C_unfix(w)))

#ifndef S_IFLNK
#define S_IFLNK S_IFREG
#endif

#ifndef S_IFREG
#define S_IFREG S_IFREG
#endif

#ifndef S_IFDIR
#define S_IFDIR S_IFREG
#endif

#ifndef S_IFCHR
#define S_IFCHR S_IFREG
#endif

#ifndef S_IFBLK
#define S_IFBLK S_IFREG
#endif

#ifndef S_IFSOCK
#define S_IFSOCK S_IFREG
#endif

#ifndef S_IFIFO
#define S_IFIFO S_IFREG
#endif



static C_TLS int C_wait_status;

#include <sys/time.h>
#include <sys/wait.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <dirent.h>
#include <pwd.h>
#include <utime.h>

#if defined(__sun) && defined(__SVR4)
# include <sys/tty.h>
# include <termios.h>
#endif

#include <sys/mman.h>
#include <poll.h>

#ifndef O_FSYNC
# define O_FSYNC O_SYNC
#endif

#ifndef PIPE_BUF
# ifdef __CYGWIN__
#  define PIPE_BUF       _POSIX_PIPE_BUF
# else
#  define PIPE_BUF 1024
# endif
#endif

#ifndef O_BINARY
# define O_BINARY        0
#endif
#ifndef O_TEXT
# define O_TEXT          0
#endif

#ifndef MAP_FILE
# define MAP_FILE    0
#endif

#ifndef MAP_ANON
# define MAP_ANON    0
#endif

#ifndef FILENAME_MAX
# define FILENAME_MAX          1024
#endif

static C_TLS struct flock C_flock;
static C_TLS DIR *temphandle;
static C_TLS struct passwd *C_user;

/* Android doesn't provide pw_gecos in the passwd struct */
#ifdef __ANDROID__
# define C_PW_GECOS ("")
#else
# define C_PW_GECOS (C_user->pw_gecos)
#endif

static C_TLS int C_pipefds[ 2 ];
static C_TLS time_t C_secs;
static C_TLS struct timeval C_timeval;
static C_TLS struct stat C_statbuf;

#define C_fchdir(fd)        C_fix(fchdir(C_unfix(fd)))

#define open_binary_input_pipe(a, n, name)   C_mpointer(a, popen(C_c_string(name), "r"))
#define open_text_input_pipe(a, n, name)     open_binary_input_pipe(a, n, name)
#define open_binary_output_pipe(a, n, name)  C_mpointer(a, popen(C_c_string(name), "w"))
#define open_text_output_pipe(a, n, name)    open_binary_output_pipe(a, n, name)
#define close_pipe(p)                        C_fix(pclose(C_port_file(p)))

#define C_fork              fork
#define C_waitpid(id, o)    C_fix(waitpid(C_unfix(id), &C_wait_status, C_unfix(o)))
#define C_getppid           getppid
#define C_kill(id, s)       C_fix(kill(C_unfix(id), C_unfix(s)))
#define C_getuid            getuid
#define C_getgid            getgid
#define C_geteuid           geteuid
#define C_getegid           getegid
#define C_chown(fn, u, g)   C_fix(chown(C_c_string(fn), C_unfix(u), C_unfix(g)))
#define C_fchown(fd, u, g)  C_fix(fchown(C_unfix(fd), C_unfix(u), C_unfix(g)))
#define C_chmod(fn, m)      C_fix(chmod(C_c_string(fn), C_unfix(m)))
#define C_fchmod(fd, m)     C_fix(fchmod(C_unfix(fd), C_unfix(m)))
#define C_setuid(id)        C_fix(setuid(C_unfix(id)))
#define C_setgid(id)        C_fix(setgid(C_unfix(id)))
#define C_seteuid(id)       C_fix(seteuid(C_unfix(id)))
#define C_setegid(id)       C_fix(setegid(C_unfix(id)))
#define C_setsid(dummy)     C_fix(setsid())
#define C_setpgid(x, y)     C_fix(setpgid(C_unfix(x), C_unfix(y)))
#define C_getpgid(x)        C_fix(getpgid(C_unfix(x)))
#define C_symlink(o, n)     C_fix(symlink(C_c_string(o), C_c_string(n)))
#define C_do_readlink(f, b) C_fix(readlink(C_c_string(f), C_c_string(b), FILENAME_MAX))
#define C_getpwnam(n)       C_mk_bool((C_user = getpwnam(C_c_string(n))) != NULL)
#define C_getpwuid(u)       C_mk_bool((C_user = getpwuid(C_unfix(u))) != NULL)
#define C_pipe(d)           C_fix(pipe(C_pipefds))
#define C_truncate(f, n)    C_fix(truncate(C_c_string(f), C_num_to_int(n)))
#define C_ftruncate(f, n)   C_fix(ftruncate(C_unfix(f), C_num_to_int(n)))
#define C_alarm             alarm
#define C_close(fd)         C_fix(close(C_unfix(fd)))
#define C_umask(m)          C_fix(umask(C_unfix(m)))

#define C_u_i_lstat(fn)     C_fix(lstat(C_c_string(fn), &C_statbuf))

#define C_u_i_execvp(f,a)   C_fix(execvp(C_c_string(f), (char *const *)C_c_pointer_vector_or_null(a)))
#define C_u_i_execve(f,a,e) C_fix(execve(C_c_string(f), (char *const *)C_c_pointer_vector_or_null(a), (char *const *)C_c_pointer_vector_or_null(e)))

#if defined(__FreeBSD__) || defined(C_MACOSX) || defined(__NetBSD__) || defined(__OpenBSD__) || defined(__sgi__) || defined(sgi) || defined(__DragonFly__) || defined(__SUNPRO_C)
static C_TLS int C_uw;
# define C_WIFEXITED(n)      (C_uw = C_unfix(n), C_mk_bool(WIFEXITED(C_uw)))
# define C_WIFSIGNALED(n)    (C_uw = C_unfix(n), C_mk_bool(WIFSIGNALED(C_uw)))
# define C_WIFSTOPPED(n)     (C_uw = C_unfix(n), C_mk_bool(WIFSTOPPED(C_uw)))
# define C_WEXITSTATUS(n)    (C_uw = C_unfix(n), C_fix(WEXITSTATUS(C_uw)))
# define C_WTERMSIG(n)       (C_uw = C_unfix(n), C_fix(WTERMSIG(C_uw)))
# define C_WSTOPSIG(n)       (C_uw = C_unfix(n), C_fix(WSTOPSIG(C_uw)))
#else
# define C_WIFEXITED(n)      C_mk_bool(WIFEXITED(C_unfix(n)))
# define C_WIFSIGNALED(n)    C_mk_bool(WIFSIGNALED(C_unfix(n)))
# define C_WIFSTOPPED(n)     C_mk_bool(WIFSTOPPED(C_unfix(n)))
# define C_WEXITSTATUS(n)    C_fix(WEXITSTATUS(C_unfix(n)))
# define C_WTERMSIG(n)       C_fix(WTERMSIG(C_unfix(n)))
# define C_WSTOPSIG(n)       C_fix(WSTOPSIG(C_unfix(n)))
#endif

#ifdef __CYGWIN__
# define C_mkfifo(fn, m)    C_fix(-1)
#else
# define C_mkfifo(fn, m)    C_fix(mkfifo(C_c_string(fn), C_unfix(m)))
#endif

#define C_flock_setup(t, s, n) (C_flock.l_type = C_unfix(t), C_flock.l_start = C_num_to_int(s), C_flock.l_whence = SEEK_SET, C_flock.l_len = C_num_to_int(n), C_SCHEME_UNDEFINED)
#define C_flock_test(p)     (fcntl(fileno(C_port_file(p)), F_GETLK, &C_flock) >= 0 ? (C_flock.l_type == F_UNLCK ? C_fix(0) : C_fix(C_flock.l_pid)) : C_SCHEME_FALSE)
#define C_flock_lock(p)     C_fix(fcntl(fileno(C_port_file(p)), F_SETLK, &C_flock))
#define C_flock_lockw(p)    C_fix(fcntl(fileno(C_port_file(p)), F_SETLKW, &C_flock))

static C_TLS sigset_t C_sigset;
#define C_sigemptyset(d)    (sigemptyset(&C_sigset), C_SCHEME_UNDEFINED)
#define C_sigaddset(s)      (sigaddset(&C_sigset, C_unfix(s)), C_SCHEME_UNDEFINED)
#define C_sigdelset(s)      (sigdelset(&C_sigset, C_unfix(s)), C_SCHEME_UNDEFINED)
#define C_sigismember(s)    C_mk_bool(sigismember(&C_sigset, C_unfix(s)))
#define C_sigprocmask_set(d)        C_fix(sigprocmask(SIG_SETMASK, &C_sigset, NULL))
#define C_sigprocmask_block(d)      C_fix(sigprocmask(SIG_BLOCK, &C_sigset, NULL))
#define C_sigprocmask_unblock(d)    C_fix(sigprocmask(SIG_UNBLOCK, &C_sigset, NULL))
#define C_sigprocmask_get(d)        C_fix(sigprocmask(SIG_SETMASK, NULL, &C_sigset))

#define C_open(fn, fl, m)   C_fix(open(C_c_string(fn), C_unfix(fl), C_unfix(m)))
#define C_read(fd, b, n)    C_fix(read(C_unfix(fd), C_data_pointer(b), C_unfix(n)))
#define C_write(fd, b, n)   C_fix(write(C_unfix(fd), C_data_pointer(b), C_unfix(n)))
#define C_mkstemp(t)        C_fix(mkstemp(C_c_string(t)))

#define C_ctime(n)          (C_secs = (n), ctime(&C_secs))

#if defined(__SVR4) || defined(C_MACOSX) || defined(__ANDROID__) || defined(_AIX)
/* Seen here: http://lists.samba.org/archive/samba-technical/2002-November/025571.html */

static time_t C_timegm(struct tm *t)
{
  time_t tl, tb;
  struct tm *tg;

  tl = mktime (t);
  if (tl == -1)
    {
      t->tm_hour--;
      tl = mktime (t);
      if (tl == -1)
        return -1; /* can't deal with output from strptime */
      tl += 3600;
    }
  tg = gmtime (&tl);
  tg->tm_isdst = 0;
  tb = mktime (tg);
  if (tb == -1)
    {
      tg->tm_hour--;
      tb = mktime (tg);
      if (tb == -1)
        return -1; /* can't deal with output from gmtime */
      tb += 3600;
    }
  return (tl - (tb - tl));
}
#else
#define C_timegm timegm
#endif

#define C_a_timegm(ptr, c, v, tm)  C_int64_to_num(ptr, C_timegm(C_tm_set((v), C_data_pointer(tm))))

#ifdef __linux__
extern char *strptime(const char *s, const char *format, struct tm *tm);
extern pid_t getpgid(pid_t pid);
#endif

/* tm_get could be in posix-common, but it's only used in here */
#define cpy_tmstc08_to_tmvec(v, ptm) \
    (C_set_block_item((v), 0, C_fix(((struct tm *)ptm)->tm_sec)), \
    C_set_block_item((v), 1, C_fix((ptm)->tm_min)), \
    C_set_block_item((v), 2, C_fix((ptm)->tm_hour)), \
    C_set_block_item((v), 3, C_fix((ptm)->tm_mday)), \
    C_set_block_item((v), 4, C_fix((ptm)->tm_mon)), \
    C_set_block_item((v), 5, C_fix((ptm)->tm_year)), \
    C_set_block_item((v), 6, C_fix((ptm)->tm_wday)), \
    C_set_block_item((v), 7, C_fix((ptm)->tm_yday)), \
    C_set_block_item((v), 8, ((ptm)->tm_isdst ? C_SCHEME_TRUE : C_SCHEME_FALSE)))

#define cpy_tmstc9_to_tmvec(v, ptm) \
    (C_set_block_item((v), 9, C_fix(-(ptm)->tm_gmtoff)))

#define C_tm_get_08(v, tm)  cpy_tmstc08_to_tmvec( (v), (tm) )
#define C_tm_get_9(v, tm)   cpy_tmstc9_to_tmvec( (v), (tm) )

static C_word
C_tm_get( C_word v, void *tm )
{
  C_tm_get_08( v, (struct tm *)tm );
#if defined(C_GNU_ENV) && !defined(__CYGWIN__) && !defined(__uClinux__)
  C_tm_get_9( v, (struct tm *)tm );
#endif
  return v;
}

#define C_strptime(s, f, v, stm) \
        (strptime(C_c_string(s), C_c_string(f), ((struct tm *)(stm))) ? C_tm_get((v), (stm)) : C_SCHEME_FALSE)

static int set_file_mtime(char *filename, C_word atime, C_word mtime)
{
  struct stat sb;
  struct utimbuf tb;

  /* Only lstat if needed */
  if (atime == C_SCHEME_FALSE || mtime == C_SCHEME_FALSE) {
    if (lstat(filename, &sb) == -1) return -1;
  }

  if (atime == C_SCHEME_FALSE) {
    tb.actime = sb.st_atime;
  } else {
    tb.actime = C_num_to_int64(atime);
  }
  if (mtime == C_SCHEME_FALSE) {
    tb.modtime = sb.st_mtime;
  } else {
    tb.modtime = C_num_to_int64(mtime);
  }
  return utime(filename, &tb);
}



static C_PTABLE_ENTRY *create_ptable(void);
C_noret_decl(C_scheduler_toplevel)
C_externimport void C_ccall C_scheduler_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_pathname_toplevel)
C_externimport void C_ccall C_pathname_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_extras_toplevel)
C_externimport void C_ccall C_extras_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_port_toplevel)
C_externimport void C_ccall C_port_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_lolevel_toplevel)
C_externimport void C_ccall C_lolevel_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_library_toplevel)
C_externimport void C_ccall C_library_toplevel(C_word c,C_word *av) C_noret;

static C_TLS C_word lf[492];
static double C_possibly_force_alignment;
static C_char C_TLS li0[] C_aligned={C_lihdr(0,0,28),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,115,121,115,116,101,109,32,99,109,100,41,0,0,0,0};
static C_char C_TLS li1[] C_aligned={C_lihdr(0,0,29),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,115,121,115,116,101,109,42,32,115,116,114,41,0,0,0};
static C_char C_TLS li2[] C_aligned={C_lihdr(0,0,8),40,103,50,53,54,32,99,41};
static C_char C_TLS li3[] C_aligned={C_lihdr(0,0,18),40,109,97,112,45,108,111,111,112,50,53,48,32,103,50,54,50,41,0,0,0,0,0,0};
static C_char C_TLS li4[] C_aligned={C_lihdr(0,0,31),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,113,115,32,115,116,114,32,46,32,114,101,115,116,41,0};
static C_char C_TLS li5[] C_aligned={C_lihdr(0,0,47),40,99,104,105,99,107,101,110,46,112,111,115,105,120,35,112,111,115,105,120,45,101,114,114,111,114,32,116,121,112,101,32,108,111,99,32,109,115,103,32,46,32,97,114,103,115,41,0};
static C_char C_TLS li6[] C_aligned={C_lihdr(0,0,38),40,99,104,105,99,107,101,110,46,112,111,115,105,120,35,115,116,97,116,32,102,105,108,101,32,108,105,110,107,32,101,114,114,32,108,111,99,41,0,0};
static C_char C_TLS li7[] C_aligned={C_lihdr(0,0,39),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,115,116,97,116,32,102,32,46,32,114,101,115,116,41,0};
static C_char C_TLS li8[] C_aligned={C_lihdr(0,0,46),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,115,101,116,45,102,105,108,101,45,112,101,114,109,105,115,115,105,111,110,115,33,32,102,32,112,41,0,0};
static C_char C_TLS li9[] C_aligned={C_lihdr(0,0,45),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,109,111,100,105,102,105,99,97,116,105,111,110,45,116,105,109,101,32,102,41,0,0,0};
static C_char C_TLS li10[] C_aligned={C_lihdr(0,0,39),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,97,99,99,101,115,115,45,116,105,109,101,32,102,41,0};
static C_char C_TLS li11[] C_aligned={C_lihdr(0,0,39),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,99,104,97,110,103,101,45,116,105,109,101,32,102,41,0};
static C_char C_TLS li12[] C_aligned={C_lihdr(0,0,45),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,115,101,116,45,102,105,108,101,45,116,105,109,101,115,33,32,102,32,46,32,114,101,115,116,41,0,0,0};
static C_char C_TLS li13[] C_aligned={C_lihdr(0,0,32),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,115,105,122,101,32,102,41};
static C_char C_TLS li14[] C_aligned={C_lihdr(0,0,42),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,115,101,116,45,102,105,108,101,45,111,119,110,101,114,33,32,102,32,117,105,100,41,0,0,0,0,0,0};
static C_char C_TLS li15[] C_aligned={C_lihdr(0,0,42),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,115,101,116,45,102,105,108,101,45,103,114,111,117,112,33,32,102,32,103,105,100,41,0,0,0,0,0,0};
static C_char C_TLS li16[] C_aligned={C_lihdr(0,0,42),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,116,121,112,101,32,102,105,108,101,32,46,32,114,101,115,116,41,0,0,0,0,0,0};
static C_char C_TLS li17[] C_aligned={C_lihdr(0,0,39),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,114,101,103,117,108,97,114,45,102,105,108,101,63,32,102,105,108,101,41,0};
static C_char C_TLS li18[] C_aligned={C_lihdr(0,0,40),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,115,121,109,98,111,108,105,99,45,108,105,110,107,63,32,102,105,108,101,41};
static C_char C_TLS li19[] C_aligned={C_lihdr(0,0,39),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,98,108,111,99,107,45,100,101,118,105,99,101,63,32,102,105,108,101,41,0};
static C_char C_TLS li20[] C_aligned={C_lihdr(0,0,43),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,99,104,97,114,97,99,116,101,114,45,100,101,118,105,99,101,63,32,102,105,108,101,41,0,0,0,0,0};
static C_char C_TLS li21[] C_aligned={C_lihdr(0,0,31),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,102,111,63,32,102,105,108,101,41,0};
static C_char C_TLS li22[] C_aligned={C_lihdr(0,0,33),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,115,111,99,107,101,116,63,32,102,105,108,101,41,0,0,0,0,0,0,0};
static C_char C_TLS li23[] C_aligned={C_lihdr(0,0,36),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,100,105,114,101,99,116,111,114,121,63,32,102,105,108,101,41,0,0,0,0};
static C_char C_TLS li24[] C_aligned={C_lihdr(0,0,57),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,115,101,116,45,102,105,108,101,45,112,111,115,105,116,105,111,110,33,32,112,111,114,116,32,112,111,115,32,46,32,119,104,101,110,99,101,41,0,0,0,0,0,0,0};
static C_char C_TLS li25[] C_aligned={C_lihdr(0,0,16),40,109,111,100,101,32,105,110,112,32,109,32,108,111,99,41};
static C_char C_TLS li26[] C_aligned={C_lihdr(0,0,20),40,99,104,101,99,107,32,108,111,99,32,102,100,32,105,110,112,32,114,41,0,0,0,0};
static C_char C_TLS li27[] C_aligned={C_lihdr(0,0,44),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,111,112,101,110,45,105,110,112,117,116,45,102,105,108,101,42,32,102,100,32,46,32,109,41,0,0,0,0};
static C_char C_TLS li28[] C_aligned={C_lihdr(0,0,45),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,111,112,101,110,45,111,117,116,112,117,116,45,102,105,108,101,42,32,102,100,32,46,32,109,41,0,0,0};
static C_char C_TLS li29[] C_aligned={C_lihdr(0,0,38),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,112,111,114,116,45,62,102,105,108,101,110,111,32,112,111,114,116,41,0,0};
static C_char C_TLS li30[] C_aligned={C_lihdr(0,0,47),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,100,117,112,108,105,99,97,116,101,45,102,105,108,101,110,111,32,111,108,100,32,46,32,110,101,119,41,0};
static C_char C_TLS li31[] C_aligned={C_lihdr(0,0,50),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,45,99,111,110,116,101,120,116,46,112,111,115,105,120,35,99,117,114,114,101,110,116,45,112,114,111,99,101,115,115,45,105,100,41,0,0,0,0,0,0};
static C_char C_TLS li32[] C_aligned={C_lihdr(0,0,52),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,45,99,111,110,116,101,120,116,46,112,111,115,105,120,35,99,104,97,110,103,101,45,100,105,114,101,99,116,111,114,121,42,32,102,100,41,0,0,0,0};
static C_char C_TLS li33[] C_aligned={C_lihdr(0,0,33),40,35,35,115,121,115,35,99,104,97,110,103,101,45,100,105,114,101,99,116,111,114,121,45,104,111,111,107,32,100,105,114,41,0,0,0,0,0,0,0};
static C_char C_TLS li34[] C_aligned={C_lihdr(0,0,16),67,95,100,101,99,111,100,101,95,115,101,99,111,110,100,115};
static C_char C_TLS li35[] C_aligned={C_lihdr(0,0,40),40,99,104,105,99,107,101,110,46,112,111,115,105,120,35,99,104,101,99,107,45,116,105,109,101,45,118,101,99,116,111,114,32,108,111,99,32,116,109,41};
static C_char C_TLS li36[] C_aligned={C_lihdr(0,0,47),40,99,104,105,99,107,101,110,46,116,105,109,101,46,112,111,115,105,120,35,115,101,99,111,110,100,115,45,62,108,111,99,97,108,45,116,105,109,101,32,46,32,114,101,115,116,41,0};
static C_char C_TLS li37[] C_aligned={C_lihdr(0,0,45),40,99,104,105,99,107,101,110,46,116,105,109,101,46,112,111,115,105,120,35,115,101,99,111,110,100,115,45,62,117,116,99,45,116,105,109,101,32,46,32,114,101,115,116,41,0,0,0};
static C_char C_TLS li38[] C_aligned={C_lihdr(0,0,43),40,99,104,105,99,107,101,110,46,116,105,109,101,46,112,111,115,105,120,35,115,101,99,111,110,100,115,45,62,115,116,114,105,110,103,32,46,32,114,101,115,116,41,0,0,0,0,0};
static C_char C_TLS li39[] C_aligned={C_lihdr(0,0,43),40,99,104,105,99,107,101,110,46,116,105,109,101,46,112,111,115,105,120,35,108,111,99,97,108,45,116,105,109,101,45,62,115,101,99,111,110,100,115,32,116,109,41,0,0,0,0,0};
static C_char C_TLS li40[] C_aligned={C_lihdr(0,0,43),40,99,104,105,99,107,101,110,46,116,105,109,101,46,112,111,115,105,120,35,116,105,109,101,45,62,115,116,114,105,110,103,32,116,109,32,46,32,114,101,115,116,41,0,0,0,0,0};
static C_char C_TLS li41[] C_aligned={C_lihdr(0,0,53),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,46,115,105,103,110,97,108,35,115,101,116,45,115,105,103,110,97,108,45,104,97,110,100,108,101,114,33,32,115,105,103,32,112,114,111,99,41,0,0,0};
static C_char C_TLS li42[] C_aligned={C_lihdr(0,0,33),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,112,114,111,99,101,115,115,45,115,108,101,101,112,32,110,41,0,0,0,0,0,0,0};
static C_char C_TLS li43[] C_aligned={C_lihdr(0,0,7),40,97,51,56,57,48,41,0};
static C_char C_TLS li44[] C_aligned={C_lihdr(0,0,24),40,97,51,56,57,54,32,101,112,105,100,32,101,110,111,114,109,32,101,99,111,100,101,41};
static C_char C_TLS li45[] C_aligned={C_lihdr(0,0,37),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,112,114,111,99,101,115,115,45,119,97,105,116,32,46,32,97,114,103,115,41,0,0,0};
static C_char C_TLS li46[] C_aligned={C_lihdr(0,0,7),40,97,51,57,54,53,41,0};
static C_char C_TLS li47[] C_aligned={C_lihdr(0,0,11),40,97,51,57,53,57,32,101,120,110,41,0,0,0,0,0};
static C_char C_TLS li48[] C_aligned={C_lihdr(0,0,16),40,100,111,108,111,111,112,57,57,51,32,115,108,32,105,41};
static C_char C_TLS li49[] C_aligned={C_lihdr(0,0,10),40,116,109,112,49,50,53,50,53,41,0,0,0,0,0,0};
static C_char C_TLS li50[] C_aligned={C_lihdr(0,0,7),40,97,52,48,51,57,41,0};
static C_char C_TLS li51[] C_aligned={C_lihdr(0,0,15),40,116,109,112,50,50,53,50,54,32,97,114,103,115,41,0};
static C_char C_TLS li52[] C_aligned={C_lihdr(0,0,7),40,97,51,57,55,52,41,0};
static C_char C_TLS li53[] C_aligned={C_lihdr(0,0,9),40,97,51,57,53,51,32,107,41,0,0,0,0,0,0,0};
static C_char C_TLS li54[] C_aligned={C_lihdr(0,0,61),40,99,104,105,99,107,101,110,46,112,111,115,105,120,35,108,105,115,116,45,62,99,45,115,116,114,105,110,103,45,98,117,102,102,101,114,32,115,116,114,105,110,103,45,108,105,115,116,32,99,111,110,118,101,114,116,32,108,111,99,41,0,0,0};
static C_char C_TLS li55[] C_aligned={C_lihdr(0,0,14),40,100,111,108,111,111,112,49,48,49,50,32,105,41,0,0};
static C_char C_TLS li56[] C_aligned={C_lihdr(0,0,49),40,99,104,105,99,107,101,110,46,112,111,115,105,120,35,102,114,101,101,45,99,45,115,116,114,105,110,103,45,98,117,102,102,101,114,32,98,117,102,102,101,114,45,97,114,114,97,121,41,0,0,0,0,0,0,0};
static C_char C_TLS li57[] C_aligned={C_lihdr(0,0,7),40,103,49,48,50,53,41,0};
static C_char C_TLS li58[] C_aligned={C_lihdr(0,0,19),40,102,111,114,45,101,97,99,104,45,108,111,111,112,49,48,50,52,41,0,0,0,0,0};
static C_char C_TLS li59[] C_aligned={C_lihdr(0,0,46),40,99,104,105,99,107,101,110,46,112,111,115,105,120,35,99,104,101,99,107,45,101,110,118,105,114,111,110,109,101,110,116,45,108,105,115,116,32,108,115,116,32,108,111,99,41,0,0};
static C_char C_TLS li60[] C_aligned={C_lihdr(0,0,7),40,110,111,112,32,120,41,0};
static C_char C_TLS li61[] C_aligned={C_lihdr(0,0,7),40,97,52,49,54,55,41,0};
static C_char C_TLS li62[] C_aligned={C_lihdr(0,0,11),40,97,52,49,54,49,32,101,120,110,41,0,0,0,0,0};
static C_char C_TLS li63[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,49,48,54,56,32,103,49,48,56,48,41,0,0,0,0};
static C_char C_TLS li64[] C_aligned={C_lihdr(0,0,7),40,97,52,49,56,56,41,0};
static C_char C_TLS li65[] C_aligned={C_lihdr(0,0,7),40,97,52,50,54,57,41,0};
static C_char C_TLS li66[] C_aligned={C_lihdr(0,0,14),40,97,52,50,54,51,32,46,32,97,114,103,115,41,0,0};
static C_char C_TLS li67[] C_aligned={C_lihdr(0,0,7),40,97,52,49,56,50,41,0};
static C_char C_TLS li68[] C_aligned={C_lihdr(0,0,9),40,97,52,49,53,53,32,107,41,0,0,0,0,0,0,0};
static C_char C_TLS li69[] C_aligned={C_lihdr(0,0,77),40,99,104,105,99,107,101,110,46,112,111,115,105,120,35,99,97,108,108,45,119,105,116,104,45,101,120,101,99,45,97,114,103,115,32,108,111,99,32,102,105,108,101,110,97,109,101,32,97,114,103,99,111,110,118,32,97,114,103,108,105,115,116,32,101,110,118,108,105,115,116,32,112,114,111,99,41,0,0,0};
static C_char C_TLS li70[] C_aligned={C_lihdr(0,0,21),40,99,104,101,99,107,32,108,111,99,32,99,109,100,32,105,110,112,32,114,41,0,0,0};
static C_char C_TLS li71[] C_aligned={C_lihdr(0,0,41),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,111,112,101,110,45,105,110,112,117,116,45,112,105,112,101,32,99,109,100,32,46,32,109,41,0,0,0,0,0,0,0};
static C_char C_TLS li72[] C_aligned={C_lihdr(0,0,42),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,111,112,101,110,45,111,117,116,112,117,116,45,112,105,112,101,32,99,109,100,32,46,32,109,41,0,0,0,0,0,0};
static C_char C_TLS li73[] C_aligned={C_lihdr(0,0,39),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,99,108,111,115,101,45,105,110,112,117,116,45,112,105,112,101,32,112,111,114,116,41,0};
static C_char C_TLS li74[] C_aligned={C_lihdr(0,0,40),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,99,108,111,115,101,45,111,117,116,112,117,116,45,112,105,112,101,32,112,111,114,116,41};
static C_char C_TLS li75[] C_aligned={C_lihdr(0,0,7),40,97,52,52,51,50,41,0};
static C_char C_TLS li76[] C_aligned={C_lihdr(0,0,17),40,97,52,52,52,51,32,46,32,114,101,115,117,108,116,115,41,0,0,0,0,0,0,0};
static C_char C_TLS li77[] C_aligned={C_lihdr(0,0,7),40,97,52,52,51,55,41,0};
static C_char C_TLS li78[] C_aligned={C_lihdr(0,0,7),40,97,52,52,53,50,41,0};
static C_char C_TLS li79[] C_aligned={C_lihdr(0,0,55),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,119,105,116,104,45,105,110,112,117,116,45,102,114,111,109,45,112,105,112,101,32,99,109,100,32,116,104,117,110,107,32,46,32,109,111,100,101,41,0};
static C_char C_TLS li80[] C_aligned={C_lihdr(0,0,7),40,97,52,52,54,54,41,0};
static C_char C_TLS li81[] C_aligned={C_lihdr(0,0,17),40,97,52,52,55,50,32,46,32,114,101,115,117,108,116,115,41,0,0,0,0,0,0,0};
static C_char C_TLS li82[] C_aligned={C_lihdr(0,0,55),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,99,97,108,108,45,119,105,116,104,45,111,117,116,112,117,116,45,112,105,112,101,32,99,109,100,32,112,114,111,99,32,46,32,109,111,100,101,41,0};
static C_char C_TLS li83[] C_aligned={C_lihdr(0,0,7),40,97,52,52,57,48,41,0};
static C_char C_TLS li84[] C_aligned={C_lihdr(0,0,17),40,97,52,52,57,54,32,46,32,114,101,115,117,108,116,115,41,0,0,0,0,0,0,0};
static C_char C_TLS li85[] C_aligned={C_lihdr(0,0,54),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,99,97,108,108,45,119,105,116,104,45,105,110,112,117,116,45,112,105,112,101,32,99,109,100,32,112,114,111,99,32,46,32,109,111,100,101,41,0,0};
static C_char C_TLS li86[] C_aligned={C_lihdr(0,0,7),40,97,52,53,49,52,41,0};
static C_char C_TLS li87[] C_aligned={C_lihdr(0,0,17),40,97,52,53,50,53,32,46,32,114,101,115,117,108,116,115,41,0,0,0,0,0,0,0};
static C_char C_TLS li88[] C_aligned={C_lihdr(0,0,7),40,97,52,53,49,57,41,0};
static C_char C_TLS li89[] C_aligned={C_lihdr(0,0,7),40,97,52,53,51,52,41,0};
static C_char C_TLS li90[] C_aligned={C_lihdr(0,0,54),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,119,105,116,104,45,111,117,116,112,117,116,45,116,111,45,112,105,112,101,32,99,109,100,32,116,104,117,110,107,32,46,32,109,111,100,101,41,0,0};
static C_char C_TLS li91[] C_aligned={C_lihdr(0,0,32),40,35,35,115,121,115,35,102,105,108,101,45,110,111,110,98,108,111,99,107,105,110,103,33,32,102,100,49,50,54,55,41};
static C_char C_TLS li92[] C_aligned={C_lihdr(0,0,31),40,35,35,115,121,115,35,102,105,108,101,45,115,101,108,101,99,116,45,111,110,101,32,105,110,116,49,50,55,50,41,0};
static C_char C_TLS li93[] C_aligned={C_lihdr(0,0,47),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,99,111,110,116,114,111,108,32,102,100,32,99,109,100,32,46,32,114,101,115,116,41,0};
static C_char C_TLS li94[] C_aligned={C_lihdr(0,0,52),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,111,112,101,110,32,102,105,108,101,110,97,109,101,32,102,108,97,103,115,32,46,32,109,111,100,101,41,0,0,0,0};
static C_char C_TLS li95[] C_aligned={C_lihdr(0,0,6),40,108,111,111,112,41,0,0};
static C_char C_TLS li96[] C_aligned={C_lihdr(0,0,34),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,99,108,111,115,101,32,102,100,41,0,0,0,0,0,0};
static C_char C_TLS li97[] C_aligned={C_lihdr(0,0,47),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,114,101,97,100,32,102,100,32,115,105,122,101,32,46,32,98,117,102,102,101,114,41,0};
static C_char C_TLS li98[] C_aligned={C_lihdr(0,0,48),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,119,114,105,116,101,32,102,100,32,98,117,102,102,101,114,32,46,32,115,105,122,101,41};
static C_char C_TLS li99[] C_aligned={C_lihdr(0,0,42),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,109,107,115,116,101,109,112,32,116,101,109,112,108,97,116,101,41,0,0,0,0,0,0};
static C_char C_TLS li100[] C_aligned={C_lihdr(0,0,18),40,100,111,108,111,111,112,49,51,54,54,32,102,100,115,114,108,41,0,0,0,0,0,0};
static C_char C_TLS li101[] C_aligned={C_lihdr(0,0,18),40,100,111,108,111,111,112,49,51,54,55,32,102,100,115,119,108,41,0,0,0,0,0,0};
static C_char C_TLS li102[] C_aligned={C_lihdr(0,0,14),40,108,112,32,105,32,114,101,115,32,102,100,115,41,0,0};
static C_char C_TLS li103[] C_aligned={C_lihdr(0,0,14),40,108,112,32,105,32,114,101,115,32,102,100,115,41,0,0};
static C_char C_TLS li104[] C_aligned={C_lihdr(0,0,52),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,115,101,108,101,99,116,32,102,100,115,114,32,102,100,115,119,32,46,32,116,105,109,101,111,117,116,41,0,0,0,0};
static C_char C_TLS li105[] C_aligned={C_lihdr(0,0,36),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,99,114,101,97,116,101,45,112,105,112,101,32,46,32,114,101,115,116,41,0,0,0,0};
static C_char C_TLS li106[] C_aligned={C_lihdr(0,0,25),40,102,111,114,45,101,97,99,104,45,108,111,111,112,49,52,56,48,32,103,49,52,56,55,41,0,0,0,0,0,0,0};
static C_char C_TLS li107[] C_aligned={C_lihdr(0,0,46),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,46,115,105,103,110,97,108,35,115,101,116,45,115,105,103,110,97,108,45,109,97,115,107,33,32,115,105,103,115,41,0,0};
static C_char C_TLS li108[] C_aligned={C_lihdr(0,0,43),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,46,115,105,103,110,97,108,35,115,105,103,110,97,108,45,109,97,115,107,101,100,63,32,115,105,103,41,0,0,0,0,0};
static C_char C_TLS li109[] C_aligned={C_lihdr(0,0,41),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,46,115,105,103,110,97,108,35,115,105,103,110,97,108,45,109,97,115,107,33,32,115,105,103,41,0,0,0,0,0,0,0};
static C_char C_TLS li110[] C_aligned={C_lihdr(0,0,43),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,46,115,105,103,110,97,108,35,115,105,103,110,97,108,45,117,110,109,97,115,107,33,32,115,105,103,41,0,0,0,0,0};
static C_char C_TLS li111[] C_aligned={C_lihdr(0,0,60),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,45,99,111,110,116,101,120,116,46,112,111,115,105,120,35,117,115,101,114,45,105,110,102,111,114,109,97,116,105,111,110,32,117,115,101,114,32,46,32,114,101,115,116,41,0,0,0,0};
static C_char C_TLS li112[] C_aligned={C_lihdr(0,0,49),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,45,99,111,110,116,101,120,116,46,112,111,115,105,120,35,99,117,114,114,101,110,116,45,117,115,101,114,45,110,97,109,101,41,0,0,0,0,0,0,0};
static C_char C_TLS li113[] C_aligned={C_lihdr(0,0,59),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,45,99,111,110,116,101,120,116,46,112,111,115,105,120,35,99,117,114,114,101,110,116,45,101,102,102,101,99,116,105,118,101,45,117,115,101,114,45,110,97,109,101,41,0,0,0,0,0};
static C_char C_TLS li114[] C_aligned={C_lihdr(0,0,35),40,99,104,105,99,107,101,110,46,112,111,115,105,120,35,99,104,111,119,110,32,108,111,99,32,102,32,117,105,100,32,103,105,100,41,0,0,0,0,0};
static C_char C_TLS li115[] C_aligned={C_lihdr(0,0,46),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,45,99,111,110,116,101,120,116,46,112,111,115,105,120,35,99,114,101,97,116,101,45,115,101,115,115,105,111,110,41,0,0};
static C_char C_TLS li116[] C_aligned={C_lihdr(0,0,49),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,99,114,101,97,116,101,45,115,121,109,98,111,108,105,99,45,108,105,110,107,32,111,108,100,32,110,101,119,41,0,0,0,0,0,0,0};
static C_char C_TLS li117[] C_aligned={C_lihdr(0,0,41),40,35,35,115,121,115,35,114,101,97,100,45,115,121,109,98,111,108,105,99,45,108,105,110,107,32,102,110,97,109,101,32,108,111,99,97,116,105,111,110,41,0,0,0,0,0,0,0};
static C_char C_TLS li118[] C_aligned={C_lihdr(0,0,7),40,97,53,53,52,50,41,0};
static C_char C_TLS li119[] C_aligned={C_lihdr(0,0,24),40,108,111,111,112,32,99,111,109,112,111,110,101,110,116,115,32,114,101,115,117,108,116,41};
static C_char C_TLS li120[] C_aligned={C_lihdr(0,0,55),40,97,53,53,52,56,32,98,97,115,101,45,111,114,105,103,105,110,32,98,97,115,101,45,100,105,114,101,99,116,111,114,121,32,100,105,114,101,99,116,111,114,121,45,99,111,109,112,111,110,101,110,116,115,41,0};
static C_char C_TLS li121[] C_aligned={C_lihdr(0,0,52),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,114,101,97,100,45,115,121,109,98,111,108,105,99,45,108,105,110,107,32,102,110,97,109,101,32,46,32,114,101,115,116,41,0,0,0,0};
static C_char C_TLS li122[] C_aligned={C_lihdr(0,0,38),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,108,105,110,107,32,111,108,100,32,110,101,119,41,0,0};
static C_char C_TLS li123[] C_aligned={C_lihdr(0,0,8),40,114,101,97,100,121,63,41};
static C_char C_TLS li124[] C_aligned={C_lihdr(0,0,6),40,108,111,111,112,41,0,0};
static C_char C_TLS li125[] C_aligned={C_lihdr(0,0,7),40,102,101,116,99,104,41,0};
static C_char C_TLS li126[] C_aligned={C_lihdr(0,0,7),40,97,53,56,51,56,41,0};
static C_char C_TLS li127[] C_aligned={C_lihdr(0,0,7),40,97,53,56,54,51,41,0};
static C_char C_TLS li128[] C_aligned={C_lihdr(0,0,7),40,97,53,56,55,53,41,0};
static C_char C_TLS li129[] C_aligned={C_lihdr(0,0,7),40,97,53,56,57,49,41,0};
static C_char C_TLS li130[] C_aligned={C_lihdr(0,0,16),40,108,111,111,112,32,110,32,109,32,115,116,97,114,116,41};
static C_char C_TLS li131[] C_aligned={C_lihdr(0,0,25),40,97,53,57,48,54,32,112,111,114,116,32,110,32,100,101,115,116,32,115,116,97,114,116,41,0,0,0,0,0,0,0};
static C_char C_TLS li132[] C_aligned={C_lihdr(0,0,11),40,97,54,48,49,48,32,112,111,115,41,0,0,0,0,0};
static C_char C_TLS li133[] C_aligned={C_lihdr(0,0,7),40,97,54,48,48,48,41,0};
static C_char C_TLS li134[] C_aligned={C_lihdr(0,0,28),40,97,54,48,53,54,32,110,101,120,116,32,108,105,110,101,32,102,117,108,108,45,108,105,110,101,63,41,0,0,0,0};
static C_char C_TLS li135[] C_aligned={C_lihdr(0,0,15),40,97,53,57,56,50,32,112,32,108,105,109,105,116,41,0};
static C_char C_TLS li136[] C_aligned={C_lihdr(0,0,12),40,97,54,49,48,49,32,112,111,114,116,41,0,0,0,0};
static C_char C_TLS li137[] C_aligned={C_lihdr(0,0,43),40,35,35,115,121,115,35,99,117,115,116,111,109,45,105,110,112,117,116,45,112,111,114,116,32,108,111,99,32,110,97,109,32,102,100,32,46,32,114,101,115,116,41,0,0,0,0,0};
static C_char C_TLS li138[] C_aligned={C_lihdr(0,0,6),40,108,111,111,112,41,0,0};
static C_char C_TLS li139[] C_aligned={C_lihdr(0,0,18),40,112,111,107,101,49,55,54,49,32,115,116,114,32,108,101,110,41,0,0,0,0,0,0};
static C_char C_TLS li140[] C_aligned={C_lihdr(0,0,11),40,97,54,50,55,55,32,115,116,114,41,0,0,0,0,0};
static C_char C_TLS li141[] C_aligned={C_lihdr(0,0,7),40,97,54,50,56,51,41,0};
static C_char C_TLS li142[] C_aligned={C_lihdr(0,0,7),40,97,54,50,57,57,41,0};
static C_char C_TLS li143[] C_aligned={C_lihdr(0,0,12),40,102,95,54,51,48,56,32,115,116,114,41,0,0,0,0};
static C_char C_TLS li144[] C_aligned={C_lihdr(0,0,20),40,108,111,111,112,32,114,101,109,32,115,116,97,114,116,32,108,101,110,41,0,0,0,0};
static C_char C_TLS li145[] C_aligned={C_lihdr(0,0,12),40,102,95,54,51,50,51,32,115,116,114,41,0,0,0,0};
static C_char C_TLS li146[] C_aligned={C_lihdr(0,0,44),40,35,35,115,121,115,35,99,117,115,116,111,109,45,111,117,116,112,117,116,45,112,111,114,116,32,108,111,99,32,110,97,109,32,102,100,32,46,32,114,101,115,116,41,0,0,0,0};
static C_char C_TLS li147[] C_aligned={C_lihdr(0,0,44),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,116,114,117,110,99,97,116,101,32,102,110,97,109,101,32,111,102,102,41,0,0,0,0};
static C_char C_TLS li148[] C_aligned={C_lihdr(0,0,21),40,115,101,116,117,112,32,112,111,114,116,32,97,114,103,115,32,108,111,99,41,0,0,0};
static C_char C_TLS li149[] C_aligned={C_lihdr(0,0,6),40,108,111,111,112,41,0,0};
static C_char C_TLS li150[] C_aligned={C_lihdr(0,0,42),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,108,111,99,107,32,112,111,114,116,32,46,32,97,114,103,115,41,0,0,0,0,0,0};
static C_char C_TLS li151[] C_aligned={C_lihdr(0,0,6),40,108,111,111,112,41,0,0};
static C_char C_TLS li152[] C_aligned={C_lihdr(0,0,51),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,108,111,99,107,47,98,108,111,99,107,105,110,103,32,112,111,114,116,32,46,32,97,114,103,115,41,0,0,0,0,0};
static C_char C_TLS li153[] C_aligned={C_lihdr(0,0,47),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,116,101,115,116,45,108,111,99,107,32,112,111,114,116,32,46,32,97,114,103,115,41,0};
static C_char C_TLS li154[] C_aligned={C_lihdr(0,0,7),40,97,54,54,56,49,41,0};
static C_char C_TLS li155[] C_aligned={C_lihdr(0,0,37),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,117,110,108,111,99,107,32,108,111,99,107,41,0,0,0};
static C_char C_TLS li156[] C_aligned={C_lihdr(0,0,45),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,99,114,101,97,116,101,45,102,105,102,111,32,102,110,97,109,101,32,46,32,109,111,100,101,41,0,0,0};
static C_char C_TLS li157[] C_aligned={C_lihdr(0,0,44),40,99,104,105,99,107,101,110,46,116,105,109,101,46,112,111,115,105,120,35,115,116,114,105,110,103,45,62,116,105,109,101,32,116,105,109,32,46,32,114,101,115,116,41,0,0,0,0};
static C_char C_TLS li158[] C_aligned={C_lihdr(0,0,41),40,99,104,105,99,107,101,110,46,116,105,109,101,46,112,111,115,105,120,35,117,116,99,45,116,105,109,101,45,62,115,101,99,111,110,100,115,32,116,109,41,0,0,0,0,0,0,0};
static C_char C_TLS li159[] C_aligned={C_lihdr(0,0,48),40,99,104,105,99,107,101,110,46,116,105,109,101,46,112,111,115,105,120,35,108,111,99,97,108,45,116,105,109,101,122,111,110,101,45,97,98,98,114,101,118,105,97,116,105,111,110,41};
static C_char C_TLS li160[] C_aligned={C_lihdr(0,0,43),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,46,115,105,103,110,97,108,35,115,101,116,45,97,108,97,114,109,33,32,105,110,116,49,57,50,52,41,0,0,0,0,0};
static C_char C_TLS li161[] C_aligned={C_lihdr(0,0,14),40,102,95,54,56,55,51,32,116,104,117,110,107,41,0,0};
static C_char C_TLS li162[] C_aligned={C_lihdr(0,0,7),40,97,54,56,54,52,41,0};
static C_char C_TLS li163[] C_aligned={C_lihdr(0,0,7),40,97,54,56,53,56,41,0};
static C_char C_TLS li164[] C_aligned={C_lihdr(0,0,37),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,112,114,111,99,101,115,115,45,102,111,114,107,32,46,32,114,101,115,116,41,0,0,0};
static C_char C_TLS li165[] C_aligned={C_lihdr(0,0,9),40,97,54,57,51,54,32,120,41,0,0,0,0,0,0,0};
static C_char C_TLS li166[] C_aligned={C_lihdr(0,0,25),40,97,54,57,51,57,32,112,114,103,32,97,114,103,98,117,102,32,101,110,118,98,117,102,41,0,0,0,0,0,0,0};
static C_char C_TLS li167[] C_aligned={C_lihdr(0,0,49),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,112,114,111,99,101,115,115,45,101,120,101,99,117,116,101,32,102,105,108,101,110,97,109,101,32,46,32,114,101,115,116,41,0,0,0,0,0,0,0};
static C_char C_TLS li168[] C_aligned={C_lihdr(0,0,7),40,97,55,48,48,54,41,0};
static C_char C_TLS li169[] C_aligned={C_lihdr(0,0,44),40,99,104,105,99,107,101,110,46,112,111,115,105,120,35,112,114,111,99,101,115,115,45,119,97,105,116,45,105,109,112,108,32,112,105,100,32,110,111,104,97,110,103,41,0,0,0,0};
static C_char C_TLS li170[] C_aligned={C_lihdr(0,0,49),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,45,99,111,110,116,101,120,116,46,112,111,115,105,120,35,112,97,114,101,110,116,45,112,114,111,99,101,115,115,45,105,100,41,0,0,0,0,0,0,0};
static C_char C_TLS li171[] C_aligned={C_lihdr(0,0,41),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,112,114,111,99,101,115,115,45,115,105,103,110,97,108,32,105,100,32,46,32,115,105,103,41,0,0,0,0,0,0,0};
static C_char C_TLS li172[] C_aligned={C_lihdr(0,0,38),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,112,114,111,99,101,115,115,45,114,117,110,32,102,32,46,32,97,114,103,115,41,0,0};
static C_char C_TLS li173[] C_aligned={C_lihdr(0,0,7),40,97,55,49,52,50,41,0};
static C_char C_TLS li174[] C_aligned={C_lihdr(0,0,17),40,97,55,49,52,56,32,95,32,102,108,103,32,99,111,100,41,0,0,0,0,0,0,0};
static C_char C_TLS li175[] C_aligned={C_lihdr(0,0,8),40,102,95,55,49,50,56,41};
static C_char C_TLS li176[] C_aligned={C_lihdr(0,0,44),40,109,97,107,101,45,111,110,45,99,108,111,115,101,32,108,111,99,32,112,105,100,32,99,108,115,118,101,99,32,105,100,120,32,105,100,120,97,32,105,100,120,98,41,0,0,0,0};
static C_char C_TLS li177[] C_aligned={C_lihdr(0,0,7),40,97,55,49,55,49,41,0};
static C_char C_TLS li178[] C_aligned={C_lihdr(0,0,11),40,97,55,49,55,55,32,105,32,111,41,0,0,0,0,0};
static C_char C_TLS li179[] C_aligned={C_lihdr(0,0,18),40,110,101,101,100,101,100,45,112,105,112,101,32,112,111,114,116,41,0,0,0,0,0,0};
static C_char C_TLS li180[] C_aligned={C_lihdr(0,0,26),40,99,111,110,110,101,99,116,45,112,97,114,101,110,116,32,112,105,112,101,32,112,111,114,116,41,0,0,0,0,0,0};
static C_char C_TLS li181[] C_aligned={C_lihdr(0,0,31),40,99,111,110,110,101,99,116,45,99,104,105,108,100,32,112,105,112,101,32,112,111,114,116,32,115,116,100,102,100,41,0};
static C_char C_TLS li182[] C_aligned={C_lihdr(0,0,7),40,97,55,50,52,54,41,0};
static C_char C_TLS li183[] C_aligned={C_lihdr(0,0,43),40,115,112,97,119,110,32,99,109,100,32,97,114,103,115,32,101,110,118,32,115,116,100,111,117,116,102,32,115,116,100,105,110,102,32,115,116,100,101,114,114,102,41,0,0,0,0,0};
static C_char C_TLS li184[] C_aligned={C_lihdr(0,0,39),40,105,110,112,117,116,45,112,111,114,116,32,108,111,99,32,99,109,100,32,112,105,112,101,32,115,116,100,102,32,111,110,45,99,108,111,115,101,41,0};
static C_char C_TLS li185[] C_aligned={C_lihdr(0,0,40),40,111,117,116,112,117,116,45,112,111,114,116,32,108,111,99,32,99,109,100,32,112,105,112,101,32,115,116,100,102,32,111,110,45,99,108,111,115,101,41};
static C_char C_TLS li186[] C_aligned={C_lihdr(0,0,7),40,97,55,50,57,54,41,0};
static C_char C_TLS li187[] C_aligned={C_lihdr(0,0,34),40,97,55,51,48,50,32,105,110,112,105,112,101,32,111,117,116,112,105,112,101,32,101,114,114,112,105,112,101,32,112,105,100,41,0,0,0,0,0,0};
static C_char C_TLS li188[] C_aligned={C_lihdr(0,0,68),40,99,104,105,99,107,101,110,46,112,111,115,105,120,35,112,114,111,99,101,115,115,45,105,109,112,108,32,108,111,99,32,99,109,100,32,97,114,103,115,32,101,110,118,32,115,116,100,111,117,116,102,32,115,116,100,105,110,102,32,115,116,100,101,114,114,102,41,0,0,0,0};
static C_char C_TLS li189[] C_aligned={C_lihdr(0,0,7),40,103,50,49,50,48,41,0};
static C_char C_TLS li190[] C_aligned={C_lihdr(0,0,19),40,102,111,114,45,101,97,99,104,45,108,111,111,112,50,49,49,57,41,0,0,0,0,0};
static C_char C_TLS li191[] C_aligned={C_lihdr(0,0,15),40,99,104,107,115,116,114,108,115,116,32,108,115,116,41,0};
static C_char C_TLS li192[] C_aligned={C_lihdr(0,0,7),40,97,55,51,57,55,41,0};
static C_char C_TLS li193[] C_aligned={C_lihdr(0,0,34),40,37,112,114,111,99,101,115,115,32,108,111,99,32,101,114,114,63,32,99,109,100,32,97,114,103,115,32,101,110,118,32,107,41,0,0,0,0,0,0};
static C_char C_TLS li194[] C_aligned={C_lihdr(0,0,15),40,97,55,52,52,49,32,105,32,111,32,112,32,101,41,0};
static C_char C_TLS li195[] C_aligned={C_lihdr(0,0,36),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,112,114,111,99,101,115,115,32,99,109,100,32,46,32,114,101,115,116,41,0,0,0,0};
static C_char C_TLS li196[] C_aligned={C_lihdr(0,0,37),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,112,114,111,99,101,115,115,42,32,99,109,100,32,46,32,114,101,115,116,41,0,0,0};
static C_char C_TLS li197[] C_aligned={C_lihdr(0,0,55),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,45,99,111,110,116,101,120,116,46,112,111,115,105,120,35,115,101,116,45,114,111,111,116,45,100,105,114,101,99,116,111,114,121,33,32,100,105,114,41,0};
static C_char C_TLS li198[] C_aligned={C_lihdr(0,0,35),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,112,114,111,99,101,115,115,45,115,112,97,119,110,32,46,32,95,41,0,0,0,0,0};
static C_char C_TLS li199[] C_aligned={C_lihdr(0,0,21),40,99,104,105,99,107,101,110,46,101,114,114,110,111,35,101,114,114,110,111,41,0,0,0};
static C_char C_TLS li200[] C_aligned={C_lihdr(0,0,11),40,97,55,54,50,55,32,112,105,100,41,0,0,0,0,0};
static C_char C_TLS li201[] C_aligned={C_lihdr(0,0,16),40,97,55,54,52,53,32,112,105,100,32,112,103,105,100,41};
static C_char C_TLS li202[] C_aligned={C_lihdr(0,0,7),40,97,55,54,54,55,41,0};
static C_char C_TLS li203[] C_aligned={C_lihdr(0,0,10),40,97,55,54,55,48,32,105,100,41,0,0,0,0,0,0};
static C_char C_TLS li204[] C_aligned={C_lihdr(0,0,7),40,97,55,54,56,54,41,0};
static C_char C_TLS li205[] C_aligned={C_lihdr(0,0,10),40,97,55,54,56,57,32,105,100,41,0,0,0,0,0,0};
static C_char C_TLS li206[] C_aligned={C_lihdr(0,0,7),40,97,55,55,48,53,41,0};
static C_char C_TLS li207[] C_aligned={C_lihdr(0,0,10),40,97,55,55,48,56,32,105,100,41,0,0,0,0,0,0};
static C_char C_TLS li208[] C_aligned={C_lihdr(0,0,7),40,97,55,55,50,52,41,0};
static C_char C_TLS li209[] C_aligned={C_lihdr(0,0,10),40,97,55,55,50,55,32,105,100,41,0,0,0,0,0,0};
static C_char C_TLS li210[] C_aligned={C_lihdr(0,0,16),40,108,111,111,112,32,115,105,103,115,32,109,97,115,107,41};
static C_char C_TLS li211[] C_aligned={C_lihdr(0,0,7),40,97,55,55,52,51,41,0};
static C_char C_TLS li212[] C_aligned={C_lihdr(0,0,11),40,97,55,55,55,51,32,115,105,103,41,0,0,0,0,0};
static C_char C_TLS li213[] C_aligned={C_lihdr(0,0,14),40,97,55,55,56,50,32,46,32,114,101,115,116,41,0,0};
static C_char C_TLS li214[] C_aligned={C_lihdr(0,0,10),40,97,55,56,48,55,32,117,109,41,0,0,0,0,0,0};
static C_char C_TLS li215[] C_aligned={C_lihdr(0,0,12),40,97,55,56,49,51,32,112,111,114,116,41,0,0,0,0};
static C_char C_TLS li216[] C_aligned={C_lihdr(0,0,9),40,97,55,56,53,48,32,102,41,0,0,0,0,0,0,0};
static C_char C_TLS li217[] C_aligned={C_lihdr(0,0,9),40,97,55,56,53,54,32,102,41,0,0,0,0,0,0,0};
static C_char C_TLS li218[] C_aligned={C_lihdr(0,0,9),40,97,55,56,54,50,32,102,41,0,0,0,0,0,0,0};
static C_char C_TLS li219[] C_aligned={C_lihdr(0,0,10),40,116,111,112,108,101,118,101,108,41,0,0,0,0,0,0};


/* from k7547 in chicken.process-context.posix#set-root-directory! in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
C_regparm static C_word C_fcall stub2190(C_word C_buf,C_word C_a0){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
char * t0=(char * )C_c_string(C_a0);
C_r=C_fix((C_word)chroot(t0));
return C_r;}

/* from chicken.process-context.posix#parent-process-id in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
C_regparm static C_word C_fcall stub1995(C_word C_buf){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
C_r=C_fix((C_word)C_getppid());
return C_r;}

/* from k6831 */
C_regparm static C_word C_fcall stub1949(C_word C_buf,C_word C_a0){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
void * t0=(void * )C_c_pointer_or_null(C_a0);
C_r=C_fix((C_word)C_fflush(t0));
return C_r;}

/* from fork */
C_regparm static C_word C_fcall stub1929(C_word C_buf){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
C_r=C_fix((C_word)C_fork());
return C_r;}

/* from k6808 */
C_regparm static C_word C_fcall stub1925(C_word C_buf,C_word C_a0){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int t0=(int )C_unfix(C_a0);
C_r=C_fix((C_word)C_alarm(t0));
return C_r;}

#define return(x) C_cblock C_r = (C_mpointer(&C_a,(void*)(x))); goto C_ret; C_cblockend
C_regparm static C_word C_fcall stub1921(C_word C_buf){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;

#if !defined(__CYGWIN__) && !defined(__SVR4) && !defined(__uClinux__) && !defined(__hpux__) && !defined(_AIX)

time_t clock = time(NULL);
struct tm *ltm = C_localtime(&clock);
char *z = ltm ? (char *)ltm->tm_zone : 0;

#else

char *z = (daylight ? tzname[1] : tzname[0]);

#endif

C_return(z);
C_ret:
#undef return

return C_r;}

/* from k6736 */
C_regparm static C_word C_fcall stub1894(C_word C_buf,C_word C_a0,C_word C_a1,C_word C_a2,C_word C_a3){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
C_word t0=(C_word )(C_a0);
C_word t1=(C_word )(C_a1);
C_word t2=(C_word )(C_a2);
void * t3=(void * )C_data_pointer_or_null(C_a3);
C_r=((C_word)C_strptime(t0,t1,t2,t3));
return C_r;}

/* from k5632 in k5628 in chicken.file.posix#file-link in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 in ... */
C_regparm static C_word C_fcall stub1614(C_word C_buf,C_word C_a0,C_word C_a1){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
char * t0=(char * )C_c_string(C_a0);
char * t1=(char * )C_c_string(C_a1);
C_r=C_fix((C_word)link(t0,t1));
return C_r;}

/* from a7667 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
C_regparm static C_word C_fcall stub1530(C_word C_buf){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
C_r=C_fix((C_word)C_getegid());
return C_r;}

/* from a7686 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
C_regparm static C_word C_fcall stub1526(C_word C_buf){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
C_r=C_fix((C_word)C_getgid());
return C_r;}

/* from a7705 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
C_regparm static C_word C_fcall stub1522(C_word C_buf){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
C_r=C_fix((C_word)C_geteuid());
return C_r;}

/* from a7724 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
C_regparm static C_word C_fcall stub1518(C_word C_buf){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
C_r=C_fix((C_word)C_getuid());
return C_r;}

#define return(x) C_cblock C_r = (C_mk_bool((x))); goto C_ret; C_cblockend
C_regparm static C_word C_fcall stub1452(C_word C_buf,C_word C_a0,C_word C_a1){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int i=(int )C_unfix(C_a0);
void * p=(void * )C_data_pointer_or_null(C_a1);
struct pollfd *fds = p;
C_return(fds[i].revents & (POLLOUT|POLLERR|POLLHUP|POLLNVAL));
C_ret:
#undef return

return C_r;}

#define return(x) C_cblock C_r = (C_mk_bool((x))); goto C_ret; C_cblockend
C_regparm static C_word C_fcall stub1433(C_word C_buf,C_word C_a0,C_word C_a1){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int i=(int )C_unfix(C_a0);
void * p=(void * )C_data_pointer_or_null(C_a1);
struct pollfd *fds = p;
C_return(fds[i].revents & (POLLIN|POLLERR|POLLHUP|POLLNVAL));
C_ret:
#undef return

return C_r;}

/* from k4832 */
C_regparm static C_word C_fcall stub1408(C_word C_buf,C_word C_a0,C_word C_a1,C_word C_a2){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
void * t0=(void * )C_data_pointer_or_null(C_a0);
int t1=(int )C_unfix(C_a1);
int t2=(int )C_unfix(C_a2);
C_r=C_fix((C_word)poll(t0,t1,t2));
return C_r;}

#define return(x) C_cblock C_r = (((C_word)(x))); goto C_ret; C_cblockend
C_regparm static C_word C_fcall stub1393(C_word C_buf,C_word C_a0,C_word C_a1,C_word C_a2){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int i=(int )C_unfix(C_a0);
int fd=(int )C_unfix(C_a1);
void * p=(void * )C_data_pointer_or_null(C_a2);
struct pollfd *fds = p;
fds[i].fd = fd; fds[i].events = POLLOUT;
C_ret:
#undef return

return C_r;}

#define return(x) C_cblock C_r = (((C_word)(x))); goto C_ret; C_cblockend
C_regparm static C_word C_fcall stub1376(C_word C_buf,C_word C_a0,C_word C_a1,C_word C_a2){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int i=(int )C_unfix(C_a0);
int fd=(int )C_unfix(C_a1);
void * p=(void * )C_data_pointer_or_null(C_a2);
struct pollfd *fds = p;
fds[i].fd = fd; fds[i].events = POLLIN;
C_ret:
#undef return

return C_r;}

/* from k4583 */
C_regparm static C_word C_fcall stub1280(C_word C_buf,C_word C_a0,C_word C_a1,C_word C_a2){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int t0=(int )C_unfix(C_a0);
int t1=(int )C_unfix(C_a1);
long t2=(long )C_num_to_long(C_a2);
C_r=C_fix((C_word)fcntl(t0,t1,t2));
return C_r;}

/* from k4550 */
C_regparm static C_word C_fcall stub1273(C_word C_buf,C_word C_a0){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int t0=(int )C_unfix(C_a0);
C_r=C_fix((C_word)C_check_fd_ready(t0));
return C_r;}

#define return(x) C_cblock C_r = (C_mk_bool((x))); goto C_ret; C_cblockend
C_regparm static C_word C_fcall stub1268(C_word C_buf,C_word C_a0){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int fd=(int )C_unfix(C_a0);
int val = fcntl(fd, F_GETFL, 0);
if(val == -1) C_return(0);
C_return(fcntl(fd, F_SETFL, val | O_NONBLOCK) != -1);
C_ret:
#undef return

return C_r;}

#define return(x) C_cblock C_r = (C_mpointer_or_false(&C_a,(void*)(x))); goto C_ret; C_cblockend
C_regparm static C_word C_fcall stub976(C_word C_buf,C_word C_a0){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
C_word o=(C_word )(C_a0);
char *ptr = C_malloc(C_header_size(o)); 

if (ptr != NULL) {

  C_memcpy(ptr, C_data_pointer(o), C_header_size(o)); 

}

C_return(ptr);
C_ret:
#undef return

return C_r;}

/* from k3776 */
C_regparm static C_word C_fcall stub919(C_word C_buf,C_word C_a0,C_word C_a1,C_word C_a2){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
C_word t0=(C_word )(C_a0);
C_word t1=(C_word )(C_a1);
void * t2=(void * )C_data_pointer_or_null(C_a2);
C_r=C_mpointer(&C_a,(void*)C_strftime(t0,t1,t2));
return C_r;}

/* from k3763 */
C_regparm static C_word C_fcall stub909(C_word C_buf,C_word C_a0,C_word C_a1){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
C_word t0=(C_word )(C_a0);
void * t1=(void * )C_data_pointer_or_null(C_a1);
C_r=C_mpointer(&C_a,(void*)C_asctime(t0,t1));
return C_r;}

/* from k3695 */
C_regparm static C_word C_fcall stub882(C_word C_buf,C_word C_a0){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int t0=(int )C_num_to_int(C_a0);
C_r=C_mpointer(&C_a,(void*)C_ctime(t0));
return C_r;}

/* from chicken.process-context.posix#current-process-id in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
C_regparm static C_word C_fcall stub826(C_word C_buf){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
C_r=C_fix((C_word)C_getpid());
return C_r;}

/* from k3106 in k3100 in k3097 in k3085 in chicken.file.posix#set-file-times! in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
C_regparm static C_word C_fcall stub707(C_word C_buf,C_word C_a0,C_word C_a1,C_word C_a2){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
char * t0=(char * )C_string_or_null(C_a0);
C_word t1=(C_word )(C_a1);
C_word t2=(C_word )(C_a2);
C_r=C_fix((C_word)set_file_mtime(t0,t1,t2));
return C_r;}

/* from k2936 */
C_regparm static C_word C_fcall stub633(C_word C_buf,C_word C_a0){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int t0=(int )C_unfix(C_a0);
C_r=C_mpointer(&C_a,(void*)strerror(t0));
return C_r;}

C_noret_decl(f8684)
static void C_ccall f8684(C_word c,C_word *av) C_noret;
C_noret_decl(f8688)
static void C_ccall f8688(C_word c,C_word *av) C_noret;
C_noret_decl(f8730)
static void C_ccall f8730(C_word c,C_word *av) C_noret;
C_noret_decl(f8736)
static void C_ccall f8736(C_word c,C_word *av) C_noret;
C_noret_decl(f_2617)
static void C_ccall f_2617(C_word c,C_word *av) C_noret;
C_noret_decl(f_2620)
static void C_ccall f_2620(C_word c,C_word *av) C_noret;
C_noret_decl(f_2623)
static void C_ccall f_2623(C_word c,C_word *av) C_noret;
C_noret_decl(f_2626)
static void C_ccall f_2626(C_word c,C_word *av) C_noret;
C_noret_decl(f_2629)
static void C_ccall f_2629(C_word c,C_word *av) C_noret;
C_noret_decl(f_2632)
static void C_ccall f_2632(C_word c,C_word *av) C_noret;
C_noret_decl(f_2718)
static void C_ccall f_2718(C_word c,C_word *av) C_noret;
C_noret_decl(f_2731)
static void C_ccall f_2731(C_word c,C_word *av) C_noret;
C_noret_decl(f_2736)
static void C_ccall f_2736(C_word c,C_word *av) C_noret;
C_noret_decl(f_2740)
static void C_ccall f_2740(C_word c,C_word *av) C_noret;
C_noret_decl(f_2752)
static void C_ccall f_2752(C_word c,C_word *av) C_noret;
C_noret_decl(f_2756)
static void C_ccall f_2756(C_word c,C_word *av) C_noret;
C_noret_decl(f_2766)
static void C_fcall f_2766(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_2787)
static void C_ccall f_2787(C_word c,C_word *av) C_noret;
C_noret_decl(f_2790)
static void C_ccall f_2790(C_word c,C_word *av) C_noret;
C_noret_decl(f_2801)
static void C_ccall f_2801(C_word c,C_word *av) C_noret;
C_noret_decl(f_2807)
static void C_fcall f_2807(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_2832)
static void C_ccall f_2832(C_word c,C_word *av) C_noret;
C_noret_decl(f_2939)
static void C_ccall f_2939(C_word c,C_word *av) C_noret;
C_noret_decl(f_2943)
static void C_ccall f_2943(C_word c,C_word *av) C_noret;
C_noret_decl(f_2950)
static void C_ccall f_2950(C_word c,C_word *av) C_noret;
C_noret_decl(f_2954)
static void C_ccall f_2954(C_word c,C_word *av) C_noret;
C_noret_decl(f_2957)
static void C_fcall f_2957(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_2961)
static void C_ccall f_2961(C_word c,C_word *av) C_noret;
C_noret_decl(f_2982)
static void C_ccall f_2982(C_word c,C_word *av) C_noret;
C_noret_decl(f_2986)
static void C_ccall f_2986(C_word c,C_word *av) C_noret;
C_noret_decl(f_2995)
static void C_ccall f_2995(C_word c,C_word *av) C_noret;
C_noret_decl(f_3003)
static void C_ccall f_3003(C_word c,C_word *av) C_noret;
C_noret_decl(f_3010)
static void C_ccall f_3010(C_word c,C_word *av) C_noret;
C_noret_decl(f_3021)
static void C_ccall f_3021(C_word c,C_word *av) C_noret;
C_noret_decl(f_3025)
static void C_ccall f_3025(C_word c,C_word *av) C_noret;
C_noret_decl(f_3028)
static void C_ccall f_3028(C_word c,C_word *av) C_noret;
C_noret_decl(f_3046)
static void C_ccall f_3046(C_word c,C_word *av) C_noret;
C_noret_decl(f_3050)
static void C_ccall f_3050(C_word c,C_word *av) C_noret;
C_noret_decl(f_3060)
static void C_ccall f_3060(C_word c,C_word *av) C_noret;
C_noret_decl(f_3065)
static void C_ccall f_3065(C_word c,C_word *av) C_noret;
C_noret_decl(f_3069)
static void C_ccall f_3069(C_word c,C_word *av) C_noret;
C_noret_decl(f_3071)
static void C_ccall f_3071(C_word c,C_word *av) C_noret;
C_noret_decl(f_3075)
static void C_ccall f_3075(C_word c,C_word *av) C_noret;
C_noret_decl(f_3077)
static void C_ccall f_3077(C_word c,C_word *av) C_noret;
C_noret_decl(f_3081)
static void C_ccall f_3081(C_word c,C_word *av) C_noret;
C_noret_decl(f_3083)
static void C_ccall f_3083(C_word c,C_word *av) C_noret;
C_noret_decl(f_3087)
static void C_ccall f_3087(C_word c,C_word *av) C_noret;
C_noret_decl(f_3099)
static void C_ccall f_3099(C_word c,C_word *av) C_noret;
C_noret_decl(f_3102)
static void C_ccall f_3102(C_word c,C_word *av) C_noret;
C_noret_decl(f_3108)
static void C_ccall f_3108(C_word c,C_word *av) C_noret;
C_noret_decl(f_3118)
static void C_fcall f_3118(C_word t0,C_word t1) C_noret;
C_noret_decl(f_3162)
static void C_ccall f_3162(C_word c,C_word *av) C_noret;
C_noret_decl(f_3166)
static void C_ccall f_3166(C_word c,C_word *av) C_noret;
C_noret_decl(f_3168)
static void C_ccall f_3168(C_word c,C_word *av) C_noret;
C_noret_decl(f_3174)
static void C_ccall f_3174(C_word c,C_word *av) C_noret;
C_noret_decl(f_3182)
static void C_ccall f_3182(C_word c,C_word *av) C_noret;
C_noret_decl(f_3186)
static void C_ccall f_3186(C_word c,C_word *av) C_noret;
C_noret_decl(f_3190)
static void C_ccall f_3190(C_word c,C_word *av) C_noret;
C_noret_decl(f_3192)
static void C_ccall f_3192(C_word c,C_word *av) C_noret;
C_noret_decl(f_3211)
static void C_ccall f_3211(C_word c,C_word *av) C_noret;
C_noret_decl(f_3279)
static void C_ccall f_3279(C_word c,C_word *av) C_noret;
C_noret_decl(f_3287)
static void C_ccall f_3287(C_word c,C_word *av) C_noret;
C_noret_decl(f_3289)
static void C_ccall f_3289(C_word c,C_word *av) C_noret;
C_noret_decl(f_3297)
static void C_ccall f_3297(C_word c,C_word *av) C_noret;
C_noret_decl(f_3299)
static void C_ccall f_3299(C_word c,C_word *av) C_noret;
C_noret_decl(f_3307)
static void C_ccall f_3307(C_word c,C_word *av) C_noret;
C_noret_decl(f_3309)
static void C_ccall f_3309(C_word c,C_word *av) C_noret;
C_noret_decl(f_3317)
static void C_ccall f_3317(C_word c,C_word *av) C_noret;
C_noret_decl(f_3319)
static void C_ccall f_3319(C_word c,C_word *av) C_noret;
C_noret_decl(f_3327)
static void C_ccall f_3327(C_word c,C_word *av) C_noret;
C_noret_decl(f_3329)
static void C_ccall f_3329(C_word c,C_word *av) C_noret;
C_noret_decl(f_3337)
static void C_ccall f_3337(C_word c,C_word *av) C_noret;
C_noret_decl(f_3339)
static void C_ccall f_3339(C_word c,C_word *av) C_noret;
C_noret_decl(f_3347)
static void C_ccall f_3347(C_word c,C_word *av) C_noret;
C_noret_decl(f_3352)
static void C_ccall f_3352(C_word c,C_word *av) C_noret;
C_noret_decl(f_3359)
static void C_ccall f_3359(C_word c,C_word *av) C_noret;
C_noret_decl(f_3362)
static void C_ccall f_3362(C_word c,C_word *av) C_noret;
C_noret_decl(f_3368)
static void C_ccall f_3368(C_word c,C_word *av) C_noret;
C_noret_decl(f_3374)
static void C_ccall f_3374(C_word c,C_word *av) C_noret;
C_noret_decl(f_3407)
static void C_ccall f_3407(C_word c,C_word *av) C_noret;
C_noret_decl(f_3435)
static void C_fcall f_3435(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_3443)
static void C_ccall f_3443(C_word c,C_word *av) C_noret;
C_noret_decl(f_3472)
static void C_fcall f_3472(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_3485)
static void C_ccall f_3485(C_word c,C_word *av) C_noret;
C_noret_decl(f_3491)
static void C_ccall f_3491(C_word c,C_word *av) C_noret;
C_noret_decl(f_3495)
static void C_ccall f_3495(C_word c,C_word *av) C_noret;
C_noret_decl(f_3503)
static void C_ccall f_3503(C_word c,C_word *av) C_noret;
C_noret_decl(f_3505)
static void C_ccall f_3505(C_word c,C_word *av) C_noret;
C_noret_decl(f_3509)
static void C_ccall f_3509(C_word c,C_word *av) C_noret;
C_noret_decl(f_3517)
static void C_ccall f_3517(C_word c,C_word *av) C_noret;
C_noret_decl(f_3519)
static void C_ccall f_3519(C_word c,C_word *av) C_noret;
C_noret_decl(f_3535)
static void C_ccall f_3535(C_word c,C_word *av) C_noret;
C_noret_decl(f_3544)
static void C_ccall f_3544(C_word c,C_word *av) C_noret;
C_noret_decl(f_3558)
static void C_ccall f_3558(C_word c,C_word *av) C_noret;
C_noret_decl(f_3564)
static void C_ccall f_3564(C_word c,C_word *av) C_noret;
C_noret_decl(f_3568)
static void C_ccall f_3568(C_word c,C_word *av) C_noret;
C_noret_decl(f_3571)
static void C_fcall f_3571(C_word t0,C_word t1) C_noret;
C_noret_decl(f_3574)
static void C_ccall f_3574(C_word c,C_word *av) C_noret;
C_noret_decl(f_3589)
static void C_ccall f_3589(C_word c,C_word *av) C_noret;
C_noret_decl(f_3591)
static void C_ccall f_3591(C_word c,C_word *av) C_noret;
C_noret_decl(f_3594)
static void C_ccall f_3594(C_word c,C_word *av) C_noret;
C_noret_decl(f_3598)
static void C_ccall f_3598(C_word c,C_word *av) C_noret;
C_noret_decl(f_3601)
static void C_ccall f_3601(C_word c,C_word *av) C_noret;
C_noret_decl(f_3610)
static void C_ccall f_3610(C_word c,C_word *av) C_noret;
C_noret_decl(f_3624)
static void C_ccall f_3624(C_word c,C_word *av) C_noret;
C_noret_decl(f_3627)
static void C_fcall f_3627(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3646)
static void C_ccall f_3646(C_word c,C_word *av) C_noret;
C_noret_decl(f_3650)
static void C_ccall f_3650(C_word c,C_word *av) C_noret;
C_noret_decl(f_3653)
static void C_ccall f_3653(C_word c,C_word *av) C_noret;
C_noret_decl(f_3667)
static void C_ccall f_3667(C_word c,C_word *av) C_noret;
C_noret_decl(f_3671)
static void C_ccall f_3671(C_word c,C_word *av) C_noret;
C_noret_decl(f_3674)
static void C_ccall f_3674(C_word c,C_word *av) C_noret;
C_noret_decl(f_3699)
static void C_ccall f_3699(C_word c,C_word *av) C_noret;
C_noret_decl(f_3703)
static void C_ccall f_3703(C_word c,C_word *av) C_noret;
C_noret_decl(f_3706)
static void C_ccall f_3706(C_word c,C_word *av) C_noret;
C_noret_decl(f_3709)
static void C_ccall f_3709(C_word c,C_word *av) C_noret;
C_noret_decl(f_3737)
static void C_ccall f_3737(C_word c,C_word *av) C_noret;
C_noret_decl(f_3741)
static void C_ccall f_3741(C_word c,C_word *av) C_noret;
C_noret_decl(f_3745)
static void C_ccall f_3745(C_word c,C_word *av) C_noret;
C_noret_decl(f_3782)
static void C_ccall f_3782(C_word c,C_word *av) C_noret;
C_noret_decl(f_3789)
static void C_ccall f_3789(C_word c,C_word *av) C_noret;
C_noret_decl(f_3798)
static void C_ccall f_3798(C_word c,C_word *av) C_noret;
C_noret_decl(f_3808)
static void C_ccall f_3808(C_word c,C_word *av) C_noret;
C_noret_decl(f_3812)
static void C_ccall f_3812(C_word c,C_word *av) C_noret;
C_noret_decl(f_3815)
static void C_ccall f_3815(C_word c,C_word *av) C_noret;
C_noret_decl(f_3836)
static void C_ccall f_3836(C_word c,C_word *av) C_noret;
C_noret_decl(f_3844)
static void C_ccall f_3844(C_word c,C_word *av) C_noret;
C_noret_decl(f_3848)
static void C_ccall f_3848(C_word c,C_word *av) C_noret;
C_noret_decl(f_3859)
static void C_ccall f_3859(C_word c,C_word *av) C_noret;
C_noret_decl(f_3861)
static void C_ccall f_3861(C_word c,C_word *av) C_noret;
C_noret_decl(f_3865)
static void C_ccall f_3865(C_word c,C_word *av) C_noret;
C_noret_decl(f_3867)
static void C_ccall f_3867(C_word c,C_word *av) C_noret;
C_noret_decl(f_3886)
static void C_ccall f_3886(C_word c,C_word *av) C_noret;
C_noret_decl(f_3891)
static void C_ccall f_3891(C_word c,C_word *av) C_noret;
C_noret_decl(f_3897)
static void C_ccall f_3897(C_word c,C_word *av) C_noret;
C_noret_decl(f_3938)
static void C_fcall f_3938(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_3946)
static void C_ccall f_3946(C_word c,C_word *av) C_noret;
C_noret_decl(f_3949)
static void C_ccall f_3949(C_word c,C_word *av) C_noret;
C_noret_decl(f_3954)
static void C_ccall f_3954(C_word c,C_word *av) C_noret;
C_noret_decl(f_3960)
static void C_ccall f_3960(C_word c,C_word *av) C_noret;
C_noret_decl(f_3966)
static void C_ccall f_3966(C_word c,C_word *av) C_noret;
C_noret_decl(f_3970)
static void C_ccall f_3970(C_word c,C_word *av) C_noret;
C_noret_decl(f_3975)
static void C_ccall f_3975(C_word c,C_word *av) C_noret;
C_noret_decl(f_3977)
static void C_fcall f_3977(C_word t0,C_word t1) C_noret;
C_noret_decl(f_3981)
static void C_ccall f_3981(C_word c,C_word *av) C_noret;
C_noret_decl(f_3983)
static void C_fcall f_3983(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_3999)
static void C_ccall f_3999(C_word c,C_word *av) C_noret;
C_noret_decl(f_4005)
static void C_ccall f_4005(C_word c,C_word *av) C_noret;
C_noret_decl(f_4008)
static void C_ccall f_4008(C_word c,C_word *av) C_noret;
C_noret_decl(f_4024)
static void C_ccall f_4024(C_word c,C_word *av) C_noret;
C_noret_decl(f_4034)
static void C_fcall f_4034(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4040)
static void C_ccall f_4040(C_word c,C_word *av) C_noret;
C_noret_decl(f_4051)
static void C_ccall f_4051(C_word c,C_word *av) C_noret;
C_noret_decl(f_4055)
static void C_fcall f_4055(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4059)
static void C_ccall f_4059(C_word c,C_word *av) C_noret;
C_noret_decl(f_4064)
static void C_fcall f_4064(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4074)
static void C_ccall f_4074(C_word c,C_word *av) C_noret;
C_noret_decl(f_4077)
static void C_ccall f_4077(C_word c,C_word *av) C_noret;
C_noret_decl(f_4089)
static void C_fcall f_4089(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4094)
static C_word C_fcall f_4094(C_word t0,C_word t1);
C_noret_decl(f_4113)
static C_word C_fcall f_4113(C_word t0,C_word t1);
C_noret_decl(f_4136)
static void C_ccall f_4136(C_word c,C_word *av) C_noret;
C_noret_decl(f_4138)
static void C_fcall f_4138(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6,C_word t7) C_noret;
C_noret_decl(f_4142)
static void C_ccall f_4142(C_word c,C_word *av) C_noret;
C_noret_decl(f_4148)
static void C_ccall f_4148(C_word c,C_word *av) C_noret;
C_noret_decl(f_4151)
static void C_ccall f_4151(C_word c,C_word *av) C_noret;
C_noret_decl(f_4156)
static void C_ccall f_4156(C_word c,C_word *av) C_noret;
C_noret_decl(f_4162)
static void C_ccall f_4162(C_word c,C_word *av) C_noret;
C_noret_decl(f_4168)
static void C_ccall f_4168(C_word c,C_word *av) C_noret;
C_noret_decl(f_4172)
static void C_ccall f_4172(C_word c,C_word *av) C_noret;
C_noret_decl(f_4175)
static void C_ccall f_4175(C_word c,C_word *av) C_noret;
C_noret_decl(f_4183)
static void C_ccall f_4183(C_word c,C_word *av) C_noret;
C_noret_decl(f_4189)
static void C_ccall f_4189(C_word c,C_word *av) C_noret;
C_noret_decl(f_4193)
static void C_fcall f_4193(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4200)
static void C_ccall f_4200(C_word c,C_word *av) C_noret;
C_noret_decl(f_4203)
static void C_ccall f_4203(C_word c,C_word *av) C_noret;
C_noret_decl(f_4207)
static void C_ccall f_4207(C_word c,C_word *av) C_noret;
C_noret_decl(f_4228)
static void C_ccall f_4228(C_word c,C_word *av) C_noret;
C_noret_decl(f_4230)
static void C_fcall f_4230(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4255)
static void C_ccall f_4255(C_word c,C_word *av) C_noret;
C_noret_decl(f_4264)
static void C_ccall f_4264(C_word c,C_word *av) C_noret;
C_noret_decl(f_4270)
static void C_ccall f_4270(C_word c,C_word *av) C_noret;
C_noret_decl(f_4295)
static void C_fcall f_4295(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_4308)
static void C_ccall f_4308(C_word c,C_word *av) C_noret;
C_noret_decl(f_4314)
static void C_ccall f_4314(C_word c,C_word *av) C_noret;
C_noret_decl(f_4328)
static void C_ccall f_4328(C_word c,C_word *av) C_noret;
C_noret_decl(f_4335)
static void C_ccall f_4335(C_word c,C_word *av) C_noret;
C_noret_decl(f_4345)
static void C_ccall f_4345(C_word c,C_word *av) C_noret;
C_noret_decl(f_4354)
static void C_ccall f_4354(C_word c,C_word *av) C_noret;
C_noret_decl(f_4368)
static void C_ccall f_4368(C_word c,C_word *av) C_noret;
C_noret_decl(f_4375)
static void C_ccall f_4375(C_word c,C_word *av) C_noret;
C_noret_decl(f_4385)
static void C_ccall f_4385(C_word c,C_word *av) C_noret;
C_noret_decl(f_4394)
static void C_ccall f_4394(C_word c,C_word *av) C_noret;
C_noret_decl(f_4401)
static void C_ccall f_4401(C_word c,C_word *av) C_noret;
C_noret_decl(f_4409)
static void C_ccall f_4409(C_word c,C_word *av) C_noret;
C_noret_decl(f_4416)
static void C_ccall f_4416(C_word c,C_word *av) C_noret;
C_noret_decl(f_4424)
static void C_ccall f_4424(C_word c,C_word *av) C_noret;
C_noret_decl(f_4428)
static void C_ccall f_4428(C_word c,C_word *av) C_noret;
C_noret_decl(f_4433)
static void C_ccall f_4433(C_word c,C_word *av) C_noret;
C_noret_decl(f_4438)
static void C_ccall f_4438(C_word c,C_word *av) C_noret;
C_noret_decl(f_4444)
static void C_ccall f_4444(C_word c,C_word *av) C_noret;
C_noret_decl(f_4448)
static void C_ccall f_4448(C_word c,C_word *av) C_noret;
C_noret_decl(f_4453)
static void C_ccall f_4453(C_word c,C_word *av) C_noret;
C_noret_decl(f_4458)
static void C_ccall f_4458(C_word c,C_word *av) C_noret;
C_noret_decl(f_4462)
static void C_ccall f_4462(C_word c,C_word *av) C_noret;
C_noret_decl(f_4467)
static void C_ccall f_4467(C_word c,C_word *av) C_noret;
C_noret_decl(f_4473)
static void C_ccall f_4473(C_word c,C_word *av) C_noret;
C_noret_decl(f_4477)
static void C_ccall f_4477(C_word c,C_word *av) C_noret;
C_noret_decl(f_4482)
static void C_ccall f_4482(C_word c,C_word *av) C_noret;
C_noret_decl(f_4486)
static void C_ccall f_4486(C_word c,C_word *av) C_noret;
C_noret_decl(f_4491)
static void C_ccall f_4491(C_word c,C_word *av) C_noret;
C_noret_decl(f_4497)
static void C_ccall f_4497(C_word c,C_word *av) C_noret;
C_noret_decl(f_4501)
static void C_ccall f_4501(C_word c,C_word *av) C_noret;
C_noret_decl(f_4506)
static void C_ccall f_4506(C_word c,C_word *av) C_noret;
C_noret_decl(f_4510)
static void C_ccall f_4510(C_word c,C_word *av) C_noret;
C_noret_decl(f_4515)
static void C_ccall f_4515(C_word c,C_word *av) C_noret;
C_noret_decl(f_4520)
static void C_ccall f_4520(C_word c,C_word *av) C_noret;
C_noret_decl(f_4526)
static void C_ccall f_4526(C_word c,C_word *av) C_noret;
C_noret_decl(f_4530)
static void C_ccall f_4530(C_word c,C_word *av) C_noret;
C_noret_decl(f_4535)
static void C_ccall f_4535(C_word c,C_word *av) C_noret;
C_noret_decl(f_4540)
static void C_ccall f_4540(C_word c,C_word *av) C_noret;
C_noret_decl(f_4547)
static void C_ccall f_4547(C_word c,C_word *av) C_noret;
C_noret_decl(f_4587)
static void C_ccall f_4587(C_word c,C_word *av) C_noret;
C_noret_decl(f_4594)
static void C_ccall f_4594(C_word c,C_word *av) C_noret;
C_noret_decl(f_4597)
static void C_ccall f_4597(C_word c,C_word *av) C_noret;
C_noret_decl(f_4621)
static void C_ccall f_4621(C_word c,C_word *av) C_noret;
C_noret_decl(f_4631)
static void C_ccall f_4631(C_word c,C_word *av) C_noret;
C_noret_decl(f_4634)
static void C_ccall f_4634(C_word c,C_word *av) C_noret;
C_noret_decl(f_4638)
static void C_ccall f_4638(C_word c,C_word *av) C_noret;
C_noret_decl(f_4641)
static void C_ccall f_4641(C_word c,C_word *av) C_noret;
C_noret_decl(f_4653)
static void C_ccall f_4653(C_word c,C_word *av) C_noret;
C_noret_decl(f_4657)
static void C_ccall f_4657(C_word c,C_word *av) C_noret;
C_noret_decl(f_4662)
static void C_ccall f_4662(C_word c,C_word *av) C_noret;
C_noret_decl(f_4684)
static void C_ccall f_4684(C_word c,C_word *av) C_noret;
C_noret_decl(f_4688)
static void C_ccall f_4688(C_word c,C_word *av) C_noret;
C_noret_decl(f_4691)
static void C_ccall f_4691(C_word c,C_word *av) C_noret;
C_noret_decl(f_4694)
static void C_ccall f_4694(C_word c,C_word *av) C_noret;
C_noret_decl(f_4697)
static void C_ccall f_4697(C_word c,C_word *av) C_noret;
C_noret_decl(f_4700)
static void C_ccall f_4700(C_word c,C_word *av) C_noret;
C_noret_decl(f_4724)
static void C_ccall f_4724(C_word c,C_word *av) C_noret;
C_noret_decl(f_4728)
static void C_ccall f_4728(C_word c,C_word *av) C_noret;
C_noret_decl(f_4731)
static void C_ccall f_4731(C_word c,C_word *av) C_noret;
C_noret_decl(f_4737)
static void C_ccall f_4737(C_word c,C_word *av) C_noret;
C_noret_decl(f_4740)
static void C_ccall f_4740(C_word c,C_word *av) C_noret;
C_noret_decl(f_4761)
static void C_ccall f_4761(C_word c,C_word *av) C_noret;
C_noret_decl(f_4768)
static void C_ccall f_4768(C_word c,C_word *av) C_noret;
C_noret_decl(f_4774)
static void C_ccall f_4774(C_word c,C_word *av) C_noret;
C_noret_decl(f_4781)
static void C_ccall f_4781(C_word c,C_word *av) C_noret;
C_noret_decl(f_4793)
static void C_ccall f_4793(C_word c,C_word *av) C_noret;
C_noret_decl(f_4800)
static void C_fcall f_4800(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4803)
static void C_fcall f_4803(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4811)
static void C_ccall f_4811(C_word c,C_word *av) C_noret;
C_noret_decl(f_4814)
static void C_ccall f_4814(C_word c,C_word *av) C_noret;
C_noret_decl(f_4875)
static void C_ccall f_4875(C_word c,C_word *av) C_noret;
C_noret_decl(f_4878)
static void C_ccall f_4878(C_word c,C_word *av) C_noret;
C_noret_decl(f_4885)
static void C_fcall f_4885(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4915)
static void C_fcall f_4915(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_4974)
static void C_fcall f_4974(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_5035)
static void C_fcall f_5035(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5042)
static void C_ccall f_5042(C_word c,C_word *av) C_noret;
C_noret_decl(f_5044)
static C_word C_fcall f_5044(C_word t0,C_word t1,C_word t2);
C_noret_decl(f_5086)
static C_word C_fcall f_5086(C_word t0,C_word t1,C_word t2);
C_noret_decl(f_5170)
static void C_ccall f_5170(C_word c,C_word *av) C_noret;
C_noret_decl(f_5177)
static void C_ccall f_5177(C_word c,C_word *av) C_noret;
C_noret_decl(f_5226)
static void C_ccall f_5226(C_word c,C_word *av) C_noret;
C_noret_decl(f_5235)
static void C_ccall f_5235(C_word c,C_word *av) C_noret;
C_noret_decl(f_5238)
static void C_ccall f_5238(C_word c,C_word *av) C_noret;
C_noret_decl(f_5250)
static void C_fcall f_5250(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5275)
static void C_ccall f_5275(C_word c,C_word *av) C_noret;
C_noret_decl(f_5277)
static void C_ccall f_5277(C_word c,C_word *av) C_noret;
C_noret_decl(f_5281)
static void C_ccall f_5281(C_word c,C_word *av) C_noret;
C_noret_decl(f_5283)
static void C_ccall f_5283(C_word c,C_word *av) C_noret;
C_noret_decl(f_5287)
static void C_ccall f_5287(C_word c,C_word *av) C_noret;
C_noret_decl(f_5299)
static void C_ccall f_5299(C_word c,C_word *av) C_noret;
C_noret_decl(f_5303)
static void C_ccall f_5303(C_word c,C_word *av) C_noret;
C_noret_decl(f_5317)
static void C_ccall f_5317(C_word c,C_word *av) C_noret;
C_noret_decl(f_5321)
static void C_ccall f_5321(C_word c,C_word *av) C_noret;
C_noret_decl(f_5325)
static void C_ccall f_5325(C_word c,C_word *av) C_noret;
C_noret_decl(f_5329)
static void C_ccall f_5329(C_word c,C_word *av) C_noret;
C_noret_decl(f_5331)
static void C_ccall f_5331(C_word c,C_word *av) C_noret;
C_noret_decl(f_5338)
static void C_fcall f_5338(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5351)
static void C_ccall f_5351(C_word c,C_word *av) C_noret;
C_noret_decl(f_5355)
static void C_ccall f_5355(C_word c,C_word *av) C_noret;
C_noret_decl(f_5359)
static void C_ccall f_5359(C_word c,C_word *av) C_noret;
C_noret_decl(f_5363)
static void C_ccall f_5363(C_word c,C_word *av) C_noret;
C_noret_decl(f_5367)
static void C_ccall f_5367(C_word c,C_word *av) C_noret;
C_noret_decl(f_5377)
static void C_ccall f_5377(C_word c,C_word *av) C_noret;
C_noret_decl(f_5385)
static void C_ccall f_5385(C_word c,C_word *av) C_noret;
C_noret_decl(f_5393)
static void C_ccall f_5393(C_word c,C_word *av) C_noret;
C_noret_decl(f_5397)
static void C_ccall f_5397(C_word c,C_word *av) C_noret;
C_noret_decl(f_5399)
static void C_ccall f_5399(C_word c,C_word *av) C_noret;
C_noret_decl(f_5407)
static void C_ccall f_5407(C_word c,C_word *av) C_noret;
C_noret_decl(f_5411)
static void C_ccall f_5411(C_word c,C_word *av) C_noret;
C_noret_decl(f_5413)
static void C_fcall f_5413(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_5417)
static void C_ccall f_5417(C_word c,C_word *av) C_noret;
C_noret_decl(f_5420)
static void C_ccall f_5420(C_word c,C_word *av) C_noret;
C_noret_decl(f_5423)
static void C_ccall f_5423(C_word c,C_word *av) C_noret;
C_noret_decl(f_5435)
static void C_ccall f_5435(C_word c,C_word *av) C_noret;
C_noret_decl(f_5439)
static void C_ccall f_5439(C_word c,C_word *av) C_noret;
C_noret_decl(f_5455)
static void C_ccall f_5455(C_word c,C_word *av) C_noret;
C_noret_decl(f_5460)
static void C_ccall f_5460(C_word c,C_word *av) C_noret;
C_noret_decl(f_5464)
static void C_ccall f_5464(C_word c,C_word *av) C_noret;
C_noret_decl(f_5470)
static void C_ccall f_5470(C_word c,C_word *av) C_noret;
C_noret_decl(f_5477)
static void C_ccall f_5477(C_word c,C_word *av) C_noret;
C_noret_decl(f_5479)
static void C_ccall f_5479(C_word c,C_word *av) C_noret;
C_noret_decl(f_5500)
static void C_ccall f_5500(C_word c,C_word *av) C_noret;
C_noret_decl(f_5504)
static void C_ccall f_5504(C_word c,C_word *av) C_noret;
C_noret_decl(f_5508)
static void C_ccall f_5508(C_word c,C_word *av) C_noret;
C_noret_decl(f_5509)
static void C_ccall f_5509(C_word c,C_word *av) C_noret;
C_noret_decl(f_5514)
static void C_ccall f_5514(C_word c,C_word *av) C_noret;
C_noret_decl(f_5528)
static void C_ccall f_5528(C_word c,C_word *av) C_noret;
C_noret_decl(f_5543)
static void C_ccall f_5543(C_word c,C_word *av) C_noret;
C_noret_decl(f_5549)
static void C_ccall f_5549(C_word c,C_word *av) C_noret;
C_noret_decl(f_5557)
static void C_ccall f_5557(C_word c,C_word *av) C_noret;
C_noret_decl(f_5559)
static void C_fcall f_5559(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_5569)
static void C_ccall f_5569(C_word c,C_word *av) C_noret;
C_noret_decl(f_5575)
static void C_ccall f_5575(C_word c,C_word *av) C_noret;
C_noret_decl(f_5584)
static void C_ccall f_5584(C_word c,C_word *av) C_noret;
C_noret_decl(f_5587)
static void C_ccall f_5587(C_word c,C_word *av) C_noret;
C_noret_decl(f_5590)
static void C_ccall f_5590(C_word c,C_word *av) C_noret;
C_noret_decl(f_5596)
static void C_ccall f_5596(C_word c,C_word *av) C_noret;
C_noret_decl(f_5630)
static void C_ccall f_5630(C_word c,C_word *av) C_noret;
C_noret_decl(f_5634)
static void C_ccall f_5634(C_word c,C_word *av) C_noret;
C_noret_decl(f_5643)
static void C_ccall f_5643(C_word c,C_word *av) C_noret;
C_noret_decl(f_5665)
static void C_ccall f_5665(C_word c,C_word *av) C_noret;
C_noret_decl(f_5693)
static void C_ccall f_5693(C_word c,C_word *av) C_noret;
C_noret_decl(f_5699)
static void C_ccall f_5699(C_word c,C_word *av) C_noret;
C_noret_decl(f_5700)
static void C_fcall f_5700(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5704)
static void C_ccall f_5704(C_word c,C_word *av) C_noret;
C_noret_decl(f_5729)
static C_word C_fcall f_5729(C_word t0);
C_noret_decl(f_5737)
static void C_fcall f_5737(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5743)
static void C_ccall f_5743(C_word c,C_word *av) C_noret;
C_noret_decl(f_5762)
static void C_ccall f_5762(C_word c,C_word *av) C_noret;
C_noret_decl(f_5765)
static void C_ccall f_5765(C_word c,C_word *av) C_noret;
C_noret_decl(f_5795)
static void C_ccall f_5795(C_word c,C_word *av) C_noret;
C_noret_decl(f_5798)
static void C_ccall f_5798(C_word c,C_word *av) C_noret;
C_noret_decl(f_5804)
static void C_ccall f_5804(C_word c,C_word *av) C_noret;
C_noret_decl(f_5833)
static void C_ccall f_5833(C_word c,C_word *av) C_noret;
C_noret_decl(f_5839)
static void C_ccall f_5839(C_word c,C_word *av) C_noret;
C_noret_decl(f_5843)
static void C_ccall f_5843(C_word c,C_word *av) C_noret;
C_noret_decl(f_5864)
static void C_ccall f_5864(C_word c,C_word *av) C_noret;
C_noret_decl(f_5876)
static void C_ccall f_5876(C_word c,C_word *av) C_noret;
C_noret_decl(f_5880)
static void C_ccall f_5880(C_word c,C_word *av) C_noret;
C_noret_decl(f_5892)
static void C_ccall f_5892(C_word c,C_word *av) C_noret;
C_noret_decl(f_5896)
static void C_ccall f_5896(C_word c,C_word *av) C_noret;
C_noret_decl(f_5907)
static void C_ccall f_5907(C_word c,C_word *av) C_noret;
C_noret_decl(f_5917)
static void C_fcall f_5917(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_5965)
static void C_ccall f_5965(C_word c,C_word *av) C_noret;
C_noret_decl(f_5983)
static void C_ccall f_5983(C_word c,C_word *av) C_noret;
C_noret_decl(f_5987)
static void C_ccall f_5987(C_word c,C_word *av) C_noret;
C_noret_decl(f_6001)
static void C_ccall f_6001(C_word c,C_word *av) C_noret;
C_noret_decl(f_6011)
static void C_ccall f_6011(C_word c,C_word *av) C_noret;
C_noret_decl(f_6031)
static void C_ccall f_6031(C_word c,C_word *av) C_noret;
C_noret_decl(f_6057)
static void C_ccall f_6057(C_word c,C_word *av) C_noret;
C_noret_decl(f_6102)
static void C_ccall f_6102(C_word c,C_word *av) C_noret;
C_noret_decl(f_6112)
static void C_ccall f_6112(C_word c,C_word *av) C_noret;
C_noret_decl(f_6178)
static void C_ccall f_6178(C_word c,C_word *av) C_noret;
C_noret_decl(f_6200)
static void C_ccall f_6200(C_word c,C_word *av) C_noret;
C_noret_decl(f_6201)
static void C_ccall f_6201(C_word c,C_word *av) C_noret;
C_noret_decl(f_6207)
static void C_ccall f_6207(C_word c,C_word *av) C_noret;
C_noret_decl(f_6226)
static void C_ccall f_6226(C_word c,C_word *av) C_noret;
C_noret_decl(f_6257)
static void C_ccall f_6257(C_word c,C_word *av) C_noret;
C_noret_decl(f_6267)
static void C_fcall f_6267(C_word t0,C_word t1) C_noret;
C_noret_decl(f_6272)
static void C_ccall f_6272(C_word c,C_word *av) C_noret;
C_noret_decl(f_6278)
static void C_ccall f_6278(C_word c,C_word *av) C_noret;
C_noret_decl(f_6284)
static void C_ccall f_6284(C_word c,C_word *av) C_noret;
C_noret_decl(f_6288)
static void C_ccall f_6288(C_word c,C_word *av) C_noret;
C_noret_decl(f_6300)
static void C_ccall f_6300(C_word c,C_word *av) C_noret;
C_noret_decl(f_6308)
static void C_ccall f_6308(C_word c,C_word *av) C_noret;
C_noret_decl(f_6322)
static void C_ccall f_6322(C_word c,C_word *av) C_noret;
C_noret_decl(f_6323)
static void C_ccall f_6323(C_word c,C_word *av) C_noret;
C_noret_decl(f_6340)
static void C_fcall f_6340(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_6350)
static void C_ccall f_6350(C_word c,C_word *av) C_noret;
C_noret_decl(f_6433)
static void C_ccall f_6433(C_word c,C_word *av) C_noret;
C_noret_decl(f_6437)
static void C_ccall f_6437(C_word c,C_word *av) C_noret;
C_noret_decl(f_6443)
static void C_fcall f_6443(C_word t0,C_word t1) C_noret;
C_noret_decl(f_6450)
static void C_ccall f_6450(C_word c,C_word *av) C_noret;
C_noret_decl(f_6457)
static void C_ccall f_6457(C_word c,C_word *av) C_noret;
C_noret_decl(f_6463)
static void C_ccall f_6463(C_word c,C_word *av) C_noret;
C_noret_decl(f_6467)
static void C_ccall f_6467(C_word c,C_word *av) C_noret;
C_noret_decl(f_6478)
static void C_fcall f_6478(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_6496)
static void C_ccall f_6496(C_word c,C_word *av) C_noret;
C_noret_decl(f_6499)
static void C_ccall f_6499(C_word c,C_word *av) C_noret;
C_noret_decl(f_6564)
static void C_ccall f_6564(C_word c,C_word *av) C_noret;
C_noret_decl(f_6570)
static void C_ccall f_6570(C_word c,C_word *av) C_noret;
C_noret_decl(f_6574)
static void C_ccall f_6574(C_word c,C_word *av) C_noret;
C_noret_decl(f_6595)
static void C_ccall f_6595(C_word c,C_word *av) C_noret;
C_noret_decl(f_6601)
static void C_ccall f_6601(C_word c,C_word *av) C_noret;
C_noret_decl(f_6605)
static void C_ccall f_6605(C_word c,C_word *av) C_noret;
C_noret_decl(f_6626)
static void C_ccall f_6626(C_word c,C_word *av) C_noret;
C_noret_decl(f_6630)
static void C_ccall f_6630(C_word c,C_word *av) C_noret;
C_noret_decl(f_6653)
static void C_ccall f_6653(C_word c,C_word *av) C_noret;
C_noret_decl(f_6682)
static void C_ccall f_6682(C_word c,C_word *av) C_noret;
C_noret_decl(f_6696)
static void C_ccall f_6696(C_word c,C_word *av) C_noret;
C_noret_decl(f_6706)
static void C_ccall f_6706(C_word c,C_word *av) C_noret;
C_noret_decl(f_6720)
static void C_ccall f_6720(C_word c,C_word *av) C_noret;
C_noret_decl(f_6742)
static void C_ccall f_6742(C_word c,C_word *av) C_noret;
C_noret_decl(f_6759)
static void C_ccall f_6759(C_word c,C_word *av) C_noret;
C_noret_decl(f_6763)
static void C_ccall f_6763(C_word c,C_word *av) C_noret;
C_noret_decl(f_6771)
static void C_ccall f_6771(C_word c,C_word *av) C_noret;
C_noret_decl(f_6779)
static void C_ccall f_6779(C_word c,C_word *av) C_noret;
C_noret_decl(f_6783)
static void C_ccall f_6783(C_word c,C_word *av) C_noret;
C_noret_decl(f_6787)
static void C_ccall f_6787(C_word c,C_word *av) C_noret;
C_noret_decl(f_6798)
static void C_ccall f_6798(C_word c,C_word *av) C_noret;
C_noret_decl(f_6805)
static void C_ccall f_6805(C_word c,C_word *av) C_noret;
C_noret_decl(f_6814)
static void C_ccall f_6814(C_word c,C_word *av) C_noret;
C_noret_decl(f_6845)
static void C_ccall f_6845(C_word c,C_word *av) C_noret;
C_noret_decl(f_6859)
static void C_ccall f_6859(C_word c,C_word *av) C_noret;
C_noret_decl(f_6865)
static void C_ccall f_6865(C_word c,C_word *av) C_noret;
C_noret_decl(f_6869)
static void C_ccall f_6869(C_word c,C_word *av) C_noret;
C_noret_decl(f_6873)
static void C_ccall f_6873(C_word c,C_word *av) C_noret;
C_noret_decl(f_6913)
static void C_ccall f_6913(C_word c,C_word *av) C_noret;
C_noret_decl(f_6937)
static void C_ccall f_6937(C_word c,C_word *av) C_noret;
C_noret_decl(f_6940)
static void C_ccall f_6940(C_word c,C_word *av) C_noret;
C_noret_decl(f_6991)
static void C_fcall f_6991(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7007)
static void C_ccall f_7007(C_word c,C_word *av) C_noret;
C_noret_decl(f_7029)
static void C_ccall f_7029(C_word c,C_word *av) C_noret;
C_noret_decl(f_7032)
static void C_ccall f_7032(C_word c,C_word *av) C_noret;
C_noret_decl(f_7039)
static void C_ccall f_7039(C_word c,C_word *av) C_noret;
C_noret_decl(f_7042)
static void C_ccall f_7042(C_word c,C_word *av) C_noret;
C_noret_decl(f_7072)
static void C_ccall f_7072(C_word c,C_word *av) C_noret;
C_noret_decl(f_7079)
static void C_ccall f_7079(C_word c,C_word *av) C_noret;
C_noret_decl(f_7122)
static void C_ccall f_7122(C_word c,C_word *av) C_noret;
C_noret_decl(f_7126)
static void C_fcall f_7126(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6) C_noret;
C_noret_decl(f_7128)
static void C_ccall f_7128(C_word c,C_word *av) C_noret;
C_noret_decl(f_7143)
static void C_ccall f_7143(C_word c,C_word *av) C_noret;
C_noret_decl(f_7149)
static void C_ccall f_7149(C_word c,C_word *av) C_noret;
C_noret_decl(f_7163)
static void C_fcall f_7163(C_word t0,C_word t1) C_noret;
C_noret_decl(f_7172)
static void C_ccall f_7172(C_word c,C_word *av) C_noret;
C_noret_decl(f_7178)
static void C_ccall f_7178(C_word c,C_word *av) C_noret;
C_noret_decl(f_7183)
static void C_fcall f_7183(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7194)
static void C_ccall f_7194(C_word c,C_word *av) C_noret;
C_noret_decl(f_7195)
static void C_fcall f_7195(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_7206)
static void C_ccall f_7206(C_word c,C_word *av) C_noret;
C_noret_decl(f_7224)
static void C_fcall f_7224(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6,C_word t7) C_noret;
C_noret_decl(f_7228)
static void C_ccall f_7228(C_word c,C_word *av) C_noret;
C_noret_decl(f_7231)
static void C_ccall f_7231(C_word c,C_word *av) C_noret;
C_noret_decl(f_7234)
static void C_ccall f_7234(C_word c,C_word *av) C_noret;
C_noret_decl(f_7241)
static void C_fcall f_7241(C_word t0,C_word t1) C_noret;
C_noret_decl(f_7245)
static void C_ccall f_7245(C_word c,C_word *av) C_noret;
C_noret_decl(f_7247)
static void C_ccall f_7247(C_word c,C_word *av) C_noret;
C_noret_decl(f_7251)
static void C_ccall f_7251(C_word c,C_word *av) C_noret;
C_noret_decl(f_7254)
static void C_ccall f_7254(C_word c,C_word *av) C_noret;
C_noret_decl(f_7257)
static void C_ccall f_7257(C_word c,C_word *av) C_noret;
C_noret_decl(f_7269)
static void C_fcall f_7269(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6) C_noret;
C_noret_decl(f_7273)
static void C_ccall f_7273(C_word c,C_word *av) C_noret;
C_noret_decl(f_7280)
static void C_fcall f_7280(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6) C_noret;
C_noret_decl(f_7284)
static void C_ccall f_7284(C_word c,C_word *av) C_noret;
C_noret_decl(f_7291)
static void C_fcall f_7291(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6,C_word t7,C_word t8) C_noret;
C_noret_decl(f_7297)
static void C_ccall f_7297(C_word c,C_word *av) C_noret;
C_noret_decl(f_7303)
static void C_ccall f_7303(C_word c,C_word *av) C_noret;
C_noret_decl(f_7314)
static void C_ccall f_7314(C_word c,C_word *av) C_noret;
C_noret_decl(f_7318)
static void C_ccall f_7318(C_word c,C_word *av) C_noret;
C_noret_decl(f_7322)
static void C_ccall f_7322(C_word c,C_word *av) C_noret;
C_noret_decl(f_7326)
static void C_ccall f_7326(C_word c,C_word *av) C_noret;
C_noret_decl(f_7330)
static void C_ccall f_7330(C_word c,C_word *av) C_noret;
C_noret_decl(f_7334)
static void C_ccall f_7334(C_word c,C_word *av) C_noret;
C_noret_decl(f_7347)
static void C_fcall f_7347(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6) C_noret;
C_noret_decl(f_7349)
static void C_fcall f_7349(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7354)
static C_word C_fcall f_7354(C_word t0,C_word t1);
C_noret_decl(f_7363)
static C_word C_fcall f_7363(C_word t0,C_word t1);
C_noret_decl(f_7390)
static void C_ccall f_7390(C_word c,C_word *av) C_noret;
C_noret_decl(f_7393)
static void C_ccall f_7393(C_word c,C_word *av) C_noret;
C_noret_decl(f_7398)
static void C_ccall f_7398(C_word c,C_word *av) C_noret;
C_noret_decl(f_7418)
static void C_ccall f_7418(C_word c,C_word *av) C_noret;
C_noret_decl(f_7442)
static void C_ccall f_7442(C_word c,C_word *av) C_noret;
C_noret_decl(f_7484)
static void C_ccall f_7484(C_word c,C_word *av) C_noret;
C_noret_decl(f_7549)
static void C_ccall f_7549(C_word c,C_word *av) C_noret;
C_noret_decl(f_7554)
static void C_ccall f_7554(C_word c,C_word *av) C_noret;
C_noret_decl(f_7573)
static void C_ccall f_7573(C_word c,C_word *av) C_noret;
C_noret_decl(f_7579)
static void C_ccall f_7579(C_word c,C_word *av) C_noret;
C_noret_decl(f_7628)
static void C_ccall f_7628(C_word c,C_word *av) C_noret;
C_noret_decl(f_7632)
static void C_ccall f_7632(C_word c,C_word *av) C_noret;
C_noret_decl(f_7635)
static void C_ccall f_7635(C_word c,C_word *av) C_noret;
C_noret_decl(f_7641)
static void C_ccall f_7641(C_word c,C_word *av) C_noret;
C_noret_decl(f_7646)
static void C_ccall f_7646(C_word c,C_word *av) C_noret;
C_noret_decl(f_7650)
static void C_ccall f_7650(C_word c,C_word *av) C_noret;
C_noret_decl(f_7653)
static void C_ccall f_7653(C_word c,C_word *av) C_noret;
C_noret_decl(f_7662)
static void C_ccall f_7662(C_word c,C_word *av) C_noret;
C_noret_decl(f_7668)
static void C_ccall f_7668(C_word c,C_word *av) C_noret;
C_noret_decl(f_7671)
static void C_ccall f_7671(C_word c,C_word *av) C_noret;
C_noret_decl(f_7681)
static void C_ccall f_7681(C_word c,C_word *av) C_noret;
C_noret_decl(f_7687)
static void C_ccall f_7687(C_word c,C_word *av) C_noret;
C_noret_decl(f_7690)
static void C_ccall f_7690(C_word c,C_word *av) C_noret;
C_noret_decl(f_7700)
static void C_ccall f_7700(C_word c,C_word *av) C_noret;
C_noret_decl(f_7706)
static void C_ccall f_7706(C_word c,C_word *av) C_noret;
C_noret_decl(f_7709)
static void C_ccall f_7709(C_word c,C_word *av) C_noret;
C_noret_decl(f_7719)
static void C_ccall f_7719(C_word c,C_word *av) C_noret;
C_noret_decl(f_7725)
static void C_ccall f_7725(C_word c,C_word *av) C_noret;
C_noret_decl(f_7728)
static void C_ccall f_7728(C_word c,C_word *av) C_noret;
C_noret_decl(f_7738)
static void C_ccall f_7738(C_word c,C_word *av) C_noret;
C_noret_decl(f_7744)
static void C_ccall f_7744(C_word c,C_word *av) C_noret;
C_noret_decl(f_7750)
static void C_fcall f_7750(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_7774)
static void C_ccall f_7774(C_word c,C_word *av) C_noret;
C_noret_decl(f_7778)
static void C_ccall f_7778(C_word c,C_word *av) C_noret;
C_noret_decl(f_7783)
static void C_ccall f_7783(C_word c,C_word *av) C_noret;
C_noret_decl(f_7790)
static void C_ccall f_7790(C_word c,C_word *av) C_noret;
C_noret_decl(f_7808)
static void C_ccall f_7808(C_word c,C_word *av) C_noret;
C_noret_decl(f_7812)
static void C_ccall f_7812(C_word c,C_word *av) C_noret;
C_noret_decl(f_7814)
static void C_ccall f_7814(C_word c,C_word *av) C_noret;
C_noret_decl(f_7818)
static void C_ccall f_7818(C_word c,C_word *av) C_noret;
C_noret_decl(f_7821)
static void C_ccall f_7821(C_word c,C_word *av) C_noret;
C_noret_decl(f_7830)
static void C_ccall f_7830(C_word c,C_word *av) C_noret;
C_noret_decl(f_7851)
static void C_ccall f_7851(C_word c,C_word *av) C_noret;
C_noret_decl(f_7855)
static void C_ccall f_7855(C_word c,C_word *av) C_noret;
C_noret_decl(f_7857)
static void C_ccall f_7857(C_word c,C_word *av) C_noret;
C_noret_decl(f_7861)
static void C_ccall f_7861(C_word c,C_word *av) C_noret;
C_noret_decl(f_7863)
static void C_ccall f_7863(C_word c,C_word *av) C_noret;
C_noret_decl(f_7867)
static void C_ccall f_7867(C_word c,C_word *av) C_noret;
C_noret_decl(C_posix_toplevel)
C_externexport void C_ccall C_posix_toplevel(C_word c,C_word *av) C_noret;

C_noret_decl(trf_2766)
static void C_ccall trf_2766(C_word c,C_word *av) C_noret;
static void C_ccall trf_2766(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_2766(t0,t1,t2);}

C_noret_decl(trf_2807)
static void C_ccall trf_2807(C_word c,C_word *av) C_noret;
static void C_ccall trf_2807(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_2807(t0,t1,t2);}

C_noret_decl(trf_2957)
static void C_ccall trf_2957(C_word c,C_word *av) C_noret;
static void C_ccall trf_2957(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_2957(t0,t1,t2,t3,t4);}

C_noret_decl(trf_3118)
static void C_ccall trf_3118(C_word c,C_word *av) C_noret;
static void C_ccall trf_3118(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_3118(t0,t1);}

C_noret_decl(trf_3435)
static void C_ccall trf_3435(C_word c,C_word *av) C_noret;
static void C_ccall trf_3435(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_3435(t0,t1,t2,t3);}

C_noret_decl(trf_3472)
static void C_ccall trf_3472(C_word c,C_word *av) C_noret;
static void C_ccall trf_3472(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_3472(t0,t1,t2,t3,t4);}

C_noret_decl(trf_3571)
static void C_ccall trf_3571(C_word c,C_word *av) C_noret;
static void C_ccall trf_3571(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_3571(t0,t1);}

C_noret_decl(trf_3627)
static void C_ccall trf_3627(C_word c,C_word *av) C_noret;
static void C_ccall trf_3627(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3627(t0,t1,t2);}

C_noret_decl(trf_3938)
static void C_ccall trf_3938(C_word c,C_word *av) C_noret;
static void C_ccall trf_3938(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_3938(t0,t1,t2,t3);}

C_noret_decl(trf_3977)
static void C_ccall trf_3977(C_word c,C_word *av) C_noret;
static void C_ccall trf_3977(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_3977(t0,t1);}

C_noret_decl(trf_3983)
static void C_ccall trf_3983(C_word c,C_word *av) C_noret;
static void C_ccall trf_3983(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_3983(t0,t1,t2,t3);}

C_noret_decl(trf_4034)
static void C_ccall trf_4034(C_word c,C_word *av) C_noret;
static void C_ccall trf_4034(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4034(t0,t1,t2);}

C_noret_decl(trf_4055)
static void C_ccall trf_4055(C_word c,C_word *av) C_noret;
static void C_ccall trf_4055(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4055(t0,t1);}

C_noret_decl(trf_4064)
static void C_ccall trf_4064(C_word c,C_word *av) C_noret;
static void C_ccall trf_4064(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4064(t0,t1,t2);}

C_noret_decl(trf_4089)
static void C_ccall trf_4089(C_word c,C_word *av) C_noret;
static void C_ccall trf_4089(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4089(t0,t1,t2);}

C_noret_decl(trf_4138)
static void C_ccall trf_4138(C_word c,C_word *av) C_noret;
static void C_ccall trf_4138(C_word c,C_word *av){
C_word t0=av[7];
C_word t1=av[6];
C_word t2=av[5];
C_word t3=av[4];
C_word t4=av[3];
C_word t5=av[2];
C_word t6=av[1];
C_word t7=av[0];
f_4138(t0,t1,t2,t3,t4,t5,t6,t7);}

C_noret_decl(trf_4193)
static void C_ccall trf_4193(C_word c,C_word *av) C_noret;
static void C_ccall trf_4193(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4193(t0,t1);}

C_noret_decl(trf_4230)
static void C_ccall trf_4230(C_word c,C_word *av) C_noret;
static void C_ccall trf_4230(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4230(t0,t1,t2);}

C_noret_decl(trf_4295)
static void C_ccall trf_4295(C_word c,C_word *av) C_noret;
static void C_ccall trf_4295(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_4295(t0,t1,t2,t3,t4);}

C_noret_decl(trf_4800)
static void C_ccall trf_4800(C_word c,C_word *av) C_noret;
static void C_ccall trf_4800(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4800(t0,t1);}

C_noret_decl(trf_4803)
static void C_ccall trf_4803(C_word c,C_word *av) C_noret;
static void C_ccall trf_4803(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4803(t0,t1);}

C_noret_decl(trf_4885)
static void C_ccall trf_4885(C_word c,C_word *av) C_noret;
static void C_ccall trf_4885(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4885(t0,t1);}

C_noret_decl(trf_4915)
static void C_ccall trf_4915(C_word c,C_word *av) C_noret;
static void C_ccall trf_4915(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_4915(t0,t1,t2,t3,t4);}

C_noret_decl(trf_4974)
static void C_ccall trf_4974(C_word c,C_word *av) C_noret;
static void C_ccall trf_4974(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_4974(t0,t1,t2,t3,t4);}

C_noret_decl(trf_5035)
static void C_ccall trf_5035(C_word c,C_word *av) C_noret;
static void C_ccall trf_5035(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5035(t0,t1);}

C_noret_decl(trf_5250)
static void C_ccall trf_5250(C_word c,C_word *av) C_noret;
static void C_ccall trf_5250(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5250(t0,t1,t2);}

C_noret_decl(trf_5338)
static void C_ccall trf_5338(C_word c,C_word *av) C_noret;
static void C_ccall trf_5338(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5338(t0,t1);}

C_noret_decl(trf_5413)
static void C_ccall trf_5413(C_word c,C_word *av) C_noret;
static void C_ccall trf_5413(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_5413(t0,t1,t2,t3,t4);}

C_noret_decl(trf_5559)
static void C_ccall trf_5559(C_word c,C_word *av) C_noret;
static void C_ccall trf_5559(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_5559(t0,t1,t2,t3);}

C_noret_decl(trf_5700)
static void C_ccall trf_5700(C_word c,C_word *av) C_noret;
static void C_ccall trf_5700(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5700(t0,t1);}

C_noret_decl(trf_5737)
static void C_ccall trf_5737(C_word c,C_word *av) C_noret;
static void C_ccall trf_5737(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5737(t0,t1);}

C_noret_decl(trf_5917)
static void C_ccall trf_5917(C_word c,C_word *av) C_noret;
static void C_ccall trf_5917(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_5917(t0,t1,t2,t3,t4);}

C_noret_decl(trf_6267)
static void C_ccall trf_6267(C_word c,C_word *av) C_noret;
static void C_ccall trf_6267(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_6267(t0,t1);}

C_noret_decl(trf_6340)
static void C_ccall trf_6340(C_word c,C_word *av) C_noret;
static void C_ccall trf_6340(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_6340(t0,t1,t2,t3,t4);}

C_noret_decl(trf_6443)
static void C_ccall trf_6443(C_word c,C_word *av) C_noret;
static void C_ccall trf_6443(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_6443(t0,t1);}

C_noret_decl(trf_6478)
static void C_ccall trf_6478(C_word c,C_word *av) C_noret;
static void C_ccall trf_6478(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_6478(t0,t1,t2,t3);}

C_noret_decl(trf_6991)
static void C_ccall trf_6991(C_word c,C_word *av) C_noret;
static void C_ccall trf_6991(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6991(t0,t1,t2);}

C_noret_decl(trf_7126)
static void C_ccall trf_7126(C_word c,C_word *av) C_noret;
static void C_ccall trf_7126(C_word c,C_word *av){
C_word t0=av[6];
C_word t1=av[5];
C_word t2=av[4];
C_word t3=av[3];
C_word t4=av[2];
C_word t5=av[1];
C_word t6=av[0];
f_7126(t0,t1,t2,t3,t4,t5,t6);}

C_noret_decl(trf_7163)
static void C_ccall trf_7163(C_word c,C_word *av) C_noret;
static void C_ccall trf_7163(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_7163(t0,t1);}

C_noret_decl(trf_7183)
static void C_ccall trf_7183(C_word c,C_word *av) C_noret;
static void C_ccall trf_7183(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7183(t0,t1,t2);}

C_noret_decl(trf_7195)
static void C_ccall trf_7195(C_word c,C_word *av) C_noret;
static void C_ccall trf_7195(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_7195(t0,t1,t2,t3);}

C_noret_decl(trf_7224)
static void C_ccall trf_7224(C_word c,C_word *av) C_noret;
static void C_ccall trf_7224(C_word c,C_word *av){
C_word t0=av[7];
C_word t1=av[6];
C_word t2=av[5];
C_word t3=av[4];
C_word t4=av[3];
C_word t5=av[2];
C_word t6=av[1];
C_word t7=av[0];
f_7224(t0,t1,t2,t3,t4,t5,t6,t7);}

C_noret_decl(trf_7241)
static void C_ccall trf_7241(C_word c,C_word *av) C_noret;
static void C_ccall trf_7241(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_7241(t0,t1);}

C_noret_decl(trf_7269)
static void C_ccall trf_7269(C_word c,C_word *av) C_noret;
static void C_ccall trf_7269(C_word c,C_word *av){
C_word t0=av[6];
C_word t1=av[5];
C_word t2=av[4];
C_word t3=av[3];
C_word t4=av[2];
C_word t5=av[1];
C_word t6=av[0];
f_7269(t0,t1,t2,t3,t4,t5,t6);}

C_noret_decl(trf_7280)
static void C_ccall trf_7280(C_word c,C_word *av) C_noret;
static void C_ccall trf_7280(C_word c,C_word *av){
C_word t0=av[6];
C_word t1=av[5];
C_word t2=av[4];
C_word t3=av[3];
C_word t4=av[2];
C_word t5=av[1];
C_word t6=av[0];
f_7280(t0,t1,t2,t3,t4,t5,t6);}

C_noret_decl(trf_7291)
static void C_ccall trf_7291(C_word c,C_word *av) C_noret;
static void C_ccall trf_7291(C_word c,C_word *av){
C_word t0=av[8];
C_word t1=av[7];
C_word t2=av[6];
C_word t3=av[5];
C_word t4=av[4];
C_word t5=av[3];
C_word t6=av[2];
C_word t7=av[1];
C_word t8=av[0];
f_7291(t0,t1,t2,t3,t4,t5,t6,t7,t8);}

C_noret_decl(trf_7347)
static void C_ccall trf_7347(C_word c,C_word *av) C_noret;
static void C_ccall trf_7347(C_word c,C_word *av){
C_word t0=av[6];
C_word t1=av[5];
C_word t2=av[4];
C_word t3=av[3];
C_word t4=av[2];
C_word t5=av[1];
C_word t6=av[0];
f_7347(t0,t1,t2,t3,t4,t5,t6);}

C_noret_decl(trf_7349)
static void C_ccall trf_7349(C_word c,C_word *av) C_noret;
static void C_ccall trf_7349(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7349(t0,t1,t2);}

C_noret_decl(trf_7750)
static void C_ccall trf_7750(C_word c,C_word *av) C_noret;
static void C_ccall trf_7750(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_7750(t0,t1,t2,t3);}

/* f8684 in chicken.time.posix#seconds->local-time in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f8684(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f8684,c,av);}
/* posix-common.scm:584: decode-seconds */
{C_proc tp=(C_proc)C_fast_retrieve_proc(lf[254]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=lf[254];
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=C_SCHEME_FALSE;
tp(4,av2);}}

/* f8688 in chicken.time.posix#seconds->utc-time in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f8688(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f8688,c,av);}
/* posix-common.scm:589: decode-seconds */
{C_proc tp=(C_proc)C_fast_retrieve_proc(lf[254]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=lf[254];
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=C_SCHEME_TRUE;
tp(4,av2);}}

/* f8730 in k7077 in chicken.process#process-run in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 in ... */
static void C_ccall f8730(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f8730,c,av);}
a=C_alloc(6);
t2=(C_truep(t1)?t1:lf[407]);
t3=C_a_i_list2(&a,2,lf[408],((C_word*)t0)[2]);
/* posixunix.scm:1154: chicken.process#process-execute */
t4=*((C_word*)lf[107]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[3];
av2[2]=t2;
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* f8736 in %process in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f8736(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f8736,c,av);}
t2=(C_truep(t1)?t1:lf[407]);
t3=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
f_7390(2,av2);}}

/* k2615 */
static void C_ccall f_2617(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2617,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2620,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_pathname_toplevel(2,av2);}}

/* k2618 in k2615 */
static void C_ccall f_2620(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2620,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2623,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_extras_toplevel(2,av2);}}

/* k2621 in k2618 in k2615 */
static void C_ccall f_2623(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2623,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2626,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_port_toplevel(2,av2);}}

/* k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_2626(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2626,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2629,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_lolevel_toplevel(2,av2);}}

/* k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_2629(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2629,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2632,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_library_toplevel(2,av2);}}

/* k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_2632(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word t24;
C_word t25;
C_word t26;
C_word t27;
C_word t28;
C_word t29;
C_word t30;
C_word t31;
C_word t32;
C_word t33;
C_word t34;
C_word t35;
C_word t36;
C_word t37;
C_word t38;
C_word t39;
C_word t40;
C_word t41;
C_word t42;
C_word t43;
C_word t44;
C_word t45;
C_word t46;
C_word t47;
C_word t48;
C_word t49;
C_word t50;
C_word t51;
C_word t52;
C_word t53;
C_word t54;
C_word t55;
C_word t56;
C_word t57;
C_word t58;
C_word t59;
C_word t60;
C_word t61;
C_word t62;
C_word t63;
C_word t64;
C_word t65;
C_word t66;
C_word t67;
C_word t68;
C_word t69;
C_word t70;
C_word t71;
C_word t72;
C_word t73;
C_word t74;
C_word t75;
C_word t76;
C_word t77;
C_word t78;
C_word t79;
C_word t80;
C_word t81;
C_word t82;
C_word t83;
C_word t84;
C_word t85;
C_word t86;
C_word t87;
C_word t88;
C_word t89;
C_word t90;
C_word t91;
C_word t92;
C_word t93;
C_word t94;
C_word t95;
C_word t96;
C_word t97;
C_word t98;
C_word t99;
C_word t100;
C_word t101;
C_word t102;
C_word t103;
C_word t104;
C_word t105;
C_word t106;
C_word t107;
C_word t108;
C_word t109;
C_word t110;
C_word t111;
C_word t112;
C_word t113;
C_word t114;
C_word t115;
C_word t116;
C_word t117;
C_word t118;
C_word t119;
C_word t120;
C_word t121;
C_word t122;
C_word t123;
C_word t124;
C_word t125;
C_word t126;
C_word t127;
C_word t128;
C_word t129;
C_word t130;
C_word t131;
C_word t132;
C_word t133;
C_word t134;
C_word t135;
C_word t136;
C_word t137;
C_word t138;
C_word t139;
C_word t140;
C_word t141;
C_word t142;
C_word t143;
C_word t144;
C_word t145;
C_word t146;
C_word t147;
C_word t148;
C_word t149;
C_word t150;
C_word t151;
C_word t152;
C_word t153;
C_word t154;
C_word t155;
C_word t156;
C_word t157;
C_word t158;
C_word t159;
C_word t160;
C_word t161;
C_word t162;
C_word t163;
C_word t164;
C_word t165;
C_word t166;
C_word t167;
C_word t168;
C_word t169;
C_word t170;
C_word t171;
C_word t172;
C_word t173;
C_word t174;
C_word t175;
C_word t176;
C_word t177;
C_word t178;
C_word t179;
C_word t180;
C_word t181;
C_word t182;
C_word t183;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(104,c,6)))){
C_save_and_reclaim((void *)f_2632,c,av);}
a=C_alloc(104);
t2=C_a_i_provide(&a,1,lf[0]);
t3=C_a_i_provide(&a,1,lf[1]);
t4=C_set_block_item(lf[2] /* chicken.file.posix#create-fifo */,0,C_SCHEME_UNDEFINED);
t5=C_set_block_item(lf[3] /* chicken.file.posix#create-symbolic-link */,0,C_SCHEME_UNDEFINED);
t6=C_set_block_item(lf[4] /* chicken.file.posix#read-symbolic-link */,0,C_SCHEME_UNDEFINED);
t7=C_set_block_item(lf[5] /* chicken.file.posix#duplicate-fileno */,0,C_SCHEME_UNDEFINED);
t8=C_set_block_item(lf[6] /* chicken.file.posix#fcntl/dupfd */,0,C_SCHEME_UNDEFINED);
t9=C_set_block_item(lf[7] /* chicken.file.posix#fcntl/getfd */,0,C_SCHEME_UNDEFINED);
t10=C_set_block_item(lf[8] /* chicken.file.posix#fcntl/getfl */,0,C_SCHEME_UNDEFINED);
t11=C_set_block_item(lf[9] /* chicken.file.posix#fcntl/setfd */,0,C_SCHEME_UNDEFINED);
t12=C_set_block_item(lf[10] /* chicken.file.posix#fcntl/setfl */,0,C_SCHEME_UNDEFINED);
t13=C_set_block_item(lf[11] /* chicken.file.posix#file-close */,0,C_SCHEME_UNDEFINED);
t14=C_set_block_item(lf[12] /* chicken.file.posix#file-control */,0,C_SCHEME_UNDEFINED);
t15=C_set_block_item(lf[13] /* chicken.file.posix#file-creation-mode */,0,C_SCHEME_UNDEFINED);
t16=C_set_block_item(lf[14] /* chicken.file.posix#file-group */,0,C_SCHEME_UNDEFINED);
t17=C_set_block_item(lf[15] /* chicken.file.posix#file-link */,0,C_SCHEME_UNDEFINED);
t18=C_set_block_item(lf[16] /* chicken.file.posix#file-lock */,0,C_SCHEME_UNDEFINED);
t19=C_set_block_item(lf[17] /* chicken.file.posix#file-lock/blocking */,0,C_SCHEME_UNDEFINED);
t20=C_set_block_item(lf[18] /* chicken.file.posix#file-mkstemp */,0,C_SCHEME_UNDEFINED);
t21=C_set_block_item(lf[19] /* chicken.file.posix#file-open */,0,C_SCHEME_UNDEFINED);
t22=C_set_block_item(lf[20] /* chicken.file.posix#file-owner */,0,C_SCHEME_UNDEFINED);
t23=C_set_block_item(lf[21] /* chicken.file.posix#file-permissions */,0,C_SCHEME_UNDEFINED);
t24=C_set_block_item(lf[22] /* chicken.file.posix#file-position */,0,C_SCHEME_UNDEFINED);
t25=C_set_block_item(lf[23] /* chicken.file.posix#file-read */,0,C_SCHEME_UNDEFINED);
t26=C_set_block_item(lf[24] /* chicken.file.posix#file-select */,0,C_SCHEME_UNDEFINED);
t27=C_set_block_item(lf[25] /* chicken.file.posix#file-test-lock */,0,C_SCHEME_UNDEFINED);
t28=C_set_block_item(lf[26] /* chicken.file.posix#file-truncate */,0,C_SCHEME_UNDEFINED);
t29=C_set_block_item(lf[27] /* chicken.file.posix#file-unlock */,0,C_SCHEME_UNDEFINED);
t30=C_set_block_item(lf[28] /* chicken.file.posix#file-write */,0,C_SCHEME_UNDEFINED);
t31=C_set_block_item(lf[29] /* chicken.file.posix#file-type */,0,C_SCHEME_UNDEFINED);
t32=C_set_block_item(lf[30] /* chicken.file.posix#block-device? */,0,C_SCHEME_UNDEFINED);
t33=C_set_block_item(lf[31] /* chicken.file.posix#character-device? */,0,C_SCHEME_UNDEFINED);
t34=C_set_block_item(lf[32] /* chicken.file.posix#directory? */,0,C_SCHEME_UNDEFINED);
t35=C_set_block_item(lf[33] /* chicken.file.posix#fifo? */,0,C_SCHEME_UNDEFINED);
t36=C_set_block_item(lf[34] /* chicken.file.posix#regular-file? */,0,C_SCHEME_UNDEFINED);
t37=C_set_block_item(lf[35] /* chicken.file.posix#socket? */,0,C_SCHEME_UNDEFINED);
t38=C_set_block_item(lf[36] /* chicken.file.posix#symbolic-link? */,0,C_SCHEME_UNDEFINED);
t39=C_set_block_item(lf[37] /* chicken.file.posix#fileno/stderr */,0,C_SCHEME_UNDEFINED);
t40=C_set_block_item(lf[38] /* chicken.file.posix#fileno/stdin */,0,C_SCHEME_UNDEFINED);
t41=C_set_block_item(lf[39] /* chicken.file.posix#fileno/stdout */,0,C_SCHEME_UNDEFINED);
t42=C_set_block_item(lf[40] /* chicken.file.posix#open-input-file* */,0,C_SCHEME_UNDEFINED);
t43=C_set_block_item(lf[41] /* chicken.file.posix#open-output-file* */,0,C_SCHEME_UNDEFINED);
t44=C_set_block_item(lf[42] /* chicken.file.posix#open/append */,0,C_SCHEME_UNDEFINED);
t45=C_set_block_item(lf[43] /* chicken.file.posix#open/binary */,0,C_SCHEME_UNDEFINED);
t46=C_set_block_item(lf[44] /* chicken.file.posix#open/creat */,0,C_SCHEME_UNDEFINED);
t47=C_set_block_item(lf[45] /* chicken.file.posix#open/excl */,0,C_SCHEME_UNDEFINED);
t48=C_set_block_item(lf[46] /* chicken.file.posix#open/fsync */,0,C_SCHEME_UNDEFINED);
t49=C_set_block_item(lf[47] /* chicken.file.posix#open/noctty */,0,C_SCHEME_UNDEFINED);
t50=C_set_block_item(lf[48] /* chicken.file.posix#open/noinherit */,0,C_SCHEME_UNDEFINED);
t51=C_set_block_item(lf[49] /* chicken.file.posix#open/nonblock */,0,C_SCHEME_UNDEFINED);
t52=C_set_block_item(lf[50] /* chicken.file.posix#open/rdonly */,0,C_SCHEME_UNDEFINED);
t53=C_set_block_item(lf[51] /* chicken.file.posix#open/rdwr */,0,C_SCHEME_UNDEFINED);
t54=C_set_block_item(lf[52] /* chicken.file.posix#open/read */,0,C_SCHEME_UNDEFINED);
t55=C_set_block_item(lf[53] /* chicken.file.posix#open/sync */,0,C_SCHEME_UNDEFINED);
t56=C_set_block_item(lf[54] /* chicken.file.posix#open/text */,0,C_SCHEME_UNDEFINED);
t57=C_set_block_item(lf[55] /* chicken.file.posix#open/trunc */,0,C_SCHEME_UNDEFINED);
t58=C_set_block_item(lf[56] /* chicken.file.posix#open/write */,0,C_SCHEME_UNDEFINED);
t59=C_set_block_item(lf[57] /* chicken.file.posix#open/wronly */,0,C_SCHEME_UNDEFINED);
t60=C_set_block_item(lf[58] /* chicken.file.posix#perm/irgrp */,0,C_SCHEME_UNDEFINED);
t61=C_set_block_item(lf[59] /* chicken.file.posix#perm/iroth */,0,C_SCHEME_UNDEFINED);
t62=C_set_block_item(lf[60] /* chicken.file.posix#perm/irusr */,0,C_SCHEME_UNDEFINED);
t63=C_set_block_item(lf[61] /* chicken.file.posix#perm/irwxg */,0,C_SCHEME_UNDEFINED);
t64=C_set_block_item(lf[62] /* chicken.file.posix#perm/irwxo */,0,C_SCHEME_UNDEFINED);
t65=C_set_block_item(lf[63] /* chicken.file.posix#perm/irwxu */,0,C_SCHEME_UNDEFINED);
t66=C_set_block_item(lf[64] /* chicken.file.posix#perm/isgid */,0,C_SCHEME_UNDEFINED);
t67=C_set_block_item(lf[65] /* chicken.file.posix#perm/isuid */,0,C_SCHEME_UNDEFINED);
t68=C_set_block_item(lf[66] /* chicken.file.posix#perm/isvtx */,0,C_SCHEME_UNDEFINED);
t69=C_set_block_item(lf[67] /* chicken.file.posix#perm/iwgrp */,0,C_SCHEME_UNDEFINED);
t70=C_set_block_item(lf[68] /* chicken.file.posix#perm/iwoth */,0,C_SCHEME_UNDEFINED);
t71=C_set_block_item(lf[69] /* chicken.file.posix#perm/iwusr */,0,C_SCHEME_UNDEFINED);
t72=C_set_block_item(lf[70] /* chicken.file.posix#perm/ixgrp */,0,C_SCHEME_UNDEFINED);
t73=C_set_block_item(lf[71] /* chicken.file.posix#perm/ixoth */,0,C_SCHEME_UNDEFINED);
t74=C_set_block_item(lf[72] /* chicken.file.posix#perm/ixusr */,0,C_SCHEME_UNDEFINED);
t75=C_set_block_item(lf[73] /* chicken.file.posix#port->fileno */,0,C_SCHEME_UNDEFINED);
t76=C_set_block_item(lf[74] /* chicken.file.posix#seek/cur */,0,C_SCHEME_UNDEFINED);
t77=C_set_block_item(lf[75] /* chicken.file.posix#seek/end */,0,C_SCHEME_UNDEFINED);
t78=C_set_block_item(lf[76] /* chicken.file.posix#seek/set */,0,C_SCHEME_UNDEFINED);
t79=C_set_block_item(lf[77] /* chicken.file.posix#set-file-position! */,0,C_SCHEME_UNDEFINED);
t80=C_a_i_provide(&a,1,lf[78]);
t81=C_set_block_item(lf[79] /* chicken.time.posix#seconds->utc-time */,0,C_SCHEME_UNDEFINED);
t82=C_set_block_item(lf[80] /* chicken.time.posix#utc-time->seconds */,0,C_SCHEME_UNDEFINED);
t83=C_set_block_item(lf[81] /* chicken.time.posix#seconds->local-time */,0,C_SCHEME_UNDEFINED);
t84=C_set_block_item(lf[82] /* chicken.time.posix#seconds->string */,0,C_SCHEME_UNDEFINED);
t85=C_set_block_item(lf[83] /* chicken.time.posix#local-time->seconds */,0,C_SCHEME_UNDEFINED);
t86=C_set_block_item(lf[84] /* chicken.time.posix#string->time */,0,C_SCHEME_UNDEFINED);
t87=C_set_block_item(lf[85] /* chicken.time.posix#time->string */,0,C_SCHEME_UNDEFINED);
t88=C_set_block_item(lf[86] /* chicken.time.posix#local-timezone-abbreviation */,0,C_SCHEME_UNDEFINED);
t89=C_a_i_provide(&a,1,lf[87]);
t90=C_mutate((C_word*)lf[88]+1 /* (set! chicken.process#system ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2718,a[2]=((C_word)li0),tmp=(C_word)a,a+=3,tmp));
t91=C_mutate((C_word*)lf[94]+1 /* (set! chicken.process#system* ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2736,a[2]=((C_word)li1),tmp=(C_word)a,a+=3,tmp));
t92=C_mutate((C_word*)lf[97]+1 /* (set! chicken.process#qs ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2752,a[2]=((C_word)li4),tmp=(C_word)a,a+=3,tmp));
t93=C_set_block_item(lf[107] /* chicken.process#process-execute */,0,C_SCHEME_UNDEFINED);
t94=C_set_block_item(lf[108] /* chicken.process#process-fork */,0,C_SCHEME_UNDEFINED);
t95=C_set_block_item(lf[109] /* chicken.process#process-run */,0,C_SCHEME_UNDEFINED);
t96=C_set_block_item(lf[110] /* chicken.process#process-signal */,0,C_SCHEME_UNDEFINED);
t97=C_set_block_item(lf[111] /* chicken.process#process-spawn */,0,C_SCHEME_UNDEFINED);
t98=C_set_block_item(lf[112] /* chicken.process#process-wait */,0,C_SCHEME_UNDEFINED);
t99=C_set_block_item(lf[113] /* chicken.process#call-with-input-pipe */,0,C_SCHEME_UNDEFINED);
t100=C_set_block_item(lf[114] /* chicken.process#call-with-output-pipe */,0,C_SCHEME_UNDEFINED);
t101=C_set_block_item(lf[115] /* chicken.process#close-input-pipe */,0,C_SCHEME_UNDEFINED);
t102=C_set_block_item(lf[116] /* chicken.process#close-output-pipe */,0,C_SCHEME_UNDEFINED);
t103=C_set_block_item(lf[117] /* chicken.process#create-pipe */,0,C_SCHEME_UNDEFINED);
t104=C_set_block_item(lf[118] /* chicken.process#open-input-pipe */,0,C_SCHEME_UNDEFINED);
t105=C_set_block_item(lf[119] /* chicken.process#open-output-pipe */,0,C_SCHEME_UNDEFINED);
t106=C_set_block_item(lf[120] /* chicken.process#with-input-from-pipe */,0,C_SCHEME_UNDEFINED);
t107=C_set_block_item(lf[121] /* chicken.process#with-output-to-pipe */,0,C_SCHEME_UNDEFINED);
t108=C_set_block_item(lf[122] /* chicken.process#process */,0,C_SCHEME_UNDEFINED);
t109=C_set_block_item(lf[123] /* chicken.process#process* */,0,C_SCHEME_UNDEFINED);
t110=C_set_block_item(lf[124] /* chicken.process#process-sleep */,0,C_SCHEME_UNDEFINED);
t111=C_set_block_item(lf[125] /* chicken.process#pipe/buf */,0,C_SCHEME_UNDEFINED);
t112=C_set_block_item(lf[126] /* chicken.process#spawn/overlay */,0,C_SCHEME_UNDEFINED);
t113=C_set_block_item(lf[127] /* chicken.process#spawn/wait */,0,C_SCHEME_UNDEFINED);
t114=C_set_block_item(lf[128] /* chicken.process#spawn/nowait */,0,C_SCHEME_UNDEFINED);
t115=C_set_block_item(lf[129] /* chicken.process#spawn/nowaito */,0,C_SCHEME_UNDEFINED);
t116=C_set_block_item(lf[130] /* chicken.process#spawn/detach */,0,C_SCHEME_UNDEFINED);
t117=C_a_i_provide(&a,1,lf[131]);
t118=C_set_block_item(lf[132] /* chicken.process.signal#set-alarm! */,0,C_SCHEME_UNDEFINED);
t119=C_set_block_item(lf[133] /* chicken.process.signal#set-signal-handler! */,0,C_SCHEME_UNDEFINED);
t120=C_set_block_item(lf[134] /* chicken.process.signal#set-signal-mask! */,0,C_SCHEME_UNDEFINED);
t121=C_set_block_item(lf[135] /* chicken.process.signal#signal-handler */,0,C_SCHEME_UNDEFINED);
t122=C_set_block_item(lf[136] /* chicken.process.signal#signal-mask */,0,C_SCHEME_UNDEFINED);
t123=C_set_block_item(lf[137] /* chicken.process.signal#signal-mask! */,0,C_SCHEME_UNDEFINED);
t124=C_set_block_item(lf[138] /* chicken.process.signal#signal-masked? */,0,C_SCHEME_UNDEFINED);
t125=C_set_block_item(lf[139] /* chicken.process.signal#signal-unmask! */,0,C_SCHEME_UNDEFINED);
t126=C_set_block_item(lf[140] /* chicken.process.signal#signal/abrt */,0,C_SCHEME_UNDEFINED);
t127=C_set_block_item(lf[141] /* chicken.process.signal#signal/alrm */,0,C_SCHEME_UNDEFINED);
t128=C_set_block_item(lf[142] /* chicken.process.signal#signal/break */,0,C_SCHEME_UNDEFINED);
t129=C_set_block_item(lf[143] /* chicken.process.signal#signal/bus */,0,C_SCHEME_UNDEFINED);
t130=C_set_block_item(lf[144] /* chicken.process.signal#signal/chld */,0,C_SCHEME_UNDEFINED);
t131=C_set_block_item(lf[145] /* chicken.process.signal#signal/cont */,0,C_SCHEME_UNDEFINED);
t132=C_set_block_item(lf[146] /* chicken.process.signal#signal/fpe */,0,C_SCHEME_UNDEFINED);
t133=C_set_block_item(lf[147] /* chicken.process.signal#signal/hup */,0,C_SCHEME_UNDEFINED);
t134=C_set_block_item(lf[148] /* chicken.process.signal#signal/ill */,0,C_SCHEME_UNDEFINED);
t135=C_set_block_item(lf[149] /* chicken.process.signal#signal/int */,0,C_SCHEME_UNDEFINED);
t136=C_set_block_item(lf[150] /* chicken.process.signal#signal/io */,0,C_SCHEME_UNDEFINED);
t137=C_set_block_item(lf[151] /* chicken.process.signal#signal/kill */,0,C_SCHEME_UNDEFINED);
t138=C_set_block_item(lf[152] /* chicken.process.signal#signal/pipe */,0,C_SCHEME_UNDEFINED);
t139=C_set_block_item(lf[153] /* chicken.process.signal#signal/prof */,0,C_SCHEME_UNDEFINED);
t140=C_set_block_item(lf[154] /* chicken.process.signal#signal/quit */,0,C_SCHEME_UNDEFINED);
t141=C_set_block_item(lf[155] /* chicken.process.signal#signal/segv */,0,C_SCHEME_UNDEFINED);
t142=C_set_block_item(lf[156] /* chicken.process.signal#signal/stop */,0,C_SCHEME_UNDEFINED);
t143=C_set_block_item(lf[157] /* chicken.process.signal#signal/term */,0,C_SCHEME_UNDEFINED);
t144=C_set_block_item(lf[158] /* chicken.process.signal#signal/trap */,0,C_SCHEME_UNDEFINED);
t145=C_set_block_item(lf[159] /* chicken.process.signal#signal/tstp */,0,C_SCHEME_UNDEFINED);
t146=C_set_block_item(lf[160] /* chicken.process.signal#signal/urg */,0,C_SCHEME_UNDEFINED);
t147=C_set_block_item(lf[161] /* chicken.process.signal#signal/usr1 */,0,C_SCHEME_UNDEFINED);
t148=C_set_block_item(lf[162] /* chicken.process.signal#signal/usr2 */,0,C_SCHEME_UNDEFINED);
t149=C_set_block_item(lf[163] /* chicken.process.signal#signal/vtalrm */,0,C_SCHEME_UNDEFINED);
t150=C_set_block_item(lf[164] /* chicken.process.signal#signal/winch */,0,C_SCHEME_UNDEFINED);
t151=C_set_block_item(lf[165] /* chicken.process.signal#signal/xcpu */,0,C_SCHEME_UNDEFINED);
t152=C_set_block_item(lf[166] /* chicken.process.signal#signal/xfsz */,0,C_SCHEME_UNDEFINED);
t153=C_set_block_item(lf[167] /* chicken.process.signal#signals-list */,0,C_SCHEME_UNDEFINED);
t154=C_a_i_provide(&a,1,lf[168]);
t155=C_set_block_item(lf[169] /* chicken.process-context.posix#change-directory* */,0,C_SCHEME_UNDEFINED);
t156=C_set_block_item(lf[170] /* chicken.process-context.posix#set-root-directory! */,0,C_SCHEME_UNDEFINED);
t157=C_set_block_item(lf[171] /* chicken.process-context.posix#current-effective-group-id */,0,C_SCHEME_UNDEFINED);
t158=C_set_block_item(lf[172] /* chicken.process-context.posix#current-effective-user-id */,0,C_SCHEME_UNDEFINED);
t159=C_set_block_item(lf[173] /* chicken.process-context.posix#current-group-id */,0,C_SCHEME_UNDEFINED);
t160=C_set_block_item(lf[174] /* chicken.process-context.posix#current-user-id */,0,C_SCHEME_UNDEFINED);
t161=C_set_block_item(lf[175] /* chicken.process-context.posix#current-process-id */,0,C_SCHEME_UNDEFINED);
t162=C_set_block_item(lf[176] /* chicken.process-context.posix#parent-process-id */,0,C_SCHEME_UNDEFINED);
t163=C_set_block_item(lf[177] /* chicken.process-context.posix#current-user-name */,0,C_SCHEME_UNDEFINED);
t164=C_set_block_item(lf[178] /* chicken.process-context.posix#current-effective-user-name */,0,C_SCHEME_UNDEFINED);
t165=C_set_block_item(lf[179] /* chicken.process-context.posix#create-session */,0,C_SCHEME_UNDEFINED);
t166=C_set_block_item(lf[180] /* chicken.process-context.posix#process-group-id */,0,C_SCHEME_UNDEFINED);
t167=C_set_block_item(lf[181] /* chicken.process-context.posix#user-information */,0,C_SCHEME_UNDEFINED);
t168=C_a_i_provide(&a,1,lf[182]);
t169=C_mutate(&lf[183] /* (set! chicken.posix#posix-error ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2939,a[2]=((C_word)li5),tmp=(C_word)a,a+=3,tmp));
t170=C_mutate((C_word*)lf[186]+1 /* (set! ##sys#posix-error ...) */,lf[183]);
t171=C_mutate(&lf[187] /* (set! chicken.posix#stat ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2957,a[2]=((C_word)li6),tmp=(C_word)a,a+=3,tmp));
t172=C_mutate((C_word*)lf[194]+1 /* (set! chicken.file.posix#file-stat ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3003,a[2]=((C_word)li7),tmp=(C_word)a,a+=3,tmp));
t173=C_mutate((C_word*)lf[196]+1 /* (set! chicken.file.posix#set-file-permissions! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3021,a[2]=((C_word)li8),tmp=(C_word)a,a+=3,tmp));
t174=C_mutate((C_word*)lf[202]+1 /* (set! chicken.file.posix#file-modification-time ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3065,a[2]=((C_word)li9),tmp=(C_word)a,a+=3,tmp));
t175=C_mutate((C_word*)lf[204]+1 /* (set! chicken.file.posix#file-access-time ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3071,a[2]=((C_word)li10),tmp=(C_word)a,a+=3,tmp));
t176=C_mutate((C_word*)lf[206]+1 /* (set! chicken.file.posix#file-change-time ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3077,a[2]=((C_word)li11),tmp=(C_word)a,a+=3,tmp));
t177=C_mutate((C_word*)lf[208]+1 /* (set! chicken.file.posix#set-file-times! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3083,a[2]=((C_word)li12),tmp=(C_word)a,a+=3,tmp));
t178=C_mutate((C_word*)lf[213]+1 /* (set! chicken.file.posix#file-size ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3162,a[2]=((C_word)li13),tmp=(C_word)a,a+=3,tmp));
t179=C_mutate((C_word*)lf[215]+1 /* (set! chicken.file.posix#set-file-owner! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3168,a[2]=((C_word)li14),tmp=(C_word)a,a+=3,tmp));
t180=C_mutate((C_word*)lf[218]+1 /* (set! chicken.file.posix#set-file-group! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3174,a[2]=((C_word)li15),tmp=(C_word)a,a+=3,tmp));
t181=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3182,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t182=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7863,a[2]=((C_word)li218),tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:312: chicken.base#getter-with-setter */
t183=*((C_word*)lf[464]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t183;
av2[1]=t181;
av2[2]=t182;
av2[3]=*((C_word*)lf[215]+1);
av2[4]=lf[491];
((C_proc)(void*)(*((C_word*)t183+1)))(5,av2);}}

/* chicken.process#system in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_2718(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_2718,c,av);}
a=C_alloc(4);
t3=C_i_check_string_2(t2,lf[89]);
t4=C_execute_shell_command(t2);
if(C_truep(C_fixnum_lessp(t4,C_fix(0)))){
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2731,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* posix.scm:202: ##sys#update-errno */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[93]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[93]+1);
av2[1]=t5;
tp(2,av2);}}
else{
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* k2729 in chicken.process#system in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_2731(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_2731,c,av);}
/* posix.scm:203: ##sys#signal-hook */
t2=*((C_word*)lf[90]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[91];
av2[3]=lf[89];
av2[4]=lf[92];
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* chicken.process#system* in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_2736(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_2736,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2740,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* posix.scm:209: system */
t4=*((C_word*)lf[88]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k2738 in chicken.process#system* in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_2740(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_2740,c,av);}
t2=C_eqp(t1,C_fix(0));
if(C_truep(t2)){
t3=C_SCHEME_UNDEFINED;
t4=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
/* posix.scm:211: ##sys#error */
t3=*((C_word*)lf[95]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[96];
av2[3]=((C_word*)t0)[3];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}}

/* chicken.process#qs in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_2752(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_2752,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2756,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_rest_nullp(c,3))){
/* posix.scm:216: chicken.platform#software-version */
t4=*((C_word*)lf[106]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_get_rest_arg(c,3,av,3,t0);
f_2756(2,av2);}}}

/* k2754 in chicken.process#qs in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_2756(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,3)))){
C_save_and_reclaim((void *)f_2756,c,av);}
a=C_alloc(18);
t2=C_eqp(t1,lf[98]);
t3=(C_truep(t2)?C_make_character(34):C_make_character(39));
t4=C_eqp(t1,lf[98]);
t5=(C_truep(t4)?lf[99]:lf[100]);
t6=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t7=t6;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=((C_word*)t8)[1];
t10=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2766,a[2]=t3,a[3]=t5,a[4]=((C_word*)t0)[2],a[5]=((C_word)li2),tmp=(C_word)a,a+=6,tmp);
t11=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_2787,a[2]=t3,a[3]=((C_word*)t0)[3],a[4]=t8,a[5]=t10,a[6]=t9,tmp=(C_word)a,a+=7,tmp);
/* ##sys#string->list */
t12=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t12;
av2[1]=t11;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t12+1)))(3,av2);}}

/* g256 in k2754 in chicken.process#qs in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_2766(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(2,0,4)))){
C_save_and_reclaim_args((void *)trf_2766,3,t0,t1,t2);}
a=C_alloc(2);
if(C_truep(C_i_char_equalp(t2,((C_word*)t0)[2]))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
if(C_truep(C_u_i_char_equalp(t2,C_make_character(0)))){
/* posix.scm:224: chicken.base#error */
t3=*((C_word*)lf[101]+1);{
C_word av2[5];
av2[0]=t3;
av2[1]=t1;
av2[2]=lf[102];
av2[3]=lf[103];
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_a_i_string(&a,1,t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}}

/* k2785 in k2754 in chicken.process#qs in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_2787(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,3)))){
C_save_and_reclaim((void *)f_2787,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2790,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_2807,a[2]=((C_word*)t0)[4],a[3]=t4,a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word)li3),tmp=(C_word)a,a+=7,tmp));
t6=((C_word*)t4)[1];
f_2807(t6,t2,t1);}

/* k2788 in k2785 in k2754 in chicken.process#qs in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_2790(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_2790,c,av);}
a=C_alloc(7);
t2=C_a_i_string(&a,1,((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2801,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t2,tmp=(C_word)a,a+=5,tmp);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=t3;
av2[2]=*((C_word*)lf[104]+1);
av2[3]=t1;
C_apply(4,av2);}}

/* k2799 in k2788 in k2785 in k2754 in chicken.process#qs in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_2801(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(2,c,4)))){
C_save_and_reclaim((void *)f_2801,c,av);}
a=C_alloc(2);
t2=C_a_i_string(&a,1,((C_word*)t0)[2]);
/* posix.scm:227: scheme#string-append */
t3=*((C_word*)lf[104]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=t1;
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* map-loop250 in k2785 in k2754 in chicken.process#qs in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_2807(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_2807,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2832,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* posix.scm:220: g256 */
t4=((C_word*)t0)[4];
f_2766(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k2830 in map-loop250 in k2785 in k2754 in chicken.process#qs in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_2832(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2832,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_2807(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* chicken.posix#posix-error in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_2939(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c<5) C_bad_min_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-5)*C_SIZEOF_PAIR +7,c,2)))){
C_save_and_reclaim((void*)f_2939,c,av);}
a=C_alloc((c-5)*C_SIZEOF_PAIR+7);
t5=C_build_rest(&a,c,5,av);
C_word t6;
C_word t7;
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_2943,a[2]=t1,a[3]=t2,a[4]=t3,a[5]=t5,a[6]=t4,tmp=(C_word)a,a+=7,tmp);
/* posix-common.scm:191: ##sys#update-errno */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[93]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[93]+1);
av2[1]=t6;
tp(2,av2);}}

/* k2941 in chicken.posix#posix-error in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_2943(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,3)))){
C_save_and_reclaim((void *)f_2943,c,av);}
a=C_alloc(15);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2950,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2954,a[2]=t2,a[3]=((C_word*)t0)[6],tmp=(C_word)a,a+=4,tmp);
t4=C_a_i_bytevector(&a,1,C_fix(3));
t5=C_i_foreign_fixnum_argumentp(t1);
/* posix-common.scm:188: ##sys#peek-c-string */
t6=*((C_word*)lf[185]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t3;
av2[2]=stub633(t4,t5);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* k2948 in k2941 in chicken.posix#posix-error in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_2950(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_2950,c,av);}{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=*((C_word*)lf[90]+1);
av2[3]=((C_word*)t0)[3];
av2[4]=((C_word*)t0)[4];
av2[5]=t1;
av2[6]=((C_word*)t0)[5];
C_apply(7,av2);}}

/* k2952 in k2941 in chicken.posix#posix-error in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_2954(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_2954,c,av);}
/* posix-common.scm:192: string-append */
t2=*((C_word*)lf[104]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=lf[184];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* chicken.posix#stat in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_2957(C_word t1,C_word t2,C_word t3,C_word t4,C_word t5){
C_word tmp;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,2)))){
C_save_and_reclaim_args((void *)trf_2957,5,t1,t2,t3,t4,t5);}
a=C_alloc(12);
t6=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2961,a[2]=t4,a[3]=t1,a[4]=t5,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_fixnump(t2))){
t7=t6;{
C_word av2[2];
av2[0]=t7;
av2[1]=C_u_i_fstat(t2);
f_2961(2,av2);}}
else{
t7=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2982,a[2]=t6,a[3]=t2,a[4]=t3,a[5]=t5,tmp=(C_word)a,a+=6,tmp);
/* posix-common.scm:235: chicken.base#port? */
t8=*((C_word*)lf[193]+1);{
C_word av2[3];
av2[0]=t8;
av2[1]=t7;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t8+1)))(3,av2);}}}

/* k2959 in chicken.posix#stat in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_2961(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_2961,c,av);}
if(C_truep(C_fixnum_lessp(t1,C_fix(0)))){
if(C_truep(((C_word*)t0)[2])){
/* posix-common.scm:246: posix-error */
t2=lf[183];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[188];
av2[3]=((C_word*)t0)[4];
av2[4]=lf[189];
av2[5]=((C_word*)t0)[5];
f_2939(6,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k2980 in chicken.posix#stat in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_2982(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,5)))){
C_save_and_reclaim((void *)f_2982,c,av);}
a=C_alloc(4);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2986,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:235: chicken.file.posix#port->fileno */
t3=*((C_word*)lf[73]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
if(C_truep(C_i_stringp(((C_word*)t0)[3]))){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2995,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:237: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[190]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[190]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[5];
tp(4,av2);}}
else{
/* posix-common.scm:242: ##sys#signal-hook */
t2=*((C_word*)lf[90]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[191];
av2[3]=((C_word*)t0)[5];
av2[4]=lf[192];
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}}}

/* k2984 in k2980 in chicken.posix#stat in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_2986(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2986,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_u_i_fstat(t1);
f_2961(2,av2);}}

/* k2993 in k2980 in chicken.posix#stat in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_2995(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2995,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(C_truep(((C_word*)t0)[3])?C_u_i_lstat(t1):C_u_i_stat(t1));
f_2961(2,av2);}}

/* chicken.file.posix#file-stat in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3003(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_3003,c,av);}
a=C_alloc(3);
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,3,t0));
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3010,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:252: stat */
f_2957(t5,t2,t4,C_SCHEME_TRUE,lf[195]);}

/* k3008 in chicken.file.posix#file-stat in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3010(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(42,c,1)))){
C_save_and_reclaim((void *)f_3010,c,av);}
a=C_alloc(42);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_vector(&a,13,C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)C_statbuf.st_ino),C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)C_statbuf.st_mode),C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)C_statbuf.st_nlink),C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)C_statbuf.st_uid),C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)C_statbuf.st_gid),C_int64_to_num(&a,C_statbuf.st_size),C_int64_to_num(&a,C_statbuf.st_atime),C_int64_to_num(&a,C_statbuf.st_ctime),C_int64_to_num(&a,C_statbuf.st_mtime),C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)C_statbuf.st_dev),C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)C_statbuf.st_rdev),C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)C_statbuf.st_blksize),C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)C_statbuf.st_blocks));
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.file.posix#set-file-permissions! in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3021(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_3021,c,av);}
a=C_alloc(5);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3025,a[2]=t1,a[3]=t2,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:261: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t4;
av2[2]=t3;
av2[3]=lf[197];
tp(4,av2);}}

/* k3023 in chicken.file.posix#set-file-permissions! in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3025(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_3025,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3028,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_fixnump(((C_word*)t0)[3]))){
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_fchmod(((C_word*)t0)[3],((C_word*)t0)[4]);
f_3028(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3046,a[2]=t2,a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:263: chicken.base#port? */
t4=*((C_word*)lf[193]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}}

/* k3026 in k3023 in chicken.file.posix#set-file-permissions! in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3028(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_3028,c,av);}
if(C_truep(C_fixnum_lessp(t1,C_fix(0)))){
/* posix-common.scm:272: posix-error */
t2=lf[183];{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[188];
av2[3]=lf[197];
av2[4]=lf[198];
av2[5]=((C_word*)t0)[3];
av2[6]=((C_word*)t0)[4];
f_2939(7,av2);}}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k3044 in k3023 in chicken.file.posix#set-file-permissions! in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3046(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,5)))){
C_save_and_reclaim((void *)f_3046,c,av);}
a=C_alloc(4);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3050,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:263: chicken.file.posix#port->fileno */
t3=*((C_word*)lf[73]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
if(C_truep(C_i_stringp(((C_word*)t0)[4]))){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3060,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:266: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[190]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[190]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=lf[197];
tp(4,av2);}}
else{
/* posix-common.scm:268: ##sys#signal-hook */
t2=*((C_word*)lf[90]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[191];
av2[3]=lf[199];
av2[4]=lf[200];
av2[5]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}}}

/* k3048 in k3044 in k3023 in chicken.file.posix#set-file-permissions! in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3050(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3050,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_fchmod(t1,((C_word*)t0)[3]);
f_3028(2,av2);}}

/* k3058 in k3044 in k3023 in chicken.file.posix#set-file-permissions! in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3060(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3060,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_chmod(t1,((C_word*)t0)[3]);
f_3028(2,av2);}}

/* chicken.file.posix#file-modification-time in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3065(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_3065,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3069,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:276: stat */
f_2957(t3,t2,C_SCHEME_FALSE,C_SCHEME_TRUE,lf[203]);}

/* k3067 in chicken.file.posix#file-modification-time in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3069(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,1)))){
C_save_and_reclaim((void *)f_3069,c,av);}
a=C_alloc(7);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_int64_to_num(&a,C_statbuf.st_mtime);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.file.posix#file-access-time in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3071(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_3071,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3075,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:280: stat */
f_2957(t3,t2,C_SCHEME_FALSE,C_SCHEME_TRUE,lf[205]);}

/* k3073 in chicken.file.posix#file-access-time in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3075(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,1)))){
C_save_and_reclaim((void *)f_3075,c,av);}
a=C_alloc(7);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_int64_to_num(&a,C_statbuf.st_atime);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.file.posix#file-change-time in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3077(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_3077,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3081,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:284: stat */
f_2957(t3,t2,C_SCHEME_FALSE,C_SCHEME_TRUE,lf[207]);}

/* k3079 in chicken.file.posix#file-change-time in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3081(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,1)))){
C_save_and_reclaim((void *)f_3081,c,av);}
a=C_alloc(7);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_int64_to_num(&a,C_statbuf.st_ctime);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.file.posix#set-file-times! in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3083(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +5,c,2)))){
C_save_and_reclaim((void*)f_3083,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+5);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3087,a[2]=t3,a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_i_nullp(t3))){
/* posix-common.scm:289: chicken.time#current-seconds */
t5=*((C_word*)lf[212]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t5=t4;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_i_car(t3);
f_3087(2,av2);}}}

/* k3085 in chicken.file.posix#set-file-times! in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3087(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_3087,c,av);}
a=C_alloc(7);
t2=C_i_nullp(((C_word*)t0)[2]);
t3=(C_truep(t2)?C_SCHEME_END_OF_LIST:C_i_cdr(((C_word*)t0)[2]));
t4=C_i_nullp(t3);
t5=(C_truep(t4)?t1:C_i_car(t3));
t6=C_i_nullp(t3);
t7=(C_truep(t6)?C_SCHEME_END_OF_LIST:C_i_cdr(t3));
t8=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3099,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[2],a[5]=t1,a[6]=t5,tmp=(C_word)a,a+=7,tmp);
if(C_truep(t1)){
/* posix-common.scm:290: ##sys#check-exact-integer */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[211]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[211]+1);
av2[1]=t8;
av2[2]=t1;
av2[3]=lf[209];
tp(4,av2);}}
else{
t9=t8;{
C_word *av2=av;
av2[0]=t9;
av2[1]=C_SCHEME_UNDEFINED;
f_3099(2,av2);}}}

/* k3097 in k3085 in chicken.file.posix#set-file-times! in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3099(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_3099,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3102,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
if(C_truep(((C_word*)t0)[6])){
/* posix-common.scm:291: ##sys#check-exact-integer */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[211]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[211]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[6];
av2[3]=lf[209];
tp(4,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_3102(2,av2);}}}

/* k3100 in k3097 in k3085 in chicken.file.posix#set-file-times! in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3102(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_3102,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3118,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3108,a[2]=t2,a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],tmp=(C_word)a,a+=5,tmp);
if(C_truep(((C_word*)t0)[3])){
/* posix-common.scm:292: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[190]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[190]+1);
av2[1]=t3;
av2[2]=C_i_foreign_string_argumentp(((C_word*)t0)[3]);
tp(3,av2);}}
else{
t4=t2;
f_3118(t4,stub707(C_SCHEME_UNDEFINED,C_SCHEME_FALSE,((C_word*)t0)[5],((C_word*)t0)[6]));}}

/* k3106 in k3100 in k3097 in k3085 in chicken.file.posix#set-file-times! in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3108(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3108,c,av);}
t2=((C_word*)t0)[2];
f_3118(t2,stub707(C_SCHEME_UNDEFINED,t1,((C_word*)t0)[3],((C_word*)t0)[4]));}

/* k3116 in k3100 in k3097 in k3085 in chicken.file.posix#set-file-times! in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_3118(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,7)))){
C_save_and_reclaim_args((void *)trf_3118,2,t0,t1);}
if(C_truep(C_fixnum_lessp(t1,C_fix(0)))){{
C_word av2[8];
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[183];
av2[3]=lf[188];
av2[4]=lf[209];
av2[5]=lf[210];
av2[6]=((C_word*)t0)[3];
av2[7]=((C_word*)t0)[4];
C_apply(8,av2);}}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* chicken.file.posix#file-size in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3162(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_3162,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3166,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:301: stat */
f_2957(t3,t2,C_SCHEME_FALSE,C_SCHEME_TRUE,lf[214]);}

/* k3164 in chicken.file.posix#file-size in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3166(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,1)))){
C_save_and_reclaim((void *)f_3166,c,av);}
a=C_alloc(7);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_int64_to_num(&a,C_statbuf.st_size);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.file.posix#set-file-owner! in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3168(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_3168,c,av);}
/* posix-common.scm:305: chown */
f_5413(t1,lf[217],t2,t3,C_fix(-1));}

/* chicken.file.posix#set-file-group! in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3174(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_3174,c,av);}
/* posix-common.scm:309: chown */
f_5413(t1,lf[219],t2,C_fix(-1),t3);}

/* k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3182(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_3182,c,av);}
a=C_alloc(6);
t2=C_mutate((C_word*)lf[20]+1 /* (set! chicken.file.posix#file-owner ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3186,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7857,a[2]=((C_word)li217),tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:318: chicken.base#getter-with-setter */
t5=*((C_word*)lf[464]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
av2[3]=*((C_word*)lf[218]+1);
av2[4]=lf[489];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}

/* k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3186(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_3186,c,av);}
a=C_alloc(6);
t2=C_mutate((C_word*)lf[14]+1 /* (set! chicken.file.posix#file-group ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3190,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7851,a[2]=((C_word)li216),tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:324: chicken.base#getter-with-setter */
t5=*((C_word*)lf[464]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
av2[3]=*((C_word*)lf[196]+1);
av2[4]=lf[487];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}

/* k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3190(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(33,c,4)))){
C_save_and_reclaim((void *)f_3190,c,av);}
a=C_alloc(33);
t2=C_mutate((C_word*)lf[21]+1 /* (set! chicken.file.posix#file-permissions ...) */,t1);
t3=C_mutate((C_word*)lf[29]+1 /* (set! chicken.file.posix#file-type ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3192,a[2]=((C_word)li16),tmp=(C_word)a,a+=3,tmp));
t4=C_mutate((C_word*)lf[34]+1 /* (set! chicken.file.posix#regular-file? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3279,a[2]=((C_word)li17),tmp=(C_word)a,a+=3,tmp));
t5=C_mutate((C_word*)lf[36]+1 /* (set! chicken.file.posix#symbolic-link? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3289,a[2]=((C_word)li18),tmp=(C_word)a,a+=3,tmp));
t6=C_mutate((C_word*)lf[30]+1 /* (set! chicken.file.posix#block-device? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3299,a[2]=((C_word)li19),tmp=(C_word)a,a+=3,tmp));
t7=C_mutate((C_word*)lf[31]+1 /* (set! chicken.file.posix#character-device? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3309,a[2]=((C_word)li20),tmp=(C_word)a,a+=3,tmp));
t8=C_mutate((C_word*)lf[33]+1 /* (set! chicken.file.posix#fifo? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3319,a[2]=((C_word)li21),tmp=(C_word)a,a+=3,tmp));
t9=C_mutate((C_word*)lf[35]+1 /* (set! chicken.file.posix#socket? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3329,a[2]=((C_word)li22),tmp=(C_word)a,a+=3,tmp));
t10=C_mutate((C_word*)lf[32]+1 /* (set! chicken.file.posix#directory? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3339,a[2]=((C_word)li23),tmp=(C_word)a,a+=3,tmp));
t11=C_set_block_item(lf[76] /* chicken.file.posix#seek/set */,0,C_fix((C_word)SEEK_SET));
t12=C_set_block_item(lf[75] /* chicken.file.posix#seek/end */,0,C_fix((C_word)SEEK_END));
t13=C_set_block_item(lf[74] /* chicken.file.posix#seek/cur */,0,C_fix((C_word)SEEK_CUR));
t14=C_mutate((C_word*)lf[77]+1 /* (set! chicken.file.posix#set-file-position! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3352,a[2]=((C_word)li24),tmp=(C_word)a,a+=3,tmp));
t15=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3407,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t16=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7814,a[2]=((C_word)li215),tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:401: chicken.base#getter-with-setter */
t17=*((C_word*)lf[464]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t17;
av2[1]=t15;
av2[2]=t16;
av2[3]=*((C_word*)lf[77]+1);
av2[4]=lf[486];
((C_proc)(void*)(*((C_word*)t17+1)))(5,av2);}}

/* chicken.file.posix#file-type in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3192(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_3192,c,av);}
a=C_alloc(3);
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,3,t0));
t5=C_rest_nullp(c,3);
t6=C_rest_nullp(c,4);
t7=(C_truep(t6)?C_SCHEME_TRUE:C_get_rest_arg(c,4,av,3,t0));
t8=C_rest_nullp(c,4);
t9=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3211,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:333: stat */
f_2957(t9,t2,t4,t7,lf[227]);}

/* k3209 in chicken.file.posix#file-type in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3211(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3211,c,av);}
if(C_truep(t1)){
t2=C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)C_stat_type);
t3=C_eqp(t2,C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)S_IFREG));
if(C_truep(t3)){
t4=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t4;
av2[1]=lf[220];
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=C_eqp(t2,C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)S_IFLNK));
if(C_truep(t4)){
t5=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t5;
av2[1]=lf[221];
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t5=C_eqp(t2,C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)S_IFDIR));
if(C_truep(t5)){
t6=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t6;
av2[1]=lf[222];
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
t6=C_eqp(t2,C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)S_IFCHR));
if(C_truep(t6)){
t7=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t7;
av2[1]=lf[223];
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}
else{
t7=C_eqp(t2,C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)S_IFBLK));
if(C_truep(t7)){
t8=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t8;
av2[1]=lf[224];
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}
else{
t8=C_eqp(t2,C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)S_IFIFO));
if(C_truep(t8)){
t9=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t9;
av2[1]=lf[225];
((C_proc)(void*)(*((C_word*)t9+1)))(2,av2);}}
else{
t9=C_eqp(t2,C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)S_IFSOCK));
t10=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t10;
av2[1]=(C_truep(t9)?lf[226]:lf[220]);
((C_proc)(void*)(*((C_word*)t10+1)))(2,av2);}}}}}}}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* chicken.file.posix#regular-file? in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3279(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_3279,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3287,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:347: chicken.file.posix#file-type */
t4=*((C_word*)lf[29]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k3285 in chicken.file.posix#regular-file? in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3287(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3287,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_eqp(lf[220],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.file.posix#symbolic-link? in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3289(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_3289,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3297,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:351: chicken.file.posix#file-type */
t4=*((C_word*)lf[29]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=C_SCHEME_TRUE;
av2[4]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k3295 in chicken.file.posix#symbolic-link? in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3297(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3297,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_eqp(lf[221],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.file.posix#block-device? in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3299(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_3299,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3307,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:355: chicken.file.posix#file-type */
t4=*((C_word*)lf[29]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k3305 in chicken.file.posix#block-device? in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3307(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3307,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_eqp(lf[224],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.file.posix#character-device? in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3309(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_3309,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3317,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:359: chicken.file.posix#file-type */
t4=*((C_word*)lf[29]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k3315 in chicken.file.posix#character-device? in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3317(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3317,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_eqp(lf[223],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.file.posix#fifo? in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3319(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_3319,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3327,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:363: chicken.file.posix#file-type */
t4=*((C_word*)lf[29]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k3325 in chicken.file.posix#fifo? in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3327(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3327,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_eqp(lf[225],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.file.posix#socket? in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3329(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_3329,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3337,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:367: chicken.file.posix#file-type */
t4=*((C_word*)lf[29]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k3335 in chicken.file.posix#socket? in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3337(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3337,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_eqp(lf[226],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.file.posix#directory? in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3339(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_3339,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3347,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:371: chicken.file.posix#file-type */
t4=*((C_word*)lf[29]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k3345 in chicken.file.posix#directory? in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3347(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3347,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_eqp(lf[222],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.file.posix#set-file-position! in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3352(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c<4) C_bad_min_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-4)*C_SIZEOF_PAIR +6,c,3)))){
C_save_and_reclaim((void*)f_3352,c,av);}
a=C_alloc((c-4)*C_SIZEOF_PAIR+6);
t4=C_build_rest(&a,c,4,av);
C_word t5;
C_word t6;
C_word t7;
C_word t8;
t5=C_i_pairp(t4);
t6=(C_truep(t5)?C_get_rest_arg(c,4,av,4,t0):C_fix((C_word)SEEK_SET));
t7=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3359,a[2]=t1,a[3]=t2,a[4]=t3,a[5]=t6,tmp=(C_word)a,a+=6,tmp);
/* posix-common.scm:387: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t7;
av2[2]=t3;
av2[3]=lf[228];
tp(4,av2);}}

/* k3357 in chicken.file.posix#set-file-position! in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3359(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_3359,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3362,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* posix-common.scm:388: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=lf[228];
tp(4,av2);}}

/* k3360 in k3357 in chicken.file.posix#set-file-position! in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3362(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_3362,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3368,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3374,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* posix-common.scm:389: chicken.base#port? */
t4=*((C_word*)lf[193]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k3366 in k3360 in k3357 in chicken.file.posix#set-file-position! in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3368(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_3368,c,av);}
if(C_truep(t1)){
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
/* posix-common.scm:398: posix-error */
t2=lf[183];{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[188];
av2[3]=lf[228];
av2[4]=lf[229];
av2[5]=((C_word*)t0)[3];
av2[6]=((C_word*)t0)[4];
f_2939(7,av2);}}}

/* k3372 in k3360 in k3357 in chicken.file.posix#set-file-position! in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3374(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_3374,c,av);}
if(C_truep(t1)){
t2=C_slot(((C_word*)t0)[2],C_fix(7));
t3=C_eqp(t2,lf[230]);
if(C_truep(t3)){
t4=C_fseek(((C_word*)t0)[2],((C_word*)t0)[3],((C_word*)t0)[4]);
if(C_truep(t4)){
t5=C_i_set_i_slot(((C_word*)t0)[2],C_fix(6),C_SCHEME_FALSE);
t6=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t6;
av2[1]=t4;
f_3368(2,av2);}}
else{
t5=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_FALSE;
f_3368(2,av2);}}}
else{
t4=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
f_3368(2,av2);}}}
else{
if(C_truep(C_fixnump(((C_word*)t0)[2]))){
t2=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_lseek(((C_word*)t0)[2],((C_word*)t0)[3],((C_word*)t0)[4]);
f_3368(2,av2);}}
else{
/* posix-common.scm:397: ##sys#signal-hook */
t2=*((C_word*)lf[90]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[5];
av2[2]=lf[191];
av2[3]=lf[228];
av2[4]=lf[231];
av2[5]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}}}

/* k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3407(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word t24;
C_word t25;
C_word t26;
C_word t27;
C_word t28;
C_word t29;
C_word t30;
C_word t31;
C_word t32;
C_word t33;
C_word t34;
C_word t35;
C_word t36;
C_word t37;
C_word t38;
C_word t39;
C_word t40;
C_word t41;
C_word t42;
C_word t43;
C_word t44;
C_word t45;
C_word t46;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(45,c,6)))){
C_save_and_reclaim((void *)f_3407,c,av);}
a=C_alloc(45);
t2=C_mutate((C_word*)lf[22]+1 /* (set! chicken.file.posix#file-position ...) */,t1);
t3=C_set_block_item(lf[38] /* chicken.file.posix#fileno/stdin */,0,C_fix((C_word)STDIN_FILENO));
t4=C_set_block_item(lf[39] /* chicken.file.posix#fileno/stdout */,0,C_fix((C_word)STDOUT_FILENO));
t5=C_set_block_item(lf[37] /* chicken.file.posix#fileno/stderr */,0,C_fix((C_word)STDERR_FILENO));
t6=C_set_block_item(lf[50] /* chicken.file.posix#open/rdonly */,0,C_fix((C_word)O_RDONLY));
t7=C_set_block_item(lf[57] /* chicken.file.posix#open/wronly */,0,C_fix((C_word)O_WRONLY));
t8=C_set_block_item(lf[51] /* chicken.file.posix#open/rdwr */,0,C_fix((C_word)O_RDWR));
t9=C_set_block_item(lf[52] /* chicken.file.posix#open/read */,0,C_fix((C_word)O_RDONLY));
t10=C_set_block_item(lf[56] /* chicken.file.posix#open/write */,0,C_fix((C_word)O_WRONLY));
t11=C_set_block_item(lf[44] /* chicken.file.posix#open/creat */,0,C_fix((C_word)O_CREAT));
t12=C_set_block_item(lf[42] /* chicken.file.posix#open/append */,0,C_fix((C_word)O_APPEND));
t13=C_set_block_item(lf[45] /* chicken.file.posix#open/excl */,0,C_fix((C_word)O_EXCL));
t14=C_set_block_item(lf[55] /* chicken.file.posix#open/trunc */,0,C_fix((C_word)O_TRUNC));
t15=C_set_block_item(lf[43] /* chicken.file.posix#open/binary */,0,C_fix((C_word)O_BINARY));
t16=C_set_block_item(lf[54] /* chicken.file.posix#open/text */,0,C_fix((C_word)O_TEXT));
t17=C_set_block_item(lf[60] /* chicken.file.posix#perm/irusr */,0,C_fix((C_word)S_IRUSR));
t18=C_set_block_item(lf[69] /* chicken.file.posix#perm/iwusr */,0,C_fix((C_word)S_IWUSR));
t19=C_set_block_item(lf[72] /* chicken.file.posix#perm/ixusr */,0,C_fix((C_word)S_IXUSR));
t20=C_set_block_item(lf[58] /* chicken.file.posix#perm/irgrp */,0,C_fix((C_word)S_IRGRP));
t21=C_set_block_item(lf[67] /* chicken.file.posix#perm/iwgrp */,0,C_fix((C_word)S_IWGRP));
t22=C_set_block_item(lf[70] /* chicken.file.posix#perm/ixgrp */,0,C_fix((C_word)S_IXGRP));
t23=C_set_block_item(lf[59] /* chicken.file.posix#perm/iroth */,0,C_fix((C_word)S_IROTH));
t24=C_set_block_item(lf[68] /* chicken.file.posix#perm/iwoth */,0,C_fix((C_word)S_IWOTH));
t25=C_set_block_item(lf[71] /* chicken.file.posix#perm/ixoth */,0,C_fix((C_word)S_IXOTH));
t26=C_set_block_item(lf[63] /* chicken.file.posix#perm/irwxu */,0,C_fix((C_word)S_IRUSR | S_IWUSR | S_IXUSR));
t27=C_set_block_item(lf[61] /* chicken.file.posix#perm/irwxg */,0,C_fix((C_word)S_IRGRP | S_IWGRP | S_IXGRP));
t28=C_set_block_item(lf[62] /* chicken.file.posix#perm/irwxo */,0,C_fix((C_word)S_IROTH | S_IWOTH | S_IXOTH));
t29=C_SCHEME_UNDEFINED;
t30=(*a=C_VECTOR_TYPE|1,a[1]=t29,tmp=(C_word)a,a+=2,tmp);
t31=C_SCHEME_UNDEFINED;
t32=(*a=C_VECTOR_TYPE|1,a[1]=t31,tmp=(C_word)a,a+=2,tmp);
t33=C_set_block_item(t30,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3435,a[2]=((C_word)li25),tmp=(C_word)a,a+=3,tmp));
t34=C_set_block_item(t32,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3472,a[2]=((C_word)li26),tmp=(C_word)a,a+=3,tmp));
t35=C_mutate((C_word*)lf[40]+1 /* (set! chicken.file.posix#open-input-file* ...) */,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3491,a[2]=t32,a[3]=t30,a[4]=((C_word)li27),tmp=(C_word)a,a+=5,tmp));
t36=C_mutate((C_word*)lf[41]+1 /* (set! chicken.file.posix#open-output-file* ...) */,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3505,a[2]=t32,a[3]=t30,a[4]=((C_word)li28),tmp=(C_word)a,a+=5,tmp));
t37=C_mutate((C_word*)lf[73]+1 /* (set! chicken.file.posix#port->fileno ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3519,a[2]=((C_word)li29),tmp=(C_word)a,a+=3,tmp));
t38=C_mutate((C_word*)lf[5]+1 /* (set! chicken.file.posix#duplicate-fileno ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3564,a[2]=((C_word)li30),tmp=(C_word)a,a+=3,tmp));
t39=C_mutate((C_word*)lf[175]+1 /* (set! chicken.process-context.posix#current-process-id ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3591,a[2]=((C_word)li31),tmp=(C_word)a,a+=3,tmp));
t40=C_mutate((C_word*)lf[169]+1 /* (set! chicken.process-context.posix#change-directory* ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3594,a[2]=((C_word)li32),tmp=(C_word)a,a+=3,tmp));
t41=*((C_word*)lf[253]+1);
t42=C_mutate((C_word*)lf[253]+1 /* (set! ##sys#change-directory-hook ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3610,a[2]=t41,a[3]=((C_word)li33),tmp=(C_word)a,a+=4,tmp));
t43=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3624,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t44=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7783,a[2]=((C_word)li213),tmp=(C_word)a,a+=3,tmp);
t45=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7808,a[2]=((C_word)li214),tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:560: chicken.base#getter-with-setter */
t46=*((C_word*)lf[464]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t46;
av2[1]=t43;
av2[2]=t44;
av2[3]=t45;
av2[4]=lf[482];
((C_proc)(void*)(*((C_word*)t46+1)))(5,av2);}}

/* mode in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_3435(C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,3)))){
C_save_and_reclaim_args((void *)trf_3435,4,t1,t2,t3,t4);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3443,a[2]=t1,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_i_pairp(t3))){
t6=C_u_i_car(t3);
t7=C_eqp(t6,lf[232]);
if(C_truep(t7)){
if(C_truep(C_i_not(t2))){
/* posix-common.scm:482: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[190]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[190]+1);
av2[1]=t1;
av2[2]=lf[233];
av2[3]=t4;
tp(4,av2);}}
else{
/* posix-common.scm:486: ##sys#error */
t8=*((C_word*)lf[95]+1);{
C_word av2[4];
av2[0]=t8;
av2[1]=t5;
av2[2]=lf[234];
av2[3]=t6;
((C_proc)(void*)(*((C_word*)t8+1)))(4,av2);}}}
else{
/* posix-common.scm:487: ##sys#error */
t8=*((C_word*)lf[95]+1);{
C_word av2[4];
av2[0]=t8;
av2[1]=t5;
av2[2]=lf[235];
av2[3]=t6;
((C_proc)(void*)(*((C_word*)t8+1)))(4,av2);}}}
else{
if(C_truep(t2)){
/* posix-common.scm:482: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[190]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[190]+1);
av2[1]=t1;
av2[2]=lf[236];
av2[3]=t4;
tp(4,av2);}}
else{
/* posix-common.scm:482: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[190]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[190]+1);
av2[1]=t1;
av2[2]=lf[237];
av2[3]=t4;
tp(4,av2);}}}}

/* k3441 in mode in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3443(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3443,c,av);}
/* posix-common.scm:482: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[190]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[190]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
tp(4,av2);}}

/* check in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_3472(C_word t1,C_word t2,C_word t3,C_word t4,C_word t5){
C_word tmp;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,5)))){
C_save_and_reclaim_args((void *)trf_3472,5,t1,t2,t3,t4,t5);}
a=C_alloc(4);
if(C_truep(C_null_pointerp(t5))){
/* posix-common.scm:493: posix-error */
t6=lf[183];{
C_word av2[6];
av2[0]=t6;
av2[1]=t1;
av2[2]=lf[188];
av2[3]=t2;
av2[4]=lf[238];
av2[5]=t3;
f_2939(6,av2);}}
else{
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3485,a[2]=t5,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
if(C_truep(t4)){
/* posix-common.scm:494: ##sys#make-port */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[239]+1));
C_word av2[6];
av2[0]=*((C_word*)lf[239]+1);
av2[1]=t6;
av2[2]=C_fix(1);
av2[3]=*((C_word*)lf[240]+1);
av2[4]=lf[241];
av2[5]=lf[230];
tp(6,av2);}}
else{
/* posix-common.scm:494: ##sys#make-port */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[239]+1));
C_word av2[6];
av2[0]=*((C_word*)lf[239]+1);
av2[1]=t6;
av2[2]=C_fix(2);
av2[3]=*((C_word*)lf[240]+1);
av2[4]=lf[241];
av2[5]=lf[230];
tp(6,av2);}}}}

/* k3483 in check in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3485(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3485,c,av);}
t2=C_set_file_ptr(t1,((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.file.posix#open-input-file* in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3491(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +7,c,3)))){
C_save_and_reclaim((void*)f_3491,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+7);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3495,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=t1,a[5]=((C_word*)t0)[3],a[6]=t3,tmp=(C_word)a,a+=7,tmp);
/* posix-common.scm:499: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t4;
av2[2]=t2;
av2[3]=lf[242];
tp(4,av2);}}

/* k3493 in chicken.file.posix#open-input-file* in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3495(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_3495,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3503,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:500: mode */
f_3435(t2,C_SCHEME_TRUE,((C_word*)t0)[6],lf[242]);}

/* k3501 in k3493 in chicken.file.posix#open-input-file* in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3503(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(2,c,5)))){
C_save_and_reclaim((void *)f_3503,c,av);}
a=C_alloc(2);
t2=C_fdopen(&a,2,((C_word*)t0)[2],t1);
/* posix-common.scm:500: check */
f_3472(((C_word*)t0)[4],lf[242],((C_word*)t0)[2],C_SCHEME_TRUE,t2);}

/* chicken.file.posix#open-output-file* in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3505(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +7,c,3)))){
C_save_and_reclaim((void*)f_3505,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+7);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3509,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=t1,a[5]=((C_word*)t0)[3],a[6]=t3,tmp=(C_word)a,a+=7,tmp);
/* posix-common.scm:503: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t4;
av2[2]=t2;
av2[3]=lf[243];
tp(4,av2);}}

/* k3507 in chicken.file.posix#open-output-file* in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3509(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_3509,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3517,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:504: mode */
f_3435(t2,C_SCHEME_FALSE,((C_word*)t0)[6],lf[243]);}

/* k3515 in k3507 in chicken.file.posix#open-output-file* in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3517(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(2,c,5)))){
C_save_and_reclaim((void *)f_3517,c,av);}
a=C_alloc(2);
t2=C_fdopen(&a,2,((C_word*)t0)[2],t1);
/* posix-common.scm:504: check */
f_3472(((C_word*)t0)[4],lf[243],((C_word*)t0)[2],C_SCHEME_FALSE,t2);}

/* chicken.file.posix#port->fileno in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3519(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_3519,c,av);}
a=C_alloc(4);
t3=C_i_check_port_2(t2,C_fix(0),C_SCHEME_TRUE,lf[244]);
t4=C_slot(t2,C_fix(7));
t5=C_eqp(lf[226],t4);
if(C_truep(t5)){
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3535,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:514: ##sys#port-data */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[245]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[245]+1);
av2[1]=t6;
av2[2]=t2;
tp(3,av2);}}
else{
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3558,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:515: ##sys#peek-unsigned-integer */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[248]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[248]+1);
av2[1]=t6;
av2[2]=t2;
av2[3]=C_fix(0);
tp(4,av2);}}}

/* k3533 in chicken.file.posix#port->fileno in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3535(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3535,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_slot(t1,C_fix(0));
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k3542 in k3556 in chicken.file.posix#port->fileno in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3544(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3544,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k3556 in chicken.file.posix#port->fileno in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3558(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,5)))){
C_save_and_reclaim((void *)f_3558,c,av);}
a=C_alloc(4);
if(C_truep(C_i_not(C_i_zerop(t1)))){
t2=C_port_fileno(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3544,a[2]=((C_word*)t0)[3],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_fixnum_lessp(t2,C_fix(0)))){
/* posix-common.scm:518: posix-error */
t4=lf[183];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[188];
av2[3]=lf[244];
av2[4]=lf[246];
av2[5]=((C_word*)t0)[2];
f_2939(6,av2);}}
else{
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}
else{
/* posix-common.scm:520: posix-error */
t2=lf[183];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[191];
av2[3]=lf[244];
av2[4]=lf[247];
av2[5]=((C_word*)t0)[2];
f_2939(6,av2);}}}

/* chicken.file.posix#duplicate-fileno in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3564(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +5,c,3)))){
C_save_and_reclaim((void*)f_3564,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+5);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3568,a[2]=t1,a[3]=t2,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:524: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t4;
av2[2]=t2;
av2[3]=lf[249];
tp(4,av2);}}

/* k3566 in chicken.file.posix#duplicate-fileno in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3568(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_3568,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3571,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_i_nullp(((C_word*)t0)[4]))){
t3=t2;
f_3571(t3,C_dup(((C_word*)t0)[3]));}
else{
t3=C_i_car(((C_word*)t0)[4]);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3589,a[2]=t2,a[3]=((C_word*)t0)[3],a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:528: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t4;
av2[2]=t3;
av2[3]=lf[249];
tp(4,av2);}}}

/* k3569 in k3566 in chicken.file.posix#duplicate-fileno in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_3571(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,5)))){
C_save_and_reclaim_args((void *)trf_3571,2,t0,t1);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3574,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_fixnum_lessp(t1,C_fix(0)))){
/* posix-common.scm:531: posix-error */
t3=lf[183];{
C_word av2[6];
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[188];
av2[3]=lf[249];
av2[4]=lf[250];
av2[5]=((C_word*)t0)[3];
f_2939(6,av2);}}
else{
t3=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t3;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k3572 in k3569 in k3566 in chicken.file.posix#duplicate-fileno in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3574(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3574,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k3587 in k3566 in chicken.file.posix#duplicate-fileno in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3589(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3589,c,av);}
t2=((C_word*)t0)[2];
f_3571(t2,C_dup2(((C_word*)t0)[3],((C_word*)t0)[4]));}

/* chicken.process-context.posix#current-process-id in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3591(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3591,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=stub826(C_SCHEME_UNDEFINED);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.process-context.posix#change-directory* in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3594(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_3594,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3598,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:545: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t3;
av2[2]=t2;
av2[3]=lf[251];
tp(4,av2);}}

/* k3596 in chicken.process-context.posix#change-directory* in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3598(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,5)))){
C_save_and_reclaim((void *)f_3598,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3601,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=C_eqp(C_fix(0),C_fchdir(((C_word*)t0)[3]));
if(C_truep(t3)){
t4=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t4;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
/* posix-common.scm:547: posix-error */
t4=lf[183];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=lf[188];
av2[3]=lf[251];
av2[4]=lf[252];
av2[5]=((C_word*)t0)[3];
f_2939(6,av2);}}}

/* k3599 in k3596 in chicken.process-context.posix#change-directory* in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3601(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3601,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* ##sys#change-directory-hook in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3610(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_3610,c,av);}
if(C_truep(C_fixnump(t2))){
t3=*((C_word*)lf[169]+1);
t4=*((C_word*)lf[169]+1);
/* posix-common.scm:552: g833 */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[169]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[169]+1);
av2[1]=t1;
av2[2]=t2;
tp(3,av2);}}
else{
/* posix-common.scm:552: g833 */
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t1;
av2[2]=t2;
((C_proc)C_fast_retrieve_proc(t3))(3,av2);}}}

/* k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3624(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(32,c,4)))){
C_save_and_reclaim((void *)f_3624,c,av);}
a=C_alloc(32);
t2=C_mutate((C_word*)lf[13]+1 /* (set! chicken.file.posix#file-creation-mode ...) */,t1);
t3=C_mutate(&lf[254] /* (set! chicken.posix#decode-seconds ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)C_decode_seconds,a[2]=((C_word)li34),tmp=(C_word)a,a+=3,tmp));
t4=C_mutate(&lf[255] /* (set! chicken.posix#check-time-vector ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3627,a[2]=((C_word)li35),tmp=(C_word)a,a+=3,tmp));
t5=C_mutate((C_word*)lf[81]+1 /* (set! chicken.time.posix#seconds->local-time ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3646,a[2]=((C_word)li36),tmp=(C_word)a,a+=3,tmp));
t6=C_mutate((C_word*)lf[79]+1 /* (set! chicken.time.posix#seconds->utc-time ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3667,a[2]=((C_word)li37),tmp=(C_word)a,a+=3,tmp));
t7=C_mutate((C_word*)lf[82]+1 /* (set! chicken.time.posix#seconds->string ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3699,a[2]=((C_word)li38),tmp=(C_word)a,a+=3,tmp));
t8=C_fix((C_word)sizeof(struct tm));
t9=C_mutate((C_word*)lf[83]+1 /* (set! chicken.time.posix#local-time->seconds ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3737,a[2]=t8,a[3]=((C_word)li39),tmp=(C_word)a,a+=4,tmp));
t10=C_fix((C_word)sizeof(struct tm));
t11=C_mutate((C_word*)lf[85]+1 /* (set! chicken.time.posix#time->string ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3782,a[2]=t10,a[3]=((C_word)li40),tmp=(C_word)a,a+=4,tmp));
t12=C_mutate((C_word*)lf[133]+1 /* (set! chicken.process.signal#set-signal-handler! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3844,a[2]=((C_word)li41),tmp=(C_word)a,a+=3,tmp));
t13=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3859,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t14=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7774,a[2]=((C_word)li212),tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:635: chicken.base#getter-with-setter */
t15=*((C_word*)lf[464]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t15;
av2[1]=t13;
av2[2]=t14;
av2[3]=*((C_word*)lf[133]+1);
av2[4]=lf[480];
((C_proc)(void*)(*((C_word*)t15+1)))(5,av2);}}

/* chicken.posix#check-time-vector in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_3627(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,4)))){
C_save_and_reclaim_args((void *)trf_3627,3,t1,t2,t3);}
t4=C_i_check_vector_2(t3,t2);
t5=C_block_size(t3);
if(C_truep(C_fixnum_lessp(t5,C_fix(10)))){
/* posix-common.scm:579: ##sys#error */
t6=*((C_word*)lf[95]+1);{
C_word av2[5];
av2[0]=t6;
av2[1]=t1;
av2[2]=t2;
av2[3]=lf[256];
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}
else{
t6=C_SCHEME_UNDEFINED;
t7=t1;{
C_word av2[2];
av2[0]=t7;
av2[1]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}}

/* chicken.time.posix#seconds->local-time in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3646(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_3646,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3650,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_rest_nullp(c,2))){
/* posix-common.scm:582: chicken.time#current-seconds */
t3=*((C_word*)lf[212]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_get_rest_arg(c,2,av,2,t0);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f8684,a[2]=t1,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:583: ##sys#check-exact-integer */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[211]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[211]+1);
av2[1]=t4;
av2[2]=t3;
av2[3]=lf[257];
tp(4,av2);}}}

/* k3648 in chicken.time.posix#seconds->local-time in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3650(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_3650,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3653,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:583: ##sys#check-exact-integer */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[211]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[211]+1);
av2[1]=t2;
av2[2]=t1;
av2[3]=lf[257];
tp(4,av2);}}

/* k3651 in k3648 in chicken.time.posix#seconds->local-time in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3653(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3653,c,av);}
/* posix-common.scm:584: decode-seconds */
{C_proc tp=(C_proc)C_fast_retrieve_proc(lf[254]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=lf[254];
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=C_SCHEME_FALSE;
tp(4,av2);}}

/* chicken.time.posix#seconds->utc-time in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3667(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_3667,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3671,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_rest_nullp(c,2))){
/* posix-common.scm:587: chicken.time#current-seconds */
t3=*((C_word*)lf[212]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_get_rest_arg(c,2,av,2,t0);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f8688,a[2]=t1,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:588: ##sys#check-exact-integer */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[211]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[211]+1);
av2[1]=t4;
av2[2]=t3;
av2[3]=lf[258];
tp(4,av2);}}}

/* k3669 in chicken.time.posix#seconds->utc-time in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3671(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_3671,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3674,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:588: ##sys#check-exact-integer */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[211]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[211]+1);
av2[1]=t2;
av2[2]=t1;
av2[3]=lf[258];
tp(4,av2);}}

/* k3672 in k3669 in chicken.time.posix#seconds->utc-time in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3674(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3674,c,av);}
/* posix-common.scm:589: decode-seconds */
{C_proc tp=(C_proc)C_fast_retrieve_proc(lf[254]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=lf[254];
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=C_SCHEME_TRUE;
tp(4,av2);}}

/* chicken.time.posix#seconds->string in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3699(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_3699,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3703,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_rest_nullp(c,2))){
/* posix-common.scm:593: chicken.time#current-seconds */
t3=*((C_word*)lf[212]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_get_rest_arg(c,2,av,2,t0);
f_3703(2,av2);}}}

/* k3701 in chicken.time.posix#seconds->string in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3703(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_3703,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3706,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:594: ##sys#check-exact-integer */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[211]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[211]+1);
av2[1]=t2;
av2[2]=t1;
av2[3]=lf[260];
tp(4,av2);}}

/* k3704 in k3701 in chicken.time.posix#seconds->string in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3706(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_3706,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3709,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=C_a_i_bytevector(&a,1,C_fix(3));
t4=C_fix((C_word)sizeof(int) * CHAR_BIT);
t5=C_i_foreign_ranged_integer_argumentp(((C_word*)t0)[3],t4);
/* posix-common.scm:592: ##sys#peek-c-string */
t6=*((C_word*)lf[185]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t2;
av2[2]=stub882(t3,t5);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* k3707 in k3704 in k3701 in chicken.time.posix#seconds->string in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3709(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_3709,c,av);}
if(C_truep(t1)){
t2=C_block_size(t1);
/* posix-common.scm:597: ##sys#substring */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[259]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[259]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_fix(0);
av2[4]=C_fixnum_difference(t2,C_fix(1));
tp(5,av2);}}
else{
/* posix-common.scm:598: ##sys#error */
t2=*((C_word*)lf[95]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[260];
av2[3]=lf[261];
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}}

/* chicken.time.posix#local-time->seconds in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3737(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_3737,c,av);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3741,a[2]=t2,a[3]=t1,a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:603: check-time-vector */
f_3627(t3,lf[262],t2);}

/* k3739 in chicken.time.posix#local-time->seconds in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3741(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_3741,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3745,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:604: ##sys#make-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[264]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[264]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=C_make_character(0);
tp(4,av2);}}

/* k3743 in k3739 in chicken.time.posix#local-time->seconds in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3745(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,4)))){
C_save_and_reclaim((void *)f_3745,c,av);}
a=C_alloc(7);
t2=C_a_mktime(&a,2,((C_word*)t0)[2],t1);
if(C_truep(C_i_nequalp(C_fix(-1),t2))){
/* posix-common.scm:606: ##sys#error */
t3=*((C_word*)lf[95]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[262];
av2[3]=lf[263];
av2[4]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}
else{
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* chicken.time.posix#time->string in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3782(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_3782,c,av);}
a=C_alloc(6);
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,3,t0));
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3789,a[2]=t4,a[3]=t1,a[4]=t2,a[5]=((C_word*)t0)[2],tmp=(C_word)a,a+=6,tmp);
/* posix-common.scm:614: check-time-vector */
f_3627(t5,lf[265],t2);}

/* k3787 in chicken.time.posix#time->string in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3789(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_3789,c,av);}
a=C_alloc(9);
if(C_truep(((C_word*)t0)[2])){
t2=C_i_check_string_2(((C_word*)t0)[2],lf[265]);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3798,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3808,a[2]=t3,a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:618: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[190]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[190]+1);
av2[1]=t4;
av2[2]=((C_word*)t0)[2];
av2[3]=lf[265];
tp(4,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3815,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3836,a[2]=t2,a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:620: ##sys#make-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[264]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[264]+1);
av2[1]=t3;
av2[2]=((C_word*)t0)[5];
av2[3]=C_make_character(0);
tp(4,av2);}}}

/* k3796 in k3787 in chicken.time.posix#time->string in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3798(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_3798,c,av);}
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
/* posix-common.scm:619: ##sys#error */
t2=*((C_word*)lf[95]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[265];
av2[3]=lf[266];
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}}

/* k3806 in k3787 in chicken.time.posix#time->string in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3808(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_3808,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3812,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:618: ##sys#make-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[264]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[264]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=C_make_character(0);
tp(4,av2);}}

/* k3810 in k3806 in k3787 in chicken.time.posix#time->string in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3812(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_3812,c,av);}
a=C_alloc(5);
t2=C_a_i_bytevector(&a,1,C_fix(3));
if(C_truep(t1)){
t3=C_i_foreign_block_argumentp(t1);
/* posix-common.scm:611: ##sys#peek-c-string */
t4=*((C_word*)lf[185]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[2];
av2[2]=stub919(t2,((C_word*)t0)[3],((C_word*)t0)[4],t3);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
/* posix-common.scm:611: ##sys#peek-c-string */
t3=*((C_word*)lf[185]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=stub919(t2,((C_word*)t0)[3],((C_word*)t0)[4],C_SCHEME_FALSE);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}}

/* k3813 in k3787 in chicken.time.posix#time->string in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3815(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_3815,c,av);}
if(C_truep(t1)){
t2=C_block_size(t1);
/* posix-common.scm:622: ##sys#substring */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[259]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[259]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_fix(0);
av2[4]=C_fixnum_difference(t2,C_fix(1));
tp(5,av2);}}
else{
/* posix-common.scm:623: ##sys#error */
t2=*((C_word*)lf[95]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[265];
av2[3]=lf[267];
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}}

/* k3834 in k3787 in chicken.time.posix#time->string in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3836(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_3836,c,av);}
a=C_alloc(5);
t2=C_a_i_bytevector(&a,1,C_fix(3));
if(C_truep(t1)){
t3=C_i_foreign_block_argumentp(t1);
/* posix-common.scm:610: ##sys#peek-c-string */
t4=*((C_word*)lf[185]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[2];
av2[2]=stub909(t2,((C_word*)t0)[3],t3);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
/* posix-common.scm:610: ##sys#peek-c-string */
t3=*((C_word*)lf[185]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=stub909(t2,((C_word*)t0)[3],C_SCHEME_FALSE);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}}

/* chicken.process.signal#set-signal-handler! in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3844(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_3844,c,av);}
a=C_alloc(5);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3848,a[2]=t3,a[3]=t2,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:630: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t4;
av2[2]=t2;
av2[3]=lf[269];
tp(4,av2);}}

/* k3846 in chicken.process.signal#set-signal-handler! in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3848(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3848,c,av);}
if(C_truep(((C_word*)t0)[2])){
t2=C_establish_signal_handler(((C_word*)t0)[3],((C_word*)t0)[3]);
t3=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_vector_set(*((C_word*)lf[268]+1),((C_word*)t0)[3],((C_word*)t0)[2]);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=C_establish_signal_handler(((C_word*)t0)[3],C_SCHEME_FALSE);
t3=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_vector_set(*((C_word*)lf[268]+1),((C_word*)t0)[3],((C_word*)t0)[2]);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3859(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word t24;
C_word t25;
C_word t26;
C_word t27;
C_word t28;
C_word t29;
C_word t30;
C_word t31;
C_word t32;
C_word t33;
C_word t34;
C_word t35;
C_word t36;
C_word t37;
C_word t38;
C_word t39;
C_word t40;
C_word t41;
C_word t42;
C_word t43;
C_word t44;
C_word t45;
C_word t46;
C_word t47;
C_word t48;
C_word t49;
C_word t50;
C_word t51;
C_word t52;
C_word t53;
C_word t54;
C_word t55;
C_word t56;
C_word t57;
C_word t58;
C_word t59;
C_word t60;
C_word t61;
C_word t62;
C_word t63;
C_word t64;
C_word t65;
C_word t66;
C_word t67;
C_word t68;
C_word t69;
C_word t70;
C_word t71;
C_word t72;
C_word t73;
C_word t74;
C_word t75;
C_word t76;
C_word t77;
C_word t78;
C_word t79;
C_word t80;
C_word t81;
C_word t82;
C_word t83;
C_word t84;
C_word t85;
C_word t86;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(170,c,8)))){
C_save_and_reclaim((void *)f_3859,c,av);}
a=C_alloc(170);
t2=C_mutate((C_word*)lf[135]+1 /* (set! chicken.process.signal#signal-handler ...) */,t1);
t3=C_mutate((C_word*)lf[124]+1 /* (set! chicken.process#process-sleep ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3861,a[2]=((C_word)li42),tmp=(C_word)a,a+=3,tmp));
t4=C_mutate((C_word*)lf[112]+1 /* (set! chicken.process#process-wait ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3867,a[2]=((C_word)li45),tmp=(C_word)a,a+=3,tmp));
t5=C_mutate(&lf[274] /* (set! chicken.posix#list->c-string-buffer ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3938,a[2]=((C_word)li54),tmp=(C_word)a,a+=3,tmp));
t6=C_mutate(&lf[276] /* (set! chicken.posix#free-c-string-buffer ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4055,a[2]=((C_word)li56),tmp=(C_word)a,a+=3,tmp));
t7=C_mutate(&lf[285] /* (set! chicken.posix#check-environment-list ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4089,a[2]=((C_word)li59),tmp=(C_word)a,a+=3,tmp));
t8=*((C_word*)lf[286]+1);
t9=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4136,a[2]=((C_word)li60),tmp=(C_word)a,a+=3,tmp);
t10=C_mutate(&lf[287] /* (set! chicken.posix#call-with-exec-args ...) */,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4138,a[2]=t9,a[3]=t8,a[4]=((C_word)li69),tmp=(C_word)a,a+=5,tmp));
t11=C_set_block_item(lf[125] /* chicken.process#pipe/buf */,0,C_fix((C_word)PIPE_BUF));
t12=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4295,a[2]=((C_word)li70),tmp=(C_word)a,a+=3,tmp);
t13=C_mutate((C_word*)lf[118]+1 /* (set! chicken.process#open-input-pipe ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4314,a[2]=t12,a[3]=((C_word)li71),tmp=(C_word)a,a+=4,tmp));
t14=C_mutate((C_word*)lf[119]+1 /* (set! chicken.process#open-output-pipe ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4354,a[2]=t12,a[3]=((C_word)li72),tmp=(C_word)a,a+=4,tmp));
t15=C_mutate((C_word*)lf[115]+1 /* (set! chicken.process#close-input-pipe ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4394,a[2]=((C_word)li73),tmp=(C_word)a,a+=3,tmp));
t16=C_mutate((C_word*)lf[116]+1 /* (set! chicken.process#close-output-pipe ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4409,a[2]=((C_word)li74),tmp=(C_word)a,a+=3,tmp));
t17=C_mutate((C_word*)lf[120]+1 /* (set! chicken.process#with-input-from-pipe ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4424,a[2]=((C_word)li79),tmp=(C_word)a,a+=3,tmp));
t18=C_mutate((C_word*)lf[114]+1 /* (set! chicken.process#call-with-output-pipe ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4458,a[2]=((C_word)li82),tmp=(C_word)a,a+=3,tmp));
t19=C_mutate((C_word*)lf[113]+1 /* (set! chicken.process#call-with-input-pipe ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4482,a[2]=((C_word)li85),tmp=(C_word)a,a+=3,tmp));
t20=C_mutate((C_word*)lf[121]+1 /* (set! chicken.process#with-output-to-pipe ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4506,a[2]=((C_word)li90),tmp=(C_word)a,a+=3,tmp));
t21=C_mutate((C_word*)lf[305]+1 /* (set! ##sys#file-nonblocking! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4540,a[2]=((C_word)li91),tmp=(C_word)a,a+=3,tmp));
t22=C_mutate((C_word*)lf[306]+1 /* (set! ##sys#file-select-one ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4547,a[2]=((C_word)li92),tmp=(C_word)a,a+=3,tmp));
t23=C_set_block_item(lf[6] /* chicken.file.posix#fcntl/dupfd */,0,C_fix((C_word)F_DUPFD));
t24=C_set_block_item(lf[7] /* chicken.file.posix#fcntl/getfd */,0,C_fix((C_word)F_GETFD));
t25=C_set_block_item(lf[9] /* chicken.file.posix#fcntl/setfd */,0,C_fix((C_word)F_SETFD));
t26=C_set_block_item(lf[8] /* chicken.file.posix#fcntl/getfl */,0,C_fix((C_word)F_GETFL));
t27=C_set_block_item(lf[10] /* chicken.file.posix#fcntl/setfl */,0,C_fix((C_word)F_SETFL));
t28=C_set_block_item(lf[49] /* chicken.file.posix#open/nonblock */,0,C_fix((C_word)O_NONBLOCK));
t29=C_set_block_item(lf[47] /* chicken.file.posix#open/noctty */,0,C_fix((C_word)O_NOCTTY));
t30=C_set_block_item(lf[46] /* chicken.file.posix#open/fsync */,0,C_fix((C_word)O_FSYNC));
t31=C_set_block_item(lf[53] /* chicken.file.posix#open/sync */,0,C_fix((C_word)O_SYNC));
t32=C_set_block_item(lf[48] /* chicken.file.posix#open/noinherit */,0,C_fix(0));
t33=C_set_block_item(lf[126] /* chicken.process#spawn/overlay */,0,C_fix(0));
t34=C_set_block_item(lf[127] /* chicken.process#spawn/wait */,0,C_fix(0));
t35=C_set_block_item(lf[128] /* chicken.process#spawn/nowait */,0,C_fix(0));
t36=C_set_block_item(lf[129] /* chicken.process#spawn/nowaito */,0,C_fix(0));
t37=C_set_block_item(lf[130] /* chicken.process#spawn/detach */,0,C_fix(0));
t38=C_set_block_item(lf[66] /* chicken.file.posix#perm/isvtx */,0,C_fix((C_word)S_ISVTX));
t39=C_set_block_item(lf[65] /* chicken.file.posix#perm/isuid */,0,C_fix((C_word)S_ISUID));
t40=C_set_block_item(lf[64] /* chicken.file.posix#perm/isgid */,0,C_fix((C_word)S_ISGID));
t41=C_mutate((C_word*)lf[12]+1 /* (set! chicken.file.posix#file-control ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4587,a[2]=((C_word)li93),tmp=(C_word)a,a+=3,tmp));
t42=C_fix((C_word)S_IRUSR | S_IWUSR | S_IXUSR);
t43=C_fix((C_word)S_IRGRP);
t44=C_fix((C_word)S_IROTH);
t45=C_u_fixnum_or(t43,t44);
t46=C_u_fixnum_or(t42,t45);
t47=C_mutate((C_word*)lf[19]+1 /* (set! chicken.file.posix#file-open ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4621,a[2]=t46,a[3]=((C_word)li94),tmp=(C_word)a,a+=4,tmp));
t48=C_mutate((C_word*)lf[11]+1 /* (set! chicken.file.posix#file-close ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4653,a[2]=((C_word)li96),tmp=(C_word)a,a+=3,tmp));
t49=C_mutate((C_word*)lf[23]+1 /* (set! chicken.file.posix#file-read ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4684,a[2]=((C_word)li97),tmp=(C_word)a,a+=3,tmp));
t50=C_mutate((C_word*)lf[28]+1 /* (set! chicken.file.posix#file-write ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4724,a[2]=((C_word)li98),tmp=(C_word)a,a+=3,tmp));
t51=C_mutate((C_word*)lf[18]+1 /* (set! chicken.file.posix#file-mkstemp ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4761,a[2]=((C_word)li99),tmp=(C_word)a,a+=3,tmp));
t52=C_mutate((C_word*)lf[24]+1 /* (set! chicken.file.posix#file-select ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4793,a[2]=((C_word)li104),tmp=(C_word)a,a+=3,tmp));
t53=C_mutate((C_word*)lf[117]+1 /* (set! chicken.process#create-pipe ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5170,a[2]=((C_word)li105),tmp=(C_word)a,a+=3,tmp));
t54=C_set_block_item(lf[157] /* chicken.process.signal#signal/term */,0,C_fix((C_word)SIGTERM));
t55=C_set_block_item(lf[151] /* chicken.process.signal#signal/kill */,0,C_fix((C_word)SIGKILL));
t56=C_set_block_item(lf[149] /* chicken.process.signal#signal/int */,0,C_fix((C_word)SIGINT));
t57=C_set_block_item(lf[147] /* chicken.process.signal#signal/hup */,0,C_fix((C_word)SIGHUP));
t58=C_set_block_item(lf[146] /* chicken.process.signal#signal/fpe */,0,C_fix((C_word)SIGFPE));
t59=C_set_block_item(lf[148] /* chicken.process.signal#signal/ill */,0,C_fix((C_word)SIGILL));
t60=C_set_block_item(lf[155] /* chicken.process.signal#signal/segv */,0,C_fix((C_word)SIGSEGV));
t61=C_set_block_item(lf[140] /* chicken.process.signal#signal/abrt */,0,C_fix((C_word)SIGABRT));
t62=C_set_block_item(lf[158] /* chicken.process.signal#signal/trap */,0,C_fix((C_word)SIGTRAP));
t63=C_set_block_item(lf[154] /* chicken.process.signal#signal/quit */,0,C_fix((C_word)SIGQUIT));
t64=C_set_block_item(lf[141] /* chicken.process.signal#signal/alrm */,0,C_fix((C_word)SIGALRM));
t65=C_set_block_item(lf[163] /* chicken.process.signal#signal/vtalrm */,0,C_fix((C_word)SIGVTALRM));
t66=C_set_block_item(lf[153] /* chicken.process.signal#signal/prof */,0,C_fix((C_word)SIGPROF));
t67=C_set_block_item(lf[150] /* chicken.process.signal#signal/io */,0,C_fix((C_word)SIGIO));
t68=C_set_block_item(lf[160] /* chicken.process.signal#signal/urg */,0,C_fix((C_word)SIGURG));
t69=C_set_block_item(lf[144] /* chicken.process.signal#signal/chld */,0,C_fix((C_word)SIGCHLD));
t70=C_set_block_item(lf[145] /* chicken.process.signal#signal/cont */,0,C_fix((C_word)SIGCONT));
t71=C_set_block_item(lf[156] /* chicken.process.signal#signal/stop */,0,C_fix((C_word)SIGSTOP));
t72=C_set_block_item(lf[159] /* chicken.process.signal#signal/tstp */,0,C_fix((C_word)SIGTSTP));
t73=C_set_block_item(lf[152] /* chicken.process.signal#signal/pipe */,0,C_fix((C_word)SIGPIPE));
t74=C_set_block_item(lf[165] /* chicken.process.signal#signal/xcpu */,0,C_fix((C_word)SIGXCPU));
t75=C_set_block_item(lf[166] /* chicken.process.signal#signal/xfsz */,0,C_fix((C_word)SIGXFSZ));
t76=C_set_block_item(lf[161] /* chicken.process.signal#signal/usr1 */,0,C_fix((C_word)SIGUSR1));
t77=C_set_block_item(lf[162] /* chicken.process.signal#signal/usr2 */,0,C_fix((C_word)SIGUSR2));
t78=C_set_block_item(lf[164] /* chicken.process.signal#signal/winch */,0,C_fix((C_word)SIGWINCH));
t79=C_set_block_item(lf[143] /* chicken.process.signal#signal/bus */,0,C_fix((C_word)SIGBUS));
t80=C_set_block_item(lf[142] /* chicken.process.signal#signal/break */,0,C_fix(0));
t81=C_a_i_list(&a,26,*((C_word*)lf[157]+1),*((C_word*)lf[151]+1),*((C_word*)lf[149]+1),*((C_word*)lf[147]+1),*((C_word*)lf[146]+1),*((C_word*)lf[148]+1),*((C_word*)lf[155]+1),*((C_word*)lf[140]+1),*((C_word*)lf[158]+1),*((C_word*)lf[154]+1),*((C_word*)lf[141]+1),*((C_word*)lf[163]+1),*((C_word*)lf[153]+1),*((C_word*)lf[150]+1),*((C_word*)lf[160]+1),*((C_word*)lf[144]+1),*((C_word*)lf[145]+1),*((C_word*)lf[156]+1),*((C_word*)lf[159]+1),*((C_word*)lf[152]+1),*((C_word*)lf[165]+1),*((C_word*)lf[166]+1),*((C_word*)lf[161]+1),*((C_word*)lf[162]+1),*((C_word*)lf[164]+1),*((C_word*)lf[143]+1));
t82=C_mutate((C_word*)lf[167]+1 /* (set! chicken.process.signal#signals-list ...) */,t81);
t83=C_mutate((C_word*)lf[134]+1 /* (set! chicken.process.signal#set-signal-mask! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5226,a[2]=((C_word)li107),tmp=(C_word)a,a+=3,tmp));
t84=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5275,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t85=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7744,a[2]=((C_word)li211),tmp=(C_word)a,a+=3,tmp);
/* posixunix.scm:573: chicken.base#getter-with-setter */
t86=*((C_word*)lf[464]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t86;
av2[1]=t84;
av2[2]=t85;
av2[3]=*((C_word*)lf[134]+1);
av2[4]=lf[478];
((C_proc)(void*)(*((C_word*)t86+1)))(5,av2);}}

/* chicken.process#process-sleep in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3861(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_3861,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3865,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:647: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t3;
av2[2]=t2;
av2[3]=lf[270];
tp(4,av2);}}

/* k3863 in chicken.process#process-sleep in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3865(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3865,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_i_process_sleep(((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.process#process-wait in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3867(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_3867,c,av);}
a=C_alloc(5);
t2=C_rest_nullp(c,2);
t3=(C_truep(t2)?C_SCHEME_FALSE:C_get_rest_arg(c,2,av,2,t0));
t4=C_rest_nullp(c,2);
t5=C_rest_nullp(c,3);
t6=(C_truep(t5)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,2,t0));
t7=C_rest_nullp(c,3);
t8=(C_truep(t3)?t3:C_fix(-1));
t9=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3886,a[2]=t8,a[3]=t6,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:654: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t9;
av2[2]=t8;
av2[3]=lf[272];
tp(4,av2);}}

/* k3884 in chicken.process#process-wait in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3886(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,5)))){
C_save_and_reclaim((void *)f_3886,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3891,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word)li43),tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3897,a[2]=((C_word*)t0)[2],a[3]=((C_word)li44),tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:655: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[4];
av2[2]=t2;
av2[3]=t3;
C_call_with_values(4,av2);}}

/* a3890 in k3884 in chicken.process#process-wait in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3891(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3891,c,av);}
/* posix-common.scm:655: process-wait-impl */
f_6991(t1,((C_word*)t0)[2],((C_word*)t0)[3]);}

/* a3896 in k3884 in chicken.process#process-wait in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3897(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_3897,c,av);}
t5=C_eqp(t2,C_fix(-1));
if(C_truep(t5)){
/* posix-common.scm:657: posix-error */
t6=lf[183];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t6;
av2[1]=t1;
av2[2]=lf[91];
av2[3]=lf[272];
av2[4]=lf[273];
av2[5]=((C_word*)t0)[2];
f_2939(6,av2);}}
else{
/* posix-common.scm:658: scheme#values */{
C_word *av2=av;
av2[0]=0;
av2[1]=t1;
av2[2]=t2;
av2[3]=t3;
av2[4]=t4;
C_values(5,av2);}}}

/* chicken.posix#list->c-string-buffer in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_3938(C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,3)))){
C_save_and_reclaim_args((void *)trf_3938,4,t1,t2,t3,t4);}
a=C_alloc(12);
t5=C_i_check_list_2(t2,t4);
t6=C_u_i_length(t2);
t7=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3946,a[2]=t1,a[3]=t6,a[4]=t4,a[5]=t3,a[6]=t2,tmp=(C_word)a,a+=7,tmp);
t8=C_a_i_fixnum_plus(&a,2,t6,C_fix(1));
/* posix-common.scm:674: chicken.memory#make-pointer-vector */
t9=*((C_word*)lf[281]+1);{
C_word av2[4];
av2[0]=t9;
av2[1]=t7;
av2[2]=t8;
av2[3]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t9+1)))(4,av2);}}

/* k3944 in chicken.posix#list->c-string-buffer in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3946(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,3)))){
C_save_and_reclaim((void *)f_3946,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3949,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3954,a[2]=t1,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word)li53),tmp=(C_word)a,a+=8,tmp);
/* posix-common.scm:676: scheme#call-with-current-continuation */
t4=*((C_word*)lf[280]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k3947 in k3944 in chicken.posix#list->c-string-buffer in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3949(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3949,c,av);}
/* posix-common.scm:672: g988 */
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)C_fast_retrieve_proc(t2))(2,av2);}}

/* a3953 in k3944 in chicken.posix#list->c-string-buffer in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3954(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(14,c,3)))){
C_save_and_reclaim((void *)f_3954,c,av);}
a=C_alloc(14);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3960,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=((C_word)li47),tmp=(C_word)a,a+=5,tmp);
t4=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_3975,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t2,a[8]=((C_word)li52),tmp=(C_word)a,a+=9,tmp);
/* posix-common.scm:676: chicken.condition#with-exception-handler */
t5=*((C_word*)lf[279]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t1;
av2[2]=t3;
av2[3]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* a3959 in a3953 in k3944 in chicken.posix#list->c-string-buffer in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3960(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_3960,c,av);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3966,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word)li46),tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:676: k985 */
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t1;
av2[2]=t3;
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}

/* a3965 in a3959 in a3953 in k3944 in chicken.posix#list->c-string-buffer in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3966(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_3966,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3970,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:678: free-c-string-buffer */
f_4055(t2,((C_word*)t0)[3]);}

/* k3968 in a3965 in a3959 in a3953 in k3944 in chicken.posix#list->c-string-buffer in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3970(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_3970,c,av);}
/* posix-common.scm:678: chicken.condition#signal */
t2=*((C_word*)lf[275]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* a3974 in a3953 in k3944 in chicken.posix#list->c-string-buffer in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3975(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(16,c,3)))){
C_save_and_reclaim((void *)f_3975,c,av);}
a=C_alloc(16);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3977,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word)li49),tmp=(C_word)a,a+=8,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4034,a[2]=((C_word*)t0)[7],a[3]=((C_word)li51),tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4051,a[2]=t3,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* tmp12525 */
t5=t2;
f_3977(t5,t4);}

/* tmp12525 in a3974 in a3953 in k3944 in chicken.posix#list->c-string-buffer in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_3977(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,0,4)))){
C_save_and_reclaim_args((void *)trf_3977,2,t0,t1);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3981,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3983,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t4,a[5]=((C_word*)t0)[2],a[6]=((C_word*)t0)[5],a[7]=((C_word)li48),tmp=(C_word)a,a+=8,tmp));
t6=((C_word*)t4)[1];
f_3983(t6,t2,((C_word*)t0)[6],C_fix(0));}

/* k3979 in tmp12525 in a3974 in a3953 in k3944 in chicken.posix#list->c-string-buffer in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3981(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3981,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* doloop993 in tmp12525 in a3974 in a3953 in k3944 in chicken.posix#list->c-string-buffer in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_3983(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,2)))){
C_save_and_reclaim_args((void *)trf_3983,4,t0,t1,t2,t3);}
a=C_alloc(12);
t4=C_i_nullp(t2);
t5=(C_truep(t4)?t4:C_eqp(t3,((C_word*)t0)[2]));
if(C_truep(t5)){
t6=C_SCHEME_UNDEFINED;
t7=t1;{
C_word av2[2];
av2[0]=t7;
av2[1]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}
else{
t6=C_i_car(t2);
t7=C_i_check_string_2(t6,((C_word*)t0)[3]);
t8=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3999,a[2]=((C_word*)t0)[4],a[3]=t1,a[4]=t2,a[5]=t3,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[3],tmp=(C_word)a,a+=8,tmp);
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4024,a[2]=t8,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:687: convert */
t10=((C_word*)t0)[6];{
C_word av2[3];
av2[0]=t10;
av2[1]=t9;
av2[2]=C_u_i_car(t2);
((C_proc)C_fast_retrieve_proc(t10))(3,av2);}}}

/* k3997 in doloop993 in tmp12525 in a3974 in a3953 in k3944 in chicken.posix#list->c-string-buffer in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_3999(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,3)))){
C_save_and_reclaim((void *)f_3999,c,av);}
a=C_alloc(13);
t2=C_a_i_bytevector(&a,1,C_fix(3));
t3=stub976(t2,t1);
t4=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4005,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t3,tmp=(C_word)a,a+=8,tmp);
if(C_truep(t3)){
t5=t4;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_UNDEFINED;
f_4005(2,av2);}}
else{
/* posix-common.scm:689: chicken.base#error */
t5=*((C_word*)lf[101]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[7];
av2[3]=lf[278];
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}}

/* k4003 in k3997 in doloop993 in tmp12525 in a3974 in a3953 in k3944 in chicken.posix#list->c-string-buffer in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4005(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_4005,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4008,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* posix-common.scm:690: chicken.memory#pointer-vector-set! */
t3=*((C_word*)lf[277]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[6];
av2[3]=((C_word*)t0)[5];
av2[4]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4006 in k4003 in k3997 in doloop993 in tmp12525 in a3974 in a3953 in k3944 in chicken.posix#list->c-string-buffer in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4008(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4008,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_3983(t2,((C_word*)t0)[3],C_u_i_cdr(((C_word*)t0)[4]),C_fixnum_plus(((C_word*)t0)[5],C_fix(1)));}

/* k4022 in doloop993 in tmp12525 in a3974 in a3953 in k3944 in chicken.posix#list->c-string-buffer in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4024(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4024,c,av);}
/* posix-common.scm:687: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[190]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[190]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
tp(4,av2);}}

/* tmp22526 in a3974 in a3953 in k3944 in chicken.posix#list->c-string-buffer in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_4034(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_4034,3,t0,t1,t2);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4040,a[2]=t2,a[3]=((C_word)li50),tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:676: k985 */
t4=((C_word*)t0)[2];{
C_word av2[3];
av2[0]=t4;
av2[1]=t1;
av2[2]=t3;
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}

/* a4039 in tmp22526 in a3974 in a3953 in k3944 in chicken.posix#list->c-string-buffer in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4040(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4040,c,av);}{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=0;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
C_apply_values(3,av2);}}

/* k4049 in a3974 in a3953 in k3944 in chicken.posix#list->c-string-buffer in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4051(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4051,c,av);}
a=C_alloc(3);
/* tmp22526 */
t2=((C_word*)t0)[2];
f_4034(t2,((C_word*)t0)[3],C_a_i_list(&a,1,t1));}

/* chicken.posix#free-c-string-buffer in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_4055(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_4055,2,t1,t2);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4059,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:695: chicken.memory#pointer-vector-length */
t4=*((C_word*)lf[284]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k4057 in chicken.posix#free-c-string-buffer in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4059(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_4059,c,av);}
a=C_alloc(8);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4064,a[2]=t1,a[3]=t3,a[4]=((C_word*)t0)[2],a[5]=((C_word)li55),tmp=(C_word)a,a+=6,tmp));
t5=((C_word*)t3)[1];
f_4064(t5,((C_word*)t0)[3],C_fix(0));}

/* doloop1012 in k4057 in chicken.posix#free-c-string-buffer in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_4064(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,3)))){
C_save_and_reclaim_args((void *)trf_4064,3,t0,t1,t2);}
a=C_alloc(5);
t3=C_eqp(t2,((C_word*)t0)[2]);
if(C_truep(t3)){
t4=C_SCHEME_UNDEFINED;
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4074,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:698: chicken.memory#pointer-vector-ref */
t5=*((C_word*)lf[283]+1);{
C_word av2[4];
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[4];
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}}

/* k4072 in doloop1012 in k4057 in chicken.posix#free-c-string-buffer in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4074(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_4074,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4077,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
if(C_truep(t1)){
/* posix-common.scm:699: chicken.memory#free */
t3=*((C_word*)lf[282]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t3=((C_word*)((C_word*)t0)[2])[1];
f_4064(t3,((C_word*)t0)[3],C_fixnum_plus(((C_word*)t0)[4],C_fix(1)));}}

/* k4075 in k4072 in doloop1012 in k4057 in chicken.posix#free-c-string-buffer in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4077(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4077,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_4064(t2,((C_word*)t0)[3],C_fixnum_plus(((C_word*)t0)[4],C_fix(1)));}

/* chicken.posix#check-environment-list in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_4089(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_4089,3,t1,t2,t3);}
a=C_alloc(8);
t4=C_i_check_list_2(t2,t3);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4094,a[2]=t3,a[3]=((C_word)li57),tmp=(C_word)a,a+=4,tmp);
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4113,a[2]=t5,a[3]=((C_word)li58),tmp=(C_word)a,a+=4,tmp);
t7=t1;{
C_word av2[2];
av2[0]=t7;
av2[1]=(
  f_4113(t6,t2)
);
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}

/* g1025 in chicken.posix#check-environment-list in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static C_word C_fcall f_4094(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_stack_overflow_check;{}
t2=C_i_check_pair_2(t1,((C_word*)t0)[2]);
t3=C_i_check_string_2(C_u_i_car(t1),((C_word*)t0)[2]);
return(C_i_check_string_2(C_u_i_cdr(t1),((C_word*)t0)[2]));}

/* for-each-loop1024 in chicken.posix#check-environment-list in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static C_word C_fcall f_4113(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_stack_overflow_check;
loop:{}
if(C_truep(C_i_pairp(t1))){
t2=(
/* posix-common.scm:704: g1025 */
  f_4094(((C_word*)t0)[2],C_slot(t1,C_fix(0)))
);
t4=C_slot(t1,C_fix(1));
t1=t4;
goto loop;}
else{
t2=C_SCHEME_UNDEFINED;
return(t2);}}

/* nop in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4136(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4136,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.posix#call-with-exec-args in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_4138(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6,C_word t7){
C_word tmp;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,2)))){
C_save_and_reclaim_args((void *)trf_4138,8,t0,t1,t2,t3,t4,t5,t6,t7);}
a=C_alloc(10);
t8=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_4142,a[2]=t5,a[3]=t1,a[4]=t7,a[5]=t3,a[6]=t2,a[7]=t6,a[8]=((C_word*)t0)[2],a[9]=t4,tmp=(C_word)a,a+=10,tmp);
/* posix-common.scm:715: pathname-strip-directory */
t9=((C_word*)t0)[3];{
C_word av2[3];
av2[0]=t9;
av2[1]=t8;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}

/* k4140 in chicken.posix#call-with-exec-args in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4142(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,4)))){
C_save_and_reclaim((void *)f_4142,c,av);}
a=C_alloc(11);
t2=C_a_i_cons(&a,2,t1,((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4148,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],tmp=(C_word)a,a+=8,tmp);
/* posix-common.scm:717: list->c-string-buffer */
f_3938(t3,t2,((C_word*)t0)[9],((C_word*)t0)[6]);}

/* k4146 in k4140 in chicken.posix#call-with-exec-args in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4148(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,3)))){
C_save_and_reclaim((void *)f_4148,c,av);}
a=C_alloc(15);
t2=C_SCHEME_FALSE;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4151,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t5=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_4156,a[2]=t3,a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word)li68),tmp=(C_word)a,a+=10,tmp);
/* posix-common.scm:720: scheme#call-with-current-continuation */
t6=*((C_word*)lf[280]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t4;
av2[2]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k4149 in k4146 in k4140 in chicken.posix#call-with-exec-args in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4151(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4151,c,av);}
/* posix-common.scm:715: g1060 */
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)C_fast_retrieve_proc(t2))(2,av2);}}

/* a4155 in k4146 in k4140 in chicken.posix#call-with-exec-args in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4156(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(17,c,3)))){
C_save_and_reclaim((void *)f_4156,c,av);}
a=C_alloc(17);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4162,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t2,a[5]=((C_word)li62),tmp=(C_word)a,a+=6,tmp);
t4=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_4183,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=t2,a[10]=((C_word)li67),tmp=(C_word)a,a+=11,tmp);
/* posix-common.scm:720: chicken.condition#with-exception-handler */
t5=*((C_word*)lf[279]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t1;
av2[2]=t3;
av2[3]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* a4161 in a4155 in k4146 in k4140 in chicken.posix#call-with-exec-args in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4162(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_4162,c,av);}
a=C_alloc(6);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4168,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word)li61),tmp=(C_word)a,a+=6,tmp);
/* posix-common.scm:720: k1057 */
t4=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t1;
av2[2]=t3;
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}

/* a4167 in a4161 in a4155 in k4146 in k4140 in chicken.posix#call-with-exec-args in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4168(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_4168,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4172,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:722: free-c-string-buffer */
f_4055(t2,((C_word*)t0)[4]);}

/* k4170 in a4167 in a4161 in a4155 in k4146 in k4140 in chicken.posix#call-with-exec-args in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4172(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_4172,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4175,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
if(C_truep(((C_word*)((C_word*)t0)[4])[1])){
/* posix-common.scm:723: free-c-string-buffer */
f_4055(t2,((C_word*)((C_word*)t0)[4])[1]);}
else{
/* posix-common.scm:724: chicken.condition#signal */
t3=*((C_word*)lf[275]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}

/* k4173 in k4170 in a4167 in a4161 in a4155 in k4146 in k4140 in chicken.posix#call-with-exec-args in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4175(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4175,c,av);}
/* posix-common.scm:724: chicken.condition#signal */
t2=*((C_word*)lf[275]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* a4182 in a4155 in k4146 in k4140 in chicken.posix#call-with-exec-args in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4183(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(14,c,3)))){
C_save_and_reclaim((void *)f_4183,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_4189,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word)li64),tmp=(C_word)a,a+=10,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4264,a[2]=((C_word*)t0)[9],a[3]=((C_word)li66),tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:720: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=t1;
av2[2]=t2;
av2[3]=t3;
C_call_with_values(4,av2);}}

/* a4188 in a4182 in a4155 in k4146 in k4140 in chicken.posix#call-with-exec-args in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4189(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(15,c,3)))){
C_save_and_reclaim((void *)f_4189,c,av);}
a=C_alloc(15);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4193,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
if(C_truep(((C_word*)t0)[7])){
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4203,a[2]=((C_word*)t0)[4],a[3]=t2,a[4]=((C_word*)t0)[7],a[5]=((C_word*)t0)[8],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* posix-common.scm:728: check-environment-list */
f_4089(t3,((C_word*)t0)[7],((C_word*)t0)[6]);}
else{
t3=t2;
f_4193(t3,C_SCHEME_UNDEFINED);}}

/* k4191 in a4188 in a4182 in a4155 in k4146 in k4140 in chicken.posix#call-with-exec-args in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_4193(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_4193,2,t0,t1);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4200,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* posix-common.scm:734: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[190]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[190]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[6];
av2[3]=((C_word*)t0)[7];
tp(4,av2);}}

/* k4198 in k4191 in a4188 in a4182 in a4155 in k4146 in k4140 in chicken.posix#call-with-exec-args in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4200(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_4200,c,av);}
/* posix-common.scm:734: proc */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
av2[3]=((C_word*)t0)[4];
av2[4]=((C_word*)((C_word*)t0)[5])[1];
((C_proc)C_fast_retrieve_proc(t2))(5,av2);}}

/* k4201 in a4188 in a4182 in a4155 in k4146 in k4140 in chicken.posix#call-with-exec-args in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4203(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(22,c,3)))){
C_save_and_reclaim((void *)f_4203,c,av);}
a=C_alloc(22);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4207,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=C_i_check_list_2(((C_word*)t0)[4],lf[288]);
t8=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4228,a[2]=t2,a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],tmp=(C_word)a,a+=5,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4230,a[2]=t5,a[3]=t10,a[4]=t6,a[5]=((C_word)li63),tmp=(C_word)a,a+=6,tmp));
t12=((C_word*)t10)[1];
f_4230(t12,t8,((C_word*)t0)[4]);}

/* k4205 in k4201 in a4188 in a4182 in a4155 in k4146 in k4140 in chicken.posix#call-with-exec-args in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4207(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4207,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=((C_word*)t0)[3];
f_4193(t3,t2);}

/* k4226 in k4201 in a4188 in a4182 in a4155 in k4146 in k4140 in chicken.posix#call-with-exec-args in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4228(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_4228,c,av);}
/* posix-common.scm:730: list->c-string-buffer */
f_3938(((C_word*)t0)[2],t1,((C_word*)t0)[3],((C_word*)t0)[4]);}

/* map-loop1068 in k4201 in a4188 in a4182 in a4155 in k4146 in k4140 in chicken.posix#call-with-exec-args in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_4230(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,4)))){
C_save_and_reclaim_args((void *)trf_4230,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4255,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
t4=C_slot(t2,C_fix(0));
/* posix-common.scm:731: scheme#string-append */
t5=*((C_word*)lf[104]+1);{
C_word av2[5];
av2[0]=t5;
av2[1]=t3;
av2[2]=C_i_car(t4);
av2[3]=lf[289];
av2[4]=C_u_i_cdr(t4);
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k4253 in map-loop1068 in k4201 in a4188 in a4182 in a4155 in k4146 in k4140 in chicken.posix#call-with-exec-args in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4255(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4255,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_4230(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* a4263 in a4182 in a4155 in k4146 in k4140 in chicken.posix#call-with-exec-args in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4264(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +4,c,2)))){
C_save_and_reclaim((void*)f_4264,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+4);
t2=C_build_rest(&a,c,2,av);
C_word t3;
C_word t4;
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4270,a[2]=t2,a[3]=((C_word)li65),tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:720: k1057 */
t4=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t1;
av2[2]=t3;
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}

/* a4269 in a4263 in a4182 in a4155 in k4146 in k4140 in chicken.posix#call-with-exec-args in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4270(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4270,c,av);}{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=0;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
C_apply_values(3,av2);}}

/* check in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_4295(C_word t1,C_word t2,C_word t3,C_word t4,C_word t5){
C_word tmp;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,5)))){
C_save_and_reclaim_args((void *)trf_4295,5,t1,t2,t3,t4,t5);}
a=C_alloc(4);
if(C_truep(C_null_pointerp(t5))){
/* posix-common.scm:746: posix-error */
t6=lf[183];{
C_word av2[6];
av2[0]=t6;
av2[1]=t1;
av2[2]=lf[188];
av2[3]=t2;
av2[4]=lf[290];
av2[5]=t3;
f_2939(6,av2);}}
else{
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4308,a[2]=t5,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
if(C_truep(t4)){
/* posix-common.scm:747: ##sys#make-port */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[239]+1));
C_word av2[6];
av2[0]=*((C_word*)lf[239]+1);
av2[1]=t6;
av2[2]=C_fix(1);
av2[3]=*((C_word*)lf[240]+1);
av2[4]=lf[291];
av2[5]=lf[230];
tp(6,av2);}}
else{
/* posix-common.scm:747: ##sys#make-port */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[239]+1));
C_word av2[6];
av2[0]=*((C_word*)lf[239]+1);
av2[1]=t6;
av2[2]=C_fix(2);
av2[3]=*((C_word*)lf[240]+1);
av2[4]=lf[291];
av2[5]=lf[230];
tp(6,av2);}}}}

/* k4306 in check in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4308(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4308,c,av);}
t2=C_set_file_ptr(t1,((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.process#open-input-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4314(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +10,c,3)))){
C_save_and_reclaim((void*)f_4314,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+10);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
t4=C_i_check_string_2(t2,lf[292]);
t5=C_i_pairp(t3);
t6=(C_truep(t5)?C_slot(t3,C_fix(0)):lf[293]);
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4328,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t8=C_eqp(t6,lf[293]);
if(C_truep(t8)){
t9=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4335,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:758: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[190]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[190]+1);
av2[1]=t9;
av2[2]=t2;
av2[3]=lf[292];
tp(4,av2);}}
else{
t9=C_eqp(t6,lf[294]);
if(C_truep(t9)){
t10=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4345,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:759: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[190]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[190]+1);
av2[1]=t10;
av2[2]=t2;
av2[3]=lf[292];
tp(4,av2);}}
else{
/* posix-common.scm:743: ##sys#error */
t10=*((C_word*)lf[95]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t10;
av2[1]=t7;
av2[2]=lf[295];
av2[3]=t6;
((C_proc)(void*)(*((C_word*)t10+1)))(4,av2);}}}}

/* k4326 in chicken.process#open-input-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4328(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_4328,c,av);}
/* posix-common.scm:754: check */
f_4295(((C_word*)t0)[3],lf[292],((C_word*)t0)[4],C_SCHEME_TRUE,t1);}

/* k4333 in chicken.process#open-input-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4335(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(2,c,5)))){
C_save_and_reclaim((void *)f_4335,c,av);}
a=C_alloc(2);
t2=open_text_input_pipe(&a,1,t1);
/* posix-common.scm:754: check */
f_4295(((C_word*)t0)[3],lf[292],((C_word*)t0)[4],C_SCHEME_TRUE,t2);}

/* k4343 in chicken.process#open-input-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4345(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(2,c,5)))){
C_save_and_reclaim((void *)f_4345,c,av);}
a=C_alloc(2);
t2=open_binary_input_pipe(&a,1,t1);
/* posix-common.scm:754: check */
f_4295(((C_word*)t0)[3],lf[292],((C_word*)t0)[4],C_SCHEME_TRUE,t2);}

/* chicken.process#open-output-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4354(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +10,c,3)))){
C_save_and_reclaim((void*)f_4354,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+10);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
t4=C_i_check_string_2(t2,lf[296]);
t5=C_i_pairp(t3);
t6=(C_truep(t5)?C_slot(t3,C_fix(0)):lf[293]);
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4368,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t8=C_eqp(t6,lf[293]);
if(C_truep(t8)){
t9=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4375,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:769: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[190]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[190]+1);
av2[1]=t9;
av2[2]=t2;
av2[3]=lf[296];
tp(4,av2);}}
else{
t9=C_eqp(t6,lf[294]);
if(C_truep(t9)){
t10=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4385,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:770: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[190]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[190]+1);
av2[1]=t10;
av2[2]=t2;
av2[3]=lf[296];
tp(4,av2);}}
else{
/* posix-common.scm:743: ##sys#error */
t10=*((C_word*)lf[95]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t10;
av2[1]=t7;
av2[2]=lf[295];
av2[3]=t6;
((C_proc)(void*)(*((C_word*)t10+1)))(4,av2);}}}}

/* k4366 in chicken.process#open-output-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4368(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_4368,c,av);}
/* posix-common.scm:765: check */
f_4295(((C_word*)t0)[3],lf[296],((C_word*)t0)[4],C_SCHEME_FALSE,t1);}

/* k4373 in chicken.process#open-output-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4375(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(2,c,5)))){
C_save_and_reclaim((void *)f_4375,c,av);}
a=C_alloc(2);
t2=open_text_output_pipe(&a,1,t1);
/* posix-common.scm:765: check */
f_4295(((C_word*)t0)[3],lf[296],((C_word*)t0)[4],C_SCHEME_FALSE,t2);}

/* k4383 in chicken.process#open-output-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4385(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(2,c,5)))){
C_save_and_reclaim((void *)f_4385,c,av);}
a=C_alloc(2);
t2=open_binary_output_pipe(&a,1,t1);
/* posix-common.scm:765: check */
f_4295(((C_word*)t0)[3],lf[296],((C_word*)t0)[4],C_SCHEME_FALSE,t2);}

/* chicken.process#close-input-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4394(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,5)))){
C_save_and_reclaim((void *)f_4394,c,av);}
a=C_alloc(4);
t3=C_i_check_port_2(t2,C_fix(1),C_SCHEME_TRUE,lf[297]);
t4=close_pipe(t2);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4401,a[2]=t1,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
t6=C_eqp(C_fix(-1),t4);
if(C_truep(t6)){
/* posix-common.scm:777: posix-error */
t7=lf[183];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t7;
av2[1]=t5;
av2[2]=lf[188];
av2[3]=lf[297];
av2[4]=lf[298];
av2[5]=t2;
f_2939(6,av2);}}
else{
t7=t1;{
C_word *av2=av;
av2[0]=t7;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}}

/* k4399 in chicken.process#close-input-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4401(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4401,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.process#close-output-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4409(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,5)))){
C_save_and_reclaim((void *)f_4409,c,av);}
a=C_alloc(4);
t3=C_i_check_port_2(t2,C_fix(2),C_SCHEME_TRUE,lf[299]);
t4=close_pipe(t2);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4416,a[2]=t1,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
t6=C_eqp(C_fix(-1),t4);
if(C_truep(t6)){
/* posix-common.scm:784: posix-error */
t7=lf[183];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t7;
av2[1]=t5;
av2[2]=lf[188];
av2[3]=lf[299];
av2[4]=lf[300];
av2[5]=t2;
f_2939(6,av2);}}
else{
t7=t1;{
C_word *av2=av;
av2[0]=t7;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}}

/* k4414 in chicken.process#close-output-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4416(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4416,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.process#with-input-from-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4424(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c<4) C_bad_min_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-4)*C_SIZEOF_PAIR +4,c,4)))){
C_save_and_reclaim((void*)f_4424,c,av);}
a=C_alloc((c-4)*C_SIZEOF_PAIR+4);
t4=C_build_rest(&a,c,4,av);
C_word t5;
C_word t6;
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4428,a[2]=t3,a[3]=t1,tmp=(C_word)a,a+=4,tmp);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=t5;
av2[2]=*((C_word*)lf[118]+1);
av2[3]=t2;
av2[4]=t4;
C_apply(5,av2);}}

/* k4426 in chicken.process#with-input-from-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4428(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(19,c,4)))){
C_save_and_reclaim((void *)f_4428,c,av);}
a=C_alloc(19);
t2=t1;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_SCHEME_FALSE;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4433,a[2]=t5,a[3]=t3,a[4]=((C_word)li75),tmp=(C_word)a,a+=5,tmp);
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4438,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word)li77),tmp=(C_word)a,a+=5,tmp);
t8=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4453,a[2]=t3,a[3]=t5,a[4]=((C_word)li78),tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:790: ##sys#dynamic-wind */
t9=*((C_word*)lf[303]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t9;
av2[1]=((C_word*)t0)[3];
av2[2]=t6;
av2[3]=t7;
av2[4]=t8;
((C_proc)(void*)(*((C_word*)t9+1)))(5,av2);}}

/* a4432 in k4426 in chicken.process#with-input-from-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4433(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4433,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,*((C_word*)lf[301]+1));
t3=C_mutate((C_word*)lf[301]+1 /* (set! ##sys#standard-input ...) */,((C_word*)((C_word*)t0)[3])[1]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* a4437 in k4426 in chicken.process#with-input-from-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4438(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_4438,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4444,a[2]=((C_word*)t0)[2],a[3]=((C_word)li76),tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:791: scheme#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=t1;
av2[2]=((C_word*)t0)[3];
av2[3]=t2;
C_call_with_values(4,av2);}}

/* a4443 in a4437 in k4426 in chicken.process#with-input-from-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4444(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +4,c,2)))){
C_save_and_reclaim((void*)f_4444,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+4);
t2=C_build_rest(&a,c,2,av);
C_word t3;
C_word t4;
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4448,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:793: chicken.process#close-input-pipe */
t4=*((C_word*)lf[115]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k4446 in a4443 in a4437 in k4426 in chicken.process#with-input-from-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4448(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4448,c,av);}{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=*((C_word*)lf[302]+1);
av2[3]=((C_word*)t0)[3];
C_apply(4,av2);}}

/* a4452 in k4426 in chicken.process#with-input-from-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4453(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4453,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,*((C_word*)lf[301]+1));
t3=C_mutate((C_word*)lf[301]+1 /* (set! ##sys#standard-input ...) */,((C_word*)((C_word*)t0)[3])[1]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* chicken.process#call-with-output-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4458(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c<4) C_bad_min_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-4)*C_SIZEOF_PAIR +4,c,4)))){
C_save_and_reclaim((void*)f_4458,c,av);}
a=C_alloc((c-4)*C_SIZEOF_PAIR+4);
t4=C_build_rest(&a,c,4,av);
C_word t5;
C_word t6;
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4462,a[2]=t3,a[3]=t1,tmp=(C_word)a,a+=4,tmp);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=t5;
av2[2]=*((C_word*)lf[119]+1);
av2[3]=t2;
av2[4]=t4;
C_apply(5,av2);}}

/* k4460 in chicken.process#call-with-output-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4462(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_4462,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4467,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word)li80),tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4473,a[2]=t1,a[3]=((C_word)li81),tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:799: scheme#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[3];
av2[2]=t2;
av2[3]=t3;
C_call_with_values(4,av2);}}

/* a4466 in k4460 in chicken.process#call-with-output-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4467(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4467,c,av);}
/* posix-common.scm:800: proc */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=((C_word*)t0)[3];
((C_proc)C_fast_retrieve_proc(t2))(3,av2);}}

/* a4472 in k4460 in chicken.process#call-with-output-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4473(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +4,c,2)))){
C_save_and_reclaim((void*)f_4473,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+4);
t2=C_build_rest(&a,c,2,av);
C_word t3;
C_word t4;
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4477,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:802: chicken.process#close-output-pipe */
t4=*((C_word*)lf[116]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k4475 in a4472 in k4460 in chicken.process#call-with-output-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4477(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4477,c,av);}{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=*((C_word*)lf[302]+1);
av2[3]=((C_word*)t0)[3];
C_apply(4,av2);}}

/* chicken.process#call-with-input-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4482(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c<4) C_bad_min_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-4)*C_SIZEOF_PAIR +4,c,4)))){
C_save_and_reclaim((void*)f_4482,c,av);}
a=C_alloc((c-4)*C_SIZEOF_PAIR+4);
t4=C_build_rest(&a,c,4,av);
C_word t5;
C_word t6;
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4486,a[2]=t3,a[3]=t1,tmp=(C_word)a,a+=4,tmp);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=t5;
av2[2]=*((C_word*)lf[118]+1);
av2[3]=t2;
av2[4]=t4;
C_apply(5,av2);}}

/* k4484 in chicken.process#call-with-input-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4486(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_4486,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4491,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word)li83),tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4497,a[2]=t1,a[3]=((C_word)li84),tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:808: scheme#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[3];
av2[2]=t2;
av2[3]=t3;
C_call_with_values(4,av2);}}

/* a4490 in k4484 in chicken.process#call-with-input-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4491(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4491,c,av);}
/* posix-common.scm:809: proc */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=((C_word*)t0)[3];
((C_proc)C_fast_retrieve_proc(t2))(3,av2);}}

/* a4496 in k4484 in chicken.process#call-with-input-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4497(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +4,c,2)))){
C_save_and_reclaim((void*)f_4497,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+4);
t2=C_build_rest(&a,c,2,av);
C_word t3;
C_word t4;
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4501,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:811: chicken.process#close-input-pipe */
t4=*((C_word*)lf[115]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k4499 in a4496 in k4484 in chicken.process#call-with-input-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4501(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4501,c,av);}{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=*((C_word*)lf[302]+1);
av2[3]=((C_word*)t0)[3];
C_apply(4,av2);}}

/* chicken.process#with-output-to-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4506(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c<4) C_bad_min_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-4)*C_SIZEOF_PAIR +4,c,4)))){
C_save_and_reclaim((void*)f_4506,c,av);}
a=C_alloc((c-4)*C_SIZEOF_PAIR+4);
t4=C_build_rest(&a,c,4,av);
C_word t5;
C_word t6;
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4510,a[2]=t3,a[3]=t1,tmp=(C_word)a,a+=4,tmp);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=t5;
av2[2]=*((C_word*)lf[119]+1);
av2[3]=t2;
av2[4]=t4;
C_apply(5,av2);}}

/* k4508 in chicken.process#with-output-to-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4510(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(19,c,4)))){
C_save_and_reclaim((void *)f_4510,c,av);}
a=C_alloc(19);
t2=t1;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_SCHEME_FALSE;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4515,a[2]=t5,a[3]=t3,a[4]=((C_word)li86),tmp=(C_word)a,a+=5,tmp);
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4520,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word)li88),tmp=(C_word)a,a+=5,tmp);
t8=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4535,a[2]=t3,a[3]=t5,a[4]=((C_word)li89),tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:817: ##sys#dynamic-wind */
t9=*((C_word*)lf[303]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t9;
av2[1]=((C_word*)t0)[3];
av2[2]=t6;
av2[3]=t7;
av2[4]=t8;
((C_proc)(void*)(*((C_word*)t9+1)))(5,av2);}}

/* a4514 in k4508 in chicken.process#with-output-to-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4515(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4515,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,*((C_word*)lf[304]+1));
t3=C_mutate((C_word*)lf[304]+1 /* (set! ##sys#standard-output ...) */,((C_word*)((C_word*)t0)[3])[1]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* a4519 in k4508 in chicken.process#with-output-to-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4520(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_4520,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4526,a[2]=((C_word*)t0)[2],a[3]=((C_word)li87),tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:818: scheme#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=t1;
av2[2]=((C_word*)t0)[3];
av2[3]=t2;
C_call_with_values(4,av2);}}

/* a4525 in a4519 in k4508 in chicken.process#with-output-to-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4526(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +4,c,2)))){
C_save_and_reclaim((void*)f_4526,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+4);
t2=C_build_rest(&a,c,2,av);
C_word t3;
C_word t4;
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4530,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:820: chicken.process#close-output-pipe */
t4=*((C_word*)lf[116]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k4528 in a4525 in a4519 in k4508 in chicken.process#with-output-to-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4530(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4530,c,av);}{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=*((C_word*)lf[302]+1);
av2[3]=((C_word*)t0)[3];
C_apply(4,av2);}}

/* a4534 in k4508 in chicken.process#with-output-to-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4535(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4535,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,*((C_word*)lf[304]+1));
t3=C_mutate((C_word*)lf[304]+1 /* (set! ##sys#standard-output ...) */,((C_word*)((C_word*)t0)[3])[1]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* ##sys#file-nonblocking! in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4540(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4540,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=stub1268(C_SCHEME_UNDEFINED,C_i_foreign_fixnum_argumentp(t2));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* ##sys#file-select-one in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4547(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4547,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=stub1273(C_SCHEME_UNDEFINED,C_i_foreign_fixnum_argumentp(t2));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.file.posix#file-control in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4587(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c<4) C_bad_min_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_4587,c,av);}
a=C_alloc(6);
t4=C_rest_nullp(c,4);
t5=(C_truep(t4)?C_fix(0):C_get_rest_arg(c,4,av,4,t0));
t6=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4594,a[2]=t2,a[3]=t3,a[4]=t5,a[5]=t1,tmp=(C_word)a,a+=6,tmp);
/* posixunix.scm:339: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t6;
av2[2]=t2;
av2[3]=lf[307];
tp(4,av2);}}

/* k4592 in chicken.file.posix#file-control in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4594(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_4594,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4597,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* posixunix.scm:340: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=lf[307];
tp(4,av2);}}

/* k4595 in k4592 in chicken.file.posix#file-control in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4597(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_4597,c,av);}
t2=C_i_foreign_fixnum_argumentp(((C_word*)t0)[2]);
t3=C_i_foreign_fixnum_argumentp(((C_word*)t0)[3]);
t4=C_fix((C_word)sizeof(long) * CHAR_BIT);
t5=C_i_foreign_ranged_integer_argumentp(((C_word*)t0)[4],t4);
t6=stub1280(C_SCHEME_UNDEFINED,t2,t3,t5);
t7=C_eqp(t6,C_fix(-1));
if(C_truep(t7)){
/* posixunix.scm:343: posix-error */
t8=lf[183];{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t8;
av2[1]=((C_word*)t0)[5];
av2[2]=lf[188];
av2[3]=lf[307];
av2[4]=lf[308];
av2[5]=((C_word*)t0)[2];
av2[6]=((C_word*)t0)[3];
f_2939(7,av2);}}
else{
t8=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t8;
av2[1]=t6;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}}

/* chicken.file.posix#file-open in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4621(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c<4) C_bad_min_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-4)*C_SIZEOF_PAIR +6,c,3)))){
C_save_and_reclaim((void*)f_4621,c,av);}
a=C_alloc((c-4)*C_SIZEOF_PAIR+6);
t4=C_build_rest(&a,c,4,av);
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
t5=C_i_pairp(t4);
t6=(C_truep(t5)?C_get_rest_arg(c,4,av,4,t0):((C_word*)t0)[2]);
t7=C_i_check_string_2(t2,lf[309]);
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4631,a[2]=t3,a[3]=t6,a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* posixunix.scm:351: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t8;
av2[2]=t3;
av2[3]=lf[309];
tp(4,av2);}}

/* k4629 in chicken.file.posix#file-open in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4631(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_4631,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4634,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* posixunix.scm:352: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=lf[309];
tp(4,av2);}}

/* k4632 in k4629 in chicken.file.posix#file-open in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4634(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_4634,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4638,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* posixunix.scm:353: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[190]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[190]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=lf[309];
tp(4,av2);}}

/* k4636 in k4632 in k4629 in chicken.file.posix#file-open in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4638(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,7)))){
C_save_and_reclaim((void *)f_4638,c,av);}
a=C_alloc(4);
t2=C_open(t1,((C_word*)t0)[2],((C_word*)t0)[3]);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4641,a[2]=((C_word*)t0)[4],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t4=C_eqp(C_fix(-1),t2);
if(C_truep(t4)){
/* posixunix.scm:355: posix-error */
t5=lf[183];{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=lf[188];
av2[3]=lf[309];
av2[4]=lf[310];
av2[5]=((C_word*)t0)[5];
av2[6]=((C_word*)t0)[2];
av2[7]=((C_word*)t0)[3];
f_2939(8,av2);}}
else{
t5=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* k4639 in k4636 in k4632 in k4629 in chicken.file.posix#file-open in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4641(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4641,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.file.posix#file-close in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4653(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_4653,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4657,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* posixunix.scm:360: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t3;
av2[2]=t2;
av2[3]=lf[312];
tp(4,av2);}}

/* k4655 in chicken.file.posix#file-close in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4657(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_4657,c,av);}
a=C_alloc(7);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4662,a[2]=((C_word*)t0)[2],a[3]=t3,a[4]=((C_word)li95),tmp=(C_word)a,a+=5,tmp));
t5=((C_word*)t3)[1];{
C_word *av2=av;
av2[0]=t5;
av2[1]=((C_word*)t0)[3];
f_4662(2,av2);}}

/* loop in k4655 in chicken.file.posix#file-close in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4662(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_4662,c,av);}
if(C_truep(C_fixnum_lessp(C_close(((C_word*)t0)[2]),C_fix(0)))){
t2=C_eqp(C_fix((C_word)errno),C_fix((C_word)EINTR));
if(C_truep(t2)){
/* posixunix.scm:364: ##sys#dispatch-interrupt */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[311]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[311]+1);
av2[1]=t1;
av2[2]=((C_word*)((C_word*)t0)[3])[1];
tp(3,av2);}}
else{
/* posixunix.scm:366: posix-error */
t3=lf[183];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=lf[188];
av2[3]=lf[312];
av2[4]=lf[313];
av2[5]=((C_word*)t0)[2];
f_2939(6,av2);}}}
else{
t2=C_SCHEME_UNDEFINED;
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* chicken.file.posix#file-read in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4684(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c<4) C_bad_min_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-4)*C_SIZEOF_PAIR +6,c,3)))){
C_save_and_reclaim((void*)f_4684,c,av);}
a=C_alloc((c-4)*C_SIZEOF_PAIR+6);
t4=C_build_rest(&a,c,4,av);
C_word t5;
C_word t6;
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4688,a[2]=t2,a[3]=t3,a[4]=t1,a[5]=t4,tmp=(C_word)a,a+=6,tmp);
/* posixunix.scm:370: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t5;
av2[2]=t2;
av2[3]=lf[314];
tp(4,av2);}}

/* k4686 in chicken.file.posix#file-read in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4688(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_4688,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4691,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* posixunix.scm:371: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=lf[314];
tp(4,av2);}}

/* k4689 in k4686 in chicken.file.posix#file-read in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4691(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_4691,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4694,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_i_pairp(((C_word*)t0)[5]))){
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_u_i_car(((C_word*)t0)[5]);
f_4694(2,av2);}}
else{
/* posixunix.scm:372: scheme#make-string */
t3=*((C_word*)lf[317]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}

/* k4692 in k4689 in k4686 in chicken.file.posix#file-read in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4694(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,5)))){
C_save_and_reclaim((void *)f_4694,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4697,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_blockp(t1))){
if(C_truep(C_byteblockp(t1))){
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_4697(2,av2);}}
else{
/* posixunix.scm:374: ##sys#signal-hook */
t3=*((C_word*)lf[90]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[191];
av2[3]=lf[314];
av2[4]=lf[316];
av2[5]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}}
else{
/* posixunix.scm:374: ##sys#signal-hook */
t3=*((C_word*)lf[90]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[191];
av2[3]=lf[314];
av2[4]=lf[316];
av2[5]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}}

/* k4695 in k4692 in k4689 in k4686 in chicken.file.posix#file-read in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4697(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,6)))){
C_save_and_reclaim((void *)f_4697,c,av);}
a=C_alloc(11);
t2=C_read(((C_word*)t0)[2],((C_word*)t0)[3],((C_word*)t0)[4]);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4700,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[3],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t4=C_eqp(C_fix(-1),t2);
if(C_truep(t4)){
/* posixunix.scm:377: posix-error */
t5=lf[183];{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=lf[188];
av2[3]=lf[314];
av2[4]=lf[315];
av2[5]=((C_word*)t0)[2];
av2[6]=((C_word*)t0)[4];
f_2939(7,av2);}}
else{
t5=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_a_i_list2(&a,2,((C_word*)t0)[3],t2);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* k4698 in k4695 in k4692 in k4689 in k4686 in chicken.file.posix#file-read in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4700(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_4700,c,av);}
a=C_alloc(6);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list2(&a,2,((C_word*)t0)[3],((C_word*)t0)[4]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.file.posix#file-write in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4724(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c<4) C_bad_min_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-4)*C_SIZEOF_PAIR +6,c,3)))){
C_save_and_reclaim((void*)f_4724,c,av);}
a=C_alloc((c-4)*C_SIZEOF_PAIR+6);
t4=C_build_rest(&a,c,4,av);
C_word t5;
C_word t6;
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4728,a[2]=t4,a[3]=t3,a[4]=t2,a[5]=t1,tmp=(C_word)a,a+=6,tmp);
/* posixunix.scm:382: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t5;
av2[2]=t2;
av2[3]=lf[318];
tp(4,av2);}}

/* k4726 in chicken.file.posix#file-write in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4728(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,5)))){
C_save_and_reclaim((void *)f_4728,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4731,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_blockp(((C_word*)t0)[3]))){
if(C_truep(C_byteblockp(((C_word*)t0)[3]))){
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_4731(2,av2);}}
else{
/* posixunix.scm:384: ##sys#signal-hook */
t3=*((C_word*)lf[90]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[191];
av2[3]=lf[318];
av2[4]=lf[320];
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}}
else{
/* posixunix.scm:384: ##sys#signal-hook */
t3=*((C_word*)lf[90]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[191];
av2[3]=lf[318];
av2[4]=lf[320];
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}}

/* k4729 in k4726 in chicken.file.posix#file-write in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4731(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_4731,c,av);}
a=C_alloc(6);
t2=C_i_pairp(((C_word*)t0)[2]);
t3=(C_truep(t2)?C_u_i_car(((C_word*)t0)[2]):C_block_size(((C_word*)t0)[3]));
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4737,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[3],a[4]=t3,a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* posixunix.scm:386: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t4;
av2[2]=t3;
av2[3]=lf[318];
tp(4,av2);}}

/* k4735 in k4729 in k4726 in chicken.file.posix#file-write in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4737(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,6)))){
C_save_and_reclaim((void *)f_4737,c,av);}
a=C_alloc(4);
t2=C_write(((C_word*)t0)[2],((C_word*)t0)[3],((C_word*)t0)[4]);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4740,a[2]=((C_word*)t0)[5],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t4=C_eqp(C_fix(-1),t2);
if(C_truep(t4)){
/* posixunix.scm:389: posix-error */
t5=lf[183];{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=lf[188];
av2[3]=lf[318];
av2[4]=lf[319];
av2[5]=((C_word*)t0)[2];
av2[6]=((C_word*)t0)[4];
f_2939(7,av2);}}
else{
t5=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* k4738 in k4735 in k4729 in k4726 in chicken.file.posix#file-write in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4740(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4740,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.file.posix#file-mkstemp in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4761(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_4761,c,av);}
a=C_alloc(4);
t3=C_i_check_string_2(t2,lf[321]);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4768,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* posixunix.scm:395: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[190]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[190]+1);
av2[1]=t4;
av2[2]=t2;
av2[3]=lf[321];
tp(4,av2);}}

/* k4766 in chicken.file.posix#file-mkstemp in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4768(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,5)))){
C_save_and_reclaim((void *)f_4768,c,av);}
a=C_alloc(6);
t2=C_mkstemp(t1);
t3=C_block_size(t1);
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4774,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=t1,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
t5=C_eqp(C_fix(-1),t2);
if(C_truep(t5)){
/* posixunix.scm:399: posix-error */
t6=lf[183];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t6;
av2[1]=t4;
av2[2]=lf[188];
av2[3]=lf[321];
av2[4]=lf[322];
av2[5]=((C_word*)t0)[3];
f_2939(6,av2);}}
else{
t6=t4;{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_SCHEME_UNDEFINED;
f_4774(2,av2);}}}

/* k4772 in k4766 in chicken.file.posix#file-mkstemp in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4774(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_4774,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4781,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* posixunix.scm:400: ##sys#substring */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[259]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[259]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=C_fix(0);
av2[4]=C_fixnum_difference(((C_word*)t0)[5],C_fix(1));
tp(5,av2);}}

/* k4779 in k4772 in k4766 in chicken.file.posix#file-mkstemp in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4781(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4781,c,av);}
/* posixunix.scm:400: scheme#values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
C_values(4,av2);}}

/* chicken.file.posix#file-select in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4793(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c<4) C_bad_min_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-4)*C_SIZEOF_PAIR +9,c,2)))){
C_save_and_reclaim((void*)f_4793,c,av);}
a=C_alloc((c-4)*C_SIZEOF_PAIR+9);
t4=C_build_rest(&a,c,4,av);
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
t5=C_i_pairp(t4);
t6=(C_truep(t5)?C_get_rest_arg(c,4,av,4,t0):C_SCHEME_FALSE);
t7=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4800,a[2]=t1,a[3]=t2,a[4]=t3,a[5]=t6,tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_i_not(t2))){
t8=t7;
f_4800(t8,C_SCHEME_END_OF_LIST);}
else{
if(C_truep(C_fixnump(t2))){
t8=t7;
f_4800(t8,C_a_i_list1(&a,1,t2));}
else{
t8=C_i_check_list_2(t2,lf[323]);
t9=t7;
f_4800(t9,t2);}}}

/* k4798 in chicken.file.posix#file-select in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_4800(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,2)))){
C_save_and_reclaim_args((void *)trf_4800,2,t0,t1);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4803,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
if(C_truep(C_i_not(((C_word*)t0)[4]))){
t3=t2;
f_4803(t3,C_SCHEME_END_OF_LIST);}
else{
if(C_truep(C_fixnump(((C_word*)t0)[4]))){
t3=t2;
f_4803(t3,C_a_i_list1(&a,1,((C_word*)t0)[4]));}
else{
t3=C_i_check_list_2(((C_word*)t0)[4],lf[323]);
t4=t2;
f_4803(t4,((C_word*)t0)[4]);}}}

/* k4801 in k4798 in chicken.file.posix#file-select in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_4803(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,2)))){
C_save_and_reclaim_args((void *)trf_4803,2,t0,t1);}
a=C_alloc(10);
t2=C_u_i_length(((C_word*)t0)[2]);
t3=C_u_i_length(t1);
t4=C_fixnum_plus(t2,t3);
t5=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_4811,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=t1,a[5]=t4,a[6]=((C_word*)t0)[3],a[7]=((C_word*)t0)[4],a[8]=((C_word*)t0)[5],a[9]=((C_word*)t0)[6],tmp=(C_word)a,a+=10,tmp);
t6=C_fix((C_word)sizeof(struct pollfd));
/* posixunix.scm:419: ##sys#make-blob */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[327]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[327]+1);
av2[1]=t5;
av2[2]=C_fixnum_times(t4,t6);
tp(3,av2);}}

/* k4809 in k4801 in k4798 in chicken.file.posix#file-select in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4811(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_4811,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_4814,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],tmp=(C_word)a,a+=11,tmp);
if(C_truep(((C_word*)t0)[9])){
/* posixunix.scm:421: ##sys#check-exact-integer */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[211]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[211]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[9];
tp(3,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_4814(2,av2);}}}

/* k4812 in k4809 in k4801 in k4798 in chicken.file.posix#file-select in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4814(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(21,c,3)))){
C_save_and_reclaim((void *)f_4814,c,av);}
a=C_alloc(21);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5086,a[2]=((C_word*)t0)[2],a[3]=((C_word)li100),tmp=(C_word)a,a+=4,tmp);
t3=(
  f_5086(t2,C_fix(0),((C_word*)t0)[3])
);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5044,a[2]=((C_word*)t0)[2],a[3]=((C_word)li101),tmp=(C_word)a,a+=4,tmp);
t5=(
  f_5044(t4,((C_word*)t0)[4],((C_word*)t0)[5])
);
t6=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_5035,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[7],a[5]=((C_word*)t0)[8],a[6]=((C_word*)t0)[9],a[7]=((C_word*)t0)[4],a[8]=((C_word*)t0)[5],a[9]=((C_word*)t0)[3],tmp=(C_word)a,a+=10,tmp);
if(C_truep(((C_word*)t0)[10])){
t7=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5042,a[2]=t6,tmp=(C_word)a,a+=3,tmp);
/* posixunix.scm:435: scheme#max */
t8=*((C_word*)lf[326]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t8;
av2[1]=t7;
av2[2]=C_fix(0);
av2[3]=((C_word*)t0)[10];
((C_proc)(void*)(*((C_word*)t8+1)))(4,av2);}}
else{
t7=t6;
f_5035(t7,C_fix(-1));}}

/* k4873 in k5033 in k4812 in k4809 in k4801 in k4798 in chicken.file.posix#file-select in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4875(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,5)))){
C_save_and_reclaim((void *)f_4875,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4878,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4915,a[2]=((C_word*)t0)[5],a[3]=t4,a[4]=((C_word)li102),tmp=(C_word)a,a+=5,tmp));
t6=((C_word*)t4)[1];
f_4915(t6,t2,((C_word*)t0)[6],C_SCHEME_END_OF_LIST,((C_word*)t0)[7]);}

/* k4876 in k4873 in k5033 in k4812 in k4809 in k4801 in k4798 in chicken.file.posix#file-select in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_4878(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_4878,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4885,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
if(C_truep(((C_word*)t0)[4])){
if(C_truep(C_fixnump(((C_word*)t0)[4]))){
t3=C_i_memq(((C_word*)t0)[4],((C_word*)t0)[5]);
t4=t2;
f_4885(t4,(C_truep(t3)?((C_word*)t0)[4]:C_SCHEME_FALSE));}
else{
t3=t2;
f_4885(t3,((C_word*)t0)[5]);}}
else{
t3=t2;
f_4885(t3,C_SCHEME_FALSE);}}

/* k4883 in k4876 in k4873 in k5033 in k4812 in k4809 in k4801 in k4798 in chicken.file.posix#file-select in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_4885(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,3)))){
C_save_and_reclaim_args((void *)trf_4885,2,t0,t1);}
if(C_truep(((C_word*)t0)[2])){
if(C_truep(C_fixnump(((C_word*)t0)[2]))){
if(C_truep(C_i_memq(((C_word*)t0)[2],((C_word*)t0)[3]))){
/* posixunix.scm:456: scheme#values */{
C_word av2[4];
av2[0]=0;
av2[1]=((C_word*)t0)[4];
av2[2]=t1;
av2[3]=((C_word*)t0)[2];
C_values(4,av2);}}
else{
/* posixunix.scm:456: scheme#values */{
C_word av2[4];
av2[0]=0;
av2[1]=((C_word*)t0)[4];
av2[2]=t1;
av2[3]=C_SCHEME_FALSE;
C_values(4,av2);}}}
else{
/* posixunix.scm:456: scheme#values */{
C_word av2[4];
av2[0]=0;
av2[1]=((C_word*)t0)[4];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
C_values(4,av2);}}}
else{
/* posixunix.scm:456: scheme#values */{
C_word av2[4];
av2[0]=0;
av2[1]=((C_word*)t0)[4];
av2[2]=t1;
av2[3]=C_SCHEME_FALSE;
C_values(4,av2);}}}

/* lp in k4873 in k5033 in k4812 in k4809 in k4801 in k4798 in chicken.file.posix#file-select in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_4915(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,4)))){
C_save_and_reclaim_args((void *)trf_4915,5,t0,t1,t2,t3,t4);}
a=C_alloc(3);
if(C_truep(C_i_nullp(t4))){
/* posixunix.scm:449: ##sys#fast-reverse */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[325]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[325]+1);
av2[1]=t1;
av2[2]=t3;
tp(3,av2);}}
else{
t5=C_i_foreign_fixnum_argumentp(t2);
t6=(C_truep(((C_word*)t0)[2])?stub1452(C_SCHEME_UNDEFINED,t5,C_i_foreign_block_argumentp(((C_word*)t0)[2])):stub1452(C_SCHEME_UNDEFINED,t5,C_SCHEME_FALSE));
if(C_truep(t6)){
t7=C_fixnum_plus(t2,C_fix(1));
t8=C_i_car(t4);
t9=C_a_i_cons(&a,2,t8,t3);
/* posixunix.scm:454: lp */
t11=t1;
t12=t7;
t13=t9;
t14=C_u_i_cdr(t4);
t1=t11;
t2=t12;
t3=t13;
t4=t14;
goto loop;}
else{
/* posixunix.scm:455: lp */
t11=t1;
t12=C_fixnum_plus(t2,C_fix(1));
t13=t3;
t14=C_i_cdr(t4);
t1=t11;
t2=t12;
t3=t13;
t4=t14;
goto loop;}}}

/* lp in k5033 in k4812 in k4809 in k4801 in k4798 in chicken.file.posix#file-select in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_4974(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,4)))){
C_save_and_reclaim_args((void *)trf_4974,5,t0,t1,t2,t3,t4);}
a=C_alloc(3);
if(C_truep(C_i_nullp(t4))){
/* posixunix.scm:441: ##sys#fast-reverse */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[325]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[325]+1);
av2[1]=t1;
av2[2]=t3;
tp(3,av2);}}
else{
t5=C_i_foreign_fixnum_argumentp(t2);
t6=(C_truep(((C_word*)t0)[2])?stub1433(C_SCHEME_UNDEFINED,t5,C_i_foreign_block_argumentp(((C_word*)t0)[2])):stub1433(C_SCHEME_UNDEFINED,t5,C_SCHEME_FALSE));
if(C_truep(t6)){
t7=C_fixnum_plus(t2,C_fix(1));
t8=C_i_car(t4);
t9=C_a_i_cons(&a,2,t8,t3);
/* posixunix.scm:446: lp */
t11=t1;
t12=t7;
t13=t9;
t14=C_u_i_cdr(t4);
t1=t11;
t2=t12;
t3=t13;
t4=t14;
goto loop;}
else{
/* posixunix.scm:447: lp */
t11=t1;
t12=C_fixnum_plus(t2,C_fix(1));
t13=t3;
t14=C_i_cdr(t4);
t1=t11;
t2=t12;
t3=t13;
t4=t14;
goto loop;}}}

/* k5033 in k4812 in k4809 in k4801 in k4798 in chicken.file.posix#file-select in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_5035(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,0,6)))){
C_save_and_reclaim_args((void *)trf_5035,2,t0,t1);}
a=C_alloc(15);
t2=(C_truep(((C_word*)t0)[2])?C_i_foreign_block_argumentp(((C_word*)t0)[2]):C_SCHEME_FALSE);
t3=C_i_foreign_fixnum_argumentp(((C_word*)t0)[3]);
t4=C_i_foreign_fixnum_argumentp(t1);
t5=stub1408(C_SCHEME_UNDEFINED,t2,t3,t4);
if(C_truep(C_fixnum_lessp(t5,C_fix(0)))){
/* posixunix.scm:437: posix-error */
t6=lf[183];{
C_word av2[7];
av2[0]=t6;
av2[1]=((C_word*)t0)[4];
av2[2]=lf[188];
av2[3]=lf[323];
av2[4]=lf[324];
av2[5]=((C_word*)t0)[5];
av2[6]=((C_word*)t0)[6];
f_2939(7,av2);}}
else{
t6=C_eqp(t5,C_fix(0));
if(C_truep(t6)){
t7=C_i_pairp(((C_word*)t0)[5]);
t8=(C_truep(t7)?C_SCHEME_END_OF_LIST:C_SCHEME_FALSE);
if(C_truep(C_i_pairp(((C_word*)t0)[6]))){
/* posixunix.scm:438: scheme#values */{
C_word av2[4];
av2[0]=0;
av2[1]=((C_word*)t0)[4];
av2[2]=t8;
av2[3]=C_SCHEME_END_OF_LIST;
C_values(4,av2);}}
else{
/* posixunix.scm:438: scheme#values */{
C_word av2[4];
av2[0]=0;
av2[1]=((C_word*)t0)[4];
av2[2]=t8;
av2[3]=C_SCHEME_FALSE;
C_values(4,av2);}}}
else{
t7=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4875,a[2]=((C_word*)t0)[6],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[2],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],tmp=(C_word)a,a+=8,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4974,a[2]=((C_word*)t0)[2],a[3]=t9,a[4]=((C_word)li103),tmp=(C_word)a,a+=5,tmp));
t11=((C_word*)t9)[1];
f_4974(t11,t7,C_fix(0),C_SCHEME_END_OF_LIST,((C_word*)t0)[9]);}}}

/* k5040 in k4812 in k4809 in k4801 in k4798 in chicken.file.posix#file-select in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5042(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(33,c,1)))){
C_save_and_reclaim((void *)f_5042,c,av);}
a=C_alloc(33);
t2=((C_word*)t0)[2];
f_5035(t2,C_s_a_i_times(&a,2,t1,C_fix(1000)));}

/* doloop1367 in k4812 in k4809 in k4801 in k4798 in chicken.file.posix#file-select in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static C_word C_fcall f_5044(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_stack_overflow_check;
loop:{}
if(C_truep(C_i_nullp(t2))){
t3=C_SCHEME_UNDEFINED;
return(t3);}
else{
t3=C_i_car(t2);
t4=C_i_foreign_fixnum_argumentp(t1);
t5=C_i_foreign_fixnum_argumentp(t3);
t6=(C_truep(((C_word*)t0)[2])?stub1393(C_SCHEME_UNDEFINED,t4,t5,C_i_foreign_block_argumentp(((C_word*)t0)[2])):stub1393(C_SCHEME_UNDEFINED,t4,t5,C_SCHEME_FALSE));
t8=C_fixnum_plus(t1,C_fix(1));
t9=C_u_i_cdr(t2);
t1=t8;
t2=t9;
goto loop;}}

/* doloop1366 in k4812 in k4809 in k4801 in k4798 in chicken.file.posix#file-select in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static C_word C_fcall f_5086(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_stack_overflow_check;
loop:{}
if(C_truep(C_i_nullp(t2))){
t3=C_SCHEME_UNDEFINED;
return(t3);}
else{
t3=C_i_car(t2);
t4=C_i_foreign_fixnum_argumentp(t1);
t5=C_i_foreign_fixnum_argumentp(t3);
t6=(C_truep(((C_word*)t0)[2])?stub1376(C_SCHEME_UNDEFINED,t4,t5,C_i_foreign_block_argumentp(((C_word*)t0)[2])):stub1376(C_SCHEME_UNDEFINED,t4,t5,C_SCHEME_FALSE));
t8=C_fixnum_plus(t1,C_fix(1));
t9=C_u_i_cdr(t2);
t1=t8;
t2=t9;
goto loop;}}

/* chicken.process#create-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5170(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_5170,c,av);}
a=C_alloc(3);
t2=C_rest_nullp(c,2);
t3=(C_truep(t2)?C_SCHEME_FALSE:C_get_rest_arg(c,2,av,2,t0));
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5177,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_fixnum_lessp(C_pipe(C_SCHEME_FALSE),C_fix(0)))){
/* posixunix.scm:469: posix-error */
t5=lf[183];{
C_word av2[5];
av2[0]=t5;
av2[1]=t4;
av2[2]=lf[188];
av2[3]=lf[328];
av2[4]=lf[329];
f_2939(5,av2);}}
else{
/* posixunix.scm:470: scheme#values */{
C_word av2[4];
av2[0]=0;
av2[1]=t1;
av2[2]=C_fix((C_word)C_pipefds[ 0 ]);
av2[3]=C_fix((C_word)C_pipefds[ 1 ]);
C_values(4,av2);}}}

/* k5175 in chicken.process#create-pipe in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5177(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5177,c,av);}
/* posixunix.scm:470: scheme#values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=C_fix((C_word)C_pipefds[ 0 ]);
av2[3]=C_fix((C_word)C_pipefds[ 1 ]);
C_values(4,av2);}}

/* chicken.process.signal#set-signal-mask! in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5226(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_5226,c,av);}
a=C_alloc(9);
t3=C_i_check_list_2(t2,lf[330]);
t4=C_sigemptyset(C_fix(0));
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5238,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5250,a[2]=t7,a[3]=((C_word)li106),tmp=(C_word)a,a+=4,tmp));
t9=((C_word*)t7)[1];
f_5250(t9,t5,t2);}

/* k5233 in for-each-loop1480 in chicken.process.signal#set-signal-mask! in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5235(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5235,c,av);}
t2=C_sigaddset(((C_word*)t0)[2]);
t3=((C_word*)((C_word*)t0)[3])[1];
f_5250(t3,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k5236 in chicken.process.signal#set-signal-mask! in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5238(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_5238,c,av);}
if(C_truep(C_fixnum_lessp(C_sigprocmask_set(C_fix(0)),C_fix(0)))){
/* posixunix.scm:570: posix-error */
t2=lf[183];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[91];
av2[3]=lf[330];
av2[4]=lf[331];
f_2939(5,av2);}}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* for-each-loop1480 in chicken.process.signal#set-signal-mask! in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_5250(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_5250,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5235,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* posixunix.scm:566: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t4;
av2[2]=t3;
av2[3]=lf[330];
tp(4,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5275(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,4)))){
C_save_and_reclaim((void *)f_5275,c,av);}
a=C_alloc(18);
t2=C_mutate((C_word*)lf[136]+1 /* (set! chicken.process.signal#signal-mask ...) */,t1);
t3=C_mutate((C_word*)lf[138]+1 /* (set! chicken.process.signal#signal-masked? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5277,a[2]=((C_word)li108),tmp=(C_word)a,a+=3,tmp));
t4=C_mutate((C_word*)lf[137]+1 /* (set! chicken.process.signal#signal-mask! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5283,a[2]=((C_word)li109),tmp=(C_word)a,a+=3,tmp));
t5=C_mutate((C_word*)lf[139]+1 /* (set! chicken.process.signal#signal-unmask! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5299,a[2]=((C_word)li110),tmp=(C_word)a,a+=3,tmp));
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5317,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t7=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7725,a[2]=((C_word)li208),tmp=(C_word)a,a+=3,tmp);
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7728,a[2]=((C_word)li209),tmp=(C_word)a,a+=3,tmp);
/* posixunix.scm:611: chicken.base#getter-with-setter */
t9=*((C_word*)lf[464]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t9;
av2[1]=t6;
av2[2]=t7;
av2[3]=t8;
av2[4]=lf[477];
((C_proc)(void*)(*((C_word*)t9+1)))(5,av2);}}

/* chicken.process.signal#signal-masked? in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5277(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_5277,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5281,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* posixunix.scm:587: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t3;
av2[2]=t2;
av2[3]=lf[332];
tp(4,av2);}}

/* k5279 in chicken.process.signal#signal-masked? in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5281(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5281,c,av);}
t2=C_sigprocmask_get(C_fix(0));
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_sigismember(((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.process.signal#signal-mask! in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5283(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_5283,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5287,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* posixunix.scm:593: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t3;
av2[2]=t2;
av2[3]=lf[333];
tp(4,av2);}}

/* k5285 in chicken.process.signal#signal-mask! in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5287(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_5287,c,av);}
t2=C_sigemptyset(C_fix(0));
t3=C_sigaddset(((C_word*)t0)[2]);
if(C_truep(C_fixnum_lessp(C_sigprocmask_block(C_fix(0)),C_fix(0)))){
/* posixunix.scm:597: posix-error */
t4=lf[183];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[91];
av2[3]=lf[333];
av2[4]=lf[334];
f_2939(5,av2);}}
else{
t4=C_SCHEME_UNDEFINED;
t5=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* chicken.process.signal#signal-unmask! in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5299(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_5299,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5303,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* posixunix.scm:601: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t3;
av2[2]=t2;
av2[3]=lf[335];
tp(4,av2);}}

/* k5301 in chicken.process.signal#signal-unmask! in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5303(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_5303,c,av);}
t2=C_sigemptyset(C_fix(0));
t3=C_sigaddset(((C_word*)t0)[2]);
if(C_truep(C_fixnum_lessp(C_sigprocmask_unblock(C_fix(0)),C_fix(0)))){
/* posixunix.scm:605: posix-error */
t4=lf[183];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[91];
av2[3]=lf[335];
av2[4]=lf[336];
f_2939(5,av2);}}
else{
t4=C_SCHEME_UNDEFINED;
t5=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5317(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_5317,c,av);}
a=C_alloc(9);
t2=C_mutate((C_word*)lf[174]+1 /* (set! chicken.process-context.posix#current-user-id ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5321,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7706,a[2]=((C_word)li206),tmp=(C_word)a,a+=3,tmp);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7709,a[2]=((C_word)li207),tmp=(C_word)a,a+=3,tmp);
/* posixunix.scm:620: chicken.base#getter-with-setter */
t6=*((C_word*)lf[464]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t6;
av2[1]=t3;
av2[2]=t4;
av2[3]=t5;
av2[4]=lf[474];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5321(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_5321,c,av);}
a=C_alloc(9);
t2=C_mutate((C_word*)lf[172]+1 /* (set! chicken.process-context.posix#current-effective-user-id ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5325,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7687,a[2]=((C_word)li204),tmp=(C_word)a,a+=3,tmp);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7690,a[2]=((C_word)li205),tmp=(C_word)a,a+=3,tmp);
/* posixunix.scm:630: chicken.base#getter-with-setter */
t6=*((C_word*)lf[464]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t6;
av2[1]=t3;
av2[2]=t4;
av2[3]=t5;
av2[4]=lf[471];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5325(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_5325,c,av);}
a=C_alloc(9);
t2=C_mutate((C_word*)lf[173]+1 /* (set! chicken.process-context.posix#current-group-id ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5329,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7668,a[2]=((C_word)li202),tmp=(C_word)a,a+=3,tmp);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7671,a[2]=((C_word)li203),tmp=(C_word)a,a+=3,tmp);
/* posixunix.scm:639: chicken.base#getter-with-setter */
t6=*((C_word*)lf[464]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t6;
av2[1]=t3;
av2[2]=t4;
av2[3]=t5;
av2[4]=lf[468];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5329(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,6)))){
C_save_and_reclaim((void *)f_5329,c,av);}
a=C_alloc(24);
t2=C_mutate((C_word*)lf[171]+1 /* (set! chicken.process-context.posix#current-effective-group-id ...) */,t1);
t3=C_mutate((C_word*)lf[181]+1 /* (set! chicken.process-context.posix#user-information ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5331,a[2]=((C_word)li111),tmp=(C_word)a,a+=3,tmp));
t4=C_mutate((C_word*)lf[177]+1 /* (set! chicken.process-context.posix#current-user-name ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5385,a[2]=((C_word)li112),tmp=(C_word)a,a+=3,tmp));
t5=C_mutate((C_word*)lf[178]+1 /* (set! chicken.process-context.posix#current-effective-user-name ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5399,a[2]=((C_word)li113),tmp=(C_word)a,a+=3,tmp));
t6=C_mutate(&lf[216] /* (set! chicken.posix#chown ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5413,a[2]=((C_word)li114),tmp=(C_word)a,a+=3,tmp));
t7=C_mutate((C_word*)lf[179]+1 /* (set! chicken.process-context.posix#create-session ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5460,a[2]=((C_word)li115),tmp=(C_word)a,a+=3,tmp));
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5477,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t9=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7628,a[2]=((C_word)li200),tmp=(C_word)a,a+=3,tmp);
t10=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7646,a[2]=((C_word)li201),tmp=(C_word)a,a+=3,tmp);
/* posixunix.scm:710: chicken.base#getter-with-setter */
t11=*((C_word*)lf[464]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t11;
av2[1]=t8;
av2[2]=t9;
av2[3]=t10;
av2[4]=lf[465];
((C_proc)(void*)(*((C_word*)t11+1)))(5,av2);}}

/* chicken.process-context.posix#user-information in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5331(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_5331,c,av);}
a=C_alloc(7);
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,3,t0));
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5338,a[2]=t4,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_fixnump(t2))){
t6=t5;
f_5338(t6,C_getpwuid(t2));}
else{
t6=C_i_check_string_2(t2,lf[340]);
t7=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5377,a[2]=t5,tmp=(C_word)a,a+=3,tmp);
/* posixunix.scm:662: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[190]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[190]+1);
av2[1]=t7;
av2[2]=t2;
av2[3]=lf[340];
tp(4,av2);}}}

/* k5336 in chicken.process-context.posix#user-information in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_5338(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,3)))){
C_save_and_reclaim_args((void *)trf_5338,2,t0,t1);}
a=C_alloc(7);
if(C_truep(t1)){
t2=(C_truep(((C_word*)t0)[2])?*((C_word*)lf[337]+1):*((C_word*)lf[338]+1));
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5351,a[2]=t2,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* ##sys#peek-nonnull-c-string */
t4=*((C_word*)lf[339]+1);{
C_word av2[4];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_mpointer(&a,(void*)C_user->pw_name);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word av2[2];
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k5349 in k5336 in chicken.process-context.posix#user-information in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5351(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_5351,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5355,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* ##sys#peek-nonnull-c-string */
t3=*((C_word*)lf[339]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_mpointer(&a,(void*)C_user->pw_passwd);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k5353 in k5349 in k5336 in chicken.process-context.posix#user-information in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5355(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_5355,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5359,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
/* ##sys#peek-nonnull-c-string */
t3=*((C_word*)lf[339]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_mpointer(&a,(void*)C_PW_GECOS);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k5357 in k5353 in k5349 in k5336 in chicken.process-context.posix#user-information in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 in ... */
static void C_ccall f_5359(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_5359,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5363,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,tmp=(C_word)a,a+=7,tmp);
/* ##sys#peek-c-string */
t3=*((C_word*)lf[185]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_mpointer(&a,(void*)C_user->pw_dir);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k5361 in k5357 in k5353 in k5349 in k5336 in chicken.process-context.posix#user-information in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in ... */
static void C_ccall f_5363(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,3)))){
C_save_and_reclaim((void *)f_5363,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_5367,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t1,tmp=(C_word)a,a+=8,tmp);
/* ##sys#peek-c-string */
t3=*((C_word*)lf[185]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_mpointer(&a,(void*)C_user->pw_shell);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k5365 in k5361 in k5357 in k5353 in k5349 in k5336 in chicken.process-context.posix#user-information in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in ... */
static void C_ccall f_5367(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,8)))){
C_save_and_reclaim((void *)f_5367,c,av);}
/* posixunix.scm:663: g1548 */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 9) {
  av2=av;
} else {
  av2=C_alloc(9);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=((C_word*)t0)[5];
av2[4]=C_fix((C_word)C_user->pw_uid);
av2[5]=C_fix((C_word)C_user->pw_gid);
av2[6]=((C_word*)t0)[6];
av2[7]=((C_word*)t0)[7];
av2[8]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(9,av2);}}

/* k5375 in chicken.process-context.posix#user-information in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5377(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5377,c,av);}
t2=((C_word*)t0)[2];
f_5338(t2,C_getpwnam(t1));}

/* chicken.process-context.posix#current-user-name in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5385(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_5385,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5393,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5397,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* posixunix.scm:676: chicken.process-context.posix#current-user-id */
t4=*((C_word*)lf[174]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k5391 in chicken.process-context.posix#current-user-name in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5393(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5393,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_i_car(t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k5395 in chicken.process-context.posix#current-user-name in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5397(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5397,c,av);}
/* posixunix.scm:675: chicken.process-context.posix#user-information */
t2=*((C_word*)lf[181]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* chicken.process-context.posix#current-effective-user-name in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5399(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_5399,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5407,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5411,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* posixunix.scm:681: chicken.process-context.posix#current-effective-user-id */
t4=*((C_word*)lf[172]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k5405 in chicken.process-context.posix#current-effective-user-name in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5407(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5407,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_i_car(t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k5409 in chicken.process-context.posix#current-effective-user-name in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5411(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5411,c,av);}
/* posixunix.scm:680: chicken.process-context.posix#user-information */
t2=*((C_word*)lf[181]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* chicken.posix#chown in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_5413(C_word t1,C_word t2,C_word t3,C_word t4,C_word t5){
C_word tmp;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,3)))){
C_save_and_reclaim_args((void *)trf_5413,5,t1,t2,t3,t4,t5);}
a=C_alloc(7);
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5417,a[2]=t1,a[3]=t2,a[4]=t3,a[5]=t4,a[6]=t5,tmp=(C_word)a,a+=7,tmp);
/* posixunix.scm:685: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t6;
av2[2]=t4;
av2[3]=t2;
tp(4,av2);}}

/* k5415 in chicken.posix#chown in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5417(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_5417,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5420,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* posixunix.scm:686: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[6];
av2[3]=((C_word*)t0)[3];
tp(4,av2);}}

/* k5418 in k5415 in chicken.posix#chown in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5420(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,2)))){
C_save_and_reclaim((void *)f_5420,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5423,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5435,a[2]=t2,a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[3],tmp=(C_word)a,a+=7,tmp);
/* posixunix.scm:688: chicken.base#port? */
t4=*((C_word*)lf[193]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k5421 in k5418 in k5415 in chicken.posix#chown in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5423(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_5423,c,av);}
if(C_truep(C_fixnum_lessp(t1,C_fix(0)))){
/* posixunix.scm:699: posix-error */
t2=lf[183];{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[188];
av2[3]=((C_word*)t0)[3];
av2[4]=lf[341];
av2[5]=((C_word*)t0)[4];
av2[6]=((C_word*)t0)[5];
av2[7]=((C_word*)t0)[6];
f_2939(8,av2);}}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k5433 in k5418 in k5415 in chicken.posix#chown in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5435(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,5)))){
C_save_and_reclaim((void *)f_5435,c,av);}
a=C_alloc(5);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5439,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* posixunix.scm:689: chicken.file.posix#port->fileno */
t3=*((C_word*)lf[73]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
if(C_truep(C_fixnump(((C_word*)t0)[5]))){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_fchown(((C_word*)t0)[5],((C_word*)t0)[3],((C_word*)t0)[4]);
f_5423(2,av2);}}
else{
if(C_truep(C_i_stringp(((C_word*)t0)[5]))){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5455,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* posixunix.scm:694: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[190]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[190]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=((C_word*)t0)[6];
tp(4,av2);}}
else{
/* posixunix.scm:695: ##sys#signal-hook */
t2=*((C_word*)lf[90]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[191];
av2[3]=((C_word*)t0)[6];
av2[4]=lf[342];
av2[5]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}}}}

/* k5437 in k5433 in k5418 in k5415 in chicken.posix#chown in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 in ... */
static void C_ccall f_5439(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5439,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_fchown(t1,((C_word*)t0)[3],((C_word*)t0)[4]);
f_5423(2,av2);}}

/* k5453 in k5433 in k5418 in k5415 in chicken.posix#chown in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 in ... */
static void C_ccall f_5455(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5455,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_chown(t1,((C_word*)t0)[3],((C_word*)t0)[4]);
f_5423(2,av2);}}

/* chicken.process-context.posix#create-session in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5460(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_5460,c,av);}
a=C_alloc(7);
t2=C_setsid(C_SCHEME_FALSE);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5464,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_fixnum_lessp(t2,C_fix(0)))){
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5470,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* posixunix.scm:705: ##sys#update-errno */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[93]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[93]+1);
av2[1]=t4;
tp(2,av2);}}
else{
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k5462 in chicken.process-context.posix#create-session in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5464(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5464,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k5468 in chicken.process-context.posix#create-session in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5470(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5470,c,av);}
/* posixunix.scm:706: ##sys#error */
t2=*((C_word*)lf[95]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[343];
av2[3]=lf[344];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5477(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_5477,c,av);}
a=C_alloc(6);
t2=C_mutate((C_word*)lf[180]+1 /* (set! chicken.process-context.posix#process-group-id ...) */,t1);
t3=C_mutate((C_word*)lf[3]+1 /* (set! chicken.file.posix#create-symbolic-link ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5479,a[2]=((C_word)li116),tmp=(C_word)a,a+=3,tmp));
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5508,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* ##sys#make-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[264]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[264]+1);
av2[1]=t4;
av2[2]=C_fixnum_plus(C_fix((C_word)FILENAME_MAX),C_fix(1));
av2[3]=C_make_character(32);
tp(4,av2);}}

/* chicken.file.posix#create-symbolic-link in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5479(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_5479,c,av);}
a=C_alloc(5);
t4=C_i_check_string_2(t2,lf[345]);
t5=C_i_check_string_2(t3,lf[345]);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5500,a[2]=t1,a[3]=t2,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* posixunix.scm:735: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[190]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[190]+1);
av2[1]=t6;
av2[2]=t2;
av2[3]=lf[345];
tp(4,av2);}}

/* k5498 in chicken.file.posix#create-symbolic-link in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5500(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_5500,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5504,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* posixunix.scm:736: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[190]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[190]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=lf[345];
tp(4,av2);}}

/* k5502 in k5498 in chicken.file.posix#create-symbolic-link in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5504(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_5504,c,av);}
if(C_truep(C_fixnum_lessp(C_symlink(((C_word*)t0)[2],t1),C_fix(0)))){
/* posixunix.scm:738: posix-error */
t2=lf[183];{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[188];
av2[3]=lf[346];
av2[4]=lf[347];
av2[5]=((C_word*)t0)[4];
av2[6]=((C_word*)t0)[5];
f_2939(7,av2);}}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5508(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word t24;
C_word t25;
C_word t26;
C_word t27;
C_word t28;
C_word t29;
C_word t30;
C_word t31;
C_word t32;
C_word t33;
C_word t34;
C_word t35;
C_word t36;
C_word t37;
C_word t38;
C_word t39;
C_word t40;
C_word t41;
C_word t42;
C_word t43;
C_word t44;
C_word t45;
C_word t46;
C_word t47;
C_word t48;
C_word t49;
C_word t50;
C_word t51;
C_word t52;
C_word t53;
C_word t54;
C_word t55;
C_word t56;
C_word t57;
C_word t58;
C_word t59;
C_word t60;
C_word t61;
C_word t62;
C_word t63;
C_word t64;
C_word t65;
C_word t66;
C_word t67;
C_word t68;
C_word t69;
C_word t70;
C_word t71;
C_word t72;
C_word t73;
C_word t74;
C_word t75;
C_word t76;
C_word t77;
C_word t78;
C_word t79;
C_word t80;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(132,c,9)))){
C_save_and_reclaim((void *)f_5508,c,av);}
a=C_alloc(132);
t2=C_mutate((C_word*)lf[348]+1 /* (set! ##sys#read-symbolic-link ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5509,a[2]=t1,a[3]=((C_word)li117),tmp=(C_word)a,a+=4,tmp));
t3=C_mutate((C_word*)lf[4]+1 /* (set! chicken.file.posix#read-symbolic-link ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5528,a[2]=((C_word)li121),tmp=(C_word)a,a+=3,tmp));
t4=C_mutate((C_word*)lf[15]+1 /* (set! chicken.file.posix#file-link ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5643,a[2]=((C_word)li122),tmp=(C_word)a,a+=3,tmp));
t5=C_mutate((C_word*)lf[362]+1 /* (set! ##sys#custom-input-port ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5665,a[2]=((C_word)li137),tmp=(C_word)a,a+=3,tmp));
t6=C_mutate((C_word*)lf[376]+1 /* (set! ##sys#custom-output-port ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6178,a[2]=((C_word)li146),tmp=(C_word)a,a+=3,tmp));
t7=C_mutate((C_word*)lf[26]+1 /* (set! chicken.file.posix#file-truncate ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6433,a[2]=((C_word)li147),tmp=(C_word)a,a+=3,tmp));
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6478,a[2]=((C_word)li148),tmp=(C_word)a,a+=3,tmp);
t9=C_mutate((C_word*)lf[16]+1 /* (set! chicken.file.posix#file-lock ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6564,a[2]=t8,a[3]=((C_word)li150),tmp=(C_word)a,a+=4,tmp));
t10=C_mutate((C_word*)lf[17]+1 /* (set! chicken.file.posix#file-lock/blocking ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6595,a[2]=t8,a[3]=((C_word)li152),tmp=(C_word)a,a+=4,tmp));
t11=C_mutate((C_word*)lf[25]+1 /* (set! chicken.file.posix#file-test-lock ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6626,a[2]=t8,a[3]=((C_word)li153),tmp=(C_word)a,a+=4,tmp));
t12=C_mutate((C_word*)lf[27]+1 /* (set! chicken.file.posix#file-unlock ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6653,a[2]=((C_word)li155),tmp=(C_word)a,a+=3,tmp));
t13=C_mutate((C_word*)lf[2]+1 /* (set! chicken.file.posix#create-fifo ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6696,a[2]=((C_word)li156),tmp=(C_word)a,a+=3,tmp));
t14=C_fix((C_word)sizeof(struct tm));
t15=C_mutate((C_word*)lf[84]+1 /* (set! chicken.time.posix#string->time ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6742,a[2]=t14,a[3]=((C_word)li157),tmp=(C_word)a,a+=4,tmp));
t16=C_fix((C_word)sizeof(struct tm));
t17=C_mutate((C_word*)lf[80]+1 /* (set! chicken.time.posix#utc-time->seconds ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6779,a[2]=t16,a[3]=((C_word)li158),tmp=(C_word)a,a+=4,tmp));
t18=C_mutate((C_word*)lf[86]+1 /* (set! chicken.time.posix#local-timezone-abbreviation ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6798,a[2]=((C_word)li159),tmp=(C_word)a,a+=3,tmp));
t19=C_mutate((C_word*)lf[132]+1 /* (set! chicken.process.signal#set-alarm! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6805,a[2]=((C_word)li160),tmp=(C_word)a,a+=3,tmp));
t20=C_mutate((C_word*)lf[108]+1 /* (set! chicken.process#process-fork ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6814,a[2]=((C_word)li164),tmp=(C_word)a,a+=3,tmp));
t21=C_mutate((C_word*)lf[107]+1 /* (set! chicken.process#process-execute ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6913,a[2]=((C_word)li167),tmp=(C_word)a,a+=3,tmp));
t22=C_mutate(&lf[271] /* (set! chicken.posix#process-wait-impl ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6991,a[2]=((C_word)li169),tmp=(C_word)a,a+=3,tmp));
t23=C_mutate((C_word*)lf[176]+1 /* (set! chicken.process-context.posix#parent-process-id ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7029,a[2]=((C_word)li170),tmp=(C_word)a,a+=3,tmp));
t24=C_mutate((C_word*)lf[110]+1 /* (set! chicken.process#process-signal ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7032,a[2]=((C_word)li171),tmp=(C_word)a,a+=3,tmp));
t25=C_mutate((C_word*)lf[109]+1 /* (set! chicken.process#process-run ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7072,a[2]=((C_word)li172),tmp=(C_word)a,a+=3,tmp));
t26=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7126,a[2]=((C_word)li176),tmp=(C_word)a,a+=3,tmp);
t27=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7163,a[2]=((C_word)li179),tmp=(C_word)a,a+=3,tmp);
t28=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7183,a[2]=((C_word)li180),tmp=(C_word)a,a+=3,tmp);
t29=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7195,a[2]=((C_word)li181),tmp=(C_word)a,a+=3,tmp);
t30=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7224,a[2]=t29,a[3]=t27,a[4]=((C_word)li183),tmp=(C_word)a,a+=5,tmp);
t31=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7269,a[2]=t28,a[3]=((C_word)li184),tmp=(C_word)a,a+=4,tmp);
t32=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7280,a[2]=t28,a[3]=((C_word)li185),tmp=(C_word)a,a+=4,tmp);
t33=C_mutate(&lf[412] /* (set! chicken.posix#process-impl ...) */,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7291,a[2]=t30,a[3]=t31,a[4]=t26,a[5]=t32,a[6]=((C_word)li188),tmp=(C_word)a,a+=7,tmp));
t34=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7347,a[2]=((C_word)li193),tmp=(C_word)a,a+=3,tmp);
t35=C_mutate((C_word*)lf[122]+1 /* (set! chicken.process#process ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7418,a[2]=t34,a[3]=((C_word)li195),tmp=(C_word)a,a+=4,tmp));
t36=C_mutate((C_word*)lf[123]+1 /* (set! chicken.process#process* ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7484,a[2]=t34,a[3]=((C_word)li196),tmp=(C_word)a,a+=4,tmp));
t37=C_mutate((C_word*)lf[170]+1 /* (set! chicken.process-context.posix#set-root-directory! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7554,a[2]=((C_word)li197),tmp=(C_word)a,a+=3,tmp));
t38=C_mutate((C_word*)lf[111]+1 /* (set! chicken.process#process-spawn ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7573,a[2]=((C_word)li198),tmp=(C_word)a,a+=3,tmp));
t39=C_a_i_provide(&a,1,lf[418]);
t40=C_mutate((C_word*)lf[419]+1 /* (set! chicken.errno#errno ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7579,a[2]=((C_word)li199),tmp=(C_word)a,a+=3,tmp));
t41=C_set_block_item(lf[421] /* chicken.errno#errno/2big */,0,C_fix((C_word)E2BIG));
t42=C_set_block_item(lf[422] /* chicken.errno#errno/acces */,0,C_fix((C_word)EACCES));
t43=C_set_block_item(lf[423] /* chicken.errno#errno/again */,0,C_fix((C_word)EAGAIN));
t44=C_set_block_item(lf[424] /* chicken.errno#errno/badf */,0,C_fix((C_word)EBADF));
t45=C_set_block_item(lf[425] /* chicken.errno#errno/busy */,0,C_fix((C_word)EBUSY));
t46=C_set_block_item(lf[426] /* chicken.errno#errno/child */,0,C_fix((C_word)ECHILD));
t47=C_set_block_item(lf[427] /* chicken.errno#errno/deadlk */,0,C_fix((C_word)EDEADLK));
t48=C_set_block_item(lf[428] /* chicken.errno#errno/dom */,0,C_fix((C_word)EDOM));
t49=C_set_block_item(lf[429] /* chicken.errno#errno/exist */,0,C_fix((C_word)EEXIST));
t50=C_set_block_item(lf[430] /* chicken.errno#errno/fault */,0,C_fix((C_word)EFAULT));
t51=C_set_block_item(lf[431] /* chicken.errno#errno/fbig */,0,C_fix((C_word)EFBIG));
t52=C_set_block_item(lf[432] /* chicken.errno#errno/ilseq */,0,C_fix((C_word)EILSEQ));
t53=C_set_block_item(lf[433] /* chicken.errno#errno/intr */,0,C_fix((C_word)EINTR));
t54=C_set_block_item(lf[434] /* chicken.errno#errno/inval */,0,C_fix((C_word)EINVAL));
t55=C_set_block_item(lf[435] /* chicken.errno#errno/io */,0,C_fix((C_word)EIO));
t56=C_set_block_item(lf[436] /* chicken.errno#errno/isdir */,0,C_fix((C_word)EISDIR));
t57=C_set_block_item(lf[437] /* chicken.errno#errno/mfile */,0,C_fix((C_word)EMFILE));
t58=C_set_block_item(lf[438] /* chicken.errno#errno/mlink */,0,C_fix((C_word)EMLINK));
t59=C_set_block_item(lf[439] /* chicken.errno#errno/nametoolong */,0,C_fix((C_word)ENAMETOOLONG));
t60=C_set_block_item(lf[440] /* chicken.errno#errno/nfile */,0,C_fix((C_word)ENFILE));
t61=C_set_block_item(lf[441] /* chicken.errno#errno/nodev */,0,C_fix((C_word)ENODEV));
t62=C_set_block_item(lf[442] /* chicken.errno#errno/noent */,0,C_fix((C_word)ENOENT));
t63=C_set_block_item(lf[443] /* chicken.errno#errno/noexec */,0,C_fix((C_word)ENOEXEC));
t64=C_set_block_item(lf[444] /* chicken.errno#errno/nolck */,0,C_fix((C_word)ENOLCK));
t65=C_set_block_item(lf[445] /* chicken.errno#errno/nomem */,0,C_fix((C_word)ENOMEM));
t66=C_set_block_item(lf[446] /* chicken.errno#errno/nospc */,0,C_fix((C_word)ENOSPC));
t67=C_set_block_item(lf[447] /* chicken.errno#errno/nosys */,0,C_fix((C_word)ENOSYS));
t68=C_set_block_item(lf[448] /* chicken.errno#errno/notdir */,0,C_fix((C_word)ENOTDIR));
t69=C_set_block_item(lf[449] /* chicken.errno#errno/notempty */,0,C_fix((C_word)ENOTEMPTY));
t70=C_set_block_item(lf[450] /* chicken.errno#errno/notty */,0,C_fix((C_word)ENOTTY));
t71=C_set_block_item(lf[451] /* chicken.errno#errno/nxio */,0,C_fix((C_word)ENXIO));
t72=C_set_block_item(lf[452] /* chicken.errno#errno/perm */,0,C_fix((C_word)EPERM));
t73=C_set_block_item(lf[453] /* chicken.errno#errno/pipe */,0,C_fix((C_word)EPIPE));
t74=C_set_block_item(lf[454] /* chicken.errno#errno/range */,0,C_fix((C_word)ERANGE));
t75=C_set_block_item(lf[455] /* chicken.errno#errno/rofs */,0,C_fix((C_word)EROFS));
t76=C_set_block_item(lf[456] /* chicken.errno#errno/spipe */,0,C_fix((C_word)ESPIPE));
t77=C_set_block_item(lf[457] /* chicken.errno#errno/srch */,0,C_fix((C_word)ESRCH));
t78=C_set_block_item(lf[458] /* chicken.errno#errno/wouldblock */,0,C_fix((C_word)EWOULDBLOCK));
t79=C_set_block_item(lf[459] /* chicken.errno#errno/xdev */,0,C_fix((C_word)EXDEV));
t80=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t80;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t80+1)))(2,av2);}}

/* ##sys#read-symbolic-link in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5509(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_5509,c,av);}
a=C_alloc(6);
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5514,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t3,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* posixunix.scm:747: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[190]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[190]+1);
av2[1]=t4;
av2[2]=t2;
av2[3]=t3;
tp(4,av2);}}

/* k5512 in ##sys#read-symbolic-link in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5514(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_5514,c,av);}
t2=C_do_readlink(t1,((C_word*)t0)[2]);
if(C_truep(C_fixnum_lessp(t2,C_fix(0)))){
/* posixunix.scm:749: posix-error */
t3=lf[183];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[188];
av2[3]=((C_word*)t0)[4];
av2[4]=lf[349];
av2[5]=((C_word*)t0)[5];
f_2939(6,av2);}}
else{
/* posixunix.scm:750: scheme#substring */
t3=*((C_word*)lf[350]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[2];
av2[3]=C_fix(0);
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}}

/* chicken.file.posix#read-symbolic-link in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5528(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(7,c,5)))){
C_save_and_reclaim((void *)f_5528,c,av);}
a=C_alloc(7);
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,3,t0));
t5=C_i_check_string_2(t2,lf[351]);
if(C_truep(t4)){
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5543,a[2]=t2,a[3]=((C_word)li118),tmp=(C_word)a,a+=4,tmp);
t7=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5549,a[2]=((C_word)li120),tmp=(C_word)a,a+=3,tmp);
/* posixunix.scm:756: ##sys#call-with-values */{
C_word av2[4];
av2[0]=0;
av2[1]=t1;
av2[2]=t6;
av2[3]=t7;
C_call_with_values(4,av2);}}
else{
/* posixunix.scm:771: ##sys#read-symbolic-link */
t6=*((C_word*)lf[348]+1);{
C_word av2[4];
av2[0]=t6;
av2[1]=t1;
av2[2]=t2;
av2[3]=lf[351];
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}}

/* a5542 in chicken.file.posix#read-symbolic-link in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5543(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5543,c,av);}
/* posixunix.scm:756: chicken.pathname#decompose-directory */
t2=*((C_word*)lf[352]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* a5548 in chicken.file.posix#read-symbolic-link in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5549(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_5549,c,av);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5557,a[2]=t1,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
t6=(C_truep(t2)?t2:lf[357]);
if(C_truep(t3)){
/* posixunix.scm:758: scheme#string-append */
t7=*((C_word*)lf[104]+1);{
C_word *av2=av;
av2[0]=t7;
av2[1]=t5;
av2[2]=t6;
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}
else{
/* posixunix.scm:758: scheme#string-append */
t7=*((C_word*)lf[104]+1);{
C_word *av2=av;
av2[0]=t7;
av2[1]=t5;
av2[2]=t6;
av2[3]=lf[358];
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}}

/* k5555 in a5548 in chicken.file.posix#read-symbolic-link in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 in ... */
static void C_ccall f_5557(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_5557,c,av);}
a=C_alloc(6);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5559,a[2]=t3,a[3]=((C_word)li119),tmp=(C_word)a,a+=4,tmp));
t5=((C_word*)t3)[1];
f_5559(t5,((C_word*)t0)[2],((C_word*)t0)[3],t1);}

/* loop in k5555 in a5548 in chicken.file.posix#read-symbolic-link in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in ... */
static void C_fcall f_5559(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_5559,4,t0,t1,t2,t3);}
a=C_alloc(6);
if(C_truep(C_i_nullp(t2))){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5569,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=t1,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* posixunix.scm:761: chicken.pathname#make-pathname */
t5=*((C_word*)lf[353]+1);{
C_word av2[4];
av2[0]=t5;
av2[1]=t4;
av2[2]=t3;
av2[3]=C_i_car(t2);
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}}

/* k5567 in loop in k5555 in a5548 in chicken.file.posix#read-symbolic-link in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in ... */
static void C_ccall f_5569(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,5)))){
C_save_and_reclaim((void *)f_5569,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5575,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,tmp=(C_word)a,a+=7,tmp);
/* posixunix.scm:762: ##sys#file-exists? */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[356]+1));
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=*((C_word*)lf[356]+1);
av2[1]=t2;
av2[2]=t1;
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_FALSE;
av2[5]=lf[351];
tp(6,av2);}}

/* k5573 in k5567 in loop in k5555 in a5548 in chicken.file.posix#read-symbolic-link in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in ... */
static void C_ccall f_5575(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,5)))){
C_save_and_reclaim((void *)f_5575,c,av);}
a=C_alloc(13);
if(C_truep(t1)){
t2=C_u_i_cdr(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5584,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t4=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_5587,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t2,a[5]=t3,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* posixunix.scm:764: chicken.file.posix#symbolic-link? */
t5=*((C_word*)lf[36]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}
else{
/* posixunix.scm:770: ##sys#signal-hook */
t2=*((C_word*)lf[90]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[4];
av2[2]=lf[188];
av2[3]=lf[351];
av2[4]=lf[355];
av2[5]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}}

/* k5582 in k5573 in k5567 in loop in k5555 in a5548 in chicken.file.posix#read-symbolic-link in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in ... */
static void C_ccall f_5584(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5584,c,av);}
/* posixunix.scm:763: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_5559(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* k5585 in k5573 in k5567 in loop in k5555 in a5548 in chicken.file.posix#read-symbolic-link in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in ... */
static void C_ccall f_5587(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_5587,c,av);}
a=C_alloc(7);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5590,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* posixunix.scm:765: ##sys#read-symbolic-link */
t3=*((C_word*)lf[348]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[7];
av2[3]=lf[351];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}
else{
/* posixunix.scm:763: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_5559(t2,((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[7]);}}

/* k5588 in k5585 in k5573 in k5567 in loop in k5555 in a5548 in chicken.file.posix#read-symbolic-link in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in ... */
static void C_ccall f_5590(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_5590,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_5596,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* posixunix.scm:766: chicken.pathname#absolute-pathname? */
t3=*((C_word*)lf[354]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k5594 in k5588 in k5585 in k5573 in k5567 in loop in k5555 in a5548 in chicken.file.posix#read-symbolic-link in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in ... */
static void C_ccall f_5596(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5596,c,av);}
if(C_truep(t1)){
/* posixunix.scm:763: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_5559(t2,((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5]);}
else{
/* posixunix.scm:768: chicken.pathname#make-pathname */
t2=*((C_word*)lf[353]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[6];
av2[2]=((C_word*)t0)[7];
av2[3]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}}

/* k5628 in chicken.file.posix#file-link in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5630(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_5630,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5634,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* posixunix.scm:774: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[190]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[190]+1);
av2[1]=t2;
av2[2]=C_i_foreign_string_argumentp(((C_word*)t0)[4]);
tp(3,av2);}}

/* k5632 in k5628 in chicken.file.posix#file-link in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 in ... */
static void C_ccall f_5634(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_5634,c,av);}
if(C_truep(C_fixnum_lessp(stub1614(C_SCHEME_UNDEFINED,((C_word*)t0)[2],t1),C_fix(0)))){
/* posixunix.scm:779: posix-error */
t2=lf[183];{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[188];
av2[3]=lf[360];
av2[4]=lf[361];
av2[5]=((C_word*)t0)[4];
av2[6]=((C_word*)t0)[5];
f_2939(7,av2);}}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* chicken.file.posix#file-link in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5643(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_5643,c,av);}
a=C_alloc(5);
t4=C_i_check_string_2(t2,lf[359]);
t5=C_i_check_string_2(t3,lf[359]);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5630,a[2]=t1,a[3]=t2,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* posixunix.scm:774: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[190]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[190]+1);
av2[1]=t6;
av2[2]=C_i_foreign_string_argumentp(t2);
tp(3,av2);}}

/* ##sys#custom-input-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5665(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word *a;
if(c<5) C_bad_min_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_5665,c,av);}
a=C_alloc(9);
t5=C_rest_nullp(c,5);
t6=(C_truep(t5)?C_SCHEME_FALSE:C_get_rest_arg(c,5,av,5,t0));
t7=C_rest_nullp(c,5);
t8=C_rest_nullp(c,6);
t9=(C_truep(t8)?C_fix(1):C_get_rest_arg(c,6,av,5,t0));
t10=C_rest_nullp(c,6);
t11=C_rest_nullp(c,7);
t12=(C_truep(t11)?*((C_word*)lf[363]+1):C_get_rest_arg(c,7,av,5,t0));
t13=C_rest_nullp(c,7);
t14=C_rest_nullp(c,8);
t15=(C_truep(t14)?C_SCHEME_FALSE:C_get_rest_arg(c,8,av,5,t0));
t16=C_rest_nullp(c,8);
t17=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_5693,a[2]=t9,a[3]=t2,a[4]=t4,a[5]=t3,a[6]=t15,a[7]=t1,a[8]=t12,tmp=(C_word)a,a+=9,tmp);
if(C_truep(t6)){
/* posixunix.scm:784: ##sys#file-nonblocking! */
t18=*((C_word*)lf[305]+1);{
C_word *av2=av;
av2[0]=t18;
av2[1]=t17;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t18+1)))(3,av2);}}
else{
t18=t17;{
C_word *av2=av;
av2[0]=t18;
av2[1]=C_SCHEME_UNDEFINED;
f_5693(2,av2);}}}

/* k5691 in ##sys#custom-input-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_5693(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_5693,c,av);}
a=C_alloc(9);
t2=C_fixnump(((C_word*)t0)[2]);
t3=(C_truep(t2)?((C_word*)t0)[2]:C_block_size(((C_word*)t0)[2]));
t4=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_5699,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=t3,a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
if(C_truep(C_fixnump(((C_word*)t0)[2]))){
/* posixunix.scm:786: ##sys#make-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[264]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[264]+1);
av2[1]=t4;
av2[2]=((C_word*)t0)[2];
tp(3,av2);}}
else{
t5=t4;{
C_word *av2=av;
av2[0]=t5;
av2[1]=((C_word*)t0)[2];
f_5699(2,av2);}}}

/* k5697 in k5691 in ##sys#custom-input-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 in ... */
static void C_ccall f_5699(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(80,c,8)))){
C_save_and_reclaim((void *)f_5699,c,av);}
a=C_alloc(80);
t2=C_fix(0);
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_fix(0);
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5700,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word)li123),tmp=(C_word)a,a+=6,tmp);
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5729,a[2]=t5,a[3]=t3,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t8=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_5737,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[2],a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[6],a[8]=t3,a[9]=t5,a[10]=((C_word)li125),tmp=(C_word)a,a+=11,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5833,a[2]=t10,a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[7],tmp=(C_word)a,a+=5,tmp);
t12=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5839,a[2]=t7,a[3]=t5,a[4]=t3,a[5]=t8,a[6]=((C_word)li126),tmp=(C_word)a,a+=7,tmp);
t13=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5864,a[2]=t5,a[3]=t3,a[4]=t6,a[5]=((C_word)li127),tmp=(C_word)a,a+=6,tmp);
t14=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5876,a[2]=((C_word*)t0)[8],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[4],a[6]=((C_word)li128),tmp=(C_word)a,a+=7,tmp);
t15=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5892,a[2]=t7,a[3]=t5,a[4]=t3,a[5]=t8,a[6]=((C_word)li129),tmp=(C_word)a,a+=7,tmp);
t16=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5907,a[2]=t5,a[3]=t3,a[4]=t1,a[5]=t8,a[6]=((C_word)li131),tmp=(C_word)a,a+=7,tmp);
t17=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5983,a[2]=t5,a[3]=t3,a[4]=t1,a[5]=t8,a[6]=((C_word)li135),tmp=(C_word)a,a+=7,tmp);
t18=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6102,a[2]=t5,a[3]=t3,a[4]=t1,a[5]=((C_word)li136),tmp=(C_word)a,a+=6,tmp);
/* posixunix.scm:836: chicken.port#make-input-port */
t19=*((C_word*)lf[375]+1);{
C_word *av2;
if(c >= 9) {
  av2=av;
} else {
  av2=C_alloc(9);
}
av2[0]=t19;
av2[1]=t11;
av2[2]=t12;
av2[3]=t13;
av2[4]=t14;
av2[5]=t15;
av2[6]=t16;
av2[7]=t17;
av2[8]=t18;
((C_proc)(void*)(*((C_word*)t19+1)))(9,av2);}}

/* ready? in k5697 in k5691 in ##sys#custom-input-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in ... */
static void C_fcall f_5700(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_5700,2,t0,t1);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5704,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* posixunix.scm:791: ##sys#file-select-one */
t3=*((C_word*)lf[306]+1);{
C_word av2[3];
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k5702 in ready? in k5697 in k5691 in ##sys#custom-input-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in ... */
static void C_ccall f_5704(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_5704,c,av);}
t2=C_eqp(C_fix(-1),t1);
if(C_truep(t2)){
t3=C_eqp(C_fix((C_word)errno),C_fix((C_word)EWOULDBLOCK));
t4=(C_truep(t3)?t3:C_eqp(C_fix((C_word)errno),C_fix((C_word)EAGAIN)));
if(C_truep(t4)){
t5=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
/* posixunix.scm:796: posix-error */
t5=lf[183];{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t5;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[188];
av2[3]=((C_word*)t0)[3];
av2[4]=lf[364];
av2[5]=((C_word*)t0)[4];
av2[6]=((C_word*)t0)[5];
f_2939(7,av2);}}}
else{
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_eqp(C_fix(1),t1);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* peek in k5697 in k5691 in ##sys#custom-input-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in ... */
static C_word C_fcall f_5729(C_word t0){
C_word tmp;
C_word t1;
C_word t2;
C_stack_overflow_check;{}
t1=C_fixnum_greater_or_equal_p(((C_word*)((C_word*)t0)[2])[1],((C_word*)((C_word*)t0)[3])[1]);
return((C_truep(t1)?C_SCHEME_END_OF_FILE:C_subchar(((C_word*)t0)[4],((C_word*)((C_word*)t0)[2])[1])));}

/* fetch in k5697 in k5691 in ##sys#custom-input-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in ... */
static void C_fcall f_5737(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,0,2)))){
C_save_and_reclaim_args((void *)trf_5737,2,t0,t1);}
a=C_alloc(14);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_5743,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t3,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word)li124),tmp=(C_word)a,a+=12,tmp));
t5=((C_word*)t3)[1];{
C_word av2[2];
av2[0]=t5;
av2[1]=t1;
f_5743(2,av2);}}

/* loop in fetch in k5697 in k5691 in ##sys#custom-input-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in ... */
static void C_ccall f_5743(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(11,c,6)))){
C_save_and_reclaim((void *)f_5743,c,av);}
a=C_alloc(11);
t2=C_read(((C_word*)t0)[2],((C_word*)t0)[3],((C_word*)t0)[4]);
t3=C_eqp(t2,C_fix(-1));
if(C_truep(t3)){
t4=C_eqp(C_fix((C_word)errno),C_fix((C_word)EWOULDBLOCK));
t5=(C_truep(t4)?t4:C_eqp(C_fix((C_word)errno),C_fix((C_word)EAGAIN)));
if(C_truep(t5)){
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5762,a[2]=((C_word*)t0)[5],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* posixunix.scm:811: ##sys#thread-block-for-i/o! */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[366]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[366]+1);
av2[1]=t6;
av2[2]=*((C_word*)lf[367]+1);
av2[3]=((C_word*)t0)[2];
av2[4]=lf[368];
tp(5,av2);}}
else{
t6=C_eqp(C_fix((C_word)errno),C_fix((C_word)EINTR));
if(C_truep(t6)){
/* posixunix.scm:815: ##sys#dispatch-interrupt */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[311]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[311]+1);
av2[1]=t1;
av2[2]=((C_word*)((C_word*)t0)[5])[1];
tp(3,av2);}}
else{
/* posixunix.scm:816: posix-error */
t7=lf[183];{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t7;
av2[1]=t1;
av2[2]=lf[188];
av2[3]=((C_word*)t0)[6];
av2[4]=lf[369];
av2[5]=((C_word*)t0)[2];
av2[6]=((C_word*)t0)[7];
f_2939(7,av2);}}}}
else{
t4=(C_truep(((C_word*)t0)[8])?C_eqp(t2,C_fix(0)):C_SCHEME_FALSE);
if(C_truep(t4)){
t5=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_5795,a[2]=((C_word*)t0)[5],a[3]=t1,a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[9],a[8]=((C_word*)t0)[10],a[9]=((C_word*)t0)[6],a[10]=((C_word*)t0)[7],tmp=(C_word)a,a+=11,tmp);
/* posixunix.scm:820: more? */
t6=((C_word*)t0)[8];{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
((C_proc)C_fast_retrieve_proc(t6))(2,av2);}}
else{
t5=C_mutate(((C_word *)((C_word*)t0)[9])+1,t2);
t6=C_set_block_item(((C_word*)t0)[10],0,C_fix(0));
t7=t1;{
C_word *av2=av;
av2[0]=t7;
av2[1]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}}}

/* k5760 in loop in fetch in k5697 in k5691 in ##sys#custom-input-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in ... */
static void C_ccall f_5762(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_5762,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5765,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* posixunix.scm:812: ##sys#thread-yield! */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[365]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[365]+1);
av2[1]=t2;
tp(2,av2);}}

/* k5763 in k5760 in loop in fetch in k5697 in k5691 in ##sys#custom-input-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in ... */
static void C_ccall f_5765(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5765,c,av);}
/* posixunix.scm:813: loop */
t2=((C_word*)((C_word*)t0)[2])[1];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
f_5743(2,av2);}}

/* k5793 in loop in fetch in k5697 in k5691 in ##sys#custom-input-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in ... */
static void C_ccall f_5795(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,6)))){
C_save_and_reclaim((void *)f_5795,c,av);}
a=C_alloc(8);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5798,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* posixunix.scm:822: ##sys#thread-yield! */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[365]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[365]+1);
av2[1]=t2;
tp(2,av2);}}
else{
t2=C_read(((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6]);
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5804,a[2]=((C_word*)t0)[7],a[3]=t3,a[4]=((C_word*)t0)[8],a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
t5=C_eqp(((C_word*)t3)[1],C_fix(-1));
if(C_truep(t5)){
t6=C_eqp(C_fix((C_word)errno),C_fix((C_word)EWOULDBLOCK));
t7=(C_truep(t6)?t6:C_eqp(C_fix((C_word)errno),C_fix((C_word)EAGAIN)));
if(C_truep(t7)){
t8=C_set_block_item(t3,0,C_fix(0));
t9=C_mutate(((C_word *)((C_word*)t0)[7])+1,((C_word*)t3)[1]);
t10=C_set_block_item(((C_word*)t0)[8],0,C_fix(0));
t11=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t11;
av2[1]=t10;
((C_proc)(void*)(*((C_word*)t11+1)))(2,av2);}}
else{
/* posixunix.scm:829: posix-error */
t8=lf[183];{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t8;
av2[1]=t4;
av2[2]=lf[188];
av2[3]=((C_word*)t0)[9];
av2[4]=lf[370];
av2[5]=((C_word*)t0)[4];
av2[6]=((C_word*)t0)[10];
f_2939(7,av2);}}}
else{
t6=C_mutate(((C_word *)((C_word*)t0)[7])+1,((C_word*)t3)[1]);
t7=C_set_block_item(((C_word*)t0)[8],0,C_fix(0));
t8=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t8;
av2[1]=t7;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}}}

/* k5796 in k5793 in loop in fetch in k5697 in k5691 in ##sys#custom-input-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in ... */
static void C_ccall f_5798(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5798,c,av);}
/* posixunix.scm:823: loop */
t2=((C_word*)((C_word*)t0)[2])[1];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
f_5743(2,av2);}}

/* k5802 in k5793 in loop in fetch in k5697 in k5691 in ##sys#custom-input-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in ... */
static void C_ccall f_5804(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5804,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,((C_word*)((C_word*)t0)[3])[1]);
t3=C_set_block_item(((C_word*)t0)[4],0,C_fix(0));
t4=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k5831 in k5697 in k5691 in ##sys#custom-input-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in ... */
static void C_ccall f_5833(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5833,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(3),((C_word*)t0)[3]);
t4=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t4;
av2[1]=((C_word*)((C_word*)t0)[2])[1];
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* a5838 in k5697 in k5691 in ##sys#custom-input-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in ... */
static void C_ccall f_5839(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_5839,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5843,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_fixnum_greater_or_equal_p(((C_word*)((C_word*)t0)[3])[1],((C_word*)((C_word*)t0)[4])[1]))){
/* posixunix.scm:839: fetch */
t3=((C_word*)t0)[5];
f_5737(t3,t2);}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_5843(2,av2);}}}

/* k5841 in a5838 in k5697 in k5691 in ##sys#custom-input-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in ... */
static void C_ccall f_5843(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5843,c,av);}
t2=(
/* posixunix.scm:840: peek */
  f_5729(((C_word*)t0)[2])
);
if(C_truep(C_eofp(t2))){
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_fixnum_plus(((C_word*)((C_word*)t0)[4])[1],C_fix(1));
t4=C_set_block_item(((C_word*)t0)[4],0,t3);
t5=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* a5863 in k5697 in k5691 in ##sys#custom-input-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in ... */
static void C_ccall f_5864(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5864,c,av);}
t2=C_fixnum_lessp(((C_word*)((C_word*)t0)[2])[1],((C_word*)((C_word*)t0)[3])[1]);
if(C_truep(t2)){
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
/* posixunix.scm:845: ready? */
t3=((C_word*)t0)[4];
f_5700(t3,t1);}}

/* a5875 in k5697 in k5691 in ##sys#custom-input-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in ... */
static void C_ccall f_5876(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,6)))){
C_save_and_reclaim((void *)f_5876,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5880,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_fixnum_lessp(C_close(((C_word*)t0)[3]),C_fix(0)))){
/* posixunix.scm:848: posix-error */
t3=lf[183];{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[188];
av2[3]=((C_word*)t0)[4];
av2[4]=lf[371];
av2[5]=((C_word*)t0)[3];
av2[6]=((C_word*)t0)[5];
f_2939(7,av2);}}
else{
/* posixunix.scm:849: on-close */
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t1;
((C_proc)C_fast_retrieve_proc(t3))(2,av2);}}}

/* k5878 in a5875 in k5697 in k5691 in ##sys#custom-input-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in ... */
static void C_ccall f_5880(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5880,c,av);}
/* posixunix.scm:849: on-close */
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)C_fast_retrieve_proc(t2))(2,av2);}}

/* a5891 in k5697 in k5691 in ##sys#custom-input-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in ... */
static void C_ccall f_5892(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_5892,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5896,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_fixnum_greater_or_equal_p(((C_word*)((C_word*)t0)[3])[1],((C_word*)((C_word*)t0)[4])[1]))){
/* posixunix.scm:852: fetch */
t3=((C_word*)t0)[5];
f_5737(t3,t2);}
else{
/* posixunix.scm:853: peek */
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=(
/* posixunix.scm:853: peek */
  f_5729(((C_word*)t0)[2])
);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k5894 in a5891 in k5697 in k5691 in ##sys#custom-input-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in ... */
static void C_ccall f_5896(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5896,c,av);}
/* posixunix.scm:853: peek */
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(
/* posixunix.scm:853: peek */
  f_5729(((C_word*)t0)[3])
);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a5906 in k5697 in k5691 in ##sys#custom-input-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in ... */
static void C_ccall f_5907(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(c!=6) C_bad_argc_2(c,6,t0);
if(C_unlikely(!C_demand(C_calculate_demand(11,c,5)))){
C_save_and_reclaim((void *)f_5907,c,av);}
a=C_alloc(11);
t6=(C_truep(t3)?t3:C_fixnum_difference(C_block_size(t4),t5));
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_5917,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t4,a[6]=t8,a[7]=((C_word*)t0)[5],a[8]=((C_word)li130),tmp=(C_word)a,a+=9,tmp));
t10=((C_word*)t8)[1];
f_5917(t10,t1,t6,C_fix(0),t5);}

/* loop in a5906 in k5697 in k5691 in ##sys#custom-input-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in ... */
static void C_fcall f_5917(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(8,0,4)))){
C_save_and_reclaim_args((void *)trf_5917,5,t0,t1,t2,t3,t4);}
a=C_alloc(8);
t5=C_eqp(C_fix(0),t2);
if(C_truep(t5)){
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
if(C_truep(C_fixnum_lessp(((C_word*)((C_word*)t0)[2])[1],((C_word*)((C_word*)t0)[3])[1]))){
t6=C_fixnum_difference(((C_word*)((C_word*)t0)[3])[1],((C_word*)((C_word*)t0)[2])[1]);
t7=C_fixnum_lessp(t2,t6);
t8=(C_truep(t7)?t2:t6);
t9=C_fixnum_plus(((C_word*)((C_word*)t0)[2])[1],t8);
t10=C_substring_copy(((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)((C_word*)t0)[2])[1],t9,t4);
t11=C_fixnum_plus(((C_word*)((C_word*)t0)[2])[1],t8);
t12=C_set_block_item(((C_word*)t0)[2],0,t11);
/* posixunix.scm:862: loop */
t14=t1;
t15=C_fixnum_difference(t2,t8);
t16=C_fixnum_plus(t3,t8);
t17=C_fixnum_plus(t4,t8);
t1=t14;
t2=t15;
t3=t16;
t4=t17;
goto loop;}
else{
t6=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_5965,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t3,a[5]=((C_word*)t0)[6],a[6]=t2,a[7]=t4,tmp=(C_word)a,a+=8,tmp);
/* posixunix.scm:864: fetch */
t7=((C_word*)t0)[7];
f_5737(t7,t6);}}}

/* k5963 in loop in a5906 in k5697 in k5691 in ##sys#custom-input-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in ... */
static void C_ccall f_5965(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_5965,c,av);}
t2=C_eqp(C_fix(0),((C_word*)((C_word*)t0)[2])[1]);
if(C_truep(t2)){
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
/* posixunix.scm:867: loop */
t3=((C_word*)((C_word*)t0)[5])[1];
f_5917(t3,((C_word*)t0)[3],((C_word*)t0)[6],((C_word*)t0)[4],((C_word*)t0)[7]);}}

/* a5982 in k5697 in k5691 in ##sys#custom-input-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in ... */
static void C_ccall f_5983(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_5983,c,av);}
a=C_alloc(9);
t4=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_5987,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t3,a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[5],a[8]=t2,tmp=(C_word)a,a+=9,tmp);
if(C_truep(C_fixnum_greater_or_equal_p(((C_word*)((C_word*)t0)[2])[1],((C_word*)((C_word*)t0)[3])[1]))){
/* posixunix.scm:870: fetch */
t5=((C_word*)t0)[5];
f_5737(t5,t4);}
else{
t5=t4;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_UNDEFINED;
f_5987(2,av2);}}}

/* k5985 in a5982 in k5697 in k5691 in ##sys#custom-input-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in ... */
static void C_ccall f_5987(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,5)))){
C_save_and_reclaim((void *)f_5987,c,av);}
a=C_alloc(15);
if(C_truep(C_fixnum_greater_or_equal_p(((C_word*)((C_word*)t0)[2])[1],((C_word*)((C_word*)t0)[3])[1]))){
t2=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_END_OF_FILE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(C_truep(((C_word*)t0)[5])?((C_word*)t0)[5]:C_fixnum_difference(*((C_word*)lf[372]+1),((C_word*)((C_word*)t0)[2])[1]));
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6001,a[2]=((C_word*)t0)[2],a[3]=t4,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word)li133),tmp=(C_word)a,a+=8,tmp);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6057,a[2]=((C_word*)t0)[8],a[3]=((C_word*)t0)[2],a[4]=((C_word)li134),tmp=(C_word)a,a+=5,tmp);
/* posixunix.scm:874: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[4];
av2[2]=t5;
av2[3]=t6;
C_call_with_values(4,av2);}}}

/* a6000 in k5985 in a5982 in k5697 in k5691 in ##sys#custom-input-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in ... */
static void C_ccall f_6001(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(8,c,5)))){
C_save_and_reclaim((void *)f_6001,c,av);}
a=C_alloc(8);
t2=C_fixnum_plus(((C_word*)((C_word*)t0)[2])[1],((C_word*)((C_word*)t0)[3])[1]);
t3=C_i_fixnum_min(((C_word*)((C_word*)t0)[4])[1],t2);
t4=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6011,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word)li132),tmp=(C_word)a,a+=8,tmp);
/* posixunix.scm:875: ##sys#scan-buffer-line */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[373]+1));
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=*((C_word*)lf[373]+1);
av2[1]=t1;
av2[2]=((C_word*)t0)[5];
av2[3]=t3;
av2[4]=((C_word*)((C_word*)t0)[2])[1];
av2[5]=t4;
tp(6,av2);}}

/* a6010 in a6000 in k5985 in a5982 in k5697 in k5691 in ##sys#custom-input-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in ... */
static void C_ccall f_6011(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(7,c,4)))){
C_save_and_reclaim((void *)f_6011,c,av);}
a=C_alloc(7);
t3=C_fixnum_difference(t2,((C_word*)((C_word*)t0)[2])[1]);
if(C_truep(C_fixnum_greater_or_equal_p(t3,((C_word*)((C_word*)t0)[3])[1]))){
/* posixunix.scm:882: scheme#values */{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=t1;
av2[2]=C_SCHEME_FALSE;
av2[3]=t2;
av2[4]=C_SCHEME_FALSE;
C_values(5,av2);}}
else{
t4=C_fixnum_difference(((C_word*)((C_word*)t0)[3])[1],t3);
t5=C_set_block_item(((C_word*)t0)[3],0,t4);
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6031,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[3],a[5]=t1,a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* posixunix.scm:885: fetch */
t7=((C_word*)t0)[6];
f_5737(t7,t6);}}

/* k6029 in a6010 in a6000 in k5985 in a5982 in k5697 in k5691 in ##sys#custom-input-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in ... */
static void C_ccall f_6031(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6031,c,av);}
if(C_truep(C_fixnum_lessp(((C_word*)((C_word*)t0)[2])[1],((C_word*)((C_word*)t0)[3])[1]))){
t2=C_fixnum_plus(((C_word*)((C_word*)t0)[2])[1],((C_word*)((C_word*)t0)[4])[1]);
/* posixunix.scm:887: scheme#values */{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=((C_word*)t0)[5];
av2[2]=((C_word*)t0)[6];
av2[3]=((C_word*)((C_word*)t0)[2])[1];
av2[4]=C_i_fixnum_min(((C_word*)((C_word*)t0)[3])[1],t2);
C_values(5,av2);}}
else{
/* posixunix.scm:890: scheme#values */{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=((C_word*)t0)[5];
av2[2]=C_SCHEME_FALSE;
av2[3]=((C_word*)((C_word*)t0)[2])[1];
av2[4]=C_SCHEME_FALSE;
C_values(5,av2);}}}

/* a6056 in k5985 in a5982 in k5697 in k5691 in ##sys#custom-input-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in ... */
static void C_ccall f_6057(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6057,c,av);}
if(C_truep(t4)){
t5=C_slot(((C_word*)t0)[2],C_fix(4));
t6=C_fixnum_plus(t5,C_fix(1));
t7=C_i_set_i_slot(((C_word*)t0)[2],C_fix(4),t6);
t8=C_i_set_i_slot(((C_word*)t0)[2],C_fix(5),C_fix(0));
t9=C_mutate(((C_word *)((C_word*)t0)[3])+1,t2);
t10=t1;{
C_word *av2=av;
av2[0]=t10;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t10+1)))(2,av2);}}
else{
t5=C_slot(((C_word*)t0)[2],C_fix(5));
t6=C_block_size(t3);
t7=C_fixnum_plus(t5,t6);
t8=C_i_set_i_slot(((C_word*)t0)[2],C_fix(5),t7);
t9=C_mutate(((C_word *)((C_word*)t0)[3])+1,t2);
t10=t1;{
C_word *av2=av;
av2[0]=t10;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t10+1)))(2,av2);}}}

/* a6101 in k5697 in k5691 in ##sys#custom-input-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in ... */
static void C_ccall f_6102(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_6102,c,av);}
a=C_alloc(5);
if(C_truep(C_fixnum_greater_or_equal_p(((C_word*)((C_word*)t0)[2])[1],((C_word*)((C_word*)t0)[3])[1]))){
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=lf[374];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6112,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* posixunix.scm:903: ##sys#substring */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[259]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[259]+1);
av2[1]=t3;
av2[2]=((C_word*)t0)[4];
av2[3]=((C_word*)((C_word*)t0)[2])[1];
av2[4]=((C_word*)((C_word*)t0)[3])[1];
tp(5,av2);}}}

/* k6110 in a6101 in k5697 in k5691 in ##sys#custom-input-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in ... */
static void C_ccall f_6112(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6112,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,((C_word*)((C_word*)t0)[3])[1]);
t3=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* ##sys#custom-output-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_6178(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(c<5) C_bad_min_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_6178,c,av);}
a=C_alloc(8);
t5=C_rest_nullp(c,5);
t6=(C_truep(t5)?C_SCHEME_FALSE:C_get_rest_arg(c,5,av,5,t0));
t7=C_rest_nullp(c,5);
t8=C_rest_nullp(c,6);
t9=(C_truep(t8)?C_fix(0):C_get_rest_arg(c,6,av,5,t0));
t10=C_rest_nullp(c,6);
t11=C_rest_nullp(c,7);
t12=(C_truep(t11)?*((C_word*)lf[363]+1):C_get_rest_arg(c,7,av,5,t0));
t13=C_rest_nullp(c,7);
t14=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6200,a[2]=t4,a[3]=t2,a[4]=t3,a[5]=t9,a[6]=t1,a[7]=t12,tmp=(C_word)a,a+=8,tmp);
if(C_truep(t6)){
/* posixunix.scm:912: ##sys#file-nonblocking! */
t15=*((C_word*)lf[305]+1);{
C_word *av2=av;
av2[0]=t15;
av2[1]=t14;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t15+1)))(3,av2);}}
else{
t15=t14;{
C_word *av2=av;
av2[0]=t15;
av2[1]=C_SCHEME_UNDEFINED;
f_6200(2,av2);}}}

/* k6198 in ##sys#custom-output-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_6200(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(26,c,4)))){
C_save_and_reclaim((void *)f_6200,c,av);}
a=C_alloc(26);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6201,a[2]=((C_word*)t0)[2],a[3]=t3,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word)li139),tmp=(C_word)a,a+=7,tmp);
t7=C_fixnump(((C_word*)t0)[5]);
t8=(C_truep(t7)?((C_word*)t0)[5]:C_block_size(((C_word*)t0)[5]));
t9=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_6267,a[2]=t3,a[3]=t6,a[4]=t5,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[2],a[9]=((C_word*)t0)[3],tmp=(C_word)a,a+=10,tmp);
t10=C_eqp(C_fix(0),t8);
if(C_truep(t10)){
t11=t9;
f_6267(t11,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6308,a[2]=t3,a[3]=((C_word)li143),tmp=(C_word)a,a+=4,tmp));}
else{
t11=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6322,a[2]=t9,a[3]=t8,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_fixnump(((C_word*)t0)[5]))){
/* posixunix.scm:935: ##sys#make-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[264]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[264]+1);
av2[1]=t11;
av2[2]=((C_word*)t0)[5];
tp(3,av2);}}
else{
t12=t11;{
C_word *av2=av;
av2[0]=t12;
av2[1]=((C_word*)t0)[5];
f_6322(2,av2);}}}}

/* poke1761 in k6198 in ##sys#custom-output-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 in ... */
static void C_ccall f_6201(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_6201,c,av);}
a=C_alloc(12);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_6207,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=t3,a[5]=((C_word*)t0)[3],a[6]=t5,a[7]=((C_word*)t0)[4],a[8]=((C_word*)t0)[5],a[9]=((C_word)li138),tmp=(C_word)a,a+=10,tmp));
t7=((C_word*)t5)[1];{
C_word *av2=av;
av2[0]=t7;
av2[1]=t1;
f_6207(2,av2);}}

/* loop in poke1761 in k6198 in ##sys#custom-output-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in ... */
static void C_ccall f_6207(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,6)))){
C_save_and_reclaim((void *)f_6207,c,av);}
a=C_alloc(6);
t2=C_write(((C_word*)t0)[2],((C_word*)t0)[3],((C_word*)t0)[4]);
t3=C_eqp(C_fix(-1),t2);
if(C_truep(t3)){
t4=C_eqp(C_fix((C_word)errno),C_fix((C_word)EWOULDBLOCK));
t5=(C_truep(t4)?t4:C_eqp(C_fix((C_word)errno),C_fix((C_word)EAGAIN)));
if(C_truep(t5)){
t6=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6226,a[2]=((C_word*)t0)[5],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* posixunix.scm:921: ##sys#thread-yield! */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[365]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[365]+1);
av2[1]=t6;
tp(2,av2);}}
else{
t6=C_eqp(C_fix((C_word)errno),C_fix((C_word)EINTR));
if(C_truep(t6)){
/* posixunix.scm:924: ##sys#dispatch-interrupt */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[311]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[311]+1);
av2[1]=t1;
av2[2]=((C_word*)((C_word*)t0)[6])[1];
tp(3,av2);}}
else{
/* posixunix.scm:926: posix-error */
t7=lf[183];{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t7;
av2[1]=t1;
av2[2]=((C_word*)t0)[7];
av2[3]=lf[188];
av2[4]=lf[377];
av2[5]=((C_word*)t0)[2];
av2[6]=((C_word*)t0)[8];
f_2939(7,av2);}}}}
else{
if(C_truep(C_fixnum_lessp(t2,((C_word*)t0)[4]))){
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6257,a[2]=((C_word*)t0)[5],a[3]=t1,a[4]=((C_word*)t0)[4],a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* posixunix.scm:928: ##sys#substring */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[259]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[259]+1);
av2[1]=t4;
av2[2]=((C_word*)t0)[3];
av2[3]=t2;
av2[4]=((C_word*)t0)[4];
tp(5,av2);}}
else{
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}}

/* k6224 in loop in poke1761 in k6198 in ##sys#custom-output-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in ... */
static void C_ccall f_6226(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6226,c,av);}
/* posixunix.scm:922: poke */
t2=((C_word*)((C_word*)t0)[2])[1];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=((C_word*)t0)[5];
((C_proc)C_fast_retrieve_proc(t2))(4,av2);}}

/* k6255 in loop in poke1761 in k6198 in ##sys#custom-output-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in ... */
static void C_ccall f_6257(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6257,c,av);}
/* posixunix.scm:928: poke */
t2=((C_word*)((C_word*)t0)[2])[1];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
av2[3]=C_fixnum_difference(((C_word*)t0)[4],((C_word*)t0)[5]);
((C_proc)C_fast_retrieve_proc(t2))(4,av2);}}

/* k6265 in k6198 in ##sys#custom-output-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 in ... */
static void C_fcall f_6267(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(22,0,4)))){
C_save_and_reclaim_args((void *)trf_6267,2,t0,t1);}
a=C_alloc(22);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,((C_word*)t0)[3]);
t3=C_mutate(((C_word *)((C_word*)t0)[4])+1,t1);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6272,a[2]=t5,a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],tmp=(C_word)a,a+=5,tmp);
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6278,a[2]=((C_word*)t0)[4],a[3]=((C_word)li140),tmp=(C_word)a,a+=4,tmp);
t8=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6284,a[2]=((C_word*)t0)[7],a[3]=((C_word*)t0)[8],a[4]=((C_word*)t0)[9],a[5]=((C_word*)t0)[5],a[6]=((C_word)li141),tmp=(C_word)a,a+=7,tmp);
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6300,a[2]=((C_word*)t0)[4],a[3]=((C_word)li142),tmp=(C_word)a,a+=4,tmp);
/* posixunix.scm:953: chicken.port#make-output-port */
t10=*((C_word*)lf[379]+1);{
C_word av2[5];
av2[0]=t10;
av2[1]=t6;
av2[2]=t7;
av2[3]=t8;
av2[4]=t9;
((C_proc)(void*)(*((C_word*)t10+1)))(5,av2);}}

/* k6270 in k6265 in k6198 in ##sys#custom-output-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in ... */
static void C_ccall f_6272(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6272,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(3),((C_word*)t0)[3]);
t4=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t4;
av2[1]=((C_word*)((C_word*)t0)[2])[1];
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* a6277 in k6265 in k6198 in ##sys#custom-output-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in ... */
static void C_ccall f_6278(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6278,c,av);}
/* posixunix.scm:955: store */
t3=((C_word*)((C_word*)t0)[2])[1];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t1;
av2[2]=t2;
((C_proc)C_fast_retrieve_proc(t3))(3,av2);}}

/* a6283 in k6265 in k6198 in ##sys#custom-output-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in ... */
static void C_ccall f_6284(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,6)))){
C_save_and_reclaim((void *)f_6284,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6288,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_fixnum_lessp(C_close(((C_word*)t0)[3]),C_fix(0)))){
/* posixunix.scm:958: posix-error */
t3=lf[183];{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[188];
av2[3]=((C_word*)t0)[4];
av2[4]=lf[378];
av2[5]=((C_word*)t0)[3];
av2[6]=((C_word*)t0)[5];
f_2939(7,av2);}}
else{
/* posixunix.scm:959: on-close */
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t1;
((C_proc)C_fast_retrieve_proc(t3))(2,av2);}}}

/* k6286 in a6283 in k6265 in k6198 in ##sys#custom-output-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in ... */
static void C_ccall f_6288(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6288,c,av);}
/* posixunix.scm:959: on-close */
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)C_fast_retrieve_proc(t2))(2,av2);}}

/* a6299 in k6265 in k6198 in ##sys#custom-output-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in ... */
static void C_ccall f_6300(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6300,c,av);}
/* posixunix.scm:961: store */
t2=((C_word*)((C_word*)t0)[2])[1];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t2))(3,av2);}}

/* f_6308 in k6198 in ##sys#custom-output-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 in ... */
static void C_ccall f_6308(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6308,c,av);}
if(C_truep(t2)){
/* posixunix.scm:934: poke */
t3=((C_word*)((C_word*)t0)[2])[1];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=t2;
av2[3]=C_block_size(t2);
((C_proc)C_fast_retrieve_proc(t3))(4,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k6320 in k6198 in ##sys#custom-output-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 in ... */
static void C_ccall f_6322(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_6322,c,av);}
a=C_alloc(9);
t2=C_fix(0);
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=((C_word*)t0)[2];
f_6267(t4,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6323,a[2]=((C_word*)t0)[3],a[3]=t3,a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word)li145),tmp=(C_word)a,a+=7,tmp));}

/* f_6323 in k6320 in k6198 in ##sys#custom-output-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in ... */
static void C_ccall f_6323(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(11,c,5)))){
C_save_and_reclaim((void *)f_6323,c,av);}
a=C_alloc(11);
if(C_truep(t2)){
t3=C_fixnum_difference(((C_word*)t0)[2],((C_word*)((C_word*)t0)[3])[1]);
t4=C_block_size(t2);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_6340,a[2]=((C_word*)t0)[3],a[3]=t6,a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=t2,a[8]=((C_word)li144),tmp=(C_word)a,a+=9,tmp));
t8=((C_word*)t6)[1];
f_6340(t8,t1,t3,C_fix(0),t4);}
else{
if(C_truep(C_fixnum_lessp(C_fix(0),((C_word*)((C_word*)t0)[3])[1]))){
/* posixunix.scm:951: poke */
t3=((C_word*)((C_word*)t0)[4])[1];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=((C_word*)t0)[5];
av2[3]=((C_word*)((C_word*)t0)[3])[1];
((C_proc)C_fast_retrieve_proc(t3))(4,av2);}}
else{
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}}

/* loop */
static void C_fcall f_6340(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(7,0,4)))){
C_save_and_reclaim_args((void *)trf_6340,5,t0,t1,t2,t3,t4);}
a=C_alloc(7);
t5=C_eqp(C_fix(0),t2);
if(C_truep(t5)){
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6350,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=t4,tmp=(C_word)a,a+=7,tmp);
/* posixunix.scm:941: poke */
t7=((C_word*)((C_word*)t0)[5])[1];{
C_word av2[4];
av2[0]=t7;
av2[1]=t6;
av2[2]=((C_word*)t0)[6];
av2[3]=((C_word*)t0)[4];
((C_proc)C_fast_retrieve_proc(t7))(4,av2);}}
else{
if(C_truep(C_fixnum_lessp(t2,t4))){
t6=C_substring_copy(((C_word*)t0)[7],((C_word*)t0)[6],t3,t2,((C_word*)((C_word*)t0)[2])[1]);
/* posixunix.scm:946: loop */
t10=t1;
t11=C_fix(0);
t12=t2;
t13=C_fixnum_difference(t4,t2);
t1=t10;
t2=t11;
t3=t12;
t4=t13;
goto loop;}
else{
t6=C_substring_copy(((C_word*)t0)[7],((C_word*)t0)[6],t3,t4,((C_word*)((C_word*)t0)[2])[1]);
t7=C_fixnum_plus(((C_word*)((C_word*)t0)[2])[1],t4);
t8=C_set_block_item(((C_word*)t0)[2],0,t7);
t9=t1;{
C_word av2[2];
av2[0]=t9;
av2[1]=t8;
((C_proc)(void*)(*((C_word*)t9+1)))(2,av2);}}}}

/* k6348 in loop */
static void C_ccall f_6350(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6350,c,av);}
t2=C_set_block_item(((C_word*)t0)[2],0,C_fix(0));
/* posixunix.scm:943: loop */
t3=((C_word*)((C_word*)t0)[3])[1];
f_6340(t3,((C_word*)t0)[4],((C_word*)t0)[5],C_fix(0),((C_word*)t0)[6]);}

/* chicken.file.posix#file-truncate in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_6433(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_6433,c,av);}
a=C_alloc(5);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6437,a[2]=t1,a[3]=t2,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* posixunix.scm:970: ##sys#check-exact-integer */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[211]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[211]+1);
av2[1]=t4;
av2[2]=t3;
av2[3]=lf[380];
tp(4,av2);}}

/* k6435 in chicken.file.posix#file-truncate in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_6437(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,3)))){
C_save_and_reclaim((void *)f_6437,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6443,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6450,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_i_stringp(((C_word*)t0)[3]))){
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6457,a[2]=t2,a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
/* posixunix.scm:971: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[190]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[190]+1);
av2[1]=t4;
av2[2]=((C_word*)t0)[3];
av2[3]=lf[380];
tp(4,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6463,a[2]=t2,a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[3],a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* posixunix.scm:972: chicken.base#port? */
t5=*((C_word*)lf[193]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}}

/* k6441 in k6435 in chicken.file.posix#file-truncate in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 in ... */
static void C_fcall f_6443(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,6)))){
C_save_and_reclaim_args((void *)trf_6443,2,t0,t1);}
if(C_truep(t1)){
/* posixunix.scm:976: posix-error */
t2=lf[183];{
C_word av2[7];
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[188];
av2[3]=lf[380];
av2[4]=lf[381];
av2[5]=((C_word*)t0)[3];
av2[6]=((C_word*)t0)[4];
f_2939(7,av2);}}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k6448 in k6435 in chicken.file.posix#file-truncate in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 in ... */
static void C_ccall f_6450(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6450,c,av);}
t2=((C_word*)t0)[2];
f_6443(t2,C_fixnum_lessp(t1,C_fix(0)));}

/* k6455 in k6435 in chicken.file.posix#file-truncate in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 in ... */
static void C_ccall f_6457(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6457,c,av);}
t2=((C_word*)t0)[2];
f_6443(t2,C_fixnum_lessp(C_truncate(t1,((C_word*)t0)[3]),C_fix(0)));}

/* k6461 in k6435 in chicken.file.posix#file-truncate in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 in ... */
static void C_ccall f_6463(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_6463,c,av);}
a=C_alloc(4);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6467,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* posixunix.scm:972: chicken.file.posix#port->fileno */
t3=*((C_word*)lf[73]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
if(C_truep(C_fixnump(((C_word*)t0)[4]))){
t2=((C_word*)t0)[2];
f_6443(t2,C_fixnum_lessp(C_ftruncate(((C_word*)t0)[4],((C_word*)t0)[3]),C_fix(0)));}
else{
/* posixunix.scm:974: ##sys#error */
t2=*((C_word*)lf[95]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[5];
av2[2]=lf[380];
av2[3]=lf[382];
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}}}

/* k6465 in k6461 in k6435 in chicken.file.posix#file-truncate in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in ... */
static void C_ccall f_6467(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6467,c,av);}
t2=((C_word*)t0)[2];
f_6443(t2,C_fixnum_lessp(C_ftruncate(t1,((C_word*)t0)[3]),C_fix(0)));}

/* setup in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_6478(C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,3)))){
C_save_and_reclaim_args((void *)trf_6478,4,t1,t2,t3,t4);}
a=C_alloc(9);
t5=C_i_nullp(t3);
t6=(C_truep(t5)?C_fix(0):C_i_car(t3));
t7=C_i_nullp(t3);
t8=(C_truep(t7)?C_SCHEME_END_OF_LIST:C_i_cdr(t3));
t9=C_i_nullp(t8);
t10=(C_truep(t9)?C_SCHEME_TRUE:C_i_car(t8));
t11=t10;
t12=(*a=C_VECTOR_TYPE|1,a[1]=t11,tmp=(C_word)a,a+=2,tmp);
t13=C_i_nullp(t8);
t14=(C_truep(t13)?C_SCHEME_END_OF_LIST:C_i_cdr(t8));
t15=C_i_check_port_2(t2,C_fix(0),C_SCHEME_TRUE,t4);
t16=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6496,a[2]=t2,a[3]=t6,a[4]=t12,a[5]=t1,a[6]=t4,tmp=(C_word)a,a+=7,tmp);
/* posixunix.scm:990: ##sys#check-exact-integer */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[211]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[211]+1);
av2[1]=t16;
av2[2]=t6;
av2[3]=t4;
tp(4,av2);}}

/* k6494 in setup in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_6496(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_6496,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6499,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t3=C_eqp(C_SCHEME_TRUE,((C_word*)((C_word*)t0)[4])[1]);
if(C_truep(t3)){
t4=C_set_block_item(((C_word*)t0)[4],0,C_fix(0));
t5=t2;{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
f_6499(2,av2);}}
else{
/* posixunix.scm:993: ##sys#check-exact-integer */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[211]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[211]+1);
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[4])[1];
av2[3]=((C_word*)t0)[6];
tp(4,av2);}}}

/* k6497 in k6494 in setup in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 in ... */
static void C_ccall f_6499(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,1)))){
C_save_and_reclaim((void *)f_6499,c,av);}
a=C_alloc(5);
t2=C_slot(((C_word*)t0)[2],C_fix(1));
t3=C_i_nequalp(t2,C_fix(1));
t4=(C_truep(t3)?C_fix((C_word)F_RDLCK):C_fix((C_word)F_WRLCK));
t5=C_flock_setup(t4,((C_word*)t0)[3],((C_word*)((C_word*)t0)[4])[1]);
t6=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_a_i_record4(&a,4,lf[383],((C_word*)t0)[2],((C_word*)t0)[3],((C_word*)((C_word*)t0)[4])[1]);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}

/* chicken.file.posix#file-lock in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_6564(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +9,c,2)))){
C_save_and_reclaim((void*)f_6564,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+9);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
C_word t6;
C_word t7;
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6570,a[2]=t2,a[3]=t5,a[4]=((C_word*)t0)[2],a[5]=t3,a[6]=((C_word)li149),tmp=(C_word)a,a+=7,tmp));
t7=((C_word*)t5)[1];{
C_word *av2=av;
av2[0]=t7;
av2[1]=t1;
f_6570(2,av2);}}

/* loop in chicken.file.posix#file-lock in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_6570(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_6570,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6574,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* posixunix.scm:1001: setup */
f_6478(t2,((C_word*)t0)[2],((C_word*)t0)[5],lf[384]);}

/* k6572 in loop in chicken.file.posix#file-lock in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 in ... */
static void C_ccall f_6574(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_6574,c,av);}
if(C_truep(C_fixnum_lessp(C_flock_lock(((C_word*)t0)[2]),C_fix(0)))){
t2=C_eqp(C_fix((C_word)errno),C_fix((C_word)EINTR));
if(C_truep(t2)){
/* posixunix.scm:1004: ##sys#dispatch-interrupt */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[311]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[311]+1);
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)((C_word*)t0)[4])[1];
tp(3,av2);}}
else{
/* posixunix.scm:997: posix-error */
t3=lf[183];{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[188];
av2[3]=lf[384];
av2[4]=lf[385];
av2[5]=C_slot(t1,C_fix(1));
av2[6]=C_slot(t1,C_fix(2));
av2[7]=C_slot(t1,C_fix(3));
f_2939(8,av2);}}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* chicken.file.posix#file-lock/blocking in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_6595(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +9,c,2)))){
C_save_and_reclaim((void*)f_6595,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+9);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
C_word t6;
C_word t7;
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6601,a[2]=t2,a[3]=t5,a[4]=((C_word*)t0)[2],a[5]=t3,a[6]=((C_word)li151),tmp=(C_word)a,a+=7,tmp));
t7=((C_word*)t5)[1];{
C_word *av2=av;
av2[0]=t7;
av2[1]=t1;
f_6601(2,av2);}}

/* loop in chicken.file.posix#file-lock/blocking in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_6601(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_6601,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6605,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* posixunix.scm:1010: setup */
f_6478(t2,((C_word*)t0)[2],((C_word*)t0)[5],lf[386]);}

/* k6603 in loop in chicken.file.posix#file-lock/blocking in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 in ... */
static void C_ccall f_6605(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_6605,c,av);}
if(C_truep(C_fixnum_lessp(C_flock_lockw(((C_word*)t0)[2]),C_fix(0)))){
t2=C_eqp(C_fix((C_word)errno),C_fix((C_word)EINTR));
if(C_truep(t2)){
/* posixunix.scm:1013: ##sys#dispatch-interrupt */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[311]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[311]+1);
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)((C_word*)t0)[4])[1];
tp(3,av2);}}
else{
/* posixunix.scm:997: posix-error */
t3=lf[183];{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[188];
av2[3]=lf[386];
av2[4]=lf[387];
av2[5]=C_slot(t1,C_fix(1));
av2[6]=C_slot(t1,C_fix(2));
av2[7]=C_slot(t1,C_fix(3));
f_2939(8,av2);}}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* chicken.file.posix#file-test-lock in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_6626(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +4,c,4)))){
C_save_and_reclaim((void*)f_6626,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+4);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6630,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* posixunix.scm:1018: setup */
f_6478(t4,t2,t3,lf[388]);}

/* k6628 in chicken.file.posix#file-test-lock in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_6630(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_6630,c,av);}
t2=C_flock_test(((C_word*)t0)[2]);
if(C_truep(t2)){
t3=C_eqp(t2,C_fix(0));
t4=C_i_not(t3);
t5=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t5;
av2[1]=(C_truep(t4)?t2:C_SCHEME_FALSE);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
/* posixunix.scm:997: posix-error */
t3=lf[183];{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[188];
av2[3]=lf[388];
av2[4]=lf[389];
av2[5]=C_slot(t1,C_fix(1));
av2[6]=C_slot(t1,C_fix(2));
av2[7]=C_slot(t1,C_fix(3));
f_2939(8,av2);}}}

/* chicken.file.posix#file-unlock in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_6653(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,5)))){
C_save_and_reclaim((void *)f_6653,c,av);}
a=C_alloc(4);
t3=C_i_check_structure_2(t2,lf[383],lf[390]);
t4=C_slot(t2,C_fix(2));
t5=C_slot(t2,C_fix(3));
t6=C_flock_setup(C_fix((C_word)F_UNLCK),t4,t5);
t7=C_slot(t2,C_fix(1));
if(C_truep(C_fixnum_lessp(C_flock_lock(t7),C_fix(0)))){
t8=C_eqp(C_fix((C_word)errno),C_fix((C_word)EINTR));
if(C_truep(t8)){
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6682,a[2]=t2,a[3]=((C_word)li154),tmp=(C_word)a,a+=4,tmp);
/* posixunix.scm:1029: ##sys#dispatch-interrupt */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[311]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[311]+1);
av2[1]=t1;
av2[2]=t9;
tp(3,av2);}}
else{
/* posixunix.scm:1031: posix-error */
t9=lf[183];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t9;
av2[1]=t1;
av2[2]=lf[188];
av2[3]=lf[390];
av2[4]=lf[391];
av2[5]=t2;
f_2939(6,av2);}}}
else{
t8=C_SCHEME_UNDEFINED;
t9=t1;{
C_word *av2=av;
av2[0]=t9;
av2[1]=t8;
((C_proc)(void*)(*((C_word*)t9+1)))(2,av2);}}}

/* a6681 in chicken.file.posix#file-unlock in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_6682(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6682,c,av);}
/* posixunix.scm:1030: chicken.file.posix#file-unlock */
t2=*((C_word*)lf[27]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* chicken.file.posix#create-fifo in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_6696(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +5,c,3)))){
C_save_and_reclaim((void*)f_6696,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+5);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
t4=C_i_check_string_2(t2,lf[392]);
t5=C_i_pairp(t3);
t6=(C_truep(t5)?C_get_rest_arg(c,3,av,3,t0):C_fixnum_or(C_fix((C_word)S_IRUSR | S_IWUSR | S_IXUSR),C_fixnum_or(C_fix((C_word)S_IRGRP | S_IWGRP | S_IXGRP),C_fix((C_word)S_IROTH | S_IWOTH | S_IXOTH))));
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6706,a[2]=t6,a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* posixunix.scm:1040: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t7;
av2[2]=t6;
av2[3]=lf[392];
tp(4,av2);}}

/* k6704 in chicken.file.posix#create-fifo in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_6706(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_6706,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6720,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* posixunix.scm:1041: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[190]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[190]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=lf[392];
tp(4,av2);}}

/* k6718 in k6704 in chicken.file.posix#create-fifo in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 in ... */
static void C_ccall f_6720(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_6720,c,av);}
if(C_truep(C_fixnum_lessp(C_mkfifo(t1,((C_word*)t0)[2]),C_fix(0)))){
/* posixunix.scm:1042: posix-error */
t2=lf[183];{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[188];
av2[3]=lf[392];
av2[4]=lf[393];
av2[5]=((C_word*)t0)[4];
av2[6]=((C_word*)t0)[2];
f_2939(7,av2);}}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* chicken.time.posix#string->time in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_6742(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_6742,c,av);}
a=C_alloc(5);
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?lf[394]:C_get_rest_arg(c,3,av,3,t0));
t5=C_i_check_string_2(t2,lf[395]);
t6=C_i_check_string_2(t4,lf[395]);
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6759,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t4,tmp=(C_word)a,a+=5,tmp);
/* posixunix.scm:1053: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[190]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[190]+1);
av2[1]=t7;
av2[2]=t2;
av2[3]=lf[395];
tp(4,av2);}}

/* k6757 in chicken.time.posix#string->time in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_6759(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_6759,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6763,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* posixunix.scm:1053: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[190]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[190]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
tp(3,av2);}}

/* k6761 in k6757 in chicken.time.posix#string->time in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 in ... */
static void C_ccall f_6763(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(17,c,3)))){
C_save_and_reclaim((void *)f_6763,c,av);}
a=C_alloc(17);
t2=C_a_i_vector(&a,10,C_SCHEME_FALSE,C_SCHEME_FALSE,C_SCHEME_FALSE,C_SCHEME_FALSE,C_SCHEME_FALSE,C_SCHEME_FALSE,C_SCHEME_FALSE,C_SCHEME_FALSE,C_SCHEME_FALSE,C_SCHEME_FALSE);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6771,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* posixunix.scm:1053: ##sys#make-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[264]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[264]+1);
av2[1]=t3;
av2[2]=((C_word*)t0)[4];
av2[3]=C_make_character(0);
tp(4,av2);}}

/* k6769 in k6761 in k6757 in chicken.time.posix#string->time in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in ... */
static void C_ccall f_6771(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6771,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(C_truep(t1)?stub1894(C_SCHEME_UNDEFINED,((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],C_i_foreign_block_argumentp(t1)):stub1894(C_SCHEME_UNDEFINED,((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],C_SCHEME_FALSE));
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.time.posix#utc-time->seconds in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_6779(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_6779,c,av);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6783,a[2]=t2,a[3]=t1,a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
/* posixunix.scm:1058: check-time-vector */
f_3627(t3,lf[396],t2);}

/* k6781 in chicken.time.posix#utc-time->seconds in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_6783(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_6783,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6787,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* posixunix.scm:1059: ##sys#make-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[264]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[264]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=C_make_character(0);
tp(4,av2);}}

/* k6785 in k6781 in chicken.time.posix#utc-time->seconds in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 in ... */
static void C_ccall f_6787(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,4)))){
C_save_and_reclaim((void *)f_6787,c,av);}
a=C_alloc(7);
t2=C_a_timegm(&a,2,((C_word*)t0)[2],t1);
if(C_truep(C_i_nequalp(C_fix(-1),t2))){
/* posixunix.scm:1061: ##sys#error */
t3=*((C_word*)lf[95]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[396];
av2[3]=lf[397];
av2[4]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}
else{
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* chicken.time.posix#local-timezone-abbreviation in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_6798(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_6798,c,av);}
a=C_alloc(5);
t2=C_a_i_bytevector(&a,1,C_fix(3));
/* posixunix.scm:1065: ##sys#peek-c-string */
t3=*((C_word*)lf[185]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=stub1921(t2);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* chicken.process.signal#set-alarm! in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_6805(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6805,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=stub1925(C_SCHEME_UNDEFINED,C_i_foreign_fixnum_argumentp(t2));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.process#process-fork in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_6814(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_6814,c,av);}
a=C_alloc(6);
t2=C_rest_nullp(c,2);
t3=(C_truep(t2)?C_SCHEME_FALSE:C_get_rest_arg(c,2,av,2,t0));
t4=C_rest_nullp(c,2);
t5=C_rest_nullp(c,3);
t6=(C_truep(t5)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,2,t0));
t7=C_rest_nullp(c,3);
t8=stub1949(C_SCHEME_UNDEFINED,C_SCHEME_FALSE);
t9=stub1929(C_SCHEME_UNDEFINED);
t10=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6845,a[2]=t3,a[3]=t9,a[4]=t6,a[5]=t1,tmp=(C_word)a,a+=6,tmp);
t11=C_eqp(C_fix(-1),t9);
if(C_truep(t11)){
/* posixunix.scm:1091: posix-error */
t12=lf[183];{
C_word av2[5];
av2[0]=t12;
av2[1]=t10;
av2[2]=lf[91];
av2[3]=lf[401];
av2[4]=lf[402];
f_2939(5,av2);}}
else{
t12=t10;{
C_word *av2=av;
av2[0]=t12;
av2[1]=C_SCHEME_UNDEFINED;
f_6845(2,av2);}}}

/* k6843 in chicken.process#process-fork in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_6845(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_6845,c,av);}
a=C_alloc(7);
t2=(C_truep(((C_word*)t0)[2])?C_eqp(((C_word*)t0)[3],C_fix(0)):C_SCHEME_FALSE);
if(C_truep(t2)){
t3=(C_truep(((C_word*)t0)[4])?*((C_word*)lf[398]+1):(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6873,a[2]=((C_word)li161),tmp=(C_word)a,a+=3,tmp));
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6859,a[2]=((C_word*)t0)[2],a[3]=((C_word)li163),tmp=(C_word)a,a+=4,tmp);
/* posixunix.scm:1092: g1956 */
t5=t3;{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=((C_word*)t0)[5];
av2[2]=t4;
((C_proc)C_fast_retrieve_proc(t5))(3,av2);}}
else{
t3=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* a6858 in k6843 in chicken.process#process-fork in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 in ... */
static void C_ccall f_6859(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_6859,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6865,a[2]=((C_word*)t0)[2],a[3]=((C_word)li162),tmp=(C_word)a,a+=4,tmp);
/* posixunix.scm:1097: ##sys#call-with-cthulhu */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[400]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[400]+1);
av2[1]=t1;
av2[2]=t2;
tp(3,av2);}}

/* a6864 in a6858 in k6843 in chicken.process#process-fork in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in ... */
static void C_ccall f_6865(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_6865,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6869,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posixunix.scm:1099: thunk */
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)C_fast_retrieve_proc(t3))(2,av2);}}

/* k6867 in a6864 in a6858 in k6843 in chicken.process#process-fork in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in ... */
static void C_ccall f_6869(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6869,c,av);}
/* posixunix.scm:1100: chicken.base#exit */
t2=*((C_word*)lf[399]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_fix(0);
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* f_6873 in k6843 in chicken.process#process-fork in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 in ... */
static void C_ccall f_6873(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6873,c,av);}
/* posixunix.scm:1095: thunk */
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=t1;
((C_proc)C_fast_retrieve_proc(t3))(2,av2);}}

/* chicken.process#process-execute in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_6913(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(7,c,7)))){
C_save_and_reclaim((void *)f_6913,c,av);}
a=C_alloc(7);
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_SCHEME_END_OF_LIST:C_get_rest_arg(c,3,av,3,t0));
t5=C_rest_nullp(c,3);
t6=C_rest_nullp(c,4);
t7=(C_truep(t6)?C_SCHEME_FALSE:C_get_rest_arg(c,4,av,3,t0));
t8=C_rest_nullp(c,4);
t9=C_rest_nullp(c,5);
t10=(C_truep(t9)?C_SCHEME_FALSE:C_get_rest_arg(c,5,av,3,t0));
t11=C_rest_nullp(c,5);
t12=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6937,a[2]=((C_word)li165),tmp=(C_word)a,a+=3,tmp);
t13=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6940,a[2]=t2,a[3]=((C_word)li166),tmp=(C_word)a,a+=4,tmp);
/* posixunix.scm:1105: call-with-exec-args */
t14=lf[287];
f_4138(t14,t1,lf[403],t2,t12,t4,t7,t13);}

/* a6936 in chicken.process#process-execute in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_6937(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6937,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* a6939 in chicken.process#process-execute in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_6940(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_6940,c,av);}
t5=(C_truep(t4)?C_u_i_execve(t2,t3,t4):C_u_i_execvp(t2,t3));
t6=C_eqp(t5,C_fix(-1));
if(C_truep(t6)){
/* posixunix.scm:1112: posix-error */
t7=lf[183];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t7;
av2[1]=t1;
av2[2]=lf[91];
av2[3]=lf[403];
av2[4]=lf[404];
av2[5]=((C_word*)t0)[2];
f_2939(6,av2);}}
else{
t7=C_SCHEME_UNDEFINED;
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=t7;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}}

/* chicken.posix#process-wait-impl in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_6991(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,4)))){
C_save_and_reclaim_args((void *)trf_6991,3,t1,t2,t3);}
a=C_alloc(5);
t4=(C_truep(t3)?C_fix((C_word)WNOHANG):C_fix(0));
t5=C_waitpid(t2,t4);
t6=C_WIFEXITED(C_fix((C_word)C_wait_status));
t7=C_eqp(t5,C_fix(-1));
t8=(C_truep(t7)?C_eqp(C_fix((C_word)errno),C_fix((C_word)EINTR)):C_SCHEME_FALSE);
if(C_truep(t8)){
t9=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7007,a[2]=t2,a[3]=t3,a[4]=((C_word)li168),tmp=(C_word)a,a+=5,tmp);
/* posixunix.scm:1121: ##sys#dispatch-interrupt */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[311]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[311]+1);
av2[1]=t1;
av2[2]=t9;
tp(3,av2);}}
else{
if(C_truep(t6)){
/* posixunix.scm:1123: scheme#values */{
C_word av2[5];
av2[0]=0;
av2[1]=t1;
av2[2]=t5;
av2[3]=t6;
av2[4]=C_WEXITSTATUS(C_fix((C_word)C_wait_status));
C_values(5,av2);}}
else{
if(C_truep(C_WIFSIGNALED(C_fix((C_word)C_wait_status)))){
/* posixunix.scm:1123: scheme#values */{
C_word av2[5];
av2[0]=0;
av2[1]=t1;
av2[2]=t5;
av2[3]=t6;
av2[4]=C_WTERMSIG(C_fix((C_word)C_wait_status));
C_values(5,av2);}}
else{
/* posixunix.scm:1123: scheme#values */{
C_word av2[5];
av2[0]=0;
av2[1]=t1;
av2[2]=t5;
av2[3]=t6;
av2[4]=C_WSTOPSIG(C_fix((C_word)C_wait_status));
C_values(5,av2);}}}}}

/* a7006 in chicken.posix#process-wait-impl in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7007(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7007,c,av);}
/* posixunix.scm:1122: process-wait-impl */
f_6991(t1,((C_word*)t0)[2],((C_word*)t0)[3]);}

/* chicken.process-context.posix#parent-process-id in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7029(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7029,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=stub1995(C_SCHEME_UNDEFINED);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.process#process-signal in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7032(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +5,c,3)))){
C_save_and_reclaim((void*)f_7032,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+5);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
C_word t6;
C_word t7;
t4=C_i_pairp(t3);
t5=(C_truep(t4)?C_get_rest_arg(c,3,av,3,t0):C_fix((C_word)SIGTERM));
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7039,a[2]=t2,a[3]=t5,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* posixunix.scm:1136: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t6;
av2[2]=t2;
av2[3]=lf[405];
tp(4,av2);}}

/* k7037 in chicken.process#process-signal in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7039(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_7039,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7042,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* posixunix.scm:1137: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=lf[405];
tp(4,av2);}}

/* k7040 in k7037 in chicken.process#process-signal in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 in ... */
static void C_ccall f_7042(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_7042,c,av);}
t2=C_kill(((C_word*)t0)[2],((C_word*)t0)[3]);
t3=C_eqp(t2,C_fix(-1));
if(C_truep(t3)){
/* posixunix.scm:1139: posix-error */
t4=lf[183];{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[4];
av2[2]=lf[91];
av2[3]=lf[405];
av2[4]=lf[406];
av2[5]=((C_word*)t0)[2];
av2[6]=((C_word*)t0)[3];
f_2939(7,av2);}}
else{
t4=C_SCHEME_UNDEFINED;
t5=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* chicken.process#process-run in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7072(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +5,c,2)))){
C_save_and_reclaim((void*)f_7072,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+5);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
C_word t6;
C_word t7;
t4=C_i_pairp(t3);
t5=(C_truep(t4)?C_get_rest_arg(c,3,av,3,t0):C_SCHEME_FALSE);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7079,a[2]=t1,a[3]=t5,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* posixunix.scm:1150: chicken.process#process-fork */
t7=*((C_word*)lf[108]+1);{
C_word *av2=av;
av2[0]=t7;
av2[1]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}

/* k7077 in chicken.process#process-run in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7079(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_7079,c,av);}
a=C_alloc(4);
t2=C_eqp(C_fix(0),t1);
if(C_truep(C_i_not(t2))){
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
if(C_truep(((C_word*)t0)[3])){
/* posixunix.scm:1152: chicken.process#process-execute */
t3=*((C_word*)lf[107]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[4];
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f8730,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* posixunix.scm:1142: chicken.process-context#get-environment-variable */
t4=*((C_word*)lf[409]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[410];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}}}

/* k7120 in k7204 in connect-child in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 in ... */
static void C_ccall f_7122(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7122,c,av);}
/* posixunix.scm:1182: chicken.file.posix#file-close */
t2=*((C_word*)lf[11]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* make-on-close in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_7126(C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6,C_word t7){
C_word tmp;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,2)))){
C_save_and_reclaim_args((void *)trf_7126,7,t1,t2,t3,t4,t5,t6,t7);}
a=C_alloc(9);
t8=t1;{
C_word av2[2];
av2[0]=t8;
av2[1]=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_7128,a[2]=t4,a[3]=t5,a[4]=t6,a[5]=t7,a[6]=t3,a[7]=t2,a[8]=((C_word)li175),tmp=(C_word)a,a+=9,tmp);
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}

/* f_7128 in make-on-close in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7128(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(9,c,5)))){
C_save_and_reclaim((void *)f_7128,c,av);}
a=C_alloc(9);
t2=C_i_vector_set(((C_word*)t0)[2],((C_word*)t0)[3],C_SCHEME_TRUE);
t3=C_i_vector_ref(((C_word*)t0)[2],((C_word*)t0)[4]);
t4=(C_truep(t3)?C_i_vector_ref(((C_word*)t0)[2],((C_word*)t0)[5]):C_SCHEME_FALSE);
if(C_truep(t4)){
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7143,a[2]=((C_word*)t0)[6],a[3]=((C_word)li173),tmp=(C_word)a,a+=4,tmp);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7149,a[2]=((C_word*)t0)[7],a[3]=((C_word*)t0)[6],a[4]=((C_word)li174),tmp=(C_word)a,a+=5,tmp);
/* posixunix.scm:1188: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=t1;
av2[2]=t5;
av2[3]=t6;
C_call_with_values(4,av2);}}
else{
t5=C_SCHEME_UNDEFINED;
t6=t1;{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* a7142 */
static void C_ccall f_7143(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7143,c,av);}
/* posixunix.scm:1188: process-wait-impl */
f_6991(t1,((C_word*)t0)[2],C_SCHEME_FALSE);}

/* a7148 */
static void C_ccall f_7149(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_7149,c,av);}
if(C_truep(t3)){
t5=C_SCHEME_UNDEFINED;
t6=t1;{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
/* posixunix.scm:1190: ##sys#signal-hook */
t5=*((C_word*)lf[90]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t5;
av2[1]=t1;
av2[2]=lf[91];
av2[3]=((C_word*)t0)[2];
av2[4]=lf[411];
av2[5]=((C_word*)t0)[3];
av2[6]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(7,av2);}}}

/* needed-pipe in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_7163(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,4)))){
C_save_and_reclaim_args((void *)trf_7163,2,t1,t2);}
a=C_alloc(6);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7172,a[2]=((C_word)li177),tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7178,a[2]=((C_word)li178),tmp=(C_word)a,a+=3,tmp);
/* posixunix.scm:1195: ##sys#call-with-values */{
C_word av2[4];
av2[0]=0;
av2[1]=t1;
av2[2]=t3;
av2[3]=t4;
C_call_with_values(4,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* a7171 in needed-pipe in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7172(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7172,c,av);}
/* posixunix.scm:1195: chicken.process#create-pipe */
t2=*((C_word*)lf[117]+1);{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a7177 in needed-pipe in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7178(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_7178,c,av);}
a=C_alloc(3);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_cons(&a,2,t2,t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* connect-parent in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_7183(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_7183,3,t1,t2,t3);}
a=C_alloc(4);
if(C_truep(t3)){
t4=C_i_car(t2);
t5=C_u_i_cdr(t2);
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7194,a[2]=t1,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* posixunix.scm:1201: chicken.file.posix#file-close */
t7=*((C_word*)lf[11]+1);{
C_word av2[3];
av2[0]=t7;
av2[1]=t6;
av2[2]=t5;
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}
else{
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k7192 in connect-parent in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7194(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7194,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* connect-child in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_7195(C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_7195,4,t1,t2,t3,t4);}
a=C_alloc(5);
if(C_truep(t3)){
t5=C_i_car(t2);
t6=C_u_i_cdr(t2);
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7206,a[2]=t4,a[3]=t5,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* posixunix.scm:1207: chicken.file.posix#file-close */
t8=*((C_word*)lf[11]+1);{
C_word av2[3];
av2[0]=t8;
av2[1]=t7;
av2[2]=t6;
((C_proc)(void*)(*((C_word*)t8+1)))(3,av2);}}
else{
t5=C_SCHEME_UNDEFINED;
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* k7204 in connect-child in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7206(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_7206,c,av);}
a=C_alloc(4);
t2=C_eqp(((C_word*)t0)[2],((C_word*)t0)[3]);
if(C_truep(t2)){
t3=C_SCHEME_UNDEFINED;
t4=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7122,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* posixunix.scm:1181: chicken.file.posix#duplicate-fileno */
t4=*((C_word*)lf[5]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}}

/* spawn in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_7224(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6,C_word t7){
C_word tmp;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,0,2)))){
C_save_and_reclaim_args((void *)trf_7224,8,t0,t1,t2,t3,t4,t5,t6,t7);}
a=C_alloc(11);
t8=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_7228,a[2]=t1,a[3]=t2,a[4]=t3,a[5]=t4,a[6]=((C_word*)t0)[2],a[7]=t7,a[8]=t5,a[9]=t6,a[10]=((C_word*)t0)[3],tmp=(C_word)a,a+=11,tmp);
/* posixunix.scm:1216: needed-pipe */
f_7163(t8,t6);}

/* k7226 in spawn in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7228(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_7228,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_7231,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],tmp=(C_word)a,a+=12,tmp);
/* posixunix.scm:1217: needed-pipe */
f_7163(t2,((C_word*)t0)[8]);}

/* k7229 in k7226 in spawn in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 in ... */
static void C_ccall f_7231(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_7231,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_7234,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=t1,a[11]=((C_word*)t0)[10],tmp=(C_word)a,a+=12,tmp);
/* posixunix.scm:1218: needed-pipe */
f_7163(t2,((C_word*)t0)[8]);}

/* k7232 in k7229 in k7226 in spawn in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in ... */
static void C_ccall f_7234(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,2)))){
C_save_and_reclaim((void *)f_7234,c,av);}
a=C_alloc(16);
t2=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_7241,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],tmp=(C_word)a,a+=13,tmp);
if(C_truep(((C_word*)t0)[10])){
t3=C_i_cdr(((C_word*)t0)[10]);
t4=C_u_i_car(((C_word*)t0)[10]);
t5=t2;
f_7241(t5,C_a_i_cons(&a,2,t3,t4));}
else{
t3=t2;
f_7241(t3,C_SCHEME_FALSE);}}

/* k7239 in k7232 in k7229 in k7226 in spawn in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in ... */
static void C_fcall f_7241(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(19,0,2)))){
C_save_and_reclaim_args((void *)trf_7241,2,t0,t1);}
a=C_alloc(19);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7245,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_7247,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[7],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[8],a[7]=((C_word*)t0)[9],a[8]=((C_word*)t0)[3],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],a[12]=((C_word)li182),tmp=(C_word)a,a+=13,tmp);
/* posixunix.scm:1221: chicken.process#process-fork */
t4=*((C_word*)lf[108]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t2;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k7243 in k7239 in k7232 in k7229 in k7226 in spawn in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in ... */
static void C_ccall f_7245(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_7245,c,av);}
/* posixunix.scm:1219: scheme#values */{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
av2[4]=((C_word*)t0)[5];
av2[5]=t1;
C_values(6,av2);}}

/* a7246 in k7239 in k7232 in k7229 in k7226 in spawn in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in ... */
static void C_ccall f_7247(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(11,c,4)))){
C_save_and_reclaim((void *)f_7247,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_7251,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],tmp=(C_word)a,a+=11,tmp);
/* posixunix.scm:1223: connect-child */
f_7195(t2,((C_word*)t0)[10],((C_word*)t0)[11],*((C_word*)lf[38]+1));}

/* k7249 in a7246 in k7239 in k7232 in k7229 in k7226 in spawn in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in ... */
static void C_ccall f_7251(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,4)))){
C_save_and_reclaim((void *)f_7251,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_7254,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
if(C_truep(((C_word*)t0)[9])){
t3=C_i_cdr(((C_word*)t0)[9]);
t4=C_u_i_car(((C_word*)t0)[9]);
t5=C_a_i_cons(&a,2,t3,t4);
/* posixunix.scm:1224: connect-child */
f_7195(t2,t5,((C_word*)t0)[10],*((C_word*)lf[39]+1));}
else{
/* posixunix.scm:1224: connect-child */
f_7195(t2,C_SCHEME_FALSE,((C_word*)t0)[10],*((C_word*)lf[39]+1));}}

/* k7252 in k7249 in a7246 in k7239 in k7232 in k7229 in k7226 in spawn in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in ... */
static void C_ccall f_7254(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_7254,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7257,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
if(C_truep(((C_word*)t0)[6])){
t3=C_i_cdr(((C_word*)t0)[6]);
t4=C_u_i_car(((C_word*)t0)[6]);
t5=C_a_i_cons(&a,2,t3,t4);
/* posixunix.scm:1225: connect-child */
f_7195(t2,t5,((C_word*)t0)[8],*((C_word*)lf[37]+1));}
else{
/* posixunix.scm:1225: connect-child */
f_7195(t2,C_SCHEME_FALSE,((C_word*)t0)[8],*((C_word*)lf[37]+1));}}

/* k7255 in k7252 in k7249 in a7246 in k7239 in k7232 in k7229 in k7226 in spawn in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in ... */
static void C_ccall f_7257(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_7257,c,av);}
/* posixunix.scm:1226: chicken.process#process-execute */
t2=*((C_word*)lf[107]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
av2[4]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* input-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_7269(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6){
C_word tmp;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_7269,7,t0,t1,t2,t3,t4,t5,t6);}
a=C_alloc(6);
t7=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7273,a[2]=t1,a[3]=t2,a[4]=t3,a[5]=t6,tmp=(C_word)a,a+=6,tmp);
/* posixunix.scm:1229: connect-parent */
f_7183(t7,t4,t5);}

/* k7271 in input-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7273(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_7273,c,av);}
if(C_truep(t1)){
/* posixunix.scm:1230: ##sys#custom-input-port */
t2=*((C_word*)lf[362]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
av2[4]=t1;
av2[5]=C_SCHEME_TRUE;
av2[6]=C_fix(256);
av2[7]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t2+1)))(8,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* output-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_7280(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6){
C_word tmp;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_7280,7,t0,t1,t2,t3,t4,t5,t6);}
a=C_alloc(6);
t7=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7284,a[2]=t1,a[3]=t2,a[4]=t3,a[5]=t6,tmp=(C_word)a,a+=6,tmp);
/* posixunix.scm:1233: connect-parent */
f_7183(t7,t4,t5);}

/* k7282 in output-port in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7284(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_7284,c,av);}
if(C_truep(t1)){
/* posixunix.scm:1234: ##sys#custom-output-port */
t2=*((C_word*)lf[376]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
av2[4]=t1;
av2[5]=C_SCHEME_TRUE;
av2[6]=C_fix(0);
av2[7]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t2+1)))(8,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* chicken.posix#process-impl in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_7291(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6,C_word t7,C_word t8){
C_word tmp;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(21,0,6)))){
C_save_and_reclaim_args((void *)trf_7291,9,t0,t1,t2,t3,t4,t5,t6,t7,t8);}
a=C_alloc(21);
t9=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_7297,a[2]=((C_word*)t0)[2],a[3]=t3,a[4]=t4,a[5]=t5,a[6]=t6,a[7]=t7,a[8]=t8,a[9]=((C_word)li186),tmp=(C_word)a,a+=10,tmp);
t10=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_7303,a[2]=t7,a[3]=t6,a[4]=t8,a[5]=((C_word*)t0)[3],a[6]=t2,a[7]=t3,a[8]=((C_word*)t0)[4],a[9]=((C_word*)t0)[5],a[10]=((C_word)li187),tmp=(C_word)a,a+=11,tmp);
/* posixunix.scm:1236: ##sys#call-with-values */{
C_word av2[4];
av2[0]=0;
av2[1]=t1;
av2[2]=t9;
av2[3]=t10;
C_call_with_values(4,av2);}}

/* a7296 in chicken.posix#process-impl in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7297(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_7297,c,av);}
/* posixunix.scm:1237: spawn */
t2=((C_word*)t0)[2];
f_7224(t2,t1,((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6],((C_word*)t0)[7],((C_word*)t0)[8]);}

/* a7302 in chicken.posix#process-impl in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7303(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(c!=6) C_bad_argc_2(c,6,t0);
if(C_unlikely(!C_demand(C_calculate_demand(26,c,7)))){
C_save_and_reclaim((void *)f_7303,c,av);}
a=C_alloc(26);
t6=C_i_not(((C_word*)t0)[2]);
t7=C_i_not(((C_word*)t0)[3]);
t8=C_i_not(((C_word*)t0)[4]);
t9=C_a_i_vector3(&a,3,t6,t7,t8);
t10=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_7314,a[2]=t1,a[3]=t5,a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=t4,a[8]=((C_word*)t0)[4],a[9]=((C_word*)t0)[8],a[10]=t9,a[11]=((C_word*)t0)[9],a[12]=t3,a[13]=((C_word*)t0)[3],tmp=(C_word)a,a+=14,tmp);
t11=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_7334,a[2]=((C_word*)t0)[5],a[3]=t10,a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[7],a[6]=t2,a[7]=((C_word*)t0)[2],tmp=(C_word)a,a+=8,tmp);
/* posixunix.scm:1245: make-on-close */
f_7126(t11,((C_word*)t0)[6],t5,t9,C_fix(0),C_fix(1),C_fix(2));}

/* k7312 in a7302 in chicken.posix#process-impl in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 in ... */
static void C_ccall f_7314(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(20,c,7)))){
C_save_and_reclaim((void *)f_7314,c,av);}
a=C_alloc(20);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_7318,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],tmp=(C_word)a,a+=12,tmp);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_7330,a[2]=((C_word*)t0)[11],a[3]=t2,a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[12],a[7]=((C_word*)t0)[13],tmp=(C_word)a,a+=8,tmp);
/* posixunix.scm:1248: make-on-close */
f_7126(t3,((C_word*)t0)[5],((C_word*)t0)[3],((C_word*)t0)[10],C_fix(1),C_fix(0),C_fix(2));}

/* k7316 in k7312 in a7302 in chicken.posix#process-impl in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in ... */
static void C_ccall f_7318(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,7)))){
C_save_and_reclaim((void *)f_7318,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7322,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_7326,a[2]=((C_word*)t0)[5],a[3]=t2,a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[7],a[6]=((C_word*)t0)[8],a[7]=((C_word*)t0)[9],tmp=(C_word)a,a+=8,tmp);
/* posixunix.scm:1252: make-on-close */
f_7126(t3,((C_word*)t0)[6],((C_word*)t0)[4],((C_word*)t0)[11],C_fix(2),C_fix(0),C_fix(1));}

/* k7320 in k7316 in k7312 in a7302 in chicken.posix#process-impl in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in ... */
static void C_ccall f_7322(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_7322,c,av);}
/* posixunix.scm:1242: scheme#values */{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
av2[4]=((C_word*)t0)[5];
av2[5]=t1;
C_values(6,av2);}}

/* k7324 in k7316 in k7312 in a7302 in chicken.posix#process-impl in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in ... */
static void C_ccall f_7326(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_7326,c,av);}
/* posixunix.scm:1250: input-port */
t2=((C_word*)t0)[2];
f_7269(t2,((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6],((C_word*)t0)[7],t1);}

/* k7328 in k7312 in a7302 in chicken.posix#process-impl in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in ... */
static void C_ccall f_7330(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_7330,c,av);}
/* posixunix.scm:1246: output-port */
t2=((C_word*)t0)[2];
f_7280(t2,((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6],((C_word*)t0)[7],t1);}

/* k7332 in a7302 in chicken.posix#process-impl in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 in ... */
static void C_ccall f_7334(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_7334,c,av);}
/* posixunix.scm:1243: input-port */
t2=((C_word*)t0)[2];
f_7269(t2,((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6],((C_word*)t0)[7],t1);}

/* %process in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_7347(C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6,C_word t7){
C_word tmp;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(27,0,3)))){
C_save_and_reclaim_args((void *)trf_7347,7,t1,t2,t3,t4,t5,t6,t7);}
a=C_alloc(27);
t8=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t9=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t10=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7349,a[2]=t2,a[3]=((C_word)li191),tmp=(C_word)a,a+=4,tmp);
t11=C_i_check_string_2(((C_word*)t8)[1],t2);
t12=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_7390,a[2]=t2,a[3]=t8,a[4]=t9,a[5]=t6,a[6]=t3,a[7]=t1,a[8]=t7,tmp=(C_word)a,a+=9,tmp);
if(C_truep(((C_word*)t9)[1])){
/* posixunix.scm:1265: chkstrlst */
t13=t10;
f_7349(t13,t12,((C_word*)t9)[1]);}
else{
t13=((C_word*)t8)[1];
t14=C_a_i_list2(&a,2,lf[408],t13);
t15=C_set_block_item(t9,0,t14);
t16=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f8736,a[2]=t8,a[3]=t12,tmp=(C_word)a,a+=4,tmp);
/* posixunix.scm:1142: chicken.process-context#get-environment-variable */
t17=*((C_word*)lf[409]+1);{
C_word av2[3];
av2[0]=t17;
av2[1]=t16;
av2[2]=lf[410];
((C_proc)(void*)(*((C_word*)t17+1)))(3,av2);}}}

/* chkstrlst in %process in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_7349(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_7349,3,t0,t1,t2);}
a=C_alloc(8);
t3=C_i_check_list_2(t2,((C_word*)t0)[2]);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7354,a[2]=((C_word*)t0)[2],a[3]=((C_word)li189),tmp=(C_word)a,a+=4,tmp);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7363,a[2]=t4,a[3]=((C_word)li190),tmp=(C_word)a,a+=4,tmp);
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=(
  f_7363(t5,t2)
);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}

/* g2120 in chkstrlst in %process in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 in ... */
static C_word C_fcall f_7354(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_stack_overflow_check;{}
return(C_i_check_string_2(t1,((C_word*)t0)[2]));}

/* for-each-loop2119 in chkstrlst in %process in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 in ... */
static C_word C_fcall f_7363(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_stack_overflow_check;
loop:{}
if(C_truep(C_i_pairp(t1))){
t2=(
/* posixunix.scm:1262: g2120 */
  f_7354(((C_word*)t0)[2],C_slot(t1,C_fix(0)))
);
t4=C_slot(t1,C_fix(1));
t1=t4;
goto loop;}
else{
t2=C_SCHEME_UNDEFINED;
return(t2);}}

/* k7388 in %process in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7390(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_7390,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_7393,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
if(C_truep(((C_word*)t0)[5])){
/* posixunix.scm:1269: check-environment-list */
f_4089(t2,((C_word*)t0)[5],((C_word*)t0)[2]);}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_7393(2,av2);}}}

/* k7391 in k7388 in %process in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 in ... */
static void C_ccall f_7393(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_7393,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_7398,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word)li192),tmp=(C_word)a,a+=8,tmp);
/* posixunix.scm:1270: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[7];
av2[2]=t2;
av2[3]=((C_word*)t0)[8];
C_call_with_values(4,av2);}}

/* a7397 in k7391 in k7388 in %process in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in ... */
static void C_ccall f_7398(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,8)))){
C_save_and_reclaim((void *)f_7398,c,av);}
/* posixunix.scm:1271: process-impl */
t2=lf[412];
f_7291(t2,t1,((C_word*)t0)[2],((C_word*)((C_word*)t0)[3])[1],((C_word*)((C_word*)t0)[4])[1],((C_word*)t0)[5],C_SCHEME_TRUE,C_SCHEME_TRUE,((C_word*)t0)[6]);}

/* chicken.process#process in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7418(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_7418,c,av);}
a=C_alloc(3);
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,3,t0));
t5=C_rest_nullp(c,3);
t6=C_rest_nullp(c,4);
t7=(C_truep(t6)?C_SCHEME_FALSE:C_get_rest_arg(c,4,av,3,t0));
t8=C_rest_nullp(c,4);
t9=C_rest_nullp(c,5);
t10=(C_truep(t9)?C_SCHEME_FALSE:C_get_rest_arg(c,5,av,3,t0));
t11=C_rest_nullp(c,5);
t12=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7442,a[2]=((C_word)li194),tmp=(C_word)a,a+=3,tmp);
/* posixunix.scm:1275: %process */
f_7347(t1,lf[413],C_SCHEME_FALSE,t2,t4,t7,t12);}

/* a7441 in chicken.process#process in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7442(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word *a;
if(c!=6) C_bad_argc_2(c,6,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_7442,c,av);}
/* posixunix.scm:1277: scheme#values */{
C_word *av2=av;
av2[0]=0;
av2[1]=t1;
av2[2]=t2;
av2[3]=t3;
av2[4]=t4;
C_values(5,av2);}}

/* chicken.process#process* in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7484(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_7484,c,av);}
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,3,t0));
t5=C_rest_nullp(c,3);
t6=C_rest_nullp(c,4);
t7=(C_truep(t6)?C_SCHEME_FALSE:C_get_rest_arg(c,4,av,3,t0));
t8=C_rest_nullp(c,4);
t9=C_rest_nullp(c,5);
t10=(C_truep(t9)?C_SCHEME_FALSE:C_get_rest_arg(c,5,av,3,t0));
if(C_truep(C_rest_nullp(c,5))){
/* posixunix.scm:1280: %process */
f_7347(t1,lf[414],C_SCHEME_TRUE,t2,t4,t7,*((C_word*)lf[302]+1));}
else{
/* posixunix.scm:1280: %process */
f_7347(t1,lf[414],C_SCHEME_TRUE,t2,t4,t7,*((C_word*)lf[302]+1));}}

/* k7547 in chicken.process-context.posix#set-root-directory! in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7549(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_7549,c,av);}
if(C_truep(C_fixnum_lessp(stub2190(C_SCHEME_UNDEFINED,t1),C_fix(0)))){
/* posixunix.scm:1292: posix-error */
t2=lf[183];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[188];
av2[3]=lf[415];
av2[4]=lf[416];
av2[5]=((C_word*)t0)[3];
f_2939(6,av2);}}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* chicken.process-context.posix#set-root-directory! in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7554(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_7554,c,av);}
a=C_alloc(4);
t3=C_i_check_string_2(t2,lf[415]);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7549,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* posixunix.scm:1288: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[190]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[190]+1);
av2[1]=t4;
av2[2]=C_i_foreign_string_argumentp(t2);
tp(3,av2);}}

/* chicken.process#process-spawn in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7573(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7573,c,av);}
/* posixunix.scm:1296: chicken.base#error */
t2=*((C_word*)lf[101]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[111];
av2[3]=lf[417];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.errno#errno in k5506 in k5475 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7579(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7579,c,av);}
/* posix.scm:374: ##sys#errno */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[420]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[420]+1);
av2[1]=t1;
tp(2,av2);}}

/* a7627 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7628(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_7628,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7632,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* posixunix.scm:712: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t3;
av2[2]=t2;
av2[3]=lf[460];
tp(4,av2);}}

/* k7630 in a7627 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7632(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_7632,c,av);}
a=C_alloc(8);
t2=C_getpgid(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7635,a[2]=((C_word*)t0)[3],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_fixnum_lessp(t2,C_fix(0)))){
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7641,a[2]=t3,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* posixunix.scm:715: ##sys#update-errno */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[93]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[93]+1);
av2[1]=t4;
tp(2,av2);}}
else{
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k7633 in k7630 in a7627 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7635(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7635,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k7639 in k7630 in a7627 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7641(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_7641,c,av);}
/* posixunix.scm:716: ##sys#error */
t2=*((C_word*)lf[95]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[460];
av2[3]=lf[461];
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a7645 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7646(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_7646,c,av);}
a=C_alloc(5);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7650,a[2]=t2,a[3]=t3,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* posixunix.scm:719: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t4;
av2[2]=t2;
av2[3]=lf[462];
tp(4,av2);}}

/* k7648 in a7645 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7650(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_7650,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7653,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* posixunix.scm:720: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=lf[462];
tp(4,av2);}}

/* k7651 in k7648 in a7645 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7653(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_7653,c,av);}
a=C_alloc(5);
if(C_truep(C_fixnum_lessp(C_setpgid(((C_word*)t0)[2],((C_word*)t0)[3]),C_fix(0)))){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7662,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* posixunix.scm:722: ##sys#update-errno */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[93]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[93]+1);
av2[1]=t2;
tp(2,av2);}}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k7660 in k7651 in k7648 in a7645 in k5327 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7662(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_7662,c,av);}
/* posixunix.scm:723: ##sys#error */
t2=*((C_word*)lf[95]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[462];
av2[3]=lf[463];
av2[4]=((C_word*)t0)[3];
av2[5]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* a7667 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7668(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7668,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=stub1530(C_SCHEME_UNDEFINED);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a7670 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7671(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_7671,c,av);}
a=C_alloc(4);
if(C_truep(C_fixnum_lessp(C_setegid(t2),C_fix(0)))){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7681,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* posixunix.scm:643: ##sys#update-errno */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[93]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[93]+1);
av2[1]=t3;
tp(2,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k7679 in a7670 in k5323 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7681(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_7681,c,av);}
/* posixunix.scm:644: ##sys#error */
t2=*((C_word*)lf[95]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[466];
av2[3]=lf[467];
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a7686 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7687(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7687,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=stub1526(C_SCHEME_UNDEFINED);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a7689 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7690(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_7690,c,av);}
a=C_alloc(4);
if(C_truep(C_fixnum_lessp(C_setgid(t2),C_fix(0)))){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7700,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* posixunix.scm:634: ##sys#update-errno */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[93]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[93]+1);
av2[1]=t3;
tp(2,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k7698 in a7689 in k5319 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7700(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_7700,c,av);}
/* posixunix.scm:635: ##sys#error */
t2=*((C_word*)lf[95]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[469];
av2[3]=lf[470];
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a7705 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7706(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7706,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=stub1522(C_SCHEME_UNDEFINED);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a7708 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7709(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_7709,c,av);}
a=C_alloc(4);
if(C_truep(C_fixnum_lessp(C_seteuid(t2),C_fix(0)))){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7719,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* posixunix.scm:624: ##sys#update-errno */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[93]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[93]+1);
av2[1]=t3;
tp(2,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k7717 in a7708 in k5315 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7719(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_7719,c,av);}
/* posixunix.scm:625: ##sys#error */
t2=*((C_word*)lf[95]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[472];
av2[3]=lf[473];
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a7724 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7725(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7725,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=stub1518(C_SCHEME_UNDEFINED);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a7727 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7728(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_7728,c,av);}
a=C_alloc(4);
if(C_truep(C_fixnum_lessp(C_setuid(t2),C_fix(0)))){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7738,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* posixunix.scm:615: ##sys#update-errno */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[93]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[93]+1);
av2[1]=t3;
tp(2,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k7736 in a7727 in k5273 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7738(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_7738,c,av);}
/* posixunix.scm:616: ##sys#error */
t2=*((C_word*)lf[95]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[475];
av2[3]=lf[476];
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a7743 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7744(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_7744,c,av);}
a=C_alloc(6);
t2=C_sigprocmask_get(C_fix(0));
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7750,a[2]=t4,a[3]=((C_word)li210),tmp=(C_word)a,a+=4,tmp));
t6=((C_word*)t4)[1];
f_7750(t6,t1,*((C_word*)lf[167]+1),C_SCHEME_END_OF_LIST);}

/* loop in a7743 in k3857 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_fcall f_7750(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,3)))){
C_save_and_reclaim_args((void *)trf_7750,4,t0,t1,t2,t3);}
a=C_alloc(3);
if(C_truep(C_i_nullp(t2))){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=C_i_car(t2);
t5=C_u_i_cdr(t2);
if(C_truep(C_sigismember(t4))){
t6=C_a_i_cons(&a,2,t4,t3);
/* posixunix.scm:580: loop */
t8=t1;
t9=t5;
t10=t6;
t1=t8;
t2=t9;
t3=t10;
goto loop;}
else{
/* posixunix.scm:580: loop */
t8=t1;
t9=t5;
t10=t3;
t1=t8;
t2=t9;
t3=t10;
goto loop;}}}

/* a7773 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7774(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_7774,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7778,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:637: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t3;
av2[2]=t2;
av2[3]=lf[479];
tp(4,av2);}}

/* k7776 in a7773 in k3622 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7778(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7778,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_slot(*((C_word*)lf[268]+1),((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a7782 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7783(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_7783,c,av);}
a=C_alloc(4);
t2=C_rest_nullp(c,2);
t3=(C_truep(t2)?C_SCHEME_FALSE:C_get_rest_arg(c,2,av,2,t0));
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7790,a[2]=t3,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
if(C_truep(t3)){
/* posix-common.scm:562: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t4;
av2[2]=t3;
av2[3]=lf[481];
tp(4,av2);}}
else{
t5=t4;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_UNDEFINED;
f_7790(2,av2);}}}

/* k7788 in a7782 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7790(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7790,c,av);}
if(C_truep(((C_word*)t0)[2])){
t2=C_umask(((C_word*)t0)[2]);
if(C_truep(((C_word*)t0)[2])){
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_umask(t2);
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}
else{
t2=C_umask(C_fix(0));
if(C_truep(((C_word*)t0)[2])){
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_umask(t2);
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}}

/* a7807 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7808(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_7808,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7812,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:567: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t3;
av2[2]=t2;
av2[3]=lf[481];
tp(4,av2);}}

/* k7810 in a7807 in k3405 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7812(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7812,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_umask(((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a7813 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7814(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_7814,c,av);}
a=C_alloc(8);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7818,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7830,a[2]=t2,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:403: chicken.base#port? */
t5=*((C_word*)lf[193]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k7816 in a7813 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7818(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,5)))){
C_save_and_reclaim((void *)f_7818,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7821,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_i_lessp(t1,C_fix(0)))){
/* posix-common.scm:412: posix-error */
t3=lf[183];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[188];
av2[3]=lf[483];
av2[4]=lf[484];
av2[5]=((C_word*)t0)[3];
f_2939(6,av2);}}
else{
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k7819 in k7816 in a7813 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7821(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7821,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k7828 in a7813 in k3188 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7830(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,5)))){
C_save_and_reclaim((void *)f_7830,c,av);}
a=C_alloc(7);
if(C_truep(t1)){
t2=C_slot(((C_word*)t0)[2],C_fix(7));
t3=C_eqp(t2,lf[230]);
if(C_truep(t3)){
t4=C_ftell(&a,1,((C_word*)t0)[2]);
t5=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
f_7818(2,av2);}}
else{
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_fix(-1);
f_7818(2,av2);}}}
else{
if(C_truep(C_fixnump(((C_word*)t0)[2]))){
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_lseek(((C_word*)t0)[2],C_fix(0),C_fix((C_word)SEEK_CUR));
f_7818(2,av2);}}
else{
/* posix-common.scm:410: ##sys#signal-hook */
t2=*((C_word*)lf[90]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[191];
av2[3]=lf[483];
av2[4]=lf[485];
av2[5]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}}}

/* a7850 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7851(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_7851,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7855,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:326: stat */
f_2957(t3,t2,C_SCHEME_FALSE,C_SCHEME_TRUE,lf[199]);}

/* k7853 in a7850 in k3184 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7855(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7855,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)C_stat_perm);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a7856 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7857(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_7857,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7861,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:319: stat */
f_2957(t3,t2,C_SCHEME_FALSE,C_SCHEME_TRUE,lf[488]);}

/* k7859 in a7856 in k3180 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7861(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7861,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)C_statbuf.st_gid);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a7862 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7863(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_7863,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7867,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:313: stat */
f_2957(t3,t2,C_SCHEME_FALSE,C_SCHEME_TRUE,lf[490]);}

/* k7865 in a7862 in k2630 in k2627 in k2624 in k2621 in k2618 in k2615 */
static void C_ccall f_7867(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7867,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)C_statbuf.st_uid);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* toplevel */
static C_TLS int toplevel_initialized=0;

void C_ccall C_posix_toplevel(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(toplevel_initialized) {C_kontinue(t1,C_SCHEME_UNDEFINED);}
else C_toplevel_entry(C_text("posix"));
C_check_nursery_minimum(C_calculate_demand(3,c,2));
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void*)C_posix_toplevel,c,av);}
toplevel_initialized=1;
if(C_unlikely(!C_demand_2(2639))){
C_save(t1);
C_rereclaim2(2639*sizeof(C_word),1);
t1=C_restore;}
a=C_alloc(3);
C_initialize_lf(lf,492);
lf[0]=C_h_intern(&lf[0],5, C_text("posix"));
lf[1]=C_h_intern(&lf[1],19, C_text("chicken.file.posix#"));
lf[2]=C_h_intern(&lf[2],30, C_text("chicken.file.posix#create-fifo"));
lf[3]=C_h_intern(&lf[3],39, C_text("chicken.file.posix#create-symbolic-link"));
lf[4]=C_h_intern(&lf[4],37, C_text("chicken.file.posix#read-symbolic-link"));
lf[5]=C_h_intern(&lf[5],35, C_text("chicken.file.posix#duplicate-fileno"));
lf[6]=C_h_intern(&lf[6],30, C_text("chicken.file.posix#fcntl/dupfd"));
lf[7]=C_h_intern(&lf[7],30, C_text("chicken.file.posix#fcntl/getfd"));
lf[8]=C_h_intern(&lf[8],30, C_text("chicken.file.posix#fcntl/getfl"));
lf[9]=C_h_intern(&lf[9],30, C_text("chicken.file.posix#fcntl/setfd"));
lf[10]=C_h_intern(&lf[10],30, C_text("chicken.file.posix#fcntl/setfl"));
lf[11]=C_h_intern(&lf[11],29, C_text("chicken.file.posix#file-close"));
lf[12]=C_h_intern(&lf[12],31, C_text("chicken.file.posix#file-control"));
lf[13]=C_h_intern(&lf[13],37, C_text("chicken.file.posix#file-creation-mode"));
lf[14]=C_h_intern(&lf[14],29, C_text("chicken.file.posix#file-group"));
lf[15]=C_h_intern(&lf[15],28, C_text("chicken.file.posix#file-link"));
lf[16]=C_h_intern(&lf[16],28, C_text("chicken.file.posix#file-lock"));
lf[17]=C_h_intern(&lf[17],37, C_text("chicken.file.posix#file-lock/blocking"));
lf[18]=C_h_intern(&lf[18],31, C_text("chicken.file.posix#file-mkstemp"));
lf[19]=C_h_intern(&lf[19],28, C_text("chicken.file.posix#file-open"));
lf[20]=C_h_intern(&lf[20],29, C_text("chicken.file.posix#file-owner"));
lf[21]=C_h_intern(&lf[21],35, C_text("chicken.file.posix#file-permissions"));
lf[22]=C_h_intern(&lf[22],32, C_text("chicken.file.posix#file-position"));
lf[23]=C_h_intern(&lf[23],28, C_text("chicken.file.posix#file-read"));
lf[24]=C_h_intern(&lf[24],30, C_text("chicken.file.posix#file-select"));
lf[25]=C_h_intern(&lf[25],33, C_text("chicken.file.posix#file-test-lock"));
lf[26]=C_h_intern(&lf[26],32, C_text("chicken.file.posix#file-truncate"));
lf[27]=C_h_intern(&lf[27],30, C_text("chicken.file.posix#file-unlock"));
lf[28]=C_h_intern(&lf[28],29, C_text("chicken.file.posix#file-write"));
lf[29]=C_h_intern(&lf[29],28, C_text("chicken.file.posix#file-type"));
lf[30]=C_h_intern(&lf[30],32, C_text("chicken.file.posix#block-device\077"));
lf[31]=C_h_intern(&lf[31],36, C_text("chicken.file.posix#character-device\077"));
lf[32]=C_h_intern(&lf[32],29, C_text("chicken.file.posix#directory\077"));
lf[33]=C_h_intern(&lf[33],24, C_text("chicken.file.posix#fifo\077"));
lf[34]=C_h_intern(&lf[34],32, C_text("chicken.file.posix#regular-file\077"));
lf[35]=C_h_intern(&lf[35],26, C_text("chicken.file.posix#socket\077"));
lf[36]=C_h_intern(&lf[36],33, C_text("chicken.file.posix#symbolic-link\077"));
lf[37]=C_h_intern(&lf[37],32, C_text("chicken.file.posix#fileno/stderr"));
lf[38]=C_h_intern(&lf[38],31, C_text("chicken.file.posix#fileno/stdin"));
lf[39]=C_h_intern(&lf[39],32, C_text("chicken.file.posix#fileno/stdout"));
lf[40]=C_h_intern(&lf[40],35, C_text("chicken.file.posix#open-input-file\052"));
lf[41]=C_h_intern(&lf[41],36, C_text("chicken.file.posix#open-output-file\052"));
lf[42]=C_h_intern(&lf[42],30, C_text("chicken.file.posix#open/append"));
lf[43]=C_h_intern(&lf[43],30, C_text("chicken.file.posix#open/binary"));
lf[44]=C_h_intern(&lf[44],29, C_text("chicken.file.posix#open/creat"));
lf[45]=C_h_intern(&lf[45],28, C_text("chicken.file.posix#open/excl"));
lf[46]=C_h_intern(&lf[46],29, C_text("chicken.file.posix#open/fsync"));
lf[47]=C_h_intern(&lf[47],30, C_text("chicken.file.posix#open/noctty"));
lf[48]=C_h_intern(&lf[48],33, C_text("chicken.file.posix#open/noinherit"));
lf[49]=C_h_intern(&lf[49],32, C_text("chicken.file.posix#open/nonblock"));
lf[50]=C_h_intern(&lf[50],30, C_text("chicken.file.posix#open/rdonly"));
lf[51]=C_h_intern(&lf[51],28, C_text("chicken.file.posix#open/rdwr"));
lf[52]=C_h_intern(&lf[52],28, C_text("chicken.file.posix#open/read"));
lf[53]=C_h_intern(&lf[53],28, C_text("chicken.file.posix#open/sync"));
lf[54]=C_h_intern(&lf[54],28, C_text("chicken.file.posix#open/text"));
lf[55]=C_h_intern(&lf[55],29, C_text("chicken.file.posix#open/trunc"));
lf[56]=C_h_intern(&lf[56],29, C_text("chicken.file.posix#open/write"));
lf[57]=C_h_intern(&lf[57],30, C_text("chicken.file.posix#open/wronly"));
lf[58]=C_h_intern(&lf[58],29, C_text("chicken.file.posix#perm/irgrp"));
lf[59]=C_h_intern(&lf[59],29, C_text("chicken.file.posix#perm/iroth"));
lf[60]=C_h_intern(&lf[60],29, C_text("chicken.file.posix#perm/irusr"));
lf[61]=C_h_intern(&lf[61],29, C_text("chicken.file.posix#perm/irwxg"));
lf[62]=C_h_intern(&lf[62],29, C_text("chicken.file.posix#perm/irwxo"));
lf[63]=C_h_intern(&lf[63],29, C_text("chicken.file.posix#perm/irwxu"));
lf[64]=C_h_intern(&lf[64],29, C_text("chicken.file.posix#perm/isgid"));
lf[65]=C_h_intern(&lf[65],29, C_text("chicken.file.posix#perm/isuid"));
lf[66]=C_h_intern(&lf[66],29, C_text("chicken.file.posix#perm/isvtx"));
lf[67]=C_h_intern(&lf[67],29, C_text("chicken.file.posix#perm/iwgrp"));
lf[68]=C_h_intern(&lf[68],29, C_text("chicken.file.posix#perm/iwoth"));
lf[69]=C_h_intern(&lf[69],29, C_text("chicken.file.posix#perm/iwusr"));
lf[70]=C_h_intern(&lf[70],29, C_text("chicken.file.posix#perm/ixgrp"));
lf[71]=C_h_intern(&lf[71],29, C_text("chicken.file.posix#perm/ixoth"));
lf[72]=C_h_intern(&lf[72],29, C_text("chicken.file.posix#perm/ixusr"));
lf[73]=C_h_intern(&lf[73],31, C_text("chicken.file.posix#port->fileno"));
lf[74]=C_h_intern(&lf[74],27, C_text("chicken.file.posix#seek/cur"));
lf[75]=C_h_intern(&lf[75],27, C_text("chicken.file.posix#seek/end"));
lf[76]=C_h_intern(&lf[76],27, C_text("chicken.file.posix#seek/set"));
lf[77]=C_h_intern(&lf[77],37, C_text("chicken.file.posix#set-file-position!"));
lf[78]=C_h_intern(&lf[78],19, C_text("chicken.time.posix#"));
lf[79]=C_h_intern(&lf[79],36, C_text("chicken.time.posix#seconds->utc-time"));
lf[80]=C_h_intern(&lf[80],36, C_text("chicken.time.posix#utc-time->seconds"));
lf[81]=C_h_intern(&lf[81],38, C_text("chicken.time.posix#seconds->local-time"));
lf[82]=C_h_intern(&lf[82],34, C_text("chicken.time.posix#seconds->string"));
lf[83]=C_h_intern(&lf[83],38, C_text("chicken.time.posix#local-time->seconds"));
lf[84]=C_h_intern(&lf[84],31, C_text("chicken.time.posix#string->time"));
lf[85]=C_h_intern(&lf[85],31, C_text("chicken.time.posix#time->string"));
lf[86]=C_h_intern(&lf[86],46, C_text("chicken.time.posix#local-timezone-abbreviation"));
lf[87]=C_h_intern(&lf[87],16, C_text("chicken.process#"));
lf[88]=C_h_intern(&lf[88],22, C_text("chicken.process#system"));
lf[89]=C_h_intern(&lf[89],6, C_text("system"));
lf[90]=C_h_intern(&lf[90],17, C_text("##sys#signal-hook"));
lf[91]=C_h_intern_kw(&lf[91],13, C_text("process-error"));
lf[92]=C_decode_literal(C_heaptop,C_text("\376B\000\000\032`system\047 invocation failed"));
lf[93]=C_h_intern(&lf[93],18, C_text("##sys#update-errno"));
lf[94]=C_h_intern(&lf[94],23, C_text("chicken.process#system\052"));
lf[95]=C_h_intern(&lf[95],11, C_text("##sys#error"));
lf[96]=C_decode_literal(C_heaptop,C_text("\376B\000\0003shell invocation failed with non-zero return status"));
lf[97]=C_h_intern(&lf[97],18, C_text("chicken.process#qs"));
lf[98]=C_h_intern(&lf[98],7, C_text("mingw32"));
lf[99]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002\042\042"));
lf[100]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004\047\134\047\047"));
lf[101]=C_h_intern(&lf[101],18, C_text("chicken.base#error"));
lf[102]=C_h_intern(&lf[102],2, C_text("qs"));
lf[103]=C_decode_literal(C_heaptop,C_text("\376B\000\0004NUL character can not be represented in shell string"));
lf[104]=C_h_intern(&lf[104],20, C_text("scheme#string-append"));
lf[105]=C_h_intern(&lf[105],18, C_text("##sys#string->list"));
lf[106]=C_h_intern(&lf[106],33, C_text("chicken.platform#software-version"));
lf[107]=C_h_intern(&lf[107],31, C_text("chicken.process#process-execute"));
lf[108]=C_h_intern(&lf[108],28, C_text("chicken.process#process-fork"));
lf[109]=C_h_intern(&lf[109],27, C_text("chicken.process#process-run"));
lf[110]=C_h_intern(&lf[110],30, C_text("chicken.process#process-signal"));
lf[111]=C_h_intern(&lf[111],29, C_text("chicken.process#process-spawn"));
lf[112]=C_h_intern(&lf[112],28, C_text("chicken.process#process-wait"));
lf[113]=C_h_intern(&lf[113],36, C_text("chicken.process#call-with-input-pipe"));
lf[114]=C_h_intern(&lf[114],37, C_text("chicken.process#call-with-output-pipe"));
lf[115]=C_h_intern(&lf[115],32, C_text("chicken.process#close-input-pipe"));
lf[116]=C_h_intern(&lf[116],33, C_text("chicken.process#close-output-pipe"));
lf[117]=C_h_intern(&lf[117],27, C_text("chicken.process#create-pipe"));
lf[118]=C_h_intern(&lf[118],31, C_text("chicken.process#open-input-pipe"));
lf[119]=C_h_intern(&lf[119],32, C_text("chicken.process#open-output-pipe"));
lf[120]=C_h_intern(&lf[120],36, C_text("chicken.process#with-input-from-pipe"));
lf[121]=C_h_intern(&lf[121],35, C_text("chicken.process#with-output-to-pipe"));
lf[122]=C_h_intern(&lf[122],23, C_text("chicken.process#process"));
lf[123]=C_h_intern(&lf[123],24, C_text("chicken.process#process\052"));
lf[124]=C_h_intern(&lf[124],29, C_text("chicken.process#process-sleep"));
lf[125]=C_h_intern(&lf[125],24, C_text("chicken.process#pipe/buf"));
lf[126]=C_h_intern(&lf[126],29, C_text("chicken.process#spawn/overlay"));
lf[127]=C_h_intern(&lf[127],26, C_text("chicken.process#spawn/wait"));
lf[128]=C_h_intern(&lf[128],28, C_text("chicken.process#spawn/nowait"));
lf[129]=C_h_intern(&lf[129],29, C_text("chicken.process#spawn/nowaito"));
lf[130]=C_h_intern(&lf[130],28, C_text("chicken.process#spawn/detach"));
lf[131]=C_h_intern(&lf[131],23, C_text("chicken.process.signal#"));
lf[132]=C_h_intern(&lf[132],33, C_text("chicken.process.signal#set-alarm!"));
lf[133]=C_h_intern(&lf[133],42, C_text("chicken.process.signal#set-signal-handler!"));
lf[134]=C_h_intern(&lf[134],39, C_text("chicken.process.signal#set-signal-mask!"));
lf[135]=C_h_intern(&lf[135],37, C_text("chicken.process.signal#signal-handler"));
lf[136]=C_h_intern(&lf[136],34, C_text("chicken.process.signal#signal-mask"));
lf[137]=C_h_intern(&lf[137],35, C_text("chicken.process.signal#signal-mask!"));
lf[138]=C_h_intern(&lf[138],37, C_text("chicken.process.signal#signal-masked\077"));
lf[139]=C_h_intern(&lf[139],37, C_text("chicken.process.signal#signal-unmask!"));
lf[140]=C_h_intern(&lf[140],34, C_text("chicken.process.signal#signal/abrt"));
lf[141]=C_h_intern(&lf[141],34, C_text("chicken.process.signal#signal/alrm"));
lf[142]=C_h_intern(&lf[142],35, C_text("chicken.process.signal#signal/break"));
lf[143]=C_h_intern(&lf[143],33, C_text("chicken.process.signal#signal/bus"));
lf[144]=C_h_intern(&lf[144],34, C_text("chicken.process.signal#signal/chld"));
lf[145]=C_h_intern(&lf[145],34, C_text("chicken.process.signal#signal/cont"));
lf[146]=C_h_intern(&lf[146],33, C_text("chicken.process.signal#signal/fpe"));
lf[147]=C_h_intern(&lf[147],33, C_text("chicken.process.signal#signal/hup"));
lf[148]=C_h_intern(&lf[148],33, C_text("chicken.process.signal#signal/ill"));
lf[149]=C_h_intern(&lf[149],33, C_text("chicken.process.signal#signal/int"));
lf[150]=C_h_intern(&lf[150],32, C_text("chicken.process.signal#signal/io"));
lf[151]=C_h_intern(&lf[151],34, C_text("chicken.process.signal#signal/kill"));
lf[152]=C_h_intern(&lf[152],34, C_text("chicken.process.signal#signal/pipe"));
lf[153]=C_h_intern(&lf[153],34, C_text("chicken.process.signal#signal/prof"));
lf[154]=C_h_intern(&lf[154],34, C_text("chicken.process.signal#signal/quit"));
lf[155]=C_h_intern(&lf[155],34, C_text("chicken.process.signal#signal/segv"));
lf[156]=C_h_intern(&lf[156],34, C_text("chicken.process.signal#signal/stop"));
lf[157]=C_h_intern(&lf[157],34, C_text("chicken.process.signal#signal/term"));
lf[158]=C_h_intern(&lf[158],34, C_text("chicken.process.signal#signal/trap"));
lf[159]=C_h_intern(&lf[159],34, C_text("chicken.process.signal#signal/tstp"));
lf[160]=C_h_intern(&lf[160],33, C_text("chicken.process.signal#signal/urg"));
lf[161]=C_h_intern(&lf[161],34, C_text("chicken.process.signal#signal/usr1"));
lf[162]=C_h_intern(&lf[162],34, C_text("chicken.process.signal#signal/usr2"));
lf[163]=C_h_intern(&lf[163],36, C_text("chicken.process.signal#signal/vtalrm"));
lf[164]=C_h_intern(&lf[164],35, C_text("chicken.process.signal#signal/winch"));
lf[165]=C_h_intern(&lf[165],34, C_text("chicken.process.signal#signal/xcpu"));
lf[166]=C_h_intern(&lf[166],34, C_text("chicken.process.signal#signal/xfsz"));
lf[167]=C_h_intern(&lf[167],35, C_text("chicken.process.signal#signals-list"));
lf[168]=C_h_intern(&lf[168],30, C_text("chicken.process-context.posix#"));
lf[169]=C_h_intern(&lf[169],47, C_text("chicken.process-context.posix#change-directory\052"));
lf[170]=C_h_intern(&lf[170],49, C_text("chicken.process-context.posix#set-root-directory!"));
lf[171]=C_h_intern(&lf[171],56, C_text("chicken.process-context.posix#current-effective-group-id"));
lf[172]=C_h_intern(&lf[172],55, C_text("chicken.process-context.posix#current-effective-user-id"));
lf[173]=C_h_intern(&lf[173],46, C_text("chicken.process-context.posix#current-group-id"));
lf[174]=C_h_intern(&lf[174],45, C_text("chicken.process-context.posix#current-user-id"));
lf[175]=C_h_intern(&lf[175],48, C_text("chicken.process-context.posix#current-process-id"));
lf[176]=C_h_intern(&lf[176],47, C_text("chicken.process-context.posix#parent-process-id"));
lf[177]=C_h_intern(&lf[177],47, C_text("chicken.process-context.posix#current-user-name"));
lf[178]=C_h_intern(&lf[178],57, C_text("chicken.process-context.posix#current-effective-user-name"));
lf[179]=C_h_intern(&lf[179],44, C_text("chicken.process-context.posix#create-session"));
lf[180]=C_h_intern(&lf[180],46, C_text("chicken.process-context.posix#process-group-id"));
lf[181]=C_h_intern(&lf[181],46, C_text("chicken.process-context.posix#user-information"));
lf[182]=C_h_intern(&lf[182],14, C_text("chicken.posix#"));
lf[184]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003 - "));
lf[185]=C_h_intern(&lf[185],19, C_text("##sys#peek-c-string"));
lf[186]=C_h_intern(&lf[186],17, C_text("##sys#posix-error"));
lf[188]=C_h_intern_kw(&lf[188],10, C_text("file-error"));
lf[189]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022cannot access file"));
lf[190]=C_h_intern(&lf[190],19, C_text("##sys#make-c-string"));
lf[191]=C_h_intern_kw(&lf[191],10, C_text("type-error"));
lf[192]=C_decode_literal(C_heaptop,C_text("\376B\000\0000bad argument type - not a fixnum, port or string"));
lf[193]=C_h_intern(&lf[193],18, C_text("chicken.base#port\077"));
lf[194]=C_h_intern(&lf[194],28, C_text("chicken.file.posix#file-stat"));
lf[195]=C_h_intern(&lf[195],9, C_text("file-stat"));
lf[196]=C_h_intern(&lf[196],40, C_text("chicken.file.posix#set-file-permissions!"));
lf[197]=C_h_intern(&lf[197],21, C_text("set-file-permissions!"));
lf[198]=C_decode_literal(C_heaptop,C_text("\376B\000\000\036cannot change file permissions"));
lf[199]=C_h_intern(&lf[199],16, C_text("file-permissions"));
lf[200]=C_decode_literal(C_heaptop,C_text("\376B\000\0000bad argument type - not a fixnum, port or string"));
lf[201]=C_h_intern(&lf[201],18, C_text("##sys#check-fixnum"));
lf[202]=C_h_intern(&lf[202],41, C_text("chicken.file.posix#file-modification-time"));
lf[203]=C_h_intern(&lf[203],22, C_text("file-modification-time"));
lf[204]=C_h_intern(&lf[204],35, C_text("chicken.file.posix#file-access-time"));
lf[205]=C_h_intern(&lf[205],16, C_text("file-access-time"));
lf[206]=C_h_intern(&lf[206],35, C_text("chicken.file.posix#file-change-time"));
lf[207]=C_h_intern(&lf[207],16, C_text("file-change-time"));
lf[208]=C_h_intern(&lf[208],34, C_text("chicken.file.posix#set-file-times!"));
lf[209]=C_h_intern(&lf[209],15, C_text("set-file-times!"));
lf[210]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025cannot set file times"));
lf[211]=C_h_intern(&lf[211],25, C_text("##sys#check-exact-integer"));
lf[212]=C_h_intern(&lf[212],28, C_text("chicken.time#current-seconds"));
lf[213]=C_h_intern(&lf[213],28, C_text("chicken.file.posix#file-size"));
lf[214]=C_h_intern(&lf[214],9, C_text("file-size"));
lf[215]=C_h_intern(&lf[215],34, C_text("chicken.file.posix#set-file-owner!"));
lf[217]=C_h_intern(&lf[217],15, C_text("set-file-owner!"));
lf[218]=C_h_intern(&lf[218],34, C_text("chicken.file.posix#set-file-group!"));
lf[219]=C_h_intern(&lf[219],15, C_text("set-file-group!"));
lf[220]=C_h_intern(&lf[220],12, C_text("regular-file"));
lf[221]=C_h_intern(&lf[221],13, C_text("symbolic-link"));
lf[222]=C_h_intern(&lf[222],9, C_text("directory"));
lf[223]=C_h_intern(&lf[223],16, C_text("character-device"));
lf[224]=C_h_intern(&lf[224],12, C_text("block-device"));
lf[225]=C_h_intern(&lf[225],4, C_text("fifo"));
lf[226]=C_h_intern(&lf[226],6, C_text("socket"));
lf[227]=C_h_intern(&lf[227],9, C_text("file-type"));
lf[228]=C_h_intern(&lf[228],18, C_text("set-file-position!"));
lf[229]=C_decode_literal(C_heaptop,C_text("\376B\000\000\030cannot set file position"));
lf[230]=C_h_intern(&lf[230],6, C_text("stream"));
lf[231]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014invalid file"));
lf[232]=C_h_intern_kw(&lf[232],6, C_text("append"));
lf[233]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001a"));
lf[234]=C_decode_literal(C_heaptop,C_text("\376B\000\000\033invalid mode for input file"));
lf[235]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025invalid mode argument"));
lf[236]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001r"));
lf[237]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001w"));
lf[238]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020cannot open file"));
lf[239]=C_h_intern(&lf[239],15, C_text("##sys#make-port"));
lf[240]=C_h_intern(&lf[240],23, C_text("##sys#stream-port-class"));
lf[241]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010(fdport)"));
lf[242]=C_h_intern(&lf[242],16, C_text("open-input-file\052"));
lf[243]=C_h_intern(&lf[243],17, C_text("open-output-file\052"));
lf[244]=C_h_intern(&lf[244],12, C_text("port->fileno"));
lf[245]=C_h_intern(&lf[245],15, C_text("##sys#port-data"));
lf[246]=C_decode_literal(C_heaptop,C_text("\376B\000\000%cannot access file-descriptor of port"));
lf[247]=C_decode_literal(C_heaptop,C_text("\376B\000\000\031port has no attached file"));
lf[248]=C_h_intern(&lf[248],27, C_text("##sys#peek-unsigned-integer"));
lf[249]=C_h_intern(&lf[249],16, C_text("duplicate-fileno"));
lf[250]=C_decode_literal(C_heaptop,C_text("\376B\000\000 cannot duplicate file-descriptor"));
lf[251]=C_h_intern(&lf[251],17, C_text("change-directory\052"));
lf[252]=C_decode_literal(C_heaptop,C_text("\376B\000\000\037cannot change current directory"));
lf[253]=C_h_intern(&lf[253],27, C_text("##sys#change-directory-hook"));
lf[256]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025time vector too short"));
lf[257]=C_h_intern(&lf[257],19, C_text("seconds->local-time"));
lf[258]=C_h_intern(&lf[258],17, C_text("seconds->utc-time"));
lf[259]=C_h_intern(&lf[259],15, C_text("##sys#substring"));
lf[260]=C_h_intern(&lf[260],15, C_text("seconds->string"));
lf[261]=C_decode_literal(C_heaptop,C_text("\376B\000\000 cannot convert seconds to string"));
lf[262]=C_h_intern(&lf[262],19, C_text("local-time->seconds"));
lf[263]=C_decode_literal(C_heaptop,C_text("\376B\000\000%cannot convert time vector to seconds"));
lf[264]=C_h_intern(&lf[264],17, C_text("##sys#make-string"));
lf[265]=C_h_intern(&lf[265],12, C_text("time->string"));
lf[266]=C_decode_literal(C_heaptop,C_text("\376B\000\000 time formatting overflows buffer"));
lf[267]=C_decode_literal(C_heaptop,C_text("\376B\000\000$cannot convert time vector to string"));
lf[268]=C_h_intern(&lf[268],19, C_text("##sys#signal-vector"));
lf[269]=C_h_intern(&lf[269],19, C_text("set-signal-handler!"));
lf[270]=C_h_intern(&lf[270],13, C_text("process-sleep"));
lf[272]=C_h_intern(&lf[272],12, C_text("process-wait"));
lf[273]=C_decode_literal(C_heaptop,C_text("\376B\000\000 waiting for child process failed"));
lf[275]=C_h_intern(&lf[275],24, C_text("chicken.condition#signal"));
lf[277]=C_h_intern(&lf[277],34, C_text("chicken.memory#pointer-vector-set!"));
lf[278]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015Out of memory"));
lf[279]=C_h_intern(&lf[279],40, C_text("chicken.condition#with-exception-handler"));
lf[280]=C_h_intern(&lf[280],37, C_text("scheme#call-with-current-continuation"));
lf[281]=C_h_intern(&lf[281],34, C_text("chicken.memory#make-pointer-vector"));
lf[282]=C_h_intern(&lf[282],19, C_text("chicken.memory#free"));
lf[283]=C_h_intern(&lf[283],33, C_text("chicken.memory#pointer-vector-ref"));
lf[284]=C_h_intern(&lf[284],36, C_text("chicken.memory#pointer-vector-length"));
lf[286]=C_h_intern(&lf[286],41, C_text("chicken.pathname#pathname-strip-directory"));
lf[288]=C_h_intern(&lf[288],3, C_text("map"));
lf[289]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001="));
lf[290]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020cannot open pipe"));
lf[291]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006(pipe)"));
lf[292]=C_h_intern(&lf[292],15, C_text("open-input-pipe"));
lf[293]=C_h_intern_kw(&lf[293],4, C_text("text"));
lf[294]=C_h_intern_kw(&lf[294],6, C_text("binary"));
lf[295]=C_decode_literal(C_heaptop,C_text("\376B\000\000#illegal input/output mode specifier"));
lf[296]=C_h_intern(&lf[296],16, C_text("open-output-pipe"));
lf[297]=C_h_intern(&lf[297],16, C_text("close-input-pipe"));
lf[298]=C_decode_literal(C_heaptop,C_text("\376B\000\000\030error while closing pipe"));
lf[299]=C_h_intern(&lf[299],17, C_text("close-output-pipe"));
lf[300]=C_decode_literal(C_heaptop,C_text("\376B\000\000\030error while closing pipe"));
lf[301]=C_h_intern(&lf[301],20, C_text("##sys#standard-input"));
lf[302]=C_h_intern(&lf[302],13, C_text("scheme#values"));
lf[303]=C_h_intern(&lf[303],18, C_text("##sys#dynamic-wind"));
lf[304]=C_h_intern(&lf[304],21, C_text("##sys#standard-output"));
lf[305]=C_h_intern(&lf[305],23, C_text("##sys#file-nonblocking!"));
lf[306]=C_h_intern(&lf[306],21, C_text("##sys#file-select-one"));
lf[307]=C_h_intern(&lf[307],12, C_text("file-control"));
lf[308]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023cannot control file"));
lf[309]=C_h_intern(&lf[309],9, C_text("file-open"));
lf[310]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020cannot open file"));
lf[311]=C_h_intern(&lf[311],24, C_text("##sys#dispatch-interrupt"));
lf[312]=C_h_intern(&lf[312],10, C_text("file-close"));
lf[313]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021cannot close file"));
lf[314]=C_h_intern(&lf[314],9, C_text("file-read"));
lf[315]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025cannot read from file"));
lf[316]=C_decode_literal(C_heaptop,C_text("\376B\000\000(bad argument type - not a string or blob"));
lf[317]=C_h_intern(&lf[317],18, C_text("scheme#make-string"));
lf[318]=C_h_intern(&lf[318],10, C_text("file-write"));
lf[319]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024cannot write to file"));
lf[320]=C_decode_literal(C_heaptop,C_text("\376B\000\000(bad argument type - not a string or blob"));
lf[321]=C_h_intern(&lf[321],12, C_text("file-mkstemp"));
lf[322]=C_decode_literal(C_heaptop,C_text("\376B\000\000\034cannot create temporary file"));
lf[323]=C_h_intern(&lf[323],11, C_text("file-select"));
lf[324]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006failed"));
lf[325]=C_h_intern(&lf[325],18, C_text("##sys#fast-reverse"));
lf[326]=C_h_intern(&lf[326],10, C_text("scheme#max"));
lf[327]=C_h_intern(&lf[327],15, C_text("##sys#make-blob"));
lf[328]=C_h_intern(&lf[328],11, C_text("create-pipe"));
lf[329]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022cannot create pipe"));
lf[330]=C_h_intern(&lf[330],16, C_text("set-signal-mask!"));
lf[331]=C_decode_literal(C_heaptop,C_text("\376B\000\000\026cannot set signal mask"));
lf[332]=C_h_intern(&lf[332],14, C_text("signal-masked\077"));
lf[333]=C_h_intern(&lf[333],12, C_text("signal-mask!"));
lf[334]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023cannot block signal"));
lf[335]=C_h_intern(&lf[335],14, C_text("signal-unmask!"));
lf[336]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025cannot unblock signal"));
lf[337]=C_h_intern(&lf[337],13, C_text("scheme#vector"));
lf[338]=C_h_intern(&lf[338],11, C_text("scheme#list"));
lf[339]=C_h_intern(&lf[339],27, C_text("##sys#peek-nonnull-c-string"));
lf[340]=C_h_intern(&lf[340],16, C_text("user-information"));
lf[341]=C_decode_literal(C_heaptop,C_text("\376B\000\000\030cannot change file owner"));
lf[342]=C_decode_literal(C_heaptop,C_text("\376B\000\0000bad argument type - not a fixnum, port or string"));
lf[343]=C_h_intern(&lf[343],14, C_text("create-session"));
lf[344]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025cannot create session"));
lf[345]=C_h_intern(&lf[345],20, C_text("create-symbolic-link"));
lf[346]=C_h_intern(&lf[346],18, C_text("create-symbol-link"));
lf[347]=C_decode_literal(C_heaptop,C_text("\376B\000\000\033cannot create symbolic link"));
lf[348]=C_h_intern(&lf[348],24, C_text("##sys#read-symbolic-link"));
lf[349]=C_decode_literal(C_heaptop,C_text("\376B\000\000\031cannot read symbolic link"));
lf[350]=C_h_intern(&lf[350],16, C_text("scheme#substring"));
lf[351]=C_h_intern(&lf[351],18, C_text("read-symbolic-link"));
lf[352]=C_h_intern(&lf[352],36, C_text("chicken.pathname#decompose-directory"));
lf[353]=C_h_intern(&lf[353],30, C_text("chicken.pathname#make-pathname"));
lf[354]=C_h_intern(&lf[354],35, C_text("chicken.pathname#absolute-pathname\077"));
lf[355]=C_decode_literal(C_heaptop,C_text("\376B\000\000Icould not canonicalize path with symbolic links, component does not exist"));
lf[356]=C_h_intern(&lf[356],18, C_text("##sys#file-exists\077"));
lf[357]=C_decode_literal(C_heaptop,C_text("\376B\000\000\000"));
lf[358]=C_decode_literal(C_heaptop,C_text("\376B\000\000\000"));
lf[359]=C_h_intern(&lf[359],9, C_text("file-link"));
lf[360]=C_h_intern(&lf[360],9, C_text("hard-link"));
lf[361]=C_decode_literal(C_heaptop,C_text("\376B\000\000\032could not create hard link"));
lf[362]=C_h_intern(&lf[362],23, C_text("##sys#custom-input-port"));
lf[363]=C_h_intern(&lf[363],17, C_text("chicken.base#void"));
lf[364]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015cannot select"));
lf[365]=C_h_intern(&lf[365],19, C_text("##sys#thread-yield!"));
lf[366]=C_h_intern(&lf[366],27, C_text("##sys#thread-block-for-i/o!"));
lf[367]=C_h_intern(&lf[367],20, C_text("##sys#current-thread"));
lf[368]=C_h_intern_kw(&lf[368],5, C_text("input"));
lf[369]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013cannot read"));
lf[370]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013cannot read"));
lf[371]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014cannot close"));
lf[372]=C_h_intern(&lf[372],35, C_text("chicken.fixnum#most-positive-fixnum"));
lf[373]=C_h_intern(&lf[373],22, C_text("##sys#scan-buffer-line"));
lf[374]=C_decode_literal(C_heaptop,C_text("\376B\000\000\000"));
lf[375]=C_h_intern(&lf[375],28, C_text("chicken.port#make-input-port"));
lf[376]=C_h_intern(&lf[376],24, C_text("##sys#custom-output-port"));
lf[377]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014cannot write"));
lf[378]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014cannot close"));
lf[379]=C_h_intern(&lf[379],29, C_text("chicken.port#make-output-port"));
lf[380]=C_h_intern(&lf[380],13, C_text("file-truncate"));
lf[381]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024cannot truncate file"));
lf[382]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014invalid file"));
lf[383]=C_h_intern(&lf[383],4, C_text("lock"));
lf[384]=C_h_intern(&lf[384],9, C_text("file-lock"));
lf[385]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020cannot lock file"));
lf[386]=C_h_intern(&lf[386],18, C_text("file-lock/blocking"));
lf[387]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020cannot lock file"));
lf[388]=C_h_intern(&lf[388],14, C_text("file-test-lock"));
lf[389]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022cannot unlock file"));
lf[390]=C_h_intern(&lf[390],11, C_text("file-unlock"));
lf[391]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022cannot unlock file"));
lf[392]=C_h_intern(&lf[392],11, C_text("create-fifo"));
lf[393]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022cannot create FIFO"));
lf[394]=C_decode_literal(C_heaptop,C_text("\376B\000\000\027%a %b %e %H:%M:%S %Z %Y"));
lf[395]=C_h_intern(&lf[395],12, C_text("string->time"));
lf[396]=C_h_intern(&lf[396],17, C_text("utc-time->seconds"));
lf[397]=C_decode_literal(C_heaptop,C_text("\376B\000\000%cannot convert time vector to seconds"));
lf[398]=C_h_intern(&lf[398],24, C_text("##sys#kill-other-threads"));
lf[399]=C_h_intern(&lf[399],17, C_text("chicken.base#exit"));
lf[400]=C_h_intern(&lf[400],23, C_text("##sys#call-with-cthulhu"));
lf[401]=C_h_intern(&lf[401],12, C_text("process-fork"));
lf[402]=C_decode_literal(C_heaptop,C_text("\376B\000\000\033cannot create child process"));
lf[403]=C_h_intern(&lf[403],15, C_text("process-execute"));
lf[404]=C_decode_literal(C_heaptop,C_text("\376B\000\000\026cannot execute process"));
lf[405]=C_h_intern(&lf[405],14, C_text("process-signal"));
lf[406]=C_decode_literal(C_heaptop,C_text("\376B\000\000 could not send signal to process"));
lf[407]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007/bin/sh"));
lf[408]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002-c"));
lf[409]=C_h_intern(&lf[409],48, C_text("chicken.process-context#get-environment-variable"));
lf[410]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005SHELL"));
lf[411]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025abnormal process exit"));
lf[413]=C_h_intern(&lf[413],7, C_text("process"));
lf[414]=C_h_intern(&lf[414],8, C_text("process\052"));
lf[415]=C_h_intern(&lf[415],19, C_text("set-root-directory!"));
lf[416]=C_decode_literal(C_heaptop,C_text("\376B\000\000\037unable to change root directory"));
lf[417]=C_decode_literal(C_heaptop,C_text("\376B\000\000/this function is not available on this platform"));
lf[418]=C_h_intern(&lf[418],14, C_text("chicken.errno#"));
lf[419]=C_h_intern(&lf[419],19, C_text("chicken.errno#errno"));
lf[420]=C_h_intern(&lf[420],11, C_text("##sys#errno"));
lf[421]=C_h_intern(&lf[421],24, C_text("chicken.errno#errno/2big"));
lf[422]=C_h_intern(&lf[422],25, C_text("chicken.errno#errno/acces"));
lf[423]=C_h_intern(&lf[423],25, C_text("chicken.errno#errno/again"));
lf[424]=C_h_intern(&lf[424],24, C_text("chicken.errno#errno/badf"));
lf[425]=C_h_intern(&lf[425],24, C_text("chicken.errno#errno/busy"));
lf[426]=C_h_intern(&lf[426],25, C_text("chicken.errno#errno/child"));
lf[427]=C_h_intern(&lf[427],26, C_text("chicken.errno#errno/deadlk"));
lf[428]=C_h_intern(&lf[428],23, C_text("chicken.errno#errno/dom"));
lf[429]=C_h_intern(&lf[429],25, C_text("chicken.errno#errno/exist"));
lf[430]=C_h_intern(&lf[430],25, C_text("chicken.errno#errno/fault"));
lf[431]=C_h_intern(&lf[431],24, C_text("chicken.errno#errno/fbig"));
lf[432]=C_h_intern(&lf[432],25, C_text("chicken.errno#errno/ilseq"));
lf[433]=C_h_intern(&lf[433],24, C_text("chicken.errno#errno/intr"));
lf[434]=C_h_intern(&lf[434],25, C_text("chicken.errno#errno/inval"));
lf[435]=C_h_intern(&lf[435],22, C_text("chicken.errno#errno/io"));
lf[436]=C_h_intern(&lf[436],25, C_text("chicken.errno#errno/isdir"));
lf[437]=C_h_intern(&lf[437],25, C_text("chicken.errno#errno/mfile"));
lf[438]=C_h_intern(&lf[438],25, C_text("chicken.errno#errno/mlink"));
lf[439]=C_h_intern(&lf[439],31, C_text("chicken.errno#errno/nametoolong"));
lf[440]=C_h_intern(&lf[440],25, C_text("chicken.errno#errno/nfile"));
lf[441]=C_h_intern(&lf[441],25, C_text("chicken.errno#errno/nodev"));
lf[442]=C_h_intern(&lf[442],25, C_text("chicken.errno#errno/noent"));
lf[443]=C_h_intern(&lf[443],26, C_text("chicken.errno#errno/noexec"));
lf[444]=C_h_intern(&lf[444],25, C_text("chicken.errno#errno/nolck"));
lf[445]=C_h_intern(&lf[445],25, C_text("chicken.errno#errno/nomem"));
lf[446]=C_h_intern(&lf[446],25, C_text("chicken.errno#errno/nospc"));
lf[447]=C_h_intern(&lf[447],25, C_text("chicken.errno#errno/nosys"));
lf[448]=C_h_intern(&lf[448],26, C_text("chicken.errno#errno/notdir"));
lf[449]=C_h_intern(&lf[449],28, C_text("chicken.errno#errno/notempty"));
lf[450]=C_h_intern(&lf[450],25, C_text("chicken.errno#errno/notty"));
lf[451]=C_h_intern(&lf[451],24, C_text("chicken.errno#errno/nxio"));
lf[452]=C_h_intern(&lf[452],24, C_text("chicken.errno#errno/perm"));
lf[453]=C_h_intern(&lf[453],24, C_text("chicken.errno#errno/pipe"));
lf[454]=C_h_intern(&lf[454],25, C_text("chicken.errno#errno/range"));
lf[455]=C_h_intern(&lf[455],24, C_text("chicken.errno#errno/rofs"));
lf[456]=C_h_intern(&lf[456],25, C_text("chicken.errno#errno/spipe"));
lf[457]=C_h_intern(&lf[457],24, C_text("chicken.errno#errno/srch"));
lf[458]=C_h_intern(&lf[458],30, C_text("chicken.errno#errno/wouldblock"));
lf[459]=C_h_intern(&lf[459],24, C_text("chicken.errno#errno/xdev"));
lf[460]=C_h_intern(&lf[460],16, C_text("process-group-id"));
lf[461]=C_decode_literal(C_heaptop,C_text("\376B\000\000 cannot retrieve process group ID"));
lf[462]=C_h_intern(&lf[462],13, C_text("process-group"));
lf[463]=C_decode_literal(C_heaptop,C_text("\376B\000\000\033cannot set process group ID"));
lf[464]=C_h_intern(&lf[464],31, C_text("chicken.base#getter-with-setter"));
lf[465]=C_decode_literal(C_heaptop,C_text("\376B\000\0004(chicken.process-context.posix#process-group-id pid)"));
lf[466]=C_h_intern(&lf[466],26, C_text("effective-group-id!-setter"));
lf[467]=C_decode_literal(C_heaptop,C_text("\376B\000\000\035cannot set effective group ID"));
lf[468]=C_decode_literal(C_heaptop,C_text("\376B\000\000:(chicken.process-context.posix#current-effective-group-id)"));
lf[469]=C_h_intern(&lf[469],24, C_text("current-group-id!-setter"));
lf[470]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023cannot set group ID"));
lf[471]=C_decode_literal(C_heaptop,C_text("\376B\000\0000(chicken.process-context.posix#current-group-id)"));
lf[472]=C_h_intern(&lf[472],25, C_text("effective-user-id!-setter"));
lf[473]=C_decode_literal(C_heaptop,C_text("\376B\000\000\034cannot set effective user ID"));
lf[474]=C_decode_literal(C_heaptop,C_text("\376B\000\0009(chicken.process-context.posix#current-effective-user-id)"));
lf[475]=C_h_intern(&lf[475],23, C_text("current-user-id!-setter"));
lf[476]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022cannot set user ID"));
lf[477]=C_decode_literal(C_heaptop,C_text("\376B\000\000/(chicken.process-context.posix#current-user-id)"));
lf[478]=C_decode_literal(C_heaptop,C_text("\376B\000\000$(chicken.process.signal#signal-mask)"));
lf[479]=C_h_intern(&lf[479],14, C_text("signal-handler"));
lf[480]=C_decode_literal(C_heaptop,C_text("\376B\000\000+(chicken.process.signal#signal-handler sig)"));
lf[481]=C_h_intern(&lf[481],18, C_text("file-creation-mode"));
lf[482]=C_decode_literal(C_heaptop,C_text("\376B\000\000,(chicken.file.posix#file-creation-mode mode)"));
lf[483]=C_h_intern(&lf[483],13, C_text("file-position"));
lf[484]=C_decode_literal(C_heaptop,C_text("\376B\000\000%cannot retrieve file position of port"));
lf[485]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014invalid file"));
lf[486]=C_decode_literal(C_heaptop,C_text("\376B\000\000\047(chicken.file.posix#file-position port)"));
lf[487]=C_decode_literal(C_heaptop,C_text("\376B\000\000\047(chicken.file.posix#file-permissions f)"));
lf[488]=C_h_intern(&lf[488],10, C_text("file-group"));
lf[489]=C_decode_literal(C_heaptop,C_text("\376B\000\000!(chicken.file.posix#file-group f)"));
lf[490]=C_h_intern(&lf[490],10, C_text("file-owner"));
lf[491]=C_decode_literal(C_heaptop,C_text("\376B\000\000!(chicken.file.posix#file-owner f)"));
C_register_lf2(lf,492,create_ptable());{}
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2617,a[2]=t1,tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_scheduler_toplevel(2,av2);}}

#ifdef C_ENABLE_PTABLES
static C_PTABLE_ENTRY ptable[511] = {
{C_text("f8684:posix_2escm"),(void*)f8684},
{C_text("f8688:posix_2escm"),(void*)f8688},
{C_text("f8730:posix_2escm"),(void*)f8730},
{C_text("f8736:posix_2escm"),(void*)f8736},
{C_text("f_2617:posix_2escm"),(void*)f_2617},
{C_text("f_2620:posix_2escm"),(void*)f_2620},
{C_text("f_2623:posix_2escm"),(void*)f_2623},
{C_text("f_2626:posix_2escm"),(void*)f_2626},
{C_text("f_2629:posix_2escm"),(void*)f_2629},
{C_text("f_2632:posix_2escm"),(void*)f_2632},
{C_text("f_2718:posix_2escm"),(void*)f_2718},
{C_text("f_2731:posix_2escm"),(void*)f_2731},
{C_text("f_2736:posix_2escm"),(void*)f_2736},
{C_text("f_2740:posix_2escm"),(void*)f_2740},
{C_text("f_2752:posix_2escm"),(void*)f_2752},
{C_text("f_2756:posix_2escm"),(void*)f_2756},
{C_text("f_2766:posix_2escm"),(void*)f_2766},
{C_text("f_2787:posix_2escm"),(void*)f_2787},
{C_text("f_2790:posix_2escm"),(void*)f_2790},
{C_text("f_2801:posix_2escm"),(void*)f_2801},
{C_text("f_2807:posix_2escm"),(void*)f_2807},
{C_text("f_2832:posix_2escm"),(void*)f_2832},
{C_text("f_2939:posix_2escm"),(void*)f_2939},
{C_text("f_2943:posix_2escm"),(void*)f_2943},
{C_text("f_2950:posix_2escm"),(void*)f_2950},
{C_text("f_2954:posix_2escm"),(void*)f_2954},
{C_text("f_2957:posix_2escm"),(void*)f_2957},
{C_text("f_2961:posix_2escm"),(void*)f_2961},
{C_text("f_2982:posix_2escm"),(void*)f_2982},
{C_text("f_2986:posix_2escm"),(void*)f_2986},
{C_text("f_2995:posix_2escm"),(void*)f_2995},
{C_text("f_3003:posix_2escm"),(void*)f_3003},
{C_text("f_3010:posix_2escm"),(void*)f_3010},
{C_text("f_3021:posix_2escm"),(void*)f_3021},
{C_text("f_3025:posix_2escm"),(void*)f_3025},
{C_text("f_3028:posix_2escm"),(void*)f_3028},
{C_text("f_3046:posix_2escm"),(void*)f_3046},
{C_text("f_3050:posix_2escm"),(void*)f_3050},
{C_text("f_3060:posix_2escm"),(void*)f_3060},
{C_text("f_3065:posix_2escm"),(void*)f_3065},
{C_text("f_3069:posix_2escm"),(void*)f_3069},
{C_text("f_3071:posix_2escm"),(void*)f_3071},
{C_text("f_3075:posix_2escm"),(void*)f_3075},
{C_text("f_3077:posix_2escm"),(void*)f_3077},
{C_text("f_3081:posix_2escm"),(void*)f_3081},
{C_text("f_3083:posix_2escm"),(void*)f_3083},
{C_text("f_3087:posix_2escm"),(void*)f_3087},
{C_text("f_3099:posix_2escm"),(void*)f_3099},
{C_text("f_3102:posix_2escm"),(void*)f_3102},
{C_text("f_3108:posix_2escm"),(void*)f_3108},
{C_text("f_3118:posix_2escm"),(void*)f_3118},
{C_text("f_3162:posix_2escm"),(void*)f_3162},
{C_text("f_3166:posix_2escm"),(void*)f_3166},
{C_text("f_3168:posix_2escm"),(void*)f_3168},
{C_text("f_3174:posix_2escm"),(void*)f_3174},
{C_text("f_3182:posix_2escm"),(void*)f_3182},
{C_text("f_3186:posix_2escm"),(void*)f_3186},
{C_text("f_3190:posix_2escm"),(void*)f_3190},
{C_text("f_3192:posix_2escm"),(void*)f_3192},
{C_text("f_3211:posix_2escm"),(void*)f_3211},
{C_text("f_3279:posix_2escm"),(void*)f_3279},
{C_text("f_3287:posix_2escm"),(void*)f_3287},
{C_text("f_3289:posix_2escm"),(void*)f_3289},
{C_text("f_3297:posix_2escm"),(void*)f_3297},
{C_text("f_3299:posix_2escm"),(void*)f_3299},
{C_text("f_3307:posix_2escm"),(void*)f_3307},
{C_text("f_3309:posix_2escm"),(void*)f_3309},
{C_text("f_3317:posix_2escm"),(void*)f_3317},
{C_text("f_3319:posix_2escm"),(void*)f_3319},
{C_text("f_3327:posix_2escm"),(void*)f_3327},
{C_text("f_3329:posix_2escm"),(void*)f_3329},
{C_text("f_3337:posix_2escm"),(void*)f_3337},
{C_text("f_3339:posix_2escm"),(void*)f_3339},
{C_text("f_3347:posix_2escm"),(void*)f_3347},
{C_text("f_3352:posix_2escm"),(void*)f_3352},
{C_text("f_3359:posix_2escm"),(void*)f_3359},
{C_text("f_3362:posix_2escm"),(void*)f_3362},
{C_text("f_3368:posix_2escm"),(void*)f_3368},
{C_text("f_3374:posix_2escm"),(void*)f_3374},
{C_text("f_3407:posix_2escm"),(void*)f_3407},
{C_text("f_3435:posix_2escm"),(void*)f_3435},
{C_text("f_3443:posix_2escm"),(void*)f_3443},
{C_text("f_3472:posix_2escm"),(void*)f_3472},
{C_text("f_3485:posix_2escm"),(void*)f_3485},
{C_text("f_3491:posix_2escm"),(void*)f_3491},
{C_text("f_3495:posix_2escm"),(void*)f_3495},
{C_text("f_3503:posix_2escm"),(void*)f_3503},
{C_text("f_3505:posix_2escm"),(void*)f_3505},
{C_text("f_3509:posix_2escm"),(void*)f_3509},
{C_text("f_3517:posix_2escm"),(void*)f_3517},
{C_text("f_3519:posix_2escm"),(void*)f_3519},
{C_text("f_3535:posix_2escm"),(void*)f_3535},
{C_text("f_3544:posix_2escm"),(void*)f_3544},
{C_text("f_3558:posix_2escm"),(void*)f_3558},
{C_text("f_3564:posix_2escm"),(void*)f_3564},
{C_text("f_3568:posix_2escm"),(void*)f_3568},
{C_text("f_3571:posix_2escm"),(void*)f_3571},
{C_text("f_3574:posix_2escm"),(void*)f_3574},
{C_text("f_3589:posix_2escm"),(void*)f_3589},
{C_text("f_3591:posix_2escm"),(void*)f_3591},
{C_text("f_3594:posix_2escm"),(void*)f_3594},
{C_text("f_3598:posix_2escm"),(void*)f_3598},
{C_text("f_3601:posix_2escm"),(void*)f_3601},
{C_text("f_3610:posix_2escm"),(void*)f_3610},
{C_text("f_3624:posix_2escm"),(void*)f_3624},
{C_text("f_3627:posix_2escm"),(void*)f_3627},
{C_text("f_3646:posix_2escm"),(void*)f_3646},
{C_text("f_3650:posix_2escm"),(void*)f_3650},
{C_text("f_3653:posix_2escm"),(void*)f_3653},
{C_text("f_3667:posix_2escm"),(void*)f_3667},
{C_text("f_3671:posix_2escm"),(void*)f_3671},
{C_text("f_3674:posix_2escm"),(void*)f_3674},
{C_text("f_3699:posix_2escm"),(void*)f_3699},
{C_text("f_3703:posix_2escm"),(void*)f_3703},
{C_text("f_3706:posix_2escm"),(void*)f_3706},
{C_text("f_3709:posix_2escm"),(void*)f_3709},
{C_text("f_3737:posix_2escm"),(void*)f_3737},
{C_text("f_3741:posix_2escm"),(void*)f_3741},
{C_text("f_3745:posix_2escm"),(void*)f_3745},
{C_text("f_3782:posix_2escm"),(void*)f_3782},
{C_text("f_3789:posix_2escm"),(void*)f_3789},
{C_text("f_3798:posix_2escm"),(void*)f_3798},
{C_text("f_3808:posix_2escm"),(void*)f_3808},
{C_text("f_3812:posix_2escm"),(void*)f_3812},
{C_text("f_3815:posix_2escm"),(void*)f_3815},
{C_text("f_3836:posix_2escm"),(void*)f_3836},
{C_text("f_3844:posix_2escm"),(void*)f_3844},
{C_text("f_3848:posix_2escm"),(void*)f_3848},
{C_text("f_3859:posix_2escm"),(void*)f_3859},
{C_text("f_3861:posix_2escm"),(void*)f_3861},
{C_text("f_3865:posix_2escm"),(void*)f_3865},
{C_text("f_3867:posix_2escm"),(void*)f_3867},
{C_text("f_3886:posix_2escm"),(void*)f_3886},
{C_text("f_3891:posix_2escm"),(void*)f_3891},
{C_text("f_3897:posix_2escm"),(void*)f_3897},
{C_text("f_3938:posix_2escm"),(void*)f_3938},
{C_text("f_3946:posix_2escm"),(void*)f_3946},
{C_text("f_3949:posix_2escm"),(void*)f_3949},
{C_text("f_3954:posix_2escm"),(void*)f_3954},
{C_text("f_3960:posix_2escm"),(void*)f_3960},
{C_text("f_3966:posix_2escm"),(void*)f_3966},
{C_text("f_3970:posix_2escm"),(void*)f_3970},
{C_text("f_3975:posix_2escm"),(void*)f_3975},
{C_text("f_3977:posix_2escm"),(void*)f_3977},
{C_text("f_3981:posix_2escm"),(void*)f_3981},
{C_text("f_3983:posix_2escm"),(void*)f_3983},
{C_text("f_3999:posix_2escm"),(void*)f_3999},
{C_text("f_4005:posix_2escm"),(void*)f_4005},
{C_text("f_4008:posix_2escm"),(void*)f_4008},
{C_text("f_4024:posix_2escm"),(void*)f_4024},
{C_text("f_4034:posix_2escm"),(void*)f_4034},
{C_text("f_4040:posix_2escm"),(void*)f_4040},
{C_text("f_4051:posix_2escm"),(void*)f_4051},
{C_text("f_4055:posix_2escm"),(void*)f_4055},
{C_text("f_4059:posix_2escm"),(void*)f_4059},
{C_text("f_4064:posix_2escm"),(void*)f_4064},
{C_text("f_4074:posix_2escm"),(void*)f_4074},
{C_text("f_4077:posix_2escm"),(void*)f_4077},
{C_text("f_4089:posix_2escm"),(void*)f_4089},
{C_text("f_4094:posix_2escm"),(void*)f_4094},
{C_text("f_4113:posix_2escm"),(void*)f_4113},
{C_text("f_4136:posix_2escm"),(void*)f_4136},
{C_text("f_4138:posix_2escm"),(void*)f_4138},
{C_text("f_4142:posix_2escm"),(void*)f_4142},
{C_text("f_4148:posix_2escm"),(void*)f_4148},
{C_text("f_4151:posix_2escm"),(void*)f_4151},
{C_text("f_4156:posix_2escm"),(void*)f_4156},
{C_text("f_4162:posix_2escm"),(void*)f_4162},
{C_text("f_4168:posix_2escm"),(void*)f_4168},
{C_text("f_4172:posix_2escm"),(void*)f_4172},
{C_text("f_4175:posix_2escm"),(void*)f_4175},
{C_text("f_4183:posix_2escm"),(void*)f_4183},
{C_text("f_4189:posix_2escm"),(void*)f_4189},
{C_text("f_4193:posix_2escm"),(void*)f_4193},
{C_text("f_4200:posix_2escm"),(void*)f_4200},
{C_text("f_4203:posix_2escm"),(void*)f_4203},
{C_text("f_4207:posix_2escm"),(void*)f_4207},
{C_text("f_4228:posix_2escm"),(void*)f_4228},
{C_text("f_4230:posix_2escm"),(void*)f_4230},
{C_text("f_4255:posix_2escm"),(void*)f_4255},
{C_text("f_4264:posix_2escm"),(void*)f_4264},
{C_text("f_4270:posix_2escm"),(void*)f_4270},
{C_text("f_4295:posix_2escm"),(void*)f_4295},
{C_text("f_4308:posix_2escm"),(void*)f_4308},
{C_text("f_4314:posix_2escm"),(void*)f_4314},
{C_text("f_4328:posix_2escm"),(void*)f_4328},
{C_text("f_4335:posix_2escm"),(void*)f_4335},
{C_text("f_4345:posix_2escm"),(void*)f_4345},
{C_text("f_4354:posix_2escm"),(void*)f_4354},
{C_text("f_4368:posix_2escm"),(void*)f_4368},
{C_text("f_4375:posix_2escm"),(void*)f_4375},
{C_text("f_4385:posix_2escm"),(void*)f_4385},
{C_text("f_4394:posix_2escm"),(void*)f_4394},
{C_text("f_4401:posix_2escm"),(void*)f_4401},
{C_text("f_4409:posix_2escm"),(void*)f_4409},
{C_text("f_4416:posix_2escm"),(void*)f_4416},
{C_text("f_4424:posix_2escm"),(void*)f_4424},
{C_text("f_4428:posix_2escm"),(void*)f_4428},
{C_text("f_4433:posix_2escm"),(void*)f_4433},
{C_text("f_4438:posix_2escm"),(void*)f_4438},
{C_text("f_4444:posix_2escm"),(void*)f_4444},
{C_text("f_4448:posix_2escm"),(void*)f_4448},
{C_text("f_4453:posix_2escm"),(void*)f_4453},
{C_text("f_4458:posix_2escm"),(void*)f_4458},
{C_text("f_4462:posix_2escm"),(void*)f_4462},
{C_text("f_4467:posix_2escm"),(void*)f_4467},
{C_text("f_4473:posix_2escm"),(void*)f_4473},
{C_text("f_4477:posix_2escm"),(void*)f_4477},
{C_text("f_4482:posix_2escm"),(void*)f_4482},
{C_text("f_4486:posix_2escm"),(void*)f_4486},
{C_text("f_4491:posix_2escm"),(void*)f_4491},
{C_text("f_4497:posix_2escm"),(void*)f_4497},
{C_text("f_4501:posix_2escm"),(void*)f_4501},
{C_text("f_4506:posix_2escm"),(void*)f_4506},
{C_text("f_4510:posix_2escm"),(void*)f_4510},
{C_text("f_4515:posix_2escm"),(void*)f_4515},
{C_text("f_4520:posix_2escm"),(void*)f_4520},
{C_text("f_4526:posix_2escm"),(void*)f_4526},
{C_text("f_4530:posix_2escm"),(void*)f_4530},
{C_text("f_4535:posix_2escm"),(void*)f_4535},
{C_text("f_4540:posix_2escm"),(void*)f_4540},
{C_text("f_4547:posix_2escm"),(void*)f_4547},
{C_text("f_4587:posix_2escm"),(void*)f_4587},
{C_text("f_4594:posix_2escm"),(void*)f_4594},
{C_text("f_4597:posix_2escm"),(void*)f_4597},
{C_text("f_4621:posix_2escm"),(void*)f_4621},
{C_text("f_4631:posix_2escm"),(void*)f_4631},
{C_text("f_4634:posix_2escm"),(void*)f_4634},
{C_text("f_4638:posix_2escm"),(void*)f_4638},
{C_text("f_4641:posix_2escm"),(void*)f_4641},
{C_text("f_4653:posix_2escm"),(void*)f_4653},
{C_text("f_4657:posix_2escm"),(void*)f_4657},
{C_text("f_4662:posix_2escm"),(void*)f_4662},
{C_text("f_4684:posix_2escm"),(void*)f_4684},
{C_text("f_4688:posix_2escm"),(void*)f_4688},
{C_text("f_4691:posix_2escm"),(void*)f_4691},
{C_text("f_4694:posix_2escm"),(void*)f_4694},
{C_text("f_4697:posix_2escm"),(void*)f_4697},
{C_text("f_4700:posix_2escm"),(void*)f_4700},
{C_text("f_4724:posix_2escm"),(void*)f_4724},
{C_text("f_4728:posix_2escm"),(void*)f_4728},
{C_text("f_4731:posix_2escm"),(void*)f_4731},
{C_text("f_4737:posix_2escm"),(void*)f_4737},
{C_text("f_4740:posix_2escm"),(void*)f_4740},
{C_text("f_4761:posix_2escm"),(void*)f_4761},
{C_text("f_4768:posix_2escm"),(void*)f_4768},
{C_text("f_4774:posix_2escm"),(void*)f_4774},
{C_text("f_4781:posix_2escm"),(void*)f_4781},
{C_text("f_4793:posix_2escm"),(void*)f_4793},
{C_text("f_4800:posix_2escm"),(void*)f_4800},
{C_text("f_4803:posix_2escm"),(void*)f_4803},
{C_text("f_4811:posix_2escm"),(void*)f_4811},
{C_text("f_4814:posix_2escm"),(void*)f_4814},
{C_text("f_4875:posix_2escm"),(void*)f_4875},
{C_text("f_4878:posix_2escm"),(void*)f_4878},
{C_text("f_4885:posix_2escm"),(void*)f_4885},
{C_text("f_4915:posix_2escm"),(void*)f_4915},
{C_text("f_4974:posix_2escm"),(void*)f_4974},
{C_text("f_5035:posix_2escm"),(void*)f_5035},
{C_text("f_5042:posix_2escm"),(void*)f_5042},
{C_text("f_5044:posix_2escm"),(void*)f_5044},
{C_text("f_5086:posix_2escm"),(void*)f_5086},
{C_text("f_5170:posix_2escm"),(void*)f_5170},
{C_text("f_5177:posix_2escm"),(void*)f_5177},
{C_text("f_5226:posix_2escm"),(void*)f_5226},
{C_text("f_5235:posix_2escm"),(void*)f_5235},
{C_text("f_5238:posix_2escm"),(void*)f_5238},
{C_text("f_5250:posix_2escm"),(void*)f_5250},
{C_text("f_5275:posix_2escm"),(void*)f_5275},
{C_text("f_5277:posix_2escm"),(void*)f_5277},
{C_text("f_5281:posix_2escm"),(void*)f_5281},
{C_text("f_5283:posix_2escm"),(void*)f_5283},
{C_text("f_5287:posix_2escm"),(void*)f_5287},
{C_text("f_5299:posix_2escm"),(void*)f_5299},
{C_text("f_5303:posix_2escm"),(void*)f_5303},
{C_text("f_5317:posix_2escm"),(void*)f_5317},
{C_text("f_5321:posix_2escm"),(void*)f_5321},
{C_text("f_5325:posix_2escm"),(void*)f_5325},
{C_text("f_5329:posix_2escm"),(void*)f_5329},
{C_text("f_5331:posix_2escm"),(void*)f_5331},
{C_text("f_5338:posix_2escm"),(void*)f_5338},
{C_text("f_5351:posix_2escm"),(void*)f_5351},
{C_text("f_5355:posix_2escm"),(void*)f_5355},
{C_text("f_5359:posix_2escm"),(void*)f_5359},
{C_text("f_5363:posix_2escm"),(void*)f_5363},
{C_text("f_5367:posix_2escm"),(void*)f_5367},
{C_text("f_5377:posix_2escm"),(void*)f_5377},
{C_text("f_5385:posix_2escm"),(void*)f_5385},
{C_text("f_5393:posix_2escm"),(void*)f_5393},
{C_text("f_5397:posix_2escm"),(void*)f_5397},
{C_text("f_5399:posix_2escm"),(void*)f_5399},
{C_text("f_5407:posix_2escm"),(void*)f_5407},
{C_text("f_5411:posix_2escm"),(void*)f_5411},
{C_text("f_5413:posix_2escm"),(void*)f_5413},
{C_text("f_5417:posix_2escm"),(void*)f_5417},
{C_text("f_5420:posix_2escm"),(void*)f_5420},
{C_text("f_5423:posix_2escm"),(void*)f_5423},
{C_text("f_5435:posix_2escm"),(void*)f_5435},
{C_text("f_5439:posix_2escm"),(void*)f_5439},
{C_text("f_5455:posix_2escm"),(void*)f_5455},
{C_text("f_5460:posix_2escm"),(void*)f_5460},
{C_text("f_5464:posix_2escm"),(void*)f_5464},
{C_text("f_5470:posix_2escm"),(void*)f_5470},
{C_text("f_5477:posix_2escm"),(void*)f_5477},
{C_text("f_5479:posix_2escm"),(void*)f_5479},
{C_text("f_5500:posix_2escm"),(void*)f_5500},
{C_text("f_5504:posix_2escm"),(void*)f_5504},
{C_text("f_5508:posix_2escm"),(void*)f_5508},
{C_text("f_5509:posix_2escm"),(void*)f_5509},
{C_text("f_5514:posix_2escm"),(void*)f_5514},
{C_text("f_5528:posix_2escm"),(void*)f_5528},
{C_text("f_5543:posix_2escm"),(void*)f_5543},
{C_text("f_5549:posix_2escm"),(void*)f_5549},
{C_text("f_5557:posix_2escm"),(void*)f_5557},
{C_text("f_5559:posix_2escm"),(void*)f_5559},
{C_text("f_5569:posix_2escm"),(void*)f_5569},
{C_text("f_5575:posix_2escm"),(void*)f_5575},
{C_text("f_5584:posix_2escm"),(void*)f_5584},
{C_text("f_5587:posix_2escm"),(void*)f_5587},
{C_text("f_5590:posix_2escm"),(void*)f_5590},
{C_text("f_5596:posix_2escm"),(void*)f_5596},
{C_text("f_5630:posix_2escm"),(void*)f_5630},
{C_text("f_5634:posix_2escm"),(void*)f_5634},
{C_text("f_5643:posix_2escm"),(void*)f_5643},
{C_text("f_5665:posix_2escm"),(void*)f_5665},
{C_text("f_5693:posix_2escm"),(void*)f_5693},
{C_text("f_5699:posix_2escm"),(void*)f_5699},
{C_text("f_5700:posix_2escm"),(void*)f_5700},
{C_text("f_5704:posix_2escm"),(void*)f_5704},
{C_text("f_5729:posix_2escm"),(void*)f_5729},
{C_text("f_5737:posix_2escm"),(void*)f_5737},
{C_text("f_5743:posix_2escm"),(void*)f_5743},
{C_text("f_5762:posix_2escm"),(void*)f_5762},
{C_text("f_5765:posix_2escm"),(void*)f_5765},
{C_text("f_5795:posix_2escm"),(void*)f_5795},
{C_text("f_5798:posix_2escm"),(void*)f_5798},
{C_text("f_5804:posix_2escm"),(void*)f_5804},
{C_text("f_5833:posix_2escm"),(void*)f_5833},
{C_text("f_5839:posix_2escm"),(void*)f_5839},
{C_text("f_5843:posix_2escm"),(void*)f_5843},
{C_text("f_5864:posix_2escm"),(void*)f_5864},
{C_text("f_5876:posix_2escm"),(void*)f_5876},
{C_text("f_5880:posix_2escm"),(void*)f_5880},
{C_text("f_5892:posix_2escm"),(void*)f_5892},
{C_text("f_5896:posix_2escm"),(void*)f_5896},
{C_text("f_5907:posix_2escm"),(void*)f_5907},
{C_text("f_5917:posix_2escm"),(void*)f_5917},
{C_text("f_5965:posix_2escm"),(void*)f_5965},
{C_text("f_5983:posix_2escm"),(void*)f_5983},
{C_text("f_5987:posix_2escm"),(void*)f_5987},
{C_text("f_6001:posix_2escm"),(void*)f_6001},
{C_text("f_6011:posix_2escm"),(void*)f_6011},
{C_text("f_6031:posix_2escm"),(void*)f_6031},
{C_text("f_6057:posix_2escm"),(void*)f_6057},
{C_text("f_6102:posix_2escm"),(void*)f_6102},
{C_text("f_6112:posix_2escm"),(void*)f_6112},
{C_text("f_6178:posix_2escm"),(void*)f_6178},
{C_text("f_6200:posix_2escm"),(void*)f_6200},
{C_text("f_6201:posix_2escm"),(void*)f_6201},
{C_text("f_6207:posix_2escm"),(void*)f_6207},
{C_text("f_6226:posix_2escm"),(void*)f_6226},
{C_text("f_6257:posix_2escm"),(void*)f_6257},
{C_text("f_6267:posix_2escm"),(void*)f_6267},
{C_text("f_6272:posix_2escm"),(void*)f_6272},
{C_text("f_6278:posix_2escm"),(void*)f_6278},
{C_text("f_6284:posix_2escm"),(void*)f_6284},
{C_text("f_6288:posix_2escm"),(void*)f_6288},
{C_text("f_6300:posix_2escm"),(void*)f_6300},
{C_text("f_6308:posix_2escm"),(void*)f_6308},
{C_text("f_6322:posix_2escm"),(void*)f_6322},
{C_text("f_6323:posix_2escm"),(void*)f_6323},
{C_text("f_6340:posix_2escm"),(void*)f_6340},
{C_text("f_6350:posix_2escm"),(void*)f_6350},
{C_text("f_6433:posix_2escm"),(void*)f_6433},
{C_text("f_6437:posix_2escm"),(void*)f_6437},
{C_text("f_6443:posix_2escm"),(void*)f_6443},
{C_text("f_6450:posix_2escm"),(void*)f_6450},
{C_text("f_6457:posix_2escm"),(void*)f_6457},
{C_text("f_6463:posix_2escm"),(void*)f_6463},
{C_text("f_6467:posix_2escm"),(void*)f_6467},
{C_text("f_6478:posix_2escm"),(void*)f_6478},
{C_text("f_6496:posix_2escm"),(void*)f_6496},
{C_text("f_6499:posix_2escm"),(void*)f_6499},
{C_text("f_6564:posix_2escm"),(void*)f_6564},
{C_text("f_6570:posix_2escm"),(void*)f_6570},
{C_text("f_6574:posix_2escm"),(void*)f_6574},
{C_text("f_6595:posix_2escm"),(void*)f_6595},
{C_text("f_6601:posix_2escm"),(void*)f_6601},
{C_text("f_6605:posix_2escm"),(void*)f_6605},
{C_text("f_6626:posix_2escm"),(void*)f_6626},
{C_text("f_6630:posix_2escm"),(void*)f_6630},
{C_text("f_6653:posix_2escm"),(void*)f_6653},
{C_text("f_6682:posix_2escm"),(void*)f_6682},
{C_text("f_6696:posix_2escm"),(void*)f_6696},
{C_text("f_6706:posix_2escm"),(void*)f_6706},
{C_text("f_6720:posix_2escm"),(void*)f_6720},
{C_text("f_6742:posix_2escm"),(void*)f_6742},
{C_text("f_6759:posix_2escm"),(void*)f_6759},
{C_text("f_6763:posix_2escm"),(void*)f_6763},
{C_text("f_6771:posix_2escm"),(void*)f_6771},
{C_text("f_6779:posix_2escm"),(void*)f_6779},
{C_text("f_6783:posix_2escm"),(void*)f_6783},
{C_text("f_6787:posix_2escm"),(void*)f_6787},
{C_text("f_6798:posix_2escm"),(void*)f_6798},
{C_text("f_6805:posix_2escm"),(void*)f_6805},
{C_text("f_6814:posix_2escm"),(void*)f_6814},
{C_text("f_6845:posix_2escm"),(void*)f_6845},
{C_text("f_6859:posix_2escm"),(void*)f_6859},
{C_text("f_6865:posix_2escm"),(void*)f_6865},
{C_text("f_6869:posix_2escm"),(void*)f_6869},
{C_text("f_6873:posix_2escm"),(void*)f_6873},
{C_text("f_6913:posix_2escm"),(void*)f_6913},
{C_text("f_6937:posix_2escm"),(void*)f_6937},
{C_text("f_6940:posix_2escm"),(void*)f_6940},
{C_text("f_6991:posix_2escm"),(void*)f_6991},
{C_text("f_7007:posix_2escm"),(void*)f_7007},
{C_text("f_7029:posix_2escm"),(void*)f_7029},
{C_text("f_7032:posix_2escm"),(void*)f_7032},
{C_text("f_7039:posix_2escm"),(void*)f_7039},
{C_text("f_7042:posix_2escm"),(void*)f_7042},
{C_text("f_7072:posix_2escm"),(void*)f_7072},
{C_text("f_7079:posix_2escm"),(void*)f_7079},
{C_text("f_7122:posix_2escm"),(void*)f_7122},
{C_text("f_7126:posix_2escm"),(void*)f_7126},
{C_text("f_7128:posix_2escm"),(void*)f_7128},
{C_text("f_7143:posix_2escm"),(void*)f_7143},
{C_text("f_7149:posix_2escm"),(void*)f_7149},
{C_text("f_7163:posix_2escm"),(void*)f_7163},
{C_text("f_7172:posix_2escm"),(void*)f_7172},
{C_text("f_7178:posix_2escm"),(void*)f_7178},
{C_text("f_7183:posix_2escm"),(void*)f_7183},
{C_text("f_7194:posix_2escm"),(void*)f_7194},
{C_text("f_7195:posix_2escm"),(void*)f_7195},
{C_text("f_7206:posix_2escm"),(void*)f_7206},
{C_text("f_7224:posix_2escm"),(void*)f_7224},
{C_text("f_7228:posix_2escm"),(void*)f_7228},
{C_text("f_7231:posix_2escm"),(void*)f_7231},
{C_text("f_7234:posix_2escm"),(void*)f_7234},
{C_text("f_7241:posix_2escm"),(void*)f_7241},
{C_text("f_7245:posix_2escm"),(void*)f_7245},
{C_text("f_7247:posix_2escm"),(void*)f_7247},
{C_text("f_7251:posix_2escm"),(void*)f_7251},
{C_text("f_7254:posix_2escm"),(void*)f_7254},
{C_text("f_7257:posix_2escm"),(void*)f_7257},
{C_text("f_7269:posix_2escm"),(void*)f_7269},
{C_text("f_7273:posix_2escm"),(void*)f_7273},
{C_text("f_7280:posix_2escm"),(void*)f_7280},
{C_text("f_7284:posix_2escm"),(void*)f_7284},
{C_text("f_7291:posix_2escm"),(void*)f_7291},
{C_text("f_7297:posix_2escm"),(void*)f_7297},
{C_text("f_7303:posix_2escm"),(void*)f_7303},
{C_text("f_7314:posix_2escm"),(void*)f_7314},
{C_text("f_7318:posix_2escm"),(void*)f_7318},
{C_text("f_7322:posix_2escm"),(void*)f_7322},
{C_text("f_7326:posix_2escm"),(void*)f_7326},
{C_text("f_7330:posix_2escm"),(void*)f_7330},
{C_text("f_7334:posix_2escm"),(void*)f_7334},
{C_text("f_7347:posix_2escm"),(void*)f_7347},
{C_text("f_7349:posix_2escm"),(void*)f_7349},
{C_text("f_7354:posix_2escm"),(void*)f_7354},
{C_text("f_7363:posix_2escm"),(void*)f_7363},
{C_text("f_7390:posix_2escm"),(void*)f_7390},
{C_text("f_7393:posix_2escm"),(void*)f_7393},
{C_text("f_7398:posix_2escm"),(void*)f_7398},
{C_text("f_7418:posix_2escm"),(void*)f_7418},
{C_text("f_7442:posix_2escm"),(void*)f_7442},
{C_text("f_7484:posix_2escm"),(void*)f_7484},
{C_text("f_7549:posix_2escm"),(void*)f_7549},
{C_text("f_7554:posix_2escm"),(void*)f_7554},
{C_text("f_7573:posix_2escm"),(void*)f_7573},
{C_text("f_7579:posix_2escm"),(void*)f_7579},
{C_text("f_7628:posix_2escm"),(void*)f_7628},
{C_text("f_7632:posix_2escm"),(void*)f_7632},
{C_text("f_7635:posix_2escm"),(void*)f_7635},
{C_text("f_7641:posix_2escm"),(void*)f_7641},
{C_text("f_7646:posix_2escm"),(void*)f_7646},
{C_text("f_7650:posix_2escm"),(void*)f_7650},
{C_text("f_7653:posix_2escm"),(void*)f_7653},
{C_text("f_7662:posix_2escm"),(void*)f_7662},
{C_text("f_7668:posix_2escm"),(void*)f_7668},
{C_text("f_7671:posix_2escm"),(void*)f_7671},
{C_text("f_7681:posix_2escm"),(void*)f_7681},
{C_text("f_7687:posix_2escm"),(void*)f_7687},
{C_text("f_7690:posix_2escm"),(void*)f_7690},
{C_text("f_7700:posix_2escm"),(void*)f_7700},
{C_text("f_7706:posix_2escm"),(void*)f_7706},
{C_text("f_7709:posix_2escm"),(void*)f_7709},
{C_text("f_7719:posix_2escm"),(void*)f_7719},
{C_text("f_7725:posix_2escm"),(void*)f_7725},
{C_text("f_7728:posix_2escm"),(void*)f_7728},
{C_text("f_7738:posix_2escm"),(void*)f_7738},
{C_text("f_7744:posix_2escm"),(void*)f_7744},
{C_text("f_7750:posix_2escm"),(void*)f_7750},
{C_text("f_7774:posix_2escm"),(void*)f_7774},
{C_text("f_7778:posix_2escm"),(void*)f_7778},
{C_text("f_7783:posix_2escm"),(void*)f_7783},
{C_text("f_7790:posix_2escm"),(void*)f_7790},
{C_text("f_7808:posix_2escm"),(void*)f_7808},
{C_text("f_7812:posix_2escm"),(void*)f_7812},
{C_text("f_7814:posix_2escm"),(void*)f_7814},
{C_text("f_7818:posix_2escm"),(void*)f_7818},
{C_text("f_7821:posix_2escm"),(void*)f_7821},
{C_text("f_7830:posix_2escm"),(void*)f_7830},
{C_text("f_7851:posix_2escm"),(void*)f_7851},
{C_text("f_7855:posix_2escm"),(void*)f_7855},
{C_text("f_7857:posix_2escm"),(void*)f_7857},
{C_text("f_7861:posix_2escm"),(void*)f_7861},
{C_text("f_7863:posix_2escm"),(void*)f_7863},
{C_text("f_7867:posix_2escm"),(void*)f_7867},
{C_text("toplevel:posix_2escm"),(void*)C_posix_toplevel},
{NULL,NULL}};
#endif

static C_PTABLE_ENTRY *create_ptable(void){
#ifdef C_ENABLE_PTABLES
return ptable;
#else
return NULL;
#endif
}

/*
o|hiding unexported module binding: chicken.posix#d 
o|hiding unexported module binding: chicken.posix#define-alias 
o|hiding unexported module binding: chicken.posix#define-unimplemented 
o|hiding unexported module binding: chicken.posix#set!-unimplemented 
o|hiding unexported module binding: chicken.posix#posix-error 
o|hiding unexported module binding: chicken.posix#stat-mode 
o|hiding unexported module binding: chicken.posix#stat 
o|hiding unexported module binding: chicken.posix#decode-seconds 
o|hiding unexported module binding: chicken.posix#check-time-vector 
o|hiding unexported module binding: chicken.posix#list->c-string-buffer 
o|hiding unexported module binding: chicken.posix#free-c-string-buffer 
o|hiding unexported module binding: chicken.posix#check-environment-list 
o|hiding unexported module binding: chicken.posix#call-with-exec-args 
o|hiding unexported module binding: chicken.posix#chown 
o|hiding unexported module binding: chicken.posix#process-wait-impl 
o|hiding unexported module binding: chicken.posix#shell-command 
o|hiding unexported module binding: chicken.posix#shell-command-arguments 
o|hiding unexported module binding: chicken.posix#process-impl 
S|applied compiler syntax:
S|  scheme#for-each		3
S|  scheme#map		2
o|eliminated procedure checks: 135 
o|specializations:
o|  1 (scheme#make-string fixnum)
o|  2 (chicken.bitwise#bitwise-ior fixnum fixnum)
o|  1 (##sys#check-output-port * * *)
o|  1 (##sys#check-input-port * * *)
o|  1 (##sys#call-with-values (procedure () *) *)
o|  11 (scheme#cdr pair)
o|  1 (chicken.base#add1 fixnum)
o|  3 (##sys#length list)
o|  1 (scheme#zero? *)
o|  2 (##sys#check-open-port * *)
o|  5 (scheme#eqv? * (or eof null fixnum char boolean symbol keyword))
o|  12 (scheme#car pair)
o|  4 (##sys#check-list (or pair list) *)
o|  1 (scheme#char=? char char)
o|  2 (scheme#zero? integer)
(o e)|safe calls: 751 
(o e)|assignments to immediate values: 123 
o|dropping redundant toplevel assignment: chicken.file.posix#file-stat 
o|dropping redundant toplevel assignment: chicken.file.posix#set-file-permissions! 
o|dropping redundant toplevel assignment: chicken.file.posix#file-modification-time 
o|dropping redundant toplevel assignment: chicken.file.posix#file-access-time 
o|dropping redundant toplevel assignment: chicken.file.posix#file-change-time 
o|dropping redundant toplevel assignment: chicken.file.posix#set-file-times! 
o|dropping redundant toplevel assignment: chicken.file.posix#file-size 
o|dropping redundant toplevel assignment: chicken.file.posix#set-file-owner! 
o|dropping redundant toplevel assignment: chicken.file.posix#set-file-group! 
o|safe globals: (chicken.file.posix#set-file-group! chicken.file.posix#set-file-owner! chicken.file.posix#file-size chicken.file.posix#set-file-times! chicken.file.posix#file-change-time chicken.file.posix#file-access-time chicken.file.posix#file-modification-time chicken.file.posix#set-file-permissions! chicken.file.posix#file-stat chicken.posix#stat ##sys#posix-error chicken.posix#posix-error chicken.process-context.posix#user-information chicken.process-context.posix#process-group-id chicken.process-context.posix#create-session chicken.process-context.posix#current-effective-user-name chicken.process-context.posix#current-user-name chicken.process-context.posix#parent-process-id chicken.process-context.posix#current-process-id chicken.process-context.posix#current-user-id chicken.process-context.posix#current-group-id chicken.process-context.posix#current-effective-user-id chicken.process-context.posix#current-effective-group-id chicken.process-context.posix#set-root-directory! chicken.process-context.posix#change-directory* chicken.process.signal#signals-list chicken.process.signal#signal/xfsz chicken.process.signal#signal/xcpu chicken.process.signal#signal/winch chicken.process.signal#signal/vtalrm chicken.process.signal#signal/usr2 chicken.process.signal#signal/usr1 chicken.process.signal#signal/urg chicken.process.signal#signal/tstp chicken.process.signal#signal/trap chicken.process.signal#signal/term chicken.process.signal#signal/stop chicken.process.signal#signal/segv chicken.process.signal#signal/quit chicken.process.signal#signal/prof chicken.process.signal#signal/pipe chicken.process.signal#signal/kill chicken.process.signal#signal/io chicken.process.signal#signal/int chicken.process.signal#signal/ill chicken.process.signal#signal/hup chicken.process.signal#signal/fpe chicken.process.signal#signal/cont chicken.process.signal#signal/chld chicken.process.signal#signal/bus chicken.process.signal#signal/break chicken.process.signal#signal/alrm chicken.process.signal#signal/abrt chicken.process.signal#signal-unmask! chicken.process.signal#signal-masked? chicken.process.signal#signal-mask! chicken.process.signal#signal-mask chicken.process.signal#signal-handler chicken.process.signal#set-signal-mask! chicken.process.signal#set-signal-handler! chicken.process.signal#set-alarm! chicken.process#spawn/detach chicken.process#spawn/nowaito chicken.process#spawn/nowait chicken.process#spawn/wait chicken.process#spawn/overlay chicken.process#pipe/buf chicken.process#process-sleep chicken.process#process* chicken.process#process chicken.process#with-output-to-pipe chicken.process#with-input-from-pipe chicken.process#open-output-pipe chicken.process#open-input-pipe chicken.process#create-pipe chicken.process#close-output-pipe chicken.process#close-input-pipe chicken.process#call-with-output-pipe chicken.process#call-with-input-pipe chicken.process#process-wait chicken.process#process-spawn chicken.process#process-signal chicken.process#process-run chicken.process#process-fork chicken.process#process-execute chicken.process#qs chicken.process#system* chicken.process#system chicken.time.posix#local-timezone-abbreviation chicken.time.posix#time->string chicken.time.posix#string->time chicken.time.posix#local-time->seconds chicken.time.posix#seconds->string chicken.time.posix#seconds->local-time chicken.time.posix#utc-time->seconds chicken.time.posix#seconds->utc-time chicken.file.posix#set-file-position! chicken.file.posix#seek/set chicken.file.posix#seek/end chicken.file.posix#seek/cur chicken.file.posix#port->fileno chicken.file.posix#perm/ixusr chicken.file.posix#perm/ixoth chicken.file.posix#perm/ixgrp chicken.file.posix#perm/iwusr chicken.file.posix#perm/iwoth chicken.file.posix#perm/iwgrp chicken.file.posix#perm/isvtx chicken.file.posix#perm/isuid chicken.file.posix#perm/isgid chicken.file.posix#perm/irwxu chicken.file.posix#perm/irwxo chicken.file.posix#perm/irwxg chicken.file.posix#perm/irusr chicken.file.posix#perm/iroth chicken.file.posix#perm/irgrp chicken.file.posix#open/wronly chicken.file.posix#open/write chicken.file.posix#open/trunc chicken.file.posix#open/text chicken.file.posix#open/sync chicken.file.posix#open/read chicken.file.posix#open/rdwr chicken.file.posix#open/rdonly chicken.file.posix#open/nonblock chicken.file.posix#open/noinherit chicken.file.posix#open/noctty chicken.file.posix#open/fsync chicken.file.posix#open/excl chicken.file.posix#open/creat chicken.file.posix#open/binary chicken.file.posix#open/append chicken.file.posix#open-output-file* chicken.file.posix#open-input-file* chicken.file.posix#fileno/stdout chicken.file.posix#fileno/stdin chicken.file.posix#fileno/stderr chicken.file.posix#symbolic-link? chicken.file.posix#socket? chicken.file.posix#regular-file? chicken.file.posix#fifo? chicken.file.posix#directory? chicken.file.posix#character-device? chicken.file.posix#block-device? chicken.file.posix#file-type chicken.file.posix#file-write chicken.file.posix#file-unlock chicken.file.posix#file-truncate chicken.file.posix#file-test-lock chicken.file.posix#file-select chicken.file.posix#file-read chicken.file.posix#file-position chicken.file.posix#file-permissions chicken.file.posix#file-owner chicken.file.posix#file-open chicken.file.posix#file-mkstemp chicken.file.posix#file-lock/blocking chicken.file.posix#file-lock chicken.file.posix#file-link chicken.file.posix#file-group chicken.file.posix#file-creation-mode chicken.file.posix#file-control chicken.file.posix#file-close chicken.file.posix#fcntl/setfl chicken.file.posix#fcntl/setfd chicken.file.posix#fcntl/getfl chicken.file.posix#fcntl/getfd chicken.file.posix#fcntl/dupfd chicken.file.posix#duplicate-fileno chicken.file.posix#read-symbolic-link chicken.file.posix#create-symbolic-link chicken.file.posix#create-fifo c2201) 
o|inlining procedure: k2723 
o|inlining procedure: k2723 
o|inlining procedure: k2741 
o|inlining procedure: k2741 
o|inlining procedure: k2768 
o|inlining procedure: k2768 
o|substituted constant variable: a2778 
o|inlining procedure: k2809 
o|inlining procedure: k2809 
o|contracted procedure: "(posix-common.scm:192) strerror630" 
o|inlining procedure: k2962 
o|inlining procedure: k2962 
o|inlining procedure: k2977 
o|inlining procedure: k2977 
o|inlining procedure: k2996 
o|inlining procedure: k2996 
o|inlining procedure: k3029 
o|inlining procedure: k3029 
o|inlining procedure: k3041 
o|inlining procedure: k3041 
o|inlining procedure: k3119 
o|inlining procedure: k3119 
o|contracted procedure: "(posix-common.scm:292) g702703" 
o|inlining procedure: k3106 
o|inlining procedure: k3106 
o|inlining procedure: k3206 
o|inlining procedure: k3218 
o|inlining procedure: k3218 
o|inlining procedure: k3230 
o|inlining procedure: k3230 
o|inlining procedure: k3242 
o|inlining procedure: k3242 
o|inlining procedure: k3206 
o|inlining procedure: k3363 
o|inlining procedure: k3363 
o|inlining procedure: k3378 
o|inlining procedure: k3378 
o|inlining procedure: k3391 
o|inlining procedure: k3391 
o|inlining procedure: k3441 
o|inlining procedure: k3454 
o|inlining procedure: k3454 
o|substituted constant variable: a3467 
o|inlining procedure: k3441 
o|inlining procedure: k3474 
o|inlining procedure: k3474 
o|inlining procedure: k3487 
o|inlining procedure: k3487 
o|substituted constant variable: a3522 
o|inlining procedure: k3523 
o|inlining procedure: k3523 
o|inlining procedure: k3542 
o|inlining procedure: k3542 
o|inlining procedure: k3572 
o|inlining procedure: k3572 
o|inlining procedure: k3599 
o|inlining procedure: k3599 
o|inlining procedure: k3612 
o|propagated global variable: r36137932 chicken.process-context.posix#change-directory* 
o|inlining procedure: k3612 
o|inlining procedure: k3632 
o|inlining procedure: k3632 
o|inlining procedure: k3710 
o|inlining procedure: k3710 
o|contracted procedure: "(posix-common.scm:595) ctime880" 
o|inlining procedure: k3746 
o|inlining procedure: k3746 
o|inlining procedure: k3790 
o|contracted procedure: "(posix-common.scm:618) strftime905" 
o|inlining procedure: k3776 
o|inlining procedure: k3776 
o|inlining procedure: k3790 
o|contracted procedure: "(posix-common.scm:620) asctime904" 
o|inlining procedure: k3763 
o|inlining procedure: k3763 
o|inlining procedure: k3899 
o|inlining procedure: k3899 
o|inlining procedure: k3985 
o|inlining procedure: k3985 
o|contracted procedure: "(posix-common.scm:688) c-string->allocated-pointer974" 
o|merged explicitly consed rest parameter: args9861007 
o|consed rest parameter at call site: tmp22526 1 
o|inlining procedure: k4066 
o|inlining procedure: k4066 
o|inlining procedure: k4115 
o|inlining procedure: k4115 
o|inlining procedure: k4173 
o|inlining procedure: k4173 
o|inlining procedure: k4232 
o|contracted procedure: "(posix-common.scm:731) g10741083" 
o|inlining procedure: k4232 
o|inlining procedure: k4279 
o|inlining procedure: k4279 
o|inlining procedure: k4297 
o|inlining procedure: k4297 
o|inlining procedure: k4310 
o|inlining procedure: k4310 
o|inlining procedure: k4326 
o|inlining procedure: k4326 
o|inlining procedure: "(posix-common.scm:760) badmode1097" 
o|substituted constant variable: a4350 
o|substituted constant variable: a4352 
o|inlining procedure: k4366 
o|inlining procedure: k4366 
o|inlining procedure: "(posix-common.scm:771) badmode1097" 
o|substituted constant variable: a4390 
o|substituted constant variable: a4392 
o|substituted constant variable: a4397 
o|substituted constant variable: a4398 
o|inlining procedure: k4399 
o|inlining procedure: k4399 
o|substituted constant variable: a4412 
o|substituted constant variable: a4413 
o|inlining procedure: k4414 
o|inlining procedure: k4414 
o|inlining procedure: k4601 
o|inlining procedure: k4601 
o|contracted procedure: "(posixunix.scm:341) fcntl1276" 
o|inlining procedure: k4639 
o|inlining procedure: k4639 
o|inlining procedure: k4664 
o|inlining procedure: k4664 
o|inlining procedure: k4698 
o|inlining procedure: k4698 
o|inlining procedure: k4738 
o|inlining procedure: k4738 
o|inlining procedure: k4841 
o|inlining procedure: k4841 
o|inlining procedure: k4864 
o|inlining procedure: k4864 
o|inlining procedure: k4887 
o|inlining procedure: k4896 
o|inlining procedure: k4896 
o|inlining procedure: k4887 
o|inlining procedure: k4902 
o|inlining procedure: k4902 
o|inlining procedure: k4917 
o|inlining procedure: k4917 
o|contracted procedure: "(posixunix.scm:449) g14481449" 
o|inlining procedure: k4936 
o|inlining procedure: k4936 
o|inlining procedure: k4976 
o|inlining procedure: k4976 
o|contracted procedure: "(posixunix.scm:441) g14291430" 
o|inlining procedure: k4995 
o|inlining procedure: k4995 
o|contracted procedure: "(posixunix.scm:434) g14031404" 
o|inlining procedure: k5046 
o|inlining procedure: k5046 
o|contracted procedure: "(posixunix.scm:428) g13881389" 
o|inlining procedure: k5063 
o|inlining procedure: k5063 
o|inlining procedure: k5088 
o|inlining procedure: k5088 
o|contracted procedure: "(posixunix.scm:422) g13711372" 
o|inlining procedure: k5105 
o|inlining procedure: k5105 
o|inlining procedure: k5138 
o|inlining procedure: k5138 
o|inlining procedure: k5153 
o|inlining procedure: k5153 
o|inlining procedure: k5175 
o|inlining procedure: k5175 
o|inlining procedure: k5239 
o|inlining procedure: k5239 
o|inlining procedure: k5252 
o|contracted procedure: "(posixunix.scm:564) g14811488" 
o|inlining procedure: k5252 
o|inlining procedure: k5288 
o|inlining procedure: k5288 
o|inlining procedure: k5304 
o|inlining procedure: k5304 
o|inlining procedure: k5339 
o|inlining procedure: k5339 
o|inlining procedure: k5424 
o|inlining procedure: k5424 
o|inlining procedure: k5440 
o|inlining procedure: k5440 
o|inlining procedure: k5462 
o|inlining procedure: k5462 
o|inlining procedure: k5487 
o|inlining procedure: k5487 
o|inlining procedure: k5515 
o|inlining procedure: k5515 
o|inlining procedure: k5536 
o|inlining procedure: k5561 
o|inlining procedure: k5561 
o|inlining procedure: k5582 
o|inlining procedure: k5582 
o|inlining procedure: k5612 
o|inlining procedure: k5612 
o|inlining procedure: k5536 
o|inlining procedure: k5651 
o|inlining procedure: k5651 
o|contracted procedure: "(posixunix.scm:778) link1611" 
o|inlining procedure: k5705 
o|inlining procedure: k5705 
o|inlining procedure: k5731 
o|inlining procedure: k5731 
o|inlining procedure: k5745 
o|inlining procedure: k5769 
o|inlining procedure: k5769 
o|inlining procedure: k5745 
o|inlining procedure: k5790 
o|inlining procedure: k5790 
o|inlining procedure: k5810 
o|inlining procedure: k5810 
o|inlining procedure: k5847 
o|inlining procedure: k5847 
o|inlining procedure: k5869 
o|inlining procedure: k5869 
o|inlining procedure: k5878 
o|inlining procedure: k5878 
o|inlining procedure: k5894 
o|inlining procedure: k5894 
o|inlining procedure: k5919 
o|inlining procedure: k5919 
o|inlining procedure: k5966 
o|inlining procedure: k5966 
o|inlining procedure: k5988 
o|inlining procedure: k5988 
o|inlining procedure: k6016 
o|inlining procedure: k6016 
o|inlining procedure: k6059 
o|inlining procedure: k6059 
o|inlining procedure: k6104 
o|inlining procedure: k6104 
o|inlining procedure: k6209 
o|inlining procedure: k6230 
o|inlining procedure: k6230 
o|inlining procedure: k6209 
o|inlining procedure: k6286 
o|inlining procedure: k6286 
o|inlining procedure: k6310 
o|inlining procedure: k6310 
o|inlining procedure: k6325 
o|inlining procedure: k6342 
o|inlining procedure: k6342 
o|inlining procedure: k6325 
o|inlining procedure: k6438 
o|inlining procedure: k6438 
o|inlining procedure: k6448 
o|inlining procedure: k6448 
o|inlining procedure: k6468 
o|inlining procedure: k6468 
o|inlining procedure: k6575 
o|inlining procedure: k6575 
o|inlining procedure: k6606 
o|inlining procedure: k6606 
o|inlining procedure: k6631 
o|contracted procedure: "(posixunix.scm:1019) g18681869" 
o|inlining procedure: k6636 
o|inlining procedure: k6636 
o|inlining procedure: k6631 
o|inlining procedure: k6666 
o|inlining procedure: k6666 
o|inlining procedure: k6707 
o|inlining procedure: k6707 
o|contracted procedure: "(posixunix.scm:1053) strptime1888" 
o|inlining procedure: k6736 
o|inlining procedure: k6736 
o|inlining procedure: k6788 
o|inlining procedure: k6788 
o|inlining procedure: k6846 
o|inlining procedure: k6846 
o|contracted procedure: "(posixunix.scm:1089) fork1928" 
o|contracted procedure: "(posixunix.scm:1086) g19461947" 
o|inlining procedure: k6831 
o|inlining procedure: k6831 
o|inlining procedure: k6945 
o|inlining procedure: k6945 
o|inlining procedure: k6997 
o|inlining procedure: k6997 
o|inlining procedure: k7016 
o|inlining procedure: k7016 
o|inlining procedure: k7043 
o|inlining procedure: k7043 
o|removed unused formal parameters: (loc2004) 
o|inlining procedure: k7062 
o|inlining procedure: k7062 
o|inlining procedure: k7080 
o|inlining procedure: k7080 
o|inlining procedure: "(posixunix.scm:1156) chicken.posix#shell-command-arguments" 
o|removed unused parameter to known procedure: loc2004 "(posixunix.scm:1155) chicken.posix#shell-command" 
o|inlining procedure: k7133 
o|inlining procedure: k7151 
o|inlining procedure: k7151 
o|inlining procedure: k7133 
o|removed unused formal parameters: (loc2039) 
o|inlining procedure: k7165 
o|inlining procedure: k7165 
o|removed unused formal parameters: (loc2044 fd2047) 
o|inlining procedure: k7185 
o|inlining procedure: k7185 
o|inlining procedure: k7197 
o|contracted procedure: "(posixunix.scm:1208) replace-fd2019" 
o|inlining procedure: k7114 
o|inlining procedure: k7114 
o|inlining procedure: k7197 
o|inlining procedure: k7212 
o|inlining procedure: k7212 
o|removed unused parameter to known procedure: loc2039 "(posixunix.scm:1218) needed-pipe2025" 
o|removed unused parameter to known procedure: loc2039 "(posixunix.scm:1217) needed-pipe2025" 
o|removed unused parameter to known procedure: loc2039 "(posixunix.scm:1216) needed-pipe2025" 
o|removed unused formal parameters: (pid2079) 
o|inlining procedure: k7274 
o|inlining procedure: k7274 
o|removed unused parameter to known procedure: loc2044 "(posixunix.scm:1229) connect-parent2026" 
o|removed unused parameter to known procedure: fd2047 "(posixunix.scm:1229) connect-parent2026" 
o|removed unused formal parameters: (pid2088) 
o|inlining procedure: k7285 
o|inlining procedure: k7285 
o|removed unused parameter to known procedure: loc2044 "(posixunix.scm:1233) connect-parent2026" 
o|removed unused parameter to known procedure: fd2047 "(posixunix.scm:1233) connect-parent2026" 
o|removed unused parameter to known procedure: pid2079 "(posixunix.scm:1250) input-port2060" 
o|removed unused parameter to known procedure: pid2088 "(posixunix.scm:1246) output-port2061" 
o|removed unused parameter to known procedure: pid2079 "(posixunix.scm:1243) input-port2060" 
o|inlining procedure: k7365 
o|inlining procedure: k7365 
o|removed unused parameter to known procedure: loc2004 "(posixunix.scm:1268) chicken.posix#shell-command" 
o|inlining procedure: "(posixunix.scm:1267) chicken.posix#shell-command-arguments" 
o|inlining procedure: k7559 
o|inlining procedure: k7559 
o|contracted procedure: "(posixunix.scm:1291) chroot2188" 
o|substituted constant variable: c2201 
o|inlining procedure: k7633 
o|inlining procedure: k7633 
o|inlining procedure: k7654 
o|inlining procedure: k7654 
o|inlining procedure: k7673 
o|inlining procedure: k7673 
o|inlining procedure: k7692 
o|inlining procedure: k7692 
o|inlining procedure: k7711 
o|inlining procedure: k7711 
o|inlining procedure: k7730 
o|inlining procedure: k7730 
o|inlining procedure: k7752 
o|inlining procedure: k7752 
o|inlining procedure: k7767 
o|inlining procedure: k7767 
o|inlining procedure: k7795 
o|inlining procedure: k7795 
o|inlining procedure: k7819 
o|inlining procedure: k7819 
o|inlining procedure: k7831 
o|inlining procedure: k7831 
o|inlining procedure: k7841 
o|inlining procedure: k7841 
o|replaced variables: 1207 
o|removed binding forms: 662 
o|removed side-effect free assignment to unused variable: c2201 
o|substituted constant variable: r29637877 
o|substituted constant variable: r31077890 
o|substituted constant variable: r31077890 
o|substituted constant variable: r32197893 
o|substituted constant variable: r32317895 
o|substituted constant variable: r32437897 
o|substituted constant variable: r32077899 
o|substituted constant variable: r33797903 
o|inlining procedure: k3441 
o|substituted constant variable: r34557908 
o|inlining procedure: k3441 
o|inlining procedure: k3441 
o|substituted constant variable: r34887914 
o|substituted constant variable: r34887914 
o|substituted constant variable: r34887916 
o|substituted constant variable: r34887916 
o|propagated global variable: g8338347933 chicken.process-context.posix#change-directory* 
o|substituted constant variable: r37777952 
o|substituted constant variable: r37777952 
o|substituted constant variable: r37647959 
o|substituted constant variable: r37647959 
o|substituted constant variable: r42807983 
o|removed side-effect free assignment to unused variable: badmode1097 
o|substituted constant variable: r43117986 
o|substituted constant variable: r43117986 
o|substituted constant variable: r43117988 
o|substituted constant variable: r43117988 
o|inlining procedure: k4326 
o|inlining procedure: k4366 
o|substituted constant variable: r48658040 
o|substituted constant variable: r48658040 
o|substituted constant variable: r48658042 
o|substituted constant variable: r48658042 
o|inlining procedure: k4887 
o|inlining procedure: k4887 
o|substituted constant variable: r48978047 
o|inlining procedure: k4887 
o|substituted constant variable: r48888048 
o|substituted constant variable: r48888048 
o|substituted constant variable: r49378056 
o|substituted constant variable: r49378056 
o|substituted constant variable: r49968062 
o|substituted constant variable: r49968062 
o|substituted constant variable: r50648068 
o|substituted constant variable: r50648068 
o|substituted constant variable: r51068074 
o|substituted constant variable: r51068074 
o|substituted constant variable: r53408099 
o|inlining procedure: k5582 
o|substituted constant variable: r56138121 
o|substituted constant variable: r56138121 
o|substituted constant variable: r57328128 
o|inlining procedure: k5802 
o|inlining procedure: k5802 
o|substituted constant variable: r59898168 
o|substituted constant variable: r61058180 
o|inlining procedure: k6448 
o|inlining procedure: k6448 
o|substituted constant variable: r66378216 
o|substituted constant variable: r67378224 
o|substituted constant variable: r67378224 
o|substituted constant variable: c-pointer19481952 
o|substituted constant variable: c-pointer19481952 
o|substituted constant variable: r68328232 
o|substituted constant variable: r68328232 
o|inlining procedure: k7016 
o|substituted constant variable: r70638245 
o|removed side-effect free assignment to unused variable: chicken.posix#shell-command-arguments 
o|substituted constant variable: r71668258 
o|substituted constant variable: r71868260 
o|substituted constant variable: r72138266 
o|removed unused formal parameters: (stdfd2083) 
o|substituted constant variable: r72758268 
o|removed unused formal parameters: (stdfd2092) 
o|substituted constant variable: r72868270 
o|removed unused parameter to known procedure: stdfd2083 "(posixunix.scm:1250) input-port2060" 
o|removed unused parameter to known procedure: stdfd2092 "(posixunix.scm:1246) output-port2061" 
o|removed unused parameter to known procedure: stdfd2083 "(posixunix.scm:1243) input-port2060" 
o|substituted constant variable: r78328309 
o|replaced variables: 151 
o|removed binding forms: 982 
o|removed conditional forms: 1 
o|inlining procedure: k3850 
o|inlining procedure: k3850 
o|inlining procedure: k4075 
o|inlining procedure: k4710 
o|inlining procedure: k4710 
o|inlining procedure: k4754 
o|inlining procedure: k4754 
o|substituted constant variable: r48888346 
o|inlining procedure: k5258 
o|inlining procedure: k5661 
o|contracted procedure: k7016 
o|removed unused formal parameters: (loc2052) 
o|removed unused parameter to known procedure: loc2052 "(posixunix.scm:1225) connect-child2027" 
o|removed unused parameter to known procedure: loc2052 "(posixunix.scm:1224) connect-child2027" 
o|removed unused parameter to known procedure: loc2052 "(posixunix.scm:1223) connect-child2027" 
o|inlining procedure: k7501 
o|inlining procedure: k7569 
o|replaced variables: 6 
o|removed binding forms: 213 
o|substituted constant variable: r34428314 
o|substituted constant variable: r34428316 
o|substituted constant variable: r34428318 
o|substituted constant variable: r38518418 
o|contracted procedure: k4000 
o|contracted procedure: k4598 
o|substituted constant variable: r47118435 
o|substituted constant variable: r47558437 
o|contracted procedure: k4838 
o|contracted procedure: k6837 
o|contracted procedure: k6840 
o|removed unused formal parameters: (loc2065) 
o|removed unused parameter to known procedure: loc2065 "(posixunix.scm:1237) spawn2059" 
o|inlining procedure: k7792 
o|inlining procedure: k7792 
o|simplifications: ((let . 2)) 
o|replaced variables: 1 
o|removed binding forms: 20 
o|removed conditional forms: 2 
o|substituted constant variable: r77938516 
o|replaced variables: 1 
o|removed binding forms: 8 
o|removed binding forms: 2 
o|simplifications: ((let . 50) (if . 95) (##core#call . 546)) 
o|  call simplifications:
o|    scheme#<
o|    scheme#vector-ref	2
o|    scheme#make-vector
o|    chicken.fixnum#fxior	2
o|    ##sys#check-structure
o|    ##sys#make-structure
o|    chicken.fixnum#fxmin	2
o|    scheme#eof-object?
o|    chicken.fixnum#fx>=	7
o|    chicken.fixnum#fx*
o|    scheme#*
o|    scheme#memq	2
o|    scheme#list	7
o|    scheme#call-with-values	4
o|    ##sys#check-pair
o|    ##sys#check-list	7
o|    ##sys#apply	2
o|    chicken.fixnum#fx+	21
o|    ##sys#call-with-values	8
o|    scheme#values	19
o|    scheme#vector-set!	3
o|    ##sys#foreign-block-argument	8
o|    scheme#=	3
o|    ##sys#foreign-ranged-integer-argument	2
o|    chicken.fixnum#fx-	12
o|    ##sys#check-vector
o|    ##sys#size	11
o|    ##sys#null-pointer?	2
o|    scheme#not	9
o|    ##sys#setislot	4
o|    chicken.fixnum#fx=	42
o|    scheme#cdr	29
o|    ##sys#foreign-string-argument	4
o|    scheme#vector	2
o|    chicken.base#fixnum?	16
o|    scheme#string?	4
o|    ##sys#foreign-fixnum-argument	14
o|    scheme#null?	72
o|    scheme#car	51
o|    scheme#string->list
o|    scheme#pair?	17
o|    scheme#cons	10
o|    ##sys#setslot	4
o|    ##sys#slot	27
o|    scheme#apply	11
o|    scheme#char=?
o|    scheme#string	3
o|    scheme#eq?	29
o|    ##sys#check-string	21
o|    chicken.fixnum#fx<	42
o|contracted procedure: k2720 
o|contracted procedure: k2726 
o|contracted procedure: k2744 
o|contracted procedure: k2843 
o|contracted procedure: k2757 
o|contracted procedure: k2840 
o|contracted procedure: k2760 
o|contracted procedure: k2763 
o|contracted procedure: k2771 
o|contracted procedure: k2795 
o|contracted procedure: k2803 
o|contracted procedure: k2812 
o|contracted procedure: k2815 
o|contracted procedure: k2818 
o|contracted procedure: k2826 
o|contracted procedure: k2834 
o|contracted procedure: k2846 
o|contracted procedure: k2936 
o|contracted procedure: k2965 
o|contracted procedure: k2974 
o|contracted procedure: k2990 
o|contracted procedure: k3014 
o|contracted procedure: k3005 
o|contracted procedure: k3032 
o|contracted procedure: k3038 
o|contracted procedure: k3054 
o|contracted procedure: k3146 
o|contracted procedure: k3088 
o|contracted procedure: k3140 
o|contracted procedure: k3091 
o|contracted procedure: k3134 
o|contracted procedure: k3094 
o|contracted procedure: k3122 
o|contracted procedure: k3113 
o|contracted procedure: k3152 
o|contracted procedure: k3272 
o|contracted procedure: k3194 
o|contracted procedure: k3266 
o|contracted procedure: k3197 
o|contracted procedure: k3260 
o|contracted procedure: k3200 
o|contracted procedure: k3254 
o|contracted procedure: k3203 
o|contracted procedure: k3215 
o|contracted procedure: k3221 
o|contracted procedure: k3227 
o|contracted procedure: k3233 
o|contracted procedure: k3239 
o|contracted procedure: k3245 
o|contracted procedure: k3251 
o|contracted procedure: k3400 
o|contracted procedure: k3354 
o|contracted procedure: k3388 
o|contracted procedure: k3375 
o|contracted procedure: k3384 
o|contracted procedure: k3394 
o|contracted procedure: k3444 
o|contracted procedure: k3451 
o|contracted procedure: k3457 
o|contracted procedure: k3477 
o|contracted procedure: k3560 
o|contracted procedure: k3526 
o|contracted procedure: k3539 
o|contracted procedure: k3545 
o|contracted procedure: k3575 
o|contracted procedure: k3581 
o|contracted procedure: k3584 
o|contracted procedure: k3602 
o|contracted procedure: k3618 
o|contracted procedure: k3629 
o|contracted procedure: k3642 
o|contracted procedure: k3635 
o|contracted procedure: k3657 
o|inlining procedure: k3648 
o|contracted procedure: k3678 
o|inlining procedure: k3669 
o|contracted procedure: k3721 
o|contracted procedure: k3717 
o|contracted procedure: k3695 
o|contracted procedure: k3727 
o|contracted procedure: k3749 
o|contracted procedure: k3837 
o|contracted procedure: k3784 
o|contracted procedure: k3793 
o|contracted procedure: k3776 
o|contracted procedure: k3827 
o|contracted procedure: k3823 
o|contracted procedure: k3763 
o|contracted procedure: k3929 
o|contracted procedure: k3869 
o|contracted procedure: k3923 
o|contracted procedure: k3872 
o|contracted procedure: k3917 
o|contracted procedure: k3875 
o|contracted procedure: k3911 
o|contracted procedure: k3878 
o|contracted procedure: k3881 
o|contracted procedure: k3902 
o|contracted procedure: k3940 
o|contracted procedure: k3988 
o|contracted procedure: k3991 
o|contracted procedure: k4028 
o|contracted procedure: k3994 
o|contracted procedure: k4015 
o|contracted procedure: k4069 
o|contracted procedure: k4082 
o|contracted procedure: k40828425 
o|contracted procedure: k4091 
o|contracted procedure: k4096 
o|contracted procedure: k4099 
o|contracted procedure: k4118 
o|contracted procedure: k4128 
o|contracted procedure: k4132 
o|contracted procedure: k4143 
o|contracted procedure: k4209 
o|contracted procedure: k4223 
o|contracted procedure: k4235 
o|contracted procedure: k4238 
o|contracted procedure: k4241 
o|contracted procedure: k4249 
o|contracted procedure: k4257 
o|contracted procedure: k4218 
o|contracted procedure: k4282 
o|contracted procedure: k4300 
o|contracted procedure: k4316 
o|contracted procedure: k4329 
o|contracted procedure: k4339 
o|contracted procedure: k4356 
o|contracted procedure: k4369 
o|contracted procedure: k4379 
o|contracted procedure: k4402 
o|contracted procedure: k4417 
o|contracted procedure: k4543 
o|contracted procedure: k4550 
o|contracted procedure: k4610 
o|contracted procedure: k4589 
o|contracted procedure: k4575 
o|contracted procedure: k4579 
o|contracted procedure: k4583 
o|contracted procedure: k4604 
o|contracted procedure: k4648 
o|contracted procedure: k4623 
o|contracted procedure: k4626 
o|contracted procedure: k4642 
o|contracted procedure: k4667 
o|contracted procedure: k4673 
o|contracted procedure: k4704 
o|contracted procedure: k4716 
o|contracted procedure: k4747 
o|contracted procedure: k4732 
o|contracted procedure: k4741 
o|contracted procedure: k4763 
o|contracted procedure: k4769 
o|contracted procedure: k4783 
o|contracted procedure: k4786 
o|contracted procedure: k5165 
o|contracted procedure: k4795 
o|contracted procedure: k4806 
o|contracted procedure: k4824 
o|contracted procedure: k4828 
o|contracted procedure: k4832 
o|contracted procedure: k4844 
o|contracted procedure: k4853 
o|contracted procedure: k4870 
o|contracted procedure: k4860 
o|contracted procedure: k4867 
o|contracted procedure: k4893 
o|contracted procedure: k4899 
o|contracted procedure: k4905 
o|contracted procedure: k4911 
o|contracted procedure: k4920 
o|contracted procedure: k4949 
o|contracted procedure: k4959 
o|contracted procedure: k4953 
o|contracted procedure: k4966 
o|contracted procedure: k4970 
o|contracted procedure: k4932 
o|contracted procedure: k4936 
o|contracted procedure: k4979 
o|contracted procedure: k5008 
o|contracted procedure: k5018 
o|contracted procedure: k5012 
o|contracted procedure: k5025 
o|contracted procedure: k5029 
o|contracted procedure: k4991 
o|contracted procedure: k4995 
o|contracted procedure: k5049 
o|contracted procedure: k5076 
o|contracted procedure: k5082 
o|contracted procedure: k5055 
o|contracted procedure: k5059 
o|contracted procedure: k5063 
o|contracted procedure: k5091 
o|contracted procedure: k5118 
o|contracted procedure: k5124 
o|contracted procedure: k5097 
o|contracted procedure: k5101 
o|contracted procedure: k5105 
o|contracted procedure: k5131 
o|contracted procedure: k5135 
o|contracted procedure: k5141 
o|contracted procedure: k5147 
o|contracted procedure: k5150 
o|contracted procedure: k5156 
o|contracted procedure: k5162 
o|contracted procedure: k5188 
o|contracted procedure: k5172 
o|contracted procedure: k5181 
o|contracted procedure: k5222 
o|contracted procedure: k5228 
o|contracted procedure: k5242 
o|contracted procedure: k5255 
o|contracted procedure: k5269 
o|contracted procedure: k52658449 
o|contracted procedure: k5291 
o|contracted procedure: k5307 
o|contracted procedure: k5378 
o|contracted procedure: k5333 
o|contracted procedure: k5342 
o|contracted procedure: k5368 
o|contracted procedure: k5371 
o|contracted procedure: k5427 
o|contracted procedure: k5443 
o|contracted procedure: k5449 
o|contracted procedure: k5465 
o|contracted procedure: k5481 
o|contracted procedure: k5484 
o|contracted procedure: k5490 
o|contracted procedure: k5518 
o|contracted procedure: k5618 
o|contracted procedure: k5530 
o|contracted procedure: k5533 
o|contracted procedure: k5564 
o|contracted procedure: k5604 
o|contracted procedure: k5608 
o|contracted procedure: k5645 
o|contracted procedure: k5648 
o|contracted procedure: k5654 
o|contracted procedure: k5636 
o|contracted procedure: k5640 
o|contracted procedure: k6171 
o|contracted procedure: k5667 
o|contracted procedure: k6165 
o|contracted procedure: k5670 
o|contracted procedure: k6159 
o|contracted procedure: k5673 
o|contracted procedure: k6153 
o|contracted procedure: k5676 
o|contracted procedure: k6147 
o|contracted procedure: k5679 
o|contracted procedure: k6141 
o|contracted procedure: k5682 
o|contracted procedure: k6135 
o|contracted procedure: k5685 
o|contracted procedure: k6129 
o|contracted procedure: k5688 
o|contracted procedure: k6120 
o|contracted procedure: k5694 
o|contracted procedure: k5708 
o|contracted procedure: k5714 
o|contracted procedure: k5717 
o|contracted procedure: k5734 
o|contracted procedure: k5748 
o|contracted procedure: k5754 
o|contracted procedure: k5757 
o|contracted procedure: k5772 
o|contracted procedure: k5787 
o|contracted procedure: k5807 
o|contracted procedure: k5813 
o|contracted procedure: k5816 
o|contracted procedure: k5835 
o|contracted procedure: k5850 
o|contracted procedure: k5854 
o|contracted procedure: k5857 
o|contracted procedure: k5866 
o|contracted procedure: k5884 
o|contracted procedure: k5900 
o|contracted procedure: k5922 
o|contracted procedure: k5928 
o|contracted procedure: k5931 
o|contracted procedure: k5960 
o|contracted procedure: k5934 
o|contracted procedure: k5938 
o|contracted procedure: k5942 
o|contracted procedure: k5949 
o|contracted procedure: k5953 
o|contracted procedure: k5957 
o|contracted procedure: k5969 
o|contracted procedure: k5979 
o|contracted procedure: k5991 
o|contracted procedure: k5994 
o|contracted procedure: k6053 
o|contracted procedure: k6007 
o|contracted procedure: k6013 
o|contracted procedure: k6019 
o|contracted procedure: k6026 
o|contracted procedure: k6035 
o|contracted procedure: k6046 
o|contracted procedure: k6042 
o|contracted procedure: k6074 
o|contracted procedure: k6070 
o|contracted procedure: k6063 
o|inlining procedure: k6059 
o|contracted procedure: k6085 
o|contracted procedure: k6089 
o|contracted procedure: k6081 
o|inlining procedure: k6059 
o|contracted procedure: k6095 
o|contracted procedure: k6107 
o|contracted procedure: k6114 
o|contracted procedure: k6426 
o|contracted procedure: k6180 
o|contracted procedure: k6420 
o|contracted procedure: k6183 
o|contracted procedure: k6414 
o|contracted procedure: k6186 
o|contracted procedure: k6408 
o|contracted procedure: k6189 
o|contracted procedure: k6402 
o|contracted procedure: k6192 
o|contracted procedure: k6396 
o|contracted procedure: k6195 
o|contracted procedure: k6212 
o|contracted procedure: k6218 
o|contracted procedure: k6221 
o|contracted procedure: k6233 
o|contracted procedure: k6248 
o|contracted procedure: k6259 
o|contracted procedure: k6387 
o|contracted procedure: k6262 
o|contracted procedure: k6274 
o|contracted procedure: k6292 
o|contracted procedure: k6305 
o|contracted procedure: k6317 
o|contracted procedure: k6332 
o|contracted procedure: k6336 
o|contracted procedure: k6345 
o|contracted procedure: k6358 
o|contracted procedure: k6365 
o|contracted procedure: k6369 
o|contracted procedure: k6375 
o|contracted procedure: k6381 
o|contracted procedure: k6451 
o|contracted procedure: k6471 
o|contracted procedure: k6539 
o|contracted procedure: k6480 
o|contracted procedure: k6533 
o|contracted procedure: k6483 
o|contracted procedure: k6527 
o|contracted procedure: k6486 
o|contracted procedure: k6521 
o|contracted procedure: k6489 
o|contracted procedure: k6511 
o|contracted procedure: k6507 
o|contracted procedure: k6501 
o|contracted procedure: k6514 
o|contracted procedure: k6552 
o|contracted procedure: k6556 
o|contracted procedure: k6560 
o|contracted procedure: k6578 
o|contracted procedure: k6584 
o|contracted procedure: k6609 
o|contracted procedure: k6615 
o|contracted procedure: k6643 
o|contracted procedure: k6639 
o|contracted procedure: k6655 
o|contracted procedure: k6659 
o|contracted procedure: k6663 
o|contracted procedure: k6692 
o|contracted procedure: k6669 
o|contracted procedure: k6675 
o|contracted procedure: k6698 
o|contracted procedure: k6710 
o|contracted procedure: k6721 
o|contracted procedure: k6729 
o|contracted procedure: k6772 
o|contracted procedure: k6744 
o|contracted procedure: k6747 
o|contracted procedure: k6750 
o|contracted procedure: k6765 
o|substituted constant variable: g8675 
o|substituted constant variable: g8675 
o|substituted constant variable: g8675 
o|substituted constant variable: g8675 
o|substituted constant variable: g8675 
o|substituted constant variable: g8675 
o|substituted constant variable: g8675 
o|substituted constant variable: g8675 
o|substituted constant variable: g8675 
o|substituted constant variable: g8675 
o|contracted procedure: k6736 
o|contracted procedure: k6791 
o|contracted procedure: k6808 
o|contracted procedure: k6906 
o|contracted procedure: k6816 
o|contracted procedure: k6900 
o|contracted procedure: k6819 
o|contracted procedure: k6894 
o|contracted procedure: k6822 
o|contracted procedure: k6888 
o|contracted procedure: k6825 
o|contracted procedure: k6849 
o|contracted procedure: k6852 
o|contracted procedure: k6882 
o|contracted procedure: k6984 
o|contracted procedure: k6915 
o|contracted procedure: k6978 
o|contracted procedure: k6918 
o|contracted procedure: k6972 
o|contracted procedure: k6921 
o|contracted procedure: k6966 
o|contracted procedure: k6924 
o|contracted procedure: k6960 
o|contracted procedure: k6927 
o|contracted procedure: k6954 
o|contracted procedure: k6930 
o|contracted procedure: k6942 
o|contracted procedure: k6948 
o|contracted procedure: k6994 
o|contracted procedure: k7022 
o|contracted procedure: k7000 
o|contracted procedure: k7052 
o|contracted procedure: k7034 
o|contracted procedure: k7046 
o|contracted procedure: k7107 
o|contracted procedure: k7074 
o|contracted procedure: k7104 
o|contracted procedure: k7083 
o|contracted procedure: k7100 
o|inlining procedure: "(posixunix.scm:1155) chicken.posix#shell-command" 
o|contracted procedure: k7130 
o|contracted procedure: k7157 
o|contracted procedure: k7136 
o|contracted procedure: k7188 
o|contracted procedure: k7200 
o|contracted procedure: k7117 
o|contracted procedure: k7219 
o|contracted procedure: k7336 
o|contracted procedure: k7340 
o|contracted procedure: k7344 
o|contracted procedure: k7305 
o|contracted procedure: k7351 
o|contracted procedure: k7368 
o|contracted procedure: k7378 
o|contracted procedure: k7382 
o|contracted procedure: k7385 
o|contracted procedure: k7410 
o|inlining procedure: "(posixunix.scm:1268) chicken.posix#shell-command" 
o|contracted procedure: k7477 
o|contracted procedure: k7420 
o|contracted procedure: k7471 
o|contracted procedure: k7423 
o|contracted procedure: k7465 
o|contracted procedure: k7426 
o|contracted procedure: k7459 
o|contracted procedure: k7429 
o|contracted procedure: k7453 
o|contracted procedure: k7432 
o|contracted procedure: k7447 
o|contracted procedure: k7435 
o|contracted procedure: k7537 
o|contracted procedure: k7486 
o|contracted procedure: k7531 
o|contracted procedure: k7489 
o|contracted procedure: k7525 
o|contracted procedure: k7492 
o|contracted procedure: k7519 
o|contracted procedure: k7495 
o|contracted procedure: k7513 
o|contracted procedure: k7498 
o|contracted procedure: k7507 
o|contracted procedure: k7501 
o|contracted procedure: k7556 
o|contracted procedure: k7562 
o|contracted procedure: k7551 
o|contracted procedure: k7624 
o|contracted procedure: k7636 
o|contracted procedure: k7657 
o|contracted procedure: k7676 
o|contracted procedure: k7695 
o|contracted procedure: k7714 
o|contracted procedure: k7733 
o|contracted procedure: k7755 
o|contracted procedure: k7758 
o|contracted procedure: k7767 
o|contracted procedure: k7801 
o|contracted procedure: k7785 
o|contracted procedure: k7822 
o|contracted procedure: k7838 
o|contracted procedure: k7834 
o|contracted procedure: k7844 
o|simplifications: ((if . 7) (let . 143)) 
o|removed binding forms: 475 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest237239 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest237239 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest666668 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest666668 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest732734 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest732734 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest732734 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest732734 0 
(o x)|known list op on rest arg sublist: ##core#rest-car whence761 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest861862 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest861862 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest872873 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest872873 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest890891 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest890891 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest931933 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest931933 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? args954 0 
(o x)|known list op on rest arg sublist: ##core#rest-car args954 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? args954 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr args954 0 
o|inlining procedure: "(posix-common.scm:753) mode1096" 
o|inlining procedure: "(posix-common.scm:764) mode1096" 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest12891292 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest12891292 0 
(o x)|known list op on rest arg sublist: ##core#rest-car mode1304 0 
(o x)|known list op on rest arg sublist: ##core#rest-car timeout1347 0 
o|contracted procedure: k4942 
o|contracted procedure: k5001 
o|contracted procedure: k5069 
o|contracted procedure: k5111 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest14691470 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest14691470 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest15371539 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest15371539 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest15871589 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest15871589 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest16261630 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest16261630 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest16261630 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest16261630 0 
o|contracted procedure: k5913 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest17441748 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest17441748 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest17441748 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest17441748 0 
o|inlining procedure: "(posixunix.scm:1005) err1820" 
o|inlining procedure: "(posixunix.scm:1014) err1820" 
o|inlining procedure: "(posixunix.scm:1020) err1820" 
o|contracted procedure: k6701 
(o x)|known list op on rest arg sublist: ##core#rest-car mode1884 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest19061908 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest19061908 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest19341935 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest19341935 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest19341935 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest19341935 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest19651967 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest19651967 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest19651967 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest19651967 0 
(o x)|known list op on rest arg sublist: ##core#rest-car sig1998 0 
o|removed side-effect free assignment to unused variable: chicken.posix#shell-command 
(o x)|known list op on rest arg sublist: ##core#rest-car args2011 0 
o|inlining procedure: "(posixunix.scm:1225) swapped-ends2062" 
o|inlining procedure: "(posixunix.scm:1224) swapped-ends2062" 
o|inlining procedure: "(posixunix.scm:1220) swapped-ends2062" 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest21502152 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest21502152 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest21502152 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest21502152 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest21722174 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest21722174 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest21722174 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest21722174 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest838839 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest838839 0 
o|replaced variables: 2 
o|removed binding forms: 8 
(o x)|known list op on rest arg sublist: ##core#rest-null? r3198 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r3198 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r3198 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r3198 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r3873 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r3873 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r3873 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r3873 1 
o|removed side-effect free assignment to unused variable: mode1096 
(o x)|known list op on rest arg sublist: ##core#rest-null? r5671 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r5671 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r5671 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r5671 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r6184 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r6184 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r6184 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r6184 1 
o|removed side-effect free assignment to unused variable: err1820 
o|substituted constant variable: loc18408781 
o|substituted constant variable: msg18388779 
o|substituted constant variable: loc18408787 
o|substituted constant variable: msg18388785 
o|substituted constant variable: loc18408793 
o|substituted constant variable: msg18388791 
(o x)|known list op on rest arg sublist: ##core#rest-null? r6820 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r6820 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r6820 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r6820 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r6919 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r6919 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r6919 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r6919 1 
o|inlining procedure: k7096 
o|inlining procedure: k7414 
(o x)|known list op on rest arg sublist: ##core#rest-null? r7424 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r7424 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r7424 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r7424 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r7490 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r7490 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r7490 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r7490 1 
o|replaced variables: 33 
o|removed binding forms: 4 
(o x)|known list op on rest arg sublist: ##core#rest-null? r5677 2 
(o x)|known list op on rest arg sublist: ##core#rest-car r5677 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r5677 2 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r5677 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r6190 2 
(o x)|known list op on rest arg sublist: ##core#rest-car r6190 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r6190 2 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r6190 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r6925 2 
(o x)|known list op on rest arg sublist: ##core#rest-car r6925 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r6925 2 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r6925 2 
o|inlining procedure: k7262 
o|inlining procedure: k7262 
o|inlining procedure: k7266 
o|inlining procedure: k7266 
(o x)|known list op on rest arg sublist: ##core#rest-null? r7430 2 
(o x)|known list op on rest arg sublist: ##core#rest-car r7430 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r7430 2 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r7430 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r7496 2 
(o x)|known list op on rest arg sublist: ##core#rest-car r7496 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r7496 2 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r7496 2 
o|converted assignments to bindings: (setup1819) 
o|converted assignments to bindings: (check1098) 
o|simplifications: ((let . 2)) 
o|removed binding forms: 39 
o|contracted procedure: k4319 
o|contracted procedure: k4359 
(o x)|known list op on rest arg sublist: ##core#rest-null? r5683 3 
(o x)|known list op on rest arg sublist: ##core#rest-car r5683 3 
(o x)|known list op on rest arg sublist: ##core#rest-null? r5683 3 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r5683 3 
o|substituted constant variable: r72638891 
o|substituted constant variable: r72678893 
o|removed binding forms: 13 
o|removed binding forms: 4 
o|direct leaf routine/allocation: g10251032 0 
o|direct leaf routine/allocation: doloop13671385 0 
o|direct leaf routine/allocation: doloop13661368 0 
o|direct leaf routine/allocation: peek1650 0 
o|direct leaf routine/allocation: g21202127 0 
o|contracted procedure: "(posix-common.scm:704) k4121" 
o|contracted procedure: k4815 
o|contracted procedure: k4818 
o|converted assignments to bindings: (doloop13671385) 
o|converted assignments to bindings: (doloop13661368) 
o|contracted procedure: "(posixunix.scm:840) k5844" 
o|contracted procedure: "(posixunix.scm:1262) k7371" 
o|simplifications: ((let . 2)) 
o|removed binding forms: 5 
o|direct leaf routine/allocation: for-each-loop10241037 0 
o|direct leaf routine/allocation: for-each-loop21192137 0 
o|converted assignments to bindings: (for-each-loop10241037) 
o|converted assignments to bindings: (for-each-loop21192137) 
o|simplifications: ((let . 2)) 
o|customizable procedures: (loop1501 %process2108 chkstrlst2115 chicken.posix#process-impl output-port2061 make-on-close2024 input-port2060 spawn2059 connect-parent2026 needed-pipe2025 k7239 connect-child2027 chicken.posix#call-with-exec-args setup1819 k6441 loop1789 k6265 loop1696 ready?1649 fetch1651 loop1598 k5336 for-each-loop14801492 k4798 k4801 k5033 lp1421 lp1440 k4883 check1098 chicken.posix#check-environment-list map-loop10681086 chicken.posix#list->c-string-buffer k4191 doloop10121013 tmp12525 tmp22526 doloop993994 chicken.posix#free-c-string-buffer chicken.posix#process-wait-impl chicken.posix#check-time-vector k3569 mode780 check781 chicken.posix#chown k3116 chicken.posix#stat g256265 map-loop250272) 
o|calls to known targets: 215 
o|unused rest argument: rest237239 f_2752 
o|unused rest argument: rest666668 f_3003 
o|unused rest argument: rest732734 f_3192 
o|unused rest argument: rest861862 f_3646 
o|unused rest argument: rest872873 f_3667 
o|unused rest argument: rest890891 f_3699 
o|unused rest argument: rest931933 f_3782 
o|unused rest argument: args954 f_3867 
o|identified direct recursive calls: f_4113 1 
o|unused rest argument: rest12891292 f_4587 
o|identified direct recursive calls: f_5086 1 
o|identified direct recursive calls: f_5044 1 
o|identified direct recursive calls: f_4915 2 
o|identified direct recursive calls: f_4974 2 
o|unused rest argument: rest14691470 f_5170 
o|unused rest argument: rest15371539 f_5331 
o|unused rest argument: rest15871589 f_5528 
o|identified direct recursive calls: f_5917 1 
o|unused rest argument: rest16261630 f_5665 
o|identified direct recursive calls: f_6340 1 
o|unused rest argument: rest17441748 f_6178 
o|unused rest argument: rest19061908 f_6742 
o|unused rest argument: rest19341935 f_6814 
o|unused rest argument: rest19651967 f_6913 
o|identified direct recursive calls: f_7363 1 
o|unused rest argument: rest21502152 f_7418 
o|unused rest argument: rest21722174 f_7484 
o|unused rest argument: _21972200 f_7573 
o|identified direct recursive calls: f_7750 2 
o|unused rest argument: rest838839 f_7783 
o|fast box initializations: 19 
o|fast global references: 76 
o|fast global assignments: 11 
o|dropping unused closure argument: f_2957 
o|dropping unused closure argument: f_3435 
o|dropping unused closure argument: f_3472 
o|dropping unused closure argument: f_3627 
o|dropping unused closure argument: f_3938 
o|dropping unused closure argument: f_4055 
o|dropping unused closure argument: f_4089 
o|dropping unused closure argument: f_4295 
o|dropping unused closure argument: f_5413 
o|dropping unused closure argument: f_6478 
o|dropping unused closure argument: f_6991 
o|dropping unused closure argument: f_7126 
o|dropping unused closure argument: f_7163 
o|dropping unused closure argument: f_7183 
o|dropping unused closure argument: f_7195 
o|dropping unused closure argument: f_7347 
*/
/* end of file */
