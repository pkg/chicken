/* Generated from chicken-syntax.scm by the CHICKEN compiler
   http://www.call-cc.org
   Version 5.2.0 (rev 317468e4)
   linux-unix-gnu-x86-64 [ 64bit dload ptables ]
   command line: chicken-syntax.scm -optimize-level 2 -include-path . -include-path ./ -inline -ignore-repository -feature chicken-bootstrap -no-warnings -specialize -consult-types-file ./types.db -explicit-use -no-trace -output-file chicken-syntax.c
   unit: chicken-syntax
   uses: expand internal library
*/
#include "chicken.h"

static C_PTABLE_ENTRY *create_ptable(void);
C_noret_decl(C_expand_toplevel)
C_externimport void C_ccall C_expand_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_internal_toplevel)
C_externimport void C_ccall C_internal_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_library_toplevel)
C_externimport void C_ccall C_library_toplevel(C_word c,C_word *av) C_noret;

static C_TLS C_word lf[286];
static double C_possibly_force_alignment;
static C_char C_TLS li0[] C_aligned={C_lihdr(0,0,12),40,116,97,107,101,32,108,115,116,32,110,41,0,0,0,0};
static C_char C_TLS li1[] C_aligned={C_lihdr(0,0,16),40,97,53,54,57,53,32,102,111,114,109,32,114,32,99,41};
static C_char C_TLS li2[] C_aligned={C_lihdr(0,0,16),40,97,53,55,52,52,32,102,111,114,109,32,114,32,99,41};
static C_char C_TLS li3[] C_aligned={C_lihdr(0,0,13),40,97,53,56,51,51,32,120,32,114,32,99,41,0,0,0};
static C_char C_TLS li4[] C_aligned={C_lihdr(0,0,16),40,97,53,56,53,49,32,102,111,114,109,32,114,32,99,41};
static C_char C_TLS li5[] C_aligned={C_lihdr(0,0,27),40,108,111,111,112,32,120,115,32,118,97,114,115,32,98,115,32,118,97,108,115,32,114,101,115,116,41,0,0,0,0,0};
static C_char C_TLS li6[] C_aligned={C_lihdr(0,0,16),40,97,53,57,49,54,32,102,111,114,109,32,114,32,99,41};
static C_char C_TLS li7[] C_aligned={C_lihdr(0,0,24),40,108,111,111,112,32,120,115,32,118,97,114,115,32,118,97,108,115,32,114,101,115,116,41};
static C_char C_TLS li8[] C_aligned={C_lihdr(0,0,16),40,97,54,49,49,49,32,102,111,114,109,32,114,32,99,41};
static C_char C_TLS li9[] C_aligned={C_lihdr(0,0,13),40,103,51,48,51,54,32,118,110,97,109,101,41,0,0,0};
static C_char C_TLS li10[] C_aligned={C_lihdr(0,0,7),40,103,51,48,54,50,41,0};
static C_char C_TLS li11[] C_aligned={C_lihdr(0,0,14),40,108,111,111,112,32,115,108,111,116,115,32,105,41,0,0};
static C_char C_TLS li12[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,51,48,53,54,32,103,51,48,54,56,41,0,0,0,0};
static C_char C_TLS li13[] C_aligned={C_lihdr(0,0,25),40,102,111,114,45,101,97,99,104,45,108,111,111,112,51,48,51,53,32,103,51,48,52,50,41,0,0,0,0,0,0,0};
static C_char C_TLS li14[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,51,48,48,57,32,103,51,48,50,49,41,0,0,0,0};
static C_char C_TLS li15[] C_aligned={C_lihdr(0,0,16),40,97,54,50,56,51,32,102,111,114,109,32,114,32,99,41};
static C_char C_TLS li16[] C_aligned={C_lihdr(0,0,16),40,97,54,55,50,50,32,102,111,114,109,32,114,32,99,41};
static C_char C_TLS li17[] C_aligned={C_lihdr(0,0,8),40,108,111,111,112,32,105,41};
static C_char C_TLS li18[] C_aligned={C_lihdr(0,0,11),40,103,101,110,118,97,114,115,32,110,41,0,0,0,0,0};
static C_char C_TLS li19[] C_aligned={C_lihdr(0,0,18),40,108,111,111,112,32,110,32,112,114,101,118,32,110,111,100,101,41,0,0,0,0,0,0};
static C_char C_TLS li20[] C_aligned={C_lihdr(0,0,7),40,97,54,57,54,51,41,0};
static C_char C_TLS li21[] C_aligned={C_lihdr(0,0,26),40,109,97,112,45,108,111,111,112,50,57,51,51,32,103,50,57,52,53,32,103,50,57,52,54,41,0,0,0,0,0,0};
static C_char C_TLS li22[] C_aligned={C_lihdr(0,0,19),40,98,117,105,108,100,32,118,97,114,115,50,32,118,114,101,115,116,41,0,0,0,0,0};
static C_char C_TLS li23[] C_aligned={C_lihdr(0,0,19),40,97,54,57,55,51,32,118,97,114,115,49,32,118,97,114,115,50,41,0,0,0,0,0};
static C_char C_TLS li24[] C_aligned={C_lihdr(0,0,22),40,97,54,57,52,51,32,118,97,114,115,32,97,114,103,99,32,114,101,115,116,41,0,0};
static C_char C_TLS li25[] C_aligned={C_lihdr(0,0,14),40,103,50,56,57,48,32,99,32,98,111,100,121,41,0,0};
static C_char C_TLS li26[] C_aligned={C_lihdr(0,0,17),40,102,111,108,100,114,50,56,56,53,32,103,50,56,56,54,41,0,0,0,0,0,0,0};
static C_char C_TLS li27[] C_aligned={C_lihdr(0,0,22),40,97,55,50,48,53,32,118,97,114,115,32,97,114,103,99,32,114,101,115,116,41,0,0};
static C_char C_TLS li28[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,56,52,50,32,103,50,56,53,52,41,0,0,0,0};
static C_char C_TLS li29[] C_aligned={C_lihdr(0,0,16),40,97,54,56,51,53,32,102,111,114,109,32,114,32,99,41};
static C_char C_TLS li30[] C_aligned={C_lihdr(0,0,19),40,108,111,111,112,32,97,114,103,115,32,118,97,114,100,101,102,115,41,0,0,0,0,0};
static C_char C_TLS li31[] C_aligned={C_lihdr(0,0,16),40,97,55,50,55,57,32,102,111,114,109,32,114,32,99,41};
static C_char C_TLS li32[] C_aligned={C_lihdr(0,0,16),40,97,55,52,51,57,32,102,111,114,109,32,114,32,99,41};
static C_char C_TLS li33[] C_aligned={C_lihdr(0,0,36),40,114,101,99,117,114,32,118,97,114,115,32,100,101,102,97,117,108,116,101,114,115,32,110,111,110,45,100,101,102,97,117,108,116,115,41,0,0,0,0};
static C_char C_TLS li34[] C_aligned={C_lihdr(0,0,45),40,109,97,107,101,45,105,102,45,116,114,101,101,32,118,97,114,115,32,100,101,102,97,117,108,116,101,114,115,32,98,111,100,121,45,112,114,111,99,32,114,101,115,116,41,0,0,0};
static C_char C_TLS li35[] C_aligned={C_lihdr(0,0,23),40,112,114,101,102,105,120,45,115,121,109,32,112,114,101,102,105,120,32,115,121,109,41,0};
static C_char C_TLS li36[] C_aligned={C_lihdr(0,0,9),40,103,50,54,57,53,32,118,41,0,0,0,0,0,0,0};
static C_char C_TLS li37[] C_aligned={C_lihdr(0,0,11),40,103,50,55,53,50,32,118,97,114,41,0,0,0,0,0};
static C_char C_TLS li38[] C_aligned={C_lihdr(0,0,42),40,114,101,99,117,114,32,118,97,114,115,32,100,101,102,97,117,108,116,101,114,45,110,97,109,101,115,32,100,101,102,115,32,110,101,120,116,45,103,117,121,41,0,0,0,0,0,0};
static C_char C_TLS li39[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,55,52,54,32,103,50,55,53,56,41,0,0,0,0};
static C_char C_TLS li40[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,55,49,55,32,103,50,55,50,57,41,0,0,0,0};
static C_char C_TLS li41[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,54,56,57,32,103,50,55,48,49,41,0,0,0,0};
static C_char C_TLS li42[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,54,53,57,32,103,50,54,55,49,41,0,0,0,0};
static C_char C_TLS li43[] C_aligned={C_lihdr(0,0,16),40,97,55,53,49,54,32,102,111,114,109,32,114,32,99,41};
static C_char C_TLS li44[] C_aligned={C_lihdr(0,0,14),40,102,111,108,100,32,98,115,32,108,97,115,116,41,0,0};
static C_char C_TLS li45[] C_aligned={C_lihdr(0,0,16),40,97,55,57,54,52,32,102,111,114,109,32,114,32,99,41};
static C_char C_TLS li46[] C_aligned={C_lihdr(0,0,24),40,113,117,111,116,105,102,121,45,112,114,111,99,50,53,53,57,32,120,115,32,105,100,41};
static C_char C_TLS li47[] C_aligned={C_lihdr(0,0,16),40,97,56,49,48,54,32,102,111,114,109,32,114,32,99,41};
static C_char C_TLS li48[] C_aligned={C_lihdr(0,0,16),40,97,56,50,49,48,32,102,111,114,109,32,114,32,99,41};
static C_char C_TLS li49[] C_aligned={C_lihdr(0,0,13),40,97,56,50,53,52,32,120,32,114,32,99,41,0,0,0};
static C_char C_TLS li50[] C_aligned={C_lihdr(0,0,26),40,109,97,112,45,108,111,111,112,50,53,48,55,32,103,50,53,49,57,32,103,50,53,50,48,41,0,0,0,0,0,0};
static C_char C_TLS li51[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,52,53,57,32,103,50,52,55,49,41,0,0,0,0};
static C_char C_TLS li52[] C_aligned={C_lihdr(0,0,13),40,97,56,52,53,50,32,97,32,95,32,95,41,0,0,0};
static C_char C_TLS li53[] C_aligned={C_lihdr(0,0,23),40,102,111,108,100,108,50,52,56,50,32,103,50,52,56,51,32,103,50,52,56,49,41,0};
static C_char C_TLS li54[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,52,51,48,32,103,50,52,52,50,41,0,0,0,0};
static C_char C_TLS li55[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,52,48,52,32,103,50,52,49,54,41,0,0,0,0};
static C_char C_TLS li56[] C_aligned={C_lihdr(0,0,16),40,97,56,50,55,54,32,102,111,114,109,32,114,32,99,41};
static C_char C_TLS li57[] C_aligned={C_lihdr(0,0,16),40,102,111,108,100,32,118,98,105,110,100,105,110,103,115,41};
static C_char C_TLS li58[] C_aligned={C_lihdr(0,0,16),40,97,56,53,51,52,32,102,111,114,109,32,114,32,99,41};
static C_char C_TLS li59[] C_aligned={C_lihdr(0,0,18),40,97,112,112,101,110,100,42,50,50,49,48,32,105,108,32,108,41,0,0,0,0,0,0};
static C_char C_TLS li60[] C_aligned={C_lihdr(0,0,17),40,109,97,112,42,50,50,49,49,32,112,114,111,99,32,108,41,0,0,0,0,0,0,0};
static C_char C_TLS li61[] C_aligned={C_lihdr(0,0,9),40,103,50,50,55,49,32,118,41,0,0,0,0,0,0,0};
static C_char C_TLS li62[] C_aligned={C_lihdr(0,0,10),40,108,111,111,107,117,112,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li63[] C_aligned={C_lihdr(0,0,9),40,103,50,51,50,50,32,118,41,0,0,0,0,0,0,0};
static C_char C_TLS li64[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,51,49,54,32,103,50,51,50,56,41,0,0,0,0};
static C_char C_TLS li65[] C_aligned={C_lihdr(0,0,26),40,102,111,108,100,32,108,108,105,115,116,115,32,101,120,112,115,32,108,108,105,115,116,115,50,41,0,0,0,0,0,0};
static C_char C_TLS li66[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,51,53,49,32,103,50,51,54,51,41,0,0,0,0};
static C_char C_TLS li67[] C_aligned={C_lihdr(0,0,17),40,108,111,111,112,32,108,108,105,115,116,115,32,97,99,99,41,0,0,0,0,0,0,0};
static C_char C_TLS li68[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,50,54,53,32,103,50,50,55,55,41,0,0,0,0};
static C_char C_TLS li69[] C_aligned={C_lihdr(0,0,17),40,108,111,111,112,32,108,108,105,115,116,115,32,97,99,99,41,0,0,0,0,0,0,0};
static C_char C_TLS li70[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,50,50,55,32,103,50,50,51,57,41,0,0,0,0};
static C_char C_TLS li71[] C_aligned={C_lihdr(0,0,16),40,97,56,53,56,56,32,102,111,114,109,32,114,32,99,41};
static C_char C_TLS li72[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,49,54,54,32,103,50,49,55,56,41,0,0,0,0};
static C_char C_TLS li73[] C_aligned={C_lihdr(0,0,25),40,102,111,114,45,101,97,99,104,45,108,111,111,112,50,49,53,54,32,103,50,49,54,51,41,0,0,0,0,0,0,0};
static C_char C_TLS li74[] C_aligned={C_lihdr(0,0,22),40,97,57,49,48,50,32,118,97,114,115,32,97,114,103,99,32,114,101,115,116,41,0,0};
static C_char C_TLS li75[] C_aligned={C_lihdr(0,0,16),40,97,57,48,54,53,32,102,111,114,109,32,114,32,99,41};
static C_char C_TLS li76[] C_aligned={C_lihdr(0,0,16),40,97,57,49,57,53,32,102,111,114,109,32,114,32,99,41};
static C_char C_TLS li77[] C_aligned={C_lihdr(0,0,16),40,97,57,50,49,54,32,102,111,114,109,32,114,32,99,41};
static C_char C_TLS li78[] C_aligned={C_lihdr(0,0,16),40,97,57,50,52,52,32,102,111,114,109,32,114,32,99,41};
static C_char C_TLS li79[] C_aligned={C_lihdr(0,0,7),40,97,57,50,56,51,41,0};
static C_char C_TLS li80[] C_aligned={C_lihdr(0,0,48),40,97,57,50,56,57,32,110,97,109,101,50,48,57,51,32,108,105,98,50,48,57,53,32,95,50,48,57,55,32,95,50,48,57,55,32,95,50,48,57,55,32,95,50,48,57,55,41};
static C_char C_TLS li81[] C_aligned={C_lihdr(0,0,9),40,103,50,48,56,49,32,120,41,0,0,0,0,0,0,0};
static C_char C_TLS li82[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,48,55,53,32,103,50,48,56,55,41,0,0,0,0};
static C_char C_TLS li83[] C_aligned={C_lihdr(0,0,13),40,97,57,50,54,56,32,120,32,114,32,99,41,0,0,0};
static C_char C_TLS li84[] C_aligned={C_lihdr(0,0,9),40,103,49,54,54,54,32,122,41,0,0,0,0,0,0,0};
static C_char C_TLS li85[] C_aligned={C_lihdr(0,0,7),40,103,49,54,57,52,41,0};
static C_char C_TLS li86[] C_aligned={C_lihdr(0,0,7),40,103,49,55,50,50,41,0};
static C_char C_TLS li87[] C_aligned={C_lihdr(0,0,14),40,103,49,56,52,53,32,115,32,116,101,109,112,41,0,0};
static C_char C_TLS li88[] C_aligned={C_lihdr(0,0,26),40,109,97,112,45,108,111,111,112,50,48,51,49,32,103,50,48,52,51,32,103,50,48,52,52,41,0,0,0,0,0,0};
static C_char C_TLS li89[] C_aligned={C_lihdr(0,0,26),40,109,97,112,45,108,111,111,112,49,57,57,53,32,103,50,48,48,55,32,103,50,48,48,56,41,0,0,0,0,0,0};
static C_char C_TLS li90[] C_aligned={C_lihdr(0,0,32),40,109,97,112,45,108,111,111,112,49,57,53,51,32,103,49,57,54,53,32,103,49,57,54,54,32,103,49,57,54,55,41};
static C_char C_TLS li91[] C_aligned={C_lihdr(0,0,26),40,109,97,112,45,108,111,111,112,49,57,49,55,32,103,49,57,50,57,32,103,49,57,51,48,41,0,0,0,0,0,0};
static C_char C_TLS li92[] C_aligned={C_lihdr(0,0,26),40,109,97,112,45,108,111,111,112,49,56,56,49,32,103,49,56,57,51,32,103,49,56,57,52,41,0,0,0,0,0,0};
static C_char C_TLS li93[] C_aligned={C_lihdr(0,0,32),40,109,97,112,45,108,111,111,112,49,56,51,57,32,103,49,56,53,49,32,103,49,56,53,50,32,103,49,56,53,51,41};
static C_char C_TLS li94[] C_aligned={C_lihdr(0,0,26),40,109,97,112,45,108,111,111,112,49,56,48,56,32,103,49,56,50,48,32,103,49,56,50,49,41,0,0,0,0,0,0};
static C_char C_TLS li95[] C_aligned={C_lihdr(0,0,26),40,109,97,112,45,108,111,111,112,49,55,55,55,32,103,49,55,56,57,32,103,49,55,57,48,41,0,0,0,0,0,0};
static C_char C_TLS li96[] C_aligned={C_lihdr(0,0,26),40,109,97,112,45,108,111,111,112,49,55,52,54,32,103,49,55,53,56,32,103,49,55,53,57,41,0,0,0,0,0,0};
static C_char C_TLS li97[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,49,55,49,54,32,103,49,55,50,56,41,0,0,0,0};
static C_char C_TLS li98[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,49,54,56,56,32,103,49,55,48,48,41,0,0,0,0};
static C_char C_TLS li99[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,49,54,54,48,32,103,49,54,55,50,41,0,0,0,0};
static C_char C_TLS li100[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,49,54,51,51,32,103,49,54,52,53,41,0,0,0,0};
static C_char C_TLS li101[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,49,54,48,54,32,103,49,54,49,56,41,0,0,0,0};
static C_char C_TLS li102[] C_aligned={C_lihdr(0,0,16),40,97,57,51,53,50,32,102,111,114,109,32,114,32,99,41};
static C_char C_TLS li103[] C_aligned={C_lihdr(0,0,7),40,103,49,51,48,57,41,0};
static C_char C_TLS li104[] C_aligned={C_lihdr(0,0,7),40,103,49,51,51,55,41,0};
static C_char C_TLS li105[] C_aligned={C_lihdr(0,0,26),40,109,97,112,45,108,111,111,112,49,53,54,48,32,103,49,53,55,50,32,103,49,53,55,51,41,0,0,0,0,0,0};
static C_char C_TLS li106[] C_aligned={C_lihdr(0,0,26),40,109,97,112,45,108,111,111,112,49,53,50,52,32,103,49,53,51,54,32,103,49,53,51,55,41,0,0,0,0,0,0};
static C_char C_TLS li107[] C_aligned={C_lihdr(0,0,26),40,109,97,112,45,108,111,111,112,49,52,56,56,32,103,49,53,48,48,32,103,49,53,48,49,41,0,0,0,0,0,0};
static C_char C_TLS li108[] C_aligned={C_lihdr(0,0,26),40,109,97,112,45,108,111,111,112,49,52,53,50,32,103,49,52,54,52,32,103,49,52,54,53,41,0,0,0,0,0,0};
static C_char C_TLS li109[] C_aligned={C_lihdr(0,0,26),40,109,97,112,45,108,111,111,112,49,52,49,56,32,103,49,52,51,48,32,103,49,52,51,49,41,0,0,0,0,0,0};
static C_char C_TLS li110[] C_aligned={C_lihdr(0,0,8),40,108,111,111,112,32,110,41};
static C_char C_TLS li111[] C_aligned={C_lihdr(0,0,26),40,109,97,112,45,108,111,111,112,49,51,54,49,32,103,49,51,55,51,32,103,49,51,55,52,41,0,0,0,0,0,0};
static C_char C_TLS li112[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,49,51,56,50,32,103,49,51,57,52,41,0,0,0,0};
static C_char C_TLS li113[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,49,51,51,49,32,103,49,51,52,51,41,0,0,0,0};
static C_char C_TLS li114[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,49,51,48,51,32,103,49,51,49,53,41,0,0,0,0};
static C_char C_TLS li115[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,49,50,55,54,32,103,49,50,56,56,41,0,0,0,0};
static C_char C_TLS li116[] C_aligned={C_lihdr(0,0,17),40,97,49,48,50,56,50,32,102,111,114,109,32,114,32,99,41,0,0,0,0,0,0,0};
static C_char C_TLS li117[] C_aligned={C_lihdr(0,0,17),40,97,49,48,57,51,53,32,102,111,114,109,32,114,32,99,41,0,0,0,0,0,0,0};
static C_char C_TLS li118[] C_aligned={C_lihdr(0,0,17),40,97,49,48,57,53,50,32,102,111,114,109,32,114,32,99,41,0,0,0,0,0,0,0};
static C_char C_TLS li119[] C_aligned={C_lihdr(0,0,17),40,97,49,48,57,54,57,32,102,111,114,109,32,114,32,99,41,0,0,0,0,0,0,0};
static C_char C_TLS li120[] C_aligned={C_lihdr(0,0,17),40,97,49,48,57,57,48,32,102,111,114,109,32,114,32,99,41,0,0,0,0,0,0,0};
static C_char C_TLS li121[] C_aligned={C_lihdr(0,0,17),40,97,49,49,48,48,52,32,102,111,114,109,32,114,32,99,41,0,0,0,0,0,0,0};
static C_char C_TLS li122[] C_aligned={C_lihdr(0,0,12),40,103,49,49,54,51,32,115,108,111,116,41,0,0,0,0};
static C_char C_TLS li123[] C_aligned={C_lihdr(0,0,18),40,109,97,112,115,108,111,116,115,32,115,108,111,116,115,32,105,41,0,0,0,0,0,0};
static C_char C_TLS li124[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,49,49,53,55,32,103,49,49,54,57,41,0,0,0,0};
static C_char C_TLS li125[] C_aligned={C_lihdr(0,0,14),40,97,49,49,48,57,55,32,120,32,114,32,99,41,0,0};
static C_char C_TLS li126[] C_aligned={C_lihdr(0,0,17),40,97,49,49,52,57,50,32,102,111,114,109,32,114,32,99,41,0,0,0,0,0,0,0};
static C_char C_TLS li127[] C_aligned={C_lihdr(0,0,16),40,108,111,111,112,49,48,56,56,32,108,32,108,101,110,41};
static C_char C_TLS li128[] C_aligned={C_lihdr(0,0,16),40,108,111,111,112,49,48,56,56,32,108,32,108,101,110,41};
static C_char C_TLS li129[] C_aligned={C_lihdr(0,0,29),40,97,49,49,53,48,57,32,105,110,112,117,116,32,114,101,110,97,109,101,32,99,111,109,112,97,114,101,41,0,0,0};
static C_char C_TLS li130[] C_aligned={C_lihdr(0,0,29),40,97,49,49,54,53,55,32,105,110,112,117,116,32,114,101,110,97,109,101,32,99,111,109,112,97,114,101,41,0,0,0};
static C_char C_TLS li131[] C_aligned={C_lihdr(0,0,17),40,97,49,49,55,53,52,32,102,111,114,109,32,114,32,99,41,0,0,0,0,0,0,0};
static C_char C_TLS li132[] C_aligned={C_lihdr(0,0,14),40,97,49,49,55,56,51,32,120,32,114,32,99,41,0,0};
static C_char C_TLS li133[] C_aligned={C_lihdr(0,0,14),40,97,49,49,56,49,51,32,120,32,114,32,99,41,0,0};
static C_char C_TLS li134[] C_aligned={C_lihdr(0,0,14),40,97,49,49,56,51,48,32,120,32,114,32,99,41,0,0};
static C_char C_TLS li135[] C_aligned={C_lihdr(0,0,13),40,103,57,55,52,32,99,108,97,117,115,101,41,0,0,0};
static C_char C_TLS li136[] C_aligned={C_lihdr(0,0,18),40,109,97,112,45,108,111,111,112,57,54,56,32,103,57,56,48,41,0,0,0,0,0,0};
static C_char C_TLS li137[] C_aligned={C_lihdr(0,0,14),40,97,49,49,56,57,48,32,120,32,114,32,99,41,0,0};
static C_char C_TLS li138[] C_aligned={C_lihdr(0,0,23),40,109,97,112,45,108,111,111,112,57,49,51,32,103,57,50,53,32,103,57,50,54,41,0};
static C_char C_TLS li139[] C_aligned={C_lihdr(0,0,18),40,109,97,112,45,108,111,111,112,56,55,57,32,103,56,57,49,41,0,0,0,0,0,0};
static C_char C_TLS li140[] C_aligned={C_lihdr(0,0,16),40,108,111,111,112,50,32,97,110,97,109,101,115,32,105,41};
static C_char C_TLS li141[] C_aligned={C_lihdr(0,0,25),40,108,111,111,112,32,97,114,103,115,32,97,110,97,109,101,115,32,97,116,121,112,101,115,41,0,0,0,0,0,0,0};
static C_char C_TLS li142[] C_aligned={C_lihdr(0,0,14),40,97,49,50,48,49,51,32,120,32,114,32,99,41,0,0};
static C_char C_TLS li143[] C_aligned={C_lihdr(0,0,17),40,97,49,50,52,52,56,32,116,121,112,101,32,118,97,114,41,0,0,0,0,0,0,0};
static C_char C_TLS li144[] C_aligned={C_lihdr(0,0,18),40,109,97,112,45,108,111,111,112,56,49,48,32,103,56,50,50,41,0,0,0,0,0,0};
static C_char C_TLS li145[] C_aligned={C_lihdr(0,0,18),40,109,97,112,45,108,111,111,112,55,56,50,32,103,55,57,52,41,0,0,0,0,0,0};
static C_char C_TLS li146[] C_aligned={C_lihdr(0,0,15),40,108,111,111,112,55,51,52,32,108,32,108,101,110,41,0};
static C_char C_TLS li147[] C_aligned={C_lihdr(0,0,15),40,108,111,111,112,55,51,52,32,108,32,108,101,110,41,0};
static C_char C_TLS li148[] C_aligned={C_lihdr(0,0,29),40,97,49,50,51,56,48,32,105,110,112,117,116,32,114,101,110,97,109,101,32,99,111,109,112,97,114,101,41,0,0,0};
static C_char C_TLS li149[] C_aligned={C_lihdr(0,0,14),40,97,49,50,54,56,57,32,120,32,114,32,99,41,0,0};
static C_char C_TLS li150[] C_aligned={C_lihdr(0,0,8),40,97,49,50,55,52,55,41};
static C_char C_TLS li151[] C_aligned={C_lihdr(0,0,32),40,97,49,50,55,53,55,32,116,121,112,101,54,57,49,32,112,114,101,100,54,57,51,32,112,117,114,101,54,57,53,41};
static C_char C_TLS li152[] C_aligned={C_lihdr(0,0,14),40,97,49,50,55,50,55,32,120,32,114,32,99,41,0,0};
static C_char C_TLS li153[] C_aligned={C_lihdr(0,0,6),40,103,54,50,51,41,0,0};
static C_char C_TLS li154[] C_aligned={C_lihdr(0,0,18),40,109,97,112,45,108,111,111,112,54,49,55,32,103,54,50,57,41,0,0,0,0,0,0};
static C_char C_TLS li155[] C_aligned={C_lihdr(0,0,16),40,112,97,114,115,101,45,99,108,97,117,115,101,32,99,41};
static C_char C_TLS li156[] C_aligned={C_lihdr(0,0,18),40,109,97,112,45,108,111,111,112,54,52,56,32,103,54,54,48,41,0,0,0,0,0,0};
static C_char C_TLS li157[] C_aligned={C_lihdr(0,0,17),40,97,49,50,56,51,56,32,102,111,114,109,32,114,32,99,41,0,0,0,0,0,0,0};
static C_char C_TLS li158[] C_aligned={C_lihdr(0,0,17),40,97,49,51,49,49,49,32,102,111,114,109,32,114,32,99,41,0,0,0,0,0,0,0};
static C_char C_TLS li159[] C_aligned={C_lihdr(0,0,10),40,116,111,112,108,101,118,101,108,41,0,0,0,0,0,0};


C_noret_decl(f_10013)
static void C_fcall f_10013(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_10061)
static void C_fcall f_10061(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_10109)
static void C_fcall f_10109(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10134)
static void C_ccall f_10134(C_word c,C_word *av) C_noret;
C_noret_decl(f_10143)
static void C_fcall f_10143(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10168)
static void C_ccall f_10168(C_word c,C_word *av) C_noret;
C_noret_decl(f_10177)
static void C_fcall f_10177(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10202)
static void C_ccall f_10202(C_word c,C_word *av) C_noret;
C_noret_decl(f_10211)
static void C_fcall f_10211(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10245)
static void C_fcall f_10245(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10281)
static void C_ccall f_10281(C_word c,C_word *av) C_noret;
C_noret_decl(f_10283)
static void C_ccall f_10283(C_word c,C_word *av) C_noret;
C_noret_decl(f_10287)
static void C_ccall f_10287(C_word c,C_word *av) C_noret;
C_noret_decl(f_10301)
static void C_ccall f_10301(C_word c,C_word *av) C_noret;
C_noret_decl(f_10305)
static void C_fcall f_10305(C_word t0,C_word t1) C_noret;
C_noret_decl(f_10313)
static void C_ccall f_10313(C_word c,C_word *av) C_noret;
C_noret_decl(f_10316)
static void C_ccall f_10316(C_word c,C_word *av) C_noret;
C_noret_decl(f_10320)
static void C_fcall f_10320(C_word t0,C_word t1) C_noret;
C_noret_decl(f_10328)
static void C_ccall f_10328(C_word c,C_word *av) C_noret;
C_noret_decl(f_10331)
static void C_ccall f_10331(C_word c,C_word *av) C_noret;
C_noret_decl(f_10338)
static void C_ccall f_10338(C_word c,C_word *av) C_noret;
C_noret_decl(f_10362)
static void C_ccall f_10362(C_word c,C_word *av) C_noret;
C_noret_decl(f_10374)
static void C_ccall f_10374(C_word c,C_word *av) C_noret;
C_noret_decl(f_10378)
static void C_ccall f_10378(C_word c,C_word *av) C_noret;
C_noret_decl(f_10390)
static void C_ccall f_10390(C_word c,C_word *av) C_noret;
C_noret_decl(f_10400)
static void C_fcall f_10400(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_10448)
static void C_fcall f_10448(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_10506)
static void C_ccall f_10506(C_word c,C_word *av) C_noret;
C_noret_decl(f_10521)
static void C_ccall f_10521(C_word c,C_word *av) C_noret;
C_noret_decl(f_10525)
static void C_ccall f_10525(C_word c,C_word *av) C_noret;
C_noret_decl(f_10537)
static void C_ccall f_10537(C_word c,C_word *av) C_noret;
C_noret_decl(f_10547)
static void C_fcall f_10547(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_10595)
static void C_fcall f_10595(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_10651)
static void C_ccall f_10651(C_word c,C_word *av) C_noret;
C_noret_decl(f_10660)
static void C_ccall f_10660(C_word c,C_word *av) C_noret;
C_noret_decl(f_10667)
static void C_ccall f_10667(C_word c,C_word *av) C_noret;
C_noret_decl(f_10676)
static void C_ccall f_10676(C_word c,C_word *av) C_noret;
C_noret_decl(f_10678)
static void C_fcall f_10678(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_10728)
static void C_fcall f_10728(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10742)
static void C_ccall f_10742(C_word c,C_word *av) C_noret;
C_noret_decl(f_10748)
static void C_fcall f_10748(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_10796)
static void C_fcall f_10796(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10830)
static void C_fcall f_10830(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10855)
static void C_ccall f_10855(C_word c,C_word *av) C_noret;
C_noret_decl(f_10864)
static void C_fcall f_10864(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10889)
static void C_ccall f_10889(C_word c,C_word *av) C_noret;
C_noret_decl(f_10898)
static void C_fcall f_10898(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10934)
static void C_ccall f_10934(C_word c,C_word *av) C_noret;
C_noret_decl(f_10936)
static void C_ccall f_10936(C_word c,C_word *av) C_noret;
C_noret_decl(f_10940)
static void C_ccall f_10940(C_word c,C_word *av) C_noret;
C_noret_decl(f_10951)
static void C_ccall f_10951(C_word c,C_word *av) C_noret;
C_noret_decl(f_10953)
static void C_ccall f_10953(C_word c,C_word *av) C_noret;
C_noret_decl(f_10957)
static void C_ccall f_10957(C_word c,C_word *av) C_noret;
C_noret_decl(f_10968)
static void C_ccall f_10968(C_word c,C_word *av) C_noret;
C_noret_decl(f_10970)
static void C_ccall f_10970(C_word c,C_word *av) C_noret;
C_noret_decl(f_10974)
static void C_ccall f_10974(C_word c,C_word *av) C_noret;
C_noret_decl(f_10989)
static void C_ccall f_10989(C_word c,C_word *av) C_noret;
C_noret_decl(f_10991)
static void C_ccall f_10991(C_word c,C_word *av) C_noret;
C_noret_decl(f_11003)
static void C_ccall f_11003(C_word c,C_word *av) C_noret;
C_noret_decl(f_11005)
static void C_ccall f_11005(C_word c,C_word *av) C_noret;
C_noret_decl(f_11009)
static void C_ccall f_11009(C_word c,C_word *av) C_noret;
C_noret_decl(f_11031)
static void C_ccall f_11031(C_word c,C_word *av) C_noret;
C_noret_decl(f_11096)
static void C_ccall f_11096(C_word c,C_word *av) C_noret;
C_noret_decl(f_11098)
static void C_ccall f_11098(C_word c,C_word *av) C_noret;
C_noret_decl(f_11102)
static void C_ccall f_11102(C_word c,C_word *av) C_noret;
C_noret_decl(f_11108)
static void C_ccall f_11108(C_word c,C_word *av) C_noret;
C_noret_decl(f_11111)
static void C_ccall f_11111(C_word c,C_word *av) C_noret;
C_noret_decl(f_11114)
static void C_ccall f_11114(C_word c,C_word *av) C_noret;
C_noret_decl(f_11120)
static void C_ccall f_11120(C_word c,C_word *av) C_noret;
C_noret_decl(f_11123)
static void C_ccall f_11123(C_word c,C_word *av) C_noret;
C_noret_decl(f_11126)
static void C_ccall f_11126(C_word c,C_word *av) C_noret;
C_noret_decl(f_11130)
static void C_fcall f_11130(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11143)
static void C_fcall f_11143(C_word t0,C_word t1) C_noret;
C_noret_decl(f_11158)
static void C_ccall f_11158(C_word c,C_word *av) C_noret;
C_noret_decl(f_11190)
static void C_ccall f_11190(C_word c,C_word *av) C_noret;
C_noret_decl(f_11221)
static void C_ccall f_11221(C_word c,C_word *av) C_noret;
C_noret_decl(f_11223)
static void C_fcall f_11223(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_11239)
static void C_ccall f_11239(C_word c,C_word *av) C_noret;
C_noret_decl(f_11242)
static void C_ccall f_11242(C_word c,C_word *av) C_noret;
C_noret_decl(f_11245)
static void C_ccall f_11245(C_word c,C_word *av) C_noret;
C_noret_decl(f_11259)
static void C_ccall f_11259(C_word c,C_word *av) C_noret;
C_noret_decl(f_11271)
static void C_ccall f_11271(C_word c,C_word *av) C_noret;
C_noret_decl(f_11275)
static void C_fcall f_11275(C_word t0,C_word t1) C_noret;
C_noret_decl(f_11368)
static void C_ccall f_11368(C_word c,C_word *av) C_noret;
C_noret_decl(f_11372)
static void C_ccall f_11372(C_word c,C_word *av) C_noret;
C_noret_decl(f_11387)
static void C_ccall f_11387(C_word c,C_word *av) C_noret;
C_noret_decl(f_11407)
static void C_ccall f_11407(C_word c,C_word *av) C_noret;
C_noret_decl(f_11413)
static void C_ccall f_11413(C_word c,C_word *av) C_noret;
C_noret_decl(f_11433)
static void C_ccall f_11433(C_word c,C_word *av) C_noret;
C_noret_decl(f_11441)
static void C_fcall f_11441(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11466)
static void C_ccall f_11466(C_word c,C_word *av) C_noret;
C_noret_decl(f_11476)
static void C_ccall f_11476(C_word c,C_word *av) C_noret;
C_noret_decl(f_11483)
static void C_ccall f_11483(C_word c,C_word *av) C_noret;
C_noret_decl(f_11487)
static void C_ccall f_11487(C_word c,C_word *av) C_noret;
C_noret_decl(f_11491)
static void C_ccall f_11491(C_word c,C_word *av) C_noret;
C_noret_decl(f_11493)
static void C_ccall f_11493(C_word c,C_word *av) C_noret;
C_noret_decl(f_11497)
static void C_ccall f_11497(C_word c,C_word *av) C_noret;
C_noret_decl(f_11508)
static void C_ccall f_11508(C_word c,C_word *av) C_noret;
C_noret_decl(f_11510)
static void C_ccall f_11510(C_word c,C_word *av) C_noret;
C_noret_decl(f_11520)
static void C_ccall f_11520(C_word c,C_word *av) C_noret;
C_noret_decl(f_11533)
static void C_ccall f_11533(C_word c,C_word *av) C_noret;
C_noret_decl(f_11549)
static void C_ccall f_11549(C_word c,C_word *av) C_noret;
C_noret_decl(f_11555)
static void C_ccall f_11555(C_word c,C_word *av) C_noret;
C_noret_decl(f_11558)
static void C_ccall f_11558(C_word c,C_word *av) C_noret;
C_noret_decl(f_11564)
static void C_ccall f_11564(C_word c,C_word *av) C_noret;
C_noret_decl(f_11573)
static void C_fcall f_11573(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_11580)
static void C_ccall f_11580(C_word c,C_word *av) C_noret;
C_noret_decl(f_11594)
static void C_ccall f_11594(C_word c,C_word *av) C_noret;
C_noret_decl(f_11605)
static void C_ccall f_11605(C_word c,C_word *av) C_noret;
C_noret_decl(f_11608)
static void C_ccall f_11608(C_word c,C_word *av) C_noret;
C_noret_decl(f_11614)
static void C_ccall f_11614(C_word c,C_word *av) C_noret;
C_noret_decl(f_11623)
static void C_fcall f_11623(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_11630)
static void C_ccall f_11630(C_word c,C_word *av) C_noret;
C_noret_decl(f_11644)
static void C_ccall f_11644(C_word c,C_word *av) C_noret;
C_noret_decl(f_11656)
static void C_ccall f_11656(C_word c,C_word *av) C_noret;
C_noret_decl(f_11658)
static void C_ccall f_11658(C_word c,C_word *av) C_noret;
C_noret_decl(f_11678)
static void C_ccall f_11678(C_word c,C_word *av) C_noret;
C_noret_decl(f_11692)
static void C_fcall f_11692(C_word t0,C_word t1) C_noret;
C_noret_decl(f_11705)
static void C_ccall f_11705(C_word c,C_word *av) C_noret;
C_noret_decl(f_11753)
static void C_ccall f_11753(C_word c,C_word *av) C_noret;
C_noret_decl(f_11755)
static void C_ccall f_11755(C_word c,C_word *av) C_noret;
C_noret_decl(f_11759)
static void C_ccall f_11759(C_word c,C_word *av) C_noret;
C_noret_decl(f_11766)
static void C_ccall f_11766(C_word c,C_word *av) C_noret;
C_noret_decl(f_11774)
static void C_ccall f_11774(C_word c,C_word *av) C_noret;
C_noret_decl(f_11782)
static void C_ccall f_11782(C_word c,C_word *av) C_noret;
C_noret_decl(f_11784)
static void C_ccall f_11784(C_word c,C_word *av) C_noret;
C_noret_decl(f_11788)
static void C_ccall f_11788(C_word c,C_word *av) C_noret;
C_noret_decl(f_11791)
static void C_ccall f_11791(C_word c,C_word *av) C_noret;
C_noret_decl(f_11812)
static void C_ccall f_11812(C_word c,C_word *av) C_noret;
C_noret_decl(f_11814)
static void C_ccall f_11814(C_word c,C_word *av) C_noret;
C_noret_decl(f_11818)
static void C_ccall f_11818(C_word c,C_word *av) C_noret;
C_noret_decl(f_11829)
static void C_ccall f_11829(C_word c,C_word *av) C_noret;
C_noret_decl(f_11831)
static void C_ccall f_11831(C_word c,C_word *av) C_noret;
C_noret_decl(f_11835)
static void C_ccall f_11835(C_word c,C_word *av) C_noret;
C_noret_decl(f_11844)
static void C_ccall f_11844(C_word c,C_word *av) C_noret;
C_noret_decl(f_11847)
static void C_ccall f_11847(C_word c,C_word *av) C_noret;
C_noret_decl(f_11850)
static void C_ccall f_11850(C_word c,C_word *av) C_noret;
C_noret_decl(f_11873)
static void C_ccall f_11873(C_word c,C_word *av) C_noret;
C_noret_decl(f_11889)
static void C_ccall f_11889(C_word c,C_word *av) C_noret;
C_noret_decl(f_11891)
static void C_ccall f_11891(C_word c,C_word *av) C_noret;
C_noret_decl(f_11895)
static void C_ccall f_11895(C_word c,C_word *av) C_noret;
C_noret_decl(f_11901)
static void C_ccall f_11901(C_word c,C_word *av) C_noret;
C_noret_decl(f_11904)
static void C_ccall f_11904(C_word c,C_word *av) C_noret;
C_noret_decl(f_11928)
static void C_fcall f_11928(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11932)
static void C_ccall f_11932(C_word c,C_word *av) C_noret;
C_noret_decl(f_11939)
static void C_ccall f_11939(C_word c,C_word *av) C_noret;
C_noret_decl(f_11966)
static void C_ccall f_11966(C_word c,C_word *av) C_noret;
C_noret_decl(f_11968)
static void C_fcall f_11968(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11993)
static void C_ccall f_11993(C_word c,C_word *av) C_noret;
C_noret_decl(f_12012)
static void C_ccall f_12012(C_word c,C_word *av) C_noret;
C_noret_decl(f_12014)
static void C_ccall f_12014(C_word c,C_word *av) C_noret;
C_noret_decl(f_12024)
static void C_ccall f_12024(C_word c,C_word *av) C_noret;
C_noret_decl(f_12033)
static void C_ccall f_12033(C_word c,C_word *av) C_noret;
C_noret_decl(f_12037)
static void C_ccall f_12037(C_word c,C_word *av) C_noret;
C_noret_decl(f_12040)
static void C_ccall f_12040(C_word c,C_word *av) C_noret;
C_noret_decl(f_12043)
static void C_ccall f_12043(C_word c,C_word *av) C_noret;
C_noret_decl(f_12046)
static void C_ccall f_12046(C_word c,C_word *av) C_noret;
C_noret_decl(f_12054)
static void C_fcall f_12054(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_12064)
static void C_ccall f_12064(C_word c,C_word *av) C_noret;
C_noret_decl(f_12067)
static void C_ccall f_12067(C_word c,C_word *av) C_noret;
C_noret_decl(f_12073)
static void C_ccall f_12073(C_word c,C_word *av) C_noret;
C_noret_decl(f_12108)
static void C_ccall f_12108(C_word c,C_word *av) C_noret;
C_noret_decl(f_12110)
static void C_fcall f_12110(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_12168)
static void C_ccall f_12168(C_word c,C_word *av) C_noret;
C_noret_decl(f_12172)
static void C_ccall f_12172(C_word c,C_word *av) C_noret;
C_noret_decl(f_12205)
static void C_ccall f_12205(C_word c,C_word *av) C_noret;
C_noret_decl(f_12207)
static void C_fcall f_12207(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_12232)
static void C_ccall f_12232(C_word c,C_word *av) C_noret;
C_noret_decl(f_12249)
static void C_ccall f_12249(C_word c,C_word *av) C_noret;
C_noret_decl(f_12251)
static void C_fcall f_12251(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_12269)
static void C_ccall f_12269(C_word c,C_word *av) C_noret;
C_noret_decl(f_12305)
static void C_fcall f_12305(C_word t0,C_word t1) C_noret;
C_noret_decl(f_12322)
static void C_ccall f_12322(C_word c,C_word *av) C_noret;
C_noret_decl(f_12379)
static void C_ccall f_12379(C_word c,C_word *av) C_noret;
C_noret_decl(f_12381)
static void C_ccall f_12381(C_word c,C_word *av) C_noret;
C_noret_decl(f_12391)
static void C_ccall f_12391(C_word c,C_word *av) C_noret;
C_noret_decl(f_12408)
static void C_ccall f_12408(C_word c,C_word *av) C_noret;
C_noret_decl(f_12429)
static void C_ccall f_12429(C_word c,C_word *av) C_noret;
C_noret_decl(f_12439)
static void C_ccall f_12439(C_word c,C_word *av) C_noret;
C_noret_decl(f_12447)
static void C_ccall f_12447(C_word c,C_word *av) C_noret;
C_noret_decl(f_12449)
static void C_ccall f_12449(C_word c,C_word *av) C_noret;
C_noret_decl(f_12465)
static void C_ccall f_12465(C_word c,C_word *av) C_noret;
C_noret_decl(f_12475)
static void C_fcall f_12475(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_12509)
static void C_fcall f_12509(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_12553)
static void C_ccall f_12553(C_word c,C_word *av) C_noret;
C_noret_decl(f_12559)
static void C_ccall f_12559(C_word c,C_word *av) C_noret;
C_noret_decl(f_12562)
static void C_ccall f_12562(C_word c,C_word *av) C_noret;
C_noret_decl(f_12568)
static void C_ccall f_12568(C_word c,C_word *av) C_noret;
C_noret_decl(f_12577)
static void C_fcall f_12577(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_12584)
static void C_ccall f_12584(C_word c,C_word *av) C_noret;
C_noret_decl(f_12598)
static void C_ccall f_12598(C_word c,C_word *av) C_noret;
C_noret_decl(f_12609)
static void C_ccall f_12609(C_word c,C_word *av) C_noret;
C_noret_decl(f_12612)
static void C_ccall f_12612(C_word c,C_word *av) C_noret;
C_noret_decl(f_12618)
static void C_ccall f_12618(C_word c,C_word *av) C_noret;
C_noret_decl(f_12627)
static void C_fcall f_12627(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_12634)
static void C_ccall f_12634(C_word c,C_word *av) C_noret;
C_noret_decl(f_12646)
static void C_fcall f_12646(C_word t0,C_word t1) C_noret;
C_noret_decl(f_12657)
static void C_ccall f_12657(C_word c,C_word *av) C_noret;
C_noret_decl(f_12688)
static void C_ccall f_12688(C_word c,C_word *av) C_noret;
C_noret_decl(f_12690)
static void C_ccall f_12690(C_word c,C_word *av) C_noret;
C_noret_decl(f_12694)
static void C_ccall f_12694(C_word c,C_word *av) C_noret;
C_noret_decl(f_12710)
static void C_ccall f_12710(C_word c,C_word *av) C_noret;
C_noret_decl(f_12726)
static void C_ccall f_12726(C_word c,C_word *av) C_noret;
C_noret_decl(f_12728)
static void C_ccall f_12728(C_word c,C_word *av) C_noret;
C_noret_decl(f_12732)
static void C_ccall f_12732(C_word c,C_word *av) C_noret;
C_noret_decl(f_12741)
static void C_ccall f_12741(C_word c,C_word *av) C_noret;
C_noret_decl(f_12748)
static void C_ccall f_12748(C_word c,C_word *av) C_noret;
C_noret_decl(f_12756)
static void C_ccall f_12756(C_word c,C_word *av) C_noret;
C_noret_decl(f_12758)
static void C_ccall f_12758(C_word c,C_word *av) C_noret;
C_noret_decl(f_12783)
static void C_ccall f_12783(C_word c,C_word *av) C_noret;
C_noret_decl(f_12787)
static void C_fcall f_12787(C_word t0,C_word t1) C_noret;
C_noret_decl(f_12837)
static void C_ccall f_12837(C_word c,C_word *av) C_noret;
C_noret_decl(f_12839)
static void C_ccall f_12839(C_word c,C_word *av) C_noret;
C_noret_decl(f_12843)
static void C_ccall f_12843(C_word c,C_word *av) C_noret;
C_noret_decl(f_12846)
static void C_ccall f_12846(C_word c,C_word *av) C_noret;
C_noret_decl(f_12849)
static void C_ccall f_12849(C_word c,C_word *av) C_noret;
C_noret_decl(f_12852)
static void C_ccall f_12852(C_word c,C_word *av) C_noret;
C_noret_decl(f_12855)
static void C_ccall f_12855(C_word c,C_word *av) C_noret;
C_noret_decl(f_12858)
static void C_ccall f_12858(C_word c,C_word *av) C_noret;
C_noret_decl(f_12860)
static void C_fcall f_12860(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_12870)
static void C_fcall f_12870(C_word t0,C_word t1) C_noret;
C_noret_decl(f_12895)
static C_word C_fcall f_12895(C_word *a,C_word t0,C_word t1);
C_noret_decl(f_12909)
static void C_ccall f_12909(C_word c,C_word *av) C_noret;
C_noret_decl(f_12911)
static void C_fcall f_12911(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_12990)
static void C_ccall f_12990(C_word c,C_word *av) C_noret;
C_noret_decl(f_13015)
static void C_ccall f_13015(C_word c,C_word *av) C_noret;
C_noret_decl(f_13022)
static void C_ccall f_13022(C_word c,C_word *av) C_noret;
C_noret_decl(f_13026)
static void C_ccall f_13026(C_word c,C_word *av) C_noret;
C_noret_decl(f_13046)
static void C_fcall f_13046(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_13071)
static void C_ccall f_13071(C_word c,C_word *av) C_noret;
C_noret_decl(f_13110)
static void C_ccall f_13110(C_word c,C_word *av) C_noret;
C_noret_decl(f_13112)
static void C_ccall f_13112(C_word c,C_word *av) C_noret;
C_noret_decl(f_13116)
static void C_ccall f_13116(C_word c,C_word *av) C_noret;
C_noret_decl(f_13119)
static void C_ccall f_13119(C_word c,C_word *av) C_noret;
C_noret_decl(f_13122)
static void C_ccall f_13122(C_word c,C_word *av) C_noret;
C_noret_decl(f_13133)
static void C_ccall f_13133(C_word c,C_word *av) C_noret;
C_noret_decl(f_4218)
static void C_ccall f_4218(C_word c,C_word *av) C_noret;
C_noret_decl(f_4221)
static void C_ccall f_4221(C_word c,C_word *av) C_noret;
C_noret_decl(f_4224)
static void C_ccall f_4224(C_word c,C_word *av) C_noret;
C_noret_decl(f_4334)
static void C_fcall f_4334(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4352)
static void C_ccall f_4352(C_word c,C_word *av) C_noret;
C_noret_decl(f_4392)
static void C_fcall f_4392(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_4406)
static void C_ccall f_4406(C_word c,C_word *av) C_noret;
C_noret_decl(f_5506)
static void C_ccall f_5506(C_word c,C_word *av) C_noret;
C_noret_decl(f_5509)
static void C_ccall f_5509(C_word c,C_word *av) C_noret;
C_noret_decl(f_5512)
static void C_ccall f_5512(C_word c,C_word *av) C_noret;
C_noret_decl(f_5515)
static void C_ccall f_5515(C_word c,C_word *av) C_noret;
C_noret_decl(f_5519)
static void C_ccall f_5519(C_word c,C_word *av) C_noret;
C_noret_decl(f_5522)
static void C_ccall f_5522(C_word c,C_word *av) C_noret;
C_noret_decl(f_5525)
static void C_ccall f_5525(C_word c,C_word *av) C_noret;
C_noret_decl(f_5528)
static void C_ccall f_5528(C_word c,C_word *av) C_noret;
C_noret_decl(f_5531)
static void C_ccall f_5531(C_word c,C_word *av) C_noret;
C_noret_decl(f_5534)
static void C_ccall f_5534(C_word c,C_word *av) C_noret;
C_noret_decl(f_5537)
static void C_ccall f_5537(C_word c,C_word *av) C_noret;
C_noret_decl(f_5540)
static void C_ccall f_5540(C_word c,C_word *av) C_noret;
C_noret_decl(f_5544)
static void C_ccall f_5544(C_word c,C_word *av) C_noret;
C_noret_decl(f_5547)
static void C_ccall f_5547(C_word c,C_word *av) C_noret;
C_noret_decl(f_5550)
static void C_ccall f_5550(C_word c,C_word *av) C_noret;
C_noret_decl(f_5553)
static void C_ccall f_5553(C_word c,C_word *av) C_noret;
C_noret_decl(f_5556)
static void C_ccall f_5556(C_word c,C_word *av) C_noret;
C_noret_decl(f_5559)
static void C_ccall f_5559(C_word c,C_word *av) C_noret;
C_noret_decl(f_5562)
static void C_ccall f_5562(C_word c,C_word *av) C_noret;
C_noret_decl(f_5566)
static void C_ccall f_5566(C_word c,C_word *av) C_noret;
C_noret_decl(f_5569)
static void C_ccall f_5569(C_word c,C_word *av) C_noret;
C_noret_decl(f_5572)
static void C_ccall f_5572(C_word c,C_word *av) C_noret;
C_noret_decl(f_5575)
static void C_ccall f_5575(C_word c,C_word *av) C_noret;
C_noret_decl(f_5578)
static void C_ccall f_5578(C_word c,C_word *av) C_noret;
C_noret_decl(f_5581)
static void C_ccall f_5581(C_word c,C_word *av) C_noret;
C_noret_decl(f_5584)
static void C_ccall f_5584(C_word c,C_word *av) C_noret;
C_noret_decl(f_5587)
static void C_ccall f_5587(C_word c,C_word *av) C_noret;
C_noret_decl(f_5590)
static void C_ccall f_5590(C_word c,C_word *av) C_noret;
C_noret_decl(f_5593)
static void C_ccall f_5593(C_word c,C_word *av) C_noret;
C_noret_decl(f_5596)
static void C_ccall f_5596(C_word c,C_word *av) C_noret;
C_noret_decl(f_5599)
static void C_ccall f_5599(C_word c,C_word *av) C_noret;
C_noret_decl(f_5602)
static void C_ccall f_5602(C_word c,C_word *av) C_noret;
C_noret_decl(f_5605)
static void C_ccall f_5605(C_word c,C_word *av) C_noret;
C_noret_decl(f_5609)
static void C_ccall f_5609(C_word c,C_word *av) C_noret;
C_noret_decl(f_5612)
static void C_ccall f_5612(C_word c,C_word *av) C_noret;
C_noret_decl(f_5615)
static void C_ccall f_5615(C_word c,C_word *av) C_noret;
C_noret_decl(f_5618)
static void C_ccall f_5618(C_word c,C_word *av) C_noret;
C_noret_decl(f_5621)
static void C_ccall f_5621(C_word c,C_word *av) C_noret;
C_noret_decl(f_5624)
static void C_ccall f_5624(C_word c,C_word *av) C_noret;
C_noret_decl(f_5627)
static void C_ccall f_5627(C_word c,C_word *av) C_noret;
C_noret_decl(f_5630)
static void C_ccall f_5630(C_word c,C_word *av) C_noret;
C_noret_decl(f_5633)
static void C_ccall f_5633(C_word c,C_word *av) C_noret;
C_noret_decl(f_5636)
static void C_ccall f_5636(C_word c,C_word *av) C_noret;
C_noret_decl(f_5639)
static void C_ccall f_5639(C_word c,C_word *av) C_noret;
C_noret_decl(f_5642)
static void C_ccall f_5642(C_word c,C_word *av) C_noret;
C_noret_decl(f_5645)
static void C_ccall f_5645(C_word c,C_word *av) C_noret;
C_noret_decl(f_5648)
static void C_ccall f_5648(C_word c,C_word *av) C_noret;
C_noret_decl(f_5651)
static void C_ccall f_5651(C_word c,C_word *av) C_noret;
C_noret_decl(f_5654)
static void C_ccall f_5654(C_word c,C_word *av) C_noret;
C_noret_decl(f_5657)
static void C_ccall f_5657(C_word c,C_word *av) C_noret;
C_noret_decl(f_5660)
static void C_ccall f_5660(C_word c,C_word *av) C_noret;
C_noret_decl(f_5663)
static void C_ccall f_5663(C_word c,C_word *av) C_noret;
C_noret_decl(f_5666)
static void C_ccall f_5666(C_word c,C_word *av) C_noret;
C_noret_decl(f_5670)
static void C_ccall f_5670(C_word c,C_word *av) C_noret;
C_noret_decl(f_5673)
static void C_ccall f_5673(C_word c,C_word *av) C_noret;
C_noret_decl(f_5676)
static void C_ccall f_5676(C_word c,C_word *av) C_noret;
C_noret_decl(f_5680)
static void C_ccall f_5680(C_word c,C_word *av) C_noret;
C_noret_decl(f_5683)
static void C_ccall f_5683(C_word c,C_word *av) C_noret;
C_noret_decl(f_5686)
static void C_ccall f_5686(C_word c,C_word *av) C_noret;
C_noret_decl(f_5690)
static void C_ccall f_5690(C_word c,C_word *av) C_noret;
C_noret_decl(f_5694)
static void C_ccall f_5694(C_word c,C_word *av) C_noret;
C_noret_decl(f_5696)
static void C_ccall f_5696(C_word c,C_word *av) C_noret;
C_noret_decl(f_5700)
static void C_ccall f_5700(C_word c,C_word *av) C_noret;
C_noret_decl(f_5743)
static void C_ccall f_5743(C_word c,C_word *av) C_noret;
C_noret_decl(f_5745)
static void C_ccall f_5745(C_word c,C_word *av) C_noret;
C_noret_decl(f_5749)
static void C_ccall f_5749(C_word c,C_word *av) C_noret;
C_noret_decl(f_5760)
static void C_ccall f_5760(C_word c,C_word *av) C_noret;
C_noret_decl(f_5763)
static void C_fcall f_5763(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5786)
static void C_fcall f_5786(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5805)
static void C_ccall f_5805(C_word c,C_word *av) C_noret;
C_noret_decl(f_5815)
static void C_ccall f_5815(C_word c,C_word *av) C_noret;
C_noret_decl(f_5822)
static void C_ccall f_5822(C_word c,C_word *av) C_noret;
C_noret_decl(f_5832)
static void C_ccall f_5832(C_word c,C_word *av) C_noret;
C_noret_decl(f_5834)
static void C_ccall f_5834(C_word c,C_word *av) C_noret;
C_noret_decl(f_5842)
static void C_ccall f_5842(C_word c,C_word *av) C_noret;
C_noret_decl(f_5850)
static void C_ccall f_5850(C_word c,C_word *av) C_noret;
C_noret_decl(f_5852)
static void C_ccall f_5852(C_word c,C_word *av) C_noret;
C_noret_decl(f_5856)
static void C_ccall f_5856(C_word c,C_word *av) C_noret;
C_noret_decl(f_5915)
static void C_ccall f_5915(C_word c,C_word *av) C_noret;
C_noret_decl(f_5917)
static void C_ccall f_5917(C_word c,C_word *av) C_noret;
C_noret_decl(f_5921)
static void C_ccall f_5921(C_word c,C_word *av) C_noret;
C_noret_decl(f_5924)
static void C_ccall f_5924(C_word c,C_word *av) C_noret;
C_noret_decl(f_5927)
static void C_ccall f_5927(C_word c,C_word *av) C_noret;
C_noret_decl(f_5930)
static void C_ccall f_5930(C_word c,C_word *av) C_noret;
C_noret_decl(f_5937)
static void C_fcall f_5937(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6) C_noret;
C_noret_decl(f_5947)
static void C_ccall f_5947(C_word c,C_word *av) C_noret;
C_noret_decl(f_5950)
static void C_ccall f_5950(C_word c,C_word *av) C_noret;
C_noret_decl(f_5956)
static void C_ccall f_5956(C_word c,C_word *av) C_noret;
C_noret_decl(f_5967)
static void C_ccall f_5967(C_word c,C_word *av) C_noret;
C_noret_decl(f_5983)
static void C_ccall f_5983(C_word c,C_word *av) C_noret;
C_noret_decl(f_5993)
static void C_ccall f_5993(C_word c,C_word *av) C_noret;
C_noret_decl(f_6016)
static void C_ccall f_6016(C_word c,C_word *av) C_noret;
C_noret_decl(f_6019)
static void C_ccall f_6019(C_word c,C_word *av) C_noret;
C_noret_decl(f_6036)
static void C_ccall f_6036(C_word c,C_word *av) C_noret;
C_noret_decl(f_6042)
static void C_ccall f_6042(C_word c,C_word *av) C_noret;
C_noret_decl(f_6059)
static void C_ccall f_6059(C_word c,C_word *av) C_noret;
C_noret_decl(f_6082)
static void C_ccall f_6082(C_word c,C_word *av) C_noret;
C_noret_decl(f_6110)
static void C_ccall f_6110(C_word c,C_word *av) C_noret;
C_noret_decl(f_6112)
static void C_ccall f_6112(C_word c,C_word *av) C_noret;
C_noret_decl(f_6116)
static void C_ccall f_6116(C_word c,C_word *av) C_noret;
C_noret_decl(f_6119)
static void C_ccall f_6119(C_word c,C_word *av) C_noret;
C_noret_decl(f_6122)
static void C_ccall f_6122(C_word c,C_word *av) C_noret;
C_noret_decl(f_6125)
static void C_ccall f_6125(C_word c,C_word *av) C_noret;
C_noret_decl(f_6132)
static void C_fcall f_6132(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5) C_noret;
C_noret_decl(f_6142)
static void C_ccall f_6142(C_word c,C_word *av) C_noret;
C_noret_decl(f_6145)
static void C_ccall f_6145(C_word c,C_word *av) C_noret;
C_noret_decl(f_6151)
static void C_ccall f_6151(C_word c,C_word *av) C_noret;
C_noret_decl(f_6158)
static void C_ccall f_6158(C_word c,C_word *av) C_noret;
C_noret_decl(f_6174)
static void C_ccall f_6174(C_word c,C_word *av) C_noret;
C_noret_decl(f_6184)
static void C_ccall f_6184(C_word c,C_word *av) C_noret;
C_noret_decl(f_6207)
static void C_ccall f_6207(C_word c,C_word *av) C_noret;
C_noret_decl(f_6210)
static void C_ccall f_6210(C_word c,C_word *av) C_noret;
C_noret_decl(f_6227)
static void C_ccall f_6227(C_word c,C_word *av) C_noret;
C_noret_decl(f_6233)
static void C_ccall f_6233(C_word c,C_word *av) C_noret;
C_noret_decl(f_6282)
static void C_ccall f_6282(C_word c,C_word *av) C_noret;
C_noret_decl(f_6284)
static void C_ccall f_6284(C_word c,C_word *av) C_noret;
C_noret_decl(f_6288)
static void C_ccall f_6288(C_word c,C_word *av) C_noret;
C_noret_decl(f_6294)
static void C_ccall f_6294(C_word c,C_word *av) C_noret;
C_noret_decl(f_6297)
static void C_ccall f_6297(C_word c,C_word *av) C_noret;
C_noret_decl(f_6309)
static void C_ccall f_6309(C_word c,C_word *av) C_noret;
C_noret_decl(f_6312)
static void C_ccall f_6312(C_word c,C_word *av) C_noret;
C_noret_decl(f_6318)
static void C_ccall f_6318(C_word c,C_word *av) C_noret;
C_noret_decl(f_6321)
static void C_ccall f_6321(C_word c,C_word *av) C_noret;
C_noret_decl(f_6330)
static void C_ccall f_6330(C_word c,C_word *av) C_noret;
C_noret_decl(f_6331)
static void C_fcall f_6331(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6347)
static void C_ccall f_6347(C_word c,C_word *av) C_noret;
C_noret_decl(f_6378)
static void C_ccall f_6378(C_word c,C_word *av) C_noret;
C_noret_decl(f_6380)
static void C_fcall f_6380(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_6399)
static void C_fcall f_6399(C_word t0,C_word t1) C_noret;
C_noret_decl(f_6405)
static void C_fcall f_6405(C_word t0,C_word t1) C_noret;
C_noret_decl(f_6412)
static void C_fcall f_6412(C_word t0,C_word t1) C_noret;
C_noret_decl(f_6416)
static void C_ccall f_6416(C_word c,C_word *av) C_noret;
C_noret_decl(f_6420)
static void C_fcall f_6420(C_word t0,C_word t1) C_noret;
C_noret_decl(f_6424)
static void C_ccall f_6424(C_word c,C_word *av) C_noret;
C_noret_decl(f_6452)
static void C_ccall f_6452(C_word c,C_word *av) C_noret;
C_noret_decl(f_6473)
static void C_ccall f_6473(C_word c,C_word *av) C_noret;
C_noret_decl(f_6552)
static void C_ccall f_6552(C_word c,C_word *av) C_noret;
C_noret_decl(f_6595)
static C_word C_fcall f_6595(C_word t0,C_word t1);
C_noret_decl(f_6608)
static void C_ccall f_6608(C_word c,C_word *av) C_noret;
C_noret_decl(f_6610)
static void C_fcall f_6610(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6648)
static void C_fcall f_6648(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6658)
static void C_ccall f_6658(C_word c,C_word *av) C_noret;
C_noret_decl(f_6671)
static void C_fcall f_6671(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6706)
static void C_ccall f_6706(C_word c,C_word *av) C_noret;
C_noret_decl(f_6713)
static void C_ccall f_6713(C_word c,C_word *av) C_noret;
C_noret_decl(f_6717)
static void C_ccall f_6717(C_word c,C_word *av) C_noret;
C_noret_decl(f_6721)
static void C_ccall f_6721(C_word c,C_word *av) C_noret;
C_noret_decl(f_6723)
static void C_ccall f_6723(C_word c,C_word *av) C_noret;
C_noret_decl(f_6727)
static void C_ccall f_6727(C_word c,C_word *av) C_noret;
C_noret_decl(f_6741)
static void C_ccall f_6741(C_word c,C_word *av) C_noret;
C_noret_decl(f_6744)
static void C_ccall f_6744(C_word c,C_word *av) C_noret;
C_noret_decl(f_6747)
static void C_ccall f_6747(C_word c,C_word *av) C_noret;
C_noret_decl(f_6769)
static void C_ccall f_6769(C_word c,C_word *av) C_noret;
C_noret_decl(f_6776)
static void C_ccall f_6776(C_word c,C_word *av) C_noret;
C_noret_decl(f_6780)
static void C_ccall f_6780(C_word c,C_word *av) C_noret;
C_noret_decl(f_6791)
static void C_ccall f_6791(C_word c,C_word *av) C_noret;
C_noret_decl(f_6794)
static void C_ccall f_6794(C_word c,C_word *av) C_noret;
C_noret_decl(f_6797)
static void C_ccall f_6797(C_word c,C_word *av) C_noret;
C_noret_decl(f_6811)
static void C_ccall f_6811(C_word c,C_word *av) C_noret;
C_noret_decl(f_6818)
static void C_ccall f_6818(C_word c,C_word *av) C_noret;
C_noret_decl(f_6822)
static void C_ccall f_6822(C_word c,C_word *av) C_noret;
C_noret_decl(f_6834)
static void C_ccall f_6834(C_word c,C_word *av) C_noret;
C_noret_decl(f_6836)
static void C_ccall f_6836(C_word c,C_word *av) C_noret;
C_noret_decl(f_6840)
static void C_ccall f_6840(C_word c,C_word *av) C_noret;
C_noret_decl(f_6842)
static void C_fcall f_6842(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6848)
static void C_fcall f_6848(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6862)
static void C_ccall f_6862(C_word c,C_word *av) C_noret;
C_noret_decl(f_6866)
static void C_ccall f_6866(C_word c,C_word *av) C_noret;
C_noret_decl(f_6874)
static void C_ccall f_6874(C_word c,C_word *av) C_noret;
C_noret_decl(f_6877)
static void C_ccall f_6877(C_word c,C_word *av) C_noret;
C_noret_decl(f_6880)
static void C_ccall f_6880(C_word c,C_word *av) C_noret;
C_noret_decl(f_6883)
static void C_ccall f_6883(C_word c,C_word *av) C_noret;
C_noret_decl(f_6886)
static void C_ccall f_6886(C_word c,C_word *av) C_noret;
C_noret_decl(f_6889)
static void C_ccall f_6889(C_word c,C_word *av) C_noret;
C_noret_decl(f_6892)
static void C_ccall f_6892(C_word c,C_word *av) C_noret;
C_noret_decl(f_6895)
static void C_ccall f_6895(C_word c,C_word *av) C_noret;
C_noret_decl(f_6898)
static void C_ccall f_6898(C_word c,C_word *av) C_noret;
C_noret_decl(f_6901)
static void C_ccall f_6901(C_word c,C_word *av) C_noret;
C_noret_decl(f_6908)
static void C_ccall f_6908(C_word c,C_word *av) C_noret;
C_noret_decl(f_6924)
static void C_ccall f_6924(C_word c,C_word *av) C_noret;
C_noret_decl(f_6926)
static void C_fcall f_6926(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6934)
static void C_fcall f_6934(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_6944)
static void C_ccall f_6944(C_word c,C_word *av) C_noret;
C_noret_decl(f_6948)
static void C_ccall f_6948(C_word c,C_word *av) C_noret;
C_noret_decl(f_6958)
static void C_fcall f_6958(C_word t0,C_word t1) C_noret;
C_noret_decl(f_6962)
static void C_ccall f_6962(C_word c,C_word *av) C_noret;
C_noret_decl(f_6964)
static void C_ccall f_6964(C_word c,C_word *av) C_noret;
C_noret_decl(f_6972)
static void C_ccall f_6972(C_word c,C_word *av) C_noret;
C_noret_decl(f_6974)
static void C_ccall f_6974(C_word c,C_word *av) C_noret;
C_noret_decl(f_6978)
static void C_ccall f_6978(C_word c,C_word *av) C_noret;
C_noret_decl(f_7000)
static void C_ccall f_7000(C_word c,C_word *av) C_noret;
C_noret_decl(f_7002)
static void C_fcall f_7002(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_7050)
static void C_fcall f_7050(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_7103)
static void C_ccall f_7103(C_word c,C_word *av) C_noret;
C_noret_decl(f_7114)
static void C_ccall f_7114(C_word c,C_word *av) C_noret;
C_noret_decl(f_7151)
static void C_ccall f_7151(C_word c,C_word *av) C_noret;
C_noret_decl(f_7179)
static void C_ccall f_7179(C_word c,C_word *av) C_noret;
C_noret_decl(f_7206)
static void C_ccall f_7206(C_word c,C_word *av) C_noret;
C_noret_decl(f_7216)
static void C_ccall f_7216(C_word c,C_word *av) C_noret;
C_noret_decl(f_7218)
static void C_fcall f_7218(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7243)
static void C_ccall f_7243(C_word c,C_word *av) C_noret;
C_noret_decl(f_7278)
static void C_ccall f_7278(C_word c,C_word *av) C_noret;
C_noret_decl(f_7280)
static void C_ccall f_7280(C_word c,C_word *av) C_noret;
C_noret_decl(f_7284)
static void C_ccall f_7284(C_word c,C_word *av) C_noret;
C_noret_decl(f_7296)
static void C_ccall f_7296(C_word c,C_word *av) C_noret;
C_noret_decl(f_7299)
static void C_ccall f_7299(C_word c,C_word *av) C_noret;
C_noret_decl(f_7302)
static void C_ccall f_7302(C_word c,C_word *av) C_noret;
C_noret_decl(f_7305)
static void C_ccall f_7305(C_word c,C_word *av) C_noret;
C_noret_decl(f_7316)
static void C_ccall f_7316(C_word c,C_word *av) C_noret;
C_noret_decl(f_7318)
static void C_fcall f_7318(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_7344)
static void C_ccall f_7344(C_word c,C_word *av) C_noret;
C_noret_decl(f_7355)
static void C_ccall f_7355(C_word c,C_word *av) C_noret;
C_noret_decl(f_7438)
static void C_ccall f_7438(C_word c,C_word *av) C_noret;
C_noret_decl(f_7440)
static void C_ccall f_7440(C_word c,C_word *av) C_noret;
C_noret_decl(f_7444)
static void C_ccall f_7444(C_word c,C_word *av) C_noret;
C_noret_decl(f_7447)
static void C_ccall f_7447(C_word c,C_word *av) C_noret;
C_noret_decl(f_7477)
static void C_ccall f_7477(C_word c,C_word *av) C_noret;
C_noret_decl(f_7487)
static void C_ccall f_7487(C_word c,C_word *av) C_noret;
C_noret_decl(f_7515)
static void C_ccall f_7515(C_word c,C_word *av) C_noret;
C_noret_decl(f_7517)
static void C_ccall f_7517(C_word c,C_word *av) C_noret;
C_noret_decl(f_7521)
static void C_ccall f_7521(C_word c,C_word *av) C_noret;
C_noret_decl(f_7540)
static void C_ccall f_7540(C_word c,C_word *av) C_noret;
C_noret_decl(f_7544)
static void C_ccall f_7544(C_word c,C_word *av) C_noret;
C_noret_decl(f_7548)
static void C_ccall f_7548(C_word c,C_word *av) C_noret;
C_noret_decl(f_7550)
static void C_fcall f_7550(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5) C_noret;
C_noret_decl(f_7571)
static void C_ccall f_7571(C_word c,C_word *av) C_noret;
C_noret_decl(f_7589)
static void C_ccall f_7589(C_word c,C_word *av) C_noret;
C_noret_decl(f_7597)
static void C_ccall f_7597(C_word c,C_word *av) C_noret;
C_noret_decl(f_7601)
static void C_ccall f_7601(C_word c,C_word *av) C_noret;
C_noret_decl(f_7611)
static void C_fcall f_7611(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5) C_noret;
C_noret_decl(f_7617)
static void C_fcall f_7617(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_7631)
static void C_ccall f_7631(C_word c,C_word *av) C_noret;
C_noret_decl(f_7657)
static void C_ccall f_7657(C_word c,C_word *av) C_noret;
C_noret_decl(f_7681)
static void C_ccall f_7681(C_word c,C_word *av) C_noret;
C_noret_decl(f_7689)
static void C_ccall f_7689(C_word c,C_word *av) C_noret;
C_noret_decl(f_7697)
static void C_ccall f_7697(C_word c,C_word *av) C_noret;
C_noret_decl(f_7701)
static void C_ccall f_7701(C_word c,C_word *av) C_noret;
C_noret_decl(f_7704)
static void C_ccall f_7704(C_word c,C_word *av) C_noret;
C_noret_decl(f_7707)
static void C_ccall f_7707(C_word c,C_word *av) C_noret;
C_noret_decl(f_7716)
static void C_ccall f_7716(C_word c,C_word *av) C_noret;
C_noret_decl(f_7717)
static void C_fcall f_7717(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7725)
static void C_ccall f_7725(C_word c,C_word *av) C_noret;
C_noret_decl(f_7729)
static void C_ccall f_7729(C_word c,C_word *av) C_noret;
C_noret_decl(f_7733)
static void C_fcall f_7733(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7741)
static void C_ccall f_7741(C_word c,C_word *av) C_noret;
C_noret_decl(f_7747)
static void C_ccall f_7747(C_word c,C_word *av) C_noret;
C_noret_decl(f_7753)
static void C_ccall f_7753(C_word c,C_word *av) C_noret;
C_noret_decl(f_7756)
static void C_ccall f_7756(C_word c,C_word *av) C_noret;
C_noret_decl(f_7759)
static void C_ccall f_7759(C_word c,C_word *av) C_noret;
C_noret_decl(f_7763)
static void C_fcall f_7763(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7771)
static void C_ccall f_7771(C_word c,C_word *av) C_noret;
C_noret_decl(f_7774)
static void C_ccall f_7774(C_word c,C_word *av) C_noret;
C_noret_decl(f_7777)
static void C_ccall f_7777(C_word c,C_word *av) C_noret;
C_noret_decl(f_7780)
static void C_ccall f_7780(C_word c,C_word *av) C_noret;
C_noret_decl(f_7787)
static void C_ccall f_7787(C_word c,C_word *av) C_noret;
C_noret_decl(f_7813)
static void C_fcall f_7813(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7838)
static void C_ccall f_7838(C_word c,C_word *av) C_noret;
C_noret_decl(f_7847)
static void C_fcall f_7847(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7881)
static void C_fcall f_7881(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7906)
static void C_ccall f_7906(C_word c,C_word *av) C_noret;
C_noret_decl(f_7915)
static void C_fcall f_7915(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7963)
static void C_ccall f_7963(C_word c,C_word *av) C_noret;
C_noret_decl(f_7965)
static void C_ccall f_7965(C_word c,C_word *av) C_noret;
C_noret_decl(f_7969)
static void C_ccall f_7969(C_word c,C_word *av) C_noret;
C_noret_decl(f_7979)
static void C_fcall f_7979(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_8006)
static void C_ccall f_8006(C_word c,C_word *av) C_noret;
C_noret_decl(f_8009)
static void C_ccall f_8009(C_word c,C_word *av) C_noret;
C_noret_decl(f_8024)
static void C_ccall f_8024(C_word c,C_word *av) C_noret;
C_noret_decl(f_8032)
static void C_ccall f_8032(C_word c,C_word *av) C_noret;
C_noret_decl(f_8041)
static void C_ccall f_8041(C_word c,C_word *av) C_noret;
C_noret_decl(f_8056)
static void C_ccall f_8056(C_word c,C_word *av) C_noret;
C_noret_decl(f_8066)
static void C_ccall f_8066(C_word c,C_word *av) C_noret;
C_noret_decl(f_8069)
static void C_ccall f_8069(C_word c,C_word *av) C_noret;
C_noret_decl(f_8085)
static void C_ccall f_8085(C_word c,C_word *av) C_noret;
C_noret_decl(f_8105)
static void C_ccall f_8105(C_word c,C_word *av) C_noret;
C_noret_decl(f_8107)
static void C_ccall f_8107(C_word c,C_word *av) C_noret;
C_noret_decl(f_8109)
static void C_ccall f_8109(C_word c,C_word *av) C_noret;
C_noret_decl(f_8113)
static void C_ccall f_8113(C_word c,C_word *av) C_noret;
C_noret_decl(f_8122)
static void C_fcall f_8122(C_word t0,C_word t1) C_noret;
C_noret_decl(f_8125)
static void C_ccall f_8125(C_word c,C_word *av) C_noret;
C_noret_decl(f_8134)
static void C_fcall f_8134(C_word t0,C_word t1) C_noret;
C_noret_decl(f_8150)
static void C_ccall f_8150(C_word c,C_word *av) C_noret;
C_noret_decl(f_8154)
static void C_ccall f_8154(C_word c,C_word *av) C_noret;
C_noret_decl(f_8197)
static void C_ccall f_8197(C_word c,C_word *av) C_noret;
C_noret_decl(f_8209)
static void C_ccall f_8209(C_word c,C_word *av) C_noret;
C_noret_decl(f_8211)
static void C_ccall f_8211(C_word c,C_word *av) C_noret;
C_noret_decl(f_8215)
static void C_ccall f_8215(C_word c,C_word *av) C_noret;
C_noret_decl(f_8218)
static void C_ccall f_8218(C_word c,C_word *av) C_noret;
C_noret_decl(f_8237)
static void C_ccall f_8237(C_word c,C_word *av) C_noret;
C_noret_decl(f_8253)
static void C_ccall f_8253(C_word c,C_word *av) C_noret;
C_noret_decl(f_8255)
static void C_ccall f_8255(C_word c,C_word *av) C_noret;
C_noret_decl(f_8259)
static void C_ccall f_8259(C_word c,C_word *av) C_noret;
C_noret_decl(f_8262)
static void C_ccall f_8262(C_word c,C_word *av) C_noret;
C_noret_decl(f_8275)
static void C_ccall f_8275(C_word c,C_word *av) C_noret;
C_noret_decl(f_8277)
static void C_ccall f_8277(C_word c,C_word *av) C_noret;
C_noret_decl(f_8281)
static void C_ccall f_8281(C_word c,C_word *av) C_noret;
C_noret_decl(f_8295)
static void C_ccall f_8295(C_word c,C_word *av) C_noret;
C_noret_decl(f_8301)
static void C_ccall f_8301(C_word c,C_word *av) C_noret;
C_noret_decl(f_8323)
static void C_ccall f_8323(C_word c,C_word *av) C_noret;
C_noret_decl(f_8329)
static void C_ccall f_8329(C_word c,C_word *av) C_noret;
C_noret_decl(f_8333)
static void C_ccall f_8333(C_word c,C_word *av) C_noret;
C_noret_decl(f_8343)
static void C_ccall f_8343(C_word c,C_word *av) C_noret;
C_noret_decl(f_8345)
static void C_fcall f_8345(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_8374)
static void C_ccall f_8374(C_word c,C_word *av) C_noret;
C_noret_decl(f_8393)
static void C_fcall f_8393(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8427)
static void C_fcall f_8427(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_8451)
static void C_ccall f_8451(C_word c,C_word *av) C_noret;
C_noret_decl(f_8453)
static void C_ccall f_8453(C_word c,C_word *av) C_noret;
C_noret_decl(f_8457)
static void C_ccall f_8457(C_word c,C_word *av) C_noret;
C_noret_decl(f_8463)
static void C_fcall f_8463(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8497)
static void C_fcall f_8497(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8533)
static void C_ccall f_8533(C_word c,C_word *av) C_noret;
C_noret_decl(f_8535)
static void C_ccall f_8535(C_word c,C_word *av) C_noret;
C_noret_decl(f_8539)
static void C_ccall f_8539(C_word c,C_word *av) C_noret;
C_noret_decl(f_8547)
static void C_ccall f_8547(C_word c,C_word *av) C_noret;
C_noret_decl(f_8552)
static void C_fcall f_8552(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8577)
static void C_ccall f_8577(C_word c,C_word *av) C_noret;
C_noret_decl(f_8587)
static void C_ccall f_8587(C_word c,C_word *av) C_noret;
C_noret_decl(f_8589)
static void C_ccall f_8589(C_word c,C_word *av) C_noret;
C_noret_decl(f_8593)
static void C_ccall f_8593(C_word c,C_word *av) C_noret;
C_noret_decl(f_8599)
static void C_ccall f_8599(C_word c,C_word *av) C_noret;
C_noret_decl(f_8620)
static void C_ccall f_8620(C_word c,C_word *av) C_noret;
C_noret_decl(f_8627)
static void C_ccall f_8627(C_word c,C_word *av) C_noret;
C_noret_decl(f_8650)
static void C_ccall f_8650(C_word c,C_word *av) C_noret;
C_noret_decl(f_8654)
static void C_ccall f_8654(C_word c,C_word *av) C_noret;
C_noret_decl(f_8675)
static void C_ccall f_8675(C_word c,C_word *av) C_noret;
C_noret_decl(f_8678)
static void C_ccall f_8678(C_word c,C_word *av) C_noret;
C_noret_decl(f_8682)
static void C_fcall f_8682(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8690)
static void C_ccall f_8690(C_word c,C_word *av) C_noret;
C_noret_decl(f_8694)
static void C_ccall f_8694(C_word c,C_word *av) C_noret;
C_noret_decl(f_8700)
static void C_ccall f_8700(C_word c,C_word *av) C_noret;
C_noret_decl(f_8701)
static void C_ccall f_8701(C_word c,C_word *av) C_noret;
C_noret_decl(f_8712)
static void C_ccall f_8712(C_word c,C_word *av) C_noret;
C_noret_decl(f_8727)
static void C_ccall f_8727(C_word c,C_word *av) C_noret;
C_noret_decl(f_8729)
static void C_fcall f_8729(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_8748)
static void C_fcall f_8748(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8756)
static void C_ccall f_8756(C_word c,C_word *av) C_noret;
C_noret_decl(f_8762)
static void C_ccall f_8762(C_word c,C_word *av) C_noret;
C_noret_decl(f_8764)
static void C_fcall f_8764(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8789)
static void C_ccall f_8789(C_word c,C_word *av) C_noret;
C_noret_decl(f_8813)
static void C_ccall f_8813(C_word c,C_word *av) C_noret;
C_noret_decl(f_8850)
static void C_ccall f_8850(C_word c,C_word *av) C_noret;
C_noret_decl(f_8878)
static void C_fcall f_8878(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8912)
static void C_fcall f_8912(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_8943)
static void C_ccall f_8943(C_word c,C_word *av) C_noret;
C_noret_decl(f_8950)
static void C_ccall f_8950(C_word c,C_word *av) C_noret;
C_noret_decl(f_8956)
static void C_fcall f_8956(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8981)
static void C_ccall f_8981(C_word c,C_word *av) C_noret;
C_noret_decl(f_8990)
static void C_fcall f_8990(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_9003)
static void C_ccall f_9003(C_word c,C_word *av) C_noret;
C_noret_decl(f_9028)
static void C_fcall f_9028(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9064)
static void C_ccall f_9064(C_word c,C_word *av) C_noret;
C_noret_decl(f_9066)
static void C_ccall f_9066(C_word c,C_word *av) C_noret;
C_noret_decl(f_9070)
static void C_ccall f_9070(C_word c,C_word *av) C_noret;
C_noret_decl(f_9077)
static void C_ccall f_9077(C_word c,C_word *av) C_noret;
C_noret_decl(f_9081)
static void C_ccall f_9081(C_word c,C_word *av) C_noret;
C_noret_decl(f_9089)
static void C_ccall f_9089(C_word c,C_word *av) C_noret;
C_noret_decl(f_9103)
static void C_ccall f_9103(C_word c,C_word *av) C_noret;
C_noret_decl(f_9109)
static void C_ccall f_9109(C_word c,C_word *av) C_noret;
C_noret_decl(f_9116)
static void C_ccall f_9116(C_word c,C_word *av) C_noret;
C_noret_decl(f_9122)
static void C_ccall f_9122(C_word c,C_word *av) C_noret;
C_noret_decl(f_9135)
static void C_fcall f_9135(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9169)
static void C_fcall f_9169(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9179)
static void C_ccall f_9179(C_word c,C_word *av) C_noret;
C_noret_decl(f_9194)
static void C_ccall f_9194(C_word c,C_word *av) C_noret;
C_noret_decl(f_9196)
static void C_ccall f_9196(C_word c,C_word *av) C_noret;
C_noret_decl(f_9200)
static void C_ccall f_9200(C_word c,C_word *av) C_noret;
C_noret_decl(f_9215)
static void C_ccall f_9215(C_word c,C_word *av) C_noret;
C_noret_decl(f_9217)
static void C_ccall f_9217(C_word c,C_word *av) C_noret;
C_noret_decl(f_9221)
static void C_ccall f_9221(C_word c,C_word *av) C_noret;
C_noret_decl(f_9243)
static void C_ccall f_9243(C_word c,C_word *av) C_noret;
C_noret_decl(f_9245)
static void C_ccall f_9245(C_word c,C_word *av) C_noret;
C_noret_decl(f_9249)
static void C_ccall f_9249(C_word c,C_word *av) C_noret;
C_noret_decl(f_9267)
static void C_ccall f_9267(C_word c,C_word *av) C_noret;
C_noret_decl(f_9269)
static void C_ccall f_9269(C_word c,C_word *av) C_noret;
C_noret_decl(f_9278)
static void C_fcall f_9278(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9284)
static void C_ccall f_9284(C_word c,C_word *av) C_noret;
C_noret_decl(f_9290)
static void C_ccall f_9290(C_word c,C_word *av) C_noret;
C_noret_decl(f_9304)
static void C_ccall f_9304(C_word c,C_word *av) C_noret;
C_noret_decl(f_9313)
static void C_ccall f_9313(C_word c,C_word *av) C_noret;
C_noret_decl(f_9315)
static void C_fcall f_9315(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9340)
static void C_ccall f_9340(C_word c,C_word *av) C_noret;
C_noret_decl(f_9351)
static void C_ccall f_9351(C_word c,C_word *av) C_noret;
C_noret_decl(f_9353)
static void C_ccall f_9353(C_word c,C_word *av) C_noret;
C_noret_decl(f_9372)
static void C_ccall f_9372(C_word c,C_word *av) C_noret;
C_noret_decl(f_9380)
static void C_ccall f_9380(C_word c,C_word *av) C_noret;
C_noret_decl(f_9389)
static void C_ccall f_9389(C_word c,C_word *av) C_noret;
C_noret_decl(f_9395)
static void C_ccall f_9395(C_word c,C_word *av) C_noret;
C_noret_decl(f_9399)
static void C_fcall f_9399(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9407)
static void C_ccall f_9407(C_word c,C_word *av) C_noret;
C_noret_decl(f_9413)
static void C_ccall f_9413(C_word c,C_word *av) C_noret;
C_noret_decl(f_9417)
static void C_fcall f_9417(C_word t0,C_word t1) C_noret;
C_noret_decl(f_9425)
static void C_ccall f_9425(C_word c,C_word *av) C_noret;
C_noret_decl(f_9428)
static void C_ccall f_9428(C_word c,C_word *av) C_noret;
C_noret_decl(f_9432)
static void C_fcall f_9432(C_word t0,C_word t1) C_noret;
C_noret_decl(f_9440)
static void C_ccall f_9440(C_word c,C_word *av) C_noret;
C_noret_decl(f_9443)
static void C_ccall f_9443(C_word c,C_word *av) C_noret;
C_noret_decl(f_9456)
static void C_ccall f_9456(C_word c,C_word *av) C_noret;
C_noret_decl(f_9473)
static void C_ccall f_9473(C_word c,C_word *av) C_noret;
C_noret_decl(f_9484)
static void C_ccall f_9484(C_word c,C_word *av) C_noret;
C_noret_decl(f_9532)
static void C_ccall f_9532(C_word c,C_word *av) C_noret;
C_noret_decl(f_9536)
static void C_ccall f_9536(C_word c,C_word *av) C_noret;
C_noret_decl(f_9548)
static void C_ccall f_9548(C_word c,C_word *av) C_noret;
C_noret_decl(f_9560)
static void C_ccall f_9560(C_word c,C_word *av) C_noret;
C_noret_decl(f_9562)
static void C_fcall f_9562(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_9610)
static void C_fcall f_9610(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_9658)
static void C_fcall f_9658(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_9665)
static void C_fcall f_9665(C_word t0,C_word t1) C_noret;
C_noret_decl(f_9735)
static C_word C_fcall f_9735(C_word *a,C_word t0,C_word t1,C_word t2,C_word t3);
C_noret_decl(f_9753)
static void C_ccall f_9753(C_word c,C_word *av) C_noret;
C_noret_decl(f_9757)
static void C_ccall f_9757(C_word c,C_word *av) C_noret;
C_noret_decl(f_9773)
static void C_ccall f_9773(C_word c,C_word *av) C_noret;
C_noret_decl(f_9777)
static void C_ccall f_9777(C_word c,C_word *av) C_noret;
C_noret_decl(f_9789)
static void C_ccall f_9789(C_word c,C_word *av) C_noret;
C_noret_decl(f_9799)
static void C_fcall f_9799(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_9847)
static void C_fcall f_9847(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_9895)
static void C_fcall f_9895(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_9902)
static void C_fcall f_9902(C_word t0,C_word t1) C_noret;
C_noret_decl(f_9965)
static void C_fcall f_9965(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(C_chicken_2dsyntax_toplevel)
C_externexport void C_ccall C_chicken_2dsyntax_toplevel(C_word c,C_word *av) C_noret;

C_noret_decl(trf_10013)
static void C_ccall trf_10013(C_word c,C_word *av) C_noret;
static void C_ccall trf_10013(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_10013(t0,t1,t2,t3);}

C_noret_decl(trf_10061)
static void C_ccall trf_10061(C_word c,C_word *av) C_noret;
static void C_ccall trf_10061(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_10061(t0,t1,t2,t3);}

C_noret_decl(trf_10109)
static void C_ccall trf_10109(C_word c,C_word *av) C_noret;
static void C_ccall trf_10109(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10109(t0,t1,t2);}

C_noret_decl(trf_10143)
static void C_ccall trf_10143(C_word c,C_word *av) C_noret;
static void C_ccall trf_10143(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10143(t0,t1,t2);}

C_noret_decl(trf_10177)
static void C_ccall trf_10177(C_word c,C_word *av) C_noret;
static void C_ccall trf_10177(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10177(t0,t1,t2);}

C_noret_decl(trf_10211)
static void C_ccall trf_10211(C_word c,C_word *av) C_noret;
static void C_ccall trf_10211(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10211(t0,t1,t2);}

C_noret_decl(trf_10245)
static void C_ccall trf_10245(C_word c,C_word *av) C_noret;
static void C_ccall trf_10245(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10245(t0,t1,t2);}

C_noret_decl(trf_10305)
static void C_ccall trf_10305(C_word c,C_word *av) C_noret;
static void C_ccall trf_10305(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_10305(t0,t1);}

C_noret_decl(trf_10320)
static void C_ccall trf_10320(C_word c,C_word *av) C_noret;
static void C_ccall trf_10320(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_10320(t0,t1);}

C_noret_decl(trf_10400)
static void C_ccall trf_10400(C_word c,C_word *av) C_noret;
static void C_ccall trf_10400(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_10400(t0,t1,t2,t3);}

C_noret_decl(trf_10448)
static void C_ccall trf_10448(C_word c,C_word *av) C_noret;
static void C_ccall trf_10448(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_10448(t0,t1,t2,t3);}

C_noret_decl(trf_10547)
static void C_ccall trf_10547(C_word c,C_word *av) C_noret;
static void C_ccall trf_10547(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_10547(t0,t1,t2,t3);}

C_noret_decl(trf_10595)
static void C_ccall trf_10595(C_word c,C_word *av) C_noret;
static void C_ccall trf_10595(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_10595(t0,t1,t2,t3);}

C_noret_decl(trf_10678)
static void C_ccall trf_10678(C_word c,C_word *av) C_noret;
static void C_ccall trf_10678(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_10678(t0,t1,t2,t3);}

C_noret_decl(trf_10728)
static void C_ccall trf_10728(C_word c,C_word *av) C_noret;
static void C_ccall trf_10728(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10728(t0,t1,t2);}

C_noret_decl(trf_10748)
static void C_ccall trf_10748(C_word c,C_word *av) C_noret;
static void C_ccall trf_10748(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_10748(t0,t1,t2,t3);}

C_noret_decl(trf_10796)
static void C_ccall trf_10796(C_word c,C_word *av) C_noret;
static void C_ccall trf_10796(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10796(t0,t1,t2);}

C_noret_decl(trf_10830)
static void C_ccall trf_10830(C_word c,C_word *av) C_noret;
static void C_ccall trf_10830(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10830(t0,t1,t2);}

C_noret_decl(trf_10864)
static void C_ccall trf_10864(C_word c,C_word *av) C_noret;
static void C_ccall trf_10864(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10864(t0,t1,t2);}

C_noret_decl(trf_10898)
static void C_ccall trf_10898(C_word c,C_word *av) C_noret;
static void C_ccall trf_10898(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10898(t0,t1,t2);}

C_noret_decl(trf_11130)
static void C_ccall trf_11130(C_word c,C_word *av) C_noret;
static void C_ccall trf_11130(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11130(t0,t1,t2);}

C_noret_decl(trf_11143)
static void C_ccall trf_11143(C_word c,C_word *av) C_noret;
static void C_ccall trf_11143(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_11143(t0,t1);}

C_noret_decl(trf_11223)
static void C_ccall trf_11223(C_word c,C_word *av) C_noret;
static void C_ccall trf_11223(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_11223(t0,t1,t2,t3);}

C_noret_decl(trf_11275)
static void C_ccall trf_11275(C_word c,C_word *av) C_noret;
static void C_ccall trf_11275(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_11275(t0,t1);}

C_noret_decl(trf_11441)
static void C_ccall trf_11441(C_word c,C_word *av) C_noret;
static void C_ccall trf_11441(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11441(t0,t1,t2);}

C_noret_decl(trf_11573)
static void C_ccall trf_11573(C_word c,C_word *av) C_noret;
static void C_ccall trf_11573(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_11573(t0,t1,t2,t3);}

C_noret_decl(trf_11623)
static void C_ccall trf_11623(C_word c,C_word *av) C_noret;
static void C_ccall trf_11623(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_11623(t0,t1,t2,t3);}

C_noret_decl(trf_11692)
static void C_ccall trf_11692(C_word c,C_word *av) C_noret;
static void C_ccall trf_11692(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_11692(t0,t1);}

C_noret_decl(trf_11928)
static void C_ccall trf_11928(C_word c,C_word *av) C_noret;
static void C_ccall trf_11928(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11928(t0,t1,t2);}

C_noret_decl(trf_11968)
static void C_ccall trf_11968(C_word c,C_word *av) C_noret;
static void C_ccall trf_11968(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11968(t0,t1,t2);}

C_noret_decl(trf_12054)
static void C_ccall trf_12054(C_word c,C_word *av) C_noret;
static void C_ccall trf_12054(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_12054(t0,t1,t2,t3,t4);}

C_noret_decl(trf_12110)
static void C_ccall trf_12110(C_word c,C_word *av) C_noret;
static void C_ccall trf_12110(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_12110(t0,t1,t2,t3);}

C_noret_decl(trf_12207)
static void C_ccall trf_12207(C_word c,C_word *av) C_noret;
static void C_ccall trf_12207(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_12207(t0,t1,t2);}

C_noret_decl(trf_12251)
static void C_ccall trf_12251(C_word c,C_word *av) C_noret;
static void C_ccall trf_12251(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_12251(t0,t1,t2,t3);}

C_noret_decl(trf_12305)
static void C_ccall trf_12305(C_word c,C_word *av) C_noret;
static void C_ccall trf_12305(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_12305(t0,t1);}

C_noret_decl(trf_12475)
static void C_ccall trf_12475(C_word c,C_word *av) C_noret;
static void C_ccall trf_12475(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_12475(t0,t1,t2);}

C_noret_decl(trf_12509)
static void C_ccall trf_12509(C_word c,C_word *av) C_noret;
static void C_ccall trf_12509(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_12509(t0,t1,t2);}

C_noret_decl(trf_12577)
static void C_ccall trf_12577(C_word c,C_word *av) C_noret;
static void C_ccall trf_12577(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_12577(t0,t1,t2,t3);}

C_noret_decl(trf_12627)
static void C_ccall trf_12627(C_word c,C_word *av) C_noret;
static void C_ccall trf_12627(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_12627(t0,t1,t2,t3);}

C_noret_decl(trf_12646)
static void C_ccall trf_12646(C_word c,C_word *av) C_noret;
static void C_ccall trf_12646(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_12646(t0,t1);}

C_noret_decl(trf_12787)
static void C_ccall trf_12787(C_word c,C_word *av) C_noret;
static void C_ccall trf_12787(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_12787(t0,t1);}

C_noret_decl(trf_12860)
static void C_ccall trf_12860(C_word c,C_word *av) C_noret;
static void C_ccall trf_12860(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_12860(t0,t1,t2);}

C_noret_decl(trf_12870)
static void C_ccall trf_12870(C_word c,C_word *av) C_noret;
static void C_ccall trf_12870(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_12870(t0,t1);}

C_noret_decl(trf_12911)
static void C_ccall trf_12911(C_word c,C_word *av) C_noret;
static void C_ccall trf_12911(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_12911(t0,t1,t2);}

C_noret_decl(trf_13046)
static void C_ccall trf_13046(C_word c,C_word *av) C_noret;
static void C_ccall trf_13046(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_13046(t0,t1,t2);}

C_noret_decl(trf_4334)
static void C_ccall trf_4334(C_word c,C_word *av) C_noret;
static void C_ccall trf_4334(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4334(t0,t1,t2);}

C_noret_decl(trf_4392)
static void C_ccall trf_4392(C_word c,C_word *av) C_noret;
static void C_ccall trf_4392(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_4392(t0,t1,t2,t3,t4);}

C_noret_decl(trf_5763)
static void C_ccall trf_5763(C_word c,C_word *av) C_noret;
static void C_ccall trf_5763(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5763(t0,t1);}

C_noret_decl(trf_5786)
static void C_ccall trf_5786(C_word c,C_word *av) C_noret;
static void C_ccall trf_5786(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5786(t0,t1);}

C_noret_decl(trf_5937)
static void C_ccall trf_5937(C_word c,C_word *av) C_noret;
static void C_ccall trf_5937(C_word c,C_word *av){
C_word t0=av[6];
C_word t1=av[5];
C_word t2=av[4];
C_word t3=av[3];
C_word t4=av[2];
C_word t5=av[1];
C_word t6=av[0];
f_5937(t0,t1,t2,t3,t4,t5,t6);}

C_noret_decl(trf_6132)
static void C_ccall trf_6132(C_word c,C_word *av) C_noret;
static void C_ccall trf_6132(C_word c,C_word *av){
C_word t0=av[5];
C_word t1=av[4];
C_word t2=av[3];
C_word t3=av[2];
C_word t4=av[1];
C_word t5=av[0];
f_6132(t0,t1,t2,t3,t4,t5);}

C_noret_decl(trf_6331)
static void C_ccall trf_6331(C_word c,C_word *av) C_noret;
static void C_ccall trf_6331(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6331(t0,t1,t2);}

C_noret_decl(trf_6380)
static void C_ccall trf_6380(C_word c,C_word *av) C_noret;
static void C_ccall trf_6380(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_6380(t0,t1,t2,t3);}

C_noret_decl(trf_6399)
static void C_ccall trf_6399(C_word c,C_word *av) C_noret;
static void C_ccall trf_6399(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_6399(t0,t1);}

C_noret_decl(trf_6405)
static void C_ccall trf_6405(C_word c,C_word *av) C_noret;
static void C_ccall trf_6405(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_6405(t0,t1);}

C_noret_decl(trf_6412)
static void C_ccall trf_6412(C_word c,C_word *av) C_noret;
static void C_ccall trf_6412(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_6412(t0,t1);}

C_noret_decl(trf_6420)
static void C_ccall trf_6420(C_word c,C_word *av) C_noret;
static void C_ccall trf_6420(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_6420(t0,t1);}

C_noret_decl(trf_6610)
static void C_ccall trf_6610(C_word c,C_word *av) C_noret;
static void C_ccall trf_6610(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6610(t0,t1,t2);}

C_noret_decl(trf_6648)
static void C_ccall trf_6648(C_word c,C_word *av) C_noret;
static void C_ccall trf_6648(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6648(t0,t1,t2);}

C_noret_decl(trf_6671)
static void C_ccall trf_6671(C_word c,C_word *av) C_noret;
static void C_ccall trf_6671(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6671(t0,t1,t2);}

C_noret_decl(trf_6842)
static void C_ccall trf_6842(C_word c,C_word *av) C_noret;
static void C_ccall trf_6842(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6842(t0,t1,t2);}

C_noret_decl(trf_6848)
static void C_ccall trf_6848(C_word c,C_word *av) C_noret;
static void C_ccall trf_6848(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6848(t0,t1,t2);}

C_noret_decl(trf_6926)
static void C_ccall trf_6926(C_word c,C_word *av) C_noret;
static void C_ccall trf_6926(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6926(t0,t1,t2);}

C_noret_decl(trf_6934)
static void C_ccall trf_6934(C_word c,C_word *av) C_noret;
static void C_ccall trf_6934(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_6934(t0,t1,t2,t3);}

C_noret_decl(trf_6958)
static void C_ccall trf_6958(C_word c,C_word *av) C_noret;
static void C_ccall trf_6958(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_6958(t0,t1);}

C_noret_decl(trf_7002)
static void C_ccall trf_7002(C_word c,C_word *av) C_noret;
static void C_ccall trf_7002(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_7002(t0,t1,t2,t3);}

C_noret_decl(trf_7050)
static void C_ccall trf_7050(C_word c,C_word *av) C_noret;
static void C_ccall trf_7050(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_7050(t0,t1,t2,t3);}

C_noret_decl(trf_7218)
static void C_ccall trf_7218(C_word c,C_word *av) C_noret;
static void C_ccall trf_7218(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7218(t0,t1,t2);}

C_noret_decl(trf_7318)
static void C_ccall trf_7318(C_word c,C_word *av) C_noret;
static void C_ccall trf_7318(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_7318(t0,t1,t2,t3);}

C_noret_decl(trf_7550)
static void C_ccall trf_7550(C_word c,C_word *av) C_noret;
static void C_ccall trf_7550(C_word c,C_word *av){
C_word t0=av[5];
C_word t1=av[4];
C_word t2=av[3];
C_word t3=av[2];
C_word t4=av[1];
C_word t5=av[0];
f_7550(t0,t1,t2,t3,t4,t5);}

C_noret_decl(trf_7611)
static void C_ccall trf_7611(C_word c,C_word *av) C_noret;
static void C_ccall trf_7611(C_word c,C_word *av){
C_word t0=av[5];
C_word t1=av[4];
C_word t2=av[3];
C_word t3=av[2];
C_word t4=av[1];
C_word t5=av[0];
f_7611(t0,t1,t2,t3,t4,t5);}

C_noret_decl(trf_7617)
static void C_ccall trf_7617(C_word c,C_word *av) C_noret;
static void C_ccall trf_7617(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_7617(t0,t1,t2,t3,t4);}

C_noret_decl(trf_7717)
static void C_ccall trf_7717(C_word c,C_word *av) C_noret;
static void C_ccall trf_7717(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7717(t0,t1,t2);}

C_noret_decl(trf_7733)
static void C_ccall trf_7733(C_word c,C_word *av) C_noret;
static void C_ccall trf_7733(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7733(t0,t1,t2);}

C_noret_decl(trf_7763)
static void C_ccall trf_7763(C_word c,C_word *av) C_noret;
static void C_ccall trf_7763(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7763(t0,t1,t2);}

C_noret_decl(trf_7813)
static void C_ccall trf_7813(C_word c,C_word *av) C_noret;
static void C_ccall trf_7813(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7813(t0,t1,t2);}

C_noret_decl(trf_7847)
static void C_ccall trf_7847(C_word c,C_word *av) C_noret;
static void C_ccall trf_7847(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7847(t0,t1,t2);}

C_noret_decl(trf_7881)
static void C_ccall trf_7881(C_word c,C_word *av) C_noret;
static void C_ccall trf_7881(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7881(t0,t1,t2);}

C_noret_decl(trf_7915)
static void C_ccall trf_7915(C_word c,C_word *av) C_noret;
static void C_ccall trf_7915(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7915(t0,t1,t2);}

C_noret_decl(trf_7979)
static void C_ccall trf_7979(C_word c,C_word *av) C_noret;
static void C_ccall trf_7979(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_7979(t0,t1,t2,t3);}

C_noret_decl(trf_8122)
static void C_ccall trf_8122(C_word c,C_word *av) C_noret;
static void C_ccall trf_8122(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_8122(t0,t1);}

C_noret_decl(trf_8134)
static void C_ccall trf_8134(C_word c,C_word *av) C_noret;
static void C_ccall trf_8134(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_8134(t0,t1);}

C_noret_decl(trf_8345)
static void C_ccall trf_8345(C_word c,C_word *av) C_noret;
static void C_ccall trf_8345(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_8345(t0,t1,t2,t3);}

C_noret_decl(trf_8393)
static void C_ccall trf_8393(C_word c,C_word *av) C_noret;
static void C_ccall trf_8393(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8393(t0,t1,t2);}

C_noret_decl(trf_8427)
static void C_ccall trf_8427(C_word c,C_word *av) C_noret;
static void C_ccall trf_8427(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_8427(t0,t1,t2,t3);}

C_noret_decl(trf_8463)
static void C_ccall trf_8463(C_word c,C_word *av) C_noret;
static void C_ccall trf_8463(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8463(t0,t1,t2);}

C_noret_decl(trf_8497)
static void C_ccall trf_8497(C_word c,C_word *av) C_noret;
static void C_ccall trf_8497(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8497(t0,t1,t2);}

C_noret_decl(trf_8552)
static void C_ccall trf_8552(C_word c,C_word *av) C_noret;
static void C_ccall trf_8552(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8552(t0,t1,t2);}

C_noret_decl(trf_8682)
static void C_ccall trf_8682(C_word c,C_word *av) C_noret;
static void C_ccall trf_8682(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8682(t0,t1,t2);}

C_noret_decl(trf_8729)
static void C_ccall trf_8729(C_word c,C_word *av) C_noret;
static void C_ccall trf_8729(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_8729(t0,t1,t2,t3,t4);}

C_noret_decl(trf_8748)
static void C_ccall trf_8748(C_word c,C_word *av) C_noret;
static void C_ccall trf_8748(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8748(t0,t1,t2);}

C_noret_decl(trf_8764)
static void C_ccall trf_8764(C_word c,C_word *av) C_noret;
static void C_ccall trf_8764(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8764(t0,t1,t2);}

C_noret_decl(trf_8878)
static void C_ccall trf_8878(C_word c,C_word *av) C_noret;
static void C_ccall trf_8878(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8878(t0,t1,t2);}

C_noret_decl(trf_8912)
static void C_ccall trf_8912(C_word c,C_word *av) C_noret;
static void C_ccall trf_8912(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_8912(t0,t1,t2,t3);}

C_noret_decl(trf_8956)
static void C_ccall trf_8956(C_word c,C_word *av) C_noret;
static void C_ccall trf_8956(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8956(t0,t1,t2);}

C_noret_decl(trf_8990)
static void C_ccall trf_8990(C_word c,C_word *av) C_noret;
static void C_ccall trf_8990(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_8990(t0,t1,t2,t3);}

C_noret_decl(trf_9028)
static void C_ccall trf_9028(C_word c,C_word *av) C_noret;
static void C_ccall trf_9028(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9028(t0,t1,t2);}

C_noret_decl(trf_9135)
static void C_ccall trf_9135(C_word c,C_word *av) C_noret;
static void C_ccall trf_9135(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9135(t0,t1,t2);}

C_noret_decl(trf_9169)
static void C_ccall trf_9169(C_word c,C_word *av) C_noret;
static void C_ccall trf_9169(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9169(t0,t1,t2);}

C_noret_decl(trf_9278)
static void C_ccall trf_9278(C_word c,C_word *av) C_noret;
static void C_ccall trf_9278(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9278(t0,t1,t2);}

C_noret_decl(trf_9315)
static void C_ccall trf_9315(C_word c,C_word *av) C_noret;
static void C_ccall trf_9315(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9315(t0,t1,t2);}

C_noret_decl(trf_9399)
static void C_ccall trf_9399(C_word c,C_word *av) C_noret;
static void C_ccall trf_9399(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9399(t0,t1,t2);}

C_noret_decl(trf_9417)
static void C_ccall trf_9417(C_word c,C_word *av) C_noret;
static void C_ccall trf_9417(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_9417(t0,t1);}

C_noret_decl(trf_9432)
static void C_ccall trf_9432(C_word c,C_word *av) C_noret;
static void C_ccall trf_9432(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_9432(t0,t1);}

C_noret_decl(trf_9562)
static void C_ccall trf_9562(C_word c,C_word *av) C_noret;
static void C_ccall trf_9562(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_9562(t0,t1,t2,t3);}

C_noret_decl(trf_9610)
static void C_ccall trf_9610(C_word c,C_word *av) C_noret;
static void C_ccall trf_9610(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_9610(t0,t1,t2,t3);}

C_noret_decl(trf_9658)
static void C_ccall trf_9658(C_word c,C_word *av) C_noret;
static void C_ccall trf_9658(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_9658(t0,t1,t2,t3,t4);}

C_noret_decl(trf_9665)
static void C_ccall trf_9665(C_word c,C_word *av) C_noret;
static void C_ccall trf_9665(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_9665(t0,t1);}

C_noret_decl(trf_9799)
static void C_ccall trf_9799(C_word c,C_word *av) C_noret;
static void C_ccall trf_9799(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_9799(t0,t1,t2,t3);}

C_noret_decl(trf_9847)
static void C_ccall trf_9847(C_word c,C_word *av) C_noret;
static void C_ccall trf_9847(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_9847(t0,t1,t2,t3);}

C_noret_decl(trf_9895)
static void C_ccall trf_9895(C_word c,C_word *av) C_noret;
static void C_ccall trf_9895(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_9895(t0,t1,t2,t3,t4);}

C_noret_decl(trf_9902)
static void C_ccall trf_9902(C_word c,C_word *av) C_noret;
static void C_ccall trf_9902(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_9902(t0,t1);}

C_noret_decl(trf_9965)
static void C_ccall trf_9965(C_word c,C_word *av) C_noret;
static void C_ccall trf_9965(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_9965(t0,t1,t2,t3);}

/* map-loop1777 in k9454 in k9441 in k9426 in k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in ... */
static void C_fcall f_10013(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(9,0,3)))){
C_save_and_reclaim_args((void *)trf_10013,4,t0,t1,t2,t3);}
a=C_alloc(9);
t4=C_i_pairp(t2);
t5=(C_truep(t4)?C_i_pairp(t3):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_slot(t2,C_fix(0));
t7=C_slot(t3,C_fix(0));
t8=C_a_i_list(&a,2,t6,t7);
t9=C_a_i_cons(&a,2,t8,C_SCHEME_END_OF_LIST);
t10=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t9);
t11=C_mutate(((C_word *)((C_word*)t0)[2])+1,t9);
t13=t1;
t14=C_slot(t2,C_fix(1));
t15=C_slot(t3,C_fix(1));
t1=t13;
t2=t14;
t3=t15;
goto loop;}
else{
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* map-loop1746 in k9441 in k9426 in k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in ... */
static void C_fcall f_10061(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(9,0,3)))){
C_save_and_reclaim_args((void *)trf_10061,4,t0,t1,t2,t3);}
a=C_alloc(9);
t4=C_i_pairp(t2);
t5=(C_truep(t4)?C_i_pairp(t3):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_slot(t2,C_fix(0));
t7=C_slot(t3,C_fix(0));
t8=C_a_i_list(&a,2,t6,t7);
t9=C_a_i_cons(&a,2,t8,C_SCHEME_END_OF_LIST);
t10=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t9);
t11=C_mutate(((C_word *)((C_word*)t0)[2])+1,t9);
t13=t1;
t14=C_slot(t2,C_fix(1));
t15=C_slot(t3,C_fix(1));
t1=t13;
t2=t14;
t3=t15;
goto loop;}
else{
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* map-loop1716 in k9426 in k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in ... */
static void C_fcall f_10109(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_10109,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10134,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:496: g1722 */
t4=((C_word*)t0)[4];
f_9432(t4,t3);}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k10132 in map-loop1716 in k9426 in k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in ... */
static void C_ccall f_10134(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_10134,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_10109(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* map-loop1688 in k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in ... */
static void C_fcall f_10143(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_10143,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10168,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:495: g1694 */
t4=((C_word*)t0)[4];
f_9417(t4,t3);}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k10166 in map-loop1688 in k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in ... */
static void C_ccall f_10168(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_10168,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_10143(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* map-loop1660 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in ... */
static void C_fcall f_10177(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_10177,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10202,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:494: g1666 */
t4=((C_word*)t0)[4];
f_9399(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k10200 in map-loop1660 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in ... */
static void C_ccall f_10202(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_10202,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_10177(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* map-loop1633 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in ... */
static void C_fcall f_10211(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_10211,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_cadr(t3);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* map-loop1606 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in ... */
static void C_fcall f_10245(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_10245,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_car(t3);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k10279 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in ... */
static void C_ccall f_10281(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_10281,c,av);}
/* chicken-syntax.scm:448: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[182];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in ... */
static void C_ccall f_10283(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_10283,c,av);}
a=C_alloc(5);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10287,a[2]=t2,a[3]=t3,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:452: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[182];
av2[3]=t2;
av2[4]=lf[183];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in ... */
static void C_ccall f_10287(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(19,c,3)))){
C_save_and_reclaim((void *)f_10287,c,av);}
a=C_alloc(19);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=C_u_i_cdr(((C_word*)t0)[2]);
t4=C_u_i_cdr(t3);
t5=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t6=t5;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=((C_word*)t7)[1];
t9=C_i_check_list_2(t2,lf[67]);
t10=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10301,a[2]=((C_word*)t0)[3],a[3]=t4,a[4]=((C_word*)t0)[4],a[5]=t2,tmp=(C_word)a,a+=6,tmp);
t11=C_SCHEME_UNDEFINED;
t12=(*a=C_VECTOR_TYPE|1,a[1]=t11,tmp=(C_word)a,a+=2,tmp);
t13=C_set_block_item(t12,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10898,a[2]=t7,a[3]=t12,a[4]=t8,a[5]=((C_word)li115),tmp=(C_word)a,a+=6,tmp));
t14=((C_word*)t12)[1];
f_10898(t14,t10,t2);}

/* k10299 in k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in ... */
static void C_ccall f_10301(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(25,c,3)))){
C_save_and_reclaim((void *)f_10301,c,av);}
a=C_alloc(25);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10305,a[2]=((C_word*)t0)[2],a[3]=((C_word)li103),tmp=(C_word)a,a+=4,tmp);
t7=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10316,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10864,a[2]=t4,a[3]=t9,a[4]=t6,a[5]=t5,a[6]=((C_word)li114),tmp=(C_word)a,a+=7,tmp));
t11=((C_word*)t9)[1];
f_10864(t11,t7,((C_word*)t0)[5]);}

/* g1309 in k10299 in k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in ... */
static void C_fcall f_10305(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_10305,2,t0,t1);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10313,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:456: chicken.base#gensym */
t3=*((C_word*)lf[56]+1);{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k10311 in g1309 in k10299 in k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in ... */
static void C_ccall f_10313(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_10313,c,av);}
/* chicken-syntax.scm:456: r */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k10314 in k10299 in k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in ... */
static void C_ccall f_10316(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(25,c,3)))){
C_save_and_reclaim((void *)f_10316,c,av);}
a=C_alloc(25);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10320,a[2]=((C_word*)t0)[2],a[3]=((C_word)li104),tmp=(C_word)a,a+=4,tmp);
t7=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10331,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=t1,a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10830,a[2]=t4,a[3]=t9,a[4]=t6,a[5]=t5,a[6]=((C_word)li113),tmp=(C_word)a,a+=7,tmp));
t11=((C_word*)t9)[1];
f_10830(t11,t7,((C_word*)t0)[6]);}

/* g1337 in k10314 in k10299 in k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in ... */
static void C_fcall f_10320(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_10320,2,t0,t1);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10328,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:457: chicken.base#gensym */
t3=*((C_word*)lf[56]+1);{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k10326 in g1337 in k10314 in k10299 in k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in ... */
static void C_ccall f_10328(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_10328,c,av);}
/* chicken-syntax.scm:457: r */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k10329 in k10314 in k10299 in k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in ... */
static void C_ccall f_10331(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(33,c,3)))){
C_save_and_reclaim((void *)f_10331,c,av);}
a=C_alloc(33);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10338,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t8=t7;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=((C_word*)t9)[1];
t11=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_10651,a[2]=((C_word*)t0)[5],a[3]=t1,a[4]=t2,a[5]=((C_word*)t0)[6],a[6]=t5,a[7]=t6,tmp=(C_word)a,a+=8,tmp);
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10796,a[2]=t9,a[3]=t13,a[4]=t10,a[5]=((C_word)li112),tmp=(C_word)a,a+=6,tmp));
t15=((C_word*)t13)[1];
f_10796(t15,t11,((C_word*)t0)[6]);}

/* k10336 in k10329 in k10314 in k10299 in k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in ... */
static void C_ccall f_10338(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(26,c,4)))){
C_save_and_reclaim((void *)f_10338,c,av);}
a=C_alloc(26);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_10506,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=C_i_check_list_2(((C_word*)t0)[4],lf[67]);
t8=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10521,a[2]=t2,a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[6],tmp=(C_word)a,a+=5,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10595,a[2]=t5,a[3]=t10,a[4]=t6,a[5]=((C_word)li108),tmp=(C_word)a,a+=6,tmp));
t12=((C_word*)t10)[1];
f_10595(t12,t8,((C_word*)t0)[5],((C_word*)t0)[4]);}

/* k10360 in k10504 in k10336 in k10329 in k10314 in k10299 in k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in ... */
static void C_ccall f_10362(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(27,c,1)))){
C_save_and_reclaim((void *)f_10362,c,av);}
a=C_alloc(27);
t2=C_a_i_cons(&a,2,C_SCHEME_END_OF_LIST,t1);
t3=C_a_i_cons(&a,2,lf[23],t2);
t4=C_a_i_list(&a,4,lf[178],((C_word*)t0)[2],((C_word*)t0)[3],t3);
t5=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_a_i_list(&a,3,lf[37],((C_word*)t0)[5],t4);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* k10372 in k10504 in k10336 in k10329 in k10314 in k10299 in k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in ... */
static void C_ccall f_10374(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(20,c,4)))){
C_save_and_reclaim((void *)f_10374,c,av);}
a=C_alloc(20);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10378,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10390,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10400,a[2]=t5,a[3]=t9,a[4]=t6,a[5]=((C_word)li105),tmp=(C_word)a,a+=6,tmp));
t11=((C_word*)t9)[1];
f_10400(t11,t7,((C_word*)t0)[3],((C_word*)t0)[4]);}

/* k10376 in k10372 in k10504 in k10336 in k10329 in k10314 in k10299 in k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in ... */
static void C_ccall f_10378(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_10378,c,av);}
/* chicken-syntax.scm:458: ##sys#append */
t2=*((C_word*)lf[55]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k10388 in k10372 in k10504 in k10336 in k10329 in k10314 in k10299 in k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in ... */
static void C_ccall f_10390(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_10390,c,av);}
a=C_alloc(6);
t2=C_a_i_list(&a,1,lf[164]);
t3=C_a_i_list(&a,1,t2);
/* chicken-syntax.scm:458: ##sys#append */
t4=*((C_word*)lf[55]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* map-loop1560 in k10372 in k10504 in k10336 in k10329 in k10314 in k10299 in k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in ... */
static void C_fcall f_10400(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(12,0,3)))){
C_save_and_reclaim_args((void *)trf_10400,4,t0,t1,t2,t3);}
a=C_alloc(12);
t4=C_i_pairp(t2);
t5=(C_truep(t4)?C_i_pairp(t3):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_slot(t2,C_fix(0));
t7=C_slot(t3,C_fix(0));
t8=C_a_i_list(&a,3,lf[179],t6,t7);
t9=C_a_i_cons(&a,2,t8,C_SCHEME_END_OF_LIST);
t10=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t9);
t11=C_mutate(((C_word *)((C_word*)t0)[2])+1,t9);
t13=t1;
t14=C_slot(t2,C_fix(1));
t15=C_slot(t3,C_fix(1));
t1=t13;
t2=t14;
t3=t15;
goto loop;}
else{
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* map-loop1524 in k10504 in k10336 in k10329 in k10314 in k10299 in k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in ... */
static void C_fcall f_10448(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(12,0,3)))){
C_save_and_reclaim_args((void *)trf_10448,4,t0,t1,t2,t3);}
a=C_alloc(12);
t4=C_i_pairp(t2);
t5=(C_truep(t4)?C_i_pairp(t3):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_slot(t2,C_fix(0));
t7=C_slot(t3,C_fix(0));
t8=C_a_i_list(&a,3,lf[179],t6,t7);
t9=C_a_i_cons(&a,2,t8,C_SCHEME_END_OF_LIST);
t10=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t9);
t11=C_mutate(((C_word *)((C_word*)t0)[2])+1,t9);
t13=t1;
t14=C_slot(t2,C_fix(1));
t15=C_slot(t3,C_fix(1));
t1=t13;
t2=t14;
t3=t15;
goto loop;}
else{
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* k10504 in k10336 in k10329 in k10314 in k10299 in k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in ... */
static void C_ccall f_10506(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(36,c,4)))){
C_save_and_reclaim((void *)f_10506,c,av);}
a=C_alloc(36);
t2=C_a_i_cons(&a,2,C_SCHEME_END_OF_LIST,t1);
t3=C_a_i_cons(&a,2,lf[23],t2);
t4=C_a_i_cons(&a,2,C_SCHEME_END_OF_LIST,((C_word*)t0)[2]);
t5=C_a_i_cons(&a,2,lf[23],t4);
t6=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10362,a[2]=t3,a[3]=t5,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
t7=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t8=t7;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=((C_word*)t9)[1];
t11=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10374,a[2]=t6,a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],tmp=(C_word)a,a+=5,tmp);
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10448,a[2]=t9,a[3]=t13,a[4]=t10,a[5]=((C_word)li106),tmp=(C_word)a,a+=6,tmp));
t15=((C_word*)t13)[1];
f_10448(t15,t11,((C_word*)t0)[7],((C_word*)t0)[5]);}

/* k10519 in k10336 in k10329 in k10314 in k10299 in k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in ... */
static void C_ccall f_10521(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(20,c,4)))){
C_save_and_reclaim((void *)f_10521,c,av);}
a=C_alloc(20);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10525,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10537,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10547,a[2]=t5,a[3]=t9,a[4]=t6,a[5]=((C_word)li107),tmp=(C_word)a,a+=6,tmp));
t11=((C_word*)t9)[1];
f_10547(t11,t7,((C_word*)t0)[3],((C_word*)t0)[4]);}

/* k10523 in k10519 in k10336 in k10329 in k10314 in k10299 in k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in ... */
static void C_ccall f_10525(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_10525,c,av);}
/* chicken-syntax.scm:458: ##sys#append */
t2=*((C_word*)lf[55]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k10535 in k10519 in k10336 in k10329 in k10314 in k10299 in k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in ... */
static void C_ccall f_10537(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_10537,c,av);}
a=C_alloc(6);
t2=C_a_i_list(&a,1,lf[164]);
t3=C_a_i_list(&a,1,t2);
/* chicken-syntax.scm:458: ##sys#append */
t4=*((C_word*)lf[55]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* map-loop1488 in k10519 in k10336 in k10329 in k10314 in k10299 in k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in ... */
static void C_fcall f_10547(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(12,0,3)))){
C_save_and_reclaim_args((void *)trf_10547,4,t0,t1,t2,t3);}
a=C_alloc(12);
t4=C_i_pairp(t2);
t5=(C_truep(t4)?C_i_pairp(t3):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_slot(t2,C_fix(0));
t7=C_slot(t3,C_fix(0));
t8=C_a_i_list(&a,3,lf[179],t6,t7);
t9=C_a_i_cons(&a,2,t8,C_SCHEME_END_OF_LIST);
t10=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t9);
t11=C_mutate(((C_word *)((C_word*)t0)[2])+1,t9);
t13=t1;
t14=C_slot(t2,C_fix(1));
t15=C_slot(t3,C_fix(1));
t1=t13;
t2=t14;
t3=t15;
goto loop;}
else{
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* map-loop1452 in k10336 in k10329 in k10314 in k10299 in k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in ... */
static void C_fcall f_10595(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(12,0,3)))){
C_save_and_reclaim_args((void *)trf_10595,4,t0,t1,t2,t3);}
a=C_alloc(12);
t4=C_i_pairp(t2);
t5=(C_truep(t4)?C_i_pairp(t3):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_slot(t2,C_fix(0));
t7=C_slot(t3,C_fix(0));
t8=C_a_i_list(&a,3,lf[179],t6,t7);
t9=C_a_i_cons(&a,2,t8,C_SCHEME_END_OF_LIST);
t10=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t9);
t11=C_mutate(((C_word *)((C_word*)t0)[2])+1,t9);
t13=t1;
t14=C_slot(t2,C_fix(1));
t15=C_slot(t3,C_fix(1));
t1=t13;
t2=t14;
t3=t15;
goto loop;}
else{
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* k10649 in k10329 in k10314 in k10299 in k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in ... */
static void C_ccall f_10651(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,4)))){
C_save_and_reclaim((void *)f_10651,c,av);}
a=C_alloc(13);
t2=C_i_check_list_2(((C_word*)t0)[2],lf[67]);
t3=C_i_check_list_2(t1,lf[67]);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10660,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10748,a[2]=((C_word*)t0)[6],a[3]=t6,a[4]=((C_word*)t0)[7],a[5]=((C_word)li111),tmp=(C_word)a,a+=6,tmp));
t8=((C_word*)t6)[1];
f_10748(t8,t4,((C_word*)t0)[2],t1);}

/* k10658 in k10649 in k10329 in k10314 in k10299 in k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in ... */
static void C_ccall f_10660(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,3)))){
C_save_and_reclaim((void *)f_10660,c,av);}
a=C_alloc(18);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10667,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t4,a[6]=t5,tmp=(C_word)a,a+=7,tmp);
t7=C_u_i_length(((C_word*)t0)[4]);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10728,a[2]=t9,a[3]=((C_word)li110),tmp=(C_word)a,a+=4,tmp));
t11=((C_word*)t9)[1];
f_10728(t11,t6,t7);}

/* k10665 in k10658 in k10649 in k10329 in k10314 in k10299 in k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in ... */
static void C_ccall f_10667(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,4)))){
C_save_and_reclaim((void *)f_10667,c,av);}
a=C_alloc(12);
t2=C_i_check_list_2(((C_word*)t0)[2],lf[67]);
t3=C_i_check_list_2(t1,lf[67]);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10676,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10678,a[2]=((C_word*)t0)[5],a[3]=t6,a[4]=((C_word*)t0)[6],a[5]=((C_word)li109),tmp=(C_word)a,a+=6,tmp));
t8=((C_word*)t6)[1];
f_10678(t8,t4,((C_word*)t0)[2],t1);}

/* k10674 in k10665 in k10658 in k10649 in k10329 in k10314 in k10299 in k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in ... */
static void C_ccall f_10676(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_10676,c,av);}
/* chicken-syntax.scm:458: ##sys#append */
t2=*((C_word*)lf[55]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* map-loop1418 in k10665 in k10658 in k10649 in k10329 in k10314 in k10299 in k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in ... */
static void C_fcall f_10678(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(9,0,3)))){
C_save_and_reclaim_args((void *)trf_10678,4,t0,t1,t2,t3);}
a=C_alloc(9);
t4=C_i_pairp(t2);
t5=(C_truep(t4)?C_i_pairp(t3):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_slot(t2,C_fix(0));
t7=C_slot(t3,C_fix(0));
t8=C_a_i_list(&a,2,t6,t7);
t9=C_a_i_cons(&a,2,t8,C_SCHEME_END_OF_LIST);
t10=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t9);
t11=C_mutate(((C_word *)((C_word*)t0)[2])+1,t9);
t13=t1;
t14=C_slot(t2,C_fix(1));
t15=C_slot(t3,C_fix(1));
t1=t13;
t2=t14;
t3=t15;
goto loop;}
else{
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* loop in k10658 in k10649 in k10329 in k10314 in k10299 in k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in ... */
static void C_fcall f_10728(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_10728,3,t0,t1,t2);}
a=C_alloc(3);
t3=C_eqp(t2,C_fix(0));
if(C_truep(t3)){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10742,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:464: loop */
t6=t4;
t7=C_fixnum_difference(t2,C_fix(1));
t1=t6;
t2=t7;
goto loop;}}

/* k10740 in loop in k10658 in k10649 in k10329 in k10314 in k10299 in k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in ... */
static void C_ccall f_10742(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_10742,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,C_SCHEME_FALSE,t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* map-loop1361 in k10649 in k10329 in k10314 in k10299 in k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in ... */
static void C_fcall f_10748(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(9,0,3)))){
C_save_and_reclaim_args((void *)trf_10748,4,t0,t1,t2,t3);}
a=C_alloc(9);
t4=C_i_pairp(t2);
t5=(C_truep(t4)?C_i_pairp(t3):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_slot(t2,C_fix(0));
t7=C_slot(t3,C_fix(0));
t8=C_a_i_list(&a,2,t6,t7);
t9=C_a_i_cons(&a,2,t8,C_SCHEME_END_OF_LIST);
t10=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t9);
t11=C_mutate(((C_word *)((C_word*)t0)[2])+1,t9);
t13=t1;
t14=C_slot(t2,C_fix(1));
t15=C_slot(t3,C_fix(1));
t1=t13;
t2=t14;
t3=t15;
goto loop;}
else{
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* map-loop1382 in k10329 in k10314 in k10299 in k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in ... */
static void C_fcall f_10796(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_10796,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_cadr(t3);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* map-loop1331 in k10314 in k10299 in k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in ... */
static void C_fcall f_10830(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_10830,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10855,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:457: g1337 */
t4=((C_word*)t0)[4];
f_10320(t4,t3);}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k10853 in map-loop1331 in k10314 in k10299 in k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in ... */
static void C_ccall f_10855(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_10855,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_10830(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* map-loop1303 in k10299 in k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in ... */
static void C_fcall f_10864(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_10864,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10889,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:456: g1309 */
t4=((C_word*)t0)[4];
f_10305(t4,t3);}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k10887 in map-loop1303 in k10299 in k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in ... */
static void C_ccall f_10889(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_10889,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_10864(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* map-loop1276 in k10285 in a10282 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in ... */
static void C_fcall f_10898(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_10898,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_car(t3);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k10932 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in ... */
static void C_ccall f_10934(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_10934,c,av);}
/* chicken-syntax.scm:441: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[184];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a10935 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in ... */
static void C_ccall f_10936(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_10936,c,av);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10940,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:445: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[184];
av2[3]=t2;
av2[4]=lf[187];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k10938 in a10935 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in ... */
static void C_ccall f_10940(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_10940,c,av);}
a=C_alloc(9);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_list(&a,3,lf[185],t2,*((C_word*)lf[186]+1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k10949 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in ... */
static void C_ccall f_10951(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_10951,c,av);}
/* chicken-syntax.scm:434: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[188];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a10952 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in ... */
static void C_ccall f_10953(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_10953,c,av);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10957,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:438: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[188];
av2[3]=t2;
av2[4]=lf[189];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k10955 in a10952 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in ... */
static void C_ccall f_10957(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_10957,c,av);}
a=C_alloc(9);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_list(&a,3,lf[185],t2,C_SCHEME_FALSE);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k10966 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in ... */
static void C_ccall f_10968(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_10968,c,av);}
/* chicken-syntax.scm:426: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[190];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a10969 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in ... */
static void C_ccall f_10970(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_10970,c,av);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10974,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:431: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[190];
av2[3]=t2;
av2[4]=lf[192];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k10972 in a10969 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in ... */
static void C_ccall f_10974(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,1)))){
C_save_and_reclaim((void *)f_10974,c,av);}
a=C_alloc(15);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=C_a_i_list(&a,3,lf[23],C_SCHEME_END_OF_LIST,t2);
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_list(&a,2,lf[191],t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k10987 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in ... */
static void C_ccall f_10989(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_10989,c,av);}
/* chicken-syntax.scm:420: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[193];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a10990 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in ... */
static void C_ccall f_10991(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_10991,c,av);}
a=C_alloc(3);
t5=C_i_cdr(t2);
t6=t1;{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_a_i_cons(&a,2,lf[194],t5);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}

/* k11001 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in ... */
static void C_ccall f_11003(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11003,c,av);}
/* chicken-syntax.scm:401: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[195];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a11004 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in ... */
static void C_ccall f_11005(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_11005,c,av);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11009,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:406: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[195];
av2[3]=t2;
av2[4]=lf[198];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k11007 in a11004 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in ... */
static void C_ccall f_11009(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,4)))){
C_save_and_reclaim((void *)f_11009,c,av);}
a=C_alloc(15);
t2=C_i_cddr(((C_word*)t0)[2]);
if(C_truep(C_i_nullp(t2))){
t3=C_u_i_cdr(((C_word*)t0)[2]);
t4=C_a_i_cons(&a,2,C_SCHEME_END_OF_LIST,t3);
t5=C_a_i_cons(&a,2,lf[23],t4);
t6=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_a_i_list(&a,3,lf[28],t5,lf[196]);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11031,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:410: ##sys#check-syntax */
t4=*((C_word*)lf[45]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[195];
av2[3]=((C_word*)t0)[2];
av2[4]=lf[197];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}}

/* k11029 in k11007 in a11004 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in ... */
static void C_ccall f_11031(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,1)))){
C_save_and_reclaim((void *)f_11031,c,av);}
a=C_alloc(24);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=C_i_caddr(((C_word*)t0)[2]);
t4=C_u_i_cdr(((C_word*)t0)[2]);
t5=C_u_i_cdr(t4);
t6=C_u_i_cdr(t5);
t7=C_i_pairp(t2);
t8=(C_truep(t7)?C_i_nullp(C_u_i_cdr(t2)):C_SCHEME_FALSE);
if(C_truep(t8)){
t9=C_i_car(t2);
t10=C_a_i_list(&a,2,t9,t3);
t11=C_a_i_list(&a,1,t10);
t12=C_a_i_cons(&a,2,t11,t6);
t13=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t13;
av2[1]=C_a_i_cons(&a,2,lf[37],t12);
((C_proc)(void*)(*((C_word*)t13+1)))(2,av2);}}
else{
t9=C_a_i_list(&a,3,lf[23],C_SCHEME_END_OF_LIST,t3);
t10=C_a_i_cons(&a,2,t2,t6);
t11=C_a_i_cons(&a,2,lf[23],t10);
t12=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t12;
av2[1]=C_a_i_list(&a,3,lf[28],t9,t11);
((C_proc)(void*)(*((C_word*)t12+1)))(2,av2);}}}

/* k11094 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in ... */
static void C_ccall f_11096(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11096,c,av);}
/* chicken-syntax.scm:329: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[199];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a11097 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in ... */
static void C_ccall f_11098(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_11098,c,av);}
a=C_alloc(6);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11102,a[2]=t2,a[3]=t4,a[4]=t1,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:333: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[199];
av2[3]=t2;
av2[4]=lf[209];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k11100 in a11097 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in ... */
static void C_ccall f_11102(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_11102,c,av);}
a=C_alloc(7);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11108,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t2,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* chicken-syntax.scm:335: chicken.syntax#strip-syntax */
t4=*((C_word*)lf[39]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k11106 in k11100 in a11097 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in ... */
static void C_ccall f_11108(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_11108,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_11111,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t1,tmp=(C_word)a,a+=8,tmp);
/* chicken-syntax.scm:336: scheme#symbol->string */
t3=*((C_word*)lf[120]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k11109 in k11106 in k11100 in a11097 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in ... */
static void C_ccall f_11111(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_11111,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_11114,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11476,a[2]=t2,a[3]=((C_word*)t0)[7],tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:337: ##sys#current-module */
t4=*((C_word*)lf[85]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k11112 in k11109 in k11106 in k11100 in a11097 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in ... */
static void C_ccall f_11114(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_11114,c,av);}
a=C_alloc(9);
t2=C_i_cddr(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_11120,a[2]=((C_word*)t0)[3],a[3]=t2,a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
/* chicken-syntax.scm:342: r */
t4=((C_word*)t0)[7];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[81];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k11118 in k11112 in k11109 in k11106 in k11100 in a11097 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in ... */
static void C_ccall f_11120(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_11120,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_11123,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
/* chicken-syntax.scm:343: r */
t3=((C_word*)t0)[8];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[208];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k11121 in k11118 in k11112 in k11109 in k11106 in k11100 in a11097 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in ... */
static void C_ccall f_11123(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_11123,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_11126,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
/* chicken-syntax.scm:344: r */
t3=((C_word*)t0)[9];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[80];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k11124 in k11121 in k11118 in k11112 in k11109 in k11106 in k11100 in a11097 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in ... */
static void C_ccall f_11126(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(28,c,3)))){
C_save_and_reclaim((void *)f_11126,c,av);}
a=C_alloc(28);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11130,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word)li122),tmp=(C_word)a,a+=5,tmp);
t7=C_i_check_list_2(((C_word*)t0)[4],lf[67]);
t8=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_11190,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[7],a[5]=((C_word*)t0)[8],a[6]=t1,a[7]=((C_word*)t0)[9],a[8]=((C_word*)t0)[4],tmp=(C_word)a,a+=9,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11441,a[2]=t4,a[3]=t10,a[4]=t6,a[5]=t5,a[6]=((C_word)li124),tmp=(C_word)a,a+=7,tmp));
t12=((C_word*)t10)[1];
f_11441(t12,t8,((C_word*)t0)[4]);}

/* g1163 in k11124 in k11121 in k11118 in k11112 in k11109 in k11106 in k11100 in a11097 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in ... */
static void C_fcall f_11130(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,4)))){
C_save_and_reclaim_args((void *)trf_11130,3,t0,t1,t2);}
a=C_alloc(9);
if(C_truep(C_i_symbolp(t2))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11143,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_i_pairp(t2))){
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11158,a[2]=t2,a[3]=t1,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:349: c */
t5=((C_word*)t0)[2];{
C_word av2[4];
av2[0]=t5;
av2[1]=t4;
av2[2]=C_u_i_car(t2);
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}
else{
/* chicken-syntax.scm:355: chicken.syntax#syntax-error */
t4=*((C_word*)lf[58]+1);{
C_word av2[5];
av2[0]=t4;
av2[1]=t1;
av2[2]=lf[199];
av2[3]=lf[200];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}}}

/* k11141 in g1163 in k11124 in k11121 in k11118 in k11112 in k11109 in k11106 in k11100 in a11097 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in ... */
static void C_fcall f_11143(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,4)))){
C_save_and_reclaim_args((void *)trf_11143,2,t0,t1);}
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t2;
av2[1]=C_i_cadr(((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
/* chicken-syntax.scm:355: chicken.syntax#syntax-error */
t2=*((C_word*)lf[58]+1);{
C_word av2[5];
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[199];
av2[3]=lf[200];
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}}

/* k11156 in g1163 in k11124 in k11121 in k11118 in k11112 in k11109 in k11106 in k11100 in a11097 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in ... */
static void C_ccall f_11158(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11158,c,av);}
if(C_truep(t1)){
if(C_truep(C_i_pairp(C_u_i_cdr(((C_word*)t0)[2])))){
t2=C_i_cadr(((C_word*)t0)[2]);
if(C_truep(C_i_symbolp(t2))){
t3=C_u_i_cdr(((C_word*)t0)[2]);
if(C_truep(C_i_nullp(C_u_i_cdr(t3)))){
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_i_cadr(((C_word*)t0)[2]);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
/* chicken-syntax.scm:355: chicken.syntax#syntax-error */
t4=*((C_word*)lf[58]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[199];
av2[3]=lf[200];
av2[4]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}}
else{
/* chicken-syntax.scm:355: chicken.syntax#syntax-error */
t3=*((C_word*)lf[58]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[199];
av2[3]=lf[200];
av2[4]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}}
else{
t2=((C_word*)t0)[4];
f_11143(t2,C_SCHEME_FALSE);}}
else{
t2=((C_word*)t0)[4];
f_11143(t2,C_SCHEME_FALSE);}}

/* k11188 in k11124 in k11121 in k11118 in k11112 in k11109 in k11106 in k11100 in a11097 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in ... */
static void C_ccall f_11190(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(28,c,3)))){
C_save_and_reclaim((void *)f_11190,c,av);}
a=C_alloc(28);
t2=C_a_i_list(&a,2,lf[38],((C_word*)t0)[2]);
t3=C_a_i_list(&a,3,((C_word*)t0)[3],((C_word*)t0)[4],t2);
t4=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_11413,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=t3,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11433,a[2]=t4,tmp=(C_word)a,a+=3,tmp);
/* ##sys#string-append */
t6=*((C_word*)lf[205]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[207];
av2[3]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* k11219 in k11385 in k11411 in k11188 in k11124 in k11121 in k11118 in k11112 in k11109 in k11106 in k11100 in a11097 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in ... */
static void C_ccall f_11221(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,1)))){
C_save_and_reclaim((void *)f_11221,c,av);}
a=C_alloc(12);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_cons(&a,2,((C_word*)t0)[3],t2);
t4=C_a_i_cons(&a,2,((C_word*)t0)[4],t3);
t5=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_a_i_cons(&a,2,lf[29],t4);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* mapslots in k11385 in k11411 in k11188 in k11124 in k11121 in k11118 in k11112 in k11109 in k11106 in k11100 in a11097 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in ... */
static void C_fcall f_11223(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,0,2)))){
C_save_and_reclaim_args((void *)trf_11223,4,t0,t1,t2,t3);}
a=C_alloc(11);
t4=C_eqp(t2,C_SCHEME_END_OF_LIST);
if(C_truep(t4)){
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t5=C_i_car(t2);
t6=C_i_symbolp(t5);
t7=C_i_not(t6);
t8=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_11239,a[2]=((C_word*)t0)[2],a[3]=t3,a[4]=t1,a[5]=((C_word*)t0)[3],a[6]=t2,a[7]=t7,a[8]=((C_word*)t0)[4],a[9]=((C_word*)t0)[5],a[10]=((C_word*)t0)[6],tmp=(C_word)a,a+=11,tmp);
if(C_truep(t7)){
/* chicken-syntax.scm:373: scheme#symbol->string */
t9=*((C_word*)lf[120]+1);{
C_word av2[3];
av2[0]=t9;
av2[1]=t8;
av2[2]=C_i_cadr(t5);
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}
else{
/* chicken-syntax.scm:373: scheme#symbol->string */
t9=*((C_word*)lf[120]+1);{
C_word av2[3];
av2[0]=t9;
av2[1]=t8;
av2[2]=t5;
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}}}

/* k11237 in mapslots in k11385 in k11411 in k11188 in k11124 in k11121 in k11118 in k11112 in k11109 in k11106 in k11100 in a11097 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in ... */
static void C_ccall f_11239(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,5)))){
C_save_and_reclaim((void *)f_11239,c,av);}
a=C_alloc(15);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_11242,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=t1,tmp=(C_word)a,a+=12,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11372,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:374: scheme#string-append */
t4=*((C_word*)lf[40]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[10];
av2[3]=lf[203];
av2[4]=t1;
av2[5]=lf[204];
((C_proc)(void*)(*((C_word*)t4+1)))(6,av2);}}

/* k11240 in k11237 in mapslots in k11385 in k11411 in k11188 in k11124 in k11121 in k11118 in k11112 in k11109 in k11106 in k11100 in a11097 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in ... */
static void C_ccall f_11242(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,4)))){
C_save_and_reclaim((void *)f_11242,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_11245,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=t1,tmp=(C_word)a,a+=11,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11368,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:375: scheme#string-append */
t4=*((C_word*)lf[40]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[10];
av2[3]=lf[202];
av2[4]=((C_word*)t0)[11];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k11243 in k11240 in k11237 in mapslots in k11385 in k11411 in k11188 in k11124 in k11121 in k11118 in k11112 in k11109 in k11106 in k11100 in a11097 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in ... */
static void C_ccall f_11245(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(79,c,2)))){
C_save_and_reclaim((void *)f_11245,c,av);}
a=C_alloc(79);
t2=C_a_i_list(&a,2,lf[79],lf[201]);
t3=C_a_i_list(&a,2,lf[38],((C_word*)t0)[2]);
t4=C_a_i_list(&a,3,lf[73],lf[79],t3);
t5=C_a_i_list(&a,2,lf[34],t4);
t6=C_a_i_list(&a,4,lf[76],lf[79],((C_word*)t0)[3],lf[201]);
t7=C_a_i_list(&a,4,lf[23],t2,t5,t6);
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11271,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
t9=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_11275,a[2]=((C_word*)t0)[7],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[8],a[6]=t7,a[7]=((C_word*)t0)[9],a[8]=t1,a[9]=t8,tmp=(C_word)a,a+=10,tmp);
if(C_truep(((C_word*)t0)[7])){
t10=t9;
f_11275(t10,C_SCHEME_END_OF_LIST);}
else{
t10=C_a_i_list(&a,3,((C_word*)t0)[9],((C_word*)t0)[10],t7);
t11=t9;
f_11275(t11,C_a_i_list(&a,1,t10));}}

/* k11257 in k11269 in k11243 in k11240 in k11237 in mapslots in k11385 in k11411 in k11188 in k11124 in k11121 in k11118 in k11112 in k11109 in k11106 in k11100 in a11097 in k5567 in k5564 in k5560 in k5557 in k5554 in ... */
static void C_ccall f_11259(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_11259,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k11269 in k11243 in k11240 in k11237 in mapslots in k11385 in k11411 in k11188 in k11124 in k11121 in k11118 in k11112 in k11109 in k11106 in k11100 in a11097 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in ... */
static void C_ccall f_11271(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_11271,c,av);}
a=C_alloc(7);
t2=C_a_i_cons(&a,2,lf[29],t1);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11259,a[2]=((C_word*)t0)[2],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:399: mapslots */
t4=((C_word*)((C_word*)t0)[3])[1];
f_11223(t4,t3,C_slot(((C_word*)t0)[4],C_fix(1)),C_fixnum_plus(((C_word*)t0)[5],C_fix(1)));}

/* k11273 in k11243 in k11240 in k11237 in mapslots in k11385 in k11411 in k11188 in k11124 in k11121 in k11118 in k11112 in k11109 in k11106 in k11100 in a11097 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in ... */
static void C_fcall f_11275(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(66,0,3)))){
C_save_and_reclaim_args((void *)trf_11275,2,t0,t1);}
a=C_alloc(66);
if(C_truep(((C_word*)t0)[2])){
t2=C_a_i_list(&a,1,lf[79]);
t3=C_a_i_list(&a,2,lf[38],((C_word*)t0)[3]);
t4=C_a_i_list(&a,3,lf[73],lf[79],t3);
t5=C_a_i_list(&a,2,lf[34],t4);
t6=C_a_i_list(&a,3,lf[74],lf[79],((C_word*)t0)[4]);
t7=C_a_i_list(&a,4,lf[23],t2,t5,t6);
t8=C_a_i_list(&a,3,((C_word*)t0)[5],t7,((C_word*)t0)[6]);
t9=C_a_i_list(&a,3,((C_word*)t0)[7],((C_word*)t0)[8],t8);
t10=C_a_i_list(&a,1,t9);
/* chicken-syntax.scm:382: ##sys#append */
t11=*((C_word*)lf[55]+1);{
C_word av2[4];
av2[0]=t11;
av2[1]=((C_word*)t0)[9];
av2[2]=t1;
av2[3]=t10;
((C_proc)(void*)(*((C_word*)t11+1)))(4,av2);}}
else{
t2=C_a_i_list(&a,1,lf[79]);
t3=C_a_i_list(&a,2,lf[38],((C_word*)t0)[3]);
t4=C_a_i_list(&a,3,lf[73],lf[79],t3);
t5=C_a_i_list(&a,2,lf[34],t4);
t6=C_a_i_list(&a,3,lf[74],lf[79],((C_word*)t0)[4]);
t7=C_a_i_list(&a,4,lf[23],t2,t5,t6);
t8=C_a_i_list(&a,3,((C_word*)t0)[7],((C_word*)t0)[8],t7);
t9=C_a_i_list(&a,1,t8);
/* chicken-syntax.scm:382: ##sys#append */
t10=*((C_word*)lf[55]+1);{
C_word av2[4];
av2[0]=t10;
av2[1]=((C_word*)t0)[9];
av2[2]=t1;
av2[3]=t9;
((C_proc)(void*)(*((C_word*)t10+1)))(4,av2);}}}

/* k11366 in k11240 in k11237 in mapslots in k11385 in k11411 in k11188 in k11124 in k11121 in k11118 in k11112 in k11109 in k11106 in k11100 in a11097 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in ... */
static void C_ccall f_11368(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_11368,c,av);}
/* chicken-syntax.scm:375: scheme#string->symbol */
t2=*((C_word*)lf[119]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k11370 in k11237 in mapslots in k11385 in k11411 in k11188 in k11124 in k11121 in k11118 in k11112 in k11109 in k11106 in k11100 in a11097 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in ... */
static void C_ccall f_11372(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_11372,c,av);}
/* chicken-syntax.scm:374: scheme#string->symbol */
t2=*((C_word*)lf[119]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k11385 in k11411 in k11188 in k11124 in k11121 in k11118 in k11112 in k11109 in k11106 in k11100 in a11097 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in ... */
static void C_ccall f_11387(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(52,c,4)))){
C_save_and_reclaim((void *)f_11387,c,av);}
a=C_alloc(52);
t2=C_a_i_list(&a,1,lf[79]);
t3=C_a_i_list(&a,2,lf[38],((C_word*)t0)[2]);
t4=C_a_i_list(&a,3,lf[72],lf[79],t3);
t5=C_a_i_list(&a,3,lf[23],t2,t4);
t6=C_a_i_list(&a,3,((C_word*)t0)[3],t1,t5);
t7=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11221,a[2]=t6,a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],tmp=(C_word)a,a+=6,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_11223,a[2]=((C_word*)t0)[2],a[3]=t9,a[4]=((C_word*)t0)[7],a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[8],a[7]=((C_word)li123),tmp=(C_word)a,a+=8,tmp));
t11=((C_word*)t9)[1];
f_11223(t11,t7,((C_word*)t0)[9],C_fix(1));}

/* k11405 in k11411 in k11188 in k11124 in k11121 in k11118 in k11112 in k11109 in k11106 in k11100 in a11097 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in ... */
static void C_ccall f_11407(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_11407,c,av);}
/* chicken-syntax.scm:366: scheme#string->symbol */
t2=*((C_word*)lf[119]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k11411 in k11188 in k11124 in k11121 in k11118 in k11112 in k11109 in k11106 in k11100 in a11097 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in ... */
static void C_ccall f_11413(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(43,c,3)))){
C_save_and_reclaim((void *)f_11413,c,av);}
a=C_alloc(43);
t2=C_a_i_list(&a,2,lf[38],((C_word*)t0)[2]);
t3=C_a_i_cons(&a,2,t2,((C_word*)t0)[3]);
t4=C_a_i_cons(&a,2,lf[71],t3);
t5=C_a_i_list(&a,3,lf[23],((C_word*)t0)[3],t4);
t6=C_a_i_list(&a,3,((C_word*)t0)[4],t1,t5);
t7=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_11387,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[4],a[4]=t6,a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],tmp=(C_word)a,a+=10,tmp);
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11407,a[2]=t7,tmp=(C_word)a,a+=3,tmp);
/* ##sys#string-append */
t9=*((C_word*)lf[205]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t9;
av2[1]=t8;
av2[2]=((C_word*)t0)[8];
av2[3]=lf[206];
((C_proc)(void*)(*((C_word*)t9+1)))(4,av2);}}

/* k11431 in k11188 in k11124 in k11121 in k11118 in k11112 in k11109 in k11106 in k11100 in a11097 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in ... */
static void C_ccall f_11433(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_11433,c,av);}
/* chicken-syntax.scm:361: scheme#string->symbol */
t2=*((C_word*)lf[119]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* map-loop1157 in k11124 in k11121 in k11118 in k11112 in k11109 in k11106 in k11100 in a11097 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in ... */
static void C_fcall f_11441(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_11441,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11466,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:346: g1163 */
t4=((C_word*)t0)[4];
f_11130(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k11464 in map-loop1157 in k11124 in k11121 in k11118 in k11112 in k11109 in k11106 in k11100 in a11097 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in ... */
static void C_ccall f_11466(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_11466,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_11441(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k11474 in k11109 in k11106 in k11100 in a11097 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in ... */
static void C_ccall f_11476(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_11476,c,av);}
a=C_alloc(7);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11483,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11487,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:339: ##sys#current-module */
t4=*((C_word*)lf[85]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
f_11114(2,av2);}}}

/* k11481 in k11474 in k11109 in k11106 in k11100 in a11097 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in ... */
static void C_ccall f_11483(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11483,c,av);}
/* chicken-syntax.scm:338: chicken.base#symbol-append */
t2=*((C_word*)lf[82]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=lf[83];
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k11485 in k11474 in k11109 in k11106 in k11100 in a11097 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in ... */
static void C_ccall f_11487(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_11487,c,av);}
/* chicken-syntax.scm:339: ##sys#module-name */
t2=*((C_word*)lf[84]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k11489 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in ... */
static void C_ccall f_11491(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11491,c,av);}
/* chicken-syntax.scm:321: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[210];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a11492 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in ... */
static void C_ccall f_11493(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_11493,c,av);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11497,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:326: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[210];
av2[3]=t2;
av2[4]=lf[212];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k11495 in a11492 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in ... */
static void C_ccall f_11497(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_11497,c,av);}
a=C_alloc(3);
t2=C_i_cdr(((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_cons(&a,2,lf[211],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k11506 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_11508(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11508,c,av);}
/* chicken-syntax.scm:307: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[213];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a11509 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_11510(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(14,c,2)))){
C_save_and_reclaim((void *)f_11510,c,av);}
a=C_alloc(14);
t5=C_i_cdr(t2);
t6=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11520,a[2]=t5,a[3]=t1,a[4]=t3,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_i_pairp(t5))){
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11549,a[2]=t5,a[3]=t6,tmp=(C_word)a,a+=4,tmp);
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11605,a[2]=t5,a[3]=t7,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:309: ##sys#list? */
t9=*((C_word*)lf[220]+1);{
C_word *av2=av;
av2[0]=t9;
av2[1]=t8;
av2[2]=C_i_car(t5);
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}
else{
t7=t6;{
C_word *av2=av;
av2[0]=t7;
av2[1]=C_SCHEME_FALSE;
f_11520(2,av2);}}}

/* k11518 in a11509 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 in ... */
static void C_ccall f_11520(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_11520,c,av);}
a=C_alloc(5);
if(C_truep(t1)){
t2=C_i_car(((C_word*)t0)[2]);
t3=C_i_cdr(((C_word*)t0)[2]);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11533,a[2]=t2,a[3]=t3,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:309: rename1091 */
t5=((C_word*)t0)[4];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=lf[214];
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}
else{
/* chicken-syntax.scm:309: chicken.internal.syntax-rules#syntax-rules-mismatch */
t2=*((C_word*)lf[215]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}}

/* k11531 in k11518 in a11509 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in ... */
static void C_ccall f_11533(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_11533,c,av);}
a=C_alloc(6);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],((C_word*)t0)[3]);
t3=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_cons(&a,2,t1,t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k11547 in a11509 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 in ... */
static void C_ccall f_11549(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_11549,c,av);}
a=C_alloc(4);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11555,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:309: ##sys#list? */
t3=*((C_word*)lf[220]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_i_cdr(((C_word*)t0)[2]);
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
f_11520(2,av2);}}}

/* k11553 in k11547 in a11509 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in ... */
static void C_ccall f_11555(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_11555,c,av);}
a=C_alloc(4);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11558,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:309: ##sys#length */
t3=*((C_word*)lf[219]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_i_cdr(((C_word*)t0)[2]);
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
f_11520(2,av2);}}}

/* k11556 in k11553 in k11547 in a11509 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in ... */
static void C_ccall f_11558(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_11558,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11564,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:309: ##sys#>= */
t3=*((C_word*)lf[218]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=t1;
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k11562 in k11556 in k11553 in k11547 in a11509 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in ... */
static void C_ccall f_11564(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_11564,c,av);}
a=C_alloc(6);
if(C_truep(t1)){
t2=C_i_cdr(((C_word*)t0)[2]);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11573,a[2]=t4,a[3]=((C_word)li127),tmp=(C_word)a,a+=4,tmp));
t6=((C_word*)t4)[1];
f_11573(t6,((C_word*)t0)[3],t2,((C_word*)t0)[4]);}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
f_11520(2,av2);}}}

/* loop1088 in k11562 in k11556 in k11553 in k11547 in a11509 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in ... */
static void C_fcall f_11573(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_11573,4,t0,t1,t2,t3);}
a=C_alloc(6);
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11580,a[2]=t1,a[3]=t2,a[4]=((C_word*)t0)[2],a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:309: ##sys#= */
t5=*((C_word*)lf[217]+1);{
C_word av2[4];
av2[0]=t5;
av2[1]=t4;
av2[2]=t3;
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k11578 in loop1088 in k11562 in k11556 in k11553 in k11547 in a11509 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in ... */
static void C_ccall f_11580(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_11580,c,av);}
a=C_alloc(5);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_eqp(((C_word*)t0)[3],C_SCHEME_END_OF_LIST);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_i_cdr(((C_word*)t0)[3]);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11594,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:309: ##sys#+ */
t4=*((C_word*)lf[216]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[5];
av2[3]=C_fix(-1);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}}

/* k11592 in k11578 in loop1088 in k11562 in k11556 in k11553 in k11547 in a11509 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in ... */
static void C_ccall f_11594(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_11594,c,av);}
/* chicken-syntax.scm:309: loop1088 */
t2=((C_word*)((C_word*)t0)[2])[1];
f_11573(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* k11603 in a11509 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 in ... */
static void C_ccall f_11605(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_11605,c,av);}
a=C_alloc(4);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11608,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:309: ##sys#length */
t3=*((C_word*)lf[219]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_i_car(((C_word*)t0)[2]);
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
f_11549(2,av2);}}}

/* k11606 in k11603 in a11509 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in ... */
static void C_ccall f_11608(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_11608,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11614,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:309: ##sys#>= */
t3=*((C_word*)lf[218]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=t1;
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k11612 in k11606 in k11603 in a11509 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in ... */
static void C_ccall f_11614(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_11614,c,av);}
a=C_alloc(6);
if(C_truep(t1)){
t2=C_i_car(((C_word*)t0)[2]);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11623,a[2]=t4,a[3]=((C_word)li128),tmp=(C_word)a,a+=4,tmp));
t6=((C_word*)t4)[1];
f_11623(t6,((C_word*)t0)[3],t2,((C_word*)t0)[4]);}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
f_11549(2,av2);}}}

/* loop1088 in k11612 in k11606 in k11603 in a11509 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in ... */
static void C_fcall f_11623(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_11623,4,t0,t1,t2,t3);}
a=C_alloc(6);
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11630,a[2]=t1,a[3]=t2,a[4]=((C_word*)t0)[2],a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:309: ##sys#= */
t5=*((C_word*)lf[217]+1);{
C_word av2[4];
av2[0]=t5;
av2[1]=t4;
av2[2]=t3;
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k11628 in loop1088 in k11612 in k11606 in k11603 in a11509 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in ... */
static void C_ccall f_11630(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_11630,c,av);}
a=C_alloc(5);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_eqp(((C_word*)t0)[3],C_SCHEME_END_OF_LIST);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_i_cdr(((C_word*)t0)[3]);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11644,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:309: ##sys#+ */
t4=*((C_word*)lf[216]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[5];
av2[3]=C_fix(-1);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}}

/* k11642 in k11628 in loop1088 in k11612 in k11606 in k11603 in a11509 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in ... */
static void C_ccall f_11644(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_11644,c,av);}
/* chicken-syntax.scm:309: loop1088 */
t2=((C_word*)((C_word*)t0)[2])[1];
f_11623(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* k11654 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_11656(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11656,c,av);}
/* chicken-syntax.scm:299: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[221];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a11657 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_11658(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_11658,c,av);}
a=C_alloc(6);
t5=C_i_cdr(t2);
t6=C_i_pairp(t5);
t7=(C_truep(t6)?C_eqp(C_i_cdr(t5),C_SCHEME_END_OF_LIST):C_SCHEME_FALSE);
if(C_truep(t7)){
t8=C_i_car(t5);
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11678,a[2]=t8,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:301: rename1057 */
t10=t3;{
C_word *av2=av;
av2[0]=t10;
av2[1]=t9;
av2[2]=lf[222];
((C_proc)(void*)(*((C_word*)t10+1)))(3,av2);}}
else{
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11692,a[2]=t5,a[3]=t1,a[4]=t3,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_i_pairp(t5))){
t9=C_i_cdr(t5);
t10=C_i_pairp(t9);
t11=t8;
f_11692(t11,(C_truep(t10)?C_eqp(C_i_cdr(t9),C_SCHEME_END_OF_LIST):C_SCHEME_FALSE));}
else{
t9=t8;
f_11692(t9,C_SCHEME_FALSE);}}}

/* k11676 in a11657 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_11678(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_11678,c,av);}
a=C_alloc(9);
t2=C_a_i_cons(&a,2,C_SCHEME_FALSE,C_SCHEME_END_OF_LIST);
t3=C_a_i_cons(&a,2,((C_word*)t0)[2],t2);
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_cons(&a,2,t1,t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k11690 in a11657 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_fcall f_11692(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_11692,2,t0,t1);}
a=C_alloc(5);
if(C_truep(t1)){
t2=C_i_car(((C_word*)t0)[2]);
t3=C_i_cdr(((C_word*)t0)[2]);
t4=C_i_car(t3);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11705,a[2]=t4,a[3]=t2,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:301: rename1057 */
t6=((C_word*)t0)[4];{
C_word av2[3];
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[222];
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}
else{
/* chicken-syntax.scm:301: chicken.internal.syntax-rules#syntax-rules-mismatch */
t2=*((C_word*)lf[215]+1);{
C_word av2[3];
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}}

/* k11703 in k11690 in a11657 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 in ... */
static void C_ccall f_11705(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_11705,c,av);}
a=C_alloc(9);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],C_SCHEME_END_OF_LIST);
t3=C_a_i_cons(&a,2,((C_word*)t0)[3],t2);
t4=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_cons(&a,2,t1,t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k11751 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_11753(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11753,c,av);}
/* chicken-syntax.scm:288: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[223];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a11754 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_11755(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_11755,c,av);}
a=C_alloc(5);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11759,a[2]=t2,a[3]=t1,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:292: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[223];
av2[3]=t2;
av2[4]=lf[225];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k11757 in a11754 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_11759(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_11759,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11766,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:293: r */
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[224];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k11764 in k11757 in a11754 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_11766(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_11766,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11774,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:294: r */
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[81];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k11772 in k11764 in k11757 in a11754 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 in ... */
static void C_ccall f_11774(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_11774,c,av);}
a=C_alloc(9);
t2=C_i_cdr(((C_word*)t0)[2]);
t3=C_a_i_cons(&a,2,t1,t2);
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_list(&a,2,((C_word*)t0)[4],t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k11780 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_11782(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11782,c,av);}
/* chicken-syntax.scm:280: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[224];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a11783 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_11784(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_11784,c,av);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11788,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:284: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[224];
av2[3]=t2;
av2[4]=lf[228];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k11786 in a11783 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_11788(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_11788,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11791,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=C_i_cdr(((C_word*)t0)[2]);
t4=C_a_i_cons(&a,2,lf[29],t3);
/* chicken-syntax.scm:285: ##sys#register-meta-expression */
t5=*((C_word*)lf[227]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k11789 in k11786 in a11783 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_11791(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_11791,c,av);}
a=C_alloc(9);
t2=C_u_i_cdr(((C_word*)t0)[2]);
t3=C_a_i_cons(&a,2,lf[29],t2);
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_list(&a,2,lf[226],t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k11810 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_11812(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11812,c,av);}
/* chicken-syntax.scm:272: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[229];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a11813 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_11814(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_11814,c,av);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11818,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:277: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[229];
av2[3]=t2;
av2[4]=lf[231];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k11816 in a11813 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_11818(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_11818,c,av);}
a=C_alloc(6);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_list(&a,2,lf[230],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k11827 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_11829(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11829,c,av);}
/* chicken-syntax.scm:247: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[232];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a11830 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_11831(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_11831,c,av);}
a=C_alloc(5);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11835,a[2]=t1,a[3]=t2,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:251: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[232];
av2[3]=t2;
av2[4]=lf[240];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k11833 in a11830 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_11835(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_11835,c,av);}
a=C_alloc(5);
t2=C_i_memq(lf[233],*((C_word*)lf[234]+1));
if(C_truep(C_i_not(t2))){
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=lf[235];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11844,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:254: chicken.syntax#strip-syntax */
t4=*((C_word*)lf[39]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_i_cadr(((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}}

/* k11842 in k11833 in a11830 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_11844(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_11844,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11847,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:255: r */
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[239];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k11845 in k11842 in k11833 in a11830 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_11847(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_11847,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11850,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:256: chicken.syntax#strip-syntax */
t3=*((C_word*)lf[39]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_i_caddr(((C_word*)t0)[4]);
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k11848 in k11845 in k11842 in k11833 in a11830 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_11850(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,4)))){
C_save_and_reclaim((void *)f_11850,c,av);}
a=C_alloc(18);
t2=C_a_i_list(&a,2,((C_word*)t0)[2],((C_word*)t0)[3]);
t3=C_a_i_list(&a,2,((C_word*)t0)[2],lf[236]);
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11873,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=t3,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:262: chicken.compiler.scrutinizer#check-and-validate-type */
t5=*((C_word*)lf[238]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=t1;
av2[3]=lf[232];
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}

/* k11871 in k11848 in k11845 in k11842 in k11833 in a11830 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_11873(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,1)))){
C_save_and_reclaim((void *)f_11873,c,av);}
a=C_alloc(24);
t2=C_a_i_list(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_list(&a,4,lf[237],((C_word*)t0)[3],((C_word*)t0)[4],t2);
t4=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_list(&a,2,lf[226],t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k11887 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_11889(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11889,c,av);}
/* chicken-syntax.scm:222: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[241];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a11890 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_11891(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_11891,c,av);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11895,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:226: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[241];
av2[3]=t2;
av2[4]=lf[244];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k11893 in a11890 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_11895(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_11895,c,av);}
a=C_alloc(5);
t2=C_i_memq(lf[233],*((C_word*)lf[234]+1));
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11901,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:228: chicken.base#gensym */
t4=*((C_word*)lf[56]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k11899 in k11893 in a11890 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_11901(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_11901,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11904,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:229: chicken.syntax#get-line-number */
t3=*((C_word*)lf[43]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k11902 in k11899 in k11893 in a11890 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_11904(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(33,c,3)))){
C_save_and_reclaim((void *)f_11904,c,av);}
a=C_alloc(33);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=C_a_i_list(&a,2,((C_word*)t0)[3],t2);
t4=C_a_i_list(&a,1,t3);
t5=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t6=t5;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=((C_word*)t7)[1];
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11928,a[2]=((C_word*)t0)[4],a[3]=((C_word)li135),tmp=(C_word)a,a+=4,tmp);
t10=C_u_i_cdr(((C_word*)t0)[2]);
t11=C_u_i_cdr(t10);
t12=C_i_check_list_2(t11,lf[67]);
t13=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11966,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=((C_word*)t0)[5],a[5]=t4,tmp=(C_word)a,a+=6,tmp);
t14=C_SCHEME_UNDEFINED;
t15=(*a=C_VECTOR_TYPE|1,a[1]=t14,tmp=(C_word)a,a+=2,tmp);
t16=C_set_block_item(t15,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11968,a[2]=t7,a[3]=t15,a[4]=t9,a[5]=t8,a[6]=((C_word)li136),tmp=(C_word)a,a+=7,tmp));
t17=((C_word*)t15)[1];
f_11968(t17,t13,t11);}

/* g974 in k11902 in k11899 in k11893 in a11890 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_fcall f_11928(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_11928,3,t0,t1,t2);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11932,a[2]=t2,a[3]=t1,a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:235: chicken.syntax#strip-syntax */
t4=*((C_word*)lf[39]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_i_car(t2);
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k11930 in g974 in k11902 in k11899 in k11893 in a11890 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_11932(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,3)))){
C_save_and_reclaim((void *)f_11932,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11939,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=C_eqp(t1,lf[242]);
if(C_truep(t3)){
t4=C_u_i_cdr(((C_word*)t0)[2]);
t5=C_a_i_cons(&a,2,lf[29],t4);
t6=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_a_i_list2(&a,2,lf[242],t5);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
if(C_truep(((C_word*)t0)[4])){
/* chicken-syntax.scm:240: chicken.compiler.scrutinizer#check-and-validate-type */
t4=*((C_word*)lf[238]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=t1;
av2[3]=lf[241];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t4=C_u_i_cdr(((C_word*)t0)[2]);
t5=C_a_i_cons(&a,2,lf[29],t4);
t6=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_a_i_list2(&a,2,t1,t5);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}}

/* k11937 in k11930 in g974 in k11902 in k11899 in k11893 in a11890 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_11939(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_11939,c,av);}
a=C_alloc(9);
t2=C_u_i_cdr(((C_word*)t0)[2]);
t3=C_a_i_cons(&a,2,lf[29],t2);
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_list2(&a,2,t1,t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k11964 in k11902 in k11899 in k11893 in a11890 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_11966(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,1)))){
C_save_and_reclaim((void *)f_11966,c,av);}
a=C_alloc(18);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_cons(&a,2,((C_word*)t0)[3],t2);
t4=C_a_i_cons(&a,2,lf[243],t3);
t5=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_a_i_list(&a,3,lf[37],((C_word*)t0)[5],t4);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* map-loop968 in k11902 in k11899 in k11893 in a11890 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_fcall f_11968(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_11968,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11993,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:234: g974 */
t4=((C_word*)t0)[4];
f_11928(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k11991 in map-loop968 in k11902 in k11899 in k11893 in a11890 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_11993(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_11993,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_11968(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k12010 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12012(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_12012,c,av);}
/* chicken-syntax.scm:158: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[245];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a12013 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12014(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_12014,c,av);}
a=C_alloc(5);
t5=C_i_memq(lf[233],*((C_word*)lf[234]+1));
if(C_truep(C_i_not(t5))){
t6=t1;{
C_word *av2=av;
av2[0]=t6;
av2[1]=lf[246];
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_12024,a[2]=t2,a[3]=t1,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:164: ##sys#check-syntax */
t7=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t7;
av2[1]=t6;
av2[2]=lf[245];
av2[3]=t2;
av2[4]=lf[254];
((C_proc)(void*)(*((C_word*)t7+1)))(5,av2);}}}

/* k12022 in a12013 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12024(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_12024,c,av);}
a=C_alloc(7);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=C_i_car(t2);
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_12033,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=t3,tmp=(C_word)a,a+=7,tmp);
/* chicken-syntax.scm:167: ##sys#globalize */
t5=*((C_word*)lf[253]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=t3;
av2[3]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k12031 in k12022 in a12013 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12033(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_12033,c,av);}
a=C_alloc(8);
t2=C_u_i_cdr(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_12037,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[4],a[6]=t2,a[7]=((C_word*)t0)[5],tmp=(C_word)a,a+=8,tmp);
/* chicken-syntax.scm:169: chicken.base#gensym */
t4=*((C_word*)lf[56]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k12035 in k12031 in k12022 in a12013 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12037(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_12037,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_12040,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
/* chicken-syntax.scm:170: ##sys#globalize */
t3=*((C_word*)lf[253]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=t1;
av2[3]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k12038 in k12035 in k12031 in k12022 in a12013 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12040(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_12040,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_12043,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
t3=C_i_cdddr(((C_word*)t0)[2]);
if(C_truep(C_i_pairp(t3))){
t4=C_u_i_cdr(((C_word*)t0)[2]);
t5=C_u_i_cdr(t4);
/* chicken-syntax.scm:171: chicken.syntax#strip-syntax */
t6=*((C_word*)lf[39]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t2;
av2[2]=C_u_i_car(t5);
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}
else{
t4=t2;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
f_12043(2,av2);}}}

/* k12041 in k12038 in k12035 in k12031 in k12022 in a12013 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12043(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_12043,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_12046,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
/* chicken-syntax.scm:172: r */
t3=((C_word*)t0)[9];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[81];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k12044 in k12041 in k12038 in k12035 in k12031 in k12022 in a12013 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12046(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,5)))){
C_save_and_reclaim((void *)f_12046,c,av);}
a=C_alloc(13);
t2=(C_truep(((C_word*)t0)[2])?C_i_cadddr(((C_word*)t0)[3]):C_i_caddr(((C_word*)t0)[3]));
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_12054,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=t2,a[5]=t1,a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[2],a[8]=t4,a[9]=((C_word*)t0)[7],a[10]=((C_word)li141),tmp=(C_word)a,a+=11,tmp));
t6=((C_word*)t4)[1];
f_12054(t6,((C_word*)t0)[8],((C_word*)t0)[9],C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST);}

/* loop in k12044 in k12041 in k12038 in k12035 in k12031 in k12022 in a12013 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_fcall f_12054(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(11,0,4)))){
C_save_and_reclaim_args((void *)trf_12054,5,t0,t1,t2,t3,t4);}
a=C_alloc(11);
if(C_truep(C_i_nullp(t2))){
t5=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_12064,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=t3,a[10]=t4,tmp=(C_word)a,a+=11,tmp);
/* chicken-syntax.scm:176: scheme#reverse */
t6=*((C_word*)lf[57]+1);{
C_word av2[3];
av2[0]=t6;
av2[1]=t5;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}
else{
t5=C_i_car(t2);
if(C_truep(C_i_symbolp(t5))){
t6=C_u_i_cdr(t2);
t7=C_a_i_cons(&a,2,t5,t3);
t8=C_a_i_cons(&a,2,lf[251],t4);
/* chicken-syntax.scm:208: loop */
t10=t1;
t11=t6;
t12=t7;
t13=t8;
t1=t10;
t2=t11;
t3=t12;
t4=t13;
goto loop;}
else{
t6=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_12305,a[2]=t2,a[3]=t5,a[4]=t3,a[5]=t4,a[6]=((C_word*)t0)[8],a[7]=t1,a[8]=((C_word*)t0)[9],tmp=(C_word)a,a+=9,tmp);
if(C_truep(C_i_listp(t5))){
t7=C_eqp(C_fix(2),C_u_i_length(t5));
t8=t6;
f_12305(t8,(C_truep(t7)?C_i_symbolp(C_i_car(t5)):C_SCHEME_FALSE));}
else{
t7=t6;
f_12305(t7,C_SCHEME_FALSE);}}}}

/* k12062 in loop in k12044 in k12041 in k12038 in k12035 in k12031 in k12022 in a12013 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12064(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_12064,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_12067,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],tmp=(C_word)a,a+=11,tmp);
/* chicken-syntax.scm:177: scheme#reverse */
t3=*((C_word*)lf[57]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[10];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k12065 in k12062 in loop in k12044 in k12041 in k12038 in k12035 in k12031 in k12022 in a12013 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12067(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(17,c,4)))){
C_save_and_reclaim((void *)f_12067,c,av);}
a=C_alloc(17);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_12249,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=t1,a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],tmp=(C_word)a,a+=11,tmp);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12251,a[2]=t4,a[3]=((C_word)li140),tmp=(C_word)a,a+=4,tmp));
t6=((C_word*)t4)[1];
f_12251(t6,t2,((C_word*)t0)[10],C_fix(1));}

/* k12071 in k12247 in k12065 in k12062 in loop in k12044 in k12041 in k12038 in k12035 in k12031 in k12022 in a12013 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in ... */
static void C_ccall f_12073(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(44,c,4)))){
C_save_and_reclaim((void *)f_12073,c,av);}
a=C_alloc(44);
t2=C_a_i_list(&a,2,lf[247],((C_word*)t0)[2]);
t3=C_a_i_list(&a,2,lf[248],((C_word*)t0)[2]);
t4=C_a_i_list(&a,3,lf[194],t2,t3);
t5=C_a_i_cons(&a,2,((C_word*)t0)[2],((C_word*)t0)[3]);
t6=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t7=t6;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=((C_word*)t8)[1];
t10=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_12108,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=t5,a[5]=((C_word*)t0)[6],a[6]=t4,tmp=(C_word)a,a+=7,tmp);
t11=C_SCHEME_UNDEFINED;
t12=(*a=C_VECTOR_TYPE|1,a[1]=t11,tmp=(C_word)a,a+=2,tmp);
t13=C_set_block_item(t12,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_12110,a[2]=t8,a[3]=t12,a[4]=t9,a[5]=((C_word)li138),tmp=(C_word)a,a+=6,tmp));
t14=((C_word*)t12)[1];
f_12110(t14,t10,((C_word*)t0)[3],((C_word*)t0)[7]);}

/* k12106 in k12071 in k12247 in k12065 in k12062 in loop in k12044 in k12041 in k12038 in k12035 in k12031 in k12022 in a12013 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in ... */
static void C_ccall f_12108(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(27,c,1)))){
C_save_and_reclaim((void *)f_12108,c,av);}
a=C_alloc(27);
t2=C_a_i_list(&a,3,lf[37],t1,((C_word*)t0)[2]);
t3=C_a_i_list(&a,3,((C_word*)t0)[3],((C_word*)t0)[4],t2);
t4=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_list(&a,3,lf[29],((C_word*)t0)[6],t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* map-loop913 in k12071 in k12247 in k12065 in k12062 in loop in k12044 in k12041 in k12038 in k12035 in k12031 in k12022 in a12013 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in ... */
static void C_fcall f_12110(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(21,0,3)))){
C_save_and_reclaim_args((void *)trf_12110,4,t0,t1,t2,t3);}
a=C_alloc(21);
t4=C_i_pairp(t2);
t5=(C_truep(t4)?C_i_pairp(t3):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_slot(t2,C_fix(0));
t7=C_slot(t3,C_fix(0));
t8=C_a_i_list(&a,4,lf[176],t7,C_SCHEME_TRUE,t6);
t9=C_a_i_list2(&a,2,t6,t8);
t10=C_a_i_cons(&a,2,t9,C_SCHEME_END_OF_LIST);
t11=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t10);
t12=C_mutate(((C_word *)((C_word*)t0)[2])+1,t10);
t14=t1;
t15=C_slot(t2,C_fix(1));
t16=C_slot(t3,C_fix(1));
t1=t14;
t2=t15;
t3=t16;
goto loop;}
else{
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* k12166 in k12247 in k12065 in k12062 in loop in k12044 in k12041 in k12038 in k12035 in k12031 in k12022 in a12013 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in ... */
static void C_ccall f_12168(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_12168,c,av);}
/* chicken-syntax.scm:184: ##sys#put! */
t2=*((C_word*)lf[249]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=lf[250];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k12170 in k12247 in k12065 in k12062 in loop in k12044 in k12041 in k12038 in k12035 in k12031 in k12022 in a12013 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in ... */
static void C_ccall f_12172(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(19,c,3)))){
C_save_and_reclaim((void *)f_12172,c,av);}
a=C_alloc(19);
t2=(C_truep(((C_word*)t0)[2])?C_i_pairp(((C_word*)t0)[2]):C_SCHEME_FALSE);
if(C_truep(t2)){
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=C_i_check_list_2(((C_word*)t0)[2],lf[67]);
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_12205,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_12207,a[2]=t5,a[3]=t10,a[4]=t6,a[5]=((C_word)li139),tmp=(C_word)a,a+=6,tmp));
t12=((C_word*)t10)[1];
f_12207(t12,t8,((C_word*)t0)[2]);}
else{
t3=C_a_i_list1(&a,1,((C_word*)t0)[3]);
t4=C_a_i_cons(&a,2,((C_word*)t0)[4],t3);
t5=C_a_i_list1(&a,1,t4);
/* chicken-syntax.scm:186: ##sys#append */
t6=*((C_word*)lf[55]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=((C_word*)t0)[5];
av2[2]=t1;
av2[3]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}}

/* k12203 in k12170 in k12247 in k12065 in k12062 in loop in k12044 in k12041 in k12038 in k12035 in k12031 in k12022 in a12013 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in ... */
static void C_ccall f_12205(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_12205,c,av);}
a=C_alloc(12);
t2=C_a_i_list2(&a,2,t1,((C_word*)t0)[2]);
t3=C_a_i_cons(&a,2,((C_word*)t0)[3],t2);
t4=C_a_i_list1(&a,1,t3);
/* chicken-syntax.scm:186: ##sys#append */
t5=*((C_word*)lf[55]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=((C_word*)t0)[4];
av2[2]=((C_word*)t0)[5];
av2[3]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* map-loop879 in k12170 in k12247 in k12065 in k12062 in loop in k12044 in k12041 in k12038 in k12035 in k12031 in k12022 in a12013 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in ... */
static void C_fcall f_12207(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_12207,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_12232,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
t4=C_slot(t2,C_fix(0));
t5=*((C_word*)lf[238]+1);
/* chicken-syntax.scm:192: g902 */
t6=*((C_word*)lf[238]+1);{
C_word av2[4];
av2[0]=t6;
av2[1]=t3;
av2[2]=t4;
av2[3]=lf[245];
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k12230 in map-loop879 in k12170 in k12247 in k12065 in k12062 in loop in k12044 in k12041 in k12038 in k12035 in k12031 in k12022 in a12013 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in ... */
static void C_ccall f_12232(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_12232,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_12207(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k12247 in k12065 in k12062 in loop in k12044 in k12041 in k12038 in k12035 in k12031 in k12022 in a12013 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 in ... */
static void C_ccall f_12249(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(21,c,4)))){
C_save_and_reclaim((void *)f_12249,c,av);}
a=C_alloc(21);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_12073,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],tmp=(C_word)a,a+=8,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12168,a[2]=t3,a[3]=((C_word*)t0)[9],tmp=(C_word)a,a+=4,tmp);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_12172,a[2]=((C_word*)t0)[10],a[3]=t2,a[4]=((C_word*)t0)[8],a[5]=t4,tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:187: ##sys#get */
t6=*((C_word*)lf[158]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=((C_word*)t0)[9];
av2[3]=lf[250];
av2[4]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* loop2 in k12065 in k12062 in loop in k12044 in k12041 in k12038 in k12035 in k12031 in k12022 in a12013 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 in ... */
static void C_fcall f_12251(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_12251,4,t0,t1,t2,t3);}
a=C_alloc(6);
if(C_truep(C_i_nullp(t2))){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=C_a_i_vector1(&a,1,t3);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12269,a[2]=t1,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:183: loop2 */
t7=t5;
t8=C_i_cdr(t2);
t9=C_fixnum_plus(t3,C_fix(1));
t1=t7;
t2=t8;
t3=t9;
goto loop;}}

/* k12267 in loop2 in k12065 in k12062 in loop in k12044 in k12041 in k12038 in k12035 in k12031 in k12022 in a12013 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in ... */
static void C_ccall f_12269(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_12269,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k12303 in loop in k12044 in k12041 in k12038 in k12035 in k12031 in k12022 in a12013 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_fcall f_12305(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,5)))){
C_save_and_reclaim_args((void *)trf_12305,2,t0,t1);}
a=C_alloc(10);
if(C_truep(t1)){
t2=C_u_i_cdr(((C_word*)t0)[2]);
t3=C_i_car(((C_word*)t0)[3]);
t4=C_a_i_cons(&a,2,t3,((C_word*)t0)[4]);
t5=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_12322,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[7],a[5]=t2,a[6]=t4,tmp=(C_word)a,a+=7,tmp);
/* chicken-syntax.scm:214: chicken.compiler.scrutinizer#check-and-validate-type */
t6=*((C_word*)lf[238]+1);{
C_word av2[4];
av2[0]=t6;
av2[1]=t5;
av2[2]=C_i_cadr(((C_word*)t0)[3]);
av2[3]=lf[245];
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}
else{
/* chicken-syntax.scm:218: chicken.syntax#syntax-error */
t2=*((C_word*)lf[58]+1);{
C_word av2[6];
av2[0]=t2;
av2[1]=((C_word*)t0)[7];
av2[2]=lf[245];
av2[3]=lf[252];
av2[4]=((C_word*)t0)[3];
av2[5]=((C_word*)t0)[8];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}}

/* k12320 in k12303 in loop in k12044 in k12041 in k12038 in k12035 in k12031 in k12022 in a12013 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12322(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_12322,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,((C_word*)t0)[2]);
/* chicken-syntax.scm:210: loop */
t3=((C_word*)((C_word*)t0)[3])[1];
f_12054(t3,((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6],t2);}

/* k12377 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12379(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_12379,c,av);}
/* chicken-syntax.scm:152: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[255];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a12380 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12381(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(14,c,2)))){
C_save_and_reclaim((void *)f_12381,c,av);}
a=C_alloc(14);
t5=C_i_cdr(t2);
t6=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_12391,a[2]=t5,a[3]=t1,a[4]=t3,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_i_pairp(t5))){
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12553,a[2]=t5,a[3]=t6,tmp=(C_word)a,a+=4,tmp);
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12609,a[2]=t5,a[3]=t7,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:154: ##sys#list? */
t9=*((C_word*)lf[220]+1);{
C_word *av2=av;
av2[0]=t9;
av2[1]=t8;
av2[2]=C_i_car(t5);
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}
else{
t7=t6;{
C_word *av2=av;
av2[0]=t7;
av2[1]=C_SCHEME_FALSE;
f_12391(2,av2);}}}

/* k12389 in a12380 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12391(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,3)))){
C_save_and_reclaim((void *)f_12391,c,av);}
a=C_alloc(18);
if(C_truep(t1)){
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=C_i_car(((C_word*)t0)[2]);
t7=C_i_check_list_2(t6,lf[67]);
t8=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_12408,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_12509,a[2]=t4,a[3]=t10,a[4]=t5,a[5]=((C_word)li145),tmp=(C_word)a,a+=6,tmp));
t12=((C_word*)t10)[1];
f_12509(t12,t8,t6);}
else{
/* chicken-syntax.scm:154: chicken.internal.syntax-rules#syntax-rules-mismatch */
t2=*((C_word*)lf[215]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}}

/* k12406 in k12389 in a12380 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12408(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(19,c,3)))){
C_save_and_reclaim((void *)f_12408,c,av);}
a=C_alloc(19);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=C_i_car(((C_word*)t0)[2]);
t7=C_i_check_list_2(t6,lf[67]);
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_12429,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_12475,a[2]=t4,a[3]=t10,a[4]=t5,a[5]=((C_word)li144),tmp=(C_word)a,a+=6,tmp));
t12=((C_word*)t10)[1];
f_12475(t12,t8,t6);}

/* k12427 in k12406 in k12389 in a12380 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12429(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_12429,c,av);}
a=C_alloc(7);
t2=C_i_cdr(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_12439,a[2]=t2,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* chicken-syntax.scm:154: rename737 */
t4=((C_word*)t0)[4];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[258];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k12437 in k12427 in k12406 in k12389 in a12380 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12439(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_12439,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_12447,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12449,a[2]=((C_word*)t0)[4],a[3]=((C_word)li143),tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:154: ##sys#map-n */
t4=*((C_word*)lf[257]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=t3;
av2[3]=((C_word*)t0)[5];
av2[4]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k12445 in k12437 in k12427 in k12406 in k12389 in a12380 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12447(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_12447,c,av);}
a=C_alloc(6);
t2=C_a_i_cons(&a,2,t1,((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[4],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* a12448 in k12437 in k12427 in k12406 in k12389 in a12380 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12449(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_12449,c,av);}
a=C_alloc(5);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_12465,a[2]=t3,a[3]=t2,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:154: rename737 */
t5=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
av2[2]=lf[256];
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k12463 in a12448 in k12437 in k12427 in k12406 in k12389 in a12380 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12465(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,1)))){
C_save_and_reclaim((void *)f_12465,c,av);}
a=C_alloc(15);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],C_SCHEME_END_OF_LIST);
t3=C_a_i_cons(&a,2,((C_word*)t0)[3],t2);
t4=C_a_i_cons(&a,2,t1,t3);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[2],t5);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}

/* map-loop810 in k12406 in k12389 in a12380 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_fcall f_12475(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_12475,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_cdr(t3);
t5=C_i_car(t4);
t6=C_a_i_cons(&a,2,t5,C_SCHEME_END_OF_LIST);
t7=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t6);
t8=C_mutate(((C_word *)((C_word*)t0)[2])+1,t6);
t10=t1;
t11=C_slot(t2,C_fix(1));
t1=t10;
t2=t11;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* map-loop782 in k12389 in a12380 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_fcall f_12509(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_12509,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_car(t3);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k12551 in a12380 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12553(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_12553,c,av);}
a=C_alloc(4);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12559,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:154: ##sys#list? */
t3=*((C_word*)lf[220]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_i_cdr(((C_word*)t0)[2]);
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
f_12391(2,av2);}}}

/* k12557 in k12551 in a12380 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12559(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_12559,c,av);}
a=C_alloc(4);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12562,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:154: ##sys#length */
t3=*((C_word*)lf[219]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_i_cdr(((C_word*)t0)[2]);
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
f_12391(2,av2);}}}

/* k12560 in k12557 in k12551 in a12380 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12562(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_12562,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_12568,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:154: ##sys#>= */
t3=*((C_word*)lf[218]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=t1;
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k12566 in k12560 in k12557 in k12551 in a12380 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12568(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_12568,c,av);}
a=C_alloc(6);
if(C_truep(t1)){
t2=C_i_cdr(((C_word*)t0)[2]);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12577,a[2]=t4,a[3]=((C_word)li146),tmp=(C_word)a,a+=4,tmp));
t6=((C_word*)t4)[1];
f_12577(t6,((C_word*)t0)[3],t2,((C_word*)t0)[4]);}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
f_12391(2,av2);}}}

/* loop734 in k12566 in k12560 in k12557 in k12551 in a12380 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_fcall f_12577(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_12577,4,t0,t1,t2,t3);}
a=C_alloc(6);
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_12584,a[2]=t1,a[3]=t2,a[4]=((C_word*)t0)[2],a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:154: ##sys#= */
t5=*((C_word*)lf[217]+1);{
C_word av2[4];
av2[0]=t5;
av2[1]=t4;
av2[2]=t3;
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k12582 in loop734 in k12566 in k12560 in k12557 in k12551 in a12380 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12584(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_12584,c,av);}
a=C_alloc(5);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_eqp(((C_word*)t0)[3],C_SCHEME_END_OF_LIST);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_i_cdr(((C_word*)t0)[3]);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_12598,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:154: ##sys#+ */
t4=*((C_word*)lf[216]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[5];
av2[3]=C_fix(-1);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}}

/* k12596 in k12582 in loop734 in k12566 in k12560 in k12557 in k12551 in a12380 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12598(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_12598,c,av);}
/* chicken-syntax.scm:154: loop734 */
t2=((C_word*)((C_word*)t0)[2])[1];
f_12577(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* k12607 in a12380 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12609(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_12609,c,av);}
a=C_alloc(4);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12612,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:154: ##sys#length */
t3=*((C_word*)lf[219]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_i_car(((C_word*)t0)[2]);
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
f_12553(2,av2);}}}

/* k12610 in k12607 in a12380 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12612(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_12612,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_12618,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:154: ##sys#>= */
t3=*((C_word*)lf[218]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=t1;
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k12616 in k12610 in k12607 in a12380 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12618(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_12618,c,av);}
a=C_alloc(6);
if(C_truep(t1)){
t2=C_i_car(((C_word*)t0)[2]);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12627,a[2]=t4,a[3]=((C_word)li147),tmp=(C_word)a,a+=4,tmp));
t6=((C_word*)t4)[1];
f_12627(t6,((C_word*)t0)[3],t2,((C_word*)t0)[4]);}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
f_12553(2,av2);}}}

/* loop734 in k12616 in k12610 in k12607 in a12380 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_fcall f_12627(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_12627,4,t0,t1,t2,t3);}
a=C_alloc(6);
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_12634,a[2]=t1,a[3]=t2,a[4]=((C_word*)t0)[2],a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:154: ##sys#= */
t5=*((C_word*)lf[217]+1);{
C_word av2[4];
av2[0]=t5;
av2[1]=t4;
av2[2]=t3;
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k12632 in loop734 in k12616 in k12610 in k12607 in a12380 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12634(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_12634,c,av);}
a=C_alloc(6);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_eqp(((C_word*)t0)[3],C_SCHEME_END_OF_LIST);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_i_car(((C_word*)t0)[3]);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_12646,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_i_pairp(t2))){
t4=C_i_cdr(t2);
t5=C_i_pairp(t4);
t6=t3;
f_12646(t6,(C_truep(t5)?C_eqp(C_i_cdr(t4),C_SCHEME_END_OF_LIST):C_SCHEME_FALSE));}
else{
t4=t3;
f_12646(t4,C_SCHEME_FALSE);}}}

/* k12644 in k12632 in loop734 in k12616 in k12610 in k12607 in a12380 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_fcall f_12646(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,3)))){
C_save_and_reclaim_args((void *)trf_12646,2,t0,t1);}
a=C_alloc(5);
if(C_truep(t1)){
t2=C_i_cdr(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_12657,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:154: ##sys#+ */
t4=*((C_word*)lf[216]+1);{
C_word av2[4];
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[5];
av2[3]=C_fix(-1);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t2=((C_word*)t0)[4];{
C_word av2[2];
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k12655 in k12644 in k12632 in loop734 in k12616 in k12610 in k12607 in a12380 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12657(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_12657,c,av);}
/* chicken-syntax.scm:154: loop734 */
t2=((C_word*)((C_word*)t0)[2])[1];
f_12627(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* k12686 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12688(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_12688,c,av);}
/* chicken-syntax.scm:141: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[256];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a12689 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12690(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_12690,c,av);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12694,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:145: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[256];
av2[3]=t2;
av2[4]=lf[259];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k12692 in a12689 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12694(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_12694,c,av);}
a=C_alloc(4);
t2=C_i_memq(lf[233],*((C_word*)lf[234]+1));
if(C_truep(C_i_not(t2))){
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_caddr(((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12710,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:148: chicken.compiler.scrutinizer#check-and-validate-type */
t4=*((C_word*)lf[238]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_i_cadr(((C_word*)t0)[3]);
av2[3]=lf[256];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}}

/* k12708 in k12692 in a12689 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12710(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,1)))){
C_save_and_reclaim((void *)f_12710,c,av);}
a=C_alloc(12);
t2=C_i_caddr(((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_list(&a,4,lf[176],t1,C_SCHEME_TRUE,t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k12724 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12726(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_12726,c,av);}
/* chicken-syntax.scm:118: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[260];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a12727 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12728(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_12728,c,av);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12732,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:122: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[260];
av2[3]=t2;
av2[4]=lf[267];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k12730 in a12727 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12732(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_12732,c,av);}
a=C_alloc(4);
t2=C_i_memq(lf[233],*((C_word*)lf[234]+1));
if(C_truep(C_i_not(t2))){
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=lf[261];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12741,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:125: chicken.syntax#strip-syntax */
t4=*((C_word*)lf[39]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_i_caddr(((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}}

/* k12739 in k12730 in a12727 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12741(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,5)))){
C_save_and_reclaim((void *)f_12741,c,av);}
a=C_alloc(11);
t2=C_u_i_cdr(((C_word*)t0)[2]);
t3=C_u_i_car(t2);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_12748,a[2]=t1,a[3]=t3,a[4]=((C_word)li150),tmp=(C_word)a,a+=5,tmp);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_12758,a[2]=t3,a[3]=t1,a[4]=((C_word*)t0)[2],a[5]=((C_word)li151),tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:129: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[3];
av2[2]=t4;
av2[3]=t5;
C_call_with_values(4,av2);}}

/* a12747 in k12739 in k12730 in a12727 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12748(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_12748,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12756,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:132: chicken.syntax#strip-syntax */
t3=*((C_word*)lf[39]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k12754 in a12747 in k12739 in k12730 in a12727 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12756(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_12756,c,av);}
/* chicken-syntax.scm:130: chicken.compiler.scrutinizer#validate-type */
t2=*((C_word*)lf[262]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* a12757 in k12739 in k12730 in a12727 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12758(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(30,c,5)))){
C_save_and_reclaim((void *)f_12758,c,av);}
a=C_alloc(30);
if(C_truep(C_i_not(t2))){
/* chicken-syntax.scm:134: chicken.syntax#syntax-error */
t5=*((C_word*)lf[58]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t5;
av2[1]=t1;
av2[2]=lf[260];
av2[3]=lf[263];
av2[4]=((C_word*)t0)[2];
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t5+1)))(6,av2);}}
else{
t5=C_i_cdddr(((C_word*)t0)[4]);
t6=C_a_i_cons(&a,2,((C_word*)t0)[3],t5);
t7=C_a_i_cons(&a,2,((C_word*)t0)[2],t6);
t8=C_a_i_list(&a,2,lf[264],t7);
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12783,a[2]=t8,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t10=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_12787,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=t9,tmp=(C_word)a,a+=5,tmp);
if(C_truep(t4)){
t11=C_a_i_list(&a,2,lf[266],((C_word*)t0)[2]);
t12=t10;
f_12787(t12,C_a_i_list(&a,1,t11));}
else{
t11=t10;
f_12787(t11,C_SCHEME_END_OF_LIST);}}}

/* k12781 in a12757 in k12739 in k12730 in a12727 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12783(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_12783,c,av);}
a=C_alloc(6);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_cons(&a,2,lf[194],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k12785 in a12757 in k12739 in k12730 in a12727 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_fcall f_12787(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,0,3)))){
C_save_and_reclaim_args((void *)trf_12787,2,t0,t1);}
a=C_alloc(15);
if(C_truep(((C_word*)t0)[2])){
t2=C_a_i_list(&a,2,((C_word*)t0)[3],((C_word*)t0)[2]);
t3=C_a_i_list(&a,2,lf[265],t2);
t4=C_a_i_list(&a,1,t3);
/* chicken-syntax.scm:136: ##sys#append */
t5=*((C_word*)lf[55]+1);{
C_word av2[4];
av2[0]=t5;
av2[1]=((C_word*)t0)[4];
av2[2]=t1;
av2[3]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}
else{
/* chicken-syntax.scm:136: ##sys#append */
t2=*((C_word*)lf[55]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=((C_word*)t0)[4];
av2[2]=t1;
av2[3]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}}

/* k12835 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12837(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_12837,c,av);}
/* chicken-syntax.scm:76: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[270];
av2[3]=((C_word*)t0)[3];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a12838 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12839(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_12839,c,av);}
a=C_alloc(5);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_12843,a[2]=t2,a[3]=t1,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:81: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[270];
av2[3]=t2;
av2[4]=lf[279];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k12841 in a12838 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12843(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_12843,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_12846,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:82: r */
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[278];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k12844 in k12841 in a12838 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12846(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_12846,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_12849,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:83: r */
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[277];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k12847 in k12844 in k12841 in a12838 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12849(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_12849,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_12852,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* chicken-syntax.scm:84: r */
t3=((C_word*)t0)[5];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[276];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k12850 in k12847 in k12844 in k12841 in a12838 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12852(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_12852,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_12855,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* chicken-syntax.scm:85: r */
t3=((C_word*)t0)[6];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[268];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k12853 in k12850 in k12847 in k12844 in k12841 in a12838 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12855(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_12855,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_12858,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
/* chicken-syntax.scm:86: r */
t3=((C_word*)t0)[7];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[242];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k12856 in k12853 in k12850 in k12847 in k12844 in k12841 in a12838 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12858(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,3)))){
C_save_and_reclaim((void *)f_12858,c,av);}
a=C_alloc(18);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_12860,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word)li155),tmp=(C_word)a,a+=8,tmp);
t3=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_12990,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=t1,a[8]=((C_word*)t0)[8],a[9]=t2,tmp=(C_word)a,a+=10,tmp);
/* chicken-syntax.scm:98: r */
t4=((C_word*)t0)[8];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[275];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* parse-clause in k12856 in k12853 in k12850 in k12847 in k12844 in k12841 in a12838 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_fcall f_12860(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(23,0,2)))){
C_save_and_reclaim_args((void *)trf_12860,3,t0,t1,t2);}
a=C_alloc(23);
t3=C_i_car(t2);
t4=C_i_symbolp(t3);
t5=(C_truep(t4)?C_u_i_car(t2):C_SCHEME_FALSE);
t6=(C_truep(t5)?C_i_cadr(t2):C_u_i_car(t2));
t7=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_12870,a[2]=t6,a[3]=t1,a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[5],tmp=(C_word)a,a+=8,tmp);
if(C_truep(t5)){
t8=C_a_i_list(&a,2,t5,((C_word*)t0)[6]);
t9=C_a_i_list(&a,1,t8);
t10=C_i_cddr(t2);
t11=C_a_i_cons(&a,2,t9,t10);
t12=t7;
f_12870(t12,C_a_i_cons(&a,2,lf[37],t11));}
else{
t8=C_u_i_cdr(t2);
t9=C_a_i_cons(&a,2,C_SCHEME_END_OF_LIST,t8);
t10=t7;
f_12870(t10,C_a_i_cons(&a,2,lf[37],t9));}}

/* k12868 in parse-clause in k12856 in k12853 in k12850 in k12847 in k12844 in k12841 in a12838 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_fcall f_12870(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(25,0,3)))){
C_save_and_reclaim_args((void *)trf_12870,2,t0,t1);}
a=C_alloc(25);
if(C_truep(C_i_nullp(((C_word*)t0)[2]))){
t2=((C_word*)t0)[3];{
C_word av2[2];
av2[0]=t2;
av2[1]=C_a_i_list(&a,2,((C_word*)t0)[4],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_12895,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],a[4]=((C_word)li153),tmp=(C_word)a,a+=5,tmp);
t7=C_i_check_list_2(((C_word*)t0)[2],lf[67]);
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_12909,a[2]=((C_word*)t0)[6],a[3]=((C_word*)t0)[7],a[4]=((C_word*)t0)[3],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_12911,a[2]=t6,a[3]=t4,a[4]=t10,a[5]=t5,a[6]=((C_word)li154),tmp=(C_word)a,a+=7,tmp));
t12=((C_word*)t10)[1];
f_12911(t12,t8,((C_word*)t0)[2]);}}

/* g623 in k12868 in parse-clause in k12856 in k12853 in k12850 in k12847 in k12844 in k12841 in a12838 in k5507 in k5504 in k4222 in k4219 in k4216 */
static C_word C_fcall f_12895(C_word *a,C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_stack_overflow_check;{}
t2=C_a_i_list(&a,2,lf[38],t1);
return(C_a_i_list(&a,3,((C_word*)t0)[2],t2,((C_word*)t0)[3]));}

/* k12907 in k12868 in parse-clause in k12856 in k12853 in k12850 in k12847 in k12844 in k12841 in a12838 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12909(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,1)))){
C_save_and_reclaim((void *)f_12909,c,av);}
a=C_alloc(12);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_cons(&a,2,((C_word*)t0)[3],t2);
t4=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_list(&a,2,t3,((C_word*)t0)[5]);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* map-loop617 in k12868 in parse-clause in k12856 in k12853 in k12850 in k12847 in k12844 in k12841 in a12838 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_fcall f_12911(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(18,0,2)))){
C_save_and_reclaim_args((void *)trf_12911,3,t0,t1,t2);}
a=C_alloc(18);
if(C_truep(C_i_pairp(t2))){
t3=(
/* chicken-syntax.scm:95: g623 */
  f_12895(C_a_i(&a,15),((C_word*)t0)[2],C_slot(t2,C_fix(0)))
);
t4=C_a_i_cons(&a,2,t3,C_SCHEME_END_OF_LIST);
t5=C_i_setslot(((C_word*)((C_word*)t0)[3])[1],C_fix(1),t4);
t6=C_mutate(((C_word *)((C_word*)t0)[3])+1,t4);
t8=t1;
t9=C_slot(t2,C_fix(1));
t1=t8;
t2=t9;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k12988 in k12856 in k12853 in k12850 in k12847 in k12844 in k12841 in a12838 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_12990(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(65,c,3)))){
C_save_and_reclaim((void *)f_12990,c,av);}
a=C_alloc(65);
t2=C_a_i_list(&a,2,lf[38],lf[271]);
t3=C_a_i_list(&a,3,lf[72],((C_word*)t0)[2],t2);
t4=C_a_i_list(&a,3,lf[272],((C_word*)t0)[2],C_fix(1));
t5=C_a_i_list(&a,3,((C_word*)t0)[3],t3,t4);
t6=C_a_i_list(&a,2,((C_word*)t0)[4],t5);
t7=C_a_i_list(&a,1,t6);
t8=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t9=t8;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=((C_word*)t10)[1];
t12=C_i_cddr(((C_word*)t0)[5]);
t13=C_i_check_list_2(t12,lf[67]);
t14=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_13015,a[2]=t7,a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=t1,a[6]=((C_word*)t0)[2],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
t15=C_SCHEME_UNDEFINED;
t16=(*a=C_VECTOR_TYPE|1,a[1]=t15,tmp=(C_word)a,a+=2,tmp);
t17=C_set_block_item(t16,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_13046,a[2]=t10,a[3]=t16,a[4]=((C_word*)t0)[9],a[5]=t11,a[6]=((C_word)li156),tmp=(C_word)a,a+=7,tmp));
t18=((C_word*)t16)[1];
f_13046(t18,t14,t12);}

/* k13013 in k12988 in k12856 in k12853 in k12850 in k12847 in k12844 in k12841 in a12838 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_13015(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_13015,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_13022,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=t1,tmp=(C_word)a,a+=9,tmp);
/* chicken-syntax.scm:103: r */
t3=((C_word*)t0)[8];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[274];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k13020 in k13013 in k12988 in k12856 in k12853 in k12850 in k12847 in k12844 in k12841 in a12838 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_13022(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(23,c,3)))){
C_save_and_reclaim((void *)f_13022,c,av);}
a=C_alloc(23);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_13026,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
if(C_truep(C_i_assq(((C_word*)t0)[7],((C_word*)t0)[8]))){
/* chicken-syntax.scm:103: ##sys#append */
t3=*((C_word*)lf[55]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[8];
av2[3]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}
else{
t3=C_a_i_list(&a,2,lf[273],((C_word*)t0)[6]);
t4=C_a_i_list(&a,2,((C_word*)t0)[7],t3);
t5=C_a_i_list(&a,1,t4);
/* chicken-syntax.scm:103: ##sys#append */
t6=*((C_word*)lf[55]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t2;
av2[2]=((C_word*)t0)[8];
av2[3]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}}

/* k13024 in k13020 in k13013 in k12988 in k12856 in k12853 in k12850 in k12847 in k12844 in k12841 in a12838 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_13026(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,1)))){
C_save_and_reclaim((void *)f_13026,c,av);}
a=C_alloc(24);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_list(&a,3,lf[37],((C_word*)t0)[3],t2);
t4=C_i_cadr(((C_word*)t0)[4]);
t5=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_a_i_list(&a,4,((C_word*)t0)[6],((C_word*)t0)[7],t3,t4);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* map-loop648 in k12988 in k12856 in k12853 in k12850 in k12847 in k12844 in k12841 in a12838 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_fcall f_13046(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_13046,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_13071,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:102: g654 */
t4=((C_word*)t0)[4];
f_12860(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k13069 in map-loop648 in k12988 in k12856 in k12853 in k12850 in k12847 in k12844 in k12841 in a12838 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_13071(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_13071,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_13046(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k13108 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_13110(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_13110,c,av);}
/* chicken-syntax.scm:55: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[275];
av2[3]=((C_word*)t0)[3];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a13111 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_13112(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_13112,c,av);}
a=C_alloc(5);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_13116,a[2]=t2,a[3]=t1,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:60: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[275];
av2[3]=t2;
av2[4]=lf[285];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k13114 in a13111 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_13116(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_13116,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_13119,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:61: r */
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[284];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k13117 in k13114 in a13111 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_13119(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_13119,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_13122,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:62: r */
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[283];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k13120 in k13117 in k13114 in a13111 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_13122(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_13122,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_13133,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:63: r */
t3=((C_word*)t0)[5];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[280];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k13131 in k13120 in k13117 in k13114 in a13111 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_13133(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(114,c,1)))){
C_save_and_reclaim((void *)f_13133,c,av);}
a=C_alloc(114);
t2=C_a_i_list(&a,1,((C_word*)t0)[2]);
t3=C_i_cadr(((C_word*)t0)[3]);
t4=C_a_i_list(&a,1,t3);
t5=C_i_caddr(((C_word*)t0)[3]);
t6=C_a_i_list(&a,3,lf[23],C_SCHEME_END_OF_LIST,t5);
t7=C_a_i_list(&a,2,((C_word*)t0)[2],t6);
t8=C_a_i_list(&a,3,lf[23],t4,t7);
t9=C_u_i_cdr(((C_word*)t0)[3]);
t10=C_u_i_cdr(t9);
t11=C_u_i_cdr(t10);
t12=C_a_i_cons(&a,2,C_SCHEME_END_OF_LIST,t11);
t13=C_a_i_cons(&a,2,lf[23],t12);
t14=C_a_i_list(&a,3,lf[26],lf[27],((C_word*)t0)[4]);
t15=C_a_i_list(&a,3,lf[23],C_SCHEME_END_OF_LIST,t14);
t16=C_a_i_list(&a,2,((C_word*)t0)[2],t15);
t17=C_a_i_list(&a,3,lf[23],((C_word*)t0)[4],t16);
t18=C_a_i_list(&a,3,lf[28],t13,t17);
t19=C_a_i_list(&a,3,lf[23],C_SCHEME_END_OF_LIST,t18);
t20=C_a_i_list(&a,3,lf[282],t8,t19);
t21=C_a_i_list(&a,3,lf[23],t2,t20);
t22=C_a_i_list(&a,2,t1,t21);
t23=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t23;
av2[1]=C_a_i_list(&a,1,t22);
((C_proc)(void*)(*((C_word*)t23+1)))(2,av2);}}

/* k4216 */
static void C_ccall f_4218(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4218,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4221,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_internal_toplevel(2,av2);}}

/* k4219 in k4216 */
static void C_ccall f_4221(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4221,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4224,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_library_toplevel(2,av2);}}

/* k4222 in k4219 in k4216 */
static void C_ccall f_4224(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,4)))){
C_save_and_reclaim((void *)f_4224,c,av);}
a=C_alloc(14);
t2=C_a_i_provide(&a,1,lf[0]);
t3=C_mutate(&lf[1] /* (set! take ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4334,a[2]=((C_word)li0),tmp=(C_word)a,a+=3,tmp));
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5506,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:53: ##sys#macro-environment */
t5=*((C_word*)lf[19]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* take in k4222 in k4219 in k4216 */
static void C_fcall f_4334(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(4,0,3)))){
C_save_and_reclaim_args((void *)trf_4334,3,t1,t2,t3);}
a=C_alloc(4);
if(C_truep(C_fixnum_less_or_equal_p(t3,C_fix(0)))){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=C_i_car(t2);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4352,a[2]=t1,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* mini-srfi-1.scm:56: take */
t7=t5;
t8=C_u_i_cdr(t2);
t9=C_fixnum_difference(t3,C_fix(1));
t1=t7;
t2=t8;
t3=t9;
goto loop;}}

/* k4350 in take in k4222 in k4219 in k4216 */
static void C_ccall f_4352(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_4352,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* loop in k6970 in a6963 in k6956 in k6946 in a6943 in g2890 in foldr2885 in k6906 in k6899 in k6896 in k6893 in k6890 in k6887 in k6884 in k6881 in k6878 in k6875 in k6838 in a6835 in k5637 in k5634 in ... */
static void C_fcall f_4392(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(4,0,4)))){
C_save_and_reclaim_args((void *)trf_4392,5,t0,t1,t2,t3,t4);}
a=C_alloc(4);
if(C_truep(C_fixnum_less_or_equal_p(t2,C_fix(0)))){
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4406,a[2]=t1,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* mini-srfi-1.scm:67: scheme#reverse */
t6=*((C_word*)lf[57]+1);{
C_word av2[3];
av2[0]=t6;
av2[1]=t5;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}
else{
t5=C_fixnum_difference(t2,C_fix(1));
t6=C_i_car(t4);
t7=C_a_i_cons(&a,2,t6,t3);
/* mini-srfi-1.scm:68: loop */
t9=t1;
t10=t5;
t11=t7;
t12=C_u_i_cdr(t4);
t1=t9;
t2=t10;
t3=t11;
t4=t12;
goto loop;}}

/* k4404 in loop in k6970 in a6963 in k6956 in k6946 in a6943 in g2890 in foldr2885 in k6906 in k6899 in k6896 in k6893 in k6890 in k6887 in k6884 in k6881 in k6878 in k6875 in k6838 in a6835 in k5637 in ... */
static void C_ccall f_4406(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4406,c,av);}
/* mini-srfi-1.scm:67: scheme#values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
C_values(4,av2);}}

/* k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_5506(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(17,c,5)))){
C_save_and_reclaim((void *)f_5506,c,av);}
a=C_alloc(17);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5509,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=C_a_i_cons(&a,2,lf[280],lf[281]);
t4=C_a_i_list(&a,1,t3);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_13110,a[2]=t2,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_13112,a[2]=((C_word)li158),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:58: ##sys#er-transformer */
t7=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t7;
av2[1]=t5;
av2[2]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}

/* k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_5509(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(17,c,5)))){
C_save_and_reclaim((void *)f_5509,c,av);}
a=C_alloc(17);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5512,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=C_a_i_cons(&a,2,lf[268],lf[269]);
t4=C_a_i_list(&a,1,t3);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_12837,a[2]=t2,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_12839,a[2]=((C_word)li157),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:79: ##sys#er-transformer */
t7=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t7;
av2[1]=t5;
av2[2]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}

/* k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_5512(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_5512,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5515,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:110: chicken.internal#macro-subset */
t3=*((C_word*)lf[17]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=*((C_word*)lf[18]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_5515(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_5515,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[2]+1 /* (set! ##sys#chicken.condition-macro-environment ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5519,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:116: ##sys#macro-environment */
t4=*((C_word*)lf[19]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_5519(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5519,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5522,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_12726,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_12728,a[2]=((C_word)li152),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:120: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_5522(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5522,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5525,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_12688,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_12690,a[2]=((C_word)li149),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:143: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_5525(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5525,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5528,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_12379,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_12381,a[2]=((C_word)li148),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:154: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_5528(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5528,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5531,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_12012,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_12014,a[2]=((C_word)li142),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:160: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_5531(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5531,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5534,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11889,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11891,a[2]=((C_word)li137),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:224: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_5534(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5534,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5537,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11829,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11831,a[2]=((C_word)li134),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:249: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_5537(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_5537,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5540,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:265: chicken.internal#macro-subset */
t3=*((C_word*)lf[17]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=*((C_word*)lf[18]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_5540(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_5540,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[3]+1 /* (set! ##sys#chicken.type-macro-environment ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5544,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:270: ##sys#macro-environment */
t4=*((C_word*)lf[19]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_5544(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5544,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5547,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11812,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11814,a[2]=((C_word)li133),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:275: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_5547(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5547,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5550,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11782,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11784,a[2]=((C_word)li132),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:282: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_5550(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5550,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5553,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11753,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11755,a[2]=((C_word)li131),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:290: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_5553(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5553,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5556,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11656,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11658,a[2]=((C_word)li130),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:301: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_5556(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5556,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5559,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11508,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11510,a[2]=((C_word)li129),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:309: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 */
static void C_ccall f_5559(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_5559,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5562,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:313: chicken.internal#macro-subset */
t3=*((C_word*)lf[17]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=*((C_word*)lf[18]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in k4216 in ... */
static void C_ccall f_5562(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_5562,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[4]+1 /* (set! ##sys#chicken.syntax-macro-environment ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5566,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:319: ##sys#macro-environment */
t4=*((C_word*)lf[19]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in k4219 in ... */
static void C_ccall f_5566(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5566,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5569,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11491,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11493,a[2]=((C_word)li126),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:324: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in k4222 in ... */
static void C_ccall f_5569(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5569,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5572,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11096,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11098,a[2]=((C_word)li125),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:331: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in k5504 in ... */
static void C_ccall f_5572(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5572,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5575,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11003,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11005,a[2]=((C_word)li121),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:404: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in k5507 in ... */
static void C_ccall f_5575(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5575,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5578,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10989,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10991,a[2]=((C_word)li120),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:422: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in k5510 in ... */
static void C_ccall f_5578(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5578,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5581,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10968,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10970,a[2]=((C_word)li119),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:429: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in k5513 in ... */
static void C_ccall f_5581(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5581,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5584,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10951,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10953,a[2]=((C_word)li118),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:436: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in k5517 in ... */
static void C_ccall f_5584(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5584,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5587,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10934,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10936,a[2]=((C_word)li117),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:443: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in k5520 in ... */
static void C_ccall f_5587(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5587,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5590,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10281,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10283,a[2]=((C_word)li116),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:450: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in k5523 in ... */
static void C_ccall f_5590(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5590,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5593,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9351,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9353,a[2]=((C_word)li102),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:482: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in ... */
static void C_ccall f_5593(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5593,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5596,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9267,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9269,a[2]=((C_word)li83),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:537: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in ... */
static void C_ccall f_5596(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5596,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5599,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9243,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9245,a[2]=((C_word)li78),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:549: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in ... */
static void C_ccall f_5599(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5599,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5602,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9215,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9217,a[2]=((C_word)li77),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:557: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in ... */
static void C_ccall f_5602(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5602,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5605,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9194,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9196,a[2]=((C_word)li76),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:566: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in ... */
static void C_ccall f_5605(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5605,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5609,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9064,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9066,a[2]=((C_word)li75),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:574: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in ... */
static void C_ccall f_5609(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5609,c,av);}
a=C_alloc(10);
t2=C_mutate((C_word*)lf[5]+1 /* (set! chicken.syntax#define-values-definition ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5612,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8587,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8589,a[2]=((C_word)li71),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:591: ##sys#er-transformer */
t6=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t4;
av2[2]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in ... */
static void C_ccall f_5612(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5612,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5615,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8533,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8535,a[2]=((C_word)li58),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:645: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in ... */
static void C_ccall f_5615(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5615,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5618,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8275,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8277,a[2]=((C_word)li56),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:660: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in ... */
static void C_ccall f_5618(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5618,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5621,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8253,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8255,a[2]=((C_word)li49),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:680: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in ... */
static void C_ccall f_5621(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(17,c,5)))){
C_save_and_reclaim((void *)f_5621,c,av);}
a=C_alloc(17);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5624,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=C_a_i_cons(&a,2,lf[138],lf[139]);
t4=C_a_i_list(&a,1,t3);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8209,a[2]=t2,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8211,a[2]=((C_word)li48),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:689: ##sys#er-transformer */
t7=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t7;
av2[1]=t5;
av2[2]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}

/* k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in ... */
static void C_ccall f_5624(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5624,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5627,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8105,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8107,a[2]=((C_word)li47),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:699: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in ... */
static void C_ccall f_5627(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5627,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5630,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7963,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7965,a[2]=((C_word)li45),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:720: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in ... */
static void C_ccall f_5630(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(29,c,5)))){
C_save_and_reclaim((void *)f_5630,c,av);}
a=C_alloc(29);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5633,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=C_a_i_cons(&a,2,lf[111],lf[112]);
t4=C_a_i_cons(&a,2,lf[94],lf[95]);
t5=C_a_i_cons(&a,2,lf[96],lf[97]);
t6=C_a_i_list(&a,3,t3,t4,t5);
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7515,a[2]=t2,a[3]=t6,tmp=(C_word)a,a+=4,tmp);
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7517,a[2]=((C_word)li43),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:827: ##sys#er-transformer */
t9=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t9;
av2[1]=t7;
av2[2]=t8;
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}

/* k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in ... */
static void C_ccall f_5633(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(29,c,5)))){
C_save_and_reclaim((void *)f_5633,c,av);}
a=C_alloc(29);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5636,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=C_a_i_cons(&a,2,lf[111],lf[112]);
t4=C_a_i_cons(&a,2,lf[94],lf[95]);
t5=C_a_i_cons(&a,2,lf[96],lf[97]);
t6=C_a_i_list(&a,3,t3,t4,t5);
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7438,a[2]=t2,a[3]=t6,tmp=(C_word)a,a+=4,tmp);
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7440,a[2]=((C_word)li32),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:916: ##sys#er-transformer */
t9=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t9;
av2[1]=t7;
av2[2]=t8;
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}

/* k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in ... */
static void C_ccall f_5636(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(29,c,5)))){
C_save_and_reclaim((void *)f_5636,c,av);}
a=C_alloc(29);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5639,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=C_a_i_cons(&a,2,lf[111],lf[112]);
t4=C_a_i_cons(&a,2,lf[94],lf[95]);
t5=C_a_i_cons(&a,2,lf[96],lf[97]);
t6=C_a_i_list(&a,3,t3,t4,t5);
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7278,a[2]=t2,a[3]=t6,tmp=(C_word)a,a+=4,tmp);
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7280,a[2]=((C_word)li31),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:944: ##sys#er-transformer */
t9=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t9;
av2[1]=t7;
av2[2]=t8;
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}

/* k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in ... */
static void C_ccall f_5639(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(41,c,5)))){
C_save_and_reclaim((void *)f_5639,c,av);}
a=C_alloc(41);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5642,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=C_a_i_cons(&a,2,lf[92],lf[93]);
t4=C_a_i_cons(&a,2,lf[94],lf[95]);
t5=C_a_i_cons(&a,2,lf[96],lf[97]);
t6=C_a_i_cons(&a,2,lf[98],lf[99]);
t7=C_a_i_cons(&a,2,lf[100],lf[101]);
t8=C_a_i_list(&a,5,t3,t4,t5,t6,t7);
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6834,a[2]=t2,a[3]=t8,tmp=(C_word)a,a+=4,tmp);
t10=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6836,a[2]=((C_word)li29),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:981: ##sys#er-transformer */
t11=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t11;
av2[1]=t9;
av2[2]=t10;
((C_proc)(void*)(*((C_word*)t11+1)))(3,av2);}}

/* k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in ... */
static void C_ccall f_5642(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5642,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5645,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6721,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6723,a[2]=((C_word)li16),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:1044: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in ... */
static void C_ccall f_5645(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5645,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5648,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6282,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6284,a[2]=((C_word)li15),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:1078: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in ... */
static void C_ccall f_5648(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(17,c,5)))){
C_save_and_reclaim((void *)f_5648,c,av);}
a=C_alloc(17);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5651,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=C_a_i_cons(&a,2,lf[52],lf[53]);
t4=C_a_i_list(&a,1,t3);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6110,a[2]=t2,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6112,a[2]=((C_word)li8),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:1167: ##sys#er-transformer */
t7=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t7;
av2[1]=t5;
av2[2]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}

/* k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in ... */
static void C_ccall f_5651(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(17,c,5)))){
C_save_and_reclaim((void *)f_5651,c,av);}
a=C_alloc(17);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5654,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=C_a_i_cons(&a,2,lf[52],lf[53]);
t4=C_a_i_list(&a,1,t3);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5915,a[2]=t2,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5917,a[2]=((C_word)li6),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:1200: ##sys#er-transformer */
t7=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t7;
av2[1]=t5;
av2[2]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}

/* k5652 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in ... */
static void C_ccall f_5654(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5654,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5657,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5850,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5852,a[2]=((C_word)li4),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:1241: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5655 in k5652 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in ... */
static void C_ccall f_5657(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5657,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5660,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5832,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5834,a[2]=((C_word)li3),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:1258: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5658 in k5655 in k5652 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in ... */
static void C_ccall f_5660(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5660,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5663,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5743,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5745,a[2]=((C_word)li2),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:1267: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5661 in k5658 in k5655 in k5652 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in ... */
static void C_ccall f_5663(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_5663,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5666,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:1287: chicken.internal#macro-subset */
t3=*((C_word*)lf[17]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=*((C_word*)lf[18]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k5664 in k5661 in k5658 in k5655 in k5652 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in ... */
static void C_ccall f_5666(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_5666,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[6]+1 /* (set! ##sys#chicken.base-macro-environment ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5670,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:1293: ##sys#macro-environment */
t4=*((C_word*)lf[19]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k5668 in k5664 in k5661 in k5658 in k5655 in k5652 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in ... */
static void C_ccall f_5670(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_5670,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5673,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5694,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5696,a[2]=((C_word)li1),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:1297: ##sys#er-transformer */
t5=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5671 in k5668 in k5664 in k5661 in k5658 in k5655 in k5652 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in ... */
static void C_ccall f_5673(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_5673,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5676,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:1309: chicken.internal#macro-subset */
t3=*((C_word*)lf[17]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=*((C_word*)lf[18]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k5674 in k5671 in k5668 in k5664 in k5661 in k5658 in k5655 in k5652 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in ... */
static void C_ccall f_5676(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_5676,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[7]+1 /* (set! ##sys#chicken.time-macro-environment ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5680,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:1313: ##sys#macro-environment */
t4=*((C_word*)lf[19]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k5678 in k5674 in k5671 in k5668 in k5664 in k5661 in k5658 in k5655 in k5652 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in ... */
static void C_ccall f_5680(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_5680,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5683,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5690,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:1322: chicken.internal#macro-subset */
t4=*((C_word*)lf[17]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t1;
av2[3]=*((C_word*)lf[18]+1);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k5681 in k5678 in k5674 in k5671 in k5668 in k5664 in k5661 in k5658 in k5655 in k5652 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in ... */
static void C_ccall f_5683(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_5683,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[8]+1 /* (set! ##sys#chicken-macro-environment ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5686,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:1326: chicken.platform#register-feature! */
t4=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[10];
av2[3]=lf[11];
av2[4]=lf[12];
av2[5]=lf[13];
av2[6]=lf[14];
av2[7]=lf[15];
((C_proc)(void*)(*((C_word*)t4+1)))(8,av2);}}

/* k5684 in k5681 in k5678 in k5674 in k5671 in k5668 in k5664 in k5661 in k5658 in k5655 in k5652 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in ... */
static void C_ccall f_5686(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5686,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k5688 in k5678 in k5674 in k5671 in k5668 in k5664 in k5661 in k5658 in k5655 in k5652 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in ... */
static void C_ccall f_5690(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_5690,c,av);}
/* chicken-syntax.scm:1318: scheme#append */
t2=*((C_word*)lf[16]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=*((C_word*)lf[2]+1);
av2[3]=*((C_word*)lf[7]+1);
av2[4]=*((C_word*)lf[3]+1);
av2[5]=*((C_word*)lf[6]+1);
av2[6]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(7,av2);}}

/* k5692 in k5668 in k5664 in k5661 in k5658 in k5655 in k5652 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in ... */
static void C_ccall f_5694(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_5694,c,av);}
/* chicken-syntax.scm:1295: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[21];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a5695 in k5668 in k5664 in k5661 in k5658 in k5655 in k5652 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in ... */
static void C_ccall f_5696(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_5696,c,av);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5700,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:1299: r */
t6=t3;{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[30];
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k5698 in a5695 in k5668 in k5664 in k5661 in k5658 in k5655 in k5652 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in ... */
static void C_ccall f_5700(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(57,c,1)))){
C_save_and_reclaim((void *)f_5700,c,av);}
a=C_alloc(57);
t2=C_a_i_list(&a,1,lf[22]);
t3=C_i_cdr(((C_word*)t0)[2]);
t4=C_a_i_cons(&a,2,C_SCHEME_END_OF_LIST,t3);
t5=C_a_i_cons(&a,2,lf[23],t4);
t6=C_a_i_list(&a,1,lf[24]);
t7=C_a_i_list(&a,2,lf[25],t6);
t8=C_a_i_list(&a,3,lf[26],lf[27],t1);
t9=C_a_i_list(&a,4,lf[23],t1,t7,t8);
t10=C_a_i_list(&a,3,lf[28],t5,t9);
t11=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t11;
av2[1]=C_a_i_list(&a,3,lf[29],t2,t10);
((C_proc)(void*)(*((C_word*)t11+1)))(2,av2);}}

/* k5741 in k5658 in k5655 in k5652 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in ... */
static void C_ccall f_5743(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_5743,c,av);}
/* chicken-syntax.scm:1265: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[32];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a5744 in k5658 in k5655 in k5652 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in ... */
static void C_ccall f_5745(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_5745,c,av);}
a=C_alloc(5);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5749,a[2]=t2,a[3]=t1,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:1270: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[32];
av2[3]=t2;
av2[4]=lf[46];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k5747 in a5744 in k5658 in k5655 in k5652 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in ... */
static void C_ccall f_5749(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_5749,c,av);}
a=C_alloc(9);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=C_u_i_cdr(((C_word*)t0)[2]);
t4=C_u_i_cdr(t3);
t5=C_i_nullp(t4);
t6=(C_truep(t5)?lf[33]:C_i_car(t4));
t7=t6;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5760,a[2]=t2,a[3]=((C_word*)t0)[3],a[4]=t4,a[5]=t8,a[6]=((C_word*)t0)[2],tmp=(C_word)a,a+=7,tmp);
/* chicken-syntax.scm:1274: r */
t10=((C_word*)t0)[4];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t10;
av2[1]=t9;
av2[2]=lf[44];
((C_proc)(void*)(*((C_word*)t10+1)))(3,av2);}}

/* k5758 in k5747 in a5744 in k5658 in k5655 in k5652 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in ... */
static void C_ccall f_5760(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_5760,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5763,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
if(C_truep(C_i_stringp(((C_word*)((C_word*)t0)[5])[1]))){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5815,a[2]=((C_word*)t0)[5],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:1276: chicken.syntax#get-line-number */
t4=*((C_word*)lf[43]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=t2;
f_5763(t3,C_SCHEME_UNDEFINED);}}

/* k5761 in k5758 in k5747 in a5744 in k5658 in k5655 in k5652 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in ... */
static void C_fcall f_5763(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(25,0,2)))){
C_save_and_reclaim_args((void *)trf_5763,2,t0,t1);}
a=C_alloc(25);
t2=C_a_i_list(&a,2,((C_word*)t0)[2],((C_word*)t0)[3]);
t3=C_a_i_list(&a,1,t2);
t4=C_a_i_list(&a,2,lf[34],((C_word*)t0)[2]);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5786,a[2]=t4,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[4],a[5]=t3,tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_i_pairp(((C_word*)t0)[5]))){
t6=C_u_i_cdr(((C_word*)t0)[5]);
t7=t5;
f_5786(t7,C_a_i_cons(&a,2,((C_word*)((C_word*)t0)[6])[1],t6));}
else{
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5805,a[2]=t5,a[3]=((C_word*)t0)[6],tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:1285: chicken.syntax#strip-syntax */
t7=*((C_word*)lf[39]+1);{
C_word av2[3];
av2[0]=t7;
av2[1]=t6;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}}

/* k5784 in k5761 in k5758 in k5747 in a5744 in k5658 in k5655 in k5652 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in ... */
static void C_fcall f_5786(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,0,1)))){
C_save_and_reclaim_args((void *)trf_5786,2,t0,t1);}
a=C_alloc(24);
t2=C_a_i_cons(&a,2,lf[35],t1);
t3=C_a_i_list(&a,4,lf[36],((C_word*)t0)[2],((C_word*)t0)[3],t2);
t4=((C_word*)t0)[4];{
C_word av2[2];
av2[0]=t4;
av2[1]=C_a_i_list(&a,3,lf[37],((C_word*)t0)[5],t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k5803 in k5761 in k5758 in k5747 in a5744 in k5658 in k5655 in k5652 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in ... */
static void C_ccall f_5805(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,1)))){
C_save_and_reclaim((void *)f_5805,c,av);}
a=C_alloc(12);
t2=C_a_i_list(&a,2,lf[38],t1);
t3=C_a_i_list(&a,1,t2);
t4=((C_word*)t0)[2];
f_5786(t4,C_a_i_cons(&a,2,((C_word*)((C_word*)t0)[3])[1],t3));}

/* k5813 in k5758 in k5747 in a5744 in k5658 in k5655 in k5652 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in ... */
static void C_ccall f_5815(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,5)))){
C_save_and_reclaim((void *)f_5815,c,av);}
a=C_alloc(4);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5822,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:1277: string-append */
t3=*((C_word*)lf[40]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[41];
av2[3]=t1;
av2[4]=lf[42];
av2[5]=((C_word*)((C_word*)t0)[2])[1];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}
else{
t2=((C_word*)t0)[3];
f_5763(t2,C_SCHEME_FALSE);}}

/* k5820 in k5813 in k5758 in k5747 in a5744 in k5658 in k5655 in k5652 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in ... */
static void C_ccall f_5822(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5822,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=((C_word*)t0)[3];
f_5763(t3,t2);}

/* k5830 in k5655 in k5652 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in ... */
static void C_ccall f_5832(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_5832,c,av);}
/* chicken-syntax.scm:1255: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[47];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a5833 in k5655 in k5652 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in ... */
static void C_ccall f_5834(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_5834,c,av);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5842,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:1260: r */
t6=t3;{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[48];
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k5840 in a5833 in k5655 in k5652 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in ... */
static void C_ccall f_5842(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_5842,c,av);}
a=C_alloc(3);
t2=C_i_cdr(((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_cons(&a,2,t1,t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k5848 in k5652 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in ... */
static void C_ccall f_5850(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_5850,c,av);}
/* chicken-syntax.scm:1239: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[49];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a5851 in k5652 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in ... */
static void C_ccall f_5852(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_5852,c,av);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5856,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:1243: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[49];
av2[3]=t2;
av2[4]=lf[51];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k5854 in a5851 in k5652 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in ... */
static void C_ccall f_5856(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,1)))){
C_save_and_reclaim((void *)f_5856,c,av);}
a=C_alloc(24);
t2=C_i_cadr(((C_word*)t0)[2]);
if(C_truep(C_i_pairp(t2))){
t3=C_u_i_car(t2);
t4=C_u_i_cdr(t2);
t5=C_u_i_cdr(((C_word*)t0)[2]);
t6=C_u_i_cdr(t5);
t7=C_a_i_cons(&a,2,t4,t6);
t8=C_a_i_cons(&a,2,lf[23],t7);
t9=C_a_i_list(&a,2,t3,t8);
t10=C_a_i_list(&a,1,t9);
t11=C_u_i_car(t2);
t12=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t12;
av2[1]=C_a_i_list(&a,3,lf[50],t10,t11);
((C_proc)(void*)(*((C_word*)t12+1)))(2,av2);}}
else{
t3=C_u_i_cdr(((C_word*)t0)[2]);
t4=C_u_i_cdr(t3);
t5=C_a_i_cons(&a,2,t2,t4);
t6=C_a_i_list(&a,1,t5);
t7=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t7;
av2[1]=C_a_i_list(&a,3,lf[50],t6,t2);
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}}

/* k5913 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in ... */
static void C_ccall f_5915(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_5915,c,av);}
/* chicken-syntax.scm:1197: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[54];
av2[3]=((C_word*)t0)[3];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a5916 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in ... */
static void C_ccall f_5917(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_5917,c,av);}
a=C_alloc(6);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5921,a[2]=t2,a[3]=t3,a[4]=t4,a[5]=t1,tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:1202: r */
t6=t3;{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[52];
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k5919 in a5916 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in ... */
static void C_ccall f_5921(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_5921,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5924,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* chicken-syntax.scm:1203: r */
t3=((C_word*)t0)[3];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[62];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k5922 in k5919 in a5916 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in ... */
static void C_ccall f_5924(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_5924,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_5927,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* chicken-syntax.scm:1204: r */
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[61];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k5925 in k5922 in k5919 in a5916 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in ... */
static void C_ccall f_5927(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_5927,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_5930,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
t3=C_i_cdr(((C_word*)t0)[2]);
if(C_truep(C_i_nullp(t3))){
/* chicken-syntax.scm:1206: chicken.syntax#syntax-error */
t4=*((C_word*)lf[58]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=lf[54];
av2[3]=lf[60];
av2[4]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}
else{
t4=t2;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_5930(2,av2);}}}

/* k5928 in k5925 in k5922 in k5919 in a5916 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in ... */
static void C_ccall f_5930(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,7)))){
C_save_and_reclaim((void *)f_5930,c,av);}
a=C_alloc(12);
t2=C_u_i_cdr(((C_word*)t0)[2]);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_5937,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t4,a[5]=((C_word*)t0)[2],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word)li5),tmp=(C_word)a,a+=10,tmp));
t6=((C_word*)t4)[1];
f_5937(t6,((C_word*)t0)[8],t2,C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST,C_SCHEME_FALSE);}

/* loop in k5928 in k5925 in k5922 in k5919 in a5916 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in ... */
static void C_fcall f_5937(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6){
C_word tmp;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,3)))){
C_save_and_reclaim_args((void *)trf_5937,7,t0,t1,t2,t3,t4,t5,t6);}
a=C_alloc(12);
if(C_truep(C_i_nullp(t2))){
t7=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_5947,a[2]=t6,a[3]=((C_word*)t0)[2],a[4]=t1,a[5]=t4,a[6]=((C_word*)t0)[3],a[7]=t5,tmp=(C_word)a,a+=8,tmp);
/* chicken-syntax.scm:1209: scheme#reverse */
t8=*((C_word*)lf[57]+1);{
C_word av2[3];
av2[0]=t8;
av2[1]=t7;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t8+1)))(3,av2);}}
else{
t7=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_6016,a[2]=t2,a[3]=t3,a[4]=t5,a[5]=((C_word*)t0)[4],a[6]=t1,a[7]=t4,a[8]=((C_word*)t0)[3],a[9]=((C_word*)t0)[5],a[10]=((C_word*)t0)[6],a[11]=((C_word*)t0)[7],tmp=(C_word)a,a+=12,tmp);
/* chicken-syntax.scm:1219: c */
t8=((C_word*)t0)[6];{
C_word av2[4];
av2[0]=t8;
av2[1]=t7;
av2[2]=((C_word*)t0)[8];
av2[3]=C_i_car(t2);
((C_proc)(void*)(*((C_word*)t8+1)))(4,av2);}}}

/* k5945 in loop in k5928 in k5925 in k5922 in k5919 in a5916 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in ... */
static void C_ccall f_5947(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_5947,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_5950,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* chicken-syntax.scm:1210: scheme#reverse */
t3=*((C_word*)lf[57]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k5948 in k5945 in loop in k5928 in k5925 in k5922 in k5919 in a5916 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in ... */
static void C_ccall f_5950(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(21,c,2)))){
C_save_and_reclaim((void *)f_5950,c,av);}
a=C_alloc(21);
if(C_truep(((C_word*)t0)[2])){
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5956,a[2]=t1,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5993,a[2]=((C_word*)t0)[7],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:1212: chicken.base#gensym */
t4=*((C_word*)lf[56]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t2=C_i_car(t1);
t3=C_u_i_cdr(t1);
t4=C_a_i_cons(&a,2,t2,t3);
t5=C_a_i_list(&a,3,lf[23],((C_word*)t0)[6],t4);
t6=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_a_i_list(&a,3,lf[37],((C_word*)t0)[5],t5);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* k5954 in k5948 in k5945 in loop in k5928 in k5925 in k5922 in k5919 in a5916 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in ... */
static void C_ccall f_5956(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_5956,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5967,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,tmp=(C_word)a,a+=7,tmp);
/* chicken-syntax.scm:1213: ##sys#append */
t3=*((C_word*)lf[55]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[6];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k5965 in k5954 in k5948 in k5945 in loop in k5928 in k5925 in k5922 in k5919 in a5916 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in ... */
static void C_ccall f_5967(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_5967,c,av);}
a=C_alloc(10);
t2=C_i_car(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5983,a[2]=t2,a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
t4=C_u_i_cdr(((C_word*)t0)[2]);
t5=C_a_i_list(&a,1,((C_word*)t0)[6]);
/* chicken-syntax.scm:1213: ##sys#append */
t6=*((C_word*)lf[55]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t3;
av2[2]=t4;
av2[3]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* k5981 in k5965 in k5954 in k5948 in k5945 in loop in k5928 in k5925 in k5922 in k5919 in a5916 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in ... */
static void C_ccall f_5983(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,1)))){
C_save_and_reclaim((void *)f_5983,c,av);}
a=C_alloc(24);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_cons(&a,2,((C_word*)t0)[3],t2);
t4=C_a_i_list(&a,3,lf[23],((C_word*)t0)[4],t3);
t5=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_a_i_list(&a,3,lf[37],((C_word*)t0)[6],t4);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* k5991 in k5948 in k5945 in loop in k5928 in k5925 in k5922 in k5919 in a5916 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in ... */
static void C_ccall f_5993(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5993,c,av);}
/* chicken-syntax.scm:1212: r */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k6014 in loop in k5928 in k5925 in k5922 in k5919 in a5916 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in ... */
static void C_ccall f_6016(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_6016,c,av);}
a=C_alloc(12);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6019,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6036,a[2]=((C_word*)t0)[8],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:1220: chicken.base#gensym */
t4=*((C_word*)lf[56]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_6042,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[4],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
/* chicken-syntax.scm:1222: c */
t3=((C_word*)t0)[10];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[11];
av2[3]=C_u_i_car(((C_word*)t0)[2]);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}}

/* k6017 in k6014 in loop in k5928 in k5925 in k5922 in k5919 in a5916 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in ... */
static void C_ccall f_6019(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,6)))){
C_save_and_reclaim((void *)f_6019,c,av);}
a=C_alloc(6);
t2=C_u_i_cdr(((C_word*)t0)[2]);
t3=C_a_i_cons(&a,2,t1,((C_word*)t0)[3]);
t4=C_a_i_cons(&a,2,t1,((C_word*)t0)[4]);
/* chicken-syntax.scm:1221: loop */
t5=((C_word*)((C_word*)t0)[5])[1];
f_5937(t5,((C_word*)t0)[6],t2,t3,((C_word*)t0)[7],t4,C_SCHEME_FALSE);}

/* k6034 in k6014 in loop in k5928 in k5925 in k5922 in k5919 in a5916 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in ... */
static void C_ccall f_6036(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6036,c,av);}
/* chicken-syntax.scm:1220: r */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k6040 in k6014 in loop in k5928 in k5925 in k5922 in k5919 in a5916 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in ... */
static void C_ccall f_6042(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,6)))){
C_save_and_reclaim((void *)f_6042,c,av);}
a=C_alloc(12);
if(C_truep(t1)){
if(C_truep(C_i_nullp(C_u_i_cdr(((C_word*)t0)[2])))){
/* chicken-syntax.scm:1224: loop */
t2=((C_word*)((C_word*)t0)[3])[1];
f_5937(t2,((C_word*)t0)[4],C_SCHEME_END_OF_LIST,((C_word*)t0)[5],((C_word*)t0)[6],((C_word*)t0)[7],C_SCHEME_TRUE);}
else{
/* chicken-syntax.scm:1225: chicken.syntax#syntax-error */
t2=*((C_word*)lf[58]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[4];
av2[2]=lf[54];
av2[3]=lf[59];
av2[4]=((C_word*)t0)[8];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}}
else{
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6059,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[7],a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[5],tmp=(C_word)a,a+=8,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6082,a[2]=((C_word*)t0)[9],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:1230: chicken.base#gensym */
t4=*((C_word*)lf[56]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k6057 in k6040 in k6014 in loop in k5928 in k5925 in k5922 in k5919 in a5916 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in ... */
static void C_ccall f_6059(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,6)))){
C_save_and_reclaim((void *)f_6059,c,av);}
a=C_alloc(12);
t2=C_u_i_cdr(((C_word*)t0)[2]);
t3=C_u_i_car(((C_word*)t0)[2]);
t4=C_a_i_list2(&a,2,t1,t3);
t5=C_a_i_cons(&a,2,t4,((C_word*)t0)[3]);
t6=C_a_i_cons(&a,2,t1,((C_word*)t0)[4]);
/* chicken-syntax.scm:1231: loop */
t7=((C_word*)((C_word*)t0)[5])[1];
f_5937(t7,((C_word*)t0)[6],t2,((C_word*)t0)[7],t5,t6,C_SCHEME_FALSE);}

/* k6080 in k6040 in k6014 in loop in k5928 in k5925 in k5922 in k5919 in a5916 in k5649 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in ... */
static void C_ccall f_6082(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6082,c,av);}
/* chicken-syntax.scm:1230: r */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k6108 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in ... */
static void C_ccall f_6110(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6110,c,av);}
/* chicken-syntax.scm:1164: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[63];
av2[3]=((C_word*)t0)[3];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a6111 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in ... */
static void C_ccall f_6112(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_6112,c,av);}
a=C_alloc(6);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6116,a[2]=t2,a[3]=t3,a[4]=t4,a[5]=t1,tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:1169: r */
t6=t3;{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[62];
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k6114 in a6111 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in ... */
static void C_ccall f_6116(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_6116,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6119,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* chicken-syntax.scm:1170: r */
t3=((C_word*)t0)[3];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[61];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k6117 in k6114 in a6111 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in ... */
static void C_ccall f_6119(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_6119,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6122,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* chicken-syntax.scm:1171: r */
t3=((C_word*)t0)[3];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[52];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k6120 in k6117 in k6114 in a6111 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in ... */
static void C_ccall f_6122(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_6122,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_6125,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
t3=C_i_cdr(((C_word*)t0)[2]);
if(C_truep(C_i_nullp(t3))){
/* chicken-syntax.scm:1173: chicken.syntax#syntax-error */
t4=*((C_word*)lf[58]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=lf[63];
av2[3]=lf[65];
av2[4]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}
else{
t4=t2;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_6125(2,av2);}}}

/* k6123 in k6120 in k6117 in k6114 in a6111 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in ... */
static void C_ccall f_6125(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,6)))){
C_save_and_reclaim((void *)f_6125,c,av);}
a=C_alloc(12);
t2=C_u_i_cdr(((C_word*)t0)[2]);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_6132,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t4,a[5]=((C_word*)t0)[2],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word)li7),tmp=(C_word)a,a+=10,tmp));
t6=((C_word*)t4)[1];
f_6132(t6,((C_word*)t0)[8],t2,C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST,C_SCHEME_FALSE);}

/* loop in k6123 in k6120 in k6117 in k6114 in a6111 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in ... */
static void C_fcall f_6132(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5){
C_word tmp;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,0,3)))){
C_save_and_reclaim_args((void *)trf_6132,6,t0,t1,t2,t3,t4,t5);}
a=C_alloc(11);
if(C_truep(C_i_nullp(t2))){
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6142,a[2]=t5,a[3]=((C_word*)t0)[2],a[4]=t1,a[5]=((C_word*)t0)[3],a[6]=t4,tmp=(C_word)a,a+=7,tmp);
/* chicken-syntax.scm:1176: scheme#reverse */
t7=*((C_word*)lf[57]+1);{
C_word av2[3];
av2[0]=t7;
av2[1]=t6;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}
else{
t6=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_6207,a[2]=t2,a[3]=t3,a[4]=t4,a[5]=((C_word*)t0)[4],a[6]=t1,a[7]=((C_word*)t0)[3],a[8]=((C_word*)t0)[5],a[9]=((C_word*)t0)[6],a[10]=((C_word*)t0)[7],tmp=(C_word)a,a+=11,tmp);
/* chicken-syntax.scm:1185: c */
t7=((C_word*)t0)[6];{
C_word av2[4];
av2[0]=t7;
av2[1]=t6;
av2[2]=((C_word*)t0)[8];
av2[3]=C_i_car(t2);
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}}

/* k6140 in loop in k6123 in k6120 in k6117 in k6114 in a6111 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in ... */
static void C_ccall f_6142(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_6142,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6145,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* chicken-syntax.scm:1177: scheme#reverse */
t3=*((C_word*)lf[57]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k6143 in k6140 in loop in k6123 in k6120 in k6117 in k6114 in a6111 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in ... */
static void C_ccall f_6145(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,2)))){
C_save_and_reclaim((void *)f_6145,c,av);}
a=C_alloc(18);
if(C_truep(((C_word*)t0)[2])){
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6151,a[2]=t1,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6184,a[2]=((C_word*)t0)[6],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:1179: chicken.base#gensym */
t4=*((C_word*)lf[56]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t2=C_i_car(t1);
t3=C_a_i_list(&a,2,lf[29],t2);
t4=C_u_i_cdr(t1);
t5=C_a_i_cons(&a,2,t3,t4);
t6=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_a_i_list(&a,3,lf[23],((C_word*)t0)[5],t5);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* k6149 in k6143 in k6140 in loop in k6123 in k6120 in k6117 in k6114 in a6111 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in ... */
static void C_ccall f_6151(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_6151,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6158,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:1180: ##sys#append */
t3=*((C_word*)lf[55]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k6156 in k6149 in k6143 in k6140 in loop in k6123 in k6120 in k6117 in k6114 in a6111 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in ... */
static void C_ccall f_6158(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_6158,c,av);}
a=C_alloc(9);
t2=C_i_car(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6174,a[2]=t2,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
t4=C_u_i_cdr(((C_word*)t0)[2]);
t5=C_a_i_list(&a,1,((C_word*)t0)[5]);
/* chicken-syntax.scm:1180: ##sys#append */
t6=*((C_word*)lf[55]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t3;
av2[2]=t4;
av2[3]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* k6172 in k6156 in k6149 in k6143 in k6140 in loop in k6123 in k6120 in k6117 in k6114 in a6111 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in ... */
static void C_ccall f_6174(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,1)))){
C_save_and_reclaim((void *)f_6174,c,av);}
a=C_alloc(15);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_cons(&a,2,((C_word*)t0)[3],t2);
t4=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_list(&a,3,lf[23],((C_word*)t0)[5],t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k6182 in k6143 in k6140 in loop in k6123 in k6120 in k6117 in k6114 in a6111 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in ... */
static void C_ccall f_6184(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6184,c,av);}
/* chicken-syntax.scm:1179: r */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k6205 in loop in k6123 in k6120 in k6117 in k6114 in a6111 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in ... */
static void C_ccall f_6207(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,3)))){
C_save_and_reclaim((void *)f_6207,c,av);}
a=C_alloc(11);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6210,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6227,a[2]=((C_word*)t0)[7],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:1186: chicken.base#gensym */
t4=*((C_word*)lf[56]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6233,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[8],tmp=(C_word)a,a+=8,tmp);
/* chicken-syntax.scm:1188: c */
t3=((C_word*)t0)[9];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[10];
av2[3]=C_u_i_car(((C_word*)t0)[2]);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}}

/* k6208 in k6205 in loop in k6123 in k6120 in k6117 in k6114 in a6111 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in ... */
static void C_ccall f_6210(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,5)))){
C_save_and_reclaim((void *)f_6210,c,av);}
a=C_alloc(6);
t2=C_u_i_cdr(((C_word*)t0)[2]);
t3=C_a_i_cons(&a,2,t1,((C_word*)t0)[3]);
t4=C_a_i_cons(&a,2,t1,((C_word*)t0)[4]);
/* chicken-syntax.scm:1187: loop */
t5=((C_word*)((C_word*)t0)[5])[1];
f_6132(t5,((C_word*)t0)[6],t2,t3,t4,C_SCHEME_FALSE);}

/* k6225 in k6205 in loop in k6123 in k6120 in k6117 in k6114 in a6111 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in ... */
static void C_ccall f_6227(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6227,c,av);}
/* chicken-syntax.scm:1186: r */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k6231 in k6205 in loop in k6123 in k6120 in k6117 in k6114 in a6111 in k5646 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in ... */
static void C_ccall f_6233(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_6233,c,av);}
a=C_alloc(3);
if(C_truep(t1)){
if(C_truep(C_i_nullp(C_u_i_cdr(((C_word*)t0)[2])))){
/* chicken-syntax.scm:1190: loop */
t2=((C_word*)((C_word*)t0)[3])[1];
f_6132(t2,((C_word*)t0)[4],C_SCHEME_END_OF_LIST,((C_word*)t0)[5],((C_word*)t0)[6],C_SCHEME_TRUE);}
else{
/* chicken-syntax.scm:1191: chicken.syntax#syntax-error */
t2=*((C_word*)lf[58]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[4];
av2[2]=lf[63];
av2[3]=lf[64];
av2[4]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}}
else{
t2=C_u_i_cdr(((C_word*)t0)[2]);
t3=C_u_i_car(((C_word*)t0)[2]);
t4=C_a_i_cons(&a,2,t3,((C_word*)t0)[6]);
/* chicken-syntax.scm:1195: loop */
t5=((C_word*)((C_word*)t0)[3])[1];
f_6132(t5,((C_word*)t0)[4],t2,((C_word*)t0)[5],t4,C_SCHEME_FALSE);}}

/* k6280 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in ... */
static void C_ccall f_6282(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6282,c,av);}
/* chicken-syntax.scm:1075: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[66];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a6283 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in ... */
static void C_ccall f_6284(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_6284,c,av);}
a=C_alloc(6);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6288,a[2]=t2,a[3]=t1,a[4]=t4,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:1080: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[66];
av2[3]=t2;
av2[4]=lf[86];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k6286 in a6283 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in ... */
static void C_ccall f_6288(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_6288,c,av);}
a=C_alloc(7);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6294,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* chicken-syntax.scm:1085: chicken.syntax#strip-syntax */
t4=*((C_word*)lf[39]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k6292 in k6286 in a6283 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in ... */
static void C_ccall f_6294(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_6294,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6297,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6706,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:1086: ##sys#current-module */
t4=*((C_word*)lf[85]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k6295 in k6292 in k6286 in a6283 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in ... */
static void C_ccall f_6297(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_6297,c,av);}
a=C_alloc(10);
t2=C_i_caddr(((C_word*)t0)[2]);
t3=C_i_cadddr(((C_word*)t0)[2]);
t4=C_i_cddddr(((C_word*)t0)[2]);
t5=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_6309,a[2]=t2,a[3]=t4,a[4]=t1,a[5]=((C_word*)t0)[3],a[6]=t3,a[7]=((C_word*)t0)[4],a[8]=((C_word*)t0)[5],a[9]=((C_word*)t0)[6],tmp=(C_word)a,a+=10,tmp);
/* chicken-syntax.scm:1094: r */
t6=((C_word*)t0)[6];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[81];
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k6307 in k6295 in k6292 in k6286 in a6283 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in ... */
static void C_ccall f_6309(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_6309,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_6312,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],tmp=(C_word)a,a+=11,tmp);
/* chicken-syntax.scm:1095: r */
t3=((C_word*)t0)[9];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[80];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k6310 in k6307 in k6295 in k6292 in k6286 in a6283 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in ... */
static void C_ccall f_6312(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_6312,c,av);}
a=C_alloc(13);
t2=C_i_cdr(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_6318,a[2]=((C_word*)t0)[3],a[3]=t2,a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[2],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=t1,a[12]=((C_word*)t0)[10],tmp=(C_word)a,a+=13,tmp);
/* chicken-syntax.scm:1097: r */
t4=((C_word*)t0)[10];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[79];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k6316 in k6310 in k6307 in k6295 in k6292 in k6286 in a6283 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in ... */
static void C_ccall f_6318(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_6318,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_6321,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=t1,a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],tmp=(C_word)a,a+=13,tmp);
/* chicken-syntax.scm:1098: r */
t3=((C_word*)t0)[12];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[78];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k6319 in k6316 in k6310 in k6307 in k6295 in k6292 in k6286 in a6283 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in ... */
static void C_ccall f_6321(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(27,c,3)))){
C_save_and_reclaim((void *)f_6321,c,av);}
a=C_alloc(27);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=C_i_check_list_2(((C_word*)t0)[2],lf[67]);
t7=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_6330,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],a[12]=t1,a[13]=((C_word*)t0)[2],tmp=(C_word)a,a+=14,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6671,a[2]=t4,a[3]=t9,a[4]=t5,a[5]=((C_word)li14),tmp=(C_word)a,a+=6,tmp));
t11=((C_word*)t9)[1];
f_6671(t11,t7,((C_word*)t0)[2]);}

/* k6328 in k6319 in k6316 in k6310 in k6307 in k6295 in k6292 in k6286 in a6283 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in ... */
static void C_ccall f_6330(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(26,c,3)))){
C_save_and_reclaim((void *)f_6330,c,av);}
a=C_alloc(26);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6331,a[2]=t1,a[3]=((C_word)li9),tmp=(C_word)a,a+=4,tmp);
t3=C_i_check_list_2(((C_word*)t0)[2],lf[69]);
t4=(*a=C_CLOSURE_TYPE|14,a[1]=(C_word)f_6347,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[2],a[6]=t1,a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],a[13]=((C_word*)t0)[12],a[14]=((C_word*)t0)[13],tmp=(C_word)a,a+=15,tmp);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6648,a[2]=t6,a[3]=t2,a[4]=((C_word)li13),tmp=(C_word)a,a+=5,tmp));
t8=((C_word*)t6)[1];
f_6648(t8,t4,((C_word*)t0)[2]);}

/* g3036 in k6328 in k6319 in k6316 in k6310 in k6307 in k6295 in k6292 in k6286 in a6283 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in ... */
static void C_fcall f_6331(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,4)))){
C_save_and_reclaim_args((void *)trf_6331,3,t0,t1,t2);}
if(C_truep(C_i_memq(t2,((C_word*)t0)[2]))){
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
/* chicken-syntax.scm:1103: chicken.syntax#syntax-error */
t3=*((C_word*)lf[58]+1);{
C_word av2[5];
av2[0]=t3;
av2[1]=t1;
av2[2]=lf[66];
av2[3]=lf[68];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}}

/* k6345 in k6328 in k6319 in k6316 in k6310 in k6307 in k6295 in k6292 in k6286 in a6283 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in ... */
static void C_ccall f_6347(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(53,c,3)))){
C_save_and_reclaim((void *)f_6347,c,av);}
a=C_alloc(53);
t2=C_a_i_list(&a,2,lf[38],((C_word*)t0)[2]);
t3=C_a_i_list(&a,3,((C_word*)t0)[3],((C_word*)t0)[4],t2);
t4=C_a_i_list(&a,2,lf[38],((C_word*)t0)[2]);
t5=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t6=t5;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=((C_word*)t7)[1];
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6595,a[2]=((C_word*)t0)[5],a[3]=((C_word)li10),tmp=(C_word)a,a+=4,tmp);
t10=C_i_check_list_2(((C_word*)t0)[6],lf[67]);
t11=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_6608,a[2]=t4,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[7],a[5]=((C_word*)t0)[8],a[6]=((C_word*)t0)[9],a[7]=((C_word*)t0)[2],a[8]=t3,a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],a[12]=((C_word*)t0)[13],a[13]=((C_word*)t0)[14],tmp=(C_word)a,a+=14,tmp);
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6610,a[2]=t9,a[3]=t7,a[4]=t13,a[5]=t8,a[6]=((C_word)li12),tmp=(C_word)a,a+=7,tmp));
t15=((C_word*)t13)[1];
f_6610(t15,t11,((C_word*)t0)[6]);}

/* k6376 in k6606 in k6345 in k6328 in k6319 in k6316 in k6310 in k6307 in k6295 in k6292 in k6286 in a6283 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in ... */
static void C_ccall f_6378(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,1)))){
C_save_and_reclaim((void *)f_6378,c,av);}
a=C_alloc(12);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_cons(&a,2,((C_word*)t0)[3],t2);
t4=C_a_i_cons(&a,2,((C_word*)t0)[4],t3);
t5=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_a_i_cons(&a,2,lf[29],t4);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* loop in k6606 in k6345 in k6328 in k6319 in k6316 in k6310 in k6307 in k6295 in k6292 in k6286 in a6283 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in ... */
static void C_fcall f_6380(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(19,0,3)))){
C_save_and_reclaim_args((void *)trf_6380,4,t0,t1,t2,t3);}
a=C_alloc(19);
if(C_truep(C_i_nullp(t2))){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=C_i_car(t2);
t5=C_i_cddr(t4);
t6=C_i_pairp(t5);
t7=(C_truep(t6)?C_i_caddr(t4):C_SCHEME_FALSE);
t8=(*a=C_CLOSURE_TYPE|14,a[1]=(C_word)f_6399,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t4,a[5]=t3,a[6]=t1,a[7]=t2,a[8]=((C_word*)t0)[4],a[9]=t6,a[10]=((C_word*)t0)[5],a[11]=((C_word*)t0)[6],a[12]=t7,a[13]=((C_word*)t0)[7],a[14]=((C_word*)t0)[8],tmp=(C_word)a,a+=15,tmp);
if(C_truep(C_i_pairp(t7))){
if(C_truep(C_i_pairp(C_u_i_cdr(t7)))){
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6552,a[2]=t8,a[3]=t7,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:1128: c */
t10=((C_word*)t0)[5];{
C_word av2[4];
av2[0]=t10;
av2[1]=t9;
av2[2]=lf[77];
av2[3]=C_u_i_car(t7);
((C_proc)(void*)(*((C_word*)t10+1)))(4,av2);}}
else{
t9=t8;
f_6399(t9,C_SCHEME_FALSE);}}
else{
t9=t8;
f_6399(t9,C_SCHEME_FALSE);}}}

/* k6397 in loop in k6606 in k6345 in k6328 in k6319 in k6316 in k6310 in k6307 in k6295 in k6292 in k6286 in a6283 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in ... */
static void C_fcall f_6399(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(128,0,2)))){
C_save_and_reclaim_args((void *)trf_6399,2,t0,t1);}
a=C_alloc(128);
t2=C_a_i_list(&a,1,((C_word*)t0)[2]);
t3=C_a_i_list(&a,2,lf[38],((C_word*)t0)[3]);
t4=C_i_cadr(((C_word*)t0)[4]);
t5=C_a_i_list(&a,2,lf[38],t4);
t6=C_a_i_list(&a,4,lf[73],((C_word*)t0)[2],t3,t5);
t7=C_a_i_list(&a,2,lf[34],t6);
t8=C_a_i_list(&a,3,lf[74],((C_word*)t0)[2],((C_word*)t0)[5]);
t9=C_a_i_list(&a,4,lf[23],t2,t7,t8);
t10=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_6405,a[2]=((C_word*)t0)[6],a[3]=((C_word*)t0)[7],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[8],a[6]=((C_word*)t0)[9],a[7]=t1,a[8]=((C_word*)t0)[10],a[9]=((C_word*)t0)[4],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],a[12]=((C_word*)t0)[13],a[13]=t9,tmp=(C_word)a,a+=14,tmp);
if(C_truep(((C_word*)t0)[9])){
t11=C_a_i_list(&a,2,((C_word*)t0)[2],((C_word*)t0)[14]);
t12=C_a_i_list(&a,2,lf[38],((C_word*)t0)[3]);
t13=C_a_i_list(&a,2,lf[38],t1);
t14=C_a_i_list(&a,4,lf[73],((C_word*)t0)[2],t12,t13);
t15=C_a_i_list(&a,2,lf[34],t14);
t16=C_a_i_list(&a,4,lf[76],((C_word*)t0)[2],((C_word*)t0)[5],((C_word*)t0)[14]);
t17=t10;
f_6405(t17,C_a_i_list(&a,4,lf[23],t11,t15,t16));}
else{
t11=t10;
f_6405(t11,C_SCHEME_FALSE);}}

/* k6403 in k6397 in loop in k6606 in k6345 in k6328 in k6319 in k6316 in k6310 in k6307 in k6295 in k6292 in k6286 in a6283 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in ... */
static void C_fcall f_6405(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(21,0,3)))){
C_save_and_reclaim_args((void *)trf_6405,2,t0,t1);}
a=C_alloc(21);
t2=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_6412,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=t1,a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],tmp=(C_word)a,a+=13,tmp);
t3=C_u_i_cdr(((C_word*)t0)[9]);
t4=C_u_i_car(t3);
t5=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6473,a[2]=((C_word*)t0)[12],a[3]=((C_word*)t0)[13],a[4]=t1,a[5]=t2,a[6]=((C_word*)t0)[10],a[7]=t4,tmp=(C_word)a,a+=8,tmp);
if(C_truep(((C_word*)t0)[7])){
t6=C_u_i_cdr(((C_word*)t0)[9]);
/* chicken-syntax.scm:1149: c */
t7=((C_word*)t0)[8];{
C_word av2[4];
av2[0]=t7;
av2[1]=t5;
av2[2]=((C_word*)t0)[7];
av2[3]=C_u_i_car(t6);
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}
else{
t6=t5;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_SCHEME_FALSE;
f_6473(2,av2);}}}

/* k6410 in k6403 in k6397 in loop in k6606 in k6345 in k6328 in k6319 in k6316 in k6310 in k6307 in k6295 in k6292 in k6286 in a6283 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in ... */
static void C_fcall f_6412(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(22,0,3)))){
C_save_and_reclaim_args((void *)trf_6412,2,t0,t1);}
a=C_alloc(22);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6416,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6420,a[2]=t2,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
if(C_truep(((C_word*)t0)[6])){
if(C_truep(((C_word*)t0)[7])){
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6452,a[2]=((C_word*)t0)[7],a[3]=((C_word*)t0)[8],a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:1154: c */
t5=((C_word*)t0)[9];{
C_word av2[4];
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[7];
av2[3]=C_i_cadr(((C_word*)t0)[10]);
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}
else{
t4=C_a_i_list(&a,3,((C_word*)t0)[11],((C_word*)t0)[12],((C_word*)t0)[8]);
t5=t3;
f_6420(t5,C_a_i_list(&a,1,t4));}}
else{
t4=t3;
f_6420(t4,C_SCHEME_END_OF_LIST);}}

/* k6414 in k6410 in k6403 in k6397 in loop in k6606 in k6345 in k6328 in k6319 in k6316 in k6310 in k6307 in k6295 in k6292 in k6286 in a6283 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in ... */
static void C_ccall f_6416(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_6416,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k6418 in k6410 in k6403 in k6397 in loop in k6606 in k6345 in k6328 in k6319 in k6316 in k6310 in k6307 in k6295 in k6292 in k6286 in a6283 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in ... */
static void C_fcall f_6420(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(33,0,3)))){
C_save_and_reclaim_args((void *)trf_6420,2,t0,t1);}
a=C_alloc(33);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6424,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=C_u_i_cdr(((C_word*)t0)[3]);
t4=C_s_a_i_plus(&a,2,((C_word*)t0)[4],C_fix(1));
/* chicken-syntax.scm:1159: loop */
t5=((C_word*)((C_word*)t0)[5])[1];
f_6380(t5,t2,t3,t4);}

/* k6422 in k6418 in k6410 in k6403 in k6397 in loop in k6606 in k6345 in k6328 in k6319 in k6316 in k6310 in k6307 in k6295 in k6292 in k6286 in a6283 in k5643 in k5640 in k5637 in k5634 in k5631 in ... */
static void C_ccall f_6424(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6424,c,av);}
/* chicken-syntax.scm:1147: ##sys#append */
t2=*((C_word*)lf[55]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k6450 in k6410 in k6403 in k6397 in loop in k6606 in k6345 in k6328 in k6319 in k6316 in k6310 in k6307 in k6295 in k6292 in k6286 in a6283 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in ... */
static void C_ccall f_6452(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,1)))){
C_save_and_reclaim((void *)f_6452,c,av);}
a=C_alloc(18);
if(C_truep(C_i_not(t1))){
t2=C_a_i_list(&a,2,lf[75],lf[75]);
t3=C_a_i_list(&a,3,t2,((C_word*)t0)[2],((C_word*)t0)[3]);
t4=((C_word*)t0)[4];
f_6420(t4,C_a_i_list(&a,1,t3));}
else{
t2=((C_word*)t0)[4];
f_6420(t2,C_SCHEME_END_OF_LIST);}}

/* k6471 in k6403 in k6397 in loop in k6606 in k6345 in k6328 in k6319 in k6316 in k6310 in k6307 in k6295 in k6292 in k6286 in a6283 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in ... */
static void C_ccall f_6473(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,1)))){
C_save_and_reclaim((void *)f_6473,c,av);}
a=C_alloc(18);
if(C_truep(t1)){
t2=C_a_i_list(&a,3,((C_word*)t0)[2],((C_word*)t0)[3],((C_word*)t0)[4]);
t3=((C_word*)t0)[5];
f_6412(t3,C_a_i_list(&a,3,((C_word*)t0)[6],((C_word*)t0)[7],t2));}
else{
t2=((C_word*)t0)[5];
f_6412(t2,C_a_i_list(&a,3,((C_word*)t0)[6],((C_word*)t0)[7],((C_word*)t0)[3]));}}

/* k6550 in loop in k6606 in k6345 in k6328 in k6319 in k6316 in k6310 in k6307 in k6295 in k6292 in k6286 in a6283 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in ... */
static void C_ccall f_6552(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6552,c,av);}
t2=((C_word*)t0)[2];
f_6399(t2,(C_truep(t1)?C_i_cadr(((C_word*)t0)[3]):C_SCHEME_FALSE));}

/* g3062 in k6345 in k6328 in k6319 in k6316 in k6310 in k6307 in k6295 in k6292 in k6286 in a6283 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in ... */
static C_word C_fcall f_6595(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_stack_overflow_check;{}
t2=C_i_memq(t1,((C_word*)t0)[2]);
return((C_truep(t2)?t1:lf[70]));}

/* k6606 in k6345 in k6328 in k6319 in k6316 in k6310 in k6307 in k6295 in k6292 in k6286 in a6283 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in ... */
static void C_ccall f_6608(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(63,c,4)))){
C_save_and_reclaim((void *)f_6608,c,av);}
a=C_alloc(63);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_cons(&a,2,lf[71],t2);
t4=C_a_i_list(&a,3,((C_word*)t0)[3],((C_word*)t0)[4],t3);
t5=C_a_i_list(&a,2,((C_word*)t0)[5],((C_word*)t0)[6]);
t6=C_a_i_list(&a,2,lf[38],((C_word*)t0)[7]);
t7=C_a_i_list(&a,3,lf[72],((C_word*)t0)[6],t6);
t8=C_a_i_list(&a,3,((C_word*)t0)[3],t5,t7);
t9=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6378,a[2]=t8,a[3]=t4,a[4]=((C_word*)t0)[8],a[5]=((C_word*)t0)[9],tmp=(C_word)a,a+=6,tmp);
t10=C_SCHEME_UNDEFINED;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_set_block_item(t11,0,(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_6380,a[2]=((C_word*)t0)[6],a[3]=((C_word*)t0)[7],a[4]=t11,a[5]=((C_word*)t0)[10],a[6]=((C_word*)t0)[3],a[7]=((C_word*)t0)[11],a[8]=((C_word*)t0)[12],a[9]=((C_word)li11),tmp=(C_word)a,a+=10,tmp));
t13=((C_word*)t11)[1];
f_6380(t13,t9,((C_word*)t0)[13],C_fix(1));}

/* map-loop3056 in k6345 in k6328 in k6319 in k6316 in k6310 in k6307 in k6295 in k6292 in k6286 in a6283 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in ... */
static void C_fcall f_6610(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_6610,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=(
/* chicken-syntax.scm:1114: g3062 */
  f_6595(((C_word*)t0)[2],C_slot(t2,C_fix(0)))
);
t4=C_a_i_cons(&a,2,t3,C_SCHEME_END_OF_LIST);
t5=C_i_setslot(((C_word*)((C_word*)t0)[3])[1],C_fix(1),t4);
t6=C_mutate(((C_word *)((C_word*)t0)[3])+1,t4);
t8=t1;
t9=C_slot(t2,C_fix(1));
t1=t8;
t2=t9;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* for-each-loop3035 in k6328 in k6319 in k6316 in k6310 in k6307 in k6295 in k6292 in k6286 in a6283 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in ... */
static void C_fcall f_6648(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_6648,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6658,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:1101: g3036 */
t4=((C_word*)t0)[3];
f_6331(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k6656 in for-each-loop3035 in k6328 in k6319 in k6316 in k6310 in k6307 in k6295 in k6292 in k6286 in a6283 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in ... */
static void C_ccall f_6658(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6658,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_6648(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* map-loop3009 in k6319 in k6316 in k6310 in k6307 in k6295 in k6292 in k6286 in a6283 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in ... */
static void C_fcall f_6671(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_6671,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_car(t3);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k6704 in k6292 in k6286 in a6283 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in ... */
static void C_ccall f_6706(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_6706,c,av);}
a=C_alloc(7);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6713,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6717,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:1088: ##sys#current-module */
t4=*((C_word*)lf[85]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
f_6297(2,av2);}}}

/* k6711 in k6704 in k6292 in k6286 in a6283 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in ... */
static void C_ccall f_6713(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6713,c,av);}
/* chicken-syntax.scm:1087: chicken.base#symbol-append */
t2=*((C_word*)lf[82]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=lf[83];
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k6715 in k6704 in k6292 in k6286 in a6283 in k5643 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in ... */
static void C_ccall f_6717(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6717,c,av);}
/* chicken-syntax.scm:1088: ##sys#module-name */
t2=*((C_word*)lf[84]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k6719 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in ... */
static void C_ccall f_6721(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6721,c,av);}
/* chicken-syntax.scm:1042: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[87];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a6722 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in ... */
static void C_ccall f_6723(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_6723,c,av);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6727,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:1046: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[87];
av2[3]=t2;
av2[4]=lf[91];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k6725 in a6722 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in ... */
static void C_ccall f_6727(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_6727,c,av);}
a=C_alloc(8);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=C_u_i_cdr(((C_word*)t0)[2]);
t4=C_u_i_cdr(t3);
if(C_truep(C_i_pairp(t2))){
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6741,a[2]=t2,a[3]=t4,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
t6=C_a_i_cons(&a,2,t2,t4);
/* chicken-syntax.scm:1050: ##sys#check-syntax */
t7=*((C_word*)lf[45]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t7;
av2[1]=t5;
av2[2]=lf[87];
av2[3]=t6;
av2[4]=lf[89];
((C_proc)(void*)(*((C_word*)t7+1)))(5,av2);}}
else{
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6791,a[2]=t4,a[3]=((C_word*)t0)[3],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t6=C_a_i_cons(&a,2,t2,t4);
/* chicken-syntax.scm:1063: ##sys#check-syntax */
t7=*((C_word*)lf[45]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t7;
av2[1]=t5;
av2[2]=lf[87];
av2[3]=t6;
av2[4]=lf[90];
((C_proc)(void*)(*((C_word*)t7+1)))(5,av2);}}}

/* k6739 in k6725 in a6722 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in ... */
static void C_ccall f_6741(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_6741,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6744,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:1053: chicken.syntax#strip-syntax */
t3=*((C_word*)lf[39]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_slot(((C_word*)t0)[2],C_fix(0));
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k6742 in k6739 in k6725 in a6722 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in ... */
static void C_ccall f_6744(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_6744,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6747,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6769,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:1054: ##sys#current-module */
t4=*((C_word*)lf[85]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k6745 in k6742 in k6739 in k6725 in a6722 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in ... */
static void C_ccall f_6747(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(21,c,1)))){
C_save_and_reclaim((void *)f_6747,c,av);}
a=C_alloc(21);
t2=C_a_i_list(&a,2,lf[38],t1);
t3=C_slot(((C_word*)t0)[2],C_fix(1));
t4=C_a_i_cons(&a,2,t3,((C_word*)t0)[3]);
t5=C_a_i_cons(&a,2,lf[23],t4);
t6=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_a_i_list(&a,3,lf[88],t2,t5);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}

/* k6767 in k6742 in k6739 in k6725 in a6722 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in ... */
static void C_ccall f_6769(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_6769,c,av);}
a=C_alloc(7);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6776,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6780,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:1056: ##sys#current-module */
t4=*((C_word*)lf[85]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
f_6747(2,av2);}}}

/* k6774 in k6767 in k6742 in k6739 in k6725 in a6722 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in ... */
static void C_ccall f_6776(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6776,c,av);}
/* chicken-syntax.scm:1055: chicken.base#symbol-append */
t2=*((C_word*)lf[82]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=lf[83];
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k6778 in k6767 in k6742 in k6739 in k6725 in a6722 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in ... */
static void C_ccall f_6780(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6780,c,av);}
/* chicken-syntax.scm:1056: ##sys#module-name */
t2=*((C_word*)lf[84]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k6789 in k6725 in a6722 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in ... */
static void C_ccall f_6791(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_6791,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6794,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:1064: chicken.syntax#strip-syntax */
t3=*((C_word*)lf[39]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k6792 in k6789 in k6725 in a6722 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in ... */
static void C_ccall f_6794(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_6794,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6797,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6811,a[2]=t2,a[3]=t1,a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:1065: ##sys#current-module */
t4=*((C_word*)lf[85]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k6795 in k6792 in k6789 in k6725 in a6722 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in ... */
static void C_ccall f_6797(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,1)))){
C_save_and_reclaim((void *)f_6797,c,av);}
a=C_alloc(12);
t2=C_a_i_list(&a,2,lf[38],t1);
t3=C_a_i_cons(&a,2,t2,((C_word*)t0)[2]);
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_cons(&a,2,lf[88],t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k6809 in k6792 in k6789 in k6725 in a6722 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in ... */
static void C_ccall f_6811(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_6811,c,av);}
a=C_alloc(12);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6818,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6822,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:1067: ##sys#current-module */
t4=*((C_word*)lf[85]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t2=C_a_i_list(&a,2,lf[38],((C_word*)t0)[3]);
t3=C_a_i_cons(&a,2,t2,((C_word*)t0)[4]);
t4=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_cons(&a,2,lf[88],t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k6816 in k6809 in k6792 in k6789 in k6725 in a6722 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in ... */
static void C_ccall f_6818(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6818,c,av);}
/* chicken-syntax.scm:1066: chicken.base#symbol-append */
t2=*((C_word*)lf[82]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=lf[83];
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k6820 in k6809 in k6792 in k6789 in k6725 in a6722 in k5640 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in ... */
static void C_ccall f_6822(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6822,c,av);}
/* chicken-syntax.scm:1067: ##sys#module-name */
t2=*((C_word*)lf[84]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k6832 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in ... */
static void C_ccall f_6834(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6834,c,av);}
/* chicken-syntax.scm:974: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[102];
av2[3]=((C_word*)t0)[3];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a6835 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in ... */
static void C_ccall f_6836(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_6836,c,av);}
a=C_alloc(5);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6840,a[2]=t3,a[3]=t2,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:983: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[102];
av2[3]=t2;
av2[4]=lf[110];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k6838 in a6835 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in ... */
static void C_ccall f_6840(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(26,c,3)))){
C_save_and_reclaim((void *)f_6840,c,av);}
a=C_alloc(26);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6842,a[2]=((C_word*)t0)[2],a[3]=((C_word)li18),tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6877,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[2],a[5]=t2,tmp=(C_word)a,a+=6,tmp);
t4=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t5=t4;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=((C_word*)t6)[1];
t8=C_i_cdr(((C_word*)t0)[3]);
t9=C_i_check_list_2(t8,lf[67]);
t10=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7216,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
t11=C_SCHEME_UNDEFINED;
t12=(*a=C_VECTOR_TYPE|1,a[1]=t11,tmp=(C_word)a,a+=2,tmp);
t13=C_set_block_item(t12,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7218,a[2]=t6,a[3]=t12,a[4]=t7,a[5]=((C_word)li28),tmp=(C_word)a,a+=6,tmp));
t14=((C_word*)t12)[1];
f_7218(t14,t10,t8);}

/* genvars in k6838 in a6835 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in ... */
static void C_fcall f_6842(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,3)))){
C_save_and_reclaim_args((void *)trf_6842,3,t0,t1,t2);}
a=C_alloc(8);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6848,a[2]=t2,a[3]=t4,a[4]=((C_word*)t0)[2],a[5]=((C_word)li17),tmp=(C_word)a,a+=6,tmp));
t6=((C_word*)t4)[1];
f_6848(t6,t1,C_fix(0));}

/* loop in genvars in k6838 in a6835 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in ... */
static void C_fcall f_6848(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,2)))){
C_save_and_reclaim_args((void *)trf_6848,3,t0,t1,t2);}
a=C_alloc(9);
if(C_truep(C_fixnum_greater_or_equal_p(t2,((C_word*)t0)[2]))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6862,a[2]=t1,a[3]=((C_word*)t0)[3],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6874,a[2]=((C_word*)t0)[4],a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:988: chicken.base#gensym */
t5=*((C_word*)lf[56]+1);{
C_word av2[2];
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* k6860 in loop in genvars in k6838 in a6835 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in ... */
static void C_ccall f_6862(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_6862,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6866,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:988: loop */
t3=((C_word*)((C_word*)t0)[3])[1];
f_6848(t3,t2,C_fixnum_plus(((C_word*)t0)[4],C_fix(1)));}

/* k6864 in k6860 in loop in genvars in k6838 in a6835 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in ... */
static void C_ccall f_6866(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_6866,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k6872 in loop in genvars in k6838 in a6835 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in ... */
static void C_ccall f_6874(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6874,c,av);}
/* chicken-syntax.scm:988: r */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k6875 in k6838 in a6835 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in ... */
static void C_ccall f_6877(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_6877,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6880,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:994: genvars */
t3=((C_word*)t0)[5];
f_6842(t3,t2,t1);}

/* k6878 in k6875 in k6838 in a6835 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in ... */
static void C_ccall f_6880(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_6880,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6883,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* chicken-syntax.scm:995: r */
t3=((C_word*)t0)[5];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[108];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k6881 in k6878 in k6875 in k6838 in a6835 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in ... */
static void C_ccall f_6883(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_6883,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6886,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* chicken-syntax.scm:996: r */
t3=((C_word*)t0)[6];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[107];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k6884 in k6881 in k6878 in k6875 in k6838 in a6835 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in ... */
static void C_ccall f_6886(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_6886,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_6889,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
/* chicken-syntax.scm:997: r */
t3=((C_word*)t0)[7];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[92];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k6887 in k6884 in k6881 in k6878 in k6875 in k6838 in a6835 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in ... */
static void C_ccall f_6889(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_6889,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_6892,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=t1,tmp=(C_word)a,a+=10,tmp);
/* chicken-syntax.scm:998: r */
t3=((C_word*)t0)[8];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[98];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k6890 in k6887 in k6884 in k6881 in k6878 in k6875 in k6838 in a6835 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in ... */
static void C_ccall f_6892(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_6892,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_6895,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=t1,tmp=(C_word)a,a+=11,tmp);
/* chicken-syntax.scm:999: r */
t3=((C_word*)t0)[8];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[94];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k6893 in k6890 in k6887 in k6884 in k6881 in k6878 in k6875 in k6838 in a6835 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in ... */
static void C_ccall f_6895(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_6895,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_6898,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=t1,a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],tmp=(C_word)a,a+=12,tmp);
/* chicken-syntax.scm:1000: r */
t3=((C_word*)t0)[8];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[96];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k6896 in k6893 in k6890 in k6887 in k6884 in k6881 in k6878 in k6875 in k6838 in a6835 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in ... */
static void C_ccall f_6898(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_6898,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_6901,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=t1,a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],tmp=(C_word)a,a+=13,tmp);
/* chicken-syntax.scm:1001: r */
t3=((C_word*)t0)[9];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[100];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k6899 in k6896 in k6893 in k6890 in k6887 in k6884 in k6881 in k6878 in k6875 in k6838 in a6835 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in ... */
static void C_ccall f_6901(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,3)))){
C_save_and_reclaim((void *)f_6901,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_6908,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],a[13]=((C_word*)t0)[12],tmp=(C_word)a,a+=14,tmp);
/* chicken-syntax.scm:1003: scheme#append */
t3=*((C_word*)lf[16]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[7];
av2[3]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k6906 in k6899 in k6896 in k6893 in k6890 in k6887 in k6884 in k6881 in k6878 in k6875 in k6838 in a6835 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in ... */
static void C_ccall f_6908(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(35,c,3)))){
C_save_and_reclaim((void *)f_6908,c,av);}
a=C_alloc(35);
t2=C_a_i_list(&a,2,((C_word*)t0)[2],((C_word*)t0)[3]);
t3=C_a_i_list(&a,2,((C_word*)t0)[4],t2);
t4=C_a_i_list(&a,1,t3);
t5=C_u_i_cdr(((C_word*)t0)[5]);
t6=C_i_check_list_2(t5,lf[103]);
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6924,a[2]=t4,a[3]=((C_word*)t0)[6],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_6926,a[2]=((C_word*)t0)[7],a[3]=((C_word*)t0)[8],a[4]=((C_word*)t0)[9],a[5]=((C_word*)t0)[10],a[6]=((C_word*)t0)[11],a[7]=((C_word*)t0)[3],a[8]=((C_word*)t0)[12],a[9]=((C_word*)t0)[4],a[10]=((C_word*)t0)[13],a[11]=t9,a[12]=((C_word)li26),tmp=(C_word)a,a+=13,tmp));
t11=((C_word*)t9)[1];
f_6926(t11,t7,t5);}

/* k6922 in k6906 in k6899 in k6896 in k6893 in k6890 in k6887 in k6884 in k6881 in k6878 in k6875 in k6838 in a6835 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in ... */
static void C_ccall f_6924(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,1)))){
C_save_and_reclaim((void *)f_6924,c,av);}
a=C_alloc(18);
t2=C_a_i_list(&a,3,lf[37],((C_word*)t0)[2],t1);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_list(&a,3,lf[23],((C_word*)t0)[4],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* foldr2885 in k6906 in k6899 in k6896 in k6893 in k6890 in k6887 in k6884 in k6881 in k6878 in k6875 in k6838 in a6835 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in ... */
static void C_fcall f_6926(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(17,0,4)))){
C_save_and_reclaim_args((void *)trf_6926,3,t0,t1,t2);}
a=C_alloc(17);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_6934,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word)li25),tmp=(C_word)a,a+=12,tmp);
t4=C_slot(t2,C_fix(0));
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7179,a[2]=t3,a[3]=t1,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
t7=t5;
t8=C_slot(t2,C_fix(1));
t1=t7;
t2=t8;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=lf[106];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* g2890 in foldr2885 in k6906 in k6899 in k6896 in k6893 in k6890 in k6887 in k6884 in k6881 in k6878 in k6875 in k6838 in a6835 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in ... */
static void C_fcall f_6934(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,0,5)))){
C_save_and_reclaim_args((void *)trf_6934,4,t0,t1,t2,t3);}
a=C_alloc(14);
t4=C_i_car(t2);
t5=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_6944,a[2]=((C_word*)t0)[2],a[3]=t3,a[4]=((C_word*)t0)[3],a[5]=t2,a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[5],a[8]=((C_word*)t0)[6],a[9]=((C_word*)t0)[7],a[10]=((C_word*)t0)[8],a[11]=((C_word*)t0)[9],a[12]=((C_word*)t0)[10],a[13]=((C_word)li24),tmp=(C_word)a,a+=14,tmp);
/* chicken-syntax.scm:1008: ##sys#decompose-lambda-list */
t6=*((C_word*)lf[105]+1);{
C_word av2[4];
av2[0]=t6;
av2[1]=t1;
av2[2]=t4;
av2[3]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* a6943 in g2890 in foldr2885 in k6906 in k6899 in k6896 in k6893 in k6890 in k6887 in k6884 in k6881 in k6878 in k6875 in k6838 in a6835 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in ... */
static void C_ccall f_6944(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(17,c,4)))){
C_save_and_reclaim((void *)f_6944,c,av);}
a=C_alloc(17);
t5=(*a=C_CLOSURE_TYPE|16,a[1]=(C_word)f_6948,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=t1,a[5]=((C_word*)t0)[3],a[6]=t2,a[7]=((C_word*)t0)[4],a[8]=t4,a[9]=((C_word*)t0)[5],a[10]=((C_word*)t0)[6],a[11]=((C_word*)t0)[7],a[12]=((C_word*)t0)[8],a[13]=((C_word*)t0)[9],a[14]=((C_word*)t0)[10],a[15]=((C_word*)t0)[11],a[16]=((C_word*)t0)[12],tmp=(C_word)a,a+=17,tmp);
/* chicken-syntax.scm:1011: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[102];
av2[3]=C_i_car(((C_word*)t0)[5]);
av2[4]=lf[104];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k6946 in a6943 in g2890 in foldr2885 in k6906 in k6899 in k6896 in k6893 in k6890 in k6887 in k6884 in k6881 in k6878 in k6875 in k6838 in a6835 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in ... */
static void C_ccall f_6948(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(23,c,2)))){
C_save_and_reclaim((void *)f_6948,c,av);}
a=C_alloc(23);
t2=C_fixnum_difference(((C_word*)t0)[2],((C_word*)t0)[3]);
t3=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_6958,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[2],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],tmp=(C_word)a,a+=14,tmp);
if(C_truep(((C_word*)t0)[8])){
t4=C_eqp(t2,C_fix(0));
t5=t3;
f_6958(t5,(C_truep(t4)?C_SCHEME_TRUE:C_a_i_list(&a,3,((C_word*)t0)[14],((C_word*)t0)[15],t2)));}
else{
t4=t3;
f_6958(t4,C_a_i_list(&a,3,((C_word*)t0)[16],((C_word*)t0)[15],t2));}}

/* k6956 in k6946 in a6943 in g2890 in foldr2885 in k6906 in k6899 in k6896 in k6893 in k6890 in k6887 in k6884 in k6881 in k6878 in k6875 in k6838 in a6835 in k5637 in k5634 in k5631 in k5628 in k5625 in ... */
static void C_fcall f_6958(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(21,0,4)))){
C_save_and_reclaim_args((void *)trf_6958,2,t0,t1);}
a=C_alloc(21);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6962,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6964,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word)li20),tmp=(C_word)a,a+=6,tmp);
t4=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_6974,a[2]=((C_word*)t0)[7],a[3]=((C_word*)t0)[8],a[4]=((C_word*)t0)[9],a[5]=((C_word*)t0)[10],a[6]=((C_word*)t0)[11],a[7]=((C_word*)t0)[12],a[8]=((C_word*)t0)[13],a[9]=((C_word)li23),tmp=(C_word)a,a+=10,tmp);
/* chicken-syntax.scm:1018: ##sys#call-with-values */{
C_word av2[4];
av2[0]=0;
av2[1]=t2;
av2[2]=t3;
av2[3]=t4;
C_call_with_values(4,av2);}}

/* k6960 in k6956 in k6946 in a6943 in g2890 in foldr2885 in k6906 in k6899 in k6896 in k6893 in k6890 in k6887 in k6884 in k6881 in k6878 in k6875 in k6838 in a6835 in k5637 in k5634 in k5631 in k5628 in ... */
static void C_ccall f_6962(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,1)))){
C_save_and_reclaim((void *)f_6962,c,av);}
a=C_alloc(12);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,4,lf[36],((C_word*)t0)[3],t1,((C_word*)t0)[4]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a6963 in k6956 in k6946 in a6943 in g2890 in foldr2885 in k6906 in k6899 in k6896 in k6893 in k6890 in k6887 in k6884 in k6881 in k6878 in k6875 in k6838 in a6835 in k5637 in k5634 in k5631 in k5628 in ... */
static void C_ccall f_6964(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_6964,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6972,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:1019: take */
f_4334(t2,((C_word*)t0)[3],((C_word*)t0)[4]);}

/* k6970 in a6963 in k6956 in k6946 in a6943 in g2890 in foldr2885 in k6906 in k6899 in k6896 in k6893 in k6890 in k6887 in k6884 in k6881 in k6878 in k6875 in k6838 in a6835 in k5637 in k5634 in k5631 in ... */
static void C_ccall f_6972(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,5)))){
C_save_and_reclaim((void *)f_6972,c,av);}
a=C_alloc(6);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4392,a[2]=t3,a[3]=((C_word)li19),tmp=(C_word)a,a+=4,tmp));
t5=((C_word*)t3)[1];
f_4392(t5,((C_word*)t0)[2],((C_word*)t0)[3],C_SCHEME_END_OF_LIST,t1);}

/* a6973 in k6956 in k6946 in a6943 in g2890 in foldr2885 in k6906 in k6899 in k6896 in k6893 in k6890 in k6887 in k6884 in k6881 in k6878 in k6875 in k6838 in a6835 in k5637 in k5634 in k5631 in k5628 in ... */
static void C_ccall f_6974(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(16,c,4)))){
C_save_and_reclaim((void *)f_6974,c,av);}
a=C_alloc(16);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6978,a[2]=t2,a[3]=t1,a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_7050,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=t6,a[7]=((C_word*)t0)[7],a[8]=((C_word)li22),tmp=(C_word)a,a+=9,tmp));
t8=((C_word*)t6)[1];
f_7050(t8,t4,t3,((C_word*)t0)[8]);}

/* k6976 in a6973 in k6956 in k6946 in a6943 in g2890 in foldr2885 in k6906 in k6899 in k6896 in k6893 in k6890 in k6887 in k6884 in k6881 in k6878 in k6875 in k6838 in a6835 in k5637 in k5634 in k5631 in ... */
static void C_ccall f_6978(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(17,c,4)))){
C_save_and_reclaim((void *)f_6978,c,av);}
a=C_alloc(17);
if(C_truep(C_i_nullp(((C_word*)t0)[2]))){
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=C_i_check_list_2(((C_word*)t0)[2],lf[67]);
t7=C_i_check_list_2(((C_word*)t0)[4],lf[67]);
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7000,a[2]=((C_word*)t0)[3],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7002,a[2]=t4,a[3]=t10,a[4]=t5,a[5]=((C_word)li21),tmp=(C_word)a,a+=6,tmp));
t12=((C_word*)t10)[1];
f_7002(t12,t8,((C_word*)t0)[2],((C_word*)t0)[4]);}}

/* k6998 in k6976 in a6973 in k6956 in k6946 in a6943 in g2890 in foldr2885 in k6906 in k6899 in k6896 in k6893 in k6890 in k6887 in k6884 in k6881 in k6878 in k6875 in k6838 in a6835 in k5637 in k5634 in ... */
static void C_ccall f_7000(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_7000,c,av);}
a=C_alloc(9);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,3,lf[37],t1,((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* map-loop2933 in k6976 in a6973 in k6956 in k6946 in a6943 in g2890 in foldr2885 in k6906 in k6899 in k6896 in k6893 in k6890 in k6887 in k6884 in k6881 in k6878 in k6875 in k6838 in a6835 in k5637 in k5634 in ... */
static void C_fcall f_7002(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(9,0,3)))){
C_save_and_reclaim_args((void *)trf_7002,4,t0,t1,t2,t3);}
a=C_alloc(9);
t4=C_i_pairp(t2);
t5=(C_truep(t4)?C_i_pairp(t3):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_slot(t2,C_fix(0));
t7=C_slot(t3,C_fix(0));
t8=C_a_i_list2(&a,2,t6,t7);
t9=C_a_i_cons(&a,2,t8,C_SCHEME_END_OF_LIST);
t10=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t9);
t11=C_mutate(((C_word *)((C_word*)t0)[2])+1,t9);
t13=t1;
t14=C_slot(t2,C_fix(1));
t15=C_slot(t3,C_fix(1));
t1=t13;
t2=t14;
t3=t15;
goto loop;}
else{
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* build in a6973 in k6956 in k6946 in a6943 in g2890 in foldr2885 in k6906 in k6899 in k6896 in k6893 in k6890 in k6887 in k6884 in k6881 in k6878 in k6875 in k6838 in a6835 in k5637 in k5634 in k5631 in ... */
static void C_fcall f_7050(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,0,2)))){
C_save_and_reclaim_args((void *)trf_7050,4,t0,t1,t2,t3);}
a=C_alloc(15);
if(C_truep(C_i_nullp(t2))){
if(C_truep(((C_word*)t0)[2])){
t4=C_a_i_list(&a,2,((C_word*)t0)[2],t3);
t5=C_a_i_list(&a,1,t4);
t6=C_i_cdr(((C_word*)t0)[3]);
t7=C_a_i_cons(&a,2,t5,t6);
t8=t1;{
C_word av2[2];
av2[0]=t8;
av2[1]=C_a_i_cons(&a,2,lf[37],t7);
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}
else{
t4=C_i_cddr(((C_word*)t0)[3]);
if(C_truep(C_i_nullp(t4))){
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=C_u_i_car(C_u_i_cdr(((C_word*)t0)[3]));
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t5=C_u_i_cdr(((C_word*)t0)[3]);
t6=C_a_i_cons(&a,2,C_SCHEME_END_OF_LIST,t5);
t7=t1;{
C_word av2[2];
av2[0]=t7;
av2[1]=C_a_i_cons(&a,2,lf[37],t6);
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}}}
else{
t4=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_7103,a[2]=t2,a[3]=((C_word*)t0)[4],a[4]=t3,a[5]=((C_word*)t0)[5],a[6]=t1,a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7151,a[2]=((C_word*)t0)[7],a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:1026: chicken.base#gensym */
t6=*((C_word*)lf[56]+1);{
C_word av2[2];
av2[0]=t6;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* k7101 in build in a6973 in k6956 in k6946 in a6943 in g2890 in foldr2885 in k6906 in k6899 in k6896 in k6893 in k6890 in k6887 in k6884 in k6881 in k6878 in k6875 in k6838 in a6835 in k5637 in k5634 in ... */
static void C_ccall f_7103(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(34,c,3)))){
C_save_and_reclaim((void *)f_7103,c,av);}
a=C_alloc(34);
t2=C_i_car(((C_word*)t0)[2]);
t3=C_a_i_list(&a,2,((C_word*)t0)[3],((C_word*)t0)[4]);
t4=C_a_i_list(&a,2,t2,t3);
t5=C_a_i_list(&a,2,((C_word*)t0)[5],((C_word*)t0)[4]);
t6=C_a_i_list(&a,2,t1,t5);
t7=C_a_i_list(&a,2,t4,t6);
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7114,a[2]=((C_word*)t0)[6],a[3]=t7,tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_i_pairp(C_u_i_cdr(((C_word*)t0)[2])))){
/* chicken-syntax.scm:1030: build */
t9=((C_word*)((C_word*)t0)[7])[1];
f_7050(t9,t8,C_u_i_cdr(((C_word*)t0)[2]),t1);}
else{
/* chicken-syntax.scm:1031: build */
t9=((C_word*)((C_word*)t0)[7])[1];
f_7050(t9,t8,C_SCHEME_END_OF_LIST,t1);}}

/* k7112 in k7101 in build in a6973 in k6956 in k6946 in a6943 in g2890 in foldr2885 in k6906 in k6899 in k6896 in k6893 in k6890 in k6887 in k6884 in k6881 in k6878 in k6875 in k6838 in a6835 in k5637 in ... */
static void C_ccall f_7114(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_7114,c,av);}
a=C_alloc(9);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,3,lf[37],((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k7149 in build in a6973 in k6956 in k6946 in a6943 in g2890 in foldr2885 in k6906 in k6899 in k6896 in k6893 in k6890 in k6887 in k6884 in k6881 in k6878 in k6875 in k6838 in a6835 in k5637 in k5634 in ... */
static void C_ccall f_7151(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7151,c,av);}
/* chicken-syntax.scm:1026: r */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k7177 in foldr2885 in k6906 in k6899 in k6896 in k6893 in k6890 in k6887 in k6884 in k6881 in k6878 in k6875 in k6838 in a6835 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in ... */
static void C_ccall f_7179(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7179,c,av);}
/* chicken-syntax.scm:1006: g2890 */
t2=((C_word*)t0)[2];
f_6934(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* a7205 in map-loop2842 in k6838 in a6835 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in ... */
static void C_ccall f_7206(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7206,c,av);}
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* k7214 in k6838 in a6835 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in ... */
static void C_ccall f_7216(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7216,c,av);}{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=*((C_word*)lf[109]+1);
av2[3]=t1;
C_apply(4,av2);}}

/* map-loop2842 in k6838 in a6835 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in ... */
static void C_fcall f_7218(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,5)))){
C_save_and_reclaim_args((void *)trf_7218,3,t0,t1,t2);}
a=C_alloc(9);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7243,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
t4=C_slot(t2,C_fix(0));
t5=C_i_car(t4);
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7206,a[2]=((C_word)li27),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:990: ##sys#decompose-lambda-list */
t7=*((C_word*)lf[105]+1);{
C_word av2[4];
av2[0]=t7;
av2[1]=t3;
av2[2]=t5;
av2[3]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k7241 in map-loop2842 in k6838 in a6835 in k5637 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in ... */
static void C_ccall f_7243(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7243,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_7218(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k7276 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in ... */
static void C_ccall f_7278(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_7278,c,av);}
/* chicken-syntax.scm:939: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[113];
av2[3]=((C_word*)t0)[3];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a7279 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in ... */
static void C_ccall f_7280(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_7280,c,av);}
a=C_alloc(5);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7284,a[2]=t2,a[3]=t1,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:946: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[113];
av2[3]=t2;
av2[4]=lf[115];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k7282 in a7279 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in ... */
static void C_ccall f_7284(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_7284,c,av);}
a=C_alloc(7);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=C_i_caddr(((C_word*)t0)[2]);
t4=C_u_i_cdr(((C_word*)t0)[2]);
t5=C_u_i_cdr(t4);
t6=C_u_i_cdr(t5);
t7=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7296,a[2]=t2,a[3]=((C_word*)t0)[3],a[4]=t6,a[5]=((C_word*)t0)[4],a[6]=t3,tmp=(C_word)a,a+=7,tmp);
/* chicken-syntax.scm:950: r */
t8=((C_word*)t0)[4];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t8;
av2[1]=t7;
av2[2]=lf[111];
((C_proc)(void*)(*((C_word*)t8+1)))(3,av2);}}

/* k7294 in k7282 in a7279 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in ... */
static void C_ccall f_7296(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_7296,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_7299,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* chicken-syntax.scm:951: r */
t3=((C_word*)t0)[5];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[94];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7297 in k7294 in k7282 in a7279 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in ... */
static void C_ccall f_7299(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_7299,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_7302,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
/* chicken-syntax.scm:952: r */
t3=((C_word*)t0)[6];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[96];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7300 in k7297 in k7294 in k7282 in a7279 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in ... */
static void C_ccall f_7302(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_7302,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_7305,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t1,a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
/* chicken-syntax.scm:953: r */
t3=((C_word*)t0)[7];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[44];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7303 in k7300 in k7297 in k7294 in k7282 in a7279 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in ... */
static void C_ccall f_7305(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,4)))){
C_save_and_reclaim((void *)f_7305,c,av);}
a=C_alloc(24);
t2=C_a_i_list(&a,2,t1,((C_word*)t0)[2]);
t3=C_a_i_list(&a,1,t2);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7316,a[2]=((C_word*)t0)[3],a[3]=t3,tmp=(C_word)a,a+=4,tmp);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_7318,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[7],a[6]=t6,a[7]=((C_word*)t0)[8],a[8]=((C_word)li30),tmp=(C_word)a,a+=9,tmp));
t8=((C_word*)t6)[1];
f_7318(t8,t4,t1,((C_word*)t0)[9]);}

/* k7314 in k7303 in k7300 in k7297 in k7294 in k7282 in a7279 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in ... */
static void C_ccall f_7316(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_7316,c,av);}
a=C_alloc(9);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,3,lf[37],((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* loop in k7303 in k7300 in k7297 in k7294 in k7282 in a7279 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in ... */
static void C_fcall f_7318(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,0,2)))){
C_save_and_reclaim_args((void *)trf_7318,4,t0,t1,t2,t3);}
a=C_alloc(15);
if(C_truep(C_i_nullp(t3))){
t4=C_a_i_cons(&a,2,C_SCHEME_END_OF_LIST,((C_word*)t0)[2]);
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=C_a_i_cons(&a,2,lf[37],t4);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t4=C_i_car(t3);
if(C_truep(C_i_pairp(t4))){
t5=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_7344,a[2]=t4,a[3]=((C_word*)t0)[3],a[4]=t2,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=t1,a[8]=((C_word*)t0)[6],a[9]=t3,tmp=(C_word)a,a+=10,tmp);
/* chicken-syntax.scm:961: r */
t6=((C_word*)t0)[7];{
C_word av2[3];
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[114];
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}
else{
t5=C_a_i_list(&a,2,t4,t2);
t6=C_a_i_list(&a,1,t5);
t7=C_a_i_cons(&a,2,t6,((C_word*)t0)[2]);
t8=t1;{
C_word av2[2];
av2[0]=t8;
av2[1]=C_a_i_cons(&a,2,lf[37],t7);
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}}}

/* k7342 in loop in k7303 in k7300 in k7297 in k7294 in k7282 in a7279 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in ... */
static void C_ccall f_7344(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(76,c,3)))){
C_save_and_reclaim((void *)f_7344,c,av);}
a=C_alloc(76);
t2=C_u_i_car(((C_word*)t0)[2]);
t3=C_a_i_list(&a,2,((C_word*)t0)[3],((C_word*)t0)[4]);
t4=C_i_cadr(((C_word*)t0)[2]);
t5=C_a_i_list(&a,2,((C_word*)t0)[5],((C_word*)t0)[4]);
t6=C_a_i_list(&a,4,lf[36],t3,t4,t5);
t7=C_a_i_list(&a,2,t2,t6);
t8=C_a_i_list(&a,2,((C_word*)t0)[3],((C_word*)t0)[4]);
t9=C_a_i_list(&a,2,lf[38],C_SCHEME_END_OF_LIST);
t10=C_a_i_list(&a,2,((C_word*)t0)[6],((C_word*)t0)[4]);
t11=C_a_i_list(&a,4,lf[36],t8,t9,t10);
t12=C_a_i_list(&a,2,t1,t11);
t13=C_a_i_list(&a,2,t7,t12);
t14=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7355,a[2]=((C_word*)t0)[7],a[3]=t13,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:968: loop */
t15=((C_word*)((C_word*)t0)[8])[1];
f_7318(t15,t14,t1,C_u_i_cdr(((C_word*)t0)[9]));}

/* k7353 in k7342 in loop in k7303 in k7300 in k7297 in k7294 in k7282 in a7279 in k5634 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in ... */
static void C_ccall f_7355(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_7355,c,av);}
a=C_alloc(9);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,3,lf[37],((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k7436 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in ... */
static void C_ccall f_7438(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_7438,c,av);}
/* chicken-syntax.scm:911: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[116];
av2[3]=((C_word*)t0)[3];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a7439 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in ... */
static void C_ccall f_7440(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_7440,c,av);}
a=C_alloc(5);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7444,a[2]=t2,a[3]=t1,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:918: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[116];
av2[3]=t2;
av2[4]=lf[117];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k7442 in a7439 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in ... */
static void C_ccall f_7444(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_7444,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7447,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:919: r */
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[44];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7445 in k7442 in a7439 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in ... */
static void C_ccall f_7447(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,2)))){
C_save_and_reclaim((void *)f_7447,c,av);}
a=C_alloc(16);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=C_a_i_list(&a,2,t1,t2);
t4=C_a_i_list(&a,1,t3);
t5=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7487,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=t4,a[6]=((C_word*)t0)[4],tmp=(C_word)a,a+=7,tmp);
/* chicken-syntax.scm:921: r */
t6=((C_word*)t0)[4];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[111];
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k7475 in k7485 in k7445 in k7442 in a7439 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in ... */
static void C_ccall f_7477(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(27,c,1)))){
C_save_and_reclaim((void *)f_7477,c,av);}
a=C_alloc(27);
t2=C_a_i_list(&a,2,t1,((C_word*)t0)[2]);
t3=C_a_i_list(&a,4,lf[36],((C_word*)t0)[3],((C_word*)t0)[4],t2);
t4=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_list(&a,3,lf[37],((C_word*)t0)[6],t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k7485 in k7445 in k7442 in a7439 in k5631 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in ... */
static void C_ccall f_7487(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_7487,c,av);}
a=C_alloc(13);
t2=C_a_i_list(&a,2,t1,((C_word*)t0)[2]);
t3=C_i_cddr(((C_word*)t0)[3]);
t4=C_i_nullp(t3);
t5=(C_truep(t4)?C_SCHEME_FALSE:C_i_car(t3));
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7477,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=t5,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* chicken-syntax.scm:923: r */
t7=((C_word*)t0)[6];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t7;
av2[1]=t6;
av2[2]=lf[94];
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}

/* k7513 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in ... */
static void C_ccall f_7515(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_7515,c,av);}
/* chicken-syntax.scm:822: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[118];
av2[3]=((C_word*)t0)[3];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in ... */
static void C_ccall f_7517(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_7517,c,av);}
a=C_alloc(5);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7521,a[2]=t2,a[3]=t3,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:829: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[118];
av2[3]=t2;
av2[4]=lf[128];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in ... */
static void C_ccall f_7521(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,6)))){
C_save_and_reclaim((void *)f_7521,c,av);}
a=C_alloc(12);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=C_i_caddr(((C_word*)t0)[2]);
t4=C_u_i_cdr(((C_word*)t0)[2]);
t5=C_u_i_cdr(t4);
t6=C_u_i_cdr(t5);
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7611,a[2]=((C_word*)t0)[3],a[3]=((C_word)li34),tmp=(C_word)a,a+=4,tmp);
t8=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_7704,a[2]=t3,a[3]=((C_word*)t0)[3],a[4]=t2,a[5]=t6,a[6]=((C_word*)t0)[4],a[7]=t7,tmp=(C_word)a,a+=8,tmp);
/* chicken-syntax.scm:868: ##sys#check-syntax */
t9=*((C_word*)lf[45]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t9;
av2[1]=t8;
av2[2]=lf[118];
av2[3]=t3;
av2[4]=lf[127];
((C_proc)(void*)(*((C_word*)t9+1)))(5,av2);}}

/* k7538 in k7772 in k7757 in k7754 in k7751 in k7745 in k7714 in k7705 in k7702 in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in ... */
static void C_ccall f_7540(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_7540,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7544,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:839: scheme#reverse */
t3=*((C_word*)lf[57]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7542 in k7538 in k7772 in k7757 in k7754 in k7751 in k7745 in k7714 in k7705 in k7702 in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in ... */
static void C_ccall f_7544(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_7544,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7548,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:840: scheme#reverse */
t3=*((C_word*)lf[57]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7546 in k7542 in k7538 in k7772 in k7757 in k7754 in k7751 in k7745 in k7714 in k7705 in k7702 in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in ... */
static void C_ccall f_7548(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,6)))){
C_save_and_reclaim((void *)f_7548,c,av);}
a=C_alloc(6);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7550,a[2]=t3,a[3]=((C_word)li38),tmp=(C_word)a,a+=4,tmp));
t5=((C_word*)t3)[1];
f_7550(t5,((C_word*)t0)[2],((C_word*)t0)[3],((C_word*)t0)[4],t1,((C_word*)t0)[5]);}

/* recur in k7546 in k7542 in k7538 in k7772 in k7757 in k7754 in k7751 in k7745 in k7714 in k7705 in k7702 in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in ... */
static void C_fcall f_7550(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5){
C_word tmp;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,2)))){
C_save_and_reclaim_args((void *)trf_7550,6,t0,t1,t2,t3,t4,t5);}
a=C_alloc(9);
if(C_truep(C_i_nullp(t2))){
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
t6=C_i_cdr(t2);
t7=C_i_car(t3);
t8=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_7589,a[2]=t5,a[3]=t7,a[4]=t1,a[5]=((C_word*)t0)[2],a[6]=t6,a[7]=t3,a[8]=t4,tmp=(C_word)a,a+=9,tmp);
/* chicken-syntax.scm:845: scheme#reverse */
t9=*((C_word*)lf[57]+1);{
C_word av2[3];
av2[0]=t9;
av2[1]=t8;
av2[2]=t6;
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}}

/* k7569 in k7595 in k7587 in recur in k7546 in k7542 in k7538 in k7772 in k7757 in k7754 in k7751 in k7745 in k7714 in k7705 in k7702 in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in ... */
static void C_ccall f_7571(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_7571,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k7587 in recur in k7546 in k7542 in k7538 in k7772 in k7757 in k7754 in k7751 in k7745 in k7714 in k7705 in k7702 in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in ... */
static void C_ccall f_7589(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,2)))){
C_save_and_reclaim((void *)f_7589,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_7597,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7601,a[2]=((C_word*)t0)[8],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:846: scheme#reverse */
t4=*((C_word*)lf[57]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k7595 in k7587 in recur in k7546 in k7542 in k7538 in k7772 in k7757 in k7754 in k7751 in k7745 in k7714 in k7705 in k7702 in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in ... */
static void C_ccall f_7597(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(22,c,5)))){
C_save_and_reclaim((void *)f_7597,c,av);}
a=C_alloc(22);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_list(&a,3,lf[23],((C_word*)t0)[3],t2);
t4=C_a_i_list(&a,2,((C_word*)t0)[4],t3);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7571,a[2]=((C_word*)t0)[5],a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:847: recur */
t6=((C_word*)((C_word*)t0)[6])[1];
f_7550(t6,t5,((C_word*)t0)[7],C_u_i_cdr(((C_word*)t0)[8]),C_u_i_cdr(((C_word*)t0)[9]),C_u_i_car(((C_word*)t0)[8]));}

/* k7599 in k7587 in recur in k7546 in k7542 in k7538 in k7772 in k7757 in k7754 in k7751 in k7745 in k7714 in k7705 in k7702 in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in ... */
static void C_ccall f_7601(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_7601,c,av);}
a=C_alloc(3);
t2=C_i_car(((C_word*)t0)[2]);
t3=C_a_i_list(&a,1,t2);
/* chicken-syntax.scm:844: ##sys#append */
t4=*((C_word*)lf[55]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* make-if-tree in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in ... */
static void C_fcall f_7611(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5){
C_word tmp;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,5)))){
C_save_and_reclaim_args((void *)trf_7611,6,t0,t1,t2,t3,t4,t5);}
a=C_alloc(9);
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7617,a[2]=t4,a[3]=t5,a[4]=t7,a[5]=((C_word*)t0)[2],a[6]=((C_word)li33),tmp=(C_word)a,a+=7,tmp));
t9=((C_word*)t7)[1];
f_7617(t9,t1,t2,t3,C_SCHEME_END_OF_LIST);}

/* recur in make-if-tree in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in ... */
static void C_fcall f_7617(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,2)))){
C_save_and_reclaim_args((void *)trf_7617,5,t0,t1,t2,t3,t4);}
a=C_alloc(10);
if(C_truep(C_i_nullp(t2))){
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7631,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:858: scheme#reverse */
t6=*((C_word*)lf[57]+1);{
C_word av2[3];
av2[0]=t6;
av2[1]=t5;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}
else{
t5=C_i_car(t2);
t6=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_7701,a[2]=((C_word*)t0)[3],a[3]=t3,a[4]=t5,a[5]=t1,a[6]=t2,a[7]=t4,a[8]=((C_word*)t0)[4],a[9]=((C_word*)t0)[5],tmp=(C_word)a,a+=10,tmp);
/* chicken-syntax.scm:860: r */
t7=((C_word*)t0)[5];{
C_word av2[3];
av2[0]=t7;
av2[1]=t6;
av2[2]=lf[111];
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}}

/* k7629 in recur in make-if-tree in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in ... */
static void C_ccall f_7631(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_7631,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k7655 in k7679 in k7687 in k7695 in k7699 in recur in make-if-tree in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in ... */
static void C_ccall f_7657(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(21,c,1)))){
C_save_and_reclaim((void *)f_7657,c,av);}
a=C_alloc(21);
t2=C_a_i_list(&a,3,lf[37],((C_word*)t0)[2],t1);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_list(&a,4,lf[36],((C_word*)t0)[4],((C_word*)t0)[5],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k7679 in k7687 in k7695 in k7699 in recur in make-if-tree in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in ... */
static void C_ccall f_7681(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(27,c,4)))){
C_save_and_reclaim((void *)f_7681,c,av);}
a=C_alloc(27);
t2=C_a_i_list(&a,2,t1,((C_word*)t0)[2]);
t3=C_a_i_list(&a,2,((C_word*)t0)[2],t2);
t4=C_a_i_list(&a,2,((C_word*)t0)[3],t3);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7657,a[2]=t4,a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],tmp=(C_word)a,a+=6,tmp);
t6=C_u_i_cdr(((C_word*)t0)[7]);
t7=C_u_i_cdr(((C_word*)t0)[8]);
t8=C_a_i_cons(&a,2,((C_word*)t0)[9],((C_word*)t0)[10]);
/* chicken-syntax.scm:864: recur */
t9=((C_word*)((C_word*)t0)[11])[1];
f_7617(t9,t5,t6,t7,t8);}

/* k7687 in k7695 in k7699 in recur in make-if-tree in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in ... */
static void C_ccall f_7689(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,2)))){
C_save_and_reclaim((void *)f_7689,c,av);}
a=C_alloc(24);
t2=C_a_i_list(&a,2,t1,((C_word*)t0)[2]);
t3=C_a_i_list(&a,2,((C_word*)t0)[3],t2);
t4=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_7681,a[2]=((C_word*)t0)[2],a[3]=t3,a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[3],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],tmp=(C_word)a,a+=12,tmp);
/* chicken-syntax.scm:863: r */
t5=((C_word*)t0)[11];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=lf[96];
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k7695 in k7699 in recur in make-if-tree in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in ... */
static void C_ccall f_7697(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,2)))){
C_save_and_reclaim((void *)f_7697,c,av);}
a=C_alloc(15);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_7689,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=t2,a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],tmp=(C_word)a,a+=12,tmp);
/* chicken-syntax.scm:862: r */
t4=((C_word*)t0)[11];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[94];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k7699 in recur in make-if-tree in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in ... */
static void C_ccall f_7701(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,2)))){
C_save_and_reclaim((void *)f_7701,c,av);}
a=C_alloc(18);
t2=C_a_i_list(&a,2,t1,((C_word*)t0)[2]);
t3=C_i_car(((C_word*)t0)[3]);
t4=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_7697,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t2,a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[3],a[9]=((C_word*)t0)[7],a[10]=((C_word*)t0)[8],a[11]=((C_word*)t0)[9],tmp=(C_word)a,a+=12,tmp);
/* chicken-syntax.scm:861: scheme#reverse */
t5=*((C_word*)lf[57]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k7702 in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in ... */
static void C_ccall f_7704(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_7704,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_7707,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
/* chicken-syntax.scm:869: ##sys#check-syntax */
t3=*((C_word*)lf[45]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[118];
av2[3]=((C_word*)t0)[5];
av2[4]=lf[126];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k7705 in k7702 in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in ... */
static void C_ccall f_7707(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(21,c,3)))){
C_save_and_reclaim((void *)f_7707,c,av);}
a=C_alloc(21);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=C_i_check_list_2(((C_word*)t0)[2],lf[67]);
t7=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_7716,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[2],tmp=(C_word)a,a+=8,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7915,a[2]=t4,a[3]=t9,a[4]=t5,a[5]=((C_word)li42),tmp=(C_word)a,a+=6,tmp));
t11=((C_word*)t9)[1];
f_7915(t11,t7,((C_word*)t0)[2]);}

/* k7714 in k7705 in k7702 in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in ... */
static void C_ccall f_7716(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(32,c,4)))){
C_save_and_reclaim((void *)f_7716,c,av);}
a=C_alloc(32);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7717,a[2]=((C_word)li35),tmp=(C_word)a,a+=3,tmp);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7733,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=((C_word)li36),tmp=(C_word)a,a+=5,tmp);
t8=C_i_check_list_2(t1,lf[67]);
t9=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_7747,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=((C_word*)t0)[3],a[5]=t1,a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[5],a[8]=((C_word*)t0)[6],a[9]=((C_word*)t0)[7],tmp=(C_word)a,a+=10,tmp);
t10=C_SCHEME_UNDEFINED;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_set_block_item(t11,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7881,a[2]=t5,a[3]=t11,a[4]=t7,a[5]=t6,a[6]=((C_word)li41),tmp=(C_word)a,a+=7,tmp));
t13=((C_word*)t11)[1];
f_7881(t13,t9,t1);}

/* prefix-sym in k7714 in k7705 in k7702 in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in ... */
static void C_fcall f_7717(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,2)))){
C_save_and_reclaim_args((void *)trf_7717,3,t1,t2,t3);}
a=C_alloc(7);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7725,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7729,a[2]=t4,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:872: scheme#symbol->string */
t6=*((C_word*)lf[120]+1);{
C_word av2[3];
av2[0]=t6;
av2[1]=t5;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k7723 in prefix-sym in k7714 in k7705 in k7702 in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in ... */
static void C_ccall f_7725(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7725,c,av);}
/* chicken-syntax.scm:872: scheme#string->symbol */
t2=*((C_word*)lf[119]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k7727 in prefix-sym in k7714 in k7705 in k7702 in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in ... */
static void C_ccall f_7729(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7729,c,av);}
/* chicken-syntax.scm:872: scheme#string-append */
t2=*((C_word*)lf[40]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* g2695 in k7714 in k7705 in k7702 in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in ... */
static void C_fcall f_7733(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,3)))){
C_save_and_reclaim_args((void *)trf_7733,3,t0,t1,t2);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7741,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:877: prefix-sym */
f_7717(t3,lf[121],t2);}

/* k7739 in g2695 in k7714 in k7705 in k7702 in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in ... */
static void C_ccall f_7741(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7741,c,av);}
/* chicken-syntax.scm:877: r */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k7745 in k7714 in k7705 in k7702 in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in ... */
static void C_ccall f_7747(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(23,c,3)))){
C_save_and_reclaim((void *)f_7747,c,av);}
a=C_alloc(23);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_7753,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=t1,tmp=(C_word)a,a+=10,tmp);
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7847,a[2]=t4,a[3]=t8,a[4]=t5,a[5]=((C_word)li40),tmp=(C_word)a,a+=6,tmp));
t10=((C_word*)t8)[1];
f_7847(t10,t6,((C_word*)t0)[9]);}

/* k7751 in k7745 in k7714 in k7705 in k7702 in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in ... */
static void C_ccall f_7753(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_7753,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_7756,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=t1,tmp=(C_word)a,a+=11,tmp);
/* chicken-syntax.scm:881: r */
t3=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[125];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7754 in k7751 in k7745 in k7714 in k7705 in k7702 in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in ... */
static void C_ccall f_7756(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_7756,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_7759,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t1,a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],tmp=(C_word)a,a+=12,tmp);
/* chicken-syntax.scm:884: r */
t3=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[124];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7757 in k7754 in k7751 in k7745 in k7714 in k7705 in k7702 in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in ... */
static void C_ccall f_7759(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(31,c,3)))){
C_save_and_reclaim((void *)f_7759,c,av);}
a=C_alloc(31);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7763,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word)li37),tmp=(C_word)a,a+=5,tmp);
t7=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_7774,a[2]=t1,a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[2],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],tmp=(C_word)a,a+=12,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7813,a[2]=t4,a[3]=t9,a[4]=t6,a[5]=t5,a[6]=((C_word)li39),tmp=(C_word)a,a+=7,tmp));
t11=((C_word*)t9)[1];
f_7813(t11,t7,((C_word*)t0)[5]);}

/* g2752 in k7757 in k7754 in k7751 in k7745 in k7714 in k7705 in k7702 in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in ... */
static void C_fcall f_7763(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,3)))){
C_save_and_reclaim_args((void *)trf_7763,3,t0,t1,t2);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7771,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:886: prefix-sym */
f_7717(t3,lf[122],t2);}

/* k7769 in g2752 in k7757 in k7754 in k7751 in k7745 in k7714 in k7705 in k7702 in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in ... */
static void C_ccall f_7771(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7771,c,av);}
/* chicken-syntax.scm:886: r */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k7772 in k7757 in k7754 in k7751 in k7745 in k7714 in k7705 in k7702 in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in ... */
static void C_ccall f_7774(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,2)))){
C_save_and_reclaim((void *)f_7774,c,av);}
a=C_alloc(18);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_7777,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=t1,tmp=(C_word)a,a+=12,tmp);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7540,a[2]=t2,a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[11],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:838: scheme#reverse */
t4=*((C_word*)lf[57]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[10];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k7775 in k7772 in k7757 in k7754 in k7751 in k7745 in k7714 in k7705 in k7702 in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in ... */
static void C_ccall f_7777(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_7777,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_7780,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t1,a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
/* chicken-syntax.scm:891: make-if-tree */
t3=((C_word*)t0)[9];
f_7611(t3,t2,((C_word*)t0)[10],((C_word*)t0)[11],((C_word*)t0)[6],((C_word*)t0)[2]);}

/* k7778 in k7775 in k7772 in k7757 in k7754 in k7751 in k7745 in k7714 in k7705 in k7702 in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in ... */
static void C_ccall f_7780(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_7780,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_7787,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=t1,tmp=(C_word)a,a+=10,tmp);
/* chicken-syntax.scm:894: r */
t3=((C_word*)t0)[9];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[123];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7785 in k7778 in k7775 in k7772 in k7757 in k7754 in k7751 in k7745 in k7714 in k7705 in k7702 in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in ... */
static void C_ccall f_7787(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(33,c,1)))){
C_save_and_reclaim((void *)f_7787,c,av);}
a=C_alloc(33);
t2=C_a_i_list(&a,2,((C_word*)t0)[2],((C_word*)t0)[3]);
t3=C_a_i_cons(&a,2,((C_word*)t0)[4],((C_word*)t0)[5]);
t4=C_a_i_cons(&a,2,lf[23],t3);
t5=C_a_i_list(&a,2,((C_word*)t0)[6],t4);
t6=C_a_i_cons(&a,2,t5,((C_word*)t0)[7]);
t7=C_a_i_cons(&a,2,t2,t6);
t8=((C_word*)t0)[8];{
C_word *av2=av;
av2[0]=t8;
av2[1]=C_a_i_list(&a,3,t1,t7,((C_word*)t0)[9]);
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}

/* map-loop2746 in k7757 in k7754 in k7751 in k7745 in k7714 in k7705 in k7702 in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in ... */
static void C_fcall f_7813(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_7813,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7838,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:886: g2752 */
t4=((C_word*)t0)[4];
f_7763(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k7836 in map-loop2746 in k7757 in k7754 in k7751 in k7745 in k7714 in k7705 in k7702 in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in ... */
static void C_ccall f_7838(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7838,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_7813(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* map-loop2717 in k7745 in k7714 in k7705 in k7702 in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in ... */
static void C_fcall f_7847(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_7847,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_cadr(t3);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* map-loop2689 in k7714 in k7705 in k7702 in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in ... */
static void C_fcall f_7881(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_7881,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7906,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:877: g2695 */
t4=((C_word*)t0)[4];
f_7733(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k7904 in map-loop2689 in k7714 in k7705 in k7702 in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in ... */
static void C_ccall f_7906(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7906,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_7881(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* map-loop2659 in k7705 in k7702 in k7519 in a7516 in k5628 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in ... */
static void C_fcall f_7915(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_7915,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_car(t3);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k7961 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in ... */
static void C_ccall f_7963(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_7963,c,av);}
/* chicken-syntax.scm:718: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[129];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a7964 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in ... */
static void C_ccall f_7965(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_7965,c,av);}
a=C_alloc(5);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7969,a[2]=t2,a[3]=t3,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:722: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[129];
av2[3]=t2;
av2[4]=lf[132];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k7967 in a7964 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in ... */
static void C_ccall f_7969(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_7969,c,av);}
a=C_alloc(8);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=C_u_i_cdr(((C_word*)t0)[2]);
t4=C_u_i_cdr(t3);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7979,a[2]=t4,a[3]=t6,a[4]=((C_word*)t0)[3],a[5]=((C_word)li44),tmp=(C_word)a,a+=6,tmp));
t8=((C_word*)t6)[1];
f_7979(t8,((C_word*)t0)[4],t2,C_SCHEME_TRUE);}

/* fold in k7967 in a7964 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in ... */
static void C_fcall f_7979(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,4)))){
C_save_and_reclaim_args((void *)trf_7979,4,t0,t1,t2,t3);}
a=C_alloc(10);
if(C_truep(C_i_nullp(t2))){
t4=C_a_i_cons(&a,2,t3,((C_word*)t0)[2]);
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=C_a_i_cons(&a,2,lf[29],t4);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t4=C_i_car(t2);
t5=C_u_i_cdr(t2);
t6=C_i_pairp(t4);
if(C_truep(C_i_not(t6))){
t7=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_8006,a[2]=t4,a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=t5,a[6]=((C_word*)t0)[4],tmp=(C_word)a,a+=7,tmp);
/* chicken-syntax.scm:731: ##sys#check-syntax */
t8=*((C_word*)lf[45]+1);{
C_word av2[5];
av2[0]=t8;
av2[1]=t7;
av2[2]=lf[129];
av2[3]=t4;
av2[4]=lf[130];
((C_proc)(void*)(*((C_word*)t8+1)))(5,av2);}}
else{
t7=C_i_cdr(t4);
if(C_truep(C_i_nullp(t7))){
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8041,a[2]=t4,a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=t5,tmp=(C_word)a,a+=6,tmp);
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8066,a[2]=((C_word*)t0)[4],a[3]=t8,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:736: chicken.base#gensym */
t10=*((C_word*)lf[56]+1);{
C_word av2[2];
av2[0]=t10;
av2[1]=t9;
((C_proc)(void*)(*((C_word*)t10+1)))(2,av2);}}
else{
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8069,a[2]=t4,a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=t5,tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:740: ##sys#check-syntax */
t9=*((C_word*)lf[45]+1);{
C_word av2[5];
av2[0]=t9;
av2[1]=t8;
av2[2]=lf[129];
av2[3]=t4;
av2[4]=lf[131];
((C_proc)(void*)(*((C_word*)t9+1)))(5,av2);}}}}}

/* k8004 in fold in k7967 in a7964 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in ... */
static void C_ccall f_8006(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_8006,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8009,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8032,a[2]=((C_word*)t0)[6],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:732: chicken.base#gensym */
t4=*((C_word*)lf[56]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k8007 in k8004 in fold in k7967 in a7964 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in ... */
static void C_ccall f_8009(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,3)))){
C_save_and_reclaim((void *)f_8009,c,av);}
a=C_alloc(14);
t2=C_a_i_list(&a,2,t1,((C_word*)t0)[2]);
t3=C_a_i_list(&a,1,t2);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8024,a[2]=t1,a[3]=((C_word*)t0)[3],a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:734: fold */
t5=((C_word*)((C_word*)t0)[4])[1];
f_7979(t5,t4,((C_word*)t0)[5],t1);}

/* k8022 in k8007 in k8004 in fold in k7967 in a7964 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in ... */
static void C_ccall f_8024(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(21,c,1)))){
C_save_and_reclaim((void *)f_8024,c,av);}
a=C_alloc(21);
t2=C_a_i_list(&a,4,lf[36],((C_word*)t0)[2],t1,C_SCHEME_FALSE);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_list(&a,3,lf[37],((C_word*)t0)[4],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k8030 in k8004 in fold in k7967 in a7964 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in ... */
static void C_ccall f_8032(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8032,c,av);}
/* chicken-syntax.scm:732: r */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k8039 in fold in k7967 in a7964 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in ... */
static void C_ccall f_8041(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,3)))){
C_save_and_reclaim((void *)f_8041,c,av);}
a=C_alloc(14);
t2=C_u_i_car(((C_word*)t0)[2]);
t3=C_a_i_list(&a,2,t1,t2);
t4=C_a_i_list(&a,1,t3);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8056,a[2]=t1,a[3]=((C_word*)t0)[3],a[4]=t4,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:738: fold */
t6=((C_word*)((C_word*)t0)[4])[1];
f_7979(t6,t5,((C_word*)t0)[5],t1);}

/* k8054 in k8039 in fold in k7967 in a7964 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in ... */
static void C_ccall f_8056(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(21,c,1)))){
C_save_and_reclaim((void *)f_8056,c,av);}
a=C_alloc(21);
t2=C_a_i_list(&a,4,lf[36],((C_word*)t0)[2],t1,C_SCHEME_FALSE);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_list(&a,3,lf[37],((C_word*)t0)[4],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k8064 in fold in k7967 in a7964 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in ... */
static void C_ccall f_8066(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8066,c,av);}
/* chicken-syntax.scm:736: r */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k8067 in fold in k7967 in a7964 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in ... */
static void C_ccall f_8069(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,3)))){
C_save_and_reclaim((void *)f_8069,c,av);}
a=C_alloc(14);
t2=C_u_i_car(((C_word*)t0)[2]);
t3=C_i_cadr(((C_word*)t0)[2]);
t4=C_a_i_list(&a,2,t2,t3);
t5=C_a_i_list(&a,1,t4);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8085,a[2]=t2,a[3]=((C_word*)t0)[3],a[4]=t5,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:743: fold */
t7=((C_word*)((C_word*)t0)[4])[1];
f_7979(t7,t6,((C_word*)t0)[5],t2);}

/* k8083 in k8067 in fold in k7967 in a7964 in k5625 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in ... */
static void C_ccall f_8085(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(21,c,1)))){
C_save_and_reclaim((void *)f_8085,c,av);}
a=C_alloc(21);
t2=C_a_i_list(&a,4,lf[36],((C_word*)t0)[2],t1,C_SCHEME_FALSE);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_list(&a,3,lf[37],((C_word*)t0)[4],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k8103 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in ... */
static void C_ccall f_8105(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_8105,c,av);}
/* chicken-syntax.scm:697: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[133];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a8106 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in ... */
static void C_ccall f_8107(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_8107,c,av);}
a=C_alloc(8);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8109,a[2]=t4,a[3]=t3,a[4]=((C_word)li46),tmp=(C_word)a,a+=5,tmp);
t7=t5=t6;
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8197,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:716: quotify-proc */
t9=t5;{
C_word *av2=av;
av2[0]=t9;
av2[1]=t8;
av2[2]=C_i_cdr(t2);
av2[3]=lf[133];
((C_proc)(void*)(*((C_word*)t9+1)))(4,av2);}}

/* quotify-proc2559 in a8106 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in ... */
static void C_ccall f_8109(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_8109,c,av);}
a=C_alloc(6);
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8113,a[2]=t2,a[3]=t1,a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:703: ##sys#check-syntax */
t5=*((C_word*)lf[45]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=t3;
av2[3]=t2;
av2[4]=lf[136];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}

/* k8111 in quotify-proc2559 in a8106 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in ... */
static void C_ccall f_8113(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_8113,c,av);}
a=C_alloc(12);
t2=C_i_car(((C_word*)t0)[2]);
t3=C_i_pairp(t2);
t4=(C_truep(t3)?C_u_i_car(t2):t2);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8122,a[2]=((C_word*)t0)[3],a[3]=t4,a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_i_pairp(t2))){
t6=C_u_i_cdr(t2);
t7=C_u_i_cdr(((C_word*)t0)[2]);
t8=C_a_i_cons(&a,2,t6,t7);
t9=t5;
f_8122(t9,C_a_i_cons(&a,2,lf[23],t8));}
else{
t6=t5;
f_8122(t6,C_i_cadr(((C_word*)t0)[2]));}}

/* k8120 in k8111 in quotify-proc2559 in a8106 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in ... */
static void C_fcall f_8122(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(19,0,2)))){
C_save_and_reclaim_args((void *)trf_8122,2,t0,t1);}
a=C_alloc(19);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8125,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t3=C_i_pairp(t1);
t4=C_i_not(t3);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8134,a[2]=t2,a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[2],tmp=(C_word)a,a+=6,tmp);
if(C_truep(t4)){
t6=t5;
f_8134(t6,t4);}
else{
t6=C_i_car(t1);
t7=C_eqp(lf[23],t6);
if(C_truep(C_i_not(t7))){
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8150,a[2]=t5,tmp=(C_word)a,a+=3,tmp);
t9=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8154,a[2]=((C_word*)t0)[4],a[3]=t8,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:711: r */
t10=((C_word*)t0)[5];{
C_word av2[3];
av2[0]=t10;
av2[1]=t9;
av2[2]=lf[135];
((C_proc)(void*)(*((C_word*)t10+1)))(3,av2);}}
else{
t8=t5;
f_8134(t8,C_SCHEME_FALSE);}}}

/* k8123 in k8120 in k8111 in quotify-proc2559 in a8106 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in ... */
static void C_ccall f_8125(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_8125,c,av);}
a=C_alloc(6);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list2(&a,2,((C_word*)t0)[3],((C_word*)t0)[4]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k8132 in k8120 in k8111 in quotify-proc2559 in a8106 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in ... */
static void C_fcall f_8134(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,5)))){
C_save_and_reclaim_args((void *)trf_8134,2,t0,t1);}
a=C_alloc(6);
if(C_truep(t1)){
/* chicken-syntax.scm:712: chicken.syntax#syntax-error */
t2=*((C_word*)lf[58]+1);{
C_word av2[6];
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[133];
av2[3]=lf[134];
av2[4]=((C_word*)t0)[3];
av2[5]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}
else{
t2=((C_word*)t0)[5];{
C_word av2[2];
av2[0]=t2;
av2[1]=C_a_i_list2(&a,2,((C_word*)t0)[3],((C_word*)t0)[4]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k8148 in k8120 in k8111 in quotify-proc2559 in a8106 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in ... */
static void C_ccall f_8150(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_8150,c,av);}
t2=((C_word*)t0)[2];
f_8134(t2,C_i_not(t1));}

/* k8152 in k8120 in k8111 in quotify-proc2559 in a8106 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in ... */
static void C_ccall f_8154(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_8154,c,av);}
/* chicken-syntax.scm:711: c */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
av2[3]=C_u_i_car(((C_word*)t0)[4]);
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k8195 in a8106 in k5622 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in ... */
static void C_ccall f_8197(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_8197,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,lf[137],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k8207 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in ... */
static void C_ccall f_8209(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_8209,c,av);}
/* chicken-syntax.scm:686: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[140];
av2[3]=((C_word*)t0)[3];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a8210 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in ... */
static void C_ccall f_8211(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_8211,c,av);}
a=C_alloc(5);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8215,a[2]=t2,a[3]=t1,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:691: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[140];
av2[3]=t2;
av2[4]=lf[141];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k8213 in a8210 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in ... */
static void C_ccall f_8215(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_8215,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8218,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:692: r */
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[44];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k8216 in k8213 in a8210 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in ... */
static void C_ccall f_8218(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,2)))){
C_save_and_reclaim((void *)f_8218,c,av);}
a=C_alloc(15);
t2=C_i_caddr(((C_word*)t0)[2]);
t3=C_a_i_list(&a,3,lf[23],C_SCHEME_END_OF_LIST,t2);
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8237,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:695: r */
t5=((C_word*)t0)[4];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=lf[138];
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k8235 in k8216 in k8213 in a8210 in k5619 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in ... */
static void C_ccall f_8237(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(27,c,1)))){
C_save_and_reclaim((void *)f_8237,c,av);}
a=C_alloc(27);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=C_a_i_list(&a,3,t1,((C_word*)t0)[3],t2);
t4=C_a_i_list(&a,3,lf[23],((C_word*)t0)[3],t3);
t5=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_a_i_list(&a,3,lf[28],((C_word*)t0)[5],t4);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* k8251 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in ... */
static void C_ccall f_8253(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_8253,c,av);}
/* chicken-syntax.scm:677: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[142];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a8254 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in ... */
static void C_ccall f_8255(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_8255,c,av);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8259,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:682: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[142];
av2[3]=t2;
av2[4]=lf[145];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k8257 in a8254 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in ... */
static void C_ccall f_8259(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_8259,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8262,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:683: chicken.internal#check-for-multiple-bindings */
t3=*((C_word*)lf[143]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_i_cadr(((C_word*)t0)[2]);
av2[3]=((C_word*)t0)[2];
av2[4]=lf[144];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k8260 in k8257 in a8254 in k5616 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in ... */
static void C_ccall f_8262(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_8262,c,av);}
a=C_alloc(3);
t2=C_u_i_cdr(((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_cons(&a,2,lf[50],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k8273 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in ... */
static void C_ccall f_8275(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_8275,c,av);}
/* chicken-syntax.scm:658: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[146];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a8276 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in ... */
static void C_ccall f_8277(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_8277,c,av);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8281,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:662: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[146];
av2[3]=t2;
av2[4]=lf[150];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k8279 in a8276 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in ... */
static void C_ccall f_8281(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,3)))){
C_save_and_reclaim((void *)f_8281,c,av);}
a=C_alloc(18);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=C_u_i_cdr(((C_word*)t0)[2]);
t4=C_u_i_cdr(t3);
t5=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t6=t5;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=((C_word*)t7)[1];
t9=C_i_check_list_2(t2,lf[67]);
t10=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8295,a[2]=((C_word*)t0)[3],a[3]=t4,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t11=C_SCHEME_UNDEFINED;
t12=(*a=C_VECTOR_TYPE|1,a[1]=t11,tmp=(C_word)a,a+=2,tmp);
t13=C_set_block_item(t12,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8497,a[2]=t7,a[3]=t12,a[4]=t8,a[5]=((C_word)li55),tmp=(C_word)a,a+=6,tmp));
t14=((C_word*)t12)[1];
f_8497(t14,t10,t2);}

/* k8293 in k8279 in a8276 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in ... */
static void C_ccall f_8295(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,3)))){
C_save_and_reclaim((void *)f_8295,c,av);}
a=C_alloc(18);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8301,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8463,a[2]=t4,a[3]=t8,a[4]=t5,a[5]=((C_word)li54),tmp=(C_word)a,a+=6,tmp));
t10=((C_word*)t8)[1];
f_8463(t10,t6,((C_word*)t0)[4]);}

/* k8299 in k8293 in k8279 in a8276 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in ... */
static void C_ccall f_8301(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(19,c,4)))){
C_save_and_reclaim((void *)f_8301,c,av);}
a=C_alloc(19);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=C_i_check_list_2(((C_word*)t0)[2],lf[147]);
t7=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_8323,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[2],a[6]=t4,a[7]=t5,tmp=(C_word)a,a+=8,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8427,a[2]=t9,a[3]=((C_word)li53),tmp=(C_word)a,a+=4,tmp));
t11=((C_word*)t9)[1];
f_8427(t11,t7,((C_word*)t0)[2],C_SCHEME_END_OF_LIST);}

/* k8321 in k8299 in k8293 in k8279 in a8276 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in ... */
static void C_ccall f_8323(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,3)))){
C_save_and_reclaim((void *)f_8323,c,av);}
a=C_alloc(14);
t2=C_i_check_list_2(t1,lf[67]);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8329,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8393,a[2]=((C_word*)t0)[6],a[3]=t5,a[4]=((C_word*)t0)[7],a[5]=((C_word)li51),tmp=(C_word)a,a+=6,tmp));
t7=((C_word*)t5)[1];
f_8393(t7,t3,t1);}

/* k8327 in k8321 in k8299 in k8293 in k8279 in a8276 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in ... */
static void C_ccall f_8329(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(22,c,4)))){
C_save_and_reclaim((void *)f_8329,c,av);}
a=C_alloc(22);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8333,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=*((C_word*)lf[148]+1);
t8=C_i_check_list_2(((C_word*)t0)[3],lf[67]);
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8343,a[2]=t2,a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
t10=C_SCHEME_UNDEFINED;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_set_block_item(t11,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_8345,a[2]=t5,a[3]=t11,a[4]=t7,a[5]=t6,a[6]=((C_word)li50),tmp=(C_word)a,a+=7,tmp));
t13=((C_word*)t11)[1];
f_8345(t13,t9,((C_word*)t0)[5],((C_word*)t0)[3]);}

/* k8331 in k8327 in k8321 in k8299 in k8293 in k8279 in a8276 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in ... */
static void C_ccall f_8333(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_8333,c,av);}
a=C_alloc(6);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_cons(&a,2,lf[37],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k8341 in k8327 in k8321 in k8299 in k8293 in k8279 in a8276 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in ... */
static void C_ccall f_8343(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_8343,c,av);}
/* chicken-syntax.scm:667: ##sys#append */
t2=*((C_word*)lf[55]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* map-loop2507 in k8327 in k8321 in k8299 in k8293 in k8279 in a8276 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in ... */
static void C_fcall f_8345(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,3)))){
C_save_and_reclaim_args((void *)trf_8345,4,t0,t1,t2,t3);}
a=C_alloc(7);
t4=C_i_pairp(t2);
t5=(C_truep(t4)?C_i_pairp(t3):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_8374,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,a[6]=t3,tmp=(C_word)a,a+=7,tmp);
/* chicken-syntax.scm:674: g2513 */
t7=((C_word*)t0)[4];{
C_word av2[4];
av2[0]=t7;
av2[1]=t6;
av2[2]=C_slot(t2,C_fix(0));
av2[3]=C_slot(t3,C_fix(0));
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}
else{
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* k8372 in map-loop2507 in k8327 in k8321 in k8299 in k8293 in k8279 in a8276 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in ... */
static void C_ccall f_8374(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_8374,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_8345(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)),C_slot(((C_word*)t0)[6],C_fix(1)));}

/* map-loop2459 in k8321 in k8299 in k8293 in k8279 in a8276 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in ... */
static void C_fcall f_8393(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(9,0,2)))){
C_save_and_reclaim_args((void *)trf_8393,3,t0,t1,t2);}
a=C_alloc(9);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_a_i_list(&a,2,t3,lf[149]);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* foldl2482 in k8299 in k8293 in k8279 in a8276 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in ... */
static void C_fcall f_8427(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,5)))){
C_save_and_reclaim_args((void *)trf_8427,4,t0,t1,t2,t3);}
a=C_alloc(12);
if(C_truep(C_i_pairp(t2))){
t4=C_slot(t2,C_fix(1));
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8457,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
t6=C_slot(t2,C_fix(0));
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8451,a[2]=t5,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8453,a[2]=((C_word)li52),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:670: ##sys#decompose-lambda-list */
t9=*((C_word*)lf[105]+1);{
C_word av2[4];
av2[0]=t9;
av2[1]=t7;
av2[2]=t6;
av2[3]=t8;
((C_proc)(void*)(*((C_word*)t9+1)))(4,av2);}}
else{
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k8449 in foldl2482 in k8299 in k8293 in k8279 in a8276 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in ... */
static void C_ccall f_8451(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_8451,c,av);}
/* chicken-syntax.scm:670: ##sys#append */
t2=*((C_word*)lf[55]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* a8452 in foldl2482 in k8299 in k8293 in k8279 in a8276 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in ... */
static void C_ccall f_8453(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_8453,c,av);}
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* k8455 in foldl2482 in k8299 in k8293 in k8279 in a8276 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in ... */
static void C_ccall f_8457(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_8457,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_8427(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* map-loop2430 in k8293 in k8279 in a8276 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in ... */
static void C_fcall f_8463(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_8463,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_cadr(t3);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* map-loop2404 in k8279 in a8276 in k5613 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in ... */
static void C_fcall f_8497(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_8497,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_car(t3);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k8531 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in ... */
static void C_ccall f_8533(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_8533,c,av);}
/* chicken-syntax.scm:643: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[151];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a8534 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in ... */
static void C_ccall f_8535(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_8535,c,av);}
a=C_alloc(5);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8539,a[2]=t2,a[3]=t1,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:647: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[151];
av2[3]=t2;
av2[4]=lf[153];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k8537 in a8534 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in ... */
static void C_ccall f_8539(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_8539,c,av);}
a=C_alloc(5);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=C_u_i_cdr(((C_word*)t0)[2]);
t4=C_u_i_cdr(t3);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8547,a[2]=t4,a[3]=((C_word*)t0)[3],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:650: r */
t6=((C_word*)t0)[4];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[152];
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k8545 in k8537 in a8534 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in ... */
static void C_ccall f_8547(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_8547,c,av);}
a=C_alloc(8);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8552,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t3,a[5]=((C_word)li57),tmp=(C_word)a,a+=6,tmp));
t5=((C_word*)t3)[1];
f_8552(t5,((C_word*)t0)[3],((C_word*)t0)[4]);}

/* fold in k8545 in k8537 in a8534 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in ... */
static void C_fcall f_8552(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_8552,3,t0,t1,t2);}
a=C_alloc(8);
if(C_truep(C_i_nullp(t2))){
t3=C_a_i_cons(&a,2,C_SCHEME_END_OF_LIST,((C_word*)t0)[2]);
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=C_a_i_cons(&a,2,lf[37],t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=C_i_car(t2);
t4=C_a_i_list(&a,1,t3);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8577,a[2]=t1,a[3]=((C_word*)t0)[3],a[4]=t4,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:655: fold */
t7=t5;
t8=C_u_i_cdr(t2);
t1=t7;
t2=t8;
goto loop;}}

/* k8575 in fold in k8545 in k8537 in a8534 in k5610 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in ... */
static void C_ccall f_8577(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_8577,c,av);}
a=C_alloc(9);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,3,((C_word*)t0)[3],((C_word*)t0)[4],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k8585 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in ... */
static void C_ccall f_8587(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_8587,c,av);}
/* chicken-syntax.scm:589: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[152];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a8588 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in ... */
static void C_ccall f_8589(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_8589,c,av);}
a=C_alloc(5);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8593,a[2]=t2,a[3]=t3,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:593: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[152];
av2[3]=t2;
av2[4]=lf[154];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k8591 in a8588 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in ... */
static void C_ccall f_8593(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(33,c,4)))){
C_save_and_reclaim((void *)f_8593,c,av);}
a=C_alloc(33);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=C_u_i_cdr(((C_word*)t0)[2]);
t4=C_u_i_cdr(t3);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8599,a[2]=t6,a[3]=((C_word)li59),tmp=(C_word)a,a+=4,tmp);
t10=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8627,a[2]=t8,a[3]=((C_word)li60),tmp=(C_word)a,a+=4,tmp);
t11=C_set_block_item(t6,0,t9);
t12=C_set_block_item(t8,0,t10);
t13=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t14=t13;
t15=(*a=C_VECTOR_TYPE|1,a[1]=t14,tmp=(C_word)a,a+=2,tmp);
t16=((C_word*)t15)[1];
t17=C_i_check_list_2(t2,lf[67]);
t18=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_8675,a[2]=((C_word*)t0)[3],a[3]=t4,a[4]=((C_word*)t0)[4],a[5]=t2,a[6]=t8,a[7]=t6,tmp=(C_word)a,a+=8,tmp);
t19=C_SCHEME_UNDEFINED;
t20=(*a=C_VECTOR_TYPE|1,a[1]=t19,tmp=(C_word)a,a+=2,tmp);
t21=C_set_block_item(t20,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9028,a[2]=t15,a[3]=t20,a[4]=t16,a[5]=((C_word)li70),tmp=(C_word)a,a+=6,tmp));
t22=((C_word*)t20)[1];
f_9028(t22,t18,t2);}

/* append*2210 in k8591 in a8588 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in ... */
static void C_ccall f_8599(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_8599,c,av);}
a=C_alloc(4);
t4=C_i_pairp(t2);
if(C_truep(C_i_not(t4))){
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_a_i_cons(&a,2,t2,t3);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t5=C_i_car(t2);
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8620,a[2]=t1,a[3]=t5,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:600: append* */
t7=((C_word*)((C_word*)t0)[2])[1];{
C_word *av2=av;
av2[0]=t7;
av2[1]=t6;
av2[2]=C_u_i_cdr(t2);
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}}

/* k8618 in append*2210 in k8591 in a8588 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in ... */
static void C_ccall f_8620(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_8620,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* map*2211 in k8591 in a8588 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in ... */
static void C_ccall f_8627(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_8627,c,av);}
a=C_alloc(6);
if(C_truep(C_i_nullp(t3))){
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=C_i_pairp(t3);
if(C_truep(C_i_not(t4))){
/* chicken-syntax.scm:603: proc */
t5=t2;{
C_word *av2=av;
av2[0]=t5;
av2[1]=t1;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}
else{
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8650,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:604: proc */
t6=t2;{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=C_i_car(t3);
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}}}

/* k8648 in map*2211 in k8591 in a8588 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in ... */
static void C_ccall f_8650(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_8650,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8654,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:604: map* */
t3=((C_word*)((C_word*)t0)[3])[1];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=C_u_i_cdr(((C_word*)t0)[5]);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k8652 in k8648 in map*2211 in k8591 in a8588 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in ... */
static void C_ccall f_8654(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_8654,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k8673 in k8591 in a8588 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in ... */
static void C_ccall f_8675(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,4)))){
C_save_and_reclaim((void *)f_8675,c,av);}
a=C_alloc(15);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_8678,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8990,a[2]=t4,a[3]=((C_word*)t0)[7],a[4]=((C_word)li69),tmp=(C_word)a,a+=5,tmp));
t6=((C_word*)t4)[1];
f_8990(t6,t2,t1,C_SCHEME_END_OF_LIST);}

/* k8676 in k8673 in k8591 in a8588 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in ... */
static void C_ccall f_8678(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(26,c,3)))){
C_save_and_reclaim((void *)f_8678,c,av);}
a=C_alloc(26);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8682,a[2]=((C_word*)t0)[2],a[3]=((C_word)li61),tmp=(C_word)a,a+=4,tmp);
t7=C_i_check_list_2(t1,lf[67]);
t8=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_8700,a[2]=t1,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_8956,a[2]=t4,a[3]=t10,a[4]=t6,a[5]=t5,a[6]=((C_word)li68),tmp=(C_word)a,a+=7,tmp));
t12=((C_word*)t10)[1];
f_8956(t12,t8,t1);}

/* g2271 in k8676 in k8673 in k8591 in a8588 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in ... */
static void C_fcall f_8682(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_8682,3,t0,t1,t2);}
a=C_alloc(8);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8690,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8694,a[2]=((C_word*)t0)[2],a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:615: chicken.base#gensym */
t5=*((C_word*)lf[56]+1);{
C_word av2[3];
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k8688 in g2271 in k8676 in k8673 in k8591 in a8588 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in ... */
static void C_ccall f_8690(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_8690,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k8692 in g2271 in k8676 in k8673 in k8591 in a8588 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in ... */
static void C_ccall f_8694(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8694,c,av);}
/* chicken-syntax.scm:615: r */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k8698 in k8676 in k8673 in k8591 in a8588 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in ... */
static void C_ccall f_8700(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(20,c,4)))){
C_save_and_reclaim((void *)f_8700,c,av);}
a=C_alloc(20);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8701,a[2]=t1,a[3]=((C_word)li62),tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_8712,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8912,a[2]=t5,a[3]=t2,a[4]=((C_word*)t0)[7],a[5]=((C_word)li67),tmp=(C_word)a,a+=6,tmp));
t7=((C_word*)t5)[1];
f_8912(t7,t3,((C_word*)t0)[5],C_SCHEME_END_OF_LIST);}

/* lookup in k8698 in k8676 in k8673 in k8591 in a8588 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in ... */
static void C_ccall f_8701(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_8701,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_cdr(C_i_assq(t2,((C_word*)t0)[2]));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k8710 in k8698 in k8676 in k8673 in k8591 in a8588 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in ... */
static void C_ccall f_8712(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(21,c,3)))){
C_save_and_reclaim((void *)f_8712,c,av);}
a=C_alloc(21);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_8727,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t1,tmp=(C_word)a,a+=8,tmp);
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8878,a[2]=t4,a[3]=t8,a[4]=t5,a[5]=((C_word)li66),tmp=(C_word)a,a+=6,tmp));
t10=((C_word*)t8)[1];
f_8878(t10,t6,((C_word*)t0)[7]);}

/* k8725 in k8710 in k8698 in k8676 in k8673 in k8591 in a8588 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in ... */
static void C_ccall f_8727(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,5)))){
C_save_and_reclaim((void *)f_8727,c,av);}
a=C_alloc(9);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_8729,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t3,a[6]=((C_word)li65),tmp=(C_word)a,a+=7,tmp));
t5=((C_word*)t3)[1];
f_8729(t5,((C_word*)t0)[5],((C_word*)t0)[6],t1,((C_word*)t0)[7]);}

/* fold in k8725 in k8710 in k8698 in k8676 in k8673 in k8591 in a8588 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in ... */
static void C_fcall f_8729(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(22,0,4)))){
C_save_and_reclaim_args((void *)trf_8729,5,t0,t1,t2,t3,t4);}
a=C_alloc(22);
if(C_truep(C_i_nullp(t2))){
t5=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t6=t5;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=((C_word*)t7)[1];
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8748,a[2]=((C_word*)t0)[2],a[3]=((C_word)li63),tmp=(C_word)a,a+=4,tmp);
t10=C_i_check_list_2(((C_word*)t0)[3],lf[67]);
t11=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8762,a[2]=((C_word*)t0)[4],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_8764,a[2]=t7,a[3]=t13,a[4]=t9,a[5]=t8,a[6]=((C_word)li64),tmp=(C_word)a,a+=7,tmp));
t15=((C_word*)t13)[1];
f_8764(t15,t11,((C_word*)t0)[3]);}
else{
t5=C_i_car(t4);
t6=C_i_pairp(t5);
t7=(C_truep(t6)?C_i_nullp(C_i_cdar(t4)):C_SCHEME_FALSE);
if(C_truep(t7)){
t8=C_i_caar(t4);
t9=C_i_car(t3);
t10=C_a_i_list(&a,2,t8,t9);
t11=C_a_i_list(&a,1,t10);
t12=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8813,a[2]=t1,a[3]=t11,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:635: fold */
t16=t12;
t17=C_i_cdr(t2);
t18=C_u_i_cdr(t3);
t19=C_u_i_cdr(t4);
t1=t16;
t2=t17;
t3=t18;
t4=t19;
goto loop;}
else{
t8=C_i_car(t3);
t9=C_a_i_list(&a,3,lf[23],C_SCHEME_END_OF_LIST,t8);
t10=C_u_i_car(t4);
t11=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8850,a[2]=t10,a[3]=t1,a[4]=t9,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:641: fold */
t16=t11;
t17=C_i_cdr(t2);
t18=C_u_i_cdr(t3);
t19=C_u_i_cdr(t4);
t1=t16;
t2=t17;
t3=t18;
t4=t19;
goto loop;}}}

/* g2322 in fold in k8725 in k8710 in k8698 in k8676 in k8673 in k8591 in a8588 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in ... */
static void C_fcall f_8748(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_8748,3,t0,t1,t2);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8756,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:630: lookup */
t4=((C_word*)t0)[2];{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
f_8701(3,av2);}}

/* k8754 in g2322 in fold in k8725 in k8710 in k8698 in k8676 in k8673 in k8591 in a8588 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in ... */
static void C_ccall f_8756(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_8756,c,av);}
a=C_alloc(6);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k8760 in fold in k8725 in k8710 in k8698 in k8676 in k8673 in k8591 in a8588 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in ... */
static void C_ccall f_8762(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_8762,c,av);}
a=C_alloc(6);
t2=C_a_i_cons(&a,2,t1,((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_cons(&a,2,lf[37],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* map-loop2316 in fold in k8725 in k8710 in k8698 in k8676 in k8673 in k8591 in a8588 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in ... */
static void C_fcall f_8764(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_8764,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8789,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:630: g2322 */
t4=((C_word*)t0)[4];
f_8748(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k8787 in map-loop2316 in fold in k8725 in k8710 in k8698 in k8676 in k8673 in k8591 in a8588 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in ... */
static void C_ccall f_8789(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_8789,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_8764(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k8811 in fold in k8725 in k8710 in k8698 in k8676 in k8673 in k8591 in a8588 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in ... */
static void C_ccall f_8813(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_8813,c,av);}
a=C_alloc(9);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,3,lf[37],((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k8848 in fold in k8725 in k8710 in k8698 in k8676 in k8673 in k8591 in a8588 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in ... */
static void C_ccall f_8850(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,1)))){
C_save_and_reclaim((void *)f_8850,c,av);}
a=C_alloc(18);
t2=C_a_i_list(&a,3,lf[23],((C_word*)t0)[2],t1);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_list(&a,3,lf[28],((C_word*)t0)[4],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* map-loop2351 in k8710 in k8698 in k8676 in k8673 in k8591 in a8588 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in ... */
static void C_fcall f_8878(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_8878,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_cadr(t3);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* loop in k8698 in k8676 in k8673 in k8591 in a8588 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in ... */
static void C_fcall f_8912(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_8912,4,t0,t1,t2,t3);}
a=C_alloc(6);
if(C_truep(C_i_nullp(t2))){
/* chicken-syntax.scm:619: scheme#reverse */
t4=*((C_word*)lf[57]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t1;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t4=C_i_car(t2);
t5=C_i_pairp(t4);
if(C_truep(C_i_not(t5))){
t6=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8943,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:622: lookup */
t7=((C_word*)t0)[3];{
C_word av2[3];
av2[0]=t7;
av2[1]=t6;
av2[2]=t4;
f_8701(3,av2);}}
else{
t6=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8950,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:623: map* */
t7=((C_word*)((C_word*)t0)[4])[1];{
C_word av2[4];
av2[0]=t7;
av2[1]=t6;
av2[2]=((C_word*)t0)[3];
av2[3]=t4;
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}}}

/* k8941 in loop in k8698 in k8676 in k8673 in k8591 in a8588 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in ... */
static void C_ccall f_8943(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_8943,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,((C_word*)t0)[2]);
/* chicken-syntax.scm:624: loop */
t3=((C_word*)((C_word*)t0)[3])[1];
f_8912(t3,((C_word*)t0)[4],C_u_i_cdr(((C_word*)t0)[5]),t2);}

/* k8948 in loop in k8698 in k8676 in k8673 in k8591 in a8588 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in ... */
static void C_ccall f_8950(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_8950,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,((C_word*)t0)[2]);
/* chicken-syntax.scm:624: loop */
t3=((C_word*)((C_word*)t0)[3])[1];
f_8912(t3,((C_word*)t0)[4],C_u_i_cdr(((C_word*)t0)[5]),t2);}

/* map-loop2265 in k8676 in k8673 in k8591 in a8588 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in ... */
static void C_fcall f_8956(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_8956,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8981,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:615: g2271 */
t4=((C_word*)t0)[4];
f_8682(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k8979 in map-loop2265 in k8676 in k8673 in k8591 in a8588 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in ... */
static void C_ccall f_8981(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_8981,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_8956(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* loop in k8673 in k8591 in a8588 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in ... */
static void C_fcall f_8990(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(8,0,3)))){
C_save_and_reclaim_args((void *)trf_8990,4,t0,t1,t2,t3);}
a=C_alloc(8);
if(C_truep(C_i_nullp(t2))){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=C_i_car(t2);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9003,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_i_listp(t4))){
/* chicken-syntax.scm:611: scheme#append */
t6=*((C_word*)lf[16]+1);{
C_word av2[4];
av2[0]=t6;
av2[1]=t5;
av2[2]=t4;
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}
else{
if(C_truep(C_i_pairp(t4))){
/* chicken-syntax.scm:612: append* */
t6=((C_word*)((C_word*)t0)[3])[1];{
C_word av2[4];
av2[0]=t6;
av2[1]=t5;
av2[2]=t4;
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}
else{
t6=C_a_i_cons(&a,2,t4,t3);
/* chicken-syntax.scm:614: loop */
t8=t1;
t9=C_u_i_cdr(t2);
t10=t6;
t1=t8;
t2=t9;
t3=t10;
goto loop;}}}}

/* k9001 in loop in k8673 in k8591 in a8588 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in ... */
static void C_ccall f_9003(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_9003,c,av);}
/* chicken-syntax.scm:614: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_8990(t2,((C_word*)t0)[3],C_u_i_cdr(((C_word*)t0)[4]),t1);}

/* map-loop2227 in k8591 in a8588 in k5607 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in ... */
static void C_fcall f_9028(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_9028,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_car(t3);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k9062 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in ... */
static void C_ccall f_9064(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_9064,c,av);}
/* chicken-syntax.scm:572: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[155];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a9065 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in ... */
static void C_ccall f_9066(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_9066,c,av);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9070,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:576: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[155];
av2[3]=t2;
av2[4]=lf[160];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k9068 in a9065 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in ... */
static void C_ccall f_9070(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_9070,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9077,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9081,a[2]=t2,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t4=C_i_cadr(((C_word*)t0)[3]);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9103,a[2]=((C_word)li74),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:578: ##sys#decompose-lambda-list */
t6=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t3;
av2[2]=t4;
av2[3]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* k9075 in k9068 in a9065 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in ... */
static void C_ccall f_9077(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_9077,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,lf[29],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k9079 in k9068 in a9065 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in ... */
static void C_ccall f_9081(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_9081,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9089,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:587: ##sys#expand-multiple-values-assignment */
t3=*((C_word*)lf[148]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_i_cadr(((C_word*)t0)[3]);
av2[3]=C_i_caddr(((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k9087 in k9079 in k9068 in a9065 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in ... */
static void C_ccall f_9089(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_9089,c,av);}
a=C_alloc(3);
t2=C_a_i_list(&a,1,t1);
/* chicken-syntax.scm:577: ##sys#append */
t3=*((C_word*)lf[55]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* a9102 in k9068 in a9065 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in ... */
static void C_ccall f_9103(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_9103,c,av);}
a=C_alloc(10);
t5=C_i_check_list_2(t2,lf[69]);
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9122,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9169,a[2]=t8,a[3]=((C_word)li73),tmp=(C_word)a,a+=4,tmp));
t10=((C_word*)t8)[1];
f_9169(t10,t6,t2);}

/* k9107 in for-each-loop2156 in a9102 in k9068 in a9065 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in ... */
static void C_ccall f_9109(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_9109,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9116,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:583: ##sys#current-module */
t3=*((C_word*)lf[85]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k9114 in k9107 in for-each-loop2156 in a9102 in k9068 in a9065 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in ... */
static void C_ccall f_9116(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_9116,c,av);}
/* chicken-syntax.scm:583: ##sys#register-export */
t2=*((C_word*)lf[157]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k9120 in a9102 in k9068 in a9065 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in ... */
static void C_ccall f_9122(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,3)))){
C_save_and_reclaim((void *)f_9122,c,av);}
a=C_alloc(13);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9135,a[2]=t4,a[3]=t7,a[4]=t5,a[5]=((C_word)li72),tmp=(C_word)a,a+=6,tmp));
t9=((C_word*)t7)[1];
f_9135(t9,((C_word*)t0)[2],((C_word*)t0)[3]);}

/* map-loop2166 in k9120 in a9102 in k9068 in a9065 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in ... */
static void C_fcall f_9135(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(9,0,2)))){
C_save_and_reclaim_args((void *)trf_9135,3,t0,t1,t2);}
a=C_alloc(9);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_a_i_list(&a,2,lf[156],t3);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* for-each-loop2156 in a9102 in k9068 in a9065 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in ... */
static void C_fcall f_9169(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,4)))){
C_save_and_reclaim_args((void *)trf_9169,3,t0,t1,t2);}
a=C_alloc(8);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9179,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t4=C_slot(t2,C_fix(0));
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9109,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:582: ##sys#get */
t6=*((C_word*)lf[158]+1);{
C_word av2[5];
av2[0]=t6;
av2[1]=t5;
av2[2]=t4;
av2[3]=lf[159];
av2[4]=t4;
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k9177 in for-each-loop2156 in a9102 in k9068 in a9065 in k5603 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in ... */
static void C_ccall f_9179(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_9179,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_9169(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k9192 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in ... */
static void C_ccall f_9194(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_9194,c,av);}
/* chicken-syntax.scm:564: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[161];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a9195 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in ... */
static void C_ccall f_9196(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_9196,c,av);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9200,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:568: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[161];
av2[3]=t2;
av2[4]=lf[162];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k9198 in a9195 in k5600 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in ... */
static void C_ccall f_9200(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_9200,c,av);}
/* chicken-syntax.scm:569: ##sys#expand-multiple-values-assignment */
t2=*((C_word*)lf[148]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_i_cadr(((C_word*)t0)[3]);
av2[3]=C_i_caddr(((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k9213 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in ... */
static void C_ccall f_9215(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_9215,c,av);}
/* chicken-syntax.scm:555: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[163];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a9216 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in ... */
static void C_ccall f_9217(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_9217,c,av);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9221,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:559: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[163];
av2[3]=t2;
av2[4]=lf[165];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k9219 in a9216 in k5597 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in ... */
static void C_ccall f_9221(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,1)))){
C_save_and_reclaim((void *)f_9221,c,av);}
a=C_alloc(18);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=C_a_i_list(&a,1,lf[164]);
t4=C_u_i_cdr(((C_word*)t0)[2]);
t5=C_u_i_cdr(t4);
t6=C_a_i_cons(&a,2,lf[29],t5);
t7=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t7;
av2[1]=C_a_i_list(&a,4,lf[36],t2,t3,t6);
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}

/* k9241 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in ... */
static void C_ccall f_9243(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_9243,c,av);}
/* chicken-syntax.scm:547: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[166];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a9244 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in ... */
static void C_ccall f_9245(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_9245,c,av);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9249,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:551: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[166];
av2[3]=t2;
av2[4]=lf[167];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k9247 in a9244 in k5594 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in ... */
static void C_ccall f_9249(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,1)))){
C_save_and_reclaim((void *)f_9249,c,av);}
a=C_alloc(12);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=C_u_i_cdr(((C_word*)t0)[2]);
t4=C_u_i_cdr(t3);
t5=C_a_i_cons(&a,2,lf[29],t4);
t6=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_a_i_list(&a,3,lf[36],t2,t5);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}

/* k9265 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in ... */
static void C_ccall f_9267(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_9267,c,av);}
/* chicken-syntax.scm:534: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[168];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a9268 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in ... */
static void C_ccall f_9269(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(22,c,3)))){
C_save_and_reclaim((void *)f_9269,c,av);}
a=C_alloc(22);
t5=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t6=t5;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=((C_word*)t7)[1];
t9=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9278,a[2]=t3,a[3]=t4,a[4]=((C_word)li81),tmp=(C_word)a,a+=5,tmp);
t10=C_i_cdr(t2);
t11=C_i_check_list_2(t10,lf[67]);
t12=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9313,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t13=C_SCHEME_UNDEFINED;
t14=(*a=C_VECTOR_TYPE|1,a[1]=t13,tmp=(C_word)a,a+=2,tmp);
t15=C_set_block_item(t14,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_9315,a[2]=t7,a[3]=t14,a[4]=t9,a[5]=t8,a[6]=((C_word)li82),tmp=(C_word)a,a+=7,tmp));
t16=((C_word*)t14)[1];
f_9315(t16,t12,t10);}

/* g2081 in a9268 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in ... */
static void C_fcall f_9278(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,8)))){
C_save_and_reclaim_args((void *)trf_9278,3,t0,t1,t2);}
a=C_alloc(9);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9284,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word)li79),tmp=(C_word)a,a+=6,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9290,a[2]=((C_word)li80),tmp=(C_word)a,a+=3,tmp);
/* chicken-syntax.scm:541: ##sys#call-with-values */{
C_word av2[4];
av2[0]=0;
av2[1]=t1;
av2[2]=t3;
av2[3]=t4;
C_call_with_values(4,av2);}}

/* a9283 in g2081 in a9268 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in ... */
static void C_ccall f_9284(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_9284,c,av);}
/* chicken-syntax.scm:541: ##sys#decompose-import */
t2=*((C_word*)lf[169]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
av2[3]=((C_word*)t0)[3];
av2[4]=((C_word*)t0)[4];
av2[5]=lf[48];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* a9289 in g2081 in a9268 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in ... */
static void C_ccall f_9290(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6=av[6];
C_word t7=av[7];
C_word t8;
C_word t9;
C_word *a;
if(c!=8) C_bad_argc_2(c,8,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_9290,c,av);}
a=C_alloc(4);
if(C_truep(C_i_not(t3))){
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=lf[170];
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}
else{
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9304,a[2]=t1,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:544: chicken.internal#module-requirement */
t9=*((C_word*)lf[172]+1);{
C_word *av2=av;
av2[0]=t9;
av2[1]=t8;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}}

/* k9302 in a9289 in g2081 in a9268 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in ... */
static void C_ccall f_9304(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_9304,c,av);}
a=C_alloc(9);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,3,lf[171],((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k9311 in a9268 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in ... */
static void C_ccall f_9313(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_9313,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,lf[29],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* map-loop2075 in a9268 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in ... */
static void C_fcall f_9315(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_9315,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9340,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:540: g2081 */
t4=((C_word*)t0)[4];
f_9278(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k9338 in map-loop2075 in a9268 in k5591 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in ... */
static void C_ccall f_9340(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_9340,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_9315(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k9349 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in ... */
static void C_ccall f_9351(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_9351,c,av);}
/* chicken-syntax.scm:480: ##sys#extend-macro-environment */
t2=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[173];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in k5526 in ... */
static void C_ccall f_9353(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_9353,c,av);}
a=C_alloc(5);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9372,a[2]=t2,a[3]=t3,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* chicken-syntax.scm:488: ##sys#check-syntax */
t6=*((C_word*)lf[45]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[173];
av2[3]=t2;
av2[4]=lf[181];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in k5529 in ... */
static void C_ccall f_9372(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_9372,c,av);}
a=C_alloc(6);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=C_u_i_cdr(((C_word*)t0)[2]);
t4=C_u_i_cdr(t3);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9380,a[2]=t2,a[3]=((C_word*)t0)[3],a[4]=t4,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* chicken-syntax.scm:491: r */
t6=((C_word*)t0)[3];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[180];
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in k5532 in ... */
static void C_ccall f_9380(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(20,c,3)))){
C_save_and_reclaim((void *)f_9380,c,av);}
a=C_alloc(20);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=C_i_check_list_2(((C_word*)t0)[2],lf[67]);
t7=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_9389,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[2],tmp=(C_word)a,a+=7,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10245,a[2]=t4,a[3]=t9,a[4]=t5,a[5]=((C_word)li101),tmp=(C_word)a,a+=6,tmp));
t11=((C_word*)t9)[1];
f_10245(t11,t7,((C_word*)t0)[2]);}

/* k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in k5535 in ... */
static void C_ccall f_9389(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(20,c,3)))){
C_save_and_reclaim((void *)f_9389,c,av);}
a=C_alloc(20);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_9395,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10211,a[2]=t4,a[3]=t8,a[4]=t5,a[5]=((C_word)li100),tmp=(C_word)a,a+=6,tmp));
t10=((C_word*)t8)[1];
f_10211(t10,t6,((C_word*)t0)[6]);}

/* k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in k5538 in ... */
static void C_ccall f_9395(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(26,c,3)))){
C_save_and_reclaim((void *)f_9395,c,av);}
a=C_alloc(26);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9399,a[2]=((C_word*)t0)[2],a[3]=((C_word)li84),tmp=(C_word)a,a+=4,tmp);
t7=C_i_check_list_2(((C_word*)t0)[3],lf[67]);
t8=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_9413,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[3],tmp=(C_word)a,a+=8,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10177,a[2]=t4,a[3]=t10,a[4]=t6,a[5]=t5,a[6]=((C_word)li99),tmp=(C_word)a,a+=7,tmp));
t12=((C_word*)t10)[1];
f_10177(t12,t8,((C_word*)t0)[3]);}

/* g1666 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in ... */
static void C_fcall f_9399(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_9399,3,t0,t1,t2);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9407,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_i_symbolp(t2))){
/* chicken-syntax.scm:486: chicken.base#gensym */
t4=*((C_word*)lf[56]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
/* chicken-syntax.scm:487: chicken.base#gensym */
t4=*((C_word*)lf[56]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[174];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}}

/* k9405 in g1666 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in ... */
static void C_ccall f_9407(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_9407,c,av);}
/* chicken-syntax.scm:494: r */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in k5542 in ... */
static void C_ccall f_9413(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(27,c,3)))){
C_save_and_reclaim((void *)f_9413,c,av);}
a=C_alloc(27);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9417,a[2]=((C_word*)t0)[2],a[3]=((C_word)li85),tmp=(C_word)a,a+=4,tmp);
t7=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_9428,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10143,a[2]=t4,a[3]=t9,a[4]=t6,a[5]=t5,a[6]=((C_word)li98),tmp=(C_word)a,a+=7,tmp));
t11=((C_word*)t9)[1];
f_10143(t11,t7,((C_word*)t0)[7]);}

/* g1694 in k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in ... */
static void C_fcall f_9417(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_9417,2,t0,t1);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9425,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:495: chicken.base#gensym */
t3=*((C_word*)lf[56]+1);{
C_word av2[3];
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[175];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k9423 in g1694 in k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in ... */
static void C_ccall f_9425(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_9425,c,av);}
/* chicken-syntax.scm:495: r */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k9426 in k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in k5545 in ... */
static void C_ccall f_9428(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(27,c,3)))){
C_save_and_reclaim((void *)f_9428,c,av);}
a=C_alloc(27);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9432,a[2]=((C_word*)t0)[2],a[3]=((C_word)li86),tmp=(C_word)a,a+=4,tmp);
t7=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_9443,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10109,a[2]=t4,a[3]=t9,a[4]=t6,a[5]=t5,a[6]=((C_word)li97),tmp=(C_word)a,a+=7,tmp));
t11=((C_word*)t9)[1];
f_10109(t11,t7,((C_word*)t0)[8]);}

/* g1722 in k9426 in k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in ... */
static void C_fcall f_9432(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_9432,2,t0,t1);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9440,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* chicken-syntax.scm:496: chicken.base#gensym */
t3=*((C_word*)lf[56]+1);{
C_word av2[3];
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[44];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k9438 in g1722 in k9426 in k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in ... */
static void C_ccall f_9440(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_9440,c,av);}
/* chicken-syntax.scm:496: r */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k9441 in k9426 in k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in k5548 in ... */
static void C_ccall f_9443(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(22,c,4)))){
C_save_and_reclaim((void *)f_9443,c,av);}
a=C_alloc(22);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=C_i_check_list_2(((C_word*)t0)[2],lf[67]);
t7=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_9456,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=t1,a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[2],tmp=(C_word)a,a+=9,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10061,a[2]=t4,a[3]=t9,a[4]=t5,a[5]=((C_word)li96),tmp=(C_word)a,a+=6,tmp));
t11=((C_word*)t9)[1];
f_10061(t11,t7,((C_word*)t0)[2],((C_word*)t0)[8]);}

/* k9454 in k9441 in k9426 in k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in k5551 in ... */
static void C_ccall f_9456(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(22,c,4)))){
C_save_and_reclaim((void *)f_9456,c,av);}
a=C_alloc(22);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=C_i_check_list_2(((C_word*)t0)[2],lf[67]);
t7=C_i_check_list_2(((C_word*)t0)[3],lf[67]);
t8=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_9473,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[7],a[6]=t1,a[7]=((C_word*)t0)[2],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10013,a[2]=t4,a[3]=t10,a[4]=t5,a[5]=((C_word)li95),tmp=(C_word)a,a+=6,tmp));
t12=((C_word*)t10)[1];
f_10013(t12,t8,((C_word*)t0)[2],((C_word*)t0)[3]);}

/* k9471 in k9454 in k9441 in k9426 in k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in k5554 in ... */
static void C_ccall f_9473(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(23,c,4)))){
C_save_and_reclaim((void *)f_9473,c,av);}
a=C_alloc(23);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_9484,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9965,a[2]=t4,a[3]=t8,a[4]=t5,a[5]=((C_word)li94),tmp=(C_word)a,a+=6,tmp));
t10=((C_word*)t8)[1];
f_9965(t10,t6,((C_word*)t0)[7],((C_word*)t0)[7]);}

/* k9482 in k9471 in k9454 in k9441 in k9426 in k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in ... */
static void C_ccall f_9484(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(51,c,5)))){
C_save_and_reclaim((void *)f_9484,c,av);}
a=C_alloc(51);
t2=C_a_i_list(&a,4,lf[176],lf[177],C_SCHEME_TRUE,C_SCHEME_TRUE);
t3=C_a_i_list(&a,2,((C_word*)t0)[2],t2);
t4=C_a_i_list(&a,1,t3);
t5=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t6=t5;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=((C_word*)t7)[1];
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9735,a[2]=((C_word*)t0)[2],a[3]=((C_word)li87),tmp=(C_word)a,a+=4,tmp);
t10=C_i_check_list_2(((C_word*)t0)[3],lf[67]);
t11=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_9753,a[2]=((C_word*)t0)[4],a[3]=t4,a[4]=t1,a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[3],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[2],tmp=(C_word)a,a+=12,tmp);
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_9895,a[2]=t9,a[3]=t7,a[4]=t13,a[5]=t8,a[6]=((C_word)li93),tmp=(C_word)a,a+=7,tmp));
t15=((C_word*)t13)[1];
f_9895(t15,t11,((C_word*)t0)[9],((C_word*)t0)[8],((C_word*)t0)[3]);}

/* k9530 in k9755 in k9751 in k9482 in k9471 in k9454 in k9441 in k9426 in k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in ... */
static void C_ccall f_9532(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(28,c,4)))){
C_save_and_reclaim((void *)f_9532,c,av);}
a=C_alloc(28);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_9536,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9548,a[2]=t2,a[3]=((C_word*)t0)[9],a[4]=((C_word*)t0)[10],tmp=(C_word)a,a+=5,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9610,a[2]=t5,a[3]=t9,a[4]=t6,a[5]=((C_word)li89),tmp=(C_word)a,a+=6,tmp));
t11=((C_word*)t9)[1];
f_9610(t11,t7,((C_word*)t0)[11],((C_word*)t0)[9]);}

/* k9534 in k9530 in k9755 in k9751 in k9482 in k9471 in k9454 in k9441 in k9426 in k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in ... */
static void C_ccall f_9536(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(63,c,1)))){
C_save_and_reclaim((void *)f_9536,c,av);}
a=C_alloc(63);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_cons(&a,2,lf[37],t2);
t4=C_a_i_list(&a,3,lf[23],C_SCHEME_END_OF_LIST,t3);
t5=C_a_i_list(&a,4,lf[178],((C_word*)t0)[3],((C_word*)t0)[4],t4);
t6=C_a_i_list(&a,3,lf[37],((C_word*)t0)[5],t5);
t7=C_a_i_list(&a,3,lf[37],((C_word*)t0)[6],t6);
t8=C_a_i_list(&a,3,lf[37],((C_word*)t0)[7],t7);
t9=((C_word*)t0)[8];{
C_word *av2=av;
av2[0]=t9;
av2[1]=C_a_i_list(&a,3,lf[37],((C_word*)t0)[9],t8);
((C_proc)(void*)(*((C_word*)t9+1)))(2,av2);}}

/* k9546 in k9530 in k9755 in k9751 in k9482 in k9471 in k9454 in k9441 in k9426 in k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in ... */
static void C_ccall f_9548(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(17,c,4)))){
C_save_and_reclaim((void *)f_9548,c,av);}
a=C_alloc(17);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9560,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9562,a[2]=t4,a[3]=t8,a[4]=t5,a[5]=((C_word)li88),tmp=(C_word)a,a+=6,tmp));
t10=((C_word*)t8)[1];
f_9562(t10,t6,((C_word*)t0)[3],((C_word*)t0)[4]);}

/* k9558 in k9546 in k9530 in k9755 in k9751 in k9482 in k9471 in k9454 in k9441 in k9426 in k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in ... */
static void C_ccall f_9560(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_9560,c,av);}
/* chicken-syntax.scm:497: ##sys#append */
t2=*((C_word*)lf[55]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* map-loop2031 in k9546 in k9530 in k9755 in k9751 in k9482 in k9471 in k9454 in k9441 in k9426 in k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in ... */
static void C_fcall f_9562(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(12,0,3)))){
C_save_and_reclaim_args((void *)trf_9562,4,t0,t1,t2,t3);}
a=C_alloc(12);
t4=C_i_pairp(t2);
t5=(C_truep(t4)?C_i_pairp(t3):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_slot(t2,C_fix(0));
t7=C_slot(t3,C_fix(0));
t8=C_a_i_list(&a,3,lf[179],t6,t7);
t9=C_a_i_cons(&a,2,t8,C_SCHEME_END_OF_LIST);
t10=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t9);
t11=C_mutate(((C_word *)((C_word*)t0)[2])+1,t9);
t13=t1;
t14=C_slot(t2,C_fix(1));
t15=C_slot(t3,C_fix(1));
t1=t13;
t2=t14;
t3=t15;
goto loop;}
else{
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* map-loop1995 in k9530 in k9755 in k9751 in k9482 in k9471 in k9454 in k9441 in k9426 in k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in ... */
static void C_fcall f_9610(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(15,0,3)))){
C_save_and_reclaim_args((void *)trf_9610,4,t0,t1,t2,t3);}
a=C_alloc(15);
t4=C_i_pairp(t2);
t5=(C_truep(t4)?C_i_pairp(t3):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_slot(t2,C_fix(0));
t7=C_slot(t3,C_fix(0));
t8=C_a_i_list(&a,4,t6,t7,C_SCHEME_FALSE,C_SCHEME_TRUE);
t9=C_a_i_cons(&a,2,t8,C_SCHEME_END_OF_LIST);
t10=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t9);
t11=C_mutate(((C_word *)((C_word*)t0)[2])+1,t9);
t13=t1;
t14=C_slot(t2,C_fix(1));
t15=C_slot(t3,C_fix(1));
t1=t13;
t2=t14;
t3=t15;
goto loop;}
else{
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* map-loop1953 in k9755 in k9751 in k9482 in k9471 in k9454 in k9441 in k9426 in k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in ... */
static void C_fcall f_9658(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,2)))){
C_save_and_reclaim_args((void *)trf_9658,5,t0,t1,t2,t3,t4);}
a=C_alloc(9);
t5=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_9665,a[2]=t2,a[3]=t4,a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],a[6]=t1,a[7]=t3,a[8]=((C_word*)t0)[4],tmp=(C_word)a,a+=9,tmp);
if(C_truep(C_i_pairp(t2))){
t6=C_i_pairp(t3);
t7=t5;
f_9665(t7,(C_truep(t6)?C_i_pairp(t4):C_SCHEME_FALSE));}
else{
t6=t5;
f_9665(t6,C_SCHEME_FALSE);}}

/* k9663 in map-loop1953 in k9755 in k9751 in k9482 in k9471 in k9454 in k9441 in k9426 in k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in ... */
static void C_fcall f_9665(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,4)))){
C_save_and_reclaim_args((void *)trf_9665,2,t0,t1);}
a=C_alloc(12);
if(C_truep(t1)){
t2=C_slot(((C_word*)t0)[2],C_fix(0));
t3=C_slot(((C_word*)t0)[3],C_fix(0));
t4=C_a_i_list(&a,1,t2);
t5=C_a_i_list(&a,2,t3,t4);
t6=C_a_i_cons(&a,2,t5,C_SCHEME_END_OF_LIST);
t7=C_i_setslot(((C_word*)((C_word*)t0)[4])[1],C_fix(1),t6);
t8=C_mutate(((C_word *)((C_word*)t0)[4])+1,t6);
t9=((C_word*)((C_word*)t0)[5])[1];
f_9658(t9,((C_word*)t0)[6],C_slot(((C_word*)t0)[2],C_fix(1)),C_slot(((C_word*)t0)[7],C_fix(1)),C_slot(((C_word*)t0)[3],C_fix(1)));}
else{
t2=((C_word*)t0)[6];{
C_word av2[2];
av2[0]=t2;
av2[1]=C_slot(((C_word*)t0)[8],C_fix(1));
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* g1845 in k9482 in k9471 in k9454 in k9441 in k9426 in k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in ... */
static C_word C_fcall f_9735(C_word *a,C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_stack_overflow_check;{}
t4=C_a_i_list(&a,4,t1,t2,C_SCHEME_TRUE,C_SCHEME_FALSE);
t5=C_a_i_list(&a,4,lf[36],((C_word*)t0)[2],t4,t2);
return(C_a_i_list(&a,2,t3,t5));}

/* k9751 in k9482 in k9471 in k9454 in k9441 in k9426 in k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in ... */
static void C_ccall f_9753(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(31,c,4)))){
C_save_and_reclaim((void *)f_9753,c,av);}
a=C_alloc(31);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_9757,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],tmp=(C_word)a,a+=12,tmp);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9773,a[2]=t2,a[3]=((C_word*)t0)[11],a[4]=((C_word*)t0)[10],a[5]=((C_word*)t0)[9],tmp=(C_word)a,a+=6,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9847,a[2]=t5,a[3]=t9,a[4]=t6,a[5]=((C_word)li92),tmp=(C_word)a,a+=6,tmp));
t11=((C_word*)t9)[1];
f_9847(t11,t7,((C_word*)t0)[10],((C_word*)t0)[8]);}

/* k9755 in k9751 in k9482 in k9471 in k9454 in k9441 in k9426 in k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in ... */
static void C_ccall f_9757(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(46,c,5)))){
C_save_and_reclaim((void *)f_9757,c,av);}
a=C_alloc(46);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_cons(&a,2,lf[37],t2);
t4=C_a_i_list(&a,3,lf[23],C_SCHEME_END_OF_LIST,t3);
t5=C_a_i_cons(&a,2,C_SCHEME_END_OF_LIST,((C_word*)t0)[3]);
t6=C_a_i_cons(&a,2,lf[23],t5);
t7=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t8=t7;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=((C_word*)t9)[1];
t11=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_9532,a[2]=t4,a[3]=t6,a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],tmp=(C_word)a,a+=12,tmp);
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9658,a[2]=t9,a[3]=t13,a[4]=t10,a[5]=((C_word)li90),tmp=(C_word)a,a+=6,tmp));
t15=((C_word*)t13)[1];
f_9658(t15,t11,((C_word*)t0)[11],((C_word*)t0)[9],((C_word*)t0)[10]);}

/* k9771 in k9751 in k9482 in k9471 in k9454 in k9441 in k9426 in k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in ... */
static void C_ccall f_9773(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(21,c,4)))){
C_save_and_reclaim((void *)f_9773,c,av);}
a=C_alloc(21);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9777,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9789,a[2]=((C_word*)t0)[3],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9799,a[2]=t5,a[3]=t9,a[4]=t6,a[5]=((C_word)li91),tmp=(C_word)a,a+=6,tmp));
t11=((C_word*)t9)[1];
f_9799(t11,t7,((C_word*)t0)[4],((C_word*)t0)[5]);}

/* k9775 in k9771 in k9751 in k9482 in k9471 in k9454 in k9441 in k9426 in k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in ... */
static void C_ccall f_9777(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_9777,c,av);}
/* chicken-syntax.scm:497: ##sys#append */
t2=*((C_word*)lf[55]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k9787 in k9771 in k9751 in k9482 in k9471 in k9454 in k9441 in k9426 in k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in ... */
static void C_ccall f_9789(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_9789,c,av);}
a=C_alloc(12);
t2=C_a_i_list(&a,3,lf[179],((C_word*)t0)[2],C_SCHEME_FALSE);
t3=C_a_i_list(&a,1,t2);
/* chicken-syntax.scm:497: ##sys#append */
t4=*((C_word*)lf[55]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* map-loop1917 in k9771 in k9751 in k9482 in k9471 in k9454 in k9441 in k9426 in k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in ... */
static void C_fcall f_9799(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(15,0,3)))){
C_save_and_reclaim_args((void *)trf_9799,4,t0,t1,t2,t3);}
a=C_alloc(15);
t4=C_i_pairp(t2);
t5=(C_truep(t4)?C_i_pairp(t3):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_slot(t2,C_fix(0));
t7=C_slot(t3,C_fix(0));
t8=C_a_i_list(&a,4,t6,t7,C_SCHEME_FALSE,C_SCHEME_TRUE);
t9=C_a_i_cons(&a,2,t8,C_SCHEME_END_OF_LIST);
t10=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t9);
t11=C_mutate(((C_word *)((C_word*)t0)[2])+1,t9);
t13=t1;
t14=C_slot(t2,C_fix(1));
t15=C_slot(t3,C_fix(1));
t1=t13;
t2=t14;
t3=t15;
goto loop;}
else{
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* map-loop1881 in k9751 in k9482 in k9471 in k9454 in k9441 in k9426 in k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in ... */
static void C_fcall f_9847(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(15,0,3)))){
C_save_and_reclaim_args((void *)trf_9847,4,t0,t1,t2,t3);}
a=C_alloc(15);
t4=C_i_pairp(t2);
t5=(C_truep(t4)?C_i_pairp(t3):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_slot(t2,C_fix(0));
t7=C_slot(t3,C_fix(0));
t8=C_a_i_list(&a,1,t6);
t9=C_a_i_list(&a,3,lf[179],t7,t8);
t10=C_a_i_cons(&a,2,t9,C_SCHEME_END_OF_LIST);
t11=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t10);
t12=C_mutate(((C_word *)((C_word*)t0)[2])+1,t10);
t14=t1;
t15=C_slot(t2,C_fix(1));
t16=C_slot(t3,C_fix(1));
t1=t14;
t2=t15;
t3=t16;
goto loop;}
else{
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* map-loop1839 in k9482 in k9471 in k9454 in k9441 in k9426 in k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in ... */
static void C_fcall f_9895(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,2)))){
C_save_and_reclaim_args((void *)trf_9895,5,t0,t1,t2,t3,t4);}
a=C_alloc(10);
t5=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_9902,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=t3,a[5]=t4,a[6]=((C_word*)t0)[3],a[7]=((C_word*)t0)[4],a[8]=t1,a[9]=((C_word*)t0)[5],tmp=(C_word)a,a+=10,tmp);
if(C_truep(C_i_pairp(t2))){
t6=C_i_pairp(t3);
t7=t5;
f_9902(t7,(C_truep(t6)?C_i_pairp(t4):C_SCHEME_FALSE));}
else{
t6=t5;
f_9902(t6,C_SCHEME_FALSE);}}

/* k9900 in map-loop1839 in k9482 in k9471 in k9454 in k9441 in k9426 in k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in ... */
static void C_fcall f_9902(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(33,0,4)))){
C_save_and_reclaim_args((void *)trf_9902,2,t0,t1);}
a=C_alloc(33);
if(C_truep(t1)){
t2=(
/* chicken-syntax.scm:512: g1845 */
  f_9735(C_a_i(&a,30),((C_word*)t0)[2],C_slot(((C_word*)t0)[3],C_fix(0)),C_slot(((C_word*)t0)[4],C_fix(0)),C_slot(((C_word*)t0)[5],C_fix(0)))
);
t3=C_a_i_cons(&a,2,t2,C_SCHEME_END_OF_LIST);
t4=C_i_setslot(((C_word*)((C_word*)t0)[6])[1],C_fix(1),t3);
t5=C_mutate(((C_word *)((C_word*)t0)[6])+1,t3);
t6=((C_word*)((C_word*)t0)[7])[1];
f_9895(t6,((C_word*)t0)[8],C_slot(((C_word*)t0)[3],C_fix(1)),C_slot(((C_word*)t0)[4],C_fix(1)),C_slot(((C_word*)t0)[5],C_fix(1)));}
else{
t2=((C_word*)t0)[8];{
C_word av2[2];
av2[0]=t2;
av2[1]=C_slot(((C_word*)t0)[9],C_fix(1));
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* map-loop1808 in k9471 in k9454 in k9441 in k9426 in k9411 in k9393 in k9387 in k9378 in k9370 in a9352 in k5588 in k5585 in k5582 in k5579 in k5576 in k5573 in k5570 in k5567 in k5564 in k5560 in k5557 in ... */
static void C_fcall f_9965(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(9,0,3)))){
C_save_and_reclaim_args((void *)trf_9965,4,t0,t1,t2,t3);}
a=C_alloc(9);
t4=C_i_pairp(t2);
t5=(C_truep(t4)?C_i_pairp(t3):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_slot(t2,C_fix(0));
t7=C_slot(t3,C_fix(0));
t8=C_a_i_list(&a,2,t6,t7);
t9=C_a_i_cons(&a,2,t8,C_SCHEME_END_OF_LIST);
t10=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t9);
t11=C_mutate(((C_word *)((C_word*)t0)[2])+1,t9);
t13=t1;
t14=C_slot(t2,C_fix(1));
t15=C_slot(t3,C_fix(1));
t1=t13;
t2=t14;
t3=t15;
goto loop;}
else{
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* toplevel */
static C_TLS int toplevel_initialized=0;

void C_ccall C_chicken_2dsyntax_toplevel(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(toplevel_initialized) {C_kontinue(t1,C_SCHEME_UNDEFINED);}
else C_toplevel_entry(C_text("chicken-syntax"));
C_check_nursery_minimum(C_calculate_demand(3,c,2));
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void*)C_chicken_2dsyntax_toplevel,c,av);}
toplevel_initialized=1;
if(C_unlikely(!C_demand_2(2807))){
C_save(t1);
C_rereclaim2(2807*sizeof(C_word),1);
t1=C_restore;}
a=C_alloc(3);
C_initialize_lf(lf,286);
lf[0]=C_h_intern(&lf[0],14, C_text("chicken-syntax"));
lf[2]=C_h_intern(&lf[2],41, C_text("##sys#chicken.condition-macro-environment"));
lf[3]=C_h_intern(&lf[3],36, C_text("##sys#chicken.type-macro-environment"));
lf[4]=C_h_intern(&lf[4],38, C_text("##sys#chicken.syntax-macro-environment"));
lf[5]=C_h_intern(&lf[5],39, C_text("chicken.syntax#define-values-definition"));
lf[6]=C_h_intern(&lf[6],36, C_text("##sys#chicken.base-macro-environment"));
lf[7]=C_h_intern(&lf[7],36, C_text("##sys#chicken.time-macro-environment"));
lf[8]=C_h_intern(&lf[8],31, C_text("##sys#chicken-macro-environment"));
lf[9]=C_h_intern(&lf[9],34, C_text("chicken.platform#register-feature!"));
lf[10]=C_h_intern(&lf[10],6, C_text("srfi-8"));
lf[11]=C_h_intern(&lf[11],7, C_text("srfi-11"));
lf[12]=C_h_intern(&lf[12],7, C_text("srfi-15"));
lf[13]=C_h_intern(&lf[13],7, C_text("srfi-16"));
lf[14]=C_h_intern(&lf[14],7, C_text("srfi-26"));
lf[15]=C_h_intern(&lf[15],7, C_text("srfi-31"));
lf[16]=C_h_intern(&lf[16],13, C_text("scheme#append"));
lf[17]=C_h_intern(&lf[17],29, C_text("chicken.internal#macro-subset"));
lf[18]=C_h_intern(&lf[18],31, C_text("##sys#default-macro-environment"));
lf[19]=C_h_intern(&lf[19],23, C_text("##sys#macro-environment"));
lf[20]=C_h_intern(&lf[20],30, C_text("##sys#extend-macro-environment"));
lf[21]=C_h_intern(&lf[21],4, C_text("time"));
lf[22]=C_h_intern(&lf[22],17, C_text("##sys#start-timer"));
lf[23]=C_h_intern(&lf[23],13, C_text("##core#lambda"));
lf[24]=C_h_intern(&lf[24],16, C_text("##sys#stop-timer"));
lf[25]=C_h_intern(&lf[25],19, C_text("##sys#display-times"));
lf[26]=C_h_intern(&lf[26],11, C_text("##sys#apply"));
lf[27]=C_h_intern(&lf[27],12, C_text("##sys#values"));
lf[28]=C_h_intern(&lf[28],22, C_text("##sys#call-with-values"));
lf[29]=C_h_intern(&lf[29],12, C_text("##core#begin"));
lf[30]=C_h_intern(&lf[30],1, C_text("t"));
lf[31]=C_h_intern(&lf[31],20, C_text("##sys#er-transformer"));
lf[32]=C_h_intern(&lf[32],6, C_text("assert"));
lf[33]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020assertion failed"));
lf[34]=C_h_intern(&lf[34],12, C_text("##core#check"));
lf[35]=C_h_intern(&lf[35],11, C_text("##sys#error"));
lf[36]=C_h_intern(&lf[36],9, C_text("##core#if"));
lf[37]=C_h_intern(&lf[37],10, C_text("##core#let"));
lf[38]=C_h_intern(&lf[38],12, C_text("##core#quote"));
lf[39]=C_h_intern(&lf[39],27, C_text("chicken.syntax#strip-syntax"));
lf[40]=C_h_intern(&lf[40],20, C_text("scheme#string-append"));
lf[41]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001("));
lf[42]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002) "));
lf[43]=C_h_intern(&lf[43],30, C_text("chicken.syntax#get-line-number"));
lf[44]=C_h_intern(&lf[44],3, C_text("tmp"));
lf[45]=C_h_intern(&lf[45],18, C_text("##sys#check-syntax"));
lf[46]=C_decode_literal(C_heaptop,C_text("\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000\000\001"));
lf[47]=C_h_intern(&lf[47],17, C_text("require-extension"));
lf[48]=C_h_intern(&lf[48],6, C_text("import"));
lf[49]=C_h_intern(&lf[49],3, C_text("rec"));
lf[50]=C_h_intern(&lf[50],14, C_text("##core#letrec\052"));
lf[51]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\001\000\000\001\001_"));
lf[52]=C_h_intern(&lf[52],5, C_text("apply"));
lf[53]=C_h_intern(&lf[53],12, C_text("scheme#apply"));
lf[54]=C_h_intern(&lf[54],4, C_text("cute"));
lf[55]=C_h_intern(&lf[55],12, C_text("##sys#append"));
lf[56]=C_h_intern(&lf[56],19, C_text("chicken.base#gensym"));
lf[57]=C_h_intern(&lf[57],14, C_text("scheme#reverse"));
lf[58]=C_h_intern(&lf[58],27, C_text("chicken.syntax#syntax-error"));
lf[59]=C_decode_literal(C_heaptop,C_text("\376B\000\000+tail patterns after <...> are not supported"));
lf[60]=C_decode_literal(C_heaptop,C_text("\376B\000\000\047you need to supply at least a procedure"));
lf[61]=C_h_intern(&lf[61],5, C_text("<...>"));
lf[62]=C_h_intern(&lf[62],2, C_text("<>"));
lf[63]=C_h_intern(&lf[63],3, C_text("cut"));
lf[64]=C_decode_literal(C_heaptop,C_text("\376B\000\000+tail patterns after <...> are not supported"));
lf[65]=C_decode_literal(C_heaptop,C_text("\376B\000\000\047you need to supply at least a procedure"));
lf[66]=C_h_intern(&lf[66],18, C_text("define-record-type"));
lf[67]=C_h_intern(&lf[67],3, C_text("map"));
lf[68]=C_decode_literal(C_heaptop,C_text("\376B\000\000+unknown slot name in constructor definition"));
lf[69]=C_h_intern(&lf[69],8, C_text("for-each"));
lf[70]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016"));
lf[71]=C_h_intern(&lf[71],20, C_text("##sys#make-structure"));
lf[72]=C_h_intern(&lf[72],16, C_text("##sys#structure\077"));
lf[73]=C_h_intern(&lf[73],21, C_text("##sys#check-structure"));
lf[74]=C_h_intern(&lf[74],15, C_text("##sys#block-ref"));
lf[75]=C_h_intern(&lf[75],12, C_text("##sys#setter"));
lf[76]=C_h_intern(&lf[76],16, C_text("##sys#block-set!"));
lf[77]=C_h_intern(&lf[77],6, C_text("setter"));
lf[78]=C_h_intern(&lf[78],1, C_text("y"));
lf[79]=C_h_intern(&lf[79],1, C_text("x"));
lf[80]=C_h_intern(&lf[80],31, C_text("chicken.base#getter-with-setter"));
lf[81]=C_h_intern(&lf[81],6, C_text("define"));
lf[82]=C_h_intern(&lf[82],26, C_text("chicken.base#symbol-append"));
lf[83]=C_h_intern(&lf[83],1, C_text("#"));
lf[84]=C_h_intern(&lf[84],17, C_text("##sys#module-name"));
lf[85]=C_h_intern(&lf[85],20, C_text("##sys#current-module"));
lf[86]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\010\001variable\376\003\000\000\002\376\000\000\000\002\376\001\000\000\010\001variable\376\377\001\000\000\000\001\376\003\000\000\002\376\001\000\000\010\001variabl"
"e\376\001\000\000\001\001_"));
lf[87]=C_h_intern(&lf[87],21, C_text("define-record-printer"));
lf[88]=C_h_intern(&lf[88],29, C_text("##sys#register-record-printer"));
lf[89]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\010\001variable\376\003\000\000\002\376\001\000\000\010\001variable\376\003\000\000\002\376\001\000\000\010\001variable\376\377\016\376\000\000\000\002\376\001\000\000\001\001_\376\377\001"
"\000\000\000\001"));
lf[90]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\010\001variable\376\003\000\000\002\376\001\000\000\001\001_\376\377\016"));
lf[91]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\001\000\000\001\001_"));
lf[92]=C_h_intern(&lf[92],2, C_text(">="));
lf[93]=C_h_intern(&lf[93],9, C_text("scheme#>="));
lf[94]=C_h_intern(&lf[94],3, C_text("car"));
lf[95]=C_h_intern(&lf[95],10, C_text("scheme#car"));
lf[96]=C_h_intern(&lf[96],3, C_text("cdr"));
lf[97]=C_h_intern(&lf[97],10, C_text("scheme#cdr"));
lf[98]=C_h_intern(&lf[98],3, C_text("eq\077"));
lf[99]=C_h_intern(&lf[99],10, C_text("scheme#eq\077"));
lf[100]=C_h_intern(&lf[100],6, C_text("length"));
lf[101]=C_h_intern(&lf[101],13, C_text("scheme#length"));
lf[102]=C_h_intern(&lf[102],11, C_text("case-lambda"));
lf[103]=C_h_intern(&lf[103],5, C_text("foldr"));
lf[104]=C_h_intern(&lf[104],11, C_text("lambda-list"));
lf[105]=C_h_intern(&lf[105],27, C_text("##sys#decompose-lambda-list"));
lf[106]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\014\001##core#check\376\003\000\000\002\376\003\000\000\002\376\001\000\000\013\001##sys#error\376\003\000\000\002\376\003\000\000\002\376\001\000\000\020\001##core#immutab"
"le\376\003\000\000\002\376\003\000\000\002\376\001\000\000\014\001##core#quote\376\003\000\000\002\376B\000\0000no matching clause in call to \047case-lamb"
"da\047 form\376\377\016\376\377\016\376\377\016\376\377\016"));
lf[107]=C_h_intern(&lf[107],4, C_text("lvar"));
lf[108]=C_h_intern(&lf[108],4, C_text("rvar"));
lf[109]=C_h_intern(&lf[109],10, C_text("scheme#min"));
lf[110]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\001\000\000\001\001_"));
lf[111]=C_h_intern(&lf[111],5, C_text("null\077"));
lf[112]=C_h_intern(&lf[112],12, C_text("scheme#null\077"));
lf[113]=C_h_intern(&lf[113],14, C_text("let-optionals\052"));
lf[114]=C_h_intern(&lf[114],4, C_text("tmp2"));
lf[115]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\004\001list\376\001\000\000\001\001_"));
lf[116]=C_h_intern(&lf[116],8, C_text("optional"));
lf[117]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\000\000\000\003\376\001\000\000\001\001_\376\377\001\000\000\000\000\376\377\001\000\000\000\001"));
lf[118]=C_h_intern(&lf[118],13, C_text("let-optionals"));
lf[119]=C_h_intern(&lf[119],21, C_text("scheme#string->symbol"));
lf[120]=C_h_intern(&lf[120],21, C_text("scheme#symbol->string"));
lf[121]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001%"));
lf[122]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004def-"));
lf[123]=C_h_intern(&lf[123],4, C_text("let\052"));
lf[124]=C_h_intern(&lf[124],6, C_text("_%rest"));
lf[125]=C_h_intern(&lf[125],4, C_text("body"));
lf[126]=C_decode_literal(C_heaptop,C_text("\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000\000\001"));
lf[127]=C_decode_literal(C_heaptop,C_text("\376\000\000\000\002\376\003\000\000\002\376\001\000\000\010\001variable\376\003\000\000\002\376\001\000\000\001\001_\376\377\016\376\377\001\000\000\000\000"));
lf[128]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\001\000\000\001\001_"));
lf[129]=C_h_intern(&lf[129],8, C_text("and-let\052"));
lf[130]=C_h_intern(&lf[130],8, C_text("variable"));
lf[131]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\010\001variable\376\003\000\000\002\376\001\000\000\001\001_\376\377\016"));
lf[132]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000\000\000\376\001\000\000\001\001_"));
lf[133]=C_h_intern(&lf[133],13, C_text("define-inline"));
lf[134]=C_decode_literal(C_heaptop,C_text("\376B\000\000\052invalid substitution form - must be lambda"));
lf[135]=C_h_intern(&lf[135],6, C_text("lambda"));
lf[136]=C_decode_literal(C_heaptop,C_text("\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000\000\001"));
lf[137]=C_h_intern(&lf[137],20, C_text("##core#define-inline"));
lf[138]=C_h_intern(&lf[138],8, C_text("list-ref"));
lf[139]=C_h_intern(&lf[139],15, C_text("scheme#list-ref"));
lf[140]=C_h_intern(&lf[140],9, C_text("nth-value"));
lf[141]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\377\016"));
lf[142]=C_h_intern(&lf[142],7, C_text("letrec\052"));
lf[143]=C_h_intern(&lf[143],44, C_text("chicken.internal#check-for-multiple-bindings"));
lf[144]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007letrec\052"));
lf[145]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\000\000\000\002\376\003\000\000\002\376\001\000\000\010\001variable\376\003\000\000\002\376\001\000\000\001\001_\376\377\016\376\377\001\000\000\000\000\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000"
"\000\001"));
lf[146]=C_h_intern(&lf[146],13, C_text("letrec-values"));
lf[147]=C_h_intern(&lf[147],5, C_text("foldl"));
lf[148]=C_h_intern(&lf[148],39, C_text("##sys#expand-multiple-values-assignment"));
lf[149]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016"));
lf[150]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\000\000\000\002\376\003\000\000\002\376\001\000\000\013\001lambda-list\376\001\000\000\001\001_\376\377\001\000\000\000\000\376\001\000\000\001\001_"));
lf[151]=C_h_intern(&lf[151],11, C_text("let\052-values"));
lf[152]=C_h_intern(&lf[152],10, C_text("let-values"));
lf[153]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\004\001list\376\001\000\000\001\001_"));
lf[154]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\004\001list\376\001\000\000\001\001_"));
lf[155]=C_h_intern(&lf[155],13, C_text("define-values"));
lf[156]=C_h_intern(&lf[156],33, C_text("##core#ensure-toplevel-definition"));
lf[157]=C_h_intern(&lf[157],21, C_text("##sys#register-export"));
lf[158]=C_h_intern(&lf[158],9, C_text("##sys#get"));
lf[159]=C_h_intern(&lf[159],18, C_text("##core#macro-alias"));
lf[160]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\013\001lambda-list\376\003\000\000\002\376\001\000\000\001\001_\376\377\016"));
lf[161]=C_h_intern(&lf[161],11, C_text("set!-values"));
lf[162]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\013\001lambda-list\376\003\000\000\002\376\001\000\000\001\001_\376\377\016"));
lf[163]=C_h_intern(&lf[163],6, C_text("unless"));
lf[164]=C_h_intern(&lf[164],16, C_text("##core#undefined"));
lf[165]=C_decode_literal(C_heaptop,C_text("\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000\000\002"));
lf[166]=C_h_intern(&lf[166],4, C_text("when"));
lf[167]=C_decode_literal(C_heaptop,C_text("\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000\000\002"));
lf[168]=C_h_intern(&lf[168],15, C_text("require-library"));
lf[169]=C_h_intern(&lf[169],22, C_text("##sys#decompose-import"));
lf[170]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016"));
lf[171]=C_h_intern(&lf[171],14, C_text("##core#require"));
lf[172]=C_h_intern(&lf[172],35, C_text("chicken.internal#module-requirement"));
lf[173]=C_h_intern(&lf[173],12, C_text("parameterize"));
lf[174]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011parameter"));
lf[175]=C_h_intern(&lf[175],5, C_text("saved"));
lf[176]=C_h_intern(&lf[176],10, C_text("##core#the"));
lf[177]=C_h_intern(&lf[177],7, C_text("boolean"));
lf[178]=C_h_intern(&lf[178],18, C_text("##sys#dynamic-wind"));
lf[179]=C_h_intern(&lf[179],11, C_text("##core#set!"));
lf[180]=C_h_intern(&lf[180],8, C_text("convert\077"));
lf[181]=C_decode_literal(C_heaptop,C_text("\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000\000\002"));
lf[182]=C_h_intern(&lf[182],9, C_text("fluid-let"));
lf[183]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\000\000\000\002\376\003\000\000\002\376\001\000\000\010\001variable\376\003\000\000\002\376\001\000\000\001\001_\376\377\016\376\377\001\000\000\000\000\376\001\000\000\001\001_"));
lf[184]=C_h_intern(&lf[184],16, C_text("include-relative"));
lf[185]=C_h_intern(&lf[185],14, C_text("##core#include"));
lf[186]=C_h_intern(&lf[186],29, C_text("##sys#current-source-filename"));
lf[187]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\006\001string\376\377\016"));
lf[188]=C_h_intern(&lf[188],7, C_text("include"));
lf[189]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\006\001string\376\377\016"));
lf[190]=C_h_intern(&lf[190],11, C_text("delay-force"));
lf[191]=C_h_intern(&lf[191],18, C_text("##sys#make-promise"));
lf[192]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\377\016"));
lf[193]=C_h_intern(&lf[193],7, C_text("declare"));
lf[194]=C_h_intern(&lf[194],14, C_text("##core#declare"));
lf[195]=C_h_intern(&lf[195],7, C_text("receive"));
lf[196]=C_h_intern(&lf[196],10, C_text("##sys#list"));
lf[197]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\013\001lambda-list\376\003\000\000\002\376\001\000\000\001\001_\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000\000\001"));
lf[198]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000\000\000"));
lf[199]=C_h_intern(&lf[199],13, C_text("define-record"));
lf[200]=C_decode_literal(C_heaptop,C_text("\376B\000\000\032invalid slot specification"));
lf[201]=C_h_intern(&lf[201],3, C_text("val"));
lf[202]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001-"));
lf[203]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001-"));
lf[204]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005-set!"));
lf[205]=C_h_intern(&lf[205],19, C_text("##sys#string-append"));
lf[206]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001\077"));
lf[207]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005make-"));
lf[208]=C_h_intern(&lf[208],19, C_text("chicken.base#setter"));
lf[209]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\010\001variable\376\001\000\000\001\001_"));
lf[210]=C_h_intern(&lf[210],15, C_text("define-constant"));
lf[211]=C_h_intern(&lf[211],22, C_text("##core#define-constant"));
lf[212]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\010\001variable\376\003\000\000\002\376\001\000\000\001\001_\376\377\016"));
lf[213]=C_h_intern(&lf[213],19, C_text("let-compiler-syntax"));
lf[214]=C_h_intern(&lf[214],26, C_text("##core#let-compiler-syntax"));
lf[215]=C_h_intern(&lf[215],51, C_text("chicken.internal.syntax-rules#syntax-rules-mismatch"));
lf[216]=C_h_intern(&lf[216],7, C_text("##sys#+"));
lf[217]=C_h_intern(&lf[217],7, C_text("##sys#="));
lf[218]=C_h_intern(&lf[218],8, C_text("##sys#>="));
lf[219]=C_h_intern(&lf[219],12, C_text("##sys#length"));
lf[220]=C_h_intern(&lf[220],11, C_text("##sys#list\077"));
lf[221]=C_h_intern(&lf[221],22, C_text("define-compiler-syntax"));
lf[222]=C_h_intern(&lf[222],29, C_text("##core#define-compiler-syntax"));
lf[223]=C_h_intern(&lf[223],17, C_text("define-for-syntax"));
lf[224]=C_h_intern(&lf[224],16, C_text("begin-for-syntax"));
lf[225]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\001\000\000\001\001_"));
lf[226]=C_h_intern(&lf[226],26, C_text("##core#elaborationtimeonly"));
lf[227]=C_h_intern(&lf[227],30, C_text("##sys#register-meta-expression"));
lf[228]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000\000\000"));
lf[229]=C_h_intern(&lf[229],6, C_text("syntax"));
lf[230]=C_h_intern(&lf[230],13, C_text("##core#syntax"));
lf[231]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\377\016"));
lf[232]=C_h_intern(&lf[232],11, C_text("define-type"));
lf[233]=C_h_intern_kw(&lf[233],9, C_text("compiling"));
lf[234]=C_h_intern(&lf[234],14, C_text("##sys#features"));
lf[235]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016"));
lf[236]=C_h_intern(&lf[236],28, C_text("##compiler#type-abbreviation"));
lf[237]=C_h_intern(&lf[237],18, C_text("##sys#put/restore!"));
lf[238]=C_h_intern(&lf[238],52, C_text("chicken.compiler.scrutinizer#check-and-validate-type"));
lf[239]=C_h_intern(&lf[239],5, C_text("quote"));
lf[240]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\010\001variable\376\003\000\000\002\376\001\000\000\001\001_\376\377\016"));
lf[241]=C_h_intern(&lf[241],17, C_text("compiler-typecase"));
lf[242]=C_h_intern(&lf[242],4, C_text("else"));
lf[243]=C_h_intern(&lf[243],15, C_text("##core#typecase"));
lf[244]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\000\000\000\002\376\003\000\000\002\376\001\000\000\001\001_\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000\000\001\376\377\001\000\000\000\001"));
lf[245]=C_h_intern(&lf[245],21, C_text("define-specialization"));
lf[246]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016"));
lf[247]=C_h_intern(&lf[247],6, C_text("inline"));
lf[248]=C_h_intern(&lf[248],4, C_text("hide"));
lf[249]=C_h_intern(&lf[249],10, C_text("##sys#put!"));
lf[250]=C_h_intern(&lf[250],32, C_text("##compiler#local-specializations"));
lf[251]=C_h_intern(&lf[251],1, C_text("\052"));
lf[252]=C_decode_literal(C_heaptop,C_text("\376B\000\000\027invalid argument syntax"));
lf[253]=C_h_intern(&lf[253],15, C_text("##sys#globalize"));
lf[254]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\003\000\000\002\376\001\000\000\010\001variable\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000\000\000\376\003\000\000\002\376\001\000\000\001\001_\376\000\000\000\003\376\001\000\000\001\001_\376"
"\377\001\000\000\000\000\376\377\001\000\000\000\001"));
lf[255]=C_h_intern(&lf[255],6, C_text("assume"));
lf[256]=C_h_intern(&lf[256],3, C_text("the"));
lf[257]=C_h_intern(&lf[257],11, C_text("##sys#map-n"));
lf[258]=C_h_intern(&lf[258],3, C_text("let"));
lf[259]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\377\016"));
lf[260]=C_h_intern(&lf[260],1, C_text(":"));
lf[261]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016"));
lf[262]=C_h_intern(&lf[262],42, C_text("chicken.compiler.scrutinizer#validate-type"));
lf[263]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023invalid type syntax"));
lf[264]=C_h_intern(&lf[264],4, C_text("type"));
lf[265]=C_h_intern(&lf[265],9, C_text("predicate"));
lf[266]=C_h_intern(&lf[266],4, C_text("pure"));
lf[267]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\006\001symbol\376\003\000\000\002\376\001\000\000\001\001_\376\001\000\000\001\001_"));
lf[268]=C_h_intern(&lf[268],4, C_text("memv"));
lf[269]=C_h_intern(&lf[269],11, C_text("scheme#memv"));
lf[270]=C_h_intern(&lf[270],14, C_text("condition-case"));
lf[271]=C_h_intern(&lf[271],9, C_text("condition"));
lf[272]=C_h_intern(&lf[272],10, C_text("##sys#slot"));
lf[273]=C_h_intern(&lf[273],24, C_text("chicken.condition#signal"));
lf[274]=C_h_intern(&lf[274],4, C_text("cond"));
lf[275]=C_h_intern(&lf[275],17, C_text("handle-exceptions"));
lf[276]=C_h_intern(&lf[276],3, C_text("and"));
lf[277]=C_h_intern(&lf[277],4, C_text("kvar"));
lf[278]=C_h_intern(&lf[278],5, C_text("exvar"));
lf[279]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\001\000\000\001\001_"));
lf[280]=C_h_intern(&lf[280],30, C_text("call-with-current-continuation"));
lf[281]=C_h_intern(&lf[281],37, C_text("scheme#call-with-current-continuation"));
lf[282]=C_h_intern(&lf[282],40, C_text("chicken.condition#with-exception-handler"));
lf[283]=C_h_intern(&lf[283],4, C_text("args"));
lf[284]=C_h_intern(&lf[284],1, C_text("k"));
lf[285]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\010\001variable\376\003\000\000\002\376\001\000\000\001\001_\376\001\000\000\001\001_"));
C_register_lf2(lf,286,create_ptable());{}
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4218,a[2]=t1,tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_expand_toplevel(2,av2);}}

#ifdef C_ENABLE_PTABLES
static C_PTABLE_ENTRY ptable[664] = {
{C_text("f_10013:chicken_2dsyntax_2escm"),(void*)f_10013},
{C_text("f_10061:chicken_2dsyntax_2escm"),(void*)f_10061},
{C_text("f_10109:chicken_2dsyntax_2escm"),(void*)f_10109},
{C_text("f_10134:chicken_2dsyntax_2escm"),(void*)f_10134},
{C_text("f_10143:chicken_2dsyntax_2escm"),(void*)f_10143},
{C_text("f_10168:chicken_2dsyntax_2escm"),(void*)f_10168},
{C_text("f_10177:chicken_2dsyntax_2escm"),(void*)f_10177},
{C_text("f_10202:chicken_2dsyntax_2escm"),(void*)f_10202},
{C_text("f_10211:chicken_2dsyntax_2escm"),(void*)f_10211},
{C_text("f_10245:chicken_2dsyntax_2escm"),(void*)f_10245},
{C_text("f_10281:chicken_2dsyntax_2escm"),(void*)f_10281},
{C_text("f_10283:chicken_2dsyntax_2escm"),(void*)f_10283},
{C_text("f_10287:chicken_2dsyntax_2escm"),(void*)f_10287},
{C_text("f_10301:chicken_2dsyntax_2escm"),(void*)f_10301},
{C_text("f_10305:chicken_2dsyntax_2escm"),(void*)f_10305},
{C_text("f_10313:chicken_2dsyntax_2escm"),(void*)f_10313},
{C_text("f_10316:chicken_2dsyntax_2escm"),(void*)f_10316},
{C_text("f_10320:chicken_2dsyntax_2escm"),(void*)f_10320},
{C_text("f_10328:chicken_2dsyntax_2escm"),(void*)f_10328},
{C_text("f_10331:chicken_2dsyntax_2escm"),(void*)f_10331},
{C_text("f_10338:chicken_2dsyntax_2escm"),(void*)f_10338},
{C_text("f_10362:chicken_2dsyntax_2escm"),(void*)f_10362},
{C_text("f_10374:chicken_2dsyntax_2escm"),(void*)f_10374},
{C_text("f_10378:chicken_2dsyntax_2escm"),(void*)f_10378},
{C_text("f_10390:chicken_2dsyntax_2escm"),(void*)f_10390},
{C_text("f_10400:chicken_2dsyntax_2escm"),(void*)f_10400},
{C_text("f_10448:chicken_2dsyntax_2escm"),(void*)f_10448},
{C_text("f_10506:chicken_2dsyntax_2escm"),(void*)f_10506},
{C_text("f_10521:chicken_2dsyntax_2escm"),(void*)f_10521},
{C_text("f_10525:chicken_2dsyntax_2escm"),(void*)f_10525},
{C_text("f_10537:chicken_2dsyntax_2escm"),(void*)f_10537},
{C_text("f_10547:chicken_2dsyntax_2escm"),(void*)f_10547},
{C_text("f_10595:chicken_2dsyntax_2escm"),(void*)f_10595},
{C_text("f_10651:chicken_2dsyntax_2escm"),(void*)f_10651},
{C_text("f_10660:chicken_2dsyntax_2escm"),(void*)f_10660},
{C_text("f_10667:chicken_2dsyntax_2escm"),(void*)f_10667},
{C_text("f_10676:chicken_2dsyntax_2escm"),(void*)f_10676},
{C_text("f_10678:chicken_2dsyntax_2escm"),(void*)f_10678},
{C_text("f_10728:chicken_2dsyntax_2escm"),(void*)f_10728},
{C_text("f_10742:chicken_2dsyntax_2escm"),(void*)f_10742},
{C_text("f_10748:chicken_2dsyntax_2escm"),(void*)f_10748},
{C_text("f_10796:chicken_2dsyntax_2escm"),(void*)f_10796},
{C_text("f_10830:chicken_2dsyntax_2escm"),(void*)f_10830},
{C_text("f_10855:chicken_2dsyntax_2escm"),(void*)f_10855},
{C_text("f_10864:chicken_2dsyntax_2escm"),(void*)f_10864},
{C_text("f_10889:chicken_2dsyntax_2escm"),(void*)f_10889},
{C_text("f_10898:chicken_2dsyntax_2escm"),(void*)f_10898},
{C_text("f_10934:chicken_2dsyntax_2escm"),(void*)f_10934},
{C_text("f_10936:chicken_2dsyntax_2escm"),(void*)f_10936},
{C_text("f_10940:chicken_2dsyntax_2escm"),(void*)f_10940},
{C_text("f_10951:chicken_2dsyntax_2escm"),(void*)f_10951},
{C_text("f_10953:chicken_2dsyntax_2escm"),(void*)f_10953},
{C_text("f_10957:chicken_2dsyntax_2escm"),(void*)f_10957},
{C_text("f_10968:chicken_2dsyntax_2escm"),(void*)f_10968},
{C_text("f_10970:chicken_2dsyntax_2escm"),(void*)f_10970},
{C_text("f_10974:chicken_2dsyntax_2escm"),(void*)f_10974},
{C_text("f_10989:chicken_2dsyntax_2escm"),(void*)f_10989},
{C_text("f_10991:chicken_2dsyntax_2escm"),(void*)f_10991},
{C_text("f_11003:chicken_2dsyntax_2escm"),(void*)f_11003},
{C_text("f_11005:chicken_2dsyntax_2escm"),(void*)f_11005},
{C_text("f_11009:chicken_2dsyntax_2escm"),(void*)f_11009},
{C_text("f_11031:chicken_2dsyntax_2escm"),(void*)f_11031},
{C_text("f_11096:chicken_2dsyntax_2escm"),(void*)f_11096},
{C_text("f_11098:chicken_2dsyntax_2escm"),(void*)f_11098},
{C_text("f_11102:chicken_2dsyntax_2escm"),(void*)f_11102},
{C_text("f_11108:chicken_2dsyntax_2escm"),(void*)f_11108},
{C_text("f_11111:chicken_2dsyntax_2escm"),(void*)f_11111},
{C_text("f_11114:chicken_2dsyntax_2escm"),(void*)f_11114},
{C_text("f_11120:chicken_2dsyntax_2escm"),(void*)f_11120},
{C_text("f_11123:chicken_2dsyntax_2escm"),(void*)f_11123},
{C_text("f_11126:chicken_2dsyntax_2escm"),(void*)f_11126},
{C_text("f_11130:chicken_2dsyntax_2escm"),(void*)f_11130},
{C_text("f_11143:chicken_2dsyntax_2escm"),(void*)f_11143},
{C_text("f_11158:chicken_2dsyntax_2escm"),(void*)f_11158},
{C_text("f_11190:chicken_2dsyntax_2escm"),(void*)f_11190},
{C_text("f_11221:chicken_2dsyntax_2escm"),(void*)f_11221},
{C_text("f_11223:chicken_2dsyntax_2escm"),(void*)f_11223},
{C_text("f_11239:chicken_2dsyntax_2escm"),(void*)f_11239},
{C_text("f_11242:chicken_2dsyntax_2escm"),(void*)f_11242},
{C_text("f_11245:chicken_2dsyntax_2escm"),(void*)f_11245},
{C_text("f_11259:chicken_2dsyntax_2escm"),(void*)f_11259},
{C_text("f_11271:chicken_2dsyntax_2escm"),(void*)f_11271},
{C_text("f_11275:chicken_2dsyntax_2escm"),(void*)f_11275},
{C_text("f_11368:chicken_2dsyntax_2escm"),(void*)f_11368},
{C_text("f_11372:chicken_2dsyntax_2escm"),(void*)f_11372},
{C_text("f_11387:chicken_2dsyntax_2escm"),(void*)f_11387},
{C_text("f_11407:chicken_2dsyntax_2escm"),(void*)f_11407},
{C_text("f_11413:chicken_2dsyntax_2escm"),(void*)f_11413},
{C_text("f_11433:chicken_2dsyntax_2escm"),(void*)f_11433},
{C_text("f_11441:chicken_2dsyntax_2escm"),(void*)f_11441},
{C_text("f_11466:chicken_2dsyntax_2escm"),(void*)f_11466},
{C_text("f_11476:chicken_2dsyntax_2escm"),(void*)f_11476},
{C_text("f_11483:chicken_2dsyntax_2escm"),(void*)f_11483},
{C_text("f_11487:chicken_2dsyntax_2escm"),(void*)f_11487},
{C_text("f_11491:chicken_2dsyntax_2escm"),(void*)f_11491},
{C_text("f_11493:chicken_2dsyntax_2escm"),(void*)f_11493},
{C_text("f_11497:chicken_2dsyntax_2escm"),(void*)f_11497},
{C_text("f_11508:chicken_2dsyntax_2escm"),(void*)f_11508},
{C_text("f_11510:chicken_2dsyntax_2escm"),(void*)f_11510},
{C_text("f_11520:chicken_2dsyntax_2escm"),(void*)f_11520},
{C_text("f_11533:chicken_2dsyntax_2escm"),(void*)f_11533},
{C_text("f_11549:chicken_2dsyntax_2escm"),(void*)f_11549},
{C_text("f_11555:chicken_2dsyntax_2escm"),(void*)f_11555},
{C_text("f_11558:chicken_2dsyntax_2escm"),(void*)f_11558},
{C_text("f_11564:chicken_2dsyntax_2escm"),(void*)f_11564},
{C_text("f_11573:chicken_2dsyntax_2escm"),(void*)f_11573},
{C_text("f_11580:chicken_2dsyntax_2escm"),(void*)f_11580},
{C_text("f_11594:chicken_2dsyntax_2escm"),(void*)f_11594},
{C_text("f_11605:chicken_2dsyntax_2escm"),(void*)f_11605},
{C_text("f_11608:chicken_2dsyntax_2escm"),(void*)f_11608},
{C_text("f_11614:chicken_2dsyntax_2escm"),(void*)f_11614},
{C_text("f_11623:chicken_2dsyntax_2escm"),(void*)f_11623},
{C_text("f_11630:chicken_2dsyntax_2escm"),(void*)f_11630},
{C_text("f_11644:chicken_2dsyntax_2escm"),(void*)f_11644},
{C_text("f_11656:chicken_2dsyntax_2escm"),(void*)f_11656},
{C_text("f_11658:chicken_2dsyntax_2escm"),(void*)f_11658},
{C_text("f_11678:chicken_2dsyntax_2escm"),(void*)f_11678},
{C_text("f_11692:chicken_2dsyntax_2escm"),(void*)f_11692},
{C_text("f_11705:chicken_2dsyntax_2escm"),(void*)f_11705},
{C_text("f_11753:chicken_2dsyntax_2escm"),(void*)f_11753},
{C_text("f_11755:chicken_2dsyntax_2escm"),(void*)f_11755},
{C_text("f_11759:chicken_2dsyntax_2escm"),(void*)f_11759},
{C_text("f_11766:chicken_2dsyntax_2escm"),(void*)f_11766},
{C_text("f_11774:chicken_2dsyntax_2escm"),(void*)f_11774},
{C_text("f_11782:chicken_2dsyntax_2escm"),(void*)f_11782},
{C_text("f_11784:chicken_2dsyntax_2escm"),(void*)f_11784},
{C_text("f_11788:chicken_2dsyntax_2escm"),(void*)f_11788},
{C_text("f_11791:chicken_2dsyntax_2escm"),(void*)f_11791},
{C_text("f_11812:chicken_2dsyntax_2escm"),(void*)f_11812},
{C_text("f_11814:chicken_2dsyntax_2escm"),(void*)f_11814},
{C_text("f_11818:chicken_2dsyntax_2escm"),(void*)f_11818},
{C_text("f_11829:chicken_2dsyntax_2escm"),(void*)f_11829},
{C_text("f_11831:chicken_2dsyntax_2escm"),(void*)f_11831},
{C_text("f_11835:chicken_2dsyntax_2escm"),(void*)f_11835},
{C_text("f_11844:chicken_2dsyntax_2escm"),(void*)f_11844},
{C_text("f_11847:chicken_2dsyntax_2escm"),(void*)f_11847},
{C_text("f_11850:chicken_2dsyntax_2escm"),(void*)f_11850},
{C_text("f_11873:chicken_2dsyntax_2escm"),(void*)f_11873},
{C_text("f_11889:chicken_2dsyntax_2escm"),(void*)f_11889},
{C_text("f_11891:chicken_2dsyntax_2escm"),(void*)f_11891},
{C_text("f_11895:chicken_2dsyntax_2escm"),(void*)f_11895},
{C_text("f_11901:chicken_2dsyntax_2escm"),(void*)f_11901},
{C_text("f_11904:chicken_2dsyntax_2escm"),(void*)f_11904},
{C_text("f_11928:chicken_2dsyntax_2escm"),(void*)f_11928},
{C_text("f_11932:chicken_2dsyntax_2escm"),(void*)f_11932},
{C_text("f_11939:chicken_2dsyntax_2escm"),(void*)f_11939},
{C_text("f_11966:chicken_2dsyntax_2escm"),(void*)f_11966},
{C_text("f_11968:chicken_2dsyntax_2escm"),(void*)f_11968},
{C_text("f_11993:chicken_2dsyntax_2escm"),(void*)f_11993},
{C_text("f_12012:chicken_2dsyntax_2escm"),(void*)f_12012},
{C_text("f_12014:chicken_2dsyntax_2escm"),(void*)f_12014},
{C_text("f_12024:chicken_2dsyntax_2escm"),(void*)f_12024},
{C_text("f_12033:chicken_2dsyntax_2escm"),(void*)f_12033},
{C_text("f_12037:chicken_2dsyntax_2escm"),(void*)f_12037},
{C_text("f_12040:chicken_2dsyntax_2escm"),(void*)f_12040},
{C_text("f_12043:chicken_2dsyntax_2escm"),(void*)f_12043},
{C_text("f_12046:chicken_2dsyntax_2escm"),(void*)f_12046},
{C_text("f_12054:chicken_2dsyntax_2escm"),(void*)f_12054},
{C_text("f_12064:chicken_2dsyntax_2escm"),(void*)f_12064},
{C_text("f_12067:chicken_2dsyntax_2escm"),(void*)f_12067},
{C_text("f_12073:chicken_2dsyntax_2escm"),(void*)f_12073},
{C_text("f_12108:chicken_2dsyntax_2escm"),(void*)f_12108},
{C_text("f_12110:chicken_2dsyntax_2escm"),(void*)f_12110},
{C_text("f_12168:chicken_2dsyntax_2escm"),(void*)f_12168},
{C_text("f_12172:chicken_2dsyntax_2escm"),(void*)f_12172},
{C_text("f_12205:chicken_2dsyntax_2escm"),(void*)f_12205},
{C_text("f_12207:chicken_2dsyntax_2escm"),(void*)f_12207},
{C_text("f_12232:chicken_2dsyntax_2escm"),(void*)f_12232},
{C_text("f_12249:chicken_2dsyntax_2escm"),(void*)f_12249},
{C_text("f_12251:chicken_2dsyntax_2escm"),(void*)f_12251},
{C_text("f_12269:chicken_2dsyntax_2escm"),(void*)f_12269},
{C_text("f_12305:chicken_2dsyntax_2escm"),(void*)f_12305},
{C_text("f_12322:chicken_2dsyntax_2escm"),(void*)f_12322},
{C_text("f_12379:chicken_2dsyntax_2escm"),(void*)f_12379},
{C_text("f_12381:chicken_2dsyntax_2escm"),(void*)f_12381},
{C_text("f_12391:chicken_2dsyntax_2escm"),(void*)f_12391},
{C_text("f_12408:chicken_2dsyntax_2escm"),(void*)f_12408},
{C_text("f_12429:chicken_2dsyntax_2escm"),(void*)f_12429},
{C_text("f_12439:chicken_2dsyntax_2escm"),(void*)f_12439},
{C_text("f_12447:chicken_2dsyntax_2escm"),(void*)f_12447},
{C_text("f_12449:chicken_2dsyntax_2escm"),(void*)f_12449},
{C_text("f_12465:chicken_2dsyntax_2escm"),(void*)f_12465},
{C_text("f_12475:chicken_2dsyntax_2escm"),(void*)f_12475},
{C_text("f_12509:chicken_2dsyntax_2escm"),(void*)f_12509},
{C_text("f_12553:chicken_2dsyntax_2escm"),(void*)f_12553},
{C_text("f_12559:chicken_2dsyntax_2escm"),(void*)f_12559},
{C_text("f_12562:chicken_2dsyntax_2escm"),(void*)f_12562},
{C_text("f_12568:chicken_2dsyntax_2escm"),(void*)f_12568},
{C_text("f_12577:chicken_2dsyntax_2escm"),(void*)f_12577},
{C_text("f_12584:chicken_2dsyntax_2escm"),(void*)f_12584},
{C_text("f_12598:chicken_2dsyntax_2escm"),(void*)f_12598},
{C_text("f_12609:chicken_2dsyntax_2escm"),(void*)f_12609},
{C_text("f_12612:chicken_2dsyntax_2escm"),(void*)f_12612},
{C_text("f_12618:chicken_2dsyntax_2escm"),(void*)f_12618},
{C_text("f_12627:chicken_2dsyntax_2escm"),(void*)f_12627},
{C_text("f_12634:chicken_2dsyntax_2escm"),(void*)f_12634},
{C_text("f_12646:chicken_2dsyntax_2escm"),(void*)f_12646},
{C_text("f_12657:chicken_2dsyntax_2escm"),(void*)f_12657},
{C_text("f_12688:chicken_2dsyntax_2escm"),(void*)f_12688},
{C_text("f_12690:chicken_2dsyntax_2escm"),(void*)f_12690},
{C_text("f_12694:chicken_2dsyntax_2escm"),(void*)f_12694},
{C_text("f_12710:chicken_2dsyntax_2escm"),(void*)f_12710},
{C_text("f_12726:chicken_2dsyntax_2escm"),(void*)f_12726},
{C_text("f_12728:chicken_2dsyntax_2escm"),(void*)f_12728},
{C_text("f_12732:chicken_2dsyntax_2escm"),(void*)f_12732},
{C_text("f_12741:chicken_2dsyntax_2escm"),(void*)f_12741},
{C_text("f_12748:chicken_2dsyntax_2escm"),(void*)f_12748},
{C_text("f_12756:chicken_2dsyntax_2escm"),(void*)f_12756},
{C_text("f_12758:chicken_2dsyntax_2escm"),(void*)f_12758},
{C_text("f_12783:chicken_2dsyntax_2escm"),(void*)f_12783},
{C_text("f_12787:chicken_2dsyntax_2escm"),(void*)f_12787},
{C_text("f_12837:chicken_2dsyntax_2escm"),(void*)f_12837},
{C_text("f_12839:chicken_2dsyntax_2escm"),(void*)f_12839},
{C_text("f_12843:chicken_2dsyntax_2escm"),(void*)f_12843},
{C_text("f_12846:chicken_2dsyntax_2escm"),(void*)f_12846},
{C_text("f_12849:chicken_2dsyntax_2escm"),(void*)f_12849},
{C_text("f_12852:chicken_2dsyntax_2escm"),(void*)f_12852},
{C_text("f_12855:chicken_2dsyntax_2escm"),(void*)f_12855},
{C_text("f_12858:chicken_2dsyntax_2escm"),(void*)f_12858},
{C_text("f_12860:chicken_2dsyntax_2escm"),(void*)f_12860},
{C_text("f_12870:chicken_2dsyntax_2escm"),(void*)f_12870},
{C_text("f_12895:chicken_2dsyntax_2escm"),(void*)f_12895},
{C_text("f_12909:chicken_2dsyntax_2escm"),(void*)f_12909},
{C_text("f_12911:chicken_2dsyntax_2escm"),(void*)f_12911},
{C_text("f_12990:chicken_2dsyntax_2escm"),(void*)f_12990},
{C_text("f_13015:chicken_2dsyntax_2escm"),(void*)f_13015},
{C_text("f_13022:chicken_2dsyntax_2escm"),(void*)f_13022},
{C_text("f_13026:chicken_2dsyntax_2escm"),(void*)f_13026},
{C_text("f_13046:chicken_2dsyntax_2escm"),(void*)f_13046},
{C_text("f_13071:chicken_2dsyntax_2escm"),(void*)f_13071},
{C_text("f_13110:chicken_2dsyntax_2escm"),(void*)f_13110},
{C_text("f_13112:chicken_2dsyntax_2escm"),(void*)f_13112},
{C_text("f_13116:chicken_2dsyntax_2escm"),(void*)f_13116},
{C_text("f_13119:chicken_2dsyntax_2escm"),(void*)f_13119},
{C_text("f_13122:chicken_2dsyntax_2escm"),(void*)f_13122},
{C_text("f_13133:chicken_2dsyntax_2escm"),(void*)f_13133},
{C_text("f_4218:chicken_2dsyntax_2escm"),(void*)f_4218},
{C_text("f_4221:chicken_2dsyntax_2escm"),(void*)f_4221},
{C_text("f_4224:chicken_2dsyntax_2escm"),(void*)f_4224},
{C_text("f_4334:chicken_2dsyntax_2escm"),(void*)f_4334},
{C_text("f_4352:chicken_2dsyntax_2escm"),(void*)f_4352},
{C_text("f_4392:chicken_2dsyntax_2escm"),(void*)f_4392},
{C_text("f_4406:chicken_2dsyntax_2escm"),(void*)f_4406},
{C_text("f_5506:chicken_2dsyntax_2escm"),(void*)f_5506},
{C_text("f_5509:chicken_2dsyntax_2escm"),(void*)f_5509},
{C_text("f_5512:chicken_2dsyntax_2escm"),(void*)f_5512},
{C_text("f_5515:chicken_2dsyntax_2escm"),(void*)f_5515},
{C_text("f_5519:chicken_2dsyntax_2escm"),(void*)f_5519},
{C_text("f_5522:chicken_2dsyntax_2escm"),(void*)f_5522},
{C_text("f_5525:chicken_2dsyntax_2escm"),(void*)f_5525},
{C_text("f_5528:chicken_2dsyntax_2escm"),(void*)f_5528},
{C_text("f_5531:chicken_2dsyntax_2escm"),(void*)f_5531},
{C_text("f_5534:chicken_2dsyntax_2escm"),(void*)f_5534},
{C_text("f_5537:chicken_2dsyntax_2escm"),(void*)f_5537},
{C_text("f_5540:chicken_2dsyntax_2escm"),(void*)f_5540},
{C_text("f_5544:chicken_2dsyntax_2escm"),(void*)f_5544},
{C_text("f_5547:chicken_2dsyntax_2escm"),(void*)f_5547},
{C_text("f_5550:chicken_2dsyntax_2escm"),(void*)f_5550},
{C_text("f_5553:chicken_2dsyntax_2escm"),(void*)f_5553},
{C_text("f_5556:chicken_2dsyntax_2escm"),(void*)f_5556},
{C_text("f_5559:chicken_2dsyntax_2escm"),(void*)f_5559},
{C_text("f_5562:chicken_2dsyntax_2escm"),(void*)f_5562},
{C_text("f_5566:chicken_2dsyntax_2escm"),(void*)f_5566},
{C_text("f_5569:chicken_2dsyntax_2escm"),(void*)f_5569},
{C_text("f_5572:chicken_2dsyntax_2escm"),(void*)f_5572},
{C_text("f_5575:chicken_2dsyntax_2escm"),(void*)f_5575},
{C_text("f_5578:chicken_2dsyntax_2escm"),(void*)f_5578},
{C_text("f_5581:chicken_2dsyntax_2escm"),(void*)f_5581},
{C_text("f_5584:chicken_2dsyntax_2escm"),(void*)f_5584},
{C_text("f_5587:chicken_2dsyntax_2escm"),(void*)f_5587},
{C_text("f_5590:chicken_2dsyntax_2escm"),(void*)f_5590},
{C_text("f_5593:chicken_2dsyntax_2escm"),(void*)f_5593},
{C_text("f_5596:chicken_2dsyntax_2escm"),(void*)f_5596},
{C_text("f_5599:chicken_2dsyntax_2escm"),(void*)f_5599},
{C_text("f_5602:chicken_2dsyntax_2escm"),(void*)f_5602},
{C_text("f_5605:chicken_2dsyntax_2escm"),(void*)f_5605},
{C_text("f_5609:chicken_2dsyntax_2escm"),(void*)f_5609},
{C_text("f_5612:chicken_2dsyntax_2escm"),(void*)f_5612},
{C_text("f_5615:chicken_2dsyntax_2escm"),(void*)f_5615},
{C_text("f_5618:chicken_2dsyntax_2escm"),(void*)f_5618},
{C_text("f_5621:chicken_2dsyntax_2escm"),(void*)f_5621},
{C_text("f_5624:chicken_2dsyntax_2escm"),(void*)f_5624},
{C_text("f_5627:chicken_2dsyntax_2escm"),(void*)f_5627},
{C_text("f_5630:chicken_2dsyntax_2escm"),(void*)f_5630},
{C_text("f_5633:chicken_2dsyntax_2escm"),(void*)f_5633},
{C_text("f_5636:chicken_2dsyntax_2escm"),(void*)f_5636},
{C_text("f_5639:chicken_2dsyntax_2escm"),(void*)f_5639},
{C_text("f_5642:chicken_2dsyntax_2escm"),(void*)f_5642},
{C_text("f_5645:chicken_2dsyntax_2escm"),(void*)f_5645},
{C_text("f_5648:chicken_2dsyntax_2escm"),(void*)f_5648},
{C_text("f_5651:chicken_2dsyntax_2escm"),(void*)f_5651},
{C_text("f_5654:chicken_2dsyntax_2escm"),(void*)f_5654},
{C_text("f_5657:chicken_2dsyntax_2escm"),(void*)f_5657},
{C_text("f_5660:chicken_2dsyntax_2escm"),(void*)f_5660},
{C_text("f_5663:chicken_2dsyntax_2escm"),(void*)f_5663},
{C_text("f_5666:chicken_2dsyntax_2escm"),(void*)f_5666},
{C_text("f_5670:chicken_2dsyntax_2escm"),(void*)f_5670},
{C_text("f_5673:chicken_2dsyntax_2escm"),(void*)f_5673},
{C_text("f_5676:chicken_2dsyntax_2escm"),(void*)f_5676},
{C_text("f_5680:chicken_2dsyntax_2escm"),(void*)f_5680},
{C_text("f_5683:chicken_2dsyntax_2escm"),(void*)f_5683},
{C_text("f_5686:chicken_2dsyntax_2escm"),(void*)f_5686},
{C_text("f_5690:chicken_2dsyntax_2escm"),(void*)f_5690},
{C_text("f_5694:chicken_2dsyntax_2escm"),(void*)f_5694},
{C_text("f_5696:chicken_2dsyntax_2escm"),(void*)f_5696},
{C_text("f_5700:chicken_2dsyntax_2escm"),(void*)f_5700},
{C_text("f_5743:chicken_2dsyntax_2escm"),(void*)f_5743},
{C_text("f_5745:chicken_2dsyntax_2escm"),(void*)f_5745},
{C_text("f_5749:chicken_2dsyntax_2escm"),(void*)f_5749},
{C_text("f_5760:chicken_2dsyntax_2escm"),(void*)f_5760},
{C_text("f_5763:chicken_2dsyntax_2escm"),(void*)f_5763},
{C_text("f_5786:chicken_2dsyntax_2escm"),(void*)f_5786},
{C_text("f_5805:chicken_2dsyntax_2escm"),(void*)f_5805},
{C_text("f_5815:chicken_2dsyntax_2escm"),(void*)f_5815},
{C_text("f_5822:chicken_2dsyntax_2escm"),(void*)f_5822},
{C_text("f_5832:chicken_2dsyntax_2escm"),(void*)f_5832},
{C_text("f_5834:chicken_2dsyntax_2escm"),(void*)f_5834},
{C_text("f_5842:chicken_2dsyntax_2escm"),(void*)f_5842},
{C_text("f_5850:chicken_2dsyntax_2escm"),(void*)f_5850},
{C_text("f_5852:chicken_2dsyntax_2escm"),(void*)f_5852},
{C_text("f_5856:chicken_2dsyntax_2escm"),(void*)f_5856},
{C_text("f_5915:chicken_2dsyntax_2escm"),(void*)f_5915},
{C_text("f_5917:chicken_2dsyntax_2escm"),(void*)f_5917},
{C_text("f_5921:chicken_2dsyntax_2escm"),(void*)f_5921},
{C_text("f_5924:chicken_2dsyntax_2escm"),(void*)f_5924},
{C_text("f_5927:chicken_2dsyntax_2escm"),(void*)f_5927},
{C_text("f_5930:chicken_2dsyntax_2escm"),(void*)f_5930},
{C_text("f_5937:chicken_2dsyntax_2escm"),(void*)f_5937},
{C_text("f_5947:chicken_2dsyntax_2escm"),(void*)f_5947},
{C_text("f_5950:chicken_2dsyntax_2escm"),(void*)f_5950},
{C_text("f_5956:chicken_2dsyntax_2escm"),(void*)f_5956},
{C_text("f_5967:chicken_2dsyntax_2escm"),(void*)f_5967},
{C_text("f_5983:chicken_2dsyntax_2escm"),(void*)f_5983},
{C_text("f_5993:chicken_2dsyntax_2escm"),(void*)f_5993},
{C_text("f_6016:chicken_2dsyntax_2escm"),(void*)f_6016},
{C_text("f_6019:chicken_2dsyntax_2escm"),(void*)f_6019},
{C_text("f_6036:chicken_2dsyntax_2escm"),(void*)f_6036},
{C_text("f_6042:chicken_2dsyntax_2escm"),(void*)f_6042},
{C_text("f_6059:chicken_2dsyntax_2escm"),(void*)f_6059},
{C_text("f_6082:chicken_2dsyntax_2escm"),(void*)f_6082},
{C_text("f_6110:chicken_2dsyntax_2escm"),(void*)f_6110},
{C_text("f_6112:chicken_2dsyntax_2escm"),(void*)f_6112},
{C_text("f_6116:chicken_2dsyntax_2escm"),(void*)f_6116},
{C_text("f_6119:chicken_2dsyntax_2escm"),(void*)f_6119},
{C_text("f_6122:chicken_2dsyntax_2escm"),(void*)f_6122},
{C_text("f_6125:chicken_2dsyntax_2escm"),(void*)f_6125},
{C_text("f_6132:chicken_2dsyntax_2escm"),(void*)f_6132},
{C_text("f_6142:chicken_2dsyntax_2escm"),(void*)f_6142},
{C_text("f_6145:chicken_2dsyntax_2escm"),(void*)f_6145},
{C_text("f_6151:chicken_2dsyntax_2escm"),(void*)f_6151},
{C_text("f_6158:chicken_2dsyntax_2escm"),(void*)f_6158},
{C_text("f_6174:chicken_2dsyntax_2escm"),(void*)f_6174},
{C_text("f_6184:chicken_2dsyntax_2escm"),(void*)f_6184},
{C_text("f_6207:chicken_2dsyntax_2escm"),(void*)f_6207},
{C_text("f_6210:chicken_2dsyntax_2escm"),(void*)f_6210},
{C_text("f_6227:chicken_2dsyntax_2escm"),(void*)f_6227},
{C_text("f_6233:chicken_2dsyntax_2escm"),(void*)f_6233},
{C_text("f_6282:chicken_2dsyntax_2escm"),(void*)f_6282},
{C_text("f_6284:chicken_2dsyntax_2escm"),(void*)f_6284},
{C_text("f_6288:chicken_2dsyntax_2escm"),(void*)f_6288},
{C_text("f_6294:chicken_2dsyntax_2escm"),(void*)f_6294},
{C_text("f_6297:chicken_2dsyntax_2escm"),(void*)f_6297},
{C_text("f_6309:chicken_2dsyntax_2escm"),(void*)f_6309},
{C_text("f_6312:chicken_2dsyntax_2escm"),(void*)f_6312},
{C_text("f_6318:chicken_2dsyntax_2escm"),(void*)f_6318},
{C_text("f_6321:chicken_2dsyntax_2escm"),(void*)f_6321},
{C_text("f_6330:chicken_2dsyntax_2escm"),(void*)f_6330},
{C_text("f_6331:chicken_2dsyntax_2escm"),(void*)f_6331},
{C_text("f_6347:chicken_2dsyntax_2escm"),(void*)f_6347},
{C_text("f_6378:chicken_2dsyntax_2escm"),(void*)f_6378},
{C_text("f_6380:chicken_2dsyntax_2escm"),(void*)f_6380},
{C_text("f_6399:chicken_2dsyntax_2escm"),(void*)f_6399},
{C_text("f_6405:chicken_2dsyntax_2escm"),(void*)f_6405},
{C_text("f_6412:chicken_2dsyntax_2escm"),(void*)f_6412},
{C_text("f_6416:chicken_2dsyntax_2escm"),(void*)f_6416},
{C_text("f_6420:chicken_2dsyntax_2escm"),(void*)f_6420},
{C_text("f_6424:chicken_2dsyntax_2escm"),(void*)f_6424},
{C_text("f_6452:chicken_2dsyntax_2escm"),(void*)f_6452},
{C_text("f_6473:chicken_2dsyntax_2escm"),(void*)f_6473},
{C_text("f_6552:chicken_2dsyntax_2escm"),(void*)f_6552},
{C_text("f_6595:chicken_2dsyntax_2escm"),(void*)f_6595},
{C_text("f_6608:chicken_2dsyntax_2escm"),(void*)f_6608},
{C_text("f_6610:chicken_2dsyntax_2escm"),(void*)f_6610},
{C_text("f_6648:chicken_2dsyntax_2escm"),(void*)f_6648},
{C_text("f_6658:chicken_2dsyntax_2escm"),(void*)f_6658},
{C_text("f_6671:chicken_2dsyntax_2escm"),(void*)f_6671},
{C_text("f_6706:chicken_2dsyntax_2escm"),(void*)f_6706},
{C_text("f_6713:chicken_2dsyntax_2escm"),(void*)f_6713},
{C_text("f_6717:chicken_2dsyntax_2escm"),(void*)f_6717},
{C_text("f_6721:chicken_2dsyntax_2escm"),(void*)f_6721},
{C_text("f_6723:chicken_2dsyntax_2escm"),(void*)f_6723},
{C_text("f_6727:chicken_2dsyntax_2escm"),(void*)f_6727},
{C_text("f_6741:chicken_2dsyntax_2escm"),(void*)f_6741},
{C_text("f_6744:chicken_2dsyntax_2escm"),(void*)f_6744},
{C_text("f_6747:chicken_2dsyntax_2escm"),(void*)f_6747},
{C_text("f_6769:chicken_2dsyntax_2escm"),(void*)f_6769},
{C_text("f_6776:chicken_2dsyntax_2escm"),(void*)f_6776},
{C_text("f_6780:chicken_2dsyntax_2escm"),(void*)f_6780},
{C_text("f_6791:chicken_2dsyntax_2escm"),(void*)f_6791},
{C_text("f_6794:chicken_2dsyntax_2escm"),(void*)f_6794},
{C_text("f_6797:chicken_2dsyntax_2escm"),(void*)f_6797},
{C_text("f_6811:chicken_2dsyntax_2escm"),(void*)f_6811},
{C_text("f_6818:chicken_2dsyntax_2escm"),(void*)f_6818},
{C_text("f_6822:chicken_2dsyntax_2escm"),(void*)f_6822},
{C_text("f_6834:chicken_2dsyntax_2escm"),(void*)f_6834},
{C_text("f_6836:chicken_2dsyntax_2escm"),(void*)f_6836},
{C_text("f_6840:chicken_2dsyntax_2escm"),(void*)f_6840},
{C_text("f_6842:chicken_2dsyntax_2escm"),(void*)f_6842},
{C_text("f_6848:chicken_2dsyntax_2escm"),(void*)f_6848},
{C_text("f_6862:chicken_2dsyntax_2escm"),(void*)f_6862},
{C_text("f_6866:chicken_2dsyntax_2escm"),(void*)f_6866},
{C_text("f_6874:chicken_2dsyntax_2escm"),(void*)f_6874},
{C_text("f_6877:chicken_2dsyntax_2escm"),(void*)f_6877},
{C_text("f_6880:chicken_2dsyntax_2escm"),(void*)f_6880},
{C_text("f_6883:chicken_2dsyntax_2escm"),(void*)f_6883},
{C_text("f_6886:chicken_2dsyntax_2escm"),(void*)f_6886},
{C_text("f_6889:chicken_2dsyntax_2escm"),(void*)f_6889},
{C_text("f_6892:chicken_2dsyntax_2escm"),(void*)f_6892},
{C_text("f_6895:chicken_2dsyntax_2escm"),(void*)f_6895},
{C_text("f_6898:chicken_2dsyntax_2escm"),(void*)f_6898},
{C_text("f_6901:chicken_2dsyntax_2escm"),(void*)f_6901},
{C_text("f_6908:chicken_2dsyntax_2escm"),(void*)f_6908},
{C_text("f_6924:chicken_2dsyntax_2escm"),(void*)f_6924},
{C_text("f_6926:chicken_2dsyntax_2escm"),(void*)f_6926},
{C_text("f_6934:chicken_2dsyntax_2escm"),(void*)f_6934},
{C_text("f_6944:chicken_2dsyntax_2escm"),(void*)f_6944},
{C_text("f_6948:chicken_2dsyntax_2escm"),(void*)f_6948},
{C_text("f_6958:chicken_2dsyntax_2escm"),(void*)f_6958},
{C_text("f_6962:chicken_2dsyntax_2escm"),(void*)f_6962},
{C_text("f_6964:chicken_2dsyntax_2escm"),(void*)f_6964},
{C_text("f_6972:chicken_2dsyntax_2escm"),(void*)f_6972},
{C_text("f_6974:chicken_2dsyntax_2escm"),(void*)f_6974},
{C_text("f_6978:chicken_2dsyntax_2escm"),(void*)f_6978},
{C_text("f_7000:chicken_2dsyntax_2escm"),(void*)f_7000},
{C_text("f_7002:chicken_2dsyntax_2escm"),(void*)f_7002},
{C_text("f_7050:chicken_2dsyntax_2escm"),(void*)f_7050},
{C_text("f_7103:chicken_2dsyntax_2escm"),(void*)f_7103},
{C_text("f_7114:chicken_2dsyntax_2escm"),(void*)f_7114},
{C_text("f_7151:chicken_2dsyntax_2escm"),(void*)f_7151},
{C_text("f_7179:chicken_2dsyntax_2escm"),(void*)f_7179},
{C_text("f_7206:chicken_2dsyntax_2escm"),(void*)f_7206},
{C_text("f_7216:chicken_2dsyntax_2escm"),(void*)f_7216},
{C_text("f_7218:chicken_2dsyntax_2escm"),(void*)f_7218},
{C_text("f_7243:chicken_2dsyntax_2escm"),(void*)f_7243},
{C_text("f_7278:chicken_2dsyntax_2escm"),(void*)f_7278},
{C_text("f_7280:chicken_2dsyntax_2escm"),(void*)f_7280},
{C_text("f_7284:chicken_2dsyntax_2escm"),(void*)f_7284},
{C_text("f_7296:chicken_2dsyntax_2escm"),(void*)f_7296},
{C_text("f_7299:chicken_2dsyntax_2escm"),(void*)f_7299},
{C_text("f_7302:chicken_2dsyntax_2escm"),(void*)f_7302},
{C_text("f_7305:chicken_2dsyntax_2escm"),(void*)f_7305},
{C_text("f_7316:chicken_2dsyntax_2escm"),(void*)f_7316},
{C_text("f_7318:chicken_2dsyntax_2escm"),(void*)f_7318},
{C_text("f_7344:chicken_2dsyntax_2escm"),(void*)f_7344},
{C_text("f_7355:chicken_2dsyntax_2escm"),(void*)f_7355},
{C_text("f_7438:chicken_2dsyntax_2escm"),(void*)f_7438},
{C_text("f_7440:chicken_2dsyntax_2escm"),(void*)f_7440},
{C_text("f_7444:chicken_2dsyntax_2escm"),(void*)f_7444},
{C_text("f_7447:chicken_2dsyntax_2escm"),(void*)f_7447},
{C_text("f_7477:chicken_2dsyntax_2escm"),(void*)f_7477},
{C_text("f_7487:chicken_2dsyntax_2escm"),(void*)f_7487},
{C_text("f_7515:chicken_2dsyntax_2escm"),(void*)f_7515},
{C_text("f_7517:chicken_2dsyntax_2escm"),(void*)f_7517},
{C_text("f_7521:chicken_2dsyntax_2escm"),(void*)f_7521},
{C_text("f_7540:chicken_2dsyntax_2escm"),(void*)f_7540},
{C_text("f_7544:chicken_2dsyntax_2escm"),(void*)f_7544},
{C_text("f_7548:chicken_2dsyntax_2escm"),(void*)f_7548},
{C_text("f_7550:chicken_2dsyntax_2escm"),(void*)f_7550},
{C_text("f_7571:chicken_2dsyntax_2escm"),(void*)f_7571},
{C_text("f_7589:chicken_2dsyntax_2escm"),(void*)f_7589},
{C_text("f_7597:chicken_2dsyntax_2escm"),(void*)f_7597},
{C_text("f_7601:chicken_2dsyntax_2escm"),(void*)f_7601},
{C_text("f_7611:chicken_2dsyntax_2escm"),(void*)f_7611},
{C_text("f_7617:chicken_2dsyntax_2escm"),(void*)f_7617},
{C_text("f_7631:chicken_2dsyntax_2escm"),(void*)f_7631},
{C_text("f_7657:chicken_2dsyntax_2escm"),(void*)f_7657},
{C_text("f_7681:chicken_2dsyntax_2escm"),(void*)f_7681},
{C_text("f_7689:chicken_2dsyntax_2escm"),(void*)f_7689},
{C_text("f_7697:chicken_2dsyntax_2escm"),(void*)f_7697},
{C_text("f_7701:chicken_2dsyntax_2escm"),(void*)f_7701},
{C_text("f_7704:chicken_2dsyntax_2escm"),(void*)f_7704},
{C_text("f_7707:chicken_2dsyntax_2escm"),(void*)f_7707},
{C_text("f_7716:chicken_2dsyntax_2escm"),(void*)f_7716},
{C_text("f_7717:chicken_2dsyntax_2escm"),(void*)f_7717},
{C_text("f_7725:chicken_2dsyntax_2escm"),(void*)f_7725},
{C_text("f_7729:chicken_2dsyntax_2escm"),(void*)f_7729},
{C_text("f_7733:chicken_2dsyntax_2escm"),(void*)f_7733},
{C_text("f_7741:chicken_2dsyntax_2escm"),(void*)f_7741},
{C_text("f_7747:chicken_2dsyntax_2escm"),(void*)f_7747},
{C_text("f_7753:chicken_2dsyntax_2escm"),(void*)f_7753},
{C_text("f_7756:chicken_2dsyntax_2escm"),(void*)f_7756},
{C_text("f_7759:chicken_2dsyntax_2escm"),(void*)f_7759},
{C_text("f_7763:chicken_2dsyntax_2escm"),(void*)f_7763},
{C_text("f_7771:chicken_2dsyntax_2escm"),(void*)f_7771},
{C_text("f_7774:chicken_2dsyntax_2escm"),(void*)f_7774},
{C_text("f_7777:chicken_2dsyntax_2escm"),(void*)f_7777},
{C_text("f_7780:chicken_2dsyntax_2escm"),(void*)f_7780},
{C_text("f_7787:chicken_2dsyntax_2escm"),(void*)f_7787},
{C_text("f_7813:chicken_2dsyntax_2escm"),(void*)f_7813},
{C_text("f_7838:chicken_2dsyntax_2escm"),(void*)f_7838},
{C_text("f_7847:chicken_2dsyntax_2escm"),(void*)f_7847},
{C_text("f_7881:chicken_2dsyntax_2escm"),(void*)f_7881},
{C_text("f_7906:chicken_2dsyntax_2escm"),(void*)f_7906},
{C_text("f_7915:chicken_2dsyntax_2escm"),(void*)f_7915},
{C_text("f_7963:chicken_2dsyntax_2escm"),(void*)f_7963},
{C_text("f_7965:chicken_2dsyntax_2escm"),(void*)f_7965},
{C_text("f_7969:chicken_2dsyntax_2escm"),(void*)f_7969},
{C_text("f_7979:chicken_2dsyntax_2escm"),(void*)f_7979},
{C_text("f_8006:chicken_2dsyntax_2escm"),(void*)f_8006},
{C_text("f_8009:chicken_2dsyntax_2escm"),(void*)f_8009},
{C_text("f_8024:chicken_2dsyntax_2escm"),(void*)f_8024},
{C_text("f_8032:chicken_2dsyntax_2escm"),(void*)f_8032},
{C_text("f_8041:chicken_2dsyntax_2escm"),(void*)f_8041},
{C_text("f_8056:chicken_2dsyntax_2escm"),(void*)f_8056},
{C_text("f_8066:chicken_2dsyntax_2escm"),(void*)f_8066},
{C_text("f_8069:chicken_2dsyntax_2escm"),(void*)f_8069},
{C_text("f_8085:chicken_2dsyntax_2escm"),(void*)f_8085},
{C_text("f_8105:chicken_2dsyntax_2escm"),(void*)f_8105},
{C_text("f_8107:chicken_2dsyntax_2escm"),(void*)f_8107},
{C_text("f_8109:chicken_2dsyntax_2escm"),(void*)f_8109},
{C_text("f_8113:chicken_2dsyntax_2escm"),(void*)f_8113},
{C_text("f_8122:chicken_2dsyntax_2escm"),(void*)f_8122},
{C_text("f_8125:chicken_2dsyntax_2escm"),(void*)f_8125},
{C_text("f_8134:chicken_2dsyntax_2escm"),(void*)f_8134},
{C_text("f_8150:chicken_2dsyntax_2escm"),(void*)f_8150},
{C_text("f_8154:chicken_2dsyntax_2escm"),(void*)f_8154},
{C_text("f_8197:chicken_2dsyntax_2escm"),(void*)f_8197},
{C_text("f_8209:chicken_2dsyntax_2escm"),(void*)f_8209},
{C_text("f_8211:chicken_2dsyntax_2escm"),(void*)f_8211},
{C_text("f_8215:chicken_2dsyntax_2escm"),(void*)f_8215},
{C_text("f_8218:chicken_2dsyntax_2escm"),(void*)f_8218},
{C_text("f_8237:chicken_2dsyntax_2escm"),(void*)f_8237},
{C_text("f_8253:chicken_2dsyntax_2escm"),(void*)f_8253},
{C_text("f_8255:chicken_2dsyntax_2escm"),(void*)f_8255},
{C_text("f_8259:chicken_2dsyntax_2escm"),(void*)f_8259},
{C_text("f_8262:chicken_2dsyntax_2escm"),(void*)f_8262},
{C_text("f_8275:chicken_2dsyntax_2escm"),(void*)f_8275},
{C_text("f_8277:chicken_2dsyntax_2escm"),(void*)f_8277},
{C_text("f_8281:chicken_2dsyntax_2escm"),(void*)f_8281},
{C_text("f_8295:chicken_2dsyntax_2escm"),(void*)f_8295},
{C_text("f_8301:chicken_2dsyntax_2escm"),(void*)f_8301},
{C_text("f_8323:chicken_2dsyntax_2escm"),(void*)f_8323},
{C_text("f_8329:chicken_2dsyntax_2escm"),(void*)f_8329},
{C_text("f_8333:chicken_2dsyntax_2escm"),(void*)f_8333},
{C_text("f_8343:chicken_2dsyntax_2escm"),(void*)f_8343},
{C_text("f_8345:chicken_2dsyntax_2escm"),(void*)f_8345},
{C_text("f_8374:chicken_2dsyntax_2escm"),(void*)f_8374},
{C_text("f_8393:chicken_2dsyntax_2escm"),(void*)f_8393},
{C_text("f_8427:chicken_2dsyntax_2escm"),(void*)f_8427},
{C_text("f_8451:chicken_2dsyntax_2escm"),(void*)f_8451},
{C_text("f_8453:chicken_2dsyntax_2escm"),(void*)f_8453},
{C_text("f_8457:chicken_2dsyntax_2escm"),(void*)f_8457},
{C_text("f_8463:chicken_2dsyntax_2escm"),(void*)f_8463},
{C_text("f_8497:chicken_2dsyntax_2escm"),(void*)f_8497},
{C_text("f_8533:chicken_2dsyntax_2escm"),(void*)f_8533},
{C_text("f_8535:chicken_2dsyntax_2escm"),(void*)f_8535},
{C_text("f_8539:chicken_2dsyntax_2escm"),(void*)f_8539},
{C_text("f_8547:chicken_2dsyntax_2escm"),(void*)f_8547},
{C_text("f_8552:chicken_2dsyntax_2escm"),(void*)f_8552},
{C_text("f_8577:chicken_2dsyntax_2escm"),(void*)f_8577},
{C_text("f_8587:chicken_2dsyntax_2escm"),(void*)f_8587},
{C_text("f_8589:chicken_2dsyntax_2escm"),(void*)f_8589},
{C_text("f_8593:chicken_2dsyntax_2escm"),(void*)f_8593},
{C_text("f_8599:chicken_2dsyntax_2escm"),(void*)f_8599},
{C_text("f_8620:chicken_2dsyntax_2escm"),(void*)f_8620},
{C_text("f_8627:chicken_2dsyntax_2escm"),(void*)f_8627},
{C_text("f_8650:chicken_2dsyntax_2escm"),(void*)f_8650},
{C_text("f_8654:chicken_2dsyntax_2escm"),(void*)f_8654},
{C_text("f_8675:chicken_2dsyntax_2escm"),(void*)f_8675},
{C_text("f_8678:chicken_2dsyntax_2escm"),(void*)f_8678},
{C_text("f_8682:chicken_2dsyntax_2escm"),(void*)f_8682},
{C_text("f_8690:chicken_2dsyntax_2escm"),(void*)f_8690},
{C_text("f_8694:chicken_2dsyntax_2escm"),(void*)f_8694},
{C_text("f_8700:chicken_2dsyntax_2escm"),(void*)f_8700},
{C_text("f_8701:chicken_2dsyntax_2escm"),(void*)f_8701},
{C_text("f_8712:chicken_2dsyntax_2escm"),(void*)f_8712},
{C_text("f_8727:chicken_2dsyntax_2escm"),(void*)f_8727},
{C_text("f_8729:chicken_2dsyntax_2escm"),(void*)f_8729},
{C_text("f_8748:chicken_2dsyntax_2escm"),(void*)f_8748},
{C_text("f_8756:chicken_2dsyntax_2escm"),(void*)f_8756},
{C_text("f_8762:chicken_2dsyntax_2escm"),(void*)f_8762},
{C_text("f_8764:chicken_2dsyntax_2escm"),(void*)f_8764},
{C_text("f_8789:chicken_2dsyntax_2escm"),(void*)f_8789},
{C_text("f_8813:chicken_2dsyntax_2escm"),(void*)f_8813},
{C_text("f_8850:chicken_2dsyntax_2escm"),(void*)f_8850},
{C_text("f_8878:chicken_2dsyntax_2escm"),(void*)f_8878},
{C_text("f_8912:chicken_2dsyntax_2escm"),(void*)f_8912},
{C_text("f_8943:chicken_2dsyntax_2escm"),(void*)f_8943},
{C_text("f_8950:chicken_2dsyntax_2escm"),(void*)f_8950},
{C_text("f_8956:chicken_2dsyntax_2escm"),(void*)f_8956},
{C_text("f_8981:chicken_2dsyntax_2escm"),(void*)f_8981},
{C_text("f_8990:chicken_2dsyntax_2escm"),(void*)f_8990},
{C_text("f_9003:chicken_2dsyntax_2escm"),(void*)f_9003},
{C_text("f_9028:chicken_2dsyntax_2escm"),(void*)f_9028},
{C_text("f_9064:chicken_2dsyntax_2escm"),(void*)f_9064},
{C_text("f_9066:chicken_2dsyntax_2escm"),(void*)f_9066},
{C_text("f_9070:chicken_2dsyntax_2escm"),(void*)f_9070},
{C_text("f_9077:chicken_2dsyntax_2escm"),(void*)f_9077},
{C_text("f_9081:chicken_2dsyntax_2escm"),(void*)f_9081},
{C_text("f_9089:chicken_2dsyntax_2escm"),(void*)f_9089},
{C_text("f_9103:chicken_2dsyntax_2escm"),(void*)f_9103},
{C_text("f_9109:chicken_2dsyntax_2escm"),(void*)f_9109},
{C_text("f_9116:chicken_2dsyntax_2escm"),(void*)f_9116},
{C_text("f_9122:chicken_2dsyntax_2escm"),(void*)f_9122},
{C_text("f_9135:chicken_2dsyntax_2escm"),(void*)f_9135},
{C_text("f_9169:chicken_2dsyntax_2escm"),(void*)f_9169},
{C_text("f_9179:chicken_2dsyntax_2escm"),(void*)f_9179},
{C_text("f_9194:chicken_2dsyntax_2escm"),(void*)f_9194},
{C_text("f_9196:chicken_2dsyntax_2escm"),(void*)f_9196},
{C_text("f_9200:chicken_2dsyntax_2escm"),(void*)f_9200},
{C_text("f_9215:chicken_2dsyntax_2escm"),(void*)f_9215},
{C_text("f_9217:chicken_2dsyntax_2escm"),(void*)f_9217},
{C_text("f_9221:chicken_2dsyntax_2escm"),(void*)f_9221},
{C_text("f_9243:chicken_2dsyntax_2escm"),(void*)f_9243},
{C_text("f_9245:chicken_2dsyntax_2escm"),(void*)f_9245},
{C_text("f_9249:chicken_2dsyntax_2escm"),(void*)f_9249},
{C_text("f_9267:chicken_2dsyntax_2escm"),(void*)f_9267},
{C_text("f_9269:chicken_2dsyntax_2escm"),(void*)f_9269},
{C_text("f_9278:chicken_2dsyntax_2escm"),(void*)f_9278},
{C_text("f_9284:chicken_2dsyntax_2escm"),(void*)f_9284},
{C_text("f_9290:chicken_2dsyntax_2escm"),(void*)f_9290},
{C_text("f_9304:chicken_2dsyntax_2escm"),(void*)f_9304},
{C_text("f_9313:chicken_2dsyntax_2escm"),(void*)f_9313},
{C_text("f_9315:chicken_2dsyntax_2escm"),(void*)f_9315},
{C_text("f_9340:chicken_2dsyntax_2escm"),(void*)f_9340},
{C_text("f_9351:chicken_2dsyntax_2escm"),(void*)f_9351},
{C_text("f_9353:chicken_2dsyntax_2escm"),(void*)f_9353},
{C_text("f_9372:chicken_2dsyntax_2escm"),(void*)f_9372},
{C_text("f_9380:chicken_2dsyntax_2escm"),(void*)f_9380},
{C_text("f_9389:chicken_2dsyntax_2escm"),(void*)f_9389},
{C_text("f_9395:chicken_2dsyntax_2escm"),(void*)f_9395},
{C_text("f_9399:chicken_2dsyntax_2escm"),(void*)f_9399},
{C_text("f_9407:chicken_2dsyntax_2escm"),(void*)f_9407},
{C_text("f_9413:chicken_2dsyntax_2escm"),(void*)f_9413},
{C_text("f_9417:chicken_2dsyntax_2escm"),(void*)f_9417},
{C_text("f_9425:chicken_2dsyntax_2escm"),(void*)f_9425},
{C_text("f_9428:chicken_2dsyntax_2escm"),(void*)f_9428},
{C_text("f_9432:chicken_2dsyntax_2escm"),(void*)f_9432},
{C_text("f_9440:chicken_2dsyntax_2escm"),(void*)f_9440},
{C_text("f_9443:chicken_2dsyntax_2escm"),(void*)f_9443},
{C_text("f_9456:chicken_2dsyntax_2escm"),(void*)f_9456},
{C_text("f_9473:chicken_2dsyntax_2escm"),(void*)f_9473},
{C_text("f_9484:chicken_2dsyntax_2escm"),(void*)f_9484},
{C_text("f_9532:chicken_2dsyntax_2escm"),(void*)f_9532},
{C_text("f_9536:chicken_2dsyntax_2escm"),(void*)f_9536},
{C_text("f_9548:chicken_2dsyntax_2escm"),(void*)f_9548},
{C_text("f_9560:chicken_2dsyntax_2escm"),(void*)f_9560},
{C_text("f_9562:chicken_2dsyntax_2escm"),(void*)f_9562},
{C_text("f_9610:chicken_2dsyntax_2escm"),(void*)f_9610},
{C_text("f_9658:chicken_2dsyntax_2escm"),(void*)f_9658},
{C_text("f_9665:chicken_2dsyntax_2escm"),(void*)f_9665},
{C_text("f_9735:chicken_2dsyntax_2escm"),(void*)f_9735},
{C_text("f_9753:chicken_2dsyntax_2escm"),(void*)f_9753},
{C_text("f_9757:chicken_2dsyntax_2escm"),(void*)f_9757},
{C_text("f_9773:chicken_2dsyntax_2escm"),(void*)f_9773},
{C_text("f_9777:chicken_2dsyntax_2escm"),(void*)f_9777},
{C_text("f_9789:chicken_2dsyntax_2escm"),(void*)f_9789},
{C_text("f_9799:chicken_2dsyntax_2escm"),(void*)f_9799},
{C_text("f_9847:chicken_2dsyntax_2escm"),(void*)f_9847},
{C_text("f_9895:chicken_2dsyntax_2escm"),(void*)f_9895},
{C_text("f_9902:chicken_2dsyntax_2escm"),(void*)f_9902},
{C_text("f_9965:chicken_2dsyntax_2escm"),(void*)f_9965},
{C_text("toplevel:chicken_2dsyntax_2escm"),(void*)C_chicken_2dsyntax_toplevel},
{NULL,NULL}};
#endif

static C_PTABLE_ENTRY *create_ptable(void){
#ifdef C_ENABLE_PTABLES
return ptable;
#else
return NULL;
#endif
}

/*
S|applied compiler syntax:
S|  scheme#for-each		2
S|  ##sys#map		11
S|  chicken.base#foldl		4
S|  scheme#map		42
S|  chicken.base#foldr		4
o|eliminated procedure checks: 738 
o|specializations:
o|  1 (chicken.base#add1 *)
o|  1 (scheme#zero? integer)
o|  2 (scheme#string-append string string)
o|  14 (scheme#cddr (pair * pair))
o|  2 (scheme#length list)
o|  1 (scheme#caddr (pair * (pair * pair)))
o|  4 (scheme#cadr (pair * pair))
o|  4 (scheme#cdddr (pair * (pair * pair)))
o|  1 (scheme#eqv? * *)
o|  40 (##sys#check-list (or pair list) *)
o|  69 (scheme#cdr pair)
o|  24 (scheme#car pair)
(o e)|safe calls: 1481 
o|safe globals: (posv posq make-list iota find-tail find length+ lset=/eq? lset<=/eq? list-tabulate lset-intersection/eq? lset-union/eq? lset-difference/eq? lset-adjoin/eq? list-index last unzip1 remove filter-map filter alist-cons delete-duplicates fifth fourth third second first delete concatenate cons* any every append-map split-at drop take span partition) 
o|removed side-effect free assignment to unused variable: partition 
o|removed side-effect free assignment to unused variable: span 
o|inlining procedure: k4336 
o|inlining procedure: k4336 
o|removed side-effect free assignment to unused variable: drop 
o|removed side-effect free assignment to unused variable: append-map 
o|inlining procedure: k4606 
o|inlining procedure: k4606 
o|inlining procedure: k4637 
o|inlining procedure: k4637 
o|removed side-effect free assignment to unused variable: cons* 
o|removed side-effect free assignment to unused variable: concatenate 
o|removed side-effect free assignment to unused variable: first 
o|removed side-effect free assignment to unused variable: second 
o|removed side-effect free assignment to unused variable: third 
o|removed side-effect free assignment to unused variable: fourth 
o|removed side-effect free assignment to unused variable: fifth 
o|removed side-effect free assignment to unused variable: delete-duplicates 
o|removed side-effect free assignment to unused variable: alist-cons 
o|inlining procedure: k4854 
o|inlining procedure: k4854 
o|inlining procedure: k4846 
o|inlining procedure: k4846 
o|removed side-effect free assignment to unused variable: filter-map 
o|removed side-effect free assignment to unused variable: remove 
o|removed side-effect free assignment to unused variable: unzip1 
o|removed side-effect free assignment to unused variable: last 
o|removed side-effect free assignment to unused variable: list-index 
o|removed side-effect free assignment to unused variable: lset-adjoin/eq? 
o|removed side-effect free assignment to unused variable: lset-difference/eq? 
o|removed side-effect free assignment to unused variable: lset-union/eq? 
o|removed side-effect free assignment to unused variable: lset-intersection/eq? 
o|inlining procedure: k5245 
o|inlining procedure: k5245 
o|removed side-effect free assignment to unused variable: lset<=/eq? 
o|removed side-effect free assignment to unused variable: lset=/eq? 
o|removed side-effect free assignment to unused variable: length+ 
o|removed side-effect free assignment to unused variable: find 
o|removed side-effect free assignment to unused variable: find-tail 
o|removed side-effect free assignment to unused variable: iota 
o|removed side-effect free assignment to unused variable: make-list 
o|removed side-effect free assignment to unused variable: posq 
o|removed side-effect free assignment to unused variable: posv 
o|inlining procedure: k5788 
o|inlining procedure: k5788 
o|inlining procedure: k5816 
o|inlining procedure: k5816 
o|inlining procedure: k5860 
o|inlining procedure: k5860 
o|inlining procedure: k5939 
o|inlining procedure: k5939 
o|inlining procedure: k6037 
o|inlining procedure: k6037 
o|inlining procedure: k6134 
o|inlining procedure: k6134 
o|inlining procedure: k6228 
o|inlining procedure: k6228 
o|inlining procedure: k6333 
o|inlining procedure: k6333 
o|inlining procedure: k6382 
o|inlining procedure: k6382 
o|inlining procedure: k6429 
o|inlining procedure: k6429 
o|inlining procedure: k6468 
o|inlining procedure: k6468 
o|inlining procedure: k6541 
o|inlining procedure: k6541 
o|inlining procedure: k6597 
o|inlining procedure: k6597 
o|inlining procedure: k6612 
o|inlining procedure: k6612 
o|inlining procedure: k6650 
o|inlining procedure: k6650 
o|inlining procedure: k6673 
o|inlining procedure: k6673 
o|inlining procedure: k6733 
o|inlining procedure: k6733 
o|inlining procedure: k6850 
o|inlining procedure: k6850 
o|contracted procedure: "(chicken-syntax.scm:1019) split-at" 
o|inlining procedure: k4394 
o|inlining procedure: k4394 
o|inlining procedure: k6979 
o|inlining procedure: k6979 
o|inlining procedure: k7004 
o|inlining procedure: k7004 
o|inlining procedure: k7052 
o|inlining procedure: k7080 
o|inlining procedure: k7080 
o|inlining procedure: k7052 
o|inlining procedure: k7112 
o|inlining procedure: k7112 
o|inlining procedure: k7152 
o|inlining procedure: k7152 
o|inlining procedure: k6928 
o|inlining procedure: k6928 
o|inlining procedure: k7220 
o|contracted procedure: "(chicken-syntax.scm:989) g28482857" 
o|inlining procedure: k7220 
o|inlining procedure: k7320 
o|inlining procedure: k7320 
o|removed unused formal parameters: (rename2643) 
o|inlining procedure: k7619 
o|inlining procedure: k7619 
o|removed unused parameter to known procedure: rename2643 "(chicken-syntax.scm:891) make-if-tree2623" 
o|contracted procedure: "(chicken-syntax.scm:889) make-default-procs2622" 
o|inlining procedure: k7552 
o|inlining procedure: k7552 
o|inlining procedure: k7815 
o|inlining procedure: k7815 
o|inlining procedure: k7849 
o|inlining procedure: k7849 
o|inlining procedure: k7883 
o|inlining procedure: k7883 
o|inlining procedure: k7917 
o|inlining procedure: k7917 
o|inlining procedure: k7981 
o|inlining procedure: k7981 
o|inlining procedure: k8033 
o|inlining procedure: k8033 
o|inlining procedure: k8123 
o|inlining procedure: k8123 
o|inlining procedure: k8138 
o|inlining procedure: k8138 
o|inlining procedure: k8347 
o|inlining procedure: k8347 
o|inlining procedure: k8395 
o|contracted procedure: "(chicken-syntax.scm:668) g24652474" 
o|inlining procedure: k8395 
o|inlining procedure: k8429 
o|contracted procedure: "(chicken-syntax.scm:669) g24892490" 
o|inlining procedure: k8429 
o|substituted constant variable: g24812484 
o|inlining procedure: k8465 
o|inlining procedure: k8465 
o|inlining procedure: k8499 
o|inlining procedure: k8499 
o|inlining procedure: k8554 
o|inlining procedure: k8554 
o|inlining procedure: k8601 
o|inlining procedure: k8601 
o|inlining procedure: k8629 
o|inlining procedure: k8629 
o|inlining procedure: k8731 
o|inlining procedure: k8766 
o|inlining procedure: k8766 
o|inlining procedure: k8731 
o|inlining procedure: k8880 
o|contracted procedure: "(chicken-syntax.scm:626) g23572366" 
o|inlining procedure: k8880 
o|inlining procedure: k8914 
o|inlining procedure: k8914 
o|inlining procedure: k8958 
o|inlining procedure: k8958 
o|inlining procedure: k8992 
o|inlining procedure: k8992 
o|inlining procedure: k9015 
o|inlining procedure: k9015 
o|inlining procedure: k9030 
o|inlining procedure: k9030 
o|inlining procedure: k9137 
o|contracted procedure: "(chicken-syntax.scm:585) g21722190" 
o|inlining procedure: k9137 
o|inlining procedure: k9171 
o|contracted procedure: "(chicken-syntax.scm:581) g21572179" 
o|inlining procedure: k9171 
o|inlining procedure: k9292 
o|inlining procedure: k9292 
o|inlining procedure: k9317 
o|inlining procedure: k9317 
o|contracted procedure: "(chicken-syntax.scm:494) pname1598" 
o|inlining procedure: k9358 
o|inlining procedure: k9358 
o|removed unused formal parameters: (z1705) 
o|removed unused formal parameters: (z1733) 
o|inlining procedure: k9564 
o|contracted procedure: "(chicken-syntax.scm:531) g20372047" 
o|inlining procedure: k9564 
o|inlining procedure: k9612 
o|contracted procedure: "(chicken-syntax.scm:529) g20012011" 
o|inlining procedure: k9612 
o|inlining procedure: k9660 
o|contracted procedure: "(chicken-syntax.scm:526) g19591970" 
o|inlining procedure: k9660 
o|inlining procedure: k9710 
o|inlining procedure: k9710 
o|inlining procedure: k9801 
o|contracted procedure: "(chicken-syntax.scm:519) g19231933" 
o|inlining procedure: k9801 
o|inlining procedure: k9849 
o|contracted procedure: "(chicken-syntax.scm:516) g18871897" 
o|inlining procedure: k9849 
o|inlining procedure: k9897 
o|inlining procedure: k9897 
o|inlining procedure: k9947 
o|inlining procedure: k9947 
o|inlining procedure: k9967 
o|inlining procedure: k9967 
o|inlining procedure: k10015 
o|inlining procedure: k10015 
o|inlining procedure: k10063 
o|inlining procedure: k10063 
o|inlining procedure: k10111 
o|removed unused parameter to known procedure: z1733 "(chicken-syntax.scm:496) g17221731" 
o|inlining procedure: k10111 
o|inlining procedure: k10145 
o|removed unused parameter to known procedure: z1705 "(chicken-syntax.scm:495) g16941703" 
o|inlining procedure: k10145 
o|inlining procedure: k10179 
o|inlining procedure: k10179 
o|inlining procedure: k10213 
o|inlining procedure: k10213 
o|inlining procedure: k10247 
o|inlining procedure: k10247 
o|removed unused formal parameters: (x1320) 
o|removed unused formal parameters: (x1348) 
o|inlining procedure: k10402 
o|contracted procedure: "(chicken-syntax.scm:476) g15661576" 
o|inlining procedure: k10402 
o|inlining procedure: k10450 
o|contracted procedure: "(chicken-syntax.scm:474) g15301540" 
o|inlining procedure: k10450 
o|inlining procedure: k10549 
o|contracted procedure: "(chicken-syntax.scm:469) g14941504" 
o|inlining procedure: k10549 
o|inlining procedure: k10597 
o|contracted procedure: "(chicken-syntax.scm:467) g14581468" 
o|inlining procedure: k10597 
o|inlining procedure: k10680 
o|inlining procedure: k10680 
o|inlining procedure: k10730 
o|inlining procedure: k10730 
o|inlining procedure: k10750 
o|inlining procedure: k10750 
o|inlining procedure: k10798 
o|inlining procedure: k10798 
o|inlining procedure: k10832 
o|removed unused parameter to known procedure: x1348 "(chicken-syntax.scm:457) g13371346" 
o|inlining procedure: k10832 
o|inlining procedure: k10866 
o|removed unused parameter to known procedure: x1320 "(chicken-syntax.scm:456) g13091318" 
o|inlining procedure: k10866 
o|inlining procedure: k10900 
o|inlining procedure: k10900 
o|inlining procedure: k11010 
o|inlining procedure: k11010 
o|inlining procedure: k11132 
o|inlining procedure: k11132 
o|inlining procedure: k11153 
o|inlining procedure: k11165 
o|inlining procedure: k11165 
o|inlining procedure: k11153 
o|inlining procedure: k11225 
o|inlining procedure: k11225 
o|inlining procedure: k11285 
o|inlining procedure: k11285 
o|inlining procedure: k11374 
o|inlining procedure: k11374 
o|substituted constant variable: a11409 
o|substituted constant variable: a11434 
o|inlining procedure: k11443 
o|inlining procedure: k11443 
o|inlining procedure: k11515 
o|inlining procedure: k11515 
o|inlining procedure: k11544 
o|inlining procedure: k11559 
o|inlining procedure: k11575 
o|inlining procedure: k11575 
o|inlining procedure: k11559 
o|inlining procedure: k11544 
o|inlining procedure: k11609 
o|inlining procedure: k11625 
o|inlining procedure: k11625 
o|inlining procedure: k11609 
o|inlining procedure: k11663 
o|inlining procedure: k11663 
o|inlining procedure: k11727 
o|inlining procedure: k11727 
o|inlining procedure: k11836 
o|inlining procedure: k11836 
o|inlining procedure: k11949 
o|inlining procedure: k11949 
o|inlining procedure: k11970 
o|inlining procedure: k11970 
o|inlining procedure: k12016 
o|inlining procedure: k12016 
o|inlining procedure: k12056 
o|inlining procedure: k12112 
o|contracted procedure: "(chicken-syntax.scm:201) g919929" 
o|inlining procedure: k12112 
o|inlining procedure: k12182 
o|inlining procedure: k12209 
o|contracted procedure: "(chicken-syntax.scm:192) g885894" 
o|propagated global variable: g902903 chicken.compiler.scrutinizer#check-and-validate-type 
o|inlining procedure: k12209 
o|inlining procedure: k12182 
o|inlining procedure: k12253 
o|inlining procedure: k12253 
o|inlining procedure: k12056 
o|inlining procedure: k12300 
o|inlining procedure: k12300 
o|inlining procedure: k12337 
o|inlining procedure: k12337 
o|inlining procedure: k12386 
o|inlining procedure: k12477 
o|contracted procedure: "(chicken-syntax.scm:154) g816825" 
o|inlining procedure: k12477 
o|inlining procedure: k12511 
o|contracted procedure: "(chicken-syntax.scm:154) g788797" 
o|inlining procedure: k12511 
o|inlining procedure: k12386 
o|inlining procedure: k12548 
o|inlining procedure: k12563 
o|inlining procedure: k12579 
o|inlining procedure: k12579 
o|inlining procedure: k12563 
o|inlining procedure: k12548 
o|inlining procedure: k12613 
o|inlining procedure: k12629 
o|inlining procedure: k12629 
o|inlining procedure: k12664 
o|inlining procedure: k12664 
o|inlining procedure: k12613 
o|inlining procedure: k12695 
o|inlining procedure: k12695 
o|inlining procedure: k12733 
o|inlining procedure: k12733 
o|inlining procedure: k12760 
o|inlining procedure: k12760 
o|inlining procedure: k12789 
o|inlining procedure: k12789 
o|inlining procedure: k12871 
o|inlining procedure: k12871 
o|inlining procedure: k12913 
o|inlining procedure: k12913 
o|inlining procedure: k13028 
o|inlining procedure: k13028 
o|inlining procedure: k13048 
o|inlining procedure: k13048 
o|replaced variables: 2100 
o|removed binding forms: 476 
o|substituted constant variable: r433713210 
o|removed side-effect free assignment to unused variable: every 
o|removed side-effect free assignment to unused variable: any 
o|removed side-effect free assignment to unused variable: filter 
o|removed side-effect free assignment to unused variable: list-tabulate 
o|substituted constant variable: r581713227 
o|substituted constant variable: r638313240 
o|substituted constant variable: r654213249 
o|substituted constant variable: r659813251 
o|substituted constant variable: r685113260 
o|substituted constant variable: r715313276 
o|substituted constant variable: r692913279 
o|converted assignments to bindings: (genvars2834) 
o|substituted constant variable: r755313286 
o|converted assignments to bindings: (make-if-tree2623) 
o|substituted constant variable: r813913311 
o|substituted constant variable: r863013326 
o|substituted constant variable: r929313348 
o|substituted constant variable: r971113361 
o|substituted constant variable: r994813369 
o|substituted constant variable: r1073113396 
o|substituted constant variable: r1116613414 
o|substituted constant variable: r1115413415 
o|substituted constant variable: r1156013434 
o|substituted constant variable: r1154513435 
o|substituted constant variable: r1161013439 
o|substituted constant variable: r1172813443 
o|substituted constant variable: r1183713444 
o|substituted constant variable: r1201713450 
o|substituted constant variable: r1225413461 
o|substituted constant variable: r1233813467 
o|substituted constant variable: r1256413478 
o|substituted constant variable: r1254913479 
o|substituted constant variable: r1266513484 
o|substituted constant variable: r1261413485 
o|substituted constant variable: r1273413488 
o|substituted constant variable: r1279013494 
o|substituted constant variable: r1279013494 
o|substituted constant variable: r1302913500 
o|substituted constant variable: r1302913500 
o|converted assignments to bindings: (parse-clause597) 
o|simplifications: ((let . 3)) 
o|replaced variables: 77 
o|removed binding forms: 1837 
o|removed call to pure procedure with unused result: "(chicken-syntax.scm:526) ##sys#slot" 
o|removed call to pure procedure with unused result: "(chicken-syntax.scm:496) ##sys#slot" 
o|removed call to pure procedure with unused result: "(chicken-syntax.scm:495) ##sys#slot" 
o|removed call to pure procedure with unused result: "(chicken-syntax.scm:457) ##sys#slot" 
o|removed call to pure procedure with unused result: "(chicken-syntax.scm:456) ##sys#slot" 
o|removed binding forms: 112 
o|contracted procedure: k9697 
o|contracted procedure: k10136 
o|contracted procedure: k10170 
o|contracted procedure: k10857 
o|contracted procedure: k10891 
o|removed binding forms: 5 
o|removed binding forms: 5 
o|simplifications: ((let . 32) (if . 30) (##core#call . 1224)) 
o|  call simplifications:
o|    scheme#cdddr	2
o|    chicken.fixnum#fx=
o|    scheme#vector
o|    ##sys#pair?	7
o|    ##sys#eq?	7
o|    ##sys#car	15
o|    ##sys#cdr	22
o|    scheme#symbol?	7
o|    scheme#list?	2
o|    scheme#cdar
o|    scheme#caar
o|    scheme#assq	2
o|    scheme#apply
o|    scheme#eq?	5
o|    ##sys#call-with-values	3
o|    scheme#values
o|    chicken.fixnum#fx>=
o|    chicken.fixnum#fx+	3
o|    scheme#cadddr	2
o|    scheme#cddddr
o|    ##sys#check-list	37
o|    ##sys#setslot	50
o|    ##sys#slot	196
o|    scheme#cddr	7
o|    scheme#caddr	14
o|    scheme#not	15
o|    scheme#memq	7
o|    scheme#list	9
o|    scheme#cadr	48
o|    scheme#null?	29
o|    scheme#string?
o|    scheme#pair?	96
o|    scheme#cdr	19
o|    ##sys#cons	141
o|    ##sys#list	291
o|    chicken.fixnum#fx<=	2
o|    scheme#car	45
o|    chicken.fixnum#fx-	4
o|    scheme#cons	128
o|contracted procedure: k4339 
o|contracted procedure: k4346 
o|contracted procedure: k4356 
o|contracted procedure: k5705 
o|contracted procedure: k5737 
o|contracted procedure: k5733 
o|contracted procedure: k5713 
o|contracted procedure: k5729 
o|contracted procedure: k5721 
o|contracted procedure: k5725 
o|contracted procedure: k5717 
o|contracted procedure: k5709 
o|contracted procedure: k5750 
o|contracted procedure: k5823 
o|contracted procedure: k5755 
o|contracted procedure: k5807 
o|contracted procedure: k5768 
o|contracted procedure: k5776 
o|contracted procedure: k5780 
o|contracted procedure: k5772 
o|contracted procedure: k5791 
o|contracted procedure: k5799 
o|contracted procedure: k5788 
o|contracted procedure: k5810 
o|contracted procedure: k5844 
o|contracted procedure: k5857 
o|contracted procedure: k5863 
o|contracted procedure: k5886 
o|contracted procedure: k5882 
o|contracted procedure: k5876 
o|contracted procedure: k5870 
o|contracted procedure: k5902 
o|contracted procedure: k5898 
o|contracted procedure: k6100 
o|contracted procedure: k5909 
o|contracted procedure: k5942 
o|contracted procedure: k5977 
o|contracted procedure: k5973 
o|contracted procedure: k5969 
o|contracted procedure: k5961 
o|contracted procedure: k5987 
o|contracted procedure: k6006 
o|contracted procedure: k6002 
o|contracted procedure: k5998 
o|contracted procedure: k6026 
o|contracted procedure: k6030 
o|contracted procedure: k6046 
o|contracted procedure: k6074 
o|contracted procedure: k6066 
o|contracted procedure: k6070 
o|contracted procedure: k6086 
o|contracted procedure: k6096 
o|contracted procedure: k6089 
o|contracted procedure: k6276 
o|contracted procedure: k6104 
o|contracted procedure: k6137 
o|contracted procedure: k6168 
o|contracted procedure: k6164 
o|contracted procedure: k6160 
o|contracted procedure: k6178 
o|contracted procedure: k6199 
o|contracted procedure: k6193 
o|contracted procedure: k6189 
o|contracted procedure: k6217 
o|contracted procedure: k6221 
o|contracted procedure: k6237 
o|contracted procedure: k6254 
o|contracted procedure: k6262 
o|contracted procedure: k6272 
o|contracted procedure: k6265 
o|contracted procedure: k6289 
o|contracted procedure: k6298 
o|contracted procedure: k6301 
o|contracted procedure: k6304 
o|contracted procedure: k6313 
o|contracted procedure: k6322 
o|contracted procedure: k6325 
o|contracted procedure: k6336 
o|contracted procedure: k6342 
o|contracted procedure: k6644 
o|contracted procedure: k6356 
o|contracted procedure: k6588 
o|contracted procedure: k6592 
o|contracted procedure: k6600 
o|contracted procedure: k6603 
o|contracted procedure: k6584 
o|contracted procedure: k6580 
o|contracted procedure: k6364 
o|contracted procedure: k6568 
o|contracted procedure: k6576 
o|contracted procedure: k6572 
o|contracted procedure: k6372 
o|contracted procedure: k6368 
o|contracted procedure: k6360 
o|contracted procedure: k6352 
o|contracted procedure: k6385 
o|contracted procedure: k6388 
o|contracted procedure: k6564 
o|contracted procedure: k6391 
o|contracted procedure: k6394 
o|contracted procedure: k6511 
o|contracted procedure: k6527 
o|contracted procedure: k6535 
o|contracted procedure: k6531 
o|contracted procedure: k6523 
o|contracted procedure: k6515 
o|contracted procedure: k6519 
o|contracted procedure: k6400 
o|contracted procedure: k6435 
o|contracted procedure: k6446 
o|contracted procedure: k6442 
o|contracted procedure: k6454 
o|contracted procedure: k6461 
o|contracted procedure: k6468 
o|contracted procedure: k6487 
o|contracted procedure: k6503 
o|contracted procedure: k6507 
o|contracted procedure: k6499 
o|contracted procedure: k6491 
o|contracted procedure: k6495 
o|contracted procedure: k6538 
o|contracted procedure: k6544 
o|contracted procedure: k6615 
o|contracted procedure: k6618 
o|contracted procedure: k6621 
o|contracted procedure: k6629 
o|contracted procedure: k6637 
o|contracted procedure: k6653 
o|contracted procedure: k6663 
o|contracted procedure: k6667 
o|contracted procedure: k6676 
o|contracted procedure: k6698 
o|contracted procedure: k6694 
o|contracted procedure: k6679 
o|contracted procedure: k6682 
o|contracted procedure: k6690 
o|contracted procedure: k6728 
o|contracted procedure: k6736 
o|contracted procedure: k6752 
o|contracted procedure: k6764 
o|contracted procedure: k6760 
o|contracted procedure: k6756 
o|contracted procedure: k6782 
o|contracted procedure: k6786 
o|contracted procedure: k6806 
o|contracted procedure: k6802 
o|contracted procedure: k6824 
o|contracted procedure: k7252 
o|contracted procedure: k7256 
o|contracted procedure: k7260 
o|contracted procedure: k7264 
o|contracted procedure: k7268 
o|contracted procedure: k6828 
o|contracted procedure: k6853 
o|contracted procedure: k6868 
o|contracted procedure: k7189 
o|contracted procedure: k7185 
o|contracted procedure: k6914 
o|contracted procedure: k6919 
o|contracted procedure: k6910 
o|contracted procedure: k6931 
o|contracted procedure: k6940 
o|contracted procedure: k6953 
o|contracted procedure: k4397 
o|contracted procedure: k4411 
o|contracted procedure: k4421 
o|contracted procedure: k4415 
o|contracted procedure: k6982 
o|contracted procedure: k6989 
o|contracted procedure: k6992 
o|contracted procedure: k6995 
o|contracted procedure: k7043 
o|contracted procedure: k7007 
o|contracted procedure: k7033 
o|contracted procedure: k7037 
o|contracted procedure: k7029 
o|contracted procedure: k7010 
o|contracted procedure: k7013 
o|contracted procedure: k7021 
o|contracted procedure: k7025 
o|contracted procedure: k7055 
o|contracted procedure: k7077 
o|contracted procedure: k7069 
o|contracted procedure: k7073 
o|contracted procedure: k7065 
o|contracted procedure: k7098 
o|contracted procedure: k7083 
o|contracted procedure: k7092 
o|contracted procedure: k7141 
o|contracted procedure: k7145 
o|contracted procedure: k7129 
o|contracted procedure: k7137 
o|contracted procedure: k7133 
o|contracted procedure: k7108 
o|contracted procedure: k7115 
o|contracted procedure: k7155 
o|contracted procedure: k7166 
o|contracted procedure: k7173 
o|contracted procedure: k7181 
o|contracted procedure: k7193 
o|contracted procedure: k7208 
o|contracted procedure: k7211 
o|contracted procedure: k7223 
o|contracted procedure: k7226 
o|contracted procedure: k7229 
o|contracted procedure: k7237 
o|contracted procedure: k7245 
o|contracted procedure: k7202 
o|contracted procedure: k7420 
o|contracted procedure: k7424 
o|contracted procedure: k7428 
o|contracted procedure: k7272 
o|contracted procedure: k7285 
o|contracted procedure: k7288 
o|contracted procedure: k7416 
o|contracted procedure: k7310 
o|contracted procedure: k7323 
o|contracted procedure: k7330 
o|contracted procedure: k7333 
o|contracted procedure: k7339 
o|contracted procedure: k7389 
o|contracted procedure: k7393 
o|contracted procedure: k7397 
o|contracted procedure: k7385 
o|contracted procedure: k7359 
o|contracted procedure: k7371 
o|contracted procedure: k7375 
o|contracted procedure: k7379 
o|contracted procedure: k7367 
o|contracted procedure: k7363 
o|contracted procedure: k7349 
o|contracted procedure: k7412 
o|contracted procedure: k7408 
o|contracted procedure: k7404 
o|contracted procedure: k7497 
o|contracted procedure: k7501 
o|contracted procedure: k7505 
o|contracted procedure: k7432 
o|contracted procedure: k7493 
o|contracted procedure: k7489 
o|contracted procedure: k7452 
o|contracted procedure: k7460 
o|contracted procedure: k7464 
o|contracted procedure: k7478 
o|contracted procedure: k7467 
o|contracted procedure: k7471 
o|contracted procedure: k7456 
o|contracted procedure: k7949 
o|contracted procedure: k7953 
o|contracted procedure: k7957 
o|contracted procedure: k7509 
o|contracted procedure: k7522 
o|contracted procedure: k7525 
o|contracted procedure: k7622 
o|contracted procedure: k7632 
o|contracted procedure: k7639 
o|contracted procedure: k7691 
o|contracted procedure: k7643 
o|contracted procedure: k7683 
o|contracted procedure: k7667 
o|contracted procedure: k7675 
o|contracted procedure: k7671 
o|contracted procedure: k7651 
o|contracted procedure: k7647 
o|contracted procedure: k7663 
o|contracted procedure: k7708 
o|contracted procedure: k7711 
o|contracted procedure: k7730 
o|contracted procedure: k7742 
o|contracted procedure: k7748 
o|contracted procedure: k7760 
o|contracted procedure: k7793 
o|contracted procedure: k7809 
o|contracted procedure: k7805 
o|contracted procedure: k7801 
o|contracted procedure: k7797 
o|contracted procedure: k7789 
o|contracted procedure: k7555 
o|contracted procedure: k7558 
o|contracted procedure: k7579 
o|contracted procedure: k7591 
o|contracted procedure: k7583 
o|contracted procedure: k7565 
o|contracted procedure: k7607 
o|contracted procedure: k7603 
o|contracted procedure: k7818 
o|contracted procedure: k7821 
o|contracted procedure: k7824 
o|contracted procedure: k7832 
o|contracted procedure: k7840 
o|contracted procedure: k7852 
o|contracted procedure: k7874 
o|contracted procedure: k7870 
o|contracted procedure: k7855 
o|contracted procedure: k7858 
o|contracted procedure: k7866 
o|contracted procedure: k7886 
o|contracted procedure: k7889 
o|contracted procedure: k7892 
o|contracted procedure: k7900 
o|contracted procedure: k7908 
o|contracted procedure: k7920 
o|contracted procedure: k7942 
o|contracted procedure: k7938 
o|contracted procedure: k7923 
o|contracted procedure: k7926 
o|contracted procedure: k7934 
o|contracted procedure: k7970 
o|contracted procedure: k7984 
o|contracted procedure: k7991 
o|contracted procedure: k7994 
o|contracted procedure: k8099 
o|contracted procedure: k8001 
o|contracted procedure: k8026 
o|contracted procedure: k8014 
o|contracted procedure: k8018 
o|contracted procedure: k8095 
o|contracted procedure: k8036 
o|contracted procedure: k8058 
o|contracted procedure: k8046 
o|contracted procedure: k8050 
o|contracted procedure: k8091 
o|contracted procedure: k8087 
o|contracted procedure: k8075 
o|contracted procedure: k8079 
o|contracted procedure: k8114 
o|contracted procedure: k8186 
o|contracted procedure: k8117 
o|contracted procedure: k8166 
o|contracted procedure: k8129 
o|contracted procedure: k8162 
o|contracted procedure: k8158 
o|contracted procedure: k8141 
o|contracted procedure: k8169 
o|contracted procedure: k8176 
o|contracted procedure: k8199 
o|contracted procedure: k8247 
o|contracted procedure: k8203 
o|contracted procedure: k8243 
o|contracted procedure: k8223 
o|contracted procedure: k8239 
o|contracted procedure: k8231 
o|contracted procedure: k8227 
o|contracted procedure: k8269 
o|contracted procedure: k8282 
o|contracted procedure: k8287 
o|contracted procedure: k8290 
o|contracted procedure: k8296 
o|contracted procedure: k8310 
o|contracted procedure: k8318 
o|contracted procedure: k8324 
o|contracted procedure: k8306 
o|contracted procedure: k8335 
o|contracted procedure: k8338 
o|contracted procedure: k8386 
o|contracted procedure: k8350 
o|contracted procedure: k8353 
o|contracted procedure: k8356 
o|contracted procedure: k8364 
o|contracted procedure: k8368 
o|contracted procedure: k8376 
o|contracted procedure: k8380 
o|contracted procedure: k8398 
o|contracted procedure: k8420 
o|contracted procedure: k8416 
o|contracted procedure: k8401 
o|contracted procedure: k8404 
o|contracted procedure: k8412 
o|contracted procedure: k8432 
o|contracted procedure: k8439 
o|contracted procedure: k8459 
o|contracted procedure: k8468 
o|contracted procedure: k8490 
o|contracted procedure: k8486 
o|contracted procedure: k8471 
o|contracted procedure: k8474 
o|contracted procedure: k8482 
o|contracted procedure: k8502 
o|contracted procedure: k8524 
o|contracted procedure: k8520 
o|contracted procedure: k8505 
o|contracted procedure: k8508 
o|contracted procedure: k8516 
o|contracted procedure: k8540 
o|contracted procedure: k8557 
o|contracted procedure: k8564 
o|contracted procedure: k8581 
o|contracted procedure: k8571 
o|contracted procedure: k8594 
o|contracted procedure: k8624 
o|contracted procedure: k8604 
o|contracted procedure: k8614 
o|contracted procedure: k8632 
o|contracted procedure: k8662 
o|contracted procedure: k8638 
o|contracted procedure: k8658 
o|contracted procedure: k8667 
o|contracted procedure: k8670 
o|contracted procedure: k8679 
o|contracted procedure: k8695 
o|contracted procedure: k8707 
o|contracted procedure: k8717 
o|contracted procedure: k8734 
o|contracted procedure: k8745 
o|contracted procedure: k8757 
o|contracted procedure: k8741 
o|contracted procedure: k8769 
o|contracted procedure: k8772 
o|contracted procedure: k8775 
o|contracted procedure: k8783 
o|contracted procedure: k8791 
o|contracted procedure: k8827 
o|contracted procedure: k8831 
o|contracted procedure: k8823 
o|contracted procedure: k8807 
o|contracted procedure: k8815 
o|contracted procedure: k8860 
o|contracted procedure: k8838 
o|contracted procedure: k8842 
o|contracted procedure: k8852 
o|contracted procedure: k8874 
o|contracted procedure: k8863 
o|contracted procedure: k8870 
o|contracted procedure: k8883 
o|contracted procedure: k8905 
o|contracted procedure: k8901 
o|contracted procedure: k8886 
o|contracted procedure: k8889 
o|contracted procedure: k8897 
o|contracted procedure: k8917 
o|contracted procedure: k8923 
o|contracted procedure: k8952 
o|contracted procedure: k8934 
o|inlining procedure: k8926 
o|inlining procedure: k8926 
o|contracted procedure: k8961 
o|contracted procedure: k8964 
o|contracted procedure: k8967 
o|contracted procedure: k8975 
o|contracted procedure: k8983 
o|contracted procedure: k8995 
o|contracted procedure: k8998 
o|contracted procedure: k9009 
o|contracted procedure: k9018 
o|inlining procedure: k9001 
o|contracted procedure: k9033 
o|contracted procedure: k9055 
o|contracted procedure: k9051 
o|contracted procedure: k9036 
o|contracted procedure: k9039 
o|contracted procedure: k9047 
o|contracted procedure: k9083 
o|contracted procedure: k9091 
o|contracted procedure: k9095 
o|contracted procedure: k9099 
o|contracted procedure: k9117 
o|contracted procedure: k9123 
o|contracted procedure: k9140 
o|contracted procedure: k9162 
o|contracted procedure: k9158 
o|contracted procedure: k9143 
o|contracted procedure: k9146 
o|contracted procedure: k9154 
o|contracted procedure: k9174 
o|contracted procedure: k9184 
o|contracted procedure: k9188 
o|contracted procedure: k9205 
o|contracted procedure: k9209 
o|contracted procedure: k9226 
o|contracted procedure: k9230 
o|contracted procedure: k9234 
o|contracted procedure: k9254 
o|contracted procedure: k9258 
o|contracted procedure: k9275 
o|contracted procedure: k9295 
o|contracted procedure: k9305 
o|contracted procedure: k9308 
o|contracted procedure: k9320 
o|contracted procedure: k9323 
o|contracted procedure: k9326 
o|contracted procedure: k9334 
o|contracted procedure: k9342 
o|contracted procedure: k9373 
o|contracted procedure: k9381 
o|contracted procedure: k9384 
o|contracted procedure: k9390 
o|contracted procedure: k9396 
o|contracted procedure: k9361 
o|contracted procedure: k9408 
o|contracted procedure: k9414 
o|contracted procedure: k9429 
o|contracted procedure: k9448 
o|contracted procedure: k9451 
o|contracted procedure: k9462 
o|contracted procedure: k9465 
o|contracted procedure: k9468 
o|contracted procedure: k9479 
o|contracted procedure: k9961 
o|contracted procedure: k9957 
o|contracted procedure: k9490 
o|contracted procedure: k9732 
o|contracted procedure: k9745 
o|contracted procedure: k9741 
o|contracted procedure: k9748 
o|contracted procedure: k9728 
o|contracted procedure: k9724 
o|contracted procedure: k9498 
o|contracted procedure: k9720 
o|contracted procedure: k9502 
o|contracted procedure: k9518 
o|contracted procedure: k9514 
o|contracted procedure: k9510 
o|contracted procedure: k9506 
o|contracted procedure: k9494 
o|contracted procedure: k9486 
o|contracted procedure: k9475 
o|contracted procedure: k9458 
o|contracted procedure: k9538 
o|contracted procedure: k9550 
o|contracted procedure: k9603 
o|contracted procedure: k9567 
o|contracted procedure: k9593 
o|contracted procedure: k9597 
o|contracted procedure: k9589 
o|contracted procedure: k9570 
o|contracted procedure: k9573 
o|contracted procedure: k9581 
o|contracted procedure: k9585 
o|contracted procedure: k9651 
o|contracted procedure: k9615 
o|contracted procedure: k9641 
o|contracted procedure: k9645 
o|contracted procedure: k9637 
o|contracted procedure: k9618 
o|contracted procedure: k9621 
o|contracted procedure: k9629 
o|contracted procedure: k9633 
o|contracted procedure: k9693 
o|contracted procedure: k9701 
o|contracted procedure: k9527 
o|contracted procedure: k9689 
o|contracted procedure: k9666 
o|contracted procedure: k9669 
o|contracted procedure: k9677 
o|contracted procedure: k9681 
o|contracted procedure: k9685 
o|contracted procedure: k9707 
o|contracted procedure: k9713 
o|contracted procedure: k9759 
o|contracted procedure: k9779 
o|contracted procedure: k9795 
o|contracted procedure: k9791 
o|contracted procedure: k9840 
o|contracted procedure: k9804 
o|contracted procedure: k9830 
o|contracted procedure: k9834 
o|contracted procedure: k9826 
o|contracted procedure: k9807 
o|contracted procedure: k9810 
o|contracted procedure: k9818 
o|contracted procedure: k9822 
o|contracted procedure: k9888 
o|contracted procedure: k9852 
o|contracted procedure: k9878 
o|contracted procedure: k9882 
o|contracted procedure: k9768 
o|contracted procedure: k9874 
o|contracted procedure: k9855 
o|contracted procedure: k9858 
o|contracted procedure: k9866 
o|contracted procedure: k9870 
o|contracted procedure: k9903 
o|contracted procedure: k9906 
o|contracted procedure: k9914 
o|contracted procedure: k9918 
o|contracted procedure: k9922 
o|contracted procedure: k9930 
o|contracted procedure: k9934 
o|contracted procedure: k9938 
o|contracted procedure: k9944 
o|contracted procedure: k9950 
o|contracted procedure: k10006 
o|contracted procedure: k9970 
o|contracted procedure: k9996 
o|contracted procedure: k10000 
o|contracted procedure: k9992 
o|contracted procedure: k9973 
o|contracted procedure: k9976 
o|contracted procedure: k9984 
o|contracted procedure: k9988 
o|contracted procedure: k10054 
o|contracted procedure: k10018 
o|contracted procedure: k10044 
o|contracted procedure: k10048 
o|contracted procedure: k10040 
o|contracted procedure: k10021 
o|contracted procedure: k10024 
o|contracted procedure: k10032 
o|contracted procedure: k10036 
o|contracted procedure: k10102 
o|contracted procedure: k10066 
o|contracted procedure: k10092 
o|contracted procedure: k10096 
o|contracted procedure: k10088 
o|contracted procedure: k10069 
o|contracted procedure: k10072 
o|contracted procedure: k10080 
o|contracted procedure: k10084 
o|contracted procedure: k10114 
o|contracted procedure: k10117 
o|contracted procedure: k10120 
o|contracted procedure: k10128 
o|contracted procedure: k10148 
o|contracted procedure: k10151 
o|contracted procedure: k10154 
o|contracted procedure: k10162 
o|contracted procedure: k10182 
o|contracted procedure: k10185 
o|contracted procedure: k10188 
o|contracted procedure: k10196 
o|contracted procedure: k10204 
o|contracted procedure: k10216 
o|contracted procedure: k10238 
o|contracted procedure: k10234 
o|contracted procedure: k10219 
o|contracted procedure: k10222 
o|contracted procedure: k10230 
o|contracted procedure: k10250 
o|contracted procedure: k10272 
o|contracted procedure: k10268 
o|contracted procedure: k10253 
o|contracted procedure: k10256 
o|contracted procedure: k10264 
o|contracted procedure: k10288 
o|contracted procedure: k10293 
o|contracted procedure: k10296 
o|contracted procedure: k10302 
o|contracted procedure: k10317 
o|contracted procedure: k10500 
o|contracted procedure: k10344 
o|contracted procedure: k10496 
o|contracted procedure: k10348 
o|contracted procedure: k10356 
o|contracted procedure: k10352 
o|contracted procedure: k10340 
o|contracted procedure: k10364 
o|contracted procedure: k10380 
o|contracted procedure: k10396 
o|contracted procedure: k10392 
o|contracted procedure: k10441 
o|contracted procedure: k10405 
o|contracted procedure: k10431 
o|contracted procedure: k10435 
o|contracted procedure: k10427 
o|contracted procedure: k10408 
o|contracted procedure: k10411 
o|contracted procedure: k10419 
o|contracted procedure: k10423 
o|contracted procedure: k10489 
o|contracted procedure: k10453 
o|contracted procedure: k10479 
o|contracted procedure: k10483 
o|contracted procedure: k10475 
o|contracted procedure: k10456 
o|contracted procedure: k10459 
o|contracted procedure: k10467 
o|contracted procedure: k10471 
o|contracted procedure: k10508 
o|contracted procedure: k10516 
o|contracted procedure: k10527 
o|contracted procedure: k10543 
o|contracted procedure: k10539 
o|contracted procedure: k10588 
o|contracted procedure: k10552 
o|contracted procedure: k10578 
o|contracted procedure: k10582 
o|contracted procedure: k10574 
o|contracted procedure: k10555 
o|contracted procedure: k10558 
o|contracted procedure: k10566 
o|contracted procedure: k10570 
o|contracted procedure: k10636 
o|contracted procedure: k10600 
o|contracted procedure: k10626 
o|contracted procedure: k10630 
o|contracted procedure: k10622 
o|contracted procedure: k10603 
o|contracted procedure: k10606 
o|contracted procedure: k10614 
o|contracted procedure: k10618 
o|contracted procedure: k10643 
o|contracted procedure: k10646 
o|contracted procedure: k10652 
o|contracted procedure: k10655 
o|contracted procedure: k10662 
o|contracted procedure: k10668 
o|contracted procedure: k10671 
o|contracted procedure: k10719 
o|contracted procedure: k10683 
o|contracted procedure: k10709 
o|contracted procedure: k10713 
o|contracted procedure: k10705 
o|contracted procedure: k10686 
o|contracted procedure: k10689 
o|contracted procedure: k10697 
o|contracted procedure: k10701 
o|contracted procedure: k10733 
o|contracted procedure: k10744 
o|contracted procedure: k10789 
o|contracted procedure: k10753 
o|contracted procedure: k10779 
o|contracted procedure: k10783 
o|contracted procedure: k10775 
o|contracted procedure: k10756 
o|contracted procedure: k10759 
o|contracted procedure: k10767 
o|contracted procedure: k10771 
o|contracted procedure: k10801 
o|contracted procedure: k10823 
o|contracted procedure: k10819 
o|contracted procedure: k10804 
o|contracted procedure: k10807 
o|contracted procedure: k10815 
o|contracted procedure: k10835 
o|contracted procedure: k10838 
o|contracted procedure: k10841 
o|contracted procedure: k10849 
o|contracted procedure: k10869 
o|contracted procedure: k10872 
o|contracted procedure: k10875 
o|contracted procedure: k10883 
o|contracted procedure: k10903 
o|contracted procedure: k10925 
o|contracted procedure: k10921 
o|contracted procedure: k10906 
o|contracted procedure: k10909 
o|contracted procedure: k10917 
o|contracted procedure: k10945 
o|contracted procedure: k10962 
o|contracted procedure: k10983 
o|contracted procedure: k10979 
o|contracted procedure: k10997 
o|contracted procedure: k11090 
o|contracted procedure: k11013 
o|contracted procedure: k11024 
o|contracted procedure: k11020 
o|contracted procedure: k11032 
o|contracted procedure: k11035 
o|contracted procedure: k11081 
o|contracted procedure: k11044 
o|contracted procedure: k11063 
o|contracted procedure: k11059 
o|contracted procedure: k11055 
o|contracted procedure: k11051 
o|contracted procedure: k11070 
o|contracted procedure: k11078 
o|contracted procedure: k11074 
o|contracted procedure: k11103 
o|contracted procedure: k11115 
o|contracted procedure: k11127 
o|contracted procedure: k11135 
o|contracted procedure: k11150 
o|contracted procedure: k11162 
o|contracted procedure: k11178 
o|contracted procedure: k11168 
o|inlining procedure: k11141 
o|inlining procedure: k11141 
o|inlining procedure: k11141 
o|contracted procedure: k11185 
o|contracted procedure: k11437 
o|contracted procedure: k11199 
o|contracted procedure: k11427 
o|contracted procedure: k11423 
o|contracted procedure: k11419 
o|contracted procedure: k11415 
o|contracted procedure: k11207 
o|contracted procedure: k11393 
o|contracted procedure: k11401 
o|contracted procedure: k11397 
o|contracted procedure: k11389 
o|contracted procedure: k11215 
o|contracted procedure: k11211 
o|contracted procedure: k11203 
o|contracted procedure: k11195 
o|contracted procedure: k11228 
o|contracted procedure: k11231 
o|contracted procedure: k11381 
o|contracted procedure: k11234 
o|contracted procedure: k11346 
o|contracted procedure: k11362 
o|contracted procedure: k11358 
o|contracted procedure: k11350 
o|contracted procedure: k11354 
o|contracted procedure: k11246 
o|contracted procedure: k11253 
o|contracted procedure: k11261 
o|contracted procedure: k11265 
o|contracted procedure: k11281 
o|contracted procedure: k11277 
o|contracted procedure: k11296 
o|contracted procedure: k11312 
o|contracted procedure: k11308 
o|contracted procedure: k11300 
o|contracted procedure: k11304 
o|contracted procedure: k11292 
o|contracted procedure: k11319 
o|contracted procedure: k11335 
o|contracted procedure: k11331 
o|contracted procedure: k11323 
o|contracted procedure: k11327 
o|contracted procedure: k11342 
o|contracted procedure: k11374 
o|contracted procedure: k11446 
o|contracted procedure: k11449 
o|contracted procedure: k11452 
o|contracted procedure: k11460 
o|contracted procedure: k11468 
o|contracted procedure: k11502 
o|contracted procedure: k11512 
o|contracted procedure: k11521 
o|contracted procedure: k11524 
o|contracted procedure: k11535 
o|contracted procedure: k11541 
o|contracted procedure: k11569 
o|contracted procedure: k11588 
o|contracted procedure: k11596 
o|contracted procedure: k11600 
o|contracted procedure: k11619 
o|contracted procedure: k11638 
o|contracted procedure: k11646 
o|contracted procedure: k11650 
o|contracted procedure: k11660 
o|contracted procedure: k11669 
o|contracted procedure: k11684 
o|contracted procedure: k11680 
o|contracted procedure: k11693 
o|contracted procedure: k11715 
o|contracted procedure: k11696 
o|contracted procedure: k11711 
o|contracted procedure: k11707 
o|contracted procedure: k11721 
o|contracted procedure: k11724 
o|contracted procedure: k11730 
o|contracted procedure: k11737 
o|contracted procedure: k11740 
o|contracted procedure: k11747 
o|contracted procedure: k11776 
o|contracted procedure: k11768 
o|contracted procedure: k11796 
o|contracted procedure: k11806 
o|contracted procedure: k11802 
o|contracted procedure: k11823 
o|contracted procedure: k11883 
o|contracted procedure: k11839 
o|contracted procedure: k11859 
o|contracted procedure: k11863 
o|contracted procedure: k11867 
o|contracted procedure: k11855 
o|contracted procedure: k11875 
o|contracted procedure: k11879 
o|contracted procedure: k11896 
o|contracted procedure: k12006 
o|contracted procedure: k12002 
o|contracted procedure: k11909 
o|contracted procedure: k11925 
o|contracted procedure: k11941 
o|contracted procedure: k11946 
o|contracted procedure: k11956 
o|contracted procedure: k11961 
o|contracted procedure: k11921 
o|contracted procedure: k11917 
o|contracted procedure: k11913 
o|contracted procedure: k11973 
o|contracted procedure: k11976 
o|contracted procedure: k11979 
o|contracted procedure: k11987 
o|contracted procedure: k11995 
o|contracted procedure: k12373 
o|contracted procedure: k12019 
o|contracted procedure: k12025 
o|contracted procedure: k12028 
o|contracted procedure: k12047 
o|contracted procedure: k12059 
o|contracted procedure: k12068 
o|contracted procedure: k12158 
o|contracted procedure: k12162 
o|contracted procedure: k12078 
o|contracted procedure: k12086 
o|contracted procedure: k12094 
o|contracted procedure: k12090 
o|contracted procedure: k12082 
o|contracted procedure: k12151 
o|contracted procedure: k12115 
o|contracted procedure: k12141 
o|contracted procedure: k12145 
o|contracted procedure: k12103 
o|contracted procedure: k12137 
o|contracted procedure: k12118 
o|contracted procedure: k12121 
o|contracted procedure: k12129 
o|contracted procedure: k12133 
o|contracted procedure: k12178 
o|contracted procedure: k12174 
o|contracted procedure: k12185 
o|contracted procedure: k12192 
o|contracted procedure: k12200 
o|contracted procedure: k12212 
o|contracted procedure: k12215 
o|contracted procedure: k12218 
o|contracted procedure: k12226 
o|contracted procedure: k12234 
o|contracted procedure: k12256 
o|contracted procedure: k12263 
o|contracted procedure: k12271 
o|contracted procedure: k12275 
o|contracted procedure: k12278 
o|contracted procedure: k12284 
o|contracted procedure: k12293 
o|contracted procedure: k12297 
o|contracted procedure: k12328 
o|contracted procedure: k12312 
o|contracted procedure: k12316 
o|contracted procedure: k12324 
o|contracted procedure: k12334 
o|contracted procedure: k12340 
o|contracted procedure: k12347 
o|contracted procedure: k12369 
o|contracted procedure: k12358 
o|contracted procedure: k12383 
o|contracted procedure: k12392 
o|contracted procedure: k12400 
o|contracted procedure: k12403 
o|contracted procedure: k12409 
o|contracted procedure: k12421 
o|contracted procedure: k12424 
o|contracted procedure: k12430 
o|contracted procedure: k12441 
o|contracted procedure: k12471 
o|contracted procedure: k12467 
o|contracted procedure: k12459 
o|contracted procedure: k12455 
o|contracted procedure: k12480 
o|contracted procedure: k12502 
o|contracted procedure: k12418 
o|contracted procedure: k12498 
o|contracted procedure: k12483 
o|contracted procedure: k12486 
o|contracted procedure: k12494 
o|contracted procedure: k12514 
o|contracted procedure: k12536 
o|contracted procedure: k12532 
o|contracted procedure: k12517 
o|contracted procedure: k12520 
o|contracted procedure: k12528 
o|contracted procedure: k12545 
o|contracted procedure: k12573 
o|contracted procedure: k12592 
o|contracted procedure: k12600 
o|contracted procedure: k12604 
o|contracted procedure: k12623 
o|contracted procedure: k12641 
o|contracted procedure: k12651 
o|contracted procedure: k12658 
o|contracted procedure: k12661 
o|contracted procedure: k12667 
o|contracted procedure: k12674 
o|contracted procedure: k12678 
o|contracted procedure: k12682 
o|contracted procedure: k12720 
o|contracted procedure: k12698 
o|contracted procedure: k12712 
o|contracted procedure: k12716 
o|contracted procedure: k12827 
o|contracted procedure: k12736 
o|contracted procedure: k12763 
o|contracted procedure: k12819 
o|contracted procedure: k12815 
o|contracted procedure: k12811 
o|contracted procedure: k12777 
o|contracted procedure: k12773 
o|contracted procedure: k12800 
o|contracted procedure: k12796 
o|contracted procedure: k12789 
o|contracted procedure: k12807 
o|contracted procedure: k12823 
o|contracted procedure: k13100 
o|contracted procedure: k12831 
o|contracted procedure: k12981 
o|contracted procedure: k12976 
o|contracted procedure: k12862 
o|contracted procedure: k12865 
o|contracted procedure: k12874 
o|contracted procedure: k12892 
o|contracted procedure: k12901 
o|contracted procedure: k12904 
o|contracted procedure: k12888 
o|contracted procedure: k12884 
o|contracted procedure: k12916 
o|contracted procedure: k12919 
o|contracted procedure: k12922 
o|contracted procedure: k12930 
o|contracted procedure: k12938 
o|contracted procedure: k12960 
o|contracted procedure: k12952 
o|contracted procedure: k12956 
o|contracted procedure: k12948 
o|contracted procedure: k12967 
o|contracted procedure: k13096 
o|contracted procedure: k13088 
o|contracted procedure: k13092 
o|contracted procedure: k13084 
o|contracted procedure: k13080 
o|contracted procedure: k13000 
o|contracted procedure: k13004 
o|contracted procedure: k13007 
o|contracted procedure: k13010 
o|contracted procedure: k13016 
o|contracted procedure: k12992 
o|contracted procedure: k12996 
o|contracted procedure: k13031 
o|contracted procedure: k13042 
o|contracted procedure: k13038 
o|contracted procedure: k13028 
o|contracted procedure: k13051 
o|contracted procedure: k13054 
o|contracted procedure: k13057 
o|contracted procedure: k13065 
o|contracted procedure: k13073 
o|contracted procedure: k13207 
o|contracted procedure: k13104 
o|contracted procedure: k13139 
o|contracted procedure: k13203 
o|contracted procedure: k13187 
o|contracted procedure: k13199 
o|contracted procedure: k13195 
o|contracted procedure: k13191 
o|contracted procedure: k13147 
o|contracted procedure: k13179 
o|contracted procedure: k13159 
o|contracted procedure: k13175 
o|contracted procedure: k13171 
o|contracted procedure: k13167 
o|contracted procedure: k13163 
o|contracted procedure: k13155 
o|contracted procedure: k13151 
o|contracted procedure: k13143 
o|contracted procedure: k13135 
o|contracted procedure: k13127 
o|simplifications: ((if . 5) (let . 235)) 
o|removed binding forms: 1042 
o|inlining procedure: k6795 
o|contracted procedure: k8800 
o|substituted constant variable: r1114214585 
o|substituted constant variable: r1114214586 
o|inlining procedure: k11285 
o|inlining procedure: k11285 
o|contracted procedure: k11666 
o|inlining procedure: k11937 
o|inlining procedure: k11937 
o|inlining procedure: k12182 
o|inlining procedure: k12182 
o|simplifications: ((let . 1)) 
o|removed binding forms: 3 
o|removed conditional forms: 2 
o|substituted constant variable: r1193814756 
o|replaced variables: 2 
o|removed binding forms: 4 
o|removed binding forms: 3 
o|direct leaf routine/allocation: g30623071 0 
o|direct leaf routine/allocation: g18451856 30 
o|direct leaf routine/allocation: g623632 15 
o|contracted procedure: "(chicken-syntax.scm:1114) k6633" 
o|contracted procedure: "(chicken-syntax.scm:512) k9926" 
o|contracted procedure: "(chicken-syntax.scm:95) k12934" 
o|removed binding forms: 3 
o|customizable procedures: (parse-clause597 map-loop648665 k12868 map-loop617638 k12785 k12644 loop734755 loop734771 map-loop782800 map-loop810828 k12303 loop855 loop2869 map-loop879904 map-loop913937 g974983 map-loop968990 k11690 loop10881109 loop10881120 g11631172 map-loop11571183 k11273 mapslots1193 k11141 map-loop12761293 g13091318 map-loop13031321 g13371346 map-loop13311349 map-loop13821399 map-loop13611406 loop1437 map-loop14181440 map-loop14521476 map-loop14881512 map-loop15241548 map-loop15601584 map-loop16061623 map-loop16331650 g16661675 map-loop16601678 g16941703 map-loop16881706 g17221731 map-loop17161734 map-loop17461765 map-loop17771796 map-loop18081827 k9900 map-loop18391866 map-loop18811905 map-loop19171941 k9663 map-loop19531980 map-loop19952019 map-loop20312055 g20812090 map-loop20752120 for-each-loop21562183 map-loop21662196 map-loop22272244 loop2252 g22712280 map-loop22652283 loop2293 map-loop23512369 fold2303 g23222331 map-loop23162334 fold2385 map-loop24042421 map-loop24302447 foldl24822486 map-loop24592498 map-loop25072526 k8120 k8132 fold2585 map-loop26592676 g26952704 map-loop26892707 map-loop27172734 g27522761 map-loop27462764 recur2629 make-if-tree2623 prefix-sym2683 recur2644 loop2812 map-loop28422863 genvars2834 foldr28852888 g28902891 k6956 build2910 map-loop29332952 loop122 loop2836 map-loop30093026 g30363046 for-each-loop30353049 map-loop30563074 k6397 k6403 k6410 k6418 loop3081 loop3126 loop3156 k5761 k5784 take) 
o|calls to known targets: 246 
o|identified direct recursive calls: f_4334 1 
o|identified direct recursive calls: f_6610 1 
o|identified direct recursive calls: f_6671 1 
o|identified direct recursive calls: f_4392 1 
o|identified direct recursive calls: f_7002 1 
o|identified direct recursive calls: f_6926 1 
o|identified direct recursive calls: f_7847 1 
o|identified direct recursive calls: f_7915 1 
o|identified direct recursive calls: f_8393 1 
o|identified direct recursive calls: f_8463 1 
o|identified direct recursive calls: f_8497 1 
o|identified direct recursive calls: f_8552 1 
o|identified direct recursive calls: f_8729 2 
o|identified direct recursive calls: f_8878 1 
o|identified direct recursive calls: f_8990 1 
o|identified direct recursive calls: f_9028 1 
o|identified direct recursive calls: f_9135 1 
o|identified direct recursive calls: f_9562 1 
o|identified direct recursive calls: f_9610 1 
o|identified direct recursive calls: f_9799 1 
o|identified direct recursive calls: f_9847 1 
o|identified direct recursive calls: f_9965 1 
o|identified direct recursive calls: f_10013 1 
o|identified direct recursive calls: f_10061 1 
o|identified direct recursive calls: f_10211 1 
o|identified direct recursive calls: f_10245 1 
o|identified direct recursive calls: f_10400 1 
o|identified direct recursive calls: f_10448 1 
o|identified direct recursive calls: f_10547 1 
o|identified direct recursive calls: f_10595 1 
o|identified direct recursive calls: f_10678 1 
o|identified direct recursive calls: f_10728 1 
o|identified direct recursive calls: f_10748 1 
o|identified direct recursive calls: f_10796 1 
o|identified direct recursive calls: f_10898 1 
o|identified direct recursive calls: f_12110 1 
o|identified direct recursive calls: f_12251 1 
o|identified direct recursive calls: f_12054 1 
o|identified direct recursive calls: f_12475 1 
o|identified direct recursive calls: f_12509 1 
o|identified direct recursive calls: f_12911 1 
o|fast box initializations: 78 
o|fast global references: 2 
o|fast global assignments: 1 
o|dropping unused closure argument: f_4334 
o|dropping unused closure argument: f_7717 
*/
/* end of file */
