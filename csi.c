/* Generated from csi.scm by the CHICKEN compiler
   http://www.call-cc.org
   Version 5.2.0 (rev 317468e4)
   linux-unix-gnu-x86-64 [ 64bit dload ptables ]
   command line: csi.scm -optimize-level 2 -include-path . -include-path ./ -inline -ignore-repository -feature chicken-bootstrap -no-warnings -specialize -consult-types-file ./types.db -no-lambda-info -no-trace -output-file csi.c
   uses: library eval expand extras file internal pathname port posix repl data-structures
*/
#include "chicken.h"

#include <signal.h>

#if defined(HAVE_DIRECT_H)
# include <direct.h>
#else
# define _getcwd(buf, len)       NULL
#endif

static C_PTABLE_ENTRY *create_ptable(void);
C_noret_decl(C_library_toplevel)
C_externimport void C_ccall C_library_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_eval_toplevel)
C_externimport void C_ccall C_eval_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_expand_toplevel)
C_externimport void C_ccall C_expand_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_extras_toplevel)
C_externimport void C_ccall C_extras_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_file_toplevel)
C_externimport void C_ccall C_file_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_internal_toplevel)
C_externimport void C_ccall C_internal_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_pathname_toplevel)
C_externimport void C_ccall C_pathname_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_port_toplevel)
C_externimport void C_ccall C_port_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_posix_toplevel)
C_externimport void C_ccall C_posix_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_repl_toplevel)
C_externimport void C_ccall C_repl_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_data_2dstructures_toplevel)
C_externimport void C_ccall C_data_2dstructures_toplevel(C_word c,C_word *av) C_noret;

static C_TLS C_word lf[405];
static double C_possibly_force_alignment;


/* from k3942 */
C_regparm static C_word C_fcall stub712(C_word C_buf,C_word C_a0,C_word C_a1){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
void * t0=(void * )C_data_pointer_or_null(C_a0);
int t1=(int )C_unfix(C_a1);
C_r=C_mpointer(&C_a,(void*)_getcwd(t0,t1));
return C_r;}

C_noret_decl(f9340)
static void C_ccall f9340(C_word c,C_word *av) C_noret;
C_noret_decl(f9344)
static void C_ccall f9344(C_word c,C_word *av) C_noret;
C_noret_decl(f9432)
static void C_ccall f9432(C_word c,C_word *av) C_noret;
C_noret_decl(f9473)
static void C_ccall f9473(C_word c,C_word *av) C_noret;
C_noret_decl(f9499)
static void C_ccall f9499(C_word c,C_word *av) C_noret;
C_noret_decl(f9503)
static void C_ccall f9503(C_word c,C_word *av) C_noret;
C_noret_decl(f_2517)
static void C_ccall f_2517(C_word c,C_word *av) C_noret;
C_noret_decl(f_2520)
static void C_ccall f_2520(C_word c,C_word *av) C_noret;
C_noret_decl(f_2523)
static void C_ccall f_2523(C_word c,C_word *av) C_noret;
C_noret_decl(f_2526)
static void C_ccall f_2526(C_word c,C_word *av) C_noret;
C_noret_decl(f_2529)
static void C_ccall f_2529(C_word c,C_word *av) C_noret;
C_noret_decl(f_2532)
static void C_ccall f_2532(C_word c,C_word *av) C_noret;
C_noret_decl(f_2535)
static void C_ccall f_2535(C_word c,C_word *av) C_noret;
C_noret_decl(f_2538)
static void C_ccall f_2538(C_word c,C_word *av) C_noret;
C_noret_decl(f_2541)
static void C_ccall f_2541(C_word c,C_word *av) C_noret;
C_noret_decl(f_2544)
static void C_ccall f_2544(C_word c,C_word *av) C_noret;
C_noret_decl(f_2547)
static void C_ccall f_2547(C_word c,C_word *av) C_noret;
C_noret_decl(f_2550)
static void C_ccall f_2550(C_word c,C_word *av) C_noret;
C_noret_decl(f_3048)
static void C_fcall f_3048(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3075)
static void C_ccall f_3075(C_word c,C_word *av) C_noret;
C_noret_decl(f_3123)
static void C_fcall f_3123(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3137)
static void C_ccall f_3137(C_word c,C_word *av) C_noret;
C_noret_decl(f_3150)
static void C_ccall f_3150(C_word c,C_word *av) C_noret;
C_noret_decl(f_3840)
static void C_ccall f_3840(C_word c,C_word *av) C_noret;
C_noret_decl(f_3845)
static void C_ccall f_3845(C_word c,C_word *av) C_noret;
C_noret_decl(f_3848)
static void C_fcall f_3848(C_word t0,C_word t1) C_noret;
C_noret_decl(f_3854)
static void C_ccall f_3854(C_word c,C_word *av) C_noret;
C_noret_decl(f_3857)
static void C_ccall f_3857(C_word c,C_word *av) C_noret;
C_noret_decl(f_3864)
static void C_ccall f_3864(C_word c,C_word *av) C_noret;
C_noret_decl(f_3888)
static void C_ccall f_3888(C_word c,C_word *av) C_noret;
C_noret_decl(f_3903)
static void C_ccall f_3903(C_word c,C_word *av) C_noret;
C_noret_decl(f_3917)
static void C_ccall f_3917(C_word c,C_word *av) C_noret;
C_noret_decl(f_3930)
static void C_ccall f_3930(C_word c,C_word *av) C_noret;
C_noret_decl(f_3949)
static void C_fcall f_3949(C_word t0,C_word t1) C_noret;
C_noret_decl(f_3956)
static void C_ccall f_3956(C_word c,C_word *av) C_noret;
C_noret_decl(f_3959)
static void C_ccall f_3959(C_word c,C_word *av) C_noret;
C_noret_decl(f_3965)
static void C_ccall f_3965(C_word c,C_word *av) C_noret;
C_noret_decl(f_3978)
static void C_fcall f_3978(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3991)
static void C_ccall f_3991(C_word c,C_word *av) C_noret;
C_noret_decl(f_4000)
static void C_fcall f_4000(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4004)
static void C_ccall f_4004(C_word c,C_word *av) C_noret;
C_noret_decl(f_4016)
static void C_ccall f_4016(C_word c,C_word *av) C_noret;
C_noret_decl(f_4025)
static void C_ccall f_4025(C_word c,C_word *av) C_noret;
C_noret_decl(f_4028)
static void C_ccall f_4028(C_word c,C_word *av) C_noret;
C_noret_decl(f_4035)
static void C_ccall f_4035(C_word c,C_word *av) C_noret;
C_noret_decl(f_4039)
static void C_ccall f_4039(C_word c,C_word *av) C_noret;
C_noret_decl(f_4042)
static void C_ccall f_4042(C_word c,C_word *av) C_noret;
C_noret_decl(f_4048)
static void C_ccall f_4048(C_word c,C_word *av) C_noret;
C_noret_decl(f_4055)
static void C_ccall f_4055(C_word c,C_word *av) C_noret;
C_noret_decl(f_4057)
static void C_fcall f_4057(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4067)
static void C_ccall f_4067(C_word c,C_word *av) C_noret;
C_noret_decl(f_4070)
static void C_ccall f_4070(C_word c,C_word *av) C_noret;
C_noret_decl(f_4084)
static void C_ccall f_4084(C_word c,C_word *av) C_noret;
C_noret_decl(f_4107)
static void C_fcall f_4107(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4117)
static void C_fcall f_4117(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4131)
static void C_ccall f_4131(C_word c,C_word *av) C_noret;
C_noret_decl(f_4162)
static void C_fcall f_4162(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4175)
static void C_ccall f_4175(C_word c,C_word *av) C_noret;
C_noret_decl(f_4178)
static void C_ccall f_4178(C_word c,C_word *av) C_noret;
C_noret_decl(f_4181)
static void C_ccall f_4181(C_word c,C_word *av) C_noret;
C_noret_decl(f_4184)
static void C_ccall f_4184(C_word c,C_word *av) C_noret;
C_noret_decl(f_4187)
static void C_ccall f_4187(C_word c,C_word *av) C_noret;
C_noret_decl(f_4196)
static void C_ccall f_4196(C_word c,C_word *av) C_noret;
C_noret_decl(f_4206)
static void C_fcall f_4206(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4210)
static void C_ccall f_4210(C_word c,C_word *av) C_noret;
C_noret_decl(f_4233)
static void C_ccall f_4233(C_word c,C_word *av) C_noret;
C_noret_decl(f_4250)
static void C_ccall f_4250(C_word c,C_word *av) C_noret;
C_noret_decl(f_4262)
static void C_ccall f_4262(C_word c,C_word *av) C_noret;
C_noret_decl(f_4270)
static void C_ccall f_4270(C_word c,C_word *av) C_noret;
C_noret_decl(f_4273)
static void C_ccall f_4273(C_word c,C_word *av) C_noret;
C_noret_decl(f_4285)
static void C_ccall f_4285(C_word c,C_word *av) C_noret;
C_noret_decl(f_4292)
static void C_ccall f_4292(C_word c,C_word *av) C_noret;
C_noret_decl(f_4298)
static void C_ccall f_4298(C_word c,C_word *av) C_noret;
C_noret_decl(f_4318)
static C_word C_fcall f_4318(C_word *a,C_word t0,C_word t1);
C_noret_decl(f_4348)
static void C_ccall f_4348(C_word c,C_word *av) C_noret;
C_noret_decl(f_4381)
static void C_ccall f_4381(C_word c,C_word *av) C_noret;
C_noret_decl(f_4396)
static void C_ccall f_4396(C_word c,C_word *av) C_noret;
C_noret_decl(f_4399)
static void C_ccall f_4399(C_word c,C_word *av) C_noret;
C_noret_decl(f_4406)
static void C_ccall f_4406(C_word c,C_word *av) C_noret;
C_noret_decl(f_4410)
static void C_ccall f_4410(C_word c,C_word *av) C_noret;
C_noret_decl(f_4419)
static void C_ccall f_4419(C_word c,C_word *av) C_noret;
C_noret_decl(f_4422)
static void C_ccall f_4422(C_word c,C_word *av) C_noret;
C_noret_decl(f_4425)
static void C_ccall f_4425(C_word c,C_word *av) C_noret;
C_noret_decl(f_4437)
static void C_ccall f_4437(C_word c,C_word *av) C_noret;
C_noret_decl(f_4440)
static void C_ccall f_4440(C_word c,C_word *av) C_noret;
C_noret_decl(f_4452)
static void C_ccall f_4452(C_word c,C_word *av) C_noret;
C_noret_decl(f_4455)
static void C_ccall f_4455(C_word c,C_word *av) C_noret;
C_noret_decl(f_4467)
static void C_ccall f_4467(C_word c,C_word *av) C_noret;
C_noret_decl(f_4470)
static void C_ccall f_4470(C_word c,C_word *av) C_noret;
C_noret_decl(f_4473)
static void C_ccall f_4473(C_word c,C_word *av) C_noret;
C_noret_decl(f_4476)
static void C_ccall f_4476(C_word c,C_word *av) C_noret;
C_noret_decl(f_4506)
static void C_ccall f_4506(C_word c,C_word *av) C_noret;
C_noret_decl(f_4509)
static void C_ccall f_4509(C_word c,C_word *av) C_noret;
C_noret_decl(f_4514)
static void C_fcall f_4514(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4524)
static void C_ccall f_4524(C_word c,C_word *av) C_noret;
C_noret_decl(f_4539)
static void C_ccall f_4539(C_word c,C_word *av) C_noret;
C_noret_decl(f_4548)
static void C_ccall f_4548(C_word c,C_word *av) C_noret;
C_noret_decl(f_4549)
static void C_fcall f_4549(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4555)
static void C_ccall f_4555(C_word c,C_word *av) C_noret;
C_noret_decl(f_4559)
static void C_ccall f_4559(C_word c,C_word *av) C_noret;
C_noret_decl(f_4565)
static void C_ccall f_4565(C_word c,C_word *av) C_noret;
C_noret_decl(f_4570)
static void C_fcall f_4570(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4580)
static void C_ccall f_4580(C_word c,C_word *av) C_noret;
C_noret_decl(f_4595)
static void C_ccall f_4595(C_word c,C_word *av) C_noret;
C_noret_decl(f_4604)
static void C_ccall f_4604(C_word c,C_word *av) C_noret;
C_noret_decl(f_4609)
static void C_ccall f_4609(C_word c,C_word *av) C_noret;
C_noret_decl(f_4613)
static void C_ccall f_4613(C_word c,C_word *av) C_noret;
C_noret_decl(f_4618)
static void C_ccall f_4618(C_word c,C_word *av) C_noret;
C_noret_decl(f_4624)
static void C_ccall f_4624(C_word c,C_word *av) C_noret;
C_noret_decl(f_4628)
static void C_ccall f_4628(C_word c,C_word *av) C_noret;
C_noret_decl(f_4635)
static void C_ccall f_4635(C_word c,C_word *av) C_noret;
C_noret_decl(f_4637)
static void C_ccall f_4637(C_word c,C_word *av) C_noret;
C_noret_decl(f_4641)
static void C_ccall f_4641(C_word c,C_word *av) C_noret;
C_noret_decl(f_4656)
static void C_ccall f_4656(C_word c,C_word *av) C_noret;
C_noret_decl(f_4672)
static void C_ccall f_4672(C_word c,C_word *av) C_noret;
C_noret_decl(f_4690)
static void C_ccall f_4690(C_word c,C_word *av) C_noret;
C_noret_decl(f_4694)
static void C_ccall f_4694(C_word c,C_word *av) C_noret;
C_noret_decl(f_4710)
static void C_ccall f_4710(C_word c,C_word *av) C_noret;
C_noret_decl(f_4722)
static void C_ccall f_4722(C_word c,C_word *av) C_noret;
C_noret_decl(f_4734)
static void C_ccall f_4734(C_word c,C_word *av) C_noret;
C_noret_decl(f_4746)
static void C_ccall f_4746(C_word c,C_word *av) C_noret;
C_noret_decl(f_4753)
static void C_ccall f_4753(C_word c,C_word *av) C_noret;
C_noret_decl(f_4766)
static void C_ccall f_4766(C_word c,C_word *av) C_noret;
C_noret_decl(f_4775)
static void C_ccall f_4775(C_word c,C_word *av) C_noret;
C_noret_decl(f_4778)
static void C_ccall f_4778(C_word c,C_word *av) C_noret;
C_noret_decl(f_4781)
static void C_ccall f_4781(C_word c,C_word *av) C_noret;
C_noret_decl(f_4794)
static void C_ccall f_4794(C_word c,C_word *av) C_noret;
C_noret_decl(f_4816)
static void C_ccall f_4816(C_word c,C_word *av) C_noret;
C_noret_decl(f_4821)
static void C_fcall f_4821(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4831)
static void C_ccall f_4831(C_word c,C_word *av) C_noret;
C_noret_decl(f_4845)
static void C_ccall f_4845(C_word c,C_word *av) C_noret;
C_noret_decl(f_4891)
static void C_ccall f_4891(C_word c,C_word *av) C_noret;
C_noret_decl(f_4897)
static void C_ccall f_4897(C_word c,C_word *av) C_noret;
C_noret_decl(f_4901)
static void C_ccall f_4901(C_word c,C_word *av) C_noret;
C_noret_decl(f_4917)
static void C_ccall f_4917(C_word c,C_word *av) C_noret;
C_noret_decl(f_4923)
static void C_ccall f_4923(C_word c,C_word *av) C_noret;
C_noret_decl(f_4937)
static void C_ccall f_4937(C_word c,C_word *av) C_noret;
C_noret_decl(f_4940)
static void C_ccall f_4940(C_word c,C_word *av) C_noret;
C_noret_decl(f_4946)
static void C_ccall f_4946(C_word c,C_word *av) C_noret;
C_noret_decl(f_4949)
static void C_ccall f_4949(C_word c,C_word *av) C_noret;
C_noret_decl(f_4957)
static void C_fcall f_4957(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4967)
static void C_fcall f_4967(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4982)
static void C_ccall f_4982(C_word c,C_word *av) C_noret;
C_noret_decl(f_4991)
static void C_ccall f_4991(C_word c,C_word *av) C_noret;
C_noret_decl(f_4997)
static void C_ccall f_4997(C_word c,C_word *av) C_noret;
C_noret_decl(f_5003)
static void C_ccall f_5003(C_word c,C_word *av) C_noret;
C_noret_decl(f_5009)
static void C_ccall f_5009(C_word c,C_word *av) C_noret;
C_noret_decl(f_5015)
static void C_ccall f_5015(C_word c,C_word *av) C_noret;
C_noret_decl(f_5023)
static void C_ccall f_5023(C_word c,C_word *av) C_noret;
C_noret_decl(f_5025)
static void C_fcall f_5025(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_5042)
static void C_ccall f_5042(C_word c,C_word *av) C_noret;
C_noret_decl(f_5048)
static void C_ccall f_5048(C_word c,C_word *av) C_noret;
C_noret_decl(f_5054)
static void C_ccall f_5054(C_word c,C_word *av) C_noret;
C_noret_decl(f_5062)
static void C_ccall f_5062(C_word c,C_word *av) C_noret;
C_noret_decl(f_5063)
static void C_fcall f_5063(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5073)
static void C_ccall f_5073(C_word c,C_word *av) C_noret;
C_noret_decl(f_5077)
static void C_ccall f_5077(C_word c,C_word *av) C_noret;
C_noret_decl(f_5080)
static void C_ccall f_5080(C_word c,C_word *av) C_noret;
C_noret_decl(f_5083)
static void C_ccall f_5083(C_word c,C_word *av) C_noret;
C_noret_decl(f_5085)
static void C_fcall f_5085(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5093)
static void C_ccall f_5093(C_word c,C_word *av) C_noret;
C_noret_decl(f_5101)
static void C_ccall f_5101(C_word c,C_word *av) C_noret;
C_noret_decl(f_5104)
static void C_ccall f_5104(C_word c,C_word *av) C_noret;
C_noret_decl(f_5105)
static void C_fcall f_5105(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5109)
static void C_ccall f_5109(C_word c,C_word *av) C_noret;
C_noret_decl(f_5119)
static void C_fcall f_5119(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5128)
static void C_ccall f_5128(C_word c,C_word *av) C_noret;
C_noret_decl(f_5136)
static void C_ccall f_5136(C_word c,C_word *av) C_noret;
C_noret_decl(f_5151)
static void C_ccall f_5151(C_word c,C_word *av) C_noret;
C_noret_decl(f_5154)
static void C_ccall f_5154(C_word c,C_word *av) C_noret;
C_noret_decl(f_5157)
static void C_ccall f_5157(C_word c,C_word *av) C_noret;
C_noret_decl(f_5160)
static void C_ccall f_5160(C_word c,C_word *av) C_noret;
C_noret_decl(f_5167)
static void C_ccall f_5167(C_word c,C_word *av) C_noret;
C_noret_decl(f_5175)
static void C_ccall f_5175(C_word c,C_word *av) C_noret;
C_noret_decl(f_5179)
static void C_ccall f_5179(C_word c,C_word *av) C_noret;
C_noret_decl(f_5183)
static void C_ccall f_5183(C_word c,C_word *av) C_noret;
C_noret_decl(f_5187)
static void C_ccall f_5187(C_word c,C_word *av) C_noret;
C_noret_decl(f_5191)
static void C_ccall f_5191(C_word c,C_word *av) C_noret;
C_noret_decl(f_5195)
static void C_ccall f_5195(C_word c,C_word *av) C_noret;
C_noret_decl(f_5199)
static void C_ccall f_5199(C_word c,C_word *av) C_noret;
C_noret_decl(f_5203)
static void C_ccall f_5203(C_word c,C_word *av) C_noret;
C_noret_decl(f_5231)
static void C_ccall f_5231(C_word c,C_word *av) C_noret;
C_noret_decl(f_5243)
static void C_ccall f_5243(C_word c,C_word *av) C_noret;
C_noret_decl(f_5246)
static void C_ccall f_5246(C_word c,C_word *av) C_noret;
C_noret_decl(f_5248)
static void C_fcall f_5248(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5258)
static void C_ccall f_5258(C_word c,C_word *av) C_noret;
C_noret_decl(f_5279)
static void C_ccall f_5279(C_word c,C_word *av) C_noret;
C_noret_decl(f_5281)
static void C_fcall f_5281(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5306)
static void C_ccall f_5306(C_word c,C_word *av) C_noret;
C_noret_decl(f_5326)
static C_word C_fcall f_5326(C_word t0,C_word t1);
C_noret_decl(f_5361)
static C_word C_fcall f_5361(C_word t0);
C_noret_decl(f_5391)
static void C_ccall f_5391(C_word c,C_word *av) C_noret;
C_noret_decl(f_5393)
static void C_fcall f_5393(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_5399)
static void C_ccall f_5399(C_word c,C_word *av) C_noret;
C_noret_decl(f_5406)
static void C_ccall f_5406(C_word c,C_word *av) C_noret;
C_noret_decl(f_5411)
static void C_fcall f_5411(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5434)
static void C_ccall f_5434(C_word c,C_word *av) C_noret;
C_noret_decl(f_5443)
static void C_fcall f_5443(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_5453)
static void C_ccall f_5453(C_word c,C_word *av) C_noret;
C_noret_decl(f_5456)
static void C_ccall f_5456(C_word c,C_word *av) C_noret;
C_noret_decl(f_5485)
static void C_ccall f_5485(C_word c,C_word *av) C_noret;
C_noret_decl(f_5513)
static void C_ccall f_5513(C_word c,C_word *av) C_noret;
C_noret_decl(f_5528)
static void C_ccall f_5528(C_word c,C_word *av) C_noret;
C_noret_decl(f_5531)
static void C_ccall f_5531(C_word c,C_word *av) C_noret;
C_noret_decl(f_5534)
static void C_ccall f_5534(C_word c,C_word *av) C_noret;
C_noret_decl(f_5600)
static void C_ccall f_5600(C_word c,C_word *av) C_noret;
C_noret_decl(f_5606)
static void C_ccall f_5606(C_word c,C_word *av) C_noret;
C_noret_decl(f_5697)
static void C_ccall f_5697(C_word c,C_word *av) C_noret;
C_noret_decl(f_5704)
static void C_ccall f_5704(C_word c,C_word *av) C_noret;
C_noret_decl(f_5713)
static void C_ccall f_5713(C_word c,C_word *av) C_noret;
C_noret_decl(f_5716)
static void C_ccall f_5716(C_word c,C_word *av) C_noret;
C_noret_decl(f_5728)
static void C_ccall f_5728(C_word c,C_word *av) C_noret;
C_noret_decl(f_5733)
static void C_fcall f_5733(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5743)
static void C_ccall f_5743(C_word c,C_word *av) C_noret;
C_noret_decl(f_5746)
static void C_ccall f_5746(C_word c,C_word *av) C_noret;
C_noret_decl(f_5749)
static void C_ccall f_5749(C_word c,C_word *av) C_noret;
C_noret_decl(f_5758)
static void C_ccall f_5758(C_word c,C_word *av) C_noret;
C_noret_decl(f_5778)
static void C_ccall f_5778(C_word c,C_word *av) C_noret;
C_noret_decl(f_5781)
static void C_ccall f_5781(C_word c,C_word *av) C_noret;
C_noret_decl(f_5784)
static void C_ccall f_5784(C_word c,C_word *av) C_noret;
C_noret_decl(f_5796)
static void C_fcall f_5796(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5799)
static void C_ccall f_5799(C_word c,C_word *av) C_noret;
C_noret_decl(f_5808)
static void C_fcall f_5808(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_5839)
static void C_ccall f_5839(C_word c,C_word *av) C_noret;
C_noret_decl(f_5903)
static void C_ccall f_5903(C_word c,C_word *av) C_noret;
C_noret_decl(f_5907)
static void C_ccall f_5907(C_word c,C_word *av) C_noret;
C_noret_decl(f_5913)
static void C_ccall f_5913(C_word c,C_word *av) C_noret;
C_noret_decl(f_5932)
static void C_ccall f_5932(C_word c,C_word *av) C_noret;
C_noret_decl(f_5941)
static void C_ccall f_5941(C_word c,C_word *av) C_noret;
C_noret_decl(f_5948)
static void C_ccall f_5948(C_word c,C_word *av) C_noret;
C_noret_decl(f_6065)
static void C_ccall f_6065(C_word c,C_word *av) C_noret;
C_noret_decl(f_6071)
static void C_ccall f_6071(C_word c,C_word *av) C_noret;
C_noret_decl(f_6077)
static void C_ccall f_6077(C_word c,C_word *av) C_noret;
C_noret_decl(f_6090)
static void C_ccall f_6090(C_word c,C_word *av) C_noret;
C_noret_decl(f_6102)
static void C_ccall f_6102(C_word c,C_word *av) C_noret;
C_noret_decl(f_6105)
static void C_ccall f_6105(C_word c,C_word *av) C_noret;
C_noret_decl(f_6116)
static void C_fcall f_6116(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6124)
static void C_fcall f_6124(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6145)
static void C_ccall f_6145(C_word c,C_word *av) C_noret;
C_noret_decl(f_6154)
static void C_fcall f_6154(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6164)
static void C_ccall f_6164(C_word c,C_word *av) C_noret;
C_noret_decl(f_6199)
static void C_ccall f_6199(C_word c,C_word *av) C_noret;
C_noret_decl(f_6200)
static void C_fcall f_6200(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6204)
static void C_ccall f_6204(C_word c,C_word *av) C_noret;
C_noret_decl(f_6213)
static void C_fcall f_6213(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6223)
static void C_ccall f_6223(C_word c,C_word *av) C_noret;
C_noret_decl(f_6236)
static void C_ccall f_6236(C_word c,C_word *av) C_noret;
C_noret_decl(f_6241)
static void C_ccall f_6241(C_word c,C_word *av) C_noret;
C_noret_decl(f_6268)
static void C_fcall f_6268(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6278)
static void C_ccall f_6278(C_word c,C_word *av) C_noret;
C_noret_decl(f_6305)
static void C_ccall f_6305(C_word c,C_word *av) C_noret;
C_noret_decl(f_6309)
static void C_fcall f_6309(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6323)
static void C_fcall f_6323(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6331)
static void C_ccall f_6331(C_word c,C_word *av) C_noret;
C_noret_decl(f_6344)
static void C_ccall f_6344(C_word c,C_word *av) C_noret;
C_noret_decl(f_6350)
static void C_fcall f_6350(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6375)
static void C_ccall f_6375(C_word c,C_word *av) C_noret;
C_noret_decl(f_6388)
static void C_ccall f_6388(C_word c,C_word *av) C_noret;
C_noret_decl(f_6415)
static void C_ccall f_6415(C_word c,C_word *av) C_noret;
C_noret_decl(f_6423)
static void C_ccall f_6423(C_word c,C_word *av) C_noret;
C_noret_decl(f_6432)
static void C_fcall f_6432(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6434)
static void C_fcall f_6434(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_6437)
static void C_fcall f_6437(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6459)
static void C_ccall f_6459(C_word c,C_word *av) C_noret;
C_noret_decl(f_6466)
static void C_ccall f_6466(C_word c,C_word *av) C_noret;
C_noret_decl(f_6483)
static void C_ccall f_6483(C_word c,C_word *av) C_noret;
C_noret_decl(f_6512)
static void C_ccall f_6512(C_word c,C_word *av) C_noret;
C_noret_decl(f_6540)
static void C_fcall f_6540(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6545)
static void C_fcall f_6545(C_word t0,C_word t1) C_noret;
C_noret_decl(f_6580)
static void C_fcall f_6580(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_6583)
static void C_fcall f_6583(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_6587)
static void C_ccall f_6587(C_word c,C_word *av) C_noret;
C_noret_decl(f_6603)
static void C_ccall f_6603(C_word c,C_word *av) C_noret;
C_noret_decl(f_6615)
static void C_fcall f_6615(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6625)
static void C_ccall f_6625(C_word c,C_word *av) C_noret;
C_noret_decl(f_6628)
static void C_ccall f_6628(C_word c,C_word *av) C_noret;
C_noret_decl(f_6631)
static void C_ccall f_6631(C_word c,C_word *av) C_noret;
C_noret_decl(f_6634)
static void C_ccall f_6634(C_word c,C_word *av) C_noret;
C_noret_decl(f_6637)
static void C_ccall f_6637(C_word c,C_word *av) C_noret;
C_noret_decl(f_6640)
static void C_ccall f_6640(C_word c,C_word *av) C_noret;
C_noret_decl(f_6649)
static void C_fcall f_6649(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_6662)
static void C_ccall f_6662(C_word c,C_word *av) C_noret;
C_noret_decl(f_6665)
static void C_ccall f_6665(C_word c,C_word *av) C_noret;
C_noret_decl(f_6700)
static void C_fcall f_6700(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_6734)
static void C_fcall f_6734(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6744)
static void C_ccall f_6744(C_word c,C_word *av) C_noret;
C_noret_decl(f_6754)
static void C_ccall f_6754(C_word c,C_word *av) C_noret;
C_noret_decl(f_6757)
static void C_ccall f_6757(C_word c,C_word *av) C_noret;
C_noret_decl(f_6772)
static void C_ccall f_6772(C_word c,C_word *av) C_noret;
C_noret_decl(f_6776)
static void C_ccall f_6776(C_word c,C_word *av) C_noret;
C_noret_decl(f_6783)
static void C_ccall f_6783(C_word c,C_word *av) C_noret;
C_noret_decl(f_6785)
static void C_fcall f_6785(C_word t0,C_word t1) C_noret;
C_noret_decl(f_6788)
static void C_fcall f_6788(C_word t0,C_word t1) C_noret;
C_noret_decl(f_6794)
static void C_ccall f_6794(C_word c,C_word *av) C_noret;
C_noret_decl(f_6811)
static void C_fcall f_6811(C_word t0,C_word t1) C_noret;
C_noret_decl(f_6820)
static void C_fcall f_6820(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_6851)
static void C_ccall f_6851(C_word c,C_word *av) C_noret;
C_noret_decl(f_6854)
static void C_ccall f_6854(C_word c,C_word *av) C_noret;
C_noret_decl(f_6857)
static void C_ccall f_6857(C_word c,C_word *av) C_noret;
C_noret_decl(f_6860)
static void C_ccall f_6860(C_word c,C_word *av) C_noret;
C_noret_decl(f_6863)
static void C_ccall f_6863(C_word c,C_word *av) C_noret;
C_noret_decl(f_6866)
static void C_ccall f_6866(C_word c,C_word *av) C_noret;
C_noret_decl(f_6869)
static void C_ccall f_6869(C_word c,C_word *av) C_noret;
C_noret_decl(f_6872)
static void C_ccall f_6872(C_word c,C_word *av) C_noret;
C_noret_decl(f_6875)
static void C_ccall f_6875(C_word c,C_word *av) C_noret;
C_noret_decl(f_6878)
static void C_ccall f_6878(C_word c,C_word *av) C_noret;
C_noret_decl(f_6881)
static void C_ccall f_6881(C_word c,C_word *av) C_noret;
C_noret_decl(f_6894)
static void C_fcall f_6894(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_6904)
static void C_ccall f_6904(C_word c,C_word *av) C_noret;
C_noret_decl(f_6909)
static void C_fcall f_6909(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_6922)
static void C_ccall f_6922(C_word c,C_word *av) C_noret;
C_noret_decl(f_6925)
static void C_ccall f_6925(C_word c,C_word *av) C_noret;
C_noret_decl(f_6928)
static void C_ccall f_6928(C_word c,C_word *av) C_noret;
C_noret_decl(f_6931)
static void C_ccall f_6931(C_word c,C_word *av) C_noret;
C_noret_decl(f_6934)
static void C_ccall f_6934(C_word c,C_word *av) C_noret;
C_noret_decl(f_6968)
static void C_fcall f_6968(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_6978)
static void C_ccall f_6978(C_word c,C_word *av) C_noret;
C_noret_decl(f_7012)
static void C_ccall f_7012(C_word c,C_word *av) C_noret;
C_noret_decl(f_7015)
static void C_ccall f_7015(C_word c,C_word *av) C_noret;
C_noret_decl(f_7070)
static void C_fcall f_7070(C_word t0,C_word t1) C_noret;
C_noret_decl(f_7127)
static void C_fcall f_7127(C_word t0,C_word t1) C_noret;
C_noret_decl(f_7129)
static void C_fcall f_7129(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7140)
static void C_ccall f_7140(C_word c,C_word *av) C_noret;
C_noret_decl(f_7160)
static void C_ccall f_7160(C_word c,C_word *av) C_noret;
C_noret_decl(f_7163)
static void C_fcall f_7163(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7167)
static void C_ccall f_7167(C_word c,C_word *av) C_noret;
C_noret_decl(f_7170)
static void C_ccall f_7170(C_word c,C_word *av) C_noret;
C_noret_decl(f_7182)
static void C_fcall f_7182(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7207)
static void C_ccall f_7207(C_word c,C_word *av) C_noret;
C_noret_decl(f_7216)
static void C_fcall f_7216(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_7222)
static void C_fcall f_7222(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_7232)
static void C_ccall f_7232(C_word c,C_word *av) C_noret;
C_noret_decl(f_7244)
static void C_ccall f_7244(C_word c,C_word *av) C_noret;
C_noret_decl(f_7247)
static void C_ccall f_7247(C_word c,C_word *av) C_noret;
C_noret_decl(f_7250)
static void C_ccall f_7250(C_word c,C_word *av) C_noret;
C_noret_decl(f_7253)
static void C_ccall f_7253(C_word c,C_word *av) C_noret;
C_noret_decl(f_7256)
static void C_ccall f_7256(C_word c,C_word *av) C_noret;
C_noret_decl(f_7292)
static void C_ccall f_7292(C_word c,C_word *av) C_noret;
C_noret_decl(f_7299)
static void C_ccall f_7299(C_word c,C_word *av) C_noret;
C_noret_decl(f_7301)
static void C_fcall f_7301(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_7311)
static void C_ccall f_7311(C_word c,C_word *av) C_noret;
C_noret_decl(f_7354)
static void C_ccall f_7354(C_word c,C_word *av) C_noret;
C_noret_decl(f_7359)
static void C_fcall f_7359(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7365)
static void C_fcall f_7365(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7377)
static void C_fcall f_7377(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7414)
static void C_fcall f_7414(C_word t0,C_word t1) C_noret;
C_noret_decl(f_7420)
static void C_fcall f_7420(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7442)
static void C_fcall f_7442(C_word t0,C_word t1) C_noret;
C_noret_decl(f_7456)
static void C_ccall f_7456(C_word c,C_word *av) C_noret;
C_noret_decl(f_7477)
static void C_ccall f_7477(C_word c,C_word *av) C_noret;
C_noret_decl(f_7481)
static void C_ccall f_7481(C_word c,C_word *av) C_noret;
C_noret_decl(f_7485)
static void C_fcall f_7485(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7524)
static void C_ccall f_7524(C_word c,C_word *av) C_noret;
C_noret_decl(f_7532)
static void C_ccall f_7532(C_word c,C_word *av) C_noret;
C_noret_decl(f_7563)
static C_word C_fcall f_7563(C_word t0);
C_noret_decl(f_7593)
static void C_ccall f_7593(C_word c,C_word *av) C_noret;
C_noret_decl(f_7596)
static void C_ccall f_7596(C_word c,C_word *av) C_noret;
C_noret_decl(f_7599)
static void C_ccall f_7599(C_word c,C_word *av) C_noret;
C_noret_decl(f_7602)
static void C_ccall f_7602(C_word c,C_word *av) C_noret;
C_noret_decl(f_7605)
static void C_fcall f_7605(C_word t0,C_word t1) C_noret;
C_noret_decl(f_7608)
static void C_ccall f_7608(C_word c,C_word *av) C_noret;
C_noret_decl(f_7611)
static void C_fcall f_7611(C_word t0,C_word t1) C_noret;
C_noret_decl(f_7614)
static void C_ccall f_7614(C_word c,C_word *av) C_noret;
C_noret_decl(f_7623)
static void C_ccall f_7623(C_word c,C_word *av) C_noret;
C_noret_decl(f_7629)
static void C_ccall f_7629(C_word c,C_word *av) C_noret;
C_noret_decl(f_7631)
static void C_fcall f_7631(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7637)
static void C_fcall f_7637(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7645)
static void C_fcall f_7645(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7666)
static void C_ccall f_7666(C_word c,C_word *av) C_noret;
C_noret_decl(f_7682)
static void C_ccall f_7682(C_word c,C_word *av) C_noret;
C_noret_decl(f_7685)
static void C_ccall f_7685(C_word c,C_word *av) C_noret;
C_noret_decl(f_7688)
static void C_ccall f_7688(C_word c,C_word *av) C_noret;
C_noret_decl(f_7691)
static void C_ccall f_7691(C_word c,C_word *av) C_noret;
C_noret_decl(f_7697)
static void C_ccall f_7697(C_word c,C_word *av) C_noret;
C_noret_decl(f_7706)
static void C_ccall f_7706(C_word c,C_word *av) C_noret;
C_noret_decl(f_7728)
static void C_ccall f_7728(C_word c,C_word *av) C_noret;
C_noret_decl(f_7743)
static void C_fcall f_7743(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7750)
static void C_ccall f_7750(C_word c,C_word *av) C_noret;
C_noret_decl(f_7757)
static void C_ccall f_7757(C_word c,C_word *av) C_noret;
C_noret_decl(f_7759)
static void C_fcall f_7759(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7769)
static void C_ccall f_7769(C_word c,C_word *av) C_noret;
C_noret_decl(f_7776)
static void C_ccall f_7776(C_word c,C_word *av) C_noret;
C_noret_decl(f_7780)
static void C_ccall f_7780(C_word c,C_word *av) C_noret;
C_noret_decl(f_7782)
static void C_ccall f_7782(C_word c,C_word *av) C_noret;
C_noret_decl(f_7790)
static void C_ccall f_7790(C_word c,C_word *av) C_noret;
C_noret_decl(f_7800)
static void C_ccall f_7800(C_word c,C_word *av) C_noret;
C_noret_decl(f_7803)
static void C_ccall f_7803(C_word c,C_word *av) C_noret;
C_noret_decl(f_7806)
static void C_fcall f_7806(C_word t0,C_word t1) C_noret;
C_noret_decl(f_7809)
static void C_ccall f_7809(C_word c,C_word *av) C_noret;
C_noret_decl(f_7812)
static void C_fcall f_7812(C_word t0,C_word t1) C_noret;
C_noret_decl(f_7815)
static void C_ccall f_7815(C_word c,C_word *av) C_noret;
C_noret_decl(f_7818)
static void C_ccall f_7818(C_word c,C_word *av) C_noret;
C_noret_decl(f_7824)
static void C_ccall f_7824(C_word c,C_word *av) C_noret;
C_noret_decl(f_7827)
static void C_ccall f_7827(C_word c,C_word *av) C_noret;
C_noret_decl(f_7833)
static void C_ccall f_7833(C_word c,C_word *av) C_noret;
C_noret_decl(f_7836)
static void C_ccall f_7836(C_word c,C_word *av) C_noret;
C_noret_decl(f_7842)
static void C_ccall f_7842(C_word c,C_word *av) C_noret;
C_noret_decl(f_7846)
static void C_ccall f_7846(C_word c,C_word *av) C_noret;
C_noret_decl(f_7849)
static void C_ccall f_7849(C_word c,C_word *av) C_noret;
C_noret_decl(f_7852)
static void C_ccall f_7852(C_word c,C_word *av) C_noret;
C_noret_decl(f_7855)
static void C_ccall f_7855(C_word c,C_word *av) C_noret;
C_noret_decl(f_7858)
static void C_ccall f_7858(C_word c,C_word *av) C_noret;
C_noret_decl(f_7861)
static void C_ccall f_7861(C_word c,C_word *av) C_noret;
C_noret_decl(f_7864)
static void C_ccall f_7864(C_word c,C_word *av) C_noret;
C_noret_decl(f_7867)
static void C_ccall f_7867(C_word c,C_word *av) C_noret;
C_noret_decl(f_7870)
static void C_ccall f_7870(C_word c,C_word *av) C_noret;
C_noret_decl(f_7873)
static void C_fcall f_7873(C_word t0,C_word t1) C_noret;
C_noret_decl(f_7878)
static void C_fcall f_7878(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7906)
static void C_ccall f_7906(C_word c,C_word *av) C_noret;
C_noret_decl(f_7935)
static void C_ccall f_7935(C_word c,C_word *av) C_noret;
C_noret_decl(f_7947)
static void C_ccall f_7947(C_word c,C_word *av) C_noret;
C_noret_decl(f_7962)
static void C_ccall f_7962(C_word c,C_word *av) C_noret;
C_noret_decl(f_7981)
static void C_ccall f_7981(C_word c,C_word *av) C_noret;
C_noret_decl(f_7991)
static void C_ccall f_7991(C_word c,C_word *av) C_noret;
C_noret_decl(f_8006)
static void C_ccall f_8006(C_word c,C_word *av) C_noret;
C_noret_decl(f_8016)
static void C_ccall f_8016(C_word c,C_word *av) C_noret;
C_noret_decl(f_8026)
static void C_ccall f_8026(C_word c,C_word *av) C_noret;
C_noret_decl(f_8037)
static void C_ccall f_8037(C_word c,C_word *av) C_noret;
C_noret_decl(f_8041)
static void C_ccall f_8041(C_word c,C_word *av) C_noret;
C_noret_decl(f_8048)
static void C_ccall f_8048(C_word c,C_word *av) C_noret;
C_noret_decl(f_8050)
static void C_ccall f_8050(C_word c,C_word *av) C_noret;
C_noret_decl(f_8078)
static void C_ccall f_8078(C_word c,C_word *av) C_noret;
C_noret_decl(f_8082)
static void C_ccall f_8082(C_word c,C_word *av) C_noret;
C_noret_decl(f_8088)
static void C_ccall f_8088(C_word c,C_word *av) C_noret;
C_noret_decl(f_8091)
static void C_ccall f_8091(C_word c,C_word *av) C_noret;
C_noret_decl(f_8094)
static void C_ccall f_8094(C_word c,C_word *av) C_noret;
C_noret_decl(f_8097)
static void C_ccall f_8097(C_word c,C_word *av) C_noret;
C_noret_decl(f_8102)
static void C_fcall f_8102(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8115)
static void C_ccall f_8115(C_word c,C_word *av) C_noret;
C_noret_decl(f_8118)
static void C_ccall f_8118(C_word c,C_word *av) C_noret;
C_noret_decl(f_8133)
static void C_ccall f_8133(C_word c,C_word *av) C_noret;
C_noret_decl(f_8152)
static void C_ccall f_8152(C_word c,C_word *av) C_noret;
C_noret_decl(f_8164)
static void C_ccall f_8164(C_word c,C_word *av) C_noret;
C_noret_decl(f_8167)
static void C_ccall f_8167(C_word c,C_word *av) C_noret;
C_noret_decl(f_8181)
static void C_ccall f_8181(C_word c,C_word *av) C_noret;
C_noret_decl(f_8184)
static void C_ccall f_8184(C_word c,C_word *av) C_noret;
C_noret_decl(f_8187)
static void C_ccall f_8187(C_word c,C_word *av) C_noret;
C_noret_decl(f_8190)
static void C_ccall f_8190(C_word c,C_word *av) C_noret;
C_noret_decl(f_8193)
static void C_ccall f_8193(C_word c,C_word *av) C_noret;
C_noret_decl(f_8202)
static void C_ccall f_8202(C_word c,C_word *av) C_noret;
C_noret_decl(f_8205)
static void C_ccall f_8205(C_word c,C_word *av) C_noret;
C_noret_decl(f_8214)
static void C_ccall f_8214(C_word c,C_word *av) C_noret;
C_noret_decl(f_8217)
static void C_ccall f_8217(C_word c,C_word *av) C_noret;
C_noret_decl(f_8281)
static void C_ccall f_8281(C_word c,C_word *av) C_noret;
C_noret_decl(f_8288)
static void C_ccall f_8288(C_word c,C_word *av) C_noret;
C_noret_decl(f_8294)
static void C_ccall f_8294(C_word c,C_word *av) C_noret;
C_noret_decl(f_8301)
static void C_ccall f_8301(C_word c,C_word *av) C_noret;
C_noret_decl(f_8307)
static void C_ccall f_8307(C_word c,C_word *av) C_noret;
C_noret_decl(f_8309)
static void C_fcall f_8309(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8334)
static void C_ccall f_8334(C_word c,C_word *av) C_noret;
C_noret_decl(f_8343)
static void C_fcall f_8343(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8368)
static void C_ccall f_8368(C_word c,C_word *av) C_noret;
C_noret_decl(f_8377)
static void C_fcall f_8377(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8387)
static void C_ccall f_8387(C_word c,C_word *av) C_noret;
C_noret_decl(f_8400)
static void C_fcall f_8400(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8410)
static void C_ccall f_8410(C_word c,C_word *av) C_noret;
C_noret_decl(f_8423)
static void C_fcall f_8423(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8433)
static void C_ccall f_8433(C_word c,C_word *av) C_noret;
C_noret_decl(f_8447)
static void C_ccall f_8447(C_word c,C_word *av) C_noret;
C_noret_decl(f_8450)
static void C_ccall f_8450(C_word c,C_word *av) C_noret;
C_noret_decl(f_8453)
static void C_ccall f_8453(C_word c,C_word *av) C_noret;
C_noret_decl(f_8462)
static void C_ccall f_8462(C_word c,C_word *av) C_noret;
C_noret_decl(f_8465)
static void C_ccall f_8465(C_word c,C_word *av) C_noret;
C_noret_decl(f_8475)
static void C_ccall f_8475(C_word c,C_word *av) C_noret;
C_noret_decl(f_8482)
static void C_ccall f_8482(C_word c,C_word *av) C_noret;
C_noret_decl(f_8492)
static void C_ccall f_8492(C_word c,C_word *av) C_noret;
C_noret_decl(f_8498)
static void C_ccall f_8498(C_word c,C_word *av) C_noret;
C_noret_decl(f_8501)
static void C_ccall f_8501(C_word c,C_word *av) C_noret;
C_noret_decl(f_8506)
static void C_fcall f_8506(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8531)
static void C_ccall f_8531(C_word c,C_word *av) C_noret;
C_noret_decl(f_8542)
static void C_ccall f_8542(C_word c,C_word *av) C_noret;
C_noret_decl(f_8551)
static void C_ccall f_8551(C_word c,C_word *av) C_noret;
C_noret_decl(f_8557)
static void C_ccall f_8557(C_word c,C_word *av) C_noret;
C_noret_decl(f_8560)
static void C_ccall f_8560(C_word c,C_word *av) C_noret;
C_noret_decl(f_8563)
static void C_ccall f_8563(C_word c,C_word *av) C_noret;
C_noret_decl(f_8566)
static void C_ccall f_8566(C_word c,C_word *av) C_noret;
C_noret_decl(f_8575)
static void C_ccall f_8575(C_word c,C_word *av) C_noret;
C_noret_decl(f_8640)
static void C_ccall f_8640(C_word c,C_word *av) C_noret;
C_noret_decl(f_8653)
static void C_ccall f_8653(C_word c,C_word *av) C_noret;
C_noret_decl(f_8657)
static void C_ccall f_8657(C_word c,C_word *av) C_noret;
C_noret_decl(f_8661)
static void C_ccall f_8661(C_word c,C_word *av) C_noret;
C_noret_decl(f_8667)
static void C_ccall f_8667(C_word c,C_word *av) C_noret;
C_noret_decl(f_8673)
static void C_ccall f_8673(C_word c,C_word *av) C_noret;
C_noret_decl(f_8675)
static void C_ccall f_8675(C_word c,C_word *av) C_noret;
C_noret_decl(f_8681)
static void C_ccall f_8681(C_word c,C_word *av) C_noret;
C_noret_decl(f_8685)
static void C_ccall f_8685(C_word c,C_word *av) C_noret;
C_noret_decl(f_8694)
static void C_ccall f_8694(C_word c,C_word *av) C_noret;
C_noret_decl(f_8700)
static void C_ccall f_8700(C_word c,C_word *av) C_noret;
C_noret_decl(f_8704)
static void C_fcall f_8704(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8708)
static void C_ccall f_8708(C_word c,C_word *av) C_noret;
C_noret_decl(f_8721)
static void C_ccall f_8721(C_word c,C_word *av) C_noret;
C_noret_decl(f_8723)
static void C_ccall f_8723(C_word c,C_word *av) C_noret;
C_noret_decl(f_8731)
static void C_ccall f_8731(C_word c,C_word *av) C_noret;
C_noret_decl(f_8734)
static void C_ccall f_8734(C_word c,C_word *av) C_noret;
C_noret_decl(f_8741)
static void C_ccall f_8741(C_word c,C_word *av) C_noret;
C_noret_decl(f_8745)
static void C_ccall f_8745(C_word c,C_word *av) C_noret;
C_noret_decl(f_8754)
static void C_ccall f_8754(C_word c,C_word *av) C_noret;
C_noret_decl(f_8756)
static void C_ccall f_8756(C_word c,C_word *av) C_noret;
C_noret_decl(C_toplevel)
C_externexport void C_ccall C_toplevel(C_word c,C_word *av) C_noret;

C_noret_decl(trf_3048)
static void C_ccall trf_3048(C_word c,C_word *av) C_noret;
static void C_ccall trf_3048(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3048(t0,t1,t2);}

C_noret_decl(trf_3123)
static void C_ccall trf_3123(C_word c,C_word *av) C_noret;
static void C_ccall trf_3123(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3123(t0,t1,t2);}

C_noret_decl(trf_3848)
static void C_ccall trf_3848(C_word c,C_word *av) C_noret;
static void C_ccall trf_3848(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_3848(t0,t1);}

C_noret_decl(trf_3949)
static void C_ccall trf_3949(C_word c,C_word *av) C_noret;
static void C_ccall trf_3949(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_3949(t0,t1);}

C_noret_decl(trf_3978)
static void C_ccall trf_3978(C_word c,C_word *av) C_noret;
static void C_ccall trf_3978(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3978(t0,t1,t2);}

C_noret_decl(trf_4000)
static void C_ccall trf_4000(C_word c,C_word *av) C_noret;
static void C_ccall trf_4000(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4000(t0,t1,t2);}

C_noret_decl(trf_4057)
static void C_ccall trf_4057(C_word c,C_word *av) C_noret;
static void C_ccall trf_4057(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4057(t0,t1,t2);}

C_noret_decl(trf_4107)
static void C_ccall trf_4107(C_word c,C_word *av) C_noret;
static void C_ccall trf_4107(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4107(t0,t1,t2);}

C_noret_decl(trf_4117)
static void C_ccall trf_4117(C_word c,C_word *av) C_noret;
static void C_ccall trf_4117(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4117(t0,t1);}

C_noret_decl(trf_4162)
static void C_ccall trf_4162(C_word c,C_word *av) C_noret;
static void C_ccall trf_4162(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4162(t0,t1,t2);}

C_noret_decl(trf_4206)
static void C_ccall trf_4206(C_word c,C_word *av) C_noret;
static void C_ccall trf_4206(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4206(t0,t1);}

C_noret_decl(trf_4514)
static void C_ccall trf_4514(C_word c,C_word *av) C_noret;
static void C_ccall trf_4514(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4514(t0,t1,t2);}

C_noret_decl(trf_4549)
static void C_ccall trf_4549(C_word c,C_word *av) C_noret;
static void C_ccall trf_4549(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4549(t0,t1,t2);}

C_noret_decl(trf_4570)
static void C_ccall trf_4570(C_word c,C_word *av) C_noret;
static void C_ccall trf_4570(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4570(t0,t1,t2);}

C_noret_decl(trf_4821)
static void C_ccall trf_4821(C_word c,C_word *av) C_noret;
static void C_ccall trf_4821(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4821(t0,t1,t2);}

C_noret_decl(trf_4957)
static void C_ccall trf_4957(C_word c,C_word *av) C_noret;
static void C_ccall trf_4957(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4957(t0,t1,t2);}

C_noret_decl(trf_4967)
static void C_ccall trf_4967(C_word c,C_word *av) C_noret;
static void C_ccall trf_4967(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4967(t0,t1);}

C_noret_decl(trf_5025)
static void C_ccall trf_5025(C_word c,C_word *av) C_noret;
static void C_ccall trf_5025(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_5025(t0,t1,t2,t3);}

C_noret_decl(trf_5063)
static void C_ccall trf_5063(C_word c,C_word *av) C_noret;
static void C_ccall trf_5063(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5063(t0,t1,t2);}

C_noret_decl(trf_5085)
static void C_ccall trf_5085(C_word c,C_word *av) C_noret;
static void C_ccall trf_5085(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5085(t0,t1);}

C_noret_decl(trf_5105)
static void C_ccall trf_5105(C_word c,C_word *av) C_noret;
static void C_ccall trf_5105(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5105(t0,t1,t2);}

C_noret_decl(trf_5119)
static void C_ccall trf_5119(C_word c,C_word *av) C_noret;
static void C_ccall trf_5119(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5119(t0,t1);}

C_noret_decl(trf_5248)
static void C_ccall trf_5248(C_word c,C_word *av) C_noret;
static void C_ccall trf_5248(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5248(t0,t1,t2);}

C_noret_decl(trf_5281)
static void C_ccall trf_5281(C_word c,C_word *av) C_noret;
static void C_ccall trf_5281(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5281(t0,t1,t2);}

C_noret_decl(trf_5393)
static void C_ccall trf_5393(C_word c,C_word *av) C_noret;
static void C_ccall trf_5393(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_5393(t0,t1,t2,t3);}

C_noret_decl(trf_5411)
static void C_ccall trf_5411(C_word c,C_word *av) C_noret;
static void C_ccall trf_5411(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5411(t0,t1,t2);}

C_noret_decl(trf_5443)
static void C_ccall trf_5443(C_word c,C_word *av) C_noret;
static void C_ccall trf_5443(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_5443(t0,t1,t2,t3);}

C_noret_decl(trf_5733)
static void C_ccall trf_5733(C_word c,C_word *av) C_noret;
static void C_ccall trf_5733(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5733(t0,t1,t2);}

C_noret_decl(trf_5796)
static void C_ccall trf_5796(C_word c,C_word *av) C_noret;
static void C_ccall trf_5796(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5796(t0,t1);}

C_noret_decl(trf_5808)
static void C_ccall trf_5808(C_word c,C_word *av) C_noret;
static void C_ccall trf_5808(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_5808(t0,t1,t2,t3);}

C_noret_decl(trf_6116)
static void C_ccall trf_6116(C_word c,C_word *av) C_noret;
static void C_ccall trf_6116(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6116(t0,t1,t2);}

C_noret_decl(trf_6124)
static void C_ccall trf_6124(C_word c,C_word *av) C_noret;
static void C_ccall trf_6124(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6124(t0,t1,t2);}

C_noret_decl(trf_6154)
static void C_ccall trf_6154(C_word c,C_word *av) C_noret;
static void C_ccall trf_6154(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6154(t0,t1,t2);}

C_noret_decl(trf_6200)
static void C_ccall trf_6200(C_word c,C_word *av) C_noret;
static void C_ccall trf_6200(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6200(t0,t1,t2);}

C_noret_decl(trf_6213)
static void C_ccall trf_6213(C_word c,C_word *av) C_noret;
static void C_ccall trf_6213(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6213(t0,t1,t2);}

C_noret_decl(trf_6268)
static void C_ccall trf_6268(C_word c,C_word *av) C_noret;
static void C_ccall trf_6268(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6268(t0,t1,t2);}

C_noret_decl(trf_6309)
static void C_ccall trf_6309(C_word c,C_word *av) C_noret;
static void C_ccall trf_6309(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6309(t0,t1,t2);}

C_noret_decl(trf_6323)
static void C_ccall trf_6323(C_word c,C_word *av) C_noret;
static void C_ccall trf_6323(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6323(t0,t1,t2);}

C_noret_decl(trf_6350)
static void C_ccall trf_6350(C_word c,C_word *av) C_noret;
static void C_ccall trf_6350(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6350(t0,t1,t2);}

C_noret_decl(trf_6432)
static void C_ccall trf_6432(C_word c,C_word *av) C_noret;
static void C_ccall trf_6432(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6432(t0,t1,t2);}

C_noret_decl(trf_6434)
static void C_ccall trf_6434(C_word c,C_word *av) C_noret;
static void C_ccall trf_6434(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_6434(t0,t1,t2,t3);}

C_noret_decl(trf_6437)
static void C_ccall trf_6437(C_word c,C_word *av) C_noret;
static void C_ccall trf_6437(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6437(t0,t1,t2);}

C_noret_decl(trf_6540)
static void C_ccall trf_6540(C_word c,C_word *av) C_noret;
static void C_ccall trf_6540(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6540(t0,t1,t2);}

C_noret_decl(trf_6545)
static void C_ccall trf_6545(C_word c,C_word *av) C_noret;
static void C_ccall trf_6545(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_6545(t0,t1);}

C_noret_decl(trf_6580)
static void C_ccall trf_6580(C_word c,C_word *av) C_noret;
static void C_ccall trf_6580(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_6580(t0,t1,t2,t3,t4);}

C_noret_decl(trf_6583)
static void C_ccall trf_6583(C_word c,C_word *av) C_noret;
static void C_ccall trf_6583(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_6583(t0,t1,t2,t3,t4);}

C_noret_decl(trf_6615)
static void C_ccall trf_6615(C_word c,C_word *av) C_noret;
static void C_ccall trf_6615(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6615(t0,t1,t2);}

C_noret_decl(trf_6649)
static void C_ccall trf_6649(C_word c,C_word *av) C_noret;
static void C_ccall trf_6649(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_6649(t0,t1,t2,t3);}

C_noret_decl(trf_6700)
static void C_ccall trf_6700(C_word c,C_word *av) C_noret;
static void C_ccall trf_6700(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_6700(t0,t1,t2,t3);}

C_noret_decl(trf_6734)
static void C_ccall trf_6734(C_word c,C_word *av) C_noret;
static void C_ccall trf_6734(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6734(t0,t1,t2);}

C_noret_decl(trf_6785)
static void C_ccall trf_6785(C_word c,C_word *av) C_noret;
static void C_ccall trf_6785(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_6785(t0,t1);}

C_noret_decl(trf_6788)
static void C_ccall trf_6788(C_word c,C_word *av) C_noret;
static void C_ccall trf_6788(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_6788(t0,t1);}

C_noret_decl(trf_6811)
static void C_ccall trf_6811(C_word c,C_word *av) C_noret;
static void C_ccall trf_6811(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_6811(t0,t1);}

C_noret_decl(trf_6820)
static void C_ccall trf_6820(C_word c,C_word *av) C_noret;
static void C_ccall trf_6820(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_6820(t0,t1,t2,t3);}

C_noret_decl(trf_6894)
static void C_ccall trf_6894(C_word c,C_word *av) C_noret;
static void C_ccall trf_6894(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_6894(t0,t1,t2,t3);}

C_noret_decl(trf_6909)
static void C_ccall trf_6909(C_word c,C_word *av) C_noret;
static void C_ccall trf_6909(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_6909(t0,t1,t2,t3);}

C_noret_decl(trf_6968)
static void C_ccall trf_6968(C_word c,C_word *av) C_noret;
static void C_ccall trf_6968(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_6968(t0,t1,t2,t3);}

C_noret_decl(trf_7070)
static void C_ccall trf_7070(C_word c,C_word *av) C_noret;
static void C_ccall trf_7070(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_7070(t0,t1);}

C_noret_decl(trf_7127)
static void C_ccall trf_7127(C_word c,C_word *av) C_noret;
static void C_ccall trf_7127(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_7127(t0,t1);}

C_noret_decl(trf_7129)
static void C_ccall trf_7129(C_word c,C_word *av) C_noret;
static void C_ccall trf_7129(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7129(t0,t1,t2);}

C_noret_decl(trf_7163)
static void C_ccall trf_7163(C_word c,C_word *av) C_noret;
static void C_ccall trf_7163(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7163(t0,t1,t2);}

C_noret_decl(trf_7182)
static void C_ccall trf_7182(C_word c,C_word *av) C_noret;
static void C_ccall trf_7182(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7182(t0,t1,t2);}

C_noret_decl(trf_7216)
static void C_ccall trf_7216(C_word c,C_word *av) C_noret;
static void C_ccall trf_7216(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_7216(t0,t1,t2,t3);}

C_noret_decl(trf_7222)
static void C_ccall trf_7222(C_word c,C_word *av) C_noret;
static void C_ccall trf_7222(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_7222(t0,t1,t2,t3);}

C_noret_decl(trf_7301)
static void C_ccall trf_7301(C_word c,C_word *av) C_noret;
static void C_ccall trf_7301(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_7301(t0,t1,t2,t3);}

C_noret_decl(trf_7359)
static void C_ccall trf_7359(C_word c,C_word *av) C_noret;
static void C_ccall trf_7359(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7359(t0,t1,t2);}

C_noret_decl(trf_7365)
static void C_ccall trf_7365(C_word c,C_word *av) C_noret;
static void C_ccall trf_7365(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7365(t0,t1,t2);}

C_noret_decl(trf_7377)
static void C_ccall trf_7377(C_word c,C_word *av) C_noret;
static void C_ccall trf_7377(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7377(t0,t1,t2);}

C_noret_decl(trf_7414)
static void C_ccall trf_7414(C_word c,C_word *av) C_noret;
static void C_ccall trf_7414(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_7414(t0,t1);}

C_noret_decl(trf_7420)
static void C_ccall trf_7420(C_word c,C_word *av) C_noret;
static void C_ccall trf_7420(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7420(t0,t1,t2);}

C_noret_decl(trf_7442)
static void C_ccall trf_7442(C_word c,C_word *av) C_noret;
static void C_ccall trf_7442(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_7442(t0,t1);}

C_noret_decl(trf_7485)
static void C_ccall trf_7485(C_word c,C_word *av) C_noret;
static void C_ccall trf_7485(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7485(t0,t1,t2);}

C_noret_decl(trf_7605)
static void C_ccall trf_7605(C_word c,C_word *av) C_noret;
static void C_ccall trf_7605(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_7605(t0,t1);}

C_noret_decl(trf_7611)
static void C_ccall trf_7611(C_word c,C_word *av) C_noret;
static void C_ccall trf_7611(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_7611(t0,t1);}

C_noret_decl(trf_7631)
static void C_ccall trf_7631(C_word c,C_word *av) C_noret;
static void C_ccall trf_7631(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7631(t0,t1,t2);}

C_noret_decl(trf_7637)
static void C_ccall trf_7637(C_word c,C_word *av) C_noret;
static void C_ccall trf_7637(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7637(t0,t1,t2);}

C_noret_decl(trf_7645)
static void C_ccall trf_7645(C_word c,C_word *av) C_noret;
static void C_ccall trf_7645(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7645(t0,t1,t2);}

C_noret_decl(trf_7743)
static void C_ccall trf_7743(C_word c,C_word *av) C_noret;
static void C_ccall trf_7743(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7743(t0,t1,t2);}

C_noret_decl(trf_7759)
static void C_ccall trf_7759(C_word c,C_word *av) C_noret;
static void C_ccall trf_7759(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7759(t0,t1,t2);}

C_noret_decl(trf_7806)
static void C_ccall trf_7806(C_word c,C_word *av) C_noret;
static void C_ccall trf_7806(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_7806(t0,t1);}

C_noret_decl(trf_7812)
static void C_ccall trf_7812(C_word c,C_word *av) C_noret;
static void C_ccall trf_7812(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_7812(t0,t1);}

C_noret_decl(trf_7873)
static void C_ccall trf_7873(C_word c,C_word *av) C_noret;
static void C_ccall trf_7873(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_7873(t0,t1);}

C_noret_decl(trf_7878)
static void C_ccall trf_7878(C_word c,C_word *av) C_noret;
static void C_ccall trf_7878(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7878(t0,t1,t2);}

C_noret_decl(trf_8102)
static void C_ccall trf_8102(C_word c,C_word *av) C_noret;
static void C_ccall trf_8102(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8102(t0,t1,t2);}

C_noret_decl(trf_8309)
static void C_ccall trf_8309(C_word c,C_word *av) C_noret;
static void C_ccall trf_8309(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8309(t0,t1,t2);}

C_noret_decl(trf_8343)
static void C_ccall trf_8343(C_word c,C_word *av) C_noret;
static void C_ccall trf_8343(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8343(t0,t1,t2);}

C_noret_decl(trf_8377)
static void C_ccall trf_8377(C_word c,C_word *av) C_noret;
static void C_ccall trf_8377(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8377(t0,t1,t2);}

C_noret_decl(trf_8400)
static void C_ccall trf_8400(C_word c,C_word *av) C_noret;
static void C_ccall trf_8400(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8400(t0,t1,t2);}

C_noret_decl(trf_8423)
static void C_ccall trf_8423(C_word c,C_word *av) C_noret;
static void C_ccall trf_8423(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8423(t0,t1,t2);}

C_noret_decl(trf_8506)
static void C_ccall trf_8506(C_word c,C_word *av) C_noret;
static void C_ccall trf_8506(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8506(t0,t1,t2);}

C_noret_decl(trf_8704)
static void C_ccall trf_8704(C_word c,C_word *av) C_noret;
static void C_ccall trf_8704(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8704(t0,t1,t2);}

/* f9340 in k4692 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f9340(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f9340,c,av);}
/* csi.scm:349: scheme#string-append */
t2=*((C_word*)lf[18]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=lf[80];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* f9344 in k4692 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f9344(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f9344,c,av);}
/* csi.scm:349: scheme#string-append */
t2=*((C_word*)lf[18]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_retrieve2(lf[9],C_text("chicken.csi#default-editor"));
av2[3]=lf[80];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* f9432 in doloop1851 in k7871 in k7868 in k7865 in k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in ... */
static void C_ccall f9432(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f9432,c,av);}
/* csi.scm:1093: ##sys#write-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[90]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[90]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=C_make_character(10);
av2[3]=*((C_word*)lf[84]+1);
tp(4,av2);}}

/* f9473 in k8162 in k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in ... */
static void C_ccall f9473(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f9473,c,av);}
/* csi.scm:144: chicken.base#print */
t2=*((C_word*)lf[107]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[315];
av2[3]=t1;
av2[4]=lf[316];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* f9499 in k8445 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in ... */
static void C_ccall f9499(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f9499,c,av);}
/* csi.scm:1043: chicken.base#case-sensitive */
t2=C_fast_retrieve(lf[325]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* f9503 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in ... */
static void C_ccall f9503(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f9503,c,av);}
/* csi.scm:144: chicken.base#print */
t2=*((C_word*)lf[107]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[315];
av2[3]=t1;
av2[4]=lf[316];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k2515 */
static void C_ccall f_2517(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2517,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2520,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_eval_toplevel(2,av2);}}

/* k2518 in k2515 */
static void C_ccall f_2520(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2520,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2523,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_expand_toplevel(2,av2);}}

/* k2521 in k2518 in k2515 */
static void C_ccall f_2523(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2523,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2526,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_extras_toplevel(2,av2);}}

/* k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_2526(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2526,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2529,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_file_toplevel(2,av2);}}

/* k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_2529(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2529,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2532,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_internal_toplevel(2,av2);}}

/* k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_2532(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2532,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2535,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_pathname_toplevel(2,av2);}}

/* k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_2535(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2535,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2538,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_port_toplevel(2,av2);}}

/* k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_2538(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2538,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2541,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_posix_toplevel(2,av2);}}

/* k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_2541(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2541,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2544,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_repl_toplevel(2,av2);}}

/* k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_2544(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2544,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2547,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_data_2dstructures_toplevel(2,av2);}}

/* k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_2547(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_2547,c,av);}
a=C_alloc(13);
t2=C_a_i_provide(&a,1,lf[0]);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2550,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_8756,tmp=(C_word)a,a+=2,tmp);
/* csi.scm:43: ##sys#with-environment */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[404]);
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[404]+1);
av2[1]=t3;
av2[2]=t4;
tp(3,av2);}}

/* k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_2550(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_2550,c,av);}
a=C_alloc(6);
t2=C_mutate(&lf[1] /* (set! chicken.csi#constant680 ...) */,lf[2]);
t3=C_set_block_item(lf[3] /* ##sys#repl-print-length-limit */,0,C_fix(2048));
t4=C_a_i_cons(&a,2,lf[4],C_fast_retrieve(lf[5]));
t5=C_mutate((C_word*)lf[5]+1 /* (set! ##sys#features ...) */,t4);
t6=C_set_block_item(lf[6] /* ##sys#notices-enabled */,0,C_SCHEME_TRUE);
t7=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3840,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csi.scm:81: chicken.base#make-parameter */
t8=C_fast_retrieve(lf[400]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t8;
av2[1]=t7;
av2[2]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t8+1)))(3,av2);}}

/* loop in loop in k8279 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in ... */
static void C_fcall f_3048(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_3048,3,t0,t1,t2);}
a=C_alloc(4);
if(C_truep(C_i_nullp(t2))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_i_car(t2);
if(C_truep(C_i_string_equal_p(((C_word*)t0)[2],t3))){
/* mini-srfi-1.scm:107: loop */
t7=t1;
t8=C_u_i_cdr(t2);
t1=t7;
t2=t8;
goto loop;}
else{
t4=C_u_i_car(t2);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3075,a[2]=t1,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* mini-srfi-1.scm:109: loop */
t7=t5;
t8=C_u_i_cdr(t2);
t1=t7;
t2=t8;
goto loop;}}}

/* k3073 in loop in loop in k8279 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in ... */
static void C_ccall f_3075(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_3075,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* loop in k8279 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in ... */
static void C_fcall f_3123(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,0,3)))){
C_save_and_reclaim_args((void *)trf_3123,3,t0,t1,t2);}
a=C_alloc(16);
if(C_truep(C_i_nullp(t2))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_i_car(t2);
t4=C_u_i_cdr(t2);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3137,a[2]=t4,a[3]=t1,a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3150,a[2]=((C_word*)t0)[2],a[3]=t5,tmp=(C_word)a,a+=4,tmp);
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3048,a[2]=t3,a[3]=t8,tmp=(C_word)a,a+=4,tmp));
t10=((C_word*)t8)[1];
f_3048(t10,t6,t4);}}

/* k3135 in loop in k8279 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in ... */
static void C_ccall f_3137(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_3137,c,av);}
a=C_alloc(3);
t2=C_i_equalp(((C_word*)t0)[2],t1);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=(C_truep(t2)?((C_word*)t0)[4]:C_a_i_cons(&a,2,((C_word*)t0)[5],t1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k3148 in loop in k8279 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in ... */
static void C_ccall f_3150(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_3150,c,av);}
/* mini-srfi-1.scm:123: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3123(t2,((C_word*)t0)[3],t1);}

/* k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_3840(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_3840,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[7]+1 /* (set! chicken.csi#editor-command ...) */,t1);
t3=lf[8] /* chicken.csi#selected-frame */ =C_SCHEME_FALSE;;
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3845,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csi.scm:85: chicken.process-context#get-environment-variable */
t5=C_fast_retrieve(lf[23]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=lf[399];
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_3845(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_3845,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3848,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(t1)){
t3=t2;
f_3848(t3,t1);}
else{
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8745,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:86: chicken.process-context#get-environment-variable */
t4=C_fast_retrieve(lf[23]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[398];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}}

/* k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_fcall f_3848(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,3)))){
C_save_and_reclaim_args((void *)trf_3848,2,t0,t1);}
a=C_alloc(7);
t2=C_mutate(&lf[9] /* (set! chicken.csi#default-editor ...) */,t1);
t3=C_mutate(&lf[10] /* (set! chicken.csi#dirseparator? ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_3888,tmp=(C_word)a,a+=2,tmp));
t4=C_mutate(&lf[12] /* (set! chicken.csi#chop-separator ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_3903,tmp=(C_word)a,a+=2,tmp));
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3930,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* ##sys#make-string */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[394]);
C_word av2[4];
av2[0]=*((C_word*)lf[394]+1);
av2[1]=t5;
av2[2]=C_fix(256);
av2[3]=C_make_character(32);
tp(4,av2);}}

/* k3852 in k8496 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in ... */
static void C_ccall f_3854(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,2)))){
C_save_and_reclaim((void *)f_3854,c,av);}
a=C_alloc(15);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3857,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3864,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=C_a_i_cons(&a,2,lf[354],C_SCHEME_END_OF_LIST);
t5=C_a_i_cons(&a,2,C_retrieve2(lf[1],C_text("chicken.csi#constant680")),t4);
t6=C_a_i_cons(&a,2,lf[355],t5);
/* csi.scm:43: ##sys#print-to-string */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[356]);
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[356]+1);
av2[1]=t3;
av2[2]=t6;
tp(3,av2);}}

/* k3855 in k3852 in k8496 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in ... */
static void C_ccall f_3857(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_3857,c,av);}
/* csi.scm:121: scheme#display */
t2=*((C_word*)lf[94]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[353];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k3862 in k3852 in k8496 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in ... */
static void C_ccall f_3864(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_3864,c,av);}
/* csi.scm:116: scheme#display */
t2=*((C_word*)lf[94]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* chicken.csi#dirseparator? in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_3888(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3888,c,av);}
if(C_truep(*((C_word*)lf[11]+1))){
t3=C_i_char_equalp(t2,C_make_character(92));
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=(C_truep(t3)?t3:C_i_char_equalp(t2,C_make_character(47)));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_char_equalp(t2,C_make_character(47));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* chicken.csi#chop-separator in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_3903(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_3903,c,av);}
a=C_alloc(10);
t3=C_block_size(t2);
t4=C_a_i_fixnum_difference(&a,2,t3,C_fix(1));
t5=C_i_string_ref(t2,t4);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3917,a[2]=t1,a[3]=t2,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_fixnum_greaterp(t4,C_fix(0)))){
/* csi.scm:158: dirseparator? */
t7=C_retrieve2(lf[10],C_text("chicken.csi#dirseparator\077"));{
C_word *av2=av;
av2[0]=t7;
av2[1]=t6;
av2[2]=t5;
f_3888(3,av2);}}
else{
t7=t1;{
C_word *av2=av;
av2[0]=t7;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}}

/* k3915 in chicken.csi#chop-separator in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_3917(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_3917,c,av);}
if(C_truep(t1)){
/* csi.scm:159: substring */
t2=*((C_word*)lf[13]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=C_fix(0);
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_3930(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(49,c,3)))){
C_save_and_reclaim((void *)f_3930,c,av);}
a=C_alloc(49);
t2=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_3949,tmp=(C_word)a,a+=2,tmp);
t3=C_mutate(&lf[17] /* (set! chicken.csi#lookup-script-file ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4000,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp));
t4=C_SCHEME_UNDEFINED;
t5=C_a_i_vector(&a,32,t4,t4,t4,t4,t4,t4,t4,t4,t4,t4,t4,t4,t4,t4,t4,t4,t4,t4,t4,t4,t4,t4,t4,t4,t4,t4,t4,t4,t4,t4,t4,t4);
t6=C_mutate(&lf[25] /* (set! chicken.csi#history-list ...) */,t5);
t7=lf[26] /* chicken.csi#history-count */ =C_fix(1);;
t8=C_fast_retrieve(lf[27]);
t9=C_mutate(&lf[28] /* (set! chicken.csi#history-add ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4107,a[2]=t8,tmp=(C_word)a,a+=3,tmp));
t10=C_mutate(&lf[30] /* (set! chicken.csi#history-ref ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_4206,tmp=(C_word)a,a+=2,tmp));
t11=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4273,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t12=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_8723,tmp=(C_word)a,a+=2,tmp);
/* csi.scm:246: chicken.repl#repl-prompt */
t13=C_fast_retrieve(lf[393]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t13;
av2[1]=t11;
av2[2]=t12;
((C_proc)(void*)(*((C_word*)t13+1)))(3,av2);}}

/* addext in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_fcall f_3949(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_3949,2,t1,t2);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3956,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* csi.scm:169: chicken.file#file-exists? */
t4=C_fast_retrieve(lf[14]);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k3954 in addext in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_3956(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_3956,c,av);}
a=C_alloc(3);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3959,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[15]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[15]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=lf[16];
tp(4,av2);}}}

/* k3957 in k3954 in addext in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_3959(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_3959,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3965,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* csi.scm:172: chicken.file#file-exists? */
t3=C_fast_retrieve(lf[14]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k3963 in k3957 in k3954 in addext in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_3965(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3965,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(C_truep(t1)?((C_word*)t0)[3]:C_SCHEME_FALSE);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* loop in k4014 in k4002 in chicken.csi#lookup-script-file in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_fcall f_3978(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_3978,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_fixnum_greater_or_equal_p(t2,((C_word*)t0)[2]))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3991,a[2]=t1,a[3]=t2,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* csi.scm:177: proc */
t4=C_retrieve2(lf[10],C_text("chicken.csi#dirseparator\077"));{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_subchar(((C_word*)t0)[4],t2);
f_3888(3,av2);}}}

/* k3989 in loop in k4014 in k4002 in chicken.csi#lookup-script-file in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_3991(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_3991,c,av);}
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
/* csi.scm:178: loop */
t2=((C_word*)((C_word*)t0)[4])[1];
f_3978(t2,((C_word*)t0)[2],C_fixnum_plus(((C_word*)t0)[3],C_fix(1)));}}

/* chicken.csi#lookup-script-file in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_fcall f_4000(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_4000,3,t0,t1,t2);}
a=C_alloc(6);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4004,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=t1,a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
/* csi.scm:180: chicken.process-context#get-environment-variable */
t4=C_fast_retrieve(lf[23]);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[24];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k4002 in chicken.csi#lookup-script-file in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4004(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_4004,c,av);}
a=C_alloc(7);
t2=C_block_size(((C_word*)t0)[2]);
if(C_truep(C_fixnum_greaterp(t2,C_fix(0)))){
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4016,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[5],a[6]=t1,tmp=(C_word)a,a+=7,tmp);
/* csi.scm:182: dirseparator? */
t4=C_retrieve2(lf[10],C_text("chicken.csi#dirseparator\077"));{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_i_string_ref(((C_word*)t0)[2],C_fix(0));
f_3888(3,av2);}}
else{
t3=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k4014 in k4002 in chicken.csi#lookup-script-file in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4016(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,3)))){
C_save_and_reclaim((void *)f_4016,c,av);}
a=C_alloc(14);
if(C_truep(t1)){
/* csi.scm:182: addext */
f_3949(((C_word*)t0)[3],((C_word*)t0)[4]);}
else{
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4025,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
t3=C_retrieve2(lf[10],C_text("chicken.csi#dirseparator\077"));
t4=C_block_size(((C_word*)t0)[4]);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3978,a[2]=t4,a[3]=t6,a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp));
t8=((C_word*)t6)[1];
f_3978(t8,t2,C_fix(0));}}

/* k4023 in k4014 in k4002 in chicken.csi#lookup-script-file in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4025(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_4025,c,av);}
a=C_alloc(10);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4028,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=C_a_i_bytevector(&a,1,C_fix(3));
t4=(C_truep(((C_word*)t0)[5])?C_i_foreign_block_argumentp(((C_word*)t0)[5]):C_SCHEME_FALSE);
t5=C_fix(256);
/* csi.scm:167: ##sys#peek-nonnull-c-string */
t6=*((C_word*)lf[20]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t2;
av2[2]=stub712(t3,t4,t5);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4042,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* csi.scm:186: addext */
f_3949(t2,((C_word*)t0)[4]);}}

/* k4026 in k4023 in k4014 in k4002 in chicken.csi#lookup-script-file in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4028(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_4028,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4035,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4039,a[2]=t2,a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
/* csi.scm:185: chop-separator */
t4=C_retrieve2(lf[12],C_text("chicken.csi#chop-separator"));{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t1;
f_3903(3,av2);}}

/* k4033 in k4026 in k4023 in k4014 in k4002 in chicken.csi#lookup-script-file in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 in ... */
static void C_ccall f_4035(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4035,c,av);}
/* csi.scm:185: addext */
f_3949(((C_word*)t0)[3],t1);}

/* k4037 in k4026 in k4023 in k4014 in k4002 in chicken.csi#lookup-script-file in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 in ... */
static void C_ccall f_4039(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_4039,c,av);}
/* csi.scm:185: scheme#string-append */
t2=*((C_word*)lf[18]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=lf[19];
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k4040 in k4023 in k4014 in k4002 in chicken.csi#lookup-script-file in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4042(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_4042,c,av);}
a=C_alloc(5);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4048,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[15]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[15]+1);
av2[1]=t2;
av2[2]=lf[22];
av2[3]=((C_word*)t0)[5];
tp(4,av2);}}}

/* k4046 in k4040 in k4023 in k4014 in k4002 in chicken.csi#lookup-script-file in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 in ... */
static void C_ccall f_4048(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_4048,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4055,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* csi.scm:189: ##sys#split-path */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[21]);
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[21]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
tp(3,av2);}}

/* k4053 in k4046 in k4040 in k4023 in k4014 in k4002 in chicken.csi#lookup-script-file in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in ... */
static void C_ccall f_4055(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_4055,c,av);}
a=C_alloc(7);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4057,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp));
t5=((C_word*)t3)[1];
f_4057(t5,((C_word*)t0)[4],t1);}

/* loop in k4053 in k4046 in k4040 in k4023 in k4014 in k4002 in chicken.csi#lookup-script-file in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in ... */
static void C_fcall f_4057(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,2)))){
C_save_and_reclaim_args((void *)trf_4057,3,t0,t1,t2);}
a=C_alloc(10);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4067,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t2,a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4084,a[2]=t3,a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
/* csi.scm:191: chop-separator */
t5=C_retrieve2(lf[12],C_text("chicken.csi#chop-separator"));{
C_word av2[3];
av2[0]=t5;
av2[1]=t4;
av2[2]=C_slot(t2,C_fix(0));
f_3903(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k4065 in loop in k4053 in k4046 in k4040 in k4023 in k4014 in k4002 in chicken.csi#lookup-script-file in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in ... */
static void C_ccall f_4067(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_4067,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4070,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* csi.scm:192: addext */
f_3949(t2,t1);}

/* k4068 in k4065 in loop in k4053 in k4046 in k4040 in k4023 in k4014 in k4002 in chicken.csi#lookup-script-file in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in ... */
static void C_ccall f_4070(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4070,c,av);}
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
/* csi.scm:193: loop */
t2=((C_word*)((C_word*)t0)[3])[1];
f_4057(t2,((C_word*)t0)[2],C_slot(((C_word*)t0)[4],C_fix(1)));}}

/* k4082 in loop in k4053 in k4046 in k4040 in k4023 in k4014 in k4002 in chicken.csi#lookup-script-file in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in ... */
static void C_ccall f_4084(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4084,c,av);}
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[15]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[15]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
tp(4,av2);}}

/* chicken.csi#history-add in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_fcall f_4107(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,3)))){
C_save_and_reclaim_args((void *)trf_4107,3,t0,t1,t2);}
a=C_alloc(7);
t3=C_i_nullp(t2);
t4=(C_truep(t3)?*((C_word*)lf[29]+1):C_slot(t2,C_fix(0)));
t5=C_block_size(C_retrieve2(lf[25],C_text("chicken.csi#history-list")));
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4117,a[2]=t4,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_fixnum_greater_or_equal_p(C_retrieve2(lf[26],C_text("chicken.csi#history-count")),t5))){
t7=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4131,a[2]=t6,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:208: vector-resize */
t8=((C_word*)t0)[2];{
C_word av2[4];
av2[0]=t8;
av2[1]=t7;
av2[2]=C_retrieve2(lf[25],C_text("chicken.csi#history-list"));
av2[3]=C_fixnum_times(C_fix(2),t5);
((C_proc)(void*)(*((C_word*)t8+1)))(4,av2);}}
else{
t7=t6;
f_4117(t7,C_SCHEME_UNDEFINED);}}

/* k4115 in chicken.csi#history-add in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_fcall f_4117(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,1)))){
C_save_and_reclaim_args((void *)trf_4117,2,t0,t1);}
t2=C_i_vector_set(C_retrieve2(lf[25],C_text("chicken.csi#history-list")),C_retrieve2(lf[26],C_text("chicken.csi#history-count")),((C_word*)t0)[2]);
t3=C_fixnum_plus(C_retrieve2(lf[26],C_text("chicken.csi#history-count")),C_fix(1));
t4=lf[26] /* chicken.csi#history-count */ =t3;;
t5=((C_word*)t0)[3];{
C_word av2[2];
av2[0]=t5;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* k4129 in chicken.csi#history-add in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4131(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4131,c,av);}
t2=C_mutate(&lf[25] /* (set! chicken.csi#history-list ...) */,t1);
t3=((C_word*)t0)[2];
f_4117(t3,t2);}

/* doloop769 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_fcall f_4162(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_4162,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_greater_or_equalp(t2,C_retrieve2(lf[26],C_text("chicken.csi#history-count"))))){
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=*((C_word*)lf[84]+1);
t4=*((C_word*)lf[84]+1);
t5=C_i_check_port_2(*((C_word*)lf[84]+1),C_fix(2),C_SCHEME_TRUE,lf[85]);
t6=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4175,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* csi.scm:221: ##sys#write-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[90]);
C_word av2[4];
av2[0]=*((C_word*)lf[90]+1);
av2[1]=t6;
av2[2]=C_make_character(35);
av2[3]=*((C_word*)lf[84]+1);
tp(4,av2);}}}

/* k4173 in doloop769 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4175(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_4175,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4178,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* csi.scm:221: ##sys#print */
t3=*((C_word*)lf[87]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4176 in k4173 in doloop769 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4178(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_4178,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4181,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* csi.scm:221: ##sys#print */
t3=*((C_word*)lf[87]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[89];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4179 in k4176 in k4173 in doloop769 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 in ... */
static void C_ccall f_4181(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_4181,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4184,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4196,a[2]=((C_word*)t0)[4],tmp=(C_word)a,a+=3,tmp);
/* csi.scm:222: ##sys#with-print-length-limit */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[88]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[88]+1);
av2[1]=t2;
av2[2]=C_fix(80);
av2[3]=t3;
tp(4,av2);}}

/* k4182 in k4179 in k4176 in k4173 in doloop769 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in ... */
static void C_ccall f_4184(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_4184,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4187,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* csi.scm:226: newline */
t3=*((C_word*)lf[86]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k4185 in k4182 in k4179 in k4176 in k4173 in doloop769 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in ... */
static void C_ccall f_4187(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4187,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_4162(t2,((C_word*)t0)[3],C_fixnum_plus(((C_word*)t0)[4],C_fix(1)));}

/* a4195 in k4179 in k4176 in k4173 in doloop769 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in ... */
static void C_ccall f_4196(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_4196,c,av);}
/* csi.scm:225: ##sys#print */
t2=*((C_word*)lf[87]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=C_i_vector_ref(C_retrieve2(lf[25],C_text("chicken.csi#history-list")),((C_word*)t0)[2]);
av2[3]=C_SCHEME_TRUE;
av2[4]=*((C_word*)lf[84]+1);
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* chicken.csi#history-ref in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_fcall f_4206(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_4206,2,t1,t2);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4210,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* csi.scm:229: scheme#inexact->exact */
t4=*((C_word*)lf[33]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k4208 in chicken.csi#history-ref in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4210(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4210,c,av);}
t2=C_fixnum_greaterp(t1,C_fix(0));
t3=(C_truep(t2)?C_fixnum_less_or_equal_p(t1,C_retrieve2(lf[26],C_text("chicken.csi#history-count"))):C_SCHEME_FALSE);
if(C_truep(t3)){
t4=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_i_vector_ref(C_retrieve2(lf[25],C_text("chicken.csi#history-list")),t1);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
/* csi.scm:232: ##sys#error */
t4=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[32];
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}}

/* ##sys#user-read-hook in doloop1851 in k7871 in k7868 in k7865 in k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in ... */
static void C_ccall f_4233(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_4233,c,av);}
a=C_alloc(3);
t4=C_i_char_equalp(C_make_character(41),t2);
t5=(C_truep(t4)?t4:C_u_i_char_whitespacep(t2));
if(C_truep(t5)){
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4250,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:241: history-ref */
f_4206(t6,C_fixnum_difference(C_retrieve2(lf[26],C_text("chicken.csi#history-count")),C_fix(1)));}
else{
/* csi.scm:242: old-hook */
t6=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t6;
av2[1]=t1;
av2[2]=t2;
av2[3]=t3;
((C_proc)C_fast_retrieve_proc(t6))(4,av2);}}}

/* k4248 in ##sys#user-read-hook in doloop1851 in k7871 in k7868 in k7865 in k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in ... */
static void C_ccall f_4250(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_4250,c,av);}
a=C_alloc(6);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,2,lf[276],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* ##sys#sharp-number-hook in doloop1851 in k7871 in k7868 in k7865 in k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in ... */
static void C_ccall f_4262(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4262,c,av);}
a=C_alloc(3);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4270,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:244: history-ref */
f_4206(t4,t3);}

/* k4268 in ##sys#sharp-number-hook in doloop1851 in k7871 in k7868 in k7865 in k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in ... */
static void C_ccall f_4270(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_4270,c,av);}
a=C_alloc(6);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,2,lf[276],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4273(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,4)))){
C_save_and_reclaim((void *)f_4273,c,av);}
a=C_alloc(18);
t2=C_set_block_item(lf[34] /* ##sys#break-on-error */,0,C_SCHEME_FALSE);
t3=C_fast_retrieve(lf[35]);
t4=C_mutate((C_word*)lf[35]+1 /* (set! ##sys#read-prompt-hook ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4285,a[2]=t3,tmp=(C_word)a,a+=3,tmp));
t5=lf[38] /* chicken.csi#command-table */ =C_SCHEME_END_OF_LIST;;
t6=C_mutate((C_word*)lf[39]+1 /* (set! chicken.csi#toplevel-command ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_4298,tmp=(C_word)a,a+=2,tmp));
t7=C_fast_retrieve(lf[41]);
t8=C_fast_retrieve(lf[42]);
t9=C_fast_retrieve(lf[43]);
t10=C_fast_retrieve(lf[44]);
t11=C_fast_retrieve(lf[45]);
t12=*((C_word*)lf[46]+1);
t13=C_mutate(&lf[47] /* (set! chicken.csi#csi-eval ...) */,(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4348,a[2]=t11,a[3]=t10,a[4]=t9,a[5]=t8,a[6]=t7,a[7]=t12,tmp=(C_word)a,a+=8,tmp));
t14=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4917,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t15=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_8681,tmp=(C_word)a,a+=2,tmp);
/* csi.scm:415: toplevel-command */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[39]);
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[39]+1);
av2[1]=t14;
av2[2]=lf[386];
av2[3]=t15;
av2[4]=lf[387];
tp(5,av2);}}

/* ##sys#read-prompt-hook in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4285(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_4285,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4292,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=C_i_tty_forcedp();
if(C_truep(t3)){
if(C_truep(t3)){
/* csi.scm:268: old */
t4=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t1;
((C_proc)C_fast_retrieve_proc(t4))(2,av2);}}
else{
t4=C_SCHEME_UNDEFINED;
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}
else{
/* csi.scm:261: ##sys#tty-port? */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[36]);
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[36]+1);
av2[1]=t2;
av2[2]=*((C_word*)lf[37]+1);
tp(3,av2);}}}

/* k4290 in ##sys#read-prompt-hook in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4292(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4292,c,av);}
if(C_truep(t1)){
/* csi.scm:268: old */
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)C_fast_retrieve_proc(t2))(2,av2);}}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* chicken.csi#toplevel-command in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4298(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(c<4) C_bad_min_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_4298,c,av);}
a=C_alloc(12);
t4=C_rest_nullp(c,4);
t5=(C_truep(t4)?C_SCHEME_FALSE:C_get_rest_arg(c,4,av,4,t0));
t6=C_i_check_symbol_2(t2,lf[40]);
t7=(C_truep(t5)?C_i_check_string_2(t5,lf[40]):C_SCHEME_UNDEFINED);
t8=C_i_assq(t2,C_retrieve2(lf[38],C_text("chicken.csi#command-table")));
if(C_truep(t8)){
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4318,a[2]=t3,a[3]=t5,tmp=(C_word)a,a+=4,tmp);
t10=(
/* csi.scm:275: g834 */
  f_4318(C_a_i(&a,6),t9,t8)
);
t11=t1;{
C_word *av2=av;
av2[0]=t11;
av2[1]=*((C_word*)lf[29]+1);
((C_proc)(void*)(*((C_word*)t11+1)))(2,av2);}}
else{
t9=C_a_i_list3(&a,3,t2,t3,t5);
t10=C_a_i_cons(&a,2,t9,C_retrieve2(lf[38],C_text("chicken.csi#command-table")));
t11=C_mutate(&lf[38] /* (set! chicken.csi#command-table ...) */,t10);
t12=t1;{
C_word *av2=av;
av2[0]=t12;
av2[1]=*((C_word*)lf[29]+1);
((C_proc)(void*)(*((C_word*)t12+1)))(2,av2);}}}

/* g834 in chicken.csi#toplevel-command in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static C_word C_fcall f_4318(C_word *a,C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_stack_overflow_check;{}
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],((C_word*)t0)[3]);
return(C_i_set_cdr(t1,t2));}

/* chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4348(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word t24;
C_word t25;
C_word t26;
C_word t27;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_4348,c,av);}
a=C_alloc(10);
if(C_truep(C_eofp(t2))){
/* csi.scm:294: chicken.base#exit */
t3=C_fast_retrieve(lf[48]);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_i_pairp(t2);
t4=(C_truep(t3)?C_eqp(lf[49],C_slot(t2,C_fix(0))):C_SCHEME_FALSE);
if(C_truep(t4)){
t5=C_i_cadr(t2);
t6=C_i_assq(t5,C_retrieve2(lf[38],C_text("chicken.csi#command-table")));
if(C_truep(t6)){
t7=C_i_cadr(t6);
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4381,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:299: g867 */
t9=t7;{
C_word *av2=av;
av2[0]=t9;
av2[1]=t8;
((C_proc)C_fast_retrieve_proc(t9))(2,av2);}}
else{
t7=C_eqp(t5,lf[50]);
if(C_truep(t7)){
t8=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4396,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* csi.scm:306: read */
t9=*((C_word*)lf[52]+1);{
C_word *av2=av;
av2[0]=t9;
av2[1]=t8;
((C_proc)(void*)(*((C_word*)t9+1)))(2,av2);}}
else{
t8=C_eqp(t5,lf[53]);
if(C_truep(t8)){
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4419,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* csi.scm:310: read */
t10=*((C_word*)lf[52]+1);{
C_word *av2=av;
av2[0]=t10;
av2[1]=t9;
((C_proc)(void*)(*((C_word*)t10+1)))(2,av2);}}
else{
t9=C_eqp(t5,lf[55]);
if(C_truep(t9)){
t10=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4437,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:315: read */
t11=*((C_word*)lf[52]+1);{
C_word *av2=av;
av2[0]=t11;
av2[1]=t10;
((C_proc)(void*)(*((C_word*)t11+1)))(2,av2);}}
else{
t10=C_eqp(t5,lf[57]);
if(C_truep(t10)){
t11=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4452,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:319: read */
t12=*((C_word*)lf[52]+1);{
C_word *av2=av;
av2[0]=t12;
av2[1]=t11;
((C_proc)(void*)(*((C_word*)t12+1)))(2,av2);}}
else{
t11=C_eqp(t5,lf[59]);
if(C_truep(t11)){
t12=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4467,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:323: read */
t13=*((C_word*)lf[52]+1);{
C_word *av2=av;
av2[0]=t13;
av2[1]=t12;
((C_proc)(void*)(*((C_word*)t13+1)))(2,av2);}}
else{
t12=C_eqp(t5,lf[60]);
if(C_truep(t12)){
/* csi.scm:328: report */
t13=C_retrieve2(lf[61],C_text("chicken.csi#report"));
f_5063(t13,t1,C_SCHEME_END_OF_LIST);}
else{
t13=C_eqp(t5,lf[62]);
if(C_truep(t13)){
/* csi.scm:329: chicken.repl#quit */
t14=C_fast_retrieve(lf[63]);{
C_word *av2=av;
av2[0]=t14;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t14+1)))(2,av2);}}
else{
t14=C_eqp(t5,lf[64]);
if(C_truep(t14)){
t15=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4506,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t16=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4539,a[2]=((C_word*)t0)[4],a[3]=t15,tmp=(C_word)a,a+=4,tmp);
/* csi.scm:331: read-line */
t17=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t17;
av2[1]=t16;
((C_proc)(void*)(*((C_word*)t17+1)))(2,av2);}}
else{
t15=C_eqp(t5,lf[66]);
if(C_truep(t15)){
t16=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4548,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[6],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t17=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4595,a[2]=((C_word*)t0)[4],a[3]=t16,tmp=(C_word)a,a+=4,tmp);
/* csi.scm:335: read-line */
t18=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t18;
av2[1]=t17;
((C_proc)(void*)(*((C_word*)t18+1)))(2,av2);}}
else{
t16=C_eqp(t5,lf[70]);
if(C_truep(t16)){
t17=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4604,a[2]=((C_word*)t0)[7],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* csi.scm:339: read */
t18=*((C_word*)lf[52]+1);{
C_word *av2=av;
av2[0]=t18;
av2[1]=t17;
((C_proc)(void*)(*((C_word*)t18+1)))(2,av2);}}
else{
t17=C_eqp(t5,lf[74]);
if(C_truep(t17)){
if(C_truep(C_fast_retrieve(lf[75]))){
t18=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4656,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t19=C_a_i_list1(&a,1,C_fast_retrieve(lf[75]));
/* csi.scm:345: history-add */
t20=C_retrieve2(lf[28],C_text("chicken.csi#history-add"));
f_4107(t20,t18,t19);}
else{
t18=C_SCHEME_UNDEFINED;
t19=t1;{
C_word *av2=av;
av2[0]=t19;
av2[1]=t18;
((C_proc)(void*)(*((C_word*)t19+1)))(2,av2);}}}
else{
t18=C_eqp(t5,lf[76]);
if(C_truep(t18)){
t19=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4672,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t20=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4690,a[2]=t19,tmp=(C_word)a,a+=3,tmp);
t21=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4694,a[2]=t20,a[3]=((C_word*)t0)[5],tmp=(C_word)a,a+=4,tmp);
/* csi.scm:350: editor-command */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[7]);
C_word *av2=av;
av2[0]=*((C_word*)lf[7]+1);
av2[1]=t21;
tp(2,av2);}}
else{
t19=C_eqp(t5,lf[81]);
if(C_truep(t19)){
t20=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4710,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t21=*((C_word*)lf[29]+1);
/* csi.scm:214: scheme#vector-fill! */
t22=*((C_word*)lf[82]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t22;
av2[1]=t20;
av2[2]=C_retrieve2(lf[25],C_text("chicken.csi#history-list"));
av2[3]=*((C_word*)lf[29]+1);
((C_proc)(void*)(*((C_word*)t22+1)))(4,av2);}}
else{
t20=C_eqp(t5,lf[83]);
if(C_truep(t20)){
t21=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4722,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t22=C_SCHEME_UNDEFINED;
t23=(*a=C_VECTOR_TYPE|1,a[1]=t22,tmp=(C_word)a,a+=2,tmp);
t24=C_set_block_item(t23,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4162,a[2]=t23,tmp=(C_word)a,a+=3,tmp));
t25=((C_word*)t23)[1];
f_4162(t25,t21,C_fix(1));}
else{
t21=C_eqp(t5,lf[91]);
if(C_truep(t21)){
t22=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4734,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:361: show-frameinfo */
f_6785(t22,C_retrieve2(lf[8],C_text("chicken.csi#selected-frame")));}
else{
t22=C_eqp(t5,lf[93]);
if(C_truep(t22)){
t23=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4746,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t24=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4753,a[2]=t23,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:364: read */
t25=*((C_word*)lf[52]+1);{
C_word *av2=av;
av2[0]=t25;
av2[1]=t24;
((C_proc)(void*)(*((C_word*)t25+1)))(2,av2);}}
else{
t23=C_eqp(t5,lf[97]);
if(C_truep(t23)){
t24=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4766,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:367: read */
t25=*((C_word*)lf[52]+1);{
C_word *av2=av;
av2[0]=t25;
av2[1]=t24;
((C_proc)(void*)(*((C_word*)t25+1)))(2,av2);}}
else{
t24=C_eqp(t5,lf[105]);
if(C_truep(t24)){
t25=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4775,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:369: read-line */
t26=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t26;
av2[1]=t25;
((C_proc)(void*)(*((C_word*)t26+1)))(2,av2);}}
else{
t25=C_eqp(t5,lf[106]);
if(C_truep(t25)){
t26=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4794,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:374: display */
t27=*((C_word*)lf[94]+1);{
C_word *av2=av;
av2[0]=t27;
av2[1]=t26;
av2[2]=lf[109];
((C_proc)(void*)(*((C_word*)t27+1)))(3,av2);}}
else{
t26=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4845,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:405: printf */
t27=*((C_word*)lf[77]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t27;
av2[1]=t26;
av2[2]=lf[110];
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t27+1)))(4,av2);}}}}}}}}}}}}}}}}}}}}}}
else{
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4891,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4897,a[2]=((C_word*)t0)[7],tmp=(C_word)a,a+=3,tmp);
/* csi.scm:408: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=t1;
av2[2]=t5;
av2[3]=t6;
C_call_with_values(4,av2);}}}}

/* k4379 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4381(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4381,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=*((C_word*)lf[29]+1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k4394 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4396(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_4396,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4399,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4406,a[2]=((C_word*)t0)[3],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4410,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:307: expand */
t5=((C_word*)t0)[4];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k4397 in k4394 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4399(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4399,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=*((C_word*)lf[29]+1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k4404 in k4394 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4406(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4406,c,av);}
/* csi.scm:307: pretty-print */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k4408 in k4394 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4410(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4410,c,av);}
/* csi.scm:307: chicken.syntax#strip-syntax */
t2=C_fast_retrieve(lf[51]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k4417 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4419(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_4419,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4422,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* csi.scm:311: eval */
t3=*((C_word*)lf[54]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4420 in k4417 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4422(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4422,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4425,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csi.scm:312: pretty-print */
t3=((C_word*)t0)[3];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4423 in k4420 in k4417 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4425(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4425,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=*((C_word*)lf[29]+1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k4435 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4437(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4437,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4440,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csi.scm:316: eval */
t3=*((C_word*)lf[54]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4438 in k4435 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4440(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4440,c,av);}
/* csi.scm:317: describe */
t2=C_retrieve2(lf[56],C_text("chicken.csi#describe"));
f_5393(t2,((C_word*)t0)[2],t1,C_SCHEME_END_OF_LIST);}

/* k4450 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4452(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4452,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4455,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csi.scm:320: eval */
t3=*((C_word*)lf[54]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4453 in k4450 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4455(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4455,c,av);}
/* csi.scm:321: dump */
f_6432(((C_word*)t0)[2],t1,C_SCHEME_END_OF_LIST);}

/* k4465 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4467(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_4467,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4470,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* csi.scm:324: read */
t3=*((C_word*)lf[52]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k4468 in k4465 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4470(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_4470,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4473,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* csi.scm:325: eval */
t3=*((C_word*)lf[54]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4471 in k4468 in k4465 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4473(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_4473,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4476,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* csi.scm:326: eval */
t3=*((C_word*)lf[54]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4474 in k4471 in k4468 in k4465 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 in ... */
static void C_ccall f_4476(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_4476,c,av);}
a=C_alloc(3);
/* csi.scm:327: dump */
f_6432(((C_word*)t0)[2],((C_word*)t0)[3],C_a_i_list(&a,1,t1));}

/* k4504 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4506(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_4506,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4509,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4514,a[2]=t4,tmp=(C_word)a,a+=3,tmp));
t6=((C_word*)t4)[1];
f_4514(t6,t2,t1);}

/* k4507 in k4504 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4509(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4509,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=*((C_word*)lf[29]+1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* for-each-loop892 in k4504 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_fcall f_4514(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_4514,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4524,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* csi.scm:332: g893 */
t4=*((C_word*)lf[65]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k4522 in for-each-loop892 in k4504 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4524(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4524,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_4514(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k4537 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4539(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4539,c,av);}
/* csi.scm:331: string-split */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k4546 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4548(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,3)))){
C_save_and_reclaim((void *)f_4548,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4549,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4565,a[2]=((C_word*)t0)[4],tmp=(C_word)a,a+=3,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4570,a[2]=t5,a[3]=t2,tmp=(C_word)a,a+=4,tmp));
t7=((C_word*)t5)[1];
f_4570(t7,t3,t1);}

/* g912 in k4546 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_fcall f_4549(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,0,4)))){
C_save_and_reclaim_args((void *)trf_4549,3,t0,t1,t2);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4555,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csi.scm:336: g927 */
t4=((C_word*)t0)[3];{
C_word av2[5];
av2[0]=t4;
av2[1]=t1;
av2[2]=t2;
av2[3]=lf[69];
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* a4554 in g912 in k4546 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4555(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4555,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4559,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:336: pretty-print */
t4=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k4557 in a4554 in g912 in k4546 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 in ... */
static void C_ccall f_4559(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4559,c,av);}
/* csi.scm:336: chicken.base#print* */
t2=*((C_word*)lf[67]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[68];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k4563 in k4546 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4565(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4565,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=*((C_word*)lf[29]+1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* for-each-loop911 in k4546 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_fcall f_4570(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_4570,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4580,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* csi.scm:336: g912 */
t4=((C_word*)t0)[3];
f_4549(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k4578 in for-each-loop911 in k4546 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4580(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4580,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_4570(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k4593 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4595(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4595,c,av);}
/* csi.scm:335: string-split */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k4602 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4604(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_4604,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4609,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4637,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csi.scm:340: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[3];
av2[2]=t2;
av2[3]=t3;
C_call_with_values(4,av2);}}

/* a4608 in k4602 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4609(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_4609,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4613,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* csi.scm:340: ##sys#start-timer */
t3=*((C_word*)lf[73]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k4611 in a4608 in k4602 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4613(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_4613,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4618,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_4624,tmp=(C_word)a,a+=2,tmp);
/* csi.scm:340: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[3];
av2[2]=t2;
av2[3]=t3;
C_call_with_values(4,av2);}}

/* a4617 in k4611 in a4608 in k4602 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 in ... */
static void C_ccall f_4618(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4618,c,av);}
/* csi.scm:340: eval */
t2=*((C_word*)lf[54]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* a4623 in k4611 in a4608 in k4602 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 in ... */
static void C_ccall f_4624(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +7,c,2)))){
C_save_and_reclaim((void*)f_4624,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+7);
t2=C_build_rest(&a,c,2,av);
C_word t3;
C_word t4;
C_word t5;
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4628,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4635,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:340: ##sys#stop-timer */
t5=*((C_word*)lf[72]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* k4626 in a4623 in k4611 in a4608 in k4602 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in ... */
static void C_ccall f_4628(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4628,c,av);}{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
C_apply_values(3,av2);}}

/* k4633 in a4623 in k4611 in a4608 in k4602 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in ... */
static void C_ccall f_4635(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4635,c,av);}
/* csi.scm:340: ##sys#display-times */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[71]);
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[71]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
tp(3,av2);}}

/* a4636 in k4602 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4637(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +5,c,2)))){
C_save_and_reclaim((void*)f_4637,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+5);
t2=C_build_rest(&a,c,2,av);
C_word t3;
C_word t4;
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4641,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* csi.scm:341: history-add */
t4=C_retrieve2(lf[28],C_text("chicken.csi#history-add"));
f_4107(t4,t3,t2);}

/* k4639 in a4636 in k4602 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4641(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4641,c,av);}{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
C_apply(4,av2);}}

/* k4654 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4656(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4656,c,av);}
/* csi.scm:346: describe */
t2=C_retrieve2(lf[56],C_text("chicken.csi#describe"));
f_5393(t2,((C_word*)t0)[2],C_fast_retrieve(lf[75]),C_SCHEME_END_OF_LIST);}

/* k4670 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4672(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4672,c,av);}
t2=C_eqp(t1,C_fix(0));
if(C_truep(C_i_not(t2))){
/* csi.scm:353: printf */
t3=*((C_word*)lf[77]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[78];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}
else{
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k4688 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4690(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4690,c,av);}
/* csi.scm:348: chicken.process#system */
t2=C_fast_retrieve(lf[79]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k4692 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4694(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_4694,c,av);}
a=C_alloc(4);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f9340,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* csi.scm:351: read-line */
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=C_retrieve2(lf[9],C_text("chicken.csi#default-editor"));
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f9344,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csi.scm:351: read-line */
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k4708 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4710(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4710,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=*((C_word*)lf[29]+1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k4720 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4722(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4722,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=*((C_word*)lf[29]+1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k4732 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4734(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4734,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=*((C_word*)lf[29]+1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k4744 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4746(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4746,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=*((C_word*)lf[29]+1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k4751 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4753(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_4753,c,av);}
a=C_alloc(4);
t2=C_i_numberp(t1);
t3=C_i_not(t2);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7070,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
if(C_truep(t3)){
t5=t4;
f_7070(t5,t3);}
else{
t5=C_i_not(C_fast_retrieve(lf[96]));
if(C_truep(t5)){
t6=t4;
f_7070(t6,t5);}
else{
t6=C_fixnum_lessp(t1,C_fix(0));
t7=t4;
f_7070(t7,(C_truep(t6)?t6:C_fixnum_greater_or_equal_p(t1,C_i_length(C_fast_retrieve(lf[96])))));}}}

/* k4764 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4766(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_4766,c,av);}
a=C_alloc(7);
t2=C_fast_retrieve(lf[96]);
t3=(C_truep(C_fast_retrieve(lf[96]))?C_fast_retrieve(lf[96]):C_SCHEME_END_OF_LIST);
t4=C_i_length(t3);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7127,a[2]=t3,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_i_symbolp(t1))){
t6=t5;
f_7127(t6,C_slot(t1,C_fix(1)));}
else{
if(C_truep(C_i_stringp(t1))){
t6=t5;
f_7127(t6,t1);}
else{
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7354,a[2]=t5,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:865: display */
t7=*((C_word*)lf[94]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t7;
av2[1]=t6;
av2[2]=lf[104];
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}}}

/* k4773 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4775(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4775,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4778,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csi.scm:370: chicken.process#system */
t3=C_fast_retrieve(lf[79]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4776 in k4773 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4778(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_4778,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4781,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=C_a_i_list1(&a,1,t1);
/* csi.scm:371: history-add */
t4=C_retrieve2(lf[28],C_text("chicken.csi#history-add"));
f_4107(t4,t2,t3);}

/* k4779 in k4776 in k4773 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4781(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4781,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k4792 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4794(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_4794,c,av);}
a=C_alloc(8);
t2=C_retrieve2(lf[38],C_text("chicken.csi#command-table"));
t3=C_i_check_list_2(C_retrieve2(lf[38],C_text("chicken.csi#command-table")),lf[101]);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4816,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4821,a[2]=t6,tmp=(C_word)a,a+=3,tmp));
t8=((C_word*)t6)[1];
f_4821(t8,t4,C_retrieve2(lf[38],C_text("chicken.csi#command-table")));}

/* k4814 in k4792 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4816(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4816,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=*((C_word*)lf[29]+1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* for-each-loop958 in k4792 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_fcall f_4821(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,3)))){
C_save_and_reclaim_args((void *)trf_4821,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4831,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t4=C_slot(t2,C_fix(0));
t5=C_i_caddr(t4);
if(C_truep(t5)){
/* csi.scm:400: chicken.base#print */
t6=*((C_word*)lf[107]+1);{
C_word av2[4];
av2[0]=t6;
av2[1]=t3;
av2[2]=C_make_character(32);
av2[3]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}
else{
/* csi.scm:401: chicken.base#print */
t6=*((C_word*)lf[107]+1);{
C_word av2[4];
av2[0]=t6;
av2[1]=t3;
av2[2]=lf[108];
av2[3]=C_u_i_car(t4);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k4829 in for-each-loop958 in k4792 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4831(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4831,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_4821(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k4843 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4845(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4845,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=*((C_word*)lf[29]+1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a4890 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4891(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4891,c,av);}
/* csi.scm:408: eval */
t2=*((C_word*)lf[54]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* a4896 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4897(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +5,c,2)))){
C_save_and_reclaim((void*)f_4897,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+5);
t2=C_build_rest(&a,c,2,av);
C_word t3;
C_word t4;
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4901,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* csi.scm:409: history-add */
t4=C_retrieve2(lf[28],C_text("chicken.csi#history-add"));
f_4107(t4,t3,t2);}

/* k4899 in a4896 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4901(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4901,c,av);}{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
C_apply(4,av2);}}

/* k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_4917(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_4917,c,av);}
a=C_alloc(9);
t2=C_fast_retrieve(lf[111]);
t3=C_fast_retrieve(lf[112]);
t4=C_fast_retrieve(lf[113]);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5062,a[2]=t4,a[3]=t2,a[4]=t3,a[5]=((C_word*)t0)[2],tmp=(C_word)a,a+=6,tmp);
/* ##sys#peek-c-string */
t6=*((C_word*)lf[379]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=C_mpointer(&a,(void*)C_INSTALL_PREFIX);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* k4921 in k8659 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 in ... */
static void C_ccall f_4923(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,3)))){
C_save_and_reclaim((void *)f_4923,c,av);}
a=C_alloc(14);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4946,a[2]=t4,a[3]=t5,a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4991,a[2]=((C_word*)t0)[3],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* csi.scm:442: scheme#call-with-current-continuation */
t8=*((C_word*)lf[376]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t8;
av2[1]=t6;
av2[2]=t7;
((C_proc)(void*)(*((C_word*)t8+1)))(3,av2);}}

/* k4935 in map-loop998 in k4947 in k4944 in k4921 in k8659 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in ... */
static void C_ccall f_4937(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_4937,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4940,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* csi.scm:440: scheme#write */
t3=*((C_word*)lf[179]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k4938 in k4935 in map-loop998 in k4947 in k4944 in k4921 in k8659 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in ... */
static void C_ccall f_4940(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4940,c,av);}
/* csi.scm:441: chicken.base#get-output-string */
t2=C_fast_retrieve(lf[371]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k4944 in k4921 in k8659 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in ... */
static void C_ccall f_4946(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_4946,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4949,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* csi.scm:442: g1021 */
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)C_fast_retrieve_proc(t3))(2,av2);}}

/* k4947 in k4944 in k4921 in k8659 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in ... */
static void C_ccall f_4949(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_4949,c,av);}
a=C_alloc(7);
t2=C_i_check_list_2(t1,lf[138]);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4957,a[2]=((C_word*)t0)[2],a[3]=t4,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp));
t6=((C_word*)t4)[1];
f_4957(t6,((C_word*)t0)[4],t1);}

/* map-loop998 in k4947 in k4944 in k4921 in k8659 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in ... */
static void C_fcall f_4957(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,0,2)))){
C_save_and_reclaim_args((void *)trf_4957,3,t0,t1,t2);}
a=C_alloc(13);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4967,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4982,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
t5=C_slot(t2,C_fix(0));
if(C_truep(C_i_stringp(t5))){
t6=t3;
f_4967(t6,C_a_i_cons(&a,2,t5,C_SCHEME_END_OF_LIST));}
else{
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4937,a[2]=t4,a[3]=t5,tmp=(C_word)a,a+=4,tmp);
/* csi.scm:439: chicken.base#open-output-string */
t7=C_fast_retrieve(lf[372]);{
C_word av2[2];
av2[0]=t7;
av2[1]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k4965 in map-loop998 in k4947 in k4944 in k4921 in k8659 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in ... */
static void C_fcall f_4967(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,2)))){
C_save_and_reclaim_args((void *)trf_4967,2,t0,t1);}
t2=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t1);
t3=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t4=((C_word*)((C_word*)t0)[3])[1];
f_4957(t4,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k4980 in map-loop998 in k4947 in k4944 in k4921 in k8659 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in ... */
static void C_ccall f_4982(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_4982,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];
f_4967(t2,C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST));}

/* a4990 in k4921 in k8659 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in ... */
static void C_ccall f_4991(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_4991,c,av);}
a=C_alloc(8);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4997,a[2]=((C_word*)t0)[2],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5009,a[2]=((C_word*)t0)[3],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* csi.scm:442: chicken.condition#with-exception-handler */
t5=C_fast_retrieve(lf[375]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t1;
av2[2]=t3;
av2[3]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* a4996 in a4990 in k4921 in k8659 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in ... */
static void C_ccall f_4997(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4997,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5003,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csi.scm:442: k1018 */
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t1;
av2[2]=t3;
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}

/* a5002 in a4996 in a4990 in k4921 in k8659 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in ... */
static void C_ccall f_5003(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5003,c,av);}
/* csi.scm:442: ##sys#error */
t2=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[373];
av2[3]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* a5008 in a4990 in k4921 in k8659 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in ... */
static void C_ccall f_5009(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_5009,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5015,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5048,a[2]=((C_word*)t0)[3],tmp=(C_word)a,a+=3,tmp);
/* csi.scm:442: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=t1;
av2[2]=t2;
av2[3]=t3;
C_call_with_values(4,av2);}}

/* a5014 in a5008 in a4990 in k4921 in k8659 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in ... */
static void C_ccall f_5015(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_5015,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5023,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* csi.scm:443: scheme#read */
t3=*((C_word*)lf[52]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k5021 in a5014 in a5008 in a4990 in k4921 in k8659 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in ... */
static void C_ccall f_5023(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_5023,c,av);}
a=C_alloc(6);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5025,a[2]=t3,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp));
t5=((C_word*)t3)[1];
f_5025(t5,((C_word*)t0)[3],t1,C_SCHEME_END_OF_LIST);}

/* doloop1025 in k5021 in a5014 in a5008 in a4990 in k4921 in k8659 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in ... */
static void C_fcall f_5025(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_5025,4,t0,t1,t2,t3);}
a=C_alloc(6);
if(C_truep(C_eofp(t2))){
/* csi.scm:445: scheme#reverse */
t4=*((C_word*)lf[374]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t1;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5042,a[2]=t2,a[3]=t3,a[4]=((C_word*)t0)[2],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
/* csi.scm:443: scheme#read */
t5=*((C_word*)lf[52]+1);{
C_word av2[3];
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}}

/* k5040 in doloop1025 in k5021 in a5014 in a5008 in a4990 in k4921 in k8659 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in ... */
static void C_ccall f_5042(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_5042,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],((C_word*)t0)[3]);
t3=((C_word*)((C_word*)t0)[4])[1];
f_5025(t3,((C_word*)t0)[5],t1,t2);}

/* a5047 in a5008 in a4990 in k4921 in k8659 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in ... */
static void C_ccall f_5048(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +3,c,2)))){
C_save_and_reclaim((void*)f_5048,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+3);
t2=C_build_rest(&a,c,2,av);
C_word t3;
C_word t4;
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5054,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:442: k1018 */
t4=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t1;
av2[2]=t3;
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}

/* a5053 in a5047 in a5008 in a4990 in k4921 in k8659 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in ... */
static void C_ccall f_5054(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5054,c,av);}{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=0;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
C_apply_values(3,av2);}}

/* k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_5062(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_5062,c,av);}
a=C_alloc(9);
t2=C_mutate(&lf[61] /* (set! chicken.csi#report ...) */,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5063,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp));
t3=C_mutate(&lf[144] /* (set! chicken.csi#bytevector-data ...) */,lf[145]);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5391,a[2]=((C_word*)t0)[5],tmp=(C_word)a,a+=3,tmp);
/* csi.scm:556: scheme#make-vector */
t5=*((C_word*)lf[378]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=C_fix(37);
av2[3]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* chicken.csi#report in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_fcall f_5063(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,3)))){
C_save_and_reclaim_args((void *)trf_5063,3,t0,t1,t2);}
a=C_alloc(5);
t3=C_i_pairp(t2);
t4=(C_truep(t3)?C_u_i_car(t2):*((C_word*)lf[84]+1));
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5073,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* csi.scm:458: with-output-to-port */
t6=((C_word*)t0)[5];{
C_word av2[4];
av2[0]=t6;
av2[1]=t1;
av2[2]=t4;
av2[3]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* a5072 in chicken.csi#report in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_5073(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_5073,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5077,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* csi.scm:460: chicken.gc#gc */
t3=C_fast_retrieve(lf[143]);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k5075 in a5072 in chicken.csi#report in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 in ... */
static void C_ccall f_5077(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_5077,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5080,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* csi.scm:461: ##sys#symbol-table-info */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[142]);
C_word *av2=av;
av2[0]=*((C_word*)lf[142]+1);
av2[1]=t2;
tp(2,av2);}}

/* k5078 in k5075 in a5072 in chicken.csi#report in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in ... */
static void C_ccall f_5080(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_5080,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5083,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* csi.scm:462: chicken.gc#memory-statistics */
t3=C_fast_retrieve(lf[141]);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k5081 in k5078 in k5075 in a5072 in chicken.csi#report in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in ... */
static void C_ccall f_5083(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,3)))){
C_save_and_reclaim((void *)f_5083,c,av);}
a=C_alloc(14);
t2=C_mk_bool(C_interrupts_enabled);
t3=C_mk_bool(C_heap_size_is_fixed);
t4=C_mk_bool(C_STACK_GROWS_DOWNWARD);
t5=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_5085,tmp=(C_word)a,a+=2,tmp);
t6=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_5101,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=((C_word*)t0)[3],a[5]=t1,a[6]=t3,a[7]=t4,a[8]=((C_word*)t0)[4],a[9]=((C_word*)t0)[5],a[10]=t5,a[11]=((C_word*)t0)[6],tmp=(C_word)a,a+=12,tmp);
/* csi.scm:467: printf */
t7=*((C_word*)lf[77]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t7;
av2[1]=t6;
av2[2]=lf[140];
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}

/* shorten in k5081 in k5078 in k5075 in a5072 in chicken.csi#report in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in ... */
static void C_fcall f_5085(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(36,0,2)))){
C_save_and_reclaim_args((void *)trf_5085,2,t1,t2);}
a=C_alloc(36);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5093,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t4=C_s_a_i_times(&a,2,t2,C_fix(100));
/* csi.scm:466: scheme#truncate */
t5=*((C_word*)lf[115]+1);{
C_word av2[3];
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5091 in shorten in k5081 in k5078 in k5075 in a5072 in chicken.csi#report in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in ... */
static void C_ccall f_5093(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5093,c,av);}
/* ##sys#/-2 */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[114]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[114]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_fix(100);
tp(4,av2);}}

/* k5099 in k5081 in k5078 in k5075 in a5072 in chicken.csi#report in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in ... */
static void C_ccall f_5101(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(28,c,3)))){
C_save_and_reclaim((void *)f_5101,c,av);}
a=C_alloc(28);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_5104,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],tmp=(C_word)a,a+=11,tmp);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=C_fast_retrieve(lf[128]);
t8=C_fast_retrieve(lf[5]);
t9=C_i_check_list_2(C_fast_retrieve(lf[5]),lf[138]);
t10=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5279,a[2]=((C_word*)t0)[11],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t11=C_SCHEME_UNDEFINED;
t12=(*a=C_VECTOR_TYPE|1,a[1]=t11,tmp=(C_word)a,a+=2,tmp);
t13=C_set_block_item(t12,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5281,a[2]=t5,a[3]=t12,a[4]=t7,a[5]=t6,tmp=(C_word)a,a+=6,tmp));
t14=((C_word*)t12)[1];
f_5281(t14,t10,C_fast_retrieve(lf[5]));}

/* k5102 in k5099 in k5081 in k5078 in k5075 in a5072 in chicken.csi#report in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in ... */
static void C_ccall f_5104(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(22,c,3)))){
C_save_and_reclaim((void *)f_5104,c,av);}
a=C_alloc(22);
t2=C_fix(0);
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5105,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
t5=C_i_check_list_2(t1,lf[101]);
t6=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_5151,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],tmp=(C_word)a,a+=11,tmp);
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5248,a[2]=t8,a[3]=t4,tmp=(C_word)a,a+=4,tmp));
t10=((C_word*)t8)[1];
f_5248(t10,t6,t1);}

/* g1088 in k5102 in k5099 in k5081 in k5078 in k5075 in a5072 in chicken.csi#report in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in ... */
static void C_fcall f_5105(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,3)))){
C_save_and_reclaim_args((void *)trf_5105,3,t0,t1,t2);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5109,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* csi.scm:472: printf */
t4=*((C_word*)lf[77]+1);{
C_word av2[4];
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[118];
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k5107 in g1088 in k5102 in k5099 in k5081 in k5078 in k5075 in a5072 in chicken.csi#report in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in ... */
static void C_ccall f_5109(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(99,c,2)))){
C_save_and_reclaim((void *)f_5109,c,av);}
a=C_alloc(99);
t2=C_i_string_length(((C_word*)t0)[2]);
t3=C_a_i_fixnum_difference(&a,2,C_fix(16),t2);
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)((C_word*)t0)[3])[1];
t6=C_mutate(((C_word *)((C_word*)t0)[3])+1,C_s_a_i_plus(&a,2,t5,C_fix(1)));
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5119,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t4,tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_i_less_or_equalp(((C_word*)t4)[1],C_fix(0)))){
t8=((C_word*)((C_word*)t0)[3])[1];
t9=C_mutate(((C_word *)((C_word*)t0)[3])+1,C_s_a_i_plus(&a,2,t8,C_fix(1)));
t10=C_s_a_i_plus(&a,2,((C_word*)t4)[1],C_fix(18));
t11=C_set_block_item(t4,0,t10);
t12=t7;
f_5119(t12,t11);}
else{
t8=t7;
f_5119(t8,C_SCHEME_UNDEFINED);}}

/* k5117 in k5107 in g1088 in k5102 in k5099 in k5081 in k5078 in k5075 in a5072 in chicken.csi#report in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in ... */
static void C_fcall f_5119(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,3)))){
C_save_and_reclaim_args((void *)trf_5119,2,t0,t1);}
a=C_alloc(4);
if(C_truep(C_i_greater_or_equalp(((C_word*)((C_word*)t0)[2])[1],C_fix(3)))){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5128,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* csi.scm:480: scheme#display */
t3=*((C_word*)lf[94]+1);{
C_word av2[3];
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[116];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5136,a[2]=((C_word*)t0)[3],tmp=(C_word)a,a+=3,tmp);
/* csi.scm:483: scheme#make-string */
t3=*((C_word*)lf[117]+1);{
C_word av2[4];
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[4])[1];
av2[3]=C_make_character(32);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}}

/* k5126 in k5117 in k5107 in g1088 in k5102 in k5099 in k5081 in k5078 in k5075 in a5072 in chicken.csi#report in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in ... */
static void C_ccall f_5128(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5128,c,av);}
t2=C_set_block_item(((C_word*)t0)[2],0,C_fix(0));
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k5134 in k5117 in k5107 in g1088 in k5102 in k5099 in k5081 in k5078 in k5075 in a5072 in chicken.csi#report in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in ... */
static void C_ccall f_5136(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5136,c,av);}
/* csi.scm:483: scheme#display */
t2=*((C_word*)lf[94]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k5149 in k5102 in k5099 in k5081 in k5078 in k5075 in a5072 in chicken.csi#report in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in ... */
static void C_ccall f_5151(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,2)))){
C_save_and_reclaim((void *)f_5151,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5154,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_5167,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[7],a[6]=t2,a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],tmp=(C_word)a,a+=10,tmp);
/* csi.scm:501: chicken.platform#machine-type */
t4=C_fast_retrieve(lf[137]);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k5152 in k5149 in k5102 in k5099 in k5081 in k5078 in k5075 in a5072 in chicken.csi#report in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in ... */
static void C_ccall f_5154(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_5154,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5157,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* csi.scm:520: ##sys#write-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[90]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[90]+1);
av2[1]=t2;
av2[2]=C_make_character(10);
av2[3]=*((C_word*)lf[84]+1);
tp(4,av2);}}

/* k5155 in k5152 in k5149 in k5102 in k5099 in k5081 in k5078 in k5075 in a5072 in chicken.csi#report in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in ... */
static void C_ccall f_5157(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_5157,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5160,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(((C_word*)t0)[3])){
/* csi.scm:521: scheme#display */
t3=*((C_word*)lf[94]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[119];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k5158 in k5155 in k5152 in k5149 in k5102 in k5099 in k5081 in k5078 in k5075 in a5072 in chicken.csi#report in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in ... */
static void C_ccall f_5160(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5160,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k5165 in k5149 in k5102 in k5099 in k5081 in k5078 in k5075 in a5072 in chicken.csi#report in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in ... */
static void C_ccall f_5167(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_5167,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_5246,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t1,a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],tmp=(C_word)a,a+=11,tmp);
/* csi.scm:502: chicken.platform#feature? */
t3=C_fast_retrieve(lf[135]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[136];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k5173 in k5244 in k5165 in k5149 in k5102 in k5099 in k5081 in k5078 in k5075 in a5072 in chicken.csi#report in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in ... */
static void C_ccall f_5175(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_5175,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_5179,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=t1,a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],tmp=(C_word)a,a+=13,tmp);
/* csi.scm:504: chicken.platform#software-version */
t3=C_fast_retrieve(lf[133]);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k5177 in k5173 in k5244 in k5165 in k5149 in k5102 in k5099 in k5081 in k5078 in k5075 in a5072 in chicken.csi#report in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in ... */
static void C_ccall f_5179(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,2)))){
C_save_and_reclaim((void *)f_5179,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_5183,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=t1,a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],a[13]=((C_word*)t0)[12],tmp=(C_word)a,a+=14,tmp);
/* csi.scm:505: chicken.platform#build-platform */
t3=C_fast_retrieve(lf[132]);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k5181 in k5177 in k5173 in k5244 in k5165 in k5149 in k5102 in k5099 in k5081 in k5078 in k5075 in a5072 in chicken.csi#report in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in ... */
static void C_ccall f_5183(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,2)))){
C_save_and_reclaim((void *)f_5183,c,av);}
a=C_alloc(15);
t2=(*a=C_CLOSURE_TYPE|14,a[1]=(C_word)f_5187,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=t1,a[12]=((C_word*)t0)[11],a[13]=((C_word*)t0)[12],a[14]=((C_word*)t0)[13],tmp=(C_word)a,a+=15,tmp);
/* csi.scm:507: chicken.platform#installation-repository */
t3=C_fast_retrieve(lf[131]);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k5185 in k5181 in k5177 in k5173 in k5244 in k5165 in k5149 in k5102 in k5099 in k5081 in k5078 in k5075 in a5072 in chicken.csi#report in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in ... */
static void C_ccall f_5187(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,2)))){
C_save_and_reclaim((void *)f_5187,c,av);}
a=C_alloc(16);
t2=(*a=C_CLOSURE_TYPE|15,a[1]=(C_word)f_5191,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=t1,a[14]=((C_word*)t0)[13],a[15]=((C_word*)t0)[14],tmp=(C_word)a,a+=16,tmp);
/* csi.scm:508: chicken.platform#repository-path */
t3=C_fast_retrieve(lf[130]);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k5189 in k5185 in k5181 in k5177 in k5173 in k5244 in k5165 in k5149 in k5102 in k5099 in k5081 in k5078 in k5075 in a5072 in chicken.csi#report in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in ... */
static void C_ccall f_5191(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(20,c,2)))){
C_save_and_reclaim((void *)f_5191,c,av);}
a=C_alloc(20);
t2=(*a=C_CLOSURE_TYPE|16,a[1]=(C_word)f_5195,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=t1,a[15]=((C_word*)t0)[14],a[16]=((C_word*)t0)[15],tmp=(C_word)a,a+=17,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5243,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:510: chicken.base#keyword-style */
t4=C_fast_retrieve(lf[129]);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k5193 in k5189 in k5185 in k5181 in k5177 in k5173 in k5244 in k5165 in k5149 in k5102 in k5099 in k5081 in k5078 in k5075 in a5072 in chicken.csi#report in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in ... */
static void C_ccall f_5195(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,2)))){
C_save_and_reclaim((void *)f_5195,c,av);}
a=C_alloc(18);
t2=(*a=C_CLOSURE_TYPE|17,a[1]=(C_word)f_5199,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=t1,a[16]=((C_word*)t0)[15],a[17]=((C_word*)t0)[16],tmp=(C_word)a,a+=18,tmp);
/* csi.scm:511: shorten */
f_5085(t2,C_i_vector_ref(((C_word*)t0)[2],C_fix(0)));}

/* k5197 in k5193 in k5189 in k5185 in k5181 in k5177 in k5173 in k5244 in k5165 in k5149 in k5102 in k5099 in k5081 in k5078 in k5075 in a5072 in chicken.csi#report in k5060 in k4915 in k4271 in k3928 in k3846 in ... */
static void C_ccall f_5199(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,2)))){
C_save_and_reclaim((void *)f_5199,c,av);}
a=C_alloc(18);
t2=(*a=C_CLOSURE_TYPE|17,a[1]=(C_word)f_5203,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=t1,a[17]=((C_word*)t0)[16],tmp=(C_word)a,a+=18,tmp);
/* csi.scm:512: shorten */
f_5085(t2,C_i_vector_ref(((C_word*)t0)[2],C_fix(1)));}

/* k5201 in k5197 in k5193 in k5189 in k5185 in k5181 in k5177 in k5173 in k5244 in k5165 in k5149 in k5102 in k5099 in k5081 in k5078 in k5075 in a5072 in chicken.csi#report in k5060 in k4915 in k4271 in k3928 in ... */
static void C_ccall f_5203(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(20,c,2)))){
C_save_and_reclaim((void *)f_5203,c,av);}
a=C_alloc(20);
t2=C_i_vector_ref(((C_word*)t0)[2],C_fix(2));
t3=C_i_vector_ref(((C_word*)t0)[3],C_fix(0));
t4=(C_truep(((C_word*)t0)[4])?lf[122]:lf[123]);
t5=C_i_vector_ref(((C_word*)t0)[3],C_fix(1));
t6=C_i_vector_ref(((C_word*)t0)[3],C_fix(2));
t7=(C_truep(((C_word*)t0)[5])?lf[124]:lf[125]);
t8=(*a=C_CLOSURE_TYPE|19,a[1]=(C_word)f_5231,a[2]=((C_word*)t0)[6],a[3]=((C_word*)t0)[7],a[4]=((C_word*)t0)[8],a[5]=((C_word*)t0)[9],a[6]=((C_word*)t0)[10],a[7]=((C_word*)t0)[11],a[8]=((C_word*)t0)[12],a[9]=((C_word*)t0)[13],a[10]=((C_word*)t0)[14],a[11]=((C_word*)t0)[15],a[12]=((C_word*)t0)[16],a[13]=t1,a[14]=t2,a[15]=t3,a[16]=t4,a[17]=t5,a[18]=t6,a[19]=t7,tmp=(C_word)a,a+=20,tmp);
/* csi.scm:519: argv */
t9=((C_word*)t0)[17];{
C_word *av2=av;
av2[0]=t9;
av2[1]=t8;
((C_proc)(void*)(*((C_word*)t9+1)))(2,av2);}}

/* k5229 in k5201 in k5197 in k5193 in k5189 in k5185 in k5181 in k5177 in k5173 in k5244 in k5165 in k5149 in k5102 in k5099 in k5081 in k5078 in k5075 in a5072 in chicken.csi#report in k5060 in k4915 in k4271 in ... */
static void C_ccall f_5231(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,21)))){
C_save_and_reclaim((void *)f_5231,c,av);}
/* csi.scm:485: printf */
t2=*((C_word*)lf[77]+1);{
C_word *av2;
if(c >= 22) {
  av2=av;
} else {
  av2=C_alloc(22);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[126];
av2[3]=((C_word*)t0)[3];
av2[4]=((C_word*)t0)[4];
av2[5]=((C_word*)t0)[5];
av2[6]=((C_word*)t0)[6];
av2[7]=((C_word*)t0)[7];
av2[8]=((C_word*)t0)[8];
av2[9]=((C_word*)t0)[9];
av2[10]=((C_word*)t0)[10];
av2[11]=C_fast_retrieve(lf[127]);
av2[12]=((C_word*)t0)[11];
av2[13]=((C_word*)t0)[12];
av2[14]=((C_word*)t0)[13];
av2[15]=((C_word*)t0)[14];
av2[16]=((C_word*)t0)[15];
av2[17]=((C_word*)t0)[16];
av2[18]=((C_word*)t0)[17];
av2[19]=((C_word*)t0)[18];
av2[20]=((C_word*)t0)[19];
av2[21]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(22,av2);}}

/* k5241 in k5189 in k5185 in k5181 in k5177 in k5173 in k5244 in k5165 in k5149 in k5102 in k5099 in k5081 in k5078 in k5075 in a5072 in chicken.csi#report in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in ... */
static void C_ccall f_5243(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5243,c,av);}
/* csi.scm:510: chicken.keyword#keyword->string */
t2=C_fast_retrieve(lf[128]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k5244 in k5165 in k5149 in k5102 in k5099 in k5081 in k5078 in k5075 in a5072 in chicken.csi#report in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in ... */
static void C_ccall f_5246(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_5246,c,av);}
a=C_alloc(12);
t2=(C_truep(t1)?lf[120]:lf[121]);
t3=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_5175,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=t2,a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],tmp=(C_word)a,a+=12,tmp);
/* csi.scm:503: chicken.platform#software-type */
t4=C_fast_retrieve(lf[134]);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* for-each-loop1087 in k5102 in k5099 in k5081 in k5078 in k5075 in a5072 in chicken.csi#report in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in ... */
static void C_fcall f_5248(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_5248,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5258,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* csi.scm:470: g1088 */
t4=((C_word*)t0)[3];
f_5105(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k5256 in for-each-loop1087 in k5102 in k5099 in k5081 in k5078 in k5075 in a5072 in chicken.csi#report in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in ... */
static void C_ccall f_5258(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5258,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_5248(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k5277 in k5099 in k5081 in k5078 in k5075 in a5072 in chicken.csi#report in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in ... */
static void C_ccall f_5279(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5279,c,av);}
/* csi.scm:468: sort */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
av2[3]=*((C_word*)lf[139]+1);
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* map-loop1061 in k5099 in k5081 in k5078 in k5075 in a5072 in chicken.csi#report in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in ... */
static void C_fcall f_5281(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_5281,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5306,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* csi.scm:468: g1067 */
t4=((C_word*)t0)[4];{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k5304 in map-loop1061 in k5099 in k5081 in k5078 in k5075 in a5072 in chicken.csi#report in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in ... */
static void C_ccall f_5306(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_5306,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_5281(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* lp in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in ... */
static C_word C_fcall f_5326(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_stack_overflow_check;
loop:{}
if(C_truep(C_i_pairp(t1))){
t3=C_u_i_cdr(t1);
if(C_truep(C_i_pairp(t3))){
t4=C_u_i_cdr(t3);
t5=C_i_cdr(t2);
t6=C_eqp(t4,t5);
if(C_truep(t6)){
return(t6);}
else{
t8=t4;
t9=t5;
t1=t8;
t2=t9;
goto loop;}}
else{
return(C_SCHEME_FALSE);}}
else{
return(C_SCHEME_FALSE);}}

/* lp in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in ... */
static C_word C_fcall f_5361(C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_stack_overflow_check;
loop:{}
t2=C_i_pairp(t1);
if(C_truep(C_i_not(t2))){
return(C_SCHEME_FALSE);}
else{
t3=C_i_car(t1);
t4=C_eqp(t1,t3);
if(C_truep(t4)){
return(t4);}
else{
t6=C_u_i_cdr(t1);
t1=t6;
goto loop;}}}

/* k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_5391(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(28,c,6)))){
C_save_and_reclaim((void *)f_5391,c,av);}
a=C_alloc(28);
t2=C_mutate(&lf[146] /* (set! chicken.csi#describer-table ...) */,t1);
t3=*((C_word*)lf[147]+1);
t4=*((C_word*)lf[148]+1);
t5=*((C_word*)lf[149]+1);
t6=C_mutate(&lf[56] /* (set! chicken.csi#describe ...) */,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5393,a[2]=t5,a[3]=t3,a[4]=t4,tmp=(C_word)a,a+=5,tmp));
t7=C_mutate((C_word*)lf[239]+1 /* (set! chicken.csi#set-describer! ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_6423,tmp=(C_word)a,a+=2,tmp));
t8=C_mutate(&lf[58] /* (set! chicken.csi#dump ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_6432,tmp=(C_word)a,a+=2,tmp));
t9=C_mutate(&lf[214] /* (set! chicken.csi#hexdump ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_6580,tmp=(C_word)a,a+=2,tmp));
t10=C_mutate(&lf[92] /* (set! chicken.csi#show-frameinfo ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_6785,tmp=(C_word)a,a+=2,tmp));
t11=C_establish_signal_handler(C_fix((C_word)SIGINT),C_fix((C_word)SIGINT));
t12=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_8675,tmp=(C_word)a,a+=2,tmp);
t13=C_i_setslot(C_fast_retrieve(lf[258]),C_fix((C_word)SIGINT),t12);
t14=C_mutate(&lf[259] /* (set! chicken.csi#member* ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7359,tmp=(C_word)a,a+=2,tmp));
t15=C_mutate(&lf[260] /* (set! chicken.csi#canonicalize-args ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7414,tmp=(C_word)a,a+=2,tmp));
t16=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8667,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t17=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7593,a[2]=t16,tmp=(C_word)a,a+=3,tmp);
t18=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8661,a[2]=t17,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:973: chicken.process-context#get-environment-variable */
t19=C_fast_retrieve(lf[23]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t19;
av2[1]=t18;
av2[2]=lf[377];
((C_proc)(void*)(*((C_word*)t19+1)))(3,av2);}}

/* chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_fcall f_5393(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(17,0,6)))){
C_save_and_reclaim_args((void *)trf_5393,4,t0,t1,t2,t3);}
a=C_alloc(17);
t4=C_i_nullp(t3);
t5=(C_truep(t4)?*((C_word*)lf[84]+1):C_i_car(t3));
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5399,a[2]=t5,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t7=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_5531,a[2]=t1,a[3]=t2,a[4]=t5,a[5]=t6,a[6]=((C_word*)t0)[2],a[7]=((C_word*)t0)[3],a[8]=((C_word*)t0)[4],tmp=(C_word)a,a+=9,tmp);
if(C_truep(C_permanentp(t2))){
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6415,a[2]=t7,a[3]=t5,tmp=(C_word)a,a+=4,tmp);
/* csi.scm:590: ##sys#block-address */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[238]);
C_word av2[3];
av2[0]=*((C_word*)lf[238]+1);
av2[1]=t8;
av2[2]=t2;
tp(3,av2);}}
else{
t8=t7;{
C_word av2[2];
av2[0]=t8;
av2[1]=C_SCHEME_UNDEFINED;
f_5531(2,av2);}}}

/* descseq in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 in ... */
static void C_ccall f_5399(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word *a;
if(c!=6) C_bad_argc_2(c,6,t0);
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_5399,c,av);}
a=C_alloc(8);
t6=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_5528,a[2]=t5,a[3]=((C_word*)t0)[2],a[4]=t4,a[5]=((C_word*)t0)[3],a[6]=t1,a[7]=t2,tmp=(C_word)a,a+=8,tmp);
/* csi.scm:567: plen */
t7=t3;{
C_word *av2=av;
av2[0]=t7;
av2[1]=t6;
av2[2]=((C_word*)t0)[3];
((C_proc)C_fast_retrieve_proc(t7))(3,av2);}}

/* k5404 in k5526 in descseq in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in ... */
static void C_ccall f_5406(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_5406,c,av);}
a=C_alloc(10);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_5411,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t3,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp));
t5=((C_word*)t3)[1];
f_5411(t5,((C_word*)t0)[7],C_fix(0));}

/* loop1 in k5404 in k5526 in descseq in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in ... */
static void C_fcall f_5411(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,4)))){
C_save_and_reclaim_args((void *)trf_5411,3,t0,t1,t2);}
a=C_alloc(10);
t3=C_fixnum_greater_or_equal_p(t2,((C_word*)t0)[2]);
if(C_truep(t3)){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
if(C_truep(C_fixnum_greater_or_equal_p(t2,C_fix(40)))){
/* csi.scm:572: fprintf */
t4=*((C_word*)lf[150]+1);{
C_word av2[5];
av2[0]=t4;
av2[1]=t1;
av2[2]=((C_word*)t0)[3];
av2[3]=lf[151];
av2[4]=C_fixnum_difference(((C_word*)t0)[2],t2);
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_5434,a[2]=((C_word*)t0)[4],a[3]=t2,a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[3],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=t1,tmp=(C_word)a,a+=10,tmp);
/* csi.scm:574: pref */
t5=((C_word*)t0)[6];{
C_word av2[4];
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[7];
av2[3]=C_fixnum_plus(((C_word*)t0)[4],t2);
((C_proc)C_fast_retrieve_proc(t5))(4,av2);}}}}

/* k5432 in loop1 in k5404 in k5526 in descseq in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in ... */
static void C_ccall f_5434(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,4)))){
C_save_and_reclaim((void *)f_5434,c,av);}
a=C_alloc(12);
t2=C_fixnum_plus(((C_word*)t0)[2],C_fix(1));
t3=C_fixnum_plus(((C_word*)t0)[3],t2);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_5443,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[6],a[6]=t1,a[7]=t5,a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp));
t7=((C_word*)t5)[1];
f_5443(t7,((C_word*)t0)[9],C_fix(1),t3);}

/* loop2 in k5432 in loop1 in k5404 in k5526 in descseq in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in ... */
static void C_fcall f_5443(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,3)))){
C_save_and_reclaim_args((void *)trf_5443,4,t0,t1,t2,t3);}
a=C_alloc(12);
if(C_truep(C_fixnum_greater_or_equal_p(t3,((C_word*)t0)[2]))){
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5453,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=((C_word*)t0)[4],a[5]=t2,a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5485,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[6],tmp=(C_word)a,a+=5,tmp);
/* csi.scm:577: ##sys#with-print-length-limit */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[88]);
C_word av2[4];
av2[0]=*((C_word*)lf[88]+1);
av2[1]=t4;
av2[2]=C_fix(1000);
av2[3]=t5;
tp(4,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_5513,a[2]=((C_word*)t0)[6],a[3]=((C_word*)t0)[7],a[4]=t1,a[5]=t2,a[6]=t3,a[7]=((C_word*)t0)[2],tmp=(C_word)a,a+=8,tmp);
/* csi.scm:587: pref */
t5=((C_word*)t0)[8];{
C_word av2[4];
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[9];
av2[3]=t3;
((C_proc)C_fast_retrieve_proc(t5))(4,av2);}}}

/* k5451 in loop2 in k5432 in loop1 in k5404 in k5526 in descseq in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in ... */
static void C_ccall f_5453(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,5)))){
C_save_and_reclaim((void *)f_5453,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5456,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_fixnum_greaterp(((C_word*)t0)[5],C_fix(1)))){
t3=C_fixnum_difference(((C_word*)t0)[5],C_fix(1));
t4=C_eqp(((C_word*)t0)[5],C_fix(2));
if(C_truep(t4)){
/* csi.scm:582: fprintf */
t5=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=((C_word*)t0)[6];
av2[3]=lf[152];
av2[4]=t3;
av2[5]=lf[153];
((C_proc)(void*)(*((C_word*)t5+1)))(6,av2);}}
else{
/* csi.scm:582: fprintf */
t5=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=((C_word*)t0)[6];
av2[3]=lf[152];
av2[4]=t3;
av2[5]=lf[154];
((C_proc)(void*)(*((C_word*)t5+1)))(6,av2);}}}
else{
/* csi.scm:585: scheme#newline */
t3=*((C_word*)lf[86]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}

/* k5454 in k5451 in loop2 in k5432 in loop1 in k5404 in k5526 in descseq in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in ... */
static void C_ccall f_5456(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5456,c,av);}
/* csi.scm:586: loop1 */
t2=((C_word*)((C_word*)t0)[2])[1];
f_5411(t2,((C_word*)t0)[3],C_fixnum_plus(((C_word*)t0)[4],((C_word*)t0)[5]));}

/* a5484 in loop2 in k5432 in loop1 in k5404 in k5526 in descseq in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in ... */
static void C_ccall f_5485(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_5485,c,av);}
/* csi.scm:580: fprintf */
t2=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
av2[3]=lf[155];
av2[4]=((C_word*)t0)[3];
av2[5]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* k5511 in loop2 in k5432 in loop1 in k5404 in k5526 in descseq in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in ... */
static void C_ccall f_5513(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5513,c,av);}
t2=C_eqp(((C_word*)t0)[2],t1);
if(C_truep(t2)){
/* csi.scm:587: loop2 */
t3=((C_word*)((C_word*)t0)[3])[1];
f_5443(t3,((C_word*)t0)[4],C_fixnum_plus(((C_word*)t0)[5],C_fix(1)),C_fixnum_plus(((C_word*)t0)[6],C_fix(1)));}
else{
/* csi.scm:588: loop2 */
t3=((C_word*)((C_word*)t0)[3])[1];
f_5443(t3,((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[7]);}}

/* k5526 in descseq in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in ... */
static void C_ccall f_5528(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,5)))){
C_save_and_reclaim((void *)f_5528,c,av);}
a=C_alloc(8);
t2=C_fixnum_difference(t1,((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_5406,a[2]=t2,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
if(C_truep(((C_word*)t0)[7])){
/* csi.scm:568: fprintf */
t4=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[3];
av2[3]=lf[156];
av2[4]=((C_word*)t0)[7];
av2[5]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(6,av2);}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_5406(2,av2);}}}

/* k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 in ... */
static void C_ccall f_5531(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,7)))){
C_save_and_reclaim((void *)f_5531,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5534,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_charp(((C_word*)t0)[3]))){
t3=C_fix(C_character_code(((C_word*)t0)[3]));
/* csi.scm:593: fprintf */
t4=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=lf[157];
av2[4]=((C_word*)t0)[3];
av2[5]=t3;
av2[6]=t3;
av2[7]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(8,av2);}}
else{
switch(((C_word*)t0)[3]){
case C_SCHEME_TRUE:
/* csi.scm:594: fprintf */
t3=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=lf[158];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}
case C_SCHEME_FALSE:
/* csi.scm:595: fprintf */
t3=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=lf[159];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}
default:
if(C_truep(C_i_nullp(((C_word*)t0)[3]))){
/* csi.scm:596: fprintf */
t3=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=lf[160];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}
else{
if(C_truep(C_eofp(((C_word*)t0)[3]))){
/* csi.scm:597: fprintf */
t3=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=lf[161];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}
else{
t3=*((C_word*)lf[29]+1);
t4=C_eqp(*((C_word*)lf[29]+1),((C_word*)t0)[3]);
if(C_truep(t4)){
/* csi.scm:598: fprintf */
t5=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=lf[162];
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}
else{
if(C_truep(C_fixnump(((C_word*)t0)[3]))){
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5600,a[2]=((C_word*)t0)[3],a[3]=t2,a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* csi.scm:600: fprintf */
t6=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=((C_word*)t0)[4];
av2[3]=lf[164];
av2[4]=((C_word*)t0)[3];
av2[5]=((C_word*)t0)[3];
av2[6]=((C_word*)t0)[3];
av2[7]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t6+1)))(8,av2);}}
else{
if(C_truep(C_i_bignump(((C_word*)t0)[3]))){
/* csi.scm:606: fprintf */
t5=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=lf[165];
av2[4]=((C_word*)t0)[3];
av2[5]=((C_word*)t0)[3];
av2[6]=((C_word*)t0)[3];
av2[7]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t5+1)))(8,av2);}}
else{
if(C_truep(C_unboundvaluep(((C_word*)t0)[3]))){
/* csi.scm:609: fprintf */
t5=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=lf[166];
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}
else{
if(C_truep(C_i_flonump(((C_word*)t0)[3]))){
/* csi.scm:610: fprintf */
t5=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=lf[167];
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}
else{
if(C_truep(C_i_ratnump(((C_word*)t0)[3]))){
/* csi.scm:611: fprintf */
t5=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=lf[168];
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}
else{
if(C_truep(C_i_cplxnump(((C_word*)t0)[3]))){
if(C_truep(C_i_exactp(((C_word*)t0)[3]))){
/* csi.scm:612: fprintf */
t5=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=lf[169];
av2[4]=lf[170];
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t5+1)))(6,av2);}}
else{
/* csi.scm:612: fprintf */
t5=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=lf[169];
av2[4]=lf[171];
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t5+1)))(6,av2);}}}
else{
if(C_truep(C_i_numberp(((C_word*)t0)[3]))){
/* csi.scm:614: fprintf */
t5=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=lf[172];
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}
else{
if(C_truep(C_i_stringp(((C_word*)t0)[3]))){
/* csi.scm:615: descseq */
t5=((C_word*)t0)[5];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=lf[173];
av2[3]=*((C_word*)lf[174]+1);
av2[4]=((C_word*)t0)[6];
av2[5]=C_fix(0);
f_5399(6,av2);}}
else{
if(C_truep(C_i_vectorp(((C_word*)t0)[3]))){
/* csi.scm:616: descseq */
t5=((C_word*)t0)[5];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=lf[175];
av2[3]=*((C_word*)lf[174]+1);
av2[4]=*((C_word*)lf[176]+1);
av2[5]=C_fix(0);
f_5399(6,av2);}}
else{
t5=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_5697,a[2]=t2,a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[2],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
/* csi.scm:617: chicken.keyword#keyword? */
t6=C_fast_retrieve(lf[236]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}}}}}}}}}}}}}}}

/* k5532 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in ... */
static void C_ccall f_5534(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5534,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=*((C_word*)lf[29]+1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k5598 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in ... */
static void C_ccall f_5600(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_5600,c,av);}
a=C_alloc(3);
t2=C_make_character(C_unfix(((C_word*)t0)[2]));
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5606,a[2]=((C_word*)t0)[3],tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_fixnum_lessp(((C_word*)t0)[2],C_fix(65536)))){
/* csi.scm:603: fprintf */
t4=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[4];
av2[3]=lf[163];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}
else{
/* csi.scm:604: ##sys#write-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[90]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[90]+1);
av2[1]=((C_word*)t0)[3];
av2[2]=C_make_character(10);
av2[3]=*((C_word*)lf[84]+1);
tp(4,av2);}}}

/* k5604 in k5598 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in ... */
static void C_ccall f_5606(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5606,c,av);}
/* csi.scm:604: ##sys#write-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[90]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[90]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=C_make_character(10);
av2[3]=*((C_word*)lf[84]+1);
tp(4,av2);}}

/* k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in ... */
static void C_ccall f_5697(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_5697,c,av);}
a=C_alloc(12);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5704,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* csi.scm:619: ##sys#symbol->string */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[178]);
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[178]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
tp(3,av2);}}
else{
if(C_truep(C_i_symbolp(((C_word*)t0)[4]))){
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5713,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[2],tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5784,a[2]=t2,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* csi.scm:621: ##sys#symbol-has-toplevel-binding? */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[187]);
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[187]+1);
av2[1]=t3;
av2[2]=((C_word*)t0)[4];
tp(3,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_5326,tmp=(C_word)a,a+=2,tmp);
t3=(
  f_5326(((C_word*)t0)[4],((C_word*)t0)[4])
);
t4=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_5796,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],tmp=(C_word)a,a+=8,tmp);
if(C_truep(t3)){
t5=t4;
f_5796(t5,t3);}
else{
t5=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_5361,tmp=(C_word)a,a+=2,tmp);
t6=t4;
f_5796(t6,(
  f_5361(((C_word*)t0)[4])
));}}}}

/* k5702 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in ... */
static void C_ccall f_5704(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_5704,c,av);}
/* csi.scm:618: fprintf */
t2=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=lf[177];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k5711 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in ... */
static void C_ccall f_5713(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_5713,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5716,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5781,a[2]=t2,a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
/* csi.scm:624: ##sys#interned-symbol? */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[185]);
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[185]+1);
av2[1]=t3;
av2[2]=((C_word*)t0)[2];
tp(3,av2);}}

/* k5714 in k5711 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in ... */
static void C_ccall f_5716(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_5716,c,av);}
a=C_alloc(5);
t2=C_slot(((C_word*)t0)[2],C_fix(2));
if(C_truep(C_i_nullp(t2))){
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=*((C_word*)lf[29]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5728,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* csi.scm:628: scheme#display */
t4=*((C_word*)lf[94]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[181];
av2[3]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}}

/* k5726 in k5714 in k5711 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in ... */
static void C_ccall f_5728(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_5728,c,av);}
a=C_alloc(6);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5733,a[2]=t3,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp));
t5=((C_word*)t3)[1];
f_5733(t5,((C_word*)t0)[3],((C_word*)t0)[4]);}

/* doloop1200 in k5726 in k5714 in k5711 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in ... */
static void C_fcall f_5733(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,4)))){
C_save_and_reclaim_args((void *)trf_5733,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_nullp(t2))){
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5743,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
/* csi.scm:631: fprintf */
t4=*((C_word*)lf[150]+1);{
C_word av2[5];
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[3];
av2[3]=lf[180];
av2[4]=C_i_car(t2);
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}}

/* k5741 in doloop1200 in k5726 in k5714 in k5711 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in ... */
static void C_ccall f_5743(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_5743,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5746,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5758,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],tmp=(C_word)a,a+=4,tmp);
/* csi.scm:632: ##sys#with-print-length-limit */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[88]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[88]+1);
av2[1]=t2;
av2[2]=C_fix(1000);
av2[3]=t3;
tp(4,av2);}}

/* k5744 in k5741 in doloop1200 in k5726 in k5714 in k5711 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in ... */
static void C_ccall f_5746(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_5746,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5749,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* csi.scm:636: scheme#newline */
t3=*((C_word*)lf[86]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k5747 in k5744 in k5741 in doloop1200 in k5726 in k5714 in k5711 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in ... */
static void C_ccall f_5749(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5749,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_5733(t2,((C_word*)t0)[3],C_i_cddr(((C_word*)t0)[4]));}

/* a5757 in k5741 in doloop1200 in k5726 in k5714 in k5711 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in ... */
static void C_ccall f_5758(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5758,c,av);}
/* csi.scm:635: scheme#write */
t2=*((C_word*)lf[179]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=C_i_cadr(((C_word*)t0)[2]);
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k5776 in k5779 in k5711 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in ... */
static void C_ccall f_5778(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_5778,c,av);}
/* csi.scm:623: fprintf */
t2=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=lf[184];
av2[4]=((C_word*)t0)[4];
av2[5]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* k5779 in k5711 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in ... */
static void C_ccall f_5781(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_5781,c,av);}
a=C_alloc(5);
t2=(C_truep(t1)?lf[182]:lf[183]);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5778,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* csi.scm:625: ##sys#symbol->string */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[178]);
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[178]+1);
av2[1]=t3;
av2[2]=((C_word*)t0)[4];
tp(3,av2);}}

/* k5782 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in ... */
static void C_ccall f_5784(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5784,c,av);}
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
f_5713(2,av2);}}
else{
/* csi.scm:622: scheme#display */
t2=*((C_word*)lf[94]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[186];
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}}

/* k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in ... */
static void C_fcall f_5796(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,5)))){
C_save_and_reclaim_args((void *)trf_5796,2,t0,t1);}
a=C_alloc(7);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5799,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* csi.scm:638: fprintf */
t3=*((C_word*)lf[150]+1);{
C_word av2[4];
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=lf[191];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}
else{
if(C_truep(C_i_listp(((C_word*)t0)[2]))){
/* csi.scm:648: descseq */
t2=((C_word*)t0)[5];{
C_word av2[6];
av2[0]=t2;
av2[1]=((C_word*)t0)[4];
av2[2]=lf[192];
av2[3]=((C_word*)t0)[6];
av2[4]=((C_word*)t0)[7];
av2[5]=C_fix(0);
f_5399(6,av2);}}
else{
if(C_truep(C_i_pairp(((C_word*)t0)[2]))){
/* csi.scm:649: fprintf */
t2=*((C_word*)lf[150]+1);{
C_word av2[6];
av2[0]=t2;
av2[1]=((C_word*)t0)[4];
av2[2]=((C_word*)t0)[3];
av2[3]=lf[193];
av2[4]=C_u_i_car(((C_word*)t0)[2]);
av2[5]=C_u_i_cdr(((C_word*)t0)[2]);
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}
else{
if(C_truep(C_i_closurep(((C_word*)t0)[2]))){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5903,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5907,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:653: ##sys#peek-unsigned-integer */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[196]);
C_word av2[4];
av2[0]=*((C_word*)lf[196]+1);
av2[1]=t3;
av2[2]=((C_word*)t0)[2];
av2[3]=C_fix(0);
tp(4,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5913,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* csi.scm:655: chicken.base#port? */
t3=C_fast_retrieve(lf[235]);{
C_word av2[3];
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}}}}

/* k5797 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in ... */
static void C_ccall f_5799(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_5799,c,av);}
a=C_alloc(9);
t2=C_a_i_list1(&a,1,((C_word*)t0)[2]);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5808,a[2]=((C_word*)t0)[3],a[3]=t4,tmp=(C_word)a,a+=4,tmp));
t6=((C_word*)t4)[1];
f_5808(t6,((C_word*)t0)[4],((C_word*)t0)[2],t2);}

/* loop-print in k5797 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in ... */
static void C_fcall f_5808(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,4)))){
C_save_and_reclaim_args((void *)trf_5808,4,t0,t1,t2,t3);}
a=C_alloc(6);
t4=C_i_not_pair_p(t2);
t5=(C_truep(t4)?t4:C_i_nullp(t2));
if(C_truep(t5)){
/* csi.scm:642: printf */
t6=*((C_word*)lf[77]+1);{
C_word av2[3];
av2[0]=t6;
av2[1]=t1;
av2[2]=lf[188];
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}
else{
t6=C_i_car(t2);
if(C_truep(C_i_memq(t6,t3))){
/* csi.scm:644: fprintf */
t7=*((C_word*)lf[150]+1);{
C_word av2[4];
av2[0]=t7;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
av2[3]=lf[189];
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}
else{
t7=C_i_memq(C_u_i_car(t2),t3);
if(C_truep(C_i_not(t7))){
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5839,a[2]=t2,a[3]=t3,a[4]=((C_word*)t0)[3],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
/* csi.scm:646: fprintf */
t9=*((C_word*)lf[150]+1);{
C_word av2[5];
av2[0]=t9;
av2[1]=t8;
av2[2]=((C_word*)t0)[2];
av2[3]=lf[190];
av2[4]=C_u_i_car(t2);
((C_proc)(void*)(*((C_word*)t9+1)))(5,av2);}}
else{
t8=C_SCHEME_UNDEFINED;
t9=t1;{
C_word av2[2];
av2[0]=t9;
av2[1]=t8;
((C_proc)(void*)(*((C_word*)t9+1)))(2,av2);}}}}}

/* k5837 in loop-print in k5797 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in ... */
static void C_ccall f_5839(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_5839,c,av);}
a=C_alloc(3);
t2=C_u_i_cdr(((C_word*)t0)[2]);
t3=C_u_i_car(((C_word*)t0)[2]);
t4=C_a_i_cons(&a,2,t3,((C_word*)t0)[3]);
/* csi.scm:647: loop-print */
t5=((C_word*)((C_word*)t0)[4])[1];
f_5808(t5,((C_word*)t0)[5],t2,t4);}

/* k5901 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in ... */
static void C_ccall f_5903(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_5903,c,av);}
/* csi.scm:652: descseq */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
av2[3]=*((C_word*)lf[174]+1);
av2[4]=*((C_word*)lf[176]+1);
av2[5]=C_fix(1);
f_5399(6,av2);}}

/* k5905 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in ... */
static void C_ccall f_5907(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5907,c,av);}
/* csi.scm:653: sprintf */
t2=*((C_word*)lf[194]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[195];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k5911 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in ... */
static void C_ccall f_5913(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_5913,c,av);}
a=C_alloc(7);
if(C_truep(t1)){
t2=C_slot(((C_word*)t0)[2],C_fix(1));
t3=(C_truep(t2)?lf[197]:lf[198]);
t4=C_slot(((C_word*)t0)[2],C_fix(7));
t5=C_slot(((C_word*)t0)[2],C_fix(3));
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5932,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t3,a[5]=t4,a[6]=t5,tmp=(C_word)a,a+=7,tmp);
/* csi.scm:661: ##sys#peek-unsigned-integer */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[196]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[196]+1);
av2[1]=t6;
av2[2]=((C_word*)t0)[2];
av2[3]=C_fix(0);
tp(4,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5941,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* csi.scm:662: ##sys#locative? */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[234]);
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[234]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[2];
tp(3,av2);}}}

/* k5930 in k5911 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in ... */
static void C_ccall f_5932(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_5932,c,av);}
/* csi.scm:656: fprintf */
t2=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=lf[199];
av2[4]=((C_word*)t0)[4];
av2[5]=((C_word*)t0)[5];
av2[6]=((C_word*)t0)[6];
av2[7]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(8,av2);}}

/* k5939 in k5911 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in ... */
static void C_ccall f_5941(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_5941,c,av);}
a=C_alloc(6);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5948,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* csi.scm:664: ##sys#peek-unsigned-integer */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[196]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[196]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[2];
av2[3]=C_fix(0);
tp(4,av2);}}
else{
if(C_truep(C_anypointerp(((C_word*)t0)[2]))){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6065,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
/* csi.scm:679: ##sys#peek-unsigned-integer */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[196]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[196]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[2];
av2[3]=C_fix(0);
tp(4,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6071,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* csi.scm:680: ##sys#bytevector? */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[233]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[233]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[2];
tp(3,av2);}}}}

/* k5946 in k5939 in k5911 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in ... */
static void C_ccall f_5948(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_5948,c,av);}
t2=C_slot(((C_word*)t0)[2],C_fix(1));
t3=C_slot(((C_word*)t0)[2],C_fix(2));
switch(t3){
case C_fix(0):
/* csi.scm:663: fprintf */
t4=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=lf[200];
av2[4]=t1;
av2[5]=t2;
av2[6]=lf[201];
((C_proc)(void*)(*((C_word*)t4+1)))(7,av2);}
case C_fix(1):
/* csi.scm:663: fprintf */
t4=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=lf[200];
av2[4]=t1;
av2[5]=t2;
av2[6]=lf[202];
((C_proc)(void*)(*((C_word*)t4+1)))(7,av2);}
case C_fix(2):
/* csi.scm:663: fprintf */
t4=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=lf[200];
av2[4]=t1;
av2[5]=t2;
av2[6]=lf[203];
((C_proc)(void*)(*((C_word*)t4+1)))(7,av2);}
case C_fix(3):
/* csi.scm:663: fprintf */
t4=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=lf[200];
av2[4]=t1;
av2[5]=t2;
av2[6]=lf[204];
((C_proc)(void*)(*((C_word*)t4+1)))(7,av2);}
case C_fix(4):
/* csi.scm:663: fprintf */
t4=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=lf[200];
av2[4]=t1;
av2[5]=t2;
av2[6]=lf[205];
((C_proc)(void*)(*((C_word*)t4+1)))(7,av2);}
case C_fix(5):
/* csi.scm:663: fprintf */
t4=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=lf[200];
av2[4]=t1;
av2[5]=t2;
av2[6]=lf[206];
((C_proc)(void*)(*((C_word*)t4+1)))(7,av2);}
case C_fix(6):
/* csi.scm:663: fprintf */
t4=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=lf[200];
av2[4]=t1;
av2[5]=t2;
av2[6]=lf[207];
((C_proc)(void*)(*((C_word*)t4+1)))(7,av2);}
case C_fix(7):
/* csi.scm:663: fprintf */
t4=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=lf[200];
av2[4]=t1;
av2[5]=t2;
av2[6]=lf[208];
((C_proc)(void*)(*((C_word*)t4+1)))(7,av2);}
case C_fix(8):
/* csi.scm:663: fprintf */
t4=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=lf[200];
av2[4]=t1;
av2[5]=t2;
av2[6]=lf[209];
((C_proc)(void*)(*((C_word*)t4+1)))(7,av2);}
case C_fix(9):
/* csi.scm:663: fprintf */
t4=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=lf[200];
av2[4]=t1;
av2[5]=t2;
av2[6]=lf[210];
((C_proc)(void*)(*((C_word*)t4+1)))(7,av2);}
case C_fix(10):
/* csi.scm:663: fprintf */
t4=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=lf[200];
av2[4]=t1;
av2[5]=t2;
av2[6]=lf[211];
((C_proc)(void*)(*((C_word*)t4+1)))(7,av2);}
case C_fix(11):
/* csi.scm:663: fprintf */
t4=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=lf[200];
av2[4]=t1;
av2[5]=t2;
av2[6]=lf[212];
((C_proc)(void*)(*((C_word*)t4+1)))(7,av2);}
default:
t4=C_SCHEME_UNDEFINED;
/* csi.scm:663: fprintf */
t5=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t5;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=lf[200];
av2[4]=t1;
av2[5]=t2;
av2[6]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(7,av2);}}}

/* k6063 in k5939 in k5911 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in ... */
static void C_ccall f_6065(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6065,c,av);}
/* csi.scm:679: fprintf */
t2=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=lf[213];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k6069 in k5939 in k5911 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in ... */
static void C_ccall f_6071(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,6)))){
C_save_and_reclaim((void *)f_6071,c,av);}
a=C_alloc(7);
if(C_truep(t1)){
t2=C_block_size(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6077,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],a[4]=t2,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* csi.scm:682: fprintf */
t4=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[4];
av2[3]=lf[216];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}
else{
if(C_truep(C_lambdainfop(((C_word*)t0)[2]))){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6090,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
/* csi.scm:685: ##sys#lambda-info->string */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[218]);
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[218]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[2];
tp(3,av2);}}
else{
if(C_truep(C_i_structurep(((C_word*)t0)[2],lf[219]))){
t2=C_slot(((C_word*)t0)[2],C_fix(2));
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6102,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
t4=C_eqp(t2,C_fix(1));
t5=(C_truep(t4)?lf[222]:lf[223]);
/* csi.scm:688: fprintf */
t6=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t6;
av2[1]=t3;
av2[2]=((C_word*)t0)[4];
av2[3]=lf[224];
av2[4]=t2;
av2[5]=t5;
av2[6]=C_slot(((C_word*)t0)[2],C_fix(3));
((C_proc)(void*)(*((C_word*)t6+1)))(7,av2);}}
else{
if(C_truep(C_i_structurep(((C_word*)t0)[2],lf[225]))){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6199,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* csi.scm:702: fprintf */
t3=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=lf[228];
av2[4]=C_slot(((C_word*)t0)[2],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}
else{
if(C_truep(C_structurep(((C_word*)t0)[2]))){
t2=C_slot(((C_word*)t0)[2],C_fix(0));
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6305,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[3],a[5]=t2,a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* csi.scm:718: chicken.internal#hash-table-ref */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[231]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[231]+1);
av2[1]=t3;
av2[2]=C_retrieve2(lf[146],C_text("chicken.csi#describer-table"));
av2[3]=t2;
tp(4,av2);}}
else{
/* csi.scm:725: fprintf */
t2=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=lf[232];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}}}}}}

/* k6075 in k6069 in k5939 in k5911 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in ... */
static void C_ccall f_6077(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_6077,c,av);}
/* csi.scm:683: hexdump */
f_6580(((C_word*)t0)[2],((C_word*)t0)[3],((C_word*)t0)[4],*((C_word*)lf[215]+1),((C_word*)t0)[5]);}

/* k6088 in k6069 in k5939 in k5911 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in ... */
static void C_ccall f_6090(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6090,c,av);}
/* csi.scm:685: fprintf */
t2=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=lf[217];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k6100 in k6069 in k5939 in k5911 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in ... */
static void C_ccall f_6102(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_6102,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6105,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* csi.scm:690: fprintf */
t3=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=lf[221];
av2[4]=C_slot(((C_word*)t0)[2],C_fix(4));
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k6103 in k6100 in k6069 in k5939 in k5911 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in ... */
static void C_ccall f_6105(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_6105,c,av);}
a=C_alloc(8);
t2=C_slot(((C_word*)t0)[2],C_fix(1));
t3=C_block_size(t2);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6116,a[2]=t3,a[3]=((C_word*)t0)[3],a[4]=t2,a[5]=t5,tmp=(C_word)a,a+=6,tmp));
t7=((C_word*)t5)[1];
f_6116(t7,((C_word*)t0)[4],C_fix(0));}

/* doloop1238 in k6103 in k6100 in k6069 in k5939 in k5911 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in ... */
static void C_fcall f_6116(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,0,3)))){
C_save_and_reclaim_args((void *)trf_6116,3,t0,t1,t2);}
a=C_alloc(14);
if(C_truep(C_fixnum_greater_or_equal_p(t2,((C_word*)t0)[2]))){
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6124,a[2]=((C_word*)t0)[3],tmp=(C_word)a,a+=3,tmp);
t4=C_slot(((C_word*)t0)[4],t2);
t5=C_i_check_list_2(t4,lf[101]);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6145,a[2]=((C_word*)t0)[5],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6154,a[2]=t8,a[3]=t3,tmp=(C_word)a,a+=4,tmp));
t10=((C_word*)t8)[1];
f_6154(t10,t6,t4);}}

/* g1244 in doloop1238 in k6103 in k6100 in k6069 in k5939 in k5911 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in ... */
static void C_fcall f_6124(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,5)))){
C_save_and_reclaim_args((void *)trf_6124,3,t0,t1,t2);}
/* csi.scm:698: fprintf */
t3=*((C_word*)lf[150]+1);{
C_word av2[6];
av2[0]=t3;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
av2[3]=lf[220];
av2[4]=C_slot(t2,C_fix(0));
av2[5]=C_slot(t2,C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k6143 in doloop1238 in k6103 in k6100 in k6069 in k5939 in k5911 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in ... */
static void C_ccall f_6145(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6145,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_6116(t2,((C_word*)t0)[3],C_fixnum_plus(((C_word*)t0)[4],C_fix(1)));}

/* for-each-loop1243 in doloop1238 in k6103 in k6100 in k6069 in k5939 in k5911 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in ... */
static void C_fcall f_6154(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_6154,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6164,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* csi.scm:696: g1244 */
t4=((C_word*)t0)[3];
f_6124(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k6162 in for-each-loop1243 in doloop1238 in k6103 in k6100 in k6069 in k5939 in k5911 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in ... */
static void C_ccall f_6164(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6164,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_6154(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k6197 in k6069 in k5939 in k5911 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in ... */
static void C_ccall f_6199(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_6199,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6200,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=C_slot(((C_word*)t0)[2],C_fix(1));
t4=C_i_check_list_2(t3,lf[101]);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6268,a[2]=t6,a[3]=t2,tmp=(C_word)a,a+=4,tmp));
t8=((C_word*)t6)[1];
f_6268(t8,((C_word*)t0)[4],t3);}

/* g1266 in k6197 in k6069 in k5939 in k5911 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in ... */
static void C_fcall f_6200(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,4)))){
C_save_and_reclaim_args((void *)trf_6200,3,t0,t1,t2);}
a=C_alloc(6);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6204,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=((C_word*)t0)[3],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
/* csi.scm:705: fprintf */
t4=*((C_word*)lf[150]+1);{
C_word av2[5];
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[3];
av2[3]=lf[227];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k6202 in g1266 in k6197 in k6069 in k5939 in k5911 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in ... */
static void C_ccall f_6204(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_6204,c,av);}
a=C_alloc(7);
t2=C_slot(((C_word*)t0)[2],C_fix(2));
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6213,a[2]=t4,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp));
t6=((C_word*)t4)[1];
f_6213(t6,((C_word*)t0)[5],t2);}

/* loop in k6202 in g1266 in k6197 in k6069 in k5939 in k5911 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in ... */
static void C_fcall f_6213(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(13,0,3)))){
C_save_and_reclaim_args((void *)trf_6213,3,t0,t1,t2);}
a=C_alloc(13);
if(C_truep(C_i_nullp(t2))){
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6223,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t4=C_i_caar(t2);
t5=C_eqp(((C_word*)t0)[3],t4);
if(C_truep(t5)){
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6236,a[2]=t3,a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6241,a[2]=((C_word*)t0)[4],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* csi.scm:709: ##sys#with-print-length-limit */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[88]);
C_word av2[4];
av2[0]=*((C_word*)lf[88]+1);
av2[1]=t6;
av2[2]=C_fix(100);
av2[3]=t7;
tp(4,av2);}}
else{
/* csi.scm:714: loop */
t9=t1;
t10=C_i_cddr(t2);
t1=t9;
t2=t10;
goto loop;}}}

/* k6221 in loop in k6202 in g1266 in k6197 in k6069 in k5939 in k5911 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in ... */
static void C_ccall f_6223(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6223,c,av);}
/* csi.scm:714: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_6213(t2,((C_word*)t0)[3],C_i_cddr(((C_word*)t0)[4]));}

/* k6234 in loop in k6202 in g1266 in k6197 in k6069 in k5939 in k5911 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in ... */
static void C_ccall f_6236(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6236,c,av);}
/* csi.scm:713: scheme#newline */
t2=*((C_word*)lf[86]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* a6240 in loop in k6202 in g1266 in k6197 in k6069 in k5939 in k5911 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in ... */
static void C_ccall f_6241(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_6241,c,av);}
/* csi.scm:712: fprintf */
t2=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
av2[3]=lf[226];
av2[4]=C_i_cdar(((C_word*)t0)[3]);
av2[5]=C_i_cadr(((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* for-each-loop1265 in k6197 in k6069 in k5939 in k5911 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in ... */
static void C_fcall f_6268(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_6268,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6278,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* csi.scm:703: g1266 */
t4=((C_word*)t0)[3];
f_6200(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k6276 in for-each-loop1265 in k6197 in k6069 in k5939 in k5911 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in ... */
static void C_ccall f_6278(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6278,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_6268(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k6303 in k6069 in k5939 in k5911 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in ... */
static void C_ccall f_6305(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_6305,c,av);}
a=C_alloc(4);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6309,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* csi.scm:718: g1295 */
t3=t2;
f_6309(t3,((C_word*)t0)[4],t1);}
else{
t2=C_i_assq(((C_word*)t0)[5],C_retrieve2(lf[144],C_text("chicken.csi#bytevector-data")));
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6323,a[2]=((C_word*)t0)[6],tmp=(C_word)a,a+=3,tmp);
/* csi.scm:718: g1306 */
t4=t3;
f_6323(t4,((C_word*)t0)[4],t2);}
else{
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6388,a[2]=((C_word*)t0)[6],a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
/* csi.scm:723: fprintf */
t4=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[3];
av2[3]=lf[230];
av2[4]=C_slot(((C_word*)t0)[2],C_fix(0));
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}}}

/* g1295 in k6303 in k6069 in k5939 in k5911 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in ... */
static void C_fcall f_6309(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,3)))){
C_save_and_reclaim_args((void *)trf_6309,3,t0,t1,t2);}
/* csi.scm:718: g1303 */
t3=t2;{
C_word av2[4];
av2[0]=t3;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
av2[3]=((C_word*)t0)[3];
((C_proc)C_fast_retrieve_proc(t3))(4,av2);}}

/* g1306 in k6303 in k6069 in k5939 in k5911 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in ... */
static void C_fcall f_6323(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(19,0,3)))){
C_save_and_reclaim_args((void *)trf_6323,3,t0,t1,t2);}
a=C_alloc(19);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6331,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
t4=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t5=t4;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=((C_word*)t6)[1];
t8=C_i_cdr(t2);
t9=C_i_check_list_2(t8,lf[138]);
t10=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6344,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
t11=C_SCHEME_UNDEFINED;
t12=(*a=C_VECTOR_TYPE|1,a[1]=t11,tmp=(C_word)a,a+=2,tmp);
t13=C_set_block_item(t12,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6350,a[2]=t6,a[3]=t12,a[4]=t7,tmp=(C_word)a,a+=5,tmp));
t14=((C_word*)t12)[1];
f_6350(t14,t10,t8);}

/* k6329 in g1306 in k6303 in k6069 in k5939 in k5911 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in ... */
static void C_ccall f_6331(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6331,c,av);}{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
C_apply(4,av2);}}

/* k6342 in g1306 in k6303 in k6069 in k5939 in k5911 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in ... */
static void C_ccall f_6344(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_6344,c,av);}
a=C_alloc(3);
t2=C_a_i_list1(&a,1,C_fix(0));
/* csi.scm:721: scheme#append */
t3=*((C_word*)lf[229]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* map-loop1311 in g1306 in k6303 in k6069 in k5939 in k5911 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in ... */
static void C_fcall f_6350(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_6350,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6375,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* csi.scm:721: g1317 */
t4=*((C_word*)lf[54]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k6373 in map-loop1311 in g1306 in k6303 in k6069 in k5939 in k5911 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in ... */
static void C_ccall f_6375(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_6375,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_6350(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k6386 in k6303 in k6069 in k5939 in k5911 in k5794 in k5695 in k5529 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in ... */
static void C_ccall f_6388(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_6388,c,av);}
/* csi.scm:724: descseq */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=C_SCHEME_FALSE;
av2[3]=*((C_word*)lf[174]+1);
av2[4]=*((C_word*)lf[176]+1);
av2[5]=C_fix(1);
f_5399(6,av2);}}

/* k6413 in chicken.csi#describe in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 in ... */
static void C_ccall f_6415(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6415,c,av);}
/* csi.scm:590: fprintf */
t2=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=lf[237];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* chicken.csi#set-describer! in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_6423(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6423,c,av);}
t4=C_i_check_symbol_2(t2,lf[240]);
/* csi.scm:730: chicken.internal#hash-table-set! */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[241]);
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[241]+1);
av2[1]=t1;
av2[2]=C_retrieve2(lf[146],C_text("chicken.csi#describer-table"));
av2[3]=t2;
av2[4]=t3;
tp(5,av2);}}

/* chicken.csi#dump in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_fcall f_6432(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,4)))){
C_save_and_reclaim_args((void *)trf_6432,3,t1,t2,t3);}
a=C_alloc(9);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6434,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6540,a[2]=t4,tmp=(C_word)a,a+=3,tmp);
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6545,a[2]=t5,tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_i_nullp(t3))){
/* csi.scm:737: def-len1350 */
t7=t6;
f_6545(t7,t1);}
else{
t7=C_i_car(t3);
if(C_truep(C_mk_bool(C_unfix(C_i_length(t3)) >= 1))){
/* csi.scm:737: def-out1351 */
t8=t5;
f_6540(t8,t1,t7);}
else{
t8=C_u_i_list_ref(t3,1);
/* csi.scm:737: body1348 */
t9=t4;
f_6434(t9,t1,t7,t8);}}}

/* body1348 in chicken.csi#dump in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 in ... */
static void C_fcall f_6434(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,4)))){
C_save_and_reclaim_args((void *)trf_6434,4,t0,t1,t2,t3);}
a=C_alloc(9);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6437,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_immp(((C_word*)t0)[2]))){
/* csi.scm:741: ##sys#error */
t5=*((C_word*)lf[31]+1);{
C_word av2[5];
av2[0]=t5;
av2[1]=t1;
av2[2]=lf[243];
av2[3]=lf[244];
av2[4]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}
else{
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6459,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t3,a[5]=t4,tmp=(C_word)a,a+=6,tmp);
/* csi.scm:742: ##sys#bytevector? */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[233]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[233]+1);
av2[1]=t5;
av2[2]=((C_word*)t0)[2];
tp(3,av2);}}}

/* bestlen in body1348 in chicken.csi#dump in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in ... */
static void C_fcall f_6437(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,3)))){
C_save_and_reclaim_args((void *)trf_6437,3,t0,t1,t2);}
if(C_truep(((C_word*)t0)[2])){
/* csi.scm:740: scheme#min */
t3=*((C_word*)lf[242]+1);{
C_word av2[4];
av2[0]=t3;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k6457 in body1348 in chicken.csi#dump in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in ... */
static void C_ccall f_6459(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,5)))){
C_save_and_reclaim((void *)f_6459,c,av);}
a=C_alloc(5);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6466,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* csi.scm:742: bestlen */
t3=((C_word*)t0)[5];
f_6437(t3,t2,C_block_size(((C_word*)t0)[3]));}
else{
if(C_truep(C_i_stringp(((C_word*)t0)[3]))){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6483,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* csi.scm:743: bestlen */
t3=((C_word*)t0)[5];
f_6437(t3,t2,C_block_size(((C_word*)t0)[3]));}
else{
t2=C_immp(((C_word*)t0)[3]);
t3=C_i_not(t2);
t4=(C_truep(t3)?C_anypointerp(((C_word*)t0)[3]):C_SCHEME_FALSE);
if(C_truep(t4)){
/* csi.scm:745: hexdump */
f_6580(((C_word*)t0)[2],((C_word*)t0)[3],C_fix(32),*((C_word*)lf[245]+1),((C_word*)t0)[4]);}
else{
t5=C_structurep(((C_word*)t0)[3]);
t6=(C_truep(t5)?C_i_assq(C_slot(((C_word*)t0)[3],C_fix(0)),C_retrieve2(lf[144],C_text("chicken.csi#bytevector-data"))):C_SCHEME_FALSE);
if(C_truep(t6)){
t7=C_slot(((C_word*)t0)[3],C_fix(1));
t8=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6512,a[2]=((C_word*)t0)[2],a[3]=t7,a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* csi.scm:748: bestlen */
t9=((C_word*)t0)[5];
f_6437(t9,t8,C_block_size(t7));}
else{
/* csi.scm:749: ##sys#error */
t7=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t7;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[243];
av2[3]=lf[246];
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t7+1)))(5,av2);}}}}}}

/* k6464 in k6457 in body1348 in chicken.csi#dump in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in ... */
static void C_ccall f_6466(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_6466,c,av);}
/* csi.scm:742: hexdump */
f_6580(((C_word*)t0)[2],((C_word*)t0)[3],t1,*((C_word*)lf[215]+1),((C_word*)t0)[4]);}

/* k6481 in k6457 in body1348 in chicken.csi#dump in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in ... */
static void C_ccall f_6483(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_6483,c,av);}
/* csi.scm:743: hexdump */
f_6580(((C_word*)t0)[2],((C_word*)t0)[3],t1,*((C_word*)lf[215]+1),((C_word*)t0)[4]);}

/* k6510 in k6457 in body1348 in chicken.csi#dump in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in ... */
static void C_ccall f_6512(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_6512,c,av);}
/* csi.scm:748: hexdump */
f_6580(((C_word*)t0)[2],((C_word*)t0)[3],t1,*((C_word*)lf[215]+1),((C_word*)t0)[4]);}

/* def-out1351 in chicken.csi#dump in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 in ... */
static void C_fcall f_6540(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,3)))){
C_save_and_reclaim_args((void *)trf_6540,3,t0,t1,t2);}
/* csi.scm:737: body1348 */
t3=((C_word*)t0)[2];
f_6434(t3,t1,t2,*((C_word*)lf[84]+1));}

/* def-len1350 in chicken.csi#dump in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 in ... */
static void C_fcall f_6545(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,2)))){
C_save_and_reclaim_args((void *)trf_6545,2,t0,t1);}
/* csi.scm:737: def-out1351 */
t2=((C_word*)t0)[2];
f_6540(t2,t1,C_SCHEME_FALSE);}

/* chicken.csi#hexdump in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_fcall f_6580(C_word t1,C_word t2,C_word t3,C_word t4,C_word t5){
C_word tmp;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,6)))){
C_save_and_reclaim_args((void *)trf_6580,5,t1,t2,t3,t4,t5);}
a=C_alloc(12);
t6=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_6583,tmp=(C_word)a,a+=2,tmp);
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6615,a[2]=t3,a[3]=t8,a[4]=t5,a[5]=t4,a[6]=t2,a[7]=t6,tmp=(C_word)a,a+=8,tmp));
t10=((C_word*)t8)[1];
f_6615(t10,t1,C_fix(0));}

/* justify in chicken.csi#hexdump in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 in ... */
static void C_fcall f_6583(C_word t1,C_word t2,C_word t3,C_word t4,C_word t5){
C_word tmp;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,3)))){
C_save_and_reclaim_args((void *)trf_6583,5,t1,t2,t3,t4,t5);}
a=C_alloc(5);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6587,a[2]=t3,a[3]=t1,a[4]=t5,tmp=(C_word)a,a+=5,tmp);
/* ##sys#number->string */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[247]);
C_word av2[4];
av2[0]=*((C_word*)lf[247]+1);
av2[1]=t6;
av2[2]=t2;
av2[3]=t4;
tp(4,av2);}}

/* k6585 in justify in chicken.csi#hexdump in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in ... */
static void C_ccall f_6587(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_6587,c,av);}
a=C_alloc(4);
t2=C_block_size(t1);
if(C_truep(C_fixnum_lessp(t2,((C_word*)t0)[2]))){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6603,a[2]=((C_word*)t0)[3],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* csi.scm:762: make-string */
t4=*((C_word*)lf[117]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_fixnum_difference(((C_word*)t0)[2],t2);
av2[3]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k6601 in k6585 in justify in chicken.csi#hexdump in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in ... */
static void C_ccall f_6603(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6603,c,av);}
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[15]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[15]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
tp(4,av2);}}

/* doloop1386 in chicken.csi#hexdump in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 in ... */
static void C_fcall f_6615(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,0,5)))){
C_save_and_reclaim_args((void *)trf_6615,3,t0,t1,t2);}
a=C_alloc(14);
if(C_truep(C_fixnum_greater_or_equal_p(t2,((C_word*)t0)[2]))){
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_6625,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t2,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[2],a[7]=((C_word*)t0)[5],a[8]=((C_word*)t0)[6],a[9]=((C_word*)t0)[7],tmp=(C_word)a,a+=10,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6783,a[2]=t3,a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
/* csi.scm:767: justify */
f_6583(t4,t2,C_fix(4),C_fix(10),C_make_character(32));}}

/* k6623 in doloop1386 in chicken.csi#hexdump in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in ... */
static void C_ccall f_6625(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_6625,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_6628,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],tmp=(C_word)a,a+=10,tmp);
/* ##sys#write-char/port */
t3=C_fast_retrieve(lf[248]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_make_character(58);
av2[3]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k6626 in k6623 in doloop1386 in chicken.csi#hexdump in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in ... */
static void C_ccall f_6628(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(19,c,4)))){
C_save_and_reclaim((void *)f_6628,c,av);}
a=C_alloc(19);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_6631,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6700,a[2]=((C_word*)t0)[6],a[3]=((C_word*)t0)[5],a[4]=t4,a[5]=((C_word*)t0)[9],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],tmp=(C_word)a,a+=8,tmp));
t6=((C_word*)t4)[1];
f_6700(t6,t2,C_fix(0),((C_word*)t0)[4]);}

/* k6629 in k6626 in k6623 in doloop1386 in chicken.csi#hexdump in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in ... */
static void C_ccall f_6631(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_6631,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_6634,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
/* ##sys#write-char/port */
t3=C_fast_retrieve(lf[248]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_make_character(32);
av2[3]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k6632 in k6629 in k6626 in k6623 in doloop1386 in chicken.csi#hexdump in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in ... */
static void C_ccall f_6634(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,4)))){
C_save_and_reclaim((void *)f_6634,c,av);}
a=C_alloc(15);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6637,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6649,a[2]=((C_word*)t0)[6],a[3]=t4,a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[7],a[6]=((C_word*)t0)[8],tmp=(C_word)a,a+=7,tmp));
t6=((C_word*)t4)[1];
f_6649(t6,t2,C_fix(0),((C_word*)t0)[4]);}

/* k6635 in k6632 in k6629 in k6626 in k6623 in doloop1386 in chicken.csi#hexdump in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in ... */
static void C_ccall f_6637(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_6637,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6640,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* ##sys#write-char/port */
t3=C_fast_retrieve(lf[248]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_make_character(10);
av2[3]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k6638 in k6635 in k6632 in k6629 in k6626 in k6623 in doloop1386 in chicken.csi#hexdump in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in ... */
static void C_ccall f_6640(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6640,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_6615(t2,((C_word*)t0)[3],C_fixnum_plus(((C_word*)t0)[4],C_fix(16)));}

/* doloop1397 in k6632 in k6629 in k6626 in k6623 in doloop1386 in chicken.csi#hexdump in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in ... */
static void C_fcall f_6649(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,3)))){
C_save_and_reclaim_args((void *)trf_6649,4,t0,t1,t2,t3);}
a=C_alloc(7);
t4=C_fixnum_greater_or_equal_p(t2,C_fix(16));
t5=(C_truep(t4)?t4:C_fixnum_greater_or_equal_p(t3,((C_word*)t0)[2]));
if(C_truep(t5)){
t6=C_SCHEME_UNDEFINED;
t7=t1;{
C_word av2[2];
av2[0]=t7;
av2[1]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}
else{
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6662,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t2,a[5]=t3,a[6]=((C_word*)t0)[4],tmp=(C_word)a,a+=7,tmp);
/* csi.scm:784: ref */
t7=((C_word*)t0)[5];{
C_word av2[4];
av2[0]=t7;
av2[1]=t6;
av2[2]=((C_word*)t0)[6];
av2[3]=t3;
((C_proc)C_fast_retrieve_proc(t7))(4,av2);}}}

/* k6660 in doloop1397 in k6632 in k6629 in k6626 in k6623 in doloop1386 in chicken.csi#hexdump in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in ... */
static void C_ccall f_6662(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_6662,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6665,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t3=C_fixnum_greater_or_equal_p(t1,C_fix(32));
t4=(C_truep(t3)?C_fixnum_lessp(t1,C_fix(128)):C_SCHEME_FALSE);
if(C_truep(t4)){
/* ##sys#write-char/port */
t5=C_fast_retrieve(lf[248]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=C_make_character(C_unfix(t1));
av2[3]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}
else{
/* ##sys#write-char/port */
t5=C_fast_retrieve(lf[248]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=C_make_character(46);
av2[3]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}}

/* k6663 in k6660 in doloop1397 in k6632 in k6629 in k6626 in k6623 in doloop1386 in chicken.csi#hexdump in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in ... */
static void C_ccall f_6665(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6665,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_6649(t2,((C_word*)t0)[3],C_fixnum_plus(((C_word*)t0)[4],C_fix(1)),C_fixnum_plus(((C_word*)t0)[5],C_fix(1)));}

/* doloop1396 in k6626 in k6623 in doloop1386 in chicken.csi#hexdump in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in ... */
static void C_fcall f_6700(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,3)))){
C_save_and_reclaim_args((void *)trf_6700,4,t0,t1,t2,t3);}
a=C_alloc(10);
t4=C_fixnum_greater_or_equal_p(t2,C_fix(16));
t5=(C_truep(t4)?t4:C_fixnum_greater_or_equal_p(t3,((C_word*)t0)[2]));
if(C_truep(t5)){
if(C_truep(C_fixnum_greater_or_equal_p(t3,((C_word*)t0)[2]))){
t6=C_fixnum_modulo(((C_word*)t0)[2],C_fix(16));
t7=C_eqp(t6,C_fix(0));
if(C_truep(t7)){
t8=C_SCHEME_UNDEFINED;
t9=t1;{
C_word av2[2];
av2[0]=t9;
av2[1]=t8;
((C_proc)(void*)(*((C_word*)t9+1)))(2,av2);}}
else{
t8=C_fixnum_difference(C_fix(16),t6);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6734,a[2]=t10,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp));
t12=((C_word*)t10)[1];
f_6734(t12,t1,t8);}}
else{
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}
else{
t6=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_6754,a[2]=((C_word*)t0)[4],a[3]=t1,a[4]=t2,a[5]=t3,a[6]=((C_word*)t0)[3],a[7]=((C_word*)t0)[5],a[8]=((C_word*)t0)[6],a[9]=((C_word*)t0)[7],tmp=(C_word)a,a+=10,tmp);
/* ##sys#write-char/port */
t7=C_fast_retrieve(lf[248]);{
C_word av2[4];
av2[0]=t7;
av2[1]=t6;
av2[2]=C_make_character(32);
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}}

/* doloop1405 in doloop1396 in k6626 in k6623 in doloop1386 in chicken.csi#hexdump in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in ... */
static void C_fcall f_6734(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,3)))){
C_save_and_reclaim_args((void *)trf_6734,3,t0,t1,t2);}
a=C_alloc(5);
t3=C_eqp(t2,C_fix(0));
if(C_truep(t3)){
t4=C_SCHEME_UNDEFINED;
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6744,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* csi.scm:777: display */
t5=*((C_word*)lf[94]+1);{
C_word av2[4];
av2[0]=t5;
av2[1]=t4;
av2[2]=lf[249];
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}}

/* k6742 in doloop1405 in doloop1396 in k6626 in k6623 in doloop1386 in chicken.csi#hexdump in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in ... */
static void C_ccall f_6744(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6744,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_6734(t2,((C_word*)t0)[3],C_fixnum_difference(((C_word*)t0)[4],C_fix(1)));}

/* k6752 in doloop1396 in k6626 in k6623 in doloop1386 in chicken.csi#hexdump in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in ... */
static void C_ccall f_6754(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,3)))){
C_save_and_reclaim((void *)f_6754,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6757,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6772,a[2]=t2,a[3]=((C_word*)t0)[6],tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6776,a[2]=((C_word*)t0)[7],a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* csi.scm:779: ref */
t5=((C_word*)t0)[8];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[9];
av2[3]=((C_word*)t0)[5];
((C_proc)C_fast_retrieve_proc(t5))(4,av2);}}

/* k6755 in k6752 in doloop1396 in k6626 in k6623 in doloop1386 in chicken.csi#hexdump in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in ... */
static void C_ccall f_6757(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6757,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_6700(t2,((C_word*)t0)[3],C_fixnum_plus(((C_word*)t0)[4],C_fix(1)),C_fixnum_plus(((C_word*)t0)[5],C_fix(1)));}

/* k6770 in k6752 in doloop1396 in k6626 in k6623 in doloop1386 in chicken.csi#hexdump in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in ... */
static void C_ccall f_6772(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6772,c,av);}
/* csi.scm:779: display */
t2=*((C_word*)lf[94]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k6774 in k6752 in doloop1396 in k6626 in k6623 in doloop1386 in chicken.csi#hexdump in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in ... */
static void C_ccall f_6776(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_6776,c,av);}
/* csi.scm:779: justify */
f_6583(((C_word*)t0)[3],t1,C_fix(2),C_fix(16),C_make_character(48));}

/* k6781 in doloop1386 in chicken.csi#hexdump in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in ... */
static void C_ccall f_6783(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6783,c,av);}
/* csi.scm:767: display */
t2=*((C_word*)lf[94]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.csi#show-frameinfo in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_fcall f_6785(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,3)))){
C_save_and_reclaim_args((void *)trf_6785,2,t1,t2);}
a=C_alloc(8);
t3=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_6788,tmp=(C_word)a,a+=2,tmp);
t4=C_fast_retrieve(lf[96]);
t5=(C_truep(C_fast_retrieve(lf[96]))?C_fast_retrieve(lf[96]):C_SCHEME_END_OF_LIST);
t6=C_i_length(t5);
t7=(C_truep(C_u_i_memq(t2,t5))?t2:C_SCHEME_FALSE);
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6811,a[2]=t6,a[3]=t3,a[4]=t1,a[5]=t5,tmp=(C_word)a,a+=6,tmp);
if(C_truep(t7)){
t9=t8;
f_6811(t9,t7);}
else{
t9=C_fixnum_greaterp(t6,C_fix(0));
t10=t8;
f_6811(t10,(C_truep(t9)?C_i_list_ref(t5,C_fixnum_difference(t6,C_fix(1))):C_SCHEME_FALSE));}}

/* prin1 in chicken.csi#show-frameinfo in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 in ... */
static void C_fcall f_6788(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,0,3)))){
C_save_and_reclaim_args((void *)trf_6788,2,t1,t2);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6794,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:798: ##sys#with-print-length-limit */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[88]);
C_word av2[4];
av2[0]=*((C_word*)lf[88]+1);
av2[1]=t1;
av2[2]=C_fix(100);
av2[3]=t3;
tp(4,av2);}}

/* a6793 in prin1 in chicken.csi#show-frameinfo in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in ... */
static void C_ccall f_6794(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6794,c,av);}
/* csi.scm:801: ##sys#print */
t2=*((C_word*)lf[87]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
av2[3]=C_SCHEME_TRUE;
av2[4]=*((C_word*)lf[84]+1);
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k6809 in chicken.csi#show-frameinfo in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 in ... */
static void C_fcall f_6811(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,4)))){
C_save_and_reclaim_args((void *)trf_6811,2,t0,t1);}
a=C_alloc(6);
t2=C_mutate(&lf[8] /* (set! chicken.csi#selected-frame ...) */,t1);
t3=C_fixnum_difference(((C_word*)t0)[2],C_fix(1));
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6820,a[2]=t5,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp));
t7=((C_word*)t5)[1];
f_6820(t7,((C_word*)t0)[4],((C_word*)t0)[5],t3);}

/* doloop1442 in k6809 in chicken.csi#show-frameinfo in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in ... */
static void C_fcall f_6820(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,0,4)))){
C_save_and_reclaim_args((void *)trf_6820,4,t0,t1,t2,t3);}
a=C_alloc(14);
if(C_truep(C_i_nullp(t2))){
t4=C_SCHEME_UNDEFINED;
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t4=C_i_car(t2);
t5=C_eqp(C_retrieve2(lf[8],C_text("chicken.csi#selected-frame")),t4);
t6=C_slot(t4,C_fix(1));
t7=C_slot(t4,C_fix(2));
t8=C_i_structurep(t7,lf[99]);
t9=(C_truep(t8)?C_slot(t7,C_fix(1)):t7);
t10=*((C_word*)lf[84]+1);
t11=*((C_word*)lf[84]+1);
t12=C_i_check_port_2(*((C_word*)lf[84]+1),C_fix(2),C_SCHEME_TRUE,lf[85]);
t13=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_6851,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,a[5]=t3,a[6]=t5,a[7]=t8,a[8]=((C_word*)t0)[3],a[9]=t7,a[10]=t6,a[11]=t9,a[12]=t10,a[13]=t4,tmp=(C_word)a,a+=14,tmp);
if(C_truep(t5)){
/* csi.scm:817: ##sys#print */
t14=*((C_word*)lf[87]+1);{
C_word av2[5];
av2[0]=t14;
av2[1]=t13;
av2[2]=C_make_character(42);
av2[3]=C_SCHEME_FALSE;
av2[4]=*((C_word*)lf[84]+1);
((C_proc)(void*)(*((C_word*)t14+1)))(5,av2);}}
else{
/* csi.scm:817: ##sys#print */
t14=*((C_word*)lf[87]+1);{
C_word av2[5];
av2[0]=t14;
av2[1]=t13;
av2[2]=C_make_character(32);
av2[3]=C_SCHEME_FALSE;
av2[4]=t10;
((C_proc)(void*)(*((C_word*)t14+1)))(5,av2);}}}}

/* k6849 in doloop1442 in k6809 in chicken.csi#show-frameinfo in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in ... */
static void C_ccall f_6851(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,4)))){
C_save_and_reclaim((void *)f_6851,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_6854,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],tmp=(C_word)a,a+=14,tmp);
/* csi.scm:817: ##sys#print */
t3=*((C_word*)lf[87]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[12];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k6852 in k6849 in doloop1442 in k6809 in chicken.csi#show-frameinfo in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in ... */
static void C_ccall f_6854(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,3)))){
C_save_and_reclaim((void *)f_6854,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_6857,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],tmp=(C_word)a,a+=14,tmp);
/* csi.scm:817: ##sys#write-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[90]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[90]+1);
av2[1]=t2;
av2[2]=C_make_character(58);
av2[3]=((C_word*)t0)[12];
tp(4,av2);}}

/* k6855 in k6852 in k6849 in doloop1442 in k6809 in chicken.csi#show-frameinfo in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in ... */
static void C_ccall f_6857(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,4)))){
C_save_and_reclaim((void *)f_6857,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_6860,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],tmp=(C_word)a,a+=14,tmp);
t3=(C_truep(((C_word*)t0)[7])?C_i_pairp(C_slot(((C_word*)t0)[9],C_fix(2))):C_SCHEME_FALSE);
if(C_truep(t3)){
/* csi.scm:817: ##sys#print */
t4=*((C_word*)lf[87]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=lf[255];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[12];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}
else{
/* csi.scm:817: ##sys#print */
t4=*((C_word*)lf[87]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=lf[256];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[12];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}}

/* k6858 in k6855 in k6852 in k6849 in doloop1442 in k6809 in chicken.csi#show-frameinfo in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in ... */
static void C_ccall f_6860(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,3)))){
C_save_and_reclaim((void *)f_6860,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_6863,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],tmp=(C_word)a,a+=14,tmp);
/* csi.scm:817: ##sys#write-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[90]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[90]+1);
av2[1]=t2;
av2[2]=C_make_character(9);
av2[3]=((C_word*)t0)[12];
tp(4,av2);}}

/* k6861 in k6858 in k6855 in k6852 in k6849 in doloop1442 in k6809 in chicken.csi#show-frameinfo in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in ... */
static void C_ccall f_6863(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,4)))){
C_save_and_reclaim((void *)f_6863,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_6866,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],tmp=(C_word)a,a+=13,tmp);
/* csi.scm:817: ##sys#print */
t3=*((C_word*)lf[87]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_slot(((C_word*)t0)[13],C_fix(0));
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[12];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k6864 in k6861 in k6858 in k6855 in k6852 in k6849 in doloop1442 in k6809 in chicken.csi#show-frameinfo in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in ... */
static void C_ccall f_6866(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,4)))){
C_save_and_reclaim((void *)f_6866,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_6869,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],tmp=(C_word)a,a+=12,tmp);
/* csi.scm:817: ##sys#print */
t3=*((C_word*)lf[87]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[254];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[12];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k6867 in k6864 in k6861 in k6858 in k6855 in k6852 in k6849 in doloop1442 in k6809 in chicken.csi#show-frameinfo in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in ... */
static void C_ccall f_6869(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,3)))){
C_save_and_reclaim((void *)f_6869,c,av);}
a=C_alloc(16);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_6872,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],tmp=(C_word)a,a+=11,tmp);
if(C_truep(((C_word*)t0)[11])){
t3=*((C_word*)lf[84]+1);
t4=*((C_word*)lf[84]+1);
t5=C_i_check_port_2(*((C_word*)lf[84]+1),C_fix(2),C_SCHEME_TRUE,lf[85]);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7012,a[2]=t2,a[3]=t3,a[4]=((C_word*)t0)[11],tmp=(C_word)a,a+=5,tmp);
/* csi.scm:822: ##sys#write-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[90]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[90]+1);
av2[1]=t6;
av2[2]=C_make_character(91);
av2[3]=*((C_word*)lf[84]+1);
tp(4,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_6872(2,av2);}}}

/* k6870 in k6867 in k6864 in k6861 in k6858 in k6855 in k6852 in k6849 in doloop1442 in k6809 in chicken.csi#show-frameinfo in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in ... */
static void C_ccall f_6872(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_6872,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_6875,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],tmp=(C_word)a,a+=10,tmp);
if(C_truep(((C_word*)t0)[10])){
/* csi.scm:823: prin1 */
f_6788(t2,((C_word*)t0)[10]);}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_6875(2,av2);}}}

/* k6873 in k6870 in k6867 in k6864 in k6861 in k6858 in k6855 in k6852 in k6849 in doloop1442 in k6809 in chicken.csi#show-frameinfo in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in ... */
static void C_ccall f_6875(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_6875,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_6878,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],tmp=(C_word)a,a+=10,tmp);
/* csi.scm:824: newline */
t3=*((C_word*)lf[86]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k6876 in k6873 in k6870 in k6867 in k6864 in k6861 in k6858 in k6855 in k6852 in k6849 in doloop1442 in k6809 in chicken.csi#show-frameinfo in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in ... */
static void C_ccall f_6878(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,4)))){
C_save_and_reclaim((void *)f_6878,c,av);}
a=C_alloc(15);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6881,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t3=(C_truep(((C_word*)t0)[6])?((C_word*)t0)[7]:C_SCHEME_FALSE);
if(C_truep(t3)){
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6894,a[2]=((C_word*)t0)[8],tmp=(C_word)a,a+=3,tmp);
t5=C_slot(((C_word*)t0)[9],C_fix(2));
t6=C_slot(((C_word*)t0)[9],C_fix(3));
t7=C_i_check_list_2(t5,lf[101]);
t8=C_i_check_list_2(t6,lf[101]);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6968,a[2]=t10,a[3]=t4,tmp=(C_word)a,a+=4,tmp));
t12=((C_word*)t10)[1];
f_6968(t12,t2,t5,t6);}
else{
t4=((C_word*)((C_word*)t0)[2])[1];
f_6820(t4,((C_word*)t0)[3],C_u_i_cdr(((C_word*)t0)[4]),C_fixnum_difference(((C_word*)t0)[5],C_fix(1)));}}

/* k6879 in k6876 in k6873 in k6870 in k6867 in k6864 in k6861 in k6858 in k6855 in k6852 in k6849 in doloop1442 in k6809 in chicken.csi#show-frameinfo in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in ... */
static void C_ccall f_6881(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6881,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_6820(t2,((C_word*)t0)[3],C_u_i_cdr(((C_word*)t0)[4]),C_fixnum_difference(((C_word*)t0)[5],C_fix(1)));}

/* g1480 in k6876 in k6873 in k6870 in k6867 in k6864 in k6861 in k6858 in k6855 in k6852 in k6849 in doloop1442 in k6809 in chicken.csi#show-frameinfo in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in ... */
static void C_fcall f_6894(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_6894,4,t0,t1,t2,t3);}
a=C_alloc(6);
if(C_truep(C_i_nullp(t2))){
t4=C_SCHEME_UNDEFINED;
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6904,a[2]=((C_word*)t0)[2],a[3]=t3,a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* csi.scm:829: display */
t5=*((C_word*)lf[94]+1);{
C_word av2[3];
av2[0]=t5;
av2[1]=t4;
av2[2]=lf[252];
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}}

/* k6902 in g1480 in k6876 in k6873 in k6870 in k6867 in k6864 in k6861 in k6858 in k6855 in k6852 in k6849 in doloop1442 in k6809 in chicken.csi#show-frameinfo in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in ... */
static void C_ccall f_6904(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,4)))){
C_save_and_reclaim((void *)f_6904,c,av);}
a=C_alloc(7);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6909,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp));
t5=((C_word*)t3)[1];
f_6909(t5,((C_word*)t0)[4],C_fix(0),((C_word*)t0)[5]);}

/* doloop1493 in k6902 in g1480 in k6876 in k6873 in k6870 in k6867 in k6864 in k6861 in k6858 in k6855 in k6852 in k6849 in doloop1442 in k6809 in chicken.csi#show-frameinfo in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in ... */
static void C_fcall f_6909(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,4)))){
C_save_and_reclaim_args((void *)trf_6909,4,t0,t1,t2,t3);}
a=C_alloc(9);
if(C_truep(C_i_nullp(t3))){
t4=C_SCHEME_UNDEFINED;
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t4=*((C_word*)lf[84]+1);
t5=*((C_word*)lf[84]+1);
t6=C_i_check_port_2(*((C_word*)lf[84]+1),C_fix(2),C_SCHEME_TRUE,lf[85]);
t7=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_6922,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,a[5]=t3,a[6]=((C_word*)t0)[3],a[7]=((C_word*)t0)[4],a[8]=t4,tmp=(C_word)a,a+=9,tmp);
/* csi.scm:833: ##sys#print */
t8=*((C_word*)lf[87]+1);{
C_word av2[5];
av2[0]=t8;
av2[1]=t7;
av2[2]=lf[251];
av2[3]=C_SCHEME_FALSE;
av2[4]=*((C_word*)lf[84]+1);
((C_proc)(void*)(*((C_word*)t8+1)))(5,av2);}}}

/* k6920 in doloop1493 in k6902 in g1480 in k6876 in k6873 in k6870 in k6867 in k6864 in k6861 in k6858 in k6855 in k6852 in k6849 in doloop1442 in k6809 in chicken.csi#show-frameinfo in k5389 in k5060 in k4915 in k4271 in k3928 in ... */
static void C_ccall f_6922(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_6922,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_6925,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
/* csi.scm:833: ##sys#print */
t3=*((C_word*)lf[87]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_i_car(((C_word*)t0)[5]);
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[8];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k6923 in k6920 in doloop1493 in k6902 in g1480 in k6876 in k6873 in k6870 in k6867 in k6864 in k6861 in k6858 in k6855 in k6852 in k6849 in doloop1442 in k6809 in chicken.csi#show-frameinfo in k5389 in k5060 in k4915 in k4271 in ... */
static void C_ccall f_6925(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_6925,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6928,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
/* csi.scm:833: ##sys#print */
t3=*((C_word*)lf[87]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[250];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[8];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k6926 in k6923 in k6920 in doloop1493 in k6902 in g1480 in k6876 in k6873 in k6870 in k6867 in k6864 in k6861 in k6858 in k6855 in k6852 in k6849 in doloop1442 in k6809 in chicken.csi#show-frameinfo in k5389 in k5060 in k4915 in ... */
static void C_ccall f_6928(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_6928,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6931,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* csi.scm:834: prin1 */
f_6788(t2,C_slot(((C_word*)t0)[7],((C_word*)t0)[4]));}

/* k6929 in k6926 in k6923 in k6920 in doloop1493 in k6902 in g1480 in k6876 in k6873 in k6870 in k6867 in k6864 in k6861 in k6858 in k6855 in k6852 in k6849 in doloop1442 in k6809 in chicken.csi#show-frameinfo in k5389 in k5060 in ... */
static void C_ccall f_6931(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_6931,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6934,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* csi.scm:835: newline */
t3=*((C_word*)lf[86]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k6932 in k6929 in k6926 in k6923 in k6920 in doloop1493 in k6902 in g1480 in k6876 in k6873 in k6870 in k6867 in k6864 in k6861 in k6858 in k6855 in k6852 in k6849 in doloop1442 in k6809 in chicken.csi#show-frameinfo in k5389 in ... */
static void C_ccall f_6934(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6934,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_6909(t2,((C_word*)t0)[3],C_fixnum_plus(((C_word*)t0)[4],C_fix(1)),C_u_i_cdr(((C_word*)t0)[5]));}

/* for-each-loop1479 in k6876 in k6873 in k6870 in k6867 in k6864 in k6861 in k6858 in k6855 in k6852 in k6849 in doloop1442 in k6809 in chicken.csi#show-frameinfo in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in ... */
static void C_fcall f_6968(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_6968,4,t0,t1,t2,t3);}
a=C_alloc(6);
t4=C_i_pairp(t2);
t5=(C_truep(t4)?C_i_pairp(t3):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6978,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* csi.scm:826: g1480 */
t7=((C_word*)t0)[3];
f_6894(t7,t6,C_slot(t2,C_fix(0)),C_slot(t3,C_fix(0)));}
else{
t6=C_SCHEME_UNDEFINED;
t7=t1;{
C_word av2[2];
av2[0]=t7;
av2[1]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}}

/* k6976 in for-each-loop1479 in k6876 in k6873 in k6870 in k6867 in k6864 in k6861 in k6858 in k6855 in k6852 in k6849 in doloop1442 in k6809 in chicken.csi#show-frameinfo in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in ... */
static void C_ccall f_6978(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6978,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_6968(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)),C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k7010 in k6867 in k6864 in k6861 in k6858 in k6855 in k6852 in k6849 in doloop1442 in k6809 in chicken.csi#show-frameinfo in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in ... */
static void C_ccall f_7012(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_7012,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7015,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* csi.scm:822: ##sys#print */
t3=*((C_word*)lf[87]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k7013 in k7010 in k6867 in k6864 in k6861 in k6858 in k6855 in k6852 in k6849 in doloop1442 in k6809 in chicken.csi#show-frameinfo in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in ... */
static void C_ccall f_7015(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_7015,c,av);}
/* csi.scm:822: ##sys#print */
t2=*((C_word*)lf[87]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[253];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k7068 in k4751 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_fcall f_7070(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,2)))){
C_save_and_reclaim_args((void *)trf_7070,2,t0,t1);}
if(C_truep(t1)){
/* csi.scm:846: display */
t2=*((C_word*)lf[94]+1);{
C_word av2[3];
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[95];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}
else{
t2=C_i_length(C_fast_retrieve(lf[96]));
t3=C_fixnum_plus(((C_word*)t0)[3],C_fix(1));
t4=C_fixnum_difference(t2,t3);
t5=C_i_list_ref(C_fast_retrieve(lf[96]),t4);
t6=C_mutate(&lf[8] /* (set! chicken.csi#selected-frame ...) */,t5);
/* csi.scm:852: show-frameinfo */
f_6785(((C_word*)t0)[2],C_retrieve2(lf[8],C_text("chicken.csi#selected-frame")));}}

/* k7125 in k4764 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_fcall f_7127(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,3)))){
C_save_and_reclaim_args((void *)trf_7127,2,t0,t1);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7129,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
if(C_truep(t1)){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7160,a[2]=t2,a[3]=t1,a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
/* csi.scm:873: call/cc */
t4=*((C_word*)lf[103]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=((C_word*)t0)[3];
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=((C_word*)t0)[3];{
C_word av2[2];
av2[0]=t3;
av2[1]=*((C_word*)lf[29]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* compare in k7125 in k4764 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_fcall f_7129(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,4)))){
C_save_and_reclaim_args((void *)trf_7129,3,t0,t1,t2);}
a=C_alloc(4);
t3=C_slot(t2,C_fix(1));
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7140,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
t5=C_i_string_length(((C_word*)t0)[2]);
t6=C_i_string_length(t3);
/* csi.scm:871: scheme#substring */
t7=*((C_word*)lf[13]+1);{
C_word av2[5];
av2[0]=t7;
av2[1]=t4;
av2[2]=t3;
av2[3]=C_fix(0);
av2[4]=C_i_fixnum_min(t5,t6);
((C_proc)(void*)(*((C_word*)t7+1)))(5,av2);}}

/* k7138 in compare in k7125 in k4764 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 in ... */
static void C_ccall f_7140(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7140,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_i_string_equal_p(((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a7159 in k7125 in k4764 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_7160(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_7160,c,av);}
a=C_alloc(12);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7163,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7182,a[2]=t3,a[3]=t5,a[4]=t2,a[5]=((C_word*)t0)[2],a[6]=((C_word*)t0)[3],tmp=(C_word)a,a+=7,tmp));
t7=((C_word*)t5)[1];
f_7182(t7,t1,((C_word*)t0)[4]);}

/* fail in a7159 in k7125 in k4764 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 in ... */
static void C_fcall f_7163(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_7163,3,t0,t1,t2);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7167,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* csi.scm:876: display */
t4=*((C_word*)lf[94]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k7165 in fail in a7159 in k7125 in k4764 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in ... */
static void C_ccall f_7167(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_7167,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7170,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* csi.scm:877: newline */
t3=*((C_word*)lf[86]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k7168 in k7165 in fail in a7159 in k7125 in k4764 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in ... */
static void C_ccall f_7170(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7170,c,av);}
t2=*((C_word*)lf[29]+1);
/* csi.scm:878: return */
t3=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=*((C_word*)lf[29]+1);
((C_proc)C_fast_retrieve_proc(t3))(3,av2);}}

/* doloop1562 in a7159 in k7125 in k4764 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 in ... */
static void C_fcall f_7182(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(20,0,4)))){
C_save_and_reclaim_args((void *)trf_7182,3,t0,t1,t2);}
a=C_alloc(20);
if(C_truep(C_i_nullp(t2))){
/* csi.scm:880: fail */
t3=((C_word*)t0)[2];
f_7163(t3,t1,lf[98]);}
else{
t3=C_i_car(t2);
t4=C_eqp(C_retrieve2(lf[8],C_text("chicken.csi#selected-frame")),t3);
t5=C_slot(t3,C_fix(2));
t6=C_i_structurep(t5,lf[99]);
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7207,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t8=(C_truep(t4)?t6:C_SCHEME_FALSE);
if(C_truep(t8)){
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7216,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],tmp=(C_word)a,a+=4,tmp);
t10=C_slot(t5,C_fix(2));
t11=C_slot(t5,C_fix(3));
t12=C_i_check_list_2(t10,lf[101]);
t13=C_i_check_list_2(t11,lf[101]);
t14=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7292,a[2]=((C_word*)t0)[2],a[3]=t7,a[4]=((C_word*)t0)[6],tmp=(C_word)a,a+=5,tmp);
t15=C_SCHEME_UNDEFINED;
t16=(*a=C_VECTOR_TYPE|1,a[1]=t15,tmp=(C_word)a,a+=2,tmp);
t17=C_set_block_item(t16,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7301,a[2]=t16,a[3]=t9,tmp=(C_word)a,a+=4,tmp));
t18=((C_word*)t16)[1];
f_7301(t18,t14,t10,t11);}
else{
t19=t1;
t20=C_u_i_cdr(t2);
t1=t19;
t2=t20;
goto loop;}}}

/* k7205 in doloop1562 in a7159 in k7125 in k4764 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in ... */
static void C_ccall f_7207(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7207,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_7182(t2,((C_word*)t0)[3],C_u_i_cdr(((C_word*)t0)[4]));}

/* g1577 in doloop1562 in a7159 in k7125 in k4764 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in ... */
static void C_fcall f_7216(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,4)))){
C_save_and_reclaim_args((void *)trf_7216,4,t0,t1,t2,t3);}
a=C_alloc(8);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7222,a[2]=t5,a[3]=((C_word*)t0)[2],a[4]=t3,a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp));
t7=((C_word*)t5)[1];
f_7222(t7,t1,C_fix(0),t2);}

/* doloop1590 in g1577 in doloop1562 in a7159 in k7125 in k4764 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in ... */
static void C_fcall f_7222(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,0,2)))){
C_save_and_reclaim_args((void *)trf_7222,4,t0,t1,t2,t3);}
a=C_alloc(15);
if(C_truep(C_i_nullp(t3))){
t4=C_SCHEME_UNDEFINED;
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7232,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
t5=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_7244,a[2]=((C_word*)t0)[3],a[3]=t4,a[4]=((C_word*)t0)[4],a[5]=t2,a[6]=t3,a[7]=((C_word*)t0)[2],a[8]=t1,tmp=(C_word)a,a+=9,tmp);
/* csi.scm:892: compare */
t6=((C_word*)t0)[5];
f_7129(t6,t5,C_i_car(t3));}}

/* k7230 in doloop1590 in g1577 in doloop1562 in a7159 in k7125 in k4764 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in ... */
static void C_ccall f_7232(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7232,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_7222(t2,((C_word*)t0)[3],C_fixnum_plus(((C_word*)t0)[4],C_fix(1)),C_u_i_cdr(((C_word*)t0)[5]));}

/* k7242 in doloop1590 in g1577 in doloop1562 in a7159 in k7125 in k4764 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in ... */
static void C_ccall f_7244(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_7244,c,av);}
a=C_alloc(7);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7247,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* csi.scm:893: display */
t3=*((C_word*)lf[94]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[100];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t2=((C_word*)((C_word*)t0)[7])[1];
f_7222(t2,((C_word*)t0)[8],C_fixnum_plus(((C_word*)t0)[5],C_fix(1)),C_u_i_cdr(((C_word*)t0)[6]));}}

/* k7245 in k7242 in doloop1590 in g1577 in doloop1562 in a7159 in k7125 in k4764 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in ... */
static void C_ccall f_7247(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_7247,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7250,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* csi.scm:894: display */
t3=*((C_word*)lf[94]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_u_i_car(((C_word*)t0)[6]);
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7248 in k7245 in k7242 in doloop1590 in g1577 in doloop1562 in a7159 in k7125 in k4764 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in ... */
static void C_ccall f_7250(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_7250,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7253,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* csi.scm:895: newline */
t3=*((C_word*)lf[86]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k7251 in k7248 in k7245 in k7242 in doloop1590 in g1577 in doloop1562 in a7159 in k7125 in k4764 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in ... */
static void C_ccall f_7253(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_7253,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7256,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t3=C_slot(((C_word*)t0)[4],((C_word*)t0)[5]);
t4=C_a_i_list1(&a,1,t3);
/* csi.scm:896: history-add */
t5=C_retrieve2(lf[28],C_text("chicken.csi#history-add"));
f_4107(t5,t2,t4);}

/* k7254 in k7251 in k7248 in k7245 in k7242 in doloop1590 in g1577 in doloop1562 in a7159 in k7125 in k4764 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in ... */
static void C_ccall f_7256(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7256,c,av);}
/* csi.scm:897: return */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=C_slot(((C_word*)t0)[4],((C_word*)t0)[5]);
((C_proc)C_fast_retrieve_proc(t2))(3,av2);}}

/* k7290 in doloop1562 in a7159 in k7125 in k4764 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in ... */
static void C_ccall f_7292(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_7292,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7299,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* csi.scm:900: ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[15]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[15]+1);
av2[1]=t2;
av2[2]=lf[102];
av2[3]=((C_word*)t0)[4];
tp(4,av2);}}

/* k7297 in k7290 in doloop1562 in a7159 in k7125 in k4764 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in ... */
static void C_ccall f_7299(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7299,c,av);}
/* csi.scm:900: fail */
t2=((C_word*)t0)[2];
f_7163(t2,((C_word*)t0)[3],t1);}

/* for-each-loop1576 in doloop1562 in a7159 in k7125 in k4764 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in ... */
static void C_fcall f_7301(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_7301,4,t0,t1,t2,t3);}
a=C_alloc(6);
t4=C_i_pairp(t2);
t5=(C_truep(t4)?C_i_pairp(t3):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7311,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* csi.scm:887: g1577 */
t7=((C_word*)t0)[3];
f_7216(t7,t6,C_slot(t2,C_fix(0)),C_slot(t3,C_fix(0)));}
else{
t6=C_SCHEME_UNDEFINED;
t7=t1;{
C_word av2[2];
av2[0]=t7;
av2[1]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}}

/* k7309 in for-each-loop1576 in doloop1562 in a7159 in k7125 in k4764 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in ... */
static void C_ccall f_7311(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7311,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_7301(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)),C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k7352 in k4764 in chicken.csi#csi-eval in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_7354(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7354,c,av);}
t2=((C_word*)t0)[2];
f_7127(t2,C_SCHEME_FALSE);}

/* chicken.csi#member* in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_fcall f_7359(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_7359,3,t1,t2,t3);}
a=C_alloc(6);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7365,a[2]=t5,a[3]=t2,tmp=(C_word)a,a+=4,tmp));
t7=((C_word*)t5)[1];
f_7365(t7,t1,t3);}

/* loop in chicken.csi#member* in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 in ... */
static void C_fcall f_7365(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,3)))){
C_save_and_reclaim_args((void *)trf_7365,3,t0,t1,t2);}
a=C_alloc(7);
if(C_truep(C_i_pairp(t2))){
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7377,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=t4,tmp=(C_word)a,a+=5,tmp));
t6=((C_word*)t4)[1];
f_7377(t6,t1,((C_word*)t0)[3]);}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* find in loop in chicken.csi#member* in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in ... */
static void C_fcall f_7377(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(0,0,2)))){
C_save_and_reclaim_args((void *)trf_7377,3,t0,t1,t2);}
if(C_truep(C_i_nullp(t2))){
/* csi.scm:924: loop */
t3=((C_word*)((C_word*)t0)[2])[1];
f_7365(t3,t1,C_i_cdr(((C_word*)t0)[3]));}
else{
t3=C_i_car(t2);
t4=C_i_car(((C_word*)t0)[3]);
if(C_truep(C_i_equalp(t3,t4))){
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
/* csi.scm:926: find */
t6=t1;
t7=C_u_i_cdr(t2);
t1=t6;
t2=t7;
goto loop;}}}

/* chicken.csi#canonicalize-args in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_fcall f_7414(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,3)))){
C_save_and_reclaim_args((void *)trf_7414,2,t1,t2);}
a=C_alloc(5);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7420,a[2]=t4,tmp=(C_word)a,a+=3,tmp));
t6=((C_word*)t4)[1];
f_7420(t6,t1,t2);}

/* loop in chicken.csi#canonicalize-args in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 in ... */
static void C_fcall f_7420(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_7420,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_nullp(t2))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_i_car(t2);
if(C_truep((C_truep(C_i_equalp(t3,lf[261]))?C_SCHEME_TRUE:(C_truep(C_i_equalp(t3,lf[262]))?C_SCHEME_TRUE:(C_truep(C_i_equalp(t3,lf[263]))?C_SCHEME_TRUE:(C_truep(C_i_equalp(t3,lf[264]))?C_SCHEME_TRUE:(C_truep(C_i_equalp(t3,lf[265]))?C_SCHEME_TRUE:C_SCHEME_FALSE))))))){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7442,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
t5=C_block_size(t3);
if(C_truep(C_fixnum_greaterp(t5,C_fix(2)))){
t6=C_i_char_equalp(C_make_character(45),C_subchar(t3,C_fix(0)));
t7=t4;
f_7442(t7,(C_truep(t6)?C_i_not(C_i_member(t3,lf[268])):C_SCHEME_FALSE));}
else{
t6=t4;
f_7442(t6,C_SCHEME_FALSE);}}}}

/* k7440 in loop in chicken.csi#canonicalize-args in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in ... */
static void C_fcall f_7442(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,3)))){
C_save_and_reclaim_args((void *)trf_7442,2,t0,t1);}
a=C_alloc(9);
if(C_truep(t1)){
if(C_truep(C_i_char_equalp(C_make_character(58),C_subchar(((C_word*)t0)[2],C_fix(1))))){
/* csi.scm:948: loop */
t2=((C_word*)((C_word*)t0)[3])[1];
f_7420(t2,((C_word*)t0)[4],C_u_i_cdr(((C_word*)t0)[5]));}
else{
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7456,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[2],tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7524,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:949: scheme#substring */
t4=*((C_word*)lf[13]+1);{
C_word av2[4];
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[2];
av2[3]=C_fix(1);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}}
else{
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7532,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* csi.scm:953: loop */
t3=((C_word*)((C_word*)t0)[3])[1];
f_7420(t3,t2,C_u_i_cdr(((C_word*)t0)[5]));}}

/* k7454 in k7440 in loop in chicken.csi#canonicalize-args in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in ... */
static void C_ccall f_7456(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(19,c,3)))){
C_save_and_reclaim((void *)f_7456,c,av);}
a=C_alloc(19);
t2=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7563,tmp=(C_word)a,a+=2,tmp);
t3=(
  f_7563(t1)
);
if(C_truep(t3)){
t4=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t5=t4;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=((C_word*)t6)[1];
t8=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7477,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7485,a[2]=t6,a[3]=t10,a[4]=t7,tmp=(C_word)a,a+=5,tmp));
t12=((C_word*)t10)[1];
f_7485(t12,t8,t1);}
else{
/* csi.scm:952: ##sys#error */
t4=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[266];
av2[3]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}}

/* k7475 in k7454 in k7440 in loop in chicken.csi#canonicalize-args in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in ... */
static void C_ccall f_7477(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_7477,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7481,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* csi.scm:951: loop */
t3=((C_word*)((C_word*)t0)[3])[1];
f_7420(t3,t2,C_u_i_cdr(((C_word*)t0)[4]));}

/* k7479 in k7475 in k7454 in k7440 in loop in chicken.csi#canonicalize-args in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in ... */
static void C_ccall f_7481(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7481,c,av);}
/* csi.scm:951: scheme#append */
t2=*((C_word*)lf[229]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* map-loop1687 in k7454 in k7440 in loop in chicken.csi#canonicalize-args in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in ... */
static void C_fcall f_7485(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_7485,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_a_i_string(&a,2,C_make_character(45),t3);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k7522 in k7440 in loop in chicken.csi#canonicalize-args in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in ... */
static void C_ccall f_7524(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7524,c,av);}
/* ##sys#string->list */
t2=C_fast_retrieve(lf[267]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k7530 in k7440 in loop in chicken.csi#canonicalize-args in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in ... */
static void C_ccall f_7532(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_7532,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* loop in k7454 in k7440 in loop in chicken.csi#canonicalize-args in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in ... */
static C_word C_fcall f_7563(C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_stack_overflow_check;
loop:{}
t2=C_i_nullp(t1);
if(C_truep(t2)){
return(t2);}
else{
t3=C_i_car(t1);
if(C_truep((C_truep(C_eqp(t3,C_make_character(107)))?C_SCHEME_TRUE:(C_truep(C_eqp(t3,C_make_character(115)))?C_SCHEME_TRUE:(C_truep(C_eqp(t3,C_make_character(118)))?C_SCHEME_TRUE:(C_truep(C_eqp(t3,C_make_character(104)))?C_SCHEME_TRUE:(C_truep(C_eqp(t3,C_make_character(68)))?C_SCHEME_TRUE:(C_truep(C_eqp(t3,C_make_character(101)))?C_SCHEME_TRUE:(C_truep(C_eqp(t3,C_make_character(105)))?C_SCHEME_TRUE:(C_truep(C_eqp(t3,C_make_character(82)))?C_SCHEME_TRUE:(C_truep(C_eqp(t3,C_make_character(98)))?C_SCHEME_TRUE:(C_truep(C_eqp(t3,C_make_character(110)))?C_SCHEME_TRUE:(C_truep(C_eqp(t3,C_make_character(113)))?C_SCHEME_TRUE:(C_truep(C_eqp(t3,C_make_character(119)))?C_SCHEME_TRUE:(C_truep(C_eqp(t3,C_make_character(45)))?C_SCHEME_TRUE:(C_truep(C_eqp(t3,C_make_character(73)))?C_SCHEME_TRUE:(C_truep(C_eqp(t3,C_make_character(112)))?C_SCHEME_TRUE:(C_truep(C_eqp(t3,C_make_character(80)))?C_SCHEME_TRUE:C_SCHEME_FALSE)))))))))))))))))){
t5=C_u_i_cdr(t1);
t1=t5;
goto loop;}
else{
return(C_SCHEME_FALSE);}}}

/* k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_7593(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_7593,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7596,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8657,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:974: chicken.process-context#command-line-arguments */
t4=C_fast_retrieve(lf[299]);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 in ... */
static void C_ccall f_7596(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_7596,c,av);}
a=C_alloc(7);
t2=t1;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7599,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* csi.scm:976: member* */
f_7359(t4,lf[369],((C_word*)t3)[1]);}

/* k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in ... */
static void C_ccall f_7599(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_7599,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7602,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* csi.scm:977: member* */
f_7359(t2,lf[368],((C_word*)((C_word*)t0)[2])[1]);}

/* k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in ... */
static void C_ccall f_7602(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,2)))){
C_save_and_reclaim((void *)f_7602,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7605,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(t1)){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8557,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t4=C_i_cdr(t1);
t5=C_i_pairp(t4);
t6=C_i_not(t5);
if(C_truep(t6)){
if(C_truep(t6)){
/* csi.scm:982: ##sys#error */
t7=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t7;
av2[1]=t3;
av2[2]=lf[366];
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}
else{
t7=t3;{
C_word *av2=av;
av2[0]=t7;
av2[1]=C_SCHEME_UNDEFINED;
f_8557(2,av2);}}}
else{
t7=C_i_cadr(t1);
t8=C_i_string_length(t7);
t9=C_eqp(t8,C_fix(0));
if(C_truep(t9)){
if(C_truep(t9)){
/* csi.scm:982: ##sys#error */
t10=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t10;
av2[1]=t3;
av2[2]=lf[366];
((C_proc)(void*)(*((C_word*)t10+1)))(3,av2);}}
else{
t10=t3;{
C_word *av2=av;
av2[0]=t10;
av2[1]=C_SCHEME_UNDEFINED;
f_8557(2,av2);}}}
else{
t10=C_u_i_cdr(t1);
t11=C_i_string_ref(C_u_i_car(t10),C_fix(0));
if(C_truep(C_u_i_char_equalp(C_make_character(45),t11))){
/* csi.scm:982: ##sys#error */
t12=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t12;
av2[1]=t3;
av2[2]=lf[366];
((C_proc)(void*)(*((C_word*)t12+1)))(3,av2);}}
else{
t12=t3;{
C_word *av2=av;
av2[0]=t12;
av2[1]=C_SCHEME_UNDEFINED;
f_8557(2,av2);}}}}}
else{
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8640,a[2]=((C_word*)t0)[2],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8653,a[2]=t3,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* csi.scm:992: canonicalize-args */
f_7414(t4,((C_word*)t0)[5]);}}

/* k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in ... */
static void C_fcall f_7605(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_7605,2,t0,t1);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7608,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* csi.scm:995: member* */
f_7359(t2,lf[363],((C_word*)((C_word*)t0)[3])[1]);}

/* k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in ... */
static void C_ccall f_7608(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,3)))){
C_save_and_reclaim((void *)f_7608,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7611,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
if(C_truep(((C_word*)t0)[2])){
t3=t2;
f_7611(t3,((C_word*)t0)[2]);}
else{
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8551,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* csi.scm:996: member* */
f_7359(t3,lf[362],((C_word*)((C_word*)t0)[3])[1]);}}

/* k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in ... */
static void C_fcall f_7611(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,3)))){
C_save_and_reclaim_args((void *)trf_7611,2,t0,t1);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_7614,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* csi.scm:997: member* */
f_7359(t2,lf[361],((C_word*)((C_word*)t0)[4])[1]);}

/* k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in ... */
static void C_ccall f_7614(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(19,c,2)))){
C_save_and_reclaim((void *)f_7614,c,av);}
a=C_alloc(19);
t2=(C_truep(((C_word*)t0)[2])?((C_word*)t0)[2]:(C_truep(t1)?t1:((C_word*)t0)[3]));
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=C_retrieve2(lf[12],C_text("chicken.csi#chop-separator"));
t8=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_7623,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[3],a[7]=t2,a[8]=((C_word*)t0)[7],a[9]=t5,a[10]=t6,tmp=(C_word)a,a+=11,tmp);
t9=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8542,a[2]=t8,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:1001: chicken.process-context#get-environment-variable */
t10=C_fast_retrieve(lf[23]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t10;
av2[1]=t9;
av2[2]=lf[360];
((C_proc)(void*)(*((C_word*)t10+1)))(3,av2);}}

/* k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in ... */
static void C_ccall f_7623(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,3)))){
C_save_and_reclaim((void *)f_7623,c,av);}
a=C_alloc(16);
t2=C_i_check_list_2(t1,lf[138]);
t3=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_7629,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8506,a[2]=((C_word*)t0)[9],a[3]=t5,a[4]=((C_word*)t0)[10],tmp=(C_word)a,a+=5,tmp));
t7=((C_word*)t5)[1];
f_8506(t7,t3,t1);}

/* k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in ... */
static void C_ccall f_7629(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,4)))){
C_save_and_reclaim((void *)f_7629,c,av);}
a=C_alloc(24);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7631,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp));
t7=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7743,tmp=(C_word)a,a+=2,tmp));
t8=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_7800,a[2]=((C_word*)t0)[3],a[3]=t5,a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[2],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=t1,a[11]=t3,tmp=(C_word)a,a+=12,tmp);
t9=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8498,a[2]=t8,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:1026: member* */
f_7359(t9,lf[358],((C_word*)((C_word*)t0)[2])[1]);}

/* collect-options in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in ... */
static void C_fcall f_7631(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_7631,3,t0,t1,t2);}
a=C_alloc(6);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7637,a[2]=t2,a[3]=t4,tmp=(C_word)a,a+=4,tmp));
t6=((C_word*)t4)[1];
f_7637(t6,t1,((C_word*)((C_word*)t0)[2])[1]);}

/* loop in collect-options in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in ... */
static void C_fcall f_7637(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,3)))){
C_save_and_reclaim_args((void *)trf_7637,3,t0,t1,t2);}
a=C_alloc(4);
t3=C_i_member(((C_word*)t0)[2],t2);
if(C_truep(t3)){
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7645,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* csi.scm:1004: g1861 */
t5=t4;
f_7645(t5,t1,t3);}
else{
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* g1861 in loop in collect-options in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in ... */
static void C_fcall f_7645(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,3)))){
C_save_and_reclaim_args((void *)trf_7645,3,t0,t1,t2);}
a=C_alloc(4);
t3=C_i_cdr(t2);
if(C_truep(C_i_nullp(t3))){
/* csi.scm:1007: ##sys#error */
t4=*((C_word*)lf[31]+1);{
C_word av2[4];
av2[0]=t4;
av2[1]=t1;
av2[2]=lf[270];
av2[3]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t4=C_i_cadr(t2);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7666,a[2]=t1,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
t6=C_u_i_cdr(t2);
/* csi.scm:1008: loop */
t7=((C_word*)((C_word*)t0)[3])[1];
f_7637(t7,t5,C_u_i_cdr(t6));}}

/* k7664 in g1861 in loop in collect-options in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in ... */
static void C_ccall f_7666(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_7666,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k7680 in k8150 in k7865 in k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in ... */
static void C_ccall f_7682(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_7682,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7685,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(t1)){
t3=C_a_i_list2(&a,2,t1,lf[311]);
/* csi.scm:1012: chicken.pathname#make-pathname */
t4=C_fast_retrieve(lf[308]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=t3;
av2[3]=C_retrieve2(lf[1],C_text("chicken.csi#constant680"));
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
f_7685(2,av2);}}}

/* k7683 in k7680 in k8150 in k7865 in k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in ... */
static void C_ccall f_7685(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_7685,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7688,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* csi.scm:1014: chicken.process-context#get-environment-variable */
t3=C_fast_retrieve(lf[23]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[310];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7686 in k7683 in k7680 in k8150 in k7865 in k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in ... */
static void C_ccall f_7688(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_7688,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7691,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
if(C_truep(t1)){
t3=C_i_string_equal_p(t1,lf[307]);
if(C_truep(C_i_not(t3))){
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7728,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t5=C_retrieve2(lf[1],C_text("chicken.csi#constant680"));
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[15]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[15]+1);
av2[1]=t4;
av2[2]=lf[309];
av2[3]=C_retrieve2(lf[1],C_text("chicken.csi#constant680"));
tp(4,av2);}}
else{
t4=t2;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
f_7691(2,av2);}}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
f_7691(2,av2);}}}

/* k7689 in k7686 in k7683 in k7680 in k8150 in k7865 in k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in ... */
static void C_ccall f_7691(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_7691,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7697,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
if(C_truep(((C_word*)t0)[3])){
/* csi.scm:1017: chicken.file#file-exists? */
t3=C_fast_retrieve(lf[14]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
f_7697(2,av2);}}}

/* k7695 in k7689 in k7686 in k7683 in k7680 in k8150 in k7865 in k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in ... */
static void C_ccall f_7697(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_7697,c,av);}
a=C_alloc(4);
if(C_truep(t1)){
/* csi.scm:1018: scheme#load */
t2=*((C_word*)lf[65]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7706,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
if(C_truep(((C_word*)t0)[4])){
/* csi.scm:1019: chicken.file#file-exists? */
t3=C_fast_retrieve(lf[14]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_7870(2,av2);}}}}

/* k7704 in k7695 in k7689 in k7686 in k7683 in k7680 in k8150 in k7865 in k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in ... */
static void C_ccall f_7706(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7706,c,av);}
if(C_truep(t1)){
/* csi.scm:1020: scheme#load */
t2=*((C_word*)lf[65]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
f_7870(2,av2);}}}

/* k7726 in k7686 in k7683 in k7680 in k8150 in k7865 in k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in ... */
static void C_ccall f_7728(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7728,c,av);}
/* csi.scm:1016: chicken.pathname#make-pathname */
t2=C_fast_retrieve(lf[308]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* evalstring in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in ... */
static void C_fcall f_7743(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_7743,3,t1,t2,t3);}
a=C_alloc(6);
t4=C_i_nullp(t3);
t5=(C_truep(t4)?(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7790,tmp=(C_word)a,a+=2,tmp):C_i_car(t3));
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7750,a[2]=t5,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* csi.scm:1022: chicken.base#open-input-string */
t7=C_fast_retrieve(lf[272]);{
C_word av2[3];
av2[0]=t7;
av2[1]=t6;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}

/* k7748 in evalstring in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in ... */
static void C_ccall f_7750(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_7750,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7757,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* csi.scm:1023: scheme#read */
t3=*((C_word*)lf[52]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7755 in k7748 in evalstring in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in ... */
static void C_ccall f_7757(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_7757,c,av);}
a=C_alloc(7);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7759,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp));
t5=((C_word*)t3)[1];
f_7759(t5,((C_word*)t0)[4],t1);}

/* doloop1891 in k7755 in k7748 in evalstring in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in ... */
static void C_fcall f_7759(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,3)))){
C_save_and_reclaim_args((void *)trf_7759,3,t0,t1,t2);}
a=C_alloc(12);
if(C_truep(C_eofp(t2))){
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7769,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7780,a[2]=((C_word*)t0)[4],a[3]=t3,tmp=(C_word)a,a+=4,tmp);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7782,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:1025: ##sys#call-with-values */{
C_word av2[4];
av2[0]=0;
av2[1]=t4;
av2[2]=t5;
av2[3]=*((C_word*)lf[271]+1);
C_call_with_values(4,av2);}}}

/* k7767 in doloop1891 in k7755 in k7748 in evalstring in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in ... */
static void C_ccall f_7769(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_7769,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7776,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* csi.scm:1023: scheme#read */
t3=*((C_word*)lf[52]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7774 in k7767 in doloop1891 in k7755 in k7748 in evalstring in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in ... */
static void C_ccall f_7776(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7776,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_7759(t2,((C_word*)t0)[3],t1);}

/* k7778 in doloop1891 in k7755 in k7748 in evalstring in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in ... */
static void C_ccall f_7780(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7780,c,av);}
/* csi.scm:1025: rec */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
((C_proc)C_fast_retrieve_proc(t2))(3,av2);}}

/* a7781 in doloop1891 in k7755 in k7748 in evalstring in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in ... */
static void C_ccall f_7782(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7782,c,av);}
/* csi.scm:1025: scheme#eval */
t2=*((C_word*)lf[54]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* f_7790 in evalstring in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in ... */
static void C_ccall f_7790(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7790,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=*((C_word*)lf[29]+1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in ... */
static void C_ccall f_7800(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,2)))){
C_save_and_reclaim((void *)f_7800,c,av);}
a=C_alloc(18);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_7803,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],tmp=(C_word)a,a+=12,tmp);
if(C_truep(C_i_member(lf[352],((C_word*)((C_word*)t0)[6])[1]))){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8492,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f9503,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:144: chicken.platform#chicken-version */
t5=C_fast_retrieve(lf[317]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_7803(2,av2);}}}

/* k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in ... */
static void C_ccall f_7803(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_7803,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_7806,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],tmp=(C_word)a,a+=12,tmp);
if(C_truep(C_i_member(lf[350],((C_word*)((C_word*)t0)[6])[1]))){
t3=C_set_block_item(lf[351] /* ##sys#setup-mode */,0,C_SCHEME_TRUE);
t4=t2;
f_7806(t4,t3);}
else{
t3=t2;
f_7806(t3,C_SCHEME_UNDEFINED);}}

/* k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in ... */
static void C_fcall f_7806(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,0,2)))){
C_save_and_reclaim_args((void *)trf_7806,2,t0,t1);}
a=C_alloc(18);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_7809,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],tmp=(C_word)a,a+=12,tmp);
if(C_truep(C_i_member(lf[349],((C_word*)((C_word*)t0)[6])[1]))){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8475,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8482,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:1035: chicken.platform#chicken-version */
t5=C_fast_retrieve(lf[317]);{
C_word av2[2];
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t3=t2;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_7809(2,av2);}}}

/* k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in ... */
static void C_ccall f_7809(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,3)))){
C_save_and_reclaim((void *)f_7809,c,av);}
a=C_alloc(16);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_7812,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],tmp=(C_word)a,a+=12,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8462,a[2]=t2,a[3]=((C_word*)t0)[8],tmp=(C_word)a,a+=4,tmp);
/* csi.scm:1037: member* */
f_7359(t3,lf[348],((C_word*)((C_word*)t0)[6])[1]);}

/* k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in ... */
static void C_fcall f_7812(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,0,3)))){
C_save_and_reclaim_args((void *)trf_7812,2,t0,t1);}
a=C_alloc(16);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_7815,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],tmp=(C_word)a,a+=12,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8447,a[2]=t2,a[3]=((C_word*)t0)[8],tmp=(C_word)a,a+=4,tmp);
/* csi.scm:1040: member* */
f_7359(t3,lf[345],((C_word*)((C_word*)t0)[6])[1]);}

/* k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in ... */
static void C_ccall f_7815(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_7815,c,av);}
a=C_alloc(13);
t2=C_fast_retrieve(lf[273]);
t3=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_7818,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=t2,tmp=(C_word)a,a+=13,tmp);
/* csi.scm:1044: collect-options */
t4=((C_word*)((C_word*)t0)[11])[1];
f_7631(t4,t3,lf[342]);}

/* k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in ... */
static void C_ccall f_7818(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,3)))){
C_save_and_reclaim((void *)f_7818,c,av);}
a=C_alloc(18);
t2=C_i_check_list_2(t1,lf[101]);
t3=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_7824,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],tmp=(C_word)a,a+=12,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8423,a[2]=t5,a[3]=((C_word*)t0)[12],tmp=(C_word)a,a+=4,tmp));
t7=((C_word*)t5)[1];
f_8423(t7,t3,t1);}

/* k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in ... */
static void C_ccall f_7824(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_7824,c,av);}
a=C_alloc(13);
t2=C_fast_retrieve(lf[273]);
t3=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_7827,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=t2,tmp=(C_word)a,a+=13,tmp);
/* csi.scm:1045: collect-options */
t4=((C_word*)((C_word*)t0)[11])[1];
f_7631(t4,t3,lf[341]);}

/* k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in ... */
static void C_ccall f_7827(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,3)))){
C_save_and_reclaim((void *)f_7827,c,av);}
a=C_alloc(18);
t2=C_i_check_list_2(t1,lf[101]);
t3=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_7833,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],tmp=(C_word)a,a+=12,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8400,a[2]=t5,a[3]=((C_word*)t0)[12],tmp=(C_word)a,a+=4,tmp));
t7=((C_word*)t5)[1];
f_8400(t7,t3,t1);}

/* k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in ... */
static void C_ccall f_7833(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_7833,c,av);}
a=C_alloc(13);
t2=C_fast_retrieve(lf[274]);
t3=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_7836,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=t2,tmp=(C_word)a,a+=13,tmp);
/* csi.scm:1046: collect-options */
t4=((C_word*)((C_word*)t0)[11])[1];
f_7631(t4,t3,lf[340]);}

/* k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in ... */
static void C_ccall f_7836(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,3)))){
C_save_and_reclaim((void *)f_7836,c,av);}
a=C_alloc(18);
t2=C_i_check_list_2(t1,lf[101]);
t3=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_7842,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],tmp=(C_word)a,a+=12,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8377,a[2]=t5,a[3]=((C_word*)t0)[12],tmp=(C_word)a,a+=4,tmp));
t7=((C_word*)t5)[1];
f_8377(t7,t3,t1);}

/* k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in ... */
static void C_ccall f_7842(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(25,c,2)))){
C_save_and_reclaim((void *)f_7842,c,av);}
a=C_alloc(25);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_7846,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],tmp=(C_word)a,a+=10,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8281,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t5=t4;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=((C_word*)t6)[1];
t8=C_retrieve2(lf[12],C_text("chicken.csi#chop-separator"));
t9=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_8288,a[2]=t3,a[3]=((C_word*)t0)[10],a[4]=((C_word*)t0)[11],a[5]=t6,a[6]=t7,tmp=(C_word)a,a+=7,tmp);
/* csi.scm:1049: collect-options */
t10=((C_word*)((C_word*)t0)[11])[1];
f_7631(t10,t9,lf[339]);}

/* k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in ... */
static void C_ccall f_7846(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_7846,c,av);}
a=C_alloc(9);
t2=C_mutate((C_word*)lf[127]+1 /* (set! ##sys#include-pathnames ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_7849,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
if(C_truep(((C_word*)t0)[9])){
t4=C_i_cdr(((C_word*)t0)[9]);
t5=C_i_pairp(t4);
if(C_truep(C_i_not(t5))){
/* csi.scm:1056: ##sys#error */
t6=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t3;
av2[2]=lf[332];
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}
else{
t6=C_i_cadr(((C_word*)t0)[9]);
if(C_truep(C_i_string_equal_p(lf[333],t6))){
/* csi.scm:1058: chicken.base#keyword-style */
t7=C_fast_retrieve(lf[129]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t7;
av2[1]=t3;
av2[2]=lf[334];
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}
else{
t7=C_u_i_cdr(((C_word*)t0)[9]);
if(C_truep(C_i_string_equal_p(lf[335],C_u_i_car(t7)))){
/* csi.scm:1060: chicken.base#keyword-style */
t8=C_fast_retrieve(lf[129]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t8;
av2[1]=t3;
av2[2]=lf[324];
((C_proc)(void*)(*((C_word*)t8+1)))(3,av2);}}
else{
t8=C_u_i_cdr(((C_word*)t0)[9]);
if(C_truep(C_i_string_equal_p(lf[336],C_u_i_car(t8)))){
/* csi.scm:1062: chicken.base#keyword-style */
t9=C_fast_retrieve(lf[129]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t9;
av2[1]=t3;
av2[2]=lf[337];
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}
else{
t9=t3;{
C_word *av2=av;
av2[0]=t9;
av2[1]=C_SCHEME_UNDEFINED;
f_7849(2,av2);}}}}}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_7849(2,av2);}}}

/* k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in ... */
static void C_ccall f_7849(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,3)))){
C_save_and_reclaim((void *)f_7849,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_7852,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8214,a[2]=t2,a[3]=((C_word*)t0)[8],tmp=(C_word)a,a+=4,tmp);
/* csi.scm:1063: member* */
f_7359(t3,lf[331],((C_word*)((C_word*)t0)[6])[1]);}

/* k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in ... */
static void C_ccall f_7852(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,3)))){
C_save_and_reclaim((void *)f_7852,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_7855,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8202,a[2]=t2,a[3]=((C_word*)t0)[8],tmp=(C_word)a,a+=4,tmp);
/* csi.scm:1066: member* */
f_7359(t3,lf[329],((C_word*)((C_word*)t0)[6])[1]);}

/* k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in ... */
static void C_ccall f_7855(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,3)))){
C_save_and_reclaim((void *)f_7855,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_7858,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8181,a[2]=t2,a[3]=((C_word*)t0)[8],tmp=(C_word)a,a+=4,tmp);
/* csi.scm:1069: member* */
f_7359(t3,lf[327],((C_word*)((C_word*)t0)[6])[1]);}

/* k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in ... */
static void C_ccall f_7858(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_7858,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_7861,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
t3=C_a_i_cons(&a,2,lf[320],C_fast_retrieve(lf[321]));
/* csi.scm:1078: scheme#eval */
t4=*((C_word*)lf[54]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in ... */
static void C_ccall f_7861(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_7861,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_7864,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
t3=C_a_i_cons(&a,2,lf[289],C_fast_retrieve(lf[319]));
/* csi.scm:1079: scheme#eval */
t4=*((C_word*)lf[54]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in ... */
static void C_ccall f_7864(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_7864,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_7867,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
if(C_truep(((C_word*)t0)[8])){
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_7867(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8164,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:1081: chicken.load#load-verbose */
t4=C_fast_retrieve(lf[318]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}}

/* k7865 in k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in ... */
static void C_ccall f_7867(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_7867,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7870,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8152,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[7],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* csi.scm:1084: member* */
f_7359(t3,lf[313],((C_word*)((C_word*)t0)[6])[1]);}

/* k7868 in k7865 in k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in ... */
static void C_ccall f_7870(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_7870,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7873,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
if(C_truep(((C_word*)t0)[2])){
t3=C_set_block_item(lf[6] /* ##sys#notices-enabled */,0,C_SCHEME_FALSE);
t4=t2;
f_7873(t4,t3);}
else{
t3=t2;
f_7873(t3,C_SCHEME_UNDEFINED);}}

/* k7871 in k7868 in k7865 in k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in ... */
static void C_fcall f_7873(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,3)))){
C_save_and_reclaim_args((void *)trf_7873,2,t0,t1);}
a=C_alloc(8);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7878,a[2]=((C_word*)t0)[2],a[3]=t3,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp));
t5=((C_word*)t3)[1];
f_7878(t5,((C_word*)t0)[5],((C_word*)((C_word*)t0)[6])[1]);}

/* doloop1851 in k7871 in k7868 in k7865 in k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in ... */
static void C_fcall f_7878(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(17,0,4)))){
C_save_and_reclaim_args((void *)trf_7878,3,t0,t1,t2);}
a=C_alloc(17);
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
if(C_truep(C_i_nullp(((C_word*)t3)[1]))){
if(C_truep(((C_word*)t0)[2])){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=C_fast_retrieve(lf[275]);
t5=C_mutate((C_word*)lf[275]+1 /* (set! ##sys#user-read-hook ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4233,a[2]=t4,tmp=(C_word)a,a+=3,tmp));
t6=C_mutate((C_word*)lf[277]+1 /* (set! ##sys#sharp-number-hook ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_4262,tmp=(C_word)a,a+=2,tmp));
t7=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f9432,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:1092: chicken.repl#repl */
t8=C_fast_retrieve(lf[278]);{
C_word av2[3];
av2[0]=t8;
av2[1]=t7;
av2[2]=C_retrieve2(lf[47],C_text("chicken.csi#csi-eval"));
((C_proc)(void*)(*((C_word*)t8+1)))(3,av2);}}}
else{
t4=C_i_car(((C_word*)t3)[1]);
t5=C_i_member(t4,lf[279]);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7906,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
if(C_truep(t5)){
t19=t1;
t20=C_i_cdr(((C_word*)t3)[1]);
t1=t19;
t2=t20;
goto loop;}
else{
if(C_truep((C_truep(C_i_equalp(t4,lf[280]))?C_SCHEME_TRUE:(C_truep(C_i_equalp(t4,lf[281]))?C_SCHEME_TRUE:(C_truep(C_i_equalp(t4,lf[282]))?C_SCHEME_TRUE:(C_truep(C_i_equalp(t4,lf[283]))?C_SCHEME_TRUE:(C_truep(C_i_equalp(t4,lf[284]))?C_SCHEME_TRUE:(C_truep(C_i_equalp(t4,lf[285]))?C_SCHEME_TRUE:(C_truep(C_i_equalp(t4,lf[286]))?C_SCHEME_TRUE:C_SCHEME_FALSE))))))))){
t7=C_i_cdr(((C_word*)t3)[1]);
t8=C_set_block_item(t3,0,t7);
t19=t1;
t20=C_i_cdr(((C_word*)t3)[1]);
t1=t19;
t2=t20;
goto loop;}
else{
t7=C_i_string_equal_p(lf[287],t4);
t8=(C_truep(t7)?t7:C_u_i_string_equal_p(lf[288],t4));
if(C_truep(t8)){
t9=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7935,a[2]=t3,a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t10=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7947,a[2]=t9,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:1099: scheme#string->symbol */
t11=*((C_word*)lf[290]+1);{
C_word av2[3];
av2[0]=t11;
av2[1]=t10;
av2[2]=C_i_cadr(((C_word*)t3)[1]);
((C_proc)(void*)(*((C_word*)t11+1)))(3,av2);}}
else{
t9=C_u_i_string_equal_p(lf[291],t4);
t10=(C_truep(t9)?t9:C_u_i_string_equal_p(lf[292],t4));
if(C_truep(t10)){
t11=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7962,a[2]=t3,a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* csi.scm:1102: evalstring */
f_7743(t11,C_i_cadr(((C_word*)t3)[1]),C_SCHEME_END_OF_LIST);}
else{
t11=C_u_i_string_equal_p(lf[293],t4);
t12=(C_truep(t11)?t11:C_u_i_string_equal_p(lf[294],t4));
if(C_truep(t12)){
t13=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7981,a[2]=t3,a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t14=C_i_cadr(((C_word*)t3)[1]);
t15=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7991,tmp=(C_word)a,a+=2,tmp);
/* csi.scm:1105: evalstring */
f_7743(t13,t14,C_a_i_list(&a,1,t15));}
else{
t13=C_u_i_string_equal_p(lf[296],t4);
t14=(C_truep(t13)?t13:C_u_i_string_equal_p(lf[297],t4));
if(C_truep(t14)){
t15=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8006,a[2]=t3,a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t16=C_i_cadr(((C_word*)t3)[1]);
t17=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_8016,tmp=(C_word)a,a+=2,tmp);
/* csi.scm:1108: evalstring */
f_7743(t15,t16,C_a_i_list(&a,1,t17));}
else{
t15=(C_truep(((C_word*)t0)[5])?C_i_car(((C_word*)t0)[5]):C_SCHEME_FALSE);
t16=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_8026,a[2]=t15,a[3]=t6,a[4]=((C_word*)t0)[3],a[5]=t1,a[6]=t3,tmp=(C_word)a,a+=7,tmp);
if(C_truep(C_i_equalp(lf[301],t15))){
t17=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_8078,tmp=(C_word)a,a+=2,tmp);
/* csi.scm:1112: scheme#load */
t18=*((C_word*)lf[65]+1);{
C_word av2[4];
av2[0]=t18;
av2[1]=t16;
av2[2]=t4;
av2[3]=t17;
((C_proc)(void*)(*((C_word*)t18+1)))(4,av2);}}
else{
/* csi.scm:1112: scheme#load */
t17=*((C_word*)lf[65]+1);{
C_word av2[4];
av2[0]=t17;
av2[1]=t16;
av2[2]=t4;
av2[3]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t17+1)))(4,av2);}}}}}}}}}}

/* k7904 in doloop1851 in k7871 in k7868 in k7865 in k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in ... */
static void C_ccall f_7906(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7906,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_7878(t2,((C_word*)t0)[3],C_i_cdr(((C_word*)((C_word*)t0)[4])[1]));}

/* k7933 in doloop1851 in k7871 in k7868 in k7865 in k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in ... */
static void C_ccall f_7935(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7935,c,av);}
t2=C_i_cdr(((C_word*)((C_word*)t0)[2])[1]);
t3=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t4=((C_word*)((C_word*)t0)[3])[1];
f_7878(t4,((C_word*)t0)[4],C_i_cdr(((C_word*)((C_word*)t0)[2])[1]));}

/* k7945 in doloop1851 in k7871 in k7868 in k7865 in k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in ... */
static void C_ccall f_7947(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_7947,c,av);}
a=C_alloc(6);
t2=C_a_i_list(&a,2,lf[289],t1);
/* csi.scm:1099: scheme#eval */
t3=*((C_word*)lf[54]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7960 in doloop1851 in k7871 in k7868 in k7865 in k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in ... */
static void C_ccall f_7962(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7962,c,av);}
t2=C_i_cdr(((C_word*)((C_word*)t0)[2])[1]);
t3=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t4=((C_word*)((C_word*)t0)[3])[1];
f_7878(t4,((C_word*)t0)[4],C_i_cdr(((C_word*)((C_word*)t0)[2])[1]));}

/* k7979 in doloop1851 in k7871 in k7868 in k7865 in k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in ... */
static void C_ccall f_7981(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7981,c,av);}
t2=C_i_cdr(((C_word*)((C_word*)t0)[2])[1]);
t3=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t4=((C_word*)((C_word*)t0)[3])[1];
f_7878(t4,((C_word*)t0)[4],C_i_cdr(((C_word*)((C_word*)t0)[2])[1]));}

/* a7990 in doloop1851 in k7871 in k7868 in k7865 in k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in ... */
static void C_ccall f_7991(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +0,c,4)))){
C_save_and_reclaim((void*)f_7991,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+0);
t2=C_build_rest(&a,c,2,av);
C_word t3;{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=t1;
av2[2]=*((C_word*)lf[295]+1);
av2[3]=*((C_word*)lf[107]+1);
av2[4]=t2;
C_apply(5,av2);}}

/* k8004 in doloop1851 in k7871 in k7868 in k7865 in k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in ... */
static void C_ccall f_8006(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8006,c,av);}
t2=C_i_cdr(((C_word*)((C_word*)t0)[2])[1]);
t3=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t4=((C_word*)((C_word*)t0)[3])[1];
f_7878(t4,((C_word*)t0)[4],C_i_cdr(((C_word*)((C_word*)t0)[2])[1]));}

/* a8015 in doloop1851 in k7871 in k7868 in k7865 in k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in ... */
static void C_ccall f_8016(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +0,c,4)))){
C_save_and_reclaim((void*)f_8016,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+0);
t2=C_build_rest(&a,c,2,av);
C_word t3;{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=t1;
av2[2]=*((C_word*)lf[295]+1);
av2[3]=C_fast_retrieve(lf[45]);
av2[4]=t2;
C_apply(5,av2);}}

/* k8024 in doloop1851 in k7871 in k7868 in k7865 in k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in ... */
static void C_ccall f_8026(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_8026,c,av);}
a=C_alloc(4);
if(C_truep(C_i_equalp(lf[298],((C_word*)t0)[2]))){
t2=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_8037,tmp=(C_word)a,a+=2,tmp);
t3=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_8050,tmp=(C_word)a,a+=2,tmp);
/* csi.scm:1129: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[3];
av2[2]=t2;
av2[3]=t3;
C_call_with_values(4,av2);}}
else{
t2=((C_word*)((C_word*)t0)[4])[1];
f_7878(t2,((C_word*)t0)[5],C_i_cdr(((C_word*)((C_word*)t0)[6])[1]));}}

/* a8036 in k8024 in doloop1851 in k7871 in k7868 in k7865 in k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in ... */
static void C_ccall f_8037(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_8037,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8041,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:1129: scheme#eval */
t3=*((C_word*)lf[54]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[300];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k8039 in a8036 in k8024 in doloop1851 in k7871 in k7868 in k7865 in k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in ... */
static void C_ccall f_8041(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_8041,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8048,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* csi.scm:1129: chicken.process-context#command-line-arguments */
t3=C_fast_retrieve(lf[299]);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k8046 in k8039 in a8036 in k8024 in doloop1851 in k7871 in k7868 in k7865 in k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in ... */
static void C_ccall f_8048(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8048,c,av);}
/* csi.scm:1129: g2064 */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
((C_proc)C_fast_retrieve_proc(t2))(3,av2);}}

/* a8049 in k8024 in doloop1851 in k7871 in k7868 in k7865 in k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in ... */
static void C_ccall f_8050(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8050,c,av);}
t2=C_rest_nullp(c,2);
t3=(C_truep(t2)?C_SCHEME_FALSE:C_get_rest_arg(c,2,av,2,t0));
if(C_truep(C_fixnump(t3))){
/* csi.scm:1131: chicken.base#exit */
t4=C_fast_retrieve(lf[48]);{
C_word av2[3];
av2[0]=t4;
av2[1]=t1;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
/* csi.scm:1131: chicken.base#exit */
t4=C_fast_retrieve(lf[48]);{
C_word av2[3];
av2[0]=t4;
av2[1]=t1;
av2[2]=C_fix(0);
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}}

/* f_8078 in doloop1851 in k7871 in k7868 in k7865 in k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in ... */
static void C_ccall f_8078(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_8078,c,av);}
a=C_alloc(7);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8082,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8133,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:1116: chicken.port#with-output-to-string */
t5=C_fast_retrieve(lf[306]);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k8080 */
static void C_ccall f_8082(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_8082,c,av);}
a=C_alloc(6);
t2=C_block_size(t1);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8088,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t2,a[5]=t1,tmp=(C_word)a,a+=6,tmp);
/* csi.scm:1118: chicken.base#flush-output */
t4=*((C_word*)lf[305]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=*((C_word*)lf[84]+1);
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k8086 in k8080 */
static void C_ccall f_8088(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_8088,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8091,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* csi.scm:1119: scheme#display */
t3=*((C_word*)lf[94]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[304];
av2[3]=*((C_word*)lf[302]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k8089 in k8086 in k8080 */
static void C_ccall f_8091(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,3)))){
C_save_and_reclaim((void *)f_8091,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8094,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8102,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=t4,tmp=(C_word)a,a+=5,tmp));
t6=((C_word*)t4)[1];
f_8102(t6,t2,C_fix(0));}

/* k8092 in k8089 in k8086 in k8080 */
static void C_ccall f_8094(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_8094,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8097,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* csi.scm:1126: scheme#newline */
t3=*((C_word*)lf[86]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=*((C_word*)lf[302]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k8095 in k8092 in k8089 in k8086 in k8080 */
static void C_ccall f_8097(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8097,c,av);}
/* csi.scm:1127: scheme#eval */
t2=*((C_word*)lf[54]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* doloop2053 in k8089 in k8086 in k8080 */
static void C_fcall f_8102(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_8102,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_fixnum_greater_or_equal_p(t2,((C_word*)t0)[2]))){
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=C_i_string_ref(((C_word*)t0)[3],t2);
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8115,a[2]=((C_word*)t0)[4],a[3]=t1,a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* ##sys#write-char/port */
t5=C_fast_retrieve(lf[248]);{
C_word av2[4];
av2[0]=t5;
av2[1]=t4;
av2[2]=t3;
av2[3]=*((C_word*)lf[302]+1);
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}}

/* k8113 in doloop2053 in k8089 in k8086 in k8080 */
static void C_ccall f_8115(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_8115,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8118,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_u_i_char_equalp(C_make_character(10),((C_word*)t0)[5]))){
/* csi.scm:1125: scheme#display */
t3=*((C_word*)lf[94]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[303];
av2[3]=*((C_word*)lf[302]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}
else{
t3=((C_word*)((C_word*)t0)[2])[1];
f_8102(t3,((C_word*)t0)[3],C_fixnum_plus(((C_word*)t0)[4],C_fix(1)));}}

/* k8116 in k8113 in doloop2053 in k8089 in k8086 in k8080 */
static void C_ccall f_8118(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8118,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_8102(t2,((C_word*)t0)[3],C_fixnum_plus(((C_word*)t0)[4],C_fix(1)));}

/* a8132 */
static void C_ccall f_8133(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8133,c,av);}
t2=C_fast_retrieve(lf[45]);
/* csi.scm:1116: g2050 */
t3=C_fast_retrieve(lf[45]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k8150 in k7865 in k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in ... */
static void C_ccall f_8152(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_8152,c,av);}
a=C_alloc(3);
t2=(C_truep(t1)?t1:(C_truep(((C_word*)t0)[2])?((C_word*)t0)[2]:((C_word*)t0)[3]));
if(C_truep(t2)){
t3=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_7870(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7682,a[2]=((C_word*)t0)[4],tmp=(C_word)a,a+=3,tmp);
/* csi.scm:1011: chicken.platform#system-config-directory */
t4=C_fast_retrieve(lf[312]);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k8162 in k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in ... */
static void C_ccall f_8164(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_8164,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8167,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f9473,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:144: chicken.platform#chicken-version */
t4=C_fast_retrieve(lf[317]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k8165 in k8162 in k7862 in k7859 in k7856 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in ... */
static void C_ccall f_8167(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8167,c,av);}
/* csi.scm:1083: chicken.base#print */
t2=*((C_word*)lf[107]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[314];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k8179 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in ... */
static void C_ccall f_8181(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_8181,c,av);}
a=C_alloc(3);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8184,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(((C_word*)t0)[3])){
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_8184(2,av2);}}
else{
/* csi.scm:1070: scheme#display */
t3=*((C_word*)lf[94]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[326];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
f_7858(2,av2);}}}

/* k8182 in k8179 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in ... */
static void C_ccall f_8184(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_8184,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8187,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csi.scm:1071: chicken.base#case-sensitive */
t3=C_fast_retrieve(lf[325]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k8185 in k8182 in k8179 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in ... */
static void C_ccall f_8187(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_8187,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8190,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csi.scm:1072: chicken.base#keyword-style */
t3=C_fast_retrieve(lf[129]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[324];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k8188 in k8185 in k8182 in k8179 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in ... */
static void C_ccall f_8190(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_8190,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8193,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csi.scm:1073: chicken.base#parentheses-synonyms */
t3=C_fast_retrieve(lf[323]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k8191 in k8188 in k8185 in k8182 in k8179 in k7853 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in ... */
static void C_ccall f_8193(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8193,c,av);}
/* csi.scm:1074: chicken.base#symbol-escape */
t2=C_fast_retrieve(lf[322]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k8200 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in ... */
static void C_ccall f_8202(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_8202,c,av);}
a=C_alloc(3);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8205,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(((C_word*)t0)[3])){
/* csi.scm:1068: chicken.base#symbol-escape */
t3=C_fast_retrieve(lf[322]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
/* csi.scm:1067: scheme#display */
t3=*((C_word*)lf[94]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[328];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
f_7855(2,av2);}}}

/* k8203 in k8200 in k7850 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in ... */
static void C_ccall f_8205(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8205,c,av);}
/* csi.scm:1068: chicken.base#symbol-escape */
t2=C_fast_retrieve(lf[322]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k8212 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in ... */
static void C_ccall f_8214(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_8214,c,av);}
a=C_alloc(3);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8217,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(((C_word*)t0)[3])){
/* csi.scm:1065: chicken.base#parentheses-synonyms */
t3=C_fast_retrieve(lf[323]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
/* csi.scm:1064: scheme#display */
t3=*((C_word*)lf[94]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[330];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
f_7852(2,av2);}}}

/* k8215 in k8212 in k7847 in k7844 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in ... */
static void C_ccall f_8217(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8217,c,av);}
/* csi.scm:1065: chicken.base#parentheses-synonyms */
t2=C_fast_retrieve(lf[323]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k8279 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in ... */
static void C_ccall f_8281(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_8281,c,av);}
a=C_alloc(5);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3123,a[2]=t3,tmp=(C_word)a,a+=3,tmp));
t5=((C_word*)t3)[1];
f_3123(t5,((C_word*)t0)[2],t1);}

/* k8286 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in ... */
static void C_ccall f_8288(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_8288,c,av);}
a=C_alloc(12);
t2=C_i_check_list_2(t1,lf[138]);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8294,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8343,a[2]=((C_word*)t0)[5],a[3]=t5,a[4]=((C_word*)t0)[6],tmp=(C_word)a,a+=5,tmp));
t7=((C_word*)t5)[1];
f_8343(t7,t3,t1);}

/* k8292 in k8286 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in ... */
static void C_ccall f_8294(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_8294,c,av);}
a=C_alloc(12);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=C_retrieve2(lf[12],C_text("chicken.csi#chop-separator"));
t7=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_8301,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=t4,a[6]=t5,tmp=(C_word)a,a+=7,tmp);
/* csi.scm:1050: collect-options */
t8=((C_word*)((C_word*)t0)[4])[1];
f_7631(t8,t7,lf[338]);}

/* k8299 in k8292 in k8286 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in ... */
static void C_ccall f_8301(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_8301,c,av);}
a=C_alloc(12);
t2=C_i_check_list_2(t1,lf[138]);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8307,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8309,a[2]=((C_word*)t0)[5],a[3]=t5,a[4]=((C_word*)t0)[6],tmp=(C_word)a,a+=5,tmp));
t7=((C_word*)t5)[1];
f_8309(t7,t3,t1);}

/* k8305 in k8299 in k8292 in k8286 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in ... */
static void C_ccall f_8307(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_8307,c,av);}
/* csi.scm:1049: scheme#append */
t2=*((C_word*)lf[229]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
av2[4]=C_fast_retrieve(lf[127]);
av2[5]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* map-loop1951 in k8299 in k8292 in k8286 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in ... */
static void C_fcall f_8309(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_8309,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8334,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* csi.scm:1050: g1957 */
t4=C_retrieve2(lf[12],C_text("chicken.csi#chop-separator"));{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
f_3903(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k8332 in map-loop1951 in k8299 in k8292 in k8286 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in ... */
static void C_ccall f_8334(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_8334,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_8309(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* map-loop1925 in k8286 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in ... */
static void C_fcall f_8343(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_8343,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8368,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* csi.scm:1049: g1931 */
t4=C_retrieve2(lf[12],C_text("chicken.csi#chop-separator"));{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
f_3903(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k8366 in map-loop1925 in k8286 in k7840 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in ... */
static void C_ccall f_8368(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_8368,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_8343(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* for-each-loop1843 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in ... */
static void C_fcall f_8377(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_8377,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8387,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* csi.scm:1046: g1844 */
t4=((C_word*)t0)[3];{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k8385 in for-each-loop1843 in k7834 in k7831 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in ... */
static void C_ccall f_8387(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8387,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_8377(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* for-each-loop1833 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in ... */
static void C_fcall f_8400(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_8400,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8410,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* csi.scm:1045: g1834 */
t4=((C_word*)t0)[3];{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k8408 in for-each-loop1833 in k7825 in k7822 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in ... */
static void C_ccall f_8410(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8410,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_8400(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* for-each-loop1823 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in ... */
static void C_fcall f_8423(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_8423,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8433,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* csi.scm:1044: g1824 */
t4=((C_word*)t0)[3];{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k8431 in for-each-loop1823 in k7816 in k7813 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in ... */
static void C_ccall f_8433(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8433,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_8423(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k8445 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in ... */
static void C_ccall f_8447(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_8447,c,av);}
a=C_alloc(6);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8450,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(((C_word*)t0)[3])){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f9499,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csi.scm:1042: chicken.platform#register-feature! */
t4=C_fast_retrieve(lf[273]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[343];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
/* csi.scm:1041: scheme#display */
t3=*((C_word*)lf[94]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[344];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
f_7815(2,av2);}}}

/* k8448 in k8445 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in ... */
static void C_ccall f_8450(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_8450,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8453,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csi.scm:1042: chicken.platform#register-feature! */
t3=C_fast_retrieve(lf[273]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[343];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k8451 in k8448 in k8445 in k7810 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in ... */
static void C_ccall f_8453(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8453,c,av);}
/* csi.scm:1043: chicken.base#case-sensitive */
t2=C_fast_retrieve(lf[325]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k8460 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in ... */
static void C_ccall f_8462(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_8462,c,av);}
a=C_alloc(3);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8465,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(((C_word*)t0)[3])){
t3=C_set_block_item(lf[346] /* ##sys#warnings-enabled */,0,C_SCHEME_FALSE);
t4=((C_word*)t0)[2];
f_7812(t4,t3);}
else{
/* csi.scm:1038: scheme#display */
t3=*((C_word*)lf[94]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[347];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}
else{
t2=((C_word*)t0)[2];
f_7812(t2,C_SCHEME_UNDEFINED);}}

/* k8463 in k8460 in k7807 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in ... */
static void C_ccall f_8465(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_8465,c,av);}
t2=C_set_block_item(lf[346] /* ##sys#warnings-enabled */,0,C_SCHEME_FALSE);
t3=((C_word*)t0)[2];
f_7812(t3,t2);}

/* k8473 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in ... */
static void C_ccall f_8475(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8475,c,av);}
/* csi.scm:1036: chicken.base#exit */
t2=C_fast_retrieve(lf[48]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_fix(0);
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k8480 in k7804 in k7801 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in ... */
static void C_ccall f_8482(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8482,c,av);}
/* csi.scm:1035: chicken.base#print */
t2=*((C_word*)lf[107]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k8490 in k7798 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in ... */
static void C_ccall f_8492(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8492,c,av);}
/* csi.scm:1031: chicken.base#exit */
t2=C_fast_retrieve(lf[48]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_fix(0);
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k8496 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in ... */
static void C_ccall f_8498(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_8498,c,av);}
a=C_alloc(6);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8501,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3854,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:95: scheme#display */
t4=*((C_word*)lf[94]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[357];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
f_7800(2,av2);}}}

/* k8499 in k8496 in k7627 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in ... */
static void C_ccall f_8501(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8501,c,av);}
/* csi.scm:1028: chicken.base#exit */
t2=C_fast_retrieve(lf[48]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_fix(0);
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* map-loop1791 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in ... */
static void C_fcall f_8506(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_8506,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8531,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* csi.scm:999: g1797 */
t4=C_retrieve2(lf[12],C_text("chicken.csi#chop-separator"));{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
f_3903(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k8529 in map-loop1791 in k7621 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in ... */
static void C_ccall f_8531(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_8531,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_8506(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k8540 in k7612 in k7609 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in ... */
static void C_ccall f_8542(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8542,c,av);}
if(C_truep(t1)){
/* csi.scm:1000: ##sys#split-path */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[21]);
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[21]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
tp(3,av2);}}
else{
/* csi.scm:1000: ##sys#split-path */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[21]);
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[21]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=lf[359];
tp(3,av2);}}}

/* k8549 in k7606 in k7603 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in ... */
static void C_ccall f_8551(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_8551,c,av);}
t2=((C_word*)t0)[2];
f_7611(t2,(C_truep(t1)?t1:((C_word*)t0)[3]));}

/* k8555 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in ... */
static void C_ccall f_8557(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_8557,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8560,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* csi.scm:983: chicken.process-context#program-name */
t3=C_fast_retrieve(lf[365]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_i_cadr(((C_word*)t0)[2]);
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k8558 in k8555 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in ... */
static void C_ccall f_8560(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_8560,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8563,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=C_u_i_cdr(((C_word*)t0)[2]);
/* csi.scm:984: chicken.process-context#command-line-arguments */
t4=C_fast_retrieve(lf[299]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=C_u_i_cdr(t3);
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k8561 in k8558 in k8555 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in ... */
static void C_ccall f_8563(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_8563,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8566,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* csi.scm:986: chicken.platform#register-feature! */
t3=C_fast_retrieve(lf[273]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[364];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k8564 in k8561 in k8558 in k8555 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in ... */
static void C_ccall f_8566(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_8566,c,av);}
a=C_alloc(4);
t2=C_i_set_i_slot(C_u_i_cdr(((C_word*)t0)[2]),C_fix(1),C_SCHEME_END_OF_LIST);
if(C_truep(*((C_word*)lf[11]+1))){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8575,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* csi.scm:989: lookup-script-file */
t4=C_retrieve2(lf[17],C_text("chicken.csi#lookup-script-file"));
f_4000(t4,t3,C_i_cadr(((C_word*)t0)[2]));}
else{
t3=C_SCHEME_UNDEFINED;
t4=((C_word*)t0)[3];
f_7605(t4,t3);}}

/* k8573 in k8564 in k8561 in k8558 in k8555 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in ... */
static void C_ccall f_8575(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_8575,c,av);}
t2=((C_word*)t0)[2];
f_7605(t2,(C_truep(t1)?C_i_set_car(C_u_i_cdr(((C_word*)t0)[3]),t1):C_SCHEME_FALSE));}

/* k8638 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in ... */
static void C_ccall f_8640(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_8640,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=C_i_member(lf[367],((C_word*)((C_word*)t0)[2])[1]);
t4=((C_word*)t0)[3];
f_7605(t4,(C_truep(t3)?C_i_set_cdr(t3,C_SCHEME_END_OF_LIST):C_SCHEME_FALSE));}

/* k8651 in k7600 in k7597 in k7594 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in ... */
static void C_ccall f_8653(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_8653,c,av);}
/* csi.scm:992: scheme#append */
t2=*((C_word*)lf[229]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)((C_word*)t0)[3])[1];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k8655 in k7591 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 in ... */
static void C_ccall f_8657(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8657,c,av);}
/* csi.scm:974: canonicalize-args */
f_7414(((C_word*)t0)[2],t1);}

/* k8659 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_8661(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_8661,c,av);}
a=C_alloc(4);
t2=(C_truep(t1)?t1:lf[370]);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4923,a[2]=((C_word*)t0)[2],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* csi.scm:435: chicken.base#open-input-string */
t4=C_fast_retrieve(lf[272]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k8665 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_8667(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_8667,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8673,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* chicken.base#implicit-exit-handler */
t3=C_fast_retrieve(lf[269]);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k8671 in k8665 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 in ... */
static void C_ccall f_8673(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_8673,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a8674 in k5389 in k5060 in k4915 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_8675(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_8675,c,av);}
/* csi.scm:915: ##sys#user-interrupt-hook */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[257]);
C_word *av2=av;
av2[0]=*((C_word*)lf[257]+1);
av2[1]=t1;
tp(2,av2);}}

/* a8680 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_8681(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_8681,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8685,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:419: scheme#read */
t3=*((C_word*)lf[52]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k8683 in a8680 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_8685(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_8685,c,av);}
a=C_alloc(7);
if(C_truep(C_i_not(t1))){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8694,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csi.scm:421: ##sys#switch-module */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[381]);
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[381]+1);
av2[1]=t2;
av2[2]=C_SCHEME_FALSE;
tp(3,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8700,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8721,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:423: ##sys#resolve-module-name */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[385]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[385]+1);
av2[1]=t3;
av2[2]=t1;
av2[3]=C_SCHEME_FALSE;
tp(4,av2);}}}

/* k8692 in k8683 in a8680 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_8694(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8694,c,av);}
/* csi.scm:422: printf */
t2=*((C_word*)lf[77]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[380];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k8698 in k8683 in a8680 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_8700(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_8700,c,av);}
a=C_alloc(3);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8704,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csi.scm:420: g989 */
t3=t2;
f_8704(t3,((C_word*)t0)[3],t1);}
else{
/* csi.scm:428: printf */
t2=*((C_word*)lf[77]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[383];
av2[3]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}}

/* g989 in k8698 in k8683 in a8680 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_fcall f_8704(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_8704,3,t0,t1,t2);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8708,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* csi.scm:425: ##sys#switch-module */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[381]);
C_word av2[3];
av2[0]=*((C_word*)lf[381]+1);
av2[1]=t3;
av2[2]=t2;
tp(3,av2);}}

/* k8706 in g989 in k8698 in k8683 in a8680 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 in ... */
static void C_ccall f_8708(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_8708,c,av);}
/* csi.scm:426: printf */
t2=*((C_word*)lf[77]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[382];
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k8719 in k8683 in a8680 in k4271 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_8721(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_8721,c,av);}
/* csi.scm:423: ##sys#find-module */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[384]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[384]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_SCHEME_FALSE;
tp(4,av2);}}

/* a8722 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_8723(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_8723,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8731,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:250: ##sys#current-module */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[392]);
C_word *av2=av;
av2[0]=*((C_word*)lf[392]+1);
av2[1]=t2;
tp(2,av2);}}

/* k8729 in a8722 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_8731(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_8731,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8734,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(t1)){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8741,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* csi.scm:252: ##sys#module-name */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[390]);
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[390]+1);
av2[1]=t3;
av2[2]=t1;
tp(3,av2);}}
else{
/* csi.scm:249: sprintf */
t3=*((C_word*)lf[194]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[388];
av2[3]=lf[391];
av2[4]=C_retrieve2(lf[26],C_text("chicken.csi#history-count"));
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}}

/* k8732 in k8729 in a8722 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_8734(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_8734,c,av);}
/* csi.scm:249: sprintf */
t2=*((C_word*)lf[194]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[388];
av2[3]=t1;
av2[4]=C_retrieve2(lf[26],C_text("chicken.csi#history-count"));
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k8739 in k8729 in a8722 in k3928 in k3846 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_8741(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_8741,c,av);}
/* csi.scm:252: sprintf */
t2=*((C_word*)lf[194]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[389];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k8743 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_8745(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_8745,c,av);}
a=C_alloc(3);
if(C_truep(t1)){
t2=((C_word*)t0)[2];
f_3848(t2,t1);}
else{
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8754,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csi.scm:87: chicken.process-context#get-environment-variable */
t3=C_fast_retrieve(lf[23]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[397];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}

/* k8752 in k8743 in k3843 in k3838 in k2548 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_8754(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_8754,c,av);}
t2=((C_word*)t0)[2];
f_3848(t2,(C_truep(t1)?lf[395]:lf[396]));}

/* a8755 in k2545 in k2542 in k2539 in k2536 in k2533 in k2530 in k2527 in k2524 in k2521 in k2518 in k2515 */
static void C_ccall f_8756(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_8756,c,av);}
/* csi.scm:43: ##sys#register-compiled-module */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[401]);
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=*((C_word*)lf[401]+1);
av2[1]=t1;
av2[2]=lf[402];
av2[3]=lf[402];
av2[4]=C_SCHEME_END_OF_LIST;
av2[5]=lf[403];
av2[6]=C_SCHEME_END_OF_LIST;
av2[7]=C_SCHEME_END_OF_LIST;
tp(8,av2);}}

/* toplevel */
static C_TLS int toplevel_initialized=0;
C_main_entry_point

void C_ccall C_toplevel(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(toplevel_initialized) {C_kontinue(t1,C_SCHEME_UNDEFINED);}
else C_toplevel_entry(C_text("toplevel"));
C_check_nursery_minimum(C_calculate_demand(3,c,2));
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void*)C_toplevel,c,av);}
toplevel_initialized=1;
if(C_unlikely(!C_demand_2(1952))){
C_save(t1);
C_rereclaim2(1952*sizeof(C_word),1);
t1=C_restore;}
a=C_alloc(3);
C_initialize_lf(lf,405);
lf[0]=C_h_intern(&lf[0],12, C_text("chicken.csi#"));
lf[2]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005csirc"));
lf[3]=C_h_intern(&lf[3],29, C_text("##sys#repl-print-length-limit"));
lf[4]=C_h_intern_kw(&lf[4],3, C_text("csi"));
lf[5]=C_h_intern(&lf[5],14, C_text("##sys#features"));
lf[6]=C_h_intern(&lf[6],21, C_text("##sys#notices-enabled"));
lf[7]=C_h_intern(&lf[7],26, C_text("chicken.csi#editor-command"));
lf[11]=C_h_intern(&lf[11],22, C_text("##sys#windows-platform"));
lf[13]=C_h_intern(&lf[13],16, C_text("scheme#substring"));
lf[14]=C_h_intern(&lf[14],25, C_text("chicken.file#file-exists\077"));
lf[15]=C_h_intern(&lf[15],19, C_text("##sys#string-append"));
lf[16]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004.bat"));
lf[18]=C_h_intern(&lf[18],20, C_text("scheme#string-append"));
lf[19]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001/"));
lf[20]=C_h_intern(&lf[20],27, C_text("##sys#peek-nonnull-c-string"));
lf[21]=C_h_intern(&lf[21],16, C_text("##sys#split-path"));
lf[22]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001/"));
lf[23]=C_h_intern(&lf[23],48, C_text("chicken.process-context#get-environment-variable"));
lf[24]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004PATH"));
lf[27]=C_h_intern(&lf[27],26, C_text("chicken.base#vector-resize"));
lf[29]=C_h_intern(&lf[29],21, C_text("##sys#undefined-value"));
lf[31]=C_h_intern(&lf[31],11, C_text("##sys#error"));
lf[32]=C_decode_literal(C_heaptop,C_text("\376B\000\000 history entry index out of range"));
lf[33]=C_h_intern(&lf[33],21, C_text("scheme#inexact->exact"));
lf[34]=C_h_intern(&lf[34],20, C_text("##sys#break-on-error"));
lf[35]=C_h_intern(&lf[35],22, C_text("##sys#read-prompt-hook"));
lf[36]=C_h_intern(&lf[36],15, C_text("##sys#tty-port\077"));
lf[37]=C_h_intern(&lf[37],20, C_text("##sys#standard-input"));
lf[39]=C_h_intern(&lf[39],28, C_text("chicken.csi#toplevel-command"));
lf[40]=C_h_intern(&lf[40],16, C_text("toplevel-command"));
lf[41]=C_h_intern(&lf[41],25, C_text("chicken.load#load-noisily"));
lf[42]=C_h_intern(&lf[42],20, C_text("chicken.io#read-line"));
lf[43]=C_h_intern(&lf[43],27, C_text("chicken.string#string-split"));
lf[44]=C_h_intern(&lf[44],21, C_text("chicken.syntax#expand"));
lf[45]=C_h_intern(&lf[45],33, C_text("chicken.pretty-print#pretty-print"));
lf[46]=C_h_intern(&lf[46],13, C_text("scheme#values"));
lf[48]=C_h_intern(&lf[48],17, C_text("chicken.base#exit"));
lf[49]=C_h_intern(&lf[49],7, C_text("unquote"));
lf[50]=C_h_intern(&lf[50],1, C_text("x"));
lf[51]=C_h_intern(&lf[51],27, C_text("chicken.syntax#strip-syntax"));
lf[52]=C_h_intern(&lf[52],11, C_text("scheme#read"));
lf[53]=C_h_intern(&lf[53],1, C_text("p"));
lf[54]=C_h_intern(&lf[54],11, C_text("scheme#eval"));
lf[55]=C_h_intern(&lf[55],1, C_text("d"));
lf[57]=C_h_intern(&lf[57],2, C_text("du"));
lf[59]=C_h_intern(&lf[59],3, C_text("dur"));
lf[60]=C_h_intern(&lf[60],1, C_text("r"));
lf[62]=C_h_intern(&lf[62],1, C_text("q"));
lf[63]=C_h_intern(&lf[63],17, C_text("chicken.repl#quit"));
lf[64]=C_h_intern(&lf[64],1, C_text("l"));
lf[65]=C_h_intern(&lf[65],11, C_text("scheme#load"));
lf[66]=C_h_intern(&lf[66],2, C_text("ln"));
lf[67]=C_h_intern(&lf[67],19, C_text("chicken.base#print\052"));
lf[68]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004==> "));
lf[69]=C_h_intern_kw(&lf[69],7, C_text("printer"));
lf[70]=C_h_intern(&lf[70],1, C_text("t"));
lf[71]=C_h_intern(&lf[71],19, C_text("##sys#display-times"));
lf[72]=C_h_intern(&lf[72],16, C_text("##sys#stop-timer"));
lf[73]=C_h_intern(&lf[73],17, C_text("##sys#start-timer"));
lf[74]=C_h_intern(&lf[74],3, C_text("exn"));
lf[75]=C_h_intern(&lf[75],20, C_text("##sys#last-exception"));
lf[76]=C_h_intern(&lf[76],1, C_text("e"));
lf[77]=C_h_intern(&lf[77],21, C_text("chicken.format#printf"));
lf[78]=C_decode_literal(C_heaptop,C_text("\376B\000\000,editor returned with non-zero exit status ~a"));
lf[79]=C_h_intern(&lf[79],22, C_text("chicken.process#system"));
lf[80]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001 "));
lf[81]=C_h_intern(&lf[81],2, C_text("ch"));
lf[82]=C_h_intern(&lf[82],19, C_text("scheme#vector-fill!"));
lf[83]=C_h_intern(&lf[83],1, C_text("h"));
lf[84]=C_h_intern(&lf[84],21, C_text("##sys#standard-output"));
lf[85]=C_h_intern(&lf[85],6, C_text("printf"));
lf[86]=C_h_intern(&lf[86],14, C_text("scheme#newline"));
lf[87]=C_h_intern(&lf[87],11, C_text("##sys#print"));
lf[88]=C_h_intern(&lf[88],29, C_text("##sys#with-print-length-limit"));
lf[89]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002: "));
lf[90]=C_h_intern(&lf[90],18, C_text("##sys#write-char-0"));
lf[91]=C_h_intern(&lf[91],1, C_text("c"));
lf[93]=C_h_intern(&lf[93],1, C_text("f"));
lf[94]=C_h_intern(&lf[94],14, C_text("scheme#display"));
lf[95]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016no such frame\012"));
lf[96]=C_h_intern(&lf[96],28, C_text("##sys#repl-recent-call-chain"));
lf[97]=C_h_intern(&lf[97],1, C_text("g"));
lf[98]=C_decode_literal(C_heaptop,C_text("\376B\000\000\027no environment in frame"));
lf[99]=C_h_intern(&lf[99],9, C_text("frameinfo"));
lf[100]=C_decode_literal(C_heaptop,C_text("\376B\000\000\012; getting "));
lf[101]=C_h_intern(&lf[101],8, C_text("for-each"));
lf[102]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022no such variable: "));
lf[103]=C_h_intern(&lf[103],20, C_text("chicken.base#call/cc"));
lf[104]=C_decode_literal(C_heaptop,C_text("\376B\000\000#string or symbol required for `,g\047\012"));
lf[105]=C_h_intern(&lf[105],1, C_text("s"));
lf[106]=C_h_intern(&lf[106],1, C_text("\077"));
lf[107]=C_h_intern(&lf[107],18, C_text("chicken.base#print"));
lf[108]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002 ,"));
lf[109]=C_decode_literal(C_heaptop,C_text("\376B\000\003\266Toplevel commands:\012\012 ,\077                Show this text\012 ,p EXP            Pr"
"etty print evaluated expression EXP\012 ,d EXP            Describe result of evalua"
"ted expression EXP\012 ,du EXP           Dump data of expression EXP\012 ,dur EXP N   "
"     Dump range\012 ,q                Quit interpreter\012 ,l FILENAME ...   Load one "
"or more files\012 ,ln FILENAME ...  Load one or more files and print result of each"
" top-level expression\012 ,r                Show system information\012 ,h            "
"    Show history of expression results\012 ,ch               Clear history of expre"
"ssion results\012 ,e FILENAME       Run external editor\012 ,s TEXT ...       Execute "
"shell-command\012 ,exn              Describe last exception\012 ,c                Show"
" call-chain of most recent error\012 ,f N              Select frame N\012 ,g NAME     "
"      Get variable NAME from current frame\012 ,t EXP            Evaluate form and "
"print elapsed time\012 ,x EXP            Pretty print expanded expression EXP\012"));
lf[110]=C_decode_literal(C_heaptop,C_text("\376B\000\0005undefined toplevel command ~s - enter `,\077\047 for help~%"));
lf[111]=C_h_intern(&lf[111],17, C_text("chicken.sort#sort"));
lf[112]=C_h_intern(&lf[112],32, C_text("chicken.port#with-output-to-port"));
lf[113]=C_h_intern(&lf[113],28, C_text("chicken.process-context#argv"));
lf[114]=C_h_intern(&lf[114],9, C_text("##sys#/-2"));
lf[115]=C_h_intern(&lf[115],15, C_text("scheme#truncate"));
lf[116]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001\012"));
lf[117]=C_h_intern(&lf[117],18, C_text("scheme#make-string"));
lf[118]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004  ~a"));
lf[119]=C_decode_literal(C_heaptop,C_text("\376B\000\000\027interrupts are enabled\012"));
lf[120]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010(64-bit)"));
lf[121]=C_decode_literal(C_heaptop,C_text("\376B\000\000\000"));
lf[122]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010 (fixed)"));
lf[123]=C_decode_literal(C_heaptop,C_text("\376B\000\000\000"));
lf[124]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010downward"));
lf[125]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006upward"));
lf[126]=C_decode_literal(C_heaptop,C_text("\376B\000\002\354~%~%~\012                   Machine type:    \011~A ~A~%~\012                   Soft"
"ware type:   \011~A~%~\012                   Software version:\011~A~%~\012                 "
"  Build platform:  \011~A~%~\012                   Installation prefix:\011~A~%~\012        "
"           Extension installation location:\011~A~%~\012                   Extension p"
"ath:  \011~A~%~\012                   Include path:    \011~A~%~\012                   Keywo"
"rd style:   \011~A~%~\012                   Symbol-table load:\011~S~%  ~\012               "
"      Avg bucket length:\011~S~%  ~\012                     Total symbol count:\011~S~%~\012"
"                   Memory:\011heap size is ~S bytes~A with ~S bytes currently in us"
"e~%~  \012                     nursery size is ~S bytes, stack grows ~A~%~\012        "
"           Command line:    \011~S~%"));
lf[127]=C_h_intern(&lf[127],23, C_text("##sys#include-pathnames"));
lf[128]=C_h_intern(&lf[128],31, C_text("chicken.keyword#keyword->string"));
lf[129]=C_h_intern(&lf[129],26, C_text("chicken.base#keyword-style"));
lf[130]=C_h_intern(&lf[130],32, C_text("chicken.platform#repository-path"));
lf[131]=C_h_intern(&lf[131],40, C_text("chicken.platform#installation-repository"));
lf[132]=C_h_intern(&lf[132],31, C_text("chicken.platform#build-platform"));
lf[133]=C_h_intern(&lf[133],33, C_text("chicken.platform#software-version"));
lf[134]=C_h_intern(&lf[134],30, C_text("chicken.platform#software-type"));
lf[135]=C_h_intern(&lf[135],25, C_text("chicken.platform#feature\077"));
lf[136]=C_h_intern_kw(&lf[136],5, C_text("64bit"));
lf[137]=C_h_intern(&lf[137],29, C_text("chicken.platform#machine-type"));
lf[138]=C_h_intern(&lf[138],3, C_text("map"));
lf[139]=C_h_intern(&lf[139],15, C_text("scheme#string<\077"));
lf[140]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015Features:~%~%"));
lf[141]=C_h_intern(&lf[141],28, C_text("chicken.gc#memory-statistics"));
lf[142]=C_h_intern(&lf[142],23, C_text("##sys#symbol-table-info"));
lf[143]=C_h_intern(&lf[143],13, C_text("chicken.gc#gc"));
lf[145]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\010\001u8vector\376\003\000\000\002\376B\000\000\030vector of unsigned bytes\376\003\000\000\002\376\001\000\000\017\001u8vector-le"
"ngth\376\003\000\000\002\376\001\000\000\014\001u8vector-ref\376\377\016\376\003\000\000\002\376\003\000\000\002\376\001\000\000\010\001s8vector\376\003\000\000\002\376B\000\000\026vector of signed"
" bytes\376\003\000\000\002\376\001\000\000\017\001s8vector-length\376\003\000\000\002\376\001\000\000\014\001s8vector-ref\376\377\016\376\003\000\000\002\376\003\000\000\002\376\001\000\000\011\001u16vec"
"tor\376\003\000\000\002\376B\000\000\037vector of unsigned 16-bit words\376\003\000\000\002\376\001\000\000\020\001u16vector-length\376\003\000\000\002\376\001\000\000"
"\015\001u16vector-ref\376\377\016\376\003\000\000\002\376\003\000\000\002\376\001\000\000\011\001s16vector\376\003\000\000\002\376B\000\000\035vector of signed 16-bit wor"
"ds\376\003\000\000\002\376\001\000\000\020\001s16vector-length\376\003\000\000\002\376\001\000\000\015\001s16vector-ref\376\377\016\376\003\000\000\002\376\003\000\000\002\376\001\000\000\011\001u32vecto"
"r\376\003\000\000\002\376B\000\000\037vector of unsigned 32-bit words\376\003\000\000\002\376\001\000\000\020\001u32vector-length\376\003\000\000\002\376\001\000\000\015\001"
"u32vector-ref\376\377\016\376\003\000\000\002\376\003\000\000\002\376\001\000\000\011\001s32vector\376\003\000\000\002\376B\000\000\035vector of signed 32-bit words"
"\376\003\000\000\002\376\001\000\000\020\001s32vector-length\376\003\000\000\002\376\001\000\000\015\001s32vector-ref\376\377\016\376\003\000\000\002\376\003\000\000\002\376\001\000\000\011\001u64vector\376"
"\003\000\000\002\376B\000\000\037vector of unsigned 64-bit words\376\003\000\000\002\376\001\000\000\020\001u64vector-length\376\003\000\000\002\376\001\000\000\015\001u6"
"4vector-ref\376\377\016\376\003\000\000\002\376\003\000\000\002\376\001\000\000\011\001s64vector\376\003\000\000\002\376B\000\000\035vector of signed 64-bit words\376\003"
"\000\000\002\376\001\000\000\020\001s64vector-length\376\003\000\000\002\376\001\000\000\015\001s64vector-ref\376\377\016\376\003\000\000\002\376\003\000\000\002\376\001\000\000\011\001f32vector\376\003\000"
"\000\002\376B\000\000\027vector of 32-bit floats\376\003\000\000\002\376\001\000\000\020\001f32vector-length\376\003\000\000\002\376\001\000\000\015\001f32vector-re"
"f\376\377\016\376\003\000\000\002\376\003\000\000\002\376\001\000\000\011\001f64vector\376\003\000\000\002\376B\000\000\027vector of 64-bit floats\376\003\000\000\002\376\001\000\000\020\001f64vect"
"or-length\376\003\000\000\002\376\001\000\000\015\001f64vector-ref\376\377\016\376\377\016"));
lf[147]=C_h_intern(&lf[147],13, C_text("scheme#length"));
lf[148]=C_h_intern(&lf[148],15, C_text("scheme#list-ref"));
lf[149]=C_h_intern(&lf[149],17, C_text("scheme#string-ref"));
lf[150]=C_h_intern(&lf[150],22, C_text("chicken.format#fprintf"));
lf[151]=C_decode_literal(C_heaptop,C_text("\376B\000\000 ~% (~A elements not displayed)~%"));
lf[152]=C_decode_literal(C_heaptop,C_text("\376B\000\000.\011(followed by ~A identical instance~a)~% ...~%"));
lf[153]=C_decode_literal(C_heaptop,C_text("\376B\000\000\000"));
lf[154]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001s"));
lf[155]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007 ~S: ~S"));
lf[156]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021~A of length ~S~%"));
lf[157]=C_decode_literal(C_heaptop,C_text("\376B\000\000$character ~S, code: ~S, #x~X, #o~O~%"));
lf[158]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016boolean true~%"));
lf[159]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017boolean false~%"));
lf[160]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014empty list~%"));
lf[161]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024end-of-file object~%"));
lf[162]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024unspecified object~%"));
lf[163]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016, character ~S"));
lf[164]=C_decode_literal(C_heaptop,C_text("\376B\000\0002exact immediate integer ~S~%  #x~X~%  #o~O~%  #b~B"));
lf[165]=C_decode_literal(C_heaptop,C_text("\376B\000\0000exact large integer ~S~%  #x~X~%  #o~O~%  #b~B~%"));
lf[166]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017unbound value~%"));
lf[167]=C_decode_literal(C_heaptop,C_text("\376B\000\000\034inexact rational number ~S~%"));
lf[168]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020exact ratio ~S~%"));
lf[169]=C_decode_literal(C_heaptop,C_text("\376B\000\000\026~A complex number ~S~%"));
lf[170]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005exact"));
lf[171]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007inexact"));
lf[172]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013number ~S~%"));
lf[173]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006string"));
lf[174]=C_h_intern(&lf[174],10, C_text("##sys#size"));
lf[175]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006vector"));
lf[176]=C_h_intern(&lf[176],10, C_text("##sys#slot"));
lf[177]=C_decode_literal(C_heaptop,C_text("\376B\000\000\035keyword symbol with name ~s~%"));
lf[178]=C_h_intern(&lf[178],20, C_text("##sys#symbol->string"));
lf[179]=C_h_intern(&lf[179],12, C_text("scheme#write"));
lf[180]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005  ~s\011"));
lf[181]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020  \012properties:\012\012"));
lf[182]=C_decode_literal(C_heaptop,C_text("\376B\000\000\000"));
lf[183]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013uninterned "));
lf[184]=C_decode_literal(C_heaptop,C_text("\376B\000\000\027~asymbol with name ~S~%"));
lf[185]=C_h_intern(&lf[185],22, C_text("##sys#interned-symbol\077"));
lf[186]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010unbound "));
lf[187]=C_h_intern(&lf[187],34, C_text("##sys#symbol-has-toplevel-binding\077"));
lf[188]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005eol~%"));
lf[189]=C_decode_literal(C_heaptop,C_text("\376B\000\000\012(circle)~%"));
lf[190]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006~S -> "));
lf[191]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024circular structure: "));
lf[192]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004list"));
lf[193]=C_decode_literal(C_heaptop,C_text("\376B\000\000\036pair with car ~S~%and cdr ~S~%"));
lf[194]=C_h_intern(&lf[194],22, C_text("chicken.format#sprintf"));
lf[195]=C_decode_literal(C_heaptop,C_text("\376B\000\000 procedure with code pointer 0x~X"));
lf[196]=C_h_intern(&lf[196],27, C_text("##sys#peek-unsigned-integer"));
lf[197]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005input"));
lf[198]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006output"));
lf[199]=C_decode_literal(C_heaptop,C_text("\376B\000\0005~A port of type ~A with name ~S and file pointer ~X~%"));
lf[200]=C_decode_literal(C_heaptop,C_text("\376B\000\000/locative~%  pointer ~X~%  index ~A~%  type ~A~%"));
lf[201]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004slot"));
lf[202]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004char"));
lf[203]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010u8vector"));
lf[204]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010s8vector"));
lf[205]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011u16vector"));
lf[206]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011s16vector"));
lf[207]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011u32vector"));
lf[208]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011s32vector"));
lf[209]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011u64vector"));
lf[210]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011s64vector"));
lf[211]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011f32vector"));
lf[212]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011f64vector"));
lf[213]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024machine pointer ~X~%"));
lf[215]=C_h_intern(&lf[215],10, C_text("##sys#byte"));
lf[216]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022blob of size ~S:~%"));
lf[217]=C_decode_literal(C_heaptop,C_text("\376B\000\000\030lambda information: ~s~%"));
lf[218]=C_h_intern(&lf[218],25, C_text("##sys#lambda-info->string"));
lf[219]=C_h_intern(&lf[219],10, C_text("hash-table"));
lf[220]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013 ~S\011-> ~S~%"));
lf[221]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025  hash function: ~a~%"));
lf[222]=C_decode_literal(C_heaptop,C_text("\376B\000\000\000"));
lf[223]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001s"));
lf[224]=C_decode_literal(C_heaptop,C_text("\376B\000\000:hash-table with ~S element~a~%  comparison procedure: ~A~%"));
lf[225]=C_h_intern(&lf[225],9, C_text("condition"));
lf[226]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007\011~s: ~s"));
lf[227]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005 ~s~%"));
lf[228]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017condition: ~s~%"));
lf[229]=C_h_intern(&lf[229],13, C_text("scheme#append"));
lf[230]=C_decode_literal(C_heaptop,C_text("\376B\000\000\031structure of type `~S\047:~%"));
lf[231]=C_h_intern(&lf[231],31, C_text("chicken.internal#hash-table-ref"));
lf[232]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020unknown object~%"));
lf[233]=C_h_intern(&lf[233],17, C_text("##sys#bytevector\077"));
lf[234]=C_h_intern(&lf[234],15, C_text("##sys#locative\077"));
lf[235]=C_h_intern(&lf[235],18, C_text("chicken.base#port\077"));
lf[236]=C_h_intern(&lf[236],24, C_text("chicken.keyword#keyword\077"));
lf[237]=C_decode_literal(C_heaptop,C_text("\376B\000\000\034statically allocated (0x~X) "));
lf[238]=C_h_intern(&lf[238],19, C_text("##sys#block-address"));
lf[239]=C_h_intern(&lf[239],26, C_text("chicken.csi#set-describer!"));
lf[240]=C_h_intern(&lf[240],14, C_text("set-describer!"));
lf[241]=C_h_intern(&lf[241],32, C_text("chicken.internal#hash-table-set!"));
lf[242]=C_h_intern(&lf[242],10, C_text("scheme#min"));
lf[243]=C_h_intern(&lf[243],4, C_text("dump"));
lf[244]=C_decode_literal(C_heaptop,C_text("\376B\000\000\034cannot dump immediate object"));
lf[245]=C_h_intern(&lf[245],15, C_text("##sys#peek-byte"));
lf[246]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022cannot dump object"));
lf[247]=C_h_intern(&lf[247],20, C_text("##sys#number->string"));
lf[248]=C_h_intern(&lf[248],21, C_text("##sys#write-char/port"));
lf[249]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003   "));
lf[250]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004:\011  "));
lf[251]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002  "));
lf[252]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006  ---\012"));
lf[253]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002] "));
lf[254]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003\011  "));
lf[255]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002[]"));
lf[256]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002  "));
lf[257]=C_h_intern(&lf[257],25, C_text("##sys#user-interrupt-hook"));
lf[258]=C_h_intern(&lf[258],19, C_text("##sys#signal-vector"));
lf[261]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002-s"));
lf[262]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003-ss"));
lf[263]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007-script"));
lf[264]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003-sx"));
lf[265]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002--"));
lf[266]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016invalid option"));
lf[267]=C_h_intern(&lf[267],18, C_text("##sys#string->list"));
lf[268]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\003-ss\376\003\000\000\002\376B\000\000\003-sx\376\003\000\000\002\376B\000\000\007-script\376\003\000\000\002\376B\000\000\010-version\376\003\000\000\002\376B\000\000\005-help\376\003\000\000"
"\002\376B\000\000\006--help\376\003\000\000\002\376B\000\000\010-feature\376\003\000\000\002\376B\000\000\013-no-feature\376\003\000\000\002\376B\000\000\005-eval\376\003\000\000\002\376B\000\000\021-cas"
"e-insensitive\376\003\000\000\002\376B\000\000\016-keyword-style\376\003\000\000\002\376B\000\000\030-no-parentheses-synonyms\376\003\000\000\002\376B\000\000"
"\021-no-symbol-escape\376\003\000\000\002\376B\000\000\014-r5rs-syntax\376\003\000\000\002\376B\000\000\013-setup-mode\376\003\000\000\002\376B\000\000\022-require-"
"extension\376\003\000\000\002\376B\000\000\006-batch\376\003\000\000\002\376B\000\000\006-quiet\376\003\000\000\002\376B\000\000\014-no-warnings\376\003\000\000\002\376B\000\000\010-no-ini"
"t\376\003\000\000\002\376B\000\000\015-include-path\376\003\000\000\002\376B\000\000\010-release\376\003\000\000\002\376B\000\000\006-print\376\003\000\000\002\376B\000\000\015-pretty-prin"
"t\376\003\000\000\002\376B\000\000\002--\376\377\016"));
lf[269]=C_h_intern(&lf[269],34, C_text("chicken.base#implicit-exit-handler"));
lf[270]=C_decode_literal(C_heaptop,C_text("\376B\000\000\047missing argument to command-line option"));
lf[271]=C_h_intern(&lf[271],10, C_text("##sys#list"));
lf[272]=C_h_intern(&lf[272],30, C_text("chicken.base#open-input-string"));
lf[273]=C_h_intern(&lf[273],34, C_text("chicken.platform#register-feature!"));
lf[274]=C_h_intern(&lf[274],36, C_text("chicken.platform#unregister-feature!"));
lf[275]=C_h_intern(&lf[275],20, C_text("##sys#user-read-hook"));
lf[276]=C_h_intern(&lf[276],5, C_text("quote"));
lf[277]=C_h_intern(&lf[277],23, C_text("##sys#sharp-number-hook"));
lf[278]=C_h_intern(&lf[278],17, C_text("chicken.repl#repl"));
lf[279]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\002--\376\003\000\000\002\376B\000\000\002-b\376\003\000\000\002\376B\000\000\006-batch\376\003\000\000\002\376B\000\000\002-q\376\003\000\000\002\376B\000\000\006-quiet\376\003\000\000\002\376B\000\000\002-n"
"\376\003\000\000\002\376B\000\000\010-no-init\376\003\000\000\002\376B\000\000\002-w\376\003\000\000\002\376B\000\000\014-no-warnings\376\003\000\000\002\376B\000\000\002-i\376\003\000\000\002\376B\000\000\021-case-"
"insensitive\376\003\000\000\002\376B\000\000\030-no-parentheses-synonyms\376\003\000\000\002\376B\000\000\021-no-symbol-escape\376\003\000\000\002\376B\000"
"\000\014-r5rs-syntax\376\003\000\000\002\376B\000\000\013-setup-mode\376\003\000\000\002\376B\000\000\003-ss\376\003\000\000\002\376B\000\000\003-sx\376\003\000\000\002\376B\000\000\002-s\376\003\000\000\002\376B"
"\000\000\007-script\376\377\016"));
lf[280]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002-D"));
lf[281]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010-feature"));
lf[282]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002-I"));
lf[283]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015-include-path"));
lf[284]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002-K"));
lf[285]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016-keyword-style"));
lf[286]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013-no-feature"));
lf[287]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002-R"));
lf[288]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022-require-extension"));
lf[289]=C_h_intern(&lf[289],6, C_text("import"));
lf[290]=C_h_intern(&lf[290],21, C_text("scheme#string->symbol"));
lf[291]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002-e"));
lf[292]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005-eval"));
lf[293]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002-p"));
lf[294]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006-print"));
lf[295]=C_h_intern(&lf[295],15, C_text("scheme#for-each"));
lf[296]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002-P"));
lf[297]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015-pretty-print"));
lf[298]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003-ss"));
lf[299]=C_h_intern(&lf[299],46, C_text("chicken.process-context#command-line-arguments"));
lf[300]=C_h_intern(&lf[300],4, C_text("main"));
lf[301]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003-sx"));
lf[302]=C_h_intern(&lf[302],20, C_text("##sys#standard-error"));
lf[303]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002; "));
lf[304]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003\012; "));
lf[305]=C_h_intern(&lf[305],25, C_text("chicken.base#flush-output"));
lf[306]=C_h_intern(&lf[306],34, C_text("chicken.port#with-output-to-string"));
lf[307]=C_decode_literal(C_heaptop,C_text("\376B\000\000\000"));
lf[308]=C_h_intern(&lf[308],30, C_text("chicken.pathname#make-pathname"));
lf[309]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001."));
lf[310]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004HOME"));
lf[311]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007chicken"));
lf[312]=C_h_intern(&lf[312],40, C_text("chicken.platform#system-config-directory"));
lf[313]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\002-n\376\003\000\000\002\376B\000\000\010-no-init\376\377\016"));
lf[314]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021Type ,\077 for help."));
lf[315]=C_decode_literal(C_heaptop,C_text("\376B\000\000KCHICKEN\012(c) 2008-2020, The CHICKEN Team\012(c) 2000-2007, Felix L. Winkelmann\012"
));
lf[316]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001\012"));
lf[317]=C_h_intern(&lf[317],32, C_text("chicken.platform#chicken-version"));
lf[318]=C_h_intern(&lf[318],25, C_text("chicken.load#load-verbose"));
lf[319]=C_h_intern(&lf[319],32, C_text("chicken.internal#default-imports"));
lf[320]=C_h_intern(&lf[320],17, C_text("import-for-syntax"));
lf[321]=C_h_intern(&lf[321],39, C_text("chicken.internal#default-syntax-imports"));
lf[322]=C_h_intern(&lf[322],26, C_text("chicken.base#symbol-escape"));
lf[323]=C_h_intern(&lf[323],33, C_text("chicken.base#parentheses-synonyms"));
lf[324]=C_h_intern_kw(&lf[324],4, C_text("none"));
lf[325]=C_h_intern(&lf[325],27, C_text("chicken.base#case-sensitive"));
lf[326]=C_decode_literal(C_heaptop,C_text("\376B\000\000/Disabled the CHICKEN extensions to R5RS syntax\012"));
lf[327]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\014-r5rs-syntax\376\377\016"));
lf[328]=C_decode_literal(C_heaptop,C_text("\376B\000\000%Disabled support for escaped symbols\012"));
lf[329]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\021-no-symbol-escape\376\377\016"));
lf[330]=C_decode_literal(C_heaptop,C_text("\376B\000\000\052Disabled support for parentheses synonyms\012"));
lf[331]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\030-no-parentheses-synonyms\376\377\016"));
lf[332]=C_decode_literal(C_heaptop,C_text("\376B\000\000+missing argument to `-keyword-style\047 option"));
lf[333]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006prefix"));
lf[334]=C_h_intern_kw(&lf[334],6, C_text("prefix"));
lf[335]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004none"));
lf[336]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006suffix"));
lf[337]=C_h_intern_kw(&lf[337],6, C_text("suffix"));
lf[338]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002-I"));
lf[339]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015-include-path"));
lf[340]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013-no-feature"));
lf[341]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002-D"));
lf[342]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010-feature"));
lf[343]=C_h_intern(&lf[343],16, C_text("case-insensitive"));
lf[344]=C_decode_literal(C_heaptop,C_text("\376B\000\000-Identifiers and symbols are case insensitive\012"));
lf[345]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\002-i\376\003\000\000\002\376B\000\000\021-case-insensitive\376\377\016"));
lf[346]=C_h_intern(&lf[346],22, C_text("##sys#warnings-enabled"));
lf[347]=C_decode_literal(C_heaptop,C_text("\376B\000\000\026Warnings are disabled\012"));
lf[348]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\002-w\376\003\000\000\002\376B\000\000\014-no-warnings\376\377\016"));
lf[349]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010-release"));
lf[350]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013-setup-mode"));
lf[351]=C_h_intern(&lf[351],16, C_text("##sys#setup-mode"));
lf[352]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010-version"));
lf[353]=C_decode_literal(C_heaptop,C_text("\376B\000\004e    -b  -batch                    terminate after command-line processing\012 "
"   -w  -no-warnings              disable all warnings\012    -K  -keyword-style STY"
"LE      enable alternative keyword-syntax\012                                   (pr"
"efix, suffix or none)\012        -no-parentheses-synonyms  disables list delimiter "
"synonyms\012        -no-symbol-escape         disables support for escaped symbols\012"
"        -r5rs-syntax              disables the CHICKEN extensions to\012           "
"                        R5RS syntax\012    -s  -script PATHNAME          use csi as"
" interpreter for Scheme scripts\012        -ss PATHNAME              same as `-s\047, "
"but invoke `main\047 procedure\012        -sx PATHNAME              same as `-s\047, but "
"print each expression\012                                   as it is evaluated\012    "
"    -setup-mode               prefer the current directory when locating extensi"
"ons\012    -R  -require-extension NAME   require extension and import before\012      "
"                             executing code\012    -I  -include-path PATHNAME    ad"
"d PATHNAME to include path\012    --                            ignore all followin"
"g options\012"));
lf[354]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003 \047\012"));
lf[355]=C_decode_literal(C_heaptop,C_text("\376B\000\000D    -n  -no-init                  do not load initialization file ` "));
lf[356]=C_h_intern(&lf[356],21, C_text("##sys#print-to-string"));
lf[357]=C_decode_literal(C_heaptop,C_text("\376B\000\003.usage: csi [OPTION ...] [FILENAME ...]\012\012  `csi\047 is the CHICKEN interpreter."
"\012  \012  FILENAME is a Scheme source file name with optional extension. OPTION may "
"be\012  one of the following:\012\012    -h  -help                     display this text "
"and exit\012        -version                  display version and exit\012        -rel"
"ease                  print release number and exit\012    -i  -case-insensitive   "
"      enable case-insensitive reading\012    -e  -eval EXPRESSION          evaluate"
" given expression\012    -p  -print EXPRESSION         evaluate and print result(s)"
"\012    -P  -pretty-print EXPRESSION  evaluate and print result(s) prettily\012    -D "
" -feature SYMBOL           register feature identifier\012        -no-feature SYMBO"
"L        disable built-in feature identifier\012    -q  -quiet                    d"
"o not print banner\012"));
lf[358]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\002-h\376\003\000\000\002\376B\000\000\005-help\376\003\000\000\002\376B\000\000\006--help\376\377\016"));
lf[359]=C_decode_literal(C_heaptop,C_text("\376B\000\000\000"));
lf[360]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024CHICKEN_INCLUDE_PATH"));
lf[361]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\002-q\376\003\000\000\002\376B\000\000\006-quiet\376\377\016"));
lf[362]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\002-b\376\003\000\000\002\376B\000\000\006-batch\376\377\016"));
lf[363]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\002-e\376\003\000\000\002\376B\000\000\002-p\376\003\000\000\002\376B\000\000\002-P\376\003\000\000\002\376B\000\000\005-eval\376\003\000\000\002\376B\000\000\006-print\376\003\000\000\002\376B\000\000\015-pr"
"etty-print\376\377\016"));
lf[364]=C_h_intern(&lf[364],14, C_text("chicken-script"));
lf[365]=C_h_intern(&lf[365],36, C_text("chicken.process-context#program-name"));
lf[366]=C_decode_literal(C_heaptop,C_text("\376B\000\000\042missing or invalid script argument"));
lf[367]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002--"));
lf[368]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\003-ss\376\003\000\000\002\376B\000\000\003-sx\376\003\000\000\002\376B\000\000\002-s\376\003\000\000\002\376B\000\000\007-script\376\377\016"));
lf[369]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\002-K\376\003\000\000\002\376B\000\000\016-keyword-style\376\377\016"));
lf[370]=C_decode_literal(C_heaptop,C_text("\376B\000\000\000"));
lf[371]=C_h_intern(&lf[371],30, C_text("chicken.base#get-output-string"));
lf[372]=C_h_intern(&lf[372],31, C_text("chicken.base#open-output-string"));
lf[373]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025invalid option syntax"));
lf[374]=C_h_intern(&lf[374],14, C_text("scheme#reverse"));
lf[375]=C_h_intern(&lf[375],40, C_text("chicken.condition#with-exception-handler"));
lf[376]=C_h_intern(&lf[376],37, C_text("scheme#call-with-current-continuation"));
lf[377]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013CSI_OPTIONS"));
lf[378]=C_h_intern(&lf[378],18, C_text("scheme#make-vector"));
lf[379]=C_h_intern(&lf[379],19, C_text("##sys#peek-c-string"));
lf[380]=C_decode_literal(C_heaptop,C_text("\376B\000\000(; resetting current module to toplevel~%"));
lf[381]=C_h_intern(&lf[381],19, C_text("##sys#switch-module"));
lf[382]=C_decode_literal(C_heaptop,C_text("\376B\000\000$; switching current module to `~a\047~%"));
lf[383]=C_decode_literal(C_heaptop,C_text("\376B\000\000\027undefined module `~a\047~%"));
lf[384]=C_h_intern(&lf[384],17, C_text("##sys#find-module"));
lf[385]=C_h_intern(&lf[385],25, C_text("##sys#resolve-module-name"));
lf[386]=C_h_intern(&lf[386],1, C_text("m"));
lf[387]=C_decode_literal(C_heaptop,C_text("\376B\000\0005,m MODULE         switch to module with name `MODULE\047"));
lf[388]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010#;~A~A> "));
lf[389]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003~a:"));
lf[390]=C_h_intern(&lf[390],17, C_text("##sys#module-name"));
lf[391]=C_decode_literal(C_heaptop,C_text("\376B\000\000\000"));
lf[392]=C_h_intern(&lf[392],20, C_text("##sys#current-module"));
lf[393]=C_h_intern(&lf[393],24, C_text("chicken.repl#repl-prompt"));
lf[394]=C_h_intern(&lf[394],17, C_text("##sys#make-string"));
lf[395]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013emacsclient"));
lf[396]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002vi"));
lf[397]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005EMACS"));
lf[398]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006VISUAL"));
lf[399]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006EDITOR"));
lf[400]=C_h_intern(&lf[400],27, C_text("chicken.base#make-parameter"));
lf[401]=C_h_intern(&lf[401],30, C_text("##sys#register-compiled-module"));
lf[402]=C_h_intern(&lf[402],11, C_text("chicken.csi"));
lf[403]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\016\001editor-command\376\001\000\000\032\001chicken.csi#editor-command\376\003\000\000\002\376\003\000\000\002\376\001\000\000\020\001to"
"plevel-command\376\001\000\000\034\001chicken.csi#toplevel-command\376\003\000\000\002\376\003\000\000\002\376\001\000\000\016\001set-describer!\376\001"
"\000\000\032\001chicken.csi#set-describer!\376\377\016"));
lf[404]=C_h_intern(&lf[404],22, C_text("##sys#with-environment"));
C_register_lf2(lf,405,create_ptable());{}
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2517,a[2]=t1,tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_library_toplevel(2,av2);}}

#ifdef C_ENABLE_PTABLES
static C_PTABLE_ENTRY ptable[500] = {
{C_text("f9340:csi_2escm"),(void*)f9340},
{C_text("f9344:csi_2escm"),(void*)f9344},
{C_text("f9432:csi_2escm"),(void*)f9432},
{C_text("f9473:csi_2escm"),(void*)f9473},
{C_text("f9499:csi_2escm"),(void*)f9499},
{C_text("f9503:csi_2escm"),(void*)f9503},
{C_text("f_2517:csi_2escm"),(void*)f_2517},
{C_text("f_2520:csi_2escm"),(void*)f_2520},
{C_text("f_2523:csi_2escm"),(void*)f_2523},
{C_text("f_2526:csi_2escm"),(void*)f_2526},
{C_text("f_2529:csi_2escm"),(void*)f_2529},
{C_text("f_2532:csi_2escm"),(void*)f_2532},
{C_text("f_2535:csi_2escm"),(void*)f_2535},
{C_text("f_2538:csi_2escm"),(void*)f_2538},
{C_text("f_2541:csi_2escm"),(void*)f_2541},
{C_text("f_2544:csi_2escm"),(void*)f_2544},
{C_text("f_2547:csi_2escm"),(void*)f_2547},
{C_text("f_2550:csi_2escm"),(void*)f_2550},
{C_text("f_3048:csi_2escm"),(void*)f_3048},
{C_text("f_3075:csi_2escm"),(void*)f_3075},
{C_text("f_3123:csi_2escm"),(void*)f_3123},
{C_text("f_3137:csi_2escm"),(void*)f_3137},
{C_text("f_3150:csi_2escm"),(void*)f_3150},
{C_text("f_3840:csi_2escm"),(void*)f_3840},
{C_text("f_3845:csi_2escm"),(void*)f_3845},
{C_text("f_3848:csi_2escm"),(void*)f_3848},
{C_text("f_3854:csi_2escm"),(void*)f_3854},
{C_text("f_3857:csi_2escm"),(void*)f_3857},
{C_text("f_3864:csi_2escm"),(void*)f_3864},
{C_text("f_3888:csi_2escm"),(void*)f_3888},
{C_text("f_3903:csi_2escm"),(void*)f_3903},
{C_text("f_3917:csi_2escm"),(void*)f_3917},
{C_text("f_3930:csi_2escm"),(void*)f_3930},
{C_text("f_3949:csi_2escm"),(void*)f_3949},
{C_text("f_3956:csi_2escm"),(void*)f_3956},
{C_text("f_3959:csi_2escm"),(void*)f_3959},
{C_text("f_3965:csi_2escm"),(void*)f_3965},
{C_text("f_3978:csi_2escm"),(void*)f_3978},
{C_text("f_3991:csi_2escm"),(void*)f_3991},
{C_text("f_4000:csi_2escm"),(void*)f_4000},
{C_text("f_4004:csi_2escm"),(void*)f_4004},
{C_text("f_4016:csi_2escm"),(void*)f_4016},
{C_text("f_4025:csi_2escm"),(void*)f_4025},
{C_text("f_4028:csi_2escm"),(void*)f_4028},
{C_text("f_4035:csi_2escm"),(void*)f_4035},
{C_text("f_4039:csi_2escm"),(void*)f_4039},
{C_text("f_4042:csi_2escm"),(void*)f_4042},
{C_text("f_4048:csi_2escm"),(void*)f_4048},
{C_text("f_4055:csi_2escm"),(void*)f_4055},
{C_text("f_4057:csi_2escm"),(void*)f_4057},
{C_text("f_4067:csi_2escm"),(void*)f_4067},
{C_text("f_4070:csi_2escm"),(void*)f_4070},
{C_text("f_4084:csi_2escm"),(void*)f_4084},
{C_text("f_4107:csi_2escm"),(void*)f_4107},
{C_text("f_4117:csi_2escm"),(void*)f_4117},
{C_text("f_4131:csi_2escm"),(void*)f_4131},
{C_text("f_4162:csi_2escm"),(void*)f_4162},
{C_text("f_4175:csi_2escm"),(void*)f_4175},
{C_text("f_4178:csi_2escm"),(void*)f_4178},
{C_text("f_4181:csi_2escm"),(void*)f_4181},
{C_text("f_4184:csi_2escm"),(void*)f_4184},
{C_text("f_4187:csi_2escm"),(void*)f_4187},
{C_text("f_4196:csi_2escm"),(void*)f_4196},
{C_text("f_4206:csi_2escm"),(void*)f_4206},
{C_text("f_4210:csi_2escm"),(void*)f_4210},
{C_text("f_4233:csi_2escm"),(void*)f_4233},
{C_text("f_4250:csi_2escm"),(void*)f_4250},
{C_text("f_4262:csi_2escm"),(void*)f_4262},
{C_text("f_4270:csi_2escm"),(void*)f_4270},
{C_text("f_4273:csi_2escm"),(void*)f_4273},
{C_text("f_4285:csi_2escm"),(void*)f_4285},
{C_text("f_4292:csi_2escm"),(void*)f_4292},
{C_text("f_4298:csi_2escm"),(void*)f_4298},
{C_text("f_4318:csi_2escm"),(void*)f_4318},
{C_text("f_4348:csi_2escm"),(void*)f_4348},
{C_text("f_4381:csi_2escm"),(void*)f_4381},
{C_text("f_4396:csi_2escm"),(void*)f_4396},
{C_text("f_4399:csi_2escm"),(void*)f_4399},
{C_text("f_4406:csi_2escm"),(void*)f_4406},
{C_text("f_4410:csi_2escm"),(void*)f_4410},
{C_text("f_4419:csi_2escm"),(void*)f_4419},
{C_text("f_4422:csi_2escm"),(void*)f_4422},
{C_text("f_4425:csi_2escm"),(void*)f_4425},
{C_text("f_4437:csi_2escm"),(void*)f_4437},
{C_text("f_4440:csi_2escm"),(void*)f_4440},
{C_text("f_4452:csi_2escm"),(void*)f_4452},
{C_text("f_4455:csi_2escm"),(void*)f_4455},
{C_text("f_4467:csi_2escm"),(void*)f_4467},
{C_text("f_4470:csi_2escm"),(void*)f_4470},
{C_text("f_4473:csi_2escm"),(void*)f_4473},
{C_text("f_4476:csi_2escm"),(void*)f_4476},
{C_text("f_4506:csi_2escm"),(void*)f_4506},
{C_text("f_4509:csi_2escm"),(void*)f_4509},
{C_text("f_4514:csi_2escm"),(void*)f_4514},
{C_text("f_4524:csi_2escm"),(void*)f_4524},
{C_text("f_4539:csi_2escm"),(void*)f_4539},
{C_text("f_4548:csi_2escm"),(void*)f_4548},
{C_text("f_4549:csi_2escm"),(void*)f_4549},
{C_text("f_4555:csi_2escm"),(void*)f_4555},
{C_text("f_4559:csi_2escm"),(void*)f_4559},
{C_text("f_4565:csi_2escm"),(void*)f_4565},
{C_text("f_4570:csi_2escm"),(void*)f_4570},
{C_text("f_4580:csi_2escm"),(void*)f_4580},
{C_text("f_4595:csi_2escm"),(void*)f_4595},
{C_text("f_4604:csi_2escm"),(void*)f_4604},
{C_text("f_4609:csi_2escm"),(void*)f_4609},
{C_text("f_4613:csi_2escm"),(void*)f_4613},
{C_text("f_4618:csi_2escm"),(void*)f_4618},
{C_text("f_4624:csi_2escm"),(void*)f_4624},
{C_text("f_4628:csi_2escm"),(void*)f_4628},
{C_text("f_4635:csi_2escm"),(void*)f_4635},
{C_text("f_4637:csi_2escm"),(void*)f_4637},
{C_text("f_4641:csi_2escm"),(void*)f_4641},
{C_text("f_4656:csi_2escm"),(void*)f_4656},
{C_text("f_4672:csi_2escm"),(void*)f_4672},
{C_text("f_4690:csi_2escm"),(void*)f_4690},
{C_text("f_4694:csi_2escm"),(void*)f_4694},
{C_text("f_4710:csi_2escm"),(void*)f_4710},
{C_text("f_4722:csi_2escm"),(void*)f_4722},
{C_text("f_4734:csi_2escm"),(void*)f_4734},
{C_text("f_4746:csi_2escm"),(void*)f_4746},
{C_text("f_4753:csi_2escm"),(void*)f_4753},
{C_text("f_4766:csi_2escm"),(void*)f_4766},
{C_text("f_4775:csi_2escm"),(void*)f_4775},
{C_text("f_4778:csi_2escm"),(void*)f_4778},
{C_text("f_4781:csi_2escm"),(void*)f_4781},
{C_text("f_4794:csi_2escm"),(void*)f_4794},
{C_text("f_4816:csi_2escm"),(void*)f_4816},
{C_text("f_4821:csi_2escm"),(void*)f_4821},
{C_text("f_4831:csi_2escm"),(void*)f_4831},
{C_text("f_4845:csi_2escm"),(void*)f_4845},
{C_text("f_4891:csi_2escm"),(void*)f_4891},
{C_text("f_4897:csi_2escm"),(void*)f_4897},
{C_text("f_4901:csi_2escm"),(void*)f_4901},
{C_text("f_4917:csi_2escm"),(void*)f_4917},
{C_text("f_4923:csi_2escm"),(void*)f_4923},
{C_text("f_4937:csi_2escm"),(void*)f_4937},
{C_text("f_4940:csi_2escm"),(void*)f_4940},
{C_text("f_4946:csi_2escm"),(void*)f_4946},
{C_text("f_4949:csi_2escm"),(void*)f_4949},
{C_text("f_4957:csi_2escm"),(void*)f_4957},
{C_text("f_4967:csi_2escm"),(void*)f_4967},
{C_text("f_4982:csi_2escm"),(void*)f_4982},
{C_text("f_4991:csi_2escm"),(void*)f_4991},
{C_text("f_4997:csi_2escm"),(void*)f_4997},
{C_text("f_5003:csi_2escm"),(void*)f_5003},
{C_text("f_5009:csi_2escm"),(void*)f_5009},
{C_text("f_5015:csi_2escm"),(void*)f_5015},
{C_text("f_5023:csi_2escm"),(void*)f_5023},
{C_text("f_5025:csi_2escm"),(void*)f_5025},
{C_text("f_5042:csi_2escm"),(void*)f_5042},
{C_text("f_5048:csi_2escm"),(void*)f_5048},
{C_text("f_5054:csi_2escm"),(void*)f_5054},
{C_text("f_5062:csi_2escm"),(void*)f_5062},
{C_text("f_5063:csi_2escm"),(void*)f_5063},
{C_text("f_5073:csi_2escm"),(void*)f_5073},
{C_text("f_5077:csi_2escm"),(void*)f_5077},
{C_text("f_5080:csi_2escm"),(void*)f_5080},
{C_text("f_5083:csi_2escm"),(void*)f_5083},
{C_text("f_5085:csi_2escm"),(void*)f_5085},
{C_text("f_5093:csi_2escm"),(void*)f_5093},
{C_text("f_5101:csi_2escm"),(void*)f_5101},
{C_text("f_5104:csi_2escm"),(void*)f_5104},
{C_text("f_5105:csi_2escm"),(void*)f_5105},
{C_text("f_5109:csi_2escm"),(void*)f_5109},
{C_text("f_5119:csi_2escm"),(void*)f_5119},
{C_text("f_5128:csi_2escm"),(void*)f_5128},
{C_text("f_5136:csi_2escm"),(void*)f_5136},
{C_text("f_5151:csi_2escm"),(void*)f_5151},
{C_text("f_5154:csi_2escm"),(void*)f_5154},
{C_text("f_5157:csi_2escm"),(void*)f_5157},
{C_text("f_5160:csi_2escm"),(void*)f_5160},
{C_text("f_5167:csi_2escm"),(void*)f_5167},
{C_text("f_5175:csi_2escm"),(void*)f_5175},
{C_text("f_5179:csi_2escm"),(void*)f_5179},
{C_text("f_5183:csi_2escm"),(void*)f_5183},
{C_text("f_5187:csi_2escm"),(void*)f_5187},
{C_text("f_5191:csi_2escm"),(void*)f_5191},
{C_text("f_5195:csi_2escm"),(void*)f_5195},
{C_text("f_5199:csi_2escm"),(void*)f_5199},
{C_text("f_5203:csi_2escm"),(void*)f_5203},
{C_text("f_5231:csi_2escm"),(void*)f_5231},
{C_text("f_5243:csi_2escm"),(void*)f_5243},
{C_text("f_5246:csi_2escm"),(void*)f_5246},
{C_text("f_5248:csi_2escm"),(void*)f_5248},
{C_text("f_5258:csi_2escm"),(void*)f_5258},
{C_text("f_5279:csi_2escm"),(void*)f_5279},
{C_text("f_5281:csi_2escm"),(void*)f_5281},
{C_text("f_5306:csi_2escm"),(void*)f_5306},
{C_text("f_5326:csi_2escm"),(void*)f_5326},
{C_text("f_5361:csi_2escm"),(void*)f_5361},
{C_text("f_5391:csi_2escm"),(void*)f_5391},
{C_text("f_5393:csi_2escm"),(void*)f_5393},
{C_text("f_5399:csi_2escm"),(void*)f_5399},
{C_text("f_5406:csi_2escm"),(void*)f_5406},
{C_text("f_5411:csi_2escm"),(void*)f_5411},
{C_text("f_5434:csi_2escm"),(void*)f_5434},
{C_text("f_5443:csi_2escm"),(void*)f_5443},
{C_text("f_5453:csi_2escm"),(void*)f_5453},
{C_text("f_5456:csi_2escm"),(void*)f_5456},
{C_text("f_5485:csi_2escm"),(void*)f_5485},
{C_text("f_5513:csi_2escm"),(void*)f_5513},
{C_text("f_5528:csi_2escm"),(void*)f_5528},
{C_text("f_5531:csi_2escm"),(void*)f_5531},
{C_text("f_5534:csi_2escm"),(void*)f_5534},
{C_text("f_5600:csi_2escm"),(void*)f_5600},
{C_text("f_5606:csi_2escm"),(void*)f_5606},
{C_text("f_5697:csi_2escm"),(void*)f_5697},
{C_text("f_5704:csi_2escm"),(void*)f_5704},
{C_text("f_5713:csi_2escm"),(void*)f_5713},
{C_text("f_5716:csi_2escm"),(void*)f_5716},
{C_text("f_5728:csi_2escm"),(void*)f_5728},
{C_text("f_5733:csi_2escm"),(void*)f_5733},
{C_text("f_5743:csi_2escm"),(void*)f_5743},
{C_text("f_5746:csi_2escm"),(void*)f_5746},
{C_text("f_5749:csi_2escm"),(void*)f_5749},
{C_text("f_5758:csi_2escm"),(void*)f_5758},
{C_text("f_5778:csi_2escm"),(void*)f_5778},
{C_text("f_5781:csi_2escm"),(void*)f_5781},
{C_text("f_5784:csi_2escm"),(void*)f_5784},
{C_text("f_5796:csi_2escm"),(void*)f_5796},
{C_text("f_5799:csi_2escm"),(void*)f_5799},
{C_text("f_5808:csi_2escm"),(void*)f_5808},
{C_text("f_5839:csi_2escm"),(void*)f_5839},
{C_text("f_5903:csi_2escm"),(void*)f_5903},
{C_text("f_5907:csi_2escm"),(void*)f_5907},
{C_text("f_5913:csi_2escm"),(void*)f_5913},
{C_text("f_5932:csi_2escm"),(void*)f_5932},
{C_text("f_5941:csi_2escm"),(void*)f_5941},
{C_text("f_5948:csi_2escm"),(void*)f_5948},
{C_text("f_6065:csi_2escm"),(void*)f_6065},
{C_text("f_6071:csi_2escm"),(void*)f_6071},
{C_text("f_6077:csi_2escm"),(void*)f_6077},
{C_text("f_6090:csi_2escm"),(void*)f_6090},
{C_text("f_6102:csi_2escm"),(void*)f_6102},
{C_text("f_6105:csi_2escm"),(void*)f_6105},
{C_text("f_6116:csi_2escm"),(void*)f_6116},
{C_text("f_6124:csi_2escm"),(void*)f_6124},
{C_text("f_6145:csi_2escm"),(void*)f_6145},
{C_text("f_6154:csi_2escm"),(void*)f_6154},
{C_text("f_6164:csi_2escm"),(void*)f_6164},
{C_text("f_6199:csi_2escm"),(void*)f_6199},
{C_text("f_6200:csi_2escm"),(void*)f_6200},
{C_text("f_6204:csi_2escm"),(void*)f_6204},
{C_text("f_6213:csi_2escm"),(void*)f_6213},
{C_text("f_6223:csi_2escm"),(void*)f_6223},
{C_text("f_6236:csi_2escm"),(void*)f_6236},
{C_text("f_6241:csi_2escm"),(void*)f_6241},
{C_text("f_6268:csi_2escm"),(void*)f_6268},
{C_text("f_6278:csi_2escm"),(void*)f_6278},
{C_text("f_6305:csi_2escm"),(void*)f_6305},
{C_text("f_6309:csi_2escm"),(void*)f_6309},
{C_text("f_6323:csi_2escm"),(void*)f_6323},
{C_text("f_6331:csi_2escm"),(void*)f_6331},
{C_text("f_6344:csi_2escm"),(void*)f_6344},
{C_text("f_6350:csi_2escm"),(void*)f_6350},
{C_text("f_6375:csi_2escm"),(void*)f_6375},
{C_text("f_6388:csi_2escm"),(void*)f_6388},
{C_text("f_6415:csi_2escm"),(void*)f_6415},
{C_text("f_6423:csi_2escm"),(void*)f_6423},
{C_text("f_6432:csi_2escm"),(void*)f_6432},
{C_text("f_6434:csi_2escm"),(void*)f_6434},
{C_text("f_6437:csi_2escm"),(void*)f_6437},
{C_text("f_6459:csi_2escm"),(void*)f_6459},
{C_text("f_6466:csi_2escm"),(void*)f_6466},
{C_text("f_6483:csi_2escm"),(void*)f_6483},
{C_text("f_6512:csi_2escm"),(void*)f_6512},
{C_text("f_6540:csi_2escm"),(void*)f_6540},
{C_text("f_6545:csi_2escm"),(void*)f_6545},
{C_text("f_6580:csi_2escm"),(void*)f_6580},
{C_text("f_6583:csi_2escm"),(void*)f_6583},
{C_text("f_6587:csi_2escm"),(void*)f_6587},
{C_text("f_6603:csi_2escm"),(void*)f_6603},
{C_text("f_6615:csi_2escm"),(void*)f_6615},
{C_text("f_6625:csi_2escm"),(void*)f_6625},
{C_text("f_6628:csi_2escm"),(void*)f_6628},
{C_text("f_6631:csi_2escm"),(void*)f_6631},
{C_text("f_6634:csi_2escm"),(void*)f_6634},
{C_text("f_6637:csi_2escm"),(void*)f_6637},
{C_text("f_6640:csi_2escm"),(void*)f_6640},
{C_text("f_6649:csi_2escm"),(void*)f_6649},
{C_text("f_6662:csi_2escm"),(void*)f_6662},
{C_text("f_6665:csi_2escm"),(void*)f_6665},
{C_text("f_6700:csi_2escm"),(void*)f_6700},
{C_text("f_6734:csi_2escm"),(void*)f_6734},
{C_text("f_6744:csi_2escm"),(void*)f_6744},
{C_text("f_6754:csi_2escm"),(void*)f_6754},
{C_text("f_6757:csi_2escm"),(void*)f_6757},
{C_text("f_6772:csi_2escm"),(void*)f_6772},
{C_text("f_6776:csi_2escm"),(void*)f_6776},
{C_text("f_6783:csi_2escm"),(void*)f_6783},
{C_text("f_6785:csi_2escm"),(void*)f_6785},
{C_text("f_6788:csi_2escm"),(void*)f_6788},
{C_text("f_6794:csi_2escm"),(void*)f_6794},
{C_text("f_6811:csi_2escm"),(void*)f_6811},
{C_text("f_6820:csi_2escm"),(void*)f_6820},
{C_text("f_6851:csi_2escm"),(void*)f_6851},
{C_text("f_6854:csi_2escm"),(void*)f_6854},
{C_text("f_6857:csi_2escm"),(void*)f_6857},
{C_text("f_6860:csi_2escm"),(void*)f_6860},
{C_text("f_6863:csi_2escm"),(void*)f_6863},
{C_text("f_6866:csi_2escm"),(void*)f_6866},
{C_text("f_6869:csi_2escm"),(void*)f_6869},
{C_text("f_6872:csi_2escm"),(void*)f_6872},
{C_text("f_6875:csi_2escm"),(void*)f_6875},
{C_text("f_6878:csi_2escm"),(void*)f_6878},
{C_text("f_6881:csi_2escm"),(void*)f_6881},
{C_text("f_6894:csi_2escm"),(void*)f_6894},
{C_text("f_6904:csi_2escm"),(void*)f_6904},
{C_text("f_6909:csi_2escm"),(void*)f_6909},
{C_text("f_6922:csi_2escm"),(void*)f_6922},
{C_text("f_6925:csi_2escm"),(void*)f_6925},
{C_text("f_6928:csi_2escm"),(void*)f_6928},
{C_text("f_6931:csi_2escm"),(void*)f_6931},
{C_text("f_6934:csi_2escm"),(void*)f_6934},
{C_text("f_6968:csi_2escm"),(void*)f_6968},
{C_text("f_6978:csi_2escm"),(void*)f_6978},
{C_text("f_7012:csi_2escm"),(void*)f_7012},
{C_text("f_7015:csi_2escm"),(void*)f_7015},
{C_text("f_7070:csi_2escm"),(void*)f_7070},
{C_text("f_7127:csi_2escm"),(void*)f_7127},
{C_text("f_7129:csi_2escm"),(void*)f_7129},
{C_text("f_7140:csi_2escm"),(void*)f_7140},
{C_text("f_7160:csi_2escm"),(void*)f_7160},
{C_text("f_7163:csi_2escm"),(void*)f_7163},
{C_text("f_7167:csi_2escm"),(void*)f_7167},
{C_text("f_7170:csi_2escm"),(void*)f_7170},
{C_text("f_7182:csi_2escm"),(void*)f_7182},
{C_text("f_7207:csi_2escm"),(void*)f_7207},
{C_text("f_7216:csi_2escm"),(void*)f_7216},
{C_text("f_7222:csi_2escm"),(void*)f_7222},
{C_text("f_7232:csi_2escm"),(void*)f_7232},
{C_text("f_7244:csi_2escm"),(void*)f_7244},
{C_text("f_7247:csi_2escm"),(void*)f_7247},
{C_text("f_7250:csi_2escm"),(void*)f_7250},
{C_text("f_7253:csi_2escm"),(void*)f_7253},
{C_text("f_7256:csi_2escm"),(void*)f_7256},
{C_text("f_7292:csi_2escm"),(void*)f_7292},
{C_text("f_7299:csi_2escm"),(void*)f_7299},
{C_text("f_7301:csi_2escm"),(void*)f_7301},
{C_text("f_7311:csi_2escm"),(void*)f_7311},
{C_text("f_7354:csi_2escm"),(void*)f_7354},
{C_text("f_7359:csi_2escm"),(void*)f_7359},
{C_text("f_7365:csi_2escm"),(void*)f_7365},
{C_text("f_7377:csi_2escm"),(void*)f_7377},
{C_text("f_7414:csi_2escm"),(void*)f_7414},
{C_text("f_7420:csi_2escm"),(void*)f_7420},
{C_text("f_7442:csi_2escm"),(void*)f_7442},
{C_text("f_7456:csi_2escm"),(void*)f_7456},
{C_text("f_7477:csi_2escm"),(void*)f_7477},
{C_text("f_7481:csi_2escm"),(void*)f_7481},
{C_text("f_7485:csi_2escm"),(void*)f_7485},
{C_text("f_7524:csi_2escm"),(void*)f_7524},
{C_text("f_7532:csi_2escm"),(void*)f_7532},
{C_text("f_7563:csi_2escm"),(void*)f_7563},
{C_text("f_7593:csi_2escm"),(void*)f_7593},
{C_text("f_7596:csi_2escm"),(void*)f_7596},
{C_text("f_7599:csi_2escm"),(void*)f_7599},
{C_text("f_7602:csi_2escm"),(void*)f_7602},
{C_text("f_7605:csi_2escm"),(void*)f_7605},
{C_text("f_7608:csi_2escm"),(void*)f_7608},
{C_text("f_7611:csi_2escm"),(void*)f_7611},
{C_text("f_7614:csi_2escm"),(void*)f_7614},
{C_text("f_7623:csi_2escm"),(void*)f_7623},
{C_text("f_7629:csi_2escm"),(void*)f_7629},
{C_text("f_7631:csi_2escm"),(void*)f_7631},
{C_text("f_7637:csi_2escm"),(void*)f_7637},
{C_text("f_7645:csi_2escm"),(void*)f_7645},
{C_text("f_7666:csi_2escm"),(void*)f_7666},
{C_text("f_7682:csi_2escm"),(void*)f_7682},
{C_text("f_7685:csi_2escm"),(void*)f_7685},
{C_text("f_7688:csi_2escm"),(void*)f_7688},
{C_text("f_7691:csi_2escm"),(void*)f_7691},
{C_text("f_7697:csi_2escm"),(void*)f_7697},
{C_text("f_7706:csi_2escm"),(void*)f_7706},
{C_text("f_7728:csi_2escm"),(void*)f_7728},
{C_text("f_7743:csi_2escm"),(void*)f_7743},
{C_text("f_7750:csi_2escm"),(void*)f_7750},
{C_text("f_7757:csi_2escm"),(void*)f_7757},
{C_text("f_7759:csi_2escm"),(void*)f_7759},
{C_text("f_7769:csi_2escm"),(void*)f_7769},
{C_text("f_7776:csi_2escm"),(void*)f_7776},
{C_text("f_7780:csi_2escm"),(void*)f_7780},
{C_text("f_7782:csi_2escm"),(void*)f_7782},
{C_text("f_7790:csi_2escm"),(void*)f_7790},
{C_text("f_7800:csi_2escm"),(void*)f_7800},
{C_text("f_7803:csi_2escm"),(void*)f_7803},
{C_text("f_7806:csi_2escm"),(void*)f_7806},
{C_text("f_7809:csi_2escm"),(void*)f_7809},
{C_text("f_7812:csi_2escm"),(void*)f_7812},
{C_text("f_7815:csi_2escm"),(void*)f_7815},
{C_text("f_7818:csi_2escm"),(void*)f_7818},
{C_text("f_7824:csi_2escm"),(void*)f_7824},
{C_text("f_7827:csi_2escm"),(void*)f_7827},
{C_text("f_7833:csi_2escm"),(void*)f_7833},
{C_text("f_7836:csi_2escm"),(void*)f_7836},
{C_text("f_7842:csi_2escm"),(void*)f_7842},
{C_text("f_7846:csi_2escm"),(void*)f_7846},
{C_text("f_7849:csi_2escm"),(void*)f_7849},
{C_text("f_7852:csi_2escm"),(void*)f_7852},
{C_text("f_7855:csi_2escm"),(void*)f_7855},
{C_text("f_7858:csi_2escm"),(void*)f_7858},
{C_text("f_7861:csi_2escm"),(void*)f_7861},
{C_text("f_7864:csi_2escm"),(void*)f_7864},
{C_text("f_7867:csi_2escm"),(void*)f_7867},
{C_text("f_7870:csi_2escm"),(void*)f_7870},
{C_text("f_7873:csi_2escm"),(void*)f_7873},
{C_text("f_7878:csi_2escm"),(void*)f_7878},
{C_text("f_7906:csi_2escm"),(void*)f_7906},
{C_text("f_7935:csi_2escm"),(void*)f_7935},
{C_text("f_7947:csi_2escm"),(void*)f_7947},
{C_text("f_7962:csi_2escm"),(void*)f_7962},
{C_text("f_7981:csi_2escm"),(void*)f_7981},
{C_text("f_7991:csi_2escm"),(void*)f_7991},
{C_text("f_8006:csi_2escm"),(void*)f_8006},
{C_text("f_8016:csi_2escm"),(void*)f_8016},
{C_text("f_8026:csi_2escm"),(void*)f_8026},
{C_text("f_8037:csi_2escm"),(void*)f_8037},
{C_text("f_8041:csi_2escm"),(void*)f_8041},
{C_text("f_8048:csi_2escm"),(void*)f_8048},
{C_text("f_8050:csi_2escm"),(void*)f_8050},
{C_text("f_8078:csi_2escm"),(void*)f_8078},
{C_text("f_8082:csi_2escm"),(void*)f_8082},
{C_text("f_8088:csi_2escm"),(void*)f_8088},
{C_text("f_8091:csi_2escm"),(void*)f_8091},
{C_text("f_8094:csi_2escm"),(void*)f_8094},
{C_text("f_8097:csi_2escm"),(void*)f_8097},
{C_text("f_8102:csi_2escm"),(void*)f_8102},
{C_text("f_8115:csi_2escm"),(void*)f_8115},
{C_text("f_8118:csi_2escm"),(void*)f_8118},
{C_text("f_8133:csi_2escm"),(void*)f_8133},
{C_text("f_8152:csi_2escm"),(void*)f_8152},
{C_text("f_8164:csi_2escm"),(void*)f_8164},
{C_text("f_8167:csi_2escm"),(void*)f_8167},
{C_text("f_8181:csi_2escm"),(void*)f_8181},
{C_text("f_8184:csi_2escm"),(void*)f_8184},
{C_text("f_8187:csi_2escm"),(void*)f_8187},
{C_text("f_8190:csi_2escm"),(void*)f_8190},
{C_text("f_8193:csi_2escm"),(void*)f_8193},
{C_text("f_8202:csi_2escm"),(void*)f_8202},
{C_text("f_8205:csi_2escm"),(void*)f_8205},
{C_text("f_8214:csi_2escm"),(void*)f_8214},
{C_text("f_8217:csi_2escm"),(void*)f_8217},
{C_text("f_8281:csi_2escm"),(void*)f_8281},
{C_text("f_8288:csi_2escm"),(void*)f_8288},
{C_text("f_8294:csi_2escm"),(void*)f_8294},
{C_text("f_8301:csi_2escm"),(void*)f_8301},
{C_text("f_8307:csi_2escm"),(void*)f_8307},
{C_text("f_8309:csi_2escm"),(void*)f_8309},
{C_text("f_8334:csi_2escm"),(void*)f_8334},
{C_text("f_8343:csi_2escm"),(void*)f_8343},
{C_text("f_8368:csi_2escm"),(void*)f_8368},
{C_text("f_8377:csi_2escm"),(void*)f_8377},
{C_text("f_8387:csi_2escm"),(void*)f_8387},
{C_text("f_8400:csi_2escm"),(void*)f_8400},
{C_text("f_8410:csi_2escm"),(void*)f_8410},
{C_text("f_8423:csi_2escm"),(void*)f_8423},
{C_text("f_8433:csi_2escm"),(void*)f_8433},
{C_text("f_8447:csi_2escm"),(void*)f_8447},
{C_text("f_8450:csi_2escm"),(void*)f_8450},
{C_text("f_8453:csi_2escm"),(void*)f_8453},
{C_text("f_8462:csi_2escm"),(void*)f_8462},
{C_text("f_8465:csi_2escm"),(void*)f_8465},
{C_text("f_8475:csi_2escm"),(void*)f_8475},
{C_text("f_8482:csi_2escm"),(void*)f_8482},
{C_text("f_8492:csi_2escm"),(void*)f_8492},
{C_text("f_8498:csi_2escm"),(void*)f_8498},
{C_text("f_8501:csi_2escm"),(void*)f_8501},
{C_text("f_8506:csi_2escm"),(void*)f_8506},
{C_text("f_8531:csi_2escm"),(void*)f_8531},
{C_text("f_8542:csi_2escm"),(void*)f_8542},
{C_text("f_8551:csi_2escm"),(void*)f_8551},
{C_text("f_8557:csi_2escm"),(void*)f_8557},
{C_text("f_8560:csi_2escm"),(void*)f_8560},
{C_text("f_8563:csi_2escm"),(void*)f_8563},
{C_text("f_8566:csi_2escm"),(void*)f_8566},
{C_text("f_8575:csi_2escm"),(void*)f_8575},
{C_text("f_8640:csi_2escm"),(void*)f_8640},
{C_text("f_8653:csi_2escm"),(void*)f_8653},
{C_text("f_8657:csi_2escm"),(void*)f_8657},
{C_text("f_8661:csi_2escm"),(void*)f_8661},
{C_text("f_8667:csi_2escm"),(void*)f_8667},
{C_text("f_8673:csi_2escm"),(void*)f_8673},
{C_text("f_8675:csi_2escm"),(void*)f_8675},
{C_text("f_8681:csi_2escm"),(void*)f_8681},
{C_text("f_8685:csi_2escm"),(void*)f_8685},
{C_text("f_8694:csi_2escm"),(void*)f_8694},
{C_text("f_8700:csi_2escm"),(void*)f_8700},
{C_text("f_8704:csi_2escm"),(void*)f_8704},
{C_text("f_8708:csi_2escm"),(void*)f_8708},
{C_text("f_8721:csi_2escm"),(void*)f_8721},
{C_text("f_8723:csi_2escm"),(void*)f_8723},
{C_text("f_8731:csi_2escm"),(void*)f_8731},
{C_text("f_8734:csi_2escm"),(void*)f_8734},
{C_text("f_8741:csi_2escm"),(void*)f_8741},
{C_text("f_8745:csi_2escm"),(void*)f_8745},
{C_text("f_8754:csi_2escm"),(void*)f_8754},
{C_text("f_8756:csi_2escm"),(void*)f_8756},
{C_text("toplevel:csi_2escm"),(void*)C_toplevel},
{NULL,NULL}};
#endif

static C_PTABLE_ENTRY *create_ptable(void){
#ifdef C_ENABLE_PTABLES
return ptable;
#else
return NULL;
#endif
}

/*
o|hiding unexported module binding: chicken.csi#constant193 
o|hiding unexported module binding: chicken.csi#partition 
o|hiding unexported module binding: chicken.csi#span 
o|hiding unexported module binding: chicken.csi#take 
o|hiding unexported module binding: chicken.csi#drop 
o|hiding unexported module binding: chicken.csi#split-at 
o|hiding unexported module binding: chicken.csi#append-map 
o|hiding unexported module binding: chicken.csi#every 
o|hiding unexported module binding: chicken.csi#any 
o|hiding unexported module binding: chicken.csi#cons* 
o|hiding unexported module binding: chicken.csi#concatenate 
o|hiding unexported module binding: chicken.csi#delete 
o|hiding unexported module binding: chicken.csi#first 
o|hiding unexported module binding: chicken.csi#second 
o|hiding unexported module binding: chicken.csi#third 
o|hiding unexported module binding: chicken.csi#fourth 
o|hiding unexported module binding: chicken.csi#fifth 
o|hiding unexported module binding: chicken.csi#delete-duplicates 
o|hiding unexported module binding: chicken.csi#alist-cons 
o|hiding unexported module binding: chicken.csi#filter 
o|hiding unexported module binding: chicken.csi#filter-map 
o|hiding unexported module binding: chicken.csi#remove 
o|hiding unexported module binding: chicken.csi#unzip1 
o|hiding unexported module binding: chicken.csi#last 
o|hiding unexported module binding: chicken.csi#list-index 
o|hiding unexported module binding: chicken.csi#lset-adjoin/eq? 
o|hiding unexported module binding: chicken.csi#lset-difference/eq? 
o|hiding unexported module binding: chicken.csi#lset-union/eq? 
o|hiding unexported module binding: chicken.csi#lset-intersection/eq? 
o|hiding unexported module binding: chicken.csi#list-tabulate 
o|hiding unexported module binding: chicken.csi#lset<=/eq? 
o|hiding unexported module binding: chicken.csi#lset=/eq? 
o|hiding unexported module binding: chicken.csi#length+ 
o|hiding unexported module binding: chicken.csi#find 
o|hiding unexported module binding: chicken.csi#find-tail 
o|hiding unexported module binding: chicken.csi#iota 
o|hiding unexported module binding: chicken.csi#make-list 
o|hiding unexported module binding: chicken.csi#posq 
o|hiding unexported module binding: chicken.csi#posv 
o|hiding unexported module binding: chicken.csi#constant680 
o|hiding unexported module binding: chicken.csi#selected-frame 
o|hiding unexported module binding: chicken.csi#default-editor 
o|hiding unexported module binding: chicken.csi#print-usage 
o|hiding unexported module binding: chicken.csi#print-banner 
o|hiding unexported module binding: chicken.csi#dirseparator? 
o|hiding unexported module binding: chicken.csi#chop-separator 
o|hiding unexported module binding: chicken.csi#lookup-script-file 
o|hiding unexported module binding: chicken.csi#history-list 
o|hiding unexported module binding: chicken.csi#history-count 
o|hiding unexported module binding: chicken.csi#history-add 
o|hiding unexported module binding: chicken.csi#history-clear 
o|hiding unexported module binding: chicken.csi#history-show 
o|hiding unexported module binding: chicken.csi#history-ref 
o|hiding unexported module binding: chicken.csi#register-repl-history! 
o|hiding unexported module binding: chicken.csi#tty-input? 
o|hiding unexported module binding: chicken.csi#command-table 
o|hiding unexported module binding: chicken.csi#csi-eval 
o|hiding unexported module binding: chicken.csi#parse-option-string 
o|hiding unexported module binding: chicken.csi#report 
o|hiding unexported module binding: chicken.csi#bytevector-data 
o|hiding unexported module binding: chicken.csi#circular-list? 
o|hiding unexported module binding: chicken.csi#improper-pairs? 
o|hiding unexported module binding: chicken.csi#describer-table 
o|hiding unexported module binding: chicken.csi#describe 
o|hiding unexported module binding: chicken.csi#dump 
o|hiding unexported module binding: chicken.csi#hexdump 
o|hiding unexported module binding: chicken.csi#show-frameinfo 
o|hiding unexported module binding: chicken.csi#select-frame 
o|hiding unexported module binding: chicken.csi#copy-from-frame 
o|hiding unexported module binding: chicken.csi#defhandler 
o|hiding unexported module binding: chicken.csi#member* 
o|hiding unexported module binding: chicken.csi#constant1665 
o|hiding unexported module binding: chicken.csi#constant1671 
o|hiding unexported module binding: chicken.csi#canonicalize-args 
o|hiding unexported module binding: chicken.csi#findall 
o|hiding unexported module binding: chicken.csi#constant1735 
o|hiding unexported module binding: chicken.csi#constant1743 
o|hiding unexported module binding: chicken.csi#run 
S|applied compiler syntax:
S|  scheme#for-each		11
S|  chicken.format#printf		4
S|  chicken.base#foldl		3
S|  scheme#map		10
S|  chicken.base#foldr		3
o|eliminated procedure checks: 161 
o|eliminated procedure checks: 1 
o|specializations:
o|  1 (scheme#string-length string)
o|  7 (scheme#string=? string string)
o|  1 (scheme#set-cdr! pair *)
o|  2 (scheme#cddr (pair * pair))
o|  2 (scheme#char=? char char)
o|  3 (scheme#cadr (pair * pair))
o|  1 (scheme#min fixnum fixnum)
o|  1 (scheme#memq * list)
o|  1 (scheme#number->string * *)
o|  2 (chicken.base#add1 *)
o|  1 (scheme#- fixnum fixnum)
o|  1 (scheme#/ * *)
o|  1 (scheme#current-output-port)
o|  2 (scheme#zero? integer)
o|  31 (scheme#eqv? * (or eof null fixnum char boolean symbol keyword))
o|  4 (##sys#check-output-port * * *)
o|  1 (scheme#> fixnum fixnum)
o|  5 (scheme#string-append string string)
o|  1 (scheme#make-string fixnum)
o|  1 (chicken.base#sub1 fixnum)
o|  1 (scheme#eqv? * *)
o|  6 (##sys#check-list (or pair list) *)
o|  40 (scheme#cdr pair)
o|  14 (scheme#car pair)
(o e)|safe calls: 1003 
(o e)|assignments to immediate values: 5 
o|removed side-effect free assignment to unused variable: chicken.csi#partition 
o|removed side-effect free assignment to unused variable: chicken.csi#span 
o|removed side-effect free assignment to unused variable: chicken.csi#drop 
o|removed side-effect free assignment to unused variable: chicken.csi#split-at 
o|removed side-effect free assignment to unused variable: chicken.csi#append-map 
o|inlining procedure: k2933 
o|inlining procedure: k2933 
o|inlining procedure: k2964 
o|inlining procedure: k2964 
o|removed side-effect free assignment to unused variable: chicken.csi#cons* 
o|removed side-effect free assignment to unused variable: chicken.csi#concatenate 
o|removed side-effect free assignment to unused variable: chicken.csi#first 
o|removed side-effect free assignment to unused variable: chicken.csi#second 
o|removed side-effect free assignment to unused variable: chicken.csi#third 
o|removed side-effect free assignment to unused variable: chicken.csi#fourth 
o|removed side-effect free assignment to unused variable: chicken.csi#fifth 
o|removed side-effect free assignment to unused variable: chicken.csi#alist-cons 
o|inlining procedure: k3181 
o|inlining procedure: k3181 
o|inlining procedure: k3173 
o|inlining procedure: k3173 
o|removed side-effect free assignment to unused variable: chicken.csi#filter-map 
o|removed side-effect free assignment to unused variable: chicken.csi#remove 
o|removed side-effect free assignment to unused variable: chicken.csi#unzip1 
o|removed side-effect free assignment to unused variable: chicken.csi#last 
o|removed side-effect free assignment to unused variable: chicken.csi#list-index 
o|removed side-effect free assignment to unused variable: chicken.csi#lset-adjoin/eq? 
o|removed side-effect free assignment to unused variable: chicken.csi#lset-difference/eq? 
o|removed side-effect free assignment to unused variable: chicken.csi#lset-union/eq? 
o|removed side-effect free assignment to unused variable: chicken.csi#lset-intersection/eq? 
o|inlining procedure: k3572 
o|inlining procedure: k3572 
o|removed side-effect free assignment to unused variable: chicken.csi#lset<=/eq? 
o|removed side-effect free assignment to unused variable: chicken.csi#lset=/eq? 
o|removed side-effect free assignment to unused variable: chicken.csi#length+ 
o|removed side-effect free assignment to unused variable: chicken.csi#find 
o|removed side-effect free assignment to unused variable: chicken.csi#find-tail 
o|removed side-effect free assignment to unused variable: chicken.csi#iota 
o|removed side-effect free assignment to unused variable: chicken.csi#make-list 
o|removed side-effect free assignment to unused variable: chicken.csi#posq 
o|removed side-effect free assignment to unused variable: chicken.csi#posv 
o|substituted constant variable: chicken.csi#constant193 
o|inlining procedure: k3893 
o|inlining procedure: k3893 
o|inlining procedure: k3912 
o|inlining procedure: k3912 
o|inlining procedure: k3951 
o|inlining procedure: k3951 
o|substituted constant variable: a3967 
o|inlining procedure: k4005 
o|inlining procedure: k4020 
o|contracted procedure: "(csi.scm:184) _getcwd709" 
o|inlining procedure: k4020 
o|inlining procedure: k4059 
o|inlining procedure: k4059 
o|substituted constant variable: a4090 
o|contracted procedure: "(csi.scm:183) string-index720" 
o|inlining procedure: k3980 
o|inlining procedure: k3980 
o|inlining procedure: k4005 
o|substituted constant variable: a4100 
o|inlining procedure: k4164 
o|propagated global variable: out772775 ##sys#standard-output 
o|substituted constant variable: a4171 
o|substituted constant variable: a4172 
o|inlining procedure: k4164 
o|propagated global variable: out772775 ##sys#standard-output 
o|inlining procedure: k4211 
o|inlining procedure: k4211 
o|inlining procedure: k4287 
o|inlining procedure: k4287 
o|contracted procedure: "(csi.scm:268) chicken.csi#tty-input?" 
o|inlining procedure: k4277 
o|inlining procedure: k4277 
o|inlining procedure: k4312 
o|inlining procedure: k4312 
o|inlining procedure: k4350 
o|inlining procedure: k4350 
o|inlining procedure: k4371 
o|contracted procedure: "(csi.scm:298) g864865" 
o|inlining procedure: k4371 
o|inlining procedure: k4411 
o|inlining procedure: k4411 
o|consed rest parameter at call site: "(csi.scm:317) chicken.csi#describe" 2 
o|inlining procedure: k4444 
o|consed rest parameter at call site: "(csi.scm:321) chicken.csi#dump" 2 
o|inlining procedure: k4444 
o|consed rest parameter at call site: "(csi.scm:327) chicken.csi#dump" 2 
o|inlining procedure: k4480 
o|consed rest parameter at call site: "(csi.scm:328) chicken.csi#report" 1 
o|inlining procedure: k4480 
o|inlining procedure: k4498 
o|inlining procedure: k4516 
o|inlining procedure: k4516 
o|inlining procedure: k4498 
o|inlining procedure: k4572 
o|inlining procedure: k4572 
o|inlining procedure: k4596 
o|inlining procedure: k4596 
o|inlining procedure: k4651 
o|consed rest parameter at call site: "(csi.scm:346) chicken.csi#describe" 2 
o|inlining procedure: k4651 
o|inlining procedure: k4664 
o|inlining procedure: k4664 
o|contracted procedure: "(csi.scm:355) chicken.csi#history-clear" 
o|inlining procedure: k4714 
o|inlining procedure: k4714 
o|inlining procedure: k4738 
o|inlining procedure: k4738 
o|inlining procedure: k4767 
o|inlining procedure: k4767 
o|inlining procedure: k4823 
o|contracted procedure: "(csi.scm:396) g959966" 
o|inlining procedure: k4800 
o|inlining procedure: k4800 
o|inlining procedure: k4823 
o|propagated global variable: g965967 chicken.csi#command-table 
o|substituted constant variable: a4850 
o|substituted constant variable: a4852 
o|substituted constant variable: a4854 
o|substituted constant variable: a4856 
o|substituted constant variable: a4858 
o|substituted constant variable: a4860 
o|substituted constant variable: a4862 
o|substituted constant variable: a4864 
o|substituted constant variable: a4866 
o|substituted constant variable: a4868 
o|substituted constant variable: a4870 
o|substituted constant variable: a4872 
o|substituted constant variable: a4874 
o|substituted constant variable: a4876 
o|substituted constant variable: a4878 
o|substituted constant variable: a4880 
o|substituted constant variable: a4882 
o|substituted constant variable: a4884 
o|substituted constant variable: a4886 
o|merged explicitly consed rest parameter: port1046 
o|substituted constant variable: a5094 
o|substituted constant variable: a5113 
o|inlining procedure: k5120 
o|inlining procedure: k5120 
o|inlining procedure: k5158 
o|inlining procedure: k5158 
o|inlining procedure: k5250 
o|inlining procedure: k5250 
o|inlining procedure: k5283 
o|inlining procedure: k5283 
o|propagated global variable: g10731077 ##sys#features 
o|merged explicitly consed rest parameter: rest11551157 
o|inlining procedure: k5416 
o|inlining procedure: k5416 
o|inlining procedure: k5445 
o|inlining procedure: k5475 
o|inlining procedure: k5475 
o|inlining procedure: k5445 
o|inlining procedure: k5532 
o|inlining procedure: k5532 
o|inlining procedure: k5556 
o|inlining procedure: k5556 
o|inlining procedure: k5574 
o|inlining procedure: k5574 
o|inlining procedure: k5592 
o|inlining procedure: k5592 
o|inlining procedure: k5625 
o|inlining procedure: k5625 
o|inlining procedure: k5640 
o|inlining procedure: k5640 
o|inlining procedure: k5659 
o|inlining procedure: k5659 
o|inlining procedure: k5665 
o|inlining procedure: k5665 
o|inlining procedure: k5683 
o|inlining procedure: k5683 
o|inlining procedure: k5705 
o|inlining procedure: k5735 
o|inlining procedure: k5735 
o|inlining procedure: k5705 
o|inlining procedure: k5810 
o|inlining procedure: k5810 
o|inlining procedure: k5831 
o|inlining procedure: k5831 
o|inlining procedure: k5866 
o|inlining procedure: k5866 
o|inlining procedure: k5888 
o|inlining procedure: k5888 
o|inlining procedure: k5936 
o|inlining procedure: k5957 
o|inlining procedure: k5957 
o|inlining procedure: k5969 
o|inlining procedure: k5969 
o|inlining procedure: k5981 
o|inlining procedure: k5981 
o|inlining procedure: k5993 
o|inlining procedure: k5993 
o|inlining procedure: k6005 
o|inlining procedure: k6005 
o|inlining procedure: k6017 
o|inlining procedure: k6017 
o|substituted constant variable: a6030 
o|substituted constant variable: a6032 
o|substituted constant variable: a6034 
o|substituted constant variable: a6036 
o|substituted constant variable: a6038 
o|substituted constant variable: a6040 
o|substituted constant variable: a6042 
o|substituted constant variable: a6044 
o|substituted constant variable: a6046 
o|substituted constant variable: a6048 
o|substituted constant variable: a6050 
o|substituted constant variable: a6052 
o|inlining procedure: k5936 
o|inlining procedure: k6066 
o|inlining procedure: k6066 
o|inlining procedure: k6091 
o|inlining procedure: k6118 
o|inlining procedure: k6118 
o|inlining procedure: k6156 
o|inlining procedure: k6156 
o|inlining procedure: k6091 
o|inlining procedure: k6215 
o|inlining procedure: k6215 
o|inlining procedure: k6270 
o|inlining procedure: k6270 
o|inlining procedure: k6294 
o|inlining procedure: k6352 
o|inlining procedure: k6352 
o|inlining procedure: k6320 
o|inlining procedure: k6320 
o|inlining procedure: k6294 
o|contracted procedure: "(csi.scm:637) chicken.csi#improper-pairs?" 
o|inlining procedure: k5363 
o|inlining procedure: k5363 
o|contracted procedure: "(csi.scm:637) chicken.csi#circular-list?" 
o|inlining procedure: k5328 
o|inlining procedure: k5348 
o|inlining procedure: k5348 
o|inlining procedure: k5328 
o|merged explicitly consed rest parameter: len-out1345 
o|inlining procedure: k6439 
o|inlining procedure: k6439 
o|inlining procedure: k6445 
o|inlining procedure: k6445 
o|inlining procedure: k6471 
o|inlining procedure: k6471 
o|inlining procedure: k6497 
o|inlining procedure: k6497 
o|inlining procedure: k6550 
(o x)|known list op on rest arg sublist: ##core#rest-cdr len-out1345 0 
o|inlining procedure: k6550 
o|inlining procedure: k6591 
o|inlining procedure: k6591 
o|inlining procedure: k6617 
o|inlining procedure: k6617 
o|inlining procedure: k6651 
o|inlining procedure: k6651 
o|inlining procedure: k6702 
o|inlining procedure: k6720 
o|inlining procedure: k6720 
o|inlining procedure: k6736 
o|inlining procedure: k6736 
o|inlining procedure: k6702 
o|inlining procedure: k6822 
o|propagated global variable: out14571460 ##sys#standard-output 
o|substituted constant variable: a6847 
o|substituted constant variable: a6848 
o|inlining procedure: k6822 
o|inlining procedure: k6896 
o|inlining procedure: k6896 
o|inlining procedure: k6911 
o|propagated global variable: out14971500 ##sys#standard-output 
o|substituted constant variable: a6918 
o|substituted constant variable: a6919 
o|inlining procedure: k6911 
o|propagated global variable: out14971500 ##sys#standard-output 
o|inlining procedure: k6970 
o|inlining procedure: k6970 
o|propagated global variable: out14691472 ##sys#standard-output 
o|substituted constant variable: a7008 
o|substituted constant variable: a7009 
o|propagated global variable: out14691472 ##sys#standard-output 
o|inlining procedure: k7024 
o|inlining procedure: k7024 
o|inlining procedure: k7038 
o|propagated global variable: out14571460 ##sys#standard-output 
o|inlining procedure: k7038 
o|inlining procedure: k7044 
o|inlining procedure: k7044 
o|propagated global variable: tmp14381440 ##sys#repl-recent-call-chain 
o|propagated global variable: tmp14381440 ##sys#repl-recent-call-chain 
o|inlining procedure: k7062 
o|inlining procedure: k7062 
o|inlining procedure: k7096 
o|inlining procedure: k7096 
o|inlining procedure: k7153 
o|inlining procedure: k7184 
o|inlining procedure: k7184 
o|inlining procedure: k7224 
o|inlining procedure: k7224 
o|inlining procedure: k7303 
o|inlining procedure: k7303 
o|inlining procedure: k7153 
o|inlining procedure: k7346 
o|inlining procedure: k7346 
o|propagated global variable: tmp15481550 ##sys#repl-recent-call-chain 
o|propagated global variable: tmp15481550 ##sys#repl-recent-call-chain 
o|inlining procedure: k7367 
o|inlining procedure: k7379 
o|inlining procedure: k7379 
o|inlining procedure: k7367 
o|inlining procedure: k7422 
o|inlining procedure: k7422 
o|inlining procedure: k7437 
o|inlining procedure: k7457 
o|inlining procedure: k7487 
o|contracted procedure: "(csi.scm:951) g16931702" 
o|inlining procedure: k7487 
o|inlining procedure: k7457 
o|contracted procedure: "(csi.scm:950) chicken.csi#findall" 
o|substituted constant variable: chicken.csi#constant1665 
o|inlining procedure: k7568 
o|inlining procedure: k7568 
o|inlining procedure: k7437 
o|inlining procedure: k7538 
o|substituted constant variable: chicken.csi#constant1671 
o|inlining procedure: k7538 
o|contracted procedure: "(csi.scm:1133) chicken.csi#run" 
o|inlining procedure: k7647 
o|inlining procedure: k7647 
o|inlining procedure: k7642 
o|inlining procedure: k7642 
o|merged explicitly consed rest parameter: rest18811883 
o|inlining procedure: k7761 
o|inlining procedure: k7761 
o|inlining procedure: k7880 
o|contracted procedure: "(csi.scm:1091) chicken.csi#register-repl-history!" 
o|inlining procedure: k4235 
o|inlining procedure: k4235 
o|inlining procedure: k7880 
o|inlining procedure: k7914 
o|inlining procedure: k7914 
o|substituted constant variable: a7955 
o|inlining procedure: k7952 
o|consed rest parameter at call site: "(csi.scm:1102) evalstring1820" 2 
o|inlining procedure: k7952 
o|substituted constant variable: a7974 
o|consed rest parameter at call site: "(csi.scm:1105) evalstring1820" 2 
o|substituted constant variable: a7999 
o|inlining procedure: k7996 
o|consed rest parameter at call site: "(csi.scm:1108) evalstring1820" 2 
o|inlining procedure: k7996 
o|inlining procedure: k8059 
o|inlining procedure: k8059 
o|inlining procedure: k8072 
o|inlining procedure: k8104 
o|inlining procedure: k8104 
o|substituted constant variable: a8126 
o|propagated global variable: g20502051 chicken.pretty-print#pretty-print 
o|inlining procedure: k8072 
o|substituted constant variable: a8141 
o|substituted constant variable: a8143 
o|substituted constant variable: a8145 
o|substituted constant variable: a8147 
o|substituted constant variable: chicken.csi#constant1743 
o|substituted constant variable: chicken.csi#constant1735 
o|contracted procedure: "(csi.scm:1085) loadinit1819" 
o|inlining procedure: k7692 
o|inlining procedure: k7692 
o|inlining procedure: k7716 
o|substituted constant variable: a7729 
o|inlining procedure: k7716 
o|inlining procedure: k8159 
o|inlining procedure: k8159 
o|inlining procedure: k8203 
o|inlining procedure: k8203 
o|inlining procedure: k8215 
o|inlining procedure: k8215 
o|inlining procedure: k8224 
o|inlining procedure: k8224 
o|inlining procedure: k8242 
o|inlining procedure: k8242 
o|contracted procedure: "(csi.scm:1048) chicken.csi#delete-duplicates" 
o|inlining procedure: k3125 
o|inlining procedure: k3125 
o|contracted procedure: "(mini-srfi-1.scm:123) chicken.csi#delete" 
o|inlining procedure: k3050 
o|inlining procedure: k3050 
o|inlining procedure: k8311 
o|inlining procedure: k8311 
o|inlining procedure: k8345 
o|inlining procedure: k8345 
o|inlining procedure: k8379 
o|inlining procedure: k8379 
o|inlining procedure: k8402 
o|inlining procedure: k8402 
o|inlining procedure: k8425 
o|inlining procedure: k8425 
o|inlining procedure: k8463 
o|inlining procedure: k8463 
o|contracted procedure: "(csi.scm:1027) chicken.csi#print-usage" 
o|inlining procedure: k8508 
o|inlining procedure: k8508 
o|inlining procedure: k8543 
o|inlining procedure: k8543 
o|inlining procedure: k8546 
o|inlining procedure: k8546 
o|inlining procedure: k8552 
o|inlining procedure: k8552 
o|inlining procedure: k8570 
o|inlining procedure: k8570 
o|substituted constant variable: a8590 
o|inlining procedure: k8610 
o|inlining procedure: k8610 
o|substituted constant variable: a8613 
o|inlining procedure: k8644 
o|inlining procedure: k8644 
o|contracted procedure: "(csi.scm:973) chicken.csi#parse-option-string" 
o|inlining procedure: k4959 
o|contracted procedure: "(csi.scm:436) g10041013" 
o|inlining procedure: k4929 
o|inlining procedure: k4929 
o|inlining procedure: k4959 
o|inlining procedure: k5027 
o|inlining procedure: k5027 
o|inlining procedure: k8686 
o|inlining procedure: k8686 
o|inlining procedure: k8732 
o|inlining procedure: k8732 
o|substituted constant variable: a8742 
o|inlining procedure: k8746 
o|inlining procedure: k8746 
o|replaced variables: 1385 
o|removed binding forms: 395 
o|removed side-effect free assignment to unused variable: chicken.csi#constant193 
o|removed side-effect free assignment to unused variable: chicken.csi#every 
o|removed side-effect free assignment to unused variable: chicken.csi#any 
o|removed side-effect free assignment to unused variable: chicken.csi#filter 
o|removed side-effect free assignment to unused variable: chicken.csi#list-tabulate 
o|substituted constant variable: int711716 
o|substituted constant variable: r40608793 
o|substituted constant variable: r39818794 
o|substituted constant variable: r40068796 
o|contracted procedure: "(csi.scm:358) chicken.csi#history-show" 
o|propagated global variable: out772775 ##sys#standard-output 
o|contracted procedure: "(csi.scm:364) chicken.csi#select-frame" 
o|contracted procedure: "(csi.scm:367) chicken.csi#copy-from-frame" 
o|converted assignments to bindings: (fail1563) 
o|converted assignments to bindings: (compare1558) 
o|substituted constant variable: r73478998 
o|converted assignments to bindings: (shorten1055) 
o|substituted constant variable: r54768860 
o|substituted constant variable: r54768860 
o|substituted constant variable: r54768862 
o|substituted constant variable: r54768862 
o|inlining procedure: k5604 
o|inlining procedure: k5532 
o|substituted constant variable: r56608885 
o|substituted constant variable: r56608885 
o|substituted constant variable: r56608887 
o|substituted constant variable: r56608887 
o|inlining procedure: k5532 
o|removed call to pure procedure with unused result: "(csi.scm:651) ##sys#size" 
o|substituted constant variable: r59588906 
o|substituted constant variable: r59588906 
o|inlining procedure: k5957 
o|inlining procedure: k5957 
o|substituted constant variable: r59708910 
o|inlining procedure: k5957 
o|inlining procedure: k5957 
o|substituted constant variable: r59828912 
o|inlining procedure: k5957 
o|inlining procedure: k5957 
o|substituted constant variable: r59948914 
o|inlining procedure: k5957 
o|inlining procedure: k5957 
o|substituted constant variable: r60068916 
o|inlining procedure: k5957 
o|inlining procedure: k5957 
o|substituted constant variable: r60188918 
o|inlining procedure: k5957 
o|inlining procedure: k5957 
o|substituted constant variable: r53648939 
o|substituted constant variable: r53298944 
o|converted assignments to bindings: (descseq1163) 
o|converted assignments to bindings: (bestlen1364) 
o|converted assignments to bindings: (justify1387) 
o|propagated global variable: out14571460 ##sys#standard-output 
o|propagated global variable: out14971500 ##sys#standard-output 
o|propagated global variable: out14691472 ##sys#standard-output 
o|substituted constant variable: r70258975 
o|substituted constant variable: r70258975 
o|substituted constant variable: r70258977 
o|substituted constant variable: r70258977 
o|substituted constant variable: r70398979 
o|substituted constant variable: r70398979 
o|substituted constant variable: r70398981 
o|substituted constant variable: r70398981 
o|substituted constant variable: r70458984 
o|converted assignments to bindings: (prin11435) 
o|substituted constant variable: r73689002 
o|removed side-effect free assignment to unused variable: chicken.csi#constant1665 
o|removed side-effect free assignment to unused variable: chicken.csi#constant1671 
o|substituted constant variable: r74239003 
o|substituted constant variable: clist1722 
o|substituted constant variable: r75399014 
o|removed side-effect free assignment to unused variable: chicken.csi#constant1735 
o|removed side-effect free assignment to unused variable: chicken.csi#constant1743 
o|substituted constant variable: r76439018 
o|substituted constant variable: r80609033 
o|substituted constant variable: r80609033 
o|substituted constant variable: r80739039 
o|substituted constant variable: r80739039 
o|substituted constant variable: r77179044 
o|substituted constant variable: r85449095 
o|substituted constant variable: r85449095 
o|substituted constant variable: r86459106 
o|substituted constant variable: r87339117 
o|substituted constant variable: r87339117 
o|converted assignments to bindings: (addext719) 
o|simplifications: ((let . 8)) 
o|replaced variables: 80 
o|removed binding forms: 1161 
o|inlining procedure: k3890 
o|inlining procedure: k4290 
o|inlining procedure: k4695 
o|inlining procedure: k4695 
o|inlining procedure: k7205 
o|contracted procedure: k5894 
o|inlining procedure: k6221 
o|inlining procedure: k7889 
o|inlining procedure: k7904 
o|inlining procedure: k7904 
o|inlining procedure: k7904 
o|inlining procedure: k7904 
o|inlining procedure: k7904 
o|inlining procedure: k7904 
o|inlining procedure: k7904 
o|inlining procedure: k8116 
o|inlining procedure: k7704 
o|inlining procedure: "(csi.scm:1082) chicken.csi#print-banner" 
o|inlining procedure: k8448 
o|inlining procedure: "(csi.scm:1030) chicken.csi#print-banner" 
o|inlining procedure: k8601 
o|inlining procedure: k8601 
o|inlining procedure: k8601 
o|inlining procedure: k4980 
o|replaced variables: 13 
o|removed binding forms: 148 
o|removed side-effect free assignment to unused variable: chicken.csi#print-banner 
o|substituted constant variable: r38919326 
o|substituted constant variable: r38919326 
o|inlining procedure: k3915 
o|substituted constant variable: r59589187 
o|substituted constant variable: r59589189 
o|substituted constant variable: r59589191 
o|substituted constant variable: r59589193 
o|substituted constant variable: r59589195 
o|substituted constant variable: r59589197 
o|substituted constant variable: r59589199 
o|substituted constant variable: r59589201 
o|substituted constant variable: r59589203 
o|substituted constant variable: r59589205 
o|substituted constant variable: r59589207 
o|substituted constant variable: r77059470 
o|replaced variables: 9 
o|removed binding forms: 23 
o|removed conditional forms: 2 
o|substituted constant variable: r39169515 
o|removed binding forms: 22 
o|removed conditional forms: 1 
o|simplifications: ((let . 1)) 
o|removed binding forms: 1 
o|simplifications: ((let . 29) (if . 46) (##core#call . 550)) 
o|  call simplifications:
o|    scheme#make-vector
o|    scheme#set-car!
o|    ##sys#cons	2
o|    scheme#char-whitespace?
o|    ##sys#list	3
o|    chicken.base#void
o|    scheme#member	9
o|    scheme#string->list
o|    scheme#string
o|    scheme#equal?	4
o|    chicken.fixnum#fxmod
o|    scheme#write-char	7
o|    ##sys#immediate?	2
o|    ##sys#permanent?
o|    scheme#char?
o|    chicken.base#fixnum?	2
o|    chicken.base#bignum?
o|    chicken.base#flonum?
o|    chicken.base#ratnum?
o|    chicken.base#cplxnum?
o|    scheme#vector?
o|    scheme#list?
o|    scheme#procedure?
o|    ##sys#pointer?	2
o|    ##sys#generic-structure?	2
o|    scheme#cdr	19
o|    scheme#caar
o|    scheme#cdar
o|    chicken.fixnum#fx=	3
o|    chicken.base#atom?
o|    scheme#memq	3
o|    scheme#cddr	3
o|    scheme#exact?
o|    scheme#integer->char	2
o|    scheme#char->integer
o|    ##sys#setslot	9
o|    scheme#<=
o|    scheme#+
o|    scheme#*
o|    scheme#eof-object?	4
o|    scheme#caddr
o|    scheme#symbol?	2
o|    scheme#string?	4
o|    ##sys#structure?	4
o|    ##sys#check-list	17
o|    scheme#string-length	4
o|    chicken.fixnum#fxmin
o|    scheme#string=?	7
o|    scheme#number?	2
o|    chicken.fixnum#fx<	4
o|    scheme#length	4
o|    chicken.fixnum#fx-	11
o|    scheme#list-ref	2
o|    scheme#>=	2
o|    scheme#eq?	44
o|    scheme#not	11
o|    scheme#apply	5
o|    ##sys#call-with-values	6
o|    ##sys#apply	2
o|    scheme#cadr	13
o|    scheme#car	21
o|    ##sys#check-symbol	2
o|    ##sys#check-string
o|    scheme#assq	4
o|    scheme#cons	26
o|    scheme#list	11
o|    scheme#set-cdr!	2
o|    chicken.fixnum#fx<=
o|    scheme#vector-ref	8
o|    scheme#null?	24
o|    ##sys#void	20
o|    chicken.fixnum#fx*
o|    scheme#vector-set!
o|    chicken.fixnum#fx>=	15
o|    chicken.fixnum#fx+	20
o|    scheme#pair?	31
o|    ##sys#slot	86
o|    ##sys#foreign-block-argument
o|    ##sys#foreign-fixnum-argument
o|    ##sys#size	12
o|    scheme#string-ref	4
o|    chicken.fixnum#fx>	6
o|    scheme#char=?	6
o|contracted procedure: k3833 
o|contracted procedure: k3890 
o|contracted procedure: k3906 
o|contracted procedure: k3909 
o|contracted procedure: k3921 
o|contracted procedure: k4097 
o|contracted procedure: k4008 
o|contracted procedure: k3938 
o|contracted procedure: k3942 
o|contracted procedure: k4062 
o|contracted procedure: k4078 
o|contracted procedure: k4087 
o|contracted procedure: k3971 
o|contracted procedure: k3983 
o|contracted procedure: k3996 
o|contracted procedure: k4093 
o|contracted procedure: k4102 
o|contracted procedure: k4136 
o|contracted procedure: k4109 
o|contracted procedure: k4112 
o|contracted procedure: k4118 
o|contracted procedure: k4122 
o|contracted procedure: k4125 
o|contracted procedure: k4133 
o|contracted procedure: k4223 
o|contracted procedure: k4214 
o|contracted procedure: k4341 
o|contracted procedure: k4300 
o|contracted procedure: k4303 
o|contracted procedure: k4306 
o|contracted procedure: k4309 
o|contracted procedure: k4324 
o|contracted procedure: k4335 
o|contracted procedure: k4331 
o|contracted procedure: k4353 
o|contracted procedure: k4365 
o|contracted procedure: k4368 
o|contracted procedure: k4376 
o|contracted procedure: k4391 
o|contracted procedure: k4414 
o|contracted procedure: k4432 
o|contracted procedure: k4447 
o|contracted procedure: k4462 
o|contracted procedure: k4483 
o|contracted procedure: k4492 
o|contracted procedure: k4501 
o|contracted procedure: k4519 
o|contracted procedure: k4529 
o|contracted procedure: k4533 
o|contracted procedure: k4543 
o|contracted procedure: k4575 
o|contracted procedure: k4585 
o|contracted procedure: k4589 
o|contracted procedure: k4599 
o|contracted procedure: k4648 
o|contracted procedure: k4661 
o|contracted procedure: k4667 
o|contracted procedure: k4683 
o|contracted procedure: k4676 
o|contracted procedure: k4705 
o|contracted procedure: k4152 
o|propagated global variable: r4153 ##sys#undefined-value 
o|contracted procedure: k4717 
o|contracted procedure: k4167 
o|contracted procedure: k4192 
o|contracted procedure: k4202 
o|contracted procedure: k4729 
o|contracted procedure: k4741 
o|contracted procedure: k7113 
o|contracted procedure: k7065 
o|contracted procedure: k7086 
o|contracted procedure: k7090 
o|contracted procedure: k7082 
o|contracted procedure: k7075 
o|contracted procedure: k7093 
o|contracted procedure: k7099 
o|contracted procedure: k7109 
o|contracted procedure: k4757 
o|contracted procedure: k7119 
o|contracted procedure: k7122 
o|contracted procedure: k7131 
o|contracted procedure: k7146 
o|contracted procedure: k7150 
o|contracted procedure: k7142 
o|contracted procedure: k7175 
o|propagated global variable: r7176 ##sys#undefined-value 
o|contracted procedure: k7187 
o|contracted procedure: k7193 
o|contracted procedure: k7196 
o|contracted procedure: k7199 
o|contracted procedure: k7202 
o|contracted procedure: k7213 
o|contracted procedure: k7227 
o|contracted procedure: k7237 
o|contracted procedure: k7261 
o|contracted procedure: k7269 
o|contracted procedure: k7265 
o|contracted procedure: k7275 
o|contracted procedure: k7278 
o|contracted procedure: k7281 
o|contracted procedure: k7284 
o|contracted procedure: k7287 
o|contracted procedure: k7331 
o|contracted procedure: k7306 
o|contracted procedure: k7316 
o|contracted procedure: k7320 
o|contracted procedure: k7324 
o|contracted procedure: k7328 
o|contracted procedure: k7340 
o|contracted procedure: k7349 
o|contracted procedure: k4770 
o|contracted procedure: k4783 
o|contracted procedure: k4789 
o|contracted procedure: k4811 
o|contracted procedure: k4826 
o|contracted procedure: k4836 
o|contracted procedure: k4840 
o|contracted procedure: k4797 
o|propagated global variable: g965967 chicken.csi#command-table 
o|contracted procedure: k4905 
o|contracted procedure: k4912 
o|contracted procedure: k5314 
o|contracted procedure: k5069 
o|contracted procedure: k5096 
o|contracted procedure: k5110 
o|contracted procedure: k5123 
o|contracted procedure: k5137 
o|contracted procedure: k5143 
o|contracted procedure: k5146 
o|contracted procedure: k5169 
o|contracted procedure: k5205 
o|contracted procedure: k5209 
o|contracted procedure: k5213 
o|contracted procedure: k5217 
o|contracted procedure: k5221 
o|contracted procedure: k5225 
o|contracted procedure: k5233 
o|contracted procedure: k5237 
o|contracted procedure: k5253 
o|contracted procedure: k5263 
o|contracted procedure: k5267 
o|contracted procedure: k5271 
o|contracted procedure: k5274 
o|contracted procedure: k5286 
o|contracted procedure: k5289 
o|contracted procedure: k5292 
o|contracted procedure: k5300 
o|contracted procedure: k5308 
o|propagated global variable: g10731077 ##sys#features 
o|contracted procedure: k6416 
o|contracted procedure: k5395 
o|contracted procedure: k5401 
o|contracted procedure: k5413 
o|contracted procedure: k5422 
o|contracted procedure: k5429 
o|contracted procedure: k5515 
o|contracted procedure: k5439 
o|contracted procedure: k5448 
o|contracted procedure: k5461 
o|contracted procedure: k5464 
o|contracted procedure: k5471 
o|contracted procedure: k5478 
o|contracted procedure: k5493 
o|contracted procedure: k5500 
o|contracted procedure: k5504 
o|contracted procedure: k5519 
o|contracted procedure: k5538 
o|contracted procedure: k5541 
o|contracted procedure: k5550 
o|contracted procedure: k5559 
o|contracted procedure: k5568 
o|contracted procedure: k5577 
o|contracted procedure: k6403 
o|contracted procedure: k5586 
o|propagated global variable: r6404 ##sys#undefined-value 
o|contracted procedure: k5595 
o|contracted procedure: k5601 
o|contracted procedure: k5610 
o|contracted procedure: k5619 
o|contracted procedure: k5634 
o|contracted procedure: k5643 
o|contracted procedure: k5652 
o|contracted procedure: k5662 
o|contracted procedure: k5668 
o|contracted procedure: k5677 
o|contracted procedure: k5686 
o|contracted procedure: k5708 
o|contracted procedure: k5717 
o|contracted procedure: k5723 
o|contracted procedure: k5738 
o|contracted procedure: k5754 
o|contracted procedure: k5764 
o|contracted procedure: k5768 
o|contracted procedure: k5772 
o|contracted procedure: k5804 
o|contracted procedure: k5813 
o|contracted procedure: k5816 
o|contracted procedure: k5860 
o|contracted procedure: k5825 
o|contracted procedure: k5854 
o|contracted procedure: k5834 
o|contracted procedure: k5846 
o|contracted procedure: k5869 
o|contracted procedure: k5878 
o|contracted procedure: k5891 
o|contracted procedure: k5933 
o|contracted procedure: k5918 
o|contracted procedure: k5922 
o|contracted procedure: k5926 
o|contracted procedure: k5950 
o|contracted procedure: k5954 
o|contracted procedure: k5960 
o|contracted procedure: k5966 
o|contracted procedure: k5972 
o|contracted procedure: k5978 
o|contracted procedure: k5984 
o|contracted procedure: k5990 
o|contracted procedure: k5996 
o|contracted procedure: k6002 
o|contracted procedure: k6008 
o|contracted procedure: k6014 
o|contracted procedure: k6020 
o|contracted procedure: k6026 
o|contracted procedure: k6056 
o|contracted procedure: k6072 
o|contracted procedure: k6094 
o|contracted procedure: k6097 
o|contracted procedure: k6106 
o|contracted procedure: k6109 
o|contracted procedure: k6121 
o|contracted procedure: k6130 
o|contracted procedure: k6134 
o|contracted procedure: k6137 
o|contracted procedure: k6140 
o|contracted procedure: k6150 
o|contracted procedure: k6159 
o|contracted procedure: k6169 
o|contracted procedure: k6173 
o|contracted procedure: k6177 
o|contracted procedure: k6188 
o|contracted procedure: k6181 
o|contracted procedure: k6185 
o|contracted procedure: k6194 
o|contracted procedure: k6209 
o|contracted procedure: k6218 
o|contracted procedure: k6228 
o|contracted procedure: k6255 
o|contracted procedure: k6231 
o|contracted procedure: k6247 
o|contracted procedure: k6251 
o|contracted procedure: k62289377 
o|contracted procedure: k6258 
o|contracted procedure: k6261 
o|contracted procedure: k6273 
o|contracted procedure: k6283 
o|contracted procedure: k6287 
o|contracted procedure: k6291 
o|contracted procedure: k6297 
o|contracted procedure: k6300 
o|contracted procedure: k6317 
o|contracted procedure: k6333 
o|contracted procedure: k6336 
o|contracted procedure: k6339 
o|contracted procedure: k6346 
o|contracted procedure: k6355 
o|contracted procedure: k6358 
o|contracted procedure: k6361 
o|contracted procedure: k6369 
o|contracted procedure: k6377 
o|contracted procedure: k6393 
o|contracted procedure: k5385 
o|contracted procedure: k5366 
o|contracted procedure: k5381 
o|contracted procedure: k5369 
o|contracted procedure: k5331 
o|contracted procedure: k5338 
o|contracted procedure: k5342 
o|contracted procedure: k5345 
o|contracted procedure: k6406 
o|contracted procedure: k6425 
o|contracted procedure: k6448 
o|contracted procedure: k6468 
o|contracted procedure: k6474 
o|contracted procedure: k6485 
o|contracted procedure: k6537 
o|contracted procedure: k6530 
o|contracted procedure: k6491 
o|contracted procedure: k6503 
o|contracted procedure: k6514 
o|contracted procedure: k6520 
o|contracted procedure: k6527 
o|contracted procedure: k6553 
o|contracted procedure: k6559 
o|contracted procedure: k6566 
o|contracted procedure: k6572 
o|contracted procedure: k6588 
o|contracted procedure: k6594 
o|contracted procedure: k6606 
o|contracted procedure: k6620 
o|contracted procedure: k6645 
o|contracted procedure: k6654 
o|contracted procedure: k6657 
o|contracted procedure: k6670 
o|contracted procedure: k6674 
o|contracted procedure: k6690 
o|contracted procedure: k6677 
o|contracted procedure: k6684 
o|contracted procedure: k6705 
o|contracted procedure: k6708 
o|contracted procedure: k6714 
o|contracted procedure: k6717 
o|contracted procedure: k6723 
o|contracted procedure: k6730 
o|contracted procedure: k6739 
o|contracted procedure: k6749 
o|contracted procedure: k6762 
o|contracted procedure: k6766 
o|contracted procedure: k6799 
o|contracted procedure: k6802 
o|contracted procedure: k6806 
o|contracted procedure: k6816 
o|contracted procedure: k6825 
o|contracted procedure: k6828 
o|contracted procedure: k6831 
o|contracted procedure: k6834 
o|contracted procedure: k6837 
o|contracted procedure: k6840 
o|contracted procedure: k6843 
o|contracted procedure: k6888 
o|contracted procedure: k6891 
o|contracted procedure: k6899 
o|contracted procedure: k6914 
o|contracted procedure: k6939 
o|contracted procedure: k6945 
o|contracted procedure: k6949 
o|contracted procedure: k6952 
o|contracted procedure: k6955 
o|contracted procedure: k6958 
o|contracted procedure: k6961 
o|contracted procedure: k6998 
o|contracted procedure: k6973 
o|contracted procedure: k6983 
o|contracted procedure: k6987 
o|contracted procedure: k6991 
o|contracted procedure: k6995 
o|contracted procedure: k7020 
o|contracted procedure: k7034 
o|contracted procedure: k7047 
o|contracted procedure: k7054 
o|contracted procedure: k7355 
o|contracted procedure: k7370 
o|contracted procedure: k7382 
o|contracted procedure: k7389 
o|contracted procedure: k7404 
o|contracted procedure: k7408 
o|contracted procedure: k7395 
o|contracted procedure: k7425 
o|contracted procedure: k7428 
o|contracted procedure: k7434 
o|contracted procedure: k7446 
o|contracted procedure: k7467 
o|contracted procedure: k7490 
o|contracted procedure: k7512 
o|contracted procedure: k7508 
o|contracted procedure: k7493 
o|contracted procedure: k7496 
o|contracted procedure: k7504 
o|contracted procedure: k7565 
o|contracted procedure: k7583 
o|contracted procedure: k7574 
o|contracted procedure: k7553 
o|contracted procedure: k7535 
o|contracted procedure: k7541 
o|contracted procedure: k7548 
o|contracted procedure: k7615 
o|contracted procedure: k7618 
o|contracted procedure: k7624 
o|contracted procedure: k7639 
o|contracted procedure: k7671 
o|contracted procedure: k7650 
o|contracted procedure: k7660 
o|contracted procedure: k7787 
o|contracted procedure: k7745 
o|contracted procedure: k7764 
o|contracted procedure: k7819 
o|contracted procedure: k7828 
o|contracted procedure: k7837 
o|contracted procedure: k7883 
o|contracted procedure: k4238 
o|contracted procedure: k4241 
o|contracted procedure: k4252 
o|contracted procedure: k7898 
o|contracted procedure: k7901 
o|contracted procedure: k7911 
o|contracted procedure: k79119437 
o|contracted procedure: k7917 
o|contracted procedure: k7921 
o|contracted procedure: k79119441 
o|contracted procedure: k7927 
o|contracted procedure: k7930 
o|contracted procedure: k7937 
o|contracted procedure: k79119445 
o|contracted procedure: k7941 
o|contracted procedure: k7949 
o|contracted procedure: k7957 
o|contracted procedure: k7964 
o|contracted procedure: k79119449 
o|contracted procedure: k7968 
o|contracted procedure: k7976 
o|contracted procedure: k7983 
o|contracted procedure: k79119453 
o|contracted procedure: k7987 
o|contracted procedure: k8001 
o|contracted procedure: k8008 
o|contracted procedure: k79119457 
o|contracted procedure: k8012 
o|contracted procedure: k8021 
o|contracted procedure: k8030 
o|contracted procedure: k8065 
o|contracted procedure: k8052 
o|contracted procedure: k8062 
o|contracted procedure: k79119461 
o|contracted procedure: k8075 
o|contracted procedure: k8083 
o|contracted procedure: k8107 
o|contracted procedure: k8110 
o|contracted procedure: k8123 
o|contracted procedure: k81239465 
o|contracted procedure: k8153 
o|contracted procedure: k7732 
o|contracted procedure: k7719 
o|contracted procedure: k7739 
o|contracted procedure: k8172 
o|contracted procedure: k8176 
o|contracted procedure: k8275 
o|contracted procedure: k8271 
o|contracted procedure: k8227 
o|contracted procedure: k8267 
o|contracted procedure: k8236 
o|contracted procedure: k8245 
o|contracted procedure: k8254 
o|contracted procedure: k3128 
o|contracted procedure: k3131 
o|contracted procedure: k3141 
o|contracted procedure: k3053 
o|contracted procedure: k3079 
o|contracted procedure: k3059 
o|contracted procedure: k8283 
o|contracted procedure: k8289 
o|contracted procedure: k8296 
o|contracted procedure: k8302 
o|contracted procedure: k8314 
o|contracted procedure: k8317 
o|contracted procedure: k8320 
o|contracted procedure: k8328 
o|contracted procedure: k8336 
o|contracted procedure: k8348 
o|contracted procedure: k8351 
o|contracted procedure: k8354 
o|contracted procedure: k8362 
o|contracted procedure: k8370 
o|contracted procedure: k8382 
o|contracted procedure: k8392 
o|contracted procedure: k8396 
o|contracted procedure: k8405 
o|contracted procedure: k8415 
o|contracted procedure: k8419 
o|contracted procedure: k8428 
o|contracted procedure: k8438 
o|contracted procedure: k8442 
o|contracted procedure: k8470 
o|contracted procedure: k8483 
o|contracted procedure: k8487 
o|contracted procedure: k3874 
o|contracted procedure: k3870 
o|contracted procedure: k3866 
o|contracted procedure: k8511 
o|contracted procedure: k8514 
o|contracted procedure: k8517 
o|contracted procedure: k8525 
o|contracted procedure: k8533 
o|contracted procedure: k8567 
o|contracted procedure: k8585 
o|contracted procedure: k8595 
o|contracted procedure: k8634 
o|contracted procedure: k8630 
o|contracted procedure: k8598 
o|contracted procedure: k8626 
o|contracted procedure: k8622 
o|contracted procedure: k8607 
o|contracted procedure: k8615 
o|contracted procedure: k8641 
o|contracted procedure: k8662 
o|contracted procedure: k4924 
o|contracted procedure: k4950 
o|contracted procedure: k4962 
o|contracted procedure: k4968 
o|contracted procedure: k4976 
o|contracted procedure: k4984 
o|contracted procedure: k4932 
o|contracted procedure: k5030 
o|contracted procedure: k5044 
o|contracted procedure: k8689 
o|contracted procedure: k8762 
o|contracted procedure: k8766 
o|contracted procedure: k8770 
o|simplifications: ((if . 9) (let . 224)) 
o|removed binding forms: 501 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest819822 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest819822 0 
o|contracted procedure: k4362 
o|inlining procedure: k7230 
o|contracted procedure: k6500 
(o x)|known list op on rest arg sublist: ##core#rest-null? _%rest13491374 1 
(o x)|known list op on rest arg sublist: ##core#rest-car _%rest13491374 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr _%rest13491374 1 
o|inlining procedure: k6879 
o|contracted procedure: k7027 
(o x)|known list op on rest arg sublist: ##core#rest-null? rs2066 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rs2066 0 
o|substituted constant variable: r8763 
o|substituted constant variable: r8767 
o|substituted constant variable: r8771 
o|replaced variables: 28 
o|removed binding forms: 3 
o|removed binding forms: 10 
o|direct leaf routine/allocation: g834835 6 
o|direct leaf routine/allocation: lp1138 0 
o|direct leaf routine/allocation: lp1124 0 
o|direct leaf routine/allocation: loop1723 0 
o|contracted procedure: "(csi.scm:275) k4312" 
o|contracted procedure: k5791 
o|converted assignments to bindings: (lp1138) 
o|converted assignments to bindings: (lp1124) 
o|contracted procedure: k7460 
o|converted assignments to bindings: (loop1723) 
o|simplifications: ((let . 3)) 
o|removed binding forms: 3 
o|customizable procedures: (k3846 g989990 doloop10251026 k4965 map-loop9981031 chicken.csi#canonicalize-args chicken.csi#lookup-script-file k7603 k7609 map-loop17911811 k7804 k7810 for-each-loop18231904 for-each-loop18331911 for-each-loop18431918 map-loop19251942 collect-options1818 map-loop19511968 loop354 loop374 chicken.csi#member* k7871 doloop20532054 evalstring1820 doloop18511999 chicken.csi#history-ref doloop18911892 g18611862 loop1853 k7440 map-loop16871712 loop1675 find1654 loop1651 k6809 g14801488 for-each-loop14791509 prin11435 doloop14931494 doloop14421448 justify1387 doloop13961398 doloop14051406 doloop13971413 doloop13861394 def-len13501372 def-out13511370 body13481357 bestlen1364 k5794 g13061307 map-loop13111328 g12951296 g12661273 for-each-loop12651282 loop1276 g12441251 for-each-loop12431254 doloop12381239 chicken.csi#hexdump loop-print1213 doloop12001201 loop21183 loop11173 map-loop10611078 g10881095 for-each-loop10871109 shorten1055 k5117 for-each-loop958970 k7125 g15771585 for-each-loop15761600 compare1558 doloop15901591 doloop15621567 fail1563 k7068 chicken.csi#show-frameinfo doloop769770 chicken.csi#history-add g912919 for-each-loop911931 for-each-loop892902 chicken.csi#report chicken.csi#dump chicken.csi#describe k4115 loop727 loop746 addext719) 
o|calls to known targets: 251 
o|unused rest argument: rest819822 f_4298 
o|identified direct recursive calls: f_7182 1 
o|identified direct recursive calls: f_5326 1 
o|identified direct recursive calls: f_6213 1 
o|identified direct recursive calls: f_5361 1 
o|identified direct recursive calls: f_7377 1 
o|identified direct recursive calls: f_7563 1 
o|identified direct recursive calls: f_7485 1 
o|unused rest argument: _1889 f_7790 
o|unused rest argument: rs2066 f_8050 
o|identified direct recursive calls: f_7878 2 
o|identified direct recursive calls: f_3048 2 
o|fast box initializations: 49 
o|fast global references: 81 
o|fast global assignments: 26 
o|dropping unused closure argument: f_3949 
o|dropping unused closure argument: f_4206 
o|dropping unused closure argument: f_5085 
o|dropping unused closure argument: f_5326 
o|dropping unused closure argument: f_5361 
o|dropping unused closure argument: f_6432 
o|dropping unused closure argument: f_6580 
o|dropping unused closure argument: f_6583 
o|dropping unused closure argument: f_6785 
o|dropping unused closure argument: f_6788 
o|dropping unused closure argument: f_7359 
o|dropping unused closure argument: f_7414 
o|dropping unused closure argument: f_7563 
o|dropping unused closure argument: f_7743 
*/
/* end of file */
