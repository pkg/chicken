/* Generated from posix.scm by the CHICKEN compiler
   http://www.call-cc.org
   Version 5.2.0 (rev 317468e4)
   linux-unix-gnu-x86-64 [ 64bit dload ptables ]
   command line: posix.scm -optimize-level 2 -include-path . -include-path ./ -inline -ignore-repository -feature chicken-bootstrap -no-warnings -specialize -consult-types-file ./types.db -explicit-use -no-trace -output-file posixwin.c -feature platform-windows -emit-import-library chicken.errno -emit-import-library chicken.file.posix -emit-import-library chicken.time.posix -emit-import-library chicken.process -emit-import-library chicken.process.signal -emit-import-library chicken.process-context.posix -no-module-registration
   unit: posix
   uses: scheduler pathname extras port lolevel library data-structures
*/
#include "chicken.h"


#include <signal.h>

static int C_not_implemented(void);
int C_not_implemented() { return -1; }

static C_TLS struct stat C_statbuf;

#define C_stat_type         (C_statbuf.st_mode & S_IFMT)
#define C_stat_perm         (C_statbuf.st_mode & ~S_IFMT)

#define C_u_i_stat(fn)      C_fix(C_stat(C_c_string(fn), &C_statbuf))
#define C_u_i_fstat(fd)     C_fix(fstat(C_unfix(fd), &C_statbuf))

#ifndef S_IFSOCK
# define S_IFSOCK           0140000
#endif

#ifndef S_IRUSR
# define S_IRUSR  S_IREAD
#endif
#ifndef S_IWUSR
# define S_IWUSR  S_IWRITE
#endif
#ifndef S_IXUSR
# define S_IXUSR  S_IEXEC
#endif

#ifndef S_IRGRP
# define S_IRGRP  S_IREAD
#endif
#ifndef S_IWGRP
# define S_IWGRP  S_IWRITE
#endif
#ifndef S_IXGRP
# define S_IXGRP  S_IEXEC
#endif

#ifndef S_IROTH
# define S_IROTH  S_IREAD
#endif
#ifndef S_IWOTH
# define S_IWOTH  S_IWRITE
#endif
#ifndef S_IXOTH
# define S_IXOTH  S_IEXEC
#endif

#define cpy_tmvec_to_tmstc08(ptm, v) \
    ((ptm)->tm_sec = C_unfix(C_block_item((v), 0)), \
    (ptm)->tm_min = C_unfix(C_block_item((v), 1)), \
    (ptm)->tm_hour = C_unfix(C_block_item((v), 2)), \
    (ptm)->tm_mday = C_unfix(C_block_item((v), 3)), \
    (ptm)->tm_mon = C_unfix(C_block_item((v), 4)), \
    (ptm)->tm_year = C_unfix(C_block_item((v), 5)), \
    (ptm)->tm_wday = C_unfix(C_block_item((v), 6)), \
    (ptm)->tm_yday = C_unfix(C_block_item((v), 7)), \
    (ptm)->tm_isdst = (C_block_item((v), 8) != C_SCHEME_FALSE))

#define cpy_tmvec_to_tmstc9(ptm, v) \
    (((struct tm *)ptm)->tm_gmtoff = -C_unfix(C_block_item((v), 9)))

#define C_tm_set_08(v, tm)  cpy_tmvec_to_tmstc08( (tm), (v) )
#define C_tm_set_9(v, tm)   cpy_tmvec_to_tmstc9( (tm), (v) )

static struct tm *
C_tm_set( C_word v, void *tm )
{
  C_tm_set_08( v, (struct tm *)tm );
#if defined(C_GNU_ENV) && !defined(__CYGWIN__) && !defined(__uClinux__)
  C_tm_set_9( v, (struct tm *)tm );
#endif
  return tm;
}

#define TIME_STRING_MAXLENGTH 255
static char C_time_string [TIME_STRING_MAXLENGTH + 1];
#undef TIME_STRING_MAXLENGTH

#define C_strftime(v, f, tm) \
        (strftime(C_time_string, sizeof(C_time_string), C_c_string(f), C_tm_set((v), (tm))) ? C_time_string : NULL)
#define C_a_mktime(ptr, c, v, tm)  C_int64_to_num(ptr, mktime(C_tm_set((v), C_data_pointer(tm))))
#define C_asctime(v, tm)    (asctime(C_tm_set((v), (tm))))

#define C_fdopen(a, n, fd, m) C_mpointer(a, fdopen(C_unfix(fd), C_c_string(m)))
#define C_dup(x)            C_fix(dup(C_unfix(x)))
#define C_dup2(x, y)        C_fix(dup2(C_unfix(x), C_unfix(y)))

#define C_set_file_ptr(port, ptr)  (C_set_block_item(port, 0, (C_block_item(ptr, 0))), C_SCHEME_UNDEFINED)

/* It is assumed that 'int' is-a 'long' */
#define C_ftell(a, n, p)    C_int64_to_num(a, ftell(C_port_file(p)))
#define C_fseek(p, n, w)    C_mk_nbool(fseek(C_port_file(p), C_num_to_int64(n), C_unfix(w)))
#define C_lseek(fd, o, w)     C_fix(lseek(C_unfix(fd), C_num_to_int64(o), C_unfix(w)))

#ifndef S_IFLNK
#define S_IFLNK S_IFREG
#endif

#ifndef S_IFREG
#define S_IFREG S_IFREG
#endif

#ifndef S_IFDIR
#define S_IFDIR S_IFREG
#endif

#ifndef S_IFCHR
#define S_IFCHR S_IFREG
#endif

#ifndef S_IFBLK
#define S_IFBLK S_IFREG
#endif

#ifndef S_IFSOCK
#define S_IFSOCK S_IFREG
#endif

#ifndef S_IFIFO
#define S_IFIFO S_IFREG
#endif



#ifndef WIN32_LEAN_AND_MEAN
# define WIN32_LEAN_AND_MEAN
#endif

#include <direct.h>
#include <errno.h>
#include <fcntl.h>
#include <io.h>
#include <process.h>
#include <signal.h>
#include <stdio.h>
#include <utime.h>
#include <windows.h>
#include <winsock2.h>

#define PIPE_BUF	512

#ifndef EWOULDBLOCK
# define EWOULDBLOCK 0
#endif

static C_TLS int C_pipefds[ 2 ];
static C_TLS time_t C_secs;

/* pipe handles */
static C_TLS HANDLE C_rd0, C_wr0, C_wr0_, C_rd1, C_wr1, C_rd1_;
static C_TLS HANDLE C_save0, C_save1; /* saved I/O handles */
static C_TLS char C_rdbuf; /* one-char buffer for read */
static C_TLS int C_exstatus;

/* platform information; initialized for cached testing */
static C_TLS char C_shlcmd[256] = "";

/* Current user name */
static C_TLS TCHAR C_username[255 + 1] = "";

#define open_binary_input_pipe(a, n, name)   C_mpointer(a, _popen(C_c_string(name), "r"))
#define open_text_input_pipe(a, n, name)     open_binary_input_pipe(a, n, name)
#define open_binary_output_pipe(a, n, name)  C_mpointer(a, _popen(C_c_string(name), "w"))
#define open_text_output_pipe(a, n, name)    open_binary_output_pipe(a, n, name)
#define close_pipe(p)			     C_fix(_pclose(C_port_file(p)))

#define C_chmod(fn, m)	    C_fix(chmod(C_c_string(fn), C_unfix(m)))
#define C_pipe(d, m)	    C_fix(_pipe(C_pipefds, PIPE_BUF, C_unfix(m)))
#define C_close(fd)	    C_fix(close(C_unfix(fd)))

#define C_u_i_lstat(fn)     C_u_i_stat(fn)

#define C_u_i_execvp(f,a)   C_fix(execvp(C_c_string(f), (const char *const *)C_c_pointer_vector_or_null(a)))
#define C_u_i_execve(f,a,e) C_fix(execve(C_c_string(f), (const char *const *)C_c_pointer_vector_or_null(a), (const char *const *)C_c_pointer_vector_or_null(e)))

/* MS replacement for the fork-exec pair */
#define C_u_i_spawnvp(m,f,a)    C_fix(spawnvp(C_unfix(m), C_c_string(f), (const char *const *)C_c_pointer_vector_or_null(a)))
#define C_u_i_spawnvpe(m,f,a,e) C_fix(spawnvpe(C_unfix(m), C_c_string(f), (const char *const *)C_c_pointer_vector_or_null(a), (const char *const *)C_c_pointer_vector_or_null(e)))

#define C_open(fn, fl, m)   C_fix(open(C_c_string(fn), C_unfix(fl), C_unfix(m)))
#define C_read(fd, b, n)    C_fix(read(C_unfix(fd), C_data_pointer(b), C_unfix(n)))
#define C_write(fd, b, n)   C_fix(write(C_unfix(fd), C_data_pointer(b), C_unfix(n)))

#define C_flushall()	    C_fix(_flushall())

#define C_umask(m)          C_fix(_umask(C_unfix(m)))

#define C_ctime(n)	    (C_secs = (n), ctime(&C_secs))

#define TIME_STRING_MAXLENGTH 255
static char C_time_string [TIME_STRING_MAXLENGTH + 1];
#undef TIME_STRING_MAXLENGTH

/*
  mapping from Win32 error codes to errno
*/

typedef struct
{
    DWORD   win32;
    int	    libc;
} errmap_t;

static errmap_t errmap[] =
{
    {ERROR_INVALID_FUNCTION,	  EINVAL},
    {ERROR_FILE_NOT_FOUND,	  ENOENT},
    {ERROR_PATH_NOT_FOUND,	  ENOENT},
    {ERROR_TOO_MANY_OPEN_FILES,	  EMFILE},
    {ERROR_ACCESS_DENIED,	  EACCES},
    {ERROR_INVALID_HANDLE,	  EBADF},
    {ERROR_ARENA_TRASHED,	  ENOMEM},
    {ERROR_NOT_ENOUGH_MEMORY,	  ENOMEM},
    {ERROR_INVALID_BLOCK,	  ENOMEM},
    {ERROR_BAD_ENVIRONMENT,	  E2BIG},
    {ERROR_BAD_FORMAT,		  ENOEXEC},
    {ERROR_INVALID_ACCESS,	  EINVAL},
    {ERROR_INVALID_DATA,	  EINVAL},
    {ERROR_INVALID_DRIVE,	  ENOENT},
    {ERROR_CURRENT_DIRECTORY,	  EACCES},
    {ERROR_NOT_SAME_DEVICE,	  EXDEV},
    {ERROR_NO_MORE_FILES,	  ENOENT},
    {ERROR_LOCK_VIOLATION,	  EACCES},
    {ERROR_BAD_NETPATH,		  ENOENT},
    {ERROR_NETWORK_ACCESS_DENIED, EACCES},
    {ERROR_BAD_NET_NAME,	  ENOENT},
    {ERROR_FILE_EXISTS,		  EEXIST},
    {ERROR_CANNOT_MAKE,		  EACCES},
    {ERROR_FAIL_I24,		  EACCES},
    {ERROR_INVALID_PARAMETER,	  EINVAL},
    {ERROR_NO_PROC_SLOTS,	  EAGAIN},
    {ERROR_DRIVE_LOCKED,	  EACCES},
    {ERROR_BROKEN_PIPE,		  EPIPE},
    {ERROR_DISK_FULL,		  ENOSPC},
    {ERROR_INVALID_TARGET_HANDLE, EBADF},
    {ERROR_INVALID_HANDLE,	  EINVAL},
    {ERROR_WAIT_NO_CHILDREN,	  ECHILD},
    {ERROR_CHILD_NOT_COMPLETE,	  ECHILD},
    {ERROR_DIRECT_ACCESS_HANDLE,  EBADF},
    {ERROR_NEGATIVE_SEEK,	  EINVAL},
    {ERROR_SEEK_ON_DEVICE,	  EACCES},
    {ERROR_DIR_NOT_EMPTY,	  ENOTEMPTY},
    {ERROR_NOT_LOCKED,		  EACCES},
    {ERROR_BAD_PATHNAME,	  ENOENT},
    {ERROR_MAX_THRDS_REACHED,	  EAGAIN},
    {ERROR_LOCK_FAILED,		  EACCES},
    {ERROR_ALREADY_EXISTS,	  EEXIST},
    {ERROR_FILENAME_EXCED_RANGE,  ENOENT},
    {ERROR_NESTING_NOT_ALLOWED,	  EAGAIN},
    {ERROR_NOT_ENOUGH_QUOTA,	  ENOMEM},
    {0, 0}
};

static void C_fcall
set_errno(DWORD w32err)
{
    errmap_t *map;
    for (map = errmap; map->win32; ++map)
    {
	if (map->win32 == w32err)
	{
	    errno = map->libc;
	    return;
	}
    }
    errno = ENOSYS; /* For lack of anything better */
}

static int C_fcall
set_last_errno()
{
    set_errno(GetLastError());
    return 0;
}

static int fd_to_path(C_word fd, TCHAR path[])
{
  DWORD result;
  HANDLE fh = (HANDLE)_get_osfhandle(C_unfix(fd));

  if (fh == INVALID_HANDLE_VALUE) {
    set_last_errno();
    return -1;
  }

  result = GetFinalPathNameByHandle(fh, path, MAX_PATH, VOLUME_NAME_DOS);
  if (result == 0) {
    set_last_errno();
    return -1;
  } else if (result >= MAX_PATH) { /* Shouldn't happen */
    errno = ENOMEM; /* For lack of anything better */
    return -1;
  } else {
    return 0;
  }
}

static C_word C_fchmod(C_word fd, C_word m)
{
  TCHAR path[MAX_PATH];
  if (fd_to_path(fd, path) == -1) return C_fix(-1);
  else return C_fix(chmod(path, C_unfix(m)));
}

static C_word C_fchdir(C_word fd)
{
  TCHAR path[MAX_PATH];
  if (fd_to_path(fd, path) == -1) return C_fix(-1);
  else return C_fix(chdir(path));
}

static int C_fcall
process_wait(C_word h, C_word t)
{
    if (WaitForSingleObject((HANDLE)h, (t ? 0 : INFINITE)) == WAIT_OBJECT_0)
    {
	DWORD ret;
	if (GetExitCodeProcess((HANDLE)h, &ret))
	{
	    CloseHandle((HANDLE)h);
	    C_exstatus = ret;
	    return 1;
	}
    }
    return set_last_errno();
}

#define C_process_wait(p, t) (process_wait(C_unfix(p), C_truep(t)) ? C_SCHEME_TRUE : C_SCHEME_FALSE)


static C_TLS int C_isNT = 0;


static int C_fcall
C_windows_nt()
{
  static int has_info = 0;

  if(!has_info) {
    OSVERSIONINFO ovf;
    ZeroMemory(&ovf, sizeof(ovf));
    ovf.dwOSVersionInfoSize = sizeof(ovf);
    has_info = 1;

    if(GetVersionEx(&ovf)) {
      SYSTEM_INFO si;

      switch (ovf.dwPlatformId) {
      case VER_PLATFORM_WIN32_NT:
        return C_isNT = 1;
      }
    }
  }

  return C_isNT;
}


static int C_fcall
get_shlcmd()
{
    /* Do we need to build the shell command pathname? */
    if (!strlen(C_shlcmd))
    {
      char *cmdnam = C_windows_nt() ? "\\cmd.exe" : "\\command.com";
      UINT len = GetSystemDirectory(C_shlcmd, sizeof(C_shlcmd) - strlen(cmdnam));
      if (len)
	C_strlcpy(C_shlcmd + len, cmdnam, sizeof(C_shlcmd));
      else
	return set_last_errno();
    }

    return 1;
}

#define C_sysinfo() (sysinfo() ? C_SCHEME_TRUE : C_SCHEME_FALSE)
#define C_get_shlcmd() (get_shlcmd() ? C_SCHEME_TRUE : C_SCHEME_FALSE)

/* GetUserName */

static int C_fcall
get_user_name()
{
    if (!strlen(C_username))
    {
	DWORD bufCharCount = sizeof(C_username) / sizeof(C_username[0]);
	if (!GetUserName(C_username, &bufCharCount))
	    return set_last_errno();
    }
    return 1;
}

#define C_get_user_name() (get_user_name() ? C_SCHEME_TRUE : C_SCHEME_FALSE)

/*
    Spawn a process directly.
    Params:
    app		Command to execute.
    cmdlin	Command line (arguments).
    env		Environment for the new process (may be NULL).
    handle, stdin, stdout, stderr
		Spawned process info are returned in integers.
		When spawned process shares standard io stream with the parent
		process the respective value in handle, stdin, stdout, stderr
		is -1.
    params	A bitmask controling operation.
		Bit 1: Child & parent share standard input if this bit is set.
		Bit 2: Share standard output if bit is set.
		Bit 3: Share standard error if bit is set.

    Returns: zero return value indicates failure.
*/
static int C_fcall
C_process(const char *app, const char *cmdlin, const char **env,
	  int *phandle, int *pstdin_fd, int *pstdout_fd, int *pstderr_fd,
	  int params)
{
    int i;
    int success = TRUE;
    const int f_share_io[3] = { params & 1, params & 2, params & 4};
    int io_fds[3] = { -1, -1, -1 };
    HANDLE
	child_io_handles[3] = { NULL, NULL, NULL },
	standard_io_handles[3] = {
	    GetStdHandle(STD_INPUT_HANDLE),
	    GetStdHandle(STD_OUTPUT_HANDLE),
	    GetStdHandle(STD_ERROR_HANDLE)};
    const char modes[3] = "rww";
    HANDLE cur_process = GetCurrentProcess(), child_process = NULL;
    void* envblk = NULL;

    /****** create io handles & fds ***/

    for (i=0; i<3 && success; ++i)
    {
	if (f_share_io[i])
	{
	    success = DuplicateHandle(
		cur_process, standard_io_handles[i],
		cur_process, &child_io_handles[i],
		0, FALSE, DUPLICATE_SAME_ACCESS);
	}
	else
	{
	    HANDLE a, b;
	    success = CreatePipe(&a,&b,NULL,0);
	    if(success)
	    {
		HANDLE parent_end;
		if (modes[i]=='r') { child_io_handles[i]=a; parent_end=b; }
		else		   { parent_end=a; child_io_handles[i]=b; }
		success = (io_fds[i] = _open_osfhandle((C_word)parent_end,0)) >= 0;
                /* Make new handle inheritable */
		if (success)
		  success = SetHandleInformation(child_io_handles[i], HANDLE_FLAG_INHERIT, -1);
	    }
	}
    }

#if 0 /* Requires a sorted list by key! */
    /****** create environment block if necessary ****/

    if (env && success)
    {
	char** p;
	int len = 0;

	for (p = env; *p; ++p) len += strlen(*p) + 1;

	if (envblk = C_malloc(len + 1))
	{
	    char* pb = (char*)envblk;
	    for (p = env; *p; ++p)
	    {
		C_strlcpy(pb, *p, len+1);
		pb += strlen(*p) + 1;
	    }
	    *pb = '\0';
            /* This _should_ already have been checked for embedded NUL bytes */
	}
	else
	    success = FALSE;
    }
#endif

    /****** finally spawn process ****/

    if (success)
    {
	PROCESS_INFORMATION pi;
	STARTUPINFO si;

	ZeroMemory(&pi,sizeof pi);
	ZeroMemory(&si,sizeof si);
	si.cb = sizeof si;
	si.dwFlags = STARTF_USESTDHANDLES;
	si.hStdInput = child_io_handles[0];
	si.hStdOutput = child_io_handles[1];
	si.hStdError = child_io_handles[2];

	/* FIXME passing 'app' param causes failure & possible stack corruption */
	success = CreateProcess(
	    NULL, (char*)cmdlin, NULL, NULL, TRUE, 0, envblk, NULL, &si, &pi);

	if (success)
	{
	    child_process=pi.hProcess;
	    CloseHandle(pi.hThread);
	}
	else
	    set_last_errno();
    }
    else
	set_last_errno();

    /****** cleanup & return *********/

    /* parent must close child end */
    for (i=0; i<3; ++i) {
	if (child_io_handles[i] != NULL)
	    CloseHandle(child_io_handles[i]);
    }

    if (success)
    {
	*phandle = (C_word)child_process;
	*pstdin_fd = io_fds[0];
	*pstdout_fd = io_fds[1];
	*pstderr_fd = io_fds[2];
    }
    else
    {
	for (i=0; i<3; ++i) {
	    if (io_fds[i] != -1)
		_close(io_fds[i]);
	}
    }

    return success;
}

static int set_file_mtime(char *filename, C_word atime, C_word mtime)
{
  struct stat sb;
  struct _utimbuf tb;

  /* Only stat if needed */
  if (atime == C_SCHEME_FALSE || mtime == C_SCHEME_FALSE) {
    if (C_stat(filename, &sb) == -1) return -1;
  }

  if (atime == C_SCHEME_FALSE) {
    tb.actime = sb.st_atime;
  } else {
    tb.actime = C_num_to_int64(atime);
  }
  if (mtime == C_SCHEME_FALSE) {
    tb.modtime = sb.st_mtime;
  } else {
    tb.modtime = C_num_to_int64(mtime);
  }
  return _utime(filename, &tb);
}



static C_PTABLE_ENTRY *create_ptable(void);
C_noret_decl(C_scheduler_toplevel)
C_externimport void C_ccall C_scheduler_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_pathname_toplevel)
C_externimport void C_ccall C_pathname_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_extras_toplevel)
C_externimport void C_ccall C_extras_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_port_toplevel)
C_externimport void C_ccall C_port_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_lolevel_toplevel)
C_externimport void C_ccall C_lolevel_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_library_toplevel)
C_externimport void C_ccall C_library_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_data_2dstructures_toplevel)
C_externimport void C_ccall C_data_2dstructures_toplevel(C_word c,C_word *av) C_noret;

static C_TLS C_word lf[408];
static double C_possibly_force_alignment;
static C_char C_TLS li0[] C_aligned={C_lihdr(0,0,28),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,115,121,115,116,101,109,32,99,109,100,41,0,0,0,0};
static C_char C_TLS li1[] C_aligned={C_lihdr(0,0,29),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,115,121,115,116,101,109,42,32,115,116,114,41,0,0,0};
static C_char C_TLS li2[] C_aligned={C_lihdr(0,0,8),40,103,50,53,54,32,99,41};
static C_char C_TLS li3[] C_aligned={C_lihdr(0,0,18),40,109,97,112,45,108,111,111,112,50,53,48,32,103,50,54,50,41,0,0,0,0,0,0};
static C_char C_TLS li4[] C_aligned={C_lihdr(0,0,31),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,113,115,32,115,116,114,32,46,32,114,101,115,116,41,0};
static C_char C_TLS li5[] C_aligned={C_lihdr(0,0,47),40,99,104,105,99,107,101,110,46,112,111,115,105,120,35,112,111,115,105,120,45,101,114,114,111,114,32,116,121,112,101,32,108,111,99,32,109,115,103,32,46,32,97,114,103,115,41,0};
static C_char C_TLS li6[] C_aligned={C_lihdr(0,0,38),40,99,104,105,99,107,101,110,46,112,111,115,105,120,35,115,116,97,116,32,102,105,108,101,32,108,105,110,107,32,101,114,114,32,108,111,99,41,0,0};
static C_char C_TLS li7[] C_aligned={C_lihdr(0,0,39),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,115,116,97,116,32,102,32,46,32,114,101,115,116,41,0};
static C_char C_TLS li8[] C_aligned={C_lihdr(0,0,46),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,115,101,116,45,102,105,108,101,45,112,101,114,109,105,115,115,105,111,110,115,33,32,102,32,112,41,0,0};
static C_char C_TLS li9[] C_aligned={C_lihdr(0,0,45),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,109,111,100,105,102,105,99,97,116,105,111,110,45,116,105,109,101,32,102,41,0,0,0};
static C_char C_TLS li10[] C_aligned={C_lihdr(0,0,39),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,97,99,99,101,115,115,45,116,105,109,101,32,102,41,0};
static C_char C_TLS li11[] C_aligned={C_lihdr(0,0,39),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,99,104,97,110,103,101,45,116,105,109,101,32,102,41,0};
static C_char C_TLS li12[] C_aligned={C_lihdr(0,0,45),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,115,101,116,45,102,105,108,101,45,116,105,109,101,115,33,32,102,32,46,32,114,101,115,116,41,0,0,0};
static C_char C_TLS li13[] C_aligned={C_lihdr(0,0,32),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,115,105,122,101,32,102,41};
static C_char C_TLS li14[] C_aligned={C_lihdr(0,0,42),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,115,101,116,45,102,105,108,101,45,111,119,110,101,114,33,32,102,32,117,105,100,41,0,0,0,0,0,0};
static C_char C_TLS li15[] C_aligned={C_lihdr(0,0,42),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,115,101,116,45,102,105,108,101,45,103,114,111,117,112,33,32,102,32,103,105,100,41,0,0,0,0,0,0};
static C_char C_TLS li16[] C_aligned={C_lihdr(0,0,42),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,116,121,112,101,32,102,105,108,101,32,46,32,114,101,115,116,41,0,0,0,0,0,0};
static C_char C_TLS li17[] C_aligned={C_lihdr(0,0,39),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,114,101,103,117,108,97,114,45,102,105,108,101,63,32,102,105,108,101,41,0};
static C_char C_TLS li18[] C_aligned={C_lihdr(0,0,40),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,115,121,109,98,111,108,105,99,45,108,105,110,107,63,32,102,105,108,101,41};
static C_char C_TLS li19[] C_aligned={C_lihdr(0,0,39),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,98,108,111,99,107,45,100,101,118,105,99,101,63,32,102,105,108,101,41,0};
static C_char C_TLS li20[] C_aligned={C_lihdr(0,0,43),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,99,104,97,114,97,99,116,101,114,45,100,101,118,105,99,101,63,32,102,105,108,101,41,0,0,0,0,0};
static C_char C_TLS li21[] C_aligned={C_lihdr(0,0,31),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,102,111,63,32,102,105,108,101,41,0};
static C_char C_TLS li22[] C_aligned={C_lihdr(0,0,33),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,115,111,99,107,101,116,63,32,102,105,108,101,41,0,0,0,0,0,0,0};
static C_char C_TLS li23[] C_aligned={C_lihdr(0,0,36),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,100,105,114,101,99,116,111,114,121,63,32,102,105,108,101,41,0,0,0,0};
static C_char C_TLS li24[] C_aligned={C_lihdr(0,0,57),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,115,101,116,45,102,105,108,101,45,112,111,115,105,116,105,111,110,33,32,112,111,114,116,32,112,111,115,32,46,32,119,104,101,110,99,101,41,0,0,0,0,0,0,0};
static C_char C_TLS li25[] C_aligned={C_lihdr(0,0,16),40,109,111,100,101,32,105,110,112,32,109,32,108,111,99,41};
static C_char C_TLS li26[] C_aligned={C_lihdr(0,0,20),40,99,104,101,99,107,32,108,111,99,32,102,100,32,105,110,112,32,114,41,0,0,0,0};
static C_char C_TLS li27[] C_aligned={C_lihdr(0,0,44),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,111,112,101,110,45,105,110,112,117,116,45,102,105,108,101,42,32,102,100,32,46,32,109,41,0,0,0,0};
static C_char C_TLS li28[] C_aligned={C_lihdr(0,0,45),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,111,112,101,110,45,111,117,116,112,117,116,45,102,105,108,101,42,32,102,100,32,46,32,109,41,0,0,0};
static C_char C_TLS li29[] C_aligned={C_lihdr(0,0,38),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,112,111,114,116,45,62,102,105,108,101,110,111,32,112,111,114,116,41,0,0};
static C_char C_TLS li30[] C_aligned={C_lihdr(0,0,47),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,100,117,112,108,105,99,97,116,101,45,102,105,108,101,110,111,32,111,108,100,32,46,32,110,101,119,41,0};
static C_char C_TLS li31[] C_aligned={C_lihdr(0,0,50),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,45,99,111,110,116,101,120,116,46,112,111,115,105,120,35,99,117,114,114,101,110,116,45,112,114,111,99,101,115,115,45,105,100,41,0,0,0,0,0,0};
static C_char C_TLS li32[] C_aligned={C_lihdr(0,0,52),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,45,99,111,110,116,101,120,116,46,112,111,115,105,120,35,99,104,97,110,103,101,45,100,105,114,101,99,116,111,114,121,42,32,102,100,41,0,0,0,0};
static C_char C_TLS li33[] C_aligned={C_lihdr(0,0,33),40,35,35,115,121,115,35,99,104,97,110,103,101,45,100,105,114,101,99,116,111,114,121,45,104,111,111,107,32,100,105,114,41,0,0,0,0,0,0,0};
static C_char C_TLS li34[] C_aligned={C_lihdr(0,0,16),67,95,100,101,99,111,100,101,95,115,101,99,111,110,100,115};
static C_char C_TLS li35[] C_aligned={C_lihdr(0,0,40),40,99,104,105,99,107,101,110,46,112,111,115,105,120,35,99,104,101,99,107,45,116,105,109,101,45,118,101,99,116,111,114,32,108,111,99,32,116,109,41};
static C_char C_TLS li36[] C_aligned={C_lihdr(0,0,47),40,99,104,105,99,107,101,110,46,116,105,109,101,46,112,111,115,105,120,35,115,101,99,111,110,100,115,45,62,108,111,99,97,108,45,116,105,109,101,32,46,32,114,101,115,116,41,0};
static C_char C_TLS li37[] C_aligned={C_lihdr(0,0,45),40,99,104,105,99,107,101,110,46,116,105,109,101,46,112,111,115,105,120,35,115,101,99,111,110,100,115,45,62,117,116,99,45,116,105,109,101,32,46,32,114,101,115,116,41,0,0,0};
static C_char C_TLS li38[] C_aligned={C_lihdr(0,0,43),40,99,104,105,99,107,101,110,46,116,105,109,101,46,112,111,115,105,120,35,115,101,99,111,110,100,115,45,62,115,116,114,105,110,103,32,46,32,114,101,115,116,41,0,0,0,0,0};
static C_char C_TLS li39[] C_aligned={C_lihdr(0,0,43),40,99,104,105,99,107,101,110,46,116,105,109,101,46,112,111,115,105,120,35,108,111,99,97,108,45,116,105,109,101,45,62,115,101,99,111,110,100,115,32,116,109,41,0,0,0,0,0};
static C_char C_TLS li40[] C_aligned={C_lihdr(0,0,43),40,99,104,105,99,107,101,110,46,116,105,109,101,46,112,111,115,105,120,35,116,105,109,101,45,62,115,116,114,105,110,103,32,116,109,32,46,32,114,101,115,116,41,0,0,0,0,0};
static C_char C_TLS li41[] C_aligned={C_lihdr(0,0,53),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,46,115,105,103,110,97,108,35,115,101,116,45,115,105,103,110,97,108,45,104,97,110,100,108,101,114,33,32,115,105,103,32,112,114,111,99,41,0,0,0};
static C_char C_TLS li42[] C_aligned={C_lihdr(0,0,33),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,112,114,111,99,101,115,115,45,115,108,101,101,112,32,110,41,0,0,0,0,0,0,0};
static C_char C_TLS li43[] C_aligned={C_lihdr(0,0,7),40,97,51,53,48,51,41,0};
static C_char C_TLS li44[] C_aligned={C_lihdr(0,0,24),40,97,51,53,48,57,32,101,112,105,100,32,101,110,111,114,109,32,101,99,111,100,101,41};
static C_char C_TLS li45[] C_aligned={C_lihdr(0,0,37),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,112,114,111,99,101,115,115,45,119,97,105,116,32,46,32,97,114,103,115,41,0,0,0};
static C_char C_TLS li46[] C_aligned={C_lihdr(0,0,7),40,97,51,53,55,56,41,0};
static C_char C_TLS li47[] C_aligned={C_lihdr(0,0,11),40,97,51,53,55,50,32,101,120,110,41,0,0,0,0,0};
static C_char C_TLS li48[] C_aligned={C_lihdr(0,0,16),40,100,111,108,111,111,112,57,57,51,32,115,108,32,105,41};
static C_char C_TLS li49[] C_aligned={C_lihdr(0,0,10),40,116,109,112,49,50,49,55,55,41,0,0,0,0,0,0};
static C_char C_TLS li50[] C_aligned={C_lihdr(0,0,7),40,97,51,54,53,50,41,0};
static C_char C_TLS li51[] C_aligned={C_lihdr(0,0,15),40,116,109,112,50,50,49,55,56,32,97,114,103,115,41,0};
static C_char C_TLS li52[] C_aligned={C_lihdr(0,0,7),40,97,51,53,56,55,41,0};
static C_char C_TLS li53[] C_aligned={C_lihdr(0,0,9),40,97,51,53,54,54,32,107,41,0,0,0,0,0,0,0};
static C_char C_TLS li54[] C_aligned={C_lihdr(0,0,61),40,99,104,105,99,107,101,110,46,112,111,115,105,120,35,108,105,115,116,45,62,99,45,115,116,114,105,110,103,45,98,117,102,102,101,114,32,115,116,114,105,110,103,45,108,105,115,116,32,99,111,110,118,101,114,116,32,108,111,99,41,0,0,0};
static C_char C_TLS li55[] C_aligned={C_lihdr(0,0,14),40,100,111,108,111,111,112,49,48,49,50,32,105,41,0,0};
static C_char C_TLS li56[] C_aligned={C_lihdr(0,0,49),40,99,104,105,99,107,101,110,46,112,111,115,105,120,35,102,114,101,101,45,99,45,115,116,114,105,110,103,45,98,117,102,102,101,114,32,98,117,102,102,101,114,45,97,114,114,97,121,41,0,0,0,0,0,0,0};
static C_char C_TLS li57[] C_aligned={C_lihdr(0,0,7),40,103,49,48,50,53,41,0};
static C_char C_TLS li58[] C_aligned={C_lihdr(0,0,19),40,102,111,114,45,101,97,99,104,45,108,111,111,112,49,48,50,52,41,0,0,0,0,0};
static C_char C_TLS li59[] C_aligned={C_lihdr(0,0,46),40,99,104,105,99,107,101,110,46,112,111,115,105,120,35,99,104,101,99,107,45,101,110,118,105,114,111,110,109,101,110,116,45,108,105,115,116,32,108,115,116,32,108,111,99,41,0,0};
static C_char C_TLS li60[] C_aligned={C_lihdr(0,0,7),40,110,111,112,32,120,41,0};
static C_char C_TLS li61[] C_aligned={C_lihdr(0,0,7),40,97,51,55,56,48,41,0};
static C_char C_TLS li62[] C_aligned={C_lihdr(0,0,11),40,97,51,55,55,52,32,101,120,110,41,0,0,0,0,0};
static C_char C_TLS li63[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,49,48,54,56,32,103,49,48,56,48,41,0,0,0,0};
static C_char C_TLS li64[] C_aligned={C_lihdr(0,0,7),40,97,51,56,48,49,41,0};
static C_char C_TLS li65[] C_aligned={C_lihdr(0,0,7),40,97,51,56,56,50,41,0};
static C_char C_TLS li66[] C_aligned={C_lihdr(0,0,14),40,97,51,56,55,54,32,46,32,97,114,103,115,41,0,0};
static C_char C_TLS li67[] C_aligned={C_lihdr(0,0,7),40,97,51,55,57,53,41,0};
static C_char C_TLS li68[] C_aligned={C_lihdr(0,0,9),40,97,51,55,54,56,32,107,41,0,0,0,0,0,0,0};
static C_char C_TLS li69[] C_aligned={C_lihdr(0,0,77),40,99,104,105,99,107,101,110,46,112,111,115,105,120,35,99,97,108,108,45,119,105,116,104,45,101,120,101,99,45,97,114,103,115,32,108,111,99,32,102,105,108,101,110,97,109,101,32,97,114,103,99,111,110,118,32,97,114,103,108,105,115,116,32,101,110,118,108,105,115,116,32,112,114,111,99,41,0,0,0};
static C_char C_TLS li70[] C_aligned={C_lihdr(0,0,21),40,99,104,101,99,107,32,108,111,99,32,99,109,100,32,105,110,112,32,114,41,0,0,0};
static C_char C_TLS li71[] C_aligned={C_lihdr(0,0,41),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,111,112,101,110,45,105,110,112,117,116,45,112,105,112,101,32,99,109,100,32,46,32,109,41,0,0,0,0,0,0,0};
static C_char C_TLS li72[] C_aligned={C_lihdr(0,0,42),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,111,112,101,110,45,111,117,116,112,117,116,45,112,105,112,101,32,99,109,100,32,46,32,109,41,0,0,0,0,0,0};
static C_char C_TLS li73[] C_aligned={C_lihdr(0,0,39),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,99,108,111,115,101,45,105,110,112,117,116,45,112,105,112,101,32,112,111,114,116,41,0};
static C_char C_TLS li74[] C_aligned={C_lihdr(0,0,40),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,99,108,111,115,101,45,111,117,116,112,117,116,45,112,105,112,101,32,112,111,114,116,41};
static C_char C_TLS li75[] C_aligned={C_lihdr(0,0,7),40,97,52,48,52,53,41,0};
static C_char C_TLS li76[] C_aligned={C_lihdr(0,0,17),40,97,52,48,53,54,32,46,32,114,101,115,117,108,116,115,41,0,0,0,0,0,0,0};
static C_char C_TLS li77[] C_aligned={C_lihdr(0,0,7),40,97,52,48,53,48,41,0};
static C_char C_TLS li78[] C_aligned={C_lihdr(0,0,7),40,97,52,48,54,53,41,0};
static C_char C_TLS li79[] C_aligned={C_lihdr(0,0,55),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,119,105,116,104,45,105,110,112,117,116,45,102,114,111,109,45,112,105,112,101,32,99,109,100,32,116,104,117,110,107,32,46,32,109,111,100,101,41,0};
static C_char C_TLS li80[] C_aligned={C_lihdr(0,0,7),40,97,52,48,55,57,41,0};
static C_char C_TLS li81[] C_aligned={C_lihdr(0,0,17),40,97,52,48,56,53,32,46,32,114,101,115,117,108,116,115,41,0,0,0,0,0,0,0};
static C_char C_TLS li82[] C_aligned={C_lihdr(0,0,55),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,99,97,108,108,45,119,105,116,104,45,111,117,116,112,117,116,45,112,105,112,101,32,99,109,100,32,112,114,111,99,32,46,32,109,111,100,101,41,0};
static C_char C_TLS li83[] C_aligned={C_lihdr(0,0,7),40,97,52,49,48,51,41,0};
static C_char C_TLS li84[] C_aligned={C_lihdr(0,0,17),40,97,52,49,48,57,32,46,32,114,101,115,117,108,116,115,41,0,0,0,0,0,0,0};
static C_char C_TLS li85[] C_aligned={C_lihdr(0,0,54),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,99,97,108,108,45,119,105,116,104,45,105,110,112,117,116,45,112,105,112,101,32,99,109,100,32,112,114,111,99,32,46,32,109,111,100,101,41,0,0};
static C_char C_TLS li86[] C_aligned={C_lihdr(0,0,7),40,97,52,49,50,55,41,0};
static C_char C_TLS li87[] C_aligned={C_lihdr(0,0,17),40,97,52,49,51,56,32,46,32,114,101,115,117,108,116,115,41,0,0,0,0,0,0,0};
static C_char C_TLS li88[] C_aligned={C_lihdr(0,0,7),40,97,52,49,51,50,41,0};
static C_char C_TLS li89[] C_aligned={C_lihdr(0,0,7),40,97,52,49,52,55,41,0};
static C_char C_TLS li90[] C_aligned={C_lihdr(0,0,54),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,119,105,116,104,45,111,117,116,112,117,116,45,116,111,45,112,105,112,101,32,99,109,100,32,116,104,117,110,107,32,46,32,109,111,100,101,41,0,0};
static C_char C_TLS li91[] C_aligned={C_lihdr(0,0,52),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,111,112,101,110,32,102,105,108,101,110,97,109,101,32,102,108,97,103,115,32,46,32,109,111,100,101,41,0,0,0,0};
static C_char C_TLS li92[] C_aligned={C_lihdr(0,0,6),40,108,111,111,112,41,0,0};
static C_char C_TLS li93[] C_aligned={C_lihdr(0,0,34),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,99,108,111,115,101,32,102,100,41,0,0,0,0,0,0};
static C_char C_TLS li94[] C_aligned={C_lihdr(0,0,47),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,114,101,97,100,32,102,100,32,115,105,122,101,32,46,32,98,117,102,102,101,114,41,0};
static C_char C_TLS li95[] C_aligned={C_lihdr(0,0,48),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,119,114,105,116,101,32,102,100,32,98,117,102,102,101,114,32,46,32,115,105,122,101,41};
static C_char C_TLS li96[] C_aligned={C_lihdr(0,0,6),40,108,111,111,112,41,0,0};
static C_char C_TLS li97[] C_aligned={C_lihdr(0,0,13),40,115,117,102,102,105,120,45,108,111,111,112,41,0,0,0};
static C_char C_TLS li98[] C_aligned={C_lihdr(0,0,8),40,118,97,54,48,56,51,41};
static C_char C_TLS li99[] C_aligned={C_lihdr(0,0,12),40,108,111,111,112,32,99,111,117,110,116,41,0,0,0,0};
static C_char C_TLS li100[] C_aligned={C_lihdr(0,0,8),40,118,97,54,48,57,49,41};
static C_char C_TLS li101[] C_aligned={C_lihdr(0,0,42),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,109,107,115,116,101,109,112,32,116,101,109,112,108,97,116,101,41,0,0,0,0,0,0};
static C_char C_TLS li102[] C_aligned={C_lihdr(0,0,36),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,99,114,101,97,116,101,45,112,105,112,101,32,46,32,114,101,115,116,41,0,0,0,0};
static C_char C_TLS li103[] C_aligned={C_lihdr(0,0,42),40,99,104,105,99,107,101,110,46,112,111,115,105,120,35,100,117,112,108,105,99,97,116,101,45,102,105,108,101,110,111,32,111,108,100,32,46,32,110,101,119,41,0,0,0,0,0,0};
static C_char C_TLS li104[] C_aligned={C_lihdr(0,0,48),40,99,104,105,99,107,101,110,46,116,105,109,101,46,112,111,115,105,120,35,108,111,99,97,108,45,116,105,109,101,122,111,110,101,45,97,98,98,114,101,118,105,97,116,105,111,110,41};
static C_char C_TLS li105[] C_aligned={C_lihdr(0,0,6),40,108,111,111,112,41,0,0};
static C_char C_TLS li106[] C_aligned={C_lihdr(0,0,36),40,99,104,105,99,107,101,110,46,112,111,115,105,120,35,113,117,111,116,101,45,97,114,103,45,115,116,114,105,110,103,32,115,116,114,41,0,0,0,0};
static C_char C_TLS li107[] C_aligned={C_lihdr(0,0,10),40,102,95,52,54,53,56,32,120,41,0,0,0,0,0,0};
static C_char C_TLS li108[] C_aligned={C_lihdr(0,0,25),40,97,52,54,52,51,32,112,114,103,32,97,114,103,98,117,102,32,101,110,118,98,117,102,41,0,0,0,0,0,0,0};
static C_char C_TLS li109[] C_aligned={C_lihdr(0,0,49),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,112,114,111,99,101,115,115,45,101,120,101,99,117,116,101,32,102,105,108,101,110,97,109,101,32,46,32,114,101,115,116,41,0,0,0,0,0,0,0};
static C_char C_TLS li110[] C_aligned={C_lihdr(0,0,10),40,102,95,52,55,52,49,32,120,41,0,0,0,0,0,0};
static C_char C_TLS li111[] C_aligned={C_lihdr(0,0,25),40,97,52,55,50,54,32,112,114,103,32,97,114,103,98,117,102,32,101,110,118,98,117,102,41,0,0,0,0,0,0,0};
static C_char C_TLS li112[] C_aligned={C_lihdr(0,0,52),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,112,114,111,99,101,115,115,45,115,112,97,119,110,32,109,111,100,101,32,102,105,108,101,110,97,109,101,32,46,32,114,101,115,116,41,0,0,0,0};
static C_char C_TLS li113[] C_aligned={C_lihdr(0,0,33),40,99,104,105,99,107,101,110,46,112,111,115,105,120,35,115,104,101,108,108,45,99,111,109,109,97,110,100,32,108,111,99,41,0,0,0,0,0,0,0};
static C_char C_TLS li114[] C_aligned={C_lihdr(0,0,38),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,112,114,111,99,101,115,115,45,114,117,110,32,102,32,46,32,97,114,103,115,41,0,0};
static C_char C_TLS li115[] C_aligned={C_lihdr(0,0,7),40,103,49,53,56,49,41,0};
static C_char C_TLS li116[] C_aligned={C_lihdr(0,0,19),40,102,111,114,45,101,97,99,104,45,108,111,111,112,49,53,56,48,41,0,0,0,0,0};
static C_char C_TLS li117[] C_aligned={C_lihdr(0,0,15),40,99,104,107,115,116,114,108,115,116,32,108,115,116,41,0};
static C_char C_TLS li118[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,49,53,48,52,32,103,49,53,49,54,41,0,0,0,0};
static C_char C_TLS li119[] C_aligned={C_lihdr(0,0,7),40,97,53,48,56,48,41,0};
static C_char C_TLS li120[] C_aligned={C_lihdr(0,0,22),40,97,53,48,56,54,32,105,110,32,111,117,116,32,112,105,100,32,101,114,114,41,0,0};
static C_char C_TLS li121[] C_aligned={C_lihdr(0,0,39),40,37,112,114,111,99,101,115,115,32,108,111,99,32,101,114,114,63,32,99,109,100,32,97,114,103,115,32,101,110,118,32,101,120,97,99,116,102,41,0};
static C_char C_TLS li122[] C_aligned={C_lihdr(0,0,36),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,112,114,111,99,101,115,115,32,99,109,100,32,46,32,114,101,115,116,41,0,0,0,0};
static C_char C_TLS li123[] C_aligned={C_lihdr(0,0,37),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,112,114,111,99,101,115,115,42,32,99,109,100,32,46,32,114,101,115,116,41,0,0,0};
static C_char C_TLS li124[] C_aligned={C_lihdr(0,0,49),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,45,99,111,110,116,101,120,116,46,112,111,115,105,120,35,99,117,114,114,101,110,116,45,117,115,101,114,45,110,97,109,101,41,0,0,0,0,0,0,0};
static C_char C_TLS li125[] C_aligned={C_lihdr(0,0,36),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,99,114,101,97,116,101,45,102,105,102,111,32,46,32,95,41,0,0,0,0};
static C_char C_TLS li126[] C_aligned={C_lihdr(0,0,50),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,45,99,111,110,116,101,120,116,46,112,111,115,105,120,35,99,114,101,97,116,101,45,115,101,115,115,105,111,110,32,46,32,95,41,0,0,0,0,0,0};
static C_char C_TLS li127[] C_aligned={C_lihdr(0,0,45),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,99,114,101,97,116,101,45,115,121,109,98,111,108,105,99,45,108,105,110,107,32,46,32,95,41,0,0,0};
static C_char C_TLS li128[] C_aligned={C_lihdr(0,0,62),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,45,99,111,110,116,101,120,116,46,112,111,115,105,120,35,99,117,114,114,101,110,116,45,101,102,102,101,99,116,105,118,101,45,103,114,111,117,112,45,105,100,32,46,32,95,41,0,0};
static C_char C_TLS li129[] C_aligned={C_lihdr(0,0,61),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,45,99,111,110,116,101,120,116,46,112,111,115,105,120,35,99,117,114,114,101,110,116,45,101,102,102,101,99,116,105,118,101,45,117,115,101,114,45,105,100,32,46,32,95,41,0,0,0};
static C_char C_TLS li130[] C_aligned={C_lihdr(0,0,63),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,45,99,111,110,116,101,120,116,46,112,111,115,105,120,35,99,117,114,114,101,110,116,45,101,102,102,101,99,116,105,118,101,45,117,115,101,114,45,110,97,109,101,32,46,32,95,41,0};
static C_char C_TLS li131[] C_aligned={C_lihdr(0,0,52),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,45,99,111,110,116,101,120,116,46,112,111,115,105,120,35,99,117,114,114,101,110,116,45,103,114,111,117,112,45,105,100,32,46,32,95,41,0,0,0,0};
static C_char C_TLS li132[] C_aligned={C_lihdr(0,0,51),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,45,99,111,110,116,101,120,116,46,112,111,115,105,120,35,99,117,114,114,101,110,116,45,117,115,101,114,45,105,100,32,46,32,95,41,0,0,0,0,0};
static C_char C_TLS li133[] C_aligned={C_lihdr(0,0,37),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,99,111,110,116,114,111,108,32,46,32,95,41,0,0,0};
static C_char C_TLS li134[] C_aligned={C_lihdr(0,0,34),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,108,105,110,107,32,46,32,95,41,0,0,0,0,0,0};
static C_char C_TLS li135[] C_aligned={C_lihdr(0,0,34),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,108,111,99,107,32,46,32,95,41,0,0,0,0,0,0};
static C_char C_TLS li136[] C_aligned={C_lihdr(0,0,43),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,108,111,99,107,47,98,108,111,99,107,105,110,103,32,46,32,95,41,0,0,0,0,0};
static C_char C_TLS li137[] C_aligned={C_lihdr(0,0,36),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,115,101,108,101,99,116,32,46,32,95,41,0,0,0,0};
static C_char C_TLS li138[] C_aligned={C_lihdr(0,0,39),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,116,101,115,116,45,108,111,99,107,32,46,32,95,41,0};
static C_char C_TLS li139[] C_aligned={C_lihdr(0,0,38),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,116,114,117,110,99,97,116,101,32,46,32,95,41,0,0};
static C_char C_TLS li140[] C_aligned={C_lihdr(0,0,36),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,102,105,108,101,45,117,110,108,111,99,107,32,46,32,95,41,0,0,0,0};
static C_char C_TLS li141[] C_aligned={C_lihdr(0,0,53),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,45,99,111,110,116,101,120,116,46,112,111,115,105,120,35,112,97,114,101,110,116,45,112,114,111,99,101,115,115,45,105,100,32,46,32,95,41,0,0,0};
static C_char C_TLS li142[] C_aligned={C_lihdr(0,0,34),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,112,114,111,99,101,115,115,45,102,111,114,107,32,46,32,95,41,0,0,0,0,0,0};
static C_char C_TLS li143[] C_aligned={C_lihdr(0,0,52),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,45,99,111,110,116,101,120,116,46,112,111,115,105,120,35,112,114,111,99,101,115,115,45,103,114,111,117,112,45,105,100,32,46,32,95,41,0,0,0,0};
static C_char C_TLS li144[] C_aligned={C_lihdr(0,0,36),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,35,112,114,111,99,101,115,115,45,115,105,103,110,97,108,32,46,32,95,41,0,0,0,0};
static C_char C_TLS li145[] C_aligned={C_lihdr(0,0,43),40,99,104,105,99,107,101,110,46,102,105,108,101,46,112,111,115,105,120,35,114,101,97,100,45,115,121,109,98,111,108,105,99,45,108,105,110,107,32,46,32,95,41,0,0,0,0,0};
static C_char C_TLS li146[] C_aligned={C_lihdr(0,0,39),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,46,115,105,103,110,97,108,35,115,101,116,45,97,108,97,114,109,33,32,46,32,95,41,0};
static C_char C_TLS li147[] C_aligned={C_lihdr(0,0,55),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,45,99,111,110,116,101,120,116,46,112,111,115,105,120,35,115,101,116,45,114,111,111,116,45,100,105,114,101,99,116,111,114,121,33,32,46,32,95,41,0};
static C_char C_TLS li148[] C_aligned={C_lihdr(0,0,45),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,46,115,105,103,110,97,108,35,115,101,116,45,115,105,103,110,97,108,45,109,97,115,107,33,32,46,32,95,41,0,0,0};
static C_char C_TLS li149[] C_aligned={C_lihdr(0,0,40),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,46,115,105,103,110,97,108,35,115,105,103,110,97,108,45,109,97,115,107,32,46,32,95,41};
static C_char C_TLS li150[] C_aligned={C_lihdr(0,0,41),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,46,115,105,103,110,97,108,35,115,105,103,110,97,108,45,109,97,115,107,33,32,46,32,95,41,0,0,0,0,0,0,0};
static C_char C_TLS li151[] C_aligned={C_lihdr(0,0,43),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,46,115,105,103,110,97,108,35,115,105,103,110,97,108,45,109,97,115,107,101,100,63,32,46,32,95,41,0,0,0,0,0};
static C_char C_TLS li152[] C_aligned={C_lihdr(0,0,43),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,46,115,105,103,110,97,108,35,115,105,103,110,97,108,45,117,110,109,97,115,107,33,32,46,32,95,41,0,0,0,0,0};
static C_char C_TLS li153[] C_aligned={C_lihdr(0,0,52),40,99,104,105,99,107,101,110,46,112,114,111,99,101,115,115,45,99,111,110,116,101,120,116,46,112,111,115,105,120,35,117,115,101,114,45,105,110,102,111,114,109,97,116,105,111,110,32,46,32,95,41,0,0,0,0};
static C_char C_TLS li154[] C_aligned={C_lihdr(0,0,42),40,99,104,105,99,107,101,110,46,116,105,109,101,46,112,111,115,105,120,35,117,116,99,45,116,105,109,101,45,62,115,101,99,111,110,100,115,32,46,32,95,41,0,0,0,0,0,0};
static C_char C_TLS li155[] C_aligned={C_lihdr(0,0,37),40,99,104,105,99,107,101,110,46,116,105,109,101,46,112,111,115,105,120,35,115,116,114,105,110,103,45,62,116,105,109,101,32,46,32,95,41,0,0,0};
static C_char C_TLS li156[] C_aligned={C_lihdr(0,0,21),40,99,104,105,99,107,101,110,46,101,114,114,110,111,35,101,114,114,110,111,41,0,0,0};
static C_char C_TLS li157[] C_aligned={C_lihdr(0,0,11),40,97,53,53,48,57,32,115,105,103,41,0,0,0,0,0};
static C_char C_TLS li158[] C_aligned={C_lihdr(0,0,14),40,97,53,53,49,56,32,46,32,114,101,115,116,41,0,0};
static C_char C_TLS li159[] C_aligned={C_lihdr(0,0,10),40,97,53,53,52,51,32,117,109,41,0,0,0,0,0,0};
static C_char C_TLS li160[] C_aligned={C_lihdr(0,0,12),40,97,53,53,52,57,32,112,111,114,116,41,0,0,0,0};
static C_char C_TLS li161[] C_aligned={C_lihdr(0,0,9),40,97,53,53,56,54,32,102,41,0,0,0,0,0,0,0};
static C_char C_TLS li162[] C_aligned={C_lihdr(0,0,9),40,97,53,53,57,50,32,102,41,0,0,0,0,0,0,0};
static C_char C_TLS li163[] C_aligned={C_lihdr(0,0,9),40,97,53,53,57,56,32,102,41,0,0,0,0,0,0,0};
static C_char C_TLS li164[] C_aligned={C_lihdr(0,0,10),40,116,111,112,108,101,118,101,108,41,0,0,0,0,0,0};


/* from k4865 */
C_regparm static C_word C_fcall stub1459(C_word C_buf,C_word C_a0,C_word C_a1,C_word C_a2,C_word C_a3,C_word C_a4,C_word C_a5,C_word C_a6,C_word C_a7){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
char * t0=(char * )C_string_or_null(C_a0);
char * t1=(char * )C_string_or_null(C_a1);
void * t2=(void * )C_c_pointer_or_null(C_a2);
int *t3=(int *)C_c_pointer_or_null(C_a3);
int *t4=(int *)C_c_pointer_or_null(C_a4);
int *t5=(int *)C_c_pointer_or_null(C_a5);
int *t6=(int *)C_c_pointer_or_null(C_a6);
int t7=(int )C_unfix(C_a7);
C_r=C_mk_bool(C_process(t0,t1,t2,t3,t4,t5,t6,t7));
return C_r;}

#define return(x) C_cblock C_r = (C_mpointer(&C_a,(void*)(x))); goto C_ret; C_cblockend
C_regparm static C_word C_fcall stub1370(C_word C_buf){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
char *z = (_daylight ? _tzname[1] : _tzname[0]);

C_return(z);
C_ret:
#undef return

return C_r;}

#define return(x) C_cblock C_r = (C_mpointer_or_false(&C_a,(void*)(x))); goto C_ret; C_cblockend
C_regparm static C_word C_fcall stub976(C_word C_buf,C_word C_a0){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
C_word o=(C_word )(C_a0);
char *ptr = C_malloc(C_header_size(o)); 

if (ptr != NULL) {

  C_memcpy(ptr, C_data_pointer(o), C_header_size(o)); 

}

C_return(ptr);
C_ret:
#undef return

return C_r;}

/* from k3389 */
C_regparm static C_word C_fcall stub919(C_word C_buf,C_word C_a0,C_word C_a1,C_word C_a2){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
C_word t0=(C_word )(C_a0);
C_word t1=(C_word )(C_a1);
void * t2=(void * )C_data_pointer_or_null(C_a2);
C_r=C_mpointer(&C_a,(void*)C_strftime(t0,t1,t2));
return C_r;}

/* from k3376 */
C_regparm static C_word C_fcall stub909(C_word C_buf,C_word C_a0,C_word C_a1){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
C_word t0=(C_word )(C_a0);
void * t1=(void * )C_data_pointer_or_null(C_a1);
C_r=C_mpointer(&C_a,(void*)C_asctime(t0,t1));
return C_r;}

/* from k3308 */
C_regparm static C_word C_fcall stub882(C_word C_buf,C_word C_a0){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int t0=(int )C_num_to_int(C_a0);
C_r=C_mpointer(&C_a,(void*)C_ctime(t0));
return C_r;}

/* from chicken.process-context.posix#current-process-id in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
C_regparm static C_word C_fcall stub826(C_word C_buf){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
C_r=C_fix((C_word)C_getpid());
return C_r;}

/* from k2719 in k2713 in k2710 in k2698 in chicken.file.posix#set-file-times! in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
C_regparm static C_word C_fcall stub707(C_word C_buf,C_word C_a0,C_word C_a1,C_word C_a2){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
char * t0=(char * )C_string_or_null(C_a0);
C_word t1=(C_word )(C_a1);
C_word t2=(C_word )(C_a2);
C_r=C_fix((C_word)set_file_mtime(t0,t1,t2));
return C_r;}

/* from k2549 */
C_regparm static C_word C_fcall stub633(C_word C_buf,C_word C_a0){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int t0=(int )C_unfix(C_a0);
C_r=C_mpointer(&C_a,(void*)strerror(t0));
return C_r;}

C_noret_decl(f6102)
static void C_ccall f6102(C_word c,C_word *av) C_noret;
C_noret_decl(f6106)
static void C_ccall f6106(C_word c,C_word *av) C_noret;
C_noret_decl(f_2227)
static void C_ccall f_2227(C_word c,C_word *av) C_noret;
C_noret_decl(f_2230)
static void C_ccall f_2230(C_word c,C_word *av) C_noret;
C_noret_decl(f_2233)
static void C_ccall f_2233(C_word c,C_word *av) C_noret;
C_noret_decl(f_2236)
static void C_ccall f_2236(C_word c,C_word *av) C_noret;
C_noret_decl(f_2239)
static void C_ccall f_2239(C_word c,C_word *av) C_noret;
C_noret_decl(f_2242)
static void C_ccall f_2242(C_word c,C_word *av) C_noret;
C_noret_decl(f_2245)
static void C_ccall f_2245(C_word c,C_word *av) C_noret;
C_noret_decl(f_2331)
static void C_ccall f_2331(C_word c,C_word *av) C_noret;
C_noret_decl(f_2344)
static void C_ccall f_2344(C_word c,C_word *av) C_noret;
C_noret_decl(f_2349)
static void C_ccall f_2349(C_word c,C_word *av) C_noret;
C_noret_decl(f_2353)
static void C_ccall f_2353(C_word c,C_word *av) C_noret;
C_noret_decl(f_2365)
static void C_ccall f_2365(C_word c,C_word *av) C_noret;
C_noret_decl(f_2369)
static void C_ccall f_2369(C_word c,C_word *av) C_noret;
C_noret_decl(f_2379)
static void C_fcall f_2379(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_2400)
static void C_ccall f_2400(C_word c,C_word *av) C_noret;
C_noret_decl(f_2403)
static void C_ccall f_2403(C_word c,C_word *av) C_noret;
C_noret_decl(f_2414)
static void C_ccall f_2414(C_word c,C_word *av) C_noret;
C_noret_decl(f_2420)
static void C_fcall f_2420(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_2445)
static void C_ccall f_2445(C_word c,C_word *av) C_noret;
C_noret_decl(f_2552)
static void C_ccall f_2552(C_word c,C_word *av) C_noret;
C_noret_decl(f_2556)
static void C_ccall f_2556(C_word c,C_word *av) C_noret;
C_noret_decl(f_2563)
static void C_ccall f_2563(C_word c,C_word *av) C_noret;
C_noret_decl(f_2567)
static void C_ccall f_2567(C_word c,C_word *av) C_noret;
C_noret_decl(f_2570)
static void C_fcall f_2570(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_2574)
static void C_ccall f_2574(C_word c,C_word *av) C_noret;
C_noret_decl(f_2595)
static void C_ccall f_2595(C_word c,C_word *av) C_noret;
C_noret_decl(f_2599)
static void C_ccall f_2599(C_word c,C_word *av) C_noret;
C_noret_decl(f_2608)
static void C_ccall f_2608(C_word c,C_word *av) C_noret;
C_noret_decl(f_2616)
static void C_ccall f_2616(C_word c,C_word *av) C_noret;
C_noret_decl(f_2623)
static void C_ccall f_2623(C_word c,C_word *av) C_noret;
C_noret_decl(f_2634)
static void C_ccall f_2634(C_word c,C_word *av) C_noret;
C_noret_decl(f_2638)
static void C_ccall f_2638(C_word c,C_word *av) C_noret;
C_noret_decl(f_2641)
static void C_ccall f_2641(C_word c,C_word *av) C_noret;
C_noret_decl(f_2659)
static void C_ccall f_2659(C_word c,C_word *av) C_noret;
C_noret_decl(f_2663)
static void C_ccall f_2663(C_word c,C_word *av) C_noret;
C_noret_decl(f_2673)
static void C_ccall f_2673(C_word c,C_word *av) C_noret;
C_noret_decl(f_2678)
static void C_ccall f_2678(C_word c,C_word *av) C_noret;
C_noret_decl(f_2682)
static void C_ccall f_2682(C_word c,C_word *av) C_noret;
C_noret_decl(f_2684)
static void C_ccall f_2684(C_word c,C_word *av) C_noret;
C_noret_decl(f_2688)
static void C_ccall f_2688(C_word c,C_word *av) C_noret;
C_noret_decl(f_2690)
static void C_ccall f_2690(C_word c,C_word *av) C_noret;
C_noret_decl(f_2694)
static void C_ccall f_2694(C_word c,C_word *av) C_noret;
C_noret_decl(f_2696)
static void C_ccall f_2696(C_word c,C_word *av) C_noret;
C_noret_decl(f_2700)
static void C_ccall f_2700(C_word c,C_word *av) C_noret;
C_noret_decl(f_2712)
static void C_ccall f_2712(C_word c,C_word *av) C_noret;
C_noret_decl(f_2715)
static void C_ccall f_2715(C_word c,C_word *av) C_noret;
C_noret_decl(f_2721)
static void C_ccall f_2721(C_word c,C_word *av) C_noret;
C_noret_decl(f_2731)
static void C_fcall f_2731(C_word t0,C_word t1) C_noret;
C_noret_decl(f_2775)
static void C_ccall f_2775(C_word c,C_word *av) C_noret;
C_noret_decl(f_2779)
static void C_ccall f_2779(C_word c,C_word *av) C_noret;
C_noret_decl(f_2781)
static void C_ccall f_2781(C_word c,C_word *av) C_noret;
C_noret_decl(f_2787)
static void C_ccall f_2787(C_word c,C_word *av) C_noret;
C_noret_decl(f_2795)
static void C_ccall f_2795(C_word c,C_word *av) C_noret;
C_noret_decl(f_2799)
static void C_ccall f_2799(C_word c,C_word *av) C_noret;
C_noret_decl(f_2803)
static void C_ccall f_2803(C_word c,C_word *av) C_noret;
C_noret_decl(f_2805)
static void C_ccall f_2805(C_word c,C_word *av) C_noret;
C_noret_decl(f_2824)
static void C_ccall f_2824(C_word c,C_word *av) C_noret;
C_noret_decl(f_2892)
static void C_ccall f_2892(C_word c,C_word *av) C_noret;
C_noret_decl(f_2900)
static void C_ccall f_2900(C_word c,C_word *av) C_noret;
C_noret_decl(f_2902)
static void C_ccall f_2902(C_word c,C_word *av) C_noret;
C_noret_decl(f_2910)
static void C_ccall f_2910(C_word c,C_word *av) C_noret;
C_noret_decl(f_2912)
static void C_ccall f_2912(C_word c,C_word *av) C_noret;
C_noret_decl(f_2920)
static void C_ccall f_2920(C_word c,C_word *av) C_noret;
C_noret_decl(f_2922)
static void C_ccall f_2922(C_word c,C_word *av) C_noret;
C_noret_decl(f_2930)
static void C_ccall f_2930(C_word c,C_word *av) C_noret;
C_noret_decl(f_2932)
static void C_ccall f_2932(C_word c,C_word *av) C_noret;
C_noret_decl(f_2940)
static void C_ccall f_2940(C_word c,C_word *av) C_noret;
C_noret_decl(f_2942)
static void C_ccall f_2942(C_word c,C_word *av) C_noret;
C_noret_decl(f_2950)
static void C_ccall f_2950(C_word c,C_word *av) C_noret;
C_noret_decl(f_2952)
static void C_ccall f_2952(C_word c,C_word *av) C_noret;
C_noret_decl(f_2960)
static void C_ccall f_2960(C_word c,C_word *av) C_noret;
C_noret_decl(f_2965)
static void C_ccall f_2965(C_word c,C_word *av) C_noret;
C_noret_decl(f_2972)
static void C_ccall f_2972(C_word c,C_word *av) C_noret;
C_noret_decl(f_2975)
static void C_ccall f_2975(C_word c,C_word *av) C_noret;
C_noret_decl(f_2981)
static void C_ccall f_2981(C_word c,C_word *av) C_noret;
C_noret_decl(f_2987)
static void C_ccall f_2987(C_word c,C_word *av) C_noret;
C_noret_decl(f_3020)
static void C_ccall f_3020(C_word c,C_word *av) C_noret;
C_noret_decl(f_3048)
static void C_fcall f_3048(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_3056)
static void C_ccall f_3056(C_word c,C_word *av) C_noret;
C_noret_decl(f_3085)
static void C_fcall f_3085(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_3098)
static void C_ccall f_3098(C_word c,C_word *av) C_noret;
C_noret_decl(f_3104)
static void C_ccall f_3104(C_word c,C_word *av) C_noret;
C_noret_decl(f_3108)
static void C_ccall f_3108(C_word c,C_word *av) C_noret;
C_noret_decl(f_3116)
static void C_ccall f_3116(C_word c,C_word *av) C_noret;
C_noret_decl(f_3118)
static void C_ccall f_3118(C_word c,C_word *av) C_noret;
C_noret_decl(f_3122)
static void C_ccall f_3122(C_word c,C_word *av) C_noret;
C_noret_decl(f_3130)
static void C_ccall f_3130(C_word c,C_word *av) C_noret;
C_noret_decl(f_3132)
static void C_ccall f_3132(C_word c,C_word *av) C_noret;
C_noret_decl(f_3148)
static void C_ccall f_3148(C_word c,C_word *av) C_noret;
C_noret_decl(f_3157)
static void C_ccall f_3157(C_word c,C_word *av) C_noret;
C_noret_decl(f_3171)
static void C_ccall f_3171(C_word c,C_word *av) C_noret;
C_noret_decl(f_3177)
static void C_ccall f_3177(C_word c,C_word *av) C_noret;
C_noret_decl(f_3181)
static void C_ccall f_3181(C_word c,C_word *av) C_noret;
C_noret_decl(f_3184)
static void C_fcall f_3184(C_word t0,C_word t1) C_noret;
C_noret_decl(f_3187)
static void C_ccall f_3187(C_word c,C_word *av) C_noret;
C_noret_decl(f_3202)
static void C_ccall f_3202(C_word c,C_word *av) C_noret;
C_noret_decl(f_3204)
static void C_ccall f_3204(C_word c,C_word *av) C_noret;
C_noret_decl(f_3207)
static void C_ccall f_3207(C_word c,C_word *av) C_noret;
C_noret_decl(f_3211)
static void C_ccall f_3211(C_word c,C_word *av) C_noret;
C_noret_decl(f_3214)
static void C_ccall f_3214(C_word c,C_word *av) C_noret;
C_noret_decl(f_3223)
static void C_ccall f_3223(C_word c,C_word *av) C_noret;
C_noret_decl(f_3237)
static void C_ccall f_3237(C_word c,C_word *av) C_noret;
C_noret_decl(f_3240)
static void C_fcall f_3240(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3259)
static void C_ccall f_3259(C_word c,C_word *av) C_noret;
C_noret_decl(f_3263)
static void C_ccall f_3263(C_word c,C_word *av) C_noret;
C_noret_decl(f_3266)
static void C_ccall f_3266(C_word c,C_word *av) C_noret;
C_noret_decl(f_3280)
static void C_ccall f_3280(C_word c,C_word *av) C_noret;
C_noret_decl(f_3284)
static void C_ccall f_3284(C_word c,C_word *av) C_noret;
C_noret_decl(f_3287)
static void C_ccall f_3287(C_word c,C_word *av) C_noret;
C_noret_decl(f_3312)
static void C_ccall f_3312(C_word c,C_word *av) C_noret;
C_noret_decl(f_3316)
static void C_ccall f_3316(C_word c,C_word *av) C_noret;
C_noret_decl(f_3319)
static void C_ccall f_3319(C_word c,C_word *av) C_noret;
C_noret_decl(f_3322)
static void C_ccall f_3322(C_word c,C_word *av) C_noret;
C_noret_decl(f_3350)
static void C_ccall f_3350(C_word c,C_word *av) C_noret;
C_noret_decl(f_3354)
static void C_ccall f_3354(C_word c,C_word *av) C_noret;
C_noret_decl(f_3358)
static void C_ccall f_3358(C_word c,C_word *av) C_noret;
C_noret_decl(f_3395)
static void C_ccall f_3395(C_word c,C_word *av) C_noret;
C_noret_decl(f_3402)
static void C_ccall f_3402(C_word c,C_word *av) C_noret;
C_noret_decl(f_3411)
static void C_ccall f_3411(C_word c,C_word *av) C_noret;
C_noret_decl(f_3421)
static void C_ccall f_3421(C_word c,C_word *av) C_noret;
C_noret_decl(f_3425)
static void C_ccall f_3425(C_word c,C_word *av) C_noret;
C_noret_decl(f_3428)
static void C_ccall f_3428(C_word c,C_word *av) C_noret;
C_noret_decl(f_3449)
static void C_ccall f_3449(C_word c,C_word *av) C_noret;
C_noret_decl(f_3457)
static void C_ccall f_3457(C_word c,C_word *av) C_noret;
C_noret_decl(f_3461)
static void C_ccall f_3461(C_word c,C_word *av) C_noret;
C_noret_decl(f_3472)
static void C_ccall f_3472(C_word c,C_word *av) C_noret;
C_noret_decl(f_3474)
static void C_ccall f_3474(C_word c,C_word *av) C_noret;
C_noret_decl(f_3478)
static void C_ccall f_3478(C_word c,C_word *av) C_noret;
C_noret_decl(f_3480)
static void C_ccall f_3480(C_word c,C_word *av) C_noret;
C_noret_decl(f_3499)
static void C_ccall f_3499(C_word c,C_word *av) C_noret;
C_noret_decl(f_3504)
static void C_ccall f_3504(C_word c,C_word *av) C_noret;
C_noret_decl(f_3510)
static void C_ccall f_3510(C_word c,C_word *av) C_noret;
C_noret_decl(f_3551)
static void C_fcall f_3551(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_3559)
static void C_ccall f_3559(C_word c,C_word *av) C_noret;
C_noret_decl(f_3562)
static void C_ccall f_3562(C_word c,C_word *av) C_noret;
C_noret_decl(f_3567)
static void C_ccall f_3567(C_word c,C_word *av) C_noret;
C_noret_decl(f_3573)
static void C_ccall f_3573(C_word c,C_word *av) C_noret;
C_noret_decl(f_3579)
static void C_ccall f_3579(C_word c,C_word *av) C_noret;
C_noret_decl(f_3583)
static void C_ccall f_3583(C_word c,C_word *av) C_noret;
C_noret_decl(f_3588)
static void C_ccall f_3588(C_word c,C_word *av) C_noret;
C_noret_decl(f_3590)
static void C_fcall f_3590(C_word t0,C_word t1) C_noret;
C_noret_decl(f_3594)
static void C_ccall f_3594(C_word c,C_word *av) C_noret;
C_noret_decl(f_3596)
static void C_fcall f_3596(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_3612)
static void C_ccall f_3612(C_word c,C_word *av) C_noret;
C_noret_decl(f_3618)
static void C_ccall f_3618(C_word c,C_word *av) C_noret;
C_noret_decl(f_3621)
static void C_ccall f_3621(C_word c,C_word *av) C_noret;
C_noret_decl(f_3637)
static void C_ccall f_3637(C_word c,C_word *av) C_noret;
C_noret_decl(f_3647)
static void C_fcall f_3647(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3653)
static void C_ccall f_3653(C_word c,C_word *av) C_noret;
C_noret_decl(f_3664)
static void C_ccall f_3664(C_word c,C_word *av) C_noret;
C_noret_decl(f_3668)
static void C_fcall f_3668(C_word t0,C_word t1) C_noret;
C_noret_decl(f_3672)
static void C_ccall f_3672(C_word c,C_word *av) C_noret;
C_noret_decl(f_3677)
static void C_fcall f_3677(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3687)
static void C_ccall f_3687(C_word c,C_word *av) C_noret;
C_noret_decl(f_3690)
static void C_ccall f_3690(C_word c,C_word *av) C_noret;
C_noret_decl(f_3702)
static void C_fcall f_3702(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3707)
static C_word C_fcall f_3707(C_word t0,C_word t1);
C_noret_decl(f_3726)
static C_word C_fcall f_3726(C_word t0,C_word t1);
C_noret_decl(f_3749)
static void C_ccall f_3749(C_word c,C_word *av) C_noret;
C_noret_decl(f_3751)
static void C_fcall f_3751(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6,C_word t7) C_noret;
C_noret_decl(f_3755)
static void C_ccall f_3755(C_word c,C_word *av) C_noret;
C_noret_decl(f_3761)
static void C_ccall f_3761(C_word c,C_word *av) C_noret;
C_noret_decl(f_3764)
static void C_ccall f_3764(C_word c,C_word *av) C_noret;
C_noret_decl(f_3769)
static void C_ccall f_3769(C_word c,C_word *av) C_noret;
C_noret_decl(f_3775)
static void C_ccall f_3775(C_word c,C_word *av) C_noret;
C_noret_decl(f_3781)
static void C_ccall f_3781(C_word c,C_word *av) C_noret;
C_noret_decl(f_3785)
static void C_ccall f_3785(C_word c,C_word *av) C_noret;
C_noret_decl(f_3788)
static void C_ccall f_3788(C_word c,C_word *av) C_noret;
C_noret_decl(f_3796)
static void C_ccall f_3796(C_word c,C_word *av) C_noret;
C_noret_decl(f_3802)
static void C_ccall f_3802(C_word c,C_word *av) C_noret;
C_noret_decl(f_3806)
static void C_fcall f_3806(C_word t0,C_word t1) C_noret;
C_noret_decl(f_3813)
static void C_ccall f_3813(C_word c,C_word *av) C_noret;
C_noret_decl(f_3816)
static void C_ccall f_3816(C_word c,C_word *av) C_noret;
C_noret_decl(f_3820)
static void C_ccall f_3820(C_word c,C_word *av) C_noret;
C_noret_decl(f_3841)
static void C_ccall f_3841(C_word c,C_word *av) C_noret;
C_noret_decl(f_3843)
static void C_fcall f_3843(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3868)
static void C_ccall f_3868(C_word c,C_word *av) C_noret;
C_noret_decl(f_3877)
static void C_ccall f_3877(C_word c,C_word *av) C_noret;
C_noret_decl(f_3883)
static void C_ccall f_3883(C_word c,C_word *av) C_noret;
C_noret_decl(f_3908)
static void C_fcall f_3908(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_3921)
static void C_ccall f_3921(C_word c,C_word *av) C_noret;
C_noret_decl(f_3927)
static void C_ccall f_3927(C_word c,C_word *av) C_noret;
C_noret_decl(f_3941)
static void C_ccall f_3941(C_word c,C_word *av) C_noret;
C_noret_decl(f_3948)
static void C_ccall f_3948(C_word c,C_word *av) C_noret;
C_noret_decl(f_3958)
static void C_ccall f_3958(C_word c,C_word *av) C_noret;
C_noret_decl(f_3967)
static void C_ccall f_3967(C_word c,C_word *av) C_noret;
C_noret_decl(f_3981)
static void C_ccall f_3981(C_word c,C_word *av) C_noret;
C_noret_decl(f_3988)
static void C_ccall f_3988(C_word c,C_word *av) C_noret;
C_noret_decl(f_3998)
static void C_ccall f_3998(C_word c,C_word *av) C_noret;
C_noret_decl(f_4007)
static void C_ccall f_4007(C_word c,C_word *av) C_noret;
C_noret_decl(f_4014)
static void C_ccall f_4014(C_word c,C_word *av) C_noret;
C_noret_decl(f_4022)
static void C_ccall f_4022(C_word c,C_word *av) C_noret;
C_noret_decl(f_4029)
static void C_ccall f_4029(C_word c,C_word *av) C_noret;
C_noret_decl(f_4037)
static void C_ccall f_4037(C_word c,C_word *av) C_noret;
C_noret_decl(f_4041)
static void C_ccall f_4041(C_word c,C_word *av) C_noret;
C_noret_decl(f_4046)
static void C_ccall f_4046(C_word c,C_word *av) C_noret;
C_noret_decl(f_4051)
static void C_ccall f_4051(C_word c,C_word *av) C_noret;
C_noret_decl(f_4057)
static void C_ccall f_4057(C_word c,C_word *av) C_noret;
C_noret_decl(f_4061)
static void C_ccall f_4061(C_word c,C_word *av) C_noret;
C_noret_decl(f_4066)
static void C_ccall f_4066(C_word c,C_word *av) C_noret;
C_noret_decl(f_4071)
static void C_ccall f_4071(C_word c,C_word *av) C_noret;
C_noret_decl(f_4075)
static void C_ccall f_4075(C_word c,C_word *av) C_noret;
C_noret_decl(f_4080)
static void C_ccall f_4080(C_word c,C_word *av) C_noret;
C_noret_decl(f_4086)
static void C_ccall f_4086(C_word c,C_word *av) C_noret;
C_noret_decl(f_4090)
static void C_ccall f_4090(C_word c,C_word *av) C_noret;
C_noret_decl(f_4095)
static void C_ccall f_4095(C_word c,C_word *av) C_noret;
C_noret_decl(f_4099)
static void C_ccall f_4099(C_word c,C_word *av) C_noret;
C_noret_decl(f_4104)
static void C_ccall f_4104(C_word c,C_word *av) C_noret;
C_noret_decl(f_4110)
static void C_ccall f_4110(C_word c,C_word *av) C_noret;
C_noret_decl(f_4114)
static void C_ccall f_4114(C_word c,C_word *av) C_noret;
C_noret_decl(f_4119)
static void C_ccall f_4119(C_word c,C_word *av) C_noret;
C_noret_decl(f_4123)
static void C_ccall f_4123(C_word c,C_word *av) C_noret;
C_noret_decl(f_4128)
static void C_ccall f_4128(C_word c,C_word *av) C_noret;
C_noret_decl(f_4133)
static void C_ccall f_4133(C_word c,C_word *av) C_noret;
C_noret_decl(f_4139)
static void C_ccall f_4139(C_word c,C_word *av) C_noret;
C_noret_decl(f_4143)
static void C_ccall f_4143(C_word c,C_word *av) C_noret;
C_noret_decl(f_4148)
static void C_ccall f_4148(C_word c,C_word *av) C_noret;
C_noret_decl(f_4159)
static void C_ccall f_4159(C_word c,C_word *av) C_noret;
C_noret_decl(f_4169)
static void C_ccall f_4169(C_word c,C_word *av) C_noret;
C_noret_decl(f_4172)
static void C_ccall f_4172(C_word c,C_word *av) C_noret;
C_noret_decl(f_4176)
static void C_ccall f_4176(C_word c,C_word *av) C_noret;
C_noret_decl(f_4179)
static void C_ccall f_4179(C_word c,C_word *av) C_noret;
C_noret_decl(f_4185)
static void C_ccall f_4185(C_word c,C_word *av) C_noret;
C_noret_decl(f_4194)
static void C_ccall f_4194(C_word c,C_word *av) C_noret;
C_noret_decl(f_4198)
static void C_ccall f_4198(C_word c,C_word *av) C_noret;
C_noret_decl(f_4203)
static void C_ccall f_4203(C_word c,C_word *av) C_noret;
C_noret_decl(f_4225)
static void C_ccall f_4225(C_word c,C_word *av) C_noret;
C_noret_decl(f_4229)
static void C_ccall f_4229(C_word c,C_word *av) C_noret;
C_noret_decl(f_4232)
static void C_ccall f_4232(C_word c,C_word *av) C_noret;
C_noret_decl(f_4235)
static void C_ccall f_4235(C_word c,C_word *av) C_noret;
C_noret_decl(f_4238)
static void C_ccall f_4238(C_word c,C_word *av) C_noret;
C_noret_decl(f_4241)
static void C_ccall f_4241(C_word c,C_word *av) C_noret;
C_noret_decl(f_4250)
static void C_ccall f_4250(C_word c,C_word *av) C_noret;
C_noret_decl(f_4268)
static void C_ccall f_4268(C_word c,C_word *av) C_noret;
C_noret_decl(f_4272)
static void C_ccall f_4272(C_word c,C_word *av) C_noret;
C_noret_decl(f_4275)
static void C_ccall f_4275(C_word c,C_word *av) C_noret;
C_noret_decl(f_4281)
static void C_ccall f_4281(C_word c,C_word *av) C_noret;
C_noret_decl(f_4284)
static void C_ccall f_4284(C_word c,C_word *av) C_noret;
C_noret_decl(f_4290)
static void C_ccall f_4290(C_word c,C_word *av) C_noret;
C_noret_decl(f_4308)
static void C_ccall f_4308(C_word c,C_word *av) C_noret;
C_noret_decl(f_4321)
static void C_ccall f_4321(C_word c,C_word *av) C_noret;
C_noret_decl(f_4330)
static void C_ccall f_4330(C_word c,C_word *av) C_noret;
C_noret_decl(f_4335)
static void C_fcall f_4335(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4343)
static void C_ccall f_4343(C_word c,C_word *av) C_noret;
C_noret_decl(f_4382)
static C_word C_fcall f_4382(C_word t0,C_word t1);
C_noret_decl(f_4423)
static void C_ccall f_4423(C_word c,C_word *av) C_noret;
C_noret_decl(f_4427)
static void C_ccall f_4427(C_word c,C_word *av) C_noret;
C_noret_decl(f_4436)
static C_word C_fcall f_4436(C_word t0,C_word t1);
C_noret_decl(f_4467)
static void C_ccall f_4467(C_word c,C_word *av) C_noret;
C_noret_decl(f_4474)
static void C_ccall f_4474(C_word c,C_word *av) C_noret;
C_noret_decl(f_4483)
static void C_ccall f_4483(C_word c,C_word *av) C_noret;
C_noret_decl(f_4529)
static void C_ccall f_4529(C_word c,C_word *av) C_noret;
C_noret_decl(f_4533)
static void C_ccall f_4533(C_word c,C_word *av) C_noret;
C_noret_decl(f_4536)
static void C_fcall f_4536(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4539)
static void C_ccall f_4539(C_word c,C_word *av) C_noret;
C_noret_decl(f_4545)
static void C_ccall f_4545(C_word c,C_word *av) C_noret;
C_noret_decl(f_4557)
static void C_ccall f_4557(C_word c,C_word *av) C_noret;
C_noret_decl(f_4559)
static void C_ccall f_4559(C_word c,C_word *av) C_noret;
C_noret_decl(f_4580)
static C_word C_fcall f_4580(C_word t0,C_word t1);
C_noret_decl(f_4605)
static void C_ccall f_4605(C_word c,C_word *av) C_noret;
C_noret_decl(f_4617)
static void C_ccall f_4617(C_word c,C_word *av) C_noret;
C_noret_decl(f_4644)
static void C_ccall f_4644(C_word c,C_word *av) C_noret;
C_noret_decl(f_4658)
static void C_ccall f_4658(C_word c,C_word *av) C_noret;
C_noret_decl(f_4697)
static void C_ccall f_4697(C_word c,C_word *av) C_noret;
C_noret_decl(f_4722)
static void C_ccall f_4722(C_word c,C_word *av) C_noret;
C_noret_decl(f_4727)
static void C_ccall f_4727(C_word c,C_word *av) C_noret;
C_noret_decl(f_4734)
static void C_ccall f_4734(C_word c,C_word *av) C_noret;
C_noret_decl(f_4741)
static void C_ccall f_4741(C_word c,C_word *av) C_noret;
C_noret_decl(f_4780)
static void C_fcall f_4780(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4784)
static void C_ccall f_4784(C_word c,C_word *av) C_noret;
C_noret_decl(f_4796)
static void C_ccall f_4796(C_word c,C_word *av) C_noret;
C_noret_decl(f_4807)
static void C_ccall f_4807(C_word c,C_word *av) C_noret;
C_noret_decl(f_4824)
static void C_ccall f_4824(C_word c,C_word *av) C_noret;
C_noret_decl(f_4839)
static void C_ccall f_4839(C_word c,C_word *av) C_noret;
C_noret_decl(f_4843)
static void C_ccall f_4843(C_word c,C_word *av) C_noret;
C_noret_decl(f_4907)
static void C_ccall f_4907(C_word c,C_word *av) C_noret;
C_noret_decl(f_4924)
static void C_ccall f_4924(C_word c,C_word *av) C_noret;
C_noret_decl(f_4928)
static void C_ccall f_4928(C_word c,C_word *av) C_noret;
C_noret_decl(f_4932)
static void C_ccall f_4932(C_word c,C_word *av) C_noret;
C_noret_decl(f_4944)
static void C_ccall f_4944(C_word c,C_word *av) C_noret;
C_noret_decl(f_4951)
static void C_ccall f_4951(C_word c,C_word *av) C_noret;
C_noret_decl(f_4955)
static void C_ccall f_4955(C_word c,C_word *av) C_noret;
C_noret_decl(f_4959)
static void C_ccall f_4959(C_word c,C_word *av) C_noret;
C_noret_decl(f_4963)
static void C_ccall f_4963(C_word c,C_word *av) C_noret;
C_noret_decl(f_4983)
static void C_ccall f_4983(C_word c,C_word *av) C_noret;
C_noret_decl(f_4991)
static void C_fcall f_4991(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5016)
static void C_ccall f_5016(C_word c,C_word *av) C_noret;
C_noret_decl(f_5030)
static void C_fcall f_5030(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6) C_noret;
C_noret_decl(f_5032)
static void C_fcall f_5032(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5037)
static C_word C_fcall f_5037(C_word t0,C_word t1);
C_noret_decl(f_5046)
static C_word C_fcall f_5046(C_word t0,C_word t1);
C_noret_decl(f_5073)
static void C_ccall f_5073(C_word c,C_word *av) C_noret;
C_noret_decl(f_5076)
static void C_ccall f_5076(C_word c,C_word *av) C_noret;
C_noret_decl(f_5081)
static void C_ccall f_5081(C_word c,C_word *av) C_noret;
C_noret_decl(f_5087)
static void C_ccall f_5087(C_word c,C_word *av) C_noret;
C_noret_decl(f_5112)
static void C_ccall f_5112(C_word c,C_word *av) C_noret;
C_noret_decl(f_5114)
static void C_ccall f_5114(C_word c,C_word *av) C_noret;
C_noret_decl(f_5174)
static void C_ccall f_5174(C_word c,C_word *av) C_noret;
C_noret_decl(f_5246)
static void C_ccall f_5246(C_word c,C_word *av) C_noret;
C_noret_decl(f_5256)
static void C_ccall f_5256(C_word c,C_word *av) C_noret;
C_noret_decl(f_5267)
static void C_ccall f_5267(C_word c,C_word *av) C_noret;
C_noret_decl(f_5273)
static void C_ccall f_5273(C_word c,C_word *av) C_noret;
C_noret_decl(f_5279)
static void C_ccall f_5279(C_word c,C_word *av) C_noret;
C_noret_decl(f_5285)
static void C_ccall f_5285(C_word c,C_word *av) C_noret;
C_noret_decl(f_5291)
static void C_ccall f_5291(C_word c,C_word *av) C_noret;
C_noret_decl(f_5297)
static void C_ccall f_5297(C_word c,C_word *av) C_noret;
C_noret_decl(f_5303)
static void C_ccall f_5303(C_word c,C_word *av) C_noret;
C_noret_decl(f_5309)
static void C_ccall f_5309(C_word c,C_word *av) C_noret;
C_noret_decl(f_5315)
static void C_ccall f_5315(C_word c,C_word *av) C_noret;
C_noret_decl(f_5321)
static void C_ccall f_5321(C_word c,C_word *av) C_noret;
C_noret_decl(f_5327)
static void C_ccall f_5327(C_word c,C_word *av) C_noret;
C_noret_decl(f_5333)
static void C_ccall f_5333(C_word c,C_word *av) C_noret;
C_noret_decl(f_5339)
static void C_ccall f_5339(C_word c,C_word *av) C_noret;
C_noret_decl(f_5345)
static void C_ccall f_5345(C_word c,C_word *av) C_noret;
C_noret_decl(f_5351)
static void C_ccall f_5351(C_word c,C_word *av) C_noret;
C_noret_decl(f_5357)
static void C_ccall f_5357(C_word c,C_word *av) C_noret;
C_noret_decl(f_5363)
static void C_ccall f_5363(C_word c,C_word *av) C_noret;
C_noret_decl(f_5369)
static void C_ccall f_5369(C_word c,C_word *av) C_noret;
C_noret_decl(f_5375)
static void C_ccall f_5375(C_word c,C_word *av) C_noret;
C_noret_decl(f_5381)
static void C_ccall f_5381(C_word c,C_word *av) C_noret;
C_noret_decl(f_5387)
static void C_ccall f_5387(C_word c,C_word *av) C_noret;
C_noret_decl(f_5393)
static void C_ccall f_5393(C_word c,C_word *av) C_noret;
C_noret_decl(f_5399)
static void C_ccall f_5399(C_word c,C_word *av) C_noret;
C_noret_decl(f_5405)
static void C_ccall f_5405(C_word c,C_word *av) C_noret;
C_noret_decl(f_5411)
static void C_ccall f_5411(C_word c,C_word *av) C_noret;
C_noret_decl(f_5417)
static void C_ccall f_5417(C_word c,C_word *av) C_noret;
C_noret_decl(f_5423)
static void C_ccall f_5423(C_word c,C_word *av) C_noret;
C_noret_decl(f_5429)
static void C_ccall f_5429(C_word c,C_word *av) C_noret;
C_noret_decl(f_5435)
static void C_ccall f_5435(C_word c,C_word *av) C_noret;
C_noret_decl(f_5441)
static void C_ccall f_5441(C_word c,C_word *av) C_noret;
C_noret_decl(f_5447)
static void C_ccall f_5447(C_word c,C_word *av) C_noret;
C_noret_decl(f_5465)
static void C_ccall f_5465(C_word c,C_word *av) C_noret;
C_noret_decl(f_5510)
static void C_ccall f_5510(C_word c,C_word *av) C_noret;
C_noret_decl(f_5514)
static void C_ccall f_5514(C_word c,C_word *av) C_noret;
C_noret_decl(f_5519)
static void C_ccall f_5519(C_word c,C_word *av) C_noret;
C_noret_decl(f_5526)
static void C_ccall f_5526(C_word c,C_word *av) C_noret;
C_noret_decl(f_5544)
static void C_ccall f_5544(C_word c,C_word *av) C_noret;
C_noret_decl(f_5548)
static void C_ccall f_5548(C_word c,C_word *av) C_noret;
C_noret_decl(f_5550)
static void C_ccall f_5550(C_word c,C_word *av) C_noret;
C_noret_decl(f_5554)
static void C_ccall f_5554(C_word c,C_word *av) C_noret;
C_noret_decl(f_5557)
static void C_ccall f_5557(C_word c,C_word *av) C_noret;
C_noret_decl(f_5566)
static void C_ccall f_5566(C_word c,C_word *av) C_noret;
C_noret_decl(f_5587)
static void C_ccall f_5587(C_word c,C_word *av) C_noret;
C_noret_decl(f_5591)
static void C_ccall f_5591(C_word c,C_word *av) C_noret;
C_noret_decl(f_5593)
static void C_ccall f_5593(C_word c,C_word *av) C_noret;
C_noret_decl(f_5597)
static void C_ccall f_5597(C_word c,C_word *av) C_noret;
C_noret_decl(f_5599)
static void C_ccall f_5599(C_word c,C_word *av) C_noret;
C_noret_decl(f_5603)
static void C_ccall f_5603(C_word c,C_word *av) C_noret;
C_noret_decl(C_posix_toplevel)
C_externexport void C_ccall C_posix_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(va6083)
static void C_fcall va6083(C_word t0,C_word t1) C_noret;
C_noret_decl(va6091)
static void C_fcall va6091(C_word t0,C_word t1) C_noret;

C_noret_decl(trf_2379)
static void C_ccall trf_2379(C_word c,C_word *av) C_noret;
static void C_ccall trf_2379(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_2379(t0,t1,t2);}

C_noret_decl(trf_2420)
static void C_ccall trf_2420(C_word c,C_word *av) C_noret;
static void C_ccall trf_2420(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_2420(t0,t1,t2);}

C_noret_decl(trf_2570)
static void C_ccall trf_2570(C_word c,C_word *av) C_noret;
static void C_ccall trf_2570(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_2570(t0,t1,t2,t3,t4);}

C_noret_decl(trf_2731)
static void C_ccall trf_2731(C_word c,C_word *av) C_noret;
static void C_ccall trf_2731(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_2731(t0,t1);}

C_noret_decl(trf_3048)
static void C_ccall trf_3048(C_word c,C_word *av) C_noret;
static void C_ccall trf_3048(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_3048(t0,t1,t2,t3);}

C_noret_decl(trf_3085)
static void C_ccall trf_3085(C_word c,C_word *av) C_noret;
static void C_ccall trf_3085(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_3085(t0,t1,t2,t3,t4);}

C_noret_decl(trf_3184)
static void C_ccall trf_3184(C_word c,C_word *av) C_noret;
static void C_ccall trf_3184(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_3184(t0,t1);}

C_noret_decl(trf_3240)
static void C_ccall trf_3240(C_word c,C_word *av) C_noret;
static void C_ccall trf_3240(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3240(t0,t1,t2);}

C_noret_decl(trf_3551)
static void C_ccall trf_3551(C_word c,C_word *av) C_noret;
static void C_ccall trf_3551(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_3551(t0,t1,t2,t3);}

C_noret_decl(trf_3590)
static void C_ccall trf_3590(C_word c,C_word *av) C_noret;
static void C_ccall trf_3590(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_3590(t0,t1);}

C_noret_decl(trf_3596)
static void C_ccall trf_3596(C_word c,C_word *av) C_noret;
static void C_ccall trf_3596(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_3596(t0,t1,t2,t3);}

C_noret_decl(trf_3647)
static void C_ccall trf_3647(C_word c,C_word *av) C_noret;
static void C_ccall trf_3647(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3647(t0,t1,t2);}

C_noret_decl(trf_3668)
static void C_ccall trf_3668(C_word c,C_word *av) C_noret;
static void C_ccall trf_3668(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_3668(t0,t1);}

C_noret_decl(trf_3677)
static void C_ccall trf_3677(C_word c,C_word *av) C_noret;
static void C_ccall trf_3677(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3677(t0,t1,t2);}

C_noret_decl(trf_3702)
static void C_ccall trf_3702(C_word c,C_word *av) C_noret;
static void C_ccall trf_3702(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3702(t0,t1,t2);}

C_noret_decl(trf_3751)
static void C_ccall trf_3751(C_word c,C_word *av) C_noret;
static void C_ccall trf_3751(C_word c,C_word *av){
C_word t0=av[7];
C_word t1=av[6];
C_word t2=av[5];
C_word t3=av[4];
C_word t4=av[3];
C_word t5=av[2];
C_word t6=av[1];
C_word t7=av[0];
f_3751(t0,t1,t2,t3,t4,t5,t6,t7);}

C_noret_decl(trf_3806)
static void C_ccall trf_3806(C_word c,C_word *av) C_noret;
static void C_ccall trf_3806(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_3806(t0,t1);}

C_noret_decl(trf_3843)
static void C_ccall trf_3843(C_word c,C_word *av) C_noret;
static void C_ccall trf_3843(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3843(t0,t1,t2);}

C_noret_decl(trf_3908)
static void C_ccall trf_3908(C_word c,C_word *av) C_noret;
static void C_ccall trf_3908(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_3908(t0,t1,t2,t3,t4);}

C_noret_decl(trf_4335)
static void C_ccall trf_4335(C_word c,C_word *av) C_noret;
static void C_ccall trf_4335(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4335(t0,t1,t2);}

C_noret_decl(trf_4536)
static void C_ccall trf_4536(C_word c,C_word *av) C_noret;
static void C_ccall trf_4536(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4536(t0,t1);}

C_noret_decl(trf_4780)
static void C_ccall trf_4780(C_word c,C_word *av) C_noret;
static void C_ccall trf_4780(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4780(t0,t1);}

C_noret_decl(trf_4991)
static void C_ccall trf_4991(C_word c,C_word *av) C_noret;
static void C_ccall trf_4991(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4991(t0,t1,t2);}

C_noret_decl(trf_5030)
static void C_ccall trf_5030(C_word c,C_word *av) C_noret;
static void C_ccall trf_5030(C_word c,C_word *av){
C_word t0=av[6];
C_word t1=av[5];
C_word t2=av[4];
C_word t3=av[3];
C_word t4=av[2];
C_word t5=av[1];
C_word t6=av[0];
f_5030(t0,t1,t2,t3,t4,t5,t6);}

C_noret_decl(trf_5032)
static void C_ccall trf_5032(C_word c,C_word *av) C_noret;
static void C_ccall trf_5032(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5032(t0,t1,t2);}

C_noret_decl(trva6083)
static void C_ccall trva6083(C_word c,C_word *av) C_noret;
static void C_ccall trva6083(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
va6083(t0,t1);}

C_noret_decl(trva6091)
static void C_ccall trva6091(C_word c,C_word *av) C_noret;
static void C_ccall trva6091(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
va6091(t0,t1);}

/* f6102 in chicken.time.posix#seconds->local-time in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f6102(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f6102,c,av);}
/* posix-common.scm:584: decode-seconds */
{C_proc tp=(C_proc)C_fast_retrieve_proc(lf[256]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=lf[256];
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=C_SCHEME_FALSE;
tp(4,av2);}}

/* f6106 in chicken.time.posix#seconds->utc-time in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f6106(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f6106,c,av);}
/* posix-common.scm:589: decode-seconds */
{C_proc tp=(C_proc)C_fast_retrieve_proc(lf[256]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=lf[256];
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=C_SCHEME_TRUE;
tp(4,av2);}}

/* k2225 */
static void C_ccall f_2227(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2227,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2230,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_pathname_toplevel(2,av2);}}

/* k2228 in k2225 */
static void C_ccall f_2230(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2230,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2233,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_extras_toplevel(2,av2);}}

/* k2231 in k2228 in k2225 */
static void C_ccall f_2233(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2233,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2236,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_port_toplevel(2,av2);}}

/* k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2236(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2236,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2239,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_lolevel_toplevel(2,av2);}}

/* k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2239(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2239,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2242,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_library_toplevel(2,av2);}}

/* k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2242(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2242,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2245,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_data_2dstructures_toplevel(2,av2);}}

/* k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2245(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word t24;
C_word t25;
C_word t26;
C_word t27;
C_word t28;
C_word t29;
C_word t30;
C_word t31;
C_word t32;
C_word t33;
C_word t34;
C_word t35;
C_word t36;
C_word t37;
C_word t38;
C_word t39;
C_word t40;
C_word t41;
C_word t42;
C_word t43;
C_word t44;
C_word t45;
C_word t46;
C_word t47;
C_word t48;
C_word t49;
C_word t50;
C_word t51;
C_word t52;
C_word t53;
C_word t54;
C_word t55;
C_word t56;
C_word t57;
C_word t58;
C_word t59;
C_word t60;
C_word t61;
C_word t62;
C_word t63;
C_word t64;
C_word t65;
C_word t66;
C_word t67;
C_word t68;
C_word t69;
C_word t70;
C_word t71;
C_word t72;
C_word t73;
C_word t74;
C_word t75;
C_word t76;
C_word t77;
C_word t78;
C_word t79;
C_word t80;
C_word t81;
C_word t82;
C_word t83;
C_word t84;
C_word t85;
C_word t86;
C_word t87;
C_word t88;
C_word t89;
C_word t90;
C_word t91;
C_word t92;
C_word t93;
C_word t94;
C_word t95;
C_word t96;
C_word t97;
C_word t98;
C_word t99;
C_word t100;
C_word t101;
C_word t102;
C_word t103;
C_word t104;
C_word t105;
C_word t106;
C_word t107;
C_word t108;
C_word t109;
C_word t110;
C_word t111;
C_word t112;
C_word t113;
C_word t114;
C_word t115;
C_word t116;
C_word t117;
C_word t118;
C_word t119;
C_word t120;
C_word t121;
C_word t122;
C_word t123;
C_word t124;
C_word t125;
C_word t126;
C_word t127;
C_word t128;
C_word t129;
C_word t130;
C_word t131;
C_word t132;
C_word t133;
C_word t134;
C_word t135;
C_word t136;
C_word t137;
C_word t138;
C_word t139;
C_word t140;
C_word t141;
C_word t142;
C_word t143;
C_word t144;
C_word t145;
C_word t146;
C_word t147;
C_word t148;
C_word t149;
C_word t150;
C_word t151;
C_word t152;
C_word t153;
C_word t154;
C_word t155;
C_word t156;
C_word t157;
C_word t158;
C_word t159;
C_word t160;
C_word t161;
C_word t162;
C_word t163;
C_word t164;
C_word t165;
C_word t166;
C_word t167;
C_word t168;
C_word t169;
C_word t170;
C_word t171;
C_word t172;
C_word t173;
C_word t174;
C_word t175;
C_word t176;
C_word t177;
C_word t178;
C_word t179;
C_word t180;
C_word t181;
C_word t182;
C_word t183;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(104,c,6)))){
C_save_and_reclaim((void *)f_2245,c,av);}
a=C_alloc(104);
t2=C_a_i_provide(&a,1,lf[2]);
t3=C_a_i_provide(&a,1,lf[3]);
t4=C_set_block_item(lf[4] /* chicken.file.posix#create-fifo */,0,C_SCHEME_UNDEFINED);
t5=C_set_block_item(lf[5] /* chicken.file.posix#create-symbolic-link */,0,C_SCHEME_UNDEFINED);
t6=C_set_block_item(lf[6] /* chicken.file.posix#read-symbolic-link */,0,C_SCHEME_UNDEFINED);
t7=C_set_block_item(lf[7] /* chicken.file.posix#duplicate-fileno */,0,C_SCHEME_UNDEFINED);
t8=C_set_block_item(lf[8] /* chicken.file.posix#fcntl/dupfd */,0,C_SCHEME_UNDEFINED);
t9=C_set_block_item(lf[9] /* chicken.file.posix#fcntl/getfd */,0,C_SCHEME_UNDEFINED);
t10=C_set_block_item(lf[10] /* chicken.file.posix#fcntl/getfl */,0,C_SCHEME_UNDEFINED);
t11=C_set_block_item(lf[11] /* chicken.file.posix#fcntl/setfd */,0,C_SCHEME_UNDEFINED);
t12=C_set_block_item(lf[12] /* chicken.file.posix#fcntl/setfl */,0,C_SCHEME_UNDEFINED);
t13=C_set_block_item(lf[13] /* chicken.file.posix#file-close */,0,C_SCHEME_UNDEFINED);
t14=C_set_block_item(lf[14] /* chicken.file.posix#file-control */,0,C_SCHEME_UNDEFINED);
t15=C_set_block_item(lf[15] /* chicken.file.posix#file-creation-mode */,0,C_SCHEME_UNDEFINED);
t16=C_set_block_item(lf[16] /* chicken.file.posix#file-group */,0,C_SCHEME_UNDEFINED);
t17=C_set_block_item(lf[17] /* chicken.file.posix#file-link */,0,C_SCHEME_UNDEFINED);
t18=C_set_block_item(lf[18] /* chicken.file.posix#file-lock */,0,C_SCHEME_UNDEFINED);
t19=C_set_block_item(lf[19] /* chicken.file.posix#file-lock/blocking */,0,C_SCHEME_UNDEFINED);
t20=C_set_block_item(lf[20] /* chicken.file.posix#file-mkstemp */,0,C_SCHEME_UNDEFINED);
t21=C_set_block_item(lf[21] /* chicken.file.posix#file-open */,0,C_SCHEME_UNDEFINED);
t22=C_set_block_item(lf[22] /* chicken.file.posix#file-owner */,0,C_SCHEME_UNDEFINED);
t23=C_set_block_item(lf[23] /* chicken.file.posix#file-permissions */,0,C_SCHEME_UNDEFINED);
t24=C_set_block_item(lf[24] /* chicken.file.posix#file-position */,0,C_SCHEME_UNDEFINED);
t25=C_set_block_item(lf[25] /* chicken.file.posix#file-read */,0,C_SCHEME_UNDEFINED);
t26=C_set_block_item(lf[26] /* chicken.file.posix#file-select */,0,C_SCHEME_UNDEFINED);
t27=C_set_block_item(lf[27] /* chicken.file.posix#file-test-lock */,0,C_SCHEME_UNDEFINED);
t28=C_set_block_item(lf[28] /* chicken.file.posix#file-truncate */,0,C_SCHEME_UNDEFINED);
t29=C_set_block_item(lf[29] /* chicken.file.posix#file-unlock */,0,C_SCHEME_UNDEFINED);
t30=C_set_block_item(lf[30] /* chicken.file.posix#file-write */,0,C_SCHEME_UNDEFINED);
t31=C_set_block_item(lf[31] /* chicken.file.posix#file-type */,0,C_SCHEME_UNDEFINED);
t32=C_set_block_item(lf[32] /* chicken.file.posix#block-device? */,0,C_SCHEME_UNDEFINED);
t33=C_set_block_item(lf[33] /* chicken.file.posix#character-device? */,0,C_SCHEME_UNDEFINED);
t34=C_set_block_item(lf[34] /* chicken.file.posix#directory? */,0,C_SCHEME_UNDEFINED);
t35=C_set_block_item(lf[35] /* chicken.file.posix#fifo? */,0,C_SCHEME_UNDEFINED);
t36=C_set_block_item(lf[36] /* chicken.file.posix#regular-file? */,0,C_SCHEME_UNDEFINED);
t37=C_set_block_item(lf[37] /* chicken.file.posix#socket? */,0,C_SCHEME_UNDEFINED);
t38=C_set_block_item(lf[38] /* chicken.file.posix#symbolic-link? */,0,C_SCHEME_UNDEFINED);
t39=C_set_block_item(lf[39] /* chicken.file.posix#fileno/stderr */,0,C_SCHEME_UNDEFINED);
t40=C_set_block_item(lf[40] /* chicken.file.posix#fileno/stdin */,0,C_SCHEME_UNDEFINED);
t41=C_set_block_item(lf[41] /* chicken.file.posix#fileno/stdout */,0,C_SCHEME_UNDEFINED);
t42=C_set_block_item(lf[42] /* chicken.file.posix#open-input-file* */,0,C_SCHEME_UNDEFINED);
t43=C_set_block_item(lf[43] /* chicken.file.posix#open-output-file* */,0,C_SCHEME_UNDEFINED);
t44=C_set_block_item(lf[44] /* chicken.file.posix#open/append */,0,C_SCHEME_UNDEFINED);
t45=C_set_block_item(lf[45] /* chicken.file.posix#open/binary */,0,C_SCHEME_UNDEFINED);
t46=C_set_block_item(lf[46] /* chicken.file.posix#open/creat */,0,C_SCHEME_UNDEFINED);
t47=C_set_block_item(lf[47] /* chicken.file.posix#open/excl */,0,C_SCHEME_UNDEFINED);
t48=C_set_block_item(lf[48] /* chicken.file.posix#open/fsync */,0,C_SCHEME_UNDEFINED);
t49=C_set_block_item(lf[49] /* chicken.file.posix#open/noctty */,0,C_SCHEME_UNDEFINED);
t50=C_set_block_item(lf[50] /* chicken.file.posix#open/noinherit */,0,C_SCHEME_UNDEFINED);
t51=C_set_block_item(lf[51] /* chicken.file.posix#open/nonblock */,0,C_SCHEME_UNDEFINED);
t52=C_set_block_item(lf[52] /* chicken.file.posix#open/rdonly */,0,C_SCHEME_UNDEFINED);
t53=C_set_block_item(lf[53] /* chicken.file.posix#open/rdwr */,0,C_SCHEME_UNDEFINED);
t54=C_set_block_item(lf[54] /* chicken.file.posix#open/read */,0,C_SCHEME_UNDEFINED);
t55=C_set_block_item(lf[55] /* chicken.file.posix#open/sync */,0,C_SCHEME_UNDEFINED);
t56=C_set_block_item(lf[56] /* chicken.file.posix#open/text */,0,C_SCHEME_UNDEFINED);
t57=C_set_block_item(lf[57] /* chicken.file.posix#open/trunc */,0,C_SCHEME_UNDEFINED);
t58=C_set_block_item(lf[58] /* chicken.file.posix#open/write */,0,C_SCHEME_UNDEFINED);
t59=C_set_block_item(lf[59] /* chicken.file.posix#open/wronly */,0,C_SCHEME_UNDEFINED);
t60=C_set_block_item(lf[60] /* chicken.file.posix#perm/irgrp */,0,C_SCHEME_UNDEFINED);
t61=C_set_block_item(lf[61] /* chicken.file.posix#perm/iroth */,0,C_SCHEME_UNDEFINED);
t62=C_set_block_item(lf[62] /* chicken.file.posix#perm/irusr */,0,C_SCHEME_UNDEFINED);
t63=C_set_block_item(lf[63] /* chicken.file.posix#perm/irwxg */,0,C_SCHEME_UNDEFINED);
t64=C_set_block_item(lf[64] /* chicken.file.posix#perm/irwxo */,0,C_SCHEME_UNDEFINED);
t65=C_set_block_item(lf[65] /* chicken.file.posix#perm/irwxu */,0,C_SCHEME_UNDEFINED);
t66=C_set_block_item(lf[66] /* chicken.file.posix#perm/isgid */,0,C_SCHEME_UNDEFINED);
t67=C_set_block_item(lf[67] /* chicken.file.posix#perm/isuid */,0,C_SCHEME_UNDEFINED);
t68=C_set_block_item(lf[68] /* chicken.file.posix#perm/isvtx */,0,C_SCHEME_UNDEFINED);
t69=C_set_block_item(lf[69] /* chicken.file.posix#perm/iwgrp */,0,C_SCHEME_UNDEFINED);
t70=C_set_block_item(lf[70] /* chicken.file.posix#perm/iwoth */,0,C_SCHEME_UNDEFINED);
t71=C_set_block_item(lf[71] /* chicken.file.posix#perm/iwusr */,0,C_SCHEME_UNDEFINED);
t72=C_set_block_item(lf[72] /* chicken.file.posix#perm/ixgrp */,0,C_SCHEME_UNDEFINED);
t73=C_set_block_item(lf[73] /* chicken.file.posix#perm/ixoth */,0,C_SCHEME_UNDEFINED);
t74=C_set_block_item(lf[74] /* chicken.file.posix#perm/ixusr */,0,C_SCHEME_UNDEFINED);
t75=C_set_block_item(lf[75] /* chicken.file.posix#port->fileno */,0,C_SCHEME_UNDEFINED);
t76=C_set_block_item(lf[76] /* chicken.file.posix#seek/cur */,0,C_SCHEME_UNDEFINED);
t77=C_set_block_item(lf[77] /* chicken.file.posix#seek/end */,0,C_SCHEME_UNDEFINED);
t78=C_set_block_item(lf[78] /* chicken.file.posix#seek/set */,0,C_SCHEME_UNDEFINED);
t79=C_set_block_item(lf[79] /* chicken.file.posix#set-file-position! */,0,C_SCHEME_UNDEFINED);
t80=C_a_i_provide(&a,1,lf[80]);
t81=C_set_block_item(lf[81] /* chicken.time.posix#seconds->utc-time */,0,C_SCHEME_UNDEFINED);
t82=C_set_block_item(lf[82] /* chicken.time.posix#utc-time->seconds */,0,C_SCHEME_UNDEFINED);
t83=C_set_block_item(lf[83] /* chicken.time.posix#seconds->local-time */,0,C_SCHEME_UNDEFINED);
t84=C_set_block_item(lf[84] /* chicken.time.posix#seconds->string */,0,C_SCHEME_UNDEFINED);
t85=C_set_block_item(lf[85] /* chicken.time.posix#local-time->seconds */,0,C_SCHEME_UNDEFINED);
t86=C_set_block_item(lf[86] /* chicken.time.posix#string->time */,0,C_SCHEME_UNDEFINED);
t87=C_set_block_item(lf[87] /* chicken.time.posix#time->string */,0,C_SCHEME_UNDEFINED);
t88=C_set_block_item(lf[88] /* chicken.time.posix#local-timezone-abbreviation */,0,C_SCHEME_UNDEFINED);
t89=C_a_i_provide(&a,1,lf[89]);
t90=C_mutate((C_word*)lf[90]+1 /* (set! chicken.process#system ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2331,a[2]=((C_word)li0),tmp=(C_word)a,a+=3,tmp));
t91=C_mutate((C_word*)lf[96]+1 /* (set! chicken.process#system* ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2349,a[2]=((C_word)li1),tmp=(C_word)a,a+=3,tmp));
t92=C_mutate((C_word*)lf[99]+1 /* (set! chicken.process#qs ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2365,a[2]=((C_word)li4),tmp=(C_word)a,a+=3,tmp));
t93=C_set_block_item(lf[109] /* chicken.process#process-execute */,0,C_SCHEME_UNDEFINED);
t94=C_set_block_item(lf[110] /* chicken.process#process-fork */,0,C_SCHEME_UNDEFINED);
t95=C_set_block_item(lf[111] /* chicken.process#process-run */,0,C_SCHEME_UNDEFINED);
t96=C_set_block_item(lf[112] /* chicken.process#process-signal */,0,C_SCHEME_UNDEFINED);
t97=C_set_block_item(lf[113] /* chicken.process#process-spawn */,0,C_SCHEME_UNDEFINED);
t98=C_set_block_item(lf[114] /* chicken.process#process-wait */,0,C_SCHEME_UNDEFINED);
t99=C_set_block_item(lf[115] /* chicken.process#call-with-input-pipe */,0,C_SCHEME_UNDEFINED);
t100=C_set_block_item(lf[116] /* chicken.process#call-with-output-pipe */,0,C_SCHEME_UNDEFINED);
t101=C_set_block_item(lf[117] /* chicken.process#close-input-pipe */,0,C_SCHEME_UNDEFINED);
t102=C_set_block_item(lf[118] /* chicken.process#close-output-pipe */,0,C_SCHEME_UNDEFINED);
t103=C_set_block_item(lf[119] /* chicken.process#create-pipe */,0,C_SCHEME_UNDEFINED);
t104=C_set_block_item(lf[120] /* chicken.process#open-input-pipe */,0,C_SCHEME_UNDEFINED);
t105=C_set_block_item(lf[121] /* chicken.process#open-output-pipe */,0,C_SCHEME_UNDEFINED);
t106=C_set_block_item(lf[122] /* chicken.process#with-input-from-pipe */,0,C_SCHEME_UNDEFINED);
t107=C_set_block_item(lf[123] /* chicken.process#with-output-to-pipe */,0,C_SCHEME_UNDEFINED);
t108=C_set_block_item(lf[124] /* chicken.process#process */,0,C_SCHEME_UNDEFINED);
t109=C_set_block_item(lf[125] /* chicken.process#process* */,0,C_SCHEME_UNDEFINED);
t110=C_set_block_item(lf[126] /* chicken.process#process-sleep */,0,C_SCHEME_UNDEFINED);
t111=C_set_block_item(lf[127] /* chicken.process#pipe/buf */,0,C_SCHEME_UNDEFINED);
t112=C_set_block_item(lf[128] /* chicken.process#spawn/overlay */,0,C_SCHEME_UNDEFINED);
t113=C_set_block_item(lf[129] /* chicken.process#spawn/wait */,0,C_SCHEME_UNDEFINED);
t114=C_set_block_item(lf[130] /* chicken.process#spawn/nowait */,0,C_SCHEME_UNDEFINED);
t115=C_set_block_item(lf[131] /* chicken.process#spawn/nowaito */,0,C_SCHEME_UNDEFINED);
t116=C_set_block_item(lf[132] /* chicken.process#spawn/detach */,0,C_SCHEME_UNDEFINED);
t117=C_a_i_provide(&a,1,lf[133]);
t118=C_set_block_item(lf[134] /* chicken.process.signal#set-alarm! */,0,C_SCHEME_UNDEFINED);
t119=C_set_block_item(lf[135] /* chicken.process.signal#set-signal-handler! */,0,C_SCHEME_UNDEFINED);
t120=C_set_block_item(lf[136] /* chicken.process.signal#set-signal-mask! */,0,C_SCHEME_UNDEFINED);
t121=C_set_block_item(lf[137] /* chicken.process.signal#signal-handler */,0,C_SCHEME_UNDEFINED);
t122=C_set_block_item(lf[138] /* chicken.process.signal#signal-mask */,0,C_SCHEME_UNDEFINED);
t123=C_set_block_item(lf[139] /* chicken.process.signal#signal-mask! */,0,C_SCHEME_UNDEFINED);
t124=C_set_block_item(lf[140] /* chicken.process.signal#signal-masked? */,0,C_SCHEME_UNDEFINED);
t125=C_set_block_item(lf[141] /* chicken.process.signal#signal-unmask! */,0,C_SCHEME_UNDEFINED);
t126=C_set_block_item(lf[142] /* chicken.process.signal#signal/abrt */,0,C_SCHEME_UNDEFINED);
t127=C_set_block_item(lf[143] /* chicken.process.signal#signal/alrm */,0,C_SCHEME_UNDEFINED);
t128=C_set_block_item(lf[144] /* chicken.process.signal#signal/break */,0,C_SCHEME_UNDEFINED);
t129=C_set_block_item(lf[145] /* chicken.process.signal#signal/bus */,0,C_SCHEME_UNDEFINED);
t130=C_set_block_item(lf[146] /* chicken.process.signal#signal/chld */,0,C_SCHEME_UNDEFINED);
t131=C_set_block_item(lf[147] /* chicken.process.signal#signal/cont */,0,C_SCHEME_UNDEFINED);
t132=C_set_block_item(lf[148] /* chicken.process.signal#signal/fpe */,0,C_SCHEME_UNDEFINED);
t133=C_set_block_item(lf[149] /* chicken.process.signal#signal/hup */,0,C_SCHEME_UNDEFINED);
t134=C_set_block_item(lf[150] /* chicken.process.signal#signal/ill */,0,C_SCHEME_UNDEFINED);
t135=C_set_block_item(lf[151] /* chicken.process.signal#signal/int */,0,C_SCHEME_UNDEFINED);
t136=C_set_block_item(lf[152] /* chicken.process.signal#signal/io */,0,C_SCHEME_UNDEFINED);
t137=C_set_block_item(lf[153] /* chicken.process.signal#signal/kill */,0,C_SCHEME_UNDEFINED);
t138=C_set_block_item(lf[154] /* chicken.process.signal#signal/pipe */,0,C_SCHEME_UNDEFINED);
t139=C_set_block_item(lf[155] /* chicken.process.signal#signal/prof */,0,C_SCHEME_UNDEFINED);
t140=C_set_block_item(lf[156] /* chicken.process.signal#signal/quit */,0,C_SCHEME_UNDEFINED);
t141=C_set_block_item(lf[157] /* chicken.process.signal#signal/segv */,0,C_SCHEME_UNDEFINED);
t142=C_set_block_item(lf[158] /* chicken.process.signal#signal/stop */,0,C_SCHEME_UNDEFINED);
t143=C_set_block_item(lf[159] /* chicken.process.signal#signal/term */,0,C_SCHEME_UNDEFINED);
t144=C_set_block_item(lf[160] /* chicken.process.signal#signal/trap */,0,C_SCHEME_UNDEFINED);
t145=C_set_block_item(lf[161] /* chicken.process.signal#signal/tstp */,0,C_SCHEME_UNDEFINED);
t146=C_set_block_item(lf[162] /* chicken.process.signal#signal/urg */,0,C_SCHEME_UNDEFINED);
t147=C_set_block_item(lf[163] /* chicken.process.signal#signal/usr1 */,0,C_SCHEME_UNDEFINED);
t148=C_set_block_item(lf[164] /* chicken.process.signal#signal/usr2 */,0,C_SCHEME_UNDEFINED);
t149=C_set_block_item(lf[165] /* chicken.process.signal#signal/vtalrm */,0,C_SCHEME_UNDEFINED);
t150=C_set_block_item(lf[166] /* chicken.process.signal#signal/winch */,0,C_SCHEME_UNDEFINED);
t151=C_set_block_item(lf[167] /* chicken.process.signal#signal/xcpu */,0,C_SCHEME_UNDEFINED);
t152=C_set_block_item(lf[168] /* chicken.process.signal#signal/xfsz */,0,C_SCHEME_UNDEFINED);
t153=C_set_block_item(lf[169] /* chicken.process.signal#signals-list */,0,C_SCHEME_UNDEFINED);
t154=C_a_i_provide(&a,1,lf[170]);
t155=C_set_block_item(lf[171] /* chicken.process-context.posix#change-directory* */,0,C_SCHEME_UNDEFINED);
t156=C_set_block_item(lf[172] /* chicken.process-context.posix#set-root-directory! */,0,C_SCHEME_UNDEFINED);
t157=C_set_block_item(lf[173] /* chicken.process-context.posix#current-effective-group-id */,0,C_SCHEME_UNDEFINED);
t158=C_set_block_item(lf[174] /* chicken.process-context.posix#current-effective-user-id */,0,C_SCHEME_UNDEFINED);
t159=C_set_block_item(lf[175] /* chicken.process-context.posix#current-group-id */,0,C_SCHEME_UNDEFINED);
t160=C_set_block_item(lf[176] /* chicken.process-context.posix#current-user-id */,0,C_SCHEME_UNDEFINED);
t161=C_set_block_item(lf[177] /* chicken.process-context.posix#current-process-id */,0,C_SCHEME_UNDEFINED);
t162=C_set_block_item(lf[178] /* chicken.process-context.posix#parent-process-id */,0,C_SCHEME_UNDEFINED);
t163=C_set_block_item(lf[179] /* chicken.process-context.posix#current-user-name */,0,C_SCHEME_UNDEFINED);
t164=C_set_block_item(lf[180] /* chicken.process-context.posix#current-effective-user-name */,0,C_SCHEME_UNDEFINED);
t165=C_set_block_item(lf[181] /* chicken.process-context.posix#create-session */,0,C_SCHEME_UNDEFINED);
t166=C_set_block_item(lf[182] /* chicken.process-context.posix#process-group-id */,0,C_SCHEME_UNDEFINED);
t167=C_set_block_item(lf[183] /* chicken.process-context.posix#user-information */,0,C_SCHEME_UNDEFINED);
t168=C_a_i_provide(&a,1,lf[184]);
t169=C_mutate(&lf[185] /* (set! chicken.posix#posix-error ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2552,a[2]=((C_word)li5),tmp=(C_word)a,a+=3,tmp));
t170=C_mutate((C_word*)lf[188]+1 /* (set! ##sys#posix-error ...) */,lf[185]);
t171=C_mutate(&lf[189] /* (set! chicken.posix#stat ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2570,a[2]=((C_word)li6),tmp=(C_word)a,a+=3,tmp));
t172=C_mutate((C_word*)lf[196]+1 /* (set! chicken.file.posix#file-stat ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2616,a[2]=((C_word)li7),tmp=(C_word)a,a+=3,tmp));
t173=C_mutate((C_word*)lf[198]+1 /* (set! chicken.file.posix#set-file-permissions! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2634,a[2]=((C_word)li8),tmp=(C_word)a,a+=3,tmp));
t174=C_mutate((C_word*)lf[204]+1 /* (set! chicken.file.posix#file-modification-time ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2678,a[2]=((C_word)li9),tmp=(C_word)a,a+=3,tmp));
t175=C_mutate((C_word*)lf[206]+1 /* (set! chicken.file.posix#file-access-time ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2684,a[2]=((C_word)li10),tmp=(C_word)a,a+=3,tmp));
t176=C_mutate((C_word*)lf[208]+1 /* (set! chicken.file.posix#file-change-time ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2690,a[2]=((C_word)li11),tmp=(C_word)a,a+=3,tmp));
t177=C_mutate((C_word*)lf[210]+1 /* (set! chicken.file.posix#set-file-times! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2696,a[2]=((C_word)li12),tmp=(C_word)a,a+=3,tmp));
t178=C_mutate((C_word*)lf[215]+1 /* (set! chicken.file.posix#file-size ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2775,a[2]=((C_word)li13),tmp=(C_word)a,a+=3,tmp));
t179=C_mutate((C_word*)lf[217]+1 /* (set! chicken.file.posix#set-file-owner! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2781,a[2]=((C_word)li14),tmp=(C_word)a,a+=3,tmp));
t180=C_mutate((C_word*)lf[220]+1 /* (set! chicken.file.posix#set-file-group! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2787,a[2]=((C_word)li15),tmp=(C_word)a,a+=3,tmp));
t181=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2795,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t182=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5599,a[2]=((C_word)li163),tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:312: chicken.base#getter-with-setter */
t183=*((C_word*)lf[395]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t183;
av2[1]=t181;
av2[2]=t182;
av2[3]=*((C_word*)lf[217]+1);
av2[4]=lf[407];
((C_proc)(void*)(*((C_word*)t183+1)))(5,av2);}}

/* chicken.process#system in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2331(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_2331,c,av);}
a=C_alloc(4);
t3=C_i_check_string_2(t2,lf[91]);
t4=C_execute_shell_command(t2);
if(C_truep(C_fixnum_lessp(t4,C_fix(0)))){
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2344,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* posix.scm:202: ##sys#update-errno */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[95]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[95]+1);
av2[1]=t5;
tp(2,av2);}}
else{
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* k2342 in chicken.process#system in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2344(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_2344,c,av);}
/* posix.scm:203: ##sys#signal-hook */
t2=*((C_word*)lf[92]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[93];
av2[3]=lf[91];
av2[4]=lf[94];
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* chicken.process#system* in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2349(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_2349,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2353,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* posix.scm:209: system */
t4=*((C_word*)lf[90]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k2351 in chicken.process#system* in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2353(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_2353,c,av);}
t2=C_eqp(t1,C_fix(0));
if(C_truep(t2)){
t3=C_SCHEME_UNDEFINED;
t4=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
/* posix.scm:211: ##sys#error */
t3=*((C_word*)lf[97]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[98];
av2[3]=((C_word*)t0)[3];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}}

/* chicken.process#qs in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2365(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_2365,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2369,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_rest_nullp(c,3))){
/* posix.scm:216: chicken.platform#software-version */
t4=*((C_word*)lf[108]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_get_rest_arg(c,3,av,3,t0);
f_2369(2,av2);}}}

/* k2367 in chicken.process#qs in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2369(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,3)))){
C_save_and_reclaim((void *)f_2369,c,av);}
a=C_alloc(18);
t2=C_eqp(t1,lf[100]);
t3=(C_truep(t2)?C_make_character(34):C_make_character(39));
t4=C_eqp(t1,lf[100]);
t5=(C_truep(t4)?lf[101]:lf[102]);
t6=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t7=t6;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=((C_word*)t8)[1];
t10=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2379,a[2]=t3,a[3]=t5,a[4]=((C_word*)t0)[2],a[5]=((C_word)li2),tmp=(C_word)a,a+=6,tmp);
t11=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_2400,a[2]=t3,a[3]=((C_word*)t0)[3],a[4]=t8,a[5]=t10,a[6]=t9,tmp=(C_word)a,a+=7,tmp);
/* ##sys#string->list */
t12=*((C_word*)lf[107]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t12;
av2[1]=t11;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t12+1)))(3,av2);}}

/* g256 in k2367 in chicken.process#qs in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_fcall f_2379(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(2,0,4)))){
C_save_and_reclaim_args((void *)trf_2379,3,t0,t1,t2);}
a=C_alloc(2);
if(C_truep(C_i_char_equalp(t2,((C_word*)t0)[2]))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
if(C_truep(C_u_i_char_equalp(t2,C_make_character(0)))){
/* posix.scm:224: chicken.base#error */
t3=*((C_word*)lf[103]+1);{
C_word av2[5];
av2[0]=t3;
av2[1]=t1;
av2[2]=lf[104];
av2[3]=lf[105];
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_a_i_string(&a,1,t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}}

/* k2398 in k2367 in chicken.process#qs in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2400(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,3)))){
C_save_and_reclaim((void *)f_2400,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2403,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_2420,a[2]=((C_word*)t0)[4],a[3]=t4,a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word)li3),tmp=(C_word)a,a+=7,tmp));
t6=((C_word*)t4)[1];
f_2420(t6,t2,t1);}

/* k2401 in k2398 in k2367 in chicken.process#qs in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2403(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_2403,c,av);}
a=C_alloc(7);
t2=C_a_i_string(&a,1,((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2414,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t2,tmp=(C_word)a,a+=5,tmp);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=t3;
av2[2]=*((C_word*)lf[106]+1);
av2[3]=t1;
C_apply(4,av2);}}

/* k2412 in k2401 in k2398 in k2367 in chicken.process#qs in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2414(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(2,c,4)))){
C_save_and_reclaim((void *)f_2414,c,av);}
a=C_alloc(2);
t2=C_a_i_string(&a,1,((C_word*)t0)[2]);
/* posix.scm:227: scheme#string-append */
t3=*((C_word*)lf[106]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=t1;
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* map-loop250 in k2398 in k2367 in chicken.process#qs in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_fcall f_2420(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_2420,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2445,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* posix.scm:220: g256 */
t4=((C_word*)t0)[4];
f_2379(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k2443 in map-loop250 in k2398 in k2367 in chicken.process#qs in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2445(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2445,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_2420(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* chicken.posix#posix-error in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2552(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c<5) C_bad_min_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-5)*C_SIZEOF_PAIR +7,c,2)))){
C_save_and_reclaim((void*)f_2552,c,av);}
a=C_alloc((c-5)*C_SIZEOF_PAIR+7);
t5=C_build_rest(&a,c,5,av);
C_word t6;
C_word t7;
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_2556,a[2]=t1,a[3]=t2,a[4]=t3,a[5]=t5,a[6]=t4,tmp=(C_word)a,a+=7,tmp);
/* posix-common.scm:191: ##sys#update-errno */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[95]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[95]+1);
av2[1]=t6;
tp(2,av2);}}

/* k2554 in chicken.posix#posix-error in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2556(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,3)))){
C_save_and_reclaim((void *)f_2556,c,av);}
a=C_alloc(15);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2563,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2567,a[2]=t2,a[3]=((C_word*)t0)[6],tmp=(C_word)a,a+=4,tmp);
t4=C_a_i_bytevector(&a,1,C_fix(3));
t5=C_i_foreign_fixnum_argumentp(t1);
/* posix-common.scm:188: ##sys#peek-c-string */
t6=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t3;
av2[2]=stub633(t4,t5);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* k2561 in k2554 in chicken.posix#posix-error in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2563(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_2563,c,av);}{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=*((C_word*)lf[92]+1);
av2[3]=((C_word*)t0)[3];
av2[4]=((C_word*)t0)[4];
av2[5]=t1;
av2[6]=((C_word*)t0)[5];
C_apply(7,av2);}}

/* k2565 in k2554 in chicken.posix#posix-error in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2567(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_2567,c,av);}
/* posix-common.scm:192: string-append */
t2=*((C_word*)lf[106]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=lf[186];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* chicken.posix#stat in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_fcall f_2570(C_word t1,C_word t2,C_word t3,C_word t4,C_word t5){
C_word tmp;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,2)))){
C_save_and_reclaim_args((void *)trf_2570,5,t1,t2,t3,t4,t5);}
a=C_alloc(12);
t6=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2574,a[2]=t4,a[3]=t1,a[4]=t5,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_fixnump(t2))){
t7=t6;{
C_word av2[2];
av2[0]=t7;
av2[1]=C_u_i_fstat(t2);
f_2574(2,av2);}}
else{
t7=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2595,a[2]=t6,a[3]=t2,a[4]=t3,a[5]=t5,tmp=(C_word)a,a+=6,tmp);
/* posix-common.scm:235: chicken.base#port? */
t8=*((C_word*)lf[195]+1);{
C_word av2[3];
av2[0]=t8;
av2[1]=t7;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t8+1)))(3,av2);}}}

/* k2572 in chicken.posix#stat in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2574(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_2574,c,av);}
if(C_truep(C_fixnum_lessp(t1,C_fix(0)))){
if(C_truep(((C_word*)t0)[2])){
/* posix-common.scm:246: posix-error */
t2=lf[185];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[190];
av2[3]=((C_word*)t0)[4];
av2[4]=lf[191];
av2[5]=((C_word*)t0)[5];
f_2552(6,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k2593 in chicken.posix#stat in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2595(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,5)))){
C_save_and_reclaim((void *)f_2595,c,av);}
a=C_alloc(4);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2599,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:235: chicken.file.posix#port->fileno */
t3=*((C_word*)lf[75]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
if(C_truep(C_i_stringp(((C_word*)t0)[3]))){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2608,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:237: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[192]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[192]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[5];
tp(4,av2);}}
else{
/* posix-common.scm:242: ##sys#signal-hook */
t2=*((C_word*)lf[92]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[193];
av2[3]=((C_word*)t0)[5];
av2[4]=lf[194];
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}}}

/* k2597 in k2593 in chicken.posix#stat in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2599(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2599,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_u_i_fstat(t1);
f_2574(2,av2);}}

/* k2606 in k2593 in chicken.posix#stat in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2608(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2608,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(C_truep(((C_word*)t0)[3])?C_u_i_lstat(t1):C_u_i_stat(t1));
f_2574(2,av2);}}

/* chicken.file.posix#file-stat in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2616(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_2616,c,av);}
a=C_alloc(3);
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,3,t0));
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2623,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:252: stat */
f_2570(t5,t2,t4,C_SCHEME_TRUE,lf[197]);}

/* k2621 in chicken.file.posix#file-stat in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2623(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(42,c,1)))){
C_save_and_reclaim((void *)f_2623,c,av);}
a=C_alloc(42);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_vector(&a,13,C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)C_statbuf.st_ino),C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)C_statbuf.st_mode),C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)C_statbuf.st_nlink),C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)C_statbuf.st_uid),C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)C_statbuf.st_gid),C_int64_to_num(&a,C_statbuf.st_size),C_int64_to_num(&a,C_statbuf.st_atime),C_int64_to_num(&a,C_statbuf.st_ctime),C_int64_to_num(&a,C_statbuf.st_mtime),C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)C_statbuf.st_dev),C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)C_statbuf.st_rdev),((C_word)C_SCHEME_UNDEFINED),((C_word)C_SCHEME_UNDEFINED));
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.file.posix#set-file-permissions! in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2634(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_2634,c,av);}
a=C_alloc(5);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2638,a[2]=t1,a[3]=t2,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:261: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[203]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[203]+1);
av2[1]=t4;
av2[2]=t3;
av2[3]=lf[199];
tp(4,av2);}}

/* k2636 in chicken.file.posix#set-file-permissions! in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2638(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_2638,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2641,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_fixnump(((C_word*)t0)[3]))){
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_fchmod(((C_word*)t0)[3],((C_word*)t0)[4]);
f_2641(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2659,a[2]=t2,a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:263: chicken.base#port? */
t4=*((C_word*)lf[195]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}}

/* k2639 in k2636 in chicken.file.posix#set-file-permissions! in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2641(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_2641,c,av);}
if(C_truep(C_fixnum_lessp(t1,C_fix(0)))){
/* posix-common.scm:272: posix-error */
t2=lf[185];{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[190];
av2[3]=lf[199];
av2[4]=lf[200];
av2[5]=((C_word*)t0)[3];
av2[6]=((C_word*)t0)[4];
f_2552(7,av2);}}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k2657 in k2636 in chicken.file.posix#set-file-permissions! in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2659(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,5)))){
C_save_and_reclaim((void *)f_2659,c,av);}
a=C_alloc(4);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2663,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:263: chicken.file.posix#port->fileno */
t3=*((C_word*)lf[75]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
if(C_truep(C_i_stringp(((C_word*)t0)[4]))){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2673,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:266: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[192]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[192]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=lf[199];
tp(4,av2);}}
else{
/* posix-common.scm:268: ##sys#signal-hook */
t2=*((C_word*)lf[92]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[193];
av2[3]=lf[201];
av2[4]=lf[202];
av2[5]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}}}

/* k2661 in k2657 in k2636 in chicken.file.posix#set-file-permissions! in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2663(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2663,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_fchmod(t1,((C_word*)t0)[3]);
f_2641(2,av2);}}

/* k2671 in k2657 in k2636 in chicken.file.posix#set-file-permissions! in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2673(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2673,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_chmod(t1,((C_word*)t0)[3]);
f_2641(2,av2);}}

/* chicken.file.posix#file-modification-time in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2678(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_2678,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2682,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:276: stat */
f_2570(t3,t2,C_SCHEME_FALSE,C_SCHEME_TRUE,lf[205]);}

/* k2680 in chicken.file.posix#file-modification-time in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2682(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,1)))){
C_save_and_reclaim((void *)f_2682,c,av);}
a=C_alloc(7);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_int64_to_num(&a,C_statbuf.st_mtime);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.file.posix#file-access-time in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2684(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_2684,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2688,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:280: stat */
f_2570(t3,t2,C_SCHEME_FALSE,C_SCHEME_TRUE,lf[207]);}

/* k2686 in chicken.file.posix#file-access-time in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2688(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,1)))){
C_save_and_reclaim((void *)f_2688,c,av);}
a=C_alloc(7);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_int64_to_num(&a,C_statbuf.st_atime);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.file.posix#file-change-time in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2690(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_2690,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2694,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:284: stat */
f_2570(t3,t2,C_SCHEME_FALSE,C_SCHEME_TRUE,lf[209]);}

/* k2692 in chicken.file.posix#file-change-time in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2694(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,1)))){
C_save_and_reclaim((void *)f_2694,c,av);}
a=C_alloc(7);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_int64_to_num(&a,C_statbuf.st_ctime);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.file.posix#set-file-times! in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2696(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +5,c,2)))){
C_save_and_reclaim((void*)f_2696,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+5);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2700,a[2]=t3,a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_i_nullp(t3))){
/* posix-common.scm:289: chicken.time#current-seconds */
t5=*((C_word*)lf[214]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t5=t4;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_i_car(t3);
f_2700(2,av2);}}}

/* k2698 in chicken.file.posix#set-file-times! in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2700(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_2700,c,av);}
a=C_alloc(7);
t2=C_i_nullp(((C_word*)t0)[2]);
t3=(C_truep(t2)?C_SCHEME_END_OF_LIST:C_i_cdr(((C_word*)t0)[2]));
t4=C_i_nullp(t3);
t5=(C_truep(t4)?t1:C_i_car(t3));
t6=C_i_nullp(t3);
t7=(C_truep(t6)?C_SCHEME_END_OF_LIST:C_i_cdr(t3));
t8=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_2712,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[2],a[5]=t1,a[6]=t5,tmp=(C_word)a,a+=7,tmp);
if(C_truep(t1)){
/* posix-common.scm:290: ##sys#check-exact-integer */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[213]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[213]+1);
av2[1]=t8;
av2[2]=t1;
av2[3]=lf[211];
tp(4,av2);}}
else{
t9=t8;{
C_word *av2=av;
av2[0]=t9;
av2[1]=C_SCHEME_UNDEFINED;
f_2712(2,av2);}}}

/* k2710 in k2698 in chicken.file.posix#set-file-times! in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2712(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_2712,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_2715,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
if(C_truep(((C_word*)t0)[6])){
/* posix-common.scm:291: ##sys#check-exact-integer */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[213]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[213]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[6];
av2[3]=lf[211];
tp(4,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_2715(2,av2);}}}

/* k2713 in k2710 in k2698 in chicken.file.posix#set-file-times! in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2715(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_2715,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2731,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2721,a[2]=t2,a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],tmp=(C_word)a,a+=5,tmp);
if(C_truep(((C_word*)t0)[3])){
/* posix-common.scm:292: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[192]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[192]+1);
av2[1]=t3;
av2[2]=C_i_foreign_string_argumentp(((C_word*)t0)[3]);
tp(3,av2);}}
else{
t4=t2;
f_2731(t4,stub707(C_SCHEME_UNDEFINED,C_SCHEME_FALSE,((C_word*)t0)[5],((C_word*)t0)[6]));}}

/* k2719 in k2713 in k2710 in k2698 in chicken.file.posix#set-file-times! in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2721(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2721,c,av);}
t2=((C_word*)t0)[2];
f_2731(t2,stub707(C_SCHEME_UNDEFINED,t1,((C_word*)t0)[3],((C_word*)t0)[4]));}

/* k2729 in k2713 in k2710 in k2698 in chicken.file.posix#set-file-times! in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_fcall f_2731(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,7)))){
C_save_and_reclaim_args((void *)trf_2731,2,t0,t1);}
if(C_truep(C_fixnum_lessp(t1,C_fix(0)))){{
C_word av2[8];
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[185];
av2[3]=lf[190];
av2[4]=lf[211];
av2[5]=lf[212];
av2[6]=((C_word*)t0)[3];
av2[7]=((C_word*)t0)[4];
C_apply(8,av2);}}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* chicken.file.posix#file-size in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2775(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_2775,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2779,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:301: stat */
f_2570(t3,t2,C_SCHEME_FALSE,C_SCHEME_TRUE,lf[216]);}

/* k2777 in chicken.file.posix#file-size in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2779(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,1)))){
C_save_and_reclaim((void *)f_2779,c,av);}
a=C_alloc(7);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_int64_to_num(&a,C_statbuf.st_size);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.file.posix#set-file-owner! in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2781(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_2781,c,av);}
a=C_alloc(12);
t4=C_a_i_list(&a,4,lf[218],t2,t3,C_fix(-1));
/* posixwin.scm:877: chicken.base#error */
t5=*((C_word*)lf[103]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t1;
av2[2]=lf[219];
av2[3]=lf[0];
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* chicken.file.posix#set-file-group! in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2787(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_2787,c,av);}
a=C_alloc(12);
t4=C_a_i_list(&a,4,lf[221],t2,C_fix(-1),t3);
/* posixwin.scm:877: chicken.base#error */
t5=*((C_word*)lf[103]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t1;
av2[2]=lf[219];
av2[3]=lf[0];
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2795(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_2795,c,av);}
a=C_alloc(6);
t2=C_mutate((C_word*)lf[22]+1 /* (set! chicken.file.posix#file-owner ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2799,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5593,a[2]=((C_word)li162),tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:318: chicken.base#getter-with-setter */
t5=*((C_word*)lf[395]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
av2[3]=*((C_word*)lf[220]+1);
av2[4]=lf[405];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}

/* k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2799(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_2799,c,av);}
a=C_alloc(6);
t2=C_mutate((C_word*)lf[16]+1 /* (set! chicken.file.posix#file-group ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2803,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5587,a[2]=((C_word)li161),tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:324: chicken.base#getter-with-setter */
t5=*((C_word*)lf[395]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
av2[3]=*((C_word*)lf[198]+1);
av2[4]=lf[403];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}

/* k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2803(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(33,c,4)))){
C_save_and_reclaim((void *)f_2803,c,av);}
a=C_alloc(33);
t2=C_mutate((C_word*)lf[23]+1 /* (set! chicken.file.posix#file-permissions ...) */,t1);
t3=C_mutate((C_word*)lf[31]+1 /* (set! chicken.file.posix#file-type ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2805,a[2]=((C_word)li16),tmp=(C_word)a,a+=3,tmp));
t4=C_mutate((C_word*)lf[36]+1 /* (set! chicken.file.posix#regular-file? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2892,a[2]=((C_word)li17),tmp=(C_word)a,a+=3,tmp));
t5=C_mutate((C_word*)lf[38]+1 /* (set! chicken.file.posix#symbolic-link? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2902,a[2]=((C_word)li18),tmp=(C_word)a,a+=3,tmp));
t6=C_mutate((C_word*)lf[32]+1 /* (set! chicken.file.posix#block-device? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2912,a[2]=((C_word)li19),tmp=(C_word)a,a+=3,tmp));
t7=C_mutate((C_word*)lf[33]+1 /* (set! chicken.file.posix#character-device? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2922,a[2]=((C_word)li20),tmp=(C_word)a,a+=3,tmp));
t8=C_mutate((C_word*)lf[35]+1 /* (set! chicken.file.posix#fifo? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2932,a[2]=((C_word)li21),tmp=(C_word)a,a+=3,tmp));
t9=C_mutate((C_word*)lf[37]+1 /* (set! chicken.file.posix#socket? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2942,a[2]=((C_word)li22),tmp=(C_word)a,a+=3,tmp));
t10=C_mutate((C_word*)lf[34]+1 /* (set! chicken.file.posix#directory? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2952,a[2]=((C_word)li23),tmp=(C_word)a,a+=3,tmp));
t11=C_set_block_item(lf[78] /* chicken.file.posix#seek/set */,0,C_fix((C_word)SEEK_SET));
t12=C_set_block_item(lf[77] /* chicken.file.posix#seek/end */,0,C_fix((C_word)SEEK_END));
t13=C_set_block_item(lf[76] /* chicken.file.posix#seek/cur */,0,C_fix((C_word)SEEK_CUR));
t14=C_mutate((C_word*)lf[79]+1 /* (set! chicken.file.posix#set-file-position! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2965,a[2]=((C_word)li24),tmp=(C_word)a,a+=3,tmp));
t15=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3020,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t16=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5550,a[2]=((C_word)li160),tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:401: chicken.base#getter-with-setter */
t17=*((C_word*)lf[395]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t17;
av2[1]=t15;
av2[2]=t16;
av2[3]=*((C_word*)lf[79]+1);
av2[4]=lf[402];
((C_proc)(void*)(*((C_word*)t17+1)))(5,av2);}}

/* chicken.file.posix#file-type in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2805(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_2805,c,av);}
a=C_alloc(3);
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,3,t0));
t5=C_rest_nullp(c,3);
t6=C_rest_nullp(c,4);
t7=(C_truep(t6)?C_SCHEME_TRUE:C_get_rest_arg(c,4,av,3,t0));
t8=C_rest_nullp(c,4);
t9=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2824,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:333: stat */
f_2570(t9,t2,t4,t7,lf[229]);}

/* k2822 in chicken.file.posix#file-type in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2824(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2824,c,av);}
if(C_truep(t1)){
t2=C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)C_stat_type);
t3=C_eqp(t2,C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)S_IFREG));
if(C_truep(t3)){
t4=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t4;
av2[1]=lf[222];
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=C_eqp(t2,C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)S_IFLNK));
if(C_truep(t4)){
t5=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t5;
av2[1]=lf[223];
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t5=C_eqp(t2,C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)S_IFDIR));
if(C_truep(t5)){
t6=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t6;
av2[1]=lf[224];
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
t6=C_eqp(t2,C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)S_IFCHR));
if(C_truep(t6)){
t7=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t7;
av2[1]=lf[225];
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}
else{
t7=C_eqp(t2,C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)S_IFBLK));
if(C_truep(t7)){
t8=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t8;
av2[1]=lf[226];
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}
else{
t8=C_eqp(t2,C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)S_IFIFO));
if(C_truep(t8)){
t9=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t9;
av2[1]=lf[227];
((C_proc)(void*)(*((C_word*)t9+1)))(2,av2);}}
else{
t9=C_eqp(t2,C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)S_IFSOCK));
t10=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t10;
av2[1]=(C_truep(t9)?lf[228]:lf[222]);
((C_proc)(void*)(*((C_word*)t10+1)))(2,av2);}}}}}}}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* chicken.file.posix#regular-file? in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2892(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_2892,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2900,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:347: chicken.file.posix#file-type */
t4=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k2898 in chicken.file.posix#regular-file? in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2900(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2900,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_eqp(lf[222],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.file.posix#symbolic-link? in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2902(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_2902,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2910,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:351: chicken.file.posix#file-type */
t4=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=C_SCHEME_TRUE;
av2[4]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k2908 in chicken.file.posix#symbolic-link? in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2910(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2910,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_eqp(lf[223],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.file.posix#block-device? in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2912(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_2912,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2920,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:355: chicken.file.posix#file-type */
t4=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k2918 in chicken.file.posix#block-device? in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2920(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2920,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_eqp(lf[226],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.file.posix#character-device? in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2922(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_2922,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2930,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:359: chicken.file.posix#file-type */
t4=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k2928 in chicken.file.posix#character-device? in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2930(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2930,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_eqp(lf[225],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.file.posix#fifo? in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2932(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_2932,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2940,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:363: chicken.file.posix#file-type */
t4=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k2938 in chicken.file.posix#fifo? in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2940(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2940,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_eqp(lf[227],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.file.posix#socket? in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2942(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_2942,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2950,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:367: chicken.file.posix#file-type */
t4=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k2948 in chicken.file.posix#socket? in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2950(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2950,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_eqp(lf[228],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.file.posix#directory? in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2952(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_2952,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2960,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:371: chicken.file.posix#file-type */
t4=*((C_word*)lf[31]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k2958 in chicken.file.posix#directory? in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2960(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2960,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_eqp(lf[224],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.file.posix#set-file-position! in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2965(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c<4) C_bad_min_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-4)*C_SIZEOF_PAIR +6,c,3)))){
C_save_and_reclaim((void*)f_2965,c,av);}
a=C_alloc((c-4)*C_SIZEOF_PAIR+6);
t4=C_build_rest(&a,c,4,av);
C_word t5;
C_word t6;
C_word t7;
C_word t8;
t5=C_i_pairp(t4);
t6=(C_truep(t5)?C_get_rest_arg(c,4,av,4,t0):C_fix((C_word)SEEK_SET));
t7=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2972,a[2]=t1,a[3]=t2,a[4]=t3,a[5]=t6,tmp=(C_word)a,a+=6,tmp);
/* posix-common.scm:387: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[203]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[203]+1);
av2[1]=t7;
av2[2]=t3;
av2[3]=lf[230];
tp(4,av2);}}

/* k2970 in chicken.file.posix#set-file-position! in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2972(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2972,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2975,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* posix-common.scm:388: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[203]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[203]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=lf[230];
tp(4,av2);}}

/* k2973 in k2970 in chicken.file.posix#set-file-position! in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2975(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_2975,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2981,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2987,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* posix-common.scm:389: chicken.base#port? */
t4=*((C_word*)lf[195]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k2979 in k2973 in k2970 in chicken.file.posix#set-file-position! in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2981(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_2981,c,av);}
if(C_truep(t1)){
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
/* posix-common.scm:398: posix-error */
t2=lf[185];{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[190];
av2[3]=lf[230];
av2[4]=lf[231];
av2[5]=((C_word*)t0)[3];
av2[6]=((C_word*)t0)[4];
f_2552(7,av2);}}}

/* k2985 in k2973 in k2970 in chicken.file.posix#set-file-position! in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_2987(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_2987,c,av);}
if(C_truep(t1)){
t2=C_slot(((C_word*)t0)[2],C_fix(7));
t3=C_eqp(t2,lf[232]);
if(C_truep(t3)){
t4=C_fseek(((C_word*)t0)[2],((C_word*)t0)[3],((C_word*)t0)[4]);
if(C_truep(t4)){
t5=C_i_set_i_slot(((C_word*)t0)[2],C_fix(6),C_SCHEME_FALSE);
t6=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t6;
av2[1]=t4;
f_2981(2,av2);}}
else{
t5=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_FALSE;
f_2981(2,av2);}}}
else{
t4=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
f_2981(2,av2);}}}
else{
if(C_truep(C_fixnump(((C_word*)t0)[2]))){
t2=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_lseek(((C_word*)t0)[2],((C_word*)t0)[3],((C_word*)t0)[4]);
f_2981(2,av2);}}
else{
/* posix-common.scm:397: ##sys#signal-hook */
t2=*((C_word*)lf[92]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[5];
av2[2]=lf[193];
av2[3]=lf[230];
av2[4]=lf[233];
av2[5]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}}}

/* k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3020(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word t24;
C_word t25;
C_word t26;
C_word t27;
C_word t28;
C_word t29;
C_word t30;
C_word t31;
C_word t32;
C_word t33;
C_word t34;
C_word t35;
C_word t36;
C_word t37;
C_word t38;
C_word t39;
C_word t40;
C_word t41;
C_word t42;
C_word t43;
C_word t44;
C_word t45;
C_word t46;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(45,c,6)))){
C_save_and_reclaim((void *)f_3020,c,av);}
a=C_alloc(45);
t2=C_mutate((C_word*)lf[24]+1 /* (set! chicken.file.posix#file-position ...) */,t1);
t3=C_set_block_item(lf[40] /* chicken.file.posix#fileno/stdin */,0,C_fix((C_word)STDIN_FILENO));
t4=C_set_block_item(lf[41] /* chicken.file.posix#fileno/stdout */,0,C_fix((C_word)STDOUT_FILENO));
t5=C_set_block_item(lf[39] /* chicken.file.posix#fileno/stderr */,0,C_fix((C_word)STDERR_FILENO));
t6=C_set_block_item(lf[52] /* chicken.file.posix#open/rdonly */,0,C_fix((C_word)O_RDONLY));
t7=C_set_block_item(lf[59] /* chicken.file.posix#open/wronly */,0,C_fix((C_word)O_WRONLY));
t8=C_set_block_item(lf[53] /* chicken.file.posix#open/rdwr */,0,C_fix((C_word)O_RDWR));
t9=C_set_block_item(lf[54] /* chicken.file.posix#open/read */,0,C_fix((C_word)O_RDONLY));
t10=C_set_block_item(lf[58] /* chicken.file.posix#open/write */,0,C_fix((C_word)O_WRONLY));
t11=C_set_block_item(lf[46] /* chicken.file.posix#open/creat */,0,C_fix((C_word)O_CREAT));
t12=C_set_block_item(lf[44] /* chicken.file.posix#open/append */,0,C_fix((C_word)O_APPEND));
t13=C_set_block_item(lf[47] /* chicken.file.posix#open/excl */,0,C_fix((C_word)O_EXCL));
t14=C_set_block_item(lf[57] /* chicken.file.posix#open/trunc */,0,C_fix((C_word)O_TRUNC));
t15=C_set_block_item(lf[45] /* chicken.file.posix#open/binary */,0,C_fix((C_word)O_BINARY));
t16=C_set_block_item(lf[56] /* chicken.file.posix#open/text */,0,C_fix((C_word)O_TEXT));
t17=C_set_block_item(lf[62] /* chicken.file.posix#perm/irusr */,0,C_fix((C_word)S_IRUSR));
t18=C_set_block_item(lf[71] /* chicken.file.posix#perm/iwusr */,0,C_fix((C_word)S_IWUSR));
t19=C_set_block_item(lf[74] /* chicken.file.posix#perm/ixusr */,0,C_fix((C_word)S_IXUSR));
t20=C_set_block_item(lf[60] /* chicken.file.posix#perm/irgrp */,0,C_fix((C_word)S_IRGRP));
t21=C_set_block_item(lf[69] /* chicken.file.posix#perm/iwgrp */,0,C_fix((C_word)S_IWGRP));
t22=C_set_block_item(lf[72] /* chicken.file.posix#perm/ixgrp */,0,C_fix((C_word)S_IXGRP));
t23=C_set_block_item(lf[61] /* chicken.file.posix#perm/iroth */,0,C_fix((C_word)S_IROTH));
t24=C_set_block_item(lf[70] /* chicken.file.posix#perm/iwoth */,0,C_fix((C_word)S_IWOTH));
t25=C_set_block_item(lf[73] /* chicken.file.posix#perm/ixoth */,0,C_fix((C_word)S_IXOTH));
t26=C_set_block_item(lf[65] /* chicken.file.posix#perm/irwxu */,0,C_fix((C_word)S_IRUSR | S_IWUSR | S_IXUSR));
t27=C_set_block_item(lf[63] /* chicken.file.posix#perm/irwxg */,0,C_fix((C_word)S_IRGRP | S_IWGRP | S_IXGRP));
t28=C_set_block_item(lf[64] /* chicken.file.posix#perm/irwxo */,0,C_fix((C_word)S_IROTH | S_IWOTH | S_IXOTH));
t29=C_SCHEME_UNDEFINED;
t30=(*a=C_VECTOR_TYPE|1,a[1]=t29,tmp=(C_word)a,a+=2,tmp);
t31=C_SCHEME_UNDEFINED;
t32=(*a=C_VECTOR_TYPE|1,a[1]=t31,tmp=(C_word)a,a+=2,tmp);
t33=C_set_block_item(t30,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3048,a[2]=((C_word)li25),tmp=(C_word)a,a+=3,tmp));
t34=C_set_block_item(t32,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3085,a[2]=((C_word)li26),tmp=(C_word)a,a+=3,tmp));
t35=C_mutate((C_word*)lf[42]+1 /* (set! chicken.file.posix#open-input-file* ...) */,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3104,a[2]=t32,a[3]=t30,a[4]=((C_word)li27),tmp=(C_word)a,a+=5,tmp));
t36=C_mutate((C_word*)lf[43]+1 /* (set! chicken.file.posix#open-output-file* ...) */,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3118,a[2]=t32,a[3]=t30,a[4]=((C_word)li28),tmp=(C_word)a,a+=5,tmp));
t37=C_mutate((C_word*)lf[75]+1 /* (set! chicken.file.posix#port->fileno ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3132,a[2]=((C_word)li29),tmp=(C_word)a,a+=3,tmp));
t38=C_mutate((C_word*)lf[7]+1 /* (set! chicken.file.posix#duplicate-fileno ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3177,a[2]=((C_word)li30),tmp=(C_word)a,a+=3,tmp));
t39=C_mutate((C_word*)lf[177]+1 /* (set! chicken.process-context.posix#current-process-id ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3204,a[2]=((C_word)li31),tmp=(C_word)a,a+=3,tmp));
t40=C_mutate((C_word*)lf[171]+1 /* (set! chicken.process-context.posix#change-directory* ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3207,a[2]=((C_word)li32),tmp=(C_word)a,a+=3,tmp));
t41=*((C_word*)lf[255]+1);
t42=C_mutate((C_word*)lf[255]+1 /* (set! ##sys#change-directory-hook ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3223,a[2]=t41,a[3]=((C_word)li33),tmp=(C_word)a,a+=4,tmp));
t43=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3237,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t44=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5519,a[2]=((C_word)li158),tmp=(C_word)a,a+=3,tmp);
t45=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5544,a[2]=((C_word)li159),tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:560: chicken.base#getter-with-setter */
t46=*((C_word*)lf[395]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t46;
av2[1]=t43;
av2[2]=t44;
av2[3]=t45;
av2[4]=lf[398];
((C_proc)(void*)(*((C_word*)t46+1)))(5,av2);}}

/* mode in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_fcall f_3048(C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,3)))){
C_save_and_reclaim_args((void *)trf_3048,4,t1,t2,t3,t4);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3056,a[2]=t1,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_i_pairp(t3))){
t6=C_u_i_car(t3);
t7=C_eqp(t6,lf[234]);
if(C_truep(t7)){
if(C_truep(C_i_not(t2))){
/* posix-common.scm:482: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[192]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[192]+1);
av2[1]=t1;
av2[2]=lf[235];
av2[3]=t4;
tp(4,av2);}}
else{
/* posix-common.scm:486: ##sys#error */
t8=*((C_word*)lf[97]+1);{
C_word av2[4];
av2[0]=t8;
av2[1]=t5;
av2[2]=lf[236];
av2[3]=t6;
((C_proc)(void*)(*((C_word*)t8+1)))(4,av2);}}}
else{
/* posix-common.scm:487: ##sys#error */
t8=*((C_word*)lf[97]+1);{
C_word av2[4];
av2[0]=t8;
av2[1]=t5;
av2[2]=lf[237];
av2[3]=t6;
((C_proc)(void*)(*((C_word*)t8+1)))(4,av2);}}}
else{
if(C_truep(t2)){
/* posix-common.scm:482: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[192]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[192]+1);
av2[1]=t1;
av2[2]=lf[238];
av2[3]=t4;
tp(4,av2);}}
else{
/* posix-common.scm:482: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[192]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[192]+1);
av2[1]=t1;
av2[2]=lf[239];
av2[3]=t4;
tp(4,av2);}}}}

/* k3054 in mode in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3056(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3056,c,av);}
/* posix-common.scm:482: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[192]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[192]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
tp(4,av2);}}

/* check in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_fcall f_3085(C_word t1,C_word t2,C_word t3,C_word t4,C_word t5){
C_word tmp;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,5)))){
C_save_and_reclaim_args((void *)trf_3085,5,t1,t2,t3,t4,t5);}
a=C_alloc(4);
if(C_truep(C_null_pointerp(t5))){
/* posix-common.scm:493: posix-error */
t6=lf[185];{
C_word av2[6];
av2[0]=t6;
av2[1]=t1;
av2[2]=lf[190];
av2[3]=t2;
av2[4]=lf[240];
av2[5]=t3;
f_2552(6,av2);}}
else{
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3098,a[2]=t5,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
if(C_truep(t4)){
/* posix-common.scm:494: ##sys#make-port */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[241]+1));
C_word av2[6];
av2[0]=*((C_word*)lf[241]+1);
av2[1]=t6;
av2[2]=C_fix(1);
av2[3]=*((C_word*)lf[242]+1);
av2[4]=lf[243];
av2[5]=lf[232];
tp(6,av2);}}
else{
/* posix-common.scm:494: ##sys#make-port */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[241]+1));
C_word av2[6];
av2[0]=*((C_word*)lf[241]+1);
av2[1]=t6;
av2[2]=C_fix(2);
av2[3]=*((C_word*)lf[242]+1);
av2[4]=lf[243];
av2[5]=lf[232];
tp(6,av2);}}}}

/* k3096 in check in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3098(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3098,c,av);}
t2=C_set_file_ptr(t1,((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.file.posix#open-input-file* in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3104(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +7,c,3)))){
C_save_and_reclaim((void*)f_3104,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+7);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3108,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=t1,a[5]=((C_word*)t0)[3],a[6]=t3,tmp=(C_word)a,a+=7,tmp);
/* posix-common.scm:499: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[203]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[203]+1);
av2[1]=t4;
av2[2]=t2;
av2[3]=lf[244];
tp(4,av2);}}

/* k3106 in chicken.file.posix#open-input-file* in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3108(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_3108,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3116,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:500: mode */
f_3048(t2,C_SCHEME_TRUE,((C_word*)t0)[6],lf[244]);}

/* k3114 in k3106 in chicken.file.posix#open-input-file* in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3116(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(2,c,5)))){
C_save_and_reclaim((void *)f_3116,c,av);}
a=C_alloc(2);
t2=C_fdopen(&a,2,((C_word*)t0)[2],t1);
/* posix-common.scm:500: check */
f_3085(((C_word*)t0)[4],lf[244],((C_word*)t0)[2],C_SCHEME_TRUE,t2);}

/* chicken.file.posix#open-output-file* in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3118(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +7,c,3)))){
C_save_and_reclaim((void*)f_3118,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+7);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3122,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=t1,a[5]=((C_word*)t0)[3],a[6]=t3,tmp=(C_word)a,a+=7,tmp);
/* posix-common.scm:503: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[203]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[203]+1);
av2[1]=t4;
av2[2]=t2;
av2[3]=lf[245];
tp(4,av2);}}

/* k3120 in chicken.file.posix#open-output-file* in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3122(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_3122,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3130,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:504: mode */
f_3048(t2,C_SCHEME_FALSE,((C_word*)t0)[6],lf[245]);}

/* k3128 in k3120 in chicken.file.posix#open-output-file* in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3130(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(2,c,5)))){
C_save_and_reclaim((void *)f_3130,c,av);}
a=C_alloc(2);
t2=C_fdopen(&a,2,((C_word*)t0)[2],t1);
/* posix-common.scm:504: check */
f_3085(((C_word*)t0)[4],lf[245],((C_word*)t0)[2],C_SCHEME_FALSE,t2);}

/* chicken.file.posix#port->fileno in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3132(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_3132,c,av);}
a=C_alloc(4);
t3=C_i_check_port_2(t2,C_fix(0),C_SCHEME_TRUE,lf[246]);
t4=C_slot(t2,C_fix(7));
t5=C_eqp(lf[228],t4);
if(C_truep(t5)){
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3148,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:514: ##sys#port-data */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[247]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[247]+1);
av2[1]=t6;
av2[2]=t2;
tp(3,av2);}}
else{
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3171,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:515: ##sys#peek-unsigned-integer */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[250]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[250]+1);
av2[1]=t6;
av2[2]=t2;
av2[3]=C_fix(0);
tp(4,av2);}}}

/* k3146 in chicken.file.posix#port->fileno in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3148(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3148,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_slot(t1,C_fix(0));
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k3155 in k3169 in chicken.file.posix#port->fileno in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3157(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3157,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k3169 in chicken.file.posix#port->fileno in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3171(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,5)))){
C_save_and_reclaim((void *)f_3171,c,av);}
a=C_alloc(4);
if(C_truep(C_i_not(C_i_zerop(t1)))){
t2=C_port_fileno(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3157,a[2]=((C_word*)t0)[3],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_fixnum_lessp(t2,C_fix(0)))){
/* posix-common.scm:518: posix-error */
t4=lf[185];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[190];
av2[3]=lf[246];
av2[4]=lf[248];
av2[5]=((C_word*)t0)[2];
f_2552(6,av2);}}
else{
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}
else{
/* posix-common.scm:520: posix-error */
t2=lf[185];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[193];
av2[3]=lf[246];
av2[4]=lf[249];
av2[5]=((C_word*)t0)[2];
f_2552(6,av2);}}}

/* chicken.file.posix#duplicate-fileno in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3177(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +5,c,3)))){
C_save_and_reclaim((void*)f_3177,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+5);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3181,a[2]=t1,a[3]=t2,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:524: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[203]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[203]+1);
av2[1]=t4;
av2[2]=t2;
av2[3]=lf[251];
tp(4,av2);}}

/* k3179 in chicken.file.posix#duplicate-fileno in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3181(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_3181,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3184,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_i_nullp(((C_word*)t0)[4]))){
t3=t2;
f_3184(t3,C_dup(((C_word*)t0)[3]));}
else{
t3=C_i_car(((C_word*)t0)[4]);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3202,a[2]=t2,a[3]=((C_word*)t0)[3],a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:528: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[203]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[203]+1);
av2[1]=t4;
av2[2]=t3;
av2[3]=lf[251];
tp(4,av2);}}}

/* k3182 in k3179 in chicken.file.posix#duplicate-fileno in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_fcall f_3184(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,5)))){
C_save_and_reclaim_args((void *)trf_3184,2,t0,t1);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3187,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_fixnum_lessp(t1,C_fix(0)))){
/* posix-common.scm:531: posix-error */
t3=lf[185];{
C_word av2[6];
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[190];
av2[3]=lf[251];
av2[4]=lf[252];
av2[5]=((C_word*)t0)[3];
f_2552(6,av2);}}
else{
t3=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t3;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k3185 in k3182 in k3179 in chicken.file.posix#duplicate-fileno in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3187(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3187,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k3200 in k3179 in chicken.file.posix#duplicate-fileno in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3202(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3202,c,av);}
t2=((C_word*)t0)[2];
f_3184(t2,C_dup2(((C_word*)t0)[3],((C_word*)t0)[4]));}

/* chicken.process-context.posix#current-process-id in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3204(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3204,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=stub826(C_SCHEME_UNDEFINED);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.process-context.posix#change-directory* in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3207(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_3207,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3211,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:545: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[203]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[203]+1);
av2[1]=t3;
av2[2]=t2;
av2[3]=lf[253];
tp(4,av2);}}

/* k3209 in chicken.process-context.posix#change-directory* in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3211(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,5)))){
C_save_and_reclaim((void *)f_3211,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3214,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=C_eqp(C_fix(0),C_fchdir(((C_word*)t0)[3]));
if(C_truep(t3)){
t4=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t4;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
/* posix-common.scm:547: posix-error */
t4=lf[185];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=lf[190];
av2[3]=lf[253];
av2[4]=lf[254];
av2[5]=((C_word*)t0)[3];
f_2552(6,av2);}}}

/* k3212 in k3209 in chicken.process-context.posix#change-directory* in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3214(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3214,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* ##sys#change-directory-hook in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3223(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_3223,c,av);}
if(C_truep(C_fixnump(t2))){
t3=*((C_word*)lf[171]+1);
t4=*((C_word*)lf[171]+1);
/* posix-common.scm:552: g833 */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[171]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[171]+1);
av2[1]=t1;
av2[2]=t2;
tp(3,av2);}}
else{
/* posix-common.scm:552: g833 */
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t1;
av2[2]=t2;
((C_proc)C_fast_retrieve_proc(t3))(3,av2);}}}

/* k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3237(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(32,c,4)))){
C_save_and_reclaim((void *)f_3237,c,av);}
a=C_alloc(32);
t2=C_mutate((C_word*)lf[15]+1 /* (set! chicken.file.posix#file-creation-mode ...) */,t1);
t3=C_mutate(&lf[256] /* (set! chicken.posix#decode-seconds ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)C_decode_seconds,a[2]=((C_word)li34),tmp=(C_word)a,a+=3,tmp));
t4=C_mutate(&lf[257] /* (set! chicken.posix#check-time-vector ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3240,a[2]=((C_word)li35),tmp=(C_word)a,a+=3,tmp));
t5=C_mutate((C_word*)lf[83]+1 /* (set! chicken.time.posix#seconds->local-time ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3259,a[2]=((C_word)li36),tmp=(C_word)a,a+=3,tmp));
t6=C_mutate((C_word*)lf[81]+1 /* (set! chicken.time.posix#seconds->utc-time ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3280,a[2]=((C_word)li37),tmp=(C_word)a,a+=3,tmp));
t7=C_mutate((C_word*)lf[84]+1 /* (set! chicken.time.posix#seconds->string ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3312,a[2]=((C_word)li38),tmp=(C_word)a,a+=3,tmp));
t8=C_fix((C_word)sizeof(struct tm));
t9=C_mutate((C_word*)lf[85]+1 /* (set! chicken.time.posix#local-time->seconds ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3350,a[2]=t8,a[3]=((C_word)li39),tmp=(C_word)a,a+=4,tmp));
t10=C_fix((C_word)sizeof(struct tm));
t11=C_mutate((C_word*)lf[87]+1 /* (set! chicken.time.posix#time->string ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3395,a[2]=t10,a[3]=((C_word)li40),tmp=(C_word)a,a+=4,tmp));
t12=C_mutate((C_word*)lf[135]+1 /* (set! chicken.process.signal#set-signal-handler! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3457,a[2]=((C_word)li41),tmp=(C_word)a,a+=3,tmp));
t13=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3472,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t14=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5510,a[2]=((C_word)li157),tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:635: chicken.base#getter-with-setter */
t15=*((C_word*)lf[395]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t15;
av2[1]=t13;
av2[2]=t14;
av2[3]=*((C_word*)lf[135]+1);
av2[4]=lf[396];
((C_proc)(void*)(*((C_word*)t15+1)))(5,av2);}}

/* chicken.posix#check-time-vector in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_fcall f_3240(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,4)))){
C_save_and_reclaim_args((void *)trf_3240,3,t1,t2,t3);}
t4=C_i_check_vector_2(t3,t2);
t5=C_block_size(t3);
if(C_truep(C_fixnum_lessp(t5,C_fix(10)))){
/* posix-common.scm:579: ##sys#error */
t6=*((C_word*)lf[97]+1);{
C_word av2[5];
av2[0]=t6;
av2[1]=t1;
av2[2]=t2;
av2[3]=lf[258];
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}
else{
t6=C_SCHEME_UNDEFINED;
t7=t1;{
C_word av2[2];
av2[0]=t7;
av2[1]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}}

/* chicken.time.posix#seconds->local-time in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3259(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_3259,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3263,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_rest_nullp(c,2))){
/* posix-common.scm:582: chicken.time#current-seconds */
t3=*((C_word*)lf[214]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_get_rest_arg(c,2,av,2,t0);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f6102,a[2]=t1,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:583: ##sys#check-exact-integer */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[213]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[213]+1);
av2[1]=t4;
av2[2]=t3;
av2[3]=lf[259];
tp(4,av2);}}}

/* k3261 in chicken.time.posix#seconds->local-time in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3263(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_3263,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3266,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:583: ##sys#check-exact-integer */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[213]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[213]+1);
av2[1]=t2;
av2[2]=t1;
av2[3]=lf[259];
tp(4,av2);}}

/* k3264 in k3261 in chicken.time.posix#seconds->local-time in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3266(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3266,c,av);}
/* posix-common.scm:584: decode-seconds */
{C_proc tp=(C_proc)C_fast_retrieve_proc(lf[256]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=lf[256];
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=C_SCHEME_FALSE;
tp(4,av2);}}

/* chicken.time.posix#seconds->utc-time in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3280(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_3280,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3284,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_rest_nullp(c,2))){
/* posix-common.scm:587: chicken.time#current-seconds */
t3=*((C_word*)lf[214]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_get_rest_arg(c,2,av,2,t0);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f6106,a[2]=t1,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:588: ##sys#check-exact-integer */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[213]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[213]+1);
av2[1]=t4;
av2[2]=t3;
av2[3]=lf[260];
tp(4,av2);}}}

/* k3282 in chicken.time.posix#seconds->utc-time in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3284(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_3284,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3287,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:588: ##sys#check-exact-integer */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[213]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[213]+1);
av2[1]=t2;
av2[2]=t1;
av2[3]=lf[260];
tp(4,av2);}}

/* k3285 in k3282 in chicken.time.posix#seconds->utc-time in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3287(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3287,c,av);}
/* posix-common.scm:589: decode-seconds */
{C_proc tp=(C_proc)C_fast_retrieve_proc(lf[256]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=lf[256];
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=C_SCHEME_TRUE;
tp(4,av2);}}

/* chicken.time.posix#seconds->string in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3312(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_3312,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3316,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_rest_nullp(c,2))){
/* posix-common.scm:593: chicken.time#current-seconds */
t3=*((C_word*)lf[214]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_get_rest_arg(c,2,av,2,t0);
f_3316(2,av2);}}}

/* k3314 in chicken.time.posix#seconds->string in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3316(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_3316,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3319,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:594: ##sys#check-exact-integer */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[213]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[213]+1);
av2[1]=t2;
av2[2]=t1;
av2[3]=lf[262];
tp(4,av2);}}

/* k3317 in k3314 in chicken.time.posix#seconds->string in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3319(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_3319,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3322,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=C_a_i_bytevector(&a,1,C_fix(3));
t4=C_fix((C_word)sizeof(int) * CHAR_BIT);
t5=C_i_foreign_ranged_integer_argumentp(((C_word*)t0)[3],t4);
/* posix-common.scm:592: ##sys#peek-c-string */
t6=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t2;
av2[2]=stub882(t3,t5);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* k3320 in k3317 in k3314 in chicken.time.posix#seconds->string in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3322(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_3322,c,av);}
if(C_truep(t1)){
t2=C_block_size(t1);
/* posix-common.scm:597: ##sys#substring */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[261]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[261]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_fix(0);
av2[4]=C_fixnum_difference(t2,C_fix(1));
tp(5,av2);}}
else{
/* posix-common.scm:598: ##sys#error */
t2=*((C_word*)lf[97]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[262];
av2[3]=lf[263];
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}}

/* chicken.time.posix#local-time->seconds in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3350(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_3350,c,av);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3354,a[2]=t2,a[3]=t1,a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:603: check-time-vector */
f_3240(t3,lf[264],t2);}

/* k3352 in chicken.time.posix#local-time->seconds in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3354(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_3354,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3358,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:604: ##sys#make-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[266]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[266]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=C_make_character(0);
tp(4,av2);}}

/* k3356 in k3352 in chicken.time.posix#local-time->seconds in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3358(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,4)))){
C_save_and_reclaim((void *)f_3358,c,av);}
a=C_alloc(7);
t2=C_a_mktime(&a,2,((C_word*)t0)[2],t1);
if(C_truep(C_i_nequalp(C_fix(-1),t2))){
/* posix-common.scm:606: ##sys#error */
t3=*((C_word*)lf[97]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[264];
av2[3]=lf[265];
av2[4]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}
else{
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* chicken.time.posix#time->string in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3395(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_3395,c,av);}
a=C_alloc(6);
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,3,t0));
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3402,a[2]=t4,a[3]=t1,a[4]=t2,a[5]=((C_word*)t0)[2],tmp=(C_word)a,a+=6,tmp);
/* posix-common.scm:614: check-time-vector */
f_3240(t5,lf[267],t2);}

/* k3400 in chicken.time.posix#time->string in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3402(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_3402,c,av);}
a=C_alloc(9);
if(C_truep(((C_word*)t0)[2])){
t2=C_i_check_string_2(((C_word*)t0)[2],lf[267]);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3411,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3421,a[2]=t3,a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:618: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[192]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[192]+1);
av2[1]=t4;
av2[2]=((C_word*)t0)[2];
av2[3]=lf[267];
tp(4,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3428,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3449,a[2]=t2,a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:620: ##sys#make-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[266]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[266]+1);
av2[1]=t3;
av2[2]=((C_word*)t0)[5];
av2[3]=C_make_character(0);
tp(4,av2);}}}

/* k3409 in k3400 in chicken.time.posix#time->string in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3411(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_3411,c,av);}
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
/* posix-common.scm:619: ##sys#error */
t2=*((C_word*)lf[97]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[267];
av2[3]=lf[268];
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}}

/* k3419 in k3400 in chicken.time.posix#time->string in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3421(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_3421,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3425,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:618: ##sys#make-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[266]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[266]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=C_make_character(0);
tp(4,av2);}}

/* k3423 in k3419 in k3400 in chicken.time.posix#time->string in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3425(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_3425,c,av);}
a=C_alloc(5);
t2=C_a_i_bytevector(&a,1,C_fix(3));
if(C_truep(t1)){
t3=C_i_foreign_block_argumentp(t1);
/* posix-common.scm:611: ##sys#peek-c-string */
t4=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[2];
av2[2]=stub919(t2,((C_word*)t0)[3],((C_word*)t0)[4],t3);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
/* posix-common.scm:611: ##sys#peek-c-string */
t3=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=stub919(t2,((C_word*)t0)[3],((C_word*)t0)[4],C_SCHEME_FALSE);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}}

/* k3426 in k3400 in chicken.time.posix#time->string in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3428(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_3428,c,av);}
if(C_truep(t1)){
t2=C_block_size(t1);
/* posix-common.scm:622: ##sys#substring */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[261]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[261]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_fix(0);
av2[4]=C_fixnum_difference(t2,C_fix(1));
tp(5,av2);}}
else{
/* posix-common.scm:623: ##sys#error */
t2=*((C_word*)lf[97]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[267];
av2[3]=lf[269];
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}}

/* k3447 in k3400 in chicken.time.posix#time->string in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3449(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_3449,c,av);}
a=C_alloc(5);
t2=C_a_i_bytevector(&a,1,C_fix(3));
if(C_truep(t1)){
t3=C_i_foreign_block_argumentp(t1);
/* posix-common.scm:610: ##sys#peek-c-string */
t4=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[2];
av2[2]=stub909(t2,((C_word*)t0)[3],t3);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
/* posix-common.scm:610: ##sys#peek-c-string */
t3=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=stub909(t2,((C_word*)t0)[3],C_SCHEME_FALSE);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}}

/* chicken.process.signal#set-signal-handler! in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3457(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_3457,c,av);}
a=C_alloc(5);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3461,a[2]=t3,a[3]=t2,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:630: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[203]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[203]+1);
av2[1]=t4;
av2[2]=t2;
av2[3]=lf[271];
tp(4,av2);}}

/* k3459 in chicken.process.signal#set-signal-handler! in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3461(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3461,c,av);}
if(C_truep(((C_word*)t0)[2])){
t2=C_establish_signal_handler(((C_word*)t0)[3],((C_word*)t0)[3]);
t3=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_vector_set(*((C_word*)lf[270]+1),((C_word*)t0)[3],((C_word*)t0)[2]);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=C_establish_signal_handler(((C_word*)t0)[3],C_SCHEME_FALSE);
t3=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_vector_set(*((C_word*)lf[270]+1),((C_word*)t0)[3],((C_word*)t0)[2]);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3472(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word t24;
C_word t25;
C_word t26;
C_word t27;
C_word t28;
C_word t29;
C_word t30;
C_word t31;
C_word t32;
C_word t33;
C_word t34;
C_word t35;
C_word t36;
C_word t37;
C_word t38;
C_word t39;
C_word t40;
C_word t41;
C_word t42;
C_word t43;
C_word t44;
C_word t45;
C_word t46;
C_word t47;
C_word t48;
C_word t49;
C_word t50;
C_word t51;
C_word t52;
C_word t53;
C_word t54;
C_word t55;
C_word t56;
C_word t57;
C_word t58;
C_word t59;
C_word t60;
C_word t61;
C_word t62;
C_word t63;
C_word t64;
C_word t65;
C_word t66;
C_word t67;
C_word t68;
C_word t69;
C_word t70;
C_word t71;
C_word t72;
C_word t73;
C_word t74;
C_word t75;
C_word t76;
C_word t77;
C_word t78;
C_word t79;
C_word t80;
C_word t81;
C_word t82;
C_word t83;
C_word t84;
C_word t85;
C_word t86;
C_word t87;
C_word t88;
C_word t89;
C_word t90;
C_word t91;
C_word t92;
C_word t93;
C_word t94;
C_word t95;
C_word t96;
C_word t97;
C_word t98;
C_word t99;
C_word t100;
C_word t101;
C_word t102;
C_word t103;
C_word t104;
C_word t105;
C_word t106;
C_word t107;
C_word t108;
C_word t109;
C_word t110;
C_word t111;
C_word t112;
C_word t113;
C_word t114;
C_word t115;
C_word t116;
C_word t117;
C_word t118;
C_word t119;
C_word t120;
C_word t121;
C_word t122;
C_word t123;
C_word t124;
C_word t125;
C_word t126;
C_word t127;
C_word t128;
C_word t129;
C_word t130;
C_word t131;
C_word t132;
C_word t133;
C_word t134;
C_word t135;
C_word t136;
C_word t137;
C_word t138;
C_word t139;
C_word t140;
C_word t141;
C_word t142;
C_word t143;
C_word t144;
C_word t145;
C_word t146;
C_word t147;
C_word t148;
C_word t149;
C_word t150;
C_word t151;
C_word t152;
C_word t153;
C_word t154;
C_word t155;
C_word t156;
C_word t157;
C_word t158;
C_word t159;
C_word t160;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(231,c,8)))){
C_save_and_reclaim((void *)f_3472,c,av);}
a=C_alloc(231);
t2=C_mutate((C_word*)lf[137]+1 /* (set! chicken.process.signal#signal-handler ...) */,t1);
t3=C_mutate((C_word*)lf[126]+1 /* (set! chicken.process#process-sleep ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3474,a[2]=((C_word)li42),tmp=(C_word)a,a+=3,tmp));
t4=C_mutate((C_word*)lf[114]+1 /* (set! chicken.process#process-wait ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3480,a[2]=((C_word)li45),tmp=(C_word)a,a+=3,tmp));
t5=C_mutate(&lf[275] /* (set! chicken.posix#list->c-string-buffer ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3551,a[2]=((C_word)li54),tmp=(C_word)a,a+=3,tmp));
t6=C_mutate(&lf[277] /* (set! chicken.posix#free-c-string-buffer ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3668,a[2]=((C_word)li56),tmp=(C_word)a,a+=3,tmp));
t7=C_mutate(&lf[286] /* (set! chicken.posix#check-environment-list ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3702,a[2]=((C_word)li59),tmp=(C_word)a,a+=3,tmp));
t8=*((C_word*)lf[287]+1);
t9=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3749,a[2]=((C_word)li60),tmp=(C_word)a,a+=3,tmp);
t10=C_mutate(&lf[288] /* (set! chicken.posix#call-with-exec-args ...) */,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3751,a[2]=t9,a[3]=t8,a[4]=((C_word)li69),tmp=(C_word)a,a+=5,tmp));
t11=C_set_block_item(lf[127] /* chicken.process#pipe/buf */,0,C_fix((C_word)PIPE_BUF));
t12=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3908,a[2]=((C_word)li70),tmp=(C_word)a,a+=3,tmp);
t13=C_mutate((C_word*)lf[120]+1 /* (set! chicken.process#open-input-pipe ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3927,a[2]=t12,a[3]=((C_word)li71),tmp=(C_word)a,a+=4,tmp));
t14=C_mutate((C_word*)lf[121]+1 /* (set! chicken.process#open-output-pipe ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3967,a[2]=t12,a[3]=((C_word)li72),tmp=(C_word)a,a+=4,tmp));
t15=C_mutate((C_word*)lf[117]+1 /* (set! chicken.process#close-input-pipe ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4007,a[2]=((C_word)li73),tmp=(C_word)a,a+=3,tmp));
t16=C_mutate((C_word*)lf[118]+1 /* (set! chicken.process#close-output-pipe ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4022,a[2]=((C_word)li74),tmp=(C_word)a,a+=3,tmp));
t17=C_mutate((C_word*)lf[122]+1 /* (set! chicken.process#with-input-from-pipe ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4037,a[2]=((C_word)li79),tmp=(C_word)a,a+=3,tmp));
t18=C_mutate((C_word*)lf[116]+1 /* (set! chicken.process#call-with-output-pipe ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4071,a[2]=((C_word)li82),tmp=(C_word)a,a+=3,tmp));
t19=C_mutate((C_word*)lf[115]+1 /* (set! chicken.process#call-with-input-pipe ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4095,a[2]=((C_word)li85),tmp=(C_word)a,a+=3,tmp));
t20=C_mutate((C_word*)lf[123]+1 /* (set! chicken.process#with-output-to-pipe ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4119,a[2]=((C_word)li90),tmp=(C_word)a,a+=3,tmp));
t21=C_set_block_item(lf[50] /* chicken.file.posix#open/noinherit */,0,C_fix((C_word)O_NOINHERIT));
t22=C_fix((C_word)S_IRUSR | S_IWUSR | S_IXUSR);
t23=C_fixnum_or(C_fix((C_word)S_IRGRP),C_fix((C_word)S_IROTH));
t24=C_u_fixnum_or(t22,t23);
t25=C_mutate((C_word*)lf[21]+1 /* (set! chicken.file.posix#file-open ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4159,a[2]=t24,a[3]=((C_word)li91),tmp=(C_word)a,a+=4,tmp));
t26=C_mutate((C_word*)lf[13]+1 /* (set! chicken.file.posix#file-close ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4194,a[2]=((C_word)li93),tmp=(C_word)a,a+=3,tmp));
t27=C_mutate((C_word*)lf[25]+1 /* (set! chicken.file.posix#file-read ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4225,a[2]=((C_word)li94),tmp=(C_word)a,a+=3,tmp));
t28=C_mutate((C_word*)lf[30]+1 /* (set! chicken.file.posix#file-write ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4268,a[2]=((C_word)li95),tmp=(C_word)a,a+=3,tmp));
t29=C_mutate((C_word*)lf[20]+1 /* (set! chicken.file.posix#file-mkstemp ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4308,a[2]=((C_word)li101),tmp=(C_word)a,a+=3,tmp));
t30=C_mutate((C_word*)lf[119]+1 /* (set! chicken.process#create-pipe ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4467,a[2]=((C_word)li102),tmp=(C_word)a,a+=3,tmp));
t31=C_set_block_item(lf[159] /* chicken.process.signal#signal/term */,0,C_fix((C_word)SIGTERM));
t32=C_set_block_item(lf[151] /* chicken.process.signal#signal/int */,0,C_fix((C_word)SIGINT));
t33=C_set_block_item(lf[148] /* chicken.process.signal#signal/fpe */,0,C_fix((C_word)SIGFPE));
t34=C_set_block_item(lf[150] /* chicken.process.signal#signal/ill */,0,C_fix((C_word)SIGILL));
t35=C_set_block_item(lf[157] /* chicken.process.signal#signal/segv */,0,C_fix((C_word)SIGSEGV));
t36=C_set_block_item(lf[142] /* chicken.process.signal#signal/abrt */,0,C_fix((C_word)SIGABRT));
t37=C_set_block_item(lf[144] /* chicken.process.signal#signal/break */,0,C_fix((C_word)SIGBREAK));
t38=C_set_block_item(lf[143] /* chicken.process.signal#signal/alrm */,0,C_fix(0));
t39=C_set_block_item(lf[145] /* chicken.process.signal#signal/bus */,0,C_fix(0));
t40=C_set_block_item(lf[146] /* chicken.process.signal#signal/chld */,0,C_fix(0));
t41=C_set_block_item(lf[147] /* chicken.process.signal#signal/cont */,0,C_fix(0));
t42=C_set_block_item(lf[149] /* chicken.process.signal#signal/hup */,0,C_fix(0));
t43=C_set_block_item(lf[152] /* chicken.process.signal#signal/io */,0,C_fix(0));
t44=C_set_block_item(lf[153] /* chicken.process.signal#signal/kill */,0,C_fix(0));
t45=C_set_block_item(lf[154] /* chicken.process.signal#signal/pipe */,0,C_fix(0));
t46=C_set_block_item(lf[155] /* chicken.process.signal#signal/prof */,0,C_fix(0));
t47=C_set_block_item(lf[156] /* chicken.process.signal#signal/quit */,0,C_fix(0));
t48=C_set_block_item(lf[158] /* chicken.process.signal#signal/stop */,0,C_fix(0));
t49=C_set_block_item(lf[160] /* chicken.process.signal#signal/trap */,0,C_fix(0));
t50=C_set_block_item(lf[161] /* chicken.process.signal#signal/tstp */,0,C_fix(0));
t51=C_set_block_item(lf[162] /* chicken.process.signal#signal/urg */,0,C_fix(0));
t52=C_set_block_item(lf[163] /* chicken.process.signal#signal/usr1 */,0,C_fix(0));
t53=C_set_block_item(lf[164] /* chicken.process.signal#signal/usr2 */,0,C_fix(0));
t54=C_set_block_item(lf[165] /* chicken.process.signal#signal/vtalrm */,0,C_fix(0));
t55=C_set_block_item(lf[166] /* chicken.process.signal#signal/winch */,0,C_fix(0));
t56=C_set_block_item(lf[167] /* chicken.process.signal#signal/xcpu */,0,C_fix(0));
t57=C_set_block_item(lf[168] /* chicken.process.signal#signal/xfsz */,0,C_fix(0));
t58=C_a_i_list7(&a,7,*((C_word*)lf[159]+1),*((C_word*)lf[151]+1),*((C_word*)lf[148]+1),*((C_word*)lf[150]+1),*((C_word*)lf[157]+1),*((C_word*)lf[142]+1),*((C_word*)lf[144]+1));
t59=C_mutate((C_word*)lf[169]+1 /* (set! chicken.process.signal#signals-list ...) */,t58);
t60=C_mutate(&lf[329] /* (set! chicken.posix#duplicate-fileno ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4529,a[2]=((C_word)li103),tmp=(C_word)a,a+=3,tmp));
t61=C_mutate((C_word*)lf[88]+1 /* (set! chicken.time.posix#local-timezone-abbreviation ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4559,a[2]=((C_word)li104),tmp=(C_word)a,a+=3,tmp));
t62=C_set_block_item(lf[128] /* chicken.process#spawn/overlay */,0,C_fix((C_word)P_OVERLAY));
t63=C_set_block_item(lf[129] /* chicken.process#spawn/wait */,0,C_fix((C_word)P_WAIT));
t64=C_set_block_item(lf[130] /* chicken.process#spawn/nowait */,0,C_fix((C_word)P_NOWAIT));
t65=C_set_block_item(lf[131] /* chicken.process#spawn/nowaito */,0,C_fix((C_word)P_NOWAITO));
t66=C_set_block_item(lf[132] /* chicken.process#spawn/detach */,0,C_fix((C_word)P_DETACH));
t67=C_mutate(&lf[331] /* (set! chicken.posix#quote-arg-string ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4605,a[2]=((C_word)li106),tmp=(C_word)a,a+=3,tmp));
t68=C_mutate((C_word*)lf[109]+1 /* (set! chicken.process#process-execute ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4617,a[2]=((C_word)li109),tmp=(C_word)a,a+=3,tmp));
t69=C_mutate((C_word*)lf[113]+1 /* (set! chicken.process#process-spawn ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4697,a[2]=((C_word)li112),tmp=(C_word)a,a+=3,tmp));
t70=C_mutate(&lf[338] /* (set! chicken.posix#shell-command ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4780,a[2]=((C_word)li113),tmp=(C_word)a,a+=3,tmp));
t71=C_mutate((C_word*)lf[111]+1 /* (set! chicken.process#process-run ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4807,a[2]=((C_word)li114),tmp=(C_word)a,a+=3,tmp));
t72=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5030,a[2]=((C_word)li121),tmp=(C_word)a,a+=3,tmp);
t73=C_mutate((C_word*)lf[124]+1 /* (set! chicken.process#process ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5114,a[2]=t72,a[3]=((C_word)li122),tmp=(C_word)a,a+=4,tmp));
t74=C_mutate((C_word*)lf[125]+1 /* (set! chicken.process#process* ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5174,a[2]=t72,a[3]=((C_word)li123),tmp=(C_word)a,a+=4,tmp));
t75=C_mutate((C_word*)lf[179]+1 /* (set! chicken.process-context.posix#current-user-name ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5246,a[2]=((C_word)li124),tmp=(C_word)a,a+=3,tmp));
t76=C_mutate((C_word*)lf[4]+1 /* (set! chicken.file.posix#create-fifo ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5267,a[2]=((C_word)li125),tmp=(C_word)a,a+=3,tmp));
t77=C_mutate((C_word*)lf[181]+1 /* (set! chicken.process-context.posix#create-session ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5273,a[2]=((C_word)li126),tmp=(C_word)a,a+=3,tmp));
t78=C_mutate((C_word*)lf[5]+1 /* (set! chicken.file.posix#create-symbolic-link ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5279,a[2]=((C_word)li127),tmp=(C_word)a,a+=3,tmp));
t79=C_mutate((C_word*)lf[173]+1 /* (set! chicken.process-context.posix#current-effective-group-id ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5285,a[2]=((C_word)li128),tmp=(C_word)a,a+=3,tmp));
t80=C_mutate((C_word*)lf[174]+1 /* (set! chicken.process-context.posix#current-effective-user-id ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5291,a[2]=((C_word)li129),tmp=(C_word)a,a+=3,tmp));
t81=C_mutate((C_word*)lf[180]+1 /* (set! chicken.process-context.posix#current-effective-user-name ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5297,a[2]=((C_word)li130),tmp=(C_word)a,a+=3,tmp));
t82=C_mutate((C_word*)lf[175]+1 /* (set! chicken.process-context.posix#current-group-id ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5303,a[2]=((C_word)li131),tmp=(C_word)a,a+=3,tmp));
t83=C_mutate((C_word*)lf[176]+1 /* (set! chicken.process-context.posix#current-user-id ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5309,a[2]=((C_word)li132),tmp=(C_word)a,a+=3,tmp));
t84=C_mutate((C_word*)lf[14]+1 /* (set! chicken.file.posix#file-control ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5315,a[2]=((C_word)li133),tmp=(C_word)a,a+=3,tmp));
t85=C_mutate((C_word*)lf[17]+1 /* (set! chicken.file.posix#file-link ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5321,a[2]=((C_word)li134),tmp=(C_word)a,a+=3,tmp));
t86=C_mutate((C_word*)lf[18]+1 /* (set! chicken.file.posix#file-lock ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5327,a[2]=((C_word)li135),tmp=(C_word)a,a+=3,tmp));
t87=C_mutate((C_word*)lf[19]+1 /* (set! chicken.file.posix#file-lock/blocking ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5333,a[2]=((C_word)li136),tmp=(C_word)a,a+=3,tmp));
t88=C_mutate((C_word*)lf[26]+1 /* (set! chicken.file.posix#file-select ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5339,a[2]=((C_word)li137),tmp=(C_word)a,a+=3,tmp));
t89=C_mutate((C_word*)lf[27]+1 /* (set! chicken.file.posix#file-test-lock ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5345,a[2]=((C_word)li138),tmp=(C_word)a,a+=3,tmp));
t90=C_mutate((C_word*)lf[28]+1 /* (set! chicken.file.posix#file-truncate ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5351,a[2]=((C_word)li139),tmp=(C_word)a,a+=3,tmp));
t91=C_mutate((C_word*)lf[29]+1 /* (set! chicken.file.posix#file-unlock ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5357,a[2]=((C_word)li140),tmp=(C_word)a,a+=3,tmp));
t92=C_mutate((C_word*)lf[178]+1 /* (set! chicken.process-context.posix#parent-process-id ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5363,a[2]=((C_word)li141),tmp=(C_word)a,a+=3,tmp));
t93=C_mutate((C_word*)lf[110]+1 /* (set! chicken.process#process-fork ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5369,a[2]=((C_word)li142),tmp=(C_word)a,a+=3,tmp));
t94=C_mutate((C_word*)lf[182]+1 /* (set! chicken.process-context.posix#process-group-id ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5375,a[2]=((C_word)li143),tmp=(C_word)a,a+=3,tmp));
t95=C_mutate((C_word*)lf[112]+1 /* (set! chicken.process#process-signal ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5381,a[2]=((C_word)li144),tmp=(C_word)a,a+=3,tmp));
t96=C_mutate((C_word*)lf[6]+1 /* (set! chicken.file.posix#read-symbolic-link ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5387,a[2]=((C_word)li145),tmp=(C_word)a,a+=3,tmp));
t97=C_mutate((C_word*)lf[134]+1 /* (set! chicken.process.signal#set-alarm! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5393,a[2]=((C_word)li146),tmp=(C_word)a,a+=3,tmp));
t98=C_mutate((C_word*)lf[172]+1 /* (set! chicken.process-context.posix#set-root-directory! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5399,a[2]=((C_word)li147),tmp=(C_word)a,a+=3,tmp));
t99=C_mutate((C_word*)lf[136]+1 /* (set! chicken.process.signal#set-signal-mask! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5405,a[2]=((C_word)li148),tmp=(C_word)a,a+=3,tmp));
t100=C_mutate((C_word*)lf[138]+1 /* (set! chicken.process.signal#signal-mask ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5411,a[2]=((C_word)li149),tmp=(C_word)a,a+=3,tmp));
t101=C_mutate((C_word*)lf[139]+1 /* (set! chicken.process.signal#signal-mask! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5417,a[2]=((C_word)li150),tmp=(C_word)a,a+=3,tmp));
t102=C_mutate((C_word*)lf[140]+1 /* (set! chicken.process.signal#signal-masked? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5423,a[2]=((C_word)li151),tmp=(C_word)a,a+=3,tmp));
t103=C_mutate((C_word*)lf[141]+1 /* (set! chicken.process.signal#signal-unmask! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5429,a[2]=((C_word)li152),tmp=(C_word)a,a+=3,tmp));
t104=C_mutate((C_word*)lf[183]+1 /* (set! chicken.process-context.posix#user-information ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5435,a[2]=((C_word)li153),tmp=(C_word)a,a+=3,tmp));
t105=C_mutate((C_word*)lf[82]+1 /* (set! chicken.time.posix#utc-time->seconds ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5441,a[2]=((C_word)li154),tmp=(C_word)a,a+=3,tmp));
t106=C_mutate((C_word*)lf[86]+1 /* (set! chicken.time.posix#string->time ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5447,a[2]=((C_word)li155),tmp=(C_word)a,a+=3,tmp));
t107=C_set_block_item(lf[8] /* chicken.file.posix#fcntl/dupfd */,0,C_fix(0));
t108=C_set_block_item(lf[9] /* chicken.file.posix#fcntl/getfd */,0,C_fix(0));
t109=C_set_block_item(lf[11] /* chicken.file.posix#fcntl/setfd */,0,C_fix(0));
t110=C_set_block_item(lf[10] /* chicken.file.posix#fcntl/getfl */,0,C_fix(0));
t111=C_set_block_item(lf[12] /* chicken.file.posix#fcntl/setfl */,0,C_fix(0));
t112=C_set_block_item(lf[49] /* chicken.file.posix#open/noctty */,0,C_fix(0));
t113=C_set_block_item(lf[51] /* chicken.file.posix#open/nonblock */,0,C_fix(0));
t114=C_set_block_item(lf[48] /* chicken.file.posix#open/fsync */,0,C_fix(0));
t115=C_set_block_item(lf[55] /* chicken.file.posix#open/sync */,0,C_fix(0));
t116=C_set_block_item(lf[66] /* chicken.file.posix#perm/isgid */,0,C_fix(0));
t117=C_set_block_item(lf[67] /* chicken.file.posix#perm/isuid */,0,C_fix(0));
t118=C_set_block_item(lf[68] /* chicken.file.posix#perm/isvtx */,0,C_fix(0));
t119=C_a_i_provide(&a,1,lf[352]);
t120=C_mutate((C_word*)lf[353]+1 /* (set! chicken.errno#errno ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5465,a[2]=((C_word)li156),tmp=(C_word)a,a+=3,tmp));
t121=C_set_block_item(lf[355] /* chicken.errno#errno/2big */,0,C_fix((C_word)E2BIG));
t122=C_set_block_item(lf[356] /* chicken.errno#errno/acces */,0,C_fix((C_word)EACCES));
t123=C_set_block_item(lf[357] /* chicken.errno#errno/again */,0,C_fix((C_word)EAGAIN));
t124=C_set_block_item(lf[358] /* chicken.errno#errno/badf */,0,C_fix((C_word)EBADF));
t125=C_set_block_item(lf[359] /* chicken.errno#errno/busy */,0,C_fix((C_word)EBUSY));
t126=C_set_block_item(lf[360] /* chicken.errno#errno/child */,0,C_fix((C_word)ECHILD));
t127=C_set_block_item(lf[361] /* chicken.errno#errno/deadlk */,0,C_fix((C_word)EDEADLK));
t128=C_set_block_item(lf[362] /* chicken.errno#errno/dom */,0,C_fix((C_word)EDOM));
t129=C_set_block_item(lf[363] /* chicken.errno#errno/exist */,0,C_fix((C_word)EEXIST));
t130=C_set_block_item(lf[364] /* chicken.errno#errno/fault */,0,C_fix((C_word)EFAULT));
t131=C_set_block_item(lf[365] /* chicken.errno#errno/fbig */,0,C_fix((C_word)EFBIG));
t132=C_set_block_item(lf[366] /* chicken.errno#errno/ilseq */,0,C_fix((C_word)EILSEQ));
t133=C_set_block_item(lf[367] /* chicken.errno#errno/intr */,0,C_fix((C_word)EINTR));
t134=C_set_block_item(lf[368] /* chicken.errno#errno/inval */,0,C_fix((C_word)EINVAL));
t135=C_set_block_item(lf[369] /* chicken.errno#errno/io */,0,C_fix((C_word)EIO));
t136=C_set_block_item(lf[370] /* chicken.errno#errno/isdir */,0,C_fix((C_word)EISDIR));
t137=C_set_block_item(lf[371] /* chicken.errno#errno/mfile */,0,C_fix((C_word)EMFILE));
t138=C_set_block_item(lf[372] /* chicken.errno#errno/mlink */,0,C_fix((C_word)EMLINK));
t139=C_set_block_item(lf[373] /* chicken.errno#errno/nametoolong */,0,C_fix((C_word)ENAMETOOLONG));
t140=C_set_block_item(lf[374] /* chicken.errno#errno/nfile */,0,C_fix((C_word)ENFILE));
t141=C_set_block_item(lf[375] /* chicken.errno#errno/nodev */,0,C_fix((C_word)ENODEV));
t142=C_set_block_item(lf[376] /* chicken.errno#errno/noent */,0,C_fix((C_word)ENOENT));
t143=C_set_block_item(lf[377] /* chicken.errno#errno/noexec */,0,C_fix((C_word)ENOEXEC));
t144=C_set_block_item(lf[378] /* chicken.errno#errno/nolck */,0,C_fix((C_word)ENOLCK));
t145=C_set_block_item(lf[379] /* chicken.errno#errno/nomem */,0,C_fix((C_word)ENOMEM));
t146=C_set_block_item(lf[380] /* chicken.errno#errno/nospc */,0,C_fix((C_word)ENOSPC));
t147=C_set_block_item(lf[381] /* chicken.errno#errno/nosys */,0,C_fix((C_word)ENOSYS));
t148=C_set_block_item(lf[382] /* chicken.errno#errno/notdir */,0,C_fix((C_word)ENOTDIR));
t149=C_set_block_item(lf[383] /* chicken.errno#errno/notempty */,0,C_fix((C_word)ENOTEMPTY));
t150=C_set_block_item(lf[384] /* chicken.errno#errno/notty */,0,C_fix((C_word)ENOTTY));
t151=C_set_block_item(lf[385] /* chicken.errno#errno/nxio */,0,C_fix((C_word)ENXIO));
t152=C_set_block_item(lf[386] /* chicken.errno#errno/perm */,0,C_fix((C_word)EPERM));
t153=C_set_block_item(lf[387] /* chicken.errno#errno/pipe */,0,C_fix((C_word)EPIPE));
t154=C_set_block_item(lf[388] /* chicken.errno#errno/range */,0,C_fix((C_word)ERANGE));
t155=C_set_block_item(lf[389] /* chicken.errno#errno/rofs */,0,C_fix((C_word)EROFS));
t156=C_set_block_item(lf[390] /* chicken.errno#errno/spipe */,0,C_fix((C_word)ESPIPE));
t157=C_set_block_item(lf[391] /* chicken.errno#errno/srch */,0,C_fix((C_word)ESRCH));
t158=C_set_block_item(lf[392] /* chicken.errno#errno/wouldblock */,0,C_fix((C_word)EWOULDBLOCK));
t159=C_set_block_item(lf[393] /* chicken.errno#errno/xdev */,0,C_fix((C_word)EXDEV));
t160=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t160;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t160+1)))(2,av2);}}

/* chicken.process#process-sleep in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3474(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_3474,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3478,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:647: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[203]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[203]+1);
av2[1]=t3;
av2[2]=t2;
av2[3]=lf[272];
tp(4,av2);}}

/* k3476 in chicken.process#process-sleep in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3478(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3478,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_i_process_sleep(((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.process#process-wait in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3480(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_3480,c,av);}
a=C_alloc(5);
t2=C_rest_nullp(c,2);
t3=(C_truep(t2)?C_SCHEME_FALSE:C_get_rest_arg(c,2,av,2,t0));
t4=C_rest_nullp(c,2);
t5=C_rest_nullp(c,3);
t6=(C_truep(t5)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,2,t0));
t7=C_rest_nullp(c,3);
t8=(C_truep(t3)?t3:C_fix(-1));
t9=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3499,a[2]=t8,a[3]=t6,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:654: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[203]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[203]+1);
av2[1]=t9;
av2[2]=t8;
av2[3]=lf[273];
tp(4,av2);}}

/* k3497 in chicken.process#process-wait in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3499(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,5)))){
C_save_and_reclaim((void *)f_3499,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3504,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word)li43),tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3510,a[2]=((C_word*)t0)[2],a[3]=((C_word)li44),tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:655: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[4];
av2[2]=t2;
av2[3]=t3;
C_call_with_values(4,av2);}}

/* a3503 in k3497 in chicken.process#process-wait in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3504(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_3504,c,av);}
if(C_truep(C_process_wait(((C_word*)t0)[2],((C_word*)t0)[3]))){
/* posixwin.scm:858: scheme#values */{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
av2[3]=C_SCHEME_TRUE;
av2[4]=C_fix((C_word)C_exstatus);
C_values(5,av2);}}
else{
/* posixwin.scm:859: scheme#values */{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=t1;
av2[2]=C_fix(-1);
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_FALSE;
C_values(5,av2);}}}

/* a3509 in k3497 in chicken.process#process-wait in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3510(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_3510,c,av);}
t5=C_eqp(t2,C_fix(-1));
if(C_truep(t5)){
/* posix-common.scm:657: posix-error */
t6=lf[185];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t6;
av2[1]=t1;
av2[2]=lf[93];
av2[3]=lf[273];
av2[4]=lf[274];
av2[5]=((C_word*)t0)[2];
f_2552(6,av2);}}
else{
/* posix-common.scm:658: scheme#values */{
C_word *av2=av;
av2[0]=0;
av2[1]=t1;
av2[2]=t2;
av2[3]=t3;
av2[4]=t4;
C_values(5,av2);}}}

/* chicken.posix#list->c-string-buffer in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_fcall f_3551(C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,3)))){
C_save_and_reclaim_args((void *)trf_3551,4,t1,t2,t3,t4);}
a=C_alloc(12);
t5=C_i_check_list_2(t2,t4);
t6=C_u_i_length(t2);
t7=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3559,a[2]=t1,a[3]=t6,a[4]=t4,a[5]=t3,a[6]=t2,tmp=(C_word)a,a+=7,tmp);
t8=C_a_i_fixnum_plus(&a,2,t6,C_fix(1));
/* posix-common.scm:674: chicken.memory#make-pointer-vector */
t9=*((C_word*)lf[282]+1);{
C_word av2[4];
av2[0]=t9;
av2[1]=t7;
av2[2]=t8;
av2[3]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t9+1)))(4,av2);}}

/* k3557 in chicken.posix#list->c-string-buffer in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3559(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,3)))){
C_save_and_reclaim((void *)f_3559,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3562,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3567,a[2]=t1,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word)li53),tmp=(C_word)a,a+=8,tmp);
/* posix-common.scm:676: scheme#call-with-current-continuation */
t4=*((C_word*)lf[281]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k3560 in k3557 in chicken.posix#list->c-string-buffer in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3562(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3562,c,av);}
/* posix-common.scm:672: g988 */
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)C_fast_retrieve_proc(t2))(2,av2);}}

/* a3566 in k3557 in chicken.posix#list->c-string-buffer in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3567(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(14,c,3)))){
C_save_and_reclaim((void *)f_3567,c,av);}
a=C_alloc(14);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3573,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=((C_word)li47),tmp=(C_word)a,a+=5,tmp);
t4=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_3588,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t2,a[8]=((C_word)li52),tmp=(C_word)a,a+=9,tmp);
/* posix-common.scm:676: chicken.condition#with-exception-handler */
t5=*((C_word*)lf[280]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t1;
av2[2]=t3;
av2[3]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* a3572 in a3566 in k3557 in chicken.posix#list->c-string-buffer in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3573(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_3573,c,av);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3579,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word)li46),tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:676: k985 */
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t1;
av2[2]=t3;
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}

/* a3578 in a3572 in a3566 in k3557 in chicken.posix#list->c-string-buffer in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3579(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_3579,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3583,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:678: free-c-string-buffer */
f_3668(t2,((C_word*)t0)[3]);}

/* k3581 in a3578 in a3572 in a3566 in k3557 in chicken.posix#list->c-string-buffer in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3583(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_3583,c,av);}
/* posix-common.scm:678: chicken.condition#signal */
t2=*((C_word*)lf[276]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* a3587 in a3566 in k3557 in chicken.posix#list->c-string-buffer in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3588(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(16,c,3)))){
C_save_and_reclaim((void *)f_3588,c,av);}
a=C_alloc(16);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3590,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word)li49),tmp=(C_word)a,a+=8,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3647,a[2]=((C_word*)t0)[7],a[3]=((C_word)li51),tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3664,a[2]=t3,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* tmp12177 */
t5=t2;
f_3590(t5,t4);}

/* tmp12177 in a3587 in a3566 in k3557 in chicken.posix#list->c-string-buffer in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_fcall f_3590(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,0,4)))){
C_save_and_reclaim_args((void *)trf_3590,2,t0,t1);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3594,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3596,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t4,a[5]=((C_word*)t0)[2],a[6]=((C_word*)t0)[5],a[7]=((C_word)li48),tmp=(C_word)a,a+=8,tmp));
t6=((C_word*)t4)[1];
f_3596(t6,t2,((C_word*)t0)[6],C_fix(0));}

/* k3592 in tmp12177 in a3587 in a3566 in k3557 in chicken.posix#list->c-string-buffer in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3594(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3594,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* doloop993 in tmp12177 in a3587 in a3566 in k3557 in chicken.posix#list->c-string-buffer in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_fcall f_3596(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,2)))){
C_save_and_reclaim_args((void *)trf_3596,4,t0,t1,t2,t3);}
a=C_alloc(12);
t4=C_i_nullp(t2);
t5=(C_truep(t4)?t4:C_eqp(t3,((C_word*)t0)[2]));
if(C_truep(t5)){
t6=C_SCHEME_UNDEFINED;
t7=t1;{
C_word av2[2];
av2[0]=t7;
av2[1]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}
else{
t6=C_i_car(t2);
t7=C_i_check_string_2(t6,((C_word*)t0)[3]);
t8=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3612,a[2]=((C_word*)t0)[4],a[3]=t1,a[4]=t2,a[5]=t3,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[3],tmp=(C_word)a,a+=8,tmp);
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3637,a[2]=t8,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:687: convert */
t10=((C_word*)t0)[6];{
C_word av2[3];
av2[0]=t10;
av2[1]=t9;
av2[2]=C_u_i_car(t2);
((C_proc)C_fast_retrieve_proc(t10))(3,av2);}}}

/* k3610 in doloop993 in tmp12177 in a3587 in a3566 in k3557 in chicken.posix#list->c-string-buffer in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3612(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,3)))){
C_save_and_reclaim((void *)f_3612,c,av);}
a=C_alloc(13);
t2=C_a_i_bytevector(&a,1,C_fix(3));
t3=stub976(t2,t1);
t4=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3618,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t3,tmp=(C_word)a,a+=8,tmp);
if(C_truep(t3)){
t5=t4;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_UNDEFINED;
f_3618(2,av2);}}
else{
/* posix-common.scm:689: chicken.base#error */
t5=*((C_word*)lf[103]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[7];
av2[3]=lf[279];
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}}

/* k3616 in k3610 in doloop993 in tmp12177 in a3587 in a3566 in k3557 in chicken.posix#list->c-string-buffer in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3618(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_3618,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3621,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* posix-common.scm:690: chicken.memory#pointer-vector-set! */
t3=*((C_word*)lf[278]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[6];
av2[3]=((C_word*)t0)[5];
av2[4]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k3619 in k3616 in k3610 in doloop993 in tmp12177 in a3587 in a3566 in k3557 in chicken.posix#list->c-string-buffer in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 in ... */
static void C_ccall f_3621(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3621,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_3596(t2,((C_word*)t0)[3],C_u_i_cdr(((C_word*)t0)[4]),C_fixnum_plus(((C_word*)t0)[5],C_fix(1)));}

/* k3635 in doloop993 in tmp12177 in a3587 in a3566 in k3557 in chicken.posix#list->c-string-buffer in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3637(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3637,c,av);}
/* posix-common.scm:687: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[192]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[192]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
tp(4,av2);}}

/* tmp22178 in a3587 in a3566 in k3557 in chicken.posix#list->c-string-buffer in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_fcall f_3647(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_3647,3,t0,t1,t2);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3653,a[2]=t2,a[3]=((C_word)li50),tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:676: k985 */
t4=((C_word*)t0)[2];{
C_word av2[3];
av2[0]=t4;
av2[1]=t1;
av2[2]=t3;
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}

/* a3652 in tmp22178 in a3587 in a3566 in k3557 in chicken.posix#list->c-string-buffer in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3653(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_3653,c,av);}{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=0;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
C_apply_values(3,av2);}}

/* k3662 in a3587 in a3566 in k3557 in chicken.posix#list->c-string-buffer in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3664(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_3664,c,av);}
a=C_alloc(3);
/* tmp22178 */
t2=((C_word*)t0)[2];
f_3647(t2,((C_word*)t0)[3],C_a_i_list(&a,1,t1));}

/* chicken.posix#free-c-string-buffer in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_fcall f_3668(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_3668,2,t1,t2);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3672,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:695: chicken.memory#pointer-vector-length */
t4=*((C_word*)lf[285]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k3670 in chicken.posix#free-c-string-buffer in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3672(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_3672,c,av);}
a=C_alloc(8);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3677,a[2]=t1,a[3]=t3,a[4]=((C_word*)t0)[2],a[5]=((C_word)li55),tmp=(C_word)a,a+=6,tmp));
t5=((C_word*)t3)[1];
f_3677(t5,((C_word*)t0)[3],C_fix(0));}

/* doloop1012 in k3670 in chicken.posix#free-c-string-buffer in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_fcall f_3677(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,3)))){
C_save_and_reclaim_args((void *)trf_3677,3,t0,t1,t2);}
a=C_alloc(5);
t3=C_eqp(t2,((C_word*)t0)[2]);
if(C_truep(t3)){
t4=C_SCHEME_UNDEFINED;
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3687,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:698: chicken.memory#pointer-vector-ref */
t5=*((C_word*)lf[284]+1);{
C_word av2[4];
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[4];
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}}

/* k3685 in doloop1012 in k3670 in chicken.posix#free-c-string-buffer in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3687(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_3687,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3690,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
if(C_truep(t1)){
/* posix-common.scm:699: chicken.memory#free */
t3=*((C_word*)lf[283]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t3=((C_word*)((C_word*)t0)[2])[1];
f_3677(t3,((C_word*)t0)[3],C_fixnum_plus(((C_word*)t0)[4],C_fix(1)));}}

/* k3688 in k3685 in doloop1012 in k3670 in chicken.posix#free-c-string-buffer in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3690(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_3690,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_3677(t2,((C_word*)t0)[3],C_fixnum_plus(((C_word*)t0)[4],C_fix(1)));}

/* chicken.posix#check-environment-list in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_fcall f_3702(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_3702,3,t1,t2,t3);}
a=C_alloc(8);
t4=C_i_check_list_2(t2,t3);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3707,a[2]=t3,a[3]=((C_word)li57),tmp=(C_word)a,a+=4,tmp);
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3726,a[2]=t5,a[3]=((C_word)li58),tmp=(C_word)a,a+=4,tmp);
t7=t1;{
C_word av2[2];
av2[0]=t7;
av2[1]=(
  f_3726(t6,t2)
);
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}

/* g1025 in chicken.posix#check-environment-list in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static C_word C_fcall f_3707(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_stack_overflow_check;{}
t2=C_i_check_pair_2(t1,((C_word*)t0)[2]);
t3=C_i_check_string_2(C_u_i_car(t1),((C_word*)t0)[2]);
return(C_i_check_string_2(C_u_i_cdr(t1),((C_word*)t0)[2]));}

/* for-each-loop1024 in chicken.posix#check-environment-list in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static C_word C_fcall f_3726(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_stack_overflow_check;
loop:{}
if(C_truep(C_i_pairp(t1))){
t2=(
/* posix-common.scm:704: g1025 */
  f_3707(((C_word*)t0)[2],C_slot(t1,C_fix(0)))
);
t4=C_slot(t1,C_fix(1));
t1=t4;
goto loop;}
else{
t2=C_SCHEME_UNDEFINED;
return(t2);}}

/* nop in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3749(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3749,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.posix#call-with-exec-args in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_fcall f_3751(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6,C_word t7){
C_word tmp;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,2)))){
C_save_and_reclaim_args((void *)trf_3751,8,t0,t1,t2,t3,t4,t5,t6,t7);}
a=C_alloc(10);
t8=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_3755,a[2]=t5,a[3]=t1,a[4]=t7,a[5]=t3,a[6]=t2,a[7]=t6,a[8]=((C_word*)t0)[2],a[9]=t4,tmp=(C_word)a,a+=10,tmp);
/* posix-common.scm:715: pathname-strip-directory */
t9=((C_word*)t0)[3];{
C_word av2[3];
av2[0]=t9;
av2[1]=t8;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}

/* k3753 in chicken.posix#call-with-exec-args in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3755(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,4)))){
C_save_and_reclaim((void *)f_3755,c,av);}
a=C_alloc(11);
t2=C_a_i_cons(&a,2,t1,((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3761,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],tmp=(C_word)a,a+=8,tmp);
/* posix-common.scm:717: list->c-string-buffer */
f_3551(t3,t2,((C_word*)t0)[9],((C_word*)t0)[6]);}

/* k3759 in k3753 in chicken.posix#call-with-exec-args in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3761(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,3)))){
C_save_and_reclaim((void *)f_3761,c,av);}
a=C_alloc(15);
t2=C_SCHEME_FALSE;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3764,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t5=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_3769,a[2]=t3,a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word)li68),tmp=(C_word)a,a+=10,tmp);
/* posix-common.scm:720: scheme#call-with-current-continuation */
t6=*((C_word*)lf[281]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t4;
av2[2]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k3762 in k3759 in k3753 in chicken.posix#call-with-exec-args in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3764(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3764,c,av);}
/* posix-common.scm:715: g1060 */
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)C_fast_retrieve_proc(t2))(2,av2);}}

/* a3768 in k3759 in k3753 in chicken.posix#call-with-exec-args in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3769(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(17,c,3)))){
C_save_and_reclaim((void *)f_3769,c,av);}
a=C_alloc(17);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3775,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t2,a[5]=((C_word)li62),tmp=(C_word)a,a+=6,tmp);
t4=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_3796,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=t2,a[10]=((C_word)li67),tmp=(C_word)a,a+=11,tmp);
/* posix-common.scm:720: chicken.condition#with-exception-handler */
t5=*((C_word*)lf[280]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t1;
av2[2]=t3;
av2[3]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* a3774 in a3768 in k3759 in k3753 in chicken.posix#call-with-exec-args in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3775(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_3775,c,av);}
a=C_alloc(6);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3781,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word)li61),tmp=(C_word)a,a+=6,tmp);
/* posix-common.scm:720: k1057 */
t4=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t1;
av2[2]=t3;
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}

/* a3780 in a3774 in a3768 in k3759 in k3753 in chicken.posix#call-with-exec-args in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3781(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_3781,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3785,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:722: free-c-string-buffer */
f_3668(t2,((C_word*)t0)[4]);}

/* k3783 in a3780 in a3774 in a3768 in k3759 in k3753 in chicken.posix#call-with-exec-args in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3785(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_3785,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3788,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
if(C_truep(((C_word*)((C_word*)t0)[4])[1])){
/* posix-common.scm:723: free-c-string-buffer */
f_3668(t2,((C_word*)((C_word*)t0)[4])[1]);}
else{
/* posix-common.scm:724: chicken.condition#signal */
t3=*((C_word*)lf[276]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}

/* k3786 in k3783 in a3780 in a3774 in a3768 in k3759 in k3753 in chicken.posix#call-with-exec-args in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3788(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_3788,c,av);}
/* posix-common.scm:724: chicken.condition#signal */
t2=*((C_word*)lf[276]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* a3795 in a3768 in k3759 in k3753 in chicken.posix#call-with-exec-args in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3796(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(14,c,3)))){
C_save_and_reclaim((void *)f_3796,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_3802,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word)li64),tmp=(C_word)a,a+=10,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3877,a[2]=((C_word*)t0)[9],a[3]=((C_word)li66),tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:720: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=t1;
av2[2]=t2;
av2[3]=t3;
C_call_with_values(4,av2);}}

/* a3801 in a3795 in a3768 in k3759 in k3753 in chicken.posix#call-with-exec-args in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3802(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(15,c,3)))){
C_save_and_reclaim((void *)f_3802,c,av);}
a=C_alloc(15);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3806,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
if(C_truep(((C_word*)t0)[7])){
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3816,a[2]=((C_word*)t0)[4],a[3]=t2,a[4]=((C_word*)t0)[7],a[5]=((C_word*)t0)[8],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* posix-common.scm:728: check-environment-list */
f_3702(t3,((C_word*)t0)[7],((C_word*)t0)[6]);}
else{
t3=t2;
f_3806(t3,C_SCHEME_UNDEFINED);}}

/* k3804 in a3801 in a3795 in a3768 in k3759 in k3753 in chicken.posix#call-with-exec-args in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_fcall f_3806(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_3806,2,t0,t1);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3813,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* posix-common.scm:734: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[192]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[192]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[6];
av2[3]=((C_word*)t0)[7];
tp(4,av2);}}

/* k3811 in k3804 in a3801 in a3795 in a3768 in k3759 in k3753 in chicken.posix#call-with-exec-args in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3813(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_3813,c,av);}
/* posix-common.scm:734: proc */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
av2[3]=((C_word*)t0)[4];
av2[4]=((C_word*)((C_word*)t0)[5])[1];
((C_proc)C_fast_retrieve_proc(t2))(5,av2);}}

/* k3814 in a3801 in a3795 in a3768 in k3759 in k3753 in chicken.posix#call-with-exec-args in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3816(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(22,c,3)))){
C_save_and_reclaim((void *)f_3816,c,av);}
a=C_alloc(22);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3820,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=C_i_check_list_2(((C_word*)t0)[4],lf[289]);
t8=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3841,a[2]=t2,a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],tmp=(C_word)a,a+=5,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3843,a[2]=t5,a[3]=t10,a[4]=t6,a[5]=((C_word)li63),tmp=(C_word)a,a+=6,tmp));
t12=((C_word*)t10)[1];
f_3843(t12,t8,((C_word*)t0)[4]);}

/* k3818 in k3814 in a3801 in a3795 in a3768 in k3759 in k3753 in chicken.posix#call-with-exec-args in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3820(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3820,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=((C_word*)t0)[3];
f_3806(t3,t2);}

/* k3839 in k3814 in a3801 in a3795 in a3768 in k3759 in k3753 in chicken.posix#call-with-exec-args in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3841(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_3841,c,av);}
/* posix-common.scm:730: list->c-string-buffer */
f_3551(((C_word*)t0)[2],t1,((C_word*)t0)[3],((C_word*)t0)[4]);}

/* map-loop1068 in k3814 in a3801 in a3795 in a3768 in k3759 in k3753 in chicken.posix#call-with-exec-args in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_fcall f_3843(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,4)))){
C_save_and_reclaim_args((void *)trf_3843,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3868,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
t4=C_slot(t2,C_fix(0));
/* posix-common.scm:731: scheme#string-append */
t5=*((C_word*)lf[106]+1);{
C_word av2[5];
av2[0]=t5;
av2[1]=t3;
av2[2]=C_i_car(t4);
av2[3]=lf[290];
av2[4]=C_u_i_cdr(t4);
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k3866 in map-loop1068 in k3814 in a3801 in a3795 in a3768 in k3759 in k3753 in chicken.posix#call-with-exec-args in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 in ... */
static void C_ccall f_3868(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_3868,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_3843(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* a3876 in a3795 in a3768 in k3759 in k3753 in chicken.posix#call-with-exec-args in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3877(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +4,c,2)))){
C_save_and_reclaim((void*)f_3877,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+4);
t2=C_build_rest(&a,c,2,av);
C_word t3;
C_word t4;
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3883,a[2]=t2,a[3]=((C_word)li65),tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:720: k1057 */
t4=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t1;
av2[2]=t3;
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}

/* a3882 in a3876 in a3795 in a3768 in k3759 in k3753 in chicken.posix#call-with-exec-args in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3883(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_3883,c,av);}{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=0;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
C_apply_values(3,av2);}}

/* check in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_fcall f_3908(C_word t1,C_word t2,C_word t3,C_word t4,C_word t5){
C_word tmp;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,5)))){
C_save_and_reclaim_args((void *)trf_3908,5,t1,t2,t3,t4,t5);}
a=C_alloc(4);
if(C_truep(C_null_pointerp(t5))){
/* posix-common.scm:746: posix-error */
t6=lf[185];{
C_word av2[6];
av2[0]=t6;
av2[1]=t1;
av2[2]=lf[190];
av2[3]=t2;
av2[4]=lf[291];
av2[5]=t3;
f_2552(6,av2);}}
else{
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3921,a[2]=t5,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
if(C_truep(t4)){
/* posix-common.scm:747: ##sys#make-port */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[241]+1));
C_word av2[6];
av2[0]=*((C_word*)lf[241]+1);
av2[1]=t6;
av2[2]=C_fix(1);
av2[3]=*((C_word*)lf[242]+1);
av2[4]=lf[292];
av2[5]=lf[232];
tp(6,av2);}}
else{
/* posix-common.scm:747: ##sys#make-port */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[241]+1));
C_word av2[6];
av2[0]=*((C_word*)lf[241]+1);
av2[1]=t6;
av2[2]=C_fix(2);
av2[3]=*((C_word*)lf[242]+1);
av2[4]=lf[292];
av2[5]=lf[232];
tp(6,av2);}}}}

/* k3919 in check in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3921(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3921,c,av);}
t2=C_set_file_ptr(t1,((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.process#open-input-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3927(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +10,c,3)))){
C_save_and_reclaim((void*)f_3927,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+10);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
t4=C_i_check_string_2(t2,lf[293]);
t5=C_i_pairp(t3);
t6=(C_truep(t5)?C_slot(t3,C_fix(0)):lf[294]);
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3941,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t8=C_eqp(t6,lf[294]);
if(C_truep(t8)){
t9=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3948,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:758: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[192]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[192]+1);
av2[1]=t9;
av2[2]=t2;
av2[3]=lf[293];
tp(4,av2);}}
else{
t9=C_eqp(t6,lf[295]);
if(C_truep(t9)){
t10=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3958,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:759: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[192]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[192]+1);
av2[1]=t10;
av2[2]=t2;
av2[3]=lf[293];
tp(4,av2);}}
else{
/* posix-common.scm:743: ##sys#error */
t10=*((C_word*)lf[97]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t10;
av2[1]=t7;
av2[2]=lf[296];
av2[3]=t6;
((C_proc)(void*)(*((C_word*)t10+1)))(4,av2);}}}}

/* k3939 in chicken.process#open-input-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3941(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_3941,c,av);}
/* posix-common.scm:754: check */
f_3908(((C_word*)t0)[3],lf[293],((C_word*)t0)[4],C_SCHEME_TRUE,t1);}

/* k3946 in chicken.process#open-input-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3948(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(2,c,5)))){
C_save_and_reclaim((void *)f_3948,c,av);}
a=C_alloc(2);
t2=open_text_input_pipe(&a,1,t1);
/* posix-common.scm:754: check */
f_3908(((C_word*)t0)[3],lf[293],((C_word*)t0)[4],C_SCHEME_TRUE,t2);}

/* k3956 in chicken.process#open-input-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3958(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(2,c,5)))){
C_save_and_reclaim((void *)f_3958,c,av);}
a=C_alloc(2);
t2=open_binary_input_pipe(&a,1,t1);
/* posix-common.scm:754: check */
f_3908(((C_word*)t0)[3],lf[293],((C_word*)t0)[4],C_SCHEME_TRUE,t2);}

/* chicken.process#open-output-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3967(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +10,c,3)))){
C_save_and_reclaim((void*)f_3967,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+10);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
t4=C_i_check_string_2(t2,lf[297]);
t5=C_i_pairp(t3);
t6=(C_truep(t5)?C_slot(t3,C_fix(0)):lf[294]);
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3981,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t8=C_eqp(t6,lf[294]);
if(C_truep(t8)){
t9=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3988,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:769: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[192]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[192]+1);
av2[1]=t9;
av2[2]=t2;
av2[3]=lf[297];
tp(4,av2);}}
else{
t9=C_eqp(t6,lf[295]);
if(C_truep(t9)){
t10=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3998,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:770: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[192]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[192]+1);
av2[1]=t10;
av2[2]=t2;
av2[3]=lf[297];
tp(4,av2);}}
else{
/* posix-common.scm:743: ##sys#error */
t10=*((C_word*)lf[97]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t10;
av2[1]=t7;
av2[2]=lf[296];
av2[3]=t6;
((C_proc)(void*)(*((C_word*)t10+1)))(4,av2);}}}}

/* k3979 in chicken.process#open-output-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3981(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_3981,c,av);}
/* posix-common.scm:765: check */
f_3908(((C_word*)t0)[3],lf[297],((C_word*)t0)[4],C_SCHEME_FALSE,t1);}

/* k3986 in chicken.process#open-output-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3988(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(2,c,5)))){
C_save_and_reclaim((void *)f_3988,c,av);}
a=C_alloc(2);
t2=open_text_output_pipe(&a,1,t1);
/* posix-common.scm:765: check */
f_3908(((C_word*)t0)[3],lf[297],((C_word*)t0)[4],C_SCHEME_FALSE,t2);}

/* k3996 in chicken.process#open-output-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_3998(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(2,c,5)))){
C_save_and_reclaim((void *)f_3998,c,av);}
a=C_alloc(2);
t2=open_binary_output_pipe(&a,1,t1);
/* posix-common.scm:765: check */
f_3908(((C_word*)t0)[3],lf[297],((C_word*)t0)[4],C_SCHEME_FALSE,t2);}

/* chicken.process#close-input-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4007(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,5)))){
C_save_and_reclaim((void *)f_4007,c,av);}
a=C_alloc(4);
t3=C_i_check_port_2(t2,C_fix(1),C_SCHEME_TRUE,lf[298]);
t4=close_pipe(t2);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4014,a[2]=t1,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
t6=C_eqp(C_fix(-1),t4);
if(C_truep(t6)){
/* posix-common.scm:777: posix-error */
t7=lf[185];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t7;
av2[1]=t5;
av2[2]=lf[190];
av2[3]=lf[298];
av2[4]=lf[299];
av2[5]=t2;
f_2552(6,av2);}}
else{
t7=t1;{
C_word *av2=av;
av2[0]=t7;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}}

/* k4012 in chicken.process#close-input-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4014(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4014,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.process#close-output-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4022(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,5)))){
C_save_and_reclaim((void *)f_4022,c,av);}
a=C_alloc(4);
t3=C_i_check_port_2(t2,C_fix(2),C_SCHEME_TRUE,lf[300]);
t4=close_pipe(t2);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4029,a[2]=t1,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
t6=C_eqp(C_fix(-1),t4);
if(C_truep(t6)){
/* posix-common.scm:784: posix-error */
t7=lf[185];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t7;
av2[1]=t5;
av2[2]=lf[190];
av2[3]=lf[300];
av2[4]=lf[301];
av2[5]=t2;
f_2552(6,av2);}}
else{
t7=t1;{
C_word *av2=av;
av2[0]=t7;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}}

/* k4027 in chicken.process#close-output-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4029(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4029,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.process#with-input-from-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4037(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c<4) C_bad_min_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-4)*C_SIZEOF_PAIR +4,c,4)))){
C_save_and_reclaim((void*)f_4037,c,av);}
a=C_alloc((c-4)*C_SIZEOF_PAIR+4);
t4=C_build_rest(&a,c,4,av);
C_word t5;
C_word t6;
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4041,a[2]=t3,a[3]=t1,tmp=(C_word)a,a+=4,tmp);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=t5;
av2[2]=*((C_word*)lf[120]+1);
av2[3]=t2;
av2[4]=t4;
C_apply(5,av2);}}

/* k4039 in chicken.process#with-input-from-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4041(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(19,c,4)))){
C_save_and_reclaim((void *)f_4041,c,av);}
a=C_alloc(19);
t2=t1;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_SCHEME_FALSE;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4046,a[2]=t5,a[3]=t3,a[4]=((C_word)li75),tmp=(C_word)a,a+=5,tmp);
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4051,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word)li77),tmp=(C_word)a,a+=5,tmp);
t8=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4066,a[2]=t3,a[3]=t5,a[4]=((C_word)li78),tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:790: ##sys#dynamic-wind */
t9=*((C_word*)lf[304]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t9;
av2[1]=((C_word*)t0)[3];
av2[2]=t6;
av2[3]=t7;
av2[4]=t8;
((C_proc)(void*)(*((C_word*)t9+1)))(5,av2);}}

/* a4045 in k4039 in chicken.process#with-input-from-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4046(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4046,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,*((C_word*)lf[302]+1));
t3=C_mutate((C_word*)lf[302]+1 /* (set! ##sys#standard-input ...) */,((C_word*)((C_word*)t0)[3])[1]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* a4050 in k4039 in chicken.process#with-input-from-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4051(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_4051,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4057,a[2]=((C_word*)t0)[2],a[3]=((C_word)li76),tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:791: scheme#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=t1;
av2[2]=((C_word*)t0)[3];
av2[3]=t2;
C_call_with_values(4,av2);}}

/* a4056 in a4050 in k4039 in chicken.process#with-input-from-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4057(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +4,c,2)))){
C_save_and_reclaim((void*)f_4057,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+4);
t2=C_build_rest(&a,c,2,av);
C_word t3;
C_word t4;
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4061,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:793: chicken.process#close-input-pipe */
t4=*((C_word*)lf[117]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k4059 in a4056 in a4050 in k4039 in chicken.process#with-input-from-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4061(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4061,c,av);}{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=*((C_word*)lf[303]+1);
av2[3]=((C_word*)t0)[3];
C_apply(4,av2);}}

/* a4065 in k4039 in chicken.process#with-input-from-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4066(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4066,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,*((C_word*)lf[302]+1));
t3=C_mutate((C_word*)lf[302]+1 /* (set! ##sys#standard-input ...) */,((C_word*)((C_word*)t0)[3])[1]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* chicken.process#call-with-output-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4071(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c<4) C_bad_min_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-4)*C_SIZEOF_PAIR +4,c,4)))){
C_save_and_reclaim((void*)f_4071,c,av);}
a=C_alloc((c-4)*C_SIZEOF_PAIR+4);
t4=C_build_rest(&a,c,4,av);
C_word t5;
C_word t6;
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4075,a[2]=t3,a[3]=t1,tmp=(C_word)a,a+=4,tmp);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=t5;
av2[2]=*((C_word*)lf[121]+1);
av2[3]=t2;
av2[4]=t4;
C_apply(5,av2);}}

/* k4073 in chicken.process#call-with-output-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4075(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_4075,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4080,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word)li80),tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4086,a[2]=t1,a[3]=((C_word)li81),tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:799: scheme#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[3];
av2[2]=t2;
av2[3]=t3;
C_call_with_values(4,av2);}}

/* a4079 in k4073 in chicken.process#call-with-output-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4080(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4080,c,av);}
/* posix-common.scm:800: proc */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=((C_word*)t0)[3];
((C_proc)C_fast_retrieve_proc(t2))(3,av2);}}

/* a4085 in k4073 in chicken.process#call-with-output-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4086(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +4,c,2)))){
C_save_and_reclaim((void*)f_4086,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+4);
t2=C_build_rest(&a,c,2,av);
C_word t3;
C_word t4;
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4090,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:802: chicken.process#close-output-pipe */
t4=*((C_word*)lf[118]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k4088 in a4085 in k4073 in chicken.process#call-with-output-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4090(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4090,c,av);}{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=*((C_word*)lf[303]+1);
av2[3]=((C_word*)t0)[3];
C_apply(4,av2);}}

/* chicken.process#call-with-input-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4095(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c<4) C_bad_min_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-4)*C_SIZEOF_PAIR +4,c,4)))){
C_save_and_reclaim((void*)f_4095,c,av);}
a=C_alloc((c-4)*C_SIZEOF_PAIR+4);
t4=C_build_rest(&a,c,4,av);
C_word t5;
C_word t6;
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4099,a[2]=t3,a[3]=t1,tmp=(C_word)a,a+=4,tmp);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=t5;
av2[2]=*((C_word*)lf[120]+1);
av2[3]=t2;
av2[4]=t4;
C_apply(5,av2);}}

/* k4097 in chicken.process#call-with-input-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4099(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_4099,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4104,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word)li83),tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4110,a[2]=t1,a[3]=((C_word)li84),tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:808: scheme#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[3];
av2[2]=t2;
av2[3]=t3;
C_call_with_values(4,av2);}}

/* a4103 in k4097 in chicken.process#call-with-input-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4104(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4104,c,av);}
/* posix-common.scm:809: proc */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=((C_word*)t0)[3];
((C_proc)C_fast_retrieve_proc(t2))(3,av2);}}

/* a4109 in k4097 in chicken.process#call-with-input-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4110(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +4,c,2)))){
C_save_and_reclaim((void*)f_4110,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+4);
t2=C_build_rest(&a,c,2,av);
C_word t3;
C_word t4;
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4114,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:811: chicken.process#close-input-pipe */
t4=*((C_word*)lf[117]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k4112 in a4109 in k4097 in chicken.process#call-with-input-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4114(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4114,c,av);}{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=*((C_word*)lf[303]+1);
av2[3]=((C_word*)t0)[3];
C_apply(4,av2);}}

/* chicken.process#with-output-to-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4119(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c<4) C_bad_min_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-4)*C_SIZEOF_PAIR +4,c,4)))){
C_save_and_reclaim((void*)f_4119,c,av);}
a=C_alloc((c-4)*C_SIZEOF_PAIR+4);
t4=C_build_rest(&a,c,4,av);
C_word t5;
C_word t6;
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4123,a[2]=t3,a[3]=t1,tmp=(C_word)a,a+=4,tmp);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=t5;
av2[2]=*((C_word*)lf[121]+1);
av2[3]=t2;
av2[4]=t4;
C_apply(5,av2);}}

/* k4121 in chicken.process#with-output-to-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4123(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(19,c,4)))){
C_save_and_reclaim((void *)f_4123,c,av);}
a=C_alloc(19);
t2=t1;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_SCHEME_FALSE;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4128,a[2]=t5,a[3]=t3,a[4]=((C_word)li86),tmp=(C_word)a,a+=5,tmp);
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4133,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word)li88),tmp=(C_word)a,a+=5,tmp);
t8=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4148,a[2]=t3,a[3]=t5,a[4]=((C_word)li89),tmp=(C_word)a,a+=5,tmp);
/* posix-common.scm:817: ##sys#dynamic-wind */
t9=*((C_word*)lf[304]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t9;
av2[1]=((C_word*)t0)[3];
av2[2]=t6;
av2[3]=t7;
av2[4]=t8;
((C_proc)(void*)(*((C_word*)t9+1)))(5,av2);}}

/* a4127 in k4121 in chicken.process#with-output-to-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4128(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4128,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,*((C_word*)lf[305]+1));
t3=C_mutate((C_word*)lf[305]+1 /* (set! ##sys#standard-output ...) */,((C_word*)((C_word*)t0)[3])[1]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* a4132 in k4121 in chicken.process#with-output-to-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4133(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_4133,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4139,a[2]=((C_word*)t0)[2],a[3]=((C_word)li87),tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:818: scheme#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=t1;
av2[2]=((C_word*)t0)[3];
av2[3]=t2;
C_call_with_values(4,av2);}}

/* a4138 in a4132 in k4121 in chicken.process#with-output-to-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4139(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +4,c,2)))){
C_save_and_reclaim((void*)f_4139,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+4);
t2=C_build_rest(&a,c,2,av);
C_word t3;
C_word t4;
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4143,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:820: chicken.process#close-output-pipe */
t4=*((C_word*)lf[118]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k4141 in a4138 in a4132 in k4121 in chicken.process#with-output-to-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4143(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4143,c,av);}{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=*((C_word*)lf[303]+1);
av2[3]=((C_word*)t0)[3];
C_apply(4,av2);}}

/* a4147 in k4121 in chicken.process#with-output-to-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4148(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4148,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,*((C_word*)lf[305]+1));
t3=C_mutate((C_word*)lf[305]+1 /* (set! ##sys#standard-output ...) */,((C_word*)((C_word*)t0)[3])[1]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* chicken.file.posix#file-open in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4159(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c<4) C_bad_min_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-4)*C_SIZEOF_PAIR +6,c,3)))){
C_save_and_reclaim((void*)f_4159,c,av);}
a=C_alloc((c-4)*C_SIZEOF_PAIR+6);
t4=C_build_rest(&a,c,4,av);
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
t5=C_i_pairp(t4);
t6=(C_truep(t5)?C_get_rest_arg(c,4,av,4,t0):((C_word*)t0)[2]);
t7=C_i_check_string_2(t2,lf[306]);
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4169,a[2]=t3,a[3]=t6,a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* posixwin.scm:524: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[203]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[203]+1);
av2[1]=t8;
av2[2]=t3;
av2[3]=lf[306];
tp(4,av2);}}

/* k4167 in chicken.file.posix#file-open in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4169(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_4169,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4172,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* posixwin.scm:525: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[203]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[203]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=lf[306];
tp(4,av2);}}

/* k4170 in k4167 in chicken.file.posix#file-open in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4172(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_4172,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4176,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* posixwin.scm:526: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[192]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[192]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=lf[306];
tp(4,av2);}}

/* k4174 in k4170 in k4167 in chicken.file.posix#file-open in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4176(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_4176,c,av);}
a=C_alloc(10);
t2=C_open(t1,((C_word*)t0)[2],((C_word*)t0)[3]);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4179,a[2]=((C_word*)t0)[4],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t4=C_eqp(C_fix(-1),t2);
if(C_truep(t4)){
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4185,a[2]=t3,a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
/* posixwin.scm:528: ##sys#update-errno */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[95]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[95]+1);
av2[1]=t5;
tp(2,av2);}}
else{
t5=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* k4177 in k4174 in k4170 in k4167 in chicken.file.posix#file-open in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4179(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4179,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k4183 in k4174 in k4170 in k4167 in chicken.file.posix#file-open in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4185(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_4185,c,av);}
/* posixwin.scm:529: ##sys#signal-hook */
t2=*((C_word*)lf[92]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[190];
av2[3]=lf[306];
av2[4]=lf[307];
av2[5]=((C_word*)t0)[3];
av2[6]=((C_word*)t0)[4];
av2[7]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t2+1)))(8,av2);}}

/* chicken.file.posix#file-close in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4194(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_4194,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4198,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* posixwin.scm:534: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[203]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[203]+1);
av2[1]=t3;
av2[2]=t2;
av2[3]=lf[309];
tp(4,av2);}}

/* k4196 in chicken.file.posix#file-close in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4198(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_4198,c,av);}
a=C_alloc(7);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4203,a[2]=((C_word*)t0)[2],a[3]=t3,a[4]=((C_word)li92),tmp=(C_word)a,a+=5,tmp));
t5=((C_word*)t3)[1];{
C_word *av2=av;
av2[0]=t5;
av2[1]=((C_word*)t0)[3];
f_4203(2,av2);}}

/* loop in k4196 in chicken.file.posix#file-close in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4203(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_4203,c,av);}
if(C_truep(C_fixnum_lessp(C_close(((C_word*)t0)[2]),C_fix(0)))){
t2=C_eqp(C_fix((C_word)errno),C_fix((C_word)EINTR));
if(C_truep(t2)){
/* posixwin.scm:538: ##sys#dispatch-interrupt */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[308]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[308]+1);
av2[1]=t1;
av2[2]=((C_word*)((C_word*)t0)[3])[1];
tp(3,av2);}}
else{
/* posixwin.scm:540: posix-error */
t3=lf[185];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=lf[190];
av2[3]=lf[309];
av2[4]=lf[310];
av2[5]=((C_word*)t0)[2];
f_2552(6,av2);}}}
else{
t2=C_SCHEME_UNDEFINED;
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* chicken.file.posix#file-read in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4225(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c<4) C_bad_min_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-4)*C_SIZEOF_PAIR +6,c,3)))){
C_save_and_reclaim((void*)f_4225,c,av);}
a=C_alloc((c-4)*C_SIZEOF_PAIR+6);
t4=C_build_rest(&a,c,4,av);
C_word t5;
C_word t6;
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4229,a[2]=t2,a[3]=t3,a[4]=t1,a[5]=t4,tmp=(C_word)a,a+=6,tmp);
/* posixwin.scm:544: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[203]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[203]+1);
av2[1]=t5;
av2[2]=t2;
av2[3]=lf[311];
tp(4,av2);}}

/* k4227 in chicken.file.posix#file-read in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4229(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_4229,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4232,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* posixwin.scm:545: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[203]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[203]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=lf[311];
tp(4,av2);}}

/* k4230 in k4227 in chicken.file.posix#file-read in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4232(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_4232,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4235,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_i_pairp(((C_word*)t0)[5]))){
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_u_i_car(((C_word*)t0)[5]);
f_4235(2,av2);}}
else{
/* posixwin.scm:546: scheme#make-string */
t3=*((C_word*)lf[314]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}

/* k4233 in k4230 in k4227 in chicken.file.posix#file-read in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4235(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,5)))){
C_save_and_reclaim((void *)f_4235,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4238,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_blockp(t1))){
if(C_truep(C_byteblockp(t1))){
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_4238(2,av2);}}
else{
/* posixwin.scm:548: ##sys#signal-hook */
t3=*((C_word*)lf[92]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[193];
av2[3]=lf[311];
av2[4]=lf[313];
av2[5]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}}
else{
/* posixwin.scm:548: ##sys#signal-hook */
t3=*((C_word*)lf[92]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[193];
av2[3]=lf[311];
av2[4]=lf[313];
av2[5]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}}

/* k4236 in k4233 in k4230 in k4227 in chicken.file.posix#file-read in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4238(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_4238,c,av);}
a=C_alloc(11);
t2=C_read(((C_word*)t0)[2],((C_word*)t0)[3],((C_word*)t0)[4]);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4241,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[3],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t4=C_eqp(C_fix(-1),t2);
if(C_truep(t4)){
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4250,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* posixwin.scm:551: ##sys#update-errno */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[95]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[95]+1);
av2[1]=t5;
tp(2,av2);}}
else{
t5=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_a_i_list2(&a,2,((C_word*)t0)[3],t2);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* k4239 in k4236 in k4233 in k4230 in k4227 in chicken.file.posix#file-read in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4241(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_4241,c,av);}
a=C_alloc(6);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list2(&a,2,((C_word*)t0)[3],((C_word*)t0)[4]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k4248 in k4236 in k4233 in k4230 in k4227 in chicken.file.posix#file-read in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4250(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_4250,c,av);}
/* posixwin.scm:552: ##sys#signal-hook */
t2=*((C_word*)lf[92]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[190];
av2[3]=lf[311];
av2[4]=lf[312];
av2[5]=((C_word*)t0)[3];
av2[6]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(7,av2);}}

/* chicken.file.posix#file-write in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4268(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c<4) C_bad_min_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-4)*C_SIZEOF_PAIR +6,c,3)))){
C_save_and_reclaim((void*)f_4268,c,av);}
a=C_alloc((c-4)*C_SIZEOF_PAIR+6);
t4=C_build_rest(&a,c,4,av);
C_word t5;
C_word t6;
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4272,a[2]=t4,a[3]=t3,a[4]=t2,a[5]=t1,tmp=(C_word)a,a+=6,tmp);
/* posixwin.scm:557: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[203]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[203]+1);
av2[1]=t5;
av2[2]=t2;
av2[3]=lf[315];
tp(4,av2);}}

/* k4270 in chicken.file.posix#file-write in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4272(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,5)))){
C_save_and_reclaim((void *)f_4272,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4275,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_blockp(((C_word*)t0)[3]))){
if(C_truep(C_byteblockp(((C_word*)t0)[3]))){
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_4275(2,av2);}}
else{
/* posixwin.scm:559: ##sys#signal-hook */
t3=*((C_word*)lf[92]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[193];
av2[3]=lf[315];
av2[4]=lf[317];
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}}
else{
/* posixwin.scm:559: ##sys#signal-hook */
t3=*((C_word*)lf[92]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[193];
av2[3]=lf[315];
av2[4]=lf[317];
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}}

/* k4273 in k4270 in chicken.file.posix#file-write in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4275(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_4275,c,av);}
a=C_alloc(6);
t2=C_i_pairp(((C_word*)t0)[2]);
t3=(C_truep(t2)?C_u_i_car(((C_word*)t0)[2]):C_block_size(((C_word*)t0)[3]));
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4281,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[3],a[4]=t3,a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* posixwin.scm:561: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[203]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[203]+1);
av2[1]=t4;
av2[2]=t3;
av2[3]=lf[315];
tp(4,av2);}}

/* k4279 in k4273 in k4270 in chicken.file.posix#file-write in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4281(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_4281,c,av);}
a=C_alloc(9);
t2=C_write(((C_word*)t0)[2],((C_word*)t0)[3],((C_word*)t0)[4]);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4284,a[2]=((C_word*)t0)[5],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t4=C_eqp(C_fix(-1),t2);
if(C_truep(t4)){
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4290,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* posixwin.scm:564: ##sys#update-errno */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[95]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[95]+1);
av2[1]=t5;
tp(2,av2);}}
else{
t5=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* k4282 in k4279 in k4273 in k4270 in chicken.file.posix#file-write in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4284(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4284,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k4288 in k4279 in k4273 in k4270 in chicken.file.posix#file-write in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4290(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_4290,c,av);}
/* posixwin.scm:565: ##sys#signal-hook */
t2=*((C_word*)lf[92]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[190];
av2[3]=lf[315];
av2[4]=lf[316];
av2[5]=((C_word*)t0)[3];
av2[6]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(7,av2);}}

/* chicken.file.posix#file-mkstemp in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4308(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(40,c,2)))){
C_save_and_reclaim((void *)f_4308,c,av);}
a=C_alloc(40);
t3=C_i_check_string_2(t2,lf[318]);
t4=lf[319];
t5=C_block_size(t4);
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)va6091,a[2]=t5,a[3]=t4,a[4]=t2,a[5]=t1,a[6]=((C_word)li100),tmp=(C_word)a,a+=7,tmp);
t7=t6;
va6091(t7,C_s_a_i_times(&a,2,t5,t5));}

/* k4319 */
static void C_ccall f_4321(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(23,c,2)))){
C_save_and_reclaim((void *)f_4321,c,av);}
a=C_alloc(23);
t2=C_block_size(t1);
t3=C_fixnum_difference(t2,C_fix(1));
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4436,a[2]=t1,a[3]=((C_word)li96),tmp=(C_word)a,a+=4,tmp);
t5=(
  f_4436(t4,t3)
);
t6=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_4330,a[2]=t2,a[3]=t5,a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],a[6]=t1,a[7]=((C_word*)t0)[4],a[8]=((C_word*)t0)[5],a[9]=((C_word*)t0)[6],tmp=(C_word)a,a+=10,tmp);
t7=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4423,a[2]=t6,a[3]=((C_word*)t0)[5],a[4]=t5,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4427,a[2]=t7,tmp=(C_word)a,a+=3,tmp);
/* posixwin.scm:581: chicken.pathname#pathname-directory */
t9=*((C_word*)lf[325]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t9;
av2[1]=t8;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}

/* k4328 in k4319 */
static void C_ccall f_4330(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,3)))){
C_save_and_reclaim((void *)f_4330,c,av);}
a=C_alloc(13);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_4335,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=t3,a[9]=((C_word*)t0)[8],a[10]=((C_word)li99),tmp=(C_word)a,a+=11,tmp));
t5=((C_word*)t3)[1];
f_4335(t5,((C_word*)t0)[9],C_fix(1));}

/* loop in k4328 in k4319 */
static void C_fcall f_4335(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,0,3)))){
C_save_and_reclaim_args((void *)trf_4335,3,t0,t1,t2);}
a=C_alloc(15);
t3=C_fixnum_difference(((C_word*)t0)[2],C_fix(1));
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4382,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word)li97),tmp=(C_word)a,a+=7,tmp);
t5=(
  f_4382(t4,t3)
);
t6=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4343,a[2]=t2,a[3]=((C_word*)t0)[7],a[4]=((C_word*)t0)[8],a[5]=t1,a[6]=((C_word*)t0)[9],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* posixwin.scm:595: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[192]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[192]+1);
av2[1]=t6;
av2[2]=((C_word*)t0)[6];
av2[3]=lf[306];
tp(4,av2);}}

/* k4341 in loop in k4328 in k4319 */
static void C_ccall f_4343(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,2)))){
C_save_and_reclaim((void *)f_4343,c,av);}
a=C_alloc(15);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)va6083,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word)li98),tmp=(C_word)a,a+=10,tmp);
t3=t2;
va6083(t3,C_s_a_i_bitwise_ior(&a,2,*((C_word*)lf[53]+1),*((C_word*)lf[46]+1)));}

/* suffix-loop in loop in k4328 in k4319 */
static C_word C_fcall f_4382(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_stack_overflow_check;
loop:{}
if(C_truep(C_fixnum_greater_or_equal_p(t1,((C_word*)t0)[2]))){
t2=C_i_string_ref(((C_word*)t0)[3],C_rand(((C_word*)t0)[4]));
t3=C_i_string_set(((C_word*)t0)[5],t1,t2);
t5=C_fixnum_difference(t1,C_fix(1));
t1=t5;
goto loop;}
else{
t2=C_SCHEME_UNDEFINED;
return(t2);}}

/* k4421 in k4319 */
static void C_ccall f_4423(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_4423,c,av);}
if(C_truep(C_i_not(t1))){
/* posixwin.scm:585: ##sys#signal-hook */
t2=*((C_word*)lf[92]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[190];
av2[3]=lf[318];
av2[4]=lf[321];
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}
else{
t2=C_eqp(((C_word*)t0)[4],((C_word*)t0)[5]);
if(C_truep(t2)){
/* posixwin.scm:587: ##sys#signal-hook */
t3=*((C_word*)lf[92]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[190];
av2[3]=lf[318];
av2[4]=lf[322];
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
f_4330(2,av2);}}}}

/* k4425 in k4319 */
static void C_ccall f_4427(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_4427,c,av);}
if(C_truep(t1)){
/* posixwin.scm:581: ##sys#file-exists? */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[323]+1));
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=*((C_word*)lf[323]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
av2[5]=lf[318];
tp(6,av2);}}
else{
/* posixwin.scm:581: ##sys#file-exists? */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[323]+1));
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=*((C_word*)lf[323]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=lf[324];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
av2[5]=lf[318];
tp(6,av2);}}}

/* loop in k4319 */
static C_word C_fcall f_4436(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_stack_overflow_check;
loop:{}
t2=C_fixnum_greater_or_equal_p(t1,C_fix(0));
t3=(C_truep(t2)?C_eqp(C_i_string_ref(((C_word*)t0)[2],t1),C_make_character(88)):C_SCHEME_FALSE);
if(C_truep(t3)){
t5=C_fixnum_difference(t1,C_fix(1));
t1=t5;
goto loop;}
else{
return(C_fixnum_plus(t1,C_fix(1)));}}

/* chicken.process#create-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4467(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_4467,c,av);}
a=C_alloc(6);
t2=C_rest_nullp(c,2);
t3=(C_truep(t2)?C_fixnum_or(*((C_word*)lf[45]+1),*((C_word*)lf[50]+1)):C_get_rest_arg(c,2,av,2,t0));
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4474,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_fixnum_lessp(C_pipe(C_SCHEME_FALSE,t3),C_fix(0)))){
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4483,a[2]=t4,tmp=(C_word)a,a+=3,tmp);
/* posixwin.scm:615: ##sys#update-errno */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[95]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[95]+1);
av2[1]=t5;
tp(2,av2);}}
else{
/* posixwin.scm:617: scheme#values */{
C_word av2[4];
av2[0]=0;
av2[1]=t1;
av2[2]=C_fix((C_word)C_pipefds[ 0 ]);
av2[3]=C_fix((C_word)C_pipefds[ 1 ]);
C_values(4,av2);}}}

/* k4472 in chicken.process#create-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4474(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4474,c,av);}
/* posixwin.scm:617: scheme#values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=C_fix((C_word)C_pipefds[ 0 ]);
av2[3]=C_fix((C_word)C_pipefds[ 1 ]);
C_values(4,av2);}}

/* k4481 in chicken.process#create-pipe in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4483(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_4483,c,av);}
/* posixwin.scm:616: ##sys#signal-hook */
t2=*((C_word*)lf[92]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[190];
av2[3]=lf[327];
av2[4]=lf[328];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* chicken.posix#duplicate-fileno in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4529(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +5,c,3)))){
C_save_and_reclaim((void*)f_4529,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+5);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4533,a[2]=t1,a[3]=t2,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* posixwin.scm:672: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[203]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[203]+1);
av2[1]=t4;
av2[2]=t2;
av2[3]=lf[329];
tp(4,av2);}}

/* k4531 in chicken.posix#duplicate-fileno in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4533(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_4533,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4536,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_i_nullp(((C_word*)t0)[4]))){
t3=t2;
f_4536(t3,C_dup(((C_word*)t0)[3]));}
else{
t3=C_i_car(((C_word*)t0)[4]);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4557,a[2]=t2,a[3]=((C_word*)t0)[3],a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* posixwin.scm:676: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[203]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[203]+1);
av2[1]=t4;
av2[2]=t3;
av2[3]=lf[251];
tp(4,av2);}}}

/* k4534 in k4531 in chicken.posix#duplicate-fileno in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_fcall f_4536(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_4536,2,t0,t1);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4539,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_fixnum_lessp(t1,C_fix(0)))){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4545,a[2]=t2,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* posixwin.scm:679: ##sys#update-errno */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[95]+1));
C_word av2[2];
av2[0]=*((C_word*)lf[95]+1);
av2[1]=t3;
tp(2,av2);}}
else{
t3=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t3;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k4537 in k4534 in k4531 in chicken.posix#duplicate-fileno in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4539(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4539,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k4543 in k4534 in k4531 in chicken.posix#duplicate-fileno in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4545(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_4545,c,av);}
/* posixwin.scm:680: ##sys#signal-hook */
t2=*((C_word*)lf[92]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[190];
av2[3]=lf[251];
av2[4]=lf[330];
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* k4555 in k4531 in chicken.posix#duplicate-fileno in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4557(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4557,c,av);}
t2=((C_word*)t0)[2];
f_4536(t2,C_dup2(((C_word*)t0)[3],((C_word*)t0)[4]));}

/* chicken.time.posix#local-timezone-abbreviation in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4559(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_4559,c,av);}
a=C_alloc(5);
t2=C_a_i_bytevector(&a,1,C_fix(3));
/* posixwin.scm:687: ##sys#peek-c-string */
t3=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=stub1370(t2);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* loop in chicken.posix#quote-arg-string in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static C_word C_fcall f_4580(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_stack_overflow_check;
loop:{}
t2=C_eqp(t1,((C_word*)t0)[2]);
if(C_truep(t2)){
return(C_SCHEME_FALSE);}
else{
t3=C_i_string_ref(((C_word*)t0)[3],t1);
if(C_truep(C_u_i_char_whitespacep(t3))){
return(C_SCHEME_TRUE);}
else{
t5=C_fixnum_plus(t1,C_fix(1));
t1=t5;
goto loop;}}}

/* chicken.posix#quote-arg-string in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4605(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_4605,c,av);}
a=C_alloc(5);
t3=C_i_string_length(t2);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4580,a[2]=t3,a[3]=t2,a[4]=((C_word)li105),tmp=(C_word)a,a+=5,tmp);
t5=(
  f_4580(t4,C_fix(0))
);
if(C_truep(t5)){
/* posixwin.scm:721: scheme#string-append */
t6=*((C_word*)lf[106]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t6;
av2[1]=t1;
av2[2]=lf[332];
av2[3]=t2;
av2[4]=lf[333];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}
else{
t6=t1;{
C_word *av2=av;
av2[0]=t6;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* chicken.process#process-execute in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4617(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(7,c,7)))){
C_save_and_reclaim((void *)f_4617,c,av);}
a=C_alloc(7);
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_SCHEME_END_OF_LIST:C_get_rest_arg(c,3,av,3,t0));
t5=C_rest_nullp(c,3);
t6=C_rest_nullp(c,4);
t7=(C_truep(t6)?C_SCHEME_FALSE:C_get_rest_arg(c,4,av,3,t0));
t8=C_rest_nullp(c,4);
t9=C_rest_nullp(c,5);
t10=(C_truep(t9)?C_SCHEME_FALSE:C_get_rest_arg(c,5,av,3,t0));
t11=C_rest_nullp(c,5);
t12=(C_truep(t10)?(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4658,a[2]=((C_word)li107),tmp=(C_word)a,a+=3,tmp):lf[331]);
t13=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4644,a[2]=t2,a[3]=((C_word)li108),tmp=(C_word)a,a+=4,tmp);
/* posixwin.scm:726: call-with-exec-args */
t14=lf[288];
f_3751(t14,t1,lf[334],t2,t12,t4,t7,t13);}

/* a4643 in chicken.process#process-execute in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4644(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_4644,c,av);}
t5=C_flushall();
t6=(C_truep(t4)?C_u_i_execve(t2,t3,t4):C_u_i_execvp(t2,t3));
t7=C_eqp(t6,C_fix(-1));
if(C_truep(t7)){
/* posixwin.scm:734: posix-error */
t8=lf[185];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t8;
av2[1]=t1;
av2[2]=lf[93];
av2[3]=lf[334];
av2[4]=lf[335];
av2[5]=((C_word*)t0)[2];
f_2552(6,av2);}}
else{
t8=C_SCHEME_UNDEFINED;
t9=t1;{
C_word *av2=av;
av2[0]=t9;
av2[1]=t8;
((C_proc)(void*)(*((C_word*)t9+1)))(2,av2);}}}

/* f_4658 in chicken.process#process-execute in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4658(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4658,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.process#process-spawn in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4697(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(c<4) C_bad_min_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(11,c,3)))){
C_save_and_reclaim((void *)f_4697,c,av);}
a=C_alloc(11);
t4=C_rest_nullp(c,4);
t5=(C_truep(t4)?C_SCHEME_END_OF_LIST:C_get_rest_arg(c,4,av,4,t0));
t6=C_rest_nullp(c,4);
t7=C_rest_nullp(c,5);
t8=(C_truep(t7)?C_SCHEME_FALSE:C_get_rest_arg(c,5,av,4,t0));
t9=C_rest_nullp(c,5);
t10=C_rest_nullp(c,6);
t11=(C_truep(t10)?C_SCHEME_FALSE:C_get_rest_arg(c,6,av,4,t0));
t12=C_rest_nullp(c,6);
t13=(C_truep(t11)?(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4741,a[2]=((C_word)li110),tmp=(C_word)a,a+=3,tmp):lf[331]);
t14=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4722,a[2]=t2,a[3]=t3,a[4]=t1,a[5]=t13,a[6]=t5,a[7]=t8,tmp=(C_word)a,a+=8,tmp);
/* posixwin.scm:739: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[203]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[203]+1);
av2[1]=t14;
av2[2]=t2;
av2[3]=lf[336];
tp(4,av2);}}

/* k4720 in chicken.process#process-spawn in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4722(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,7)))){
C_save_and_reclaim((void *)f_4722,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4727,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word)li111),tmp=(C_word)a,a+=5,tmp);
/* posixwin.scm:740: call-with-exec-args */
t3=lf[288];
f_3751(t3,((C_word*)t0)[4],lf[336],((C_word*)t0)[3],((C_word*)t0)[5],((C_word*)t0)[6],((C_word*)t0)[7],t2);}

/* a4726 in k4720 in chicken.process#process-spawn in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4727(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,5)))){
C_save_and_reclaim((void *)f_4727,c,av);}
a=C_alloc(4);
t5=C_flushall();
t6=(C_truep(t4)?C_u_i_spawnvpe(((C_word*)t0)[2],t2,t3,t4):C_u_i_spawnvp(((C_word*)t0)[2],t2,t3));
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4734,a[2]=t1,a[3]=t6,tmp=(C_word)a,a+=4,tmp);
t8=C_eqp(t6,C_fix(-1));
if(C_truep(t8)){
/* posixwin.scm:748: posix-error */
t9=lf[185];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t9;
av2[1]=t7;
av2[2]=lf[93];
av2[3]=lf[336];
av2[4]=lf[337];
av2[5]=((C_word*)t0)[3];
f_2552(6,av2);}}
else{
t9=t1;{
C_word *av2=av;
av2[0]=t9;
av2[1]=t6;
((C_proc)(void*)(*((C_word*)t9+1)))(2,av2);}}}

/* k4732 in a4726 in k4720 in chicken.process#process-spawn in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4734(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4734,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* f_4741 in chicken.process#process-spawn in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4741(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4741,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.posix#shell-command in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_fcall f_4780(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_4780,2,t1,t2);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4784,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* posixwin.scm:754: chicken.process-context#get-environment-variable */
t4=*((C_word*)lf[340]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[341];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k4782 in chicken.posix#shell-command in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4784(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_4784,c,av);}
a=C_alloc(4);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
if(C_truep(C_get_shlcmd())){
/* ##sys#peek-c-string */
t2=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_mpointer(&a,(void*)C_shlcmd);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4796,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* posixwin.scm:758: ##sys#update-errno */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[95]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[95]+1);
av2[1]=t2;
tp(2,av2);}}}}

/* k4794 in k4782 in chicken.posix#shell-command in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4796(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4796,c,av);}
/* posixwin.scm:759: ##sys#error */
t2=*((C_word*)lf[97]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=lf[339];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.process#process-run in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4807(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +4,c,4)))){
C_save_and_reclaim((void*)f_4807,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+4);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
C_word t6;
C_word t7;
t4=C_i_pairp(t3);
t5=(C_truep(t4)?C_get_rest_arg(c,3,av,3,t0):C_SCHEME_FALSE);
if(C_truep(t5)){
/* posixwin.scm:768: chicken.process#process-spawn */
t6=*((C_word*)lf[113]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t6;
av2[1]=t1;
av2[2]=*((C_word*)lf[130]+1);
av2[3]=t2;
av2[4]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}
else{
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4824,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* posixwin.scm:772: shell-command */
f_4780(t6,lf[343]);}}

/* k4822 in chicken.process#process-run in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4824(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_4824,c,av);}
a=C_alloc(6);
t2=C_a_i_list2(&a,2,lf[342],((C_word*)t0)[2]);
/* posixwin.scm:770: chicken.process#process-spawn */
t3=*((C_word*)lf[113]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=*((C_word*)lf[130]+1);
av2[3]=t1;
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4837 in k4961 in k4957 in k4953 in k4949 in k4905 in a5080 in k5074 in k5071 in %process in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in ... */
static void C_ccall f_4839(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,2)))){
C_save_and_reclaim((void *)f_4839,c,av);}
a=C_alloc(16);
t2=(*a=C_CLOSURE_TYPE|15,a[1]=(C_word)f_4843,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t1,a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],a[13]=((C_word*)t0)[12],a[14]=((C_word*)t0)[13],a[15]=((C_word*)t0)[14],tmp=(C_word)a,a+=16,tmp);
if(C_truep(((C_word*)t0)[14])){
/* posixwin.scm:796: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[192]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[192]+1);
av2[1]=t2;
av2[2]=C_i_foreign_string_argumentp(((C_word*)t0)[14]);
tp(3,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
f_4843(2,av2);}}}

/* k4841 in k4837 in k4961 in k4957 in k4953 in k4949 in k4905 in a5080 in k5074 in k5071 in %process in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in ... */
static void C_ccall f_4843(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_4843,c,av);}
a=C_alloc(7);
t2=(C_truep(((C_word*)t0)[2])?C_i_foreign_pointer_argumentp(((C_word*)t0)[2]):C_SCHEME_FALSE);
t3=(C_truep(((C_word*)t0)[3])?C_i_foreign_pointer_argumentp(((C_word*)t0)[3]):C_SCHEME_FALSE);
t4=(C_truep(((C_word*)t0)[4])?C_i_foreign_pointer_argumentp(((C_word*)t0)[4]):C_SCHEME_FALSE);
t5=(C_truep(((C_word*)t0)[5])?C_i_foreign_pointer_argumentp(((C_word*)t0)[5]):C_SCHEME_FALSE);
t6=C_i_foreign_fixnum_argumentp(((C_word*)t0)[6]);
if(C_truep(stub1459(C_SCHEME_UNDEFINED,((C_word*)t0)[7],t1,C_SCHEME_FALSE,t2,t3,t4,t5,t6))){
t7=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4924,a[2]=((C_word*)t0)[8],a[3]=((C_word*)t0)[9],a[4]=((C_word*)t0)[10],a[5]=((C_word*)t0)[11],a[6]=((C_word*)t0)[12],tmp=(C_word)a,a+=7,tmp);
/* posixwin.scm:816: chicken.file.posix#open-input-file* */
t8=*((C_word*)lf[42]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t8;
av2[1]=t7;
av2[2]=C_fix((C_word)*((int *)C_data_pointer(((C_word*)t0)[13])));
((C_proc)(void*)(*((C_word*)t8+1)))(3,av2);}}
else{
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4944,a[2]=((C_word*)t0)[8],a[3]=((C_word*)t0)[14],a[4]=((C_word*)t0)[15],tmp=(C_word)a,a+=5,tmp);
/* posixwin.scm:824: ##sys#update-errno */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[95]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[95]+1);
av2[1]=t7;
tp(2,av2);}}}

/* k4905 in a5080 in k5074 in k5071 in %process in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4907(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(23,c,5)))){
C_save_and_reclaim((void *)f_4907,c,av);}
a=C_alloc(23);
t2=C_a_i_bytevector(&a,1,C_fix(1));
t3=((*(int *)C_data_pointer(t2))=C_unfix(C_fix(-1)),C_SCHEME_UNDEFINED);
t4=C_a_i_bytevector(&a,1,C_fix(1));
t5=((*(int *)C_data_pointer(t4))=C_unfix(C_fix(-1)),C_SCHEME_UNDEFINED);
t6=C_a_i_bytevector(&a,1,C_fix(1));
t7=((*(int *)C_data_pointer(t6))=C_unfix(C_fix(-1)),C_SCHEME_UNDEFINED);
t8=C_a_i_bytevector(&a,1,C_fix(1));
t9=((*(int *)C_data_pointer(t8))=C_unfix(C_fix(-1)),C_SCHEME_UNDEFINED);
t10=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_4951,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t2,a[5]=t8,a[6]=t4,a[7]=t6,a[8]=((C_word*)t0)[4],a[9]=t1,a[10]=((C_word*)t0)[5],tmp=(C_word)a,a+=11,tmp);
/* posixwin.scm:811: ##sys#make-locative */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[345]+1));
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=*((C_word*)lf[345]+1);
av2[1]=t10;
av2[2]=t2;
av2[3]=C_fix(0);
av2[4]=C_SCHEME_FALSE;
av2[5]=lf[346];
tp(6,av2);}}

/* k4922 in k4841 in k4837 in k4961 in k4957 in k4953 in k4949 in k4905 in a5080 in k5074 in k5071 in %process in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in ... */
static void C_ccall f_4924(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_4924,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4928,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* posixwin.scm:818: chicken.file.posix#open-output-file* */
t3=*((C_word*)lf[43]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_fix((C_word)*((int *)C_data_pointer(((C_word*)t0)[6])));
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4926 in k4922 in k4841 in k4837 in k4961 in k4957 in k4953 in k4949 in k4905 in a5080 in k5074 in k5071 in %process in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in ... */
static void C_ccall f_4928(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,5)))){
C_save_and_reclaim((void *)f_4928,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4932,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(((C_word*)t0)[5])){
/* posixwin.scm:821: chicken.file.posix#open-input-file* */
t3=*((C_word*)lf[42]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_fix((C_word)*((int *)C_data_pointer(((C_word*)t0)[6])));
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
/* posixwin.scm:815: scheme#values */{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
av2[4]=C_fix((C_word)*((int *)C_data_pointer(((C_word*)t0)[4])));
av2[5]=C_SCHEME_FALSE;
C_values(6,av2);}}}

/* k4930 in k4926 in k4922 in k4841 in k4837 in k4961 in k4957 in k4953 in k4949 in k4905 in a5080 in k5074 in k5071 in %process in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in ... */
static void C_ccall f_4932(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_4932,c,av);}
/* posixwin.scm:815: scheme#values */{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
av2[4]=C_fix((C_word)*((int *)C_data_pointer(((C_word*)t0)[5])));
av2[5]=t1;
C_values(6,av2);}}

/* k4942 in k4841 in k4837 in k4961 in k4957 in k4953 in k4949 in k4905 in a5080 in k5074 in k5071 in %process in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in ... */
static void C_ccall f_4944(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_4944,c,av);}
/* posixwin.scm:825: ##sys#signal-hook */
t2=*((C_word*)lf[92]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[93];
av2[3]=((C_word*)t0)[3];
av2[4]=lf[344];
av2[5]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* k4949 in k4905 in a5080 in k5074 in k5071 in %process in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4951(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,5)))){
C_save_and_reclaim((void *)f_4951,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_4955,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],tmp=(C_word)a,a+=12,tmp);
/* posixwin.scm:812: ##sys#make-locative */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[345]+1));
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=*((C_word*)lf[345]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[6];
av2[3]=C_fix(0);
av2[4]=C_SCHEME_FALSE;
av2[5]=lf[346];
tp(6,av2);}}

/* k4953 in k4949 in k4905 in a5080 in k5074 in k5071 in %process in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4955(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,5)))){
C_save_and_reclaim((void *)f_4955,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_4959,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],tmp=(C_word)a,a+=13,tmp);
/* posixwin.scm:812: ##sys#make-locative */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[345]+1));
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=*((C_word*)lf[345]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[8];
av2[3]=C_fix(0);
av2[4]=C_SCHEME_FALSE;
av2[5]=lf[346];
tp(6,av2);}}

/* k4957 in k4953 in k4949 in k4905 in a5080 in k5074 in k5071 in %process in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4959(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,5)))){
C_save_and_reclaim((void *)f_4959,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_4963,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],a[13]=((C_word*)t0)[12],tmp=(C_word)a,a+=14,tmp);
/* posixwin.scm:812: ##sys#make-locative */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[345]+1));
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=*((C_word*)lf[345]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[7];
av2[3]=C_fix(0);
av2[4]=C_SCHEME_FALSE;
av2[5]=lf[346];
tp(6,av2);}}

/* k4961 in k4957 in k4953 in k4949 in k4905 in a5080 in k5074 in k5071 in %process in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 in ... */
static void C_ccall f_4963(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,2)))){
C_save_and_reclaim((void *)f_4963,c,av);}
a=C_alloc(15);
t2=(C_truep(((C_word*)t0)[2])?C_fix(0):C_fix(4));
t3=(*a=C_CLOSURE_TYPE|14,a[1]=(C_word)f_4839,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=t1,a[6]=t2,a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[2],a[10]=((C_word*)t0)[8],a[11]=((C_word*)t0)[9],a[12]=((C_word*)t0)[10],a[13]=((C_word*)t0)[11],a[14]=((C_word*)t0)[12],tmp=(C_word)a,a+=15,tmp);
if(C_truep(((C_word*)t0)[13])){
/* posixwin.scm:796: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[192]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[192]+1);
av2[1]=t3;
av2[2]=C_i_foreign_string_argumentp(((C_word*)t0)[13]);
tp(3,av2);}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
f_4839(2,av2);}}}

/* k4981 in a5080 in k5074 in k5071 in %process in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_4983(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4983,c,av);}
/* posixwin.scm:803: chicken.string#string-intersperse */
t2=*((C_word*)lf[347]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* map-loop1504 in a5080 in k5074 in k5071 in %process in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_fcall f_4991(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_4991,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5016,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* posixwin.scm:806: g1510 */
t4=lf[331];{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
f_4605(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k5014 in map-loop1504 in a5080 in k5074 in k5071 in %process in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5016(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_5016,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_4991(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* %process in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_fcall f_5030(C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6,C_word t7){
C_word tmp;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(29,0,3)))){
C_save_and_reclaim_args((void *)trf_5030,7,t1,t2,t3,t4,t5,t6,t7);}
a=C_alloc(29);
t8=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t9=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t10=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t11=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5032,a[2]=t2,a[3]=((C_word)li117),tmp=(C_word)a,a+=4,tmp);
t12=C_i_check_string_2(((C_word*)t8)[1],t2);
t13=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_5073,a[2]=t8,a[3]=t9,a[4]=t10,a[5]=t3,a[6]=t2,a[7]=t1,a[8]=t6,tmp=(C_word)a,a+=9,tmp);
if(C_truep(((C_word*)t9)[1])){
/* posixwin.scm:836: chkstrlst */
t14=t11;
f_5032(t14,t13,((C_word*)t9)[1]);}
else{
t14=C_set_block_item(t10,0,C_SCHEME_TRUE);
t15=((C_word*)t8)[1];
t16=C_a_i_list2(&a,2,lf[342],t15);
t17=C_set_block_item(t9,0,t16);
t18=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5112,a[2]=t8,a[3]=t13,tmp=(C_word)a,a+=4,tmp);
/* posixwin.scm:840: shell-command */
f_4780(t18,t2);}}

/* chkstrlst in %process in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_fcall f_5032(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_5032,3,t0,t1,t2);}
a=C_alloc(8);
t3=C_i_check_list_2(t2,((C_word*)t0)[2]);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5037,a[2]=((C_word*)t0)[2],a[3]=((C_word)li115),tmp=(C_word)a,a+=4,tmp);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5046,a[2]=t4,a[3]=((C_word)li116),tmp=(C_word)a,a+=4,tmp);
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=(
  f_5046(t5,t2)
);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}

/* g1581 in chkstrlst in %process in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static C_word C_fcall f_5037(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_stack_overflow_check;{}
return(C_i_check_string_2(t1,((C_word*)t0)[2]));}

/* for-each-loop1580 in chkstrlst in %process in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static C_word C_fcall f_5046(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_stack_overflow_check;
loop:{}
if(C_truep(C_i_pairp(t1))){
t2=(
/* posixwin.scm:833: g1581 */
  f_5037(((C_word*)t0)[2],C_slot(t1,C_fix(0)))
);
t4=C_slot(t1,C_fix(1));
t1=t4;
goto loop;}
else{
t2=C_SCHEME_UNDEFINED;
return(t2);}}

/* k5071 in %process in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5073(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_5073,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_5076,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
if(C_truep(((C_word*)t0)[8])){
/* posixwin.scm:841: check-environment-list */
f_3702(t2,((C_word*)t0)[8],((C_word*)t0)[6]);}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_5076(2,av2);}}}

/* k5074 in k5071 in %process in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5076(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,6)))){
C_save_and_reclaim((void *)f_5076,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_5081,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word)li119),tmp=(C_word)a,a+=8,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5087,a[2]=((C_word*)t0)[5],a[3]=((C_word)li120),tmp=(C_word)a,a+=4,tmp);
/* posixwin.scm:842: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[7];
av2[2]=t2;
av2[3]=t3;
C_call_with_values(4,av2);}}

/* a5080 in k5074 in k5071 in %process in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5081(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(28,c,3)))){
C_save_and_reclaim((void *)f_5081,c,av);}
a=C_alloc(28);
t2=((C_word*)((C_word*)t0)[2])[1];
t3=((C_word*)((C_word*)t0)[3])[1];
t4=C_a_i_list(&a,1,((C_word*)((C_word*)t0)[4])[1]);
t5=C_i_nullp(t4);
t6=(C_truep(t5)?C_SCHEME_FALSE:C_i_car(t4));
t7=C_a_i_cons(&a,2,t2,t3);
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4907,a[2]=((C_word*)t0)[5],a[3]=t1,a[4]=((C_word*)t0)[6],a[5]=t2,tmp=(C_word)a,a+=6,tmp);
t9=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4983,a[2]=t8,tmp=(C_word)a,a+=3,tmp);
if(C_truep(t6)){
/* posixwin.scm:803: chicken.string#string-intersperse */
t10=*((C_word*)lf[347]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t10;
av2[1]=t8;
av2[2]=t7;
((C_proc)(void*)(*((C_word*)t10+1)))(3,av2);}}
else{
t10=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t11=t10;
t12=(*a=C_VECTOR_TYPE|1,a[1]=t11,tmp=(C_word)a,a+=2,tmp);
t13=((C_word*)t12)[1];
t14=lf[331];
t15=C_SCHEME_UNDEFINED;
t16=(*a=C_VECTOR_TYPE|1,a[1]=t15,tmp=(C_word)a,a+=2,tmp);
t17=C_set_block_item(t16,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4991,a[2]=t12,a[3]=t16,a[4]=t13,a[5]=((C_word)li118),tmp=(C_word)a,a+=6,tmp));
t18=((C_word*)t16)[1];
f_4991(t18,t9,t7);}}

/* a5086 in k5074 in k5071 in %process in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5087(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word *a;
if(c!=6) C_bad_argc_2(c,6,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_5087,c,av);}
if(C_truep(((C_word*)t0)[2])){
/* posixwin.scm:845: scheme#values */{
C_word *av2=av;
av2[0]=0;
av2[1]=t1;
av2[2]=t2;
av2[3]=t3;
av2[4]=t4;
av2[5]=t5;
C_values(6,av2);}}
else{
/* posixwin.scm:846: scheme#values */{
C_word *av2=av;
av2[0]=0;
av2[1]=t1;
av2[2]=t2;
av2[3]=t3;
av2[4]=t4;
C_values(5,av2);}}}

/* k5110 in %process in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5112(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5112,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
f_5073(2,av2);}}

/* chicken.process#process in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5114(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_5114,c,av);}
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,3,t0));
t5=C_rest_nullp(c,3);
t6=C_rest_nullp(c,4);
t7=(C_truep(t6)?C_SCHEME_FALSE:C_get_rest_arg(c,4,av,3,t0));
t8=C_rest_nullp(c,4);
t9=C_rest_nullp(c,5);
t10=(C_truep(t9)?C_SCHEME_FALSE:C_get_rest_arg(c,5,av,3,t0));
if(C_truep(C_rest_nullp(c,5))){
/* posixwin.scm:849: %process */
f_5030(t1,lf[348],C_SCHEME_FALSE,t2,t4,t7,t10);}
else{
/* posixwin.scm:849: %process */
f_5030(t1,lf[348],C_SCHEME_FALSE,t2,t4,t7,t10);}}

/* chicken.process#process* in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5174(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_5174,c,av);}
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,3,t0));
t5=C_rest_nullp(c,3);
t6=C_rest_nullp(c,4);
t7=(C_truep(t6)?C_SCHEME_FALSE:C_get_rest_arg(c,4,av,3,t0));
t8=C_rest_nullp(c,4);
t9=C_rest_nullp(c,5);
t10=(C_truep(t9)?C_SCHEME_FALSE:C_get_rest_arg(c,5,av,3,t0));
if(C_truep(C_rest_nullp(c,5))){
/* posixwin.scm:852: %process */
f_5030(t1,lf[349],C_SCHEME_TRUE,t2,t4,t7,t10);}
else{
/* posixwin.scm:852: %process */
f_5030(t1,lf[349],C_SCHEME_TRUE,t2,t4,t7,t10);}}

/* chicken.process-context.posix#current-user-name in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5246(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_5246,c,av);}
a=C_alloc(3);
if(C_truep(C_get_user_name())){
/* ##sys#peek-c-string */
t2=*((C_word*)lf[187]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=C_mpointer(&a,(void*)C_username);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5256,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posixwin.scm:871: ##sys#update-errno */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[95]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[95]+1);
av2[1]=t2;
tp(2,av2);}}}

/* k5254 in chicken.process-context.posix#current-user-name in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5256(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5256,c,av);}
/* posixwin.scm:872: ##sys#error */
t2=*((C_word*)lf[97]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[350];
av2[3]=lf[351];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.file.posix#create-fifo in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5267(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5267,c,av);}
/* posixwin.scm:878: chicken.base#error */
t2=*((C_word*)lf[103]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[4];
av2[3]=lf[0];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.process-context.posix#create-session in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5273(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5273,c,av);}
/* posixwin.scm:879: chicken.base#error */
t2=*((C_word*)lf[103]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[181];
av2[3]=lf[0];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.file.posix#create-symbolic-link in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5279(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5279,c,av);}
/* posixwin.scm:880: chicken.base#error */
t2=*((C_word*)lf[103]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[5];
av2[3]=lf[0];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.process-context.posix#current-effective-group-id in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5285(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5285,c,av);}
/* posixwin.scm:881: chicken.base#error */
t2=*((C_word*)lf[103]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[173];
av2[3]=lf[0];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.process-context.posix#current-effective-user-id in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5291(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5291,c,av);}
/* posixwin.scm:882: chicken.base#error */
t2=*((C_word*)lf[103]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[174];
av2[3]=lf[0];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.process-context.posix#current-effective-user-name in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5297(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5297,c,av);}
/* posixwin.scm:883: chicken.base#error */
t2=*((C_word*)lf[103]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[180];
av2[3]=lf[0];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.process-context.posix#current-group-id in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5303(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5303,c,av);}
/* posixwin.scm:884: chicken.base#error */
t2=*((C_word*)lf[103]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[175];
av2[3]=lf[0];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.process-context.posix#current-user-id in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5309(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5309,c,av);}
/* posixwin.scm:885: chicken.base#error */
t2=*((C_word*)lf[103]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[176];
av2[3]=lf[0];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.file.posix#file-control in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5315(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5315,c,av);}
/* posixwin.scm:887: chicken.base#error */
t2=*((C_word*)lf[103]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[14];
av2[3]=lf[0];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.file.posix#file-link in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5321(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5321,c,av);}
/* posixwin.scm:888: chicken.base#error */
t2=*((C_word*)lf[103]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[17];
av2[3]=lf[0];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.file.posix#file-lock in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5327(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5327,c,av);}
/* posixwin.scm:889: chicken.base#error */
t2=*((C_word*)lf[103]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[18];
av2[3]=lf[0];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.file.posix#file-lock/blocking in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5333(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5333,c,av);}
/* posixwin.scm:890: chicken.base#error */
t2=*((C_word*)lf[103]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[19];
av2[3]=lf[0];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.file.posix#file-select in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5339(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5339,c,av);}
/* posixwin.scm:891: chicken.base#error */
t2=*((C_word*)lf[103]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[26];
av2[3]=lf[0];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.file.posix#file-test-lock in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5345(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5345,c,av);}
/* posixwin.scm:892: chicken.base#error */
t2=*((C_word*)lf[103]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[27];
av2[3]=lf[0];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.file.posix#file-truncate in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5351(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5351,c,av);}
/* posixwin.scm:893: chicken.base#error */
t2=*((C_word*)lf[103]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[28];
av2[3]=lf[0];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.file.posix#file-unlock in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5357(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5357,c,av);}
/* posixwin.scm:894: chicken.base#error */
t2=*((C_word*)lf[103]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[29];
av2[3]=lf[0];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.process-context.posix#parent-process-id in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5363(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5363,c,av);}
/* posixwin.scm:895: chicken.base#error */
t2=*((C_word*)lf[103]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[178];
av2[3]=lf[0];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.process#process-fork in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5369(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5369,c,av);}
/* posixwin.scm:896: chicken.base#error */
t2=*((C_word*)lf[103]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[110];
av2[3]=lf[0];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.process-context.posix#process-group-id in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5375(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5375,c,av);}
/* posixwin.scm:897: chicken.base#error */
t2=*((C_word*)lf[103]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[182];
av2[3]=lf[0];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.process#process-signal in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5381(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5381,c,av);}
/* posixwin.scm:898: chicken.base#error */
t2=*((C_word*)lf[103]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[112];
av2[3]=lf[0];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.file.posix#read-symbolic-link in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5387(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5387,c,av);}
/* posixwin.scm:899: chicken.base#error */
t2=*((C_word*)lf[103]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[6];
av2[3]=lf[0];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.process.signal#set-alarm! in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5393(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5393,c,av);}
/* posixwin.scm:900: chicken.base#error */
t2=*((C_word*)lf[103]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[134];
av2[3]=lf[0];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.process-context.posix#set-root-directory! in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5399(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5399,c,av);}
/* posixwin.scm:901: chicken.base#error */
t2=*((C_word*)lf[103]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[172];
av2[3]=lf[0];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.process.signal#set-signal-mask! in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5405(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5405,c,av);}
/* posixwin.scm:902: chicken.base#error */
t2=*((C_word*)lf[103]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[136];
av2[3]=lf[0];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.process.signal#signal-mask in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5411(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5411,c,av);}
/* posixwin.scm:903: chicken.base#error */
t2=*((C_word*)lf[103]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[138];
av2[3]=lf[0];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.process.signal#signal-mask! in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5417(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5417,c,av);}
/* posixwin.scm:904: chicken.base#error */
t2=*((C_word*)lf[103]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[139];
av2[3]=lf[0];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.process.signal#signal-masked? in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5423(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5423,c,av);}
/* posixwin.scm:905: chicken.base#error */
t2=*((C_word*)lf[103]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[140];
av2[3]=lf[0];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.process.signal#signal-unmask! in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5429(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5429,c,av);}
/* posixwin.scm:906: chicken.base#error */
t2=*((C_word*)lf[103]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[141];
av2[3]=lf[0];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.process-context.posix#user-information in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5435(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5435,c,av);}
/* posixwin.scm:907: chicken.base#error */
t2=*((C_word*)lf[103]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[183];
av2[3]=lf[0];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.time.posix#utc-time->seconds in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5441(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5441,c,av);}
/* posixwin.scm:908: chicken.base#error */
t2=*((C_word*)lf[103]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[82];
av2[3]=lf[0];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.time.posix#string->time in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5447(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5447,c,av);}
/* posixwin.scm:909: chicken.base#error */
t2=*((C_word*)lf[103]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[86];
av2[3]=lf[0];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.errno#errno in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5465(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5465,c,av);}
/* posix.scm:374: ##sys#errno */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[354]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[354]+1);
av2[1]=t1;
tp(2,av2);}}

/* a5509 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5510(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_5510,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5514,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:637: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[203]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[203]+1);
av2[1]=t3;
av2[2]=t2;
av2[3]=lf[394];
tp(4,av2);}}

/* k5512 in a5509 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5514(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5514,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_slot(*((C_word*)lf[270]+1),((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a5518 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5519(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_5519,c,av);}
a=C_alloc(4);
t2=C_rest_nullp(c,2);
t3=(C_truep(t2)?C_SCHEME_FALSE:C_get_rest_arg(c,2,av,2,t0));
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5526,a[2]=t3,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
if(C_truep(t3)){
/* posix-common.scm:562: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[203]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[203]+1);
av2[1]=t4;
av2[2]=t3;
av2[3]=lf[397];
tp(4,av2);}}
else{
t5=t4;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_UNDEFINED;
f_5526(2,av2);}}}

/* k5524 in a5518 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5526(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5526,c,av);}
if(C_truep(((C_word*)t0)[2])){
t2=C_umask(((C_word*)t0)[2]);
if(C_truep(((C_word*)t0)[2])){
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_umask(t2);
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}
else{
t2=C_umask(C_fix(0));
if(C_truep(((C_word*)t0)[2])){
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_umask(t2);
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}}

/* a5543 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5544(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_5544,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5548,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:567: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[203]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[203]+1);
av2[1]=t3;
av2[2]=t2;
av2[3]=lf[397];
tp(4,av2);}}

/* k5546 in a5543 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5548(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5548,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_umask(((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a5549 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5550(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_5550,c,av);}
a=C_alloc(8);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5554,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5566,a[2]=t2,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* posix-common.scm:403: chicken.base#port? */
t5=*((C_word*)lf[195]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5552 in a5549 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5554(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,5)))){
C_save_and_reclaim((void *)f_5554,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5557,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_i_lessp(t1,C_fix(0)))){
/* posix-common.scm:412: posix-error */
t3=lf[185];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[190];
av2[3]=lf[399];
av2[4]=lf[400];
av2[5]=((C_word*)t0)[3];
f_2552(6,av2);}}
else{
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k5555 in k5552 in a5549 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5557(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5557,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k5564 in a5549 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5566(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,5)))){
C_save_and_reclaim((void *)f_5566,c,av);}
a=C_alloc(7);
if(C_truep(t1)){
t2=C_slot(((C_word*)t0)[2],C_fix(7));
t3=C_eqp(t2,lf[232]);
if(C_truep(t3)){
t4=C_ftell(&a,1,((C_word*)t0)[2]);
t5=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
f_5554(2,av2);}}
else{
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_fix(-1);
f_5554(2,av2);}}}
else{
if(C_truep(C_fixnump(((C_word*)t0)[2]))){
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_lseek(((C_word*)t0)[2],C_fix(0),C_fix((C_word)SEEK_CUR));
f_5554(2,av2);}}
else{
/* posix-common.scm:410: ##sys#signal-hook */
t2=*((C_word*)lf[92]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[193];
av2[3]=lf[399];
av2[4]=lf[401];
av2[5]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}}}

/* a5586 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5587(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_5587,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5591,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:326: stat */
f_2570(t3,t2,C_SCHEME_FALSE,C_SCHEME_TRUE,lf[201]);}

/* k5589 in a5586 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5591(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5591,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)C_stat_perm);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a5592 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5593(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_5593,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5597,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:319: stat */
f_2570(t3,t2,C_SCHEME_FALSE,C_SCHEME_TRUE,lf[404]);}

/* k5595 in a5592 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5597(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5597,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)C_statbuf.st_gid);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a5598 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5599(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_5599,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5603,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* posix-common.scm:313: stat */
f_2570(t3,t2,C_SCHEME_FALSE,C_SCHEME_TRUE,lf[406]);}

/* k5601 in a5598 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_ccall f_5603(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5603,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)C_statbuf.st_uid);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* toplevel */
static C_TLS int toplevel_initialized=0;

void C_ccall C_posix_toplevel(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(toplevel_initialized) {C_kontinue(t1,C_SCHEME_UNDEFINED);}
else C_toplevel_entry(C_text("posix"));
C_check_nursery_minimum(C_calculate_demand(3,c,2));
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void*)C_posix_toplevel,c,av);}
toplevel_initialized=1;
if(C_unlikely(!C_demand_2(2296))){
C_save(t1);
C_rereclaim2(2296*sizeof(C_word),1);
t1=C_restore;}
a=C_alloc(3);
C_initialize_lf(lf,408);
lf[1]=C_decode_literal(C_heaptop,C_text("\376B\000\000/this function is not available on this platform"));
lf[2]=C_h_intern(&lf[2],5, C_text("posix"));
lf[3]=C_h_intern(&lf[3],19, C_text("chicken.file.posix#"));
lf[4]=C_h_intern(&lf[4],30, C_text("chicken.file.posix#create-fifo"));
lf[5]=C_h_intern(&lf[5],39, C_text("chicken.file.posix#create-symbolic-link"));
lf[6]=C_h_intern(&lf[6],37, C_text("chicken.file.posix#read-symbolic-link"));
lf[7]=C_h_intern(&lf[7],35, C_text("chicken.file.posix#duplicate-fileno"));
lf[8]=C_h_intern(&lf[8],30, C_text("chicken.file.posix#fcntl/dupfd"));
lf[9]=C_h_intern(&lf[9],30, C_text("chicken.file.posix#fcntl/getfd"));
lf[10]=C_h_intern(&lf[10],30, C_text("chicken.file.posix#fcntl/getfl"));
lf[11]=C_h_intern(&lf[11],30, C_text("chicken.file.posix#fcntl/setfd"));
lf[12]=C_h_intern(&lf[12],30, C_text("chicken.file.posix#fcntl/setfl"));
lf[13]=C_h_intern(&lf[13],29, C_text("chicken.file.posix#file-close"));
lf[14]=C_h_intern(&lf[14],31, C_text("chicken.file.posix#file-control"));
lf[15]=C_h_intern(&lf[15],37, C_text("chicken.file.posix#file-creation-mode"));
lf[16]=C_h_intern(&lf[16],29, C_text("chicken.file.posix#file-group"));
lf[17]=C_h_intern(&lf[17],28, C_text("chicken.file.posix#file-link"));
lf[18]=C_h_intern(&lf[18],28, C_text("chicken.file.posix#file-lock"));
lf[19]=C_h_intern(&lf[19],37, C_text("chicken.file.posix#file-lock/blocking"));
lf[20]=C_h_intern(&lf[20],31, C_text("chicken.file.posix#file-mkstemp"));
lf[21]=C_h_intern(&lf[21],28, C_text("chicken.file.posix#file-open"));
lf[22]=C_h_intern(&lf[22],29, C_text("chicken.file.posix#file-owner"));
lf[23]=C_h_intern(&lf[23],35, C_text("chicken.file.posix#file-permissions"));
lf[24]=C_h_intern(&lf[24],32, C_text("chicken.file.posix#file-position"));
lf[25]=C_h_intern(&lf[25],28, C_text("chicken.file.posix#file-read"));
lf[26]=C_h_intern(&lf[26],30, C_text("chicken.file.posix#file-select"));
lf[27]=C_h_intern(&lf[27],33, C_text("chicken.file.posix#file-test-lock"));
lf[28]=C_h_intern(&lf[28],32, C_text("chicken.file.posix#file-truncate"));
lf[29]=C_h_intern(&lf[29],30, C_text("chicken.file.posix#file-unlock"));
lf[30]=C_h_intern(&lf[30],29, C_text("chicken.file.posix#file-write"));
lf[31]=C_h_intern(&lf[31],28, C_text("chicken.file.posix#file-type"));
lf[32]=C_h_intern(&lf[32],32, C_text("chicken.file.posix#block-device\077"));
lf[33]=C_h_intern(&lf[33],36, C_text("chicken.file.posix#character-device\077"));
lf[34]=C_h_intern(&lf[34],29, C_text("chicken.file.posix#directory\077"));
lf[35]=C_h_intern(&lf[35],24, C_text("chicken.file.posix#fifo\077"));
lf[36]=C_h_intern(&lf[36],32, C_text("chicken.file.posix#regular-file\077"));
lf[37]=C_h_intern(&lf[37],26, C_text("chicken.file.posix#socket\077"));
lf[38]=C_h_intern(&lf[38],33, C_text("chicken.file.posix#symbolic-link\077"));
lf[39]=C_h_intern(&lf[39],32, C_text("chicken.file.posix#fileno/stderr"));
lf[40]=C_h_intern(&lf[40],31, C_text("chicken.file.posix#fileno/stdin"));
lf[41]=C_h_intern(&lf[41],32, C_text("chicken.file.posix#fileno/stdout"));
lf[42]=C_h_intern(&lf[42],35, C_text("chicken.file.posix#open-input-file\052"));
lf[43]=C_h_intern(&lf[43],36, C_text("chicken.file.posix#open-output-file\052"));
lf[44]=C_h_intern(&lf[44],30, C_text("chicken.file.posix#open/append"));
lf[45]=C_h_intern(&lf[45],30, C_text("chicken.file.posix#open/binary"));
lf[46]=C_h_intern(&lf[46],29, C_text("chicken.file.posix#open/creat"));
lf[47]=C_h_intern(&lf[47],28, C_text("chicken.file.posix#open/excl"));
lf[48]=C_h_intern(&lf[48],29, C_text("chicken.file.posix#open/fsync"));
lf[49]=C_h_intern(&lf[49],30, C_text("chicken.file.posix#open/noctty"));
lf[50]=C_h_intern(&lf[50],33, C_text("chicken.file.posix#open/noinherit"));
lf[51]=C_h_intern(&lf[51],32, C_text("chicken.file.posix#open/nonblock"));
lf[52]=C_h_intern(&lf[52],30, C_text("chicken.file.posix#open/rdonly"));
lf[53]=C_h_intern(&lf[53],28, C_text("chicken.file.posix#open/rdwr"));
lf[54]=C_h_intern(&lf[54],28, C_text("chicken.file.posix#open/read"));
lf[55]=C_h_intern(&lf[55],28, C_text("chicken.file.posix#open/sync"));
lf[56]=C_h_intern(&lf[56],28, C_text("chicken.file.posix#open/text"));
lf[57]=C_h_intern(&lf[57],29, C_text("chicken.file.posix#open/trunc"));
lf[58]=C_h_intern(&lf[58],29, C_text("chicken.file.posix#open/write"));
lf[59]=C_h_intern(&lf[59],30, C_text("chicken.file.posix#open/wronly"));
lf[60]=C_h_intern(&lf[60],29, C_text("chicken.file.posix#perm/irgrp"));
lf[61]=C_h_intern(&lf[61],29, C_text("chicken.file.posix#perm/iroth"));
lf[62]=C_h_intern(&lf[62],29, C_text("chicken.file.posix#perm/irusr"));
lf[63]=C_h_intern(&lf[63],29, C_text("chicken.file.posix#perm/irwxg"));
lf[64]=C_h_intern(&lf[64],29, C_text("chicken.file.posix#perm/irwxo"));
lf[65]=C_h_intern(&lf[65],29, C_text("chicken.file.posix#perm/irwxu"));
lf[66]=C_h_intern(&lf[66],29, C_text("chicken.file.posix#perm/isgid"));
lf[67]=C_h_intern(&lf[67],29, C_text("chicken.file.posix#perm/isuid"));
lf[68]=C_h_intern(&lf[68],29, C_text("chicken.file.posix#perm/isvtx"));
lf[69]=C_h_intern(&lf[69],29, C_text("chicken.file.posix#perm/iwgrp"));
lf[70]=C_h_intern(&lf[70],29, C_text("chicken.file.posix#perm/iwoth"));
lf[71]=C_h_intern(&lf[71],29, C_text("chicken.file.posix#perm/iwusr"));
lf[72]=C_h_intern(&lf[72],29, C_text("chicken.file.posix#perm/ixgrp"));
lf[73]=C_h_intern(&lf[73],29, C_text("chicken.file.posix#perm/ixoth"));
lf[74]=C_h_intern(&lf[74],29, C_text("chicken.file.posix#perm/ixusr"));
lf[75]=C_h_intern(&lf[75],31, C_text("chicken.file.posix#port->fileno"));
lf[76]=C_h_intern(&lf[76],27, C_text("chicken.file.posix#seek/cur"));
lf[77]=C_h_intern(&lf[77],27, C_text("chicken.file.posix#seek/end"));
lf[78]=C_h_intern(&lf[78],27, C_text("chicken.file.posix#seek/set"));
lf[79]=C_h_intern(&lf[79],37, C_text("chicken.file.posix#set-file-position!"));
lf[80]=C_h_intern(&lf[80],19, C_text("chicken.time.posix#"));
lf[81]=C_h_intern(&lf[81],36, C_text("chicken.time.posix#seconds->utc-time"));
lf[82]=C_h_intern(&lf[82],36, C_text("chicken.time.posix#utc-time->seconds"));
lf[83]=C_h_intern(&lf[83],38, C_text("chicken.time.posix#seconds->local-time"));
lf[84]=C_h_intern(&lf[84],34, C_text("chicken.time.posix#seconds->string"));
lf[85]=C_h_intern(&lf[85],38, C_text("chicken.time.posix#local-time->seconds"));
lf[86]=C_h_intern(&lf[86],31, C_text("chicken.time.posix#string->time"));
lf[87]=C_h_intern(&lf[87],31, C_text("chicken.time.posix#time->string"));
lf[88]=C_h_intern(&lf[88],46, C_text("chicken.time.posix#local-timezone-abbreviation"));
lf[89]=C_h_intern(&lf[89],16, C_text("chicken.process#"));
lf[90]=C_h_intern(&lf[90],22, C_text("chicken.process#system"));
lf[91]=C_h_intern(&lf[91],6, C_text("system"));
lf[92]=C_h_intern(&lf[92],17, C_text("##sys#signal-hook"));
lf[93]=C_h_intern_kw(&lf[93],13, C_text("process-error"));
lf[94]=C_decode_literal(C_heaptop,C_text("\376B\000\000\032`system\047 invocation failed"));
lf[95]=C_h_intern(&lf[95],18, C_text("##sys#update-errno"));
lf[96]=C_h_intern(&lf[96],23, C_text("chicken.process#system\052"));
lf[97]=C_h_intern(&lf[97],11, C_text("##sys#error"));
lf[98]=C_decode_literal(C_heaptop,C_text("\376B\000\0003shell invocation failed with non-zero return status"));
lf[99]=C_h_intern(&lf[99],18, C_text("chicken.process#qs"));
lf[100]=C_h_intern(&lf[100],7, C_text("mingw32"));
lf[101]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002\042\042"));
lf[102]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004\047\134\047\047"));
lf[103]=C_h_intern(&lf[103],18, C_text("chicken.base#error"));
lf[104]=C_h_intern(&lf[104],2, C_text("qs"));
lf[105]=C_decode_literal(C_heaptop,C_text("\376B\000\0004NUL character can not be represented in shell string"));
lf[106]=C_h_intern(&lf[106],20, C_text("scheme#string-append"));
lf[107]=C_h_intern(&lf[107],18, C_text("##sys#string->list"));
lf[108]=C_h_intern(&lf[108],33, C_text("chicken.platform#software-version"));
lf[109]=C_h_intern(&lf[109],31, C_text("chicken.process#process-execute"));
lf[110]=C_h_intern(&lf[110],28, C_text("chicken.process#process-fork"));
lf[111]=C_h_intern(&lf[111],27, C_text("chicken.process#process-run"));
lf[112]=C_h_intern(&lf[112],30, C_text("chicken.process#process-signal"));
lf[113]=C_h_intern(&lf[113],29, C_text("chicken.process#process-spawn"));
lf[114]=C_h_intern(&lf[114],28, C_text("chicken.process#process-wait"));
lf[115]=C_h_intern(&lf[115],36, C_text("chicken.process#call-with-input-pipe"));
lf[116]=C_h_intern(&lf[116],37, C_text("chicken.process#call-with-output-pipe"));
lf[117]=C_h_intern(&lf[117],32, C_text("chicken.process#close-input-pipe"));
lf[118]=C_h_intern(&lf[118],33, C_text("chicken.process#close-output-pipe"));
lf[119]=C_h_intern(&lf[119],27, C_text("chicken.process#create-pipe"));
lf[120]=C_h_intern(&lf[120],31, C_text("chicken.process#open-input-pipe"));
lf[121]=C_h_intern(&lf[121],32, C_text("chicken.process#open-output-pipe"));
lf[122]=C_h_intern(&lf[122],36, C_text("chicken.process#with-input-from-pipe"));
lf[123]=C_h_intern(&lf[123],35, C_text("chicken.process#with-output-to-pipe"));
lf[124]=C_h_intern(&lf[124],23, C_text("chicken.process#process"));
lf[125]=C_h_intern(&lf[125],24, C_text("chicken.process#process\052"));
lf[126]=C_h_intern(&lf[126],29, C_text("chicken.process#process-sleep"));
lf[127]=C_h_intern(&lf[127],24, C_text("chicken.process#pipe/buf"));
lf[128]=C_h_intern(&lf[128],29, C_text("chicken.process#spawn/overlay"));
lf[129]=C_h_intern(&lf[129],26, C_text("chicken.process#spawn/wait"));
lf[130]=C_h_intern(&lf[130],28, C_text("chicken.process#spawn/nowait"));
lf[131]=C_h_intern(&lf[131],29, C_text("chicken.process#spawn/nowaito"));
lf[132]=C_h_intern(&lf[132],28, C_text("chicken.process#spawn/detach"));
lf[133]=C_h_intern(&lf[133],23, C_text("chicken.process.signal#"));
lf[134]=C_h_intern(&lf[134],33, C_text("chicken.process.signal#set-alarm!"));
lf[135]=C_h_intern(&lf[135],42, C_text("chicken.process.signal#set-signal-handler!"));
lf[136]=C_h_intern(&lf[136],39, C_text("chicken.process.signal#set-signal-mask!"));
lf[137]=C_h_intern(&lf[137],37, C_text("chicken.process.signal#signal-handler"));
lf[138]=C_h_intern(&lf[138],34, C_text("chicken.process.signal#signal-mask"));
lf[139]=C_h_intern(&lf[139],35, C_text("chicken.process.signal#signal-mask!"));
lf[140]=C_h_intern(&lf[140],37, C_text("chicken.process.signal#signal-masked\077"));
lf[141]=C_h_intern(&lf[141],37, C_text("chicken.process.signal#signal-unmask!"));
lf[142]=C_h_intern(&lf[142],34, C_text("chicken.process.signal#signal/abrt"));
lf[143]=C_h_intern(&lf[143],34, C_text("chicken.process.signal#signal/alrm"));
lf[144]=C_h_intern(&lf[144],35, C_text("chicken.process.signal#signal/break"));
lf[145]=C_h_intern(&lf[145],33, C_text("chicken.process.signal#signal/bus"));
lf[146]=C_h_intern(&lf[146],34, C_text("chicken.process.signal#signal/chld"));
lf[147]=C_h_intern(&lf[147],34, C_text("chicken.process.signal#signal/cont"));
lf[148]=C_h_intern(&lf[148],33, C_text("chicken.process.signal#signal/fpe"));
lf[149]=C_h_intern(&lf[149],33, C_text("chicken.process.signal#signal/hup"));
lf[150]=C_h_intern(&lf[150],33, C_text("chicken.process.signal#signal/ill"));
lf[151]=C_h_intern(&lf[151],33, C_text("chicken.process.signal#signal/int"));
lf[152]=C_h_intern(&lf[152],32, C_text("chicken.process.signal#signal/io"));
lf[153]=C_h_intern(&lf[153],34, C_text("chicken.process.signal#signal/kill"));
lf[154]=C_h_intern(&lf[154],34, C_text("chicken.process.signal#signal/pipe"));
lf[155]=C_h_intern(&lf[155],34, C_text("chicken.process.signal#signal/prof"));
lf[156]=C_h_intern(&lf[156],34, C_text("chicken.process.signal#signal/quit"));
lf[157]=C_h_intern(&lf[157],34, C_text("chicken.process.signal#signal/segv"));
lf[158]=C_h_intern(&lf[158],34, C_text("chicken.process.signal#signal/stop"));
lf[159]=C_h_intern(&lf[159],34, C_text("chicken.process.signal#signal/term"));
lf[160]=C_h_intern(&lf[160],34, C_text("chicken.process.signal#signal/trap"));
lf[161]=C_h_intern(&lf[161],34, C_text("chicken.process.signal#signal/tstp"));
lf[162]=C_h_intern(&lf[162],33, C_text("chicken.process.signal#signal/urg"));
lf[163]=C_h_intern(&lf[163],34, C_text("chicken.process.signal#signal/usr1"));
lf[164]=C_h_intern(&lf[164],34, C_text("chicken.process.signal#signal/usr2"));
lf[165]=C_h_intern(&lf[165],36, C_text("chicken.process.signal#signal/vtalrm"));
lf[166]=C_h_intern(&lf[166],35, C_text("chicken.process.signal#signal/winch"));
lf[167]=C_h_intern(&lf[167],34, C_text("chicken.process.signal#signal/xcpu"));
lf[168]=C_h_intern(&lf[168],34, C_text("chicken.process.signal#signal/xfsz"));
lf[169]=C_h_intern(&lf[169],35, C_text("chicken.process.signal#signals-list"));
lf[170]=C_h_intern(&lf[170],30, C_text("chicken.process-context.posix#"));
lf[171]=C_h_intern(&lf[171],47, C_text("chicken.process-context.posix#change-directory\052"));
lf[172]=C_h_intern(&lf[172],49, C_text("chicken.process-context.posix#set-root-directory!"));
lf[173]=C_h_intern(&lf[173],56, C_text("chicken.process-context.posix#current-effective-group-id"));
lf[174]=C_h_intern(&lf[174],55, C_text("chicken.process-context.posix#current-effective-user-id"));
lf[175]=C_h_intern(&lf[175],46, C_text("chicken.process-context.posix#current-group-id"));
lf[176]=C_h_intern(&lf[176],45, C_text("chicken.process-context.posix#current-user-id"));
lf[177]=C_h_intern(&lf[177],48, C_text("chicken.process-context.posix#current-process-id"));
lf[178]=C_h_intern(&lf[178],47, C_text("chicken.process-context.posix#parent-process-id"));
lf[179]=C_h_intern(&lf[179],47, C_text("chicken.process-context.posix#current-user-name"));
lf[180]=C_h_intern(&lf[180],57, C_text("chicken.process-context.posix#current-effective-user-name"));
lf[181]=C_h_intern(&lf[181],44, C_text("chicken.process-context.posix#create-session"));
lf[182]=C_h_intern(&lf[182],46, C_text("chicken.process-context.posix#process-group-id"));
lf[183]=C_h_intern(&lf[183],46, C_text("chicken.process-context.posix#user-information"));
lf[184]=C_h_intern(&lf[184],14, C_text("chicken.posix#"));
lf[186]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003 - "));
lf[187]=C_h_intern(&lf[187],19, C_text("##sys#peek-c-string"));
lf[188]=C_h_intern(&lf[188],17, C_text("##sys#posix-error"));
lf[190]=C_h_intern_kw(&lf[190],10, C_text("file-error"));
lf[191]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022cannot access file"));
lf[192]=C_h_intern(&lf[192],19, C_text("##sys#make-c-string"));
lf[193]=C_h_intern_kw(&lf[193],10, C_text("type-error"));
lf[194]=C_decode_literal(C_heaptop,C_text("\376B\000\0000bad argument type - not a fixnum, port or string"));
lf[195]=C_h_intern(&lf[195],18, C_text("chicken.base#port\077"));
lf[196]=C_h_intern(&lf[196],28, C_text("chicken.file.posix#file-stat"));
lf[197]=C_h_intern(&lf[197],9, C_text("file-stat"));
lf[198]=C_h_intern(&lf[198],40, C_text("chicken.file.posix#set-file-permissions!"));
lf[199]=C_h_intern(&lf[199],21, C_text("set-file-permissions!"));
lf[200]=C_decode_literal(C_heaptop,C_text("\376B\000\000\036cannot change file permissions"));
lf[201]=C_h_intern(&lf[201],16, C_text("file-permissions"));
lf[202]=C_decode_literal(C_heaptop,C_text("\376B\000\0000bad argument type - not a fixnum, port or string"));
lf[203]=C_h_intern(&lf[203],18, C_text("##sys#check-fixnum"));
lf[204]=C_h_intern(&lf[204],41, C_text("chicken.file.posix#file-modification-time"));
lf[205]=C_h_intern(&lf[205],22, C_text("file-modification-time"));
lf[206]=C_h_intern(&lf[206],35, C_text("chicken.file.posix#file-access-time"));
lf[207]=C_h_intern(&lf[207],16, C_text("file-access-time"));
lf[208]=C_h_intern(&lf[208],35, C_text("chicken.file.posix#file-change-time"));
lf[209]=C_h_intern(&lf[209],16, C_text("file-change-time"));
lf[210]=C_h_intern(&lf[210],34, C_text("chicken.file.posix#set-file-times!"));
lf[211]=C_h_intern(&lf[211],15, C_text("set-file-times!"));
lf[212]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025cannot set file times"));
lf[213]=C_h_intern(&lf[213],25, C_text("##sys#check-exact-integer"));
lf[214]=C_h_intern(&lf[214],28, C_text("chicken.time#current-seconds"));
lf[215]=C_h_intern(&lf[215],28, C_text("chicken.file.posix#file-size"));
lf[216]=C_h_intern(&lf[216],9, C_text("file-size"));
lf[217]=C_h_intern(&lf[217],34, C_text("chicken.file.posix#set-file-owner!"));
lf[218]=C_h_intern(&lf[218],15, C_text("set-file-owner!"));
lf[219]=C_h_intern(&lf[219],5, C_text("chown"));
lf[220]=C_h_intern(&lf[220],34, C_text("chicken.file.posix#set-file-group!"));
lf[221]=C_h_intern(&lf[221],15, C_text("set-file-group!"));
lf[222]=C_h_intern(&lf[222],12, C_text("regular-file"));
lf[223]=C_h_intern(&lf[223],13, C_text("symbolic-link"));
lf[224]=C_h_intern(&lf[224],9, C_text("directory"));
lf[225]=C_h_intern(&lf[225],16, C_text("character-device"));
lf[226]=C_h_intern(&lf[226],12, C_text("block-device"));
lf[227]=C_h_intern(&lf[227],4, C_text("fifo"));
lf[228]=C_h_intern(&lf[228],6, C_text("socket"));
lf[229]=C_h_intern(&lf[229],9, C_text("file-type"));
lf[230]=C_h_intern(&lf[230],18, C_text("set-file-position!"));
lf[231]=C_decode_literal(C_heaptop,C_text("\376B\000\000\030cannot set file position"));
lf[232]=C_h_intern(&lf[232],6, C_text("stream"));
lf[233]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014invalid file"));
lf[234]=C_h_intern_kw(&lf[234],6, C_text("append"));
lf[235]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001a"));
lf[236]=C_decode_literal(C_heaptop,C_text("\376B\000\000\033invalid mode for input file"));
lf[237]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025invalid mode argument"));
lf[238]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001r"));
lf[239]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001w"));
lf[240]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020cannot open file"));
lf[241]=C_h_intern(&lf[241],15, C_text("##sys#make-port"));
lf[242]=C_h_intern(&lf[242],23, C_text("##sys#stream-port-class"));
lf[243]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010(fdport)"));
lf[244]=C_h_intern(&lf[244],16, C_text("open-input-file\052"));
lf[245]=C_h_intern(&lf[245],17, C_text("open-output-file\052"));
lf[246]=C_h_intern(&lf[246],12, C_text("port->fileno"));
lf[247]=C_h_intern(&lf[247],15, C_text("##sys#port-data"));
lf[248]=C_decode_literal(C_heaptop,C_text("\376B\000\000%cannot access file-descriptor of port"));
lf[249]=C_decode_literal(C_heaptop,C_text("\376B\000\000\031port has no attached file"));
lf[250]=C_h_intern(&lf[250],27, C_text("##sys#peek-unsigned-integer"));
lf[251]=C_h_intern(&lf[251],16, C_text("duplicate-fileno"));
lf[252]=C_decode_literal(C_heaptop,C_text("\376B\000\000 cannot duplicate file-descriptor"));
lf[253]=C_h_intern(&lf[253],17, C_text("change-directory\052"));
lf[254]=C_decode_literal(C_heaptop,C_text("\376B\000\000\037cannot change current directory"));
lf[255]=C_h_intern(&lf[255],27, C_text("##sys#change-directory-hook"));
lf[258]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025time vector too short"));
lf[259]=C_h_intern(&lf[259],19, C_text("seconds->local-time"));
lf[260]=C_h_intern(&lf[260],17, C_text("seconds->utc-time"));
lf[261]=C_h_intern(&lf[261],15, C_text("##sys#substring"));
lf[262]=C_h_intern(&lf[262],15, C_text("seconds->string"));
lf[263]=C_decode_literal(C_heaptop,C_text("\376B\000\000 cannot convert seconds to string"));
lf[264]=C_h_intern(&lf[264],19, C_text("local-time->seconds"));
lf[265]=C_decode_literal(C_heaptop,C_text("\376B\000\000%cannot convert time vector to seconds"));
lf[266]=C_h_intern(&lf[266],17, C_text("##sys#make-string"));
lf[267]=C_h_intern(&lf[267],12, C_text("time->string"));
lf[268]=C_decode_literal(C_heaptop,C_text("\376B\000\000 time formatting overflows buffer"));
lf[269]=C_decode_literal(C_heaptop,C_text("\376B\000\000$cannot convert time vector to string"));
lf[270]=C_h_intern(&lf[270],19, C_text("##sys#signal-vector"));
lf[271]=C_h_intern(&lf[271],19, C_text("set-signal-handler!"));
lf[272]=C_h_intern(&lf[272],13, C_text("process-sleep"));
lf[273]=C_h_intern(&lf[273],12, C_text("process-wait"));
lf[274]=C_decode_literal(C_heaptop,C_text("\376B\000\000 waiting for child process failed"));
lf[276]=C_h_intern(&lf[276],24, C_text("chicken.condition#signal"));
lf[278]=C_h_intern(&lf[278],34, C_text("chicken.memory#pointer-vector-set!"));
lf[279]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015Out of memory"));
lf[280]=C_h_intern(&lf[280],40, C_text("chicken.condition#with-exception-handler"));
lf[281]=C_h_intern(&lf[281],37, C_text("scheme#call-with-current-continuation"));
lf[282]=C_h_intern(&lf[282],34, C_text("chicken.memory#make-pointer-vector"));
lf[283]=C_h_intern(&lf[283],19, C_text("chicken.memory#free"));
lf[284]=C_h_intern(&lf[284],33, C_text("chicken.memory#pointer-vector-ref"));
lf[285]=C_h_intern(&lf[285],36, C_text("chicken.memory#pointer-vector-length"));
lf[287]=C_h_intern(&lf[287],41, C_text("chicken.pathname#pathname-strip-directory"));
lf[289]=C_h_intern(&lf[289],3, C_text("map"));
lf[290]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001="));
lf[291]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020cannot open pipe"));
lf[292]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006(pipe)"));
lf[293]=C_h_intern(&lf[293],15, C_text("open-input-pipe"));
lf[294]=C_h_intern_kw(&lf[294],4, C_text("text"));
lf[295]=C_h_intern_kw(&lf[295],6, C_text("binary"));
lf[296]=C_decode_literal(C_heaptop,C_text("\376B\000\000#illegal input/output mode specifier"));
lf[297]=C_h_intern(&lf[297],16, C_text("open-output-pipe"));
lf[298]=C_h_intern(&lf[298],16, C_text("close-input-pipe"));
lf[299]=C_decode_literal(C_heaptop,C_text("\376B\000\000\030error while closing pipe"));
lf[300]=C_h_intern(&lf[300],17, C_text("close-output-pipe"));
lf[301]=C_decode_literal(C_heaptop,C_text("\376B\000\000\030error while closing pipe"));
lf[302]=C_h_intern(&lf[302],20, C_text("##sys#standard-input"));
lf[303]=C_h_intern(&lf[303],13, C_text("scheme#values"));
lf[304]=C_h_intern(&lf[304],18, C_text("##sys#dynamic-wind"));
lf[305]=C_h_intern(&lf[305],21, C_text("##sys#standard-output"));
lf[306]=C_h_intern(&lf[306],9, C_text("file-open"));
lf[307]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020cannot open file"));
lf[308]=C_h_intern(&lf[308],24, C_text("##sys#dispatch-interrupt"));
lf[309]=C_h_intern(&lf[309],10, C_text("file-close"));
lf[310]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021cannot close file"));
lf[311]=C_h_intern(&lf[311],9, C_text("file-read"));
lf[312]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025cannot read from file"));
lf[313]=C_decode_literal(C_heaptop,C_text("\376B\000\000(bad argument type - not a string or blob"));
lf[314]=C_h_intern(&lf[314],18, C_text("scheme#make-string"));
lf[315]=C_h_intern(&lf[315],10, C_text("file-write"));
lf[316]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024cannot write to file"));
lf[317]=C_decode_literal(C_heaptop,C_text("\376B\000\000(bad argument type - not a string or blob"));
lf[318]=C_h_intern(&lf[318],12, C_text("file-mkstemp"));
lf[319]=C_decode_literal(C_heaptop,C_text("\376B\000\000$0123456789abcdefghijklmnopqrstuvwxyz"));
lf[320]=C_decode_literal(C_heaptop,C_text("\376B\000\000\034cannot create temporary file"));
lf[321]=C_decode_literal(C_heaptop,C_text("\376B\000\000\026non-existent directory"));
lf[322]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020invalid template"));
lf[323]=C_h_intern(&lf[323],18, C_text("##sys#file-exists\077"));
lf[324]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001."));
lf[325]=C_h_intern(&lf[325],35, C_text("chicken.pathname#pathname-directory"));
lf[326]=C_h_intern(&lf[326],18, C_text("scheme#string-copy"));
lf[327]=C_h_intern(&lf[327],11, C_text("create-pipe"));
lf[328]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022cannot create pipe"));
lf[330]=C_decode_literal(C_heaptop,C_text("\376B\000\000 cannot duplicate file descriptor"));
lf[332]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001\042"));
lf[333]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001\042"));
lf[334]=C_h_intern(&lf[334],15, C_text("process-execute"));
lf[335]=C_decode_literal(C_heaptop,C_text("\376B\000\000\026cannot execute process"));
lf[336]=C_h_intern(&lf[336],13, C_text("process-spawn"));
lf[337]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024cannot spawn process"));
lf[339]=C_decode_literal(C_heaptop,C_text("\376B\000\000 cannot retrieve system directory"));
lf[340]=C_h_intern(&lf[340],48, C_text("chicken.process-context#get-environment-variable"));
lf[341]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007COMSPEC"));
lf[342]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002/c"));
lf[343]=C_h_intern(&lf[343],11, C_text("process-run"));
lf[344]=C_decode_literal(C_heaptop,C_text("\376B\000\000\026cannot execute process"));
lf[345]=C_h_intern(&lf[345],19, C_text("##sys#make-locative"));
lf[346]=C_h_intern(&lf[346],8, C_text("location"));
lf[347]=C_h_intern(&lf[347],33, C_text("chicken.string#string-intersperse"));
lf[348]=C_h_intern(&lf[348],7, C_text("process"));
lf[349]=C_h_intern(&lf[349],8, C_text("process\052"));
lf[350]=C_h_intern(&lf[350],17, C_text("current-user-name"));
lf[351]=C_decode_literal(C_heaptop,C_text("\376B\000\000!cannot retrieve current user-name"));
lf[352]=C_h_intern(&lf[352],14, C_text("chicken.errno#"));
lf[353]=C_h_intern(&lf[353],19, C_text("chicken.errno#errno"));
lf[354]=C_h_intern(&lf[354],11, C_text("##sys#errno"));
lf[355]=C_h_intern(&lf[355],24, C_text("chicken.errno#errno/2big"));
lf[356]=C_h_intern(&lf[356],25, C_text("chicken.errno#errno/acces"));
lf[357]=C_h_intern(&lf[357],25, C_text("chicken.errno#errno/again"));
lf[358]=C_h_intern(&lf[358],24, C_text("chicken.errno#errno/badf"));
lf[359]=C_h_intern(&lf[359],24, C_text("chicken.errno#errno/busy"));
lf[360]=C_h_intern(&lf[360],25, C_text("chicken.errno#errno/child"));
lf[361]=C_h_intern(&lf[361],26, C_text("chicken.errno#errno/deadlk"));
lf[362]=C_h_intern(&lf[362],23, C_text("chicken.errno#errno/dom"));
lf[363]=C_h_intern(&lf[363],25, C_text("chicken.errno#errno/exist"));
lf[364]=C_h_intern(&lf[364],25, C_text("chicken.errno#errno/fault"));
lf[365]=C_h_intern(&lf[365],24, C_text("chicken.errno#errno/fbig"));
lf[366]=C_h_intern(&lf[366],25, C_text("chicken.errno#errno/ilseq"));
lf[367]=C_h_intern(&lf[367],24, C_text("chicken.errno#errno/intr"));
lf[368]=C_h_intern(&lf[368],25, C_text("chicken.errno#errno/inval"));
lf[369]=C_h_intern(&lf[369],22, C_text("chicken.errno#errno/io"));
lf[370]=C_h_intern(&lf[370],25, C_text("chicken.errno#errno/isdir"));
lf[371]=C_h_intern(&lf[371],25, C_text("chicken.errno#errno/mfile"));
lf[372]=C_h_intern(&lf[372],25, C_text("chicken.errno#errno/mlink"));
lf[373]=C_h_intern(&lf[373],31, C_text("chicken.errno#errno/nametoolong"));
lf[374]=C_h_intern(&lf[374],25, C_text("chicken.errno#errno/nfile"));
lf[375]=C_h_intern(&lf[375],25, C_text("chicken.errno#errno/nodev"));
lf[376]=C_h_intern(&lf[376],25, C_text("chicken.errno#errno/noent"));
lf[377]=C_h_intern(&lf[377],26, C_text("chicken.errno#errno/noexec"));
lf[378]=C_h_intern(&lf[378],25, C_text("chicken.errno#errno/nolck"));
lf[379]=C_h_intern(&lf[379],25, C_text("chicken.errno#errno/nomem"));
lf[380]=C_h_intern(&lf[380],25, C_text("chicken.errno#errno/nospc"));
lf[381]=C_h_intern(&lf[381],25, C_text("chicken.errno#errno/nosys"));
lf[382]=C_h_intern(&lf[382],26, C_text("chicken.errno#errno/notdir"));
lf[383]=C_h_intern(&lf[383],28, C_text("chicken.errno#errno/notempty"));
lf[384]=C_h_intern(&lf[384],25, C_text("chicken.errno#errno/notty"));
lf[385]=C_h_intern(&lf[385],24, C_text("chicken.errno#errno/nxio"));
lf[386]=C_h_intern(&lf[386],24, C_text("chicken.errno#errno/perm"));
lf[387]=C_h_intern(&lf[387],24, C_text("chicken.errno#errno/pipe"));
lf[388]=C_h_intern(&lf[388],25, C_text("chicken.errno#errno/range"));
lf[389]=C_h_intern(&lf[389],24, C_text("chicken.errno#errno/rofs"));
lf[390]=C_h_intern(&lf[390],25, C_text("chicken.errno#errno/spipe"));
lf[391]=C_h_intern(&lf[391],24, C_text("chicken.errno#errno/srch"));
lf[392]=C_h_intern(&lf[392],30, C_text("chicken.errno#errno/wouldblock"));
lf[393]=C_h_intern(&lf[393],24, C_text("chicken.errno#errno/xdev"));
lf[394]=C_h_intern(&lf[394],14, C_text("signal-handler"));
lf[395]=C_h_intern(&lf[395],31, C_text("chicken.base#getter-with-setter"));
lf[396]=C_decode_literal(C_heaptop,C_text("\376B\000\000+(chicken.process.signal#signal-handler sig)"));
lf[397]=C_h_intern(&lf[397],18, C_text("file-creation-mode"));
lf[398]=C_decode_literal(C_heaptop,C_text("\376B\000\000,(chicken.file.posix#file-creation-mode mode)"));
lf[399]=C_h_intern(&lf[399],13, C_text("file-position"));
lf[400]=C_decode_literal(C_heaptop,C_text("\376B\000\000%cannot retrieve file position of port"));
lf[401]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014invalid file"));
lf[402]=C_decode_literal(C_heaptop,C_text("\376B\000\000\047(chicken.file.posix#file-position port)"));
lf[403]=C_decode_literal(C_heaptop,C_text("\376B\000\000\047(chicken.file.posix#file-permissions f)"));
lf[404]=C_h_intern(&lf[404],10, C_text("file-group"));
lf[405]=C_decode_literal(C_heaptop,C_text("\376B\000\000!(chicken.file.posix#file-group f)"));
lf[406]=C_h_intern(&lf[406],10, C_text("file-owner"));
lf[407]=C_decode_literal(C_heaptop,C_text("\376B\000\000!(chicken.file.posix#file-owner f)"));
C_register_lf2(lf,408,create_ptable());{}
t2=C_mutate(&lf[0] /* (set! c1660 ...) */,lf[1]);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2227,a[2]=t1,tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t3;
C_scheduler_toplevel(2,av2);}}

/* va6083 in k4341 in loop in k4328 in k4319 */
static void C_fcall va6083(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,5)))){
C_save_and_reclaim_args((void *)trva6083,2,t0,t1);}
a=C_alloc(5);
t2=C_s_a_i_bitwise_ior(&a,2,t1,*((C_word*)lf[47]+1));
t3=C_fixnum_or(C_fix((C_word)S_IRUSR),C_fix((C_word)S_IWUSR));
t4=C_open(((C_word*)t0)[2],t2,t3);
t5=C_eqp(C_fix(-1),t4);
if(C_truep(t5)){
if(C_truep(C_fixnum_lessp(((C_word*)t0)[3],((C_word*)t0)[4]))){
/* posixwin.scm:602: loop */
t6=((C_word*)((C_word*)t0)[5])[1];
f_4335(t6,((C_word*)t0)[6],C_fixnum_plus(((C_word*)t0)[3],C_fix(1)));}
else{
/* posixwin.scm:603: posix-error */
t6=lf[185];{
C_word av2[6];
av2[0]=t6;
av2[1]=((C_word*)t0)[6];
av2[2]=lf[190];
av2[3]=lf[318];
av2[4]=lf[320];
av2[5]=((C_word*)t0)[7];
f_2552(6,av2);}}}
else{
/* posixwin.scm:604: scheme#values */{
C_word av2[4];
av2[0]=0;
av2[1]=((C_word*)t0)[6];
av2[2]=t4;
av2[3]=((C_word*)t0)[8];
C_values(4,av2);}}}

/* va6091 in chicken.file.posix#file-mkstemp in k3470 in k3235 in k3018 in k2801 in k2797 in k2793 in k2243 in k2240 in k2237 in k2234 in k2231 in k2228 in k2225 */
static void C_fcall va6091(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(40,0,2)))){
C_save_and_reclaim_args((void *)trva6091,2,t0,t1);}
a=C_alloc(40);
t2=C_s_a_i_times(&a,2,t1,((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4321,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],a[4]=t2,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* posixwin.scm:574: scheme#string-copy */
t4=*((C_word*)lf[326]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

#ifdef C_ENABLE_PTABLES
static C_PTABLE_ENTRY ptable[354] = {
{C_text("f6102:posix_2escm"),(void*)f6102},
{C_text("f6106:posix_2escm"),(void*)f6106},
{C_text("f_2227:posix_2escm"),(void*)f_2227},
{C_text("f_2230:posix_2escm"),(void*)f_2230},
{C_text("f_2233:posix_2escm"),(void*)f_2233},
{C_text("f_2236:posix_2escm"),(void*)f_2236},
{C_text("f_2239:posix_2escm"),(void*)f_2239},
{C_text("f_2242:posix_2escm"),(void*)f_2242},
{C_text("f_2245:posix_2escm"),(void*)f_2245},
{C_text("f_2331:posix_2escm"),(void*)f_2331},
{C_text("f_2344:posix_2escm"),(void*)f_2344},
{C_text("f_2349:posix_2escm"),(void*)f_2349},
{C_text("f_2353:posix_2escm"),(void*)f_2353},
{C_text("f_2365:posix_2escm"),(void*)f_2365},
{C_text("f_2369:posix_2escm"),(void*)f_2369},
{C_text("f_2379:posix_2escm"),(void*)f_2379},
{C_text("f_2400:posix_2escm"),(void*)f_2400},
{C_text("f_2403:posix_2escm"),(void*)f_2403},
{C_text("f_2414:posix_2escm"),(void*)f_2414},
{C_text("f_2420:posix_2escm"),(void*)f_2420},
{C_text("f_2445:posix_2escm"),(void*)f_2445},
{C_text("f_2552:posix_2escm"),(void*)f_2552},
{C_text("f_2556:posix_2escm"),(void*)f_2556},
{C_text("f_2563:posix_2escm"),(void*)f_2563},
{C_text("f_2567:posix_2escm"),(void*)f_2567},
{C_text("f_2570:posix_2escm"),(void*)f_2570},
{C_text("f_2574:posix_2escm"),(void*)f_2574},
{C_text("f_2595:posix_2escm"),(void*)f_2595},
{C_text("f_2599:posix_2escm"),(void*)f_2599},
{C_text("f_2608:posix_2escm"),(void*)f_2608},
{C_text("f_2616:posix_2escm"),(void*)f_2616},
{C_text("f_2623:posix_2escm"),(void*)f_2623},
{C_text("f_2634:posix_2escm"),(void*)f_2634},
{C_text("f_2638:posix_2escm"),(void*)f_2638},
{C_text("f_2641:posix_2escm"),(void*)f_2641},
{C_text("f_2659:posix_2escm"),(void*)f_2659},
{C_text("f_2663:posix_2escm"),(void*)f_2663},
{C_text("f_2673:posix_2escm"),(void*)f_2673},
{C_text("f_2678:posix_2escm"),(void*)f_2678},
{C_text("f_2682:posix_2escm"),(void*)f_2682},
{C_text("f_2684:posix_2escm"),(void*)f_2684},
{C_text("f_2688:posix_2escm"),(void*)f_2688},
{C_text("f_2690:posix_2escm"),(void*)f_2690},
{C_text("f_2694:posix_2escm"),(void*)f_2694},
{C_text("f_2696:posix_2escm"),(void*)f_2696},
{C_text("f_2700:posix_2escm"),(void*)f_2700},
{C_text("f_2712:posix_2escm"),(void*)f_2712},
{C_text("f_2715:posix_2escm"),(void*)f_2715},
{C_text("f_2721:posix_2escm"),(void*)f_2721},
{C_text("f_2731:posix_2escm"),(void*)f_2731},
{C_text("f_2775:posix_2escm"),(void*)f_2775},
{C_text("f_2779:posix_2escm"),(void*)f_2779},
{C_text("f_2781:posix_2escm"),(void*)f_2781},
{C_text("f_2787:posix_2escm"),(void*)f_2787},
{C_text("f_2795:posix_2escm"),(void*)f_2795},
{C_text("f_2799:posix_2escm"),(void*)f_2799},
{C_text("f_2803:posix_2escm"),(void*)f_2803},
{C_text("f_2805:posix_2escm"),(void*)f_2805},
{C_text("f_2824:posix_2escm"),(void*)f_2824},
{C_text("f_2892:posix_2escm"),(void*)f_2892},
{C_text("f_2900:posix_2escm"),(void*)f_2900},
{C_text("f_2902:posix_2escm"),(void*)f_2902},
{C_text("f_2910:posix_2escm"),(void*)f_2910},
{C_text("f_2912:posix_2escm"),(void*)f_2912},
{C_text("f_2920:posix_2escm"),(void*)f_2920},
{C_text("f_2922:posix_2escm"),(void*)f_2922},
{C_text("f_2930:posix_2escm"),(void*)f_2930},
{C_text("f_2932:posix_2escm"),(void*)f_2932},
{C_text("f_2940:posix_2escm"),(void*)f_2940},
{C_text("f_2942:posix_2escm"),(void*)f_2942},
{C_text("f_2950:posix_2escm"),(void*)f_2950},
{C_text("f_2952:posix_2escm"),(void*)f_2952},
{C_text("f_2960:posix_2escm"),(void*)f_2960},
{C_text("f_2965:posix_2escm"),(void*)f_2965},
{C_text("f_2972:posix_2escm"),(void*)f_2972},
{C_text("f_2975:posix_2escm"),(void*)f_2975},
{C_text("f_2981:posix_2escm"),(void*)f_2981},
{C_text("f_2987:posix_2escm"),(void*)f_2987},
{C_text("f_3020:posix_2escm"),(void*)f_3020},
{C_text("f_3048:posix_2escm"),(void*)f_3048},
{C_text("f_3056:posix_2escm"),(void*)f_3056},
{C_text("f_3085:posix_2escm"),(void*)f_3085},
{C_text("f_3098:posix_2escm"),(void*)f_3098},
{C_text("f_3104:posix_2escm"),(void*)f_3104},
{C_text("f_3108:posix_2escm"),(void*)f_3108},
{C_text("f_3116:posix_2escm"),(void*)f_3116},
{C_text("f_3118:posix_2escm"),(void*)f_3118},
{C_text("f_3122:posix_2escm"),(void*)f_3122},
{C_text("f_3130:posix_2escm"),(void*)f_3130},
{C_text("f_3132:posix_2escm"),(void*)f_3132},
{C_text("f_3148:posix_2escm"),(void*)f_3148},
{C_text("f_3157:posix_2escm"),(void*)f_3157},
{C_text("f_3171:posix_2escm"),(void*)f_3171},
{C_text("f_3177:posix_2escm"),(void*)f_3177},
{C_text("f_3181:posix_2escm"),(void*)f_3181},
{C_text("f_3184:posix_2escm"),(void*)f_3184},
{C_text("f_3187:posix_2escm"),(void*)f_3187},
{C_text("f_3202:posix_2escm"),(void*)f_3202},
{C_text("f_3204:posix_2escm"),(void*)f_3204},
{C_text("f_3207:posix_2escm"),(void*)f_3207},
{C_text("f_3211:posix_2escm"),(void*)f_3211},
{C_text("f_3214:posix_2escm"),(void*)f_3214},
{C_text("f_3223:posix_2escm"),(void*)f_3223},
{C_text("f_3237:posix_2escm"),(void*)f_3237},
{C_text("f_3240:posix_2escm"),(void*)f_3240},
{C_text("f_3259:posix_2escm"),(void*)f_3259},
{C_text("f_3263:posix_2escm"),(void*)f_3263},
{C_text("f_3266:posix_2escm"),(void*)f_3266},
{C_text("f_3280:posix_2escm"),(void*)f_3280},
{C_text("f_3284:posix_2escm"),(void*)f_3284},
{C_text("f_3287:posix_2escm"),(void*)f_3287},
{C_text("f_3312:posix_2escm"),(void*)f_3312},
{C_text("f_3316:posix_2escm"),(void*)f_3316},
{C_text("f_3319:posix_2escm"),(void*)f_3319},
{C_text("f_3322:posix_2escm"),(void*)f_3322},
{C_text("f_3350:posix_2escm"),(void*)f_3350},
{C_text("f_3354:posix_2escm"),(void*)f_3354},
{C_text("f_3358:posix_2escm"),(void*)f_3358},
{C_text("f_3395:posix_2escm"),(void*)f_3395},
{C_text("f_3402:posix_2escm"),(void*)f_3402},
{C_text("f_3411:posix_2escm"),(void*)f_3411},
{C_text("f_3421:posix_2escm"),(void*)f_3421},
{C_text("f_3425:posix_2escm"),(void*)f_3425},
{C_text("f_3428:posix_2escm"),(void*)f_3428},
{C_text("f_3449:posix_2escm"),(void*)f_3449},
{C_text("f_3457:posix_2escm"),(void*)f_3457},
{C_text("f_3461:posix_2escm"),(void*)f_3461},
{C_text("f_3472:posix_2escm"),(void*)f_3472},
{C_text("f_3474:posix_2escm"),(void*)f_3474},
{C_text("f_3478:posix_2escm"),(void*)f_3478},
{C_text("f_3480:posix_2escm"),(void*)f_3480},
{C_text("f_3499:posix_2escm"),(void*)f_3499},
{C_text("f_3504:posix_2escm"),(void*)f_3504},
{C_text("f_3510:posix_2escm"),(void*)f_3510},
{C_text("f_3551:posix_2escm"),(void*)f_3551},
{C_text("f_3559:posix_2escm"),(void*)f_3559},
{C_text("f_3562:posix_2escm"),(void*)f_3562},
{C_text("f_3567:posix_2escm"),(void*)f_3567},
{C_text("f_3573:posix_2escm"),(void*)f_3573},
{C_text("f_3579:posix_2escm"),(void*)f_3579},
{C_text("f_3583:posix_2escm"),(void*)f_3583},
{C_text("f_3588:posix_2escm"),(void*)f_3588},
{C_text("f_3590:posix_2escm"),(void*)f_3590},
{C_text("f_3594:posix_2escm"),(void*)f_3594},
{C_text("f_3596:posix_2escm"),(void*)f_3596},
{C_text("f_3612:posix_2escm"),(void*)f_3612},
{C_text("f_3618:posix_2escm"),(void*)f_3618},
{C_text("f_3621:posix_2escm"),(void*)f_3621},
{C_text("f_3637:posix_2escm"),(void*)f_3637},
{C_text("f_3647:posix_2escm"),(void*)f_3647},
{C_text("f_3653:posix_2escm"),(void*)f_3653},
{C_text("f_3664:posix_2escm"),(void*)f_3664},
{C_text("f_3668:posix_2escm"),(void*)f_3668},
{C_text("f_3672:posix_2escm"),(void*)f_3672},
{C_text("f_3677:posix_2escm"),(void*)f_3677},
{C_text("f_3687:posix_2escm"),(void*)f_3687},
{C_text("f_3690:posix_2escm"),(void*)f_3690},
{C_text("f_3702:posix_2escm"),(void*)f_3702},
{C_text("f_3707:posix_2escm"),(void*)f_3707},
{C_text("f_3726:posix_2escm"),(void*)f_3726},
{C_text("f_3749:posix_2escm"),(void*)f_3749},
{C_text("f_3751:posix_2escm"),(void*)f_3751},
{C_text("f_3755:posix_2escm"),(void*)f_3755},
{C_text("f_3761:posix_2escm"),(void*)f_3761},
{C_text("f_3764:posix_2escm"),(void*)f_3764},
{C_text("f_3769:posix_2escm"),(void*)f_3769},
{C_text("f_3775:posix_2escm"),(void*)f_3775},
{C_text("f_3781:posix_2escm"),(void*)f_3781},
{C_text("f_3785:posix_2escm"),(void*)f_3785},
{C_text("f_3788:posix_2escm"),(void*)f_3788},
{C_text("f_3796:posix_2escm"),(void*)f_3796},
{C_text("f_3802:posix_2escm"),(void*)f_3802},
{C_text("f_3806:posix_2escm"),(void*)f_3806},
{C_text("f_3813:posix_2escm"),(void*)f_3813},
{C_text("f_3816:posix_2escm"),(void*)f_3816},
{C_text("f_3820:posix_2escm"),(void*)f_3820},
{C_text("f_3841:posix_2escm"),(void*)f_3841},
{C_text("f_3843:posix_2escm"),(void*)f_3843},
{C_text("f_3868:posix_2escm"),(void*)f_3868},
{C_text("f_3877:posix_2escm"),(void*)f_3877},
{C_text("f_3883:posix_2escm"),(void*)f_3883},
{C_text("f_3908:posix_2escm"),(void*)f_3908},
{C_text("f_3921:posix_2escm"),(void*)f_3921},
{C_text("f_3927:posix_2escm"),(void*)f_3927},
{C_text("f_3941:posix_2escm"),(void*)f_3941},
{C_text("f_3948:posix_2escm"),(void*)f_3948},
{C_text("f_3958:posix_2escm"),(void*)f_3958},
{C_text("f_3967:posix_2escm"),(void*)f_3967},
{C_text("f_3981:posix_2escm"),(void*)f_3981},
{C_text("f_3988:posix_2escm"),(void*)f_3988},
{C_text("f_3998:posix_2escm"),(void*)f_3998},
{C_text("f_4007:posix_2escm"),(void*)f_4007},
{C_text("f_4014:posix_2escm"),(void*)f_4014},
{C_text("f_4022:posix_2escm"),(void*)f_4022},
{C_text("f_4029:posix_2escm"),(void*)f_4029},
{C_text("f_4037:posix_2escm"),(void*)f_4037},
{C_text("f_4041:posix_2escm"),(void*)f_4041},
{C_text("f_4046:posix_2escm"),(void*)f_4046},
{C_text("f_4051:posix_2escm"),(void*)f_4051},
{C_text("f_4057:posix_2escm"),(void*)f_4057},
{C_text("f_4061:posix_2escm"),(void*)f_4061},
{C_text("f_4066:posix_2escm"),(void*)f_4066},
{C_text("f_4071:posix_2escm"),(void*)f_4071},
{C_text("f_4075:posix_2escm"),(void*)f_4075},
{C_text("f_4080:posix_2escm"),(void*)f_4080},
{C_text("f_4086:posix_2escm"),(void*)f_4086},
{C_text("f_4090:posix_2escm"),(void*)f_4090},
{C_text("f_4095:posix_2escm"),(void*)f_4095},
{C_text("f_4099:posix_2escm"),(void*)f_4099},
{C_text("f_4104:posix_2escm"),(void*)f_4104},
{C_text("f_4110:posix_2escm"),(void*)f_4110},
{C_text("f_4114:posix_2escm"),(void*)f_4114},
{C_text("f_4119:posix_2escm"),(void*)f_4119},
{C_text("f_4123:posix_2escm"),(void*)f_4123},
{C_text("f_4128:posix_2escm"),(void*)f_4128},
{C_text("f_4133:posix_2escm"),(void*)f_4133},
{C_text("f_4139:posix_2escm"),(void*)f_4139},
{C_text("f_4143:posix_2escm"),(void*)f_4143},
{C_text("f_4148:posix_2escm"),(void*)f_4148},
{C_text("f_4159:posix_2escm"),(void*)f_4159},
{C_text("f_4169:posix_2escm"),(void*)f_4169},
{C_text("f_4172:posix_2escm"),(void*)f_4172},
{C_text("f_4176:posix_2escm"),(void*)f_4176},
{C_text("f_4179:posix_2escm"),(void*)f_4179},
{C_text("f_4185:posix_2escm"),(void*)f_4185},
{C_text("f_4194:posix_2escm"),(void*)f_4194},
{C_text("f_4198:posix_2escm"),(void*)f_4198},
{C_text("f_4203:posix_2escm"),(void*)f_4203},
{C_text("f_4225:posix_2escm"),(void*)f_4225},
{C_text("f_4229:posix_2escm"),(void*)f_4229},
{C_text("f_4232:posix_2escm"),(void*)f_4232},
{C_text("f_4235:posix_2escm"),(void*)f_4235},
{C_text("f_4238:posix_2escm"),(void*)f_4238},
{C_text("f_4241:posix_2escm"),(void*)f_4241},
{C_text("f_4250:posix_2escm"),(void*)f_4250},
{C_text("f_4268:posix_2escm"),(void*)f_4268},
{C_text("f_4272:posix_2escm"),(void*)f_4272},
{C_text("f_4275:posix_2escm"),(void*)f_4275},
{C_text("f_4281:posix_2escm"),(void*)f_4281},
{C_text("f_4284:posix_2escm"),(void*)f_4284},
{C_text("f_4290:posix_2escm"),(void*)f_4290},
{C_text("f_4308:posix_2escm"),(void*)f_4308},
{C_text("f_4321:posix_2escm"),(void*)f_4321},
{C_text("f_4330:posix_2escm"),(void*)f_4330},
{C_text("f_4335:posix_2escm"),(void*)f_4335},
{C_text("f_4343:posix_2escm"),(void*)f_4343},
{C_text("f_4382:posix_2escm"),(void*)f_4382},
{C_text("f_4423:posix_2escm"),(void*)f_4423},
{C_text("f_4427:posix_2escm"),(void*)f_4427},
{C_text("f_4436:posix_2escm"),(void*)f_4436},
{C_text("f_4467:posix_2escm"),(void*)f_4467},
{C_text("f_4474:posix_2escm"),(void*)f_4474},
{C_text("f_4483:posix_2escm"),(void*)f_4483},
{C_text("f_4529:posix_2escm"),(void*)f_4529},
{C_text("f_4533:posix_2escm"),(void*)f_4533},
{C_text("f_4536:posix_2escm"),(void*)f_4536},
{C_text("f_4539:posix_2escm"),(void*)f_4539},
{C_text("f_4545:posix_2escm"),(void*)f_4545},
{C_text("f_4557:posix_2escm"),(void*)f_4557},
{C_text("f_4559:posix_2escm"),(void*)f_4559},
{C_text("f_4580:posix_2escm"),(void*)f_4580},
{C_text("f_4605:posix_2escm"),(void*)f_4605},
{C_text("f_4617:posix_2escm"),(void*)f_4617},
{C_text("f_4644:posix_2escm"),(void*)f_4644},
{C_text("f_4658:posix_2escm"),(void*)f_4658},
{C_text("f_4697:posix_2escm"),(void*)f_4697},
{C_text("f_4722:posix_2escm"),(void*)f_4722},
{C_text("f_4727:posix_2escm"),(void*)f_4727},
{C_text("f_4734:posix_2escm"),(void*)f_4734},
{C_text("f_4741:posix_2escm"),(void*)f_4741},
{C_text("f_4780:posix_2escm"),(void*)f_4780},
{C_text("f_4784:posix_2escm"),(void*)f_4784},
{C_text("f_4796:posix_2escm"),(void*)f_4796},
{C_text("f_4807:posix_2escm"),(void*)f_4807},
{C_text("f_4824:posix_2escm"),(void*)f_4824},
{C_text("f_4839:posix_2escm"),(void*)f_4839},
{C_text("f_4843:posix_2escm"),(void*)f_4843},
{C_text("f_4907:posix_2escm"),(void*)f_4907},
{C_text("f_4924:posix_2escm"),(void*)f_4924},
{C_text("f_4928:posix_2escm"),(void*)f_4928},
{C_text("f_4932:posix_2escm"),(void*)f_4932},
{C_text("f_4944:posix_2escm"),(void*)f_4944},
{C_text("f_4951:posix_2escm"),(void*)f_4951},
{C_text("f_4955:posix_2escm"),(void*)f_4955},
{C_text("f_4959:posix_2escm"),(void*)f_4959},
{C_text("f_4963:posix_2escm"),(void*)f_4963},
{C_text("f_4983:posix_2escm"),(void*)f_4983},
{C_text("f_4991:posix_2escm"),(void*)f_4991},
{C_text("f_5016:posix_2escm"),(void*)f_5016},
{C_text("f_5030:posix_2escm"),(void*)f_5030},
{C_text("f_5032:posix_2escm"),(void*)f_5032},
{C_text("f_5037:posix_2escm"),(void*)f_5037},
{C_text("f_5046:posix_2escm"),(void*)f_5046},
{C_text("f_5073:posix_2escm"),(void*)f_5073},
{C_text("f_5076:posix_2escm"),(void*)f_5076},
{C_text("f_5081:posix_2escm"),(void*)f_5081},
{C_text("f_5087:posix_2escm"),(void*)f_5087},
{C_text("f_5112:posix_2escm"),(void*)f_5112},
{C_text("f_5114:posix_2escm"),(void*)f_5114},
{C_text("f_5174:posix_2escm"),(void*)f_5174},
{C_text("f_5246:posix_2escm"),(void*)f_5246},
{C_text("f_5256:posix_2escm"),(void*)f_5256},
{C_text("f_5267:posix_2escm"),(void*)f_5267},
{C_text("f_5273:posix_2escm"),(void*)f_5273},
{C_text("f_5279:posix_2escm"),(void*)f_5279},
{C_text("f_5285:posix_2escm"),(void*)f_5285},
{C_text("f_5291:posix_2escm"),(void*)f_5291},
{C_text("f_5297:posix_2escm"),(void*)f_5297},
{C_text("f_5303:posix_2escm"),(void*)f_5303},
{C_text("f_5309:posix_2escm"),(void*)f_5309},
{C_text("f_5315:posix_2escm"),(void*)f_5315},
{C_text("f_5321:posix_2escm"),(void*)f_5321},
{C_text("f_5327:posix_2escm"),(void*)f_5327},
{C_text("f_5333:posix_2escm"),(void*)f_5333},
{C_text("f_5339:posix_2escm"),(void*)f_5339},
{C_text("f_5345:posix_2escm"),(void*)f_5345},
{C_text("f_5351:posix_2escm"),(void*)f_5351},
{C_text("f_5357:posix_2escm"),(void*)f_5357},
{C_text("f_5363:posix_2escm"),(void*)f_5363},
{C_text("f_5369:posix_2escm"),(void*)f_5369},
{C_text("f_5375:posix_2escm"),(void*)f_5375},
{C_text("f_5381:posix_2escm"),(void*)f_5381},
{C_text("f_5387:posix_2escm"),(void*)f_5387},
{C_text("f_5393:posix_2escm"),(void*)f_5393},
{C_text("f_5399:posix_2escm"),(void*)f_5399},
{C_text("f_5405:posix_2escm"),(void*)f_5405},
{C_text("f_5411:posix_2escm"),(void*)f_5411},
{C_text("f_5417:posix_2escm"),(void*)f_5417},
{C_text("f_5423:posix_2escm"),(void*)f_5423},
{C_text("f_5429:posix_2escm"),(void*)f_5429},
{C_text("f_5435:posix_2escm"),(void*)f_5435},
{C_text("f_5441:posix_2escm"),(void*)f_5441},
{C_text("f_5447:posix_2escm"),(void*)f_5447},
{C_text("f_5465:posix_2escm"),(void*)f_5465},
{C_text("f_5510:posix_2escm"),(void*)f_5510},
{C_text("f_5514:posix_2escm"),(void*)f_5514},
{C_text("f_5519:posix_2escm"),(void*)f_5519},
{C_text("f_5526:posix_2escm"),(void*)f_5526},
{C_text("f_5544:posix_2escm"),(void*)f_5544},
{C_text("f_5548:posix_2escm"),(void*)f_5548},
{C_text("f_5550:posix_2escm"),(void*)f_5550},
{C_text("f_5554:posix_2escm"),(void*)f_5554},
{C_text("f_5557:posix_2escm"),(void*)f_5557},
{C_text("f_5566:posix_2escm"),(void*)f_5566},
{C_text("f_5587:posix_2escm"),(void*)f_5587},
{C_text("f_5591:posix_2escm"),(void*)f_5591},
{C_text("f_5593:posix_2escm"),(void*)f_5593},
{C_text("f_5597:posix_2escm"),(void*)f_5597},
{C_text("f_5599:posix_2escm"),(void*)f_5599},
{C_text("f_5603:posix_2escm"),(void*)f_5603},
{C_text("toplevel:posix_2escm"),(void*)C_posix_toplevel},
{C_text("va6083:posix_2escm"),(void*)va6083},
{C_text("va6091:posix_2escm"),(void*)va6091},
{NULL,NULL}};
#endif

static C_PTABLE_ENTRY *create_ptable(void){
#ifdef C_ENABLE_PTABLES
return ptable;
#else
return NULL;
#endif
}

/*
o|hiding unexported module binding: chicken.posix#d 
o|hiding unexported module binding: chicken.posix#define-alias 
o|hiding unexported module binding: chicken.posix#define-unimplemented 
o|hiding unexported module binding: chicken.posix#set!-unimplemented 
o|hiding unexported module binding: chicken.posix#posix-error 
o|hiding unexported module binding: chicken.posix#stat-mode 
o|hiding unexported module binding: chicken.posix#stat 
o|hiding unexported module binding: chicken.posix#decode-seconds 
o|hiding unexported module binding: chicken.posix#check-time-vector 
o|hiding unexported module binding: chicken.posix#list->c-string-buffer 
o|hiding unexported module binding: chicken.posix#free-c-string-buffer 
o|hiding unexported module binding: chicken.posix#check-environment-list 
o|hiding unexported module binding: chicken.posix#call-with-exec-args 
o|hiding unexported module binding: chicken.posix#duplicate-fileno 
o|hiding unexported module binding: chicken.posix#quote-arg-string 
o|hiding unexported module binding: chicken.posix#shell-command 
o|hiding unexported module binding: chicken.posix#shell-command-arguments 
o|hiding unexported module binding: chicken.posix#process-impl 
o|hiding unexported module binding: chicken.posix#process-wait-impl 
o|hiding unexported module binding: chicken.posix#chown 
S|applied compiler syntax:
S|  scheme#for-each		2
S|  scheme#map		3
o|eliminated procedure checks: 80 
o|specializations:
o|  4 (##sys#foreign-fixnum-argument fixnum)
o|  2 (scheme#string-length string)
o|  1 (chicken.bitwise#bitwise-ior fixnum fixnum)
o|  1 (##sys#check-output-port * * *)
o|  1 (##sys#check-input-port * * *)
o|  1 (##sys#call-with-values (procedure () *) *)
o|  3 (scheme#cdr pair)
o|  1 (chicken.base#add1 fixnum)
o|  1 (##sys#length list)
o|  1 (scheme#zero? *)
o|  1 (##sys#check-open-port * *)
o|  5 (scheme#eqv? * (or eof null fixnum char boolean symbol keyword))
o|  8 (scheme#car pair)
o|  4 (##sys#check-list (or pair list) *)
o|  1 (scheme#char=? char char)
o|  1 (scheme#zero? integer)
(o e)|safe calls: 474 
(o e)|assignments to immediate values: 114 
o|dropping redundant toplevel assignment: chicken.file.posix#file-stat 
o|dropping redundant toplevel assignment: chicken.file.posix#set-file-permissions! 
o|dropping redundant toplevel assignment: chicken.file.posix#file-modification-time 
o|dropping redundant toplevel assignment: chicken.file.posix#file-access-time 
o|dropping redundant toplevel assignment: chicken.file.posix#file-change-time 
o|dropping redundant toplevel assignment: chicken.file.posix#set-file-times! 
o|dropping redundant toplevel assignment: chicken.file.posix#file-size 
o|dropping redundant toplevel assignment: chicken.file.posix#set-file-owner! 
o|dropping redundant toplevel assignment: chicken.file.posix#set-file-group! 
o|dropping redundant toplevel assignment: chicken.process-context.posix#user-information 
o|safe globals: (chicken.file.posix#set-file-group! chicken.file.posix#set-file-owner! chicken.file.posix#file-size chicken.file.posix#set-file-times! chicken.file.posix#file-change-time chicken.file.posix#file-access-time chicken.file.posix#file-modification-time chicken.file.posix#set-file-permissions! chicken.file.posix#file-stat chicken.posix#stat ##sys#posix-error chicken.posix#posix-error chicken.process-context.posix#user-information chicken.process-context.posix#process-group-id chicken.process-context.posix#create-session chicken.process-context.posix#current-effective-user-name chicken.process-context.posix#current-user-name chicken.process-context.posix#parent-process-id chicken.process-context.posix#current-process-id chicken.process-context.posix#current-user-id chicken.process-context.posix#current-group-id chicken.process-context.posix#current-effective-user-id chicken.process-context.posix#current-effective-group-id chicken.process-context.posix#set-root-directory! chicken.process-context.posix#change-directory* chicken.process.signal#signals-list chicken.process.signal#signal/xfsz chicken.process.signal#signal/xcpu chicken.process.signal#signal/winch chicken.process.signal#signal/vtalrm chicken.process.signal#signal/usr2 chicken.process.signal#signal/usr1 chicken.process.signal#signal/urg chicken.process.signal#signal/tstp chicken.process.signal#signal/trap chicken.process.signal#signal/term chicken.process.signal#signal/stop chicken.process.signal#signal/segv chicken.process.signal#signal/quit chicken.process.signal#signal/prof chicken.process.signal#signal/pipe chicken.process.signal#signal/kill chicken.process.signal#signal/io chicken.process.signal#signal/int chicken.process.signal#signal/ill chicken.process.signal#signal/hup chicken.process.signal#signal/fpe chicken.process.signal#signal/cont chicken.process.signal#signal/chld chicken.process.signal#signal/bus chicken.process.signal#signal/break chicken.process.signal#signal/alrm chicken.process.signal#signal/abrt chicken.process.signal#signal-unmask! chicken.process.signal#signal-masked? chicken.process.signal#signal-mask! chicken.process.signal#signal-mask chicken.process.signal#signal-handler chicken.process.signal#set-signal-mask! chicken.process.signal#set-signal-handler! chicken.process.signal#set-alarm! chicken.process#spawn/detach chicken.process#spawn/nowaito chicken.process#spawn/nowait chicken.process#spawn/wait chicken.process#spawn/overlay chicken.process#pipe/buf chicken.process#process-sleep chicken.process#process* chicken.process#process chicken.process#with-output-to-pipe chicken.process#with-input-from-pipe chicken.process#open-output-pipe chicken.process#open-input-pipe chicken.process#create-pipe chicken.process#close-output-pipe chicken.process#close-input-pipe chicken.process#call-with-output-pipe chicken.process#call-with-input-pipe chicken.process#process-wait chicken.process#process-spawn chicken.process#process-signal chicken.process#process-run chicken.process#process-fork chicken.process#process-execute chicken.process#qs chicken.process#system* chicken.process#system chicken.time.posix#local-timezone-abbreviation chicken.time.posix#time->string chicken.time.posix#string->time chicken.time.posix#local-time->seconds chicken.time.posix#seconds->string chicken.time.posix#seconds->local-time chicken.time.posix#utc-time->seconds chicken.time.posix#seconds->utc-time chicken.file.posix#set-file-position! chicken.file.posix#seek/set chicken.file.posix#seek/end chicken.file.posix#seek/cur chicken.file.posix#port->fileno chicken.file.posix#perm/ixusr chicken.file.posix#perm/ixoth chicken.file.posix#perm/ixgrp chicken.file.posix#perm/iwusr chicken.file.posix#perm/iwoth chicken.file.posix#perm/iwgrp chicken.file.posix#perm/isvtx chicken.file.posix#perm/isuid chicken.file.posix#perm/isgid chicken.file.posix#perm/irwxu chicken.file.posix#perm/irwxo chicken.file.posix#perm/irwxg chicken.file.posix#perm/irusr chicken.file.posix#perm/iroth chicken.file.posix#perm/irgrp chicken.file.posix#open/wronly chicken.file.posix#open/write chicken.file.posix#open/trunc chicken.file.posix#open/text chicken.file.posix#open/sync chicken.file.posix#open/read chicken.file.posix#open/rdwr chicken.file.posix#open/rdonly chicken.file.posix#open/nonblock chicken.file.posix#open/noinherit chicken.file.posix#open/noctty chicken.file.posix#open/fsync chicken.file.posix#open/excl chicken.file.posix#open/creat chicken.file.posix#open/binary chicken.file.posix#open/append chicken.file.posix#open-output-file* chicken.file.posix#open-input-file* chicken.file.posix#fileno/stdout chicken.file.posix#fileno/stdin chicken.file.posix#fileno/stderr chicken.file.posix#symbolic-link? chicken.file.posix#socket? chicken.file.posix#regular-file? chicken.file.posix#fifo? chicken.file.posix#directory? chicken.file.posix#character-device? chicken.file.posix#block-device? chicken.file.posix#file-type chicken.file.posix#file-write chicken.file.posix#file-unlock chicken.file.posix#file-truncate chicken.file.posix#file-test-lock chicken.file.posix#file-select chicken.file.posix#file-read chicken.file.posix#file-position chicken.file.posix#file-permissions chicken.file.posix#file-owner chicken.file.posix#file-open chicken.file.posix#file-mkstemp chicken.file.posix#file-lock/blocking chicken.file.posix#file-lock chicken.file.posix#file-link chicken.file.posix#file-group chicken.file.posix#file-creation-mode chicken.file.posix#file-control chicken.file.posix#file-close chicken.file.posix#fcntl/setfl chicken.file.posix#fcntl/setfd chicken.file.posix#fcntl/getfl chicken.file.posix#fcntl/getfd chicken.file.posix#fcntl/dupfd chicken.file.posix#duplicate-fileno chicken.file.posix#read-symbolic-link chicken.file.posix#create-symbolic-link chicken.file.posix#create-fifo c1660) 
o|inlining procedure: k2336 
o|inlining procedure: k2336 
o|inlining procedure: k2354 
o|inlining procedure: k2354 
o|inlining procedure: k2381 
o|inlining procedure: k2381 
o|substituted constant variable: a2391 
o|inlining procedure: k2422 
o|inlining procedure: k2422 
o|contracted procedure: "(posix-common.scm:192) strerror630" 
o|inlining procedure: k2575 
o|inlining procedure: k2575 
o|inlining procedure: k2590 
o|inlining procedure: k2590 
o|inlining procedure: k2609 
o|inlining procedure: k2609 
o|inlining procedure: k2642 
o|inlining procedure: k2642 
o|inlining procedure: k2654 
o|inlining procedure: k2654 
o|inlining procedure: k2732 
o|inlining procedure: k2732 
o|contracted procedure: "(posix-common.scm:292) g702703" 
o|inlining procedure: k2719 
o|inlining procedure: k2719 
o|consed rest parameter at call site: "(posix-common.scm:305) chicken.posix#chown" 1 
o|consed rest parameter at call site: "(posix-common.scm:309) chicken.posix#chown" 1 
o|inlining procedure: k2819 
o|inlining procedure: k2831 
o|inlining procedure: k2831 
o|inlining procedure: k2843 
o|inlining procedure: k2843 
o|inlining procedure: k2855 
o|inlining procedure: k2855 
o|inlining procedure: k2819 
o|inlining procedure: k2976 
o|inlining procedure: k2976 
o|inlining procedure: k2991 
o|inlining procedure: k2991 
o|inlining procedure: k3004 
o|inlining procedure: k3004 
o|inlining procedure: k3054 
o|inlining procedure: k3067 
o|inlining procedure: k3067 
o|substituted constant variable: a3080 
o|inlining procedure: k3054 
o|inlining procedure: k3087 
o|inlining procedure: k3087 
o|inlining procedure: k3100 
o|inlining procedure: k3100 
o|substituted constant variable: a3135 
o|inlining procedure: k3136 
o|inlining procedure: k3136 
o|inlining procedure: k3155 
o|inlining procedure: k3155 
o|inlining procedure: k3185 
o|inlining procedure: k3185 
o|inlining procedure: k3212 
o|inlining procedure: k3212 
o|inlining procedure: k3225 
o|propagated global variable: r32265668 chicken.process-context.posix#change-directory* 
o|inlining procedure: k3225 
o|inlining procedure: k3245 
o|inlining procedure: k3245 
o|inlining procedure: k3323 
o|inlining procedure: k3323 
o|contracted procedure: "(posix-common.scm:595) ctime880" 
o|inlining procedure: k3359 
o|inlining procedure: k3359 
o|inlining procedure: k3403 
o|contracted procedure: "(posix-common.scm:618) strftime905" 
o|inlining procedure: k3389 
o|inlining procedure: k3389 
o|inlining procedure: k3403 
o|contracted procedure: "(posix-common.scm:620) asctime904" 
o|inlining procedure: k3376 
o|inlining procedure: k3376 
o|contracted procedure: "(posix-common.scm:655) chicken.posix#process-wait-impl" 
o|inlining procedure: k5236 
o|inlining procedure: k5236 
o|inlining procedure: k3512 
o|inlining procedure: k3512 
o|inlining procedure: k3598 
o|inlining procedure: k3598 
o|contracted procedure: "(posix-common.scm:688) c-string->allocated-pointer974" 
o|merged explicitly consed rest parameter: args9861007 
o|consed rest parameter at call site: tmp22178 1 
o|inlining procedure: k3679 
o|inlining procedure: k3679 
o|inlining procedure: k3728 
o|inlining procedure: k3728 
o|inlining procedure: k3786 
o|inlining procedure: k3786 
o|inlining procedure: k3845 
o|contracted procedure: "(posix-common.scm:731) g10741083" 
o|inlining procedure: k3845 
o|inlining procedure: k3892 
o|inlining procedure: k3892 
o|inlining procedure: k3910 
o|inlining procedure: k3910 
o|inlining procedure: k3923 
o|inlining procedure: k3923 
o|inlining procedure: k3939 
o|inlining procedure: k3939 
o|inlining procedure: "(posix-common.scm:760) badmode1097" 
o|substituted constant variable: a3963 
o|substituted constant variable: a3965 
o|inlining procedure: k3979 
o|inlining procedure: k3979 
o|inlining procedure: "(posix-common.scm:771) badmode1097" 
o|substituted constant variable: a4003 
o|substituted constant variable: a4005 
o|substituted constant variable: a4010 
o|substituted constant variable: a4011 
o|inlining procedure: k4012 
o|inlining procedure: k4012 
o|substituted constant variable: a4025 
o|substituted constant variable: a4026 
o|inlining procedure: k4027 
o|inlining procedure: k4027 
o|inlining procedure: k4177 
o|inlining procedure: k4177 
o|inlining procedure: k4205 
o|inlining procedure: k4205 
o|inlining procedure: k4239 
o|inlining procedure: k4239 
o|inlining procedure: k4282 
o|inlining procedure: k4282 
o|inlining procedure: k4352 
o|inlining procedure: k4352 
o|inlining procedure: k4384 
o|inlining procedure: k4384 
o|inlining procedure: k4411 
o|inlining procedure: k4411 
o|inlining procedure: k4428 
o|inlining procedure: k4428 
o|inlining procedure: k4438 
o|inlining procedure: k4438 
o|inlining procedure: k4472 
o|inlining procedure: k4472 
o|inlining procedure: k4537 
o|inlining procedure: k4537 
o|inlining procedure: k4607 
o|inlining procedure: k4607 
o|contracted procedure: "(posixwin.scm:721) needs-quoting?1374" 
o|inlining procedure: k4582 
o|inlining procedure: k4582 
o|inlining procedure: k4649 
o|inlining procedure: k4649 
o|inlining procedure: k4732 
o|inlining procedure: k4732 
o|inlining procedure: k4785 
o|inlining procedure: k4785 
o|inlining procedure: k4812 
o|inlining procedure: k4812 
o|inlining procedure: "(posixwin.scm:773) chicken.posix#shell-command-arguments" 
o|merged explicitly consed rest parameter: rest14861494 
o|substituted constant variable: g152815291536 
o|substituted constant variable: g152815291536 
o|substituted constant variable: g152815291536 
o|substituted constant variable: g153015311537 
o|substituted constant variable: g153015311537 
o|substituted constant variable: g153015311537 
o|substituted constant variable: g153215331538 
o|substituted constant variable: g153215331538 
o|substituted constant variable: g153215331538 
o|substituted constant variable: g153415351539 
o|substituted constant variable: g153415351539 
o|substituted constant variable: g153415351539 
o|inlining procedure: k4915 
o|inlining procedure: k4930 
o|inlining procedure: k4930 
o|inlining procedure: k4915 
o|contracted procedure: "(posixwin.scm:810) c-process1450" 
o|inlining procedure: k4977 
o|inlining procedure: k4977 
o|inlining procedure: k4981 
o|inlining procedure: k4981 
o|inlining procedure: k4993 
o|inlining procedure: k4993 
o|inlining procedure: k5048 
o|inlining procedure: k5048 
o|consed rest parameter at call site: "(posixwin.scm:843) chicken.posix#process-impl" 8 
o|inlining procedure: k5089 
o|inlining procedure: k5089 
o|inlining procedure: "(posixwin.scm:839) chicken.posix#shell-command-arguments" 
o|inlining procedure: k5248 
o|inlining procedure: k5248 
o|merged explicitly consed rest parameter: _16551659 
o|inlining procedure: k5531 
o|inlining procedure: k5531 
o|inlining procedure: k5555 
o|inlining procedure: k5555 
o|inlining procedure: k5567 
o|inlining procedure: k5567 
o|inlining procedure: k5577 
o|inlining procedure: k5577 
o|replaced variables: 769 
o|removed binding forms: 559 
o|substituted constant variable: r25765613 
o|substituted constant variable: r27205626 
o|substituted constant variable: r27205626 
o|inlining procedure: "(posix-common.scm:305) chicken.posix#chown" 
o|inlining procedure: "(posix-common.scm:309) chicken.posix#chown" 
o|substituted constant variable: r28325629 
o|substituted constant variable: r28445631 
o|substituted constant variable: r28565633 
o|substituted constant variable: r28205635 
o|substituted constant variable: r29925639 
o|inlining procedure: k3054 
o|substituted constant variable: r30685644 
o|inlining procedure: k3054 
o|inlining procedure: k3054 
o|substituted constant variable: r31015650 
o|substituted constant variable: r31015650 
o|substituted constant variable: r31015652 
o|substituted constant variable: r31015652 
o|propagated global variable: g8338345669 chicken.process-context.posix#change-directory* 
o|substituted constant variable: r33905688 
o|substituted constant variable: r33905688 
o|substituted constant variable: r33775695 
o|substituted constant variable: r33775695 
o|substituted constant variable: r38935721 
o|removed side-effect free assignment to unused variable: badmode1097 
o|substituted constant variable: r39245724 
o|substituted constant variable: r39245724 
o|substituted constant variable: r39245726 
o|substituted constant variable: r39245726 
o|inlining procedure: k3939 
o|inlining procedure: k3979 
o|substituted constant variable: r44295782 
o|substituted constant variable: r44295782 
o|substituted constant variable: r45835802 
o|removed side-effect free assignment to unused variable: chicken.posix#shell-command-arguments 
o|contracted procedure: "(posixwin.scm:843) chicken.posix#process-impl" 
o|substituted constant variable: r49315822 
o|substituted constant variable: r49315822 
o|substituted constant variable: c-pointer14531470 
o|substituted constant variable: c-pointer14531470 
o|substituted constant variable: r49785825 
o|substituted constant variable: r49785825 
o|substituted constant variable: r49785827 
o|substituted constant variable: r49785827 
o|removed unused formal parameters: (_16551659) 
o|substituted constant variable: r55685855 
o|replaced variables: 84 
o|removed binding forms: 642 
o|removed conditional forms: 1 
o|inlining procedure: k3463 
o|inlining procedure: k3463 
o|inlining procedure: k3688 
o|inlining procedure: k4254 
o|inlining procedure: k4254 
o|inlining procedure: k4301 
o|inlining procedure: k4301 
o|substituted constant variable: stdinf1492 
o|substituted constant variable: stdoutf1491 
o|contracted procedure: k4845 
o|substituted constant variable: stdoutf1491 
o|substituted constant variable: stdinf1492 
o|inlining procedure: k5131 
o|inlining procedure: k5191 
o|removed side-effect free assignment to unused variable: chicken.posix#chown 
o|replaced variables: 8 
o|removed binding forms: 123 
o|removed conditional forms: 4 
o|substituted constant variable: r30555870 
o|substituted constant variable: r30555872 
o|substituted constant variable: r30555874 
o|substituted constant variable: r34645934 
o|contracted procedure: k3613 
o|substituted constant variable: r42555951 
o|substituted constant variable: r43025953 
o|contracted procedure: k4912 
o|substituted constant variable: r4846 
o|contracted procedure: k4969 
o|contracted procedure: k4973 
o|inlining procedure: k5528 
o|inlining procedure: k5528 
o|simplifications: ((let . 3)) 
o|replaced variables: 1 
o|removed binding forms: 18 
o|removed conditional forms: 2 
o|substituted constant variable: r4970 
o|substituted constant variable: r4974 
o|folded constant expression: (scheme#+ (quote 0) (quote 0) (quote 0)) 
o|substituted constant variable: r4970 
o|substituted constant variable: r4974 
o|folded constant expression: (scheme#+ (quote 0) (quote 0) (quote 4)) 
o|substituted constant variable: r55295995 
o|replaced variables: 1 
o|removed binding forms: 9 
o|removed binding forms: 4 
o|simplifications: ((let . 22) (if . 58) (##core#call . 309)) 
o|  call simplifications:
o|    scheme#<
o|    ##sys#foreign-pointer-argument	4
o|    scheme#string-length
o|    scheme#char-whitespace?
o|    scheme#*
o|    chicken.fixnum#fx>=	2
o|    scheme#string-ref	3
o|    scheme#string-set!
o|    chicken.bitwise#bitwise-ior
o|    chicken.fixnum#fxior	3
o|    scheme#list	5
o|    scheme#call-with-values	4
o|    ##sys#check-pair
o|    ##sys#check-list	4
o|    ##sys#apply	2
o|    chicken.fixnum#fx+	6
o|    ##sys#call-with-values	3
o|    scheme#values	10
o|    scheme#vector-set!	2
o|    ##sys#foreign-block-argument	2
o|    scheme#=
o|    ##sys#foreign-ranged-integer-argument
o|    chicken.fixnum#fx-	6
o|    ##sys#check-vector
o|    ##sys#size	6
o|    ##sys#null-pointer?	2
o|    scheme#not	3
o|    ##sys#setislot
o|    chicken.fixnum#fx=	16
o|    scheme#cdr	18
o|    ##sys#foreign-string-argument	3
o|    scheme#vector
o|    chicken.base#fixnum?	5
o|    scheme#string?	2
o|    ##sys#foreign-fixnum-argument	2
o|    scheme#null?	48
o|    scheme#car	31
o|    scheme#string->list
o|    scheme#pair?	12
o|    scheme#cons	8
o|    ##sys#setslot	3
o|    ##sys#slot	19
o|    scheme#apply	11
o|    scheme#char=?
o|    scheme#string	3
o|    scheme#eq?	25
o|    ##sys#check-string	11
o|    chicken.fixnum#fx<	11
o|contracted procedure: k2333 
o|contracted procedure: k2339 
o|contracted procedure: k2357 
o|contracted procedure: k2456 
o|contracted procedure: k2370 
o|contracted procedure: k2453 
o|contracted procedure: k2373 
o|contracted procedure: k2376 
o|contracted procedure: k2384 
o|contracted procedure: k2408 
o|contracted procedure: k2416 
o|contracted procedure: k2425 
o|contracted procedure: k2428 
o|contracted procedure: k2431 
o|contracted procedure: k2439 
o|contracted procedure: k2447 
o|contracted procedure: k2459 
o|contracted procedure: k2549 
o|contracted procedure: k2578 
o|contracted procedure: k2587 
o|contracted procedure: k2603 
o|contracted procedure: k2627 
o|contracted procedure: k2618 
o|contracted procedure: k2645 
o|contracted procedure: k2651 
o|contracted procedure: k2667 
o|contracted procedure: k2759 
o|contracted procedure: k2701 
o|contracted procedure: k2753 
o|contracted procedure: k2704 
o|contracted procedure: k2747 
o|contracted procedure: k2707 
o|contracted procedure: k2735 
o|contracted procedure: k2726 
o|contracted procedure: k2765 
o|contracted procedure: k2885 
o|contracted procedure: k2807 
o|contracted procedure: k2879 
o|contracted procedure: k2810 
o|contracted procedure: k2873 
o|contracted procedure: k2813 
o|contracted procedure: k2867 
o|contracted procedure: k2816 
o|contracted procedure: k2828 
o|contracted procedure: k2834 
o|contracted procedure: k2840 
o|contracted procedure: k2846 
o|contracted procedure: k2852 
o|contracted procedure: k2858 
o|contracted procedure: k2864 
o|contracted procedure: k3013 
o|contracted procedure: k2967 
o|contracted procedure: k3001 
o|contracted procedure: k2988 
o|contracted procedure: k2997 
o|contracted procedure: k3007 
o|contracted procedure: k3057 
o|contracted procedure: k3064 
o|contracted procedure: k3070 
o|contracted procedure: k3090 
o|contracted procedure: k3173 
o|contracted procedure: k3139 
o|contracted procedure: k3152 
o|contracted procedure: k3158 
o|contracted procedure: k3188 
o|contracted procedure: k3194 
o|contracted procedure: k3197 
o|contracted procedure: k3215 
o|contracted procedure: k3231 
o|contracted procedure: k3242 
o|contracted procedure: k3255 
o|contracted procedure: k3248 
o|contracted procedure: k3270 
o|inlining procedure: k3261 
o|contracted procedure: k3291 
o|inlining procedure: k3282 
o|contracted procedure: k3334 
o|contracted procedure: k3330 
o|contracted procedure: k3308 
o|contracted procedure: k3340 
o|contracted procedure: k3362 
o|contracted procedure: k3450 
o|contracted procedure: k3397 
o|contracted procedure: k3406 
o|contracted procedure: k3389 
o|contracted procedure: k3440 
o|contracted procedure: k3436 
o|contracted procedure: k3376 
o|contracted procedure: k3542 
o|contracted procedure: k3482 
o|contracted procedure: k3536 
o|contracted procedure: k3485 
o|contracted procedure: k3530 
o|contracted procedure: k3488 
o|contracted procedure: k3524 
o|contracted procedure: k3491 
o|contracted procedure: k3494 
o|contracted procedure: k3515 
o|contracted procedure: k3553 
o|contracted procedure: k3601 
o|contracted procedure: k3604 
o|contracted procedure: k3641 
o|contracted procedure: k3607 
o|contracted procedure: k3628 
o|contracted procedure: k3682 
o|contracted procedure: k3695 
o|contracted procedure: k36955941 
o|contracted procedure: k3704 
o|contracted procedure: k3709 
o|contracted procedure: k3712 
o|contracted procedure: k3731 
o|contracted procedure: k3741 
o|contracted procedure: k3745 
o|contracted procedure: k3756 
o|contracted procedure: k3822 
o|contracted procedure: k3836 
o|contracted procedure: k3848 
o|contracted procedure: k3851 
o|contracted procedure: k3854 
o|contracted procedure: k3862 
o|contracted procedure: k3870 
o|contracted procedure: k3831 
o|contracted procedure: k3895 
o|contracted procedure: k3913 
o|contracted procedure: k3929 
o|contracted procedure: k3942 
o|contracted procedure: k3952 
o|contracted procedure: k3969 
o|contracted procedure: k3982 
o|contracted procedure: k3992 
o|contracted procedure: k4015 
o|contracted procedure: k4030 
o|contracted procedure: k4156 
o|contracted procedure: k4189 
o|contracted procedure: k4161 
o|contracted procedure: k4164 
o|contracted procedure: k4180 
o|contracted procedure: k4208 
o|contracted procedure: k4214 
o|contracted procedure: k4245 
o|contracted procedure: k4260 
o|contracted procedure: k4294 
o|contracted procedure: k4276 
o|contracted procedure: k4285 
o|contracted procedure: k4310 
o|contracted procedure: k4313 
o|contracted procedure: k4322 
o|contracted procedure: k4349 
o|contracted procedure: k4355 
o|contracted procedure: k4361 
o|contracted procedure: k4368 
o|contracted procedure: k4378 
o|contracted procedure: k4387 
o|contracted procedure: k4401 
o|contracted procedure: k4390 
o|contracted procedure: k4397 
o|contracted procedure: k4405 
o|contracted procedure: k4414 
o|contracted procedure: k4432 
o|contracted procedure: k4448 
o|contracted procedure: k4454 
o|contracted procedure: k4461 
o|contracted procedure: k4488 
o|contracted procedure: k4469 
o|contracted procedure: k4478 
o|contracted procedure: k4525 
o|contracted procedure: k4540 
o|contracted procedure: k4549 
o|contracted procedure: k4552 
o|contracted procedure: k4573 
o|contracted procedure: k4585 
o|contracted procedure: k4602 
o|contracted procedure: k4591 
o|contracted procedure: k4598 
o|contracted procedure: k4690 
o|contracted procedure: k4619 
o|contracted procedure: k4684 
o|contracted procedure: k4622 
o|contracted procedure: k4678 
o|contracted procedure: k4625 
o|contracted procedure: k4672 
o|contracted procedure: k4628 
o|contracted procedure: k4666 
o|contracted procedure: k4631 
o|contracted procedure: k4660 
o|contracted procedure: k4634 
o|contracted procedure: k4637 
o|contracted procedure: k4646 
o|contracted procedure: k4652 
o|contracted procedure: k4773 
o|contracted procedure: k4699 
o|contracted procedure: k4767 
o|contracted procedure: k4702 
o|contracted procedure: k4761 
o|contracted procedure: k4705 
o|contracted procedure: k4755 
o|contracted procedure: k4708 
o|contracted procedure: k4749 
o|contracted procedure: k4711 
o|contracted procedure: k4743 
o|contracted procedure: k4714 
o|contracted procedure: k4717 
o|contracted procedure: k4729 
o|contracted procedure: k4735 
o|contracted procedure: k4829 
o|contracted procedure: k4809 
o|contracted procedure: k4826 
o|contracted procedure: k5034 
o|contracted procedure: k5051 
o|contracted procedure: k5061 
o|contracted procedure: k5065 
o|contracted procedure: k5068 
o|contracted procedure: k5024 
o|contracted procedure: k4899 
o|contracted procedure: k4902 
o|contracted procedure: k4965 
o|contracted procedure: k4849 
o|contracted procedure: k4853 
o|contracted procedure: k4857 
o|contracted procedure: k4861 
o|contracted procedure: k4865 
o|contracted procedure: k4887 
o|contracted procedure: k4894 
o|contracted procedure: k4984 
o|contracted procedure: k4996 
o|contracted procedure: k4999 
o|contracted procedure: k5002 
o|contracted procedure: k5010 
o|contracted procedure: k5018 
o|contracted procedure: k5106 
o|contracted procedure: k5167 
o|contracted procedure: k5116 
o|contracted procedure: k5161 
o|contracted procedure: k5119 
o|contracted procedure: k5155 
o|contracted procedure: k5122 
o|contracted procedure: k5149 
o|contracted procedure: k5125 
o|contracted procedure: k5143 
o|contracted procedure: k5128 
o|contracted procedure: k5137 
o|contracted procedure: k5131 
o|contracted procedure: k5227 
o|contracted procedure: k5176 
o|contracted procedure: k5221 
o|contracted procedure: k5179 
o|contracted procedure: k5215 
o|contracted procedure: k5182 
o|contracted procedure: k5209 
o|contracted procedure: k5185 
o|contracted procedure: k5203 
o|contracted procedure: k5188 
o|contracted procedure: k5197 
o|contracted procedure: k5191 
o|contracted procedure: k5537 
o|contracted procedure: k5521 
o|contracted procedure: k5558 
o|contracted procedure: k5574 
o|contracted procedure: k5570 
o|contracted procedure: k5580 
o|simplifications: ((if . 1) (##core#call . 4) (let . 68)) 
o|  call simplifications:
o|    scheme#*	2
o|    chicken.bitwise#bitwise-ior	2
o|removed binding forms: 258 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest237239 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest237239 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest666668 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest666668 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest732734 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest732734 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest732734 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest732734 0 
(o x)|known list op on rest arg sublist: ##core#rest-car whence761 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest861862 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest861862 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest872873 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest872873 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest890891 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest890891 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest931933 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest931933 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? args954 0 
(o x)|known list op on rest arg sublist: ##core#rest-car args954 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? args954 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr args954 0 
o|inlining procedure: "(posix-common.scm:753) mode1096" 
o|inlining procedure: "(posix-common.scm:764) mode1096" 
(o x)|known list op on rest arg sublist: ##core#rest-car mode1282 0 
o|contracted procedure: k4316 
o|contracted procedure: k4345 
o|contracted procedure: k4441 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest13521353 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest13521353 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest13881390 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest13881390 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest13881390 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest13881390 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest14131416 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest14131416 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest14131416 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest14131416 0 
(o x)|known list op on rest arg sublist: ##core#rest-car args1447 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest16161618 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest16161618 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest16161618 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest16161618 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest16341636 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest16341636 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest16341636 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest16341636 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest838839 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest838839 0 
o|removed binding forms: 3 
(o x)|known list op on rest arg sublist: ##core#rest-null? r2811 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r2811 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r2811 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r2811 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r3486 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r3486 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r3486 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r3486 1 
o|removed side-effect free assignment to unused variable: mode1096 
(o x)|known list op on rest arg sublist: ##core#rest-null? r4623 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r4623 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r4623 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r4623 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r4703 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r4703 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r4703 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r4703 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r5120 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r5120 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r5120 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r5120 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r5180 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r5180 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r5180 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r5180 1 
o|replaced variables: 6 
(o x)|known list op on rest arg sublist: ##core#rest-null? r4629 2 
(o x)|known list op on rest arg sublist: ##core#rest-car r4629 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r4629 2 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r4629 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r4709 2 
(o x)|known list op on rest arg sublist: ##core#rest-car r4709 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r4709 2 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r4709 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r5126 2 
(o x)|known list op on rest arg sublist: ##core#rest-car r5126 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r5126 2 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r5126 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r5186 2 
(o x)|known list op on rest arg sublist: ##core#rest-car r5186 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r5186 2 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r5186 2 
o|converted assignments to bindings: (check1098) 
o|simplifications: ((let . 1)) 
o|removed binding forms: 14 
o|contracted procedure: k3932 
o|contracted procedure: k3972 
o|removed binding forms: 10 
o|direct leaf routine/allocation: g10251032 0 
o|direct leaf routine/allocation: suffix-loop1340 0 
o|direct leaf routine/allocation: loop1327 0 
o|direct leaf routine/allocation: loop1377 0 
o|direct leaf routine/allocation: g15811588 0 
o|contracted procedure: "(posix-common.scm:704) k3734" 
o|contracted procedure: k4325 
o|contracted procedure: k4337 
o|converted assignments to bindings: (suffix-loop1340) 
o|converted assignments to bindings: (loop1327) 
o|contracted procedure: k4610 
o|converted assignments to bindings: (loop1377) 
o|contracted procedure: "(posixwin.scm:833) k5054" 
o|simplifications: ((let . 3)) 
o|removed binding forms: 5 
o|direct leaf routine/allocation: for-each-loop10241037 0 
o|direct leaf routine/allocation: for-each-loop15801598 0 
o|converted assignments to bindings: (for-each-loop10241037) 
o|converted assignments to bindings: (for-each-loop15801598) 
o|simplifications: ((let . 2)) 
o|customizable procedures: (%process1569 chkstrlst1576 map-loop15041521 chicken.posix#shell-command chicken.posix#call-with-exec-args k4534 va6091 va6083 loop1338 check1098 chicken.posix#check-environment-list map-loop10681086 chicken.posix#list->c-string-buffer k3804 doloop10121013 tmp12177 tmp22178 doloop993994 chicken.posix#free-c-string-buffer chicken.posix#check-time-vector k3182 mode780 check781 k2729 chicken.posix#stat g256265 map-loop250272) 
o|calls to known targets: 110 
o|unused rest argument: rest237239 f_2365 
o|unused rest argument: rest666668 f_2616 
o|unused rest argument: rest732734 f_2805 
o|unused rest argument: rest861862 f_3259 
o|unused rest argument: rest872873 f_3280 
o|unused rest argument: rest890891 f_3312 
o|unused rest argument: rest931933 f_3395 
o|unused rest argument: args954 f_3480 
o|identified direct recursive calls: f_3726 1 
o|identified direct recursive calls: f_4436 1 
o|identified direct recursive calls: f_4382 1 
o|unused rest argument: rest13521353 f_4467 
o|identified direct recursive calls: f_4580 1 
o|unused rest argument: rest13881390 f_4617 
o|unused rest argument: rest14131416 f_4697 
o|identified direct recursive calls: f_5046 1 
o|unused rest argument: rest16161618 f_5114 
o|unused rest argument: rest16341636 f_5174 
o|unused rest argument: _16631666 f_5267 
o|unused rest argument: _16691672 f_5273 
o|unused rest argument: _16751678 f_5279 
o|unused rest argument: _16811684 f_5285 
o|unused rest argument: _16871690 f_5291 
o|unused rest argument: _16931696 f_5297 
o|unused rest argument: _16991702 f_5303 
o|unused rest argument: _17051708 f_5309 
o|unused rest argument: _17171720 f_5315 
o|unused rest argument: _17231726 f_5321 
o|unused rest argument: _17291732 f_5327 
o|unused rest argument: _17351738 f_5333 
o|unused rest argument: _17411744 f_5339 
o|unused rest argument: _17471750 f_5345 
o|unused rest argument: _17531756 f_5351 
o|unused rest argument: _17591762 f_5357 
o|unused rest argument: _17651768 f_5363 
o|unused rest argument: _17711774 f_5369 
o|unused rest argument: _17771780 f_5375 
o|unused rest argument: _17831786 f_5381 
o|unused rest argument: _17891792 f_5387 
o|unused rest argument: _17951798 f_5393 
o|unused rest argument: _18011804 f_5399 
o|unused rest argument: _18071810 f_5405 
o|unused rest argument: _18131816 f_5411 
o|unused rest argument: _18191822 f_5417 
o|unused rest argument: _18251828 f_5423 
o|unused rest argument: _18311834 f_5429 
o|unused rest argument: _18371840 f_5435 
o|unused rest argument: _18431846 f_5441 
o|unused rest argument: _18491852 f_5447 
o|unused rest argument: rest838839 f_5519 
o|fast box initializations: 10 
o|fast global references: 83 
o|fast global assignments: 12 
o|dropping unused closure argument: f_2570 
o|dropping unused closure argument: f_3048 
o|dropping unused closure argument: f_3085 
o|dropping unused closure argument: f_3240 
o|dropping unused closure argument: f_3551 
o|dropping unused closure argument: f_3668 
o|dropping unused closure argument: f_3702 
o|dropping unused closure argument: f_3908 
o|dropping unused closure argument: f_4780 
o|dropping unused closure argument: f_5030 
*/
/* end of file */
