/* Generated from lolevel.scm by the CHICKEN compiler
   http://www.call-cc.org
   Version 5.2.0 (rev 317468e4)
   linux-unix-gnu-x86-64 [ 64bit dload ptables ]
   command line: lolevel.scm -optimize-level 2 -include-path . -include-path ./ -inline -ignore-repository -feature chicken-bootstrap -no-warnings -specialize -consult-types-file ./types.db -explicit-use -no-trace -output-file lolevel.c -emit-import-library chicken.locative -emit-import-library chicken.memory -emit-import-library chicken.memory.representation
   unit: lolevel
   uses: library
*/
#include "chicken.h"

#ifndef C_NONUNIX
# include <sys/mman.h>
#endif

#define C_memmove_o(to, from, n, toff, foff) C_memmove((char *)(to) + (toff), (char *)(from) + (foff), (n))

static C_PTABLE_ENTRY *create_ptable(void);
C_noret_decl(C_library_toplevel)
C_externimport void C_ccall C_library_toplevel(C_word c,C_word *av) C_noret;

static C_TLS C_word lf[156];
static double C_possibly_force_alignment;
static C_char C_TLS li0[] C_aligned={C_lihdr(0,0,27),40,35,35,115,121,115,35,99,104,101,99,107,45,98,108,111,99,107,32,120,32,46,32,108,111,99,41,0,0,0,0,0};
static C_char C_TLS li1[] C_aligned={C_lihdr(0,0,10),40,108,111,111,112,32,108,115,116,41,0,0,0,0,0,0};
static C_char C_TLS li2[] C_aligned={C_lihdr(0,0,32),40,35,35,115,121,115,35,99,104,101,99,107,45,98,101,99,111,109,101,45,97,108,105,115,116,32,120,32,108,111,99,41};
static C_char C_TLS li3[] C_aligned={C_lihdr(0,0,39),40,35,35,115,121,115,35,99,104,101,99,107,45,103,101,110,101,114,105,99,45,115,116,114,117,99,116,117,114,101,32,120,32,46,32,108,111,99,41,0};
static C_char C_TLS li4[] C_aligned={C_lihdr(0,0,36),40,35,35,115,121,115,35,99,104,101,99,107,45,103,101,110,101,114,105,99,45,118,101,99,116,111,114,32,120,32,46,32,108,111,99,41,0,0,0,0};
static C_char C_TLS li5[] C_aligned={C_lihdr(0,0,29),40,35,35,115,121,115,35,99,104,101,99,107,45,112,111,105,110,116,101,114,32,120,32,46,32,108,111,99,41,0,0,0};
static C_char C_TLS li6[] C_aligned={C_lihdr(0,0,10),40,110,111,115,105,122,101,114,114,41,0,0,0,0,0,0};
static C_char C_TLS li7[] C_aligned={C_lihdr(0,0,13),40,115,105,122,101,114,114,32,97,114,103,115,41,0,0,0};
static C_char C_TLS li8[] C_aligned={C_lihdr(0,0,20),40,99,104,101,99,107,110,49,32,110,32,110,109,97,120,32,111,102,102,41,0,0,0,0};
static C_char C_TLS li9[] C_aligned={C_lihdr(0,0,32),40,99,104,101,99,107,110,50,32,110,32,110,109,97,120,32,110,109,97,120,50,32,111,102,102,49,32,111,102,102,50,41};
static C_char C_TLS li10[] C_aligned={C_lihdr(0,0,14),40,109,111,118,101,32,102,114,111,109,32,116,111,41,0,0};
static C_char C_TLS li11[] C_aligned={C_lihdr(0,0,44),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,35,109,111,118,101,45,109,101,109,111,114,121,33,32,102,114,111,109,32,116,111,32,46,32,114,101,115,116,41,0,0,0,0};
static C_char C_TLS li12[] C_aligned={C_lihdr(0,0,32),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,35,97,108,108,111,99,97,116,101,32,105,110,116,51,49,54,41};
static C_char C_TLS li13[] C_aligned={C_lihdr(0,0,34),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,35,102,114,101,101,32,99,45,112,111,105,110,116,101,114,51,50,50,41,0,0,0,0,0,0};
static C_char C_TLS li14[] C_aligned={C_lihdr(0,0,27),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,35,112,111,105,110,116,101,114,63,32,120,41,0,0,0,0,0};
static C_char C_TLS li15[] C_aligned={C_lihdr(0,0,32),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,35,112,111,105,110,116,101,114,45,108,105,107,101,63,32,120,41};
static C_char C_TLS li16[] C_aligned={C_lihdr(0,0,38),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,35,97,100,100,114,101,115,115,45,62,112,111,105,110,116,101,114,32,97,100,100,114,41,0,0};
static C_char C_TLS li17[] C_aligned={C_lihdr(0,0,37),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,35,112,111,105,110,116,101,114,45,62,97,100,100,114,101,115,115,32,112,116,114,41,0,0,0};
static C_char C_TLS li18[] C_aligned={C_lihdr(0,0,34),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,35,111,98,106,101,99,116,45,62,112,111,105,110,116,101,114,32,120,41,0,0,0,0,0,0};
static C_char C_TLS li19[] C_aligned={C_lihdr(0,0,36),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,35,112,111,105,110,116,101,114,45,62,111,98,106,101,99,116,32,112,116,114,41,0,0,0,0};
static C_char C_TLS li20[] C_aligned={C_lihdr(0,0,32),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,35,112,111,105,110,116,101,114,61,63,32,112,49,32,112,50,41};
static C_char C_TLS li21[] C_aligned={C_lihdr(0,0,39),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,35,112,111,105,110,116,101,114,43,32,112,116,114,51,54,52,32,111,102,102,51,54,53,41,0};
static C_char C_TLS li22[] C_aligned={C_lihdr(0,0,32),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,35,97,108,105,103,110,45,116,111,45,119,111,114,100,32,120,41};
static C_char C_TLS li23[] C_aligned={C_lihdr(0,0,36),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,35,116,97,103,45,112,111,105,110,116,101,114,32,112,116,114,32,116,97,103,41,0,0,0,0};
static C_char C_TLS li24[] C_aligned={C_lihdr(0,0,41),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,35,116,97,103,103,101,100,45,112,111,105,110,116,101,114,63,32,120,32,46,32,114,101,115,116,41,0,0,0,0,0,0,0};
static C_char C_TLS li25[] C_aligned={C_lihdr(0,0,30),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,35,112,111,105,110,116,101,114,45,116,97,103,32,120,41,0,0};
static C_char C_TLS li26[] C_aligned={C_lihdr(0,0,36),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,35,112,111,105,110,116,101,114,45,117,56,45,115,101,116,33,32,112,32,110,41,0,0,0,0};
static C_char C_TLS li27[] C_aligned={C_lihdr(0,0,36),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,35,112,111,105,110,116,101,114,45,115,56,45,115,101,116,33,32,112,32,110,41,0,0,0,0};
static C_char C_TLS li28[] C_aligned={C_lihdr(0,0,37),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,35,112,111,105,110,116,101,114,45,117,49,54,45,115,101,116,33,32,112,32,110,41,0,0,0};
static C_char C_TLS li29[] C_aligned={C_lihdr(0,0,37),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,35,112,111,105,110,116,101,114,45,115,49,54,45,115,101,116,33,32,112,32,110,41,0,0,0};
static C_char C_TLS li30[] C_aligned={C_lihdr(0,0,37),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,35,112,111,105,110,116,101,114,45,117,51,50,45,115,101,116,33,32,112,32,110,41,0,0,0};
static C_char C_TLS li31[] C_aligned={C_lihdr(0,0,37),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,35,112,111,105,110,116,101,114,45,115,51,50,45,115,101,116,33,32,112,32,110,41,0,0,0};
static C_char C_TLS li32[] C_aligned={C_lihdr(0,0,37),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,35,112,111,105,110,116,101,114,45,117,54,52,45,115,101,116,33,32,112,32,110,41,0,0,0};
static C_char C_TLS li33[] C_aligned={C_lihdr(0,0,37),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,35,112,111,105,110,116,101,114,45,115,54,52,45,115,101,116,33,32,112,32,110,41,0,0,0};
static C_char C_TLS li34[] C_aligned={C_lihdr(0,0,37),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,35,112,111,105,110,116,101,114,45,102,51,50,45,115,101,116,33,32,112,32,110,41,0,0,0};
static C_char C_TLS li35[] C_aligned={C_lihdr(0,0,37),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,35,112,111,105,110,116,101,114,45,102,54,52,45,115,101,116,33,32,112,32,110,41,0,0,0};
static C_char C_TLS li36[] C_aligned={C_lihdr(0,0,11),40,100,111,108,111,111,112,52,57,54,41,0,0,0,0,0};
static C_char C_TLS li37[] C_aligned={C_lihdr(0,0,45),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,35,109,97,107,101,45,112,111,105,110,116,101,114,45,118,101,99,116,111,114,32,110,32,46,32,114,101,115,116,41,0,0,0};
static C_char C_TLS li38[] C_aligned={C_lihdr(0,0,34),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,35,112,111,105,110,116,101,114,45,118,101,99,116,111,114,63,32,120,41,0,0,0,0,0,0};
static C_char C_TLS li39[] C_aligned={C_lihdr(0,0,18),40,100,111,108,111,111,112,53,49,49,32,112,116,114,115,32,105,41,0,0,0,0,0,0};
static C_char C_TLS li40[] C_aligned={C_lihdr(0,0,38),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,35,112,111,105,110,116,101,114,45,118,101,99,116,111,114,32,46,32,112,116,114,115,41,0,0};
static C_char C_TLS li41[] C_aligned={C_lihdr(0,0,11),40,100,111,108,111,111,112,53,50,52,41,0,0,0,0,0};
static C_char C_TLS li42[] C_aligned={C_lihdr(0,0,44),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,35,112,111,105,110,116,101,114,45,118,101,99,116,111,114,45,102,105,108,108,33,32,112,118,32,112,116,114,41,0,0,0,0};
static C_char C_TLS li43[] C_aligned={C_lihdr(0,0,45),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,35,112,111,105,110,116,101,114,45,118,101,99,116,111,114,45,115,101,116,33,32,112,118,32,105,32,112,116,114,41,0,0,0};
static C_char C_TLS li44[] C_aligned={C_lihdr(0,0,41),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,35,112,111,105,110,116,101,114,45,118,101,99,116,111,114,45,108,101,110,103,116,104,32,112,118,41,0,0,0,0,0,0,0};
static C_char C_TLS li45[] C_aligned={C_lihdr(0,0,13),40,100,111,108,111,111,112,54,50,57,32,105,41,0,0,0};
static C_char C_TLS li46[] C_aligned={C_lihdr(0,0,8),40,99,111,112,121,32,120,41};
static C_char C_TLS li47[] C_aligned={C_lihdr(0,0,45),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,46,114,101,112,114,101,115,101,110,116,97,116,105,111,110,35,111,98,106,101,99,116,45,99,111,112,121,32,120,41,0,0,0};
static C_char C_TLS li48[] C_aligned={C_lihdr(0,0,9),40,97,50,49,50,55,32,120,41,0,0,0,0,0,0,0};
static C_char C_TLS li49[] C_aligned={C_lihdr(0,0,11),40,97,50,49,52,51,32,120,32,105,41,0,0,0,0,0};
static C_char C_TLS li50[] C_aligned={C_lihdr(0,0,58),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,46,114,101,112,114,101,115,101,110,116,97,116,105,111,110,35,101,120,116,101,110,100,45,112,114,111,99,101,100,117,114,101,32,112,114,111,99,32,100,97,116,97,41,0,0,0,0,0,0};
static C_char C_TLS li51[] C_aligned={C_lihdr(0,0,9),40,97,50,49,55,48,32,120,41,0,0,0,0,0,0,0};
static C_char C_TLS li52[] C_aligned={C_lihdr(0,0,53),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,46,114,101,112,114,101,115,101,110,116,97,116,105,111,110,35,101,120,116,101,110,100,101,100,45,112,114,111,99,101,100,117,114,101,63,32,120,41,0,0,0};
static C_char C_TLS li53[] C_aligned={C_lihdr(0,0,9),40,97,50,50,48,51,32,120,41,0,0,0,0,0,0,0};
static C_char C_TLS li54[] C_aligned={C_lihdr(0,0,48),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,46,114,101,112,114,101,115,101,110,116,97,116,105,111,110,35,112,114,111,99,101,100,117,114,101,45,100,97,116,97,32,120,41};
static C_char C_TLS li55[] C_aligned={C_lihdr(0,0,58),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,46,114,101,112,114,101,115,101,110,116,97,116,105,111,110,35,115,101,116,45,112,114,111,99,101,100,117,114,101,45,100,97,116,97,33,32,112,114,111,99,32,120,41,0,0,0,0,0,0};
static C_char C_TLS li56[] C_aligned={C_lihdr(0,0,46),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,46,114,101,112,114,101,115,101,110,116,97,116,105,111,110,35,118,101,99,116,111,114,45,108,105,107,101,63,32,120,41,0,0};
static C_char C_TLS li57[] C_aligned={C_lihdr(0,0,49),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,46,114,101,112,114,101,115,101,110,116,97,116,105,111,110,35,110,117,109,98,101,114,45,111,102,45,115,108,111,116,115,32,120,41,0,0,0,0,0,0,0};
static C_char C_TLS li58[] C_aligned={C_lihdr(0,0,49),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,46,114,101,112,114,101,115,101,110,116,97,116,105,111,110,35,110,117,109,98,101,114,45,111,102,45,98,121,116,101,115,32,120,41,0,0,0,0,0,0,0};
static C_char C_TLS li59[] C_aligned={C_lihdr(0,0,64),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,46,114,101,112,114,101,115,101,110,116,97,116,105,111,110,35,109,97,107,101,45,114,101,99,111,114,100,45,105,110,115,116,97,110,99,101,32,116,121,112,101,32,46,32,97,114,103,115,41};
static C_char C_TLS li60[] C_aligned={C_lihdr(0,0,57),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,46,114,101,112,114,101,115,101,110,116,97,116,105,111,110,35,114,101,99,111,114,100,45,105,110,115,116,97,110,99,101,63,32,120,32,46,32,114,101,115,116,41,0,0,0,0,0,0,0};
static C_char C_TLS li61[] C_aligned={C_lihdr(0,0,54),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,46,114,101,112,114,101,115,101,110,116,97,116,105,111,110,35,114,101,99,111,114,100,45,105,110,115,116,97,110,99,101,45,116,121,112,101,32,120,41,0,0};
static C_char C_TLS li62[] C_aligned={C_lihdr(0,0,56),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,46,114,101,112,114,101,115,101,110,116,97,116,105,111,110,35,114,101,99,111,114,100,45,105,110,115,116,97,110,99,101,45,108,101,110,103,116,104,32,120,41};
static C_char C_TLS li63[] C_aligned={C_lihdr(0,0,63),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,46,114,101,112,114,101,115,101,110,116,97,116,105,111,110,35,114,101,99,111,114,100,45,105,110,115,116,97,110,99,101,45,115,108,111,116,45,115,101,116,33,32,120,32,105,32,121,41,0};
static C_char C_TLS li64[] C_aligned={C_lihdr(0,0,11),40,100,111,108,111,111,112,55,51,53,41,0,0,0,0,0};
static C_char C_TLS li65[] C_aligned={C_lihdr(0,0,48),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,46,114,101,112,114,101,115,101,110,116,97,116,105,111,110,35,114,101,99,111,114,100,45,62,118,101,99,116,111,114,32,120,41};
static C_char C_TLS li66[] C_aligned={C_lihdr(0,0,51),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,46,114,101,112,114,101,115,101,110,116,97,116,105,111,110,35,111,98,106,101,99,116,45,98,101,99,111,109,101,33,32,97,108,115,116,41,0,0,0,0,0};
static C_char C_TLS li67[] C_aligned={C_lihdr(0,0,58),40,99,104,105,99,107,101,110,46,109,101,109,111,114,121,46,114,101,112,114,101,115,101,110,116,97,116,105,111,110,35,109,117,116,97,116,101,45,112,114,111,99,101,100,117,114,101,33,32,111,108,100,32,112,114,111,99,41,0,0,0,0,0,0};
static C_char C_TLS li68[] C_aligned={C_lihdr(0,0,44),40,99,104,105,99,107,101,110,46,108,111,99,97,116,105,118,101,35,109,97,107,101,45,108,111,99,97,116,105,118,101,32,111,98,106,32,46,32,105,110,100,101,120,41,0,0,0,0};
static C_char C_TLS li69[] C_aligned={C_lihdr(0,0,49),40,99,104,105,99,107,101,110,46,108,111,99,97,116,105,118,101,35,109,97,107,101,45,119,101,97,107,45,108,111,99,97,116,105,118,101,32,111,98,106,32,46,32,105,110,100,101,120,41,0,0,0,0,0,0,0};
static C_char C_TLS li70[] C_aligned={C_lihdr(0,0,36),40,99,104,105,99,107,101,110,46,108,111,99,97,116,105,118,101,35,108,111,99,97,116,105,118,101,45,115,101,116,33,32,120,32,121,41,0,0,0,0};
static C_char C_TLS li71[] C_aligned={C_lihdr(0,0,37),40,99,104,105,99,107,101,110,46,108,111,99,97,116,105,118,101,35,108,111,99,97,116,105,118,101,45,62,111,98,106,101,99,116,32,120,41,0,0,0};
static C_char C_TLS li72[] C_aligned={C_lihdr(0,0,30),40,99,104,105,99,107,101,110,46,108,111,99,97,116,105,118,101,35,108,111,99,97,116,105,118,101,63,32,120,41,0,0};
static C_char C_TLS li73[] C_aligned={C_lihdr(0,0,11),40,97,50,53,50,48,32,108,111,99,41,0,0,0,0,0};
static C_char C_TLS li74[] C_aligned={C_lihdr(0,0,11),40,97,50,53,50,51,32,120,32,105,41,0,0,0,0,0};
static C_char C_TLS li75[] C_aligned={C_lihdr(0,0,12),40,97,50,53,52,55,32,112,118,32,105,41,0,0,0,0};
static C_char C_TLS li76[] C_aligned={C_lihdr(0,0,9),40,97,50,53,54,55,32,112,41,0,0,0,0,0,0,0};
static C_char C_TLS li77[] C_aligned={C_lihdr(0,0,9),40,97,50,53,55,48,32,112,41,0,0,0,0,0,0,0};
static C_char C_TLS li78[] C_aligned={C_lihdr(0,0,9),40,97,50,53,55,51,32,112,41,0,0,0,0,0,0,0};
static C_char C_TLS li79[] C_aligned={C_lihdr(0,0,9),40,97,50,53,55,54,32,112,41,0,0,0,0,0,0,0};
static C_char C_TLS li80[] C_aligned={C_lihdr(0,0,9),40,97,50,53,55,57,32,112,41,0,0,0,0,0,0,0};
static C_char C_TLS li81[] C_aligned={C_lihdr(0,0,9),40,97,50,53,56,50,32,112,41,0,0,0,0,0,0,0};
static C_char C_TLS li82[] C_aligned={C_lihdr(0,0,9),40,97,50,53,56,53,32,112,41,0,0,0,0,0,0,0};
static C_char C_TLS li83[] C_aligned={C_lihdr(0,0,9),40,97,50,53,56,56,32,112,41,0,0,0,0,0,0,0};
static C_char C_TLS li84[] C_aligned={C_lihdr(0,0,9),40,97,50,53,57,49,32,112,41,0,0,0,0,0,0,0};
static C_char C_TLS li85[] C_aligned={C_lihdr(0,0,9),40,97,50,53,57,52,32,112,41,0,0,0,0,0,0,0};
static C_char C_TLS li86[] C_aligned={C_lihdr(0,0,10),40,116,111,112,108,101,118,101,108,41,0,0,0,0,0,0};


#define return(x) C_cblock C_r = (((C_word)(x))); goto C_ret; C_cblockend
C_regparm static C_word C_fcall stub543(C_word C_buf,C_word C_a0,C_word C_a1,C_word C_a2){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
C_word buf=(C_word )(C_a0);
unsigned int i=(unsigned int )C_unfix(C_a1);
void * ptr=(void * )C_c_pointer_or_null(C_a2);
*((void **)C_data_pointer(buf) + i) = ptr;
C_ret:
#undef return

return C_r;}

#define return(x) C_cblock C_r = (C_mpointer_or_false(&C_a,(void*)(x))); goto C_ret; C_cblockend
C_regparm static C_word C_fcall stub534(C_word C_buf,C_word C_a0,C_word C_a1){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
C_word buf=(C_word )(C_a0);
unsigned int i=(unsigned int )C_unfix(C_a1);
C_return(*((void **)C_data_pointer(buf) + i));
C_ret:
#undef return

return C_r;}

/* from k1840 */
C_regparm static C_word C_fcall stub491(C_word C_buf,C_word C_a0){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int t0=(int )C_unfix(C_a0);
C_r=C_fix((C_word)C_wordstobytes(t0));
return C_r;}

/* from k1641 */
C_regparm static C_word C_fcall stub377(C_word C_buf,C_word C_a0){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int t0=(int )C_num_to_int(C_a0);
C_r=C_int_to_num(&C_a,C_align(t0));
return C_r;}

#define return(x) C_cblock C_r = (C_mpointer(&C_a,(void*)(x))); goto C_ret; C_cblockend
C_regparm static C_word C_fcall stub366(C_word C_buf,C_word C_a0,C_word C_a1){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
void * ptr=(void * )C_c_pointer_or_null(C_a0);
int off=(int )C_num_to_int(C_a1);
C_return((unsigned char *)ptr + off);
C_ret:
#undef return

return C_r;}

#define return(x) C_cblock C_r = (C_mpointer(&C_a,(void*)(x))); goto C_ret; C_cblockend
C_regparm static C_word C_fcall stub351(C_word C_buf,C_word C_a0){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
C_word x=(C_word )(C_a0);
C_return((void *)x);
C_ret:
#undef return

return C_r;}

/* from k1553 */
C_regparm static C_word C_fcall stub323(C_word C_buf,C_word C_a0){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
void * t0=(void * )C_c_pointer_or_null(C_a0);
C_free(t0);
return C_r;}

/* from k1546 */
C_regparm static C_word C_fcall stub317(C_word C_buf,C_word C_a0){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int t0=(int )C_unfix(C_a0);
C_r=C_mpointer_or_false(&C_a,(void*)C_malloc(t0));
return C_r;}

/* from k1185 */
C_regparm static C_word C_fcall stub196(C_word C_buf,C_word C_a0,C_word C_a1,C_word C_a2,C_word C_a3,C_word C_a4){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
void * t0=(void * )C_data_pointer_or_null(C_a0);
void * t1=(void * )C_data_pointer_or_null(C_a1);
int t2=(int )C_unfix(C_a2);
int t3=(int )C_unfix(C_a3);
int t4=(int )C_unfix(C_a4);
C_memmove_o(t0,t1,t2,t3,t4);
return C_r;}

/* from k1157 */
C_regparm static C_word C_fcall stub180(C_word C_buf,C_word C_a0,C_word C_a1,C_word C_a2,C_word C_a3,C_word C_a4){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
void * t0=(void * )C_data_pointer_or_null(C_a0);
void * t1=(void * )C_c_pointer_or_null(C_a1);
int t2=(int )C_unfix(C_a2);
int t3=(int )C_unfix(C_a3);
int t4=(int )C_unfix(C_a4);
C_memmove_o(t0,t1,t2,t3,t4);
return C_r;}

/* from k1129 */
C_regparm static C_word C_fcall stub164(C_word C_buf,C_word C_a0,C_word C_a1,C_word C_a2,C_word C_a3,C_word C_a4){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
void * t0=(void * )C_c_pointer_or_null(C_a0);
void * t1=(void * )C_data_pointer_or_null(C_a1);
int t2=(int )C_unfix(C_a2);
int t3=(int )C_unfix(C_a3);
int t4=(int )C_unfix(C_a4);
C_memmove_o(t0,t1,t2,t3,t4);
return C_r;}

/* from k1101 */
C_regparm static C_word C_fcall stub148(C_word C_buf,C_word C_a0,C_word C_a1,C_word C_a2,C_word C_a3,C_word C_a4){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
void * t0=(void * )C_c_pointer_or_null(C_a0);
void * t1=(void * )C_c_pointer_or_null(C_a1);
int t2=(int )C_unfix(C_a2);
int t3=(int )C_unfix(C_a3);
int t4=(int )C_unfix(C_a4);
C_memmove_o(t0,t1,t2,t3,t4);
return C_r;}

C_noret_decl(f_1002)
static void C_ccall f_1002(C_word c,C_word *av) C_noret;
C_noret_decl(f_1027)
static void C_ccall f_1027(C_word c,C_word *av) C_noret;
C_noret_decl(f_1046)
static void C_fcall f_1046(C_word t0,C_word t1) C_noret;
C_noret_decl(f_1059)
static void C_ccall f_1059(C_word c,C_word *av) C_noret;
C_noret_decl(f_1200)
static void C_ccall f_1200(C_word c,C_word *av) C_noret;
C_noret_decl(f_1221)
static void C_fcall f_1221(C_word t0,C_word t1) C_noret;
C_noret_decl(f_1227)
static void C_fcall f_1227(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_1233)
static void C_fcall f_1233(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_1249)
static void C_fcall f_1249(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6) C_noret;
C_noret_decl(f_1276)
static void C_ccall f_1276(C_word c,C_word *av) C_noret;
C_noret_decl(f_1279)
static void C_ccall f_1279(C_word c,C_word *av) C_noret;
C_noret_decl(f_1282)
static void C_ccall f_1282(C_word c,C_word *av) C_noret;
C_noret_decl(f_1285)
static void C_ccall f_1285(C_word c,C_word *av) C_noret;
C_noret_decl(f_1290)
static void C_fcall f_1290(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_1382)
static void C_ccall f_1382(C_word c,C_word *av) C_noret;
C_noret_decl(f_1391)
static void C_ccall f_1391(C_word c,C_word *av) C_noret;
C_noret_decl(f_1401)
static void C_ccall f_1401(C_word c,C_word *av) C_noret;
C_noret_decl(f_1405)
static void C_ccall f_1405(C_word c,C_word *av) C_noret;
C_noret_decl(f_1424)
static void C_ccall f_1424(C_word c,C_word *av) C_noret;
C_noret_decl(f_1453)
static void C_ccall f_1453(C_word c,C_word *av) C_noret;
C_noret_decl(f_1463)
static void C_ccall f_1463(C_word c,C_word *av) C_noret;
C_noret_decl(f_1473)
static void C_ccall f_1473(C_word c,C_word *av) C_noret;
C_noret_decl(f_1543)
static void C_ccall f_1543(C_word c,C_word *av) C_noret;
C_noret_decl(f_1550)
static void C_ccall f_1550(C_word c,C_word *av) C_noret;
C_noret_decl(f_1560)
static void C_ccall f_1560(C_word c,C_word *av) C_noret;
C_noret_decl(f_1568)
static void C_ccall f_1568(C_word c,C_word *av) C_noret;
C_noret_decl(f_1579)
static void C_ccall f_1579(C_word c,C_word *av) C_noret;
C_noret_decl(f_1583)
static void C_ccall f_1583(C_word c,C_word *av) C_noret;
C_noret_decl(f_1588)
static void C_ccall f_1588(C_word c,C_word *av) C_noret;
C_noret_decl(f_1592)
static void C_ccall f_1592(C_word c,C_word *av) C_noret;
C_noret_decl(f_1597)
static void C_ccall f_1597(C_word c,C_word *av) C_noret;
C_noret_decl(f_1608)
static void C_ccall f_1608(C_word c,C_word *av) C_noret;
C_noret_decl(f_1612)
static void C_ccall f_1612(C_word c,C_word *av) C_noret;
C_noret_decl(f_1614)
static void C_ccall f_1614(C_word c,C_word *av) C_noret;
C_noret_decl(f_1618)
static void C_ccall f_1618(C_word c,C_word *av) C_noret;
C_noret_decl(f_1621)
static void C_ccall f_1621(C_word c,C_word *av) C_noret;
C_noret_decl(f_1623)
static void C_ccall f_1623(C_word c,C_word *av) C_noret;
C_noret_decl(f_1645)
static void C_ccall f_1645(C_word c,C_word *av) C_noret;
C_noret_decl(f_1675)
static void C_ccall f_1675(C_word c,C_word *av) C_noret;
C_noret_decl(f_1680)
static void C_ccall f_1680(C_word c,C_word *av) C_noret;
C_noret_decl(f_1684)
static void C_ccall f_1684(C_word c,C_word *av) C_noret;
C_noret_decl(f_1687)
static void C_ccall f_1687(C_word c,C_word *av) C_noret;
C_noret_decl(f_1701)
static void C_ccall f_1701(C_word c,C_word *av) C_noret;
C_noret_decl(f_1732)
static void C_ccall f_1732(C_word c,C_word *av) C_noret;
C_noret_decl(f_1756)
static void C_ccall f_1756(C_word c,C_word *av) C_noret;
C_noret_decl(f_1759)
static void C_ccall f_1759(C_word c,C_word *av) C_noret;
C_noret_decl(f_1762)
static void C_ccall f_1762(C_word c,C_word *av) C_noret;
C_noret_decl(f_1765)
static void C_ccall f_1765(C_word c,C_word *av) C_noret;
C_noret_decl(f_1768)
static void C_ccall f_1768(C_word c,C_word *av) C_noret;
C_noret_decl(f_1771)
static void C_ccall f_1771(C_word c,C_word *av) C_noret;
C_noret_decl(f_1774)
static void C_ccall f_1774(C_word c,C_word *av) C_noret;
C_noret_decl(f_1777)
static void C_ccall f_1777(C_word c,C_word *av) C_noret;
C_noret_decl(f_1780)
static void C_ccall f_1780(C_word c,C_word *av) C_noret;
C_noret_decl(f_1783)
static void C_ccall f_1783(C_word c,C_word *av) C_noret;
C_noret_decl(f_1788)
static void C_ccall f_1788(C_word c,C_word *av) C_noret;
C_noret_decl(f_1792)
static void C_ccall f_1792(C_word c,C_word *av) C_noret;
C_noret_decl(f_1796)
static void C_ccall f_1796(C_word c,C_word *av) C_noret;
C_noret_decl(f_1800)
static void C_ccall f_1800(C_word c,C_word *av) C_noret;
C_noret_decl(f_1804)
static void C_ccall f_1804(C_word c,C_word *av) C_noret;
C_noret_decl(f_1808)
static void C_ccall f_1808(C_word c,C_word *av) C_noret;
C_noret_decl(f_1812)
static void C_ccall f_1812(C_word c,C_word *av) C_noret;
C_noret_decl(f_1816)
static void C_ccall f_1816(C_word c,C_word *av) C_noret;
C_noret_decl(f_1820)
static void C_ccall f_1820(C_word c,C_word *av) C_noret;
C_noret_decl(f_1824)
static void C_ccall f_1824(C_word c,C_word *av) C_noret;
C_noret_decl(f_1829)
static void C_ccall f_1829(C_word c,C_word *av) C_noret;
C_noret_decl(f_1836)
static void C_ccall f_1836(C_word c,C_word *av) C_noret;
C_noret_decl(f_1848)
static void C_ccall f_1848(C_word c,C_word *av) C_noret;
C_noret_decl(f_1860)
static void C_ccall f_1860(C_word c,C_word *av) C_noret;
C_noret_decl(f_1865)
static C_word C_fcall f_1865(C_word t0,C_word t1);
C_noret_decl(f_1893)
static void C_ccall f_1893(C_word c,C_word *av) C_noret;
C_noret_decl(f_1899)
static void C_ccall f_1899(C_word c,C_word *av) C_noret;
C_noret_decl(f_1904)
static void C_ccall f_1904(C_word c,C_word *av) C_noret;
C_noret_decl(f_1912)
static void C_fcall f_1912(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_1925)
static void C_ccall f_1925(C_word c,C_word *av) C_noret;
C_noret_decl(f_1939)
static void C_ccall f_1939(C_word c,C_word *av) C_noret;
C_noret_decl(f_1946)
static void C_ccall f_1946(C_word c,C_word *av) C_noret;
C_noret_decl(f_1957)
static C_word C_fcall f_1957(C_word t0,C_word t1);
C_noret_decl(f_2000)
static void C_ccall f_2000(C_word c,C_word *av) C_noret;
C_noret_decl(f_2007)
static void C_ccall f_2007(C_word c,C_word *av) C_noret;
C_noret_decl(f_2010)
static void C_ccall f_2010(C_word c,C_word *av) C_noret;
C_noret_decl(f_2028)
static void C_ccall f_2028(C_word c,C_word *av) C_noret;
C_noret_decl(f_2030)
static void C_ccall f_2030(C_word c,C_word *av) C_noret;
C_noret_decl(f_2039)
static void C_ccall f_2039(C_word c,C_word *av) C_noret;
C_noret_decl(f_2045)
static void C_fcall f_2045(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_2075)
static void C_ccall f_2075(C_word c,C_word *av) C_noret;
C_noret_decl(f_2078)
static void C_ccall f_2078(C_word c,C_word *av) C_noret;
C_noret_decl(f_2087)
static void C_fcall f_2087(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_2108)
static void C_ccall f_2108(C_word c,C_word *av) C_noret;
C_noret_decl(f_2119)
static void C_ccall f_2119(C_word c,C_word *av) C_noret;
C_noret_decl(f_2123)
static void C_ccall f_2123(C_word c,C_word *av) C_noret;
C_noret_decl(f_2128)
static void C_ccall f_2128(C_word c,C_word *av) C_noret;
C_noret_decl(f_2144)
static void C_ccall f_2144(C_word c,C_word *av) C_noret;
C_noret_decl(f_2154)
static void C_ccall f_2154(C_word c,C_word *av) C_noret;
C_noret_decl(f_2171)
static void C_ccall f_2171(C_word c,C_word *av) C_noret;
C_noret_decl(f_2188)
static void C_ccall f_2188(C_word c,C_word *av) C_noret;
C_noret_decl(f_2190)
static void C_ccall f_2190(C_word c,C_word *av) C_noret;
C_noret_decl(f_2204)
static void C_ccall f_2204(C_word c,C_word *av) C_noret;
C_noret_decl(f_2221)
static void C_ccall f_2221(C_word c,C_word *av) C_noret;
C_noret_decl(f_2229)
static void C_ccall f_2229(C_word c,C_word *av) C_noret;
C_noret_decl(f_2243)
static void C_ccall f_2243(C_word c,C_word *av) C_noret;
C_noret_decl(f_2245)
static void C_ccall f_2245(C_word c,C_word *av) C_noret;
C_noret_decl(f_2266)
static void C_ccall f_2266(C_word c,C_word *av) C_noret;
C_noret_decl(f_2268)
static void C_ccall f_2268(C_word c,C_word *av) C_noret;
C_noret_decl(f_2272)
static void C_ccall f_2272(C_word c,C_word *av) C_noret;
C_noret_decl(f_2277)
static void C_ccall f_2277(C_word c,C_word *av) C_noret;
C_noret_decl(f_2300)
static void C_ccall f_2300(C_word c,C_word *av) C_noret;
C_noret_decl(f_2309)
static void C_ccall f_2309(C_word c,C_word *av) C_noret;
C_noret_decl(f_2345)
static void C_ccall f_2345(C_word c,C_word *av) C_noret;
C_noret_decl(f_2349)
static void C_ccall f_2349(C_word c,C_word *av) C_noret;
C_noret_decl(f_2354)
static void C_ccall f_2354(C_word c,C_word *av) C_noret;
C_noret_decl(f_2358)
static void C_ccall f_2358(C_word c,C_word *av) C_noret;
C_noret_decl(f_2367)
static void C_ccall f_2367(C_word c,C_word *av) C_noret;
C_noret_decl(f_2371)
static void C_ccall f_2371(C_word c,C_word *av) C_noret;
C_noret_decl(f_2374)
static void C_ccall f_2374(C_word c,C_word *av) C_noret;
C_noret_decl(f_2393)
static void C_ccall f_2393(C_word c,C_word *av) C_noret;
C_noret_decl(f_2395)
static void C_ccall f_2395(C_word c,C_word *av) C_noret;
C_noret_decl(f_2399)
static void C_ccall f_2399(C_word c,C_word *av) C_noret;
C_noret_decl(f_2405)
static void C_ccall f_2405(C_word c,C_word *av) C_noret;
C_noret_decl(f_2410)
static C_word C_fcall f_2410(C_word t0,C_word t1);
C_noret_decl(f_2433)
static void C_ccall f_2433(C_word c,C_word *av) C_noret;
C_noret_decl(f_2437)
static void C_ccall f_2437(C_word c,C_word *av) C_noret;
C_noret_decl(f_2442)
static void C_ccall f_2442(C_word c,C_word *av) C_noret;
C_noret_decl(f_2446)
static void C_ccall f_2446(C_word c,C_word *av) C_noret;
C_noret_decl(f_2449)
static void C_ccall f_2449(C_word c,C_word *av) C_noret;
C_noret_decl(f_2456)
static void C_ccall f_2456(C_word c,C_word *av) C_noret;
C_noret_decl(f_2459)
static void C_ccall f_2459(C_word c,C_word *av) C_noret;
C_noret_decl(f_2471)
static void C_ccall f_2471(C_word c,C_word *av) C_noret;
C_noret_decl(f_2473)
static void C_ccall f_2473(C_word c,C_word *av) C_noret;
C_noret_decl(f_2489)
static void C_ccall f_2489(C_word c,C_word *av) C_noret;
C_noret_decl(f_2505)
static void C_ccall f_2505(C_word c,C_word *av) C_noret;
C_noret_decl(f_2510)
static void C_ccall f_2510(C_word c,C_word *av) C_noret;
C_noret_decl(f_2512)
static void C_ccall f_2512(C_word c,C_word *av) C_noret;
C_noret_decl(f_2515)
static void C_ccall f_2515(C_word c,C_word *av) C_noret;
C_noret_decl(f_2521)
static void C_ccall f_2521(C_word c,C_word *av) C_noret;
C_noret_decl(f_2524)
static void C_ccall f_2524(C_word c,C_word *av) C_noret;
C_noret_decl(f_2528)
static void C_ccall f_2528(C_word c,C_word *av) C_noret;
C_noret_decl(f_2531)
static void C_ccall f_2531(C_word c,C_word *av) C_noret;
C_noret_decl(f_2548)
static void C_ccall f_2548(C_word c,C_word *av) C_noret;
C_noret_decl(f_2555)
static void C_ccall f_2555(C_word c,C_word *av) C_noret;
C_noret_decl(f_2568)
static void C_ccall f_2568(C_word c,C_word *av) C_noret;
C_noret_decl(f_2571)
static void C_ccall f_2571(C_word c,C_word *av) C_noret;
C_noret_decl(f_2574)
static void C_ccall f_2574(C_word c,C_word *av) C_noret;
C_noret_decl(f_2577)
static void C_ccall f_2577(C_word c,C_word *av) C_noret;
C_noret_decl(f_2580)
static void C_ccall f_2580(C_word c,C_word *av) C_noret;
C_noret_decl(f_2583)
static void C_ccall f_2583(C_word c,C_word *av) C_noret;
C_noret_decl(f_2586)
static void C_ccall f_2586(C_word c,C_word *av) C_noret;
C_noret_decl(f_2589)
static void C_ccall f_2589(C_word c,C_word *av) C_noret;
C_noret_decl(f_2592)
static void C_ccall f_2592(C_word c,C_word *av) C_noret;
C_noret_decl(f_2595)
static void C_ccall f_2595(C_word c,C_word *av) C_noret;
C_noret_decl(f_936)
static void C_ccall f_936(C_word c,C_word *av) C_noret;
C_noret_decl(f_938)
static void C_ccall f_938(C_word c,C_word *av) C_noret;
C_noret_decl(f_956)
static void C_ccall f_956(C_word c,C_word *av) C_noret;
C_noret_decl(f_965)
static void C_fcall f_965(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_985)
static void C_ccall f_985(C_word c,C_word *av) C_noret;
C_noret_decl(f_988)
static void C_ccall f_988(C_word c,C_word *av) C_noret;
C_noret_decl(C_lolevel_toplevel)
C_externexport void C_ccall C_lolevel_toplevel(C_word c,C_word *av) C_noret;

C_noret_decl(trf_1046)
static void C_ccall trf_1046(C_word c,C_word *av) C_noret;
static void C_ccall trf_1046(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_1046(t0,t1);}

C_noret_decl(trf_1221)
static void C_ccall trf_1221(C_word c,C_word *av) C_noret;
static void C_ccall trf_1221(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_1221(t0,t1);}

C_noret_decl(trf_1227)
static void C_ccall trf_1227(C_word c,C_word *av) C_noret;
static void C_ccall trf_1227(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_1227(t0,t1,t2);}

C_noret_decl(trf_1233)
static void C_ccall trf_1233(C_word c,C_word *av) C_noret;
static void C_ccall trf_1233(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_1233(t0,t1,t2,t3,t4);}

C_noret_decl(trf_1249)
static void C_ccall trf_1249(C_word c,C_word *av) C_noret;
static void C_ccall trf_1249(C_word c,C_word *av){
C_word t0=av[6];
C_word t1=av[5];
C_word t2=av[4];
C_word t3=av[3];
C_word t4=av[2];
C_word t5=av[1];
C_word t6=av[0];
f_1249(t0,t1,t2,t3,t4,t5,t6);}

C_noret_decl(trf_1290)
static void C_ccall trf_1290(C_word c,C_word *av) C_noret;
static void C_ccall trf_1290(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_1290(t0,t1,t2,t3);}

C_noret_decl(trf_1912)
static void C_ccall trf_1912(C_word c,C_word *av) C_noret;
static void C_ccall trf_1912(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_1912(t0,t1,t2,t3);}

C_noret_decl(trf_2045)
static void C_ccall trf_2045(C_word c,C_word *av) C_noret;
static void C_ccall trf_2045(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_2045(t0,t1,t2);}

C_noret_decl(trf_2087)
static void C_ccall trf_2087(C_word c,C_word *av) C_noret;
static void C_ccall trf_2087(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_2087(t0,t1,t2);}

C_noret_decl(trf_965)
static void C_ccall trf_965(C_word c,C_word *av) C_noret;
static void C_ccall trf_965(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_965(t0,t1,t2);}

/* ##sys#check-generic-structure in k934 */
static void C_ccall f_1002(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +0,c,5)))){
C_save_and_reclaim((void*)f_1002,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+0);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
C_word t6;
t4=(C_truep(C_blockp(t2))?C_structurep(t2):C_SCHEME_FALSE);
if(C_truep(t4)){
t5=C_SCHEME_UNDEFINED;
t6=t1;{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
if(C_truep(C_i_pairp(t3))){
/* lolevel.scm:105: ##sys#signal-hook */
t5=*((C_word*)lf[5]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t5;
av2[1]=t1;
av2[2]=lf[6];
av2[3]=C_get_rest_arg(c,3,av,3,t0);
av2[4]=lf[9];
av2[5]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(6,av2);}}
else{
/* lolevel.scm:105: ##sys#signal-hook */
t5=*((C_word*)lf[5]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t5;
av2[1]=t1;
av2[2]=lf[6];
av2[3]=C_SCHEME_FALSE;
av2[4]=lf[9];
av2[5]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(6,av2);}}}}

/* ##sys#check-generic-vector in k934 */
static void C_ccall f_1027(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +5,c,2)))){
C_save_and_reclaim((void*)f_1027,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+5);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
C_word t6;
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1046,a[2]=t1,a[3]=t3,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_blockp(t2))){
t5=C_specialp(t2);
t6=t4;
f_1046(t6,(C_truep(t5)?C_i_not(t5):C_i_not(C_byteblockp(t2))));}
else{
t5=t4;
f_1046(t5,C_SCHEME_FALSE);}}

/* k1044 in ##sys#check-generic-vector in k934 */
static void C_fcall f_1046(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,5)))){
C_save_and_reclaim_args((void *)trf_1046,2,t0,t1);}
if(C_truep(t1)){
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
if(C_truep(C_i_pairp(((C_word*)t0)[3]))){
/* lolevel.scm:113: ##sys#signal-hook */
t2=*((C_word*)lf[5]+1);{
C_word av2[6];
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[6];
av2[3]=C_u_i_car(((C_word*)t0)[3]);
av2[4]=lf[11];
av2[5]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}
else{
/* lolevel.scm:113: ##sys#signal-hook */
t2=*((C_word*)lf[5]+1);{
C_word av2[6];
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[6];
av2[3]=C_SCHEME_FALSE;
av2[4]=lf[11];
av2[5]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}}}

/* ##sys#check-pointer in k934 */
static void C_ccall f_1059(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +0,c,5)))){
C_save_and_reclaim((void*)f_1059,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+0);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
if(C_truep(C_i_safe_pointerp(t2))){
t4=C_SCHEME_UNDEFINED;
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t4=C_fix((C_word)C_BAD_ARGUMENT_TYPE_NO_POINTER_ERROR);
if(C_truep(C_i_pairp(t3))){
/* lolevel.scm:119: ##sys#error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[3]+1));
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=*((C_word*)lf[3]+1);
av2[1]=t1;
av2[2]=t4;
av2[3]=C_get_rest_arg(c,3,av,3,t0);
av2[4]=lf[13];
av2[5]=t2;
tp(6,av2);}}
else{
/* lolevel.scm:119: ##sys#error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[3]+1));
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=*((C_word*)lf[3]+1);
av2[1]=t1;
av2[2]=t4;
av2[3]=C_SCHEME_FALSE;
av2[4]=lf[13];
av2[5]=t2;
tp(6,av2);}}}}

/* chicken.memory#move-memory! in k934 */
static void C_ccall f_1200(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word t24;
C_word t25;
C_word t26;
C_word *a;
if(c<4) C_bad_min_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(38,c,7)))){
C_save_and_reclaim((void *)f_1200,c,av);}
a=C_alloc(38);
t4=C_rest_nullp(c,4);
t5=(C_truep(t4)?C_SCHEME_FALSE:C_get_rest_arg(c,4,av,4,t0));
t6=C_rest_nullp(c,4);
t7=C_rest_nullp(c,5);
t8=(C_truep(t7)?C_fix(0):C_get_rest_arg(c,5,av,4,t0));
t9=C_rest_nullp(c,5);
t10=C_rest_nullp(c,6);
t11=(C_truep(t10)?C_fix(0):C_get_rest_arg(c,6,av,4,t0));
t12=C_rest_nullp(c,6);
t13=C_SCHEME_UNDEFINED;
t14=(*a=C_VECTOR_TYPE|1,a[1]=t13,tmp=(C_word)a,a+=2,tmp);
t15=C_SCHEME_UNDEFINED;
t16=(*a=C_VECTOR_TYPE|1,a[1]=t15,tmp=(C_word)a,a+=2,tmp);
t17=C_SCHEME_UNDEFINED;
t18=(*a=C_VECTOR_TYPE|1,a[1]=t17,tmp=(C_word)a,a+=2,tmp);
t19=C_SCHEME_UNDEFINED;
t20=(*a=C_VECTOR_TYPE|1,a[1]=t19,tmp=(C_word)a,a+=2,tmp);
t21=C_set_block_item(t14,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1221,a[2]=t2,a[3]=t3,a[4]=((C_word)li6),tmp=(C_word)a,a+=5,tmp));
t22=C_set_block_item(t16,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1227,a[2]=t2,a[3]=t3,a[4]=((C_word)li7),tmp=(C_word)a,a+=5,tmp));
t23=C_set_block_item(t18,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1233,a[2]=t16,a[3]=((C_word)li8),tmp=(C_word)a,a+=4,tmp));
t24=C_set_block_item(t20,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1249,a[2]=t16,a[3]=((C_word)li9),tmp=(C_word)a,a+=4,tmp));
t25=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_1276,a[2]=((C_word*)t0)[2],a[3]=t11,a[4]=t8,a[5]=t5,a[6]=t14,a[7]=t18,a[8]=t20,a[9]=t1,a[10]=t2,a[11]=t3,tmp=(C_word)a,a+=12,tmp);
/* lolevel.scm:158: ##sys#check-block */
t26=*((C_word*)lf[2]+1);{
C_word *av2=av;
av2[0]=t26;
av2[1]=t25;
av2[2]=t2;
av2[3]=lf[17];
((C_proc)(void*)(*((C_word*)t26+1)))(4,av2);}}

/* nosizerr in chicken.memory#move-memory! in k934 */
static void C_fcall f_1221(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,5)))){
C_save_and_reclaim_args((void *)trf_1221,2,t0,t1);}
/* lolevel.scm:143: ##sys#error */
t2=*((C_word*)lf[16]+1);{
C_word av2[6];
av2[0]=t2;
av2[1]=t1;
av2[2]=lf[17];
av2[3]=lf[18];
av2[4]=((C_word*)t0)[2];
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* sizerr in chicken.memory#move-memory! in k934 */
static void C_fcall f_1227(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,7)))){
C_save_and_reclaim_args((void *)trf_1227,3,t0,t1,t2);}{
C_word av2[8];
av2[0]=0;
av2[1]=t1;
av2[2]=*((C_word*)lf[16]+1);
av2[3]=lf[17];
av2[4]=lf[19];
av2[5]=((C_word*)t0)[2];
av2[6]=((C_word*)t0)[3];
av2[7]=t2;
C_apply(8,av2);}}

/* checkn1 in chicken.memory#move-memory! in k934 */
static void C_fcall f_1233(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_1233,5,t0,t1,t2,t3,t4);}
a=C_alloc(6);
t5=C_fixnum_difference(t3,t4);
if(C_truep(C_fixnum_less_or_equal_p(t2,t5))){
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
/* lolevel.scm:151: sizerr */
t6=((C_word*)((C_word*)t0)[2])[1];
f_1227(t6,t1,C_a_i_list(&a,2,t2,t3));}}

/* checkn2 in chicken.memory#move-memory! in k934 */
static void C_fcall f_1249(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6){
C_word tmp;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,2)))){
C_save_and_reclaim_args((void *)trf_1249,7,t0,t1,t2,t3,t4,t5,t6);}
a=C_alloc(9);
t7=C_fixnum_difference(t3,t5);
t8=C_fixnum_less_or_equal_p(t2,t7);
t9=(C_truep(t8)?C_fixnum_less_or_equal_p(t2,C_fixnum_difference(t4,t6)):C_SCHEME_FALSE);
if(C_truep(t9)){
t10=t1;{
C_word av2[2];
av2[0]=t10;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t10+1)))(2,av2);}}
else{
/* lolevel.scm:156: sizerr */
t10=((C_word*)((C_word*)t0)[2])[1];
f_1227(t10,t1,C_a_i_list(&a,3,t2,t3,t4));}}

/* k1274 in chicken.memory#move-memory! in k934 */
static void C_ccall f_1276(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_1276,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_1279,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],tmp=(C_word)a,a+=12,tmp);
/* lolevel.scm:159: ##sys#check-block */
t3=*((C_word*)lf[2]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[11];
av2[3]=lf[17];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k1277 in k1274 in chicken.memory#move-memory! in k934 */
static void C_ccall f_1279(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,4)))){
C_save_and_reclaim((void *)f_1279,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_1282,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],tmp=(C_word)a,a+=12,tmp);
if(C_truep(C_fixnum_lessp(((C_word*)t0)[4],C_fix(0)))){
/* lolevel.scm:161: ##sys#error */
t3=*((C_word*)lf[16]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[17];
av2[3]=lf[22];
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_1282(2,av2);}}}

/* k1280 in k1277 in k1274 in chicken.memory#move-memory! in k934 */
static void C_ccall f_1282(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,4)))){
C_save_and_reclaim((void *)f_1282,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_1285,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],tmp=(C_word)a,a+=12,tmp);
if(C_truep(C_fixnum_lessp(((C_word*)t0)[3],C_fix(0)))){
/* lolevel.scm:163: ##sys#error */
t3=*((C_word*)lf[16]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[17];
av2[3]=lf[21];
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_1285(2,av2);}}}

/* k1283 in k1280 in k1277 in k1274 in chicken.memory#move-memory! in k934 */
static void C_ccall f_1285(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,4)))){
C_save_and_reclaim((void *)f_1285,c,av);}
a=C_alloc(13);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_1290,a[2]=((C_word*)t0)[2],a[3]=t3,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word)li10),tmp=(C_word)a,a+=11,tmp));
t5=((C_word*)t3)[1];
f_1290(t5,((C_word*)t0)[9],((C_word*)t0)[10],((C_word*)t0)[11]);}

/* move in k1283 in k1280 in k1277 in k1274 in chicken.memory#move-memory! in k934 */
static void C_fcall f_1290(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
loop:
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,4)))){
C_save_and_reclaim_args((void *)trf_1290,4,t0,t1,t2,t3);}
a=C_alloc(10);
if(C_truep(C_structurep(t2))){
t4=C_slot(t2,C_fix(0));
if(C_truep(C_i_memq(t4,((C_word*)t0)[2]))){
/* lolevel.scm:167: move */
t10=t1;
t11=C_slot(t2,C_fix(1));
t12=t3;
t1=t10;
t2=t11;
t3=t12;
goto loop;}
else{
t5=C_fix((C_word)C_BAD_ARGUMENT_TYPE_ERROR);
/* lolevel.scm:133: ##sys#error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[3]+1));
C_word av2[5];
av2[0]=*((C_word*)lf[3]+1);
av2[1]=t1;
av2[2]=t5;
av2[3]=lf[17];
av2[4]=t2;
tp(5,av2);}}}
else{
if(C_truep(C_structurep(t3))){
t4=C_slot(t3,C_fix(0));
if(C_truep(C_i_memq(t4,((C_word*)t0)[2]))){
/* lolevel.scm:171: move */
t10=t1;
t11=t2;
t12=C_slot(t3,C_fix(1));
t1=t10;
t2=t11;
t3=t12;
goto loop;}
else{
t5=C_fix((C_word)C_BAD_ARGUMENT_TYPE_ERROR);
/* lolevel.scm:133: ##sys#error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[3]+1));
C_word av2[5];
av2[0]=*((C_word*)lf[3]+1);
av2[1]=t1;
av2[2]=t5;
av2[3]=lf[17];
av2[4]=t3;
tp(5,av2);}}}
else{
t4=C_i_safe_pointerp(t2);
t5=(C_truep(t4)?t4:C_locativep(t2));
if(C_truep(t5)){
t6=C_i_safe_pointerp(t3);
t7=(C_truep(t6)?t6:C_locativep(t3));
if(C_truep(t7)){
t8=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_1382,a[2]=t3,a[3]=t2,a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
if(C_truep(((C_word*)t0)[6])){
t9=t8;{
C_word av2[2];
av2[0]=t9;
av2[1]=((C_word*)t0)[6];
f_1382(2,av2);}}
else{
/* lolevel.scm:175: nosizerr */
t9=((C_word*)((C_word*)t0)[7])[1];
f_1221(t9,t8);}}
else{
t8=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_1391,a[2]=t3,a[3]=t2,a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[6],a[9]=((C_word*)t0)[7],tmp=(C_word)a,a+=10,tmp);
/* lolevel.scm:176: ##sys#bytevector? */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[20]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[20]+1);
av2[1]=t8;
av2[2]=t3;
tp(3,av2);}}}
else{
t6=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_1424,a[2]=t2,a[3]=t3,a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],tmp=(C_word)a,a+=10,tmp);
/* lolevel.scm:180: ##sys#bytevector? */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[20]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[20]+1);
av2[1]=t6;
av2[2]=t2;
tp(3,av2);}}}}}

/* k1380 in move in k1283 in k1280 in k1277 in k1274 in chicken.memory#move-memory! in k934 */
static void C_ccall f_1382(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1382,c,av);}
t2=(C_truep(((C_word*)t0)[2])?C_i_foreign_pointer_argumentp(((C_word*)t0)[2]):C_SCHEME_FALSE);
t3=(C_truep(((C_word*)t0)[3])?C_i_foreign_pointer_argumentp(((C_word*)t0)[3]):C_SCHEME_FALSE);
t4=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t4;
av2[1]=stub148(C_SCHEME_UNDEFINED,t2,t3,C_i_foreign_fixnum_argumentp(t1),C_i_foreign_fixnum_argumentp(((C_word*)t0)[5]),C_i_foreign_fixnum_argumentp(((C_word*)t0)[6]));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k1389 in move in k1283 in k1280 in k1277 in k1274 in chicken.memory#move-memory! in k934 */
static void C_ccall f_1391(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,4)))){
C_save_and_reclaim((void *)f_1391,c,av);}
a=C_alloc(13);
t2=(C_truep(t1)?t1:C_i_stringp(((C_word*)t0)[2]));
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_1401,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1405,a[2]=((C_word*)t0)[7],a[3]=t3,a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
if(C_truep(((C_word*)t0)[8])){
/* lolevel.scm:177: checkn1 */
t5=((C_word*)((C_word*)t0)[7])[1];
f_1233(t5,t3,((C_word*)t0)[8],C_block_size(((C_word*)t0)[2]),((C_word*)t0)[5]);}
else{
/* lolevel.scm:177: nosizerr */
t5=((C_word*)((C_word*)t0)[9])[1];
f_1221(t5,t4);}}
else{
t3=C_fix((C_word)C_BAD_ARGUMENT_TYPE_ERROR);
/* lolevel.scm:133: ##sys#error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[3]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[3]+1);
av2[1]=((C_word*)t0)[4];
av2[2]=t3;
av2[3]=lf[17];
av2[4]=((C_word*)t0)[2];
tp(5,av2);}}}

/* k1399 in k1389 in move in k1283 in k1280 in k1277 in k1274 in chicken.memory#move-memory! in k934 */
static void C_ccall f_1401(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1401,c,av);}
t2=(C_truep(((C_word*)t0)[2])?C_i_foreign_block_argumentp(((C_word*)t0)[2]):C_SCHEME_FALSE);
t3=(C_truep(((C_word*)t0)[3])?C_i_foreign_pointer_argumentp(((C_word*)t0)[3]):C_SCHEME_FALSE);
t4=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t4;
av2[1]=stub180(C_SCHEME_UNDEFINED,t2,t3,C_i_foreign_fixnum_argumentp(t1),C_i_foreign_fixnum_argumentp(((C_word*)t0)[5]),C_i_foreign_fixnum_argumentp(((C_word*)t0)[6]));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k1403 in k1389 in move in k1283 in k1280 in k1277 in k1274 in chicken.memory#move-memory! in k934 */
static void C_ccall f_1405(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_1405,c,av);}
/* lolevel.scm:177: checkn1 */
t2=((C_word*)((C_word*)t0)[2])[1];
f_1233(t2,((C_word*)t0)[3],t1,C_block_size(((C_word*)t0)[4]),((C_word*)t0)[5]);}

/* k1422 in move in k1283 in k1280 in k1277 in k1274 in chicken.memory#move-memory! in k934 */
static void C_ccall f_1424(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,4)))){
C_save_and_reclaim((void *)f_1424,c,av);}
a=C_alloc(10);
t2=(C_truep(t1)?t1:C_i_stringp(((C_word*)t0)[2]));
if(C_truep(t2)){
t3=C_block_size(((C_word*)t0)[2]);
t4=C_i_safe_pointerp(((C_word*)t0)[3]);
t5=(C_truep(t4)?t4:C_locativep(((C_word*)t0)[3]));
if(C_truep(t5)){
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_1453,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
if(C_truep(((C_word*)t0)[7])){
/* lolevel.scm:183: checkn1 */
t7=((C_word*)((C_word*)t0)[8])[1];
f_1233(t7,t6,((C_word*)t0)[7],t3,((C_word*)t0)[6]);}
else{
/* lolevel.scm:183: checkn1 */
t7=((C_word*)((C_word*)t0)[8])[1];
f_1233(t7,t6,t3,t3,((C_word*)t0)[6]);}}
else{
t6=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_1463,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=t3,a[9]=((C_word*)t0)[9],tmp=(C_word)a,a+=10,tmp);
/* lolevel.scm:184: ##sys#bytevector? */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[20]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[20]+1);
av2[1]=t6;
av2[2]=((C_word*)t0)[3];
tp(3,av2);}}}
else{
t3=C_fix((C_word)C_BAD_ARGUMENT_TYPE_ERROR);
/* lolevel.scm:133: ##sys#error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[3]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[3]+1);
av2[1]=((C_word*)t0)[4];
av2[2]=t3;
av2[3]=lf[17];
av2[4]=((C_word*)t0)[2];
tp(5,av2);}}}

/* k1451 in k1422 in move in k1283 in k1280 in k1277 in k1274 in chicken.memory#move-memory! in k934 */
static void C_ccall f_1453(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1453,c,av);}
t2=(C_truep(((C_word*)t0)[2])?C_i_foreign_pointer_argumentp(((C_word*)t0)[2]):C_SCHEME_FALSE);
t3=(C_truep(((C_word*)t0)[3])?C_i_foreign_block_argumentp(((C_word*)t0)[3]):C_SCHEME_FALSE);
t4=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t4;
av2[1]=stub164(C_SCHEME_UNDEFINED,t2,t3,C_i_foreign_fixnum_argumentp(t1),C_i_foreign_fixnum_argumentp(((C_word*)t0)[5]),C_i_foreign_fixnum_argumentp(((C_word*)t0)[6]));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k1461 in k1422 in move in k1283 in k1280 in k1277 in k1274 in chicken.memory#move-memory! in k934 */
static void C_ccall f_1463(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,6)))){
C_save_and_reclaim((void *)f_1463,c,av);}
a=C_alloc(7);
t2=(C_truep(t1)?t1:C_i_stringp(((C_word*)t0)[2]));
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_1473,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
t4=(C_truep(((C_word*)t0)[7])?((C_word*)t0)[7]:((C_word*)t0)[8]);
/* lolevel.scm:185: checkn2 */
t5=((C_word*)((C_word*)t0)[9])[1];
f_1249(t5,t3,t4,((C_word*)t0)[8],C_block_size(((C_word*)t0)[2]),((C_word*)t0)[6],((C_word*)t0)[5]);}
else{
t3=C_fix((C_word)C_BAD_ARGUMENT_TYPE_ERROR);
/* lolevel.scm:133: ##sys#error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[3]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[3]+1);
av2[1]=((C_word*)t0)[4];
av2[2]=t3;
av2[3]=lf[17];
av2[4]=((C_word*)t0)[2];
tp(5,av2);}}}

/* k1471 in k1461 in k1422 in move in k1283 in k1280 in k1277 in k1274 in chicken.memory#move-memory! in k934 */
static void C_ccall f_1473(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1473,c,av);}
t2=(C_truep(((C_word*)t0)[2])?C_i_foreign_block_argumentp(((C_word*)t0)[2]):C_SCHEME_FALSE);
t3=(C_truep(((C_word*)t0)[3])?C_i_foreign_block_argumentp(((C_word*)t0)[3]):C_SCHEME_FALSE);
t4=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t4;
av2[1]=stub196(C_SCHEME_UNDEFINED,t2,t3,C_i_foreign_fixnum_argumentp(t1),C_i_foreign_fixnum_argumentp(((C_word*)t0)[5]),C_i_foreign_fixnum_argumentp(((C_word*)t0)[6]));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* chicken.memory#allocate in k934 */
static void C_ccall f_1543(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,1)))){
C_save_and_reclaim((void *)f_1543,c,av);}
a=C_alloc(5);
t3=C_a_i_bytevector(&a,1,C_fix(3));
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=stub317(t3,C_i_foreign_fixnum_argumentp(t2));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* chicken.memory#free in k934 */
static void C_ccall f_1550(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1550,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=(C_truep(t2)?stub323(C_SCHEME_UNDEFINED,C_i_foreign_pointer_argumentp(t2)):stub323(C_SCHEME_UNDEFINED,C_SCHEME_FALSE));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.memory#pointer? in k934 */
static void C_ccall f_1560(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1560,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_safe_pointerp(t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.memory#pointer-like? in k934 */
static void C_ccall f_1568(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1568,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=(C_truep(C_blockp(t2))?C_specialp(t2):C_SCHEME_FALSE);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.memory#address->pointer in k934 */
static void C_ccall f_1579(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_1579,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1583,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* lolevel.scm:203: ##sys#check-integer */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[29]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[29]+1);
av2[1]=t3;
av2[2]=t2;
av2[3]=lf[30];
tp(4,av2);}}

/* k1581 in chicken.memory#address->pointer in k934 */
static void C_ccall f_1583(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_1583,c,av);}
/* lolevel.scm:204: ##sys#address->pointer */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[28]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[28]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
tp(3,av2);}}

/* chicken.memory#pointer->address in k934 */
static void C_ccall f_1588(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_1588,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1592,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* lolevel.scm:207: ##sys#check-special */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[33]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[33]+1);
av2[1]=t3;
av2[2]=t2;
av2[3]=lf[34];
tp(4,av2);}}

/* k1590 in chicken.memory#pointer->address in k934 */
static void C_ccall f_1592(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_1592,c,av);}
/* lolevel.scm:208: ##sys#pointer->address */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[32]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[32]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
tp(3,av2);}}

/* chicken.memory#object->pointer in k934 */
static void C_ccall f_1597(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,1)))){
C_save_and_reclaim((void *)f_1597,c,av);}
a=C_alloc(5);
if(C_truep(C_blockp(t2))){
t3=C_a_i_bytevector(&a,1,C_fix(3));
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=stub351(t3,t2);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* chicken.memory#pointer->object in k934 */
static void C_ccall f_1608(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_1608,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1612,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* lolevel.scm:215: ##sys#check-pointer */
t4=*((C_word*)lf[12]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=lf[37];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k1610 in chicken.memory#pointer->object in k934 */
static void C_ccall f_1612(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1612,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_pointer_to_object(((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.memory#pointer=? in k934 */
static void C_ccall f_1614(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_1614,c,av);}
a=C_alloc(5);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1618,a[2]=t1,a[3]=t2,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* lolevel.scm:219: ##sys#check-special */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[33]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[33]+1);
av2[1]=t4;
av2[2]=t2;
av2[3]=lf[39];
tp(4,av2);}}

/* k1616 in chicken.memory#pointer=? in k934 */
static void C_ccall f_1618(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_1618,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1621,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* lolevel.scm:220: ##sys#check-special */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[33]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[33]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=lf[39];
tp(4,av2);}}

/* k1619 in k1616 in chicken.memory#pointer=? in k934 */
static void C_ccall f_1621(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1621,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_pointer_eqp(((C_word*)t0)[3],((C_word*)t0)[4]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.memory#pointer+ in k934 */
static void C_ccall f_1623(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,1)))){
C_save_and_reclaim((void *)f_1623,c,av);}
a=C_alloc(5);
t4=C_a_i_bytevector(&a,1,C_fix(3));
t5=(C_truep(t2)?C_i_foreign_pointer_argumentp(t2):C_SCHEME_FALSE);
t6=C_fix((C_word)sizeof(int) * CHAR_BIT);
t7=t1;{
C_word *av2=av;
av2[0]=t7;
av2[1]=stub366(t4,t5,C_i_foreign_ranged_integer_argumentp(t3,t6));
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}

/* chicken.memory#align-to-word in k934 */
static void C_ccall f_1645(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,5)))){
C_save_and_reclaim((void *)f_1645,c,av);}
a=C_alloc(8);
if(C_truep(C_i_integerp(t2))){
t3=C_a_i_bytevector(&a,1,C_fix(6));
t4=C_fix((C_word)sizeof(int) * CHAR_BIT);
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=stub377(t3,C_i_foreign_ranged_integer_argumentp(t2,t4));
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t3=(C_truep(C_blockp(t2))?C_specialp(t2):C_SCHEME_FALSE);
if(C_truep(t3)){
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1675,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* lolevel.scm:233: ##sys#pointer->address */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[32]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[32]+1);
av2[1]=t4;
av2[2]=t2;
tp(3,av2);}}
else{
/* lolevel.scm:235: ##sys#signal-hook */
t4=*((C_word*)lf[5]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t4;
av2[1]=t1;
av2[2]=lf[6];
av2[3]=lf[42];
av2[4]=lf[43];
av2[5]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(6,av2);}}}}

/* k1673 in chicken.memory#align-to-word in k934 */
static void C_ccall f_1675(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_1675,c,av);}
a=C_alloc(8);
t2=C_a_i_bytevector(&a,1,C_fix(6));
t3=C_fix((C_word)sizeof(int) * CHAR_BIT);
/* lolevel.scm:233: ##sys#address->pointer */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[28]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[28]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=stub377(t2,C_i_foreign_ranged_integer_argumentp(t1,t3));
tp(3,av2);}}

/* chicken.memory#tag-pointer in k934 */
static void C_ccall f_1680(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_1680,c,av);}
a=C_alloc(4);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1684,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* lolevel.scm:243: ##sys#make-tagged-pointer */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[46]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[46]+1);
av2[1]=t4;
av2[2]=t3;
tp(3,av2);}}

/* k1682 in chicken.memory#tag-pointer in k934 */
static void C_ccall f_1684(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_1684,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1687,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=(C_truep(C_blockp(((C_word*)t0)[3]))?C_specialp(((C_word*)t0)[3]):C_SCHEME_FALSE);
if(C_truep(t3)){
t4=C_copy_pointer(((C_word*)t0)[3],t1);
t5=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t4=C_fix((C_word)C_BAD_ARGUMENT_TYPE_NO_POINTER_ERROR);
/* lolevel.scm:246: ##sys#error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[3]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[3]+1);
av2[1]=t2;
av2[2]=t4;
av2[3]=lf[45];
av2[4]=((C_word*)t0)[3];
tp(5,av2);}}}

/* k1685 in k1682 in chicken.memory#tag-pointer in k934 */
static void C_ccall f_1687(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1687,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.memory#tagged-pointer? in k934 */
static void C_ccall f_1701(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1701,c,av);}
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,3,t0));
if(C_truep(C_blockp(t2))){
if(C_truep(C_taggedpointerp(t2))){
t5=C_i_not(t4);
t6=t1;{
C_word *av2=av;
av2[0]=t6;
av2[1]=(C_truep(t5)?t5:C_i_equalp(t4,C_slot(t2,C_fix(1))));
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}
else{
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* chicken.memory#pointer-tag in k934 */
static void C_ccall f_1732(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_1732,c,av);}
t3=(C_truep(C_blockp(t2))?C_specialp(t2):C_SCHEME_FALSE);
if(C_truep(t3)){
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=(C_truep(C_taggedpointerp(t2))?C_slot(t2,C_fix(1)):C_SCHEME_FALSE);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=C_fix((C_word)C_BAD_ARGUMENT_TYPE_NO_POINTER_ERROR);
/* lolevel.scm:258: ##sys#error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[3]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[3]+1);
av2[1]=t1;
av2[2]=t4;
av2[3]=lf[49];
av2[4]=t2;
tp(5,av2);}}}

/* chicken.memory#pointer-u8-set! in k934 */
static void C_ccall f_1756(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1756,c,av);}
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_u_i_pointer_u8_set(t2,t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* chicken.memory#pointer-s8-set! in k934 */
static void C_ccall f_1759(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1759,c,av);}
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_u_i_pointer_s8_set(t2,t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* chicken.memory#pointer-u16-set! in k934 */
static void C_ccall f_1762(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1762,c,av);}
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_u_i_pointer_u16_set(t2,t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* chicken.memory#pointer-s16-set! in k934 */
static void C_ccall f_1765(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1765,c,av);}
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_u_i_pointer_s16_set(t2,t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* chicken.memory#pointer-u32-set! in k934 */
static void C_ccall f_1768(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1768,c,av);}
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_u_i_pointer_u32_set(t2,t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* chicken.memory#pointer-s32-set! in k934 */
static void C_ccall f_1771(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1771,c,av);}
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_u_i_pointer_s32_set(t2,t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* chicken.memory#pointer-u64-set! in k934 */
static void C_ccall f_1774(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1774,c,av);}
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_u_i_pointer_u64_set(t2,t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* chicken.memory#pointer-s64-set! in k934 */
static void C_ccall f_1777(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1777,c,av);}
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_u_i_pointer_s64_set(t2,t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* chicken.memory#pointer-f32-set! in k934 */
static void C_ccall f_1780(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1780,c,av);}
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_u_i_pointer_f32_set(t2,t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* chicken.memory#pointer-f64-set! in k934 */
static void C_ccall f_1783(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1783,c,av);}
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_u_i_pointer_f64_set(t2,t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k1786 in k934 */
static void C_ccall f_1788(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_1788,c,av);}
a=C_alloc(6);
t2=C_mutate((C_word*)lf[60]+1 /* (set! chicken.memory#pointer-u8-ref ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1792,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2592,a[2]=((C_word)li84),tmp=(C_word)a,a+=3,tmp);
/* lolevel.scm:283: chicken.base#getter-with-setter */
t5=*((C_word*)lf[139]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
av2[3]=*((C_word*)lf[51]+1);
av2[4]=lf[154];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}

/* k1790 in k1786 in k934 */
static void C_ccall f_1792(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_1792,c,av);}
a=C_alloc(6);
t2=C_mutate((C_word*)lf[61]+1 /* (set! chicken.memory#pointer-s8-ref ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1796,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2589,a[2]=((C_word)li83),tmp=(C_word)a,a+=3,tmp);
/* lolevel.scm:289: chicken.base#getter-with-setter */
t5=*((C_word*)lf[139]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
av2[3]=*((C_word*)lf[52]+1);
av2[4]=lf[153];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}

/* k1794 in k1790 in k1786 in k934 */
static void C_ccall f_1796(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_1796,c,av);}
a=C_alloc(6);
t2=C_mutate((C_word*)lf[62]+1 /* (set! chicken.memory#pointer-u16-ref ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1800,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2586,a[2]=((C_word)li82),tmp=(C_word)a,a+=3,tmp);
/* lolevel.scm:295: chicken.base#getter-with-setter */
t5=*((C_word*)lf[139]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
av2[3]=*((C_word*)lf[53]+1);
av2[4]=lf[152];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}

/* k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_1800(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_1800,c,av);}
a=C_alloc(6);
t2=C_mutate((C_word*)lf[63]+1 /* (set! chicken.memory#pointer-s16-ref ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1804,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2583,a[2]=((C_word)li81),tmp=(C_word)a,a+=3,tmp);
/* lolevel.scm:301: chicken.base#getter-with-setter */
t5=*((C_word*)lf[139]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
av2[3]=*((C_word*)lf[54]+1);
av2[4]=lf[151];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}

/* k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_1804(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_1804,c,av);}
a=C_alloc(6);
t2=C_mutate((C_word*)lf[64]+1 /* (set! chicken.memory#pointer-u32-ref ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1808,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2580,a[2]=((C_word)li80),tmp=(C_word)a,a+=3,tmp);
/* lolevel.scm:307: chicken.base#getter-with-setter */
t5=*((C_word*)lf[139]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
av2[3]=*((C_word*)lf[55]+1);
av2[4]=lf[150];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}

/* k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_1808(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_1808,c,av);}
a=C_alloc(6);
t2=C_mutate((C_word*)lf[65]+1 /* (set! chicken.memory#pointer-s32-ref ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1812,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2577,a[2]=((C_word)li79),tmp=(C_word)a,a+=3,tmp);
/* lolevel.scm:313: chicken.base#getter-with-setter */
t5=*((C_word*)lf[139]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
av2[3]=*((C_word*)lf[56]+1);
av2[4]=lf[149];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}

/* k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_1812(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_1812,c,av);}
a=C_alloc(6);
t2=C_mutate((C_word*)lf[66]+1 /* (set! chicken.memory#pointer-u64-ref ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1816,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2574,a[2]=((C_word)li78),tmp=(C_word)a,a+=3,tmp);
/* lolevel.scm:319: chicken.base#getter-with-setter */
t5=*((C_word*)lf[139]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
av2[3]=*((C_word*)lf[57]+1);
av2[4]=lf[148];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}

/* k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_1816(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_1816,c,av);}
a=C_alloc(6);
t2=C_mutate((C_word*)lf[67]+1 /* (set! chicken.memory#pointer-s64-ref ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1820,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2571,a[2]=((C_word)li77),tmp=(C_word)a,a+=3,tmp);
/* lolevel.scm:325: chicken.base#getter-with-setter */
t5=*((C_word*)lf[139]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
av2[3]=*((C_word*)lf[58]+1);
av2[4]=lf[147];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}

/* k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_1820(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_1820,c,av);}
a=C_alloc(6);
t2=C_mutate((C_word*)lf[68]+1 /* (set! chicken.memory#pointer-f32-ref ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1824,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2568,a[2]=((C_word)li76),tmp=(C_word)a,a+=3,tmp);
/* lolevel.scm:331: chicken.base#getter-with-setter */
t5=*((C_word*)lf[139]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
av2[3]=*((C_word*)lf[59]+1);
av2[4]=lf[146];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}

/* k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_1824(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(25,c,5)))){
C_save_and_reclaim((void *)f_1824,c,av);}
a=C_alloc(25);
t2=C_mutate((C_word*)lf[69]+1 /* (set! chicken.memory#pointer-f64-ref ...) */,t1);
t3=C_a_i_list1(&a,1,lf[70]);
t4=C_mutate((C_word*)lf[71]+1 /* (set! chicken.memory#make-pointer-vector ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1829,a[2]=t3,a[3]=((C_word)li37),tmp=(C_word)a,a+=4,tmp));
t5=C_mutate((C_word*)lf[76]+1 /* (set! chicken.memory#pointer-vector? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1893,a[2]=((C_word)li38),tmp=(C_word)a,a+=3,tmp));
t6=C_mutate((C_word*)lf[77]+1 /* (set! chicken.memory#pointer-vector ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1899,a[2]=((C_word)li40),tmp=(C_word)a,a+=3,tmp));
t7=C_mutate((C_word*)lf[78]+1 /* (set! chicken.memory#pointer-vector-fill! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1939,a[2]=((C_word)li42),tmp=(C_word)a,a+=3,tmp));
t8=C_mutate((C_word*)lf[80]+1 /* (set! chicken.memory#pointer-vector-set! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2000,a[2]=((C_word)li43),tmp=(C_word)a,a+=3,tmp));
t9=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2028,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t10=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2548,a[2]=((C_word)li75),tmp=(C_word)a,a+=3,tmp);
/* lolevel.scm:392: chicken.base#getter-with-setter */
t11=*((C_word*)lf[139]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t11;
av2[1]=t9;
av2[2]=t10;
av2[3]=*((C_word*)lf[80]+1);
av2[4]=lf[145];
((C_proc)(void*)(*((C_word*)t11+1)))(5,av2);}}

/* chicken.memory#make-pointer-vector in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_1829(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_1829,c,av);}
a=C_alloc(6);
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?((C_word*)t0)[2]:C_get_rest_arg(c,3,av,3,t0));
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1836,a[2]=t2,a[3]=t4,a[4]=((C_word*)t0)[2],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
/* lolevel.scm:342: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[75]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[75]+1);
av2[1]=t5;
av2[2]=t2;
av2[3]=lf[73];
tp(4,av2);}}

/* k1834 in chicken.memory#make-pointer-vector in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_1836(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_1836,c,av);}
a=C_alloc(6);
t2=C_i_foreign_fixnum_argumentp(((C_word*)t0)[2]);
t3=stub491(C_SCHEME_UNDEFINED,t2);
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1848,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[2],tmp=(C_word)a,a+=6,tmp);
/* lolevel.scm:345: ##sys#make-blob */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[74]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[74]+1);
av2[1]=t4;
av2[2]=t3;
tp(3,av2);}}

/* k1846 in k1834 in chicken.memory#make-pointer-vector in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_1848(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_1848,c,av);}
a=C_alloc(6);
t2=C_eqp(((C_word*)t0)[2],((C_word*)t0)[3]);
if(C_truep(t2)){
t3=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_record3(&a,3,lf[72],((C_word*)t0)[5],t1);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1860,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[2],a[4]=t1,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(((C_word*)t0)[2])){
/* lolevel.scm:348: ##sys#check-pointer */
t4=*((C_word*)lf[12]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[2];
av2[3]=lf[73];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_1860(2,av2);}}}}

/* k1858 in k1846 in k1834 in chicken.memory#make-pointer-vector in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_1860(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_1860,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1865,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word)li36),tmp=(C_word)a,a+=6,tmp);
t3=(
  f_1865(t2,C_fix(0))
);
t4=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_record3(&a,3,lf[72],((C_word*)t0)[2],((C_word*)t0)[4]);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* doloop496 in k1858 in k1846 in k1834 in chicken.memory#make-pointer-vector in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static C_word C_fcall f_1865(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_stack_overflow_check;
loop:{}
if(C_truep(C_fixnum_greater_or_equal_p(t1,((C_word*)t0)[2]))){
t2=C_SCHEME_UNDEFINED;
return(t2);}
else{
t2=C_i_foreign_fixnum_argumentp(t1);
t3=(C_truep(((C_word*)t0)[3])?stub543(C_SCHEME_UNDEFINED,((C_word*)t0)[4],t2,C_i_foreign_pointer_argumentp(((C_word*)t0)[3])):stub543(C_SCHEME_UNDEFINED,((C_word*)t0)[4],t2,C_SCHEME_FALSE));
t5=C_fixnum_plus(t1,C_fix(1));
t1=t5;
goto loop;}}

/* chicken.memory#pointer-vector? in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_1893(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1893,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_structurep(t2,lf[72]);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.memory#pointer-vector in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_1899(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +4,c,2)))){
C_save_and_reclaim((void*)f_1899,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+4);
t2=C_build_rest(&a,c,2,av);
C_word t3;
C_word t4;
C_word t5;
t3=C_u_i_length(t2);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1904,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* lolevel.scm:359: make-pointer-vector */
t5=*((C_word*)lf[71]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k1902 in chicken.memory#pointer-vector in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_1904(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_1904,c,av);}
a=C_alloc(8);
t2=C_slot(t1,C_fix(2));
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1912,a[2]=t1,a[3]=t2,a[4]=t4,a[5]=((C_word)li39),tmp=(C_word)a,a+=6,tmp));
t6=((C_word*)t4)[1];
f_1912(t6,((C_word*)t0)[2],((C_word*)t0)[3],C_fix(0));}

/* doloop511 in k1902 in chicken.memory#pointer-vector in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_fcall f_1912(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,3)))){
C_save_and_reclaim_args((void *)trf_1912,4,t0,t1,t2,t3);}
a=C_alloc(8);
if(C_truep(C_i_nullp(t2))){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=C_i_car(t2);
t5=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_1925,a[2]=t3,a[3]=t4,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=t1,a[7]=t2,tmp=(C_word)a,a+=8,tmp);
/* lolevel.scm:365: ##sys#check-pointer */
t6=*((C_word*)lf[12]+1);{
C_word av2[4];
av2[0]=t6;
av2[1]=t5;
av2[2]=t4;
av2[3]=lf[72];
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}}

/* k1923 in doloop511 in k1902 in chicken.memory#pointer-vector in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_1925(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_1925,c,av);}
t2=C_i_foreign_fixnum_argumentp(((C_word*)t0)[2]);
t3=(C_truep(((C_word*)t0)[3])?stub543(C_SCHEME_UNDEFINED,((C_word*)t0)[4],t2,C_i_foreign_pointer_argumentp(((C_word*)t0)[3])):stub543(C_SCHEME_UNDEFINED,((C_word*)t0)[4],t2,C_SCHEME_FALSE));
t4=((C_word*)((C_word*)t0)[5])[1];
f_1912(t4,((C_word*)t0)[6],C_u_i_cdr(((C_word*)t0)[7]),C_fixnum_plus(((C_word*)t0)[2],C_fix(1)));}

/* chicken.memory#pointer-vector-fill! in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_1939(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_1939,c,av);}
a=C_alloc(5);
t4=C_i_check_structure_2(t2,lf[72],lf[79]);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1946,a[2]=t2,a[3]=t3,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
if(C_truep(t3)){
/* lolevel.scm:370: ##sys#check-pointer */
t6=*((C_word*)lf[12]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=t3;
av2[3]=lf[79];
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}
else{
t6=t5;{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_SCHEME_UNDEFINED;
f_1946(2,av2);}}}

/* k1944 in chicken.memory#pointer-vector-fill! in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_1946(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_1946,c,av);}
a=C_alloc(6);
t2=C_slot(((C_word*)t0)[2],C_fix(2));
t3=C_slot(((C_word*)t0)[2],C_fix(1));
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1957,a[2]=t3,a[3]=((C_word*)t0)[3],a[4]=t2,a[5]=((C_word)li41),tmp=(C_word)a,a+=6,tmp);
t5=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t5;
av2[1]=(
  f_1957(t4,C_fix(0))
);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* doloop524 in k1944 in chicken.memory#pointer-vector-fill! in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static C_word C_fcall f_1957(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_stack_overflow_check;
loop:{}
if(C_truep(C_fixnum_greater_or_equal_p(t1,((C_word*)t0)[2]))){
t2=C_SCHEME_UNDEFINED;
return(t2);}
else{
t2=C_i_foreign_fixnum_argumentp(t1);
t3=(C_truep(((C_word*)t0)[3])?stub543(C_SCHEME_UNDEFINED,((C_word*)t0)[4],t2,C_i_foreign_pointer_argumentp(((C_word*)t0)[3])):stub543(C_SCHEME_UNDEFINED,((C_word*)t0)[4],t2,C_SCHEME_FALSE));
t5=C_fixnum_plus(t1,C_fix(1));
t1=t5;
goto loop;}}

/* chicken.memory#pointer-vector-set! in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2000(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_2000,c,av);}
a=C_alloc(6);
t5=C_i_check_structure_2(t2,lf[72],lf[81]);
t6=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2007,a[2]=t2,a[3]=t3,a[4]=t1,a[5]=t4,tmp=(C_word)a,a+=6,tmp);
/* lolevel.scm:387: ##sys#check-range */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[83]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[83]+1);
av2[1]=t6;
av2[2]=t3;
av2[3]=C_fix(0);
av2[4]=C_slot(t2,C_fix(1));
tp(5,av2);}}

/* k2005 in chicken.memory#pointer-vector-set! in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2007(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2007,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2010,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
if(C_truep(((C_word*)t0)[5])){
/* lolevel.scm:388: ##sys#check-pointer */
t3=*((C_word*)lf[12]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=lf[82];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_2010(2,av2);}}}

/* k2008 in k2005 in chicken.memory#pointer-vector-set! in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2010(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2010,c,av);}
t2=C_slot(((C_word*)t0)[2],C_fix(2));
t3=C_i_foreign_fixnum_argumentp(((C_word*)t0)[3]);
t4=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t4;
av2[1]=(C_truep(((C_word*)t0)[5])?stub543(C_SCHEME_UNDEFINED,t2,t3,C_i_foreign_pointer_argumentp(((C_word*)t0)[5])):stub543(C_SCHEME_UNDEFINED,t2,t3,C_SCHEME_FALSE));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2028(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(34,c,4)))){
C_save_and_reclaim((void *)f_2028,c,av);}
a=C_alloc(34);
t2=C_mutate((C_word*)lf[84]+1 /* (set! chicken.memory#pointer-vector-ref ...) */,t1);
t3=C_mutate((C_word*)lf[85]+1 /* (set! chicken.memory#pointer-vector-length ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2030,a[2]=((C_word)li44),tmp=(C_word)a,a+=3,tmp));
t4=C_a_i_provide(&a,1,lf[87]);
t5=C_mutate((C_word*)lf[88]+1 /* (set! chicken.memory.representation#object-copy ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2039,a[2]=((C_word)li47),tmp=(C_word)a,a+=3,tmp));
t6=C_a_i_vector1(&a,1,lf[90]);
t7=C_mutate(&lf[91] /* (set! chicken.memory.representation#xproc-tag ...) */,t6);
t8=C_mutate((C_word*)lf[92]+1 /* (set! chicken.memory.representation#extend-procedure ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2119,a[2]=((C_word)li50),tmp=(C_word)a,a+=3,tmp));
t9=C_mutate((C_word*)lf[96]+1 /* (set! chicken.memory.representation#extended-procedure? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2154,a[2]=((C_word)li52),tmp=(C_word)a,a+=3,tmp));
t10=C_mutate((C_word*)lf[98]+1 /* (set! chicken.memory.representation#procedure-data ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2190,a[2]=((C_word)li54),tmp=(C_word)a,a+=3,tmp));
t11=C_mutate((C_word*)lf[99]+1 /* (set! chicken.memory.representation#set-procedure-data! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2229,a[2]=((C_word)li55),tmp=(C_word)a,a+=3,tmp));
t12=C_mutate((C_word*)lf[102]+1 /* (set! chicken.memory.representation#vector-like? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2245,a[2]=((C_word)li56),tmp=(C_word)a,a+=3,tmp));
t13=C_mutate((C_word*)lf[103]+1 /* (set! chicken.memory.representation#block-set! ...) */,*((C_word*)lf[104]+1));
t14=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2266,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* lolevel.scm:472: chicken.base#getter-with-setter */
t15=*((C_word*)lf[139]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t15;
av2[1]=t14;
av2[2]=*((C_word*)lf[143]+1);
av2[3]=*((C_word*)lf[104]+1);
av2[4]=lf[144];
((C_proc)(void*)(*((C_word*)t15+1)))(5,av2);}}

/* chicken.memory#pointer-vector-length in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2030(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2030,c,av);}
t3=C_i_check_structure_2(t2,lf[72],lf[86]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_slot(t2,C_fix(1));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* chicken.memory.representation#object-copy in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2039(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2039,c,av);}
a=C_alloc(6);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2045,a[2]=t4,a[3]=((C_word)li46),tmp=(C_word)a,a+=4,tmp));
t6=((C_word*)t4)[1];
f_2045(t6,t1,t2);}

/* copy in chicken.memory.representation#object-copy in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_fcall f_2045(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_2045,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_not(C_blockp(t2)))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
if(C_truep(C_i_symbolp(t2))){
/* lolevel.scm:423: ##sys#intern-symbol */{
C_word av2[3];
av2[0]=0;
av2[1]=t1;
av2[2]=C_slot(t2,C_fix(1));
C_string_to_symbol(3,av2);}}
else{
t3=C_block_size(t2);
t4=(C_truep(C_byteblockp(t2))?C_words(t3):t3);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2075,a[2]=t2,a[3]=t1,a[4]=t3,a[5]=((C_word*)t0)[2],tmp=(C_word)a,a+=6,tmp);
/* lolevel.scm:427: ##sys#make-vector */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[89]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[89]+1);
av2[1]=t5;
av2[2]=t4;
tp(3,av2);}}}}

/* k2073 in copy in chicken.memory.representation#object-copy in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2075(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,3)))){
C_save_and_reclaim((void *)f_2075,c,av);}
a=C_alloc(13);
t2=C_copy_block(((C_word*)t0)[2],t1);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2078,a[2]=((C_word*)t0)[3],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_byteblockp(((C_word*)t0)[2]))){
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=(C_truep(C_specialp(((C_word*)t0)[2]))?C_fix(1):C_fix(0));
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_2087,a[2]=((C_word*)t0)[4],a[3]=t2,a[4]=t6,a[5]=((C_word*)t0)[5],a[6]=((C_word)li45),tmp=(C_word)a,a+=7,tmp));
t8=((C_word*)t6)[1];
f_2087(t8,t3,t4);}}

/* k2076 in k2073 in copy in chicken.memory.representation#object-copy in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2078(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2078,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* doloop629 in k2073 in copy in chicken.memory.representation#object-copy in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_fcall f_2087(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_2087,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_fixnum_greater_or_equal_p(t2,((C_word*)t0)[2]))){
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2108,a[2]=((C_word*)t0)[3],a[3]=t2,a[4]=((C_word*)t0)[4],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
/* lolevel.scm:431: copy */
t4=((C_word*)((C_word*)t0)[5])[1];
f_2045(t4,t3,C_slot(((C_word*)t0)[3],t2));}}

/* k2106 in doloop629 in k2073 in copy in chicken.memory.representation#object-copy in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2108(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_2108,c,av);}
t2=C_i_setslot(((C_word*)t0)[2],((C_word*)t0)[3],t1);
t3=((C_word*)((C_word*)t0)[4])[1];
f_2087(t3,((C_word*)t0)[5],C_fixnum_plus(((C_word*)t0)[3],C_fix(1)));}

/* chicken.memory.representation#extend-procedure in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2119(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_2119,c,av);}
a=C_alloc(5);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2123,a[2]=t3,a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* lolevel.scm:441: ##sys#check-closure */
t5=*((C_word*)lf[94]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
av2[3]=lf[95];
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k2121 in chicken.memory.representation#extend-procedure in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2123(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,4)))){
C_save_and_reclaim((void *)f_2123,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2128,a[2]=((C_word)li48),tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2144,a[2]=((C_word*)t0)[2],a[3]=((C_word)li49),tmp=(C_word)a,a+=4,tmp);
/* lolevel.scm:442: ##sys#decorate-lambda */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[93]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[93]+1);
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=t2;
av2[4]=t3;
tp(5,av2);}}

/* a2127 in k2121 in chicken.memory.representation#extend-procedure in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2128(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2128,c,av);}
t3=C_i_pairp(t2);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=(C_truep(t3)?C_eqp(lf[91],C_slot(t2,C_fix(0))):C_SCHEME_FALSE);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* a2143 in k2121 in chicken.memory.representation#extend-procedure in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2144(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_2144,c,av);}
a=C_alloc(3);
t4=C_a_i_cons(&a,2,lf[91],((C_word*)t0)[2]);
t5=C_i_setslot(t2,t3,t4);
t6=t1;{
C_word *av2=av;
av2[0]=t6;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}

/* chicken.memory.representation#extended-procedure? in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2154(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2154,c,av);}
a=C_alloc(6);
if(C_truep(C_blockp(t2))){
if(C_truep(C_closurep(t2))){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2188,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2171,a[2]=((C_word)li51),tmp=(C_word)a,a+=3,tmp);
/* lolevel.scm:448: ##sys#lambda-decoration */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[97]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[97]+1);
av2[1]=t3;
av2[2]=t2;
av2[3]=t4;
tp(4,av2);}}
else{
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}
else{
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* a2170 in chicken.memory.representation#extended-procedure? in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2171(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2171,c,av);}
t3=C_i_pairp(t2);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=(C_truep(t3)?C_eqp(lf[91],C_slot(t2,C_fix(0))):C_SCHEME_FALSE);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k2186 in chicken.memory.representation#extended-procedure? in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2188(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2188,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(C_truep(t1)?C_SCHEME_TRUE:C_SCHEME_FALSE);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.memory.representation#procedure-data in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2190(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2190,c,av);}
a=C_alloc(6);
if(C_truep(C_blockp(t2))){
if(C_truep(C_closurep(t2))){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2221,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2204,a[2]=((C_word)li53),tmp=(C_word)a,a+=3,tmp);
/* lolevel.scm:448: ##sys#lambda-decoration */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[97]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[97]+1);
av2[1]=t3;
av2[2]=t2;
av2[3]=t4;
tp(4,av2);}}
else{
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}
else{
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* a2203 in chicken.memory.representation#procedure-data in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2204(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2204,c,av);}
t3=C_i_pairp(t2);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=(C_truep(t3)?C_eqp(lf[91],C_slot(t2,C_fix(0))):C_SCHEME_FALSE);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k2219 in chicken.memory.representation#procedure-data in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2221(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2221,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(C_truep(t1)?C_slot(t1,C_fix(1)):C_SCHEME_FALSE);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.memory.representation#set-procedure-data! in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2229(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_2229,c,av);}
a=C_alloc(4);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2243,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* lolevel.scm:461: extend-procedure */
t5=*((C_word*)lf[92]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k2241 in chicken.memory.representation#set-procedure-data! in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2243(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_2243,c,av);}
t2=C_eqp(((C_word*)t0)[2],t1);
if(C_truep(t2)){
t3=C_SCHEME_UNDEFINED;
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
/* lolevel.scm:462: ##sys#signal-hook */
t3=*((C_word*)lf[5]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[6];
av2[3]=lf[100];
av2[4]=lf[101];
av2[5]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}}

/* chicken.memory.representation#vector-like? in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2245(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2245,c,av);}
if(C_truep(C_blockp(t2))){
t3=C_specialp(t2);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=(C_truep(t3)?C_i_not(t3):C_i_not(C_byteblockp(t2)));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2266(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(27,c,5)))){
C_save_and_reclaim((void *)f_2266,c,av);}
a=C_alloc(27);
t2=C_mutate((C_word*)lf[105]+1 /* (set! chicken.memory.representation#block-ref ...) */,t1);
t3=C_mutate((C_word*)lf[106]+1 /* (set! chicken.memory.representation#number-of-slots ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2268,a[2]=((C_word)li57),tmp=(C_word)a,a+=3,tmp));
t4=C_mutate((C_word*)lf[108]+1 /* (set! chicken.memory.representation#number-of-bytes ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2277,a[2]=((C_word)li58),tmp=(C_word)a,a+=3,tmp));
t5=C_mutate((C_word*)lf[111]+1 /* (set! chicken.memory.representation#make-record-instance ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2300,a[2]=((C_word)li59),tmp=(C_word)a,a+=3,tmp));
t6=C_mutate((C_word*)lf[114]+1 /* (set! chicken.memory.representation#record-instance? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2309,a[2]=((C_word)li60),tmp=(C_word)a,a+=3,tmp));
t7=C_mutate((C_word*)lf[115]+1 /* (set! chicken.memory.representation#record-instance-type ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2345,a[2]=((C_word)li61),tmp=(C_word)a,a+=3,tmp));
t8=C_mutate((C_word*)lf[117]+1 /* (set! chicken.memory.representation#record-instance-length ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2354,a[2]=((C_word)li62),tmp=(C_word)a,a+=3,tmp));
t9=C_mutate((C_word*)lf[119]+1 /* (set! chicken.memory.representation#record-instance-slot-set! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2367,a[2]=((C_word)li63),tmp=(C_word)a,a+=3,tmp));
t10=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2393,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t11=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2524,a[2]=((C_word)li74),tmp=(C_word)a,a+=3,tmp);
/* lolevel.scm:520: chicken.base#getter-with-setter */
t12=*((C_word*)lf[139]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t12;
av2[1]=t10;
av2[2]=t11;
av2[3]=*((C_word*)lf[119]+1);
av2[4]=lf[142];
((C_proc)(void*)(*((C_word*)t12+1)))(5,av2);}}

/* chicken.memory.representation#number-of-slots in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2268(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_2268,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2272,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* lolevel.scm:476: ##sys#check-generic-vector */
t4=*((C_word*)lf[10]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=lf[107];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k2270 in chicken.memory.representation#number-of-slots in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2272(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2272,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_block_size(((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.memory.representation#number-of-bytes in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2277(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_2277,c,av);}
if(C_truep(C_i_not(C_blockp(t2)))){
/* lolevel.scm:481: ##sys#signal-hook */
t3=*((C_word*)lf[5]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=lf[6];
av2[3]=lf[109];
av2[4]=lf[110];
av2[5]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}
else{
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=(C_truep(C_byteblockp(t2))?C_block_size(t2):C_bytes(C_block_size(t2)));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* chicken.memory.representation#make-record-instance in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2300(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +0,c,4)))){
C_save_and_reclaim((void*)f_2300,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+0);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
t4=C_i_check_symbol_2(t2,lf[112]);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=t1;
av2[2]=*((C_word*)lf[113]+1);
av2[3]=t2;
av2[4]=t3;
C_apply(5,av2);}}

/* chicken.memory.representation#record-instance? in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2309(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2309,c,av);}
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,3,t0));
t5=(C_truep(C_blockp(t2))?C_structurep(t2):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_i_not(t4);
t7=t1;{
C_word *av2=av;
av2[0]=t7;
av2[1]=(C_truep(t6)?t6:C_eqp(t4,C_slot(t2,C_fix(0))));
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}
else{
t6=t1;{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* chicken.memory.representation#record-instance-type in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2345(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_2345,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2349,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* lolevel.scm:507: ##sys#check-generic-structure */
t4=*((C_word*)lf[8]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=lf[116];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k2347 in chicken.memory.representation#record-instance-type in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2349(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2349,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_slot(((C_word*)t0)[3],C_fix(0));
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.memory.representation#record-instance-length in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2354(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_2354,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2358,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* lolevel.scm:511: ##sys#check-generic-structure */
t4=*((C_word*)lf[8]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=lf[118];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k2356 in chicken.memory.representation#record-instance-length in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2358(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2358,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_fixnum_difference(C_block_size(((C_word*)t0)[3]),C_fix(1));
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.memory.representation#record-instance-slot-set! in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2367(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2367,c,av);}
a=C_alloc(6);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2371,a[2]=t1,a[3]=t2,a[4]=t3,a[5]=t4,tmp=(C_word)a,a+=6,tmp);
/* lolevel.scm:515: ##sys#check-generic-structure */
t6=*((C_word*)lf[8]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=t2;
av2[3]=lf[120];
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* k2369 in chicken.memory.representation#record-instance-slot-set! in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2371(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,5)))){
C_save_and_reclaim((void *)f_2371,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2374,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t3=C_block_size(((C_word*)t0)[3]);
/* lolevel.scm:516: ##sys#check-range */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[83]+1));
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=*((C_word*)lf[83]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=C_fix(0);
av2[4]=C_fixnum_difference(t3,C_fix(1));
av2[5]=lf[120];
tp(6,av2);}}

/* k2372 in k2369 in chicken.memory.representation#record-instance-slot-set! in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2374(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2374,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_i_setslot(((C_word*)t0)[3],C_fixnum_plus(((C_word*)t0)[4],C_fix(1)),((C_word*)t0)[5]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k2391 in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2393(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(32,c,4)))){
C_save_and_reclaim((void *)f_2393,c,av);}
a=C_alloc(32);
t2=C_mutate((C_word*)lf[121]+1 /* (set! chicken.memory.representation#record-instance-slot ...) */,t1);
t3=C_mutate((C_word*)lf[122]+1 /* (set! chicken.memory.representation#record->vector ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2395,a[2]=((C_word)li65),tmp=(C_word)a,a+=3,tmp));
t4=C_mutate((C_word*)lf[124]+1 /* (set! chicken.memory.representation#object-become! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2433,a[2]=((C_word)li66),tmp=(C_word)a,a+=3,tmp));
t5=C_mutate((C_word*)lf[127]+1 /* (set! chicken.memory.representation#mutate-procedure! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2442,a[2]=((C_word)li67),tmp=(C_word)a,a+=3,tmp));
t6=C_a_i_provide(&a,1,lf[129]);
t7=C_mutate((C_word*)lf[130]+1 /* (set! chicken.locative#make-locative ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2473,a[2]=((C_word)li68),tmp=(C_word)a,a+=3,tmp));
t8=C_mutate((C_word*)lf[133]+1 /* (set! chicken.locative#make-weak-locative ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2489,a[2]=((C_word)li69),tmp=(C_word)a,a+=3,tmp));
t9=C_mutate((C_word*)lf[135]+1 /* (set! chicken.locative#locative-set! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2505,a[2]=((C_word)li70),tmp=(C_word)a,a+=3,tmp));
t10=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2510,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t11=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2521,a[2]=((C_word)li73),tmp=(C_word)a,a+=3,tmp);
/* lolevel.scm:591: chicken.base#getter-with-setter */
t12=*((C_word*)lf[139]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t12;
av2[1]=t10;
av2[2]=t11;
av2[3]=*((C_word*)lf[135]+1);
av2[4]=lf[140];
((C_proc)(void*)(*((C_word*)t12+1)))(5,av2);}}

/* chicken.memory.representation#record->vector in k2391 in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2395(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_2395,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2399,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* lolevel.scm:529: ##sys#check-generic-structure */
t4=*((C_word*)lf[8]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=lf[123];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k2397 in chicken.memory.representation#record->vector in k2391 in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2399(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_2399,c,av);}
a=C_alloc(5);
t2=C_block_size(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2405,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* lolevel.scm:531: ##sys#make-vector */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[89]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[89]+1);
av2[1]=t3;
av2[2]=t2;
tp(3,av2);}}

/* k2403 in k2397 in chicken.memory.representation#record->vector in k2391 in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2405(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_2405,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2410,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word)li64),tmp=(C_word)a,a+=6,tmp);
t3=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t3;
av2[1]=(
  f_2410(t2,C_fix(0))
);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* doloop735 in k2403 in k2397 in chicken.memory.representation#record->vector in k2391 in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static C_word C_fcall f_2410(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_stack_overflow_check;
loop:{}
if(C_truep(C_fixnum_greater_or_equal_p(t1,((C_word*)t0)[2]))){
return(((C_word*)t0)[3]);}
else{
t2=C_slot(((C_word*)t0)[4],t1);
t3=C_i_setslot(((C_word*)t0)[3],t1,t2);
t5=C_fixnum_plus(t1,C_fix(1));
t1=t5;
goto loop;}}

/* chicken.memory.representation#object-become! in k2391 in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2433(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_2433,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2437,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* lolevel.scm:540: ##sys#check-become-alist */
t4=*((C_word*)lf[4]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=lf[126];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k2435 in chicken.memory.representation#object-become! in k2391 in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2437(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_2437,c,av);}
/* lolevel.scm:541: ##sys#become! */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[125]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[125]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
tp(3,av2);}}

/* chicken.memory.representation#mutate-procedure! in k2391 in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2442(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_2442,c,av);}
a=C_alloc(5);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2446,a[2]=t2,a[3]=t1,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* lolevel.scm:544: ##sys#check-closure */
t5=*((C_word*)lf[94]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
av2[3]=lf[128];
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k2444 in chicken.memory.representation#mutate-procedure! in k2391 in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2446(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_2446,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2449,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* lolevel.scm:545: ##sys#check-closure */
t3=*((C_word*)lf[94]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=lf[128];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k2447 in k2444 in chicken.memory.representation#mutate-procedure! in k2391 in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2449(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_2449,c,av);}
a=C_alloc(5);
t2=C_block_size(((C_word*)t0)[2]);
t3=C_words(t2);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2456,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* lolevel.scm:548: ##sys#make-vector */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[89]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[89]+1);
av2[1]=t4;
av2[2]=t3;
tp(3,av2);}}

/* k2454 in k2447 in k2444 in chicken.memory.representation#mutate-procedure! in k2391 in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2456(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_2456,c,av);}
a=C_alloc(8);
t2=C_copy_block(((C_word*)t0)[2],t1);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2459,a[2]=((C_word*)t0)[3],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2471,a[2]=((C_word*)t0)[2],a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* lolevel.scm:549: proc */
t5=((C_word*)t0)[4];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k2457 in k2454 in k2447 in k2444 in chicken.memory.representation#mutate-procedure! in k2391 in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2459(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2459,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k2469 in k2454 in k2447 in k2444 in chicken.memory.representation#mutate-procedure! in k2391 in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2471(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_2471,c,av);}
a=C_alloc(6);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_list1(&a,1,t2);
/* lolevel.scm:549: ##sys#become! */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[125]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[125]+1);
av2[1]=((C_word*)t0)[3];
av2[2]=t3;
tp(3,av2);}}

/* chicken.locative#make-locative in k2391 in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2473(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_2473,c,av);}
if(C_truep(C_rest_nullp(c,3))){
/* lolevel.scm:583: ##sys#make-locative */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[131]+1));
C_word av2[6];
av2[0]=*((C_word*)lf[131]+1);
av2[1]=t1;
av2[2]=t2;
av2[3]=C_fix(0);
av2[4]=C_SCHEME_FALSE;
av2[5]=lf[132];
tp(6,av2);}}
else{
/* lolevel.scm:583: ##sys#make-locative */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[131]+1));
C_word av2[6];
av2[0]=*((C_word*)lf[131]+1);
av2[1]=t1;
av2[2]=t2;
av2[3]=C_get_rest_arg(c,3,av,3,t0);
av2[4]=C_SCHEME_FALSE;
av2[5]=lf[132];
tp(6,av2);}}}

/* chicken.locative#make-weak-locative in k2391 in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2489(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_2489,c,av);}
if(C_truep(C_rest_nullp(c,3))){
/* lolevel.scm:586: ##sys#make-locative */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[131]+1));
C_word av2[6];
av2[0]=*((C_word*)lf[131]+1);
av2[1]=t1;
av2[2]=t2;
av2[3]=C_fix(0);
av2[4]=C_SCHEME_TRUE;
av2[5]=lf[134];
tp(6,av2);}}
else{
/* lolevel.scm:586: ##sys#make-locative */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[131]+1));
C_word av2[6];
av2[0]=*((C_word*)lf[131]+1);
av2[1]=t1;
av2[2]=t2;
av2[3]=C_get_rest_arg(c,3,av,3,t0);
av2[4]=C_SCHEME_TRUE;
av2[5]=lf[134];
tp(6,av2);}}}

/* chicken.locative#locative-set! in k2391 in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2505(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2505,c,av);}
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_i_locative_set(t2,t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k2508 in k2391 in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2510(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2510,c,av);}
a=C_alloc(6);
t2=C_mutate((C_word*)lf[136]+1 /* (set! chicken.locative#locative-ref ...) */,t1);
t3=C_mutate((C_word*)lf[137]+1 /* (set! chicken.locative#locative->object ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2512,a[2]=((C_word)li71),tmp=(C_word)a,a+=3,tmp));
t4=C_mutate((C_word*)lf[138]+1 /* (set! chicken.locative#locative? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2515,a[2]=((C_word)li72),tmp=(C_word)a,a+=3,tmp));
t5=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* chicken.locative#locative->object in k2508 in k2391 in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2512(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2512,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_locative_to_object(t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.locative#locative? in k2508 in k2391 in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2515(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2515,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=(C_truep(C_blockp(t2))?C_locativep(t2):C_SCHEME_FALSE);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* a2520 in k2391 in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2521(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_2521,c,av);}
a=C_alloc(6);
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_locative_ref(&a,1,t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* a2523 in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2524(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_2524,c,av);}
a=C_alloc(5);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2528,a[2]=t1,a[3]=t2,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* lolevel.scm:522: ##sys#check-generic-structure */
t5=*((C_word*)lf[8]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
av2[3]=lf[141];
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k2526 in a2523 in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2528(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,5)))){
C_save_and_reclaim((void *)f_2528,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2531,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=C_block_size(((C_word*)t0)[3]);
/* lolevel.scm:523: ##sys#check-range */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[83]+1));
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=*((C_word*)lf[83]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=C_fix(0);
av2[4]=C_fixnum_difference(t3,C_fix(1));
av2[5]=lf[141];
tp(6,av2);}}

/* k2529 in k2526 in a2523 in k2264 in k2026 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2531(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2531,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_slot(((C_word*)t0)[3],C_fixnum_plus(((C_word*)t0)[4],C_fix(1)));
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a2547 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2548(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_2548,c,av);}
a=C_alloc(5);
t4=C_i_check_structure_2(t2,lf[72],lf[81]);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2555,a[2]=t2,a[3]=t1,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* lolevel.scm:395: ##sys#check-range */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[83]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[83]+1);
av2[1]=t5;
av2[2]=t3;
av2[3]=C_fix(0);
av2[4]=C_slot(t2,C_fix(1));
tp(5,av2);}}

/* k2553 in a2547 in k1822 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2555(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,1)))){
C_save_and_reclaim((void *)f_2555,c,av);}
a=C_alloc(5);
t2=C_slot(((C_word*)t0)[2],C_fix(2));
t3=C_a_i_bytevector(&a,1,C_fix(3));
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=stub534(t3,t2,C_i_foreign_fixnum_argumentp(((C_word*)t0)[4]));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* a2567 in k1818 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2568(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,1)))){
C_save_and_reclaim((void *)f_2568,c,av);}
a=C_alloc(4);
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_u_i_pointer_f64_ref(&a,1,t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* a2570 in k1814 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2571(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,1)))){
C_save_and_reclaim((void *)f_2571,c,av);}
a=C_alloc(4);
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_u_i_pointer_f32_ref(&a,1,t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* a2573 in k1810 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2574(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,1)))){
C_save_and_reclaim((void *)f_2574,c,av);}
a=C_alloc(7);
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_u_i_pointer_s64_ref(&a,1,t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* a2576 in k1806 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2577(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,1)))){
C_save_and_reclaim((void *)f_2577,c,av);}
a=C_alloc(7);
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_u_i_pointer_u64_ref(&a,1,t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* a2579 in k1802 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2580(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_2580,c,av);}
a=C_alloc(6);
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_u_i_pointer_s32_ref(&a,1,t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* a2582 in k1798 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2583(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_2583,c,av);}
a=C_alloc(6);
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_u_i_pointer_u32_ref(&a,1,t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* a2585 in k1794 in k1790 in k1786 in k934 */
static void C_ccall f_2586(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2586,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_u_i_pointer_s16_ref(t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* a2588 in k1790 in k1786 in k934 */
static void C_ccall f_2589(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2589,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_u_i_pointer_u16_ref(t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* a2591 in k1786 in k934 */
static void C_ccall f_2592(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2592,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_u_i_pointer_s8_ref(t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* a2594 in k934 */
static void C_ccall f_2595(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2595,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_u_i_pointer_u8_ref(t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k934 */
static void C_ccall f_936(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word t24;
C_word t25;
C_word t26;
C_word t27;
C_word t28;
C_word t29;
C_word t30;
C_word t31;
C_word t32;
C_word t33;
C_word t34;
C_word t35;
C_word t36;
C_word t37;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(113,c,4)))){
C_save_and_reclaim((void *)f_936,c,av);}
a=C_alloc(113);
t2=C_a_i_provide(&a,1,lf[0]);
t3=C_a_i_provide(&a,1,lf[1]);
t4=C_mutate((C_word*)lf[2]+1 /* (set! ##sys#check-block ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_938,a[2]=((C_word)li0),tmp=(C_word)a,a+=3,tmp));
t5=C_mutate((C_word*)lf[4]+1 /* (set! ##sys#check-become-alist ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_956,a[2]=((C_word)li2),tmp=(C_word)a,a+=3,tmp));
t6=C_mutate((C_word*)lf[8]+1 /* (set! ##sys#check-generic-structure ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1002,a[2]=((C_word)li3),tmp=(C_word)a,a+=3,tmp));
t7=C_mutate((C_word*)lf[10]+1 /* (set! ##sys#check-generic-vector ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1027,a[2]=((C_word)li4),tmp=(C_word)a,a+=3,tmp));
t8=C_mutate((C_word*)lf[12]+1 /* (set! ##sys#check-pointer ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1059,a[2]=((C_word)li5),tmp=(C_word)a,a+=3,tmp));
t9=lf[14];
t10=C_mutate((C_word*)lf[15]+1 /* (set! chicken.memory#move-memory! ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1200,a[2]=t9,a[3]=((C_word)li11),tmp=(C_word)a,a+=4,tmp));
t11=C_mutate((C_word*)lf[23]+1 /* (set! chicken.memory#allocate ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1543,a[2]=((C_word)li12),tmp=(C_word)a,a+=3,tmp));
t12=C_mutate((C_word*)lf[24]+1 /* (set! chicken.memory#free ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1550,a[2]=((C_word)li13),tmp=(C_word)a,a+=3,tmp));
t13=C_mutate((C_word*)lf[25]+1 /* (set! chicken.memory#pointer? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1560,a[2]=((C_word)li14),tmp=(C_word)a,a+=3,tmp));
t14=C_mutate((C_word*)lf[26]+1 /* (set! chicken.memory#pointer-like? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1568,a[2]=((C_word)li15),tmp=(C_word)a,a+=3,tmp));
t15=C_mutate((C_word*)lf[27]+1 /* (set! chicken.memory#address->pointer ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1579,a[2]=((C_word)li16),tmp=(C_word)a,a+=3,tmp));
t16=C_mutate((C_word*)lf[31]+1 /* (set! chicken.memory#pointer->address ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1588,a[2]=((C_word)li17),tmp=(C_word)a,a+=3,tmp));
t17=C_mutate((C_word*)lf[35]+1 /* (set! chicken.memory#object->pointer ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1597,a[2]=((C_word)li18),tmp=(C_word)a,a+=3,tmp));
t18=C_mutate((C_word*)lf[36]+1 /* (set! chicken.memory#pointer->object ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1608,a[2]=((C_word)li19),tmp=(C_word)a,a+=3,tmp));
t19=C_mutate((C_word*)lf[38]+1 /* (set! chicken.memory#pointer=? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1614,a[2]=((C_word)li20),tmp=(C_word)a,a+=3,tmp));
t20=C_mutate((C_word*)lf[40]+1 /* (set! chicken.memory#pointer+ ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1623,a[2]=((C_word)li21),tmp=(C_word)a,a+=3,tmp));
t21=C_mutate((C_word*)lf[41]+1 /* (set! chicken.memory#align-to-word ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1645,a[2]=((C_word)li22),tmp=(C_word)a,a+=3,tmp));
t22=C_mutate((C_word*)lf[44]+1 /* (set! chicken.memory#tag-pointer ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1680,a[2]=((C_word)li23),tmp=(C_word)a,a+=3,tmp));
t23=C_mutate((C_word*)lf[47]+1 /* (set! chicken.memory#tagged-pointer? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1701,a[2]=((C_word)li24),tmp=(C_word)a,a+=3,tmp));
t24=C_mutate((C_word*)lf[48]+1 /* (set! chicken.memory#pointer-tag ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1732,a[2]=((C_word)li25),tmp=(C_word)a,a+=3,tmp));
t25=C_mutate((C_word*)lf[50]+1 /* (set! chicken.memory#pointer-u8-set! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1756,a[2]=((C_word)li26),tmp=(C_word)a,a+=3,tmp));
t26=C_mutate((C_word*)lf[51]+1 /* (set! chicken.memory#pointer-s8-set! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1759,a[2]=((C_word)li27),tmp=(C_word)a,a+=3,tmp));
t27=C_mutate((C_word*)lf[52]+1 /* (set! chicken.memory#pointer-u16-set! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1762,a[2]=((C_word)li28),tmp=(C_word)a,a+=3,tmp));
t28=C_mutate((C_word*)lf[53]+1 /* (set! chicken.memory#pointer-s16-set! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1765,a[2]=((C_word)li29),tmp=(C_word)a,a+=3,tmp));
t29=C_mutate((C_word*)lf[54]+1 /* (set! chicken.memory#pointer-u32-set! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1768,a[2]=((C_word)li30),tmp=(C_word)a,a+=3,tmp));
t30=C_mutate((C_word*)lf[55]+1 /* (set! chicken.memory#pointer-s32-set! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1771,a[2]=((C_word)li31),tmp=(C_word)a,a+=3,tmp));
t31=C_mutate((C_word*)lf[56]+1 /* (set! chicken.memory#pointer-u64-set! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1774,a[2]=((C_word)li32),tmp=(C_word)a,a+=3,tmp));
t32=C_mutate((C_word*)lf[57]+1 /* (set! chicken.memory#pointer-s64-set! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1777,a[2]=((C_word)li33),tmp=(C_word)a,a+=3,tmp));
t33=C_mutate((C_word*)lf[58]+1 /* (set! chicken.memory#pointer-f32-set! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1780,a[2]=((C_word)li34),tmp=(C_word)a,a+=3,tmp));
t34=C_mutate((C_word*)lf[59]+1 /* (set! chicken.memory#pointer-f64-set! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1783,a[2]=((C_word)li35),tmp=(C_word)a,a+=3,tmp));
t35=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1788,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t36=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2595,a[2]=((C_word)li85),tmp=(C_word)a,a+=3,tmp);
/* lolevel.scm:277: chicken.base#getter-with-setter */
t37=*((C_word*)lf[139]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t37;
av2[1]=t35;
av2[2]=t36;
av2[3]=*((C_word*)lf[50]+1);
av2[4]=lf[155];
((C_proc)(void*)(*((C_word*)t37+1)))(5,av2);}}

/* ##sys#check-block in k934 */
static void C_ccall f_938(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +0,c,4)))){
C_save_and_reclaim((void*)f_938,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+0);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
if(C_truep(C_blockp(t2))){
t4=C_SCHEME_UNDEFINED;
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t4=C_fix((C_word)C_BAD_ARGUMENT_TYPE_NO_BLOCK_ERROR);
if(C_truep(C_i_pairp(t3))){
/* lolevel.scm:84: ##sys#error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[3]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[3]+1);
av2[1]=t1;
av2[2]=t4;
av2[3]=C_get_rest_arg(c,3,av,3,t0);
av2[4]=t2;
tp(5,av2);}}
else{
/* lolevel.scm:84: ##sys#error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[3]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[3]+1);
av2[1]=t1;
av2[2]=t4;
av2[3]=C_SCHEME_FALSE;
av2[4]=t2;
tp(5,av2);}}}}

/* ##sys#check-become-alist in k934 */
static void C_ccall f_956(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_956,c,av);}
a=C_alloc(8);
t4=C_i_check_list_2(t2,t3);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_965,a[2]=t3,a[3]=t6,a[4]=t2,a[5]=((C_word)li1),tmp=(C_word)a,a+=6,tmp));
t8=((C_word*)t6)[1];
f_965(t8,t1,t2);}

/* loop in ##sys#check-become-alist in k934 */
static void C_fcall f_965(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,5)))){
C_save_and_reclaim_args((void *)trf_965,3,t0,t1,t2);}
a=C_alloc(7);
t3=C_i_nullp(t2);
if(C_truep(t3)){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
if(C_truep(C_i_pairp(t2))){
t4=C_u_i_car(t2);
t5=C_i_check_pair_2(t4,((C_word*)t0)[2]);
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_985,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t2,a[5]=t4,a[6]=((C_word*)t0)[2],tmp=(C_word)a,a+=7,tmp);
/* lolevel.scm:95: ##sys#check-block */
t7=*((C_word*)lf[2]+1);{
C_word av2[4];
av2[0]=t7;
av2[1]=t6;
av2[2]=C_u_i_car(t4);
av2[3]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}
else{
/* lolevel.scm:99: ##sys#signal-hook */
t4=*((C_word*)lf[5]+1);{
C_word av2[6];
av2[0]=t4;
av2[1]=t1;
av2[2]=lf[6];
av2[3]=((C_word*)t0)[2];
av2[4]=lf[7];
av2[5]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(6,av2);}}}}

/* k983 in loop in ##sys#check-become-alist in k934 */
static void C_ccall f_985(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_985,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_988,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* lolevel.scm:96: ##sys#check-block */
t3=*((C_word*)lf[2]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_u_i_cdr(((C_word*)t0)[5]);
av2[3]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k986 in k983 in loop in ##sys#check-become-alist in k934 */
static void C_ccall f_988(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_988,c,av);}
/* lolevel.scm:97: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_965(t2,((C_word*)t0)[3],C_u_i_cdr(((C_word*)t0)[4]));}

/* toplevel */
static C_TLS int toplevel_initialized=0;

void C_ccall C_lolevel_toplevel(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(toplevel_initialized) {C_kontinue(t1,C_SCHEME_UNDEFINED);}
else C_toplevel_entry(C_text("lolevel"));
C_check_nursery_minimum(C_calculate_demand(3,c,2));
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void*)C_lolevel_toplevel,c,av);}
toplevel_initialized=1;
if(C_unlikely(!C_demand_2(1013))){
C_save(t1);
C_rereclaim2(1013*sizeof(C_word),1);
t1=C_restore;}
a=C_alloc(3);
C_initialize_lf(lf,156);
lf[0]=C_h_intern(&lf[0],7, C_text("lolevel"));
lf[1]=C_h_intern(&lf[1],15, C_text("chicken.memory#"));
lf[2]=C_h_intern(&lf[2],17, C_text("##sys#check-block"));
lf[3]=C_h_intern(&lf[3],16, C_text("##sys#error-hook"));
lf[4]=C_h_intern(&lf[4],24, C_text("##sys#check-become-alist"));
lf[5]=C_h_intern(&lf[5],17, C_text("##sys#signal-hook"));
lf[6]=C_h_intern_kw(&lf[6],10, C_text("type-error"));
lf[7]=C_decode_literal(C_heaptop,C_text("\376B\000\000:bad argument type - not an a-list of non-immediate objects"));
lf[8]=C_h_intern(&lf[8],29, C_text("##sys#check-generic-structure"));
lf[9]=C_decode_literal(C_heaptop,C_text("\376B\000\000#bad argument type - not a structure"));
lf[10]=C_h_intern(&lf[10],26, C_text("##sys#check-generic-vector"));
lf[11]=C_decode_literal(C_heaptop,C_text("\376B\000\000,bad argument type - not a vector-like object"));
lf[12]=C_h_intern(&lf[12],19, C_text("##sys#check-pointer"));
lf[13]=C_decode_literal(C_heaptop,C_text("\376B\000\000!bad argument type - not a pointer"));
lf[14]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\004\001mmap\376\003\000\000\002\376\001\000\000\010\001u8vector\376\003\000\000\002\376\001\000\000\011\001u16vector\376\003\000\000\002\376\001\000\000\011\001u32vector\376\003\000\000\002\376"
"\001\000\000\011\001u64vector\376\003\000\000\002\376\001\000\000\010\001s8vector\376\003\000\000\002\376\001\000\000\011\001s16vector\376\003\000\000\002\376\001\000\000\011\001s32vector\376\003\000\000\002\376\001"
"\000\000\011\001s64vector\376\003\000\000\002\376\001\000\000\011\001f32vector\376\003\000\000\002\376\001\000\000\011\001f64vector\376\377\016"));
lf[15]=C_h_intern(&lf[15],27, C_text("chicken.memory#move-memory!"));
lf[16]=C_h_intern(&lf[16],11, C_text("##sys#error"));
lf[17]=C_h_intern(&lf[17],12, C_text("move-memory!"));
lf[18]=C_decode_literal(C_heaptop,C_text("\376B\000\000\034need number of bytes to move"));
lf[19]=C_decode_literal(C_heaptop,C_text("\376B\000\000!number of bytes to move too large"));
lf[20]=C_h_intern(&lf[20],17, C_text("##sys#bytevector\077"));
lf[21]=C_decode_literal(C_heaptop,C_text("\376B\000\000\033negative destination offset"));
lf[22]=C_decode_literal(C_heaptop,C_text("\376B\000\000\026negative source offset"));
lf[23]=C_h_intern(&lf[23],23, C_text("chicken.memory#allocate"));
lf[24]=C_h_intern(&lf[24],19, C_text("chicken.memory#free"));
lf[25]=C_h_intern(&lf[25],23, C_text("chicken.memory#pointer\077"));
lf[26]=C_h_intern(&lf[26],28, C_text("chicken.memory#pointer-like\077"));
lf[27]=C_h_intern(&lf[27],31, C_text("chicken.memory#address->pointer"));
lf[28]=C_h_intern(&lf[28],22, C_text("##sys#address->pointer"));
lf[29]=C_h_intern(&lf[29],19, C_text("##sys#check-integer"));
lf[30]=C_h_intern(&lf[30],16, C_text("address->pointer"));
lf[31]=C_h_intern(&lf[31],31, C_text("chicken.memory#pointer->address"));
lf[32]=C_h_intern(&lf[32],22, C_text("##sys#pointer->address"));
lf[33]=C_h_intern(&lf[33],19, C_text("##sys#check-special"));
lf[34]=C_h_intern(&lf[34],16, C_text("pointer->address"));
lf[35]=C_h_intern(&lf[35],30, C_text("chicken.memory#object->pointer"));
lf[36]=C_h_intern(&lf[36],30, C_text("chicken.memory#pointer->object"));
lf[37]=C_h_intern(&lf[37],15, C_text("pointer->object"));
lf[38]=C_h_intern(&lf[38],24, C_text("chicken.memory#pointer=\077"));
lf[39]=C_h_intern(&lf[39],9, C_text("pointer=\077"));
lf[40]=C_h_intern(&lf[40],23, C_text("chicken.memory#pointer+"));
lf[41]=C_h_intern(&lf[41],28, C_text("chicken.memory#align-to-word"));
lf[42]=C_h_intern(&lf[42],13, C_text("align-to-word"));
lf[43]=C_decode_literal(C_heaptop,C_text("\376B\000\000,bad argument type - not a pointer or integer"));
lf[44]=C_h_intern(&lf[44],26, C_text("chicken.memory#tag-pointer"));
lf[45]=C_h_intern(&lf[45],11, C_text("tag-pointer"));
lf[46]=C_h_intern(&lf[46],25, C_text("##sys#make-tagged-pointer"));
lf[47]=C_h_intern(&lf[47],30, C_text("chicken.memory#tagged-pointer\077"));
lf[48]=C_h_intern(&lf[48],26, C_text("chicken.memory#pointer-tag"));
lf[49]=C_h_intern(&lf[49],11, C_text("pointer-tag"));
lf[50]=C_h_intern(&lf[50],30, C_text("chicken.memory#pointer-u8-set!"));
lf[51]=C_h_intern(&lf[51],30, C_text("chicken.memory#pointer-s8-set!"));
lf[52]=C_h_intern(&lf[52],31, C_text("chicken.memory#pointer-u16-set!"));
lf[53]=C_h_intern(&lf[53],31, C_text("chicken.memory#pointer-s16-set!"));
lf[54]=C_h_intern(&lf[54],31, C_text("chicken.memory#pointer-u32-set!"));
lf[55]=C_h_intern(&lf[55],31, C_text("chicken.memory#pointer-s32-set!"));
lf[56]=C_h_intern(&lf[56],31, C_text("chicken.memory#pointer-u64-set!"));
lf[57]=C_h_intern(&lf[57],31, C_text("chicken.memory#pointer-s64-set!"));
lf[58]=C_h_intern(&lf[58],31, C_text("chicken.memory#pointer-f32-set!"));
lf[59]=C_h_intern(&lf[59],31, C_text("chicken.memory#pointer-f64-set!"));
lf[60]=C_h_intern(&lf[60],29, C_text("chicken.memory#pointer-u8-ref"));
lf[61]=C_h_intern(&lf[61],29, C_text("chicken.memory#pointer-s8-ref"));
lf[62]=C_h_intern(&lf[62],30, C_text("chicken.memory#pointer-u16-ref"));
lf[63]=C_h_intern(&lf[63],30, C_text("chicken.memory#pointer-s16-ref"));
lf[64]=C_h_intern(&lf[64],30, C_text("chicken.memory#pointer-u32-ref"));
lf[65]=C_h_intern(&lf[65],30, C_text("chicken.memory#pointer-s32-ref"));
lf[66]=C_h_intern(&lf[66],30, C_text("chicken.memory#pointer-u64-ref"));
lf[67]=C_h_intern(&lf[67],30, C_text("chicken.memory#pointer-s64-ref"));
lf[68]=C_h_intern(&lf[68],30, C_text("chicken.memory#pointer-f32-ref"));
lf[69]=C_h_intern(&lf[69],30, C_text("chicken.memory#pointer-f64-ref"));
lf[70]=C_h_intern(&lf[70],5, C_text("unset"));
lf[71]=C_h_intern(&lf[71],34, C_text("chicken.memory#make-pointer-vector"));
lf[72]=C_h_intern(&lf[72],14, C_text("pointer-vector"));
lf[73]=C_h_intern(&lf[73],19, C_text("make-pointer-vector"));
lf[74]=C_h_intern(&lf[74],15, C_text("##sys#make-blob"));
lf[75]=C_h_intern(&lf[75],18, C_text("##sys#check-fixnum"));
lf[76]=C_h_intern(&lf[76],30, C_text("chicken.memory#pointer-vector\077"));
lf[77]=C_h_intern(&lf[77],29, C_text("chicken.memory#pointer-vector"));
lf[78]=C_h_intern(&lf[78],35, C_text("chicken.memory#pointer-vector-fill!"));
lf[79]=C_h_intern(&lf[79],20, C_text("pointer-vector-fill!"));
lf[80]=C_h_intern(&lf[80],34, C_text("chicken.memory#pointer-vector-set!"));
lf[81]=C_h_intern(&lf[81],18, C_text("pointer-vector-ref"));
lf[82]=C_h_intern(&lf[82],19, C_text("pointer-vector-set!"));
lf[83]=C_h_intern(&lf[83],17, C_text("##sys#check-range"));
lf[84]=C_h_intern(&lf[84],33, C_text("chicken.memory#pointer-vector-ref"));
lf[85]=C_h_intern(&lf[85],36, C_text("chicken.memory#pointer-vector-length"));
lf[86]=C_h_intern(&lf[86],21, C_text("pointer-vector-length"));
lf[87]=C_h_intern(&lf[87],30, C_text("chicken.memory.representation#"));
lf[88]=C_h_intern(&lf[88],41, C_text("chicken.memory.representation#object-copy"));
lf[89]=C_h_intern(&lf[89],17, C_text("##sys#make-vector"));
lf[90]=C_h_intern(&lf[90],8, C_text("extended"));
lf[92]=C_h_intern(&lf[92],46, C_text("chicken.memory.representation#extend-procedure"));
lf[93]=C_h_intern(&lf[93],21, C_text("##sys#decorate-lambda"));
lf[94]=C_h_intern(&lf[94],19, C_text("##sys#check-closure"));
lf[95]=C_h_intern(&lf[95],16, C_text("extend-procedure"));
lf[96]=C_h_intern(&lf[96],49, C_text("chicken.memory.representation#extended-procedure\077"));
lf[97]=C_h_intern(&lf[97],23, C_text("##sys#lambda-decoration"));
lf[98]=C_h_intern(&lf[98],44, C_text("chicken.memory.representation#procedure-data"));
lf[99]=C_h_intern(&lf[99],49, C_text("chicken.memory.representation#set-procedure-data!"));
lf[100]=C_h_intern(&lf[100],19, C_text("set-procedure-data!"));
lf[101]=C_decode_literal(C_heaptop,C_text("\376B\000\000-bad argument type - not an extended procedure"));
lf[102]=C_h_intern(&lf[102],42, C_text("chicken.memory.representation#vector-like\077"));
lf[103]=C_h_intern(&lf[103],40, C_text("chicken.memory.representation#block-set!"));
lf[104]=C_h_intern(&lf[104],16, C_text("##sys#block-set!"));
lf[105]=C_h_intern(&lf[105],39, C_text("chicken.memory.representation#block-ref"));
lf[106]=C_h_intern(&lf[106],45, C_text("chicken.memory.representation#number-of-slots"));
lf[107]=C_h_intern(&lf[107],15, C_text("number-of-slots"));
lf[108]=C_h_intern(&lf[108],45, C_text("chicken.memory.representation#number-of-bytes"));
lf[109]=C_h_intern(&lf[109],15, C_text("number-of-bytes"));
lf[110]=C_decode_literal(C_heaptop,C_text("\376B\000\0002cannot compute number of bytes of immediate object"));
lf[111]=C_h_intern(&lf[111],50, C_text("chicken.memory.representation#make-record-instance"));
lf[112]=C_h_intern(&lf[112],20, C_text("make-record-instance"));
lf[113]=C_h_intern(&lf[113],20, C_text("##sys#make-structure"));
lf[114]=C_h_intern(&lf[114],46, C_text("chicken.memory.representation#record-instance\077"));
lf[115]=C_h_intern(&lf[115],50, C_text("chicken.memory.representation#record-instance-type"));
lf[116]=C_h_intern(&lf[116],20, C_text("record-instance-type"));
lf[117]=C_h_intern(&lf[117],52, C_text("chicken.memory.representation#record-instance-length"));
lf[118]=C_h_intern(&lf[118],22, C_text("record-instance-length"));
lf[119]=C_h_intern(&lf[119],55, C_text("chicken.memory.representation#record-instance-slot-set!"));
lf[120]=C_h_intern(&lf[120],25, C_text("record-instance-slot-set!"));
lf[121]=C_h_intern(&lf[121],50, C_text("chicken.memory.representation#record-instance-slot"));
lf[122]=C_h_intern(&lf[122],44, C_text("chicken.memory.representation#record->vector"));
lf[123]=C_h_intern(&lf[123],14, C_text("record->vector"));
lf[124]=C_h_intern(&lf[124],44, C_text("chicken.memory.representation#object-become!"));
lf[125]=C_h_intern(&lf[125],13, C_text("##sys#become!"));
lf[126]=C_h_intern(&lf[126],14, C_text("object-become!"));
lf[127]=C_h_intern(&lf[127],47, C_text("chicken.memory.representation#mutate-procedure!"));
lf[128]=C_h_intern(&lf[128],17, C_text("mutate-procedure!"));
lf[129]=C_h_intern(&lf[129],17, C_text("chicken.locative#"));
lf[130]=C_h_intern(&lf[130],30, C_text("chicken.locative#make-locative"));
lf[131]=C_h_intern(&lf[131],19, C_text("##sys#make-locative"));
lf[132]=C_h_intern(&lf[132],13, C_text("make-locative"));
lf[133]=C_h_intern(&lf[133],35, C_text("chicken.locative#make-weak-locative"));
lf[134]=C_h_intern(&lf[134],18, C_text("make-weak-locative"));
lf[135]=C_h_intern(&lf[135],30, C_text("chicken.locative#locative-set!"));
lf[136]=C_h_intern(&lf[136],29, C_text("chicken.locative#locative-ref"));
lf[137]=C_h_intern(&lf[137],33, C_text("chicken.locative#locative->object"));
lf[138]=C_h_intern(&lf[138],26, C_text("chicken.locative#locative\077"));
lf[139]=C_h_intern(&lf[139],31, C_text("chicken.base#getter-with-setter"));
lf[140]=C_decode_literal(C_heaptop,C_text("\376B\000\000#(chicken.locative#locative-ref loc)"));
lf[141]=C_h_intern(&lf[141],20, C_text("record-instance-slot"));
lf[142]=C_decode_literal(C_heaptop,C_text("\376B\000\0008(chicken.memory.representation#record-instance-slot x i)"));
lf[143]=C_h_intern(&lf[143],15, C_text("##sys#block-ref"));
lf[144]=C_decode_literal(C_heaptop,C_text("\376B\000\000-(chicken.memory.representation#block-ref x i)"));
lf[145]=C_decode_literal(C_heaptop,C_text("\376B\000\000((chicken.memory#pointer-vector-ref pv i)"));
lf[146]=C_decode_literal(C_heaptop,C_text("\376B\000\000\042(chicken.memory#pointer-f64-ref p)"));
lf[147]=C_decode_literal(C_heaptop,C_text("\376B\000\000\042(chicken.memory#pointer-f32-ref p)"));
lf[148]=C_decode_literal(C_heaptop,C_text("\376B\000\000\042(chicken.memory#pointer-s64-ref p)"));
lf[149]=C_decode_literal(C_heaptop,C_text("\376B\000\000\042(chicken.memory#pointer-u64-ref p)"));
lf[150]=C_decode_literal(C_heaptop,C_text("\376B\000\000\042(chicken.memory#pointer-s32-ref p)"));
lf[151]=C_decode_literal(C_heaptop,C_text("\376B\000\000\042(chicken.memory#pointer-u32-ref p)"));
lf[152]=C_decode_literal(C_heaptop,C_text("\376B\000\000\042(chicken.memory#pointer-s16-ref p)"));
lf[153]=C_decode_literal(C_heaptop,C_text("\376B\000\000\042(chicken.memory#pointer-u16-ref p)"));
lf[154]=C_decode_literal(C_heaptop,C_text("\376B\000\000!(chicken.memory#pointer-s8-ref p)"));
lf[155]=C_decode_literal(C_heaptop,C_text("\376B\000\000!(chicken.memory#pointer-u8-ref p)"));
C_register_lf2(lf,156,create_ptable());{}
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_936,a[2]=t1,tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_library_toplevel(2,av2);}}

#ifdef C_ENABLE_PTABLES
static C_PTABLE_ENTRY ptable[157] = {
{C_text("f_1002:lolevel_2escm"),(void*)f_1002},
{C_text("f_1027:lolevel_2escm"),(void*)f_1027},
{C_text("f_1046:lolevel_2escm"),(void*)f_1046},
{C_text("f_1059:lolevel_2escm"),(void*)f_1059},
{C_text("f_1200:lolevel_2escm"),(void*)f_1200},
{C_text("f_1221:lolevel_2escm"),(void*)f_1221},
{C_text("f_1227:lolevel_2escm"),(void*)f_1227},
{C_text("f_1233:lolevel_2escm"),(void*)f_1233},
{C_text("f_1249:lolevel_2escm"),(void*)f_1249},
{C_text("f_1276:lolevel_2escm"),(void*)f_1276},
{C_text("f_1279:lolevel_2escm"),(void*)f_1279},
{C_text("f_1282:lolevel_2escm"),(void*)f_1282},
{C_text("f_1285:lolevel_2escm"),(void*)f_1285},
{C_text("f_1290:lolevel_2escm"),(void*)f_1290},
{C_text("f_1382:lolevel_2escm"),(void*)f_1382},
{C_text("f_1391:lolevel_2escm"),(void*)f_1391},
{C_text("f_1401:lolevel_2escm"),(void*)f_1401},
{C_text("f_1405:lolevel_2escm"),(void*)f_1405},
{C_text("f_1424:lolevel_2escm"),(void*)f_1424},
{C_text("f_1453:lolevel_2escm"),(void*)f_1453},
{C_text("f_1463:lolevel_2escm"),(void*)f_1463},
{C_text("f_1473:lolevel_2escm"),(void*)f_1473},
{C_text("f_1543:lolevel_2escm"),(void*)f_1543},
{C_text("f_1550:lolevel_2escm"),(void*)f_1550},
{C_text("f_1560:lolevel_2escm"),(void*)f_1560},
{C_text("f_1568:lolevel_2escm"),(void*)f_1568},
{C_text("f_1579:lolevel_2escm"),(void*)f_1579},
{C_text("f_1583:lolevel_2escm"),(void*)f_1583},
{C_text("f_1588:lolevel_2escm"),(void*)f_1588},
{C_text("f_1592:lolevel_2escm"),(void*)f_1592},
{C_text("f_1597:lolevel_2escm"),(void*)f_1597},
{C_text("f_1608:lolevel_2escm"),(void*)f_1608},
{C_text("f_1612:lolevel_2escm"),(void*)f_1612},
{C_text("f_1614:lolevel_2escm"),(void*)f_1614},
{C_text("f_1618:lolevel_2escm"),(void*)f_1618},
{C_text("f_1621:lolevel_2escm"),(void*)f_1621},
{C_text("f_1623:lolevel_2escm"),(void*)f_1623},
{C_text("f_1645:lolevel_2escm"),(void*)f_1645},
{C_text("f_1675:lolevel_2escm"),(void*)f_1675},
{C_text("f_1680:lolevel_2escm"),(void*)f_1680},
{C_text("f_1684:lolevel_2escm"),(void*)f_1684},
{C_text("f_1687:lolevel_2escm"),(void*)f_1687},
{C_text("f_1701:lolevel_2escm"),(void*)f_1701},
{C_text("f_1732:lolevel_2escm"),(void*)f_1732},
{C_text("f_1756:lolevel_2escm"),(void*)f_1756},
{C_text("f_1759:lolevel_2escm"),(void*)f_1759},
{C_text("f_1762:lolevel_2escm"),(void*)f_1762},
{C_text("f_1765:lolevel_2escm"),(void*)f_1765},
{C_text("f_1768:lolevel_2escm"),(void*)f_1768},
{C_text("f_1771:lolevel_2escm"),(void*)f_1771},
{C_text("f_1774:lolevel_2escm"),(void*)f_1774},
{C_text("f_1777:lolevel_2escm"),(void*)f_1777},
{C_text("f_1780:lolevel_2escm"),(void*)f_1780},
{C_text("f_1783:lolevel_2escm"),(void*)f_1783},
{C_text("f_1788:lolevel_2escm"),(void*)f_1788},
{C_text("f_1792:lolevel_2escm"),(void*)f_1792},
{C_text("f_1796:lolevel_2escm"),(void*)f_1796},
{C_text("f_1800:lolevel_2escm"),(void*)f_1800},
{C_text("f_1804:lolevel_2escm"),(void*)f_1804},
{C_text("f_1808:lolevel_2escm"),(void*)f_1808},
{C_text("f_1812:lolevel_2escm"),(void*)f_1812},
{C_text("f_1816:lolevel_2escm"),(void*)f_1816},
{C_text("f_1820:lolevel_2escm"),(void*)f_1820},
{C_text("f_1824:lolevel_2escm"),(void*)f_1824},
{C_text("f_1829:lolevel_2escm"),(void*)f_1829},
{C_text("f_1836:lolevel_2escm"),(void*)f_1836},
{C_text("f_1848:lolevel_2escm"),(void*)f_1848},
{C_text("f_1860:lolevel_2escm"),(void*)f_1860},
{C_text("f_1865:lolevel_2escm"),(void*)f_1865},
{C_text("f_1893:lolevel_2escm"),(void*)f_1893},
{C_text("f_1899:lolevel_2escm"),(void*)f_1899},
{C_text("f_1904:lolevel_2escm"),(void*)f_1904},
{C_text("f_1912:lolevel_2escm"),(void*)f_1912},
{C_text("f_1925:lolevel_2escm"),(void*)f_1925},
{C_text("f_1939:lolevel_2escm"),(void*)f_1939},
{C_text("f_1946:lolevel_2escm"),(void*)f_1946},
{C_text("f_1957:lolevel_2escm"),(void*)f_1957},
{C_text("f_2000:lolevel_2escm"),(void*)f_2000},
{C_text("f_2007:lolevel_2escm"),(void*)f_2007},
{C_text("f_2010:lolevel_2escm"),(void*)f_2010},
{C_text("f_2028:lolevel_2escm"),(void*)f_2028},
{C_text("f_2030:lolevel_2escm"),(void*)f_2030},
{C_text("f_2039:lolevel_2escm"),(void*)f_2039},
{C_text("f_2045:lolevel_2escm"),(void*)f_2045},
{C_text("f_2075:lolevel_2escm"),(void*)f_2075},
{C_text("f_2078:lolevel_2escm"),(void*)f_2078},
{C_text("f_2087:lolevel_2escm"),(void*)f_2087},
{C_text("f_2108:lolevel_2escm"),(void*)f_2108},
{C_text("f_2119:lolevel_2escm"),(void*)f_2119},
{C_text("f_2123:lolevel_2escm"),(void*)f_2123},
{C_text("f_2128:lolevel_2escm"),(void*)f_2128},
{C_text("f_2144:lolevel_2escm"),(void*)f_2144},
{C_text("f_2154:lolevel_2escm"),(void*)f_2154},
{C_text("f_2171:lolevel_2escm"),(void*)f_2171},
{C_text("f_2188:lolevel_2escm"),(void*)f_2188},
{C_text("f_2190:lolevel_2escm"),(void*)f_2190},
{C_text("f_2204:lolevel_2escm"),(void*)f_2204},
{C_text("f_2221:lolevel_2escm"),(void*)f_2221},
{C_text("f_2229:lolevel_2escm"),(void*)f_2229},
{C_text("f_2243:lolevel_2escm"),(void*)f_2243},
{C_text("f_2245:lolevel_2escm"),(void*)f_2245},
{C_text("f_2266:lolevel_2escm"),(void*)f_2266},
{C_text("f_2268:lolevel_2escm"),(void*)f_2268},
{C_text("f_2272:lolevel_2escm"),(void*)f_2272},
{C_text("f_2277:lolevel_2escm"),(void*)f_2277},
{C_text("f_2300:lolevel_2escm"),(void*)f_2300},
{C_text("f_2309:lolevel_2escm"),(void*)f_2309},
{C_text("f_2345:lolevel_2escm"),(void*)f_2345},
{C_text("f_2349:lolevel_2escm"),(void*)f_2349},
{C_text("f_2354:lolevel_2escm"),(void*)f_2354},
{C_text("f_2358:lolevel_2escm"),(void*)f_2358},
{C_text("f_2367:lolevel_2escm"),(void*)f_2367},
{C_text("f_2371:lolevel_2escm"),(void*)f_2371},
{C_text("f_2374:lolevel_2escm"),(void*)f_2374},
{C_text("f_2393:lolevel_2escm"),(void*)f_2393},
{C_text("f_2395:lolevel_2escm"),(void*)f_2395},
{C_text("f_2399:lolevel_2escm"),(void*)f_2399},
{C_text("f_2405:lolevel_2escm"),(void*)f_2405},
{C_text("f_2410:lolevel_2escm"),(void*)f_2410},
{C_text("f_2433:lolevel_2escm"),(void*)f_2433},
{C_text("f_2437:lolevel_2escm"),(void*)f_2437},
{C_text("f_2442:lolevel_2escm"),(void*)f_2442},
{C_text("f_2446:lolevel_2escm"),(void*)f_2446},
{C_text("f_2449:lolevel_2escm"),(void*)f_2449},
{C_text("f_2456:lolevel_2escm"),(void*)f_2456},
{C_text("f_2459:lolevel_2escm"),(void*)f_2459},
{C_text("f_2471:lolevel_2escm"),(void*)f_2471},
{C_text("f_2473:lolevel_2escm"),(void*)f_2473},
{C_text("f_2489:lolevel_2escm"),(void*)f_2489},
{C_text("f_2505:lolevel_2escm"),(void*)f_2505},
{C_text("f_2510:lolevel_2escm"),(void*)f_2510},
{C_text("f_2512:lolevel_2escm"),(void*)f_2512},
{C_text("f_2515:lolevel_2escm"),(void*)f_2515},
{C_text("f_2521:lolevel_2escm"),(void*)f_2521},
{C_text("f_2524:lolevel_2escm"),(void*)f_2524},
{C_text("f_2528:lolevel_2escm"),(void*)f_2528},
{C_text("f_2531:lolevel_2escm"),(void*)f_2531},
{C_text("f_2548:lolevel_2escm"),(void*)f_2548},
{C_text("f_2555:lolevel_2escm"),(void*)f_2555},
{C_text("f_2568:lolevel_2escm"),(void*)f_2568},
{C_text("f_2571:lolevel_2escm"),(void*)f_2571},
{C_text("f_2574:lolevel_2escm"),(void*)f_2574},
{C_text("f_2577:lolevel_2escm"),(void*)f_2577},
{C_text("f_2580:lolevel_2escm"),(void*)f_2580},
{C_text("f_2583:lolevel_2escm"),(void*)f_2583},
{C_text("f_2586:lolevel_2escm"),(void*)f_2586},
{C_text("f_2589:lolevel_2escm"),(void*)f_2589},
{C_text("f_2592:lolevel_2escm"),(void*)f_2592},
{C_text("f_2595:lolevel_2escm"),(void*)f_2595},
{C_text("f_936:lolevel_2escm"),(void*)f_936},
{C_text("f_938:lolevel_2escm"),(void*)f_938},
{C_text("f_956:lolevel_2escm"),(void*)f_956},
{C_text("f_965:lolevel_2escm"),(void*)f_965},
{C_text("f_985:lolevel_2escm"),(void*)f_985},
{C_text("f_988:lolevel_2escm"),(void*)f_988},
{C_text("toplevel:lolevel_2escm"),(void*)C_lolevel_toplevel},
{NULL,NULL}};
#endif

static C_PTABLE_ENTRY *create_ptable(void){
#ifdef C_ENABLE_PTABLES
return ptable;
#else
return NULL;
#endif
}

/*
o|hiding unexported module binding: chicken.memory#pv-buf-ref 
o|hiding unexported module binding: chicken.memory#pv-buf-set! 
o|hiding unexported module binding: chicken.memory.representation#xproc-tag 
o|eliminated procedure checks: 75 
o|specializations:
o|  1 (scheme#length list)
o|  1 (scheme#integer? *)
o|  3 (scheme#cdr pair)
o|  6 (scheme#car pair)
(o e)|safe calls: 209 
o|safe globals: (chicken.memory#pointer-f64-set! chicken.memory#pointer-f32-set! chicken.memory#pointer-s64-set! chicken.memory#pointer-u64-set! chicken.memory#pointer-s32-set! chicken.memory#pointer-u32-set! chicken.memory#pointer-s16-set! chicken.memory#pointer-u16-set! chicken.memory#pointer-s8-set! chicken.memory#pointer-u8-set! chicken.memory#pointer-tag chicken.memory#tagged-pointer? chicken.memory#tag-pointer chicken.memory#align-to-word chicken.memory#pointer+ chicken.memory#pointer=? chicken.memory#pointer->object chicken.memory#object->pointer chicken.memory#pointer->address chicken.memory#address->pointer chicken.memory#pointer-like? chicken.memory#pointer? chicken.memory#free chicken.memory#allocate chicken.memory#move-memory! ##sys#check-pointer ##sys#check-generic-vector ##sys#check-generic-structure ##sys#check-become-alist ##sys#check-block) 
o|inlining procedure: k940 
o|inlining procedure: k940 
o|inlining procedure: k948 
o|inlining procedure: k948 
o|inlining procedure: k970 
o|inlining procedure: k970 
o|inlining procedure: k1004 
o|inlining procedure: k1004 
o|inlining procedure: k1019 
o|inlining procedure: k1019 
o|contracted procedure: "(lolevel.scm:104) g112113" 
o|inlining procedure: k1009 
o|inlining procedure: k1009 
o|inlining procedure: k1029 
o|inlining procedure: k1029 
o|inlining procedure: k1051 
o|inlining procedure: k1051 
o|contracted procedure: "(lolevel.scm:112) g120121" 
o|inlining procedure: k1034 
o|inlining procedure: k1041 
o|inlining procedure: k1041 
o|inlining procedure: k1034 
o|inlining procedure: k1061 
o|inlining procedure: k1061 
o|inlining procedure: k1074 
o|inlining procedure: k1074 
o|contracted procedure: "(lolevel.scm:118) g131132" 
o|merged explicitly consed rest parameter: args232 
o|inlining procedure: k1235 
o|inlining procedure: k1235 
o|consed rest parameter at call site: "(lolevel.scm:151) sizerr229" 1 
o|inlining procedure: k1251 
o|inlining procedure: k1251 
o|consed rest parameter at call site: "(lolevel.scm:156) sizerr229" 1 
o|inlining procedure: k1292 
o|inlining procedure: "(lolevel.scm:168) typerr141" 
o|inlining procedure: k1292 
o|inlining procedure: k1324 
o|inlining procedure: k1324 
o|inlining procedure: "(lolevel.scm:172) typerr141" 
o|inlining procedure: k1344 
o|contracted procedure: "(lolevel.scm:175) memmove1137" 
o|inlining procedure: k1386 
o|contracted procedure: "(lolevel.scm:177) memmove3139" 
o|inlining procedure: k1386 
o|inlining procedure: "(lolevel.scm:179) typerr141" 
o|contracted procedure: "(lolevel.scm:174) g262263" 
o|inlining procedure: k1370 
o|inlining procedure: k1370 
o|contracted procedure: "(lolevel.scm:63) g268269" 
o|inlining procedure: k1344 
o|inlining procedure: k1431 
o|contracted procedure: "(lolevel.scm:183) memmove2138" 
o|inlining procedure: k1455 
o|inlining procedure: k1455 
o|inlining procedure: k1431 
o|contracted procedure: "(lolevel.scm:185) memmove4140" 
o|inlining procedure: "(lolevel.scm:188) typerr141" 
o|contracted procedure: "(lolevel.scm:182) g288289" 
o|inlining procedure: k1441 
o|inlining procedure: k1441 
o|contracted procedure: "(lolevel.scm:63) g294295" 
o|inlining procedure: "(lolevel.scm:190) typerr141" 
o|contracted procedure: "(lolevel.scm:173) g249250" 
o|inlining procedure: k1354 
o|inlining procedure: k1354 
o|contracted procedure: "(lolevel.scm:63) g255256" 
o|inlining procedure: k1553 
o|inlining procedure: k1553 
o|contracted procedure: "(lolevel.scm:198) g330331" 
o|contracted procedure: "(lolevel.scm:200) g335336" 
o|inlining procedure: k1572 
o|inlining procedure: k1572 
o|inlining procedure: k1599 
o|contracted procedure: "(lolevel.scm:211) g348349" 
o|inlining procedure: k1599 
o|inlining procedure: k1647 
o|inlining procedure: k1647 
o|contracted procedure: "(lolevel.scm:232) g387388" 
o|inlining procedure: k1659 
o|inlining procedure: k1659 
o|inlining procedure: k1685 
o|inlining procedure: k1685 
o|contracted procedure: "(lolevel.scm:244) g395396" 
o|inlining procedure: k1690 
o|inlining procedure: k1690 
o|inlining procedure: k1706 
o|inlining procedure: k1715 
o|inlining procedure: k1715 
o|inlining procedure: k1706 
o|inlining procedure: k1734 
o|inlining procedure: k1734 
o|contracted procedure: "(lolevel.scm:255) g420421" 
o|inlining procedure: k1739 
o|inlining procedure: k1739 
o|inlining procedure: k1867 
o|inlining procedure: k1867 
o|contracted procedure: "(lolevel.scm:344) words->bytes489" 
o|inlining procedure: k1914 
o|inlining procedure: k1914 
o|inlining procedure: k1959 
o|inlining procedure: k1959 
o|inlining procedure: k1993 
o|inlining procedure: k1993 
o|inlining procedure: k2047 
o|inlining procedure: k2047 
o|inlining procedure: k2076 
o|inlining procedure: k2076 
o|inlining procedure: k2089 
o|inlining procedure: k2089 
o|inlining procedure: k2130 
o|inlining procedure: k2130 
o|inlining procedure: k2156 
o|inlining procedure: k2162 
o|inlining procedure: k2162 
o|contracted procedure: "(lolevel.scm:452) g651652" 
o|inlining procedure: k2173 
o|inlining procedure: k2173 
o|inlining procedure: k2156 
o|inlining procedure: k2192 
o|inlining procedure: k2222 
o|inlining procedure: k2222 
o|contracted procedure: "(lolevel.scm:457) g661662" 
o|inlining procedure: k2206 
o|inlining procedure: k2206 
o|inlining procedure: k2192 
o|inlining procedure: k2231 
o|inlining procedure: k2231 
o|contracted procedure: "(lolevel.scm:467) g672673" 
o|inlining procedure: k2249 
o|inlining procedure: k2256 
o|inlining procedure: k2256 
o|inlining procedure: k2249 
o|inlining procedure: k2279 
o|inlining procedure: k2279 
o|inlining procedure: k2314 
o|inlining procedure: k2314 
o|contracted procedure: "(lolevel.scm:502) g707708" 
o|inlining procedure: k2319 
o|inlining procedure: k2319 
o|inlining procedure: k2412 
o|inlining procedure: k2412 
o|inlining procedure: k2479 
o|inlining procedure: k2479 
o|inlining procedure: k2495 
o|inlining procedure: k2495 
o|inlining procedure: k2517 
o|inlining procedure: k2517 
o|contracted procedure: "(lolevel.scm:396) chicken.memory#pv-buf-ref" 
o|replaced variables: 404 
o|removed binding forms: 161 
o|substituted constant variable: r9492601 
o|substituted constant variable: r9492601 
o|substituted constant variable: r10202609 
o|substituted constant variable: r10202609 
o|substituted constant variable: r10102612 
o|substituted constant variable: r10522617 
o|substituted constant variable: r10522617 
o|substituted constant variable: r10352624 
o|substituted constant variable: r10752629 
o|substituted constant variable: r10752629 
o|substituted constant variable: r15542687 
o|substituted constant variable: r15542687 
o|substituted constant variable: r15732690 
o|substituted constant variable: r16002692 
o|substituted constant variable: r16602696 
o|substituted constant variable: r16912702 
o|substituted constant variable: r17072706 
o|substituted constant variable: r17402710 
o|inlining procedure: k1849 
o|substituted constant variable: r19942719 
o|substituted constant variable: r19942719 
o|substituted constant variable: r21312730 
o|substituted constant variable: r21632732 
o|substituted constant variable: r21632733 
o|substituted constant variable: r21742735 
o|substituted constant variable: r21572736 
o|substituted constant variable: r22232739 
o|substituted constant variable: r22072741 
o|substituted constant variable: r21932742 
o|substituted constant variable: r22502750 
o|substituted constant variable: r23152754 
o|substituted constant variable: r23202756 
o|substituted constant variable: r24802759 
o|substituted constant variable: r24802759 
o|substituted constant variable: r24962763 
o|substituted constant variable: r24962763 
o|substituted constant variable: r25182768 
o|replaced variables: 140 
o|removed binding forms: 325 
o|inlining procedure: k1367 
o|inlining procedure: k1438 
o|inlining procedure: k1351 
o|removed binding forms: 145 
o|contracted procedure: k1066 
o|contracted procedure: k1843 
o|simplifications: ((let . 1)) 
o|removed binding forms: 8 
o|simplifications: ((if . 39) (##core#call . 165) (let . 21)) 
o|  call simplifications:
o|    scheme#vector
o|    scheme#list	2
o|    ##sys#check-symbol
o|    scheme#cons	2
o|    scheme#symbol?
o|    ##sys#setslot	4
o|    ##sys#intern-symbol
o|    ##sys#check-structure	4
o|    ##sys#structure?
o|    scheme#eq?	6
o|    chicken.fixnum#fx>=	4
o|    chicken.fixnum#fx+	7
o|    ##sys#make-structure	2
o|    scheme#equal?
o|    ##sys#foreign-ranged-integer-argument	2
o|    scheme#car	9
o|    scheme#cdr	3
o|    chicken.fixnum#fx<	2
o|    ##sys#generic-structure?	2
o|    scheme#string?	3
o|    ##sys#size	12
o|    ##sys#foreign-block-argument	4
o|    ##sys#foreign-pointer-argument	7
o|    ##sys#foreign-fixnum-argument	16
o|    scheme#memq	2
o|    ##sys#slot	24
o|    chicken.fixnum#fx-	6
o|    chicken.fixnum#fx<=	3
o|    scheme#apply	2
o|    scheme#not	8
o|    ##sys#check-list
o|    scheme#null?	13
o|    ##sys#check-pair
o|    scheme#pair?	8
o|contracted procedure: k951 
o|contracted procedure: k958 
o|contracted procedure: k967 
o|contracted procedure: k976 
o|contracted procedure: k980 
o|contracted procedure: k1012 
o|contracted procedure: k1022 
o|contracted procedure: k1054 
o|contracted procedure: k1077 
o|contracted procedure: k1536 
o|contracted procedure: k1202 
o|contracted procedure: k1530 
o|contracted procedure: k1205 
o|contracted procedure: k1524 
o|contracted procedure: k1208 
o|contracted procedure: k1518 
o|contracted procedure: k1211 
o|contracted procedure: k1512 
o|contracted procedure: k1214 
o|contracted procedure: k1506 
o|contracted procedure: k1217 
o|contracted procedure: k1245 
o|contracted procedure: k1238 
o|contracted procedure: k1271 
o|contracted procedure: k1260 
o|contracted procedure: k1267 
o|contracted procedure: k1295 
o|contracted procedure: k1315 
o|contracted procedure: k1301 
o|contracted procedure: k1308 
o|contracted procedure: k1321 
o|contracted procedure: k1341 
o|contracted procedure: k1327 
o|contracted procedure: k1334 
o|contracted procedure: k1357 
o|contracted procedure: k1373 
o|contracted procedure: k1085 
o|contracted procedure: k1089 
o|contracted procedure: k1093 
o|contracted procedure: k1097 
o|contracted procedure: k1101 
o|contracted procedure: k1392 
o|contracted procedure: k1141 
o|contracted procedure: k1145 
o|contracted procedure: k1149 
o|contracted procedure: k1153 
o|contracted procedure: k1157 
o|contracted procedure: k1407 
o|contracted procedure: k1425 
o|contracted procedure: k1428 
o|contracted procedure: k1444 
o|contracted procedure: k1113 
o|contracted procedure: k1117 
o|contracted procedure: k1121 
o|contracted procedure: k1125 
o|contracted procedure: k1129 
o|contracted procedure: k1464 
o|contracted procedure: k1169 
o|contracted procedure: k1173 
o|contracted procedure: k1177 
o|contracted procedure: k1181 
o|contracted procedure: k1185 
o|contracted procedure: k1475 
o|contracted procedure: k1479 
o|contracted procedure: k1494 
o|contracted procedure: k1500 
o|contracted procedure: k1546 
o|contracted procedure: k1553 
o|contracted procedure: k1626 
o|contracted procedure: k1630 
o|contracted procedure: k1641 
o|contracted procedure: k1662 
o|contracted procedure: k1693 
o|contracted procedure: k1725 
o|contracted procedure: k1703 
o|contracted procedure: k1712 
o|contracted procedure: k1722 
o|contracted procedure: k1742 
o|contracted procedure: k1826 
o|contracted procedure: k1886 
o|contracted procedure: k1831 
o|contracted procedure: k1840 
o|contracted procedure: k1855 
o|contracted procedure: k1870 
o|contracted procedure: k1880 
o|contracted procedure: k1905 
o|contracted procedure: k1917 
o|contracted procedure: k1920 
o|contracted procedure: k1935 
o|contracted procedure: k1941 
o|contracted procedure: k1947 
o|contracted procedure: k1950 
o|contracted procedure: k1962 
o|contracted procedure: k1972 
o|contracted procedure: k1989 
o|contracted procedure: k1993 
o|contracted procedure: k2002 
o|contracted procedure: k2015 
o|contracted procedure: k2022 
o|contracted procedure: k2032 
o|contracted procedure: k2050 
o|contracted procedure: k2056 
o|contracted procedure: k2063 
o|contracted procedure: k2066 
o|contracted procedure: k2069 
o|contracted procedure: k2083 
o|contracted procedure: k2092 
o|contracted procedure: k2095 
o|contracted procedure: k2102 
o|contracted procedure: k2110 
o|contracted procedure: k2115 
o|contracted procedure: k2133 
o|contracted procedure: k2140 
o|contracted procedure: k2150 
o|contracted procedure: k2146 
o|contracted procedure: k2176 
o|contracted procedure: k2183 
o|contracted procedure: k2209 
o|contracted procedure: k2216 
o|contracted procedure: k2234 
o|contracted procedure: k2282 
o|contracted procedure: k2295 
o|contracted procedure: k2302 
o|contracted procedure: k2338 
o|contracted procedure: k2311 
o|contracted procedure: k2322 
o|contracted procedure: k2325 
o|contracted procedure: k2335 
o|contracted procedure: k2363 
o|contracted procedure: k2379 
o|contracted procedure: k2387 
o|contracted procedure: k2383 
o|contracted procedure: k2400 
o|contracted procedure: k2415 
o|contracted procedure: k2429 
o|contracted procedure: k2418 
o|contracted procedure: k2425 
o|contracted procedure: k2450 
o|contracted procedure: k2465 
o|contracted procedure: k2461 
o|contracted procedure: k2482 
o|contracted procedure: k2479 
o|contracted procedure: k2498 
o|contracted procedure: k2495 
o|contracted procedure: k2536 
o|contracted procedure: k2544 
o|contracted procedure: k2540 
o|contracted procedure: k2550 
o|contracted procedure: k2560 
o|contracted procedure: k1982 
o|contracted procedure: k2564 
o|simplifications: ((if . 9) (let . 69)) 
o|removed binding forms: 151 
(o x)|known list op on rest arg sublist: ##core#rest-car loc88 0 
(o x)|known list op on rest arg sublist: ##core#rest-car loc111 0 
(o x)|known list op on rest arg sublist: ##core#rest-car loc130 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest212215 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest212215 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest212215 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest212215 0 
o|contracted procedure: k1254 
o|inlining procedure: k1403 
o|inlining procedure: "(lolevel.scm:231) align375" 
o|inlining procedure: "(lolevel.scm:233) align375" 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest405407 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest405407 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest481483 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest481483 0 
o|inlining procedure: "(lolevel.scm:351) chicken.memory#pv-buf-set!" 
o|inlining procedure: "(lolevel.scm:366) chicken.memory#pv-buf-set!" 
o|inlining procedure: "(lolevel.scm:375) chicken.memory#pv-buf-set!" 
o|inlining procedure: "(lolevel.scm:389) chicken.memory#pv-buf-set!" 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest698700 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest698700 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? index779 0 
(o x)|known list op on rest arg sublist: ##core#rest-car index779 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? index786 0 
(o x)|known list op on rest arg sublist: ##core#rest-car index786 0 
o|removed binding forms: 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r1206 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r1206 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r1206 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r1206 1 
o|removed side-effect free assignment to unused variable: chicken.memory#pv-buf-set! 
o|replaced variables: 27 
o|removed binding forms: 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r1212 2 
(o x)|known list op on rest arg sublist: ##core#rest-car r1212 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r1212 2 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r1212 2 
o|inlining procedure: k1669 
o|inlining procedure: k1873 
o|inlining procedure: k1926 
o|inlining procedure: k1965 
o|removed binding forms: 22 
o|simplifications: ((let . 1)) 
o|removed binding forms: 6 
o|direct leaf routine/allocation: doloop496497 0 
o|direct leaf routine/allocation: doloop524525 0 
o|direct leaf routine/allocation: doloop735736 0 
o|contracted procedure: k1849 
o|converted assignments to bindings: (doloop496497) 
o|converted assignments to bindings: (doloop524525) 
o|converted assignments to bindings: (doloop735736) 
o|simplifications: ((let . 3)) 
o|removed binding forms: 1 
o|customizable procedures: (copy620 doloop629630 doloop511512 checkn2231 checkn1230 nosizerr228 move242 sizerr229 k1044 loop94) 
o|calls to known targets: 28 
o|identified direct recursive calls: f_1290 2 
o|unused rest argument: rest212215 f_1200 
o|unused rest argument: rest405407 f_1701 
o|identified direct recursive calls: f_1865 1 
o|unused rest argument: rest481483 f_1829 
o|identified direct recursive calls: f_1957 1 
o|unused rest argument: rest698700 f_2309 
o|identified direct recursive calls: f_2410 1 
o|unused rest argument: index779 f_2473 
o|unused rest argument: index786 f_2489 
o|fast box initializations: 9 
o|fast global references: 4 
o|fast global assignments: 1 
*/
/* end of file */
