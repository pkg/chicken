/* Generated from compiler-syntax.scm by the CHICKEN compiler
   http://www.call-cc.org
   Version 5.2.0 (rev 317468e4)
   linux-unix-gnu-x86-64 [ 64bit dload ptables ]
   command line: compiler-syntax.scm -optimize-level 2 -include-path . -include-path ./ -inline -ignore-repository -feature chicken-bootstrap -no-warnings -specialize -consult-types-file ./types.db -no-lambda-info -no-trace -emit-import-library chicken.compiler.compiler-syntax -output-file compiler-syntax.c
   unit: compiler-syntax
   uses: library eval expand extras support compiler
*/
#include "chicken.h"

static C_PTABLE_ENTRY *create_ptable(void);
C_noret_decl(C_library_toplevel)
C_externimport void C_ccall C_library_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_eval_toplevel)
C_externimport void C_ccall C_eval_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_expand_toplevel)
C_externimport void C_ccall C_expand_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_extras_toplevel)
C_externimport void C_ccall C_extras_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_support_toplevel)
C_externimport void C_ccall C_support_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_compiler_toplevel)
C_externimport void C_ccall C_compiler_toplevel(C_word c,C_word *av) C_noret;

static C_TLS C_word lf[105];
static double C_possibly_force_alignment;


C_noret_decl(f_1679)
static void C_ccall f_1679(C_word c,C_word *av) C_noret;
C_noret_decl(f_1682)
static void C_ccall f_1682(C_word c,C_word *av) C_noret;
C_noret_decl(f_1685)
static void C_ccall f_1685(C_word c,C_word *av) C_noret;
C_noret_decl(f_1688)
static void C_ccall f_1688(C_word c,C_word *av) C_noret;
C_noret_decl(f_1691)
static void C_ccall f_1691(C_word c,C_word *av) C_noret;
C_noret_decl(f_1694)
static void C_ccall f_1694(C_word c,C_word *av) C_noret;
C_noret_decl(f_2775)
static void C_fcall f_2775(C_word t0,C_word t1) C_noret;
C_noret_decl(f_2781)
static C_word C_fcall f_2781(C_word t0,C_word t1,C_word t2);
C_noret_decl(f_2975)
static void C_ccall f_2975(C_word c,C_word *av) C_noret;
C_noret_decl(f_2979)
static void C_ccall f_2979(C_word c,C_word *av) C_noret;
C_noret_decl(f_2983)
static void C_ccall f_2983(C_word c,C_word *av) C_noret;
C_noret_decl(f_2987)
static void C_fcall f_2987(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_2992)
static void C_fcall f_2992(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3001)
static void C_fcall f_3001(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3011)
static void C_ccall f_3011(C_word c,C_word *av) C_noret;
C_noret_decl(f_3026)
static void C_ccall f_3026(C_word c,C_word *av) C_noret;
C_noret_decl(f_3030)
static void C_ccall f_3030(C_word c,C_word *av) C_noret;
C_noret_decl(f_3034)
static void C_ccall f_3034(C_word c,C_word *av) C_noret;
C_noret_decl(f_3041)
static void C_ccall f_3041(C_word c,C_word *av) C_noret;
C_noret_decl(f_3044)
static void C_ccall f_3044(C_word c,C_word *av) C_noret;
C_noret_decl(f_3047)
static void C_ccall f_3047(C_word c,C_word *av) C_noret;
C_noret_decl(f_3050)
static void C_ccall f_3050(C_word c,C_word *av) C_noret;
C_noret_decl(f_3053)
static void C_ccall f_3053(C_word c,C_word *av) C_noret;
C_noret_decl(f_3056)
static void C_ccall f_3056(C_word c,C_word *av) C_noret;
C_noret_decl(f_3058)
static void C_fcall f_3058(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6) C_noret;
C_noret_decl(f_3064)
static void C_ccall f_3064(C_word c,C_word *av) C_noret;
C_noret_decl(f_3086)
static void C_fcall f_3086(C_word t0,C_word t1) C_noret;
C_noret_decl(f_3089)
static void C_ccall f_3089(C_word c,C_word *av) C_noret;
C_noret_decl(f_3092)
static void C_fcall f_3092(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_3096)
static void C_ccall f_3096(C_word c,C_word *av) C_noret;
C_noret_decl(f_3099)
static void C_ccall f_3099(C_word c,C_word *av) C_noret;
C_noret_decl(f_3109)
static void C_ccall f_3109(C_word c,C_word *av) C_noret;
C_noret_decl(f_3115)
static void C_ccall f_3115(C_word c,C_word *av) C_noret;
C_noret_decl(f_3118)
static void C_ccall f_3118(C_word c,C_word *av) C_noret;
C_noret_decl(f_3121)
static void C_ccall f_3121(C_word c,C_word *av) C_noret;
C_noret_decl(f_3124)
static void C_ccall f_3124(C_word c,C_word *av) C_noret;
C_noret_decl(f_3127)
static void C_ccall f_3127(C_word c,C_word *av) C_noret;
C_noret_decl(f_3130)
static void C_ccall f_3130(C_word c,C_word *av) C_noret;
C_noret_decl(f_3133)
static void C_ccall f_3133(C_word c,C_word *av) C_noret;
C_noret_decl(f_3136)
static void C_ccall f_3136(C_word c,C_word *av) C_noret;
C_noret_decl(f_3140)
static void C_ccall f_3140(C_word c,C_word *av) C_noret;
C_noret_decl(f_3143)
static void C_ccall f_3143(C_word c,C_word *av) C_noret;
C_noret_decl(f_3149)
static void C_ccall f_3149(C_word c,C_word *av) C_noret;
C_noret_decl(f_3152)
static void C_ccall f_3152(C_word c,C_word *av) C_noret;
C_noret_decl(f_3155)
static void C_ccall f_3155(C_word c,C_word *av) C_noret;
C_noret_decl(f_3164)
static void C_ccall f_3164(C_word c,C_word *av) C_noret;
C_noret_decl(f_3167)
static void C_ccall f_3167(C_word c,C_word *av) C_noret;
C_noret_decl(f_3170)
static void C_ccall f_3170(C_word c,C_word *av) C_noret;
C_noret_decl(f_3172)
static C_word C_fcall f_3172(C_word t0);
C_noret_decl(f_3182)
static void C_fcall f_3182(C_word t0,C_word t1) C_noret;
C_noret_decl(f_3201)
static void C_fcall f_3201(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3232)
static void C_ccall f_3232(C_word c,C_word *av) C_noret;
C_noret_decl(f_3239)
static C_word C_fcall f_3239(C_word *a,C_word t0,C_word t1);
C_noret_decl(f_3249)
static void C_fcall f_3249(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3259)
static void C_ccall f_3259(C_word c,C_word *av) C_noret;
C_noret_decl(f_3262)
static void C_ccall f_3262(C_word c,C_word *av) C_noret;
C_noret_decl(f_3285)
static void C_ccall f_3285(C_word c,C_word *av) C_noret;
C_noret_decl(f_3314)
static void C_ccall f_3314(C_word c,C_word *av) C_noret;
C_noret_decl(f_3320)
static void C_ccall f_3320(C_word c,C_word *av) C_noret;
C_noret_decl(f_3337)
static void C_ccall f_3337(C_word c,C_word *av) C_noret;
C_noret_decl(f_3354)
static void C_ccall f_3354(C_word c,C_word *av) C_noret;
C_noret_decl(f_3371)
static void C_ccall f_3371(C_word c,C_word *av) C_noret;
C_noret_decl(f_3392)
static void C_ccall f_3392(C_word c,C_word *av) C_noret;
C_noret_decl(f_3413)
static void C_ccall f_3413(C_word c,C_word *av) C_noret;
C_noret_decl(f_3434)
static void C_ccall f_3434(C_word c,C_word *av) C_noret;
C_noret_decl(f_3456)
static void C_ccall f_3456(C_word c,C_word *av) C_noret;
C_noret_decl(f_3459)
static void C_ccall f_3459(C_word c,C_word *av) C_noret;
C_noret_decl(f_3510)
static void C_fcall f_3510(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3582)
static void C_ccall f_3582(C_word c,C_word *av) C_noret;
C_noret_decl(f_3589)
static void C_ccall f_3589(C_word c,C_word *av) C_noret;
C_noret_decl(f_3593)
static void C_ccall f_3593(C_word c,C_word *av) C_noret;
C_noret_decl(f_3607)
static void C_ccall f_3607(C_word c,C_word *av) C_noret;
C_noret_decl(f_3615)
static void C_ccall f_3615(C_word c,C_word *av) C_noret;
C_noret_decl(f_3618)
static void C_ccall f_3618(C_word c,C_word *av) C_noret;
C_noret_decl(f_3620)
static void C_ccall f_3620(C_word c,C_word *av) C_noret;
C_noret_decl(f_3639)
static void C_ccall f_3639(C_word c,C_word *av) C_noret;
C_noret_decl(f_3642)
static void C_ccall f_3642(C_word c,C_word *av) C_noret;
C_noret_decl(f_3645)
static void C_ccall f_3645(C_word c,C_word *av) C_noret;
C_noret_decl(f_3648)
static void C_ccall f_3648(C_word c,C_word *av) C_noret;
C_noret_decl(f_3651)
static void C_ccall f_3651(C_word c,C_word *av) C_noret;
C_noret_decl(f_3654)
static void C_ccall f_3654(C_word c,C_word *av) C_noret;
C_noret_decl(f_3657)
static void C_ccall f_3657(C_word c,C_word *av) C_noret;
C_noret_decl(f_3736)
static void C_ccall f_3736(C_word c,C_word *av) C_noret;
C_noret_decl(f_3755)
static void C_ccall f_3755(C_word c,C_word *av) C_noret;
C_noret_decl(f_3758)
static void C_ccall f_3758(C_word c,C_word *av) C_noret;
C_noret_decl(f_3761)
static void C_ccall f_3761(C_word c,C_word *av) C_noret;
C_noret_decl(f_3764)
static void C_ccall f_3764(C_word c,C_word *av) C_noret;
C_noret_decl(f_3767)
static void C_ccall f_3767(C_word c,C_word *av) C_noret;
C_noret_decl(f_3770)
static void C_ccall f_3770(C_word c,C_word *av) C_noret;
C_noret_decl(f_3841)
static void C_ccall f_3841(C_word c,C_word *av) C_noret;
C_noret_decl(f_3845)
static void C_ccall f_3845(C_word c,C_word *av) C_noret;
C_noret_decl(f_3854)
static void C_ccall f_3854(C_word c,C_word *av) C_noret;
C_noret_decl(f_3864)
static void C_ccall f_3864(C_word c,C_word *av) C_noret;
C_noret_decl(f_3881)
static void C_ccall f_3881(C_word c,C_word *av) C_noret;
C_noret_decl(f_3885)
static void C_ccall f_3885(C_word c,C_word *av) C_noret;
C_noret_decl(f_3888)
static void C_ccall f_3888(C_word c,C_word *av) C_noret;
C_noret_decl(f_3898)
static void C_ccall f_3898(C_word c,C_word *av) C_noret;
C_noret_decl(f_3910)
static void C_ccall f_3910(C_word c,C_word *av) C_noret;
C_noret_decl(f_3922)
static void C_ccall f_3922(C_word c,C_word *av) C_noret;
C_noret_decl(f_3965)
static void C_ccall f_3965(C_word c,C_word *av) C_noret;
C_noret_decl(f_3975)
static void C_ccall f_3975(C_word c,C_word *av) C_noret;
C_noret_decl(f_3982)
static void C_ccall f_3982(C_word c,C_word *av) C_noret;
C_noret_decl(f_3993)
static void C_ccall f_3993(C_word c,C_word *av) C_noret;
C_noret_decl(f_3995)
static void C_fcall f_3995(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4013)
static void C_ccall f_4013(C_word c,C_word *av) C_noret;
C_noret_decl(f_4029)
static void C_ccall f_4029(C_word c,C_word *av) C_noret;
C_noret_decl(f_4033)
static void C_ccall f_4033(C_word c,C_word *av) C_noret;
C_noret_decl(f_4036)
static void C_ccall f_4036(C_word c,C_word *av) C_noret;
C_noret_decl(f_4039)
static void C_ccall f_4039(C_word c,C_word *av) C_noret;
C_noret_decl(f_4042)
static void C_ccall f_4042(C_word c,C_word *av) C_noret;
C_noret_decl(f_4045)
static void C_ccall f_4045(C_word c,C_word *av) C_noret;
C_noret_decl(f_4048)
static void C_ccall f_4048(C_word c,C_word *av) C_noret;
C_noret_decl(f_4051)
static void C_ccall f_4051(C_word c,C_word *av) C_noret;
C_noret_decl(f_4054)
static void C_ccall f_4054(C_word c,C_word *av) C_noret;
C_noret_decl(f_4057)
static void C_ccall f_4057(C_word c,C_word *av) C_noret;
C_noret_decl(f_4060)
static void C_ccall f_4060(C_word c,C_word *av) C_noret;
C_noret_decl(f_4063)
static void C_ccall f_4063(C_word c,C_word *av) C_noret;
C_noret_decl(f_4066)
static void C_ccall f_4066(C_word c,C_word *av) C_noret;
C_noret_decl(f_4069)
static void C_ccall f_4069(C_word c,C_word *av) C_noret;
C_noret_decl(f_4072)
static void C_ccall f_4072(C_word c,C_word *av) C_noret;
C_noret_decl(f_4081)
static void C_fcall f_4081(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4095)
static void C_ccall f_4095(C_word c,C_word *av) C_noret;
C_noret_decl(f_4118)
static void C_ccall f_4118(C_word c,C_word *av) C_noret;
C_noret_decl(f_4123)
static C_word C_fcall f_4123(C_word *a,C_word t0,C_word t1);
C_noret_decl(f_4138)
static void C_ccall f_4138(C_word c,C_word *av) C_noret;
C_noret_decl(f_4153)
static void C_ccall f_4153(C_word c,C_word *av) C_noret;
C_noret_decl(f_4201)
static void C_ccall f_4201(C_word c,C_word *av) C_noret;
C_noret_decl(f_4203)
static void C_fcall f_4203(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4263)
static void C_ccall f_4263(C_word c,C_word *av) C_noret;
C_noret_decl(f_4265)
static void C_fcall f_4265(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4302)
static C_word C_fcall f_4302(C_word *a,C_word t0,C_word t1);
C_noret_decl(f_4309)
static void C_ccall f_4309(C_word c,C_word *av) C_noret;
C_noret_decl(f_4311)
static void C_fcall f_4311(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4345)
static void C_fcall f_4345(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_4393)
static void C_fcall f_4393(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4447)
static void C_ccall f_4447(C_word c,C_word *av) C_noret;
C_noret_decl(f_4449)
static void C_fcall f_4449(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_4517)
static void C_fcall f_4517(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4542)
static void C_ccall f_4542(C_word c,C_word *av) C_noret;
C_noret_decl(f_4559)
static void C_ccall f_4559(C_word c,C_word *av) C_noret;
C_noret_decl(f_4561)
static void C_ccall f_4561(C_word c,C_word *av) C_noret;
C_noret_decl(f_4565)
static void C_ccall f_4565(C_word c,C_word *av) C_noret;
C_noret_decl(f_4568)
static void C_ccall f_4568(C_word c,C_word *av) C_noret;
C_noret_decl(f_4571)
static void C_ccall f_4571(C_word c,C_word *av) C_noret;
C_noret_decl(f_4574)
static void C_ccall f_4574(C_word c,C_word *av) C_noret;
C_noret_decl(f_4577)
static void C_ccall f_4577(C_word c,C_word *av) C_noret;
C_noret_decl(f_4580)
static void C_ccall f_4580(C_word c,C_word *av) C_noret;
C_noret_decl(f_4583)
static void C_ccall f_4583(C_word c,C_word *av) C_noret;
C_noret_decl(f_4586)
static void C_ccall f_4586(C_word c,C_word *av) C_noret;
C_noret_decl(f_4589)
static void C_ccall f_4589(C_word c,C_word *av) C_noret;
C_noret_decl(f_4598)
static void C_fcall f_4598(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4612)
static void C_ccall f_4612(C_word c,C_word *av) C_noret;
C_noret_decl(f_4627)
static void C_ccall f_4627(C_word c,C_word *av) C_noret;
C_noret_decl(f_4632)
static C_word C_fcall f_4632(C_word *a,C_word t0,C_word t1);
C_noret_decl(f_4647)
static void C_ccall f_4647(C_word c,C_word *av) C_noret;
C_noret_decl(f_4662)
static void C_ccall f_4662(C_word c,C_word *av) C_noret;
C_noret_decl(f_4698)
static void C_ccall f_4698(C_word c,C_word *av) C_noret;
C_noret_decl(f_4700)
static void C_fcall f_4700(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4744)
static void C_ccall f_4744(C_word c,C_word *av) C_noret;
C_noret_decl(f_4746)
static void C_fcall f_4746(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4783)
static C_word C_fcall f_4783(C_word *a,C_word t0,C_word t1);
C_noret_decl(f_4790)
static void C_ccall f_4790(C_word c,C_word *av) C_noret;
C_noret_decl(f_4792)
static void C_fcall f_4792(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4826)
static void C_fcall f_4826(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_4874)
static void C_fcall f_4874(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4920)
static void C_ccall f_4920(C_word c,C_word *av) C_noret;
C_noret_decl(f_4922)
static void C_fcall f_4922(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_4974)
static void C_fcall f_4974(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4999)
static void C_ccall f_4999(C_word c,C_word *av) C_noret;
C_noret_decl(f_5016)
static void C_ccall f_5016(C_word c,C_word *av) C_noret;
C_noret_decl(C_compiler_2dsyntax_toplevel)
C_externexport void C_ccall C_compiler_2dsyntax_toplevel(C_word c,C_word *av) C_noret;

C_noret_decl(trf_2775)
static void C_ccall trf_2775(C_word c,C_word *av) C_noret;
static void C_ccall trf_2775(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_2775(t0,t1);}

C_noret_decl(trf_2987)
static void C_ccall trf_2987(C_word c,C_word *av) C_noret;
static void C_ccall trf_2987(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_2987(t0,t1,t2,t3);}

C_noret_decl(trf_2992)
static void C_ccall trf_2992(C_word c,C_word *av) C_noret;
static void C_ccall trf_2992(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_2992(t0,t1,t2);}

C_noret_decl(trf_3001)
static void C_ccall trf_3001(C_word c,C_word *av) C_noret;
static void C_ccall trf_3001(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3001(t0,t1,t2);}

C_noret_decl(trf_3058)
static void C_ccall trf_3058(C_word c,C_word *av) C_noret;
static void C_ccall trf_3058(C_word c,C_word *av){
C_word t0=av[6];
C_word t1=av[5];
C_word t2=av[4];
C_word t3=av[3];
C_word t4=av[2];
C_word t5=av[1];
C_word t6=av[0];
f_3058(t0,t1,t2,t3,t4,t5,t6);}

C_noret_decl(trf_3086)
static void C_ccall trf_3086(C_word c,C_word *av) C_noret;
static void C_ccall trf_3086(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_3086(t0,t1);}

C_noret_decl(trf_3092)
static void C_ccall trf_3092(C_word c,C_word *av) C_noret;
static void C_ccall trf_3092(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_3092(t0,t1,t2,t3,t4);}

C_noret_decl(trf_3182)
static void C_ccall trf_3182(C_word c,C_word *av) C_noret;
static void C_ccall trf_3182(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_3182(t0,t1);}

C_noret_decl(trf_3201)
static void C_ccall trf_3201(C_word c,C_word *av) C_noret;
static void C_ccall trf_3201(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3201(t0,t1,t2);}

C_noret_decl(trf_3249)
static void C_ccall trf_3249(C_word c,C_word *av) C_noret;
static void C_ccall trf_3249(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3249(t0,t1,t2);}

C_noret_decl(trf_3510)
static void C_ccall trf_3510(C_word c,C_word *av) C_noret;
static void C_ccall trf_3510(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3510(t0,t1,t2);}

C_noret_decl(trf_3995)
static void C_ccall trf_3995(C_word c,C_word *av) C_noret;
static void C_ccall trf_3995(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3995(t0,t1,t2);}

C_noret_decl(trf_4081)
static void C_ccall trf_4081(C_word c,C_word *av) C_noret;
static void C_ccall trf_4081(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4081(t0,t1);}

C_noret_decl(trf_4203)
static void C_ccall trf_4203(C_word c,C_word *av) C_noret;
static void C_ccall trf_4203(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4203(t0,t1,t2);}

C_noret_decl(trf_4265)
static void C_ccall trf_4265(C_word c,C_word *av) C_noret;
static void C_ccall trf_4265(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4265(t0,t1,t2);}

C_noret_decl(trf_4311)
static void C_ccall trf_4311(C_word c,C_word *av) C_noret;
static void C_ccall trf_4311(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4311(t0,t1,t2);}

C_noret_decl(trf_4345)
static void C_ccall trf_4345(C_word c,C_word *av) C_noret;
static void C_ccall trf_4345(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_4345(t0,t1,t2,t3);}

C_noret_decl(trf_4393)
static void C_ccall trf_4393(C_word c,C_word *av) C_noret;
static void C_ccall trf_4393(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4393(t0,t1,t2);}

C_noret_decl(trf_4449)
static void C_ccall trf_4449(C_word c,C_word *av) C_noret;
static void C_ccall trf_4449(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_4449(t0,t1,t2,t3);}

C_noret_decl(trf_4517)
static void C_ccall trf_4517(C_word c,C_word *av) C_noret;
static void C_ccall trf_4517(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4517(t0,t1,t2);}

C_noret_decl(trf_4598)
static void C_ccall trf_4598(C_word c,C_word *av) C_noret;
static void C_ccall trf_4598(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4598(t0,t1);}

C_noret_decl(trf_4700)
static void C_ccall trf_4700(C_word c,C_word *av) C_noret;
static void C_ccall trf_4700(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4700(t0,t1,t2);}

C_noret_decl(trf_4746)
static void C_ccall trf_4746(C_word c,C_word *av) C_noret;
static void C_ccall trf_4746(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4746(t0,t1,t2);}

C_noret_decl(trf_4792)
static void C_ccall trf_4792(C_word c,C_word *av) C_noret;
static void C_ccall trf_4792(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4792(t0,t1,t2);}

C_noret_decl(trf_4826)
static void C_ccall trf_4826(C_word c,C_word *av) C_noret;
static void C_ccall trf_4826(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_4826(t0,t1,t2,t3);}

C_noret_decl(trf_4874)
static void C_ccall trf_4874(C_word c,C_word *av) C_noret;
static void C_ccall trf_4874(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4874(t0,t1,t2);}

C_noret_decl(trf_4922)
static void C_ccall trf_4922(C_word c,C_word *av) C_noret;
static void C_ccall trf_4922(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_4922(t0,t1,t2,t3);}

C_noret_decl(trf_4974)
static void C_ccall trf_4974(C_word c,C_word *av) C_noret;
static void C_ccall trf_4974(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4974(t0,t1,t2);}

/* k1677 */
static void C_ccall f_1679(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_1679,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1682,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_eval_toplevel(2,av2);}}

/* k1680 in k1677 */
static void C_ccall f_1682(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_1682,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1685,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_expand_toplevel(2,av2);}}

/* k1683 in k1680 in k1677 */
static void C_ccall f_1685(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_1685,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1688,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_extras_toplevel(2,av2);}}

/* k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_1688(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_1688,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1691,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_support_toplevel(2,av2);}}

/* k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_1691(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_1691,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1694,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_compiler_toplevel(2,av2);}}

/* k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_1694(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(27,c,5)))){
C_save_and_reclaim((void *)f_1694,c,av);}
a=C_alloc(27);
t2=C_a_i_provide(&a,1,lf[0]);
t3=C_a_i_provide(&a,1,lf[1]);
t4=C_mutate(&lf[2] /* (set! chicken.compiler.compiler-syntax#length+ ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_2775,tmp=(C_word)a,a+=2,tmp));
t5=C_set_block_item(lf[3] /* chicken.compiler.compiler-syntax#compiler-syntax-statistics */,0,C_SCHEME_END_OF_LIST);
t6=C_mutate((C_word*)lf[4]+1 /* (set! ##sys#compiler-syntax-hook ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_2975,tmp=(C_word)a,a+=2,tmp));
t7=C_mutate(&lf[8] /* (set! chicken.compiler.compiler-syntax#r-c-s ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_2987,tmp=(C_word)a,a+=2,tmp));
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3041,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t9=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_4561,tmp=(C_word)a,a+=2,tmp);
/* compiler-syntax.scm:70: r-c-s */
f_2987(t8,lf[103],t9,lf[104]);}

/* chicken.compiler.compiler-syntax#length+ in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_fcall f_2775(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(2,0,4)))){
C_save_and_reclaim_args((void *)trf_2775,2,t1,t2);}
a=C_alloc(2);
t3=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_2781,tmp=(C_word)a,a+=2,tmp);
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=(
  f_2781(t2,t2,C_fix(0))
);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* lp in chicken.compiler.compiler-syntax#length+ in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static C_word C_fcall f_2781(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_stack_overflow_check;
loop:{}
if(C_truep(C_i_pairp(t1))){
t4=C_u_i_cdr(t1);
t5=C_fixnum_plus(t3,C_fix(1));
if(C_truep(C_i_pairp(t4))){
t6=C_u_i_cdr(t4);
t7=C_i_cdr(t2);
t8=C_fixnum_plus(t5,C_fix(1));
t9=C_eqp(t6,t7);
if(C_truep(C_i_not(t9))){
t11=t6;
t12=t7;
t13=t8;
t1=t11;
t2=t12;
t3=t13;
goto loop;}
else{
return(C_SCHEME_FALSE);}}
else{
return(t5);}}
else{
return(t3);}}

/* ##sys#compiler-syntax-hook in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_2975(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,5)))){
C_save_and_reclaim((void *)f_2975,c,av);}
a=C_alloc(4);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2979,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* compiler-syntax.scm:51: chicken.base#alist-ref */
t5=*((C_word*)lf[6]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
av2[3]=*((C_word*)lf[3]+1);
av2[4]=*((C_word*)lf[7]+1);
av2[5]=C_fix(0);
((C_proc)(void*)(*((C_word*)t5+1)))(6,av2);}}

/* k2977 in ##sys#compiler-syntax-hook in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_2979(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(32,c,4)))){
C_save_and_reclaim((void *)f_2979,c,av);}
a=C_alloc(32);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2983,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=C_s_a_i_plus(&a,2,t1,C_fix(1));
/* compiler-syntax.scm:53: chicken.base#alist-update! */
t4=*((C_word*)lf[5]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=t3;
av2[4]=*((C_word*)lf[3]+1);
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k2981 in k2977 in ##sys#compiler-syntax-hook in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_2983(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2983,c,av);}
t2=C_mutate((C_word*)lf[3]+1 /* (set! chicken.compiler.compiler-syntax#compiler-syntax-statistics ...) */,t1);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.compiler.compiler-syntax#r-c-s in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_fcall f_2987(C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,2)))){
C_save_and_reclaim_args((void *)trf_2987,4,t1,t2,t3,t4);}
a=C_alloc(9);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3026,a[2]=t1,a[3]=t2,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3034,a[2]=t5,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* compiler-syntax.scm:57: ##sys#er-transformer */
t7=*((C_word*)lf[14]+1);{
C_word av2[3];
av2[0]=t7;
av2[1]=t6;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}

/* g545 in k3028 in k3024 in chicken.compiler.compiler-syntax#r-c-s in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_fcall f_2992(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,4)))){
C_save_and_reclaim_args((void *)trf_2992,3,t0,t1,t2);}
/* compiler-syntax.scm:62: ##sys#put! */
t3=*((C_word*)lf[9]+1);{
C_word av2[5];
av2[0]=t3;
av2[1]=t1;
av2[2]=t2;
av2[3]=lf[10];
av2[4]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* for-each-loop544 in k3028 in k3024 in chicken.compiler.compiler-syntax#r-c-s in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_fcall f_3001(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_3001,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3011,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* compiler-syntax.scm:60: g545 */
t4=((C_word*)t0)[3];
f_2992(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k3009 in for-each-loop544 in k3028 in k3024 in chicken.compiler.compiler-syntax#r-c-s in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3011(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_3011,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_3001(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k3024 in chicken.compiler.compiler-syntax#r-c-s in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3026(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_3026,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3030,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* compiler-syntax.scm:59: scheme#append */
t3=*((C_word*)lf[11]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=*((C_word*)lf[12]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k3028 in k3024 in chicken.compiler.compiler-syntax#r-c-s in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3030(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_3030,c,av);}
a=C_alloc(12);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2992,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3001,a[2]=t5,a[3]=t3,tmp=(C_word)a,a+=4,tmp));
t7=((C_word*)t5)[1];
f_3001(t7,((C_word*)t0)[3],((C_word*)t0)[4]);}

/* k3032 in chicken.compiler.compiler-syntax#r-c-s in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3034(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3034,c,av);}
/* compiler-syntax.scm:56: ##sys#ensure-transformer */
t2=*((C_word*)lf[13]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_i_car(((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3041(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,5)))){
C_save_and_reclaim((void *)f_3041,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3044,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_4029,tmp=(C_word)a,a+=2,tmp);
/* compiler-syntax.scm:100: r-c-s */
f_2987(t2,lf[98],t3,lf[99]);}

/* k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3044(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,5)))){
C_save_and_reclaim((void *)f_3044,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3047,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_3965,tmp=(C_word)a,a+=2,tmp);
/* compiler-syntax.scm:142: r-c-s */
f_2987(t2,lf[86],t3,C_SCHEME_END_OF_LIST);}

/* k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3047(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(41,c,5)))){
C_save_and_reclaim((void *)f_3047,c,av);}
a=C_alloc(41);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3050,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_3881,tmp=(C_word)a,a+=2,tmp);
t4=C_a_i_cons(&a,2,lf[75],lf[76]);
t5=C_a_i_cons(&a,2,lf[77],lf[78]);
t6=C_a_i_cons(&a,2,lf[38],lf[79]);
t7=C_a_i_cons(&a,2,lf[80],lf[81]);
t8=C_a_i_cons(&a,2,lf[72],lf[26]);
t9=C_a_i_cons(&a,2,lf[71],lf[19]);
t10=C_a_i_list(&a,6,t4,t5,t6,t7,t8,t9);
/* compiler-syntax.scm:149: r-c-s */
f_2987(t2,lf[82],t3,t10);}

/* k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3050(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,5)))){
C_save_and_reclaim((void *)f_3050,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3053,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_3854,tmp=(C_word)a,a+=2,tmp);
/* compiler-syntax.scm:166: r-c-s */
f_2987(t2,lf[69],t3,lf[70]);}

/* k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3053(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,5)))){
C_save_and_reclaim((void *)f_3053,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3056,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_3841,tmp=(C_word)a,a+=2,tmp);
/* compiler-syntax.scm:178: r-c-s */
f_2987(t2,lf[66],t3,lf[67]);}

/* k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3056(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,8)))){
C_save_and_reclaim((void *)f_3056,c,av);}
a=C_alloc(7);
t2=C_mutate(&lf[15] /* (set! chicken.compiler.compiler-syntax#compile-format-string ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_3058,tmp=(C_word)a,a+=2,tmp));
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3615,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_3736,tmp=(C_word)a,a+=2,tmp);
/* compiler-syntax.scm:277: r-c-s */
f_2987(t3,lf[62],t4,lf[63]);}

/* chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_fcall f_3058(C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6,C_word t7){
C_word tmp;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,3)))){
C_save_and_reclaim_args((void *)trf_3058,7,t1,t2,t3,t4,t5,t6,t7);}
a=C_alloc(8);
t8=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3064,a[2]=t5,a[3]=t2,a[4]=t4,a[5]=t3,a[6]=t6,a[7]=t7,tmp=(C_word)a,a+=8,tmp);
/* compiler-syntax.scm:189: chicken.base#call/cc */
t9=*((C_word*)lf[44]+1);{
C_word av2[3];
av2[0]=t9;
av2[1]=t1;
av2[2]=t8;
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}

/* a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3064(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_3064,c,av);}
a=C_alloc(10);
t3=C_i_length(((C_word*)t0)[2]);
if(C_truep(C_fixnum_greater_or_equal_p(t3,C_fix(1)))){
t4=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_3607,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=t1,a[8]=((C_word*)t0)[6],a[9]=((C_word*)t0)[7],tmp=(C_word)a,a+=10,tmp);
/* compiler-syntax.scm:192: chicken.base#symbol-append */
t5=*((C_word*)lf[42]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=lf[43];
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}
else{
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_fcall f_3086(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,2)))){
C_save_and_reclaim_args((void *)trf_3086,2,t0,t1);}
a=C_alloc(9);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_3089,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
if(C_truep(C_i_stringp(C_u_i_car(((C_word*)t0)[2])))){
t3=t2;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_u_i_car(((C_word*)t0)[2]);
f_3089(2,av2);}}
else{
/* compiler-syntax.scm:197: scheme#cadar */
t3=*((C_word*)lf[41]+1);{
C_word av2[3];
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}
else{
t2=((C_word*)t0)[7];{
C_word av2[2];
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3089(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,5)))){
C_save_and_reclaim((void *)f_3089,c,av);}
a=C_alloc(24);
t2=C_u_i_cdr(((C_word*)t0)[2]);
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3092,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t5=C_SCHEME_END_OF_LIST;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_fix(0);
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_i_string_length(t1);
t10=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_3164,a[2]=t1,a[3]=t8,a[4]=t3,a[5]=t4,a[6]=t6,a[7]=t9,a[8]=((C_word*)t0)[6],a[9]=((C_word*)t0)[4],a[10]=((C_word*)t0)[7],a[11]=((C_word*)t0)[8],tmp=(C_word)a,a+=12,tmp);
/* compiler-syntax.scm:210: r */
t11=((C_word*)t0)[8];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t11;
av2[1]=t10;
av2[2]=lf[40];
((C_proc)(void*)(*((C_word*)t11+1)))(3,av2);}}

/* fail in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_fcall f_3092(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,2)))){
C_save_and_reclaim_args((void *)trf_3092,5,t0,t1,t2,t3,t4);}
a=C_alloc(9);
t5=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_3096,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=t1,a[5]=t3,a[6]=t4,a[7]=((C_word*)t0)[3],a[8]=((C_word*)t0)[4],tmp=(C_word)a,a+=9,tmp);
/* compiler-syntax.scm:200: chicken.compiler.support#get-line */
t6=*((C_word*)lf[28]+1);{
C_word av2[3];
av2[0]=t6;
av2[1]=t5;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k3094 in fail in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3096(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_3096,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3099,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3109,a[2]=t2,a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[7],a[6]=((C_word*)t0)[8],a[7]=t1,tmp=(C_word)a,a+=8,tmp);
/* compiler-syntax.scm:202: chicken.base#open-output-string */
t4=*((C_word*)lf[26]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k3097 in k3094 in fail in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3099(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_3099,c,av);}
if(C_truep(((C_word*)t0)[2])){
/* compiler-syntax.scm:206: return */
t2=((C_word*)t0)[3];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[4];
av2[2]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k3107 in k3094 in fail in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3109(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,4)))){
C_save_and_reclaim((void *)f_3109,c,av);}
a=C_alloc(16);
t2=C_i_check_port_2(t1,C_fix(2),C_SCHEME_TRUE,lf[17]);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3115,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3140,a[2]=t3,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
if(C_truep(((C_word*)t0)[7])){
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3143,a[2]=t4,a[3]=((C_word*)t0)[7],tmp=(C_word)a,a+=4,tmp);
/* compiler-syntax.scm:203: chicken.base#open-output-string */
t6=*((C_word*)lf[26]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
/* compiler-syntax.scm:202: ##sys#print */
t5=*((C_word*)lf[21]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=lf[27];
av2[3]=C_SCHEME_FALSE;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}}

/* k3113 in k3107 in k3094 in fail in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3115(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_3115,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3118,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
/* compiler-syntax.scm:202: ##sys#write-char-0 */
t3=*((C_word*)lf[24]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_make_character(96);
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k3116 in k3113 in k3107 in k3094 in fail in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 in ... */
static void C_ccall f_3118(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,4)))){
C_save_and_reclaim((void *)f_3118,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3121,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* compiler-syntax.scm:202: ##sys#print */
t3=*((C_word*)lf[21]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[7];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k3119 in k3116 in k3113 in k3107 in k3094 in fail in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in ... */
static void C_ccall f_3121(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,4)))){
C_save_and_reclaim((void *)f_3121,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3124,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* compiler-syntax.scm:202: ##sys#print */
t3=*((C_word*)lf[21]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[23];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k3122 in k3119 in k3116 in k3113 in k3107 in k3094 in fail in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in ... */
static void C_ccall f_3124(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_3124,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3127,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* compiler-syntax.scm:202: ##sys#print */
t3=*((C_word*)lf[21]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[6];
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k3125 in k3122 in k3119 in k3116 in k3113 in k3107 in k3094 in fail in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in ... */
static void C_ccall f_3127(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_3127,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3130,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* compiler-syntax.scm:202: ##sys#print */
t3=*((C_word*)lf[21]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[22];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k3128 in k3125 in k3122 in k3119 in k3116 in k3113 in k3107 in k3094 in fail in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in ... */
static void C_ccall f_3130(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,5)))){
C_save_and_reclaim((void *)f_3130,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3133,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=0;
av2[1]=t2;
av2[2]=*((C_word*)lf[20]+1);
av2[3]=((C_word*)t0)[3];
av2[4]=((C_word*)t0)[4];
av2[5]=((C_word*)t0)[5];
C_apply(6,av2);}}

/* k3131 in k3128 in k3125 in k3122 in k3119 in k3116 in k3113 in k3107 in k3094 in fail in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in ... */
static void C_ccall f_3133(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_3133,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3136,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* compiler-syntax.scm:202: chicken.base#get-output-string */
t3=*((C_word*)lf[19]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k3134 in k3131 in k3128 in k3125 in k3122 in k3119 in k3116 in k3113 in k3107 in k3094 in fail in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in ... */
static void C_ccall f_3136(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_3136,c,av);}
/* compiler-syntax.scm:201: chicken.base#warning */
t2=*((C_word*)lf[18]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k3138 in k3107 in k3094 in fail in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3140(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_3140,c,av);}
/* compiler-syntax.scm:202: ##sys#print */
t2=*((C_word*)lf[21]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k3141 in k3107 in k3094 in fail in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3143(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_3143,c,av);}
a=C_alloc(5);
t2=C_i_check_port_2(t1,C_fix(2),C_SCHEME_TRUE,lf[17]);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3149,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* compiler-syntax.scm:203: ##sys#write-char-0 */
t4=*((C_word*)lf[24]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_make_character(40);
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k3147 in k3141 in k3107 in k3094 in fail in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 in ... */
static void C_ccall f_3149(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_3149,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3152,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* compiler-syntax.scm:203: ##sys#print */
t3=*((C_word*)lf[21]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k3150 in k3147 in k3141 in k3107 in k3094 in fail in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in ... */
static void C_ccall f_3152(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_3152,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3155,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* compiler-syntax.scm:203: ##sys#print */
t3=*((C_word*)lf[21]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[25];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k3153 in k3150 in k3147 in k3141 in k3107 in k3094 in fail in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in ... */
static void C_ccall f_3155(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_3155,c,av);}
/* compiler-syntax.scm:203: chicken.base#get-output-string */
t2=*((C_word*)lf[19]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k3162 in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3164(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_3164,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_3167,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],tmp=(C_word)a,a+=13,tmp);
/* compiler-syntax.scm:211: r */
t3=((C_word*)t0)[11];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[39];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k3165 in k3162 in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3167(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_3167,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_3170,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=t1,a[12]=((C_word*)t0)[11],tmp=(C_word)a,a+=13,tmp);
/* compiler-syntax.scm:212: r */
t3=((C_word*)t0)[12];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[38];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k3168 in k3165 in k3162 in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3170(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(42,c,3)))){
C_save_and_reclaim((void *)f_3170,c,av);}
a=C_alloc(42);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3172,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp));
t11=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3182,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],tmp=(C_word)a,a+=4,tmp));
t12=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3201,a[2]=((C_word*)t0)[6],a[3]=t9,tmp=(C_word)a,a+=4,tmp));
t13=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3239,a[2]=((C_word*)t0)[7],tmp=(C_word)a,a+=3,tmp));
t14=C_SCHEME_UNDEFINED;
t15=(*a=C_VECTOR_TYPE|1,a[1]=t14,tmp=(C_word)a,a+=2,tmp);
t16=C_set_block_item(t15,0,(*a=C_CLOSURE_TYPE|16,a[1]=(C_word)f_3249,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[8],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[9],a[6]=((C_word*)t0)[10],a[7]=((C_word*)t0)[11],a[8]=((C_word*)t0)[7],a[9]=t7,a[10]=((C_word*)t0)[4],a[11]=((C_word*)t0)[5],a[12]=t3,a[13]=t15,a[14]=t9,a[15]=t5,a[16]=t1,tmp=(C_word)a,a+=17,tmp));
t17=((C_word*)t15)[1];
f_3249(t17,((C_word*)t0)[12],C_SCHEME_END_OF_LIST);}

/* fetch in k3168 in k3165 in k3162 in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static C_word C_fcall f_3172(C_word t0){
C_word tmp;
C_word t1;
C_word t2;
C_word t3;
C_word t4;
C_stack_overflow_check;{}
t1=C_i_string_ref(((C_word*)t0)[2],((C_word*)((C_word*)t0)[3])[1]);
t2=C_fixnum_plus(((C_word*)((C_word*)t0)[3])[1],C_fix(1));
t3=C_set_block_item(((C_word*)t0)[3],0,t2);
return(t1);}

/* next in k3168 in k3165 in k3162 in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_fcall f_3182(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,4)))){
C_save_and_reclaim_args((void *)trf_3182,2,t0,t1);}
if(C_truep(C_i_nullp(((C_word*)((C_word*)t0)[2])[1]))){
/* compiler-syntax.scm:219: fail */
t2=((C_word*)t0)[3];
f_3092(t2,t1,C_SCHEME_TRUE,lf[29],C_SCHEME_END_OF_LIST);}
else{
t2=C_i_car(((C_word*)((C_word*)t0)[2])[1]);
t3=C_i_cdr(((C_word*)((C_word*)t0)[2])[1]);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t3);
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* endchunk in k3168 in k3165 in k3162 in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_fcall f_3201(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,2)))){
C_save_and_reclaim_args((void *)trf_3201,3,t0,t1,t2);}
a=C_alloc(12);
if(C_truep(C_i_pairp(t2))){
t3=C_i_length(t2);
t4=C_eqp(C_fix(1),t3);
if(C_truep(t4)){
t5=C_i_car(t2);
t6=C_a_i_list(&a,3,lf[24],t5,((C_word*)t0)[2]);
/* compiler-syntax.scm:225: push */
t7=t1;{
C_word av2[2];
av2[0]=t7;
av2[1]=(
/* compiler-syntax.scm:225: push */
  f_3239(C_a_i(&a,3),((C_word*)((C_word*)t0)[3])[1],t6)
);
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}
else{
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3232,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* compiler-syntax.scm:228: ##sys#reverse-list->string */
t6=*((C_word*)lf[30]+1);{
C_word av2[3];
av2[0]=t6;
av2[1]=t5;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k3230 in endchunk in k3168 in k3165 in k3162 in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 in ... */
static void C_ccall f_3232(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,1)))){
C_save_and_reclaim((void *)f_3232,c,av);}
a=C_alloc(15);
t2=C_a_i_list(&a,4,lf[21],t1,C_SCHEME_FALSE,((C_word*)t0)[2]);
/* compiler-syntax.scm:225: push */
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=(
/* compiler-syntax.scm:225: push */
  f_3239(C_a_i(&a,3),((C_word*)((C_word*)t0)[4])[1],t2)
);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* push in k3168 in k3165 in k3162 in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static C_word C_fcall f_3239(C_word *a,C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_stack_overflow_check;{}
t2=C_a_i_cons(&a,2,t1,((C_word*)((C_word*)t0)[2])[1]);
t3=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
return(t3);}

/* loop in k3168 in k3165 in k3162 in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_fcall f_3249(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(12,0,4)))){
C_save_and_reclaim_args((void *)trf_3249,3,t0,t1,t2);}
a=C_alloc(12);
if(C_truep(C_i_greater_or_equalp(((C_word*)((C_word*)t0)[2])[1],((C_word*)t0)[3]))){
t3=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_3259,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=t1,a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=t2,tmp=(C_word)a,a+=10,tmp);
if(C_truep(C_i_nullp(((C_word*)((C_word*)t0)[10])[1]))){
t4=t3;{
C_word av2[2];
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_3259(2,av2);}}
else{
/* compiler-syntax.scm:234: fail */
t4=((C_word*)t0)[11];
f_3092(t4,t3,C_SCHEME_FALSE,lf[34],C_SCHEME_END_OF_LIST);}}
else{
t3=(
/* compiler-syntax.scm:240: fetch */
  f_3172(((C_word*)((C_word*)t0)[12])[1])
);
t4=C_eqp(t3,C_make_character(126));
if(C_truep(t4)){
t5=(
/* compiler-syntax.scm:242: fetch */
  f_3172(((C_word*)((C_word*)t0)[12])[1])
);
t6=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_3314,a[2]=t5,a[3]=((C_word*)t0)[13],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[14],a[7]=((C_word*)t0)[15],a[8]=((C_word*)t0)[16],a[9]=((C_word*)t0)[12],a[10]=((C_word*)t0)[2],a[11]=((C_word*)t0)[11],tmp=(C_word)a,a+=12,tmp);
/* compiler-syntax.scm:243: endchunk */
t7=((C_word*)((C_word*)t0)[9])[1];
f_3201(t7,t6,t2);}
else{
t5=C_a_i_cons(&a,2,t3,t2);
/* compiler-syntax.scm:275: loop */
t8=t1;
t9=t5;
t1=t8;
t2=t9;
goto loop;}}}

/* k3257 in loop in k3168 in k3165 in k3162 in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 in ... */
static void C_ccall f_3259(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_3259,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3262,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
/* compiler-syntax.scm:235: endchunk */
t3=((C_word*)((C_word*)t0)[8])[1];
f_3201(t3,t2,((C_word*)t0)[9]);}

/* k3260 in k3257 in loop in k3168 in k3165 in k3162 in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in ... */
static void C_ccall f_3262(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(33,c,2)))){
C_save_and_reclaim((void *)f_3262,c,av);}
a=C_alloc(33);
t2=C_a_i_list(&a,2,((C_word*)t0)[2],((C_word*)t0)[3]);
t3=C_a_i_list(&a,1,t2);
t4=C_a_i_list(&a,2,lf[31],((C_word*)t0)[4]);
t5=C_a_i_list(&a,4,lf[32],((C_word*)t0)[2],C_SCHEME_TRUE,t4);
t6=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3285,a[2]=t5,a[3]=t3,a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],tmp=(C_word)a,a+=6,tmp);
/* compiler-syntax.scm:238: scheme#reverse */
t7=*((C_word*)lf[33]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t7;
av2[1]=t6;
av2[2]=((C_word*)((C_word*)t0)[7])[1];
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}

/* k3283 in k3260 in k3257 in loop in k3168 in k3165 in k3162 in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in ... */
static void C_ccall f_3285(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_3285,c,av);}
a=C_alloc(9);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_cons(&a,2,((C_word*)t0)[3],t2);
t4=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[5],t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k3312 in loop in k3168 in k3165 in k3162 in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 in ... */
static void C_ccall f_3314(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,4)))){
C_save_and_reclaim((void *)f_3314,c,av);}
a=C_alloc(16);
t2=C_u_i_char_upcase(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3320,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
switch(t2){
case C_make_character(83):
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3337,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* compiler-syntax.scm:245: next */
t5=((C_word*)((C_word*)t0)[7])[1];
f_3182(t5,t4);
case C_make_character(65):
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3354,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* compiler-syntax.scm:246: next */
t5=((C_word*)((C_word*)t0)[7])[1];
f_3182(t5,t4);
case C_make_character(67):
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3371,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* compiler-syntax.scm:247: next */
t5=((C_word*)((C_word*)t0)[7])[1];
f_3182(t5,t4);
case C_make_character(66):
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3392,a[2]=((C_word*)t0)[8],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[4],tmp=(C_word)a,a+=7,tmp);
/* compiler-syntax.scm:250: next */
t5=((C_word*)((C_word*)t0)[7])[1];
f_3182(t5,t4);
case C_make_character(79):
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3413,a[2]=((C_word*)t0)[8],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[4],tmp=(C_word)a,a+=7,tmp);
/* compiler-syntax.scm:254: next */
t5=((C_word*)((C_word*)t0)[7])[1];
f_3182(t5,t4);
case C_make_character(88):
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3434,a[2]=((C_word*)t0)[8],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[4],tmp=(C_word)a,a+=7,tmp);
/* compiler-syntax.scm:258: next */
t5=((C_word*)((C_word*)t0)[7])[1];
f_3182(t5,t4);
case C_make_character(33):
t4=C_a_i_list(&a,2,lf[35],((C_word*)t0)[5]);
t5=(
/* compiler-syntax.scm:260: push */
  f_3239(C_a_i(&a,3),((C_word*)((C_word*)t0)[6])[1],t4)
);
/* compiler-syntax.scm:274: loop */
t6=((C_word*)((C_word*)t0)[3])[1];
f_3249(t6,((C_word*)t0)[4],C_SCHEME_END_OF_LIST);
case C_make_character(63):
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3456,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[7],tmp=(C_word)a,a+=7,tmp);
/* compiler-syntax.scm:262: next */
t5=((C_word*)((C_word*)t0)[7])[1];
f_3182(t5,t4);
case C_make_character(126):
t4=C_a_i_list(&a,3,lf[24],C_make_character(126),((C_word*)t0)[5]);
t5=(
/* compiler-syntax.scm:265: push */
  f_3239(C_a_i(&a,3),((C_word*)((C_word*)t0)[6])[1],t4)
);
/* compiler-syntax.scm:274: loop */
t6=((C_word*)((C_word*)t0)[3])[1];
f_3249(t6,((C_word*)t0)[4],C_SCHEME_END_OF_LIST);
default:
t4=C_eqp(t2,C_make_character(37));
t5=(C_truep(t4)?t4:C_eqp(t2,C_make_character(78)));
if(C_truep(t5)){
t6=C_a_i_list(&a,3,lf[24],C_make_character(10),((C_word*)t0)[5]);
/* compiler-syntax.scm:266: push */
t7=t3;{
C_word *av2=av;
av2[0]=t7;
av2[1]=(
/* compiler-syntax.scm:266: push */
  f_3239(C_a_i(&a,3),((C_word*)((C_word*)t0)[6])[1],t6)
);
f_3320(2,av2);}}
else{
if(C_truep(C_u_i_char_whitespacep(((C_word*)t0)[2]))){
t6=(
/* compiler-syntax.scm:269: fetch */
  f_3172(((C_word*)((C_word*)t0)[9])[1])
);
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3510,a[2]=((C_word*)t0)[9],a[3]=t8,a[4]=((C_word*)t0)[10],tmp=(C_word)a,a+=5,tmp));
t10=((C_word*)t8)[1];
f_3510(t10,t3,t6);}
else{
/* compiler-syntax.scm:273: fail */
t6=((C_word*)t0)[11];
f_3092(t6,t3,C_SCHEME_TRUE,lf[37],C_a_i_list(&a,1,((C_word*)t0)[2]));}}}}

/* k3318 in k3312 in loop in k3168 in k3165 in k3162 in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in ... */
static void C_ccall f_3320(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_3320,c,av);}
/* compiler-syntax.scm:274: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3249(t2,((C_word*)t0)[3],C_SCHEME_END_OF_LIST);}

/* k3335 in k3312 in loop in k3168 in k3165 in k3162 in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in ... */
static void C_ccall f_3337(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,2)))){
C_save_and_reclaim((void *)f_3337,c,av);}
a=C_alloc(15);
t2=C_a_i_list(&a,4,lf[21],t1,C_SCHEME_TRUE,((C_word*)t0)[2]);
t3=(
/* compiler-syntax.scm:245: push */
  f_3239(C_a_i(&a,3),((C_word*)((C_word*)t0)[3])[1],t2)
);
/* compiler-syntax.scm:274: loop */
t4=((C_word*)((C_word*)t0)[4])[1];
f_3249(t4,((C_word*)t0)[5],C_SCHEME_END_OF_LIST);}

/* k3352 in k3312 in loop in k3168 in k3165 in k3162 in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in ... */
static void C_ccall f_3354(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,2)))){
C_save_and_reclaim((void *)f_3354,c,av);}
a=C_alloc(15);
t2=C_a_i_list(&a,4,lf[21],t1,C_SCHEME_FALSE,((C_word*)t0)[2]);
t3=(
/* compiler-syntax.scm:246: push */
  f_3239(C_a_i(&a,3),((C_word*)((C_word*)t0)[3])[1],t2)
);
/* compiler-syntax.scm:274: loop */
t4=((C_word*)((C_word*)t0)[4])[1];
f_3249(t4,((C_word*)t0)[5],C_SCHEME_END_OF_LIST);}

/* k3369 in k3312 in loop in k3168 in k3165 in k3162 in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in ... */
static void C_ccall f_3371(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_3371,c,av);}
a=C_alloc(12);
t2=C_a_i_list(&a,3,lf[24],t1,((C_word*)t0)[2]);
t3=(
/* compiler-syntax.scm:247: push */
  f_3239(C_a_i(&a,3),((C_word*)((C_word*)t0)[3])[1],t2)
);
/* compiler-syntax.scm:274: loop */
t4=((C_word*)((C_word*)t0)[4])[1];
f_3249(t4,((C_word*)t0)[5],C_SCHEME_END_OF_LIST);}

/* k3390 in k3312 in loop in k3168 in k3165 in k3162 in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in ... */
static void C_ccall f_3392(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,2)))){
C_save_and_reclaim((void *)f_3392,c,av);}
a=C_alloc(24);
t2=C_a_i_list(&a,3,((C_word*)t0)[2],t1,C_fix(2));
t3=C_a_i_list(&a,4,lf[21],t2,C_SCHEME_FALSE,((C_word*)t0)[3]);
t4=(
/* compiler-syntax.scm:249: push */
  f_3239(C_a_i(&a,3),((C_word*)((C_word*)t0)[4])[1],t3)
);
/* compiler-syntax.scm:274: loop */
t5=((C_word*)((C_word*)t0)[5])[1];
f_3249(t5,((C_word*)t0)[6],C_SCHEME_END_OF_LIST);}

/* k3411 in k3312 in loop in k3168 in k3165 in k3162 in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in ... */
static void C_ccall f_3413(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,2)))){
C_save_and_reclaim((void *)f_3413,c,av);}
a=C_alloc(24);
t2=C_a_i_list(&a,3,((C_word*)t0)[2],t1,C_fix(8));
t3=C_a_i_list(&a,4,lf[21],t2,C_SCHEME_FALSE,((C_word*)t0)[3]);
t4=(
/* compiler-syntax.scm:253: push */
  f_3239(C_a_i(&a,3),((C_word*)((C_word*)t0)[4])[1],t3)
);
/* compiler-syntax.scm:274: loop */
t5=((C_word*)((C_word*)t0)[5])[1];
f_3249(t5,((C_word*)t0)[6],C_SCHEME_END_OF_LIST);}

/* k3432 in k3312 in loop in k3168 in k3165 in k3162 in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in ... */
static void C_ccall f_3434(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,2)))){
C_save_and_reclaim((void *)f_3434,c,av);}
a=C_alloc(24);
t2=C_a_i_list(&a,3,((C_word*)t0)[2],t1,C_fix(16));
t3=C_a_i_list(&a,4,lf[21],t2,C_SCHEME_FALSE,((C_word*)t0)[3]);
t4=(
/* compiler-syntax.scm:257: push */
  f_3239(C_a_i(&a,3),((C_word*)((C_word*)t0)[4])[1],t3)
);
/* compiler-syntax.scm:274: loop */
t5=((C_word*)((C_word*)t0)[5])[1];
f_3249(t5,((C_word*)t0)[6],C_SCHEME_END_OF_LIST);}

/* k3454 in k3312 in loop in k3168 in k3165 in k3162 in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in ... */
static void C_ccall f_3456(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_3456,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3459,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* compiler-syntax.scm:263: next */
t3=((C_word*)((C_word*)t0)[6])[1];
f_3182(t3,t2);}

/* k3457 in k3454 in k3312 in loop in k3168 in k3165 in k3162 in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in ... */
static void C_ccall f_3459(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,2)))){
C_save_and_reclaim((void *)f_3459,c,av);}
a=C_alloc(18);
t2=C_a_i_list(&a,5,lf[36],lf[20],((C_word*)t0)[2],((C_word*)t0)[3],t1);
t3=(
/* compiler-syntax.scm:264: push */
  f_3239(C_a_i(&a,3),((C_word*)((C_word*)t0)[4])[1],t2)
);
/* compiler-syntax.scm:274: loop */
t4=((C_word*)((C_word*)t0)[5])[1];
f_3249(t4,((C_word*)t0)[6],C_SCHEME_END_OF_LIST);}

/* skip in k3312 in loop in k3168 in k3165 in k3162 in k3087 in k3084 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in ... */
static void C_fcall f_3510(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(29,0,2)))){
C_save_and_reclaim_args((void *)trf_3510,3,t0,t1,t2);}
a=C_alloc(29);
if(C_truep(C_u_i_char_whitespacep(t2))){
t3=(
/* compiler-syntax.scm:271: fetch */
  f_3172(((C_word*)((C_word*)t0)[2])[1])
);
/* compiler-syntax.scm:271: skip */
t6=t1;
t7=t3;
t1=t6;
t2=t7;
goto loop;}
else{
t3=((C_word*)((C_word*)t0)[4])[1];
t4=C_mutate(((C_word *)((C_word*)t0)[4])+1,C_s_a_i_minus(&a,2,t3,C_fix(1)));
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* k3580 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3582(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_3582,c,av);}
a=C_alloc(3);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3589,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* compiler-syntax.scm:196: scheme#cadar */
t3=*((C_word*)lf[41]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t2=((C_word*)t0)[2];
f_3086(t2,C_SCHEME_FALSE);}}

/* k3587 in k3580 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3589(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3589,c,av);}
t2=((C_word*)t0)[2];
f_3086(t2,C_i_stringp(t1));}

/* k3591 in k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3593(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3593,c,av);}
/* compiler-syntax.scm:195: c */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
av2[3]=C_i_caar(((C_word*)t0)[4]);
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k3605 in a3063 in chicken.compiler.compiler-syntax#compile-format-string in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3607(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,2)))){
C_save_and_reclaim((void *)f_3607,c,av);}
a=C_alloc(18);
if(C_truep(C_i_memq(t1,*((C_word*)lf[16]+1)))){
t2=C_i_car(((C_word*)t0)[2]);
t3=C_i_stringp(t2);
t4=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_3086,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
if(C_truep(t3)){
t5=t4;
f_3086(t5,t3);}
else{
if(C_truep(C_i_listp(C_u_i_car(((C_word*)t0)[2])))){
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3582,a[2]=t4,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3593,a[2]=((C_word*)t0)[9],a[3]=t5,a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
/* compiler-syntax.scm:195: r */
t7=((C_word*)t0)[8];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t7;
av2[1]=t6;
av2[2]=lf[31];
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}
else{
t5=t4;
f_3086(t5,C_SCHEME_FALSE);}}}
else{
t2=((C_word*)t0)[7];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k3613 in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3615(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,5)))){
C_save_and_reclaim((void *)f_3615,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3618,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_3620,tmp=(C_word)a,a+=2,tmp);
/* compiler-syntax.scm:299: r-c-s */
f_2987(t2,lf[57],t3,lf[58]);}

/* k3616 in k3613 in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3618(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3618,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a3619 in k3613 in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3620(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_3620,c,av);}
a=C_alloc(7);
t5=C_i_length(t2);
t6=C_eqp(t5,C_fix(4));
t7=(C_truep(t6)?C_i_memq(lf[45],*((C_word*)lf[16]+1)):C_SCHEME_FALSE);
if(C_truep(t7)){
t8=C_i_cadr(t2);
t9=C_i_caddr(t2);
t10=C_i_cadddr(t2);
t11=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3639,a[2]=t9,a[3]=t10,a[4]=t8,a[5]=t1,a[6]=t3,tmp=(C_word)a,a+=7,tmp);
/* compiler-syntax.scm:306: r */
t12=t3;{
C_word *av2=av;
av2[0]=t12;
av2[1]=t11;
av2[2]=lf[56];
((C_proc)(void*)(*((C_word*)t12+1)))(3,av2);}}
else{
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}}

/* k3637 in a3619 in k3613 in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3639(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_3639,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3642,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* compiler-syntax.scm:307: r */
t3=((C_word*)t0)[6];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[39];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k3640 in k3637 in a3619 in k3613 in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3642(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_3642,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_3645,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
/* compiler-syntax.scm:308: r */
t3=((C_word*)t0)[7];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[55];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k3643 in k3640 in k3637 in a3619 in k3613 in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3645(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_3645,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_3648,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
/* compiler-syntax.scm:309: r */
t3=((C_word*)t0)[8];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[54];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k3646 in k3643 in k3640 in k3637 in a3619 in k3613 in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3648(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_3648,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_3651,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
/* compiler-syntax.scm:310: chicken.base#gensym */
t3=*((C_word*)lf[52]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k3649 in k3646 in k3643 in k3640 in k3637 in a3619 in k3613 in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3651(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_3651,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_3654,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],tmp=(C_word)a,a+=11,tmp);
/* compiler-syntax.scm:311: chicken.base#gensym */
t3=*((C_word*)lf[52]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[53];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k3652 in k3649 in k3646 in k3643 in k3640 in k3637 in a3619 in k3613 in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3654(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_3654,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_3657,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t1,a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],tmp=(C_word)a,a+=12,tmp);
/* compiler-syntax.scm:312: chicken.base#gensym */
t3=*((C_word*)lf[52]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k3655 in k3652 in k3649 in k3646 in k3643 in k3640 in k3637 in a3619 in k3613 in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3657(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(138,c,1)))){
C_save_and_reclaim((void *)f_3657,c,av);}
a=C_alloc(138);
t2=C_a_i_list(&a,2,((C_word*)t0)[2],((C_word*)t0)[3]);
t3=C_a_i_list(&a,2,t1,((C_word*)t0)[4]);
t4=C_a_i_list(&a,2,t2,t3);
t5=C_a_i_list(&a,2,lf[46],lf[47]);
t6=C_a_i_list(&a,3,lf[48],t1,t5);
t7=C_a_i_list(&a,2,lf[49],t6);
t8=C_a_i_list(&a,2,t1,t1);
t9=C_a_i_list(&a,2,((C_word*)t0)[2],((C_word*)t0)[2]);
t10=C_a_i_list(&a,2,t8,t9);
t11=C_a_i_list(&a,2,((C_word*)t0)[5],t1);
t12=C_a_i_list(&a,3,lf[50],t1,C_fix(1));
t13=C_a_i_list(&a,3,lf[50],t1,C_fix(0));
t14=C_a_i_list(&a,3,((C_word*)t0)[6],((C_word*)t0)[2],t13);
t15=C_a_i_list(&a,4,lf[51],((C_word*)t0)[7],t12,t14);
t16=C_a_i_list(&a,4,((C_word*)t0)[8],t11,t15,((C_word*)t0)[2]);
t17=C_a_i_list(&a,4,((C_word*)t0)[9],((C_word*)t0)[7],t10,t16);
t18=((C_word*)t0)[10];{
C_word *av2=av;
av2[0]=t18;
av2[1]=C_a_i_list(&a,4,((C_word*)t0)[11],t4,t7,t17);
((C_proc)(void*)(*((C_word*)t18+1)))(2,av2);}}

/* a3735 in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3736(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_3736,c,av);}
a=C_alloc(7);
t5=C_i_length(t2);
t6=C_eqp(t5,C_fix(4));
t7=(C_truep(t6)?C_i_memq(lf[59],*((C_word*)lf[16]+1)):C_SCHEME_FALSE);
if(C_truep(t7)){
t8=C_i_cadr(t2);
t9=C_i_caddr(t2);
t10=C_i_cadddr(t2);
t11=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3755,a[2]=t10,a[3]=t8,a[4]=t9,a[5]=t1,a[6]=t3,tmp=(C_word)a,a+=7,tmp);
/* compiler-syntax.scm:284: r */
t12=t3;{
C_word *av2=av;
av2[0]=t12;
av2[1]=t11;
av2[2]=lf[56];
((C_proc)(void*)(*((C_word*)t12+1)))(3,av2);}}
else{
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}}

/* k3753 in a3735 in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3755(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_3755,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3758,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* compiler-syntax.scm:285: r */
t3=((C_word*)t0)[6];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[39];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k3756 in k3753 in a3735 in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3758(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_3758,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_3761,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
/* compiler-syntax.scm:286: r */
t3=((C_word*)t0)[7];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[54];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k3759 in k3756 in k3753 in a3735 in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3761(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_3761,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_3764,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
/* compiler-syntax.scm:287: r */
t3=((C_word*)t0)[8];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[55];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k3762 in k3759 in k3756 in k3753 in a3735 in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3764(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_3764,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_3767,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
/* compiler-syntax.scm:288: chicken.base#gensym */
t3=*((C_word*)lf[52]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[61];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k3765 in k3762 in k3759 in k3756 in k3753 in a3735 in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3767(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_3767,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_3770,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],tmp=(C_word)a,a+=11,tmp);
/* compiler-syntax.scm:289: chicken.base#gensym */
t3=*((C_word*)lf[52]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k3768 in k3765 in k3762 in k3759 in k3756 in k3753 in a3735 in k3054 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3770(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(117,c,1)))){
C_save_and_reclaim((void *)f_3770,c,av);}
a=C_alloc(117);
t2=C_a_i_list(&a,2,t1,((C_word*)t0)[2]);
t3=C_a_i_list(&a,1,t2);
t4=C_a_i_list(&a,2,lf[46],lf[60]);
t5=C_a_i_list(&a,3,lf[48],t1,t4);
t6=C_a_i_list(&a,2,lf[49],t5);
t7=C_a_i_list(&a,2,t1,t1);
t8=C_a_i_list(&a,1,t7);
t9=C_a_i_list(&a,2,((C_word*)t0)[3],t1);
t10=C_a_i_list(&a,3,lf[50],t1,C_fix(0));
t11=C_a_i_list(&a,3,lf[50],t1,C_fix(1));
t12=C_a_i_list(&a,3,lf[51],((C_word*)t0)[4],t11);
t13=C_a_i_list(&a,3,((C_word*)t0)[5],t10,t12);
t14=C_a_i_list(&a,4,((C_word*)t0)[6],t9,t13,((C_word*)t0)[7]);
t15=C_a_i_list(&a,4,((C_word*)t0)[8],((C_word*)t0)[4],t8,t14);
t16=((C_word*)t0)[9];{
C_word *av2=av;
av2[0]=t16;
av2[1]=C_a_i_list(&a,4,((C_word*)t0)[10],t3,t6,t15);
((C_proc)(void*)(*((C_word*)t16+1)))(2,av2);}}

/* a3840 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3841(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,7)))){
C_save_and_reclaim((void *)f_3841,c,av);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3845,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* compiler-syntax.scm:185: compile-format-string */
f_3058(t5,lf[64],lf[65],t2,C_i_cdr(t2),t3,t4);}

/* k3843 in a3840 in k3051 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3845(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3845,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(C_truep(t1)?t1:((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a3853 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3854(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,7)))){
C_save_and_reclaim((void *)f_3854,c,av);}
a=C_alloc(4);
t5=C_i_length(t2);
if(C_truep(C_fixnum_greater_or_equal_p(t5,C_fix(3)))){
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3864,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t7=C_i_cadr(t2);
t8=C_u_i_cdr(t2);
/* compiler-syntax.scm:174: compile-format-string */
f_3058(t6,lf[68],t7,t2,C_u_i_cdr(t8),t3,t4);}
else{
t6=t1;{
C_word *av2=av;
av2[0]=t6;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* k3862 in a3853 in k3048 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3864(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3864,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(C_truep(t1)?t1:((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a3880 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3881(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_3881,c,av);}
a=C_alloc(6);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3885,a[2]=t1,a[3]=t3,a[4]=t2,a[5]=t4,tmp=(C_word)a,a+=6,tmp);
/* compiler-syntax.scm:156: chicken.base#gensym */
t6=*((C_word*)lf[52]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[40];
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k3883 in a3880 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3885(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,7)))){
C_save_and_reclaim((void *)f_3885,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3888,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
t3=C_i_car(((C_word*)t0)[4]);
t4=C_eqp(t3,lf[73]);
t5=(C_truep(t4)?lf[74]:lf[17]);
/* compiler-syntax.scm:157: compile-format-string */
f_3058(t2,t5,t1,((C_word*)t0)[4],C_u_i_cdr(((C_word*)t0)[4]),((C_word*)t0)[3],((C_word*)t0)[5]);}

/* k3886 in k3883 in a3880 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3888(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_3888,c,av);}
a=C_alloc(6);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3898,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* compiler-syntax.scm:161: r */
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[39];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k3896 in k3886 in k3883 in a3880 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3898(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_3898,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3922,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* compiler-syntax.scm:161: r */
t3=((C_word*)t0)[5];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[72];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k3908 in k3920 in k3896 in k3886 in k3883 in a3880 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3910(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,1)))){
C_save_and_reclaim((void *)f_3910,c,av);}
a=C_alloc(18);
t2=C_a_i_list(&a,2,t1,((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_list(&a,4,((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k3920 in k3896 in k3886 in k3883 in a3880 in k3045 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3922(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(19,c,2)))){
C_save_and_reclaim((void *)f_3922,c,av);}
a=C_alloc(19);
t2=C_a_i_list(&a,1,t1);
t3=C_a_i_list(&a,2,((C_word*)t0)[2],t2);
t4=C_a_i_list(&a,1,t3);
t5=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3910,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t4,a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* compiler-syntax.scm:163: r */
t6=((C_word*)t0)[6];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[71];
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* a3964 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3965(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_3965,c,av);}
a=C_alloc(5);
t5=C_i_length(t2);
t6=C_fixnum_greaterp(t5,C_fix(1));
t7=(C_truep(t6)?C_i_memq(lf[83],*((C_word*)lf[16]+1)):C_SCHEME_FALSE);
if(C_truep(t7)){
t8=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3975,a[2]=t2,a[3]=t1,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* compiler-syntax.scm:145: r */
t9=t3;{
C_word *av2=av;
av2[0]=t9;
av2[1]=t8;
av2[2]=lf[85];
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}
else{
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}}

/* k3973 in a3964 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3975(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_3975,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3982,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* compiler-syntax.scm:146: r */
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[84];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k3980 in k3973 in a3964 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3982(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,3)))){
C_save_and_reclaim((void *)f_3982,c,av);}
a=C_alloc(14);
t2=C_a_i_list(&a,1,((C_word*)t0)[2]);
t3=C_i_cdr(((C_word*)t0)[3]);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3993,a[2]=((C_word*)t0)[4],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3995,a[2]=t6,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp));
t8=((C_word*)t6)[1];
f_3995(t8,t4,t3);}

/* k3991 in k3980 in k3973 in a3964 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_3993(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_3993,c,av);}
a=C_alloc(9);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,3,((C_word*)t0)[3],((C_word*)t0)[4],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* foldr1075 in k3980 in k3973 in a3964 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_fcall f_3995(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_3995,3,t0,t1,t2);}
a=C_alloc(4);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4013,a[2]=t1,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
t6=t4;
t7=C_slot(t2,C_fix(1));
t1=t6;
t2=t7;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k4011 in foldr1075 in k3980 in k3973 in a3964 in k3042 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_4013(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_4013,c,av);}
a=C_alloc(6);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list2(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a4028 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_4029(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_4029,c,av);}
a=C_alloc(5);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4033,a[2]=t2,a[3]=t1,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* compiler-syntax.scm:102: r */
t6=t3;{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[39];
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k4031 in a4028 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_4033(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_4033,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4036,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* compiler-syntax.scm:103: r */
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[55];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4034 in k4031 in a4028 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_4036(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_4036,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4039,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* compiler-syntax.scm:104: r */
t3=((C_word*)t0)[5];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[97];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4037 in k4034 in k4031 in a4028 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_4039(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_4039,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4042,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* compiler-syntax.scm:105: chicken.base#gensym */
t3=*((C_word*)lf[52]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k4040 in k4037 in k4034 in k4031 in a4028 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_4042(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_4042,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_4045,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
/* compiler-syntax.scm:106: r */
t3=((C_word*)t0)[7];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[96];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4043 in k4040 in k4037 in k4034 in k4031 in a4028 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_4045(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_4045,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_4048,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
/* compiler-syntax.scm:107: r */
t3=((C_word*)t0)[8];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[95];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4046 in k4043 in k4040 in k4037 in k4034 in k4031 in a4028 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_4048(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_4048,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_4051,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t1,a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],tmp=(C_word)a,a+=11,tmp);
/* compiler-syntax.scm:108: chicken.base#gensym */
t3=*((C_word*)lf[52]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k4049 in k4046 in k4043 in k4040 in k4037 in k4034 in k4031 in a4028 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_4051(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_4051,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_4054,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],tmp=(C_word)a,a+=12,tmp);
/* compiler-syntax.scm:109: chicken.base#gensym */
t3=*((C_word*)lf[52]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k4052 in k4049 in k4046 in k4043 in k4040 in k4037 in k4034 in k4031 in a4028 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_4054(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_4054,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_4057,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],tmp=(C_word)a,a+=13,tmp);
/* compiler-syntax.scm:110: chicken.base#gensym */
t3=*((C_word*)lf[52]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k4055 in k4052 in k4049 in k4046 in k4043 in k4040 in k4037 in k4034 in k4031 in a4028 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_4057(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,2)))){
C_save_and_reclaim((void *)f_4057,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_4060,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],a[13]=((C_word*)t0)[12],tmp=(C_word)a,a+=14,tmp);
/* compiler-syntax.scm:111: r */
t3=((C_word*)t0)[12];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[31];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4058 in k4055 in k4052 in k4049 in k4046 in k4043 in k4040 in k4037 in k4034 in k4031 in a4028 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_4060(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,2)))){
C_save_and_reclaim((void *)f_4060,c,av);}
a=C_alloc(15);
t2=(*a=C_CLOSURE_TYPE|14,a[1]=(C_word)f_4063,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],a[13]=((C_word*)t0)[12],a[14]=((C_word*)t0)[13],tmp=(C_word)a,a+=15,tmp);
/* compiler-syntax.scm:112: r */
t3=((C_word*)t0)[13];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[94];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4061 in k4058 in k4055 in k4052 in k4049 in k4046 in k4043 in k4040 in k4037 in k4034 in k4031 in a4028 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_4063(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,2)))){
C_save_and_reclaim((void *)f_4063,c,av);}
a=C_alloc(15);
t2=(*a=C_CLOSURE_TYPE|14,a[1]=(C_word)f_4066,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],tmp=(C_word)a,a+=15,tmp);
/* compiler-syntax.scm:113: r */
t3=((C_word*)t0)[14];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[84];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4064 in k4061 in k4058 in k4055 in k4052 in k4049 in k4046 in k4043 in k4040 in k4037 in k4034 in k4031 in a4028 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_4066(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,2)))){
C_save_and_reclaim((void *)f_4066,c,av);}
a=C_alloc(15);
t2=(*a=C_CLOSURE_TYPE|14,a[1]=(C_word)f_4069,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],tmp=(C_word)a,a+=15,tmp);
/* compiler-syntax.scm:114: r */
t3=((C_word*)t0)[14];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[93];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4067 in k4064 in k4061 in k4058 in k4055 in k4052 in k4049 in k4046 in k4043 in k4040 in k4037 in k4034 in k4031 in a4028 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_4069(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,2)))){
C_save_and_reclaim((void *)f_4069,c,av);}
a=C_alloc(15);
t2=(*a=C_CLOSURE_TYPE|14,a[1]=(C_word)f_4072,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=t1,a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],a[13]=((C_word*)t0)[12],a[14]=((C_word*)t0)[13],tmp=(C_word)a,a+=15,tmp);
/* compiler-syntax.scm:115: r */
t3=((C_word*)t0)[14];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[54];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4070 in k4067 in k4064 in k4061 in k4058 in k4055 in k4052 in k4049 in k4046 in k4043 in k4040 in k4037 in k4034 in k4031 in a4028 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 in ... */
static void C_ccall f_4072(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(20,c,2)))){
C_save_and_reclaim((void *)f_4072,c,av);}
a=C_alloc(20);
t2=C_i_cddr(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|16,a[1]=(C_word)f_4081,a[2]=t2,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[2],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=t1,a[12]=((C_word*)t0)[10],a[13]=((C_word*)t0)[11],a[14]=((C_word*)t0)[12],a[15]=((C_word*)t0)[13],a[16]=((C_word*)t0)[14],tmp=(C_word)a,a+=17,tmp);
if(C_truep(C_i_memq(lf[91],*((C_word*)lf[92]+1)))){
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4559,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* compiler-syntax.scm:118: length+ */
f_2775(t4,((C_word*)t0)[2]);}
else{
t4=t3;
f_4081(t4,C_SCHEME_FALSE);}}

/* k4079 in k4070 in k4067 in k4064 in k4061 in k4058 in k4055 in k4052 in k4049 in k4046 in k4043 in k4040 in k4037 in k4034 in k4031 in a4028 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in ... */
static void C_fcall f_4081(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(29,0,3)))){
C_save_and_reclaim_args((void *)trf_4081,2,t0,t1);}
a=C_alloc(29);
if(C_truep(t1)){
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=C_i_check_list_2(((C_word*)t0)[2],lf[87]);
t7=(*a=C_CLOSURE_TYPE|16,a[1]=(C_word)f_4095,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],a[12]=((C_word*)t0)[13],a[13]=((C_word*)t0)[14],a[14]=((C_word*)t0)[15],a[15]=((C_word*)t0)[16],a[16]=((C_word*)t0)[2],tmp=(C_word)a,a+=17,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4517,a[2]=t4,a[3]=t9,a[4]=t5,tmp=(C_word)a,a+=5,tmp));
t11=((C_word*)t9)[1];
f_4517(t11,t7,((C_word*)t0)[2]);}
else{
t2=((C_word*)t0)[10];{
C_word av2[2];
av2[0]=t2;
av2[1]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k4093 in k4079 in k4070 in k4067 in k4064 in k4061 in k4058 in k4055 in k4052 in k4049 in k4046 in k4043 in k4040 in k4037 in k4034 in k4031 in a4028 in k3039 in k1692 in k1689 in k1686 in k1683 in ... */
static void C_ccall f_4095(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(70,c,4)))){
C_save_and_reclaim((void *)f_4095,c,av);}
a=C_alloc(70);
t2=C_a_i_list(&a,1,lf[88]);
t3=C_a_i_list(&a,2,((C_word*)t0)[2],C_SCHEME_END_OF_LIST);
t4=C_a_i_list(&a,3,((C_word*)t0)[3],t2,t3);
t5=C_a_i_list(&a,2,((C_word*)t0)[4],t4);
t6=C_a_i_list(&a,1,t5);
t7=C_a_i_list(&a,2,((C_word*)t0)[5],((C_word*)t0)[4]);
t8=C_i_cadr(((C_word*)t0)[6]);
t9=C_a_i_list(&a,2,((C_word*)t0)[7],t8);
t10=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t11=t10;
t12=(*a=C_VECTOR_TYPE|1,a[1]=t11,tmp=(C_word)a,a+=2,tmp);
t13=((C_word*)t12)[1];
t14=C_i_check_list_2(t1,lf[87]);
t15=(*a=C_CLOSURE_TYPE|18,a[1]=(C_word)f_4447,a[2]=t9,a[3]=t7,a[4]=((C_word*)t0)[8],a[5]=((C_word*)t0)[9],a[6]=t6,a[7]=((C_word*)t0)[2],a[8]=((C_word*)t0)[10],a[9]=((C_word*)t0)[11],a[10]=((C_word*)t0)[7],a[11]=((C_word*)t0)[3],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[4],a[14]=((C_word*)t0)[13],a[15]=((C_word*)t0)[14],a[16]=((C_word*)t0)[5],a[17]=((C_word*)t0)[15],a[18]=t1,tmp=(C_word)a,a+=19,tmp);
t16=C_SCHEME_UNDEFINED;
t17=(*a=C_VECTOR_TYPE|1,a[1]=t16,tmp=(C_word)a,a+=2,tmp);
t18=C_set_block_item(t17,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4449,a[2]=t12,a[3]=t17,a[4]=t13,tmp=(C_word)a,a+=5,tmp));
t19=((C_word*)t17)[1];
f_4449(t19,t15,t1,((C_word*)t0)[16]);}

/* k4116 in k4445 in k4093 in k4079 in k4070 in k4067 in k4064 in k4061 in k4058 in k4055 in k4052 in k4049 in k4046 in k4043 in k4040 in k4037 in k4034 in k4031 in a4028 in k3039 in k1692 in k1689 in ... */
static void C_ccall f_4118(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,1)))){
C_save_and_reclaim((void *)f_4118,c,av);}
a=C_alloc(15);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_cons(&a,2,((C_word*)t0)[3],t2);
t4=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_list(&a,3,((C_word*)t0)[3],((C_word*)t0)[5],t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* g917 in k4445 in k4093 in k4079 in k4070 in k4067 in k4064 in k4061 in k4058 in k4055 in k4052 in k4049 in k4046 in k4043 in k4040 in k4037 in k4034 in k4031 in a4028 in k3039 in k1692 in k1689 in ... */
static C_word C_fcall f_4123(C_word *a,C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_stack_overflow_check;{}
t2=C_a_i_list(&a,2,((C_word*)t0)[2],lf[87]);
t3=C_a_i_list(&a,3,lf[48],t1,t2);
return(C_a_i_list(&a,2,lf[49],t3));}

/* k4136 in k4445 in k4093 in k4079 in k4070 in k4067 in k4064 in k4061 in k4058 in k4055 in k4052 in k4049 in k4046 in k4043 in k4040 in k4037 in k4034 in k4031 in a4028 in k3039 in k1692 in k1689 in ... */
static void C_ccall f_4138(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(29,c,4)))){
C_save_and_reclaim((void *)f_4138,c,av);}
a=C_alloc(29);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=(*a=C_CLOSURE_TYPE|16,a[1]=(C_word)f_4153,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=t1,a[16]=((C_word*)t0)[15],tmp=(C_word)a,a+=17,tmp);
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4345,a[2]=t4,a[3]=t8,a[4]=t5,tmp=(C_word)a,a+=5,tmp));
t10=((C_word*)t8)[1];
f_4345(t10,t6,((C_word*)t0)[15],((C_word*)t0)[15]);}

/* k4151 in k4136 in k4445 in k4093 in k4079 in k4070 in k4067 in k4064 in k4061 in k4058 in k4055 in k4052 in k4049 in k4046 in k4043 in k4040 in k4037 in k4034 in k4031 in a4028 in k3039 in k1692 in ... */
static void C_ccall f_4153(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(33,c,3)))){
C_save_and_reclaim((void *)f_4153,c,av);}
a=C_alloc(33);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4302,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t7=(*a=C_CLOSURE_TYPE|16,a[1]=(C_word)f_4309,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],a[12]=((C_word*)t0)[13],a[13]=t1,a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],tmp=(C_word)a,a+=17,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4311,a[2]=t6,a[3]=t4,a[4]=t9,a[5]=t5,tmp=(C_word)a,a+=6,tmp));
t11=((C_word*)t9)[1];
f_4311(t11,t7,((C_word*)t0)[16]);}

/* k4199 in k4261 in k4307 in k4151 in k4136 in k4445 in k4093 in k4079 in k4070 in k4067 in k4064 in k4061 in k4058 in k4055 in k4052 in k4049 in k4046 in k4043 in k4040 in k4037 in k4034 in k4031 in ... */
static void C_ccall f_4201(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(57,c,3)))){
C_save_and_reclaim((void *)f_4201,c,av);}
a=C_alloc(57);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_cons(&a,2,lf[51],t2);
t4=C_a_i_list(&a,5,((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6],t3);
t5=C_a_i_list(&a,3,lf[50],((C_word*)t0)[7],C_fix(1));
t6=C_a_i_list(&a,4,((C_word*)t0)[8],((C_word*)t0)[9],t4,t5);
t7=C_a_i_list(&a,4,((C_word*)t0)[3],((C_word*)t0)[2],((C_word*)t0)[10],t6);
t8=C_a_i_list(&a,1,t7);
/* compiler-syntax.scm:120: ##sys#append */
t9=*((C_word*)lf[90]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t9;
av2[1]=((C_word*)t0)[11];
av2[2]=((C_word*)t0)[12];
av2[3]=t8;
((C_proc)(void*)(*((C_word*)t9+1)))(4,av2);}}

/* map-loop1032 in k4261 in k4307 in k4151 in k4136 in k4445 in k4093 in k4079 in k4070 in k4067 in k4064 in k4061 in k4058 in k4055 in k4052 in k4049 in k4046 in k4043 in k4040 in k4037 in k4034 in k4031 in ... */
static void C_fcall f_4203(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(12,0,2)))){
C_save_and_reclaim_args((void *)trf_4203,3,t0,t1,t2);}
a=C_alloc(12);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_a_i_list(&a,3,lf[50],t3,C_fix(1));
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k4261 in k4307 in k4151 in k4136 in k4445 in k4093 in k4079 in k4070 in k4067 in k4064 in k4061 in k4058 in k4055 in k4052 in k4049 in k4046 in k4043 in k4040 in k4037 in k4034 in k4031 in a4028 in ... */
static void C_ccall f_4263(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(73,c,3)))){
C_save_and_reclaim((void *)f_4263,c,av);}
a=C_alloc(73);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_list(&a,2,((C_word*)t0)[3],C_SCHEME_END_OF_LIST);
t4=C_a_i_list(&a,3,((C_word*)t0)[4],t2,t3);
t5=C_a_i_list(&a,2,((C_word*)t0)[5],t4);
t6=C_a_i_list(&a,1,t5);
t7=C_a_i_list(&a,4,lf[89],((C_word*)t0)[6],C_fix(1),((C_word*)t0)[5]);
t8=C_a_i_list(&a,3,((C_word*)t0)[7],((C_word*)t0)[6],((C_word*)t0)[5]);
t9=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t10=t9;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=((C_word*)t11)[1];
t13=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_4201,a[2]=((C_word*)t0)[8],a[3]=((C_word*)t0)[9],a[4]=t6,a[5]=t7,a[6]=t8,a[7]=((C_word*)t0)[10],a[8]=((C_word*)t0)[11],a[9]=((C_word*)t0)[12],a[10]=((C_word*)t0)[13],a[11]=((C_word*)t0)[14],a[12]=((C_word*)t0)[15],tmp=(C_word)a,a+=13,tmp);
t14=C_SCHEME_UNDEFINED;
t15=(*a=C_VECTOR_TYPE|1,a[1]=t14,tmp=(C_word)a,a+=2,tmp);
t16=C_set_block_item(t15,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4203,a[2]=t11,a[3]=t15,a[4]=t12,tmp=(C_word)a,a+=5,tmp));
t17=((C_word*)t15)[1];
f_4203(t17,t13,((C_word*)t0)[16]);}

/* map-loop1002 in k4307 in k4151 in k4136 in k4445 in k4093 in k4079 in k4070 in k4067 in k4064 in k4061 in k4058 in k4055 in k4052 in k4049 in k4046 in k4043 in k4040 in k4037 in k4034 in k4031 in a4028 in ... */
static void C_fcall f_4265(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(12,0,2)))){
C_save_and_reclaim_args((void *)trf_4265,3,t0,t1,t2);}
a=C_alloc(12);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_a_i_list(&a,3,lf[50],t3,C_fix(0));
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* g978 in k4151 in k4136 in k4445 in k4093 in k4079 in k4070 in k4067 in k4064 in k4061 in k4058 in k4055 in k4052 in k4049 in k4046 in k4043 in k4040 in k4037 in k4034 in k4031 in a4028 in k3039 in ... */
static C_word C_fcall f_4302(C_word *a,C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_stack_overflow_check;{}
return(C_a_i_list(&a,2,((C_word*)t0)[2],t1));}

/* k4307 in k4151 in k4136 in k4445 in k4093 in k4079 in k4070 in k4067 in k4064 in k4061 in k4058 in k4055 in k4052 in k4049 in k4046 in k4043 in k4040 in k4037 in k4034 in k4031 in a4028 in k3039 in ... */
static void C_ccall f_4309(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(32,c,3)))){
C_save_and_reclaim((void *)f_4309,c,av);}
a=C_alloc(32);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=(*a=C_CLOSURE_TYPE|16,a[1]=(C_word)f_4263,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],a[12]=t2,a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],tmp=(C_word)a,a+=17,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4265,a[2]=t5,a[3]=t9,a[4]=t6,tmp=(C_word)a,a+=5,tmp));
t11=((C_word*)t9)[1];
f_4265(t11,t7,((C_word*)t0)[16]);}

/* map-loop972 in k4151 in k4136 in k4445 in k4093 in k4079 in k4070 in k4067 in k4064 in k4061 in k4058 in k4055 in k4052 in k4049 in k4046 in k4043 in k4040 in k4037 in k4034 in k4031 in a4028 in k3039 in ... */
static void C_fcall f_4311(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(9,0,2)))){
C_save_and_reclaim_args((void *)trf_4311,3,t0,t1,t2);}
a=C_alloc(9);
if(C_truep(C_i_pairp(t2))){
t3=(
/* compiler-syntax.scm:128: g978 */
  f_4302(C_a_i(&a,6),((C_word*)t0)[2],C_slot(t2,C_fix(0)))
);
t4=C_a_i_cons(&a,2,t3,C_SCHEME_END_OF_LIST);
t5=C_i_setslot(((C_word*)((C_word*)t0)[3])[1],C_fix(1),t4);
t6=C_mutate(((C_word *)((C_word*)t0)[3])+1,t4);
t8=t1;
t9=C_slot(t2,C_fix(1));
t1=t8;
t2=t9;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* map-loop941 in k4136 in k4445 in k4093 in k4079 in k4070 in k4067 in k4064 in k4061 in k4058 in k4055 in k4052 in k4049 in k4046 in k4043 in k4040 in k4037 in k4034 in k4031 in a4028 in k3039 in k1692 in ... */
static void C_fcall f_4345(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(9,0,3)))){
C_save_and_reclaim_args((void *)trf_4345,4,t0,t1,t2,t3);}
a=C_alloc(9);
t4=C_i_pairp(t2);
t5=(C_truep(t4)?C_i_pairp(t3):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_slot(t2,C_fix(0));
t7=C_slot(t3,C_fix(0));
t8=C_a_i_list2(&a,2,t6,t7);
t9=C_a_i_cons(&a,2,t8,C_SCHEME_END_OF_LIST);
t10=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t9);
t11=C_mutate(((C_word *)((C_word*)t0)[2])+1,t9);
t13=t1;
t14=C_slot(t2,C_fix(1));
t15=C_slot(t3,C_fix(1));
t1=t13;
t2=t14;
t3=t15;
goto loop;}
else{
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* map-loop911 in k4445 in k4093 in k4079 in k4070 in k4067 in k4064 in k4061 in k4058 in k4055 in k4052 in k4049 in k4046 in k4043 in k4040 in k4037 in k4034 in k4031 in a4028 in k3039 in k1692 in k1689 in ... */
static void C_fcall f_4393(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(24,0,2)))){
C_save_and_reclaim_args((void *)trf_4393,3,t0,t1,t2);}
a=C_alloc(24);
if(C_truep(C_i_pairp(t2))){
t3=(
/* compiler-syntax.scm:124: g917 */
  f_4123(C_a_i(&a,21),((C_word*)t0)[2],C_slot(t2,C_fix(0)))
);
t4=C_a_i_cons(&a,2,t3,C_SCHEME_END_OF_LIST);
t5=C_i_setslot(((C_word*)((C_word*)t0)[3])[1],C_fix(1),t4);
t6=C_mutate(((C_word *)((C_word*)t0)[3])+1,t4);
t8=t1;
t9=C_slot(t2,C_fix(1));
t1=t8;
t2=t9;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k4445 in k4093 in k4079 in k4070 in k4067 in k4064 in k4061 in k4058 in k4055 in k4052 in k4049 in k4046 in k4043 in k4040 in k4037 in k4034 in k4031 in a4028 in k3039 in k1692 in k1689 in k1686 in ... */
static void C_ccall f_4447(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(44,c,3)))){
C_save_and_reclaim((void *)f_4447,c,av);}
a=C_alloc(44);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_cons(&a,2,((C_word*)t0)[3],t2);
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4118,a[2]=t3,a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],tmp=(C_word)a,a+=6,tmp);
t5=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t6=t5;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=((C_word*)t7)[1];
t9=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4123,a[2]=((C_word*)t0)[7],tmp=(C_word)a,a+=3,tmp);
t10=(*a=C_CLOSURE_TYPE|15,a[1]=(C_word)f_4138,a[2]=((C_word*)t0)[8],a[3]=((C_word*)t0)[9],a[4]=((C_word*)t0)[10],a[5]=((C_word*)t0)[7],a[6]=((C_word*)t0)[11],a[7]=((C_word*)t0)[12],a[8]=((C_word*)t0)[13],a[9]=((C_word*)t0)[14],a[10]=((C_word*)t0)[15],a[11]=((C_word*)t0)[4],a[12]=((C_word*)t0)[16],a[13]=((C_word*)t0)[17],a[14]=t4,a[15]=((C_word*)t0)[18],tmp=(C_word)a,a+=16,tmp);
t11=C_SCHEME_UNDEFINED;
t12=(*a=C_VECTOR_TYPE|1,a[1]=t11,tmp=(C_word)a,a+=2,tmp);
t13=C_set_block_item(t12,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4393,a[2]=t9,a[3]=t7,a[4]=t12,a[5]=t8,tmp=(C_word)a,a+=6,tmp));
t14=((C_word*)t12)[1];
f_4393(t14,t10,((C_word*)t0)[18]);}

/* map-loop880 in k4093 in k4079 in k4070 in k4067 in k4064 in k4061 in k4058 in k4055 in k4052 in k4049 in k4046 in k4043 in k4040 in k4037 in k4034 in k4031 in a4028 in k3039 in k1692 in k1689 in k1686 in ... */
static void C_fcall f_4449(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(9,0,3)))){
C_save_and_reclaim_args((void *)trf_4449,4,t0,t1,t2,t3);}
a=C_alloc(9);
t4=C_i_pairp(t2);
t5=(C_truep(t4)?C_i_pairp(t3):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_slot(t2,C_fix(0));
t7=C_slot(t3,C_fix(0));
t8=C_a_i_list2(&a,2,t6,t7);
t9=C_a_i_cons(&a,2,t8,C_SCHEME_END_OF_LIST);
t10=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t9);
t11=C_mutate(((C_word *)((C_word*)t0)[2])+1,t9);
t13=t1;
t14=C_slot(t2,C_fix(1));
t15=C_slot(t3,C_fix(1));
t1=t13;
t2=t14;
t3=t15;
goto loop;}
else{
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* map-loop850 in k4079 in k4070 in k4067 in k4064 in k4061 in k4058 in k4055 in k4052 in k4049 in k4046 in k4043 in k4040 in k4037 in k4034 in k4031 in a4028 in k3039 in k1692 in k1689 in k1686 in k1683 in ... */
static void C_fcall f_4517(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_4517,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4542,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* compiler-syntax.scm:119: chicken.base#gensym */
t4=*((C_word*)lf[52]+1);{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k4540 in map-loop850 in k4079 in k4070 in k4067 in k4064 in k4061 in k4058 in k4055 in k4052 in k4049 in k4046 in k4043 in k4040 in k4037 in k4034 in k4031 in a4028 in k3039 in k1692 in k1689 in k1686 in ... */
static void C_ccall f_4542(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4542,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_4517(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k4557 in k4070 in k4067 in k4064 in k4061 in k4058 in k4055 in k4052 in k4049 in k4046 in k4043 in k4040 in k4037 in k4034 in k4031 in a4028 in k3039 in k1692 in k1689 in k1686 in k1683 in k1680 in ... */
static void C_ccall f_4559(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4559,c,av);}
t2=((C_word*)t0)[2];
f_4081(t2,C_i_greaterp(t1,C_fix(2)));}

/* a4560 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_4561(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_4561,c,av);}
a=C_alloc(5);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4565,a[2]=t2,a[3]=t1,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* compiler-syntax.scm:72: r */
t6=t3;{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[39];
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k4563 in a4560 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_4565(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_4565,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4568,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* compiler-syntax.scm:73: r */
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[55];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4566 in k4563 in a4560 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_4568(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_4568,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4571,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* compiler-syntax.scm:74: r */
t3=((C_word*)t0)[5];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[102];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4569 in k4566 in k4563 in a4560 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_4571(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_4571,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4574,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* compiler-syntax.scm:75: chicken.base#gensym */
t3=*((C_word*)lf[52]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k4572 in k4569 in k4566 in k4563 in a4560 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_4574(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_4574,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_4577,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
/* compiler-syntax.scm:76: r */
t3=((C_word*)t0)[7];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[94];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4575 in k4572 in k4569 in k4566 in k4563 in a4560 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_4577(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_4577,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_4580,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t1,a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
/* compiler-syntax.scm:77: r */
t3=((C_word*)t0)[8];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[31];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4578 in k4575 in k4572 in k4569 in k4566 in k4563 in a4560 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_4580(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_4580,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_4583,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],tmp=(C_word)a,a+=11,tmp);
/* compiler-syntax.scm:78: r */
t3=((C_word*)t0)[9];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[93];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4581 in k4578 in k4575 in k4572 in k4569 in k4566 in k4563 in a4560 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_4583(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_4583,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_4586,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t1,a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],tmp=(C_word)a,a+=12,tmp);
/* compiler-syntax.scm:79: r */
t3=((C_word*)t0)[10];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[54];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4584 in k4581 in k4578 in k4575 in k4572 in k4569 in k4566 in k4563 in a4560 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_4586(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_4586,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_4589,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t1,a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],tmp=(C_word)a,a+=12,tmp);
/* compiler-syntax.scm:80: r */
t3=((C_word*)t0)[11];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[84];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4587 in k4584 in k4581 in k4578 in k4575 in k4572 in k4569 in k4566 in k4563 in a4560 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_4589(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,2)))){
C_save_and_reclaim((void *)f_4589,c,av);}
a=C_alloc(16);
t2=C_i_cddr(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_4598,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],tmp=(C_word)a,a+=13,tmp);
if(C_truep(C_i_memq(lf[101],*((C_word*)lf[92]+1)))){
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5016,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* compiler-syntax.scm:83: length+ */
f_2775(t4,((C_word*)t0)[2]);}
else{
t4=t3;
f_4598(t4,C_SCHEME_FALSE);}}

/* k4596 in k4587 in k4584 in k4581 in k4578 in k4575 in k4572 in k4569 in k4566 in k4563 in a4560 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_fcall f_4598(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(25,0,3)))){
C_save_and_reclaim_args((void *)trf_4598,2,t0,t1);}
a=C_alloc(25);
if(C_truep(t1)){
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=C_i_check_list_2(((C_word*)t0)[2],lf[87]);
t7=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_4612,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],a[12]=((C_word*)t0)[2],tmp=(C_word)a,a+=13,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4974,a[2]=t4,a[3]=t9,a[4]=t5,tmp=(C_word)a,a+=5,tmp));
t11=((C_word*)t9)[1];
f_4974(t11,t7,((C_word*)t0)[2]);}
else{
t2=((C_word*)t0)[5];{
C_word av2[2];
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k4610 in k4596 in k4587 in k4584 in k4581 in k4578 in k4575 in k4572 in k4569 in k4566 in k4563 in a4560 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_4612(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(31,c,4)))){
C_save_and_reclaim((void *)f_4612,c,av);}
a=C_alloc(31);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=C_a_i_list(&a,2,((C_word*)t0)[3],t2);
t4=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t5=t4;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=((C_word*)t6)[1];
t8=C_i_check_list_2(t1,lf[87]);
t9=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_4920,a[2]=t3,a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[3],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=t1,tmp=(C_word)a,a+=13,tmp);
t10=C_SCHEME_UNDEFINED;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_set_block_item(t11,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4922,a[2]=t6,a[3]=t11,a[4]=t7,tmp=(C_word)a,a+=5,tmp));
t13=((C_word*)t11)[1];
f_4922(t13,t9,t1,((C_word*)t0)[12]);}

/* k4625 in k4918 in k4610 in k4596 in k4587 in k4584 in k4581 in k4578 in k4575 in k4572 in k4569 in k4566 in k4563 in a4560 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_4627(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_4627,c,av);}
a=C_alloc(6);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[4],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* g682 in k4918 in k4610 in k4596 in k4587 in k4584 in k4581 in k4578 in k4575 in k4572 in k4569 in k4566 in k4563 in a4560 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static C_word C_fcall f_4632(C_word *a,C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_stack_overflow_check;{}
t2=C_a_i_list(&a,2,((C_word*)t0)[2],lf[100]);
t3=C_a_i_list(&a,3,lf[48],t1,t2);
return(C_a_i_list(&a,2,lf[49],t3));}

/* k4645 in k4918 in k4610 in k4596 in k4587 in k4584 in k4581 in k4578 in k4575 in k4572 in k4569 in k4566 in k4563 in a4560 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_4647(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,4)))){
C_save_and_reclaim((void *)f_4647,c,av);}
a=C_alloc(24);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_4662,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=t1,a[11]=((C_word*)t0)[10],tmp=(C_word)a,a+=12,tmp);
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4826,a[2]=t4,a[3]=t8,a[4]=t5,tmp=(C_word)a,a+=5,tmp));
t10=((C_word*)t8)[1];
f_4826(t10,t6,((C_word*)t0)[10],((C_word*)t0)[10]);}

/* k4660 in k4645 in k4918 in k4610 in k4596 in k4587 in k4584 in k4581 in k4578 in k4575 in k4572 in k4569 in k4566 in k4563 in a4560 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_4662(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(28,c,3)))){
C_save_and_reclaim((void *)f_4662,c,av);}
a=C_alloc(28);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4783,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t7=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_4790,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=t1,a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],tmp=(C_word)a,a+=12,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4792,a[2]=t6,a[3]=t4,a[4]=t9,a[5]=t5,tmp=(C_word)a,a+=6,tmp));
t11=((C_word*)t9)[1];
f_4792(t11,t7,((C_word*)t0)[11]);}

/* k4696 in k4742 in k4788 in k4660 in k4645 in k4918 in k4610 in k4596 in k4587 in k4584 in k4581 in k4578 in k4575 in k4572 in k4569 in k4566 in k4563 in a4560 in k1692 in k1689 in k1686 in k1683 in ... */
static void C_ccall f_4698(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(39,c,3)))){
C_save_and_reclaim((void *)f_4698,c,av);}
a=C_alloc(39);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_cons(&a,2,lf[51],t2);
t4=C_a_i_list(&a,3,((C_word*)t0)[3],((C_word*)t0)[4],t3);
t5=C_a_i_list(&a,3,((C_word*)t0)[5],((C_word*)t0)[6],t4);
t6=C_a_i_list(&a,4,((C_word*)t0)[7],((C_word*)t0)[2],((C_word*)t0)[8],t5);
t7=C_a_i_list(&a,1,t6);
/* compiler-syntax.scm:85: ##sys#append */
t8=*((C_word*)lf[90]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t8;
av2[1]=((C_word*)t0)[9];
av2[2]=((C_word*)t0)[10];
av2[3]=t7;
((C_proc)(void*)(*((C_word*)t8+1)))(4,av2);}}

/* map-loop797 in k4742 in k4788 in k4660 in k4645 in k4918 in k4610 in k4596 in k4587 in k4584 in k4581 in k4578 in k4575 in k4572 in k4569 in k4566 in k4563 in a4560 in k1692 in k1689 in k1686 in k1683 in ... */
static void C_fcall f_4700(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(12,0,2)))){
C_save_and_reclaim_args((void *)trf_4700,3,t0,t1,t2);}
a=C_alloc(12);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_a_i_list(&a,3,lf[50],t3,C_fix(1));
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k4742 in k4788 in k4660 in k4645 in k4918 in k4610 in k4596 in k4587 in k4584 in k4581 in k4578 in k4575 in k4572 in k4569 in k4566 in k4563 in a4560 in k1692 in k1689 in k1686 in k1683 in k1680 in ... */
static void C_ccall f_4744(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(26,c,3)))){
C_save_and_reclaim((void *)f_4744,c,av);}
a=C_alloc(26);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_4698,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t2,a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],tmp=(C_word)a,a+=11,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4700,a[2]=t5,a[3]=t9,a[4]=t6,tmp=(C_word)a,a+=5,tmp));
t11=((C_word*)t9)[1];
f_4700(t11,t7,((C_word*)t0)[11]);}

/* map-loop767 in k4788 in k4660 in k4645 in k4918 in k4610 in k4596 in k4587 in k4584 in k4581 in k4578 in k4575 in k4572 in k4569 in k4566 in k4563 in a4560 in k1692 in k1689 in k1686 in k1683 in k1680 in ... */
static void C_fcall f_4746(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(12,0,2)))){
C_save_and_reclaim_args((void *)trf_4746,3,t0,t1,t2);}
a=C_alloc(12);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_a_i_list(&a,3,lf[50],t3,C_fix(0));
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* g743 in k4660 in k4645 in k4918 in k4610 in k4596 in k4587 in k4584 in k4581 in k4578 in k4575 in k4572 in k4569 in k4566 in k4563 in a4560 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 in ... */
static C_word C_fcall f_4783(C_word *a,C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_stack_overflow_check;{}
return(C_a_i_list(&a,2,((C_word*)t0)[2],t1));}

/* k4788 in k4660 in k4645 in k4918 in k4610 in k4596 in k4587 in k4584 in k4581 in k4578 in k4575 in k4572 in k4569 in k4566 in k4563 in a4560 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 in ... */
static void C_ccall f_4790(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(27,c,3)))){
C_save_and_reclaim((void *)f_4790,c,av);}
a=C_alloc(27);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_4744,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=t2,a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],tmp=(C_word)a,a+=12,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4746,a[2]=t5,a[3]=t9,a[4]=t6,tmp=(C_word)a,a+=5,tmp));
t11=((C_word*)t9)[1];
f_4746(t11,t7,((C_word*)t0)[11]);}

/* map-loop737 in k4660 in k4645 in k4918 in k4610 in k4596 in k4587 in k4584 in k4581 in k4578 in k4575 in k4572 in k4569 in k4566 in k4563 in a4560 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 in ... */
static void C_fcall f_4792(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(9,0,2)))){
C_save_and_reclaim_args((void *)trf_4792,3,t0,t1,t2);}
a=C_alloc(9);
if(C_truep(C_i_pairp(t2))){
t3=(
/* compiler-syntax.scm:91: g743 */
  f_4783(C_a_i(&a,6),((C_word*)t0)[2],C_slot(t2,C_fix(0)))
);
t4=C_a_i_cons(&a,2,t3,C_SCHEME_END_OF_LIST);
t5=C_i_setslot(((C_word*)((C_word*)t0)[3])[1],C_fix(1),t4);
t6=C_mutate(((C_word *)((C_word*)t0)[3])+1,t4);
t8=t1;
t9=C_slot(t2,C_fix(1));
t1=t8;
t2=t9;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* map-loop706 in k4645 in k4918 in k4610 in k4596 in k4587 in k4584 in k4581 in k4578 in k4575 in k4572 in k4569 in k4566 in k4563 in a4560 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_fcall f_4826(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(9,0,3)))){
C_save_and_reclaim_args((void *)trf_4826,4,t0,t1,t2,t3);}
a=C_alloc(9);
t4=C_i_pairp(t2);
t5=(C_truep(t4)?C_i_pairp(t3):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_slot(t2,C_fix(0));
t7=C_slot(t3,C_fix(0));
t8=C_a_i_list2(&a,2,t6,t7);
t9=C_a_i_cons(&a,2,t8,C_SCHEME_END_OF_LIST);
t10=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t9);
t11=C_mutate(((C_word *)((C_word*)t0)[2])+1,t9);
t13=t1;
t14=C_slot(t2,C_fix(1));
t15=C_slot(t3,C_fix(1));
t1=t13;
t2=t14;
t3=t15;
goto loop;}
else{
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* map-loop676 in k4918 in k4610 in k4596 in k4587 in k4584 in k4581 in k4578 in k4575 in k4572 in k4569 in k4566 in k4563 in a4560 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_fcall f_4874(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(24,0,2)))){
C_save_and_reclaim_args((void *)trf_4874,3,t0,t1,t2);}
a=C_alloc(24);
if(C_truep(C_i_pairp(t2))){
t3=(
/* compiler-syntax.scm:87: g682 */
  f_4632(C_a_i(&a,21),((C_word*)t0)[2],C_slot(t2,C_fix(0)))
);
t4=C_a_i_cons(&a,2,t3,C_SCHEME_END_OF_LIST);
t5=C_i_setslot(((C_word*)((C_word*)t0)[3])[1],C_fix(1),t4);
t6=C_mutate(((C_word *)((C_word*)t0)[3])+1,t4);
t8=t1;
t9=C_slot(t2,C_fix(1));
t1=t8;
t2=t9;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k4918 in k4610 in k4596 in k4587 in k4584 in k4581 in k4578 in k4575 in k4572 in k4569 in k4566 in k4563 in a4560 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_4920(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(35,c,3)))){
C_save_and_reclaim((void *)f_4920,c,av);}
a=C_alloc(35);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4627,a[2]=t2,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t4=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t5=t4;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=((C_word*)t6)[1];
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4632,a[2]=((C_word*)t0)[5],tmp=(C_word)a,a+=3,tmp);
t9=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_4647,a[2]=((C_word*)t0)[6],a[3]=((C_word*)t0)[7],a[4]=((C_word*)t0)[8],a[5]=((C_word*)t0)[9],a[6]=((C_word*)t0)[10],a[7]=((C_word*)t0)[11],a[8]=((C_word*)t0)[4],a[9]=t3,a[10]=((C_word*)t0)[12],tmp=(C_word)a,a+=11,tmp);
t10=C_SCHEME_UNDEFINED;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_set_block_item(t11,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4874,a[2]=t8,a[3]=t6,a[4]=t11,a[5]=t7,tmp=(C_word)a,a+=6,tmp));
t13=((C_word*)t11)[1];
f_4874(t13,t9,((C_word*)t0)[12]);}

/* map-loop645 in k4610 in k4596 in k4587 in k4584 in k4581 in k4578 in k4575 in k4572 in k4569 in k4566 in k4563 in a4560 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_fcall f_4922(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(9,0,3)))){
C_save_and_reclaim_args((void *)trf_4922,4,t0,t1,t2,t3);}
a=C_alloc(9);
t4=C_i_pairp(t2);
t5=(C_truep(t4)?C_i_pairp(t3):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_slot(t2,C_fix(0));
t7=C_slot(t3,C_fix(0));
t8=C_a_i_list2(&a,2,t6,t7);
t9=C_a_i_cons(&a,2,t8,C_SCHEME_END_OF_LIST);
t10=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t9);
t11=C_mutate(((C_word *)((C_word*)t0)[2])+1,t9);
t13=t1;
t14=C_slot(t2,C_fix(1));
t15=C_slot(t3,C_fix(1));
t1=t13;
t2=t14;
t3=t15;
goto loop;}
else{
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* map-loop615 in k4596 in k4587 in k4584 in k4581 in k4578 in k4575 in k4572 in k4569 in k4566 in k4563 in a4560 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_fcall f_4974(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_4974,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4999,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* compiler-syntax.scm:84: chicken.base#gensym */
t4=*((C_word*)lf[52]+1);{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k4997 in map-loop615 in k4596 in k4587 in k4584 in k4581 in k4578 in k4575 in k4572 in k4569 in k4566 in k4563 in a4560 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_4999(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4999,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_4974(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k5014 in k4587 in k4584 in k4581 in k4578 in k4575 in k4572 in k4569 in k4566 in k4563 in a4560 in k1692 in k1689 in k1686 in k1683 in k1680 in k1677 */
static void C_ccall f_5016(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5016,c,av);}
t2=((C_word*)t0)[2];
f_4598(t2,C_i_greaterp(t1,C_fix(2)));}

/* toplevel */
static C_TLS int toplevel_initialized=0;

void C_ccall C_compiler_2dsyntax_toplevel(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(toplevel_initialized) {C_kontinue(t1,C_SCHEME_UNDEFINED);}
else C_toplevel_entry(C_text("compiler-syntax"));
C_check_nursery_minimum(C_calculate_demand(3,c,2));
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void*)C_compiler_2dsyntax_toplevel,c,av);}
toplevel_initialized=1;
if(C_unlikely(!C_demand_2(1003))){
C_save(t1);
C_rereclaim2(1003*sizeof(C_word),1);
t1=C_restore;}
a=C_alloc(3);
C_initialize_lf(lf,105);
lf[0]=C_h_intern(&lf[0],15, C_text("compiler-syntax"));
lf[1]=C_h_intern(&lf[1],33, C_text("chicken.compiler.compiler-syntax#"));
lf[3]=C_h_intern(&lf[3],59, C_text("chicken.compiler.compiler-syntax#compiler-syntax-statistics"));
lf[4]=C_h_intern(&lf[4],26, C_text("##sys#compiler-syntax-hook"));
lf[5]=C_h_intern(&lf[5],26, C_text("chicken.base#alist-update!"));
lf[6]=C_h_intern(&lf[6],22, C_text("chicken.base#alist-ref"));
lf[7]=C_h_intern(&lf[7],10, C_text("scheme#eq\077"));
lf[9]=C_h_intern(&lf[9],10, C_text("##sys#put!"));
lf[10]=C_h_intern(&lf[10],26, C_text("##compiler#compiler-syntax"));
lf[11]=C_h_intern(&lf[11],13, C_text("scheme#append"));
lf[12]=C_h_intern(&lf[12],31, C_text("##sys#default-macro-environment"));
lf[13]=C_h_intern(&lf[13],24, C_text("##sys#ensure-transformer"));
lf[14]=C_h_intern(&lf[14],20, C_text("##sys#er-transformer"));
lf[16]=C_h_intern(&lf[16],39, C_text("chicken.compiler.core#extended-bindings"));
lf[17]=C_h_intern(&lf[17],6, C_text("format"));
lf[18]=C_h_intern(&lf[18],20, C_text("chicken.base#warning"));
lf[19]=C_h_intern(&lf[19],30, C_text("chicken.base#get-output-string"));
lf[20]=C_h_intern(&lf[20],22, C_text("chicken.format#fprintf"));
lf[21]=C_h_intern(&lf[21],11, C_text("##sys#print"));
lf[22]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002, "));
lf[23]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024\047, in format string "));
lf[24]=C_h_intern(&lf[24],18, C_text("##sys#write-char-0"));
lf[25]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002) "));
lf[26]=C_h_intern(&lf[26],31, C_text("chicken.base#open-output-string"));
lf[27]=C_decode_literal(C_heaptop,C_text("\376B\000\000\000"));
lf[28]=C_h_intern(&lf[28],33, C_text("chicken.compiler.support#get-line"));
lf[29]=C_decode_literal(C_heaptop,C_text("\376B\000\000/too few arguments to formatted output procedure"));
lf[30]=C_h_intern(&lf[30],26, C_text("##sys#reverse-list->string"));
lf[31]=C_h_intern(&lf[31],5, C_text("quote"));
lf[32]=C_h_intern(&lf[32],23, C_text("##sys#check-output-port"));
lf[33]=C_h_intern(&lf[33],14, C_text("scheme#reverse"));
lf[34]=C_decode_literal(C_heaptop,C_text("\376B\000\0000too many arguments to formatted output procedure"));
lf[35]=C_h_intern(&lf[35],18, C_text("##sys#flush-output"));
lf[36]=C_h_intern(&lf[36],11, C_text("##sys#apply"));
lf[37]=C_decode_literal(C_heaptop,C_text("\376B\000\000$illegal format-string character `~c\047"));
lf[38]=C_h_intern(&lf[38],14, C_text("number->string"));
lf[39]=C_h_intern(&lf[39],3, C_text("let"));
lf[40]=C_h_intern(&lf[40],3, C_text("out"));
lf[41]=C_h_intern(&lf[41],12, C_text("scheme#cadar"));
lf[42]=C_h_intern(&lf[42],26, C_text("chicken.base#symbol-append"));
lf[43]=C_h_intern(&lf[43],15, C_text("chicken.format#"));
lf[44]=C_h_intern(&lf[44],20, C_text("chicken.base#call/cc"));
lf[45]=C_h_intern(&lf[45],18, C_text("chicken.base#foldl"));
lf[46]=C_h_intern(&lf[46],12, C_text("##core#quote"));
lf[47]=C_h_intern(&lf[47],5, C_text("foldl"));
lf[48]=C_h_intern(&lf[48],16, C_text("##sys#check-list"));
lf[49]=C_h_intern(&lf[49],12, C_text("##core#check"));
lf[50]=C_h_intern(&lf[50],10, C_text("##sys#slot"));
lf[51]=C_h_intern(&lf[51],10, C_text("##core#app"));
lf[52]=C_h_intern(&lf[52],19, C_text("chicken.base#gensym"));
lf[53]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005foldl"));
lf[54]=C_h_intern(&lf[54],5, C_text("pair\077"));
lf[55]=C_h_intern(&lf[55],2, C_text("if"));
lf[56]=C_h_intern(&lf[56],4, C_text("let\052"));
lf[57]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\022\001chicken.base#foldl\376\377\016"));
lf[58]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001pair\077\376\001\000\000\014\001scheme#pair\077\376\377\016"));
lf[59]=C_h_intern(&lf[59],18, C_text("chicken.base#foldr"));
lf[60]=C_h_intern(&lf[60],5, C_text("foldr"));
lf[61]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005foldr"));
lf[62]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\022\001chicken.base#foldr\376\377\016"));
lf[63]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001pair\077\376\001\000\000\014\001scheme#pair\077\376\377\016"));
lf[64]=C_h_intern(&lf[64],6, C_text("printf"));
lf[65]=C_h_intern(&lf[65],21, C_text("##sys#standard-output"));
lf[66]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\025\001chicken.format#printf\376\377\016"));
lf[67]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\007\001display\376\001\000\000\016\001scheme#display\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001write\376\001\000\000\014\001scheme#wri"
"te\376\003\000\000\002\376\003\000\000\002\376\001\000\000\016\001number->string\376\001\000\000\025\001scheme#number->string\376\003\000\000\002\376\003\000\000\002\376\001\000\000\012\001write"
"-char\376\001\000\000\021\001scheme#write-char\376\003\000\000\002\376\003\000\000\002\376\001\000\000\022\001open-output-string\376\001\000\000\037\001chicken.base"
"#open-output-string\376\003\000\000\002\376\003\000\000\002\376\001\000\000\021\001get-output-string\376\001\000\000\036\001chicken.base#get-outpu"
"t-string\376\377\016"));
lf[68]=C_h_intern(&lf[68],7, C_text("fprintf"));
lf[69]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\026\001chicken.format#fprintf\376\377\016"));
lf[70]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\007\001display\376\001\000\000\016\001scheme#display\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001write\376\001\000\000\014\001scheme#wri"
"te\376\003\000\000\002\376\003\000\000\002\376\001\000\000\016\001number->string\376\001\000\000\025\001scheme#number->string\376\003\000\000\002\376\003\000\000\002\376\001\000\000\012\001write"
"-char\376\001\000\000\021\001scheme#write-char\376\003\000\000\002\376\003\000\000\002\376\001\000\000\022\001open-output-string\376\001\000\000\037\001chicken.base"
"#open-output-string\376\003\000\000\002\376\003\000\000\002\376\001\000\000\021\001get-output-string\376\001\000\000\036\001chicken.base#get-outpu"
"t-string\376\377\016"));
lf[71]=C_h_intern(&lf[71],17, C_text("get-output-string"));
lf[72]=C_h_intern(&lf[72],18, C_text("open-output-string"));
lf[73]=C_h_intern(&lf[73],22, C_text("chicken.format#sprintf"));
lf[74]=C_h_intern(&lf[74],7, C_text("sprintf"));
lf[75]=C_h_intern(&lf[75],7, C_text("display"));
lf[76]=C_h_intern(&lf[76],14, C_text("scheme#display"));
lf[77]=C_h_intern(&lf[77],5, C_text("write"));
lf[78]=C_h_intern(&lf[78],12, C_text("scheme#write"));
lf[79]=C_h_intern(&lf[79],21, C_text("scheme#number->string"));
lf[80]=C_h_intern(&lf[80],10, C_text("write-char"));
lf[81]=C_h_intern(&lf[81],17, C_text("scheme#write-char"));
lf[82]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\026\001chicken.format#sprintf\376\003\000\000\002\376\001\000\000\025\001chicken.format#format\376\377\016"));
lf[83]=C_h_intern(&lf[83],14, C_text("chicken.base#o"));
lf[84]=C_h_intern(&lf[84],6, C_text("lambda"));
lf[85]=C_h_intern(&lf[85],3, C_text("tmp"));
lf[86]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\016\001chicken.base#o\376\377\016"));
lf[87]=C_h_intern(&lf[87],3, C_text("map"));
lf[88]=C_h_intern(&lf[88],16, C_text("##core#undefined"));
lf[89]=C_h_intern(&lf[89],13, C_text("##sys#setslot"));
lf[90]=C_h_intern(&lf[90],12, C_text("##sys#append"));
lf[91]=C_h_intern(&lf[91],10, C_text("scheme#map"));
lf[92]=C_h_intern(&lf[92],39, C_text("chicken.compiler.core#standard-bindings"));
lf[93]=C_h_intern(&lf[93],3, C_text("and"));
lf[94]=C_h_intern(&lf[94],5, C_text("begin"));
lf[95]=C_h_intern(&lf[95],4, C_text("set!"));
lf[96]=C_h_intern(&lf[96],4, C_text("cons"));
lf[97]=C_h_intern(&lf[97],8, C_text("map-loop"));
lf[98]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\012\001scheme#map\376\003\000\000\002\376\001\000\000\011\001##sys#map\376\377\016"));
lf[99]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001pair\077\376\001\000\000\014\001scheme#pair\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\004\001cons\376\001\000\000\013\001scheme#cons\376\377\016"));
lf[100]=C_h_intern(&lf[100],8, C_text("for-each"));
lf[101]=C_h_intern(&lf[101],15, C_text("scheme#for-each"));
lf[102]=C_h_intern(&lf[102],13, C_text("for-each-loop"));
lf[103]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\017\001scheme#for-each\376\003\000\000\002\376\001\000\000\016\001##sys#for-each\376\377\016"));
lf[104]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001pair\077\376\001\000\000\014\001scheme#pair\077\376\377\016"));
C_register_lf2(lf,105,create_ptable());{}
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1679,a[2]=t1,tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_library_toplevel(2,av2);}}

#ifdef C_ENABLE_PTABLES
static C_PTABLE_ENTRY ptable[172] = {
{C_text("f_1679:compiler_2dsyntax_2escm"),(void*)f_1679},
{C_text("f_1682:compiler_2dsyntax_2escm"),(void*)f_1682},
{C_text("f_1685:compiler_2dsyntax_2escm"),(void*)f_1685},
{C_text("f_1688:compiler_2dsyntax_2escm"),(void*)f_1688},
{C_text("f_1691:compiler_2dsyntax_2escm"),(void*)f_1691},
{C_text("f_1694:compiler_2dsyntax_2escm"),(void*)f_1694},
{C_text("f_2775:compiler_2dsyntax_2escm"),(void*)f_2775},
{C_text("f_2781:compiler_2dsyntax_2escm"),(void*)f_2781},
{C_text("f_2975:compiler_2dsyntax_2escm"),(void*)f_2975},
{C_text("f_2979:compiler_2dsyntax_2escm"),(void*)f_2979},
{C_text("f_2983:compiler_2dsyntax_2escm"),(void*)f_2983},
{C_text("f_2987:compiler_2dsyntax_2escm"),(void*)f_2987},
{C_text("f_2992:compiler_2dsyntax_2escm"),(void*)f_2992},
{C_text("f_3001:compiler_2dsyntax_2escm"),(void*)f_3001},
{C_text("f_3011:compiler_2dsyntax_2escm"),(void*)f_3011},
{C_text("f_3026:compiler_2dsyntax_2escm"),(void*)f_3026},
{C_text("f_3030:compiler_2dsyntax_2escm"),(void*)f_3030},
{C_text("f_3034:compiler_2dsyntax_2escm"),(void*)f_3034},
{C_text("f_3041:compiler_2dsyntax_2escm"),(void*)f_3041},
{C_text("f_3044:compiler_2dsyntax_2escm"),(void*)f_3044},
{C_text("f_3047:compiler_2dsyntax_2escm"),(void*)f_3047},
{C_text("f_3050:compiler_2dsyntax_2escm"),(void*)f_3050},
{C_text("f_3053:compiler_2dsyntax_2escm"),(void*)f_3053},
{C_text("f_3056:compiler_2dsyntax_2escm"),(void*)f_3056},
{C_text("f_3058:compiler_2dsyntax_2escm"),(void*)f_3058},
{C_text("f_3064:compiler_2dsyntax_2escm"),(void*)f_3064},
{C_text("f_3086:compiler_2dsyntax_2escm"),(void*)f_3086},
{C_text("f_3089:compiler_2dsyntax_2escm"),(void*)f_3089},
{C_text("f_3092:compiler_2dsyntax_2escm"),(void*)f_3092},
{C_text("f_3096:compiler_2dsyntax_2escm"),(void*)f_3096},
{C_text("f_3099:compiler_2dsyntax_2escm"),(void*)f_3099},
{C_text("f_3109:compiler_2dsyntax_2escm"),(void*)f_3109},
{C_text("f_3115:compiler_2dsyntax_2escm"),(void*)f_3115},
{C_text("f_3118:compiler_2dsyntax_2escm"),(void*)f_3118},
{C_text("f_3121:compiler_2dsyntax_2escm"),(void*)f_3121},
{C_text("f_3124:compiler_2dsyntax_2escm"),(void*)f_3124},
{C_text("f_3127:compiler_2dsyntax_2escm"),(void*)f_3127},
{C_text("f_3130:compiler_2dsyntax_2escm"),(void*)f_3130},
{C_text("f_3133:compiler_2dsyntax_2escm"),(void*)f_3133},
{C_text("f_3136:compiler_2dsyntax_2escm"),(void*)f_3136},
{C_text("f_3140:compiler_2dsyntax_2escm"),(void*)f_3140},
{C_text("f_3143:compiler_2dsyntax_2escm"),(void*)f_3143},
{C_text("f_3149:compiler_2dsyntax_2escm"),(void*)f_3149},
{C_text("f_3152:compiler_2dsyntax_2escm"),(void*)f_3152},
{C_text("f_3155:compiler_2dsyntax_2escm"),(void*)f_3155},
{C_text("f_3164:compiler_2dsyntax_2escm"),(void*)f_3164},
{C_text("f_3167:compiler_2dsyntax_2escm"),(void*)f_3167},
{C_text("f_3170:compiler_2dsyntax_2escm"),(void*)f_3170},
{C_text("f_3172:compiler_2dsyntax_2escm"),(void*)f_3172},
{C_text("f_3182:compiler_2dsyntax_2escm"),(void*)f_3182},
{C_text("f_3201:compiler_2dsyntax_2escm"),(void*)f_3201},
{C_text("f_3232:compiler_2dsyntax_2escm"),(void*)f_3232},
{C_text("f_3239:compiler_2dsyntax_2escm"),(void*)f_3239},
{C_text("f_3249:compiler_2dsyntax_2escm"),(void*)f_3249},
{C_text("f_3259:compiler_2dsyntax_2escm"),(void*)f_3259},
{C_text("f_3262:compiler_2dsyntax_2escm"),(void*)f_3262},
{C_text("f_3285:compiler_2dsyntax_2escm"),(void*)f_3285},
{C_text("f_3314:compiler_2dsyntax_2escm"),(void*)f_3314},
{C_text("f_3320:compiler_2dsyntax_2escm"),(void*)f_3320},
{C_text("f_3337:compiler_2dsyntax_2escm"),(void*)f_3337},
{C_text("f_3354:compiler_2dsyntax_2escm"),(void*)f_3354},
{C_text("f_3371:compiler_2dsyntax_2escm"),(void*)f_3371},
{C_text("f_3392:compiler_2dsyntax_2escm"),(void*)f_3392},
{C_text("f_3413:compiler_2dsyntax_2escm"),(void*)f_3413},
{C_text("f_3434:compiler_2dsyntax_2escm"),(void*)f_3434},
{C_text("f_3456:compiler_2dsyntax_2escm"),(void*)f_3456},
{C_text("f_3459:compiler_2dsyntax_2escm"),(void*)f_3459},
{C_text("f_3510:compiler_2dsyntax_2escm"),(void*)f_3510},
{C_text("f_3582:compiler_2dsyntax_2escm"),(void*)f_3582},
{C_text("f_3589:compiler_2dsyntax_2escm"),(void*)f_3589},
{C_text("f_3593:compiler_2dsyntax_2escm"),(void*)f_3593},
{C_text("f_3607:compiler_2dsyntax_2escm"),(void*)f_3607},
{C_text("f_3615:compiler_2dsyntax_2escm"),(void*)f_3615},
{C_text("f_3618:compiler_2dsyntax_2escm"),(void*)f_3618},
{C_text("f_3620:compiler_2dsyntax_2escm"),(void*)f_3620},
{C_text("f_3639:compiler_2dsyntax_2escm"),(void*)f_3639},
{C_text("f_3642:compiler_2dsyntax_2escm"),(void*)f_3642},
{C_text("f_3645:compiler_2dsyntax_2escm"),(void*)f_3645},
{C_text("f_3648:compiler_2dsyntax_2escm"),(void*)f_3648},
{C_text("f_3651:compiler_2dsyntax_2escm"),(void*)f_3651},
{C_text("f_3654:compiler_2dsyntax_2escm"),(void*)f_3654},
{C_text("f_3657:compiler_2dsyntax_2escm"),(void*)f_3657},
{C_text("f_3736:compiler_2dsyntax_2escm"),(void*)f_3736},
{C_text("f_3755:compiler_2dsyntax_2escm"),(void*)f_3755},
{C_text("f_3758:compiler_2dsyntax_2escm"),(void*)f_3758},
{C_text("f_3761:compiler_2dsyntax_2escm"),(void*)f_3761},
{C_text("f_3764:compiler_2dsyntax_2escm"),(void*)f_3764},
{C_text("f_3767:compiler_2dsyntax_2escm"),(void*)f_3767},
{C_text("f_3770:compiler_2dsyntax_2escm"),(void*)f_3770},
{C_text("f_3841:compiler_2dsyntax_2escm"),(void*)f_3841},
{C_text("f_3845:compiler_2dsyntax_2escm"),(void*)f_3845},
{C_text("f_3854:compiler_2dsyntax_2escm"),(void*)f_3854},
{C_text("f_3864:compiler_2dsyntax_2escm"),(void*)f_3864},
{C_text("f_3881:compiler_2dsyntax_2escm"),(void*)f_3881},
{C_text("f_3885:compiler_2dsyntax_2escm"),(void*)f_3885},
{C_text("f_3888:compiler_2dsyntax_2escm"),(void*)f_3888},
{C_text("f_3898:compiler_2dsyntax_2escm"),(void*)f_3898},
{C_text("f_3910:compiler_2dsyntax_2escm"),(void*)f_3910},
{C_text("f_3922:compiler_2dsyntax_2escm"),(void*)f_3922},
{C_text("f_3965:compiler_2dsyntax_2escm"),(void*)f_3965},
{C_text("f_3975:compiler_2dsyntax_2escm"),(void*)f_3975},
{C_text("f_3982:compiler_2dsyntax_2escm"),(void*)f_3982},
{C_text("f_3993:compiler_2dsyntax_2escm"),(void*)f_3993},
{C_text("f_3995:compiler_2dsyntax_2escm"),(void*)f_3995},
{C_text("f_4013:compiler_2dsyntax_2escm"),(void*)f_4013},
{C_text("f_4029:compiler_2dsyntax_2escm"),(void*)f_4029},
{C_text("f_4033:compiler_2dsyntax_2escm"),(void*)f_4033},
{C_text("f_4036:compiler_2dsyntax_2escm"),(void*)f_4036},
{C_text("f_4039:compiler_2dsyntax_2escm"),(void*)f_4039},
{C_text("f_4042:compiler_2dsyntax_2escm"),(void*)f_4042},
{C_text("f_4045:compiler_2dsyntax_2escm"),(void*)f_4045},
{C_text("f_4048:compiler_2dsyntax_2escm"),(void*)f_4048},
{C_text("f_4051:compiler_2dsyntax_2escm"),(void*)f_4051},
{C_text("f_4054:compiler_2dsyntax_2escm"),(void*)f_4054},
{C_text("f_4057:compiler_2dsyntax_2escm"),(void*)f_4057},
{C_text("f_4060:compiler_2dsyntax_2escm"),(void*)f_4060},
{C_text("f_4063:compiler_2dsyntax_2escm"),(void*)f_4063},
{C_text("f_4066:compiler_2dsyntax_2escm"),(void*)f_4066},
{C_text("f_4069:compiler_2dsyntax_2escm"),(void*)f_4069},
{C_text("f_4072:compiler_2dsyntax_2escm"),(void*)f_4072},
{C_text("f_4081:compiler_2dsyntax_2escm"),(void*)f_4081},
{C_text("f_4095:compiler_2dsyntax_2escm"),(void*)f_4095},
{C_text("f_4118:compiler_2dsyntax_2escm"),(void*)f_4118},
{C_text("f_4123:compiler_2dsyntax_2escm"),(void*)f_4123},
{C_text("f_4138:compiler_2dsyntax_2escm"),(void*)f_4138},
{C_text("f_4153:compiler_2dsyntax_2escm"),(void*)f_4153},
{C_text("f_4201:compiler_2dsyntax_2escm"),(void*)f_4201},
{C_text("f_4203:compiler_2dsyntax_2escm"),(void*)f_4203},
{C_text("f_4263:compiler_2dsyntax_2escm"),(void*)f_4263},
{C_text("f_4265:compiler_2dsyntax_2escm"),(void*)f_4265},
{C_text("f_4302:compiler_2dsyntax_2escm"),(void*)f_4302},
{C_text("f_4309:compiler_2dsyntax_2escm"),(void*)f_4309},
{C_text("f_4311:compiler_2dsyntax_2escm"),(void*)f_4311},
{C_text("f_4345:compiler_2dsyntax_2escm"),(void*)f_4345},
{C_text("f_4393:compiler_2dsyntax_2escm"),(void*)f_4393},
{C_text("f_4447:compiler_2dsyntax_2escm"),(void*)f_4447},
{C_text("f_4449:compiler_2dsyntax_2escm"),(void*)f_4449},
{C_text("f_4517:compiler_2dsyntax_2escm"),(void*)f_4517},
{C_text("f_4542:compiler_2dsyntax_2escm"),(void*)f_4542},
{C_text("f_4559:compiler_2dsyntax_2escm"),(void*)f_4559},
{C_text("f_4561:compiler_2dsyntax_2escm"),(void*)f_4561},
{C_text("f_4565:compiler_2dsyntax_2escm"),(void*)f_4565},
{C_text("f_4568:compiler_2dsyntax_2escm"),(void*)f_4568},
{C_text("f_4571:compiler_2dsyntax_2escm"),(void*)f_4571},
{C_text("f_4574:compiler_2dsyntax_2escm"),(void*)f_4574},
{C_text("f_4577:compiler_2dsyntax_2escm"),(void*)f_4577},
{C_text("f_4580:compiler_2dsyntax_2escm"),(void*)f_4580},
{C_text("f_4583:compiler_2dsyntax_2escm"),(void*)f_4583},
{C_text("f_4586:compiler_2dsyntax_2escm"),(void*)f_4586},
{C_text("f_4589:compiler_2dsyntax_2escm"),(void*)f_4589},
{C_text("f_4598:compiler_2dsyntax_2escm"),(void*)f_4598},
{C_text("f_4612:compiler_2dsyntax_2escm"),(void*)f_4612},
{C_text("f_4627:compiler_2dsyntax_2escm"),(void*)f_4627},
{C_text("f_4632:compiler_2dsyntax_2escm"),(void*)f_4632},
{C_text("f_4647:compiler_2dsyntax_2escm"),(void*)f_4647},
{C_text("f_4662:compiler_2dsyntax_2escm"),(void*)f_4662},
{C_text("f_4698:compiler_2dsyntax_2escm"),(void*)f_4698},
{C_text("f_4700:compiler_2dsyntax_2escm"),(void*)f_4700},
{C_text("f_4744:compiler_2dsyntax_2escm"),(void*)f_4744},
{C_text("f_4746:compiler_2dsyntax_2escm"),(void*)f_4746},
{C_text("f_4783:compiler_2dsyntax_2escm"),(void*)f_4783},
{C_text("f_4790:compiler_2dsyntax_2escm"),(void*)f_4790},
{C_text("f_4792:compiler_2dsyntax_2escm"),(void*)f_4792},
{C_text("f_4826:compiler_2dsyntax_2escm"),(void*)f_4826},
{C_text("f_4874:compiler_2dsyntax_2escm"),(void*)f_4874},
{C_text("f_4920:compiler_2dsyntax_2escm"),(void*)f_4920},
{C_text("f_4922:compiler_2dsyntax_2escm"),(void*)f_4922},
{C_text("f_4974:compiler_2dsyntax_2escm"),(void*)f_4974},
{C_text("f_4999:compiler_2dsyntax_2escm"),(void*)f_4999},
{C_text("f_5016:compiler_2dsyntax_2escm"),(void*)f_5016},
{C_text("toplevel:compiler_2dsyntax_2escm"),(void*)C_compiler_2dsyntax_toplevel},
{NULL,NULL}};
#endif

static C_PTABLE_ENTRY *create_ptable(void){
#ifdef C_ENABLE_PTABLES
return ptable;
#else
return NULL;
#endif
}

/*
o|hiding unexported module binding: chicken.compiler.compiler-syntax#partition 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#span 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#take 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#drop 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#split-at 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#append-map 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#every 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#any 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#cons* 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#concatenate 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#delete 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#first 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#second 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#third 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#fourth 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#fifth 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#delete-duplicates 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#alist-cons 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#filter 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#filter-map 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#remove 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#unzip1 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#last 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#list-index 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#lset-adjoin/eq? 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#lset-difference/eq? 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#lset-union/eq? 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#lset-intersection/eq? 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#list-tabulate 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#lset<=/eq? 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#lset=/eq? 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#length+ 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#find 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#find-tail 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#iota 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#make-list 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#posq 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#posv 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#r-c-s 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#define-internal-compiler-syntax 
o|hiding unexported module binding: chicken.compiler.compiler-syntax#compile-format-string 
S|applied compiler syntax:
S|  chicken.format#sprintf		2
S|  scheme#for-each		1
S|  chicken.base#foldl		3
S|  scheme#map		17
S|  chicken.base#foldr		4
o|eliminated procedure checks: 220 
o|specializations:
o|  1 (chicken.base#sub1 *)
o|  11 (scheme#eqv? (or eof null fixnum char boolean symbol keyword) *)
o|  1 (scheme#= fixnum fixnum)
o|  2 (##sys#check-output-port * * *)
o|  1 (scheme#cddr (pair * pair))
o|  2 (scheme#>= fixnum fixnum)
o|  1 (chicken.base#add1 *)
o|  1 (scheme#eqv? * *)
o|  19 (##sys#check-list (or pair list) *)
o|  24 (scheme#cdr pair)
o|  10 (scheme#car pair)
(o e)|safe calls: 535 
(o e)|assignments to immediate values: 2 
o|safe globals: (chicken.compiler.compiler-syntax#r-c-s ##sys#compiler-syntax-hook chicken.compiler.compiler-syntax#compiler-syntax-statistics chicken.compiler.compiler-syntax#posv chicken.compiler.compiler-syntax#posq chicken.compiler.compiler-syntax#make-list chicken.compiler.compiler-syntax#iota chicken.compiler.compiler-syntax#find-tail chicken.compiler.compiler-syntax#find chicken.compiler.compiler-syntax#length+ chicken.compiler.compiler-syntax#lset=/eq? chicken.compiler.compiler-syntax#lset<=/eq? chicken.compiler.compiler-syntax#list-tabulate chicken.compiler.compiler-syntax#lset-intersection/eq? chicken.compiler.compiler-syntax#lset-union/eq? chicken.compiler.compiler-syntax#lset-difference/eq? chicken.compiler.compiler-syntax#lset-adjoin/eq? chicken.compiler.compiler-syntax#list-index chicken.compiler.compiler-syntax#last chicken.compiler.compiler-syntax#unzip1 chicken.compiler.compiler-syntax#remove chicken.compiler.compiler-syntax#filter-map chicken.compiler.compiler-syntax#filter chicken.compiler.compiler-syntax#alist-cons chicken.compiler.compiler-syntax#delete-duplicates chicken.compiler.compiler-syntax#fifth chicken.compiler.compiler-syntax#fourth chicken.compiler.compiler-syntax#third chicken.compiler.compiler-syntax#second chicken.compiler.compiler-syntax#first chicken.compiler.compiler-syntax#delete chicken.compiler.compiler-syntax#concatenate chicken.compiler.compiler-syntax#cons* chicken.compiler.compiler-syntax#any chicken.compiler.compiler-syntax#every chicken.compiler.compiler-syntax#append-map chicken.compiler.compiler-syntax#split-at chicken.compiler.compiler-syntax#drop chicken.compiler.compiler-syntax#take chicken.compiler.compiler-syntax#span chicken.compiler.compiler-syntax#partition) 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#partition 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#span 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#drop 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#split-at 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#append-map 
o|inlining procedure: k2076 
o|inlining procedure: k2076 
o|inlining procedure: k2107 
o|inlining procedure: k2107 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#cons* 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#concatenate 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#first 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#second 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#third 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#fourth 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#fifth 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#delete-duplicates 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#alist-cons 
o|inlining procedure: k2324 
o|inlining procedure: k2324 
o|inlining procedure: k2316 
o|inlining procedure: k2316 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#filter-map 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#remove 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#unzip1 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#last 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#list-index 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#lset-adjoin/eq? 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#lset-difference/eq? 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#lset-union/eq? 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#lset-intersection/eq? 
o|inlining procedure: k2715 
o|inlining procedure: k2715 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#lset<=/eq? 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#lset=/eq? 
o|inlining procedure: k2783 
o|inlining procedure: k2806 
o|inlining procedure: k2806 
o|inlining procedure: k2783 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#find 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#find-tail 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#iota 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#make-list 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#posq 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#posv 
o|inlining procedure: k3003 
o|inlining procedure: k3003 
o|inlining procedure: k3066 
o|merged explicitly consed rest parameter: args1137 
o|inlining procedure: k3100 
o|inlining procedure: k3100 
o|substituted constant variable: a3111 
o|substituted constant variable: a3112 
o|substituted constant variable: a3145 
o|substituted constant variable: a3146 
o|inlining procedure: k3138 
o|inlining procedure: k3138 
o|inlining procedure: k3184 
o|consed rest parameter at call site: "(compiler-syntax.scm:219) fail1134" 3 
o|inlining procedure: k3184 
o|inlining procedure: k3203 
o|inlining procedure: k3213 
o|inlining procedure: k3213 
o|substituted constant variable: a3233 
o|inlining procedure: k3203 
o|inlining procedure: k3078 
o|inlining procedure: k3251 
o|consed rest parameter at call site: "(compiler-syntax.scm:234) fail1134" 3 
o|inlining procedure: k3251 
o|inlining procedure: k3318 
o|inlining procedure: k3318 
o|inlining procedure: k3355 
o|inlining procedure: k3355 
o|inlining procedure: k3393 
o|inlining procedure: k3393 
o|inlining procedure: k3435 
o|inlining procedure: k3435 
o|inlining procedure: k3467 
o|inlining procedure: k3467 
o|inlining procedure: k3496 
o|inlining procedure: k3512 
o|inlining procedure: k3512 
o|inlining procedure: k3496 
o|consed rest parameter at call site: "(compiler-syntax.scm:273) fail1134" 3 
o|substituted constant variable: a3534 
o|substituted constant variable: a3536 
o|substituted constant variable: a3538 
o|substituted constant variable: a3540 
o|substituted constant variable: a3542 
o|substituted constant variable: a3544 
o|substituted constant variable: a3546 
o|substituted constant variable: a3548 
o|substituted constant variable: a3550 
o|substituted constant variable: a3552 
o|substituted constant variable: a3554 
o|inlining procedure: k3078 
o|inlining procedure: k3571 
o|inlining procedure: k3571 
o|inlining procedure: k3066 
o|substituted constant variable: a3612 
o|inlining procedure: k3622 
o|inlining procedure: k3622 
o|inlining procedure: k3738 
o|inlining procedure: k3738 
o|inlining procedure: k3846 
o|inlining procedure: k3846 
o|inlining procedure: k3856 
o|inlining procedure: k3856 
o|substituted constant variable: a3879 
o|inlining procedure: k3889 
o|inlining procedure: k3889 
o|inlining procedure: k3967 
o|inlining procedure: k3997 
o|inlining procedure: k3997 
o|inlining procedure: k3967 
o|inlining procedure: k4076 
o|inlining procedure: k4205 
o|contracted procedure: "(compiler-syntax.scm:138) g10381047" 
o|inlining procedure: k4205 
o|inlining procedure: k4267 
o|contracted procedure: "(compiler-syntax.scm:132) g10081017" 
o|inlining procedure: k4267 
o|inlining procedure: k4313 
o|inlining procedure: k4313 
o|inlining procedure: k4347 
o|inlining procedure: k4347 
o|inlining procedure: k4395 
o|inlining procedure: k4395 
o|inlining procedure: k4451 
o|inlining procedure: k4451 
o|inlining procedure: k4519 
o|contracted procedure: "(compiler-syntax.scm:119) g856865" 
o|inlining procedure: k4519 
o|inlining procedure: k4076 
o|inlining procedure: k4593 
o|inlining procedure: k4702 
o|contracted procedure: "(compiler-syntax.scm:97) g803812" 
o|inlining procedure: k4702 
o|inlining procedure: k4748 
o|contracted procedure: "(compiler-syntax.scm:94) g773782" 
o|inlining procedure: k4748 
o|inlining procedure: k4794 
o|inlining procedure: k4794 
o|inlining procedure: k4828 
o|inlining procedure: k4828 
o|inlining procedure: k4876 
o|inlining procedure: k4876 
o|inlining procedure: k4924 
o|inlining procedure: k4924 
o|inlining procedure: k4976 
o|contracted procedure: "(compiler-syntax.scm:84) g621630" 
o|inlining procedure: k4976 
o|inlining procedure: k4593 
o|replaced variables: 622 
o|removed binding forms: 176 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#every 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#any 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#filter 
o|removed side-effect free assignment to unused variable: chicken.compiler.compiler-syntax#list-tabulate 
o|substituted constant variable: r28075029 
o|substituted constant variable: r31395038 
o|substituted constant variable: r31395038 
o|converted assignments to bindings: (fail1134) 
o|substituted constant variable: r30795073 
o|substituted constant variable: r35725075 
o|substituted constant variable: r30675076 
o|simplifications: ((let . 1)) 
o|replaced variables: 23 
o|removed binding forms: 538 
o|removed call to pure procedure with unused result: "(compiler-syntax.scm:119) ##sys#slot" 
o|removed call to pure procedure with unused result: "(compiler-syntax.scm:84) ##sys#slot" 
o|removed binding forms: 33 
o|contracted procedure: k4544 
o|contracted procedure: k5001 
o|removed binding forms: 2 
o|removed binding forms: 2 
o|simplifications: ((let . 4) (if . 11) (##core#call . 320)) 
o|  call simplifications:
o|    scheme#cddr	2
o|    scheme#>	2
o|    ##sys#check-list	4
o|    ##sys#setslot	14
o|    chicken.fixnum#fx>
o|    scheme#list	5
o|    chicken.fixnum#fx=	2
o|    scheme#cadr	5
o|    scheme#caddr	2
o|    scheme#cadddr	2
o|    chicken.fixnum#fx>=	2
o|    scheme#memq	6
o|    scheme#list?
o|    scheme#caar
o|    scheme#string?	3
o|    scheme#string-length
o|    scheme#>=
o|    scheme#char-upcase
o|    scheme#char-whitespace?	2
o|    ##sys#cons	24
o|    scheme#length	6
o|    ##sys#list	95
o|    scheme#null?	2
o|    scheme#string-ref
o|    ##sys#apply
o|    scheme#car	5
o|    scheme#cons	31
o|    ##sys#slot	52
o|    scheme#pair?	23
o|    scheme#cdr	4
o|    chicken.fixnum#fx+	3
o|    scheme#eq?	15
o|    scheme#not
o|contracted procedure: k2786 
o|contracted procedure: k2790 
o|contracted procedure: k2796 
o|contracted procedure: k2800 
o|contracted procedure: k2803 
o|contracted procedure: k2816 
o|contracted procedure: k2809 
o|contracted procedure: k2989 
o|contracted procedure: k3006 
o|contracted procedure: k3016 
o|contracted procedure: k3020 
o|contracted procedure: k3036 
o|contracted procedure: k3609 
o|contracted procedure: k3069 
o|contracted procedure: k3075 
o|contracted procedure: k3601 
o|contracted procedure: k3081 
o|contracted procedure: k3159 
o|contracted procedure: k3174 
o|contracted procedure: k3178 
o|contracted procedure: k3187 
o|contracted procedure: k3193 
o|contracted procedure: k3197 
o|contracted procedure: k3206 
o|contracted procedure: k3235 
o|contracted procedure: k3216 
o|contracted procedure: k3223 
o|inlining procedure: k3213 
o|inlining procedure: k3213 
o|contracted procedure: k3242 
o|contracted procedure: k3254 
o|contracted procedure: k3291 
o|contracted procedure: k3271 
o|contracted procedure: k3287 
o|contracted procedure: k3279 
o|contracted procedure: k3275 
o|contracted procedure: k3267 
o|contracted procedure: k3294 
o|contracted procedure: k3306 
o|contracted procedure: k3315 
o|contracted procedure: k3324 
o|contracted procedure: k3331 
o|contracted procedure: k3341 
o|contracted procedure: k3348 
o|contracted procedure: k3358 
o|contracted procedure: k3365 
o|contracted procedure: k3375 
o|contracted procedure: k3386 
o|contracted procedure: k3382 
o|contracted procedure: k3396 
o|contracted procedure: k3407 
o|contracted procedure: k3403 
o|contracted procedure: k3417 
o|contracted procedure: k3428 
o|contracted procedure: k3424 
o|contracted procedure: k3438 
o|contracted procedure: k3445 
o|contracted procedure: k3451 
o|contracted procedure: k3464 
o|contracted procedure: k3470 
o|contracted procedure: k3477 
o|contracted procedure: k3483 
o|contracted procedure: k3486 
o|contracted procedure: k3493 
o|contracted procedure: k3499 
o|contracted procedure: k3515 
o|contracted procedure: k3559 
o|contracted procedure: k3562 
o|contracted procedure: k3574 
o|contracted procedure: k3595 
o|contracted procedure: k3732 
o|contracted procedure: k3725 
o|contracted procedure: k3625 
o|contracted procedure: k3628 
o|contracted procedure: k3631 
o|contracted procedure: k3634 
o|contracted procedure: k3718 
o|contracted procedure: k3722 
o|contracted procedure: k3662 
o|contracted procedure: k3714 
o|contracted procedure: k3710 
o|contracted procedure: k3666 
o|contracted procedure: k3702 
o|contracted procedure: k3706 
o|contracted procedure: k3674 
o|contracted procedure: k3682 
o|contracted procedure: k3690 
o|contracted procedure: k3698 
o|contracted procedure: k3694 
o|contracted procedure: k3686 
o|contracted procedure: k3678 
o|contracted procedure: k3670 
o|contracted procedure: k3837 
o|contracted procedure: k3830 
o|contracted procedure: k3741 
o|contracted procedure: k3744 
o|contracted procedure: k3747 
o|contracted procedure: k3750 
o|contracted procedure: k3827 
o|contracted procedure: k3775 
o|contracted procedure: k3823 
o|contracted procedure: k3819 
o|contracted procedure: k3779 
o|contracted procedure: k3815 
o|contracted procedure: k3787 
o|contracted procedure: k3795 
o|contracted procedure: k3803 
o|contracted procedure: k3811 
o|contracted procedure: k3807 
o|contracted procedure: k3799 
o|contracted procedure: k3791 
o|contracted procedure: k3783 
o|contracted procedure: k3850 
o|contracted procedure: k3876 
o|contracted procedure: k3859 
o|contracted procedure: k3869 
o|contracted procedure: k3916 
o|contracted procedure: k3912 
o|contracted procedure: k3900 
o|contracted procedure: k3904 
o|contracted procedure: k3933 
o|contracted procedure: k3929 
o|contracted procedure: k3924 
o|contracted procedure: k3941 
o|contracted procedure: k3945 
o|contracted procedure: k3949 
o|contracted procedure: k3953 
o|contracted procedure: k3957 
o|contracted procedure: k3961 
o|contracted procedure: k3937 
o|contracted procedure: k4025 
o|contracted procedure: k4018 
o|contracted procedure: k3970 
o|contracted procedure: k3984 
o|contracted procedure: k3988 
o|contracted procedure: k4000 
o|contracted procedure: k4007 
o|contracted procedure: k4015 
o|contracted procedure: k4073 
o|contracted procedure: k4082 
o|contracted procedure: k4090 
o|contracted procedure: k4509 
o|contracted procedure: k4513 
o|contracted procedure: k4505 
o|contracted procedure: k4501 
o|contracted procedure: k4100 
o|contracted procedure: k4427 
o|contracted procedure: k4497 
o|contracted procedure: k4435 
o|contracted procedure: k4439 
o|contracted procedure: k4442 
o|contracted procedure: k4431 
o|contracted procedure: k4112 
o|contracted procedure: k4108 
o|contracted procedure: k4104 
o|contracted procedure: k4120 
o|contracted procedure: k4133 
o|contracted procedure: k4129 
o|contracted procedure: k4148 
o|contracted procedure: k4299 
o|contracted procedure: k4159 
o|contracted procedure: k4253 
o|contracted procedure: k4245 
o|contracted procedure: k4249 
o|contracted procedure: k4241 
o|contracted procedure: k4237 
o|contracted procedure: k4171 
o|contracted procedure: k4175 
o|contracted procedure: k4179 
o|contracted procedure: k4191 
o|contracted procedure: k4187 
o|contracted procedure: k4183 
o|contracted procedure: k4163 
o|contracted procedure: k4167 
o|contracted procedure: k4155 
o|contracted procedure: k4144 
o|contracted procedure: k4140 
o|contracted procedure: k4208 
o|contracted procedure: k4230 
o|contracted procedure: k4226 
o|contracted procedure: k4211 
o|contracted procedure: k4214 
o|contracted procedure: k4222 
o|contracted procedure: k4270 
o|contracted procedure: k4292 
o|contracted procedure: k4288 
o|contracted procedure: k4273 
o|contracted procedure: k4276 
o|contracted procedure: k4284 
o|contracted procedure: k4316 
o|contracted procedure: k4319 
o|contracted procedure: k4322 
o|contracted procedure: k4330 
o|contracted procedure: k4338 
o|contracted procedure: k4386 
o|contracted procedure: k4350 
o|contracted procedure: k4376 
o|contracted procedure: k4380 
o|contracted procedure: k4372 
o|contracted procedure: k4353 
o|contracted procedure: k4356 
o|contracted procedure: k4364 
o|contracted procedure: k4368 
o|contracted procedure: k4398 
o|contracted procedure: k4401 
o|contracted procedure: k4404 
o|contracted procedure: k4412 
o|contracted procedure: k4420 
o|contracted procedure: k4490 
o|contracted procedure: k4454 
o|contracted procedure: k4480 
o|contracted procedure: k4484 
o|contracted procedure: k4476 
o|contracted procedure: k4457 
o|contracted procedure: k4460 
o|contracted procedure: k4468 
o|contracted procedure: k4472 
o|contracted procedure: k4522 
o|contracted procedure: k4525 
o|contracted procedure: k4528 
o|contracted procedure: k4536 
o|contracted procedure: k4550 
o|contracted procedure: k4590 
o|contracted procedure: k4599 
o|contracted procedure: k4607 
o|contracted procedure: k4970 
o|contracted procedure: k4908 
o|contracted procedure: k4912 
o|contracted procedure: k4915 
o|contracted procedure: k4621 
o|contracted procedure: k4617 
o|contracted procedure: k4629 
o|contracted procedure: k4642 
o|contracted procedure: k4638 
o|contracted procedure: k4657 
o|contracted procedure: k4780 
o|contracted procedure: k4668 
o|contracted procedure: k4734 
o|contracted procedure: k4676 
o|contracted procedure: k4688 
o|contracted procedure: k4684 
o|contracted procedure: k4680 
o|contracted procedure: k4672 
o|contracted procedure: k4664 
o|contracted procedure: k4653 
o|contracted procedure: k4649 
o|contracted procedure: k4705 
o|contracted procedure: k4727 
o|contracted procedure: k4723 
o|contracted procedure: k4708 
o|contracted procedure: k4711 
o|contracted procedure: k4719 
o|contracted procedure: k4751 
o|contracted procedure: k4773 
o|contracted procedure: k4769 
o|contracted procedure: k4754 
o|contracted procedure: k4757 
o|contracted procedure: k4765 
o|contracted procedure: k4797 
o|contracted procedure: k4800 
o|contracted procedure: k4803 
o|contracted procedure: k4811 
o|contracted procedure: k4819 
o|contracted procedure: k4867 
o|contracted procedure: k4831 
o|contracted procedure: k4857 
o|contracted procedure: k4861 
o|contracted procedure: k4853 
o|contracted procedure: k4834 
o|contracted procedure: k4837 
o|contracted procedure: k4845 
o|contracted procedure: k4849 
o|contracted procedure: k4879 
o|contracted procedure: k4882 
o|contracted procedure: k4885 
o|contracted procedure: k4893 
o|contracted procedure: k4901 
o|contracted procedure: k4963 
o|contracted procedure: k4927 
o|contracted procedure: k4953 
o|contracted procedure: k4957 
o|contracted procedure: k4949 
o|contracted procedure: k4930 
o|contracted procedure: k4933 
o|contracted procedure: k4941 
o|contracted procedure: k4945 
o|contracted procedure: k4979 
o|contracted procedure: k4982 
o|contracted procedure: k4985 
o|contracted procedure: k4993 
o|contracted procedure: k5007 
o|simplifications: ((let . 64)) 
o|removed binding forms: 289 
o|removed binding forms: 1 
o|direct leaf routine/allocation: lp427 0 
o|direct leaf routine/allocation: fetch1174 0 
o|direct leaf routine/allocation: push1177 3 
o|direct leaf routine/allocation: g917926 21 
o|direct leaf routine/allocation: g978987 6 
o|direct leaf routine/allocation: g682691 21 
o|direct leaf routine/allocation: g743752 6 
o|converted assignments to bindings: (lp427) 
o|contracted procedure: "(compiler-syntax.scm:240) k3300" 
o|contracted procedure: "(compiler-syntax.scm:242) k3309" 
o|inlining procedure: "(compiler-syntax.scm:245) k3318" 
o|inlining procedure: "(compiler-syntax.scm:246) k3318" 
o|inlining procedure: "(compiler-syntax.scm:247) k3318" 
o|inlining procedure: "(compiler-syntax.scm:249) k3318" 
o|inlining procedure: "(compiler-syntax.scm:253) k3318" 
o|inlining procedure: "(compiler-syntax.scm:257) k3318" 
o|inlining procedure: "(compiler-syntax.scm:260) k3318" 
o|inlining procedure: "(compiler-syntax.scm:264) k3318" 
o|inlining procedure: "(compiler-syntax.scm:265) k3318" 
o|contracted procedure: "(compiler-syntax.scm:269) k3506" 
o|contracted procedure: "(compiler-syntax.scm:271) k3522" 
o|contracted procedure: "(compiler-syntax.scm:128) k4334" 
o|contracted procedure: "(compiler-syntax.scm:124) k4416" 
o|contracted procedure: "(compiler-syntax.scm:91) k4815" 
o|contracted procedure: "(compiler-syntax.scm:87) k4897" 
o|simplifications: ((let . 1)) 
o|removed binding forms: 8 
o|customizable procedures: (k4596 map-loop615633 map-loop645664 map-loop676697 map-loop706725 map-loop737758 map-loop767788 map-loop797818 chicken.compiler.compiler-syntax#length+ k4079 map-loop850868 map-loop880899 map-loop911932 map-loop941960 map-loop972993 map-loop10021023 map-loop10321053 foldr10751078 chicken.compiler.compiler-syntax#compile-format-string chicken.compiler.compiler-syntax#r-c-s k3084 skip1244 next1175 loop1190 endchunk1176 fail1134 g545552 for-each-loop544555) 
o|calls to known targets: 84 
o|identified direct recursive calls: f_2781 1 
o|identified direct recursive calls: f_3510 1 
o|identified direct recursive calls: f_3249 1 
o|identified direct recursive calls: f_3995 1 
o|identified direct recursive calls: f_4203 1 
o|identified direct recursive calls: f_4265 1 
o|identified direct recursive calls: f_4311 1 
o|identified direct recursive calls: f_4345 1 
o|identified direct recursive calls: f_4393 1 
o|identified direct recursive calls: f_4449 1 
o|identified direct recursive calls: f_4700 1 
o|identified direct recursive calls: f_4746 1 
o|identified direct recursive calls: f_4792 1 
o|identified direct recursive calls: f_4826 1 
o|identified direct recursive calls: f_4874 1 
o|identified direct recursive calls: f_4922 1 
o|fast box initializations: 22 
o|fast global references: 13 
o|fast global assignments: 3 
o|dropping unused closure argument: f_2775 
o|dropping unused closure argument: f_2781 
o|dropping unused closure argument: f_2987 
o|dropping unused closure argument: f_3058 
*/
/* end of file */
