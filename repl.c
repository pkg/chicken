/* Generated from repl.scm by the CHICKEN compiler
   http://www.call-cc.org
   Version 5.2.0 (rev 317468e4)
   linux-unix-gnu-x86-64 [ 64bit dload ptables ]
   command line: repl.scm -optimize-level 2 -include-path . -include-path ./ -inline -ignore-repository -feature chicken-bootstrap -no-warnings -specialize -consult-types-file ./types.db -explicit-use -no-trace -output-file repl.c -emit-import-library chicken.repl
   unit: repl
   uses: eval library
*/
#include "chicken.h"

static C_word code_258() { C_clear_trace_buffer();
; return C_SCHEME_UNDEFINED; }


static C_PTABLE_ENTRY *create_ptable(void);
C_noret_decl(C_eval_toplevel)
C_externimport void C_ccall C_eval_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_library_toplevel)
C_externimport void C_ccall C_library_toplevel(C_word c,C_word *av) C_noret;

static C_TLS C_word lf[58];
static double C_possibly_force_alignment;
static C_char C_TLS li0[] C_aligned={C_lihdr(0,0,6),40,97,52,54,57,41,0,0};
static C_char C_TLS li1[] C_aligned={C_lihdr(0,0,30),40,35,35,115,121,115,35,114,101,112,108,45,112,114,105,110,116,45,104,111,111,107,32,120,32,112,111,114,116,41,0,0};
static C_char C_TLS li2[] C_aligned={C_lihdr(0,0,31),40,99,104,105,99,107,101,110,46,114,101,112,108,35,113,117,105,116,45,104,111,111,107,32,114,101,115,117,108,116,41,0};
static C_char C_TLS li3[] C_aligned={C_lihdr(0,0,26),40,99,104,105,99,107,101,110,46,114,101,112,108,35,113,117,105,116,32,46,32,114,101,115,116,41,0,0,0,0,0,0};
static C_char C_TLS li4[] C_aligned={C_lihdr(0,0,20),40,99,104,105,99,107,101,110,46,114,101,112,108,35,114,101,115,101,116,41,0,0,0,0};
static C_char C_TLS li5[] C_aligned={C_lihdr(0,0,24),40,35,35,115,121,115,35,114,101,97,100,45,112,114,111,109,112,116,45,104,111,111,107,41};
static C_char C_TLS li6[] C_aligned={C_lihdr(0,0,29),40,35,35,115,121,115,35,114,101,115,105,122,101,45,116,114,97,99,101,45,98,117,102,102,101,114,32,105,41,0,0,0};
static C_char C_TLS li7[] C_aligned={C_lihdr(0,0,23),40,102,111,114,45,101,97,99,104,45,108,111,111,112,49,53,51,32,103,49,54,48,41,0};
static C_char C_TLS li8[] C_aligned={C_lihdr(0,0,14),40,119,114,105,116,101,45,101,114,114,32,120,115,41,0,0};
static C_char C_TLS li9[] C_aligned={C_lihdr(0,0,31),40,99,104,105,99,107,101,110,46,114,101,112,108,35,113,117,105,116,45,104,111,111,107,32,114,101,115,117,108,116,41,0};
static C_char C_TLS li10[] C_aligned={C_lihdr(0,0,17),40,97,55,48,49,32,109,115,103,32,46,32,97,114,103,115,41,0,0,0,0,0,0,0};
static C_char C_TLS li11[] C_aligned={C_lihdr(0,0,6),40,97,54,56,50,41,0,0};
static C_char C_TLS li12[] C_aligned={C_lihdr(0,0,6),40,97,56,49,55,41,0,0};
static C_char C_TLS li13[] C_aligned={C_lihdr(0,0,23),40,102,111,114,45,101,97,99,104,45,108,111,111,112,49,56,51,32,103,49,57,48,41,0};
static C_char C_TLS li14[] C_aligned={C_lihdr(0,0,23),40,102,111,114,45,101,97,99,104,45,108,111,111,112,50,55,49,32,103,50,55,56,41,0};
static C_char C_TLS li15[] C_aligned={C_lihdr(0,0,13),40,108,111,111,112,32,118,97,114,115,32,117,41,0,0,0};
static C_char C_TLS li16[] C_aligned={C_lihdr(0,0,15),40,97,56,50,51,32,46,32,114,101,115,117,108,116,41,0};
static C_char C_TLS li17[] C_aligned={C_lihdr(0,0,6),40,97,57,55,56,41,0,0};
static C_char C_TLS li18[] C_aligned={C_lihdr(0,0,8),40,97,57,55,50,32,99,41};
static C_char C_TLS li19[] C_aligned={C_lihdr(0,0,6),40,108,111,111,112,41,0,0};
static C_char C_TLS li20[] C_aligned={C_lihdr(0,0,6),40,97,55,56,48,41,0,0};
static C_char C_TLS li21[] C_aligned={C_lihdr(0,0,6),40,97,57,56,56,41,0,0};
static C_char C_TLS li22[] C_aligned={C_lihdr(0,0,8),40,97,54,55,54,32,107,41};
static C_char C_TLS li23[] C_aligned={C_lihdr(0,0,26),40,99,104,105,99,107,101,110,46,114,101,112,108,35,114,101,112,108,32,46,32,114,101,115,116,41,0,0,0,0,0,0};
static C_char C_TLS li24[] C_aligned={C_lihdr(0,0,7),40,97,49,48,48,56,41,0};
static C_char C_TLS li25[] C_aligned={C_lihdr(0,0,10),40,116,111,112,108,101,118,101,108,41,0,0,0,0,0,0};


C_noret_decl(f_1009)
static void C_ccall f_1009(C_word c,C_word *av) C_noret;
C_noret_decl(f_453)
static void C_ccall f_453(C_word c,C_word *av) C_noret;
C_noret_decl(f_456)
static void C_ccall f_456(C_word c,C_word *av) C_noret;
C_noret_decl(f_461)
static void C_ccall f_461(C_word c,C_word *av) C_noret;
C_noret_decl(f_465)
static void C_ccall f_465(C_word c,C_word *av) C_noret;
C_noret_decl(f_470)
static void C_ccall f_470(C_word c,C_word *av) C_noret;
C_noret_decl(f_476)
static void C_ccall f_476(C_word c,C_word *av) C_noret;
C_noret_decl(f_482)
static void C_ccall f_482(C_word c,C_word *av) C_noret;
C_noret_decl(f_498)
static void C_ccall f_498(C_word c,C_word *av) C_noret;
C_noret_decl(f_502)
static void C_ccall f_502(C_word c,C_word *av) C_noret;
C_noret_decl(f_509)
static void C_ccall f_509(C_word c,C_word *av) C_noret;
C_noret_decl(f_511)
static void C_ccall f_511(C_word c,C_word *av) C_noret;
C_noret_decl(f_515)
static void C_ccall f_515(C_word c,C_word *av) C_noret;
C_noret_decl(f_522)
static void C_ccall f_522(C_word c,C_word *av) C_noret;
C_noret_decl(f_525)
static void C_ccall f_525(C_word c,C_word *av) C_noret;
C_noret_decl(f_527)
static void C_ccall f_527(C_word c,C_word *av) C_noret;
C_noret_decl(f_531)
static void C_ccall f_531(C_word c,C_word *av) C_noret;
C_noret_decl(f_533)
static void C_ccall f_533(C_word c,C_word *av) C_noret;
C_noret_decl(f_539)
static void C_fcall f_539(C_word t0,C_word t1) C_noret;
C_noret_decl(f_553)
static void C_fcall f_553(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_563)
static void C_ccall f_563(C_word c,C_word *av) C_noret;
C_noret_decl(f_600)
static void C_ccall f_600(C_word c,C_word *av) C_noret;
C_noret_decl(f_613)
static void C_ccall f_613(C_word c,C_word *av) C_noret;
C_noret_decl(f_617)
static void C_ccall f_617(C_word c,C_word *av) C_noret;
C_noret_decl(f_625)
static void C_fcall f_625(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_635)
static void C_ccall f_635(C_word c,C_word *av) C_noret;
C_noret_decl(f_657)
static void C_ccall f_657(C_word c,C_word *av) C_noret;
C_noret_decl(f_660)
static void C_ccall f_660(C_word c,C_word *av) C_noret;
C_noret_decl(f_662)
static C_word C_fcall f_662(C_word t0);
C_noret_decl(f_668)
static C_word C_fcall f_668(C_word t0);
C_noret_decl(f_677)
static void C_ccall f_677(C_word c,C_word *av) C_noret;
C_noret_decl(f_683)
static void C_ccall f_683(C_word c,C_word *av) C_noret;
C_noret_decl(f_688)
static void C_ccall f_688(C_word c,C_word *av) C_noret;
C_noret_decl(f_690)
static void C_ccall f_690(C_word c,C_word *av) C_noret;
C_noret_decl(f_697)
static void C_ccall f_697(C_word c,C_word *av) C_noret;
C_noret_decl(f_702)
static void C_ccall f_702(C_word c,C_word *av) C_noret;
C_noret_decl(f_709)
static void C_ccall f_709(C_word c,C_word *av) C_noret;
C_noret_decl(f_712)
static void C_ccall f_712(C_word c,C_word *av) C_noret;
C_noret_decl(f_715)
static void C_ccall f_715(C_word c,C_word *av) C_noret;
C_noret_decl(f_719)
static void C_fcall f_719(C_word t0,C_word t1) C_noret;
C_noret_decl(f_722)
static void C_ccall f_722(C_word c,C_word *av) C_noret;
C_noret_decl(f_746)
static void C_ccall f_746(C_word c,C_word *av) C_noret;
C_noret_decl(f_756)
static void C_ccall f_756(C_word c,C_word *av) C_noret;
C_noret_decl(f_762)
static void C_ccall f_762(C_word c,C_word *av) C_noret;
C_noret_decl(f_776)
static void C_ccall f_776(C_word c,C_word *av) C_noret;
C_noret_decl(f_781)
static void C_ccall f_781(C_word c,C_word *av) C_noret;
C_noret_decl(f_787)
static void C_fcall f_787(C_word t0,C_word t1) C_noret;
C_noret_decl(f_794)
static void C_ccall f_794(C_word c,C_word *av) C_noret;
C_noret_decl(f_797)
static void C_ccall f_797(C_word c,C_word *av) C_noret;
C_noret_decl(f_803)
static void C_ccall f_803(C_word c,C_word *av) C_noret;
C_noret_decl(f_812)
static void C_ccall f_812(C_word c,C_word *av) C_noret;
C_noret_decl(f_818)
static void C_ccall f_818(C_word c,C_word *av) C_noret;
C_noret_decl(f_824)
static void C_ccall f_824(C_word c,C_word *av) C_noret;
C_noret_decl(f_828)
static void C_ccall f_828(C_word c,C_word *av) C_noret;
C_noret_decl(f_831)
static void C_ccall f_831(C_word c,C_word *av) C_noret;
C_noret_decl(f_842)
static void C_fcall f_842(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_846)
static void C_ccall f_846(C_word c,C_word *av) C_noret;
C_noret_decl(f_858)
static void C_ccall f_858(C_word c,C_word *av) C_noret;
C_noret_decl(f_863)
static void C_ccall f_863(C_word c,C_word *av) C_noret;
C_noret_decl(f_866)
static void C_ccall f_866(C_word c,C_word *av) C_noret;
C_noret_decl(f_869)
static void C_ccall f_869(C_word c,C_word *av) C_noret;
C_noret_decl(f_876)
static void C_ccall f_876(C_word c,C_word *av) C_noret;
C_noret_decl(f_879)
static void C_ccall f_879(C_word c,C_word *av) C_noret;
C_noret_decl(f_891)
static void C_ccall f_891(C_word c,C_word *av) C_noret;
C_noret_decl(f_896)
static void C_fcall f_896(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_906)
static void C_ccall f_906(C_word c,C_word *av) C_noret;
C_noret_decl(f_926)
static void C_ccall f_926(C_word c,C_word *av) C_noret;
C_noret_decl(f_971)
static void C_ccall f_971(C_word c,C_word *av) C_noret;
C_noret_decl(f_973)
static void C_ccall f_973(C_word c,C_word *av) C_noret;
C_noret_decl(f_979)
static void C_ccall f_979(C_word c,C_word *av) C_noret;
C_noret_decl(f_989)
static void C_ccall f_989(C_word c,C_word *av) C_noret;
C_noret_decl(f_993)
static void C_ccall f_993(C_word c,C_word *av) C_noret;
C_noret_decl(f_998)
static void C_ccall f_998(C_word c,C_word *av) C_noret;
C_noret_decl(C_repl_toplevel)
C_externexport void C_ccall C_repl_toplevel(C_word c,C_word *av) C_noret;

C_noret_decl(trf_539)
static void C_ccall trf_539(C_word c,C_word *av) C_noret;
static void C_ccall trf_539(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_539(t0,t1);}

C_noret_decl(trf_553)
static void C_ccall trf_553(C_word c,C_word *av) C_noret;
static void C_ccall trf_553(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_553(t0,t1,t2);}

C_noret_decl(trf_625)
static void C_ccall trf_625(C_word c,C_word *av) C_noret;
static void C_ccall trf_625(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_625(t0,t1,t2);}

C_noret_decl(trf_719)
static void C_ccall trf_719(C_word c,C_word *av) C_noret;
static void C_ccall trf_719(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_719(t0,t1);}

C_noret_decl(trf_787)
static void C_ccall trf_787(C_word c,C_word *av) C_noret;
static void C_ccall trf_787(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_787(t0,t1);}

C_noret_decl(trf_842)
static void C_ccall trf_842(C_word c,C_word *av) C_noret;
static void C_ccall trf_842(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_842(t0,t1,t2,t3);}

C_noret_decl(trf_896)
static void C_ccall trf_896(C_word c,C_word *av) C_noret;
static void C_ccall trf_896(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_896(t0,t1,t2);}

/* a1008 in k454 in k451 */
static void C_ccall f_1009(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1009,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=lf[56];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k451 */
static void C_ccall f_453(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_453,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_456,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_library_toplevel(2,av2);}}

/* k454 in k451 */
static void C_ccall f_456(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(34,c,4)))){
C_save_and_reclaim((void *)f_456,c,av);}
a=C_alloc(34);
t2=C_a_i_provide(&a,1,lf[0]);
t3=C_a_i_provide(&a,1,lf[1]);
t4=C_set_block_item(lf[2] /* ##sys#repl-print-length-limit */,0,C_SCHEME_FALSE);
t5=C_set_block_item(lf[3] /* ##sys#repl-read-hook */,0,C_SCHEME_FALSE);
t6=C_set_block_item(lf[4] /* ##sys#repl-recent-call-chain */,0,C_SCHEME_FALSE);
t7=C_mutate((C_word*)lf[5]+1 /* (set! ##sys#repl-print-hook ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_461,a[2]=((C_word)li1),tmp=(C_word)a,a+=3,tmp));
t8=C_mutate(&lf[9] /* (set! chicken.repl#quit-hook ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_476,a[2]=((C_word)li2),tmp=(C_word)a,a+=3,tmp));
t9=C_mutate((C_word*)lf[11]+1 /* (set! chicken.repl#quit ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_482,a[2]=((C_word)li3),tmp=(C_word)a,a+=3,tmp));
t10=C_mutate((C_word*)lf[12]+1 /* (set! chicken.repl#reset-handler ...) */,*((C_word*)lf[13]+1));
t11=C_mutate((C_word*)lf[14]+1 /* (set! chicken.repl#reset ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_498,a[2]=((C_word)li4),tmp=(C_word)a,a+=3,tmp));
t12=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_509,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t13=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1009,a[2]=((C_word)li24),tmp=(C_word)a,a+=3,tmp);
/* repl.scm:58: chicken.base#make-parameter */
t14=*((C_word*)lf[57]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t14;
av2[1]=t12;
av2[2]=t13;
((C_proc)(void*)(*((C_word*)t14+1)))(3,av2);}}

/* ##sys#repl-print-hook in k454 in k451 */
static void C_ccall f_461(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_461,c,av);}
a=C_alloc(9);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_465,a[2]=t1,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_470,a[2]=t2,a[3]=t3,a[4]=((C_word)li0),tmp=(C_word)a,a+=5,tmp);
/* repl.scm:48: ##sys#with-print-length-limit */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[8]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[8]+1);
av2[1]=t4;
av2[2]=*((C_word*)lf[2]+1);
av2[3]=t5;
tp(4,av2);}}

/* k463 in ##sys#repl-print-hook in k454 in k451 */
static void C_ccall f_465(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_465,c,av);}
/* repl.scm:49: ##sys#write-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[6]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[6]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=C_make_character(10);
av2[3]=((C_word*)t0)[3];
tp(4,av2);}}

/* a469 in ##sys#repl-print-hook in k454 in k451 */
static void C_ccall f_470(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_470,c,av);}
t2=*((C_word*)lf[7]+1);
/* repl.scm:48: g104 */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[7]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[7]+1);
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[3];
tp(5,av2);}}

/* chicken.repl#quit-hook in k454 in k451 */
static void C_ccall f_476(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_476,c,av);}
/* repl.scm:51: chicken.base#exit */
t3=*((C_word*)lf[10]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.repl#quit in k454 in k451 */
static void C_ccall f_482(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_482,c,av);}
if(C_truep(C_rest_nullp(c,2))){
/* repl.scm:52: quit-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(lf[9]);
C_word av2[3];
av2[0]=lf[9];
av2[1]=t1;
av2[2]=C_SCHEME_FALSE;
tp(3,av2);}}
else{
/* repl.scm:52: quit-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(lf[9]);
C_word av2[3];
av2[0]=lf[9];
av2[1]=t1;
av2[2]=C_get_rest_arg(c,2,av,2,t0);
tp(3,av2);}}}

/* chicken.repl#reset in k454 in k451 */
static void C_ccall f_498(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_498,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_502,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* repl.scm:55: reset-handler */
t3=*((C_word*)lf[12]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k500 in chicken.repl#reset in k454 in k451 */
static void C_ccall f_502(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_502,c,av);}
/* repl.scm:55: g122 */
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k507 in k454 in k451 */
static void C_ccall f_509(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_509,c,av);}
a=C_alloc(12);
t2=C_mutate((C_word*)lf[15]+1 /* (set! chicken.repl#repl-prompt ...) */,t1);
t3=*((C_word*)lf[15]+1);
t4=C_mutate((C_word*)lf[16]+1 /* (set! ##sys#read-prompt-hook ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_511,a[2]=t3,a[3]=((C_word)li5),tmp=(C_word)a,a+=4,tmp));
t5=C_mutate((C_word*)lf[19]+1 /* (set! ##sys#resize-trace-buffer ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_527,a[2]=((C_word)li6),tmp=(C_word)a,a+=3,tmp));
t6=*((C_word*)lf[21]+1);
t7=*((C_word*)lf[22]+1);
t8=C_mutate((C_word*)lf[23]+1 /* (set! chicken.repl#repl ...) */,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_533,a[2]=t6,a[3]=t7,a[4]=((C_word)li23),tmp=(C_word)a,a+=5,tmp));
t9=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t9;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t9+1)))(2,av2);}}

/* ##sys#read-prompt-hook in k507 in k454 in k451 */
static void C_ccall f_511(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_511,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_515,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_522,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* repl.scm:63: repl-prompt */
t4=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k513 in ##sys#read-prompt-hook in k507 in k454 in k451 */
static void C_ccall f_515(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_515,c,av);}
/* repl.scm:64: ##sys#flush-output */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[17]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[17]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=*((C_word*)lf[18]+1);
tp(3,av2);}}

/* k520 in ##sys#read-prompt-hook in k507 in k454 in k451 */
static void C_ccall f_522(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_522,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_525,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* repl.scm:63: g127 */
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k523 in k520 in ##sys#read-prompt-hook in k507 in k454 in k451 */
static void C_ccall f_525(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_525,c,av);}
/* repl.scm:63: ##sys#print */
t2=*((C_word*)lf[7]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_SCHEME_FALSE;
av2[4]=*((C_word*)lf[18]+1);
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* ##sys#resize-trace-buffer in k507 in k454 in k451 */
static void C_ccall f_527(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_527,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_531,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* repl.scm:67: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[20]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[20]+1);
av2[1]=t3;
av2[2]=t2;
tp(3,av2);}}

/* k529 in ##sys#resize-trace-buffer in k507 in k454 in k451 */
static void C_ccall f_531(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_531,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_resize_trace_buffer(((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_533(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,3)))){
C_save_and_reclaim((void *)f_533,c,av);}
a=C_alloc(18);
t2=C_rest_nullp(c,2);
t3=(C_truep(t2)?((C_word*)t0)[2]:C_get_rest_arg(c,2,av,2,t0));
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_539,a[2]=((C_word)li8),tmp=(C_word)a,a+=3,tmp);
t5=*((C_word*)lf[26]+1);
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=*((C_word*)lf[18]+1);
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=*((C_word*)lf[25]+1);
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_657,a[2]=t6,a[3]=t8,a[4]=t10,a[5]=t4,a[6]=((C_word*)t0)[3],a[7]=t3,a[8]=t1,tmp=(C_word)a,a+=9,tmp);
/* repl.scm:93: ##sys#error-handler */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[38]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[38]+1);
av2[1]=t11;
tp(2,av2);}}

/* write-err in chicken.repl#repl in k507 in k454 in k451 */
static void C_fcall f_539(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_539,2,t1,t2);}
a=C_alloc(6);
t3=C_i_check_list_2(t2,lf[24]);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_553,a[2]=t5,a[3]=((C_word)li7),tmp=(C_word)a,a+=4,tmp));
t7=((C_word*)t5)[1];
f_553(t7,t1,t2);}

/* for-each-loop153 in write-err in chicken.repl#repl in k507 in k454 in k451 */
static void C_fcall f_553(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,3)))){
C_save_and_reclaim_args((void *)trf_553,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_563,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t4=C_slot(t2,C_fix(0));
t5=*((C_word*)lf[5]+1);
/* repl.scm:78: g169 */
t6=t5;{
C_word av2[4];
av2[0]=t6;
av2[1]=t3;
av2[2]=t4;
av2[3]=*((C_word*)lf[25]+1);
((C_proc)C_fast_retrieve_proc(t6))(4,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k561 in for-each-loop153 in write-err in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_563(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_563,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_553(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k598 in k826 in a823 in k810 in k801 in k795 in k792 in loop in a780 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_600(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_600,c,av);}
a=C_alloc(6);
if(C_truep(C_i_pairp(C_u_i_cdr(((C_word*)t0)[2])))){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_613,a[2]=((C_word*)t0)[3],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_617,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* repl.scm:87: ##sys#number->string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[44]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[44]+1);
av2[1]=t3;
av2[2]=C_i_length(((C_word*)t0)[2]);
tp(3,av2);}}
else{
/* repl.scm:183: loop */
t2=((C_word*)((C_word*)t0)[4])[1];
f_787(t2,((C_word*)t0)[5]);}}

/* k611 in k598 in k826 in a823 in k810 in k801 in k795 in k792 in loop in a780 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_613(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_613,c,av);}
/* repl.scm:86: ##sys#print */
t2=*((C_word*)lf[7]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_SCHEME_FALSE;
av2[4]=*((C_word*)lf[18]+1);
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k615 in k598 in k826 in a823 in k810 in k801 in k795 in k792 in loop in a780 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_617(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_617,c,av);}
/* repl.scm:87: string-append */
t2=*((C_word*)lf[41]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[42];
av2[3]=t1;
av2[4]=lf[43];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* for-each-loop183 in k826 in a823 in k810 in k801 in k795 in k792 in loop in a780 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_fcall f_625(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,3)))){
C_save_and_reclaim_args((void *)trf_625,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_635,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t4=C_slot(t2,C_fix(0));
t5=*((C_word*)lf[5]+1);
/* repl.scm:84: g199 */
t6=t5;{
C_word av2[4];
av2[0]=t6;
av2[1]=t3;
av2[2]=t4;
av2[3]=*((C_word*)lf[18]+1);
((C_proc)C_fast_retrieve_proc(t6))(4,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k633 in for-each-loop183 in k826 in a823 in k810 in k801 in k795 in k792 in loop in a780 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_635(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_635,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_625(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_657(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_657,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_660,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=t1,a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
/* repl.scm:94: ##sys#reset-handler */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[13]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[13]+1);
av2[1]=t2;
tp(2,av2);}}

/* k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_660(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(29,c,3)))){
C_save_and_reclaim((void *)f_660,c,av);}
a=C_alloc(29);
t2=C_SCHEME_FALSE;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=lf[9];
t5=*((C_word*)lf[27]+1);
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_662,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp));
t11=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_668,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp));
t12=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_677,a[2]=t3,a[3]=t9,a[4]=((C_word*)t0)[5],a[5]=t7,a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=t4,a[9]=t5,a[10]=t1,a[11]=((C_word*)t0)[8],a[12]=((C_word)li22),tmp=(C_word)a,a+=13,tmp);
/* repl.scm:109: call-with-current-continuation */
t13=*((C_word*)lf[54]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t13;
av2[1]=((C_word*)t0)[9];
av2[2]=t12;
((C_proc)(void*)(*((C_word*)t13+1)))(3,av2);}}

/* saveports in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static C_word C_fcall f_662(C_word t0){
C_word tmp;
C_word t1;
C_word t2;
C_word t3;
C_word t4;
C_stack_overflow_check;{}
t1=C_mutate(((C_word *)((C_word*)t0)[2])+1,*((C_word*)lf[26]+1));
t2=C_mutate(((C_word *)((C_word*)t0)[3])+1,*((C_word*)lf[18]+1));
t3=C_mutate(((C_word *)((C_word*)t0)[4])+1,*((C_word*)lf[25]+1));
return(t3);}

/* resetports in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static C_word C_fcall f_668(C_word t0){
C_word tmp;
C_word t1;
C_word t2;
C_word t3;
C_word t4;
C_stack_overflow_check;{}
t1=C_mutate((C_word*)lf[26]+1 /* (set! ##sys#standard-input ...) */,((C_word*)((C_word*)t0)[2])[1]);
t2=C_mutate((C_word*)lf[18]+1 /* (set! ##sys#standard-output ...) */,((C_word*)((C_word*)t0)[3])[1]);
t3=C_mutate((C_word*)lf[25]+1 /* (set! ##sys#standard-error ...) */,((C_word*)((C_word*)t0)[4])[1]);
return(t3);}

/* a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_677(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(22,c,4)))){
C_save_and_reclaim((void *)f_677,c,av);}
a=C_alloc(22);
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_683,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word)li11),tmp=(C_word)a,a+=7,tmp);
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_781,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[7],a[5]=((C_word*)t0)[3],a[6]=((C_word)li20),tmp=(C_word)a,a+=7,tmp);
t5=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_989,a[2]=((C_word*)t0)[8],a[3]=((C_word*)t0)[9],a[4]=((C_word*)t0)[10],a[5]=((C_word*)t0)[11],a[6]=((C_word*)t0)[2],a[7]=((C_word)li21),tmp=(C_word)a,a+=8,tmp);
/* repl.scm:111: ##sys#dynamic-wind */
t6=*((C_word*)lf[55]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t6;
av2[1]=t1;
av2[2]=t3;
av2[3]=t4;
av2[4]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* a682 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_683(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_683,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_688,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,tmp=(C_word)a,a+=7,tmp);
/* repl.scm:113: chicken.load#load-verbose */
t3=*((C_word*)lf[39]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k686 in a682 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_688(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_688,c,av);}
a=C_alloc(9);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=C_mutate(&lf[9] /* (set! chicken.repl#quit-hook ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_690,a[2]=((C_word*)t0)[3],a[3]=((C_word)li9),tmp=(C_word)a,a+=4,tmp));
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_697,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],tmp=(C_word)a,a+=5,tmp);
/* repl.scm:115: chicken.load#load-verbose */
t5=*((C_word*)lf[39]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* chicken.repl#quit-hook in k686 in a682 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_690(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_690,c,av);}
/* repl.scm:114: k */
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t1;
av2[2]=t2;
((C_proc)C_fast_retrieve_proc(t3))(3,av2);}}

/* k695 in k686 in a682 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_697(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_697,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_702,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word)li10),tmp=(C_word)a,a+=5,tmp);
/* repl.scm:116: ##sys#error-handler */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[38]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[38]+1);
av2[1]=((C_word*)t0)[4];
av2[2]=t2;
tp(3,av2);}}

/* a701 in k695 in k686 in a682 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_702(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +6,c,4)))){
C_save_and_reclaim((void*)f_702,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+6);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
C_word t6;
t4=(
/* repl.scm:118: resetports */
  f_668(((C_word*)((C_word*)t0)[2])[1])
);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_709,a[2]=t1,a[3]=t3,a[4]=((C_word*)t0)[3],a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* repl.scm:119: ##sys#print */
t6=*((C_word*)lf[7]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[37];
av2[3]=C_SCHEME_FALSE;
av2[4]=*((C_word*)lf[25]+1);
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k707 in a701 in k695 in k686 in a682 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_709(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_709,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_712,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
if(C_truep(((C_word*)t0)[5])){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_776,a[2]=t2,a[3]=((C_word*)t0)[5],tmp=(C_word)a,a+=4,tmp);
/* repl.scm:121: ##sys#print */
t4=*((C_word*)lf[7]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[36];
av2[3]=C_SCHEME_FALSE;
av2[4]=*((C_word*)lf[25]+1);
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_712(2,av2);}}}

/* k710 in k707 in a701 in k695 in k686 in a682 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_712(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_712,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_715,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=C_i_pairp(((C_word*)t0)[3]);
t4=(C_truep(t3)?C_i_nullp(C_u_i_cdr(((C_word*)t0)[3])):C_SCHEME_FALSE);
if(C_truep(t4)){
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_756,a[2]=((C_word*)t0)[4],a[3]=t2,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* repl.scm:125: ##sys#print */
t6=*((C_word*)lf[7]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[35];
av2[3]=C_SCHEME_FALSE;
av2[4]=*((C_word*)lf[25]+1);
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}
else{
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_762,a[2]=((C_word*)t0)[4],a[3]=t2,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* repl.scm:128: ##sys#write-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[6]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[6]+1);
av2[1]=t5;
av2[2]=C_make_character(10);
av2[3]=*((C_word*)lf[25]+1);
tp(4,av2);}}}

/* k713 in k710 in k707 in a701 in k695 in k686 in a682 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_715(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,4)))){
C_save_and_reclaim((void *)f_715,c,av);}
a=C_alloc(7);
t2=*((C_word*)lf[28]+1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_719,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(*((C_word*)lf[28]+1))){
if(C_truep(C_i_structurep(*((C_word*)lf[28]+1),lf[31]))){
t4=C_slot(*((C_word*)lf[28]+1),C_fix(2));
t5=C_i_member(lf[32],t4);
if(C_truep(t5)){
t6=C_i_cadr(t5);
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_746,a[2]=t3,a[3]=t6,tmp=(C_word)a,a+=4,tmp);
/* repl.scm:135: ##sys#really-print-call-chain */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[33]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[33]+1);
av2[1]=t7;
av2[2]=*((C_word*)lf[25]+1);
av2[3]=t6;
av2[4]=lf[34];
tp(5,av2);}}
else{
t6=t3;
f_719(t6,C_SCHEME_FALSE);}}
else{
t4=t3;
f_719(t4,C_SCHEME_FALSE);}}
else{
t4=t3;
f_719(t4,C_SCHEME_FALSE);}}

/* k717 in k713 in k710 in k707 in a701 in k695 in k686 in a682 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_fcall f_719(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_719,2,t0,t1);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_722,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(t1)){
t3=C_mutate((C_word*)lf[4]+1 /* (set! ##sys#repl-recent-call-chain ...) */,t1);
/* repl.scm:140: chicken.base#flush-output */
t4=*((C_word*)lf[29]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=((C_word*)t0)[2];
av2[2]=*((C_word*)lf[25]+1);
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
/* repl.scm:139: chicken.base#print-call-chain */
t3=*((C_word*)lf[30]+1);{
C_word av2[3];
av2[0]=t3;
av2[1]=t2;
av2[2]=*((C_word*)lf[25]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}

/* k720 in k717 in k713 in k710 in k707 in a701 in k695 in k686 in a682 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_722(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_722,c,av);}
t2=C_set_block_item(lf[4] /* ##sys#repl-recent-call-chain */,0,t1);
/* repl.scm:140: chicken.base#flush-output */
t3=*((C_word*)lf[29]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=*((C_word*)lf[25]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k744 in k713 in k710 in k707 in a701 in k695 in k686 in a682 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_746(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_746,c,av);}
t2=((C_word*)t0)[2];
f_719(t2,((C_word*)t0)[3]);}

/* k754 in k710 in k707 in a701 in k695 in k686 in a682 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_756(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_756,c,av);}
/* repl.scm:126: write-err */
f_539(((C_word*)t0)[3],((C_word*)t0)[4]);}

/* k760 in k710 in k707 in a701 in k695 in k686 in a682 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_762(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_762,c,av);}
/* repl.scm:129: write-err */
f_539(((C_word*)t0)[3],((C_word*)t0)[4]);}

/* k774 in k707 in a701 in k695 in k686 in a682 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_776(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_776,c,av);}
/* repl.scm:122: ##sys#print */
t2=*((C_word*)lf[7]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=C_SCHEME_FALSE;
av2[4]=*((C_word*)lf[25]+1);
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a780 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_781(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_781,c,av);}
a=C_alloc(10);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_787,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t3,a[6]=((C_word*)t0)[5],a[7]=((C_word)li19),tmp=(C_word)a,a+=8,tmp));
t5=((C_word*)t3)[1];
f_787(t5,t1);}

/* loop in a780 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_fcall f_787(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,3)))){
C_save_and_reclaim_args((void *)trf_787,2,t0,t1);}
a=C_alloc(10);
t2=(
/* repl.scm:143: saveports */
  f_662(((C_word*)((C_word*)t0)[2])[1])
);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_794,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_973,a[2]=((C_word*)t0)[6],a[3]=((C_word)li18),tmp=(C_word)a,a+=4,tmp);
/* repl.scm:144: call-with-current-continuation */
t5=*((C_word*)lf[54]+1);{
C_word av2[3];
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k792 in loop in a780 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_794(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_794,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_797,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* repl.scm:151: ##sys#read-prompt-hook */
t3=*((C_word*)lf[16]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k795 in k792 in loop in a780 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_797(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_797,c,av);}
a=C_alloc(5);
t2=*((C_word*)lf[3]+1);
t3=(C_truep(t2)?t2:((C_word*)t0)[2]);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_803,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp);
/* repl.scm:152: g253 */
t5=t3;{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)C_fast_retrieve_proc(t5))(2,av2);}}

/* k801 in k795 in k792 in loop in a780 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_803(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_803,c,av);}
a=C_alloc(9);
if(C_truep(C_eofp(t1))){
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_812,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[2],tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_971,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* repl.scm:154: ##sys#peek-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[52]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[52]+1);
av2[1]=t3;
av2[2]=*((C_word*)lf[26]+1);
tp(3,av2);}}}

/* k810 in k801 in k795 in k792 in loop in a780 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_812(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_812,c,av);}
a=C_alloc(9);
t2=code_258();
t3=C_set_block_item(lf[27] /* ##sys#unbound-in-eval */,0,C_SCHEME_END_OF_LIST);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_818,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word)li12),tmp=(C_word)a,a+=5,tmp);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_824,a[2]=((C_word*)t0)[4],a[3]=((C_word)li16),tmp=(C_word)a,a+=4,tmp);
/* repl.scm:158: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[5];
av2[2]=t4;
av2[3]=t5;
C_call_with_values(4,av2);}}

/* a817 in k810 in k801 in k795 in k792 in loop in a780 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_818(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_818,c,av);}
/* repl.scm:158: evaluator */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=((C_word*)t0)[3];
((C_proc)C_fast_retrieve_proc(t2))(3,av2);}}

/* a823 in k810 in k801 in k795 in k792 in loop in a780 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_824(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +11,c,4)))){
C_save_and_reclaim((void*)f_824,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+11);
t2=C_build_rest(&a,c,2,av);
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_828,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t4=(C_truep(*((C_word*)lf[45]+1))?C_i_pairp(*((C_word*)lf[27]+1)):C_SCHEME_FALSE);
if(C_truep(t4)){
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_842,a[2]=t6,a[3]=((C_word)li15),tmp=(C_word)a,a+=4,tmp));
t8=((C_word*)t6)[1];
f_842(t8,t3,*((C_word*)lf[27]+1),C_SCHEME_END_OF_LIST);}
else{
t5=t3;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_UNDEFINED;
f_828(2,av2);}}}

/* k826 in a823 in k810 in k801 in k795 in k792 in loop in a780 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_828(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,4)))){
C_save_and_reclaim((void *)f_828,c,av);}
a=C_alloc(16);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_831,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_i_nullp(((C_word*)t0)[4]))){
/* repl.scm:82: ##sys#print */
t3=*((C_word*)lf[7]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[40];
av2[3]=C_SCHEME_FALSE;
av2[4]=*((C_word*)lf[18]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}
else{
t3=C_i_car(((C_word*)t0)[4]);
t4=C_eqp(C_SCHEME_UNDEFINED,t3);
if(C_truep(C_i_not(t4))){
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_600,a[2]=((C_word*)t0)[4],a[3]=t2,a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_625,a[2]=t7,a[3]=((C_word)li13),tmp=(C_word)a,a+=4,tmp));
t9=((C_word*)t7)[1];
f_625(t9,t5,((C_word*)t0)[4]);}
else{
/* repl.scm:183: loop */
t5=((C_word*)((C_word*)t0)[2])[1];
f_787(t5,((C_word*)t0)[3]);}}}

/* k829 in k826 in a823 in k810 in k801 in k795 in k792 in loop in a780 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_831(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_831,c,av);}
/* repl.scm:183: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_787(t2,((C_word*)t0)[3]);}

/* loop in a823 in k810 in k801 in k795 in k792 in loop in a780 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_fcall f_842(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,2)))){
C_save_and_reclaim_args((void *)trf_842,4,t0,t1,t2,t3);}
a=C_alloc(9);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_846,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_i_nullp(t2))){
if(C_truep(C_i_pairp(t3))){
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_858,a[2]=t4,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* repl.scm:164: ##sys#notice */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[48]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[48]+1);
av2[1]=t5;
av2[2]=lf[49];
tp(3,av2);}}
else{
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=C_fix(9);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}
else{
t5=C_i_caar(t2);
t6=C_i_memq(t5,t3);
t7=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_926,a[2]=((C_word*)t0)[2],a[3]=t4,a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
if(C_truep(t6)){
t8=t7;{
C_word av2[2];
av2[0]=t8;
av2[1]=t6;
f_926(2,av2);}}
else{
t8=C_u_i_car(t2);
t9=C_u_i_car(t8);
t10=C_u_i_namespaced_symbolp(t9);
if(C_truep(t10)){
t11=t7;{
C_word av2[2];
av2[0]=t11;
av2[1]=t10;
f_926(2,av2);}}
else{
t11=C_u_i_car(t2);
/* repl.scm:179: ##sys#symbol-has-toplevel-binding? */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[50]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[50]+1);
av2[1]=t7;
av2[2]=C_u_i_car(t11);
tp(3,av2);}}}}}

/* k844 in loop in a823 in k810 in k801 in k795 in k792 in loop in a780 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_846(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_846,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_fix(9);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k856 in loop in a823 in k810 in k801 in k795 in k792 in loop in a780 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_858(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_858,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_891,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_896,a[2]=t4,a[3]=((C_word)li14),tmp=(C_word)a,a+=4,tmp));
t6=((C_word*)t4)[1];
f_896(t6,t2,((C_word*)t0)[3]);}

/* k861 in for-each-loop271 in k856 in loop in a823 in k810 in k801 in k795 in k792 in loop in a780 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_863(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_863,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_866,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* repl.scm:169: ##sys#print */
t3=*((C_word*)lf[7]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_i_car(((C_word*)t0)[3]);
av2[3]=C_SCHEME_TRUE;
av2[4]=*((C_word*)lf[25]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k864 in k861 in for-each-loop271 in k856 in loop in a823 in k810 in k801 in k795 in k792 in loop in a780 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_866(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,4)))){
C_save_and_reclaim((void *)f_866,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_869,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_u_i_cdr(((C_word*)t0)[3]))){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_876,a[2]=t2,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* repl.scm:171: ##sys#print */
t4=*((C_word*)lf[7]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[46];
av2[3]=C_SCHEME_FALSE;
av2[4]=*((C_word*)lf[25]+1);
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}
else{
/* repl.scm:174: ##sys#write-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[6]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[6]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=C_make_character(10);
av2[3]=*((C_word*)lf[25]+1);
tp(4,av2);}}}

/* k867 in k864 in k861 in for-each-loop271 in k856 in loop in a823 in k810 in k801 in k795 in k792 in loop in a780 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_869(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_869,c,av);}
/* repl.scm:174: ##sys#write-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[6]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[6]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=C_make_character(10);
av2[3]=*((C_word*)lf[25]+1);
tp(4,av2);}}

/* k874 in k864 in k861 in for-each-loop271 in k856 in loop in a823 in k810 in k801 in k795 in k792 in loop in a780 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_876(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_876,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_879,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* repl.scm:172: ##sys#print */
t3=*((C_word*)lf[7]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_u_i_cdr(((C_word*)t0)[3]);
av2[3]=C_SCHEME_TRUE;
av2[4]=*((C_word*)lf[25]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k877 in k874 in k864 in k861 in for-each-loop271 in k856 in loop in a823 in k810 in k801 in k795 in k792 in loop in a780 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_879(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_879,c,av);}
/* repl.scm:173: ##sys#write-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[6]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[6]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=C_make_character(41);
av2[3]=*((C_word*)lf[25]+1);
tp(4,av2);}}

/* k889 in k856 in loop in a823 in k810 in k801 in k795 in k792 in loop in a780 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_891(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_891,c,av);}
/* repl.scm:176: ##sys#flush-output */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[17]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[17]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=*((C_word*)lf[25]+1);
tp(3,av2);}}

/* for-each-loop271 in k856 in loop in a823 in k810 in k801 in k795 in k792 in loop in a780 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_fcall f_896(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,4)))){
C_save_and_reclaim_args((void *)trf_896,3,t0,t1,t2);}
a=C_alloc(9);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_906,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t4=C_slot(t2,C_fix(0));
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_863,a[2]=t3,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* repl.scm:168: ##sys#print */
t6=*((C_word*)lf[7]+1);{
C_word av2[5];
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[47];
av2[3]=C_SCHEME_FALSE;
av2[4]=*((C_word*)lf[25]+1);
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k904 in for-each-loop271 in k856 in loop in a823 in k810 in k801 in k795 in k792 in loop in a780 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_906(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_906,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_896(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k924 in loop in a823 in k810 in k801 in k795 in k792 in loop in a780 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_926(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_926,c,av);}
a=C_alloc(3);
if(C_truep(t1)){
/* repl.scm:180: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_842(t2,((C_word*)t0)[3],C_u_i_cdr(((C_word*)t0)[4]),((C_word*)t0)[5]);}
else{
t2=C_u_i_cdr(((C_word*)t0)[4]);
t3=C_u_i_car(((C_word*)t0)[4]);
t4=C_a_i_cons(&a,2,t3,((C_word*)t0)[5]);
/* repl.scm:181: loop */
t5=((C_word*)((C_word*)t0)[2])[1];
f_842(t5,((C_word*)t0)[3],t2,t4);}}

/* k969 in k801 in k795 in k792 in loop in a780 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_971(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_971,c,av);}
t2=C_eqp(C_make_character(10),t1);
if(C_truep(t2)){
/* repl.scm:155: ##sys#read-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[51]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[51]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=*((C_word*)lf[26]+1);
tp(3,av2);}}
else{
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_812(2,av2);}}}

/* a972 in loop in a780 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_973(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_973,c,av);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_979,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=((C_word)li17),tmp=(C_word)a,a+=5,tmp);
/* repl.scm:146: ##sys#reset-handler */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[13]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[13]+1);
av2[1]=t1;
av2[2]=t3;
tp(3,av2);}}

/* a978 in a972 in loop in a780 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_979(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_979,c,av);}
t2=C_set_block_item(lf[53] /* ##sys#read-error-with-line-number */,0,C_SCHEME_FALSE);
t3=(
/* repl.scm:149: resetports */
  f_668(((C_word*)((C_word*)t0)[2])[1])
);
/* repl.scm:150: c */
t4=((C_word*)t0)[3];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t1;
av2[2]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}

/* a988 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_989(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_989,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_993,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* repl.scm:185: chicken.load#load-verbose */
t3=*((C_word*)lf[39]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[6])[1];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k991 in a988 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_993(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_993,c,av);}
a=C_alloc(4);
t2=C_mutate(&lf[9] /* (set! chicken.repl#quit-hook ...) */,((C_word*)t0)[2]);
t3=C_mutate((C_word*)lf[27]+1 /* (set! ##sys#unbound-in-eval ...) */,((C_word*)t0)[3]);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_998,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],tmp=(C_word)a,a+=4,tmp);
/* repl.scm:188: ##sys#error-handler */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[38]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[38]+1);
av2[1]=t4;
av2[2]=((C_word*)t0)[6];
tp(3,av2);}}

/* k996 in k991 in a988 in a676 in k658 in k655 in chicken.repl#repl in k507 in k454 in k451 */
static void C_ccall f_998(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_998,c,av);}
/* repl.scm:189: ##sys#reset-handler */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[13]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[13]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
tp(3,av2);}}

/* toplevel */
static C_TLS int toplevel_initialized=0;

void C_ccall C_repl_toplevel(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(toplevel_initialized) {C_kontinue(t1,C_SCHEME_UNDEFINED);}
else C_toplevel_entry(C_text("repl"));
C_check_nursery_minimum(C_calculate_demand(3,c,2));
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void*)C_repl_toplevel,c,av);}
toplevel_initialized=1;
if(C_unlikely(!C_demand_2(332))){
C_save(t1);
C_rereclaim2(332*sizeof(C_word),1);
t1=C_restore;}
a=C_alloc(3);
C_initialize_lf(lf,58);
lf[0]=C_h_intern(&lf[0],4, C_text("repl"));
lf[1]=C_h_intern(&lf[1],13, C_text("chicken.repl#"));
lf[2]=C_h_intern(&lf[2],29, C_text("##sys#repl-print-length-limit"));
lf[3]=C_h_intern(&lf[3],20, C_text("##sys#repl-read-hook"));
lf[4]=C_h_intern(&lf[4],28, C_text("##sys#repl-recent-call-chain"));
lf[5]=C_h_intern(&lf[5],21, C_text("##sys#repl-print-hook"));
lf[6]=C_h_intern(&lf[6],18, C_text("##sys#write-char-0"));
lf[7]=C_h_intern(&lf[7],11, C_text("##sys#print"));
lf[8]=C_h_intern(&lf[8],29, C_text("##sys#with-print-length-limit"));
lf[10]=C_h_intern(&lf[10],17, C_text("chicken.base#exit"));
lf[11]=C_h_intern(&lf[11],17, C_text("chicken.repl#quit"));
lf[12]=C_h_intern(&lf[12],26, C_text("chicken.repl#reset-handler"));
lf[13]=C_h_intern(&lf[13],19, C_text("##sys#reset-handler"));
lf[14]=C_h_intern(&lf[14],18, C_text("chicken.repl#reset"));
lf[15]=C_h_intern(&lf[15],24, C_text("chicken.repl#repl-prompt"));
lf[16]=C_h_intern(&lf[16],22, C_text("##sys#read-prompt-hook"));
lf[17]=C_h_intern(&lf[17],18, C_text("##sys#flush-output"));
lf[18]=C_h_intern(&lf[18],21, C_text("##sys#standard-output"));
lf[19]=C_h_intern(&lf[19],25, C_text("##sys#resize-trace-buffer"));
lf[20]=C_h_intern(&lf[20],18, C_text("##sys#check-fixnum"));
lf[21]=C_h_intern(&lf[21],11, C_text("scheme#eval"));
lf[22]=C_h_intern(&lf[22],11, C_text("scheme#read"));
lf[23]=C_h_intern(&lf[23],17, C_text("chicken.repl#repl"));
lf[24]=C_h_intern(&lf[24],8, C_text("for-each"));
lf[25]=C_h_intern(&lf[25],20, C_text("##sys#standard-error"));
lf[26]=C_h_intern(&lf[26],20, C_text("##sys#standard-input"));
lf[27]=C_h_intern(&lf[27],21, C_text("##sys#unbound-in-eval"));
lf[28]=C_h_intern(&lf[28],20, C_text("##sys#last-exception"));
lf[29]=C_h_intern(&lf[29],25, C_text("chicken.base#flush-output"));
lf[30]=C_h_intern(&lf[30],29, C_text("chicken.base#print-call-chain"));
lf[31]=C_h_intern(&lf[31],9, C_text("condition"));
lf[32]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\003\001exn\376\001\000\000\012\001call-chain"));
lf[33]=C_h_intern(&lf[33],29, C_text("##sys#really-print-call-chain"));
lf[34]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020\012\011Call history:\012"));
lf[35]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002: "));
lf[36]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002: "));
lf[37]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006\012Error"));
lf[38]=C_h_intern(&lf[38],19, C_text("##sys#error-handler"));
lf[39]=C_h_intern(&lf[39],25, C_text("chicken.load#load-verbose"));
lf[40]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014; no values\012"));
lf[41]=C_h_intern(&lf[41],20, C_text("scheme#string-append"));
lf[42]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002; "));
lf[43]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010 values\012"));
lf[44]=C_h_intern(&lf[44],20, C_text("##sys#number->string"));
lf[45]=C_h_intern(&lf[45],22, C_text("##sys#warnings-enabled"));
lf[46]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005 (in "));
lf[47]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002  "));
lf[48]=C_h_intern(&lf[48],12, C_text("##sys#notice"));
lf[49]=C_decode_literal(C_heaptop,C_text("\376B\000\000=the following toplevel variables are referenced but unbound:\012"));
lf[50]=C_h_intern(&lf[50],34, C_text("##sys#symbol-has-toplevel-binding\077"));
lf[51]=C_h_intern(&lf[51],17, C_text("##sys#read-char-0"));
lf[52]=C_h_intern(&lf[52],17, C_text("##sys#peek-char-0"));
lf[53]=C_h_intern(&lf[53],33, C_text("##sys#read-error-with-line-number"));
lf[54]=C_h_intern(&lf[54],37, C_text("scheme#call-with-current-continuation"));
lf[55]=C_h_intern(&lf[55],18, C_text("##sys#dynamic-wind"));
lf[56]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004#;> "));
lf[57]=C_h_intern(&lf[57],27, C_text("chicken.base#make-parameter"));
C_register_lf2(lf,58,create_ptable());{}
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_453,a[2]=t1,tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_eval_toplevel(2,av2);}}

#ifdef C_ENABLE_PTABLES
static C_PTABLE_ENTRY ptable[75] = {
{C_text("f_1009:repl_2escm"),(void*)f_1009},
{C_text("f_453:repl_2escm"),(void*)f_453},
{C_text("f_456:repl_2escm"),(void*)f_456},
{C_text("f_461:repl_2escm"),(void*)f_461},
{C_text("f_465:repl_2escm"),(void*)f_465},
{C_text("f_470:repl_2escm"),(void*)f_470},
{C_text("f_476:repl_2escm"),(void*)f_476},
{C_text("f_482:repl_2escm"),(void*)f_482},
{C_text("f_498:repl_2escm"),(void*)f_498},
{C_text("f_502:repl_2escm"),(void*)f_502},
{C_text("f_509:repl_2escm"),(void*)f_509},
{C_text("f_511:repl_2escm"),(void*)f_511},
{C_text("f_515:repl_2escm"),(void*)f_515},
{C_text("f_522:repl_2escm"),(void*)f_522},
{C_text("f_525:repl_2escm"),(void*)f_525},
{C_text("f_527:repl_2escm"),(void*)f_527},
{C_text("f_531:repl_2escm"),(void*)f_531},
{C_text("f_533:repl_2escm"),(void*)f_533},
{C_text("f_539:repl_2escm"),(void*)f_539},
{C_text("f_553:repl_2escm"),(void*)f_553},
{C_text("f_563:repl_2escm"),(void*)f_563},
{C_text("f_600:repl_2escm"),(void*)f_600},
{C_text("f_613:repl_2escm"),(void*)f_613},
{C_text("f_617:repl_2escm"),(void*)f_617},
{C_text("f_625:repl_2escm"),(void*)f_625},
{C_text("f_635:repl_2escm"),(void*)f_635},
{C_text("f_657:repl_2escm"),(void*)f_657},
{C_text("f_660:repl_2escm"),(void*)f_660},
{C_text("f_662:repl_2escm"),(void*)f_662},
{C_text("f_668:repl_2escm"),(void*)f_668},
{C_text("f_677:repl_2escm"),(void*)f_677},
{C_text("f_683:repl_2escm"),(void*)f_683},
{C_text("f_688:repl_2escm"),(void*)f_688},
{C_text("f_690:repl_2escm"),(void*)f_690},
{C_text("f_697:repl_2escm"),(void*)f_697},
{C_text("f_702:repl_2escm"),(void*)f_702},
{C_text("f_709:repl_2escm"),(void*)f_709},
{C_text("f_712:repl_2escm"),(void*)f_712},
{C_text("f_715:repl_2escm"),(void*)f_715},
{C_text("f_719:repl_2escm"),(void*)f_719},
{C_text("f_722:repl_2escm"),(void*)f_722},
{C_text("f_746:repl_2escm"),(void*)f_746},
{C_text("f_756:repl_2escm"),(void*)f_756},
{C_text("f_762:repl_2escm"),(void*)f_762},
{C_text("f_776:repl_2escm"),(void*)f_776},
{C_text("f_781:repl_2escm"),(void*)f_781},
{C_text("f_787:repl_2escm"),(void*)f_787},
{C_text("f_794:repl_2escm"),(void*)f_794},
{C_text("f_797:repl_2escm"),(void*)f_797},
{C_text("f_803:repl_2escm"),(void*)f_803},
{C_text("f_812:repl_2escm"),(void*)f_812},
{C_text("f_818:repl_2escm"),(void*)f_818},
{C_text("f_824:repl_2escm"),(void*)f_824},
{C_text("f_828:repl_2escm"),(void*)f_828},
{C_text("f_831:repl_2escm"),(void*)f_831},
{C_text("f_842:repl_2escm"),(void*)f_842},
{C_text("f_846:repl_2escm"),(void*)f_846},
{C_text("f_858:repl_2escm"),(void*)f_858},
{C_text("f_863:repl_2escm"),(void*)f_863},
{C_text("f_866:repl_2escm"),(void*)f_866},
{C_text("f_869:repl_2escm"),(void*)f_869},
{C_text("f_876:repl_2escm"),(void*)f_876},
{C_text("f_879:repl_2escm"),(void*)f_879},
{C_text("f_891:repl_2escm"),(void*)f_891},
{C_text("f_896:repl_2escm"),(void*)f_896},
{C_text("f_906:repl_2escm"),(void*)f_906},
{C_text("f_926:repl_2escm"),(void*)f_926},
{C_text("f_971:repl_2escm"),(void*)f_971},
{C_text("f_973:repl_2escm"),(void*)f_973},
{C_text("f_979:repl_2escm"),(void*)f_979},
{C_text("f_989:repl_2escm"),(void*)f_989},
{C_text("f_993:repl_2escm"),(void*)f_993},
{C_text("f_998:repl_2escm"),(void*)f_998},
{C_text("toplevel:repl_2escm"),(void*)C_repl_toplevel},
{NULL,NULL}};
#endif

static C_PTABLE_ENTRY *create_ptable(void){
#ifdef C_ENABLE_PTABLES
return ptable;
#else
return NULL;
#endif
}

/*
o|hiding unexported module binding: chicken.repl#d 
o|hiding unexported module binding: chicken.repl#define-alias 
o|hiding unexported module binding: chicken.repl#quit-hook 
S|applied compiler syntax:
S|  scheme#for-each		3
o|eliminated procedure checks: 22 
o|specializations:
o|  1 (scheme#car pair)
o|  2 (scheme#caar (pair pair *))
o|  6 (scheme#cdr pair)
o|  2 (##sys#check-list (or pair list) *)
(o e)|safe calls: 52 
(o e)|assignments to immediate values: 2 
o|safe globals: (chicken.repl#reset chicken.repl#reset-handler chicken.repl#quit chicken.repl#quit-hook ##sys#repl-print-hook ##sys#repl-recent-call-chain ##sys#repl-read-hook ##sys#repl-print-length-limit) 
o|propagated global variable: g104105 ##sys#print 
o|inlining procedure: k484 
o|inlining procedure: k484 
o|inlining procedure: k555 
o|contracted procedure: "(repl.scm:78) g154161" 
o|inlining procedure: k555 
o|inlining procedure: k578 
o|inlining procedure: k578 
o|inlining procedure: k601 
o|inlining procedure: k601 
o|inlining procedure: k627 
o|contracted procedure: "(repl.scm:84) g184191" 
o|inlining procedure: k627 
o|propagated global variable: lexn234 ##sys#last-exception 
o|inlining procedure: k732 
o|inlining procedure: k732 
o|propagated global variable: lexn234 ##sys#last-exception 
o|inlining procedure: k804 
o|inlining procedure: k804 
o|inlining procedure: k844 
o|inlining procedure: k898 
o|contracted procedure: "(repl.scm:166) g272279" 
o|inlining procedure: k867 
o|inlining procedure: k867 
o|inlining procedure: k898 
o|inlining procedure: k844 
o|inlining procedure: k946 
o|inlining procedure: k946 
o|replaced variables: 104 
o|removed binding forms: 76 
o|substituted constant variable: r4851011 
o|substituted constant variable: r4851011 
o|substituted constant variable: r7331030 
o|contracted procedure: "(repl.scm:182) write-results149" 
o|inlining procedure: k844 
o|converted assignments to bindings: (write-err148) 
o|simplifications: ((let . 1)) 
o|replaced variables: 12 
o|removed binding forms: 102 
o|inlining procedure: k720 
o|replaced variables: 9 
o|removed binding forms: 15 
o|inlining procedure: k829 
o|inlining procedure: k829 
o|replaced variables: 1 
o|removed binding forms: 2 
o|removed binding forms: 3 
o|removed binding forms: 1 
o|simplifications: ((if . 4) (let . 5) (##core#call . 36)) 
o|  call simplifications:
o|    scheme#eof-object?
o|    ##sys#call-with-values
o|    scheme#caar
o|    scheme#memq
o|    scheme#cons
o|    scheme#eq?	2
o|    scheme#not
o|    scheme#length
o|    ##sys#structure?
o|    scheme#member
o|    scheme#cadr
o|    ##sys#check-list
o|    scheme#pair?	7
o|    ##sys#slot	7
o|    scheme#null?	5
o|    scheme#car	4
o|contracted procedure: k490 
o|contracted procedure: k484 
o|contracted procedure: k1002 
o|contracted procedure: k535 
o|contracted procedure: k546 
o|contracted procedure: k558 
o|contracted procedure: k568 
o|contracted procedure: k572 
o|contracted procedure: k729 
o|contracted procedure: k748 
o|propagated global variable: lexn234 ##sys#last-exception 
o|contracted procedure: k735 
o|contracted procedure: k741 
o|contracted procedure: k766 
o|contracted procedure: k751 
o|contracted procedure: k798 
o|contracted procedure: k807 
o|contracted procedure: k581 
o|contracted procedure: k652 
o|contracted procedure: k648 
o|contracted procedure: k590 
o|contracted procedure: k604 
o|contracted procedure: k619 
o|contracted procedure: k630 
o|contracted procedure: k640 
o|contracted procedure: k644 
o|contracted procedure: k835 
o|contracted procedure: k847 
o|contracted procedure: k853 
o|contracted procedure: k901 
o|contracted procedure: k911 
o|contracted procedure: k915 
o|contracted procedure: k886 
o|contracted procedure: k956 
o|contracted procedure: k921 
o|contracted procedure: k938 
o|contracted procedure: k962 
o|simplifications: ((let . 17)) 
o|removed binding forms: 36 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest113114 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest113114 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest141142 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest141142 0 
o|direct leaf routine/allocation: saveports215 0 
o|direct leaf routine/allocation: resetports216 0 
o|contracted procedure: "(repl.scm:118) k704" 
o|contracted procedure: "(repl.scm:143) k789" 
o|contracted procedure: "(repl.scm:149) k982" 
o|removed binding forms: 3 
o|customizable procedures: (loop262 for-each-loop271287 for-each-loop183201 loop248 write-err148 k717 for-each-loop153171) 
o|calls to known targets: 24 
o|unused rest argument: rest113114 f_482 
o|unused rest argument: rest141142 f_533 
o|fast box initializations: 7 
o|fast global references: 3 
o|fast global assignments: 3 
o|dropping unused closure argument: f_539 
*/
/* end of file */
