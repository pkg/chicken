/* Generated from lfa2.scm by the CHICKEN compiler
   http://www.call-cc.org
   Version 5.2.0 (rev 317468e4)
   linux-unix-gnu-x86-64 [ 64bit dload ptables ]
   command line: lfa2.scm -optimize-level 2 -include-path . -include-path ./ -inline -ignore-repository -feature chicken-bootstrap -no-warnings -specialize -consult-types-file ./types.db -no-lambda-info -no-trace -emit-import-library chicken.compiler.lfa2 -output-file lfa2.c
   unit: lfa2
   uses: library eval expand extras support
*/
#include "chicken.h"

static C_PTABLE_ENTRY *create_ptable(void);
C_noret_decl(C_library_toplevel)
C_externimport void C_ccall C_library_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_eval_toplevel)
C_externimport void C_ccall C_eval_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_expand_toplevel)
C_externimport void C_ccall C_expand_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_extras_toplevel)
C_externimport void C_ccall C_extras_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_support_toplevel)
C_externimport void C_ccall C_support_toplevel(C_word c,C_word *av) C_noret;

static C_TLS C_word lf[102];
static double C_possibly_force_alignment;


C_noret_decl(f5390)
static void C_ccall f5390(C_word c,C_word *av) C_noret;
C_noret_decl(f5448)
static void C_ccall f5448(C_word c,C_word *av) C_noret;
C_noret_decl(f5469)
static void C_ccall f5469(C_word c,C_word *av) C_noret;
C_noret_decl(f5481)
static void C_ccall f5481(C_word c,C_word *av) C_noret;
C_noret_decl(f5488)
static void C_ccall f5488(C_word c,C_word *av) C_noret;
C_noret_decl(f_1451)
static void C_ccall f_1451(C_word c,C_word *av) C_noret;
C_noret_decl(f_1454)
static void C_ccall f_1454(C_word c,C_word *av) C_noret;
C_noret_decl(f_1457)
static void C_ccall f_1457(C_word c,C_word *av) C_noret;
C_noret_decl(f_1460)
static void C_ccall f_1460(C_word c,C_word *av) C_noret;
C_noret_decl(f_1463)
static void C_ccall f_1463(C_word c,C_word *av) C_noret;
C_noret_decl(f_2083)
static void C_fcall f_2083(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_2091)
static C_word C_fcall f_2091(C_word *a,C_word t0,C_word t1,C_word t2);
C_noret_decl(f_2112)
static void C_ccall f_2112(C_word c,C_word *av) C_noret;
C_noret_decl(f_2127)
static void C_fcall f_2127(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_2143)
static C_word C_fcall f_2143(C_word *a,C_word t0,C_word t1);
C_noret_decl(f_2161)
static void C_ccall f_2161(C_word c,C_word *av) C_noret;
C_noret_decl(f_2173)
static C_word C_fcall f_2173(C_word t0,C_word t1);
C_noret_decl(f_2677)
static void C_fcall f_2677(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_2683)
static C_word C_fcall f_2683(C_word t0,C_word t1,C_word t2);
C_noret_decl(f_2748)
static void C_ccall f_2748(C_word c,C_word *av) C_noret;
C_noret_decl(f_2764)
static void C_ccall f_2764(C_word c,C_word *av) C_noret;
C_noret_decl(f_2776)
static void C_ccall f_2776(C_word c,C_word *av) C_noret;
C_noret_decl(f_2779)
static void C_ccall f_2779(C_word c,C_word *av) C_noret;
C_noret_decl(f_2879)
static C_word C_fcall f_2879(C_word t0,C_word t1);
C_noret_decl(f_2930)
static C_word C_fcall f_2930(C_word *a,C_word t0,C_word t1);
C_noret_decl(f_2956)
static void C_fcall f_2956(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_2962)
static void C_fcall f_2962(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_2967)
static void C_ccall f_2967(C_word c,C_word *av) C_noret;
C_noret_decl(f_2993)
static void C_ccall f_2993(C_word c,C_word *av) C_noret;
C_noret_decl(f_2997)
static void C_ccall f_2997(C_word c,C_word *av) C_noret;
C_noret_decl(f_3001)
static void C_ccall f_3001(C_word c,C_word *av) C_noret;
C_noret_decl(f_3003)
static void C_fcall f_3003(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3007)
static void C_ccall f_3007(C_word c,C_word *av) C_noret;
C_noret_decl(f_3010)
static void C_ccall f_3010(C_word c,C_word *av) C_noret;
C_noret_decl(f_3013)
static void C_ccall f_3013(C_word c,C_word *av) C_noret;
C_noret_decl(f_3018)
static void C_fcall f_3018(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_3025)
static void C_ccall f_3025(C_word c,C_word *av) C_noret;
C_noret_decl(f_3026)
static void C_fcall f_3026(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3033)
static void C_ccall f_3033(C_word c,C_word *av) C_noret;
C_noret_decl(f_3043)
static void C_ccall f_3043(C_word c,C_word *av) C_noret;
C_noret_decl(f_3060)
static void C_ccall f_3060(C_word c,C_word *av) C_noret;
C_noret_decl(f_3067)
static void C_ccall f_3067(C_word c,C_word *av) C_noret;
C_noret_decl(f_3095)
static void C_ccall f_3095(C_word c,C_word *av) C_noret;
C_noret_decl(f_3097)
static void C_fcall f_3097(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3107)
static void C_ccall f_3107(C_word c,C_word *av) C_noret;
C_noret_decl(f_3126)
static void C_ccall f_3126(C_word c,C_word *av) C_noret;
C_noret_decl(f_3143)
static C_word C_fcall f_3143(C_word t0,C_word t1);
C_noret_decl(f_3189)
static C_word C_fcall f_3189(C_word t0,C_word t1);
C_noret_decl(f_3195)
static void C_fcall f_3195(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3200)
static void C_ccall f_3200(C_word c,C_word *av) C_noret;
C_noret_decl(f_3202)
static C_word C_fcall f_3202(C_word t0,C_word t1);
C_noret_decl(f_3212)
static void C_fcall f_3212(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_3219)
static void C_fcall f_3219(C_word t0,C_word t1) C_noret;
C_noret_decl(f_3223)
static void C_fcall f_3223(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3231)
static void C_ccall f_3231(C_word c,C_word *av) C_noret;
C_noret_decl(f_3243)
static void C_ccall f_3243(C_word c,C_word *av) C_noret;
C_noret_decl(f_3260)
static void C_ccall f_3260(C_word c,C_word *av) C_noret;
C_noret_decl(f_3268)
static void C_fcall f_3268(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3274)
static void C_fcall f_3274(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3280)
static void C_fcall f_3280(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3286)
static void C_fcall f_3286(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_3290)
static void C_ccall f_3290(C_word c,C_word *av) C_noret;
C_noret_decl(f_3293)
static void C_ccall f_3293(C_word c,C_word *av) C_noret;
C_noret_decl(f_3296)
static void C_ccall f_3296(C_word c,C_word *av) C_noret;
C_noret_decl(f_3305)
static void C_ccall f_3305(C_word c,C_word *av) C_noret;
C_noret_decl(f_3308)
static void C_ccall f_3308(C_word c,C_word *av) C_noret;
C_noret_decl(f_3355)
static void C_ccall f_3355(C_word c,C_word *av) C_noret;
C_noret_decl(f_3358)
static void C_fcall f_3358(C_word t0,C_word t1) C_noret;
C_noret_decl(f_3365)
static void C_ccall f_3365(C_word c,C_word *av) C_noret;
C_noret_decl(f_3369)
static void C_ccall f_3369(C_word c,C_word *av) C_noret;
C_noret_decl(f_3387)
static void C_ccall f_3387(C_word c,C_word *av) C_noret;
C_noret_decl(f_3391)
static void C_ccall f_3391(C_word c,C_word *av) C_noret;
C_noret_decl(f_3399)
static void C_ccall f_3399(C_word c,C_word *av) C_noret;
C_noret_decl(f_3411)
static void C_ccall f_3411(C_word c,C_word *av) C_noret;
C_noret_decl(f_3455)
static void C_ccall f_3455(C_word c,C_word *av) C_noret;
C_noret_decl(f_3458)
static void C_ccall f_3458(C_word c,C_word *av) C_noret;
C_noret_decl(f_3461)
static void C_ccall f_3461(C_word c,C_word *av) C_noret;
C_noret_decl(f_3472)
static void C_fcall f_3472(C_word t0,C_word t1) C_noret;
C_noret_decl(f_3479)
static void C_fcall f_3479(C_word t0,C_word t1) C_noret;
C_noret_decl(f_3493)
static void C_ccall f_3493(C_word c,C_word *av) C_noret;
C_noret_decl(f_3509)
static void C_ccall f_3509(C_word c,C_word *av) C_noret;
C_noret_decl(f_3517)
static void C_ccall f_3517(C_word c,C_word *av) C_noret;
C_noret_decl(f_3523)
static void C_fcall f_3523(C_word t0,C_word t1) C_noret;
C_noret_decl(f_3565)
static void C_ccall f_3565(C_word c,C_word *av) C_noret;
C_noret_decl(f_3584)
static void C_ccall f_3584(C_word c,C_word *av) C_noret;
C_noret_decl(f_3587)
static void C_ccall f_3587(C_word c,C_word *av) C_noret;
C_noret_decl(f_3590)
static void C_fcall f_3590(C_word t0,C_word t1) C_noret;
C_noret_decl(f_3601)
static void C_ccall f_3601(C_word c,C_word *av) C_noret;
C_noret_decl(f_3615)
static void C_ccall f_3615(C_word c,C_word *av) C_noret;
C_noret_decl(f_3640)
static void C_fcall f_3640(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3644)
static void C_ccall f_3644(C_word c,C_word *av) C_noret;
C_noret_decl(f_3656)
static void C_ccall f_3656(C_word c,C_word *av) C_noret;
C_noret_decl(f_3663)
static void C_fcall f_3663(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3667)
static void C_ccall f_3667(C_word c,C_word *av) C_noret;
C_noret_decl(f_3670)
static void C_ccall f_3670(C_word c,C_word *av) C_noret;
C_noret_decl(f_3685)
static void C_fcall f_3685(C_word t0,C_word t1) C_noret;
C_noret_decl(f_3711)
static void C_ccall f_3711(C_word c,C_word *av) C_noret;
C_noret_decl(f_3731)
static void C_ccall f_3731(C_word c,C_word *av) C_noret;
C_noret_decl(f_3793)
static void C_fcall f_3793(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3800)
static void C_ccall f_3800(C_word c,C_word *av) C_noret;
C_noret_decl(f_3803)
static void C_ccall f_3803(C_word c,C_word *av) C_noret;
C_noret_decl(f_3809)
static void C_ccall f_3809(C_word c,C_word *av) C_noret;
C_noret_decl(f_3812)
static void C_ccall f_3812(C_word c,C_word *av) C_noret;
C_noret_decl(f_3819)
static void C_ccall f_3819(C_word c,C_word *av) C_noret;
C_noret_decl(f_3823)
static void C_ccall f_3823(C_word c,C_word *av) C_noret;
C_noret_decl(f_3827)
static void C_ccall f_3827(C_word c,C_word *av) C_noret;
C_noret_decl(f_3841)
static void C_fcall f_3841(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3895)
static void C_ccall f_3895(C_word c,C_word *av) C_noret;
C_noret_decl(f_3903)
static void C_ccall f_3903(C_word c,C_word *av) C_noret;
C_noret_decl(f_3918)
static void C_ccall f_3918(C_word c,C_word *av) C_noret;
C_noret_decl(f_3921)
static void C_ccall f_3921(C_word c,C_word *av) C_noret;
C_noret_decl(f_3924)
static void C_ccall f_3924(C_word c,C_word *av) C_noret;
C_noret_decl(f_3933)
static void C_fcall f_3933(C_word t0,C_word t1) C_noret;
C_noret_decl(f_3959)
static void C_ccall f_3959(C_word c,C_word *av) C_noret;
C_noret_decl(f_3979)
static void C_ccall f_3979(C_word c,C_word *av) C_noret;
C_noret_decl(f_4037)
static void C_fcall f_4037(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4047)
static void C_fcall f_4047(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4063)
static void C_ccall f_4063(C_word c,C_word *av) C_noret;
C_noret_decl(f_4079)
static void C_ccall f_4079(C_word c,C_word *av) C_noret;
C_noret_decl(f_4110)
static void C_fcall f_4110(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4120)
static void C_ccall f_4120(C_word c,C_word *av) C_noret;
C_noret_decl(f_4136)
static void C_fcall f_4136(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4146)
static void C_ccall f_4146(C_word c,C_word *av) C_noret;
C_noret_decl(f_4148)
static void C_fcall f_4148(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4158)
static void C_ccall f_4158(C_word c,C_word *av) C_noret;
C_noret_decl(f_4210)
static void C_ccall f_4210(C_word c,C_word *av) C_noret;
C_noret_decl(f_4213)
static void C_ccall f_4213(C_word c,C_word *av) C_noret;
C_noret_decl(f_4221)
static void C_ccall f_4221(C_word c,C_word *av) C_noret;
C_noret_decl(f_4225)
static void C_ccall f_4225(C_word c,C_word *av) C_noret;
C_noret_decl(f_4233)
static void C_ccall f_4233(C_word c,C_word *av) C_noret;
C_noret_decl(f_4236)
static void C_ccall f_4236(C_word c,C_word *av) C_noret;
C_noret_decl(f_4239)
static void C_ccall f_4239(C_word c,C_word *av) C_noret;
C_noret_decl(f_4242)
static void C_ccall f_4242(C_word c,C_word *av) C_noret;
C_noret_decl(f_4259)
static void C_fcall f_4259(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4269)
static void C_ccall f_4269(C_word c,C_word *av) C_noret;
C_noret_decl(f_4282)
static void C_ccall f_4282(C_word c,C_word *av) C_noret;
C_noret_decl(f_4286)
static void C_ccall f_4286(C_word c,C_word *av) C_noret;
C_noret_decl(f_4288)
static void C_ccall f_4288(C_word c,C_word *av) C_noret;
C_noret_decl(f_4292)
static void C_ccall f_4292(C_word c,C_word *av) C_noret;
C_noret_decl(f_4295)
static void C_ccall f_4295(C_word c,C_word *av) C_noret;
C_noret_decl(f_4298)
static void C_ccall f_4298(C_word c,C_word *av) C_noret;
C_noret_decl(f_4326)
static void C_ccall f_4326(C_word c,C_word *av) C_noret;
C_noret_decl(f_4363)
static void C_fcall f_4363(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4392)
static void C_ccall f_4392(C_word c,C_word *av) C_noret;
C_noret_decl(f_4394)
static void C_fcall f_4394(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4419)
static void C_ccall f_4419(C_word c,C_word *av) C_noret;
C_noret_decl(f_4443)
static void C_ccall f_4443(C_word c,C_word *av) C_noret;
C_noret_decl(f_4453)
static void C_ccall f_4453(C_word c,C_word *av) C_noret;
C_noret_decl(f_4455)
static void C_fcall f_4455(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4480)
static void C_ccall f_4480(C_word c,C_word *av) C_noret;
C_noret_decl(f_4502)
static void C_ccall f_4502(C_word c,C_word *av) C_noret;
C_noret_decl(f_4515)
static void C_ccall f_4515(C_word c,C_word *av) C_noret;
C_noret_decl(f_4519)
static void C_ccall f_4519(C_word c,C_word *av) C_noret;
C_noret_decl(f_4522)
static void C_ccall f_4522(C_word c,C_word *av) C_noret;
C_noret_decl(f_4525)
static void C_ccall f_4525(C_word c,C_word *av) C_noret;
C_noret_decl(f_4534)
static void C_ccall f_4534(C_word c,C_word *av) C_noret;
C_noret_decl(f_4548)
static void C_ccall f_4548(C_word c,C_word *av) C_noret;
C_noret_decl(f_4571)
static void C_ccall f_4571(C_word c,C_word *av) C_noret;
C_noret_decl(f_4589)
static void C_ccall f_4589(C_word c,C_word *av) C_noret;
C_noret_decl(f_4593)
static void C_ccall f_4593(C_word c,C_word *av) C_noret;
C_noret_decl(f_4610)
static void C_ccall f_4610(C_word c,C_word *av) C_noret;
C_noret_decl(f_4612)
static void C_fcall f_4612(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4637)
static void C_ccall f_4637(C_word c,C_word *av) C_noret;
C_noret_decl(f_4660)
static void C_fcall f_4660(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4672)
static void C_ccall f_4672(C_word c,C_word *av) C_noret;
C_noret_decl(f_4704)
static void C_ccall f_4704(C_word c,C_word *av) C_noret;
C_noret_decl(f_4706)
static void C_fcall f_4706(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4731)
static void C_ccall f_4731(C_word c,C_word *av) C_noret;
C_noret_decl(f_4757)
static void C_ccall f_4757(C_word c,C_word *av) C_noret;
C_noret_decl(f_4759)
static void C_fcall f_4759(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4784)
static void C_ccall f_4784(C_word c,C_word *av) C_noret;
C_noret_decl(f_4808)
static void C_ccall f_4808(C_word c,C_word *av) C_noret;
C_noret_decl(f_4810)
static void C_fcall f_4810(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4835)
static void C_ccall f_4835(C_word c,C_word *av) C_noret;
C_noret_decl(f_4856)
static void C_ccall f_4856(C_word c,C_word *av) C_noret;
C_noret_decl(f_4859)
static void C_ccall f_4859(C_word c,C_word *av) C_noret;
C_noret_decl(f_4861)
static void C_ccall f_4861(C_word c,C_word *av) C_noret;
C_noret_decl(f_4868)
static void C_ccall f_4868(C_word c,C_word *av) C_noret;
C_noret_decl(f_4871)
static void C_ccall f_4871(C_word c,C_word *av) C_noret;
C_noret_decl(f_4874)
static void C_ccall f_4874(C_word c,C_word *av) C_noret;
C_noret_decl(f_4880)
static void C_ccall f_4880(C_word c,C_word *av) C_noret;
C_noret_decl(f_4883)
static void C_ccall f_4883(C_word c,C_word *av) C_noret;
C_noret_decl(C_lfa2_toplevel)
C_externexport void C_ccall C_lfa2_toplevel(C_word c,C_word *av) C_noret;

C_noret_decl(trf_2083)
static void C_ccall trf_2083(C_word c,C_word *av) C_noret;
static void C_ccall trf_2083(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_2083(t0,t1,t2);}

C_noret_decl(trf_2127)
static void C_ccall trf_2127(C_word c,C_word *av) C_noret;
static void C_ccall trf_2127(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_2127(t0,t1,t2);}

C_noret_decl(trf_2677)
static void C_ccall trf_2677(C_word c,C_word *av) C_noret;
static void C_ccall trf_2677(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_2677(t0,t1,t2);}

C_noret_decl(trf_2956)
static void C_ccall trf_2956(C_word c,C_word *av) C_noret;
static void C_ccall trf_2956(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_2956(t0,t1,t2);}

C_noret_decl(trf_2962)
static void C_ccall trf_2962(C_word c,C_word *av) C_noret;
static void C_ccall trf_2962(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_2962(t0,t1,t2);}

C_noret_decl(trf_3003)
static void C_ccall trf_3003(C_word c,C_word *av) C_noret;
static void C_ccall trf_3003(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3003(t0,t1,t2);}

C_noret_decl(trf_3018)
static void C_ccall trf_3018(C_word c,C_word *av) C_noret;
static void C_ccall trf_3018(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_3018(t0,t1,t2,t3);}

C_noret_decl(trf_3026)
static void C_ccall trf_3026(C_word c,C_word *av) C_noret;
static void C_ccall trf_3026(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3026(t0,t1,t2);}

C_noret_decl(trf_3097)
static void C_ccall trf_3097(C_word c,C_word *av) C_noret;
static void C_ccall trf_3097(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3097(t0,t1,t2);}

C_noret_decl(trf_3195)
static void C_ccall trf_3195(C_word c,C_word *av) C_noret;
static void C_ccall trf_3195(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3195(t0,t1,t2);}

C_noret_decl(trf_3212)
static void C_ccall trf_3212(C_word c,C_word *av) C_noret;
static void C_ccall trf_3212(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_3212(t0,t1,t2,t3,t4);}

C_noret_decl(trf_3219)
static void C_ccall trf_3219(C_word c,C_word *av) C_noret;
static void C_ccall trf_3219(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_3219(t0,t1);}

C_noret_decl(trf_3223)
static void C_ccall trf_3223(C_word c,C_word *av) C_noret;
static void C_ccall trf_3223(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3223(t0,t1,t2);}

C_noret_decl(trf_3268)
static void C_ccall trf_3268(C_word c,C_word *av) C_noret;
static void C_ccall trf_3268(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3268(t0,t1,t2);}

C_noret_decl(trf_3274)
static void C_ccall trf_3274(C_word c,C_word *av) C_noret;
static void C_ccall trf_3274(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3274(t0,t1,t2);}

C_noret_decl(trf_3280)
static void C_ccall trf_3280(C_word c,C_word *av) C_noret;
static void C_ccall trf_3280(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3280(t0,t1,t2);}

C_noret_decl(trf_3286)
static void C_ccall trf_3286(C_word c,C_word *av) C_noret;
static void C_ccall trf_3286(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_3286(t0,t1,t2,t3,t4);}

C_noret_decl(trf_3358)
static void C_ccall trf_3358(C_word c,C_word *av) C_noret;
static void C_ccall trf_3358(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_3358(t0,t1);}

C_noret_decl(trf_3472)
static void C_ccall trf_3472(C_word c,C_word *av) C_noret;
static void C_ccall trf_3472(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_3472(t0,t1);}

C_noret_decl(trf_3479)
static void C_ccall trf_3479(C_word c,C_word *av) C_noret;
static void C_ccall trf_3479(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_3479(t0,t1);}

C_noret_decl(trf_3523)
static void C_ccall trf_3523(C_word c,C_word *av) C_noret;
static void C_ccall trf_3523(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_3523(t0,t1);}

C_noret_decl(trf_3590)
static void C_ccall trf_3590(C_word c,C_word *av) C_noret;
static void C_ccall trf_3590(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_3590(t0,t1);}

C_noret_decl(trf_3640)
static void C_ccall trf_3640(C_word c,C_word *av) C_noret;
static void C_ccall trf_3640(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3640(t0,t1,t2);}

C_noret_decl(trf_3663)
static void C_ccall trf_3663(C_word c,C_word *av) C_noret;
static void C_ccall trf_3663(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3663(t0,t1,t2);}

C_noret_decl(trf_3685)
static void C_ccall trf_3685(C_word c,C_word *av) C_noret;
static void C_ccall trf_3685(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_3685(t0,t1);}

C_noret_decl(trf_3793)
static void C_ccall trf_3793(C_word c,C_word *av) C_noret;
static void C_ccall trf_3793(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3793(t0,t1,t2);}

C_noret_decl(trf_3841)
static void C_ccall trf_3841(C_word c,C_word *av) C_noret;
static void C_ccall trf_3841(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3841(t0,t1,t2);}

C_noret_decl(trf_3933)
static void C_ccall trf_3933(C_word c,C_word *av) C_noret;
static void C_ccall trf_3933(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_3933(t0,t1);}

C_noret_decl(trf_4037)
static void C_ccall trf_4037(C_word c,C_word *av) C_noret;
static void C_ccall trf_4037(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4037(t0,t1,t2);}

C_noret_decl(trf_4047)
static void C_ccall trf_4047(C_word c,C_word *av) C_noret;
static void C_ccall trf_4047(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4047(t0,t1);}

C_noret_decl(trf_4110)
static void C_ccall trf_4110(C_word c,C_word *av) C_noret;
static void C_ccall trf_4110(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4110(t0,t1,t2);}

C_noret_decl(trf_4136)
static void C_ccall trf_4136(C_word c,C_word *av) C_noret;
static void C_ccall trf_4136(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4136(t0,t1,t2);}

C_noret_decl(trf_4148)
static void C_ccall trf_4148(C_word c,C_word *av) C_noret;
static void C_ccall trf_4148(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4148(t0,t1,t2);}

C_noret_decl(trf_4259)
static void C_ccall trf_4259(C_word c,C_word *av) C_noret;
static void C_ccall trf_4259(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4259(t0,t1,t2);}

C_noret_decl(trf_4363)
static void C_ccall trf_4363(C_word c,C_word *av) C_noret;
static void C_ccall trf_4363(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4363(t0,t1,t2);}

C_noret_decl(trf_4394)
static void C_ccall trf_4394(C_word c,C_word *av) C_noret;
static void C_ccall trf_4394(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4394(t0,t1,t2);}

C_noret_decl(trf_4455)
static void C_ccall trf_4455(C_word c,C_word *av) C_noret;
static void C_ccall trf_4455(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4455(t0,t1,t2);}

C_noret_decl(trf_4612)
static void C_ccall trf_4612(C_word c,C_word *av) C_noret;
static void C_ccall trf_4612(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4612(t0,t1,t2);}

C_noret_decl(trf_4660)
static void C_ccall trf_4660(C_word c,C_word *av) C_noret;
static void C_ccall trf_4660(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4660(t0,t1,t2);}

C_noret_decl(trf_4706)
static void C_ccall trf_4706(C_word c,C_word *av) C_noret;
static void C_ccall trf_4706(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4706(t0,t1,t2);}

C_noret_decl(trf_4759)
static void C_ccall trf_4759(C_word c,C_word *av) C_noret;
static void C_ccall trf_4759(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4759(t0,t1,t2);}

C_noret_decl(trf_4810)
static void C_ccall trf_4810(C_word c,C_word *av) C_noret;
static void C_ccall trf_4810(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4810(t0,t1,t2);}

/* f5390 in k3588 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f5390(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f5390,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=lf[61];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* f5448 in count-floatvar in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f5448(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f5448,c,av);}
a=C_alloc(4);
t2=C_eqp(lf[11],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3260,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* lfa2.scm:338: chicken.compiler.support#node-parameters */
t4=*((C_word*)lf[15]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=((C_word*)t0)[2];
f_3219(t3,C_SCHEME_FALSE);}}

/* f5469 in k3470 in k3459 in k3456 in k3453 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f5469(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f5469,c,av);}
a=C_alloc(7);
t2=C_eqp(lf[11],t1);
if(C_truep(t2)){
if(C_truep(C_i_not(((C_word*)t0)[2]))){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3509,a[2]=((C_word*)t0)[3],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3517,a[2]=((C_word*)t0)[4],a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* lfa2.scm:387: chicken.compiler.support#node-parameters */
t5=*((C_word*)lf[15]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}
else{
t3=((C_word*)t0)[3];
f_3479(t3,C_SCHEME_FALSE);}}
else{
t3=((C_word*)t0)[3];
f_3479(t3,C_SCHEME_FALSE);}}

/* f5481 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f5481(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f5481,c,av);}
a=C_alloc(4);
t2=C_eqp(lf[11],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3615,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* lfa2.scm:400: chicken.compiler.support#node-parameters */
t4=*((C_word*)lf[15]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=((C_word*)t0)[2];
f_3590(t3,C_SCHEME_FALSE);}}

/* f5488 in g783 in k3654 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f5488(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,4)))){
C_save_and_reclaim((void *)f5488,c,av);}
a=C_alloc(7);
t2=C_eqp(lf[11],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3918,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* lfa2.scm:450: chicken.compiler.support#node-parameters */
t4=*((C_word*)lf[15]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3921,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[7],a[6]=((C_word*)t0)[4],tmp=(C_word)a,a+=7,tmp);
/* lfa2.scm:464: walk */
t4=((C_word*)((C_word*)t0)[8])[1];
f_3286(t4,t3,((C_word*)t0)[5],((C_word*)t0)[9],((C_word*)t0)[10]);}}

/* k1449 */
static void C_ccall f_1451(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_1451,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1454,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_eval_toplevel(2,av2);}}

/* k1452 in k1449 */
static void C_ccall f_1454(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_1454,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1457,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_expand_toplevel(2,av2);}}

/* k1455 in k1452 in k1449 */
static void C_ccall f_1457(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_1457,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1460,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_extras_toplevel(2,av2);}}

/* k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_1460(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_1460,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1463,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_support_toplevel(2,av2);}}

/* k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_1463(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(22,c,4)))){
C_save_and_reclaim((void *)f_1463,c,av);}
a=C_alloc(22);
t2=C_a_i_provide(&a,1,lf[0]);
t3=C_a_i_provide(&a,1,lf[1]);
t4=C_mutate(&lf[2] /* (set! chicken.compiler.lfa2#posq ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_2677,tmp=(C_word)a,a+=2,tmp));
t5=C_mutate(&lf[3] /* (set! chicken.compiler.lfa2#+unboxed-map+ ...) */,lf[4]);
t6=C_mutate((C_word*)lf[5]+1 /* (set! chicken.compiler.lfa2#perform-secondary-flow-analysis ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_2748,tmp=(C_word)a,a+=2,tmp));
t7=C_mutate((C_word*)lf[88]+1 /* (set! chicken.compiler.lfa2#perform-unboxing ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_4282,tmp=(C_word)a,a+=2,tmp));
t8=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t8;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}

/* foldr245 in eliminate-floatvar in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_2083(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(8,0,3)))){
C_save_and_reclaim_args((void *)trf_2083,3,t0,t1,t2);}
a=C_alloc(8);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2091,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=C_slot(t2,C_fix(0));
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2112,a[2]=t1,a[3]=t3,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
t7=t5;
t8=C_slot(t2,C_fix(1));
t1=t7;
t2=t8;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* g250 in foldr245 in eliminate-floatvar in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static C_word C_fcall f_2091(C_word *a,C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_stack_overflow_check;{}
t3=(
/* mini-srfi-1.scm:131: pred */
  f_2173(((C_word*)t0)[2],t1)
);
return((C_truep(t3)?C_a_i_cons(&a,2,t1,t2):t2));}

/* k2110 in foldr245 in eliminate-floatvar in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_2112(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_2112,c,av);}
a=C_alloc(3);
/* mini-srfi-1.scm:131: g250 */
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(
/* mini-srfi-1.scm:131: g250 */
  f_2091(C_a_i(&a,3),((C_word*)t0)[3],((C_word*)t0)[4],t1)
);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* foldr263 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_2127(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_2127,3,t0,t1,t2);}
a=C_alloc(4);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2161,a[2]=t3,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t6=t4;
t7=C_slot(t2,C_fix(1));
t1=t6;
t2=t7;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* g278 in k2159 in foldr263 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static C_word C_fcall f_2143(C_word *a,C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_stack_overflow_check;{}
return(C_a_i_cons(&a,2,t1,((C_word*)t0)[2]));}

/* k2159 in foldr263 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_2161(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_2161,c,av);}
a=C_alloc(6);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=C_i_caddr(((C_word*)t0)[2]);
t4=C_i_nequalp(t2,t3);
t5=(C_truep(t4)?C_u_i_car(((C_word*)t0)[2]):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2143,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* mini-srfi-1.scm:135: g278 */
t7=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t7;
av2[1]=(
/* mini-srfi-1.scm:135: g278 */
  f_2143(C_a_i(&a,3),t6,t5)
);
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}
else{
t6=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t6;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* a2172 in eliminate-floatvar in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static C_word C_fcall f_2173(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_stack_overflow_check;{}
t2=(
/* mini-srfi-1.scm:141: pred */
  f_3202(((C_word*)t0)[2],t1)
);
return(C_i_not(t2));}

/* chicken.compiler.lfa2#posq in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_2677(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,0,3)))){
C_save_and_reclaim_args((void *)trf_2677,3,t1,t2,t3);}
a=C_alloc(3);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2683,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=(
  f_2683(t4,C_fix(0),t3)
);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* loop in chicken.compiler.lfa2#posq in k1461 in k1458 in k1455 in k1452 in k1449 */
static C_word C_fcall f_2683(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_stack_overflow_check;
loop:{}
if(C_truep(C_i_nullp(t2))){
return(C_SCHEME_FALSE);}
else{
t3=C_i_car(t2);
t4=C_eqp(t3,((C_word*)t0)[2]);
if(C_truep(t4)){
return(t1);}
else{
t6=C_fixnum_plus(t1,C_fix(1));
t7=C_u_i_cdr(t2);
t1=t6;
t2=t7;
goto loop;}}}

/* chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_2748(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word t24;
C_word t25;
C_word t26;
C_word t27;
C_word t28;
C_word t29;
C_word t30;
C_word t31;
C_word t32;
C_word t33;
C_word t34;
C_word t35;
C_word t36;
C_word t37;
C_word t38;
C_word t39;
C_word t40;
C_word t41;
C_word t42;
C_word t43;
C_word t44;
C_word t45;
C_word t46;
C_word t47;
C_word t48;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(83,c,5)))){
C_save_and_reclaim((void *)f_2748,c,av);}
a=C_alloc(83);
t4=C_SCHEME_END_OF_LIST;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_SCHEME_END_OF_LIST;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_SCHEME_UNDEFINED;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_SCHEME_UNDEFINED;
t15=(*a=C_VECTOR_TYPE|1,a[1]=t14,tmp=(C_word)a,a+=2,tmp);
t16=C_SCHEME_UNDEFINED;
t17=(*a=C_VECTOR_TYPE|1,a[1]=t16,tmp=(C_word)a,a+=2,tmp);
t18=C_SCHEME_UNDEFINED;
t19=(*a=C_VECTOR_TYPE|1,a[1]=t18,tmp=(C_word)a,a+=2,tmp);
t20=C_SCHEME_UNDEFINED;
t21=(*a=C_VECTOR_TYPE|1,a[1]=t20,tmp=(C_word)a,a+=2,tmp);
t22=C_SCHEME_UNDEFINED;
t23=(*a=C_VECTOR_TYPE|1,a[1]=t22,tmp=(C_word)a,a+=2,tmp);
t24=C_SCHEME_UNDEFINED;
t25=(*a=C_VECTOR_TYPE|1,a[1]=t24,tmp=(C_word)a,a+=2,tmp);
t26=C_SCHEME_UNDEFINED;
t27=(*a=C_VECTOR_TYPE|1,a[1]=t26,tmp=(C_word)a,a+=2,tmp);
t28=C_SCHEME_UNDEFINED;
t29=(*a=C_VECTOR_TYPE|1,a[1]=t28,tmp=(C_word)a,a+=2,tmp);
t30=C_SCHEME_UNDEFINED;
t31=(*a=C_VECTOR_TYPE|1,a[1]=t30,tmp=(C_word)a,a+=2,tmp);
t32=C_SCHEME_UNDEFINED;
t33=(*a=C_VECTOR_TYPE|1,a[1]=t32,tmp=(C_word)a,a+=2,tmp);
t34=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_2879,tmp=(C_word)a,a+=2,tmp));
t35=C_set_block_item(t11,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2930,a[2]=t5,tmp=(C_word)a,a+=3,tmp));
t36=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2956,a[2]=t3,tmp=(C_word)a,a+=3,tmp));
t37=C_set_block_item(t15,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2962,a[2]=t3,tmp=(C_word)a,a+=3,tmp));
t38=C_set_block_item(t17,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3003,a[2]=t31,tmp=(C_word)a,a+=3,tmp));
t39=C_set_block_item(t19,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3018,a[2]=t11,a[3]=t17,a[4]=t15,tmp=(C_word)a,a+=5,tmp));
t40=C_set_block_item(t21,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3189,a[2]=t7,tmp=(C_word)a,a+=3,tmp));
t41=C_set_block_item(t23,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3195,a[2]=t7,tmp=(C_word)a,a+=3,tmp));
t42=C_set_block_item(t25,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3212,a[2]=t7,tmp=(C_word)a,a+=3,tmp));
t43=C_set_block_item(t27,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3268,a[2]=t25,tmp=(C_word)a,a+=3,tmp));
t44=C_set_block_item(t29,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3274,a[2]=t25,tmp=(C_word)a,a+=3,tmp));
t45=C_set_block_item(t31,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3280,a[2]=t25,tmp=(C_word)a,a+=3,tmp));
t46=C_set_block_item(t33,0,(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_3286,a[2]=t27,a[3]=t21,a[4]=t23,a[5]=t9,a[6]=t33,a[7]=t13,a[8]=t7,a[9]=t29,a[10]=t19,tmp=(C_word)a,a+=11,tmp));
t47=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4210,a[2]=t1,a[3]=t7,a[4]=t5,tmp=(C_word)a,a+=5,tmp);
/* lfa2.scm:497: walk */
t48=((C_word*)t33)[1];
f_3286(t48,t47,t2,C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST);}

/* k2762 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_2764(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_2764,c,av);}
a=C_alloc(4);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=lf[38];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
if(C_truep(C_i_symbolp(((C_word*)t0)[3]))){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=lf[39];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2776,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* lfa2.scm:245: chicken.compiler.support#big-fixnum? */
t3=*((C_word*)lf[53]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}}

/* k2774 in k2762 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_2776(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_2776,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2779,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
if(C_truep(t1)){
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=t1;
f_2779(2,av2);}}
else{
/* lfa2.scm:245: chicken.compiler.support#small-bignum? */
t3=*((C_word*)lf[52]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}

/* k2777 in k2774 in k2762 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_2779(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_2779,c,av);}
a=C_alloc(6);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=lf[40];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
if(C_truep(C_fixnump(((C_word*)t0)[3]))){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=lf[41];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
if(C_truep(C_i_bignump(((C_word*)t0)[3]))){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=lf[42];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
if(C_truep(C_i_flonump(((C_word*)t0)[3]))){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=lf[43];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
if(C_truep(C_i_ratnump(((C_word*)t0)[3]))){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=lf[44];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
if(C_truep(C_i_cplxnump(((C_word*)t0)[3]))){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=lf[45];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
if(C_truep(C_booleanp(((C_word*)t0)[3]))){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=lf[34];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
if(C_truep(C_i_nullp(((C_word*)t0)[3]))){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=lf[46];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
if(C_truep(C_i_listp(((C_word*)t0)[3]))){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=lf[47];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
if(C_truep(C_i_pairp(((C_word*)t0)[3]))){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=lf[48];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
if(C_truep(C_eofp(((C_word*)t0)[3]))){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=lf[49];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
if(C_truep(C_i_vectorp(((C_word*)t0)[3]))){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=lf[50];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_immp(((C_word*)t0)[3]);
t3=C_i_not(t2);
t4=(C_truep(t3)?C_structurep(((C_word*)t0)[3]):C_SCHEME_FALSE);
if(C_truep(t4)){
t5=C_slot(((C_word*)t0)[3],C_fix(0));
t6=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_a_i_list(&a,2,lf[6],t5);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
t5=C_charp(((C_word*)t0)[3]);
t6=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t6;
av2[1]=(C_truep(t5)?lf[51]:lf[7]);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}}}}}}}}}}}}}

/* merge in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static C_word C_fcall f_2879(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_stack_overflow_check;{}
t3=C_eqp(t1,t2);
if(C_truep(t3)){
return(t1);}
else{
if(C_truep(C_i_pairp(t1))){
if(C_truep(C_i_pairp(t2))){
t4=C_eqp(C_u_i_car(t1),lf[6]);
if(C_truep(t4)){
t5=C_eqp(C_u_i_car(t2),lf[6]);
if(C_truep(t5)){
t6=C_i_cadr(t1);
t7=C_i_cadr(t2);
t8=C_eqp(t6,t7);
return((C_truep(t8)?t1:lf[7]));}
else{
return(lf[7]);}}
else{
return(lf[7]);}}
else{
return(lf[7]);}}
else{
return(lf[7]);}}}

/* report in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static C_word C_fcall f_2930(C_word *a,C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_stack_overflow_check;{}
t2=C_i_assoc(t1,((C_word*)((C_word*)t0)[2])[1]);
if(C_truep(t2)){
t3=C_i_cdr(t2);
t4=C_s_a_i_plus(&a,2,t3,C_fix(1));
return(C_i_set_cdr(t2,t4));}
else{
t3=((C_word*)((C_word*)t0)[2])[1];
t4=C_a_i_cons(&a,2,t1,C_fix(1));
t5=C_a_i_cons(&a,2,t4,t3);
t6=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
return(t6);}}

/* assigned? in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_2956(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,4)))){
C_save_and_reclaim_args((void *)trf_2956,3,t0,t1,t2);}
/* lfa2.scm:277: chicken.compiler.support#db-get */
t3=*((C_word*)lf[8]+1);{
C_word av2[5];
av2[0]=t3;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
av2[3]=t2;
av2[4]=lf[9];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* droppable? in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_2962(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_2962,3,t0,t1,t2);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2967,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* lfa2.scm:280: chicken.compiler.support#node-class */
t4=*((C_word*)lf[16]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k2965 in droppable? in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_2967(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_2967,c,av);}
a=C_alloc(5);
t2=C_u_i_memq(t1,lf[10]);
if(C_truep(t2)){
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3001,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* lfa2.scm:282: chicken.compiler.support#node-class */
t4=*((C_word*)lf[16]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}}

/* k2991 in k2995 in k2999 in k2965 in droppable? in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_2993(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2993,c,av);}
t2=C_i_not(t1);
if(C_truep(t2)){
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
/* lfa2.scm:285: chicken.compiler.support#variable-mark */
t3=*((C_word*)lf[12]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=lf[13];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}}

/* k2995 in k2999 in k2965 in droppable? in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_2997(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_2997,c,av);}
a=C_alloc(4);
t2=C_i_car(t1);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2993,a[2]=((C_word*)t0)[2],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* lfa2.scm:284: chicken.compiler.support#db-get */
t4=*((C_word*)lf[8]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[3];
av2[3]=t2;
av2[4]=lf[14];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k2999 in k2965 in droppable? in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3001(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_3001,c,av);}
a=C_alloc(4);
t2=C_eqp(lf[11],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2997,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* lfa2.scm:283: chicken.compiler.support#node-parameters */
t4=*((C_word*)lf[15]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* drop! in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_3003(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_3003,3,t0,t1,t2);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3007,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* lfa2.scm:288: sub-boxed */
t4=((C_word*)((C_word*)t0)[2])[1];
f_3280(t4,t3,t2);}

/* k3005 in drop! in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3007(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_3007,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3010,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* lfa2.scm:289: chicken.compiler.support#node-class-set! */
t3=*((C_word*)lf[19]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=lf[20];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k3008 in k3005 in drop! in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3010(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_3010,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3013,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* lfa2.scm:290: chicken.compiler.support#node-parameters-set! */
t3=*((C_word*)lf[18]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k3011 in k3008 in k3005 in drop! in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3013(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3013,c,av);}
/* lfa2.scm:291: chicken.compiler.support#node-subexpressions-set! */
t2=*((C_word*)lf[17]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* extinguish! in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_3018(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_3018,4,t0,t1,t2,t3);}
a=C_alloc(8);
t4=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3126,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=t2,a[7]=t3,tmp=(C_word)a,a+=8,tmp);
/* lfa2.scm:294: chicken.compiler.support#node-parameters */
t5=*((C_word*)lf[15]+1);{
C_word av2[3];
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k3023 in k3124 in extinguish! in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3025(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(20,c,3)))){
C_save_and_reclaim((void *)f_3025,c,av);}
a=C_alloc(20);
t2=C_SCHEME_TRUE;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3026,a[2]=((C_word*)t0)[2],a[3]=t3,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
t5=C_i_check_list_2(t1,lf[21]);
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3043,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3097,a[2]=t8,a[3]=t4,tmp=(C_word)a,a+=4,tmp));
t10=((C_word*)t8)[1];
f_3097(t10,t6,t1);}

/* g612 in k3023 in k3124 in extinguish! in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_3026(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_3026,3,t0,t1,t2);}
a=C_alloc(6);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3033,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
/* lfa2.scm:299: droppable? */
t4=((C_word*)((C_word*)t0)[4])[1];
f_2962(t4,t3,t2);}

/* k3031 in g612 in k3023 in k3124 in extinguish! in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3033(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_3033,c,av);}
if(C_truep(t1)){
/* lfa2.scm:300: drop! */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3003(t2,((C_word*)t0)[3],((C_word*)t0)[4]);}
else{
t2=C_set_block_item(((C_word*)t0)[5],0,C_SCHEME_FALSE);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k3041 in k3023 in k3124 in extinguish! in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3043(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_3043,c,av);}
a=C_alloc(8);
if(C_truep(((C_word*)((C_word*)t0)[2])[1])){
/* lfa2.scm:304: drop! */
t2=((C_word*)((C_word*)t0)[3])[1];
f_3003(t2,((C_word*)t0)[4],((C_word*)t0)[5]);}
else{
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3060,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3095,a[2]=t2,a[3]=((C_word*)t0)[6],tmp=(C_word)a,a+=4,tmp);
/* lfa2.scm:310: chicken.compiler.support#node-subexpressions */
t4=*((C_word*)lf[28]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}}

/* k3058 in k3041 in k3023 in k3124 in extinguish! in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3060(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_3060,c,av);}
a=C_alloc(3);
t2=C_a_i_list1(&a,1,t1);
/* lfa2.scm:305: chicken.compiler.support#node-parameters-set! */
t3=*((C_word*)lf[18]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k3065 in k3093 in k3041 in k3023 in k3124 in extinguish! in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3067(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3067,c,av);}
/* lfa2.scm:308: scheme#string-append */
t2=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k3093 in k3041 in k3023 in k3124 in extinguish! in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3095(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_3095,c,av);}
a=C_alloc(4);
t2=C_i_length(t1);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3067,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
switch(t2){
case C_fix(1):
/* lfa2.scm:308: scheme#string-append */
t4=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=lf[23];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}
case C_fix(2):
/* lfa2.scm:308: scheme#string-append */
t4=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=lf[24];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}
case C_fix(3):
/* lfa2.scm:308: scheme#string-append */
t4=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=lf[25];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}
default:
/* lfa2.scm:314: chicken.compiler.support#bomb */
t4=*((C_word*)lf[26]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[27];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}}

/* for-each-loop611 in k3023 in k3124 in extinguish! in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_3097(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_3097,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3107,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* lfa2.scm:297: g612 */
t4=((C_word*)t0)[3];
f_3026(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k3105 in for-each-loop611 in k3023 in k3124 in extinguish! in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3107(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_3107,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_3097(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k3124 in extinguish! in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3126(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(42,c,2)))){
C_save_and_reclaim((void *)f_3126,c,av);}
a=C_alloc(42);
t2=(
/* lfa2.scm:294: report */
  f_2930(C_a_i(&a,35),((C_word*)((C_word*)t0)[2])[1],C_i_car(t1))
);
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3025,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],tmp=(C_word)a,a+=7,tmp);
/* lfa2.scm:295: chicken.compiler.support#node-subexpressions */
t4=*((C_word*)lf[28]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* loop in k3306 in k3303 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static C_word C_fcall f_3143(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_stack_overflow_check;
loop:{}
if(C_truep(C_i_nullp(t1))){
return(lf[7]);}
else{
t2=C_i_cdar(t1);
t3=C_eqp(((C_word*)t0)[2],t2);
if(C_truep(t3)){
t4=C_u_i_car(t1);
t5=C_i_assq(C_u_i_car(t4),((C_word*)t0)[3]);
if(C_truep(t5)){
return(C_i_cdr(t5));}
else{
t7=C_u_i_cdr(t1);
t1=t7;
goto loop;}}
else{
t7=C_u_i_cdr(t1);
t1=t7;
goto loop;}}}

/* floatvar? in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static C_word C_fcall f_3189(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_stack_overflow_check;{}
return(C_i_assq(t1,((C_word*)((C_word*)t0)[2])[1]));}

/* eliminate-floatvar in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_3195(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,0,3)))){
C_save_and_reclaim_args((void *)trf_3195,3,t0,t1,t2);}
a=C_alloc(16);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3200,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3202,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t5=((C_word*)((C_word*)t0)[2])[1];
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2173,a[2]=t4,tmp=(C_word)a,a+=3,tmp);
t7=C_i_check_list_2(t5,lf[29]);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2083,a[2]=t6,a[3]=t9,tmp=(C_word)a,a+=4,tmp));
t11=((C_word*)t9)[1];
f_2083(t11,t3,t5);}

/* k3198 in eliminate-floatvar in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3200(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3200,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* a3201 in eliminate-floatvar in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static C_word C_fcall f_3202(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_stack_overflow_check;{}
return(C_eqp(((C_word*)t0)[2],C_i_car(t1)));}

/* count-floatvar in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_3212(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,2)))){
C_save_and_reclaim_args((void *)trf_3212,5,t0,t1,t2,t3,t4);}
a=C_alloc(10);
t5=C_i_nullp(t4);
t6=(C_truep(t5)?C_fix(1):C_i_car(t4));
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3219,a[2]=t6,a[3]=t3,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t8=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f5448,a[2]=t7,a[3]=((C_word*)t0)[2],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* lfa2.scm:327: chicken.compiler.support#node-class */
t9=*((C_word*)lf[16]+1);{
C_word av2[3];
av2[0]=t9;
av2[1]=t8;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}

/* k3217 in count-floatvar in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_3219(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,3)))){
C_save_and_reclaim_args((void *)trf_3219,2,t0,t1);}
a=C_alloc(4);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3223,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* lfa2.scm:337: g677 */
t3=t2;
f_3223(t3,((C_word*)t0)[4],t1);}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[4];{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* g677 in k3217 in count-floatvar in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_3223(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_3223,3,t0,t1,t2);}
a=C_alloc(6);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3231,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* lfa2.scm:341: acc */
t4=((C_word*)t0)[3];{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k3229 in g677 in k3217 in count-floatvar in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3231(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_3231,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3243,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* lfa2.scm:341: acc */
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k3241 in k3229 in g677 in k3217 in count-floatvar in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3243(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(29,c,1)))){
C_save_and_reclaim((void *)f_3243,c,av);}
a=C_alloc(29);
t2=C_i_car(t1);
t3=C_s_a_i_plus(&a,2,((C_word*)t0)[2],t2);
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_i_set_car(((C_word*)t0)[4],t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k3258 */
static void C_ccall f_3260(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3260,c,av);}
t2=((C_word*)t0)[2];
f_3219(t2,C_i_assq(C_i_car(t1),((C_word*)((C_word*)t0)[3])[1]));}

/* add-boxed in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_3268(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,4)))){
C_save_and_reclaim_args((void *)trf_3268,3,t0,t1,t2);}
/* lfa2.scm:343: count-floatvar */
t3=((C_word*)((C_word*)t0)[2])[1];
f_3212(t3,t1,t2,*((C_word*)lf[30]+1),C_SCHEME_END_OF_LIST);}

/* add-unboxed in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_3274(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,4)))){
C_save_and_reclaim_args((void *)trf_3274,3,t0,t1,t2);}
/* lfa2.scm:344: count-floatvar */
t3=((C_word*)((C_word*)t0)[2])[1];
f_3212(t3,t1,t2,*((C_word*)lf[31]+1),C_SCHEME_END_OF_LIST);}

/* sub-boxed in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_3280(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,0,4)))){
C_save_and_reclaim_args((void *)trf_3280,3,t0,t1,t2);}
a=C_alloc(3);
/* lfa2.scm:345: count-floatvar */
t3=((C_word*)((C_word*)t0)[2])[1];
f_3212(t3,t1,t2,*((C_word*)lf[30]+1),C_a_i_list(&a,1,C_fix(-1)));}

/* walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_3286(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,0,2)))){
C_save_and_reclaim_args((void *)trf_3286,5,t0,t1,t2,t3,t4);}
a=C_alloc(15);
t5=(*a=C_CLOSURE_TYPE|14,a[1]=(C_word)f_3290,a[2]=t3,a[3]=t1,a[4]=t4,a[5]=((C_word*)t0)[2],a[6]=t2,a[7]=((C_word*)t0)[3],a[8]=((C_word*)t0)[4],a[9]=((C_word*)t0)[5],a[10]=((C_word*)t0)[6],a[11]=((C_word*)t0)[7],a[12]=((C_word*)t0)[8],a[13]=((C_word*)t0)[9],a[14]=((C_word*)t0)[10],tmp=(C_word)a,a+=15,tmp);
/* lfa2.scm:348: chicken.compiler.support#node-class */
t6=*((C_word*)lf[16]+1);{
C_word av2[3];
av2[0]=t6;
av2[1]=t5;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3290(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,2)))){
C_save_and_reclaim((void *)f_3290,c,av);}
a=C_alloc(16);
t2=(*a=C_CLOSURE_TYPE|15,a[1]=(C_word)f_3293,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],a[13]=((C_word*)t0)[12],a[14]=((C_word*)t0)[13],a[15]=((C_word*)t0)[14],tmp=(C_word)a,a+=16,tmp);
/* lfa2.scm:349: chicken.compiler.support#node-parameters */
t3=*((C_word*)lf[15]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3293(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(17,c,2)))){
C_save_and_reclaim((void *)f_3293,c,av);}
a=C_alloc(17);
t2=(*a=C_CLOSURE_TYPE|16,a[1]=(C_word)f_3296,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],a[13]=((C_word*)t0)[12],a[14]=((C_word*)t0)[13],a[15]=((C_word*)t0)[14],a[16]=((C_word*)t0)[15],tmp=(C_word)a,a+=17,tmp);
/* lfa2.scm:350: chicken.compiler.support#node-subexpressions */
t3=*((C_word*)lf[28]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3296(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(23,c,4)))){
C_save_and_reclaim((void *)f_3296,c,av);}
a=C_alloc(23);
t2=C_eqp(((C_word*)t0)[2],lf[11]);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3305,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],tmp=(C_word)a,a+=8,tmp);
t4=(
/* lfa2.scm:353: floatvar? */
  f_3189(((C_word*)((C_word*)t0)[9])[1],C_i_car(((C_word*)t0)[3]))
);
if(C_truep(t4)){
t5=C_i_car(((C_word*)t0)[3]);
if(C_truep(C_i_not(C_i_assq(t5,((C_word*)t0)[4])))){
/* lfa2.scm:355: eliminate-floatvar */
t6=((C_word*)((C_word*)t0)[10])[1];
f_3195(t6,t3,C_i_car(((C_word*)t0)[3]));}
else{
t6=t3;{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_SCHEME_UNDEFINED;
f_3305(2,av2);}}}
else{
t5=t3;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_UNDEFINED;
f_3305(2,av2);}}}
else{
t3=C_eqp(((C_word*)t0)[2],lf[32]);
t4=(C_truep(t3)?t3:C_eqp(((C_word*)t0)[2],lf[33]));
if(C_truep(t4)){
t5=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3355,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[11],a[4]=((C_word*)t0)[12],a[5]=t1,a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* lfa2.scm:359: walk */
t6=((C_word*)((C_word*)t0)[12])[1];
f_3286(t6,t5,C_i_car(t1),((C_word*)t0)[4],((C_word*)t0)[6]);}
else{
t5=C_eqp(((C_word*)t0)[2],lf[36]);
if(C_truep(t5)){
t6=C_i_car(((C_word*)t0)[3]);
if(C_truep(C_i_stringp(t6))){
t7=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t7;
av2[1]=lf[37];
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}
else{
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2764,a[2]=((C_word*)t0)[5],a[3]=t6,tmp=(C_word)a,a+=4,tmp);
/* lfa2.scm:242: chicken.keyword#keyword? */
t8=*((C_word*)lf[54]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t8;
av2[1]=t7;
av2[2]=t6;
((C_proc)(void*)(*((C_word*)t8+1)))(3,av2);}}}
else{
t6=C_eqp(((C_word*)t0)[2],lf[55]);
if(C_truep(t6)){
t7=C_i_car(t1);
t8=C_i_car(((C_word*)t0)[3]);
t9=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_3455,a[2]=t1,a[3]=t8,a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[12],a[6]=((C_word*)t0)[5],a[7]=t7,a[8]=((C_word*)t0)[13],a[9]=((C_word*)t0)[4],a[10]=((C_word*)t0)[14],a[11]=((C_word*)t0)[15],a[12]=((C_word*)t0)[7],a[13]=((C_word*)t0)[9],tmp=(C_word)a,a+=14,tmp);
/* lfa2.scm:373: walk */
t10=((C_word*)((C_word*)t0)[12])[1];
f_3286(t10,t9,t7,((C_word*)t0)[4],((C_word*)t0)[6]);}
else{
t7=C_eqp(((C_word*)t0)[2],lf[56]);
t8=(C_truep(t7)?t7:C_eqp(((C_word*)t0)[2],lf[57]));
if(C_truep(t8)){
t9=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3565,a[2]=((C_word*)t0)[5],tmp=(C_word)a,a+=3,tmp);
/* lfa2.scm:395: walk */
t10=((C_word*)((C_word*)t0)[12])[1];
f_3286(t10,t9,C_i_car(t1),C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST);}
else{
t9=C_eqp(((C_word*)t0)[2],lf[59]);
t10=(C_truep(t9)?t9:C_eqp(((C_word*)t0)[2],lf[60]));
if(C_truep(t10)){
t11=C_i_car(t1);
t12=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3584,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[12],a[4]=t11,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
t13=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_3590,a[2]=((C_word*)t0)[10],a[3]=t12,a[4]=t11,a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[12],a[7]=((C_word*)t0)[4],a[8]=((C_word*)t0)[6],tmp=(C_word)a,a+=9,tmp);
t14=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f5481,a[2]=t13,a[3]=((C_word*)t0)[9],a[4]=t11,tmp=(C_word)a,a+=5,tmp);
/* lfa2.scm:327: chicken.compiler.support#node-class */
t15=*((C_word*)lf[16]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t15;
av2[1]=t14;
av2[2]=t11;
((C_proc)(void*)(*((C_word*)t15+1)))(3,av2);}}
else{
t11=C_eqp(((C_word*)t0)[2],lf[20]);
if(C_truep(t11)){
t12=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t12;
av2[1]=lf[61];
((C_proc)(void*)(*((C_word*)t12+1)))(2,av2);}}
else{
t12=C_eqp(((C_word*)t0)[2],lf[62]);
if(C_truep(t12)){
t13=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t13;
av2[1]=lf[58];
((C_proc)(void*)(*((C_word*)t13+1)))(2,av2);}}
else{
t13=C_eqp(((C_word*)t0)[2],lf[63]);
t14=(C_truep(t13)?t13:C_eqp(((C_word*)t0)[2],lf[64]));
if(C_truep(t14)){
t15=C_i_car(((C_word*)t0)[3]);
t16=C_i_assoc(t15,lf[3]);
t17=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3640,a[2]=t16,a[3]=((C_word*)t0)[15],a[4]=((C_word*)t0)[12],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
t18=C_i_check_list_2(t1,lf[21]);
t19=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_3656,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[16],a[4]=((C_word*)t0)[8],a[5]=t1,a[6]=((C_word*)t0)[12],a[7]=((C_word*)t0)[4],a[8]=((C_word*)t0)[6],a[9]=((C_word*)t0)[5],tmp=(C_word)a,a+=10,tmp);
t20=C_SCHEME_UNDEFINED;
t21=(*a=C_VECTOR_TYPE|1,a[1]=t20,tmp=(C_word)a,a+=2,tmp);
t22=C_set_block_item(t21,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4110,a[2]=t21,a[3]=t17,tmp=(C_word)a,a+=4,tmp));
t23=((C_word*)t21)[1];
f_4110(t23,t19,t1);}
else{
t15=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4136,a[2]=((C_word*)t0)[12],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[6],tmp=(C_word)a,a+=5,tmp);
t16=C_i_check_list_2(t1,lf[21]);
t17=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4146,a[2]=((C_word*)t0)[5],tmp=(C_word)a,a+=3,tmp);
t18=C_SCHEME_UNDEFINED;
t19=(*a=C_VECTOR_TYPE|1,a[1]=t18,tmp=(C_word)a,a+=2,tmp);
t20=C_set_block_item(t19,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4148,a[2]=t19,a[3]=t15,tmp=(C_word)a,a+=4,tmp));
t21=((C_word*)t19)[1];
f_4148(t21,t17,t1);}}}}}}}}}}

/* k3303 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3305(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_3305,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3308,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* lfa2.scm:356: add-boxed */
t3=((C_word*)((C_word*)t0)[6])[1];
f_3268(t3,t2,((C_word*)t0)[7]);}

/* k3306 in k3303 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3308(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_3308,c,av);}
a=C_alloc(4);
t2=C_i_car(((C_word*)t0)[2]);
t3=C_i_assq(t2,((C_word*)t0)[3]);
if(C_truep(t3)){
t4=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_i_cdr(t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3143,a[2]=t2,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t5=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t5;
av2[1]=(
  f_3143(t4,((C_word*)t0)[5])
);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* k3353 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3355(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(23,c,3)))){
C_save_and_reclaim((void *)f_3355,c,av);}
a=C_alloc(23);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3358,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
t3=C_i_pairp(t1);
t4=(C_truep(t3)?C_eqp(lf[34],C_u_i_car(t1)):C_SCHEME_FALSE);
if(C_truep(t4)){
t5=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_3387,a[2]=t2,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[7],a[7]=t1,a[8]=((C_word*)t0)[6],tmp=(C_word)a,a+=9,tmp);
t6=C_i_cadr(((C_word*)t0)[5]);
t7=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3411,a[2]=((C_word*)t0)[4],a[3]=t5,a[4]=t6,a[5]=((C_word*)t0)[7],tmp=(C_word)a,a+=6,tmp);
/* lfa2.scm:362: scheme#append */
t8=*((C_word*)lf[35]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t8;
av2[1]=t7;
av2[2]=C_i_cadr(t1);
av2[3]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t8+1)))(4,av2);}}
else{
t5=t2;
f_3358(t5,C_SCHEME_UNDEFINED);}}

/* k3356 in k3353 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_3358(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,4)))){
C_save_and_reclaim_args((void *)trf_3358,2,t0,t1);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3365,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
/* lfa2.scm:367: walk */
t3=((C_word*)((C_word*)t0)[4])[1];
f_3286(t3,t2,C_i_cadr(((C_word*)t0)[5]),((C_word*)t0)[6],((C_word*)t0)[7]);}

/* k3363 in k3356 in k3353 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3365(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_3365,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3369,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* lfa2.scm:368: walk */
t3=((C_word*)((C_word*)t0)[4])[1];
f_3286(t3,t2,C_i_caddr(((C_word*)t0)[5]),((C_word*)t0)[6],((C_word*)t0)[7]);}

/* k3367 in k3363 in k3356 in k3353 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3369(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3369,c,av);}
/* lfa2.scm:367: merge */
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(
/* lfa2.scm:367: merge */
  f_2879(((C_word*)t0)[4],t1)
);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k3385 in k3353 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3387(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,3)))){
C_save_and_reclaim((void *)f_3387,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3391,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t3=C_i_caddr(((C_word*)t0)[4]);
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3399,a[2]=((C_word*)t0)[5],a[3]=t2,a[4]=t3,a[5]=((C_word*)t0)[6],tmp=(C_word)a,a+=6,tmp);
/* lfa2.scm:365: scheme#append */
t5=*((C_word*)lf[35]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=C_i_caddr(((C_word*)t0)[7]);
av2[3]=((C_word*)t0)[8];
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k3389 in k3385 in k3353 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3391(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3391,c,av);}
/* lfa2.scm:361: merge */
t2=((C_word*)t0)[2];
f_3358(t2,(
/* lfa2.scm:361: merge */
  f_2879(((C_word*)t0)[4],t1)
));}

/* k3397 in k3385 in k3353 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3399(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_3399,c,av);}
/* lfa2.scm:364: walk */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3286(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1,((C_word*)t0)[5]);}

/* k3409 in k3353 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3411(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_3411,c,av);}
/* lfa2.scm:361: walk */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3286(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1,((C_word*)t0)[5]);}

/* k3453 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3455(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,2)))){
C_save_and_reclaim((void *)f_3455,c,av);}
a=C_alloc(15);
t2=(*a=C_CLOSURE_TYPE|14,a[1]=(C_word)f_3458,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=t1,a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],a[13]=((C_word*)t0)[12],a[14]=((C_word*)t0)[13],tmp=(C_word)a,a+=15,tmp);
/* lfa2.scm:374: assigned? */
t3=((C_word*)((C_word*)t0)[8])[1];
f_2956(t3,t2,((C_word*)t0)[3]);}

/* k3456 in k3453 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3458(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(20,c,2)))){
C_save_and_reclaim((void *)f_3458,c,av);}
a=C_alloc(20);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_3461,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=t1,a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],tmp=(C_word)a,a+=12,tmp);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3523,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[11],a[4]=((C_word*)t0)[12],a[5]=t2,a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[13],tmp=(C_word)a,a+=8,tmp);
if(C_truep(C_i_not(t1))){
t4=C_eqp(lf[43],((C_word*)t0)[10]);
if(C_truep(t4)){
t5=(
/* lfa2.scm:377: floatvar? */
  f_3189(((C_word*)((C_word*)t0)[14])[1],((C_word*)t0)[3])
);
t6=t3;
f_3523(t6,C_i_not(t5));}
else{
t5=t3;
f_3523(t5,C_SCHEME_FALSE);}}
else{
t4=t3;
f_3523(t4,C_SCHEME_FALSE);}}

/* k3459 in k3456 in k3453 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3461(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,2)))){
C_save_and_reclaim((void *)f_3461,c,av);}
a=C_alloc(16);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_3472,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=t2,a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],tmp=(C_word)a,a+=10,tmp);
if(C_truep(((C_word*)t0)[8])){
t4=t3;
f_3472(t4,((C_word*)t0)[10]);}
else{
t4=C_a_i_cons(&a,2,((C_word*)t0)[3],((C_word*)t0)[11]);
t5=t3;
f_3472(t5,C_a_i_cons(&a,2,t4,((C_word*)t0)[10]));}}

/* k3470 in k3459 in k3456 in k3453 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_3472(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,0,2)))){
C_save_and_reclaim_args((void *)trf_3472,2,t0,t1);}
a=C_alloc(15);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_3479,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t1,a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f5469,a[2]=((C_word*)t0)[8],a[3]=t2,a[4]=((C_word*)t0)[9],a[5]=((C_word*)t0)[7],tmp=(C_word)a,a+=6,tmp);
/* lfa2.scm:327: chicken.compiler.support#node-class */
t4=*((C_word*)lf[16]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k3477 in k3470 in k3459 in k3456 in k3453 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_3479(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,4)))){
C_save_and_reclaim_args((void *)trf_3479,2,t0,t1);}
a=C_alloc(8);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3493,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
/* lfa2.scm:388: chicken.compiler.support#node-parameters */
t3=*((C_word*)lf[15]+1);{
C_word av2[3];
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[8];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
/* lfa2.scm:381: walk */
t2=((C_word*)((C_word*)t0)[4])[1];
f_3286(t2,((C_word*)t0)[5],((C_word*)t0)[6],((C_word*)t0)[7],((C_word*)t0)[3]);}}

/* k3491 in k3477 in k3470 in k3459 in k3456 in k3453 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3493(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,4)))){
C_save_and_reclaim((void *)f_3493,c,av);}
a=C_alloc(12);
t2=C_i_car(t1);
t3=C_a_i_cons(&a,2,t2,((C_word*)t0)[2]);
t4=C_a_i_cons(&a,2,t3,((C_word*)t0)[3]);
t5=C_a_i_cons(&a,2,((C_word*)t0)[2],t2);
t6=C_a_i_cons(&a,2,t5,t4);
/* lfa2.scm:381: walk */
t7=((C_word*)((C_word*)t0)[4])[1];
f_3286(t7,((C_word*)t0)[5],((C_word*)t0)[6],((C_word*)t0)[7],t6);}

/* k3507 */
static void C_ccall f_3509(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3509,c,av);}
t2=((C_word*)t0)[2];
f_3479(t2,C_i_not(t1));}

/* k3515 */
static void C_ccall f_3517(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_3517,c,av);}
/* lfa2.scm:387: assigned? */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2956(t2,((C_word*)t0)[3],C_i_car(t1));}

/* k3521 in k3456 in k3453 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_3523(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,2)))){
C_save_and_reclaim_args((void *)trf_3523,2,t0,t1);}
a=C_alloc(12);
if(C_truep(t1)){
t2=C_a_i_list3(&a,3,((C_word*)t0)[2],C_fix(0),C_fix(0));
t3=C_a_i_cons(&a,2,t2,((C_word*)((C_word*)t0)[3])[1]);
t4=C_mutate(((C_word *)((C_word*)t0)[3])+1,t3);
/* lfa2.scm:379: add-unboxed */
t5=((C_word*)((C_word*)t0)[4])[1];
f_3274(t5,((C_word*)t0)[5],((C_word*)t0)[6]);}
else{
/* lfa2.scm:380: add-boxed */
t2=((C_word*)((C_word*)t0)[7])[1];
f_3268(t2,((C_word*)t0)[5],((C_word*)t0)[6]);}}

/* k3563 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3565(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3565,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=lf[58];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k3582 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3584(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_3584,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3587,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* lfa2.scm:402: walk */
t3=((C_word*)((C_word*)t0)[3])[1];
f_3286(t3,t2,((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6]);}

/* k3585 in k3582 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3587(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3587,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=lf[61];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k3588 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_3590(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,4)))){
C_save_and_reclaim_args((void *)trf_3590,2,t0,t1);}
a=C_alloc(4);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3601,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* lfa2.scm:401: chicken.compiler.support#node-parameters */
t3=*((C_word*)lf[15]+1);{
C_word av2[3];
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f5390,a[2]=((C_word*)t0)[5],tmp=(C_word)a,a+=3,tmp);
/* lfa2.scm:402: walk */
t3=((C_word*)((C_word*)t0)[6])[1];
f_3286(t3,t2,((C_word*)t0)[4],((C_word*)t0)[7],((C_word*)t0)[8]);}}

/* k3599 in k3588 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3601(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_3601,c,av);}
/* lfa2.scm:401: eliminate-floatvar */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3195(t2,((C_word*)t0)[3],C_i_car(t1));}

/* k3613 */
static void C_ccall f_3615(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3615,c,av);}
/* lfa2.scm:400: floatvar? */
t2=((C_word*)t0)[2];
f_3590(t2,(
/* lfa2.scm:400: floatvar? */
  f_3189(((C_word*)((C_word*)t0)[3])[1],C_i_car(t1))
));}

/* g737 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_3640(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,4)))){
C_save_and_reclaim_args((void *)trf_3640,3,t0,t1,t2);}
a=C_alloc(6);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3644,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* lfa2.scm:410: walk */
t4=((C_word*)((C_word*)t0)[4])[1];
f_3286(t4,t3,t2,((C_word*)t0)[5],((C_word*)t0)[6]);}

/* k3642 in g737 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3644(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_3644,c,av);}
if(C_truep(((C_word*)t0)[2])){
/* lfa2.scm:411: add-unboxed */
t2=((C_word*)((C_word*)t0)[3])[1];
f_3274(t2,((C_word*)t0)[4],((C_word*)t0)[5]);}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k3654 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3656(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_3656,c,av);}
a=C_alloc(8);
t2=C_i_car(((C_word*)t0)[2]);
t3=C_i_assoc(t2,lf[65]);
if(C_truep(t3)){
t4=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3663,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],tmp=(C_word)a,a+=8,tmp);
/* lfa2.scm:413: g759 */
t5=t4;
f_3663(t5,((C_word*)t0)[9],t3);}
else{
t4=C_i_car(((C_word*)t0)[2]);
t5=C_i_assoc(t4,lf[72]);
if(C_truep(t5)){
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3793,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[7],a[6]=((C_word*)t0)[8],tmp=(C_word)a,a+=7,tmp);
/* lfa2.scm:413: g774 */
t7=t6;
f_3793(t7,((C_word*)t0)[9],t5);}
else{
t6=C_i_car(((C_word*)t0)[2]);
t7=C_i_assoc(t6,lf[73]);
if(C_truep(t7)){
t8=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3841,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],tmp=(C_word)a,a+=8,tmp);
/* lfa2.scm:413: g783 */
t9=t8;
f_3841(t9,((C_word*)t0)[9],t7);}
else{
t8=C_i_car(((C_word*)t0)[2]);
t9=C_i_assoc(t8,lf[77]);
if(C_truep(t9)){
t10=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4037,a[2]=((C_word*)t0)[5],tmp=(C_word)a,a+=3,tmp);
/* lfa2.scm:413: g810 */
t11=t10;
f_4037(t11,((C_word*)t0)[9],t9);}
else{
t10=C_SCHEME_UNDEFINED;
t11=((C_word*)t0)[9];{
C_word *av2=av;
av2[0]=t11;
av2[1]=t10;
((C_proc)(void*)(*((C_word*)t11+1)))(2,av2);}}}}}}

/* g759 in k3654 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_3663(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,4)))){
C_save_and_reclaim_args((void *)trf_3663,3,t0,t1,t2);}
a=C_alloc(7);
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3667,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=t2,a[6]=((C_word*)t0)[4],tmp=(C_word)a,a+=7,tmp);
/* lfa2.scm:415: walk */
t4=((C_word*)((C_word*)t0)[5])[1];
f_3286(t4,t3,C_i_car(((C_word*)t0)[4]),((C_word*)t0)[6],((C_word*)t0)[7]);}

/* k3665 in g759 in k3654 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3667(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,3)))){
C_save_and_reclaim((void *)f_3667,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3670,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(*((C_word*)lf[66]+1))){
/* lfa2.scm:417: extinguish! */
t3=((C_word*)((C_word*)t0)[3])[1];
f_3018(t3,t2,((C_word*)t0)[4],lf[67]);}
else{
t3=C_i_cadr(((C_word*)t0)[5]);
t4=C_eqp(lf[68],t3);
if(C_truep(t4)){
t5=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3685,a[2]=t1,a[3]=((C_word*)t0)[3],a[4]=t2,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[2],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
if(C_truep(C_i_pairp(t1))){
t6=C_i_car(t1);
t7=C_eqp(lf[6],t6);
if(C_truep(t7)){
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3731,a[2]=t5,tmp=(C_word)a,a+=3,tmp);
/* lfa2.scm:422: chicken.compiler.support#node-class */
t9=*((C_word*)lf[16]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t9;
av2[1]=t8;
av2[2]=C_i_cadr(((C_word*)t0)[6]);
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}
else{
t8=t5;
f_3685(t8,C_SCHEME_FALSE);}}
else{
t6=t5;
f_3685(t6,C_SCHEME_FALSE);}}
else{
t5=C_i_pairp(t1);
t6=(C_truep(t5)?C_eqp(lf[34],C_u_i_car(t1)):C_SCHEME_FALSE);
if(C_truep(t6)){
if(C_truep(C_i_memq(lf[34],C_u_i_cdr(((C_word*)t0)[5])))){
/* lfa2.scm:429: extinguish! */
t7=((C_word*)((C_word*)t0)[3])[1];
f_3018(t7,t2,((C_word*)t0)[4],lf[70]);}
else{
t7=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t7;
av2[1]=lf[7];
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}}
else{
if(C_truep(C_i_member(t1,C_u_i_cdr(((C_word*)t0)[5])))){
/* lfa2.scm:432: extinguish! */
t7=((C_word*)((C_word*)t0)[3])[1];
f_3018(t7,t2,((C_word*)t0)[4],lf[71]);}
else{
t7=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t7;
av2[1]=lf[7];
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}}}}}

/* k3668 in k3665 in g759 in k3654 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3670(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3670,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=lf[7];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k3683 in k3665 in g759 in k3654 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_3685(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,2)))){
C_save_and_reclaim_args((void *)trf_3685,2,t0,t1);}
a=C_alloc(7);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3711,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* lfa2.scm:423: chicken.compiler.support#node-parameters */
t3=*((C_word*)lf[15]+1);{
C_word av2[3];
av2[0]=t3;
av2[1]=t2;
av2[2]=C_i_cadr(((C_word*)t0)[7]);
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t2=((C_word*)t0)[6];{
C_word av2[2];
av2[0]=t2;
av2[1]=lf[7];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k3709 in k3683 in k3665 in g759 in k3654 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3711(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3711,c,av);}
t2=C_i_car(t1);
if(C_truep(C_i_symbolp(t2))){
t3=C_i_cadr(((C_word*)t0)[2]);
t4=C_eqp(t2,t3);
if(C_truep(t4)){
/* lfa2.scm:426: extinguish! */
t5=((C_word*)((C_word*)t0)[3])[1];
f_3018(t5,((C_word*)t0)[4],((C_word*)t0)[5],lf[69]);}
else{
t5=((C_word*)t0)[6];{
C_word *av2=av;
av2[0]=t5;
av2[1]=lf[7];
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}
else{
t3=((C_word*)t0)[6];{
C_word *av2=av;
av2[0]=t3;
av2[1]=lf[7];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k3729 in k3665 in g759 in k3654 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3731(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3731,c,av);}
t2=((C_word*)t0)[2];
f_3685(t2,C_eqp(lf[36],t1));}

/* g774 in k3654 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_3793(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,4)))){
C_save_and_reclaim_args((void *)trf_3793,3,t0,t1,t2);}
a=C_alloc(6);
t3=C_i_car(((C_word*)t0)[2]);
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3800,a[2]=t1,a[3]=t2,a[4]=((C_word*)t0)[3],a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* lfa2.scm:437: walk */
t5=((C_word*)((C_word*)t0)[4])[1];
f_3286(t5,t4,t3,((C_word*)t0)[5],((C_word*)t0)[6]);}

/* k3798 in g774 in k3654 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3800(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_3800,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3803,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=C_i_cdr(((C_word*)t0)[3]);
if(C_truep(C_i_member(t1,t3))){
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3809,a[2]=t2,a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3827,a[2]=t4,a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
/* lfa2.scm:439: chicken.compiler.support#node-class */
t6=*((C_word*)lf[16]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}
else{
t4=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k3801 in k3798 in g774 in k3654 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3803(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3803,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k3807 in k3798 in g774 in k3654 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3809(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_3809,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3812,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3823,a[2]=t2,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* lfa2.scm:440: chicken.compiler.support#node-parameters */
t4=*((C_word*)lf[15]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k3810 in k3807 in k3798 in g774 in k3654 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3812(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_3812,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3819,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* lfa2.scm:441: chicken.compiler.support#node-subexpressions */
t3=*((C_word*)lf[28]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k3817 in k3810 in k3807 in k3798 in g774 in k3654 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3819(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3819,c,av);}
/* lfa2.scm:441: chicken.compiler.support#node-subexpressions-set! */
t2=*((C_word*)lf[17]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k3821 in k3807 in k3798 in g774 in k3654 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3823(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3823,c,av);}
/* lfa2.scm:440: chicken.compiler.support#node-parameters-set! */
t2=*((C_word*)lf[18]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k3825 in k3798 in g774 in k3654 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_3827(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3827,c,av);}
/* lfa2.scm:439: chicken.compiler.support#node-class-set! */
t2=*((C_word*)lf[19]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* g783 in k3654 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_3841(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,0,2)))){
C_save_and_reclaim_args((void *)trf_3841,3,t0,t1,t2);}
a=C_alloc(11);
t3=C_i_car(((C_word*)t0)[2]);
t4=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f5488,a[2]=t2,a[3]=t1,a[4]=((C_word*)t0)[2],a[5]=t3,a[6]=((C_word*)t0)[3],a[7]=((C_word*)t0)[4],a[8]=((C_word*)t0)[5],a[9]=((C_word*)t0)[6],a[10]=((C_word*)t0)[7],tmp=(C_word)a,a+=11,tmp);
/* lfa2.scm:327: chicken.compiler.support#node-class */
t5=*((C_word*)lf[16]+1);{
C_word av2[3];
av2[0]=t5;
av2[1]=t4;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k3893 in k3901 in k3916 */
static void C_ccall f_3895(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(21,c,1)))){
C_save_and_reclaim((void *)f_3895,c,av);}
a=C_alloc(21);
t2=C_i_car(t1);
if(C_truep(C_i_symbolp(t2))){
t3=C_a_i_list(&a,2,lf[6],t2);
t4=C_a_i_cons(&a,2,((C_word*)t0)[2],t3);
t5=C_a_i_list(&a,1,t4);
t6=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_a_i_list(&a,3,lf[34],t5,C_SCHEME_END_OF_LIST);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
t3=C_a_i_cons(&a,2,((C_word*)t0)[2],lf[6]);
t4=C_a_i_list(&a,1,t3);
t5=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_a_i_list(&a,3,lf[34],t4,C_SCHEME_END_OF_LIST);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* k3901 in k3916 */
static void C_ccall f_3903(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,2)))){
C_save_and_reclaim((void *)f_3903,c,av);}
a=C_alloc(15);
t2=C_eqp(lf[36],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3895,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* lfa2.scm:455: chicken.compiler.support#node-parameters */
t4=*((C_word*)lf[15]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_i_cadr(((C_word*)t0)[4]);
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=C_a_i_cons(&a,2,((C_word*)t0)[2],lf[6]);
t4=C_a_i_list(&a,1,t3);
t5=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_a_i_list(&a,3,lf[34],t4,C_SCHEME_END_OF_LIST);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* k3916 */
static void C_ccall f_3918(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,2)))){
C_save_and_reclaim((void *)f_3918,c,av);}
a=C_alloc(15);
t2=C_i_car(t1);
t3=C_i_cadr(((C_word*)t0)[2]);
t4=C_eqp(lf[68],t3);
if(C_truep(t4)){
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3903,a[2]=t2,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* lfa2.scm:453: chicken.compiler.support#node-class */
t6=*((C_word*)lf[16]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=C_i_cadr(((C_word*)t0)[4]);
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}
else{
t5=C_i_cadr(((C_word*)t0)[2]);
t6=C_a_i_cons(&a,2,t2,t5);
t7=C_a_i_list(&a,1,t6);
t8=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t8;
av2[1]=C_a_i_list(&a,3,lf[34],t7,C_SCHEME_END_OF_LIST);
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}}

/* k3919 */
static void C_ccall f_3921(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,3)))){
C_save_and_reclaim((void *)f_3921,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3924,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=C_i_cadr(((C_word*)t0)[3]);
t4=C_eqp(lf[68],t3);
if(C_truep(t4)){
t5=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3933,a[2]=t1,a[3]=((C_word*)t0)[4],a[4]=t2,a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[2],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
if(C_truep(C_i_pairp(t1))){
t6=C_i_car(t1);
t7=C_eqp(lf[6],t6);
if(C_truep(t7)){
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3979,a[2]=t5,tmp=(C_word)a,a+=3,tmp);
/* lfa2.scm:469: chicken.compiler.support#node-class */
t9=*((C_word*)lf[16]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t9;
av2[1]=t8;
av2[2]=C_i_cadr(((C_word*)t0)[6]);
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}
else{
t8=t5;
f_3933(t8,C_SCHEME_FALSE);}}
else{
t6=t5;
f_3933(t6,C_SCHEME_FALSE);}}
else{
t5=C_i_pairp(t1);
t6=(C_truep(t5)?C_eqp(lf[34],C_u_i_car(t1)):C_SCHEME_FALSE);
if(C_truep(t6)){
if(C_truep(C_i_memq(lf[34],C_u_i_cdr(((C_word*)t0)[3])))){
/* lfa2.scm:477: extinguish! */
t7=((C_word*)((C_word*)t0)[4])[1];
f_3018(t7,t2,((C_word*)t0)[5],lf[75]);}
else{
t7=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t7;
av2[1]=lf[34];
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}}
else{
if(C_truep(C_i_member(t1,C_u_i_cdr(((C_word*)t0)[3])))){
/* lfa2.scm:480: extinguish! */
t7=((C_word*)((C_word*)t0)[4])[1];
f_3018(t7,t2,((C_word*)t0)[5],lf[76]);}
else{
t7=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t7;
av2[1]=lf[34];
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}}}}

/* k3922 in k3919 */
static void C_ccall f_3924(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3924,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=lf[34];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k3931 in k3919 */
static void C_fcall f_3933(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,2)))){
C_save_and_reclaim_args((void *)trf_3933,2,t0,t1);}
a=C_alloc(7);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3959,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* lfa2.scm:471: chicken.compiler.support#node-parameters */
t3=*((C_word*)lf[15]+1);{
C_word av2[3];
av2[0]=t3;
av2[1]=t2;
av2[2]=C_i_cadr(((C_word*)t0)[7]);
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t2=((C_word*)t0)[6];{
C_word av2[2];
av2[0]=t2;
av2[1]=lf[34];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k3957 in k3931 in k3919 */
static void C_ccall f_3959(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3959,c,av);}
t2=C_i_car(t1);
if(C_truep(C_i_symbolp(t2))){
t3=C_i_cadr(((C_word*)t0)[2]);
t4=C_eqp(t2,t3);
if(C_truep(t4)){
/* lfa2.scm:474: extinguish! */
t5=((C_word*)((C_word*)t0)[3])[1];
f_3018(t5,((C_word*)t0)[4],((C_word*)t0)[5],lf[74]);}
else{
t5=((C_word*)t0)[6];{
C_word *av2=av;
av2[0]=t5;
av2[1]=lf[34];
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}
else{
t3=((C_word*)t0)[6];{
C_word *av2=av;
av2[0]=t3;
av2[1]=lf[34];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k3977 in k3919 */
static void C_ccall f_3979(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3979,c,av);}
t2=((C_word*)t0)[2];
f_3933(t2,C_eqp(lf[36],t1));}

/* g810 in k3654 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_4037(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_4037,3,t0,t1,t2);}
a=C_alloc(8);
t3=C_i_pairp(((C_word*)t0)[2]);
t4=(C_truep(t3)?C_i_car(((C_word*)t0)[2]):C_SCHEME_FALSE);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4047,a[2]=t1,a[3]=t4,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
if(C_truep(t4)){
t6=C_i_cadr(t2);
t7=C_eqp(lf[68],t6);
if(C_truep(t7)){
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4079,a[2]=t5,tmp=(C_word)a,a+=3,tmp);
/* lfa2.scm:487: chicken.compiler.support#node-class */
t9=*((C_word*)lf[16]+1);{
C_word av2[3];
av2[0]=t9;
av2[1]=t8;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}
else{
t8=t5;
f_4047(t8,C_SCHEME_FALSE);}}
else{
t6=t5;
f_4047(t6,C_SCHEME_FALSE);}}

/* k4045 in g810 in k3654 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_4047(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_4047,2,t0,t1);}
a=C_alloc(3);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4063,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* lfa2.scm:488: chicken.compiler.support#node-parameters */
t3=*((C_word*)lf[15]+1);{
C_word av2[3];
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t2;
av2[1]=C_i_cadr(((C_word*)t0)[4]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k4061 in k4045 in g810 in k3654 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4063(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_4063,c,av);}
a=C_alloc(6);
t2=C_i_car(t1);
t3=C_i_symbolp(t2);
t4=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t4;
av2[1]=(C_truep(t3)?C_a_i_list(&a,2,lf[6],t2):lf[6]);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k4077 in g810 in k3654 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4079(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4079,c,av);}
t2=((C_word*)t0)[2];
f_4047(t2,C_eqp(lf[36],t1));}

/* for-each-loop736 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_4110(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_4110,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4120,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* lfa2.scm:408: g737 */
t4=((C_word*)t0)[3];
f_3640(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k4118 in for-each-loop736 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4120(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4120,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_4110(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* g825 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_4136(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,4)))){
C_save_and_reclaim_args((void *)trf_4136,3,t0,t1,t2);}
/* lfa2.scm:494: g840 */
t3=((C_word*)((C_word*)t0)[2])[1];
f_3286(t3,t1,t2,((C_word*)t0)[3],((C_word*)t0)[4]);}

/* k4144 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4146(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4146,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=lf[7];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* for-each-loop824 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_4148(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_4148,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4158,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* lfa2.scm:494: g825 */
t4=((C_word*)t0)[3];
f_4136(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k4156 in for-each-loop824 in k3294 in k3291 in k3288 in walk in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4158(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4158,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_4148(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k4208 in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4210(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_4210,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4213,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_i_pairp(((C_word*)((C_word*)t0)[4])[1]))){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4221,a[2]=((C_word*)t0)[4],tmp=(C_word)a,a+=3,tmp);
/* lfa2.scm:499: chicken.compiler.support#with-debugging-output */
t4=*((C_word*)lf[86]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=lf[87];
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)((C_word*)t0)[3])[1];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k4211 in k4208 in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4213(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4213,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)((C_word*)t0)[3])[1];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a4220 in k4208 in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4221(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_4221,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4225,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* lfa2.scm:502: chicken.base#print */
t3=*((C_word*)lf[84]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[85];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4223 in a4220 in k4208 in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4225(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_4225,c,av);}
a=C_alloc(5);
t2=((C_word*)((C_word*)t0)[2])[1];
t3=C_i_check_list_2(t2,lf[21]);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4259,a[2]=t5,tmp=(C_word)a,a+=3,tmp));
t7=((C_word*)t5)[1];
f_4259(t7,((C_word*)t0)[3],t2);}

/* k4231 in for-each-loop850 in k4223 in a4220 in k4208 in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4233(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_4233,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4236,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* lfa2.scm:504: ##sys#print */
t3=*((C_word*)lf[81]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_i_car(((C_word*)t0)[4]);
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4234 in k4231 in for-each-loop850 in k4223 in a4220 in k4208 in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4236(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_4236,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4239,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* lfa2.scm:504: ##sys#print */
t3=*((C_word*)lf[81]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[82];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4237 in k4234 in k4231 in for-each-loop850 in k4223 in a4220 in k4208 in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4239(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_4239,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4242,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* lfa2.scm:504: ##sys#print */
t3=*((C_word*)lf[81]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_u_i_cdr(((C_word*)t0)[4]);
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4240 in k4237 in k4234 in k4231 in for-each-loop850 in k4223 in a4220 in k4208 in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4242(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4242,c,av);}
/* lfa2.scm:504: ##sys#write-char-0 */
t2=*((C_word*)lf[80]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_make_character(10);
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* for-each-loop850 in k4223 in a4220 in k4208 in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_4259(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,4)))){
C_save_and_reclaim_args((void *)trf_4259,3,t0,t1,t2);}
a=C_alloc(10);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4269,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t4=C_slot(t2,C_fix(0));
t5=*((C_word*)lf[78]+1);
t6=*((C_word*)lf[78]+1);
t7=C_i_check_port_2(*((C_word*)lf[78]+1),C_fix(2),C_SCHEME_TRUE,lf[79]);
t8=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4233,a[2]=t3,a[3]=t5,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
/* lfa2.scm:504: ##sys#print */
t9=*((C_word*)lf[81]+1);{
C_word av2[5];
av2[0]=t9;
av2[1]=t8;
av2[2]=lf[83];
av2[3]=C_SCHEME_FALSE;
av2[4]=*((C_word*)lf[78]+1);
((C_proc)(void*)(*((C_word*)t9+1)))(5,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k4267 in for-each-loop850 in k4223 in a4220 in k4208 in chicken.compiler.lfa2#perform-secondary-flow-analysis in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4269(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4269,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_4259(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4282(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_4282,c,av);}
a=C_alloc(9);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4286,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t5=C_i_check_list_2(t3,lf[29]);
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2127,a[2]=t7,tmp=(C_word)a,a+=3,tmp));
t9=((C_word*)t7)[1];
f_2127(t9,t4,t3);}

/* k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4286(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(23,c,3)))){
C_save_and_reclaim((void *)f_4286,c,av);}
a=C_alloc(23);
t2=C_fix(0);
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4288,a[2]=t1,a[3]=t3,a[4]=t5,a[5]=t7,tmp=(C_word)a,a+=6,tmp));
t9=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4515,a[2]=t1,a[3]=t7,a[4]=t5,a[5]=t3,tmp=(C_word)a,a+=6,tmp));
t10=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4856,a[2]=((C_word*)t0)[2],a[3]=t3,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* lfa2.scm:590: walk */
t11=((C_word*)t7)[1];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t11;
av2[1]=t10;
av2[2]=((C_word*)t0)[3];
f_4515(3,av2);}}

/* walk/unbox in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4288(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_4288,c,av);}
a=C_alloc(8);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4292,a[2]=t1,a[3]=t2,a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[5],tmp=(C_word)a,a+=8,tmp);
/* lfa2.scm:518: chicken.compiler.support#node-class */
t4=*((C_word*)lf[16]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k4290 in walk/unbox in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4292(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_4292,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_4295,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
/* lfa2.scm:519: chicken.compiler.support#node-parameters */
t3=*((C_word*)lf[15]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4293 in k4290 in walk/unbox in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4295(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_4295,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_4298,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
/* lfa2.scm:520: chicken.compiler.support#node-subexpressions */
t3=*((C_word*)lf[28]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4296 in k4293 in k4290 in walk/unbox in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4298(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(21,c,4)))){
C_save_and_reclaim((void *)f_4298,c,av);}
a=C_alloc(21);
t2=C_eqp(((C_word*)t0)[2],lf[36]);
if(C_truep(t2)){
t3=C_i_car(((C_word*)t0)[3]);
if(C_truep(C_i_flonump(t3))){
t4=C_a_i_list1(&a,1,t3);
/* lfa2.scm:525: chicken.compiler.support#make-node */
t5=*((C_word*)lf[89]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=((C_word*)t0)[4];
av2[2]=lf[90];
av2[3]=t4;
av2[4]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}
else{
t4=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t4;
av2[1]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}
else{
t3=C_eqp(((C_word*)t0)[2],lf[11]);
if(C_truep(t3)){
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4326,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp);
/* lfa2.scm:528: posq */
f_2677(t4,C_i_car(((C_word*)t0)[3]),((C_word*)t0)[6]);}
else{
t4=C_eqp(((C_word*)t0)[2],lf[63]);
t5=(C_truep(t4)?t4:C_eqp(((C_word*)t0)[2],lf[64]));
if(C_truep(t5)){
t6=C_i_car(((C_word*)t0)[3]);
t7=C_i_assoc(t6,lf[3]);
if(C_truep(t7)){
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4363,a[2]=((C_word*)t0)[7],a[3]=((C_word*)t0)[8],a[4]=((C_word*)t0)[9],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
/* lfa2.scm:533: g924 */
t9=t8;
f_4363(t9,((C_word*)t0)[4],t7);}
else{
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4443,a[2]=((C_word*)t0)[4],tmp=(C_word)a,a+=3,tmp);
t9=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t10=t9;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=((C_word*)t11)[1];
t13=C_i_check_list_2(t1,lf[94]);
t14=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4453,a[2]=t8,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
t15=C_SCHEME_UNDEFINED;
t16=(*a=C_VECTOR_TYPE|1,a[1]=t15,tmp=(C_word)a,a+=2,tmp);
t17=C_set_block_item(t16,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4455,a[2]=t11,a[3]=t16,a[4]=((C_word*)t0)[9],a[5]=t12,tmp=(C_word)a,a+=6,tmp));
t18=((C_word*)t16)[1];
f_4455(t18,t14,t1);}}
else{
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4502,a[2]=((C_word*)t0)[4],tmp=(C_word)a,a+=3,tmp);
/* lfa2.scm:548: walk */
t7=((C_word*)((C_word*)t0)[9])[1];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t7;
av2[1]=t6;
av2[2]=((C_word*)t0)[5];
f_4515(3,av2);}}}}}

/* k4324 in k4296 in k4293 in k4290 in walk/unbox in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4326(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_4326,c,av);}
a=C_alloc(3);
if(C_truep(t1)){
t2=C_a_i_cons(&a,2,t1,((C_word*)t0)[2]);
/* lfa2.scm:530: chicken.compiler.support#make-node */
t3=*((C_word*)lf[89]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[91];
av2[3]=t2;
av2[4]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}
else{
t2=C_a_i_list1(&a,1,((C_word*)t0)[4]);
/* lfa2.scm:531: chicken.compiler.support#make-node */
t3=*((C_word*)lf[89]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[92];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}}

/* g924 in k4296 in k4293 in k4290 in walk/unbox in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_4363(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(49,0,3)))){
C_save_and_reclaim_args((void *)trf_4363,3,t0,t1,t2);}
a=C_alloc(49);
t3=C_i_cadr(t2);
t4=C_i_caddr(t2);
t5=((C_word*)((C_word*)t0)[2])[1];
t6=C_mutate(((C_word *)((C_word*)t0)[2])+1,C_s_a_i_plus(&a,2,t5,C_fix(1)));
t7=C_a_i_list1(&a,1,t3);
t8=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t9=t8;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=((C_word*)t10)[1];
t12=C_eqp(t4,lf[93]);
t13=(C_truep(t12)?((C_word*)((C_word*)t0)[3])[1]:((C_word*)((C_word*)t0)[4])[1]);
t14=C_i_check_list_2(((C_word*)t0)[5],lf[94]);
t15=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4392,a[2]=t1,a[3]=t7,tmp=(C_word)a,a+=4,tmp);
t16=C_SCHEME_UNDEFINED;
t17=(*a=C_VECTOR_TYPE|1,a[1]=t16,tmp=(C_word)a,a+=2,tmp);
t18=C_set_block_item(t17,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4394,a[2]=t10,a[3]=t17,a[4]=t13,a[5]=t11,tmp=(C_word)a,a+=6,tmp));
t19=((C_word*)t17)[1];
f_4394(t19,t15,((C_word*)t0)[5]);}

/* k4390 in g924 in k4296 in k4293 in k4290 in walk/unbox in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4392(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_4392,c,av);}
/* lfa2.scm:538: chicken.compiler.support#make-node */
t2=*((C_word*)lf[89]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[63];
av2[3]=((C_word*)t0)[3];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* map-loop931 in g924 in k4296 in k4293 in k4290 in walk/unbox in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_4394(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_4394,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4419,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* lfa2.scm:540: g937 */
t4=((C_word*)t0)[4];{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k4417 in map-loop931 in g924 in k4296 in k4293 in k4290 in walk/unbox in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4419(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4419,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_4394(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k4441 in k4296 in k4293 in k4290 in walk/unbox in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4443(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_4443,c,av);}
a=C_alloc(3);
t2=C_a_i_list1(&a,1,t1);
/* lfa2.scm:545: chicken.compiler.support#make-node */
t3=*((C_word*)lf[89]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[92];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4451 in k4296 in k4293 in k4290 in walk/unbox in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4453(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_4453,c,av);}
/* lfa2.scm:546: chicken.compiler.support#make-node */
t2=*((C_word*)lf[89]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* map-loop958 in k4296 in k4293 in k4290 in walk/unbox in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_4455(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_4455,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4480,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* lfa2.scm:547: g964 */
t4=((C_word*)((C_word*)t0)[4])[1];{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
f_4515(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k4478 in map-loop958 in k4296 in k4293 in k4290 in walk/unbox in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4480(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4480,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_4455(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k4500 in k4296 in k4293 in k4290 in walk/unbox in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4502(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_4502,c,av);}
a=C_alloc(3);
t2=C_a_i_list1(&a,1,t1);
/* lfa2.scm:548: chicken.compiler.support#make-node */
t3=*((C_word*)lf[89]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[92];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* walk in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4515(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_4515,c,av);}
a=C_alloc(8);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4519,a[2]=t1,a[3]=t2,a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[5],tmp=(C_word)a,a+=8,tmp);
/* lfa2.scm:551: chicken.compiler.support#node-class */
t4=*((C_word*)lf[16]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k4517 in walk in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4519(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_4519,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_4522,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
/* lfa2.scm:552: chicken.compiler.support#node-parameters */
t3=*((C_word*)lf[15]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4520 in k4517 in walk in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4522(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_4522,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_4525,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
/* lfa2.scm:553: chicken.compiler.support#node-subexpressions */
t3=*((C_word*)lf[28]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4523 in k4520 in k4517 in walk in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4525(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,3)))){
C_save_and_reclaim((void *)f_4525,c,av);}
a=C_alloc(18);
t2=C_eqp(((C_word*)t0)[2],lf[11]);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4534,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp);
/* lfa2.scm:556: posq */
f_2677(t3,C_i_car(((C_word*)t0)[4]),((C_word*)t0)[6]);}
else{
t3=C_eqp(((C_word*)t0)[2],lf[55]);
if(C_truep(t3)){
t4=C_i_car(t1);
t5=C_i_car(((C_word*)t0)[4]);
t6=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_4571,a[2]=t5,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[7],a[5]=t1,a[6]=((C_word*)t0)[8],a[7]=t4,a[8]=((C_word*)t0)[4],tmp=(C_word)a,a+=9,tmp);
/* lfa2.scm:565: posq */
f_2677(t6,t5,((C_word*)t0)[6]);}
else{
t4=C_eqp(((C_word*)t0)[2],lf[63]);
t5=(C_truep(t4)?t4:C_eqp(((C_word*)t0)[2],lf[64]));
if(C_truep(t5)){
t6=C_i_car(((C_word*)t0)[4]);
t7=C_i_assoc(t6,lf[3]);
if(C_truep(t7)){
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4660,a[2]=((C_word*)t0)[9],a[3]=((C_word*)t0)[7],a[4]=((C_word*)t0)[8],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
/* lfa2.scm:572: g1031 */
t9=t8;
f_4660(t9,((C_word*)t0)[3],t7);}
else{
t8=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t9=t8;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=((C_word*)t10)[1];
t12=C_i_check_list_2(t1,lf[94]);
t13=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4757,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t14=C_SCHEME_UNDEFINED;
t15=(*a=C_VECTOR_TYPE|1,a[1]=t14,tmp=(C_word)a,a+=2,tmp);
t16=C_set_block_item(t15,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4759,a[2]=t10,a[3]=t15,a[4]=((C_word*)t0)[7],a[5]=t11,tmp=(C_word)a,a+=6,tmp));
t17=((C_word*)t15)[1];
f_4759(t17,t13,t1);}}
else{
t6=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t7=t6;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=((C_word*)t8)[1];
t10=C_i_check_list_2(t1,lf[94]);
t11=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4808,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4810,a[2]=t8,a[3]=t13,a[4]=((C_word*)t0)[7],a[5]=t9,tmp=(C_word)a,a+=6,tmp));
t15=((C_word*)t13)[1];
f_4810(t15,t11,t1);}}}}

/* k4532 in k4523 in k4520 in k4517 in walk in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4534(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_4534,c,av);}
a=C_alloc(6);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4548,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=C_a_i_cons(&a,2,t1,((C_word*)t0)[3]);
/* lfa2.scm:559: chicken.compiler.support#make-node */
t4=*((C_word*)lf[89]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=lf[91];
av2[3]=t3;
av2[4]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k4546 in k4532 in k4523 in k4520 in k4517 in walk in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4548(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_4548,c,av);}
a=C_alloc(3);
t2=C_a_i_list1(&a,1,t1);
/* lfa2.scm:558: chicken.compiler.support#make-node */
t3=*((C_word*)lf[89]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[95];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4569 in k4523 in k4520 in k4517 in walk in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4571(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(17,c,3)))){
C_save_and_reclaim((void *)f_4571,c,av);}
a=C_alloc(17);
if(C_truep(t1)){
t2=C_a_i_list2(&a,2,t1,((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4589,a[2]=((C_word*)t0)[3],a[3]=t2,a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* lfa2.scm:568: walk/unbox */
t4=((C_word*)((C_word*)t0)[6])[1];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[7];
f_4288(3,av2);}}
else{
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=C_i_check_list_2(((C_word*)t0)[5],lf[94]);
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4610,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[8],tmp=(C_word)a,a+=4,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4612,a[2]=t4,a[3]=t9,a[4]=((C_word*)t0)[4],a[5]=t5,tmp=(C_word)a,a+=6,tmp));
t11=((C_word*)t9)[1];
f_4612(t11,t7,((C_word*)t0)[5]);}}

/* k4587 in k4569 in k4523 in k4520 in k4517 in walk in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4589(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_4589,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4593,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* lfa2.scm:569: walk */
t3=((C_word*)((C_word*)t0)[4])[1];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_i_cadr(((C_word*)t0)[5]);
f_4515(3,av2);}}

/* k4591 in k4587 in k4569 in k4523 in k4520 in k4517 in walk in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4593(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_4593,c,av);}
a=C_alloc(6);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],t1);
/* lfa2.scm:567: chicken.compiler.support#make-node */
t3=*((C_word*)lf[89]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[96];
av2[3]=((C_word*)t0)[4];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4608 in k4569 in k4523 in k4520 in k4517 in walk in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4610(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_4610,c,av);}
/* lfa2.scm:570: chicken.compiler.support#make-node */
t2=*((C_word*)lf[89]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[55];
av2[3]=((C_word*)t0)[3];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* map-loop998 in k4569 in k4523 in k4520 in k4517 in walk in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_4612(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_4612,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4637,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* lfa2.scm:570: g1004 */
t4=((C_word*)((C_word*)t0)[4])[1];{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
f_4515(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k4635 in map-loop998 in k4569 in k4523 in k4520 in k4517 in walk in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4637(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4637,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_4612(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* g1031 in k4523 in k4520 in k4517 in walk in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_4660(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(53,0,3)))){
C_save_and_reclaim_args((void *)trf_4660,3,t0,t1,t2);}
a=C_alloc(53);
t3=C_i_cadr(t2);
t4=C_i_caddr(t2);
t5=((C_word*)((C_word*)t0)[2])[1];
t6=C_mutate(((C_word *)((C_word*)t0)[2])+1,C_s_a_i_plus(&a,2,t5,C_fix(1)));
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4672,a[2]=t4,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t8=C_a_i_list1(&a,1,t3);
t9=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t10=t9;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=((C_word*)t11)[1];
t13=C_eqp(t4,lf[98]);
t14=(C_truep(t13)?((C_word*)((C_word*)t0)[3])[1]:((C_word*)((C_word*)t0)[4])[1]);
t15=C_i_check_list_2(((C_word*)t0)[5],lf[94]);
t16=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4704,a[2]=t7,a[3]=t8,tmp=(C_word)a,a+=4,tmp);
t17=C_SCHEME_UNDEFINED;
t18=(*a=C_VECTOR_TYPE|1,a[1]=t17,tmp=(C_word)a,a+=2,tmp);
t19=C_set_block_item(t18,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4706,a[2]=t11,a[3]=t18,a[4]=t14,a[5]=t12,tmp=(C_word)a,a+=6,tmp));
t20=((C_word*)t18)[1];
f_4706(t20,t16,((C_word*)t0)[5]);}

/* k4670 in g1031 in k4523 in k4520 in k4517 in walk in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4672(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_4672,c,av);}
a=C_alloc(3);
t2=C_eqp(((C_word*)t0)[2],lf[97]);
if(C_truep(t2)){
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_a_i_list1(&a,1,t1);
/* lfa2.scm:585: chicken.compiler.support#make-node */
t4=*((C_word*)lf[89]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[95];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}}

/* k4702 in g1031 in k4523 in k4520 in k4517 in walk in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4704(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_4704,c,av);}
/* lfa2.scm:577: chicken.compiler.support#make-node */
t2=*((C_word*)lf[89]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[63];
av2[3]=((C_word*)t0)[3];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* map-loop1039 in g1031 in k4523 in k4520 in k4517 in walk in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_4706(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_4706,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4731,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* lfa2.scm:579: g1045 */
t4=((C_word*)t0)[4];{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k4729 in map-loop1039 in g1031 in k4523 in k4520 in k4517 in walk in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4731(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4731,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_4706(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k4755 in k4523 in k4520 in k4517 in walk in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4757(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_4757,c,av);}
/* lfa2.scm:587: chicken.compiler.support#make-node */
t2=*((C_word*)lf[89]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* map-loop1072 in k4523 in k4520 in k4517 in walk in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_4759(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_4759,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4784,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* lfa2.scm:587: g1078 */
t4=((C_word*)((C_word*)t0)[4])[1];{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
f_4515(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k4782 in map-loop1072 in k4523 in k4520 in k4517 in walk in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4784(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4784,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_4759(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k4806 in k4523 in k4520 in k4517 in walk in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4808(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_4808,c,av);}
/* lfa2.scm:588: chicken.compiler.support#make-node */
t2=*((C_word*)lf[89]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* map-loop1098 in k4523 in k4520 in k4517 in walk in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_fcall f_4810(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_4810,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4835,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* lfa2.scm:588: g1104 */
t4=((C_word*)((C_word*)t0)[4])[1];{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
f_4515(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k4833 in map-loop1098 in k4523 in k4520 in k4517 in walk in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4835(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4835,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_4810(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k4854 in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4856(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_4856,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4859,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4861,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
/* lfa2.scm:591: chicken.compiler.support#with-debugging-output */
t4=*((C_word*)lf[86]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=lf[101];
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k4857 in k4854 in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4859(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4859,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a4860 in k4854 in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4861(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_4861,c,av);}
a=C_alloc(6);
t2=*((C_word*)lf[78]+1);
t3=*((C_word*)lf[78]+1);
t4=C_i_check_port_2(*((C_word*)lf[78]+1),C_fix(2),C_SCHEME_TRUE,lf[79]);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4868,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t2,a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
/* lfa2.scm:594: ##sys#print */
t6=*((C_word*)lf[81]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[100];
av2[3]=C_SCHEME_FALSE;
av2[4]=*((C_word*)lf[78]+1);
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k4866 in a4860 in k4854 in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4868(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_4868,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4871,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* lfa2.scm:594: ##sys#print */
t3=*((C_word*)lf[81]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_i_length(((C_word*)t0)[5]);
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4869 in k4866 in a4860 in k4854 in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4871(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_4871,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4874,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* lfa2.scm:594: ##sys#write-char-0 */
t3=*((C_word*)lf[80]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_make_character(10);
av2[3]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k4872 in k4869 in k4866 in a4860 in k4854 in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4874(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_4874,c,av);}
a=C_alloc(5);
t2=*((C_word*)lf[78]+1);
t3=*((C_word*)lf[78]+1);
t4=C_i_check_port_2(*((C_word*)lf[78]+1),C_fix(2),C_SCHEME_TRUE,lf[79]);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4880,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* lfa2.scm:596: ##sys#print */
t6=*((C_word*)lf[81]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[99];
av2[3]=C_SCHEME_FALSE;
av2[4]=*((C_word*)lf[78]+1);
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k4878 in k4872 in k4869 in k4866 in a4860 in k4854 in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4880(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_4880,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4883,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* lfa2.scm:596: ##sys#print */
t3=*((C_word*)lf[81]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[4])[1];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4881 in k4878 in k4872 in k4869 in k4866 in a4860 in k4854 in k4284 in chicken.compiler.lfa2#perform-unboxing in k1461 in k1458 in k1455 in k1452 in k1449 */
static void C_ccall f_4883(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4883,c,av);}
/* lfa2.scm:596: ##sys#write-char-0 */
t2=*((C_word*)lf[80]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_make_character(10);
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* toplevel */
static C_TLS int toplevel_initialized=0;

void C_ccall C_lfa2_toplevel(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(toplevel_initialized) {C_kontinue(t1,C_SCHEME_UNDEFINED);}
else C_toplevel_entry(C_text("lfa2"));
C_check_nursery_minimum(C_calculate_demand(3,c,2));
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void*)C_lfa2_toplevel,c,av);}
toplevel_initialized=1;
if(C_unlikely(!C_demand_2(3518))){
C_save(t1);
C_rereclaim2(3518*sizeof(C_word),1);
t1=C_restore;}
a=C_alloc(3);
C_initialize_lf(lf,102);
lf[0]=C_h_intern(&lf[0],4, C_text("lfa2"));
lf[1]=C_h_intern(&lf[1],22, C_text("chicken.compiler.lfa2#"));
lf[4]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376B\000\000\021C_a_i_flonum_plus\376\003\000\000\002\376B\000\000\022C_ub_i_flonum_plus\376\003\000\000\002\376\001\000\000\002\001op\376\377\016\376\003\000\000"
"\002\376\003\000\000\002\376B\000\000\027C_a_i_flonum_difference\376\003\000\000\002\376B\000\000\030C_ub_i_flonum_difference\376\003\000\000\002\376\001\000\000\002\001o"
"p\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\022C_a_i_flonum_times\376\003\000\000\002\376B\000\000\023C_ub_i_flonum_times\376\003\000\000\002\376\001\000\000\002\001op\376"
"\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\025C_a_i_flonum_quotient\376\003\000\000\002\376B\000\000\026C_ub_i_flonum_quotient\376\003\000\000\002\376\001\000\000\002"
"\001op\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\017C_flonum_equalp\376\003\000\000\002\376B\000\000\024C_ub_i_flonum_equalp\376\003\000\000\002\376\001\000\000\004\001pre"
"d\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\021C_flonum_greaterp\376\003\000\000\002\376B\000\000\026C_ub_i_flonum_greaterp\376\003\000\000\002\376\001\000\000\004\001p"
"red\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\016C_flonum_lessp\376\003\000\000\002\376B\000\000\023C_ub_i_flonum_lessp\376\003\000\000\002\376\001\000\000\004\001pred\376"
"\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\033C_flonum_greater_or_equal_p\376\003\000\000\002\376B\000\000 C_ub_i_flonum_greater_or_e"
"qual_p\376\003\000\000\002\376\001\000\000\004\001pred\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\030C_flonum_less_or_equal_p\376\003\000\000\002\376B\000\000\035C_ub_i_"
"flonum_less_or_equal_p\376\003\000\000\002\376\001\000\000\004\001pred\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\021C_u_i_flonum_nanp\376\003\000\000\002\376B\000"
"\000\022C_ub_i_flonum_nanp\376\003\000\000\002\376\001\000\000\004\001pred\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\026C_u_i_flonum_infinitep\376\003\000\000\002"
"\376B\000\000\027C_ub_i_flonum_infinitep\376\003\000\000\002\376\001\000\000\004\001pred\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\025C_u_i_flonum_finite"
"pp\376\003\000\000\002\376B\000\000\025C_ub_i_flonum_finitep\376\003\000\000\002\376\001\000\000\004\001pred\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\020C_a_i_flonum_s"
"in\376\003\000\000\002\376B\000\000\005C_sin\376\003\000\000\002\376\001\000\000\002\001op\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\020C_a_i_flonum_cos\376\003\000\000\002\376B\000\000\005C_cos\376"
"\003\000\000\002\376\001\000\000\002\001op\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\020C_a_i_flonum_tan\376\003\000\000\002\376B\000\000\005C_tan\376\003\000\000\002\376\001\000\000\002\001op\376\377\016\376\003\000"
"\000\002\376\003\000\000\002\376B\000\000\021C_a_i_flonum_asin\376\003\000\000\002\376B\000\000\006C_asin\376\003\000\000\002\376\001\000\000\002\001op\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\021C_a_"
"i_flonum_acos\376\003\000\000\002\376B\000\000\006C_acos\376\003\000\000\002\376\001\000\000\002\001op\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\021C_a_i_flonum_atan\376\003\000"
"\000\002\376B\000\000\006C_atan\376\003\000\000\002\376\001\000\000\002\001op\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\022C_a_i_flonum_atan2\376\003\000\000\002\376B\000\000\007C_atan2\376"
"\003\000\000\002\376\001\000\000\002\001op\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\020C_a_i_flonum_exp\376\003\000\000\002\376B\000\000\005C_exp\376\003\000\000\002\376\001\000\000\002\001op\376\377\016\376\003\000"
"\000\002\376\003\000\000\002\376B\000\000\021C_a_i_flonum_expr\376\003\000\000\002\376B\000\000\005C_pow\376\003\000\000\002\376\001\000\000\002\001op\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\020C_a_i"
"_flonum_log\376\003\000\000\002\376B\000\000\005C_log\376\003\000\000\002\376\001\000\000\002\001op\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\021C_a_i_flonum_sqrt\376\003\000\000\002\376"
"B\000\000\006C_sqrt\376\003\000\000\002\376\001\000\000\002\001op\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\025C_a_i_flonum_truncate\376\003\000\000\002\376B\000\000\007C_trunc\376"
"\003\000\000\002\376\001\000\000\002\001op\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\024C_a_i_flonum_ceiling\376\003\000\000\002\376B\000\000\006C_ceil\376\003\000\000\002\376\001\000\000\002\001op\376"
"\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\022C_a_i_flonum_floor\376\003\000\000\002\376B\000\000\007C_floor\376\003\000\000\002\376\001\000\000\002\001op\376\377\016\376\003\000\000\002\376\003\000\000\002\376B"
"\000\000\022C_a_i_flonum_round\376\003\000\000\002\376B\000\000\007C_round\376\003\000\000\002\376\001\000\000\002\001op\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\020C_a_i_flonu"
"m_abs\376\003\000\000\002\376B\000\000\006C_fabs\376\003\000\000\002\376\001\000\000\002\001op\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\025C_a_u_i_f32vector_ref\376\003\000\000\002\376B"
"\000\000\024C_ub_i_f32vector_ref\376\003\000\000\002\376\001\000\000\003\001acc\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\025C_a_u_i_f64vector_ref\376\003\000\000"
"\002\376B\000\000\024C_ub_i_f64vector_ref\376\003\000\000\002\376\001\000\000\003\001acc\376\377\016\376\377\016"));
lf[5]=C_h_intern(&lf[5],53, C_text("chicken.compiler.lfa2#perform-secondary-flow-analysis"));
lf[6]=C_h_intern(&lf[6],6, C_text("struct"));
lf[7]=C_h_intern(&lf[7],1, C_text("\052"));
lf[8]=C_h_intern(&lf[8],31, C_text("chicken.compiler.support#db-get"));
lf[9]=C_h_intern(&lf[9],8, C_text("assigned"));
lf[10]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\005\001quote\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\003\000\000\002\376\001\000\000\020\001##core#primitive\376\003\000\000\002\376\001\000\000\015"
"\001##core#lambda\376\377\016"));
lf[11]=C_h_intern(&lf[11],15, C_text("##core#variable"));
lf[12]=C_h_intern(&lf[12],38, C_text("chicken.compiler.support#variable-mark"));
lf[13]=C_h_intern(&lf[13],23, C_text("##compiler#always-bound"));
lf[14]=C_h_intern(&lf[14],6, C_text("global"));
lf[15]=C_h_intern(&lf[15],40, C_text("chicken.compiler.support#node-parameters"));
lf[16]=C_h_intern(&lf[16],35, C_text("chicken.compiler.support#node-class"));
lf[17]=C_h_intern(&lf[17],49, C_text("chicken.compiler.support#node-subexpressions-set!"));
lf[18]=C_h_intern(&lf[18],45, C_text("chicken.compiler.support#node-parameters-set!"));
lf[19]=C_h_intern(&lf[19],40, C_text("chicken.compiler.support#node-class-set!"));
lf[20]=C_h_intern(&lf[20],16, C_text("##core#undefined"));
lf[21]=C_h_intern(&lf[21],8, C_text("for-each"));
lf[22]=C_h_intern(&lf[22],20, C_text("scheme#string-append"));
lf[23]=C_decode_literal(C_heaptop,C_text("\376B\000\000\0011"));
lf[24]=C_decode_literal(C_heaptop,C_text("\376B\000\000\0012"));
lf[25]=C_decode_literal(C_heaptop,C_text("\376B\000\000\0013"));
lf[26]=C_h_intern(&lf[26],29, C_text("chicken.compiler.support#bomb"));
lf[27]=C_decode_literal(C_heaptop,C_text("\376B\000\0005bad number of arguments to extinguished ##core#inline"));
lf[28]=C_h_intern(&lf[28],44, C_text("chicken.compiler.support#node-subexpressions"));
lf[29]=C_h_intern(&lf[29],5, C_text("foldr"));
lf[30]=C_h_intern(&lf[30],10, C_text("scheme#cdr"));
lf[31]=C_h_intern(&lf[31],11, C_text("scheme#cddr"));
lf[32]=C_h_intern(&lf[32],2, C_text("if"));
lf[33]=C_h_intern(&lf[33],11, C_text("##core#cond"));
lf[34]=C_h_intern(&lf[34],7, C_text("boolean"));
lf[35]=C_h_intern(&lf[35],13, C_text("scheme#append"));
lf[36]=C_h_intern(&lf[36],5, C_text("quote"));
lf[37]=C_h_intern(&lf[37],6, C_text("string"));
lf[38]=C_h_intern(&lf[38],7, C_text("keyword"));
lf[39]=C_h_intern(&lf[39],6, C_text("symbol"));
lf[40]=C_h_intern(&lf[40],7, C_text("integer"));
lf[41]=C_h_intern(&lf[41],6, C_text("fixnum"));
lf[42]=C_h_intern(&lf[42],6, C_text("bignum"));
lf[43]=C_h_intern(&lf[43],5, C_text("float"));
lf[44]=C_h_intern(&lf[44],6, C_text("ratnum"));
lf[45]=C_h_intern(&lf[45],7, C_text("cplxnum"));
lf[46]=C_h_intern(&lf[46],4, C_text("null"));
lf[47]=C_h_intern(&lf[47],4, C_text("list"));
lf[48]=C_h_intern(&lf[48],4, C_text("pair"));
lf[49]=C_h_intern(&lf[49],3, C_text("eof"));
lf[50]=C_h_intern(&lf[50],6, C_text("vector"));
lf[51]=C_h_intern(&lf[51],4, C_text("char"));
lf[52]=C_h_intern(&lf[52],38, C_text("chicken.compiler.support#small-bignum\077"));
lf[53]=C_h_intern(&lf[53],36, C_text("chicken.compiler.support#big-fixnum\077"));
lf[54]=C_h_intern(&lf[54],24, C_text("chicken.keyword#keyword\077"));
lf[55]=C_h_intern(&lf[55],3, C_text("let"));
lf[56]=C_h_intern(&lf[56],13, C_text("##core#lambda"));
lf[57]=C_h_intern(&lf[57],20, C_text("##core#direct_lambda"));
lf[58]=C_h_intern(&lf[58],9, C_text("procedure"));
lf[59]=C_h_intern(&lf[59],4, C_text("set!"));
lf[60]=C_h_intern(&lf[60],11, C_text("##core#set!"));
lf[61]=C_h_intern(&lf[61],9, C_text("undefined"));
lf[62]=C_h_intern(&lf[62],16, C_text("##core#primitive"));
lf[63]=C_h_intern(&lf[63],13, C_text("##core#inline"));
lf[64]=C_h_intern(&lf[64],22, C_text("##core#inline_allocate"));
lf[65]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376B\000\000\021C_i_check_closure\376\003\000\000\002\376\001\000\000\011\001procedure\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\017C_i_check_"
"exact\376\003\000\000\002\376\001\000\000\006\001fixnum\376\003\000\000\002\376\001\000\000\006\001bignum\376\003\000\000\002\376\001\000\000\007\001integer\376\003\000\000\002\376\001\000\000\006\001ratnum\376\377\016\376\003\000"
"\000\002\376\003\000\000\002\376B\000\000\021C_i_check_inexact\376\003\000\000\002\376\001\000\000\005\001float\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\020C_i_check_number\376"
"\003\000\000\002\376\001\000\000\006\001fixnum\376\003\000\000\002\376\001\000\000\007\001integer\376\003\000\000\002\376\001\000\000\006\001bignum\376\003\000\000\002\376\001\000\000\006\001ratnum\376\003\000\000\002\376\001\000\000\005\001f"
"loat\376\003\000\000\002\376\001\000\000\007\001cplxnum\376\003\000\000\002\376\001\000\000\006\001number\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\020C_i_check_string\376\003\000\000\002\376\001"
"\000\000\006\001string\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\024C_i_check_bytevector\376\003\000\000\002\376\001\000\000\004\001blob\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000"
"\020C_i_check_symbol\376\003\000\000\002\376\001\000\000\006\001symbol\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\021C_i_check_keyword\376\003\000\000\002\376\001\000\000\007\001"
"keyword\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\016C_i_check_list\376\003\000\000\002\376\001\000\000\004\001null\376\003\000\000\002\376\001\000\000\004\001pair\376\003\000\000\002\376\001\000\000\004\001"
"list\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\016C_i_check_pair\376\003\000\000\002\376\001\000\000\004\001pair\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\022C_i_check_l"
"ocative\376\003\000\000\002\376\001\000\000\010\001locative\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\021C_i_check_boolean\376\003\000\000\002\376\001\000\000\007\001boolean\376"
"\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\020C_i_check_vector\376\003\000\000\002\376\001\000\000\006\001vector\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\023C_i_check_st"
"ructure\376\003\000\000\002\376\001\000\000\010\001\052struct\052\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\016C_i_check_char\376\003\000\000\002\376\001\000\000\004\001char\376\377\016\376\003\000\000"
"\002\376\003\000\000\002\376B\000\000\023C_i_check_closure_2\376\003\000\000\002\376\001\000\000\011\001procedure\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\021C_i_check_ex"
"act_2\376\003\000\000\002\376\001\000\000\006\001fixnum\376\003\000\000\002\376\001\000\000\006\001bignum\376\003\000\000\002\376\001\000\000\007\001integer\376\003\000\000\002\376\001\000\000\006\001ratnum\376\377\016\376\003\000"
"\000\002\376\003\000\000\002\376B\000\000\023C_i_check_inexact_2\376\003\000\000\002\376\001\000\000\005\001float\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\022C_i_check_numbe"
"r_2\376\003\000\000\002\376\001\000\000\006\001fixnum\376\003\000\000\002\376\001\000\000\007\001integer\376\003\000\000\002\376\001\000\000\006\001bignum\376\003\000\000\002\376\001\000\000\006\001ratnum\376\003\000\000\002\376\001\000"
"\000\005\001float\376\003\000\000\002\376\001\000\000\007\001cplxnum\376\003\000\000\002\376\001\000\000\006\001number\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\022C_i_check_string_2\376"
"\003\000\000\002\376\001\000\000\006\001string\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\026C_i_check_bytevector_2\376\003\000\000\002\376\001\000\000\004\001blob\376\377\016\376\003\000\000\002\376"
"\003\000\000\002\376B\000\000\022C_i_check_symbol_2\376\003\000\000\002\376\001\000\000\006\001symbol\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\023C_i_check_keyword_"
"2\376\003\000\000\002\376\001\000\000\007\001keyword\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\020C_i_check_list_2\376\003\000\000\002\376\001\000\000\004\001null\376\003\000\000\002\376\001\000\000\004\001p"
"air\376\003\000\000\002\376\001\000\000\004\001list\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\020C_i_check_pair_2\376\003\000\000\002\376\001\000\000\004\001pair\376\377\016\376\003\000\000\002\376\003\000\000\002"
"\376B\000\000\024C_i_check_locative_2\376\003\000\000\002\376\001\000\000\010\001locative\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\023C_i_check_boolean_"
"2\376\003\000\000\002\376\001\000\000\007\001boolean\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\022C_i_check_vector_2\376\003\000\000\002\376\001\000\000\006\001vector\376\377\016\376\003\000\000\002"
"\376\003\000\000\002\376B\000\000\025C_i_check_structure_2\376\003\000\000\002\376\001\000\000\010\001\052struct\052\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\020C_i_check_ch"
"ar_2\376\003\000\000\002\376\001\000\000\004\001char\376\377\016\376\377\016"));
lf[66]=C_h_intern(&lf[66],31, C_text("chicken.compiler.support#unsafe"));
lf[67]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010C_i_noop"));
lf[68]=C_h_intern(&lf[68],8, C_text("\052struct\052"));
lf[69]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010C_i_noop"));
lf[70]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010C_i_noop"));
lf[71]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010C_i_noop"));
lf[72]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376B\000\000\034C_i_foreign_fixnum_argumentp\376\003\000\000\002\376\001\000\000\006\001fixnum\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\035C_"
"i_foreign_integer_argumentp\376\003\000\000\002\376\001\000\000\007\001integer\376\003\000\000\002\376\001\000\000\006\001fixnum\376\003\000\000\002\376\001\000\000\006\001bignum\376"
"\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\032C_i_foreign_char_argumentp\376\003\000\000\002\376\001\000\000\004\001char\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\034C_i_"
"foreign_flonum_argumentp\376\003\000\000\002\376\001\000\000\005\001float\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\034C_i_foreign_string_arg"
"umentp\376\003\000\000\002\376\001\000\000\006\001string\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\034C_i_foreign_symbol_argumentp\376\003\000\000\002\376\001\000\000\006\001"
"symbol\376\377\016\376\377\016"));
lf[73]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376B\000\000\014C_i_closurep\376\003\000\000\002\376\001\000\000\011\001procedure\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\011C_fixnump\376\003\000\000\002\376"
"\001\000\000\006\001fixnum\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\011C_bignump\376\003\000\000\002\376\001\000\000\006\001bignum\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\022C_i_exa"
"ct_integerp\376\003\000\000\002\376\001\000\000\007\001integer\376\003\000\000\002\376\001\000\000\006\001fixnum\376\003\000\000\002\376\001\000\000\006\001bignum\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000"
"\013C_i_flonump\376\003\000\000\002\376\001\000\000\005\001float\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\013C_i_numberp\376\003\000\000\002\376\001\000\000\006\001number\376\003\000\000\002\376"
"\001\000\000\006\001fixnum\376\003\000\000\002\376\001\000\000\007\001integer\376\003\000\000\002\376\001\000\000\006\001bignum\376\003\000\000\002\376\001\000\000\006\001ratnum\376\003\000\000\002\376\001\000\000\005\001float\376"
"\003\000\000\002\376\001\000\000\007\001cplxnum\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\013C_i_ratnump\376\003\000\000\002\376\001\000\000\006\001ratnum\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000"
"\014C_i_cplxnump\376\003\000\000\002\376\001\000\000\007\001cplxnum\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\011C_stringp\376\003\000\000\002\376\001\000\000\006\001string\376\377\016\376\003"
"\000\000\002\376\003\000\000\002\376B\000\000\015C_bytevectorp\376\003\000\000\002\376\001\000\000\004\001blob\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\014C_i_keywordp\376\003\000\000\002\376\001\000\000"
"\007\001keyword\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\013C_i_symbolp\376\003\000\000\002\376\001\000\000\006\001symbol\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\011C_i_lis"
"tp\376\003\000\000\002\376\001\000\000\004\001list\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\011C_i_pairp\376\003\000\000\002\376\001\000\000\004\001pair\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\013C_l"
"ocativep\376\003\000\000\002\376\001\000\000\010\001locative\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\012C_booleanp\376\003\000\000\002\376\001\000\000\007\001boolean\376\377\016\376\003\000\000"
"\002\376\003\000\000\002\376B\000\000\013C_i_vectorp\376\003\000\000\002\376\001\000\000\006\001vector\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\014C_structurep\376\003\000\000\002\376\001\000\000\006\001"
"struct\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\016C_i_structurep\376\003\000\000\002\376\001\000\000\010\001\052struct\052\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\007C_cha"
"rp\376\003\000\000\002\376\001\000\000\004\001char\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\011C_i_portp\376\003\000\000\002\376\001\000\000\004\001port\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\011C_i"
"_nullp\376\003\000\000\002\376\001\000\000\004\001null\376\377\016\376\377\016"));
lf[74]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010C_i_true"));
lf[75]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010C_i_true"));
lf[76]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010C_i_true"));
lf[77]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376B\000\000\015C_a_i_record1\376\003\000\000\002\376\001\000\000\010\001\052struct\052\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\015C_a_i_record2\376\003"
"\000\000\002\376\001\000\000\010\001\052struct\052\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\015C_a_i_record3\376\003\000\000\002\376\001\000\000\010\001\052struct\052\376\377\016\376\003\000\000\002\376\003\000\000\002"
"\376B\000\000\015C_a_i_record4\376\003\000\000\002\376\001\000\000\010\001\052struct\052\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\015C_a_i_record5\376\003\000\000\002\376\001\000\000\010\001\052"
"struct\052\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\015C_a_i_record6\376\003\000\000\002\376\001\000\000\010\001\052struct\052\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\015C_a_i"
"_record7\376\003\000\000\002\376\001\000\000\010\001\052struct\052\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\015C_a_i_record8\376\003\000\000\002\376\001\000\000\010\001\052struct\052\376\377\016"
"\376\003\000\000\002\376\003\000\000\002\376B\000\000\014C_a_i_record\376\003\000\000\002\376\001\000\000\010\001\052struct\052\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\014C_a_i_string\376\003\000\000"
"\002\376\001\000\000\006\001string\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\012C_a_i_port\376\003\000\000\002\376\001\000\000\004\001port\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\015C_a_i_"
"vector1\376\003\000\000\002\376\001\000\000\006\001vector\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\015C_a_i_vector2\376\003\000\000\002\376\001\000\000\006\001vector\376\377\016\376\003\000\000\002"
"\376\003\000\000\002\376B\000\000\015C_a_i_vector3\376\003\000\000\002\376\001\000\000\006\001vector\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\015C_a_i_vector4\376\003\000\000\002\376\001\000\000"
"\006\001vector\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\015C_a_i_vector5\376\003\000\000\002\376\001\000\000\006\001vector\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\015C_a_i_"
"vector6\376\003\000\000\002\376\001\000\000\006\001vector\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\015C_a_i_vector7\376\003\000\000\002\376\001\000\000\006\001vector\376\377\016\376\003\000\000\002"
"\376\003\000\000\002\376B\000\000\015C_a_i_vector8\376\003\000\000\002\376\001\000\000\006\001vector\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\010C_a_pair\376\003\000\000\002\376\001\000\000\004\001pai"
"r\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\020C_a_i_bytevector\376\003\000\000\002\376\001\000\000\004\001blob\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\023C_a_i_make_l"
"ocative\376\003\000\000\002\376\001\000\000\010\001locative\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\014C_a_i_vector\376\003\000\000\002\376\001\000\000\006\001vector\376\377\016\376\003\000\000"
"\002\376\003\000\000\002\376B\000\000\013C_a_i_list1\376\003\000\000\002\376\001\000\000\004\001pair\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\013C_a_i_list2\376\003\000\000\002\376\001\000\000\004\001pai"
"r\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\013C_a_i_list3\376\003\000\000\002\376\001\000\000\004\001pair\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\013C_a_i_list4\376\003\000\000\002\376"
"\001\000\000\004\001pair\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\013C_a_i_list5\376\003\000\000\002\376\001\000\000\004\001pair\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\013C_a_i_lis"
"t6\376\003\000\000\002\376\001\000\000\004\001pair\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\013C_a_i_list7\376\003\000\000\002\376\001\000\000\004\001pair\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\013C"
"_a_i_list8\376\003\000\000\002\376\001\000\000\004\001pair\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\012C_a_i_cons\376\003\000\000\002\376\001\000\000\004\001pair\376\377\016\376\003\000\000\002\376\003\000\000"
"\002\376B\000\000\014C_a_i_flonum\376\003\000\000\002\376\001\000\000\005\001float\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\020C_a_i_fix_to_flo\376\003\000\000\002\376\001\000\000\005\001f"
"loat\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\020C_a_i_big_to_flo\376\003\000\000\002\376\001\000\000\005\001float\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\020C_a_i_fi"
"x_to_big\376\003\000\000\002\376\001\000\000\006\001bignum\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\015C_a_i_bignum0\376\003\000\000\002\376\001\000\000\006\001bignum\376\377\016\376\003\000\000"
"\002\376\003\000\000\002\376B\000\000\015C_a_i_bignum1\376\003\000\000\002\376\001\000\000\006\001bignum\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\015C_a_i_bignum2\376\003\000\000\002\376\001\000"
"\000\006\001bignum\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\020C_a_i_flonum_abs\376\003\000\000\002\376\001\000\000\005\001float\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\021C_a"
"_i_flonum_acos\376\003\000\000\002\376\001\000\000\005\001float\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000$C_a_i_flonum_actual_quotient_che"
"cked\376\003\000\000\002\376\001\000\000\005\001float\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\021C_a_i_flonum_asin\376\003\000\000\002\376\001\000\000\005\001float\376\377\016\376\003\000\000\002\376"
"\003\000\000\002\376B\000\000\022C_a_i_flonum_atan2\376\003\000\000\002\376\001\000\000\005\001float\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\021C_a_i_flonum_atan\376\003"
"\000\000\002\376\001\000\000\005\001float\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\024C_a_i_flonum_ceiling\376\003\000\000\002\376\001\000\000\005\001float\376\377\016\376\003\000\000\002\376\003\000\000"
"\002\376B\000\000\020C_a_i_flonum_cos\376\003\000\000\002\376\001\000\000\005\001float\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\027C_a_i_flonum_difference\376"
"\003\000\000\002\376\001\000\000\005\001float\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\020C_a_i_flonum_exp\376\003\000\000\002\376\001\000\000\005\001float\376\377\016\376\003\000\000\002\376\003\000\000\002\376B"
"\000\000\021C_a_i_flonum_expt\376\003\000\000\002\376\001\000\000\005\001float\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\022C_a_i_flonum_floor\376\003\000\000\002\376\001\000"
"\000\005\001float\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\020C_a_i_flonum_gcd\376\003\000\000\002\376\001\000\000\005\001float\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\020C_a_"
"i_flonum_log\376\003\000\000\002\376\001\000\000\005\001float\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\033C_a_i_flonum_modulo_checked\376\003\000\000\002\376\001"
"\000\000\005\001float\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\023C_a_i_flonum_negate\376\003\000\000\002\376\001\000\000\005\001float\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\021"
"C_a_i_flonum_plus\376\003\000\000\002\376\001\000\000\005\001float\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\035C_a_i_flonum_quotient_checked"
"\376\003\000\000\002\376\001\000\000\005\001float\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\025C_a_i_flonum_quotient\376\003\000\000\002\376\001\000\000\005\001float\376\377\016\376\003\000\000\002\376"
"\003\000\000\002\376B\000\000\036C_a_i_flonum_remainder_checked\376\003\000\000\002\376\001\000\000\005\001float\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\022C_a_i_f"
"lonum_round\376\003\000\000\002\376\001\000\000\005\001float\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\031C_a_i_flonum_round_proper\376\003\000\000\002\376\001\000\000\005"
"\001float\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\020C_a_i_flonum_sin\376\003\000\000\002\376\001\000\000\005\001float\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\021C_a_i_"
"flonum_sqrt\376\003\000\000\002\376\001\000\000\005\001float\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\020C_a_i_flonum_tan\376\003\000\000\002\376\001\000\000\005\001float\376\377\016"
"\376\003\000\000\002\376\003\000\000\002\376B\000\000\022C_a_i_flonum_times\376\003\000\000\002\376\001\000\000\005\001float\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\025C_a_i_flonum_"
"truncate\376\003\000\000\002\376\001\000\000\005\001float\376\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\025C_a_u_i_f64vector_ref\376\003\000\000\002\376\001\000\000\005\001float\376"
"\377\016\376\003\000\000\002\376\003\000\000\002\376B\000\000\025C_a_u_i_f32vector_ref\376\003\000\000\002\376\001\000\000\005\001float\376\377\016\376\377\016"));
lf[78]=C_h_intern(&lf[78],21, C_text("##sys#standard-output"));
lf[79]=C_h_intern(&lf[79],6, C_text("printf"));
lf[80]=C_h_intern(&lf[80],18, C_text("##sys#write-char-0"));
lf[81]=C_h_intern(&lf[81],11, C_text("##sys#print"));
lf[82]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002:\011"));
lf[83]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002  "));
lf[84]=C_h_intern(&lf[84],18, C_text("chicken.base#print"));
lf[85]=C_decode_literal(C_heaptop,C_text("\376B\000\000\027eliminated type checks:"));
lf[86]=C_h_intern(&lf[86],46, C_text("chicken.compiler.support#with-debugging-output"));
lf[87]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001x\376\003\000\000\002\376\001\000\000\001\001o\376\377\016"));
lf[88]=C_h_intern(&lf[88],38, C_text("chicken.compiler.lfa2#perform-unboxing"));
lf[89]=C_h_intern(&lf[89],34, C_text("chicken.compiler.support#make-node"));
lf[90]=C_h_intern(&lf[90],12, C_text("##core#float"));
lf[91]=C_h_intern(&lf[91],21, C_text("##core#float-variable"));
lf[92]=C_h_intern(&lf[92],18, C_text("##core#unbox_float"));
lf[93]=C_h_intern(&lf[93],2, C_text("op"));
lf[94]=C_h_intern(&lf[94],3, C_text("map"));
lf[95]=C_h_intern(&lf[95],16, C_text("##core#box_float"));
lf[96]=C_h_intern(&lf[96],16, C_text("##core#let_float"));
lf[97]=C_h_intern(&lf[97],4, C_text("pred"));
lf[98]=C_h_intern(&lf[98],3, C_text("acc"));
lf[99]=C_decode_literal(C_heaptop,C_text("\376B\000\0008number of inline operations replaced with unboxed ones: "));
lf[100]=C_decode_literal(C_heaptop,C_text("\376B\000\000#number of unboxed float variables: "));
lf[101]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001x\376\003\000\000\002\376\001\000\000\001\001o\376\377\016"));
C_register_lf2(lf,102,create_ptable());{}
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1451,a[2]=t1,tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_library_toplevel(2,av2);}}

#ifdef C_ENABLE_PTABLES
static C_PTABLE_ENTRY ptable[184] = {
{C_text("f5390:lfa2_2escm"),(void*)f5390},
{C_text("f5448:lfa2_2escm"),(void*)f5448},
{C_text("f5469:lfa2_2escm"),(void*)f5469},
{C_text("f5481:lfa2_2escm"),(void*)f5481},
{C_text("f5488:lfa2_2escm"),(void*)f5488},
{C_text("f_1451:lfa2_2escm"),(void*)f_1451},
{C_text("f_1454:lfa2_2escm"),(void*)f_1454},
{C_text("f_1457:lfa2_2escm"),(void*)f_1457},
{C_text("f_1460:lfa2_2escm"),(void*)f_1460},
{C_text("f_1463:lfa2_2escm"),(void*)f_1463},
{C_text("f_2083:lfa2_2escm"),(void*)f_2083},
{C_text("f_2091:lfa2_2escm"),(void*)f_2091},
{C_text("f_2112:lfa2_2escm"),(void*)f_2112},
{C_text("f_2127:lfa2_2escm"),(void*)f_2127},
{C_text("f_2143:lfa2_2escm"),(void*)f_2143},
{C_text("f_2161:lfa2_2escm"),(void*)f_2161},
{C_text("f_2173:lfa2_2escm"),(void*)f_2173},
{C_text("f_2677:lfa2_2escm"),(void*)f_2677},
{C_text("f_2683:lfa2_2escm"),(void*)f_2683},
{C_text("f_2748:lfa2_2escm"),(void*)f_2748},
{C_text("f_2764:lfa2_2escm"),(void*)f_2764},
{C_text("f_2776:lfa2_2escm"),(void*)f_2776},
{C_text("f_2779:lfa2_2escm"),(void*)f_2779},
{C_text("f_2879:lfa2_2escm"),(void*)f_2879},
{C_text("f_2930:lfa2_2escm"),(void*)f_2930},
{C_text("f_2956:lfa2_2escm"),(void*)f_2956},
{C_text("f_2962:lfa2_2escm"),(void*)f_2962},
{C_text("f_2967:lfa2_2escm"),(void*)f_2967},
{C_text("f_2993:lfa2_2escm"),(void*)f_2993},
{C_text("f_2997:lfa2_2escm"),(void*)f_2997},
{C_text("f_3001:lfa2_2escm"),(void*)f_3001},
{C_text("f_3003:lfa2_2escm"),(void*)f_3003},
{C_text("f_3007:lfa2_2escm"),(void*)f_3007},
{C_text("f_3010:lfa2_2escm"),(void*)f_3010},
{C_text("f_3013:lfa2_2escm"),(void*)f_3013},
{C_text("f_3018:lfa2_2escm"),(void*)f_3018},
{C_text("f_3025:lfa2_2escm"),(void*)f_3025},
{C_text("f_3026:lfa2_2escm"),(void*)f_3026},
{C_text("f_3033:lfa2_2escm"),(void*)f_3033},
{C_text("f_3043:lfa2_2escm"),(void*)f_3043},
{C_text("f_3060:lfa2_2escm"),(void*)f_3060},
{C_text("f_3067:lfa2_2escm"),(void*)f_3067},
{C_text("f_3095:lfa2_2escm"),(void*)f_3095},
{C_text("f_3097:lfa2_2escm"),(void*)f_3097},
{C_text("f_3107:lfa2_2escm"),(void*)f_3107},
{C_text("f_3126:lfa2_2escm"),(void*)f_3126},
{C_text("f_3143:lfa2_2escm"),(void*)f_3143},
{C_text("f_3189:lfa2_2escm"),(void*)f_3189},
{C_text("f_3195:lfa2_2escm"),(void*)f_3195},
{C_text("f_3200:lfa2_2escm"),(void*)f_3200},
{C_text("f_3202:lfa2_2escm"),(void*)f_3202},
{C_text("f_3212:lfa2_2escm"),(void*)f_3212},
{C_text("f_3219:lfa2_2escm"),(void*)f_3219},
{C_text("f_3223:lfa2_2escm"),(void*)f_3223},
{C_text("f_3231:lfa2_2escm"),(void*)f_3231},
{C_text("f_3243:lfa2_2escm"),(void*)f_3243},
{C_text("f_3260:lfa2_2escm"),(void*)f_3260},
{C_text("f_3268:lfa2_2escm"),(void*)f_3268},
{C_text("f_3274:lfa2_2escm"),(void*)f_3274},
{C_text("f_3280:lfa2_2escm"),(void*)f_3280},
{C_text("f_3286:lfa2_2escm"),(void*)f_3286},
{C_text("f_3290:lfa2_2escm"),(void*)f_3290},
{C_text("f_3293:lfa2_2escm"),(void*)f_3293},
{C_text("f_3296:lfa2_2escm"),(void*)f_3296},
{C_text("f_3305:lfa2_2escm"),(void*)f_3305},
{C_text("f_3308:lfa2_2escm"),(void*)f_3308},
{C_text("f_3355:lfa2_2escm"),(void*)f_3355},
{C_text("f_3358:lfa2_2escm"),(void*)f_3358},
{C_text("f_3365:lfa2_2escm"),(void*)f_3365},
{C_text("f_3369:lfa2_2escm"),(void*)f_3369},
{C_text("f_3387:lfa2_2escm"),(void*)f_3387},
{C_text("f_3391:lfa2_2escm"),(void*)f_3391},
{C_text("f_3399:lfa2_2escm"),(void*)f_3399},
{C_text("f_3411:lfa2_2escm"),(void*)f_3411},
{C_text("f_3455:lfa2_2escm"),(void*)f_3455},
{C_text("f_3458:lfa2_2escm"),(void*)f_3458},
{C_text("f_3461:lfa2_2escm"),(void*)f_3461},
{C_text("f_3472:lfa2_2escm"),(void*)f_3472},
{C_text("f_3479:lfa2_2escm"),(void*)f_3479},
{C_text("f_3493:lfa2_2escm"),(void*)f_3493},
{C_text("f_3509:lfa2_2escm"),(void*)f_3509},
{C_text("f_3517:lfa2_2escm"),(void*)f_3517},
{C_text("f_3523:lfa2_2escm"),(void*)f_3523},
{C_text("f_3565:lfa2_2escm"),(void*)f_3565},
{C_text("f_3584:lfa2_2escm"),(void*)f_3584},
{C_text("f_3587:lfa2_2escm"),(void*)f_3587},
{C_text("f_3590:lfa2_2escm"),(void*)f_3590},
{C_text("f_3601:lfa2_2escm"),(void*)f_3601},
{C_text("f_3615:lfa2_2escm"),(void*)f_3615},
{C_text("f_3640:lfa2_2escm"),(void*)f_3640},
{C_text("f_3644:lfa2_2escm"),(void*)f_3644},
{C_text("f_3656:lfa2_2escm"),(void*)f_3656},
{C_text("f_3663:lfa2_2escm"),(void*)f_3663},
{C_text("f_3667:lfa2_2escm"),(void*)f_3667},
{C_text("f_3670:lfa2_2escm"),(void*)f_3670},
{C_text("f_3685:lfa2_2escm"),(void*)f_3685},
{C_text("f_3711:lfa2_2escm"),(void*)f_3711},
{C_text("f_3731:lfa2_2escm"),(void*)f_3731},
{C_text("f_3793:lfa2_2escm"),(void*)f_3793},
{C_text("f_3800:lfa2_2escm"),(void*)f_3800},
{C_text("f_3803:lfa2_2escm"),(void*)f_3803},
{C_text("f_3809:lfa2_2escm"),(void*)f_3809},
{C_text("f_3812:lfa2_2escm"),(void*)f_3812},
{C_text("f_3819:lfa2_2escm"),(void*)f_3819},
{C_text("f_3823:lfa2_2escm"),(void*)f_3823},
{C_text("f_3827:lfa2_2escm"),(void*)f_3827},
{C_text("f_3841:lfa2_2escm"),(void*)f_3841},
{C_text("f_3895:lfa2_2escm"),(void*)f_3895},
{C_text("f_3903:lfa2_2escm"),(void*)f_3903},
{C_text("f_3918:lfa2_2escm"),(void*)f_3918},
{C_text("f_3921:lfa2_2escm"),(void*)f_3921},
{C_text("f_3924:lfa2_2escm"),(void*)f_3924},
{C_text("f_3933:lfa2_2escm"),(void*)f_3933},
{C_text("f_3959:lfa2_2escm"),(void*)f_3959},
{C_text("f_3979:lfa2_2escm"),(void*)f_3979},
{C_text("f_4037:lfa2_2escm"),(void*)f_4037},
{C_text("f_4047:lfa2_2escm"),(void*)f_4047},
{C_text("f_4063:lfa2_2escm"),(void*)f_4063},
{C_text("f_4079:lfa2_2escm"),(void*)f_4079},
{C_text("f_4110:lfa2_2escm"),(void*)f_4110},
{C_text("f_4120:lfa2_2escm"),(void*)f_4120},
{C_text("f_4136:lfa2_2escm"),(void*)f_4136},
{C_text("f_4146:lfa2_2escm"),(void*)f_4146},
{C_text("f_4148:lfa2_2escm"),(void*)f_4148},
{C_text("f_4158:lfa2_2escm"),(void*)f_4158},
{C_text("f_4210:lfa2_2escm"),(void*)f_4210},
{C_text("f_4213:lfa2_2escm"),(void*)f_4213},
{C_text("f_4221:lfa2_2escm"),(void*)f_4221},
{C_text("f_4225:lfa2_2escm"),(void*)f_4225},
{C_text("f_4233:lfa2_2escm"),(void*)f_4233},
{C_text("f_4236:lfa2_2escm"),(void*)f_4236},
{C_text("f_4239:lfa2_2escm"),(void*)f_4239},
{C_text("f_4242:lfa2_2escm"),(void*)f_4242},
{C_text("f_4259:lfa2_2escm"),(void*)f_4259},
{C_text("f_4269:lfa2_2escm"),(void*)f_4269},
{C_text("f_4282:lfa2_2escm"),(void*)f_4282},
{C_text("f_4286:lfa2_2escm"),(void*)f_4286},
{C_text("f_4288:lfa2_2escm"),(void*)f_4288},
{C_text("f_4292:lfa2_2escm"),(void*)f_4292},
{C_text("f_4295:lfa2_2escm"),(void*)f_4295},
{C_text("f_4298:lfa2_2escm"),(void*)f_4298},
{C_text("f_4326:lfa2_2escm"),(void*)f_4326},
{C_text("f_4363:lfa2_2escm"),(void*)f_4363},
{C_text("f_4392:lfa2_2escm"),(void*)f_4392},
{C_text("f_4394:lfa2_2escm"),(void*)f_4394},
{C_text("f_4419:lfa2_2escm"),(void*)f_4419},
{C_text("f_4443:lfa2_2escm"),(void*)f_4443},
{C_text("f_4453:lfa2_2escm"),(void*)f_4453},
{C_text("f_4455:lfa2_2escm"),(void*)f_4455},
{C_text("f_4480:lfa2_2escm"),(void*)f_4480},
{C_text("f_4502:lfa2_2escm"),(void*)f_4502},
{C_text("f_4515:lfa2_2escm"),(void*)f_4515},
{C_text("f_4519:lfa2_2escm"),(void*)f_4519},
{C_text("f_4522:lfa2_2escm"),(void*)f_4522},
{C_text("f_4525:lfa2_2escm"),(void*)f_4525},
{C_text("f_4534:lfa2_2escm"),(void*)f_4534},
{C_text("f_4548:lfa2_2escm"),(void*)f_4548},
{C_text("f_4571:lfa2_2escm"),(void*)f_4571},
{C_text("f_4589:lfa2_2escm"),(void*)f_4589},
{C_text("f_4593:lfa2_2escm"),(void*)f_4593},
{C_text("f_4610:lfa2_2escm"),(void*)f_4610},
{C_text("f_4612:lfa2_2escm"),(void*)f_4612},
{C_text("f_4637:lfa2_2escm"),(void*)f_4637},
{C_text("f_4660:lfa2_2escm"),(void*)f_4660},
{C_text("f_4672:lfa2_2escm"),(void*)f_4672},
{C_text("f_4704:lfa2_2escm"),(void*)f_4704},
{C_text("f_4706:lfa2_2escm"),(void*)f_4706},
{C_text("f_4731:lfa2_2escm"),(void*)f_4731},
{C_text("f_4757:lfa2_2escm"),(void*)f_4757},
{C_text("f_4759:lfa2_2escm"),(void*)f_4759},
{C_text("f_4784:lfa2_2escm"),(void*)f_4784},
{C_text("f_4808:lfa2_2escm"),(void*)f_4808},
{C_text("f_4810:lfa2_2escm"),(void*)f_4810},
{C_text("f_4835:lfa2_2escm"),(void*)f_4835},
{C_text("f_4856:lfa2_2escm"),(void*)f_4856},
{C_text("f_4859:lfa2_2escm"),(void*)f_4859},
{C_text("f_4861:lfa2_2escm"),(void*)f_4861},
{C_text("f_4868:lfa2_2escm"),(void*)f_4868},
{C_text("f_4871:lfa2_2escm"),(void*)f_4871},
{C_text("f_4874:lfa2_2escm"),(void*)f_4874},
{C_text("f_4880:lfa2_2escm"),(void*)f_4880},
{C_text("f_4883:lfa2_2escm"),(void*)f_4883},
{C_text("toplevel:lfa2_2escm"),(void*)C_lfa2_toplevel},
{NULL,NULL}};
#endif

static C_PTABLE_ENTRY *create_ptable(void){
#ifdef C_ENABLE_PTABLES
return ptable;
#else
return NULL;
#endif
}

/*
o|hiding unexported module binding: chicken.compiler.lfa2#partition 
o|hiding unexported module binding: chicken.compiler.lfa2#span 
o|hiding unexported module binding: chicken.compiler.lfa2#take 
o|hiding unexported module binding: chicken.compiler.lfa2#drop 
o|hiding unexported module binding: chicken.compiler.lfa2#split-at 
o|hiding unexported module binding: chicken.compiler.lfa2#append-map 
o|hiding unexported module binding: chicken.compiler.lfa2#every 
o|hiding unexported module binding: chicken.compiler.lfa2#any 
o|hiding unexported module binding: chicken.compiler.lfa2#cons* 
o|hiding unexported module binding: chicken.compiler.lfa2#concatenate 
o|hiding unexported module binding: chicken.compiler.lfa2#delete 
o|hiding unexported module binding: chicken.compiler.lfa2#first 
o|hiding unexported module binding: chicken.compiler.lfa2#second 
o|hiding unexported module binding: chicken.compiler.lfa2#third 
o|hiding unexported module binding: chicken.compiler.lfa2#fourth 
o|hiding unexported module binding: chicken.compiler.lfa2#fifth 
o|hiding unexported module binding: chicken.compiler.lfa2#delete-duplicates 
o|hiding unexported module binding: chicken.compiler.lfa2#alist-cons 
o|hiding unexported module binding: chicken.compiler.lfa2#filter 
o|hiding unexported module binding: chicken.compiler.lfa2#filter-map 
o|hiding unexported module binding: chicken.compiler.lfa2#remove 
o|hiding unexported module binding: chicken.compiler.lfa2#unzip1 
o|hiding unexported module binding: chicken.compiler.lfa2#last 
o|hiding unexported module binding: chicken.compiler.lfa2#list-index 
o|hiding unexported module binding: chicken.compiler.lfa2#lset-adjoin/eq? 
o|hiding unexported module binding: chicken.compiler.lfa2#lset-difference/eq? 
o|hiding unexported module binding: chicken.compiler.lfa2#lset-union/eq? 
o|hiding unexported module binding: chicken.compiler.lfa2#lset-intersection/eq? 
o|hiding unexported module binding: chicken.compiler.lfa2#list-tabulate 
o|hiding unexported module binding: chicken.compiler.lfa2#lset<=/eq? 
o|hiding unexported module binding: chicken.compiler.lfa2#lset=/eq? 
o|hiding unexported module binding: chicken.compiler.lfa2#length+ 
o|hiding unexported module binding: chicken.compiler.lfa2#find 
o|hiding unexported module binding: chicken.compiler.lfa2#find-tail 
o|hiding unexported module binding: chicken.compiler.lfa2#iota 
o|hiding unexported module binding: chicken.compiler.lfa2#make-list 
o|hiding unexported module binding: chicken.compiler.lfa2#posq 
o|hiding unexported module binding: chicken.compiler.lfa2#posv 
o|hiding unexported module binding: chicken.compiler.lfa2#+type-check-map+ 
o|hiding unexported module binding: chicken.compiler.lfa2#+predicate-map+ 
o|hiding unexported module binding: chicken.compiler.lfa2#+ffi-type-check-map+ 
o|hiding unexported module binding: chicken.compiler.lfa2#+constructor-map+ 
o|hiding unexported module binding: chicken.compiler.lfa2#+unboxed-map+ 
S|applied compiler syntax:
S|  chicken.format#printf		3
S|  scheme#for-each		4
S|  chicken.base#foldl		3
S|  scheme#map		9
S|  chicken.base#foldr		3
o|eliminated procedure checks: 71 
o|specializations:
o|  3 (##sys#check-output-port * * *)
o|  22 (scheme#eqv? * (or eof null fixnum char boolean symbol keyword))
o|  1 (scheme#caar (pair pair *))
o|  3 (scheme#eqv? (or eof null fixnum char boolean symbol keyword) *)
o|  1 (scheme#memq * list)
o|  3 (chicken.base#add1 *)
o|  1 (scheme#eqv? * *)
o|  3 (##sys#check-list (or pair list) *)
o|  28 (scheme#cdr pair)
o|  13 (scheme#car pair)
(o e)|safe calls: 473 
o|safe globals: (chicken.compiler.lfa2#perform-unboxing chicken.compiler.lfa2#perform-secondary-flow-analysis chicken.compiler.lfa2#+unboxed-map+ chicken.compiler.lfa2#+constructor-map+ chicken.compiler.lfa2#+ffi-type-check-map+ chicken.compiler.lfa2#+predicate-map+ chicken.compiler.lfa2#+type-check-map+ chicken.compiler.lfa2#posv chicken.compiler.lfa2#posq chicken.compiler.lfa2#make-list chicken.compiler.lfa2#iota chicken.compiler.lfa2#find-tail chicken.compiler.lfa2#find chicken.compiler.lfa2#length+ chicken.compiler.lfa2#lset=/eq? chicken.compiler.lfa2#lset<=/eq? chicken.compiler.lfa2#list-tabulate chicken.compiler.lfa2#lset-intersection/eq? chicken.compiler.lfa2#lset-union/eq? chicken.compiler.lfa2#lset-difference/eq? chicken.compiler.lfa2#lset-adjoin/eq? chicken.compiler.lfa2#list-index chicken.compiler.lfa2#last chicken.compiler.lfa2#unzip1 chicken.compiler.lfa2#remove chicken.compiler.lfa2#filter-map chicken.compiler.lfa2#filter chicken.compiler.lfa2#alist-cons chicken.compiler.lfa2#delete-duplicates chicken.compiler.lfa2#fifth chicken.compiler.lfa2#fourth chicken.compiler.lfa2#third chicken.compiler.lfa2#second chicken.compiler.lfa2#first chicken.compiler.lfa2#delete chicken.compiler.lfa2#concatenate chicken.compiler.lfa2#cons* chicken.compiler.lfa2#any chicken.compiler.lfa2#every chicken.compiler.lfa2#append-map chicken.compiler.lfa2#split-at chicken.compiler.lfa2#drop chicken.compiler.lfa2#take chicken.compiler.lfa2#span chicken.compiler.lfa2#partition) 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#partition 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#span 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#drop 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#split-at 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#append-map 
o|inlining procedure: k1845 
o|inlining procedure: k1845 
o|inlining procedure: k1876 
o|inlining procedure: k1876 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#cons* 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#concatenate 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#fourth 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#fifth 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#delete-duplicates 
o|inlining procedure: k2093 
o|inlining procedure: k2093 
o|inlining procedure: k2085 
o|inlining procedure: k2085 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#unzip1 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#last 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#list-index 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#lset-adjoin/eq? 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#lset-difference/eq? 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#lset-union/eq? 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#lset-intersection/eq? 
o|inlining procedure: k2484 
o|inlining procedure: k2484 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#lset<=/eq? 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#lset=/eq? 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#length+ 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#find 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#find-tail 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#iota 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#make-list 
o|inlining procedure: k2685 
o|inlining procedure: k2685 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#posv 
o|inlining procedure: k2881 
o|inlining procedure: k2881 
o|inlining procedure: k2890 
o|inlining procedure: k2902 
o|inlining procedure: k2902 
o|inlining procedure: k2890 
o|inlining procedure: k2935 
o|contracted procedure: "(lfa2.scm:272) g588589" 
o|inlining procedure: k2935 
o|substituted constant variable: a2968 
o|inlining procedure: k2969 
o|inlining procedure: k2969 
o|inlining procedure: k2984 
o|inlining procedure: k2984 
o|inlining procedure: "(lfa2.scm:283) chicken.compiler.lfa2#first" 
o|inlining procedure: k3028 
o|inlining procedure: k3028 
o|inlining procedure: k3044 
o|inlining procedure: k3044 
o|inlining procedure: k3065 
o|inlining procedure: k3065 
o|inlining procedure: k3077 
o|inlining procedure: k3077 
o|substituted constant variable: a3087 
o|substituted constant variable: a3089 
o|substituted constant variable: a3091 
o|inlining procedure: k3099 
o|inlining procedure: k3099 
o|inlining procedure: "(lfa2.scm:294) chicken.compiler.lfa2#first" 
o|contracted procedure: "(lfa2.scm:334) chicken.compiler.lfa2#remove" 
o|merged explicitly consed rest parameter: rest661664 
o|inlining procedure: k3220 
o|inlining procedure: k3220 
o|inlining procedure: "(lfa2.scm:338) chicken.compiler.lfa2#first" 
o|consed rest parameter at call site: "(lfa2.scm:343) count-floatvar554" 3 
o|consed rest parameter at call site: "(lfa2.scm:344) count-floatvar554" 3 
o|consed rest parameter at call site: "(lfa2.scm:345) count-floatvar554" 3 
o|inlining procedure: k3297 
o|contracted procedure: "(lfa2.scm:357) vartype550" 
o|inlining procedure: k3133 
o|inlining procedure: k3133 
o|inlining procedure: k3145 
o|inlining procedure: k3145 
o|inlining procedure: "(lfa2.scm:357) chicken.compiler.lfa2#first" 
o|inlining procedure: "(lfa2.scm:355) chicken.compiler.lfa2#first" 
o|inlining procedure: "(lfa2.scm:354) chicken.compiler.lfa2#first" 
o|inlining procedure: "(lfa2.scm:353) chicken.compiler.lfa2#first" 
o|inlining procedure: k3297 
o|inlining procedure: "(lfa2.scm:368) chicken.compiler.lfa2#third" 
o|inlining procedure: "(lfa2.scm:367) chicken.compiler.lfa2#second" 
o|inlining procedure: "(lfa2.scm:365) chicken.compiler.lfa2#third" 
o|inlining procedure: "(lfa2.scm:364) chicken.compiler.lfa2#third" 
o|inlining procedure: "(lfa2.scm:362) chicken.compiler.lfa2#second" 
o|inlining procedure: "(lfa2.scm:361) chicken.compiler.lfa2#second" 
o|inlining procedure: "(lfa2.scm:359) chicken.compiler.lfa2#first" 
o|inlining procedure: k3428 
o|contracted procedure: "(lfa2.scm:369) constant-result543" 
o|inlining procedure: k2753 
o|inlining procedure: k2753 
o|inlining procedure: k2765 
o|inlining procedure: k2765 
o|inlining procedure: k2780 
o|inlining procedure: k2780 
o|inlining procedure: k2792 
o|inlining procedure: k2792 
o|inlining procedure: k2804 
o|inlining procedure: k2804 
o|inlining procedure: k2816 
o|inlining procedure: k2816 
o|inlining procedure: k2828 
o|inlining procedure: k2828 
o|inlining procedure: k2840 
o|inlining procedure: k2840 
o|inlining procedure: k2859 
o|inlining procedure: k2859 
o|inlining procedure: "(lfa2.scm:369) chicken.compiler.lfa2#first" 
o|inlining procedure: k3428 
o|inlining procedure: k3474 
o|inlining procedure: "(lfa2.scm:388) chicken.compiler.lfa2#first" 
o|inlining procedure: k3474 
o|inlining procedure: k3497 
o|inlining procedure: "(lfa2.scm:387) chicken.compiler.lfa2#first" 
o|inlining procedure: k3497 
o|inlining procedure: "(lfa2.scm:381) chicken.compiler.lfa2#second" 
o|inlining procedure: k3541 
o|inlining procedure: k3541 
o|inlining procedure: "(lfa2.scm:372) chicken.compiler.lfa2#first" 
o|inlining procedure: "(lfa2.scm:371) chicken.compiler.lfa2#first" 
o|inlining procedure: k3554 
o|inlining procedure: "(lfa2.scm:395) chicken.compiler.lfa2#first" 
o|inlining procedure: k3554 
o|inlining procedure: "(lfa2.scm:401) chicken.compiler.lfa2#first" 
o|inlining procedure: "(lfa2.scm:400) chicken.compiler.lfa2#first" 
o|inlining procedure: "(lfa2.scm:398) chicken.compiler.lfa2#first" 
o|inlining procedure: k3616 
o|inlining procedure: k3616 
o|inlining procedure: k3645 
o|inlining procedure: k3645 
o|inlining procedure: k3628 
o|inlining procedure: k3668 
o|inlining procedure: k3668 
o|inlining procedure: k3680 
o|inlining procedure: "(lfa2.scm:425) chicken.compiler.lfa2#second" 
o|inlining procedure: "(lfa2.scm:423) chicken.compiler.lfa2#first" 
o|inlining procedure: "(lfa2.scm:423) chicken.compiler.lfa2#second" 
o|inlining procedure: k3680 
o|inlining procedure: k3719 
o|inlining procedure: "(lfa2.scm:422) chicken.compiler.lfa2#second" 
o|inlining procedure: k3719 
o|inlining procedure: "(lfa2.scm:421) chicken.compiler.lfa2#first" 
o|inlining procedure: k3740 
o|inlining procedure: k3740 
o|inlining procedure: "(lfa2.scm:415) chicken.compiler.lfa2#first" 
o|inlining procedure: k3801 
o|inlining procedure: k3801 
o|inlining procedure: "(lfa2.scm:436) chicken.compiler.lfa2#first" 
o|inlining procedure: k3790 
o|inlining procedure: k3790 
o|inlining procedure: k3846 
o|inlining procedure: k3868 
o|inlining procedure: k3883 
o|inlining procedure: k3883 
o|inlining procedure: "(lfa2.scm:454) chicken.compiler.lfa2#first" 
o|inlining procedure: "(lfa2.scm:456) chicken.compiler.lfa2#second" 
o|inlining procedure: "(lfa2.scm:453) chicken.compiler.lfa2#second" 
o|inlining procedure: k3868 
o|inlining procedure: "(lfa2.scm:450) chicken.compiler.lfa2#first" 
o|inlining procedure: k3846 
o|inlining procedure: k3928 
o|inlining procedure: "(lfa2.scm:473) chicken.compiler.lfa2#second" 
o|inlining procedure: "(lfa2.scm:470) chicken.compiler.lfa2#first" 
o|inlining procedure: "(lfa2.scm:471) chicken.compiler.lfa2#second" 
o|inlining procedure: k3928 
o|inlining procedure: k3967 
o|inlining procedure: "(lfa2.scm:469) chicken.compiler.lfa2#second" 
o|inlining procedure: k3967 
o|inlining procedure: "(lfa2.scm:468) chicken.compiler.lfa2#first" 
o|inlining procedure: k3988 
o|inlining procedure: k3988 
o|inlining procedure: "(lfa2.scm:447) chicken.compiler.lfa2#first" 
o|inlining procedure: k4042 
o|inlining procedure: "(lfa2.scm:488) chicken.compiler.lfa2#first" 
o|inlining procedure: k4042 
o|inlining procedure: k4067 
o|inlining procedure: k4067 
o|inlining procedure: "(lfa2.scm:484) chicken.compiler.lfa2#first" 
o|inlining procedure: k4034 
o|inlining procedure: k4034 
o|substituted constant variable: chicken.compiler.lfa2#+constructor-map+ 
o|inlining procedure: "(lfa2.scm:482) chicken.compiler.lfa2#first" 
o|substituted constant variable: chicken.compiler.lfa2#+predicate-map+ 
o|inlining procedure: "(lfa2.scm:445) chicken.compiler.lfa2#first" 
o|substituted constant variable: chicken.compiler.lfa2#+ffi-type-check-map+ 
o|inlining procedure: "(lfa2.scm:434) chicken.compiler.lfa2#first" 
o|substituted constant variable: chicken.compiler.lfa2#+type-check-map+ 
o|inlining procedure: "(lfa2.scm:413) chicken.compiler.lfa2#first" 
o|inlining procedure: k4112 
o|inlining procedure: k4112 
o|inlining procedure: "(lfa2.scm:407) chicken.compiler.lfa2#first" 
o|inlining procedure: k3628 
o|inlining procedure: k4150 
o|inlining procedure: k4150 
o|substituted constant variable: a4174 
o|substituted constant variable: a4176 
o|substituted constant variable: a4178 
o|substituted constant variable: a4180 
o|substituted constant variable: a4185 
o|substituted constant variable: a4187 
o|substituted constant variable: a4192 
o|substituted constant variable: a4194 
o|substituted constant variable: a4196 
o|substituted constant variable: a4198 
o|substituted constant variable: a4203 
o|substituted constant variable: a4205 
o|substituted constant variable: a4207 
o|inlining procedure: k4211 
o|inlining procedure: k4261 
o|contracted procedure: "(lfa2.scm:503) g851858" 
o|propagated global variable: out861864 ##sys#standard-output 
o|substituted constant variable: a4229 
o|substituted constant variable: a4230 
o|propagated global variable: out861864 ##sys#standard-output 
o|inlining procedure: k4261 
o|inlining procedure: k4211 
o|inlining procedure: k4299 
o|inlining procedure: "(lfa2.scm:523) chicken.compiler.lfa2#first" 
o|inlining procedure: k4299 
o|inlining procedure: k4327 
o|inlining procedure: k4327 
o|inlining procedure: "(lfa2.scm:528) chicken.compiler.lfa2#first" 
o|inlining procedure: k4348 
o|inlining procedure: k4396 
o|inlining procedure: k4396 
o|inlining procedure: "(lfa2.scm:536) chicken.compiler.lfa2#third" 
o|inlining procedure: "(lfa2.scm:535) chicken.compiler.lfa2#second" 
o|inlining procedure: k4457 
o|inlining procedure: k4457 
o|inlining procedure: "(lfa2.scm:533) chicken.compiler.lfa2#first" 
o|inlining procedure: k4348 
o|substituted constant variable: a4507 
o|substituted constant variable: a4509 
o|substituted constant variable: a4511 
o|substituted constant variable: a4513 
o|inlining procedure: k4526 
o|inlining procedure: "(lfa2.scm:556) chicken.compiler.lfa2#first" 
o|inlining procedure: k4526 
o|inlining procedure: k4572 
o|inlining procedure: "(lfa2.scm:569) chicken.compiler.lfa2#second" 
o|inlining procedure: k4572 
o|inlining procedure: k4614 
o|inlining procedure: k4614 
o|inlining procedure: "(lfa2.scm:564) chicken.compiler.lfa2#first" 
o|inlining procedure: "(lfa2.scm:563) chicken.compiler.lfa2#first" 
o|inlining procedure: k4645 
o|inlining procedure: k4673 
o|inlining procedure: k4673 
o|substituted constant variable: a4687 
o|inlining procedure: k4708 
o|inlining procedure: k4708 
o|inlining procedure: "(lfa2.scm:575) chicken.compiler.lfa2#third" 
o|inlining procedure: "(lfa2.scm:574) chicken.compiler.lfa2#second" 
o|inlining procedure: k4761 
o|inlining procedure: k4761 
o|inlining procedure: "(lfa2.scm:572) chicken.compiler.lfa2#first" 
o|inlining procedure: k4645 
o|inlining procedure: k4812 
o|inlining procedure: k4812 
o|substituted constant variable: a4847 
o|substituted constant variable: a4849 
o|substituted constant variable: a4851 
o|substituted constant variable: a4853 
o|propagated global variable: out11231129 ##sys#standard-output 
o|substituted constant variable: a4864 
o|substituted constant variable: a4865 
o|propagated global variable: out11261133 ##sys#standard-output 
o|substituted constant variable: a4876 
o|substituted constant variable: a4877 
o|propagated global variable: out11261133 ##sys#standard-output 
o|propagated global variable: out11231129 ##sys#standard-output 
o|inlining procedure: k4894 
o|inlining procedure: k4894 
o|contracted procedure: "(lfa2.scm:510) chicken.compiler.lfa2#filter-map" 
o|inlining procedure: k2140 
o|inlining procedure: k2140 
o|inlining procedure: k2129 
o|inlining procedure: k2129 
o|replaced variables: 739 
o|removed binding forms: 153 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#every 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#any 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#first 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#second 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#third 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#list-tabulate 
o|substituted constant variable: r26864919 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#+type-check-map+ 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#+predicate-map+ 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#+ffi-type-check-map+ 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#+constructor-map+ 
o|inlining procedure: k2890 
o|inlining procedure: k2890 
o|substituted constant variable: r29034925 
o|inlining procedure: k2890 
o|substituted constant variable: r28914926 
o|substituted constant variable: r30664942 
o|substituted constant variable: r30664942 
o|inlining procedure: k3065 
o|inlining procedure: k3065 
o|substituted constant variable: r30784946 
o|contracted procedure: "(mini-srfi-1.scm:141) chicken.compiler.lfa2#filter" 
o|substituted constant variable: r20864916 
o|substituted constant variable: r31464965 
o|substituted constant variable: r27545024 
o|substituted constant variable: r27665026 
o|substituted constant variable: r27815028 
o|substituted constant variable: r27935030 
o|substituted constant variable: r28055032 
o|substituted constant variable: r28175034 
o|substituted constant variable: r28295036 
o|substituted constant variable: r28415038 
o|substituted constant variable: r28605040 
o|substituted constant variable: r28605041 
o|substituted constant variable: r34985063 
o|substituted constant variable: r35425070 
o|substituted constant variable: r35555081 
o|inlining procedure: k3582 
o|substituted constant variable: r36175103 
o|inlining procedure: k3668 
o|inlining procedure: k3668 
o|substituted constant variable: r37205135 
o|inlining procedure: k3668 
o|inlining procedure: k3668 
o|inlining procedure: k3868 
o|substituted constant variable: r38845163 
o|inlining procedure: k3868 
o|substituted constant variable: r38475186 
o|inlining procedure: k3922 
o|inlining procedure: k3922 
o|substituted constant variable: r39685210 
o|inlining procedure: k3922 
o|inlining procedure: k3922 
o|substituted constant variable: r40685231 
o|substituted constant variable: r36295266 
o|propagated global variable: out861864 ##sys#standard-output 
o|propagated global variable: out11231129 ##sys#standard-output 
o|propagated global variable: out11261133 ##sys#standard-output 
o|substituted constant variable: r48955362 
o|substituted constant variable: r21305366 
o|replaced variables: 247 
o|removed binding forms: 581 
o|removed conditional forms: 1 
o|substituted constant variable: r28915371 
o|substituted constant variable: r28915372 
o|substituted constant variable: r28915373 
o|inlining procedure: "(lfa2.scm:274) chicken.compiler.lfa2#alist-cons" 
o|inlining procedure: "(lfa2.scm:337) varnode?551" 
o|inlining procedure: "(lfa2.scm:389) chicken.compiler.lfa2#alist-cons" 
o|inlining procedure: "(lfa2.scm:389) chicken.compiler.lfa2#alist-cons" 
o|inlining procedure: "(lfa2.scm:385) varnode?551" 
o|inlining procedure: "(lfa2.scm:384) chicken.compiler.lfa2#alist-cons" 
o|inlining procedure: "(lfa2.scm:399) varnode?551" 
o|inlining procedure: k3692 
o|substituted constant variable: r38695396 
o|substituted constant variable: r38695398 
o|substituted constant variable: r384751865401 
o|substituted constant variable: r384751865403 
o|substituted constant variable: r384751865405 
o|substituted constant variable: r384751865407 
o|inlining procedure: "(lfa2.scm:448) varnode?551" 
o|replaced variables: 4 
o|removed binding forms: 261 
o|removed conditional forms: 3 
o|removed side-effect free assignment to unused variable: chicken.compiler.lfa2#alist-cons 
o|substituted constant variable: y2365436 
o|substituted constant variable: r30665374 
o|substituted constant variable: r30665376 
o|removed side-effect free assignment to unused variable: varnode?551 
o|substituted constant variable: r36935483 
o|inlining procedure: k3940 
o|contracted procedure: "(mini-srfi-1.scm:135) a4891" 
o|replaced variables: 22 
o|removed binding forms: 16 
o|removed conditional forms: 1 
o|substituted constant variable: r39415518 
o|contracted procedure: "(mini-srfi-1.scm:134) g268269" 
o|replaced variables: 5 
o|removed binding forms: 31 
o|removed conditional forms: 1 
o|replaced variables: 7 
o|removed binding forms: 3 
o|removed binding forms: 3 
o|simplifications: ((if . 19) (##core#call . 306) (let . 14)) 
o|  call simplifications:
o|    scheme#=
o|    ##sys#setslot	6
o|    ##sys#cons	3
o|    scheme#member	3
o|    scheme#memq	2
o|    scheme#string?
o|    scheme#symbol?	5
o|    chicken.base#fixnum?
o|    chicken.base#bignum?
o|    chicken.base#flonum?
o|    chicken.base#ratnum?
o|    chicken.base#cplxnum?
o|    scheme#boolean?
o|    scheme#list?
o|    scheme#eof-object?
o|    scheme#vector?
o|    ##sys#immediate?
o|    ##sys#generic-structure?
o|    scheme#char?
o|    ##sys#list	5
o|    scheme#caddr	6
o|    scheme#cdar
o|    scheme#+
o|    scheme#set-car!
o|    scheme#assq	5
o|    ##sys#check-list	12
o|    ##sys#slot	31
o|    scheme#length	2
o|    scheme#list	12
o|    scheme#not	8
o|    scheme#assoc	8
o|    scheme#cons	25
o|    scheme#cdr	4
o|    scheme#set-cdr!
o|    scheme#pair?	22
o|    scheme#cadr	24
o|    scheme#null?	4
o|    scheme#car	45
o|    scheme#eq?	55
o|    chicken.fixnum#fx+
o|contracted procedure: k2688 
o|contracted procedure: k2707 
o|contracted procedure: k2694 
o|contracted procedure: k2701 
o|contracted procedure: k2884 
o|contracted procedure: k2893 
o|contracted procedure: k2899 
o|contracted procedure: k2905 
o|contracted procedure: k2911 
o|contracted procedure: k2918 
o|contracted procedure: k2922 
o|contracted procedure: k2890 
o|contracted procedure: k2932 
o|contracted procedure: k2945 
o|contracted procedure: k20705440 
o|contracted procedure: k2952 
o|contracted procedure: k2975 
o|contracted procedure: k2978 
o|contracted procedure: k2981 
o|contracted procedure: k3038 
o|contracted procedure: k3054 
o|contracted procedure: k3062 
o|contracted procedure: k3068 
o|contracted procedure: k3074 
o|contracted procedure: k3080 
o|contracted procedure: k3102 
o|contracted procedure: k3112 
o|contracted procedure: k3116 
o|contracted procedure: k3120 
o|contracted procedure: k3208 
o|contracted procedure: k2076 
o|contracted procedure: k2088 
o|contracted procedure: k2106 
o|contracted procedure: k2114 
o|contracted procedure: k3261 
o|contracted procedure: k3214 
o|contracted procedure: k3237 
o|contracted procedure: k3233 
o|contracted procedure: k3247 
o|contracted procedure: k3254 
o|contracted procedure: k3300 
o|contracted procedure: k3313 
o|contracted procedure: k3130 
o|contracted procedure: k3148 
o|contracted procedure: k3175 
o|contracted procedure: k3165 
o|inlining procedure: k3151 
o|inlining procedure: k3151 
o|contracted procedure: k3323 
o|contracted procedure: k3337 
o|contracted procedure: k3333 
o|contracted procedure: k3341 
o|contracted procedure: k3347 
o|contracted procedure: k3350 
o|contracted procedure: k3371 
o|contracted procedure: k3375 
o|contracted procedure: k3416 
o|contracted procedure: k3378 
o|contracted procedure: k3393 
o|contracted procedure: k3401 
o|contracted procedure: k3405 
o|contracted procedure: k3413 
o|contracted procedure: k3425 
o|contracted procedure: k3431 
o|contracted procedure: k3438 
o|contracted procedure: k2756 
o|contracted procedure: k2768 
o|contracted procedure: k2783 
o|contracted procedure: k2789 
o|contracted procedure: k2795 
o|contracted procedure: k2801 
o|contracted procedure: k2807 
o|contracted procedure: k2813 
o|contracted procedure: k2819 
o|contracted procedure: k2825 
o|contracted procedure: k2831 
o|contracted procedure: k2837 
o|contracted procedure: k2843 
o|contracted procedure: k2872 
o|contracted procedure: k2865 
o|contracted procedure: k2849 
o|contracted procedure: k2856 
o|contracted procedure: k2862 
o|contracted procedure: k3444 
o|contracted procedure: k3447 
o|contracted procedure: k3450 
o|contracted procedure: k3466 
o|contracted procedure: k3480 
o|contracted procedure: k20705465 
o|contracted procedure: k3487 
o|contracted procedure: k20705458 
o|contracted procedure: k3474 
o|contracted procedure: k3494 
o|contracted procedure: k3500 
o|contracted procedure: k3511 
o|contracted procedure: k20705477 
o|contracted procedure: k3532 
o|contracted procedure: k3525 
o|contracted procedure: k3538 
o|contracted procedure: k3544 
o|contracted procedure: k3557 
o|contracted procedure: k3560 
o|contracted procedure: k3567 
o|contracted procedure: k3573 
o|contracted procedure: k3576 
o|contracted procedure: k3579 
o|contracted procedure: k3595 
o|contracted procedure: k3602 
o|contracted procedure: k3609 
o|contracted procedure: k3619 
o|contracted procedure: k3625 
o|contracted procedure: k3631 
o|contracted procedure: k3634 
o|contracted procedure: k4133 
o|contracted procedure: k3637 
o|contracted procedure: k3651 
o|contracted procedure: k4106 
o|contracted procedure: k3657 
o|contracted procedure: k3777 
o|contracted procedure: k3677 
o|contracted procedure: k3686 
o|contracted procedure: k3698 
o|contracted procedure: k3705 
o|contracted procedure: k3692 
o|contracted procedure: k3713 
o|contracted procedure: k3716 
o|contracted procedure: k3737 
o|contracted procedure: k3722 
o|contracted procedure: k3733 
o|contracted procedure: k3768 
o|contracted procedure: k3743 
o|contracted procedure: k3749 
o|contracted procedure: k3760 
o|contracted procedure: k3781 
o|contracted procedure: k4102 
o|contracted procedure: k3787 
o|contracted procedure: k3795 
o|contracted procedure: k3829 
o|contracted procedure: k3804 
o|contracted procedure: k4098 
o|contracted procedure: k3835 
o|contracted procedure: k3843 
o|contracted procedure: k3849 
o|contracted procedure: k3856 
o|contracted procedure: k3864 
o|contracted procedure: k3912 
o|contracted procedure: k3871 
o|contracted procedure: k3877 
o|contracted procedure: k3880 
o|contracted procedure: k3886 
o|inlining procedure: k3868 
o|contracted procedure: k3897 
o|contracted procedure: k3905 
o|inlining procedure: k3868 
o|contracted procedure: k4025 
o|contracted procedure: k3925 
o|contracted procedure: k3934 
o|contracted procedure: k3946 
o|contracted procedure: k3953 
o|contracted procedure: k3940 
o|contracted procedure: k3961 
o|contracted procedure: k3964 
o|contracted procedure: k3985 
o|contracted procedure: k3970 
o|contracted procedure: k3981 
o|contracted procedure: k4016 
o|contracted procedure: k3991 
o|contracted procedure: k3997 
o|contracted procedure: k4008 
o|contracted procedure: k4094 
o|contracted procedure: k4031 
o|contracted procedure: k4084 
o|contracted procedure: k4039 
o|contracted procedure: k4048 
o|contracted procedure: k4054 
o|contracted procedure: k4081 
o|contracted procedure: k4070 
o|contracted procedure: k4115 
o|contracted procedure: k4125 
o|contracted procedure: k4129 
o|contracted procedure: k4141 
o|contracted procedure: k4153 
o|contracted procedure: k4163 
o|contracted procedure: k4167 
o|contracted procedure: k4214 
o|contracted procedure: k4252 
o|contracted procedure: k4264 
o|contracted procedure: k4274 
o|contracted procedure: k4278 
o|contracted procedure: k4249 
o|contracted procedure: k4302 
o|contracted procedure: k4305 
o|contracted procedure: k4315 
o|contracted procedure: k4321 
o|contracted procedure: k4334 
o|contracted procedure: k4341 
o|contracted procedure: k4345 
o|contracted procedure: k4351 
o|contracted procedure: k4354 
o|contracted procedure: k4489 
o|contracted procedure: k4357 
o|contracted procedure: k4365 
o|contracted procedure: k4368 
o|contracted procedure: k4377 
o|contracted procedure: k4381 
o|contracted procedure: k4427 
o|contracted procedure: k4384 
o|contracted procedure: k4387 
o|contracted procedure: k4399 
o|contracted procedure: k4402 
o|contracted procedure: k4405 
o|contracted procedure: k4413 
o|contracted procedure: k4421 
o|contracted procedure: k4437 
o|contracted procedure: k4445 
o|contracted procedure: k4448 
o|contracted procedure: k4460 
o|contracted procedure: k4463 
o|contracted procedure: k4466 
o|contracted procedure: k4474 
o|contracted procedure: k4482 
o|contracted procedure: k4496 
o|contracted procedure: k4529 
o|contracted procedure: k4542 
o|contracted procedure: k4550 
o|contracted procedure: k4554 
o|contracted procedure: k4560 
o|contracted procedure: k4563 
o|contracted procedure: k4566 
o|contracted procedure: k4579 
o|contracted procedure: k4583 
o|contracted procedure: k4595 
o|contracted procedure: k4602 
o|contracted procedure: k4605 
o|contracted procedure: k4617 
o|contracted procedure: k4620 
o|contracted procedure: k4623 
o|contracted procedure: k4631 
o|contracted procedure: k4639 
o|contracted procedure: k4648 
o|contracted procedure: k4651 
o|contracted procedure: k4793 
o|contracted procedure: k4654 
o|contracted procedure: k4662 
o|contracted procedure: k4665 
o|contracted procedure: k4676 
o|contracted procedure: k4683 
o|contracted procedure: k4689 
o|contracted procedure: k4693 
o|contracted procedure: k4739 
o|contracted procedure: k4696 
o|contracted procedure: k4699 
o|contracted procedure: k4711 
o|contracted procedure: k4714 
o|contracted procedure: k4717 
o|contracted procedure: k4725 
o|contracted procedure: k4733 
o|contracted procedure: k4749 
o|contracted procedure: k4752 
o|contracted procedure: k4764 
o|contracted procedure: k4767 
o|contracted procedure: k4770 
o|contracted procedure: k4778 
o|contracted procedure: k4786 
o|contracted procedure: k4800 
o|contracted procedure: k4803 
o|contracted procedure: k4815 
o|contracted procedure: k4818 
o|contracted procedure: k4821 
o|contracted procedure: k4829 
o|contracted procedure: k4837 
o|contracted procedure: k4888 
o|contracted procedure: k2120 
o|contracted procedure: k2132 
o|contracted procedure: k2155 
o|contracted procedure: k4902 
o|contracted procedure: k4906 
o|contracted procedure: k4897 
o|contracted procedure: k2137 
o|contracted procedure: k2163 
o|simplifications: ((let . 93)) 
o|removed binding forms: 276 
o|substituted constant variable: r31525687 
o|substituted constant variable: r31525687 
o|inlining procedure: k3316 
o|inlining procedure: k3316 
o|inlining procedure: k3860 
o|inlining procedure: k3860 
o|inlining procedure: k3860 
o|inlining procedure: k3860 
o|removed binding forms: 2 
o|removed conditional forms: 1 
o|substituted constant variable: r33175721 
o|simplifications: ((let . 1)) 
o|removed binding forms: 3 
o|removed conditional forms: 1 
o|removed binding forms: 1 
o|direct leaf routine/allocation: loop477 0 
o|direct leaf routine/allocation: merge544 0 
o|direct leaf routine/allocation: report545 35 
o|direct leaf routine/allocation: floatvar?552 0 
o|direct leaf routine/allocation: a3201 0 
o|direct leaf routine/allocation: loop644 0 
o|direct leaf routine/allocation: g278279 3 
o|converted assignments to bindings: (loop477) 
o|contracted procedure: "(lfa2.scm:294) k3020" 
o|contracted procedure: "(mini-srfi-1.scm:141) k2179" 
o|converted assignments to bindings: (loop644) 
o|contracted procedure: "(lfa2.scm:353) k3326" 
o|contracted procedure: "(lfa2.scm:377) k3551" 
o|simplifications: ((let . 2)) 
o|removed binding forms: 4 
o|direct leaf routine/allocation: a2172 0 
o|contracted procedure: "(mini-srfi-1.scm:131) k2096" 
o|removed binding forms: 1 
o|direct leaf routine/allocation: g250251 3 
o|customizable procedures: (foldr263266 map-loop10981115 map-loop10721089 g10311032 map-loop10391056 map-loop9981015 map-loop958975 g924925 map-loop931948 chicken.compiler.lfa2#posq for-each-loop850870 g825832 for-each-loop824842 g737744 for-each-loop736748 g810811 k4045 g783784 k3931 g774775 g759760 k3683 extinguish!549 k3588 k3521 add-unboxed556 k3470 assigned?546 k3477 k3356 walk558 eliminate-floatvar553 add-boxed555 count-floatvar554 k3217 g677678 foldr245248 g612619 for-each-loop611622 droppable?547 drop!548 sub-boxed557) 
o|calls to known targets: 110 
o|identified direct recursive calls: f_2683 1 
o|identified direct recursive calls: f_2083 1 
o|identified direct recursive calls: f_3143 2 
o|identified direct recursive calls: f_2127 1 
o|fast box initializations: 27 
o|fast global references: 6 
o|fast global assignments: 2 
o|dropping unused closure argument: f_2677 
o|dropping unused closure argument: f_2879 
*/
/* end of file */
