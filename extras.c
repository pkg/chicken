/* Generated from extras.scm by the CHICKEN compiler
   http://www.call-cc.org
   Version 5.2.0 (rev 317468e4)
   linux-unix-gnu-x86-64 [ 64bit dload ptables ]
   command line: extras.scm -optimize-level 2 -include-path . -include-path ./ -inline -ignore-repository -feature chicken-bootstrap -no-warnings -specialize -consult-types-file ./types.db -explicit-use -no-trace -output-file extras.c -emit-import-library chicken.format -emit-import-library chicken.io -emit-import-library chicken.pretty-print -emit-import-library chicken.random
   unit: extras
   uses: data-structures library
*/
#include "chicken.h"

static C_PTABLE_ENTRY *create_ptable(void);
C_noret_decl(C_data_2dstructures_toplevel)
C_externimport void C_ccall C_data_2dstructures_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_library_toplevel)
C_externimport void C_ccall C_library_toplevel(C_word c,C_word *av) C_noret;

static C_TLS C_word lf[165];
static double C_possibly_force_alignment;
static C_char C_TLS li0[] C_aligned={C_lihdr(0,0,17),40,100,111,108,111,111,112,57,54,32,120,32,105,32,120,115,41,0,0,0,0,0,0,0};
static C_char C_TLS li1[] C_aligned={C_lihdr(0,0,29),40,99,104,105,99,107,101,110,46,105,111,35,114,101,97,100,45,108,105,115,116,32,46,32,114,101,115,116,41,0,0,0};
static C_char C_TLS li2[] C_aligned={C_lihdr(0,0,9),40,103,49,50,48,32,114,108,41,0,0,0,0,0,0,0};
static C_char C_TLS li3[] C_aligned={C_lihdr(0,0,8),40,108,111,111,112,32,105,41};
static C_char C_TLS li4[] C_aligned={C_lihdr(0,0,29),40,99,104,105,99,107,101,110,46,105,111,35,114,101,97,100,45,108,105,110,101,32,46,32,97,114,103,115,41,0,0,0};
static C_char C_TLS li5[] C_aligned={C_lihdr(0,0,12),40,108,111,111,112,32,108,110,115,32,110,41,0,0,0,0};
static C_char C_TLS li6[] C_aligned={C_lihdr(0,0,30),40,99,104,105,99,107,101,110,46,105,111,35,114,101,97,100,45,108,105,110,101,115,32,46,32,114,101,115,116,41,0,0};
static C_char C_TLS li7[] C_aligned={C_lihdr(0,0,34),40,99,104,105,99,107,101,110,46,105,111,35,119,114,105,116,101,45,108,105,110,101,32,115,116,114,32,46,32,112,111,114,116,41,0,0,0,0,0,0};
static C_char C_TLS li8[] C_aligned={C_lihdr(0,0,16),40,108,111,111,112,32,115,116,97,114,116,32,110,32,109,41};
static C_char C_TLS li9[] C_aligned={C_lihdr(0,0,16),40,108,111,111,112,32,115,116,97,114,116,32,110,32,109,41};
static C_char C_TLS li10[] C_aligned={C_lihdr(0,0,48),40,99,104,105,99,107,101,110,46,105,111,35,114,101,97,100,45,115,116,114,105,110,103,33,47,112,111,114,116,32,110,32,100,101,115,116,32,112,111,114,116,32,115,116,97,114,116,41};
static C_char C_TLS li11[] C_aligned={C_lihdr(0,0,39),40,99,104,105,99,107,101,110,46,105,111,35,114,101,97,100,45,115,116,114,105,110,103,33,32,110,32,100,101,115,116,32,46,32,114,101,115,116,41,0};
static C_char C_TLS li12[] C_aligned={C_lihdr(0,0,6),40,108,111,111,112,41,0,0};
static C_char C_TLS li13[] C_aligned={C_lihdr(0,0,33),40,99,104,105,99,107,101,110,46,105,111,35,114,101,97,100,45,115,116,114,105,110,103,47,112,111,114,116,32,110,32,112,41,0,0,0,0,0,0,0};
static C_char C_TLS li14[] C_aligned={C_lihdr(0,0,31),40,99,104,105,99,107,101,110,46,105,111,35,114,101,97,100,45,115,116,114,105,110,103,32,46,32,114,101,115,116,41,0};
static C_char C_TLS li15[] C_aligned={C_lihdr(0,0,33),40,99,104,105,99,107,101,110,46,105,111,35,114,101,97,100,45,98,117,102,102,101,114,101,100,32,46,32,114,101,115,116,41,0,0,0,0,0,0,0};
static C_char C_TLS li16[] C_aligned={C_lihdr(0,0,6),40,108,111,111,112,41,0,0};
static C_char C_TLS li17[] C_aligned={C_lihdr(0,0,35),40,99,104,105,99,107,101,110,46,105,111,35,114,101,97,100,45,116,111,107,101,110,32,112,114,101,100,32,46,32,112,111,114,116,41,0,0,0,0,0};
static C_char C_TLS li18[] C_aligned={C_lihdr(0,0,16),40,98,111,100,121,51,49,53,32,110,32,112,111,114,116,41};
static C_char C_TLS li19[] C_aligned={C_lihdr(0,0,16),40,100,101,102,45,112,111,114,116,51,49,56,32,37,110,41};
static C_char C_TLS li20[] C_aligned={C_lihdr(0,0,10),40,100,101,102,45,110,51,49,55,41,0,0,0,0,0,0};
static C_char C_TLS li21[] C_aligned={C_lihdr(0,0,34),40,99,104,105,99,107,101,110,46,105,111,35,119,114,105,116,101,45,115,116,114,105,110,103,32,115,32,46,32,109,111,114,101,41,0,0,0,0,0,0};
static C_char C_TLS li22[] C_aligned={C_lihdr(0,0,29),40,99,104,105,99,107,101,110,46,105,111,35,114,101,97,100,45,98,121,116,101,32,46,32,114,101,115,116,41,0,0,0};
static C_char C_TLS li23[] C_aligned={C_lihdr(0,0,35),40,99,104,105,99,107,101,110,46,105,111,35,119,114,105,116,101,45,98,121,116,101,32,98,121,116,101,32,46,32,114,101,115,116,41,0,0,0,0,0};
static C_char C_TLS li24[] C_aligned={C_lihdr(0,0,15),40,114,101,97,100,45,109,97,99,114,111,63,32,108,41,0};
static C_char C_TLS li25[] C_aligned={C_lihdr(0,0,19),40,114,101,97,100,45,109,97,99,114,111,45,112,114,101,102,105,120,41,0,0,0,0,0};
static C_char C_TLS li26[] C_aligned={C_lihdr(0,0,13),40,111,117,116,32,115,116,114,32,99,111,108,41,0,0,0};
static C_char C_TLS li27[] C_aligned={C_lihdr(0,0,18),40,119,114,45,101,120,112,114,32,101,120,112,114,32,99,111,108,41,0,0,0,0,0,0};
static C_char C_TLS li28[] C_aligned={C_lihdr(0,0,12),40,108,111,111,112,32,108,32,99,111,108,41,0,0,0,0};
static C_char C_TLS li29[] C_aligned={C_lihdr(0,0,14),40,119,114,45,108,115,116,32,108,32,99,111,108,41,0,0};
static C_char C_TLS li30[] C_aligned={C_lihdr(0,0,8),40,103,53,48,48,32,97,41};
static C_char C_TLS li31[] C_aligned={C_lihdr(0,0,14),40,108,111,111,112,32,105,32,106,32,99,111,108,41,0,0};
static C_char C_TLS li32[] C_aligned={C_lihdr(0,0,9),40,103,53,49,49,32,99,110,41,0,0,0,0,0,0,0};
static C_char C_TLS li33[] C_aligned={C_lihdr(0,0,13),40,100,111,108,111,111,112,53,50,48,32,105,41,0,0,0};
static C_char C_TLS li34[] C_aligned={C_lihdr(0,0,12),40,119,114,32,111,98,106,32,99,111,108,41,0,0,0,0};
static C_char C_TLS li35[] C_aligned={C_lihdr(0,0,14),40,115,112,97,99,101,115,32,110,32,99,111,108,41,0,0};
static C_char C_TLS li36[] C_aligned={C_lihdr(0,0,15),40,105,110,100,101,110,116,32,116,111,32,99,111,108,41,0};
static C_char C_TLS li37[] C_aligned={C_lihdr(0,0,11),40,97,50,56,52,54,32,115,116,114,41,0,0,0,0,0};
static C_char C_TLS li38[] C_aligned={C_lihdr(0,0,26),40,112,114,32,111,98,106,32,99,111,108,32,101,120,116,114,97,32,112,112,45,112,97,105,114,41,0,0,0,0,0,0};
static C_char C_TLS li39[] C_aligned={C_lihdr(0,0,24),40,112,112,45,101,120,112,114,32,101,120,112,114,32,99,111,108,32,101,120,116,114,97,41};
static C_char C_TLS li40[] C_aligned={C_lihdr(0,0,32),40,112,112,45,99,97,108,108,32,101,120,112,114,32,99,111,108,32,101,120,116,114,97,32,112,112,45,105,116,101,109,41};
static C_char C_TLS li41[] C_aligned={C_lihdr(0,0,29),40,112,112,45,108,105,115,116,32,108,32,99,111,108,32,101,120,116,114,97,32,112,112,45,105,116,101,109,41,0,0,0};
static C_char C_TLS li42[] C_aligned={C_lihdr(0,0,12),40,108,111,111,112,32,108,32,99,111,108,41,0,0,0,0};
static C_char C_TLS li43[] C_aligned={C_lihdr(0,0,35),40,112,112,45,100,111,119,110,32,108,32,99,111,108,49,32,99,111,108,50,32,101,120,116,114,97,32,112,112,45,105,116,101,109,41,0,0,0,0,0};
static C_char C_TLS li44[] C_aligned={C_lihdr(0,0,27),40,116,97,105,108,49,32,114,101,115,116,32,99,111,108,49,32,99,111,108,50,32,99,111,108,51,41,0,0,0,0,0};
static C_char C_TLS li45[] C_aligned={C_lihdr(0,0,27),40,116,97,105,108,50,32,114,101,115,116,32,99,111,108,49,32,99,111,108,50,32,99,111,108,51,41,0,0,0,0,0};
static C_char C_TLS li46[] C_aligned={C_lihdr(0,0,22),40,116,97,105,108,51,32,114,101,115,116,32,99,111,108,49,32,99,111,108,50,41,0,0};
static C_char C_TLS li47[] C_aligned={C_lihdr(0,0,49),40,112,112,45,103,101,110,101,114,97,108,32,101,120,112,114,32,99,111,108,32,101,120,116,114,97,32,110,97,109,101,100,63,32,112,112,45,49,32,112,112,45,50,32,112,112,45,51,41,0,0,0,0,0,0,0};
static C_char C_TLS li48[] C_aligned={C_lihdr(0,0,26),40,112,112,45,101,120,112,114,45,108,105,115,116,32,108,32,99,111,108,32,101,120,116,114,97,41,0,0,0,0,0,0};
static C_char C_TLS li49[] C_aligned={C_lihdr(0,0,26),40,112,112,45,108,97,109,98,100,97,32,101,120,112,114,32,99,111,108,32,101,120,116,114,97,41,0,0,0,0,0,0};
static C_char C_TLS li50[] C_aligned={C_lihdr(0,0,22),40,112,112,45,105,102,32,101,120,112,114,32,99,111,108,32,101,120,116,114,97,41,0,0};
static C_char C_TLS li51[] C_aligned={C_lihdr(0,0,24),40,112,112,45,99,111,110,100,32,101,120,112,114,32,99,111,108,32,101,120,116,114,97,41};
static C_char C_TLS li52[] C_aligned={C_lihdr(0,0,24),40,112,112,45,99,97,115,101,32,101,120,112,114,32,99,111,108,32,101,120,116,114,97,41};
static C_char C_TLS li53[] C_aligned={C_lihdr(0,0,23),40,112,112,45,97,110,100,32,101,120,112,114,32,99,111,108,32,101,120,116,114,97,41,0};
static C_char C_TLS li54[] C_aligned={C_lihdr(0,0,23),40,112,112,45,108,101,116,32,101,120,112,114,32,99,111,108,32,101,120,116,114,97,41,0};
static C_char C_TLS li55[] C_aligned={C_lihdr(0,0,25),40,112,112,45,98,101,103,105,110,32,101,120,112,114,32,99,111,108,32,101,120,116,114,97,41,0,0,0,0,0,0,0};
static C_char C_TLS li56[] C_aligned={C_lihdr(0,0,22),40,112,112,45,100,111,32,101,120,112,114,32,99,111,108,32,101,120,116,114,97,41,0,0};
static C_char C_TLS li57[] C_aligned={C_lihdr(0,0,12),40,115,116,121,108,101,32,104,101,97,100,41,0,0,0,0};
static C_char C_TLS li58[] C_aligned={C_lihdr(0,0,12),40,112,112,32,111,98,106,32,99,111,108,41,0,0,0,0};
static C_char C_TLS li59[] C_aligned={C_lihdr(0,0,62),40,99,104,105,99,107,101,110,46,112,114,101,116,116,121,45,112,114,105,110,116,35,103,101,110,101,114,105,99,45,119,114,105,116,101,32,111,98,106,32,100,105,115,112,108,97,121,63,32,119,105,100,116,104,32,111,117,116,112,117,116,41,0,0};
static C_char C_TLS li60[] C_aligned={C_lihdr(0,0,9),40,97,51,52,50,48,32,115,41,0,0,0,0,0,0,0};
static C_char C_TLS li61[] C_aligned={C_lihdr(0,0,45),40,99,104,105,99,107,101,110,46,112,114,101,116,116,121,45,112,114,105,110,116,35,112,114,101,116,116,121,45,112,114,105,110,116,32,111,98,106,32,46,32,111,112,116,41,0,0,0};
static C_char C_TLS li62[] C_aligned={C_lihdr(0,0,6),40,110,101,120,116,41,0,0};
static C_char C_TLS li63[] C_aligned={C_lihdr(0,0,6),40,115,107,105,112,41,0,0};
static C_char C_TLS li64[] C_aligned={C_lihdr(0,0,6),40,108,111,111,112,41,0,0};
static C_char C_TLS li65[] C_aligned={C_lihdr(0,0,14),40,114,101,99,32,109,115,103,32,97,114,103,115,41,0,0};
static C_char C_TLS li66[] C_aligned={C_lihdr(0,0,43),40,99,104,105,99,107,101,110,46,102,111,114,109,97,116,35,102,112,114,105,110,116,102,48,32,108,111,99,32,112,111,114,116,32,109,115,103,32,97,114,103,115,41,0,0,0,0,0};
static C_char C_TLS li67[] C_aligned={C_lihdr(0,0,41),40,99,104,105,99,107,101,110,46,102,111,114,109,97,116,35,102,112,114,105,110,116,102,32,112,111,114,116,32,102,115,116,114,32,46,32,97,114,103,115,41,0,0,0,0,0,0,0};
static C_char C_TLS li68[] C_aligned={C_lihdr(0,0,35),40,99,104,105,99,107,101,110,46,102,111,114,109,97,116,35,112,114,105,110,116,102,32,102,115,116,114,32,46,32,97,114,103,115,41,0,0,0,0,0};
static C_char C_TLS li69[] C_aligned={C_lihdr(0,0,36),40,99,104,105,99,107,101,110,46,102,111,114,109,97,116,35,115,112,114,105,110,116,102,32,102,115,116,114,32,46,32,97,114,103,115,41,0,0,0,0};
static C_char C_TLS li70[] C_aligned={C_lihdr(0,0,41),40,99,104,105,99,107,101,110,46,102,111,114,109,97,116,35,102,111,114,109,97,116,32,102,109,116,45,111,114,45,100,115,116,32,46,32,97,114,103,115,41,0,0,0,0,0,0,0};
static C_char C_TLS li71[] C_aligned={C_lihdr(0,0,51),40,99,104,105,99,107,101,110,46,114,97,110,100,111,109,35,115,101,116,45,112,115,101,117,100,111,45,114,97,110,100,111,109,45,115,101,101,100,33,32,98,117,102,32,46,32,114,101,115,116,41,0,0,0,0,0};
static C_char C_TLS li72[] C_aligned={C_lihdr(0,0,40),40,99,104,105,99,107,101,110,46,114,97,110,100,111,109,35,112,115,101,117,100,111,45,114,97,110,100,111,109,45,105,110,116,101,103,101,114,32,110,41};
static C_char C_TLS li73[] C_aligned={C_lihdr(0,0,35),40,99,104,105,99,107,101,110,46,114,97,110,100,111,109,35,112,115,101,117,100,111,45,114,97,110,100,111,109,45,114,101,97,108,41,0,0,0,0,0};
static C_char C_TLS li74[] C_aligned={C_lihdr(0,0,36),40,99,104,105,99,107,101,110,46,114,97,110,100,111,109,35,114,97,110,100,111,109,45,98,121,116,101,115,32,46,32,114,101,115,116,41,0,0,0,0};
static C_char C_TLS li75[] C_aligned={C_lihdr(0,0,10),40,116,111,112,108,101,118,101,108,41,0,0,0,0,0,0};


C_noret_decl(f_1083)
static void C_ccall f_1083(C_word c,C_word *av) C_noret;
C_noret_decl(f_1086)
static void C_ccall f_1086(C_word c,C_word *av) C_noret;
C_noret_decl(f_1088)
static void C_ccall f_1088(C_word c,C_word *av) C_noret;
C_noret_decl(f_1117)
static void C_ccall f_1117(C_word c,C_word *av) C_noret;
C_noret_decl(f_1119)
static void C_fcall f_1119(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_1139)
static void C_ccall f_1139(C_word c,C_word *av) C_noret;
C_noret_decl(f_1191)
static void C_ccall f_1191(C_word c,C_word *av) C_noret;
C_noret_decl(f_1201)
static void C_fcall f_1201(C_word t0,C_word t1) C_noret;
C_noret_decl(f_1211)
static void C_fcall f_1211(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_1224)
static void C_ccall f_1224(C_word c,C_word *av) C_noret;
C_noret_decl(f_1229)
static void C_fcall f_1229(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_1242)
static void C_ccall f_1242(C_word c,C_word *av) C_noret;
C_noret_decl(f_1275)
static void C_ccall f_1275(C_word c,C_word *av) C_noret;
C_noret_decl(f_1284)
static void C_ccall f_1284(C_word c,C_word *av) C_noret;
C_noret_decl(f_1307)
static void C_ccall f_1307(C_word c,C_word *av) C_noret;
C_noret_decl(f_1315)
static void C_ccall f_1315(C_word c,C_word *av) C_noret;
C_noret_decl(f_1344)
static void C_ccall f_1344(C_word c,C_word *av) C_noret;
C_noret_decl(f_1369)
static void C_fcall f_1369(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_1382)
static void C_ccall f_1382(C_word c,C_word *av) C_noret;
C_noret_decl(f_1428)
static void C_ccall f_1428(C_word c,C_word *av) C_noret;
C_noret_decl(f_1444)
static void C_ccall f_1444(C_word c,C_word *av) C_noret;
C_noret_decl(f_1456)
static void C_ccall f_1456(C_word c,C_word *av) C_noret;
C_noret_decl(f_1474)
static void C_fcall f_1474(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_1478)
static void C_ccall f_1478(C_word c,C_word *av) C_noret;
C_noret_decl(f_1533)
static void C_fcall f_1533(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_1537)
static void C_ccall f_1537(C_word c,C_word *av) C_noret;
C_noret_decl(f_1540)
static void C_fcall f_1540(C_word t0,C_word t1) C_noret;
C_noret_decl(f_1588)
static void C_ccall f_1588(C_word c,C_word *av) C_noret;
C_noret_decl(f_1610)
static void C_ccall f_1610(C_word c,C_word *av) C_noret;
C_noret_decl(f_1616)
static void C_fcall f_1616(C_word t0,C_word t1) C_noret;
C_noret_decl(f_1619)
static void C_ccall f_1619(C_word c,C_word *av) C_noret;
C_noret_decl(f_1665)
static void C_ccall f_1665(C_word c,C_word *av) C_noret;
C_noret_decl(f_1684)
static void C_ccall f_1684(C_word c,C_word *av) C_noret;
C_noret_decl(f_1687)
static void C_ccall f_1687(C_word c,C_word *av) C_noret;
C_noret_decl(f_1699)
static void C_ccall f_1699(C_word c,C_word *av) C_noret;
C_noret_decl(f_1702)
static void C_ccall f_1702(C_word c,C_word *av) C_noret;
C_noret_decl(f_1707)
static void C_fcall f_1707(C_word t0,C_word t1) C_noret;
C_noret_decl(f_1711)
static void C_ccall f_1711(C_word c,C_word *av) C_noret;
C_noret_decl(f_1714)
static void C_ccall f_1714(C_word c,C_word *av) C_noret;
C_noret_decl(f_1726)
static void C_ccall f_1726(C_word c,C_word *av) C_noret;
C_noret_decl(f_1734)
static void C_ccall f_1734(C_word c,C_word *av) C_noret;
C_noret_decl(f_1736)
static void C_ccall f_1736(C_word c,C_word *av) C_noret;
C_noret_decl(f_1755)
static void C_ccall f_1755(C_word c,C_word *av) C_noret;
C_noret_decl(f_1789)
static void C_ccall f_1789(C_word c,C_word *av) C_noret;
C_noret_decl(f_1817)
static void C_ccall f_1817(C_word c,C_word *av) C_noret;
C_noret_decl(f_1827)
static void C_ccall f_1827(C_word c,C_word *av) C_noret;
C_noret_decl(f_1832)
static void C_fcall f_1832(C_word t0,C_word t1) C_noret;
C_noret_decl(f_1836)
static void C_ccall f_1836(C_word c,C_word *av) C_noret;
C_noret_decl(f_1842)
static void C_ccall f_1842(C_word c,C_word *av) C_noret;
C_noret_decl(f_1845)
static void C_ccall f_1845(C_word c,C_word *av) C_noret;
C_noret_decl(f_1852)
static void C_ccall f_1852(C_word c,C_word *av) C_noret;
C_noret_decl(f_1873)
static void C_ccall f_1873(C_word c,C_word *av) C_noret;
C_noret_decl(f_1878)
static void C_fcall f_1878(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_1885)
static void C_ccall f_1885(C_word c,C_word *av) C_noret;
C_noret_decl(f_1895)
static void C_ccall f_1895(C_word c,C_word *av) C_noret;
C_noret_decl(f_1916)
static void C_fcall f_1916(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_1921)
static void C_fcall f_1921(C_word t0,C_word t1) C_noret;
C_noret_decl(f_1956)
static void C_ccall f_1956(C_word c,C_word *av) C_noret;
C_noret_decl(f_1966)
static void C_ccall f_1966(C_word c,C_word *av) C_noret;
C_noret_decl(f_1983)
static void C_ccall f_1983(C_word c,C_word *av) C_noret;
C_noret_decl(f_1990)
static void C_ccall f_1990(C_word c,C_word *av) C_noret;
C_noret_decl(f_2008)
static void C_fcall f_2008(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_2011)
static void C_fcall f_2011(C_word t0,C_word t1) C_noret;
C_noret_decl(f_2039)
static void C_fcall f_2039(C_word t0,C_word t1) C_noret;
C_noret_decl(f_2073)
static C_word C_fcall f_2073(C_word t0);
C_noret_decl(f_2112)
static void C_fcall f_2112(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_2122)
static void C_ccall f_2122(C_word c,C_word *av) C_noret;
C_noret_decl(f_2131)
static void C_fcall f_2131(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_2134)
static void C_fcall f_2134(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_2141)
static void C_ccall f_2141(C_word c,C_word *av) C_noret;
C_noret_decl(f_2152)
static void C_ccall f_2152(C_word c,C_word *av) C_noret;
C_noret_decl(f_2161)
static void C_fcall f_2161(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_2177)
static void C_ccall f_2177(C_word c,C_word *av) C_noret;
C_noret_decl(f_2179)
static void C_fcall f_2179(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_2201)
static void C_ccall f_2201(C_word c,C_word *av) C_noret;
C_noret_decl(f_2207)
static void C_ccall f_2207(C_word c,C_word *av) C_noret;
C_noret_decl(f_2223)
static void C_ccall f_2223(C_word c,C_word *av) C_noret;
C_noret_decl(f_2227)
static void C_ccall f_2227(C_word c,C_word *av) C_noret;
C_noret_decl(f_2236)
static void C_ccall f_2236(C_word c,C_word *av) C_noret;
C_noret_decl(f_2279)
static void C_ccall f_2279(C_word c,C_word *av) C_noret;
C_noret_decl(f_2283)
static void C_ccall f_2283(C_word c,C_word *av) C_noret;
C_noret_decl(f_2302)
static void C_ccall f_2302(C_word c,C_word *av) C_noret;
C_noret_decl(f_2309)
static void C_ccall f_2309(C_word c,C_word *av) C_noret;
C_noret_decl(f_2315)
static void C_ccall f_2315(C_word c,C_word *av) C_noret;
C_noret_decl(f_2321)
static void C_ccall f_2321(C_word c,C_word *av) C_noret;
C_noret_decl(f_2324)
static void C_ccall f_2324(C_word c,C_word *av) C_noret;
C_noret_decl(f_2331)
static void C_ccall f_2331(C_word c,C_word *av) C_noret;
C_noret_decl(f_2344)
static void C_ccall f_2344(C_word c,C_word *av) C_noret;
C_noret_decl(f_2363)
static void C_ccall f_2363(C_word c,C_word *av) C_noret;
C_noret_decl(f_2365)
static void C_fcall f_2365(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_2393)
static void C_ccall f_2393(C_word c,C_word *av) C_noret;
C_noret_decl(f_2397)
static void C_ccall f_2397(C_word c,C_word *av) C_noret;
C_noret_decl(f_2401)
static void C_ccall f_2401(C_word c,C_word *av) C_noret;
C_noret_decl(f_2424)
static void C_ccall f_2424(C_word c,C_word *av) C_noret;
C_noret_decl(f_2429)
static void C_ccall f_2429(C_word c,C_word *av) C_noret;
C_noret_decl(f_2430)
static void C_fcall f_2430(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_2448)
static void C_ccall f_2448(C_word c,C_word *av) C_noret;
C_noret_decl(f_2452)
static void C_ccall f_2452(C_word c,C_word *av) C_noret;
C_noret_decl(f_2460)
static void C_ccall f_2460(C_word c,C_word *av) C_noret;
C_noret_decl(f_2471)
static void C_ccall f_2471(C_word c,C_word *av) C_noret;
C_noret_decl(f_2489)
static void C_ccall f_2489(C_word c,C_word *av) C_noret;
C_noret_decl(f_2493)
static void C_ccall f_2493(C_word c,C_word *av) C_noret;
C_noret_decl(f_2516)
static void C_ccall f_2516(C_word c,C_word *av) C_noret;
C_noret_decl(f_2524)
static void C_ccall f_2524(C_word c,C_word *av) C_noret;
C_noret_decl(f_2527)
static void C_ccall f_2527(C_word c,C_word *av) C_noret;
C_noret_decl(f_2531)
static void C_fcall f_2531(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_2551)
static void C_ccall f_2551(C_word c,C_word *av) C_noret;
C_noret_decl(f_2558)
static void C_ccall f_2558(C_word c,C_word *av) C_noret;
C_noret_decl(f_2569)
static void C_ccall f_2569(C_word c,C_word *av) C_noret;
C_noret_decl(f_2576)
static void C_ccall f_2576(C_word c,C_word *av) C_noret;
C_noret_decl(f_2592)
static void C_ccall f_2592(C_word c,C_word *av) C_noret;
C_noret_decl(f_2610)
static void C_ccall f_2610(C_word c,C_word *av) C_noret;
C_noret_decl(f_2625)
static void C_ccall f_2625(C_word c,C_word *av) C_noret;
C_noret_decl(f_2628)
static void C_ccall f_2628(C_word c,C_word *av) C_noret;
C_noret_decl(f_2635)
static void C_ccall f_2635(C_word c,C_word *av) C_noret;
C_noret_decl(f_2641)
static void C_ccall f_2641(C_word c,C_word *av) C_noret;
C_noret_decl(f_2648)
static void C_ccall f_2648(C_word c,C_word *av) C_noret;
C_noret_decl(f_2658)
static void C_ccall f_2658(C_word c,C_word *av) C_noret;
C_noret_decl(f_2664)
static void C_ccall f_2664(C_word c,C_word *av) C_noret;
C_noret_decl(f_2669)
static void C_fcall f_2669(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_2682)
static void C_ccall f_2682(C_word c,C_word *av) C_noret;
C_noret_decl(f_2685)
static void C_ccall f_2685(C_word c,C_word *av) C_noret;
C_noret_decl(f_2696)
static void C_ccall f_2696(C_word c,C_word *av) C_noret;
C_noret_decl(f_2708)
static void C_ccall f_2708(C_word c,C_word *av) C_noret;
C_noret_decl(f_2711)
static void C_ccall f_2711(C_word c,C_word *av) C_noret;
C_noret_decl(f_2718)
static void C_ccall f_2718(C_word c,C_word *av) C_noret;
C_noret_decl(f_2726)
static void C_fcall f_2726(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_2729)
static void C_fcall f_2729(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_2753)
static void C_ccall f_2753(C_word c,C_word *av) C_noret;
C_noret_decl(f_2760)
static void C_ccall f_2760(C_word c,C_word *av) C_noret;
C_noret_decl(f_2762)
static void C_fcall f_2762(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_2778)
static void C_ccall f_2778(C_word c,C_word *av) C_noret;
C_noret_decl(f_2785)
static void C_ccall f_2785(C_word c,C_word *av) C_noret;
C_noret_decl(f_2796)
static void C_fcall f_2796(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5) C_noret;
C_noret_decl(f_2809)
static void C_ccall f_2809(C_word c,C_word *av) C_noret;
C_noret_decl(f_2812)
static void C_ccall f_2812(C_word c,C_word *av) C_noret;
C_noret_decl(f_2825)
static void C_ccall f_2825(C_word c,C_word *av) C_noret;
C_noret_decl(f_2841)
static void C_ccall f_2841(C_word c,C_word *av) C_noret;
C_noret_decl(f_2845)
static void C_ccall f_2845(C_word c,C_word *av) C_noret;
C_noret_decl(f_2847)
static void C_ccall f_2847(C_word c,C_word *av) C_noret;
C_noret_decl(f_2883)
static void C_ccall f_2883(C_word c,C_word *av) C_noret;
C_noret_decl(f_2890)
static void C_ccall f_2890(C_word c,C_word *av) C_noret;
C_noret_decl(f_2901)
static void C_ccall f_2901(C_word c,C_word *av) C_noret;
C_noret_decl(f_2917)
static void C_ccall f_2917(C_word c,C_word *av) C_noret;
C_noret_decl(f_2943)
static void C_ccall f_2943(C_word c,C_word *av) C_noret;
C_noret_decl(f_2948)
static void C_fcall f_2948(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5) C_noret;
C_noret_decl(f_2952)
static void C_ccall f_2952(C_word c,C_word *av) C_noret;
C_noret_decl(f_2972)
static void C_ccall f_2972(C_word c,C_word *av) C_noret;
C_noret_decl(f_2974)
static void C_fcall f_2974(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5) C_noret;
C_noret_decl(f_2978)
static void C_ccall f_2978(C_word c,C_word *av) C_noret;
C_noret_decl(f_2983)
static void C_fcall f_2983(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6) C_noret;
C_noret_decl(f_2989)
static void C_fcall f_2989(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_3010)
static void C_ccall f_3010(C_word c,C_word *av) C_noret;
C_noret_decl(f_3016)
static void C_ccall f_3016(C_word c,C_word *av) C_noret;
C_noret_decl(f_3038)
static void C_ccall f_3038(C_word c,C_word *av) C_noret;
C_noret_decl(f_3042)
static void C_ccall f_3042(C_word c,C_word *av) C_noret;
C_noret_decl(f_3050)
static void C_ccall f_3050(C_word c,C_word *av) C_noret;
C_noret_decl(f_3054)
static void C_ccall f_3054(C_word c,C_word *av) C_noret;
C_noret_decl(f_3056)
static void C_fcall f_3056(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6,C_word t7,C_word t8) C_noret;
C_noret_decl(f_3059)
static void C_fcall f_3059(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5) C_noret;
C_noret_decl(f_3080)
static void C_ccall f_3080(C_word c,C_word *av) C_noret;
C_noret_decl(f_3084)
static void C_ccall f_3084(C_word c,C_word *av) C_noret;
C_noret_decl(f_3098)
static void C_fcall f_3098(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5) C_noret;
C_noret_decl(f_3119)
static void C_ccall f_3119(C_word c,C_word *av) C_noret;
C_noret_decl(f_3123)
static void C_ccall f_3123(C_word c,C_word *av) C_noret;
C_noret_decl(f_3137)
static void C_fcall f_3137(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_3148)
static void C_ccall f_3148(C_word c,C_word *av) C_noret;
C_noret_decl(f_3161)
static void C_ccall f_3161(C_word c,C_word *av) C_noret;
C_noret_decl(f_3176)
static void C_ccall f_3176(C_word c,C_word *av) C_noret;
C_noret_decl(f_3194)
static void C_ccall f_3194(C_word c,C_word *av) C_noret;
C_noret_decl(f_3196)
static void C_ccall f_3196(C_word c,C_word *av) C_noret;
C_noret_decl(f_3202)
static void C_ccall f_3202(C_word c,C_word *av) C_noret;
C_noret_decl(f_3208)
static void C_ccall f_3208(C_word c,C_word *av) C_noret;
C_noret_decl(f_3214)
static void C_ccall f_3214(C_word c,C_word *av) C_noret;
C_noret_decl(f_3220)
static void C_ccall f_3220(C_word c,C_word *av) C_noret;
C_noret_decl(f_3226)
static void C_ccall f_3226(C_word c,C_word *av) C_noret;
C_noret_decl(f_3232)
static void C_ccall f_3232(C_word c,C_word *av) C_noret;
C_noret_decl(f_3252)
static void C_ccall f_3252(C_word c,C_word *av) C_noret;
C_noret_decl(f_3258)
static void C_ccall f_3258(C_word c,C_word *av) C_noret;
C_noret_decl(f_3267)
static void C_fcall f_3267(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3277)
static void C_fcall f_3277(C_word t0,C_word t1) C_noret;
C_noret_decl(f_3393)
static void C_ccall f_3393(C_word c,C_word *av) C_noret;
C_noret_decl(f_3397)
static void C_ccall f_3397(C_word c,C_word *av) C_noret;
C_noret_decl(f_3406)
static void C_ccall f_3406(C_word c,C_word *av) C_noret;
C_noret_decl(f_3408)
static void C_ccall f_3408(C_word c,C_word *av) C_noret;
C_noret_decl(f_3415)
static void C_ccall f_3415(C_word c,C_word *av) C_noret;
C_noret_decl(f_3419)
static void C_ccall f_3419(C_word c,C_word *av) C_noret;
C_noret_decl(f_3421)
static void C_ccall f_3421(C_word c,C_word *av) C_noret;
C_noret_decl(f_3425)
static void C_ccall f_3425(C_word c,C_word *av) C_noret;
C_noret_decl(f_3432)
static void C_fcall f_3432(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_3439)
static void C_ccall f_3439(C_word c,C_word *av) C_noret;
C_noret_decl(f_3442)
static void C_ccall f_3442(C_word c,C_word *av) C_noret;
C_noret_decl(f_3464)
static void C_ccall f_3464(C_word c,C_word *av) C_noret;
C_noret_decl(f_3470)
static void C_fcall f_3470(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_3479)
static C_word C_fcall f_3479(C_word t0);
C_noret_decl(f_3486)
static void C_fcall f_3486(C_word t0,C_word t1) C_noret;
C_noret_decl(f_3505)
static void C_fcall f_3505(C_word t0,C_word t1) C_noret;
C_noret_decl(f_3518)
static void C_ccall f_3518(C_word c,C_word *av) C_noret;
C_noret_decl(f_3543)
static void C_ccall f_3543(C_word c,C_word *av) C_noret;
C_noret_decl(f_3556)
static void C_ccall f_3556(C_word c,C_word *av) C_noret;
C_noret_decl(f_3569)
static void C_ccall f_3569(C_word c,C_word *av) C_noret;
C_noret_decl(f_3582)
static void C_ccall f_3582(C_word c,C_word *av) C_noret;
C_noret_decl(f_3586)
static void C_ccall f_3586(C_word c,C_word *av) C_noret;
C_noret_decl(f_3599)
static void C_ccall f_3599(C_word c,C_word *av) C_noret;
C_noret_decl(f_3603)
static void C_ccall f_3603(C_word c,C_word *av) C_noret;
C_noret_decl(f_3616)
static void C_ccall f_3616(C_word c,C_word *av) C_noret;
C_noret_decl(f_3620)
static void C_ccall f_3620(C_word c,C_word *av) C_noret;
C_noret_decl(f_3638)
static void C_ccall f_3638(C_word c,C_word *av) C_noret;
C_noret_decl(f_3641)
static void C_ccall f_3641(C_word c,C_word *av) C_noret;
C_noret_decl(f_3647)
static void C_ccall f_3647(C_word c,C_word *av) C_noret;
C_noret_decl(f_3683)
static C_word C_fcall f_3683(C_word t0,C_word t1);
C_noret_decl(f_3741)
static void C_ccall f_3741(C_word c,C_word *av) C_noret;
C_noret_decl(f_3752)
static void C_ccall f_3752(C_word c,C_word *av) C_noret;
C_noret_decl(f_3758)
static void C_ccall f_3758(C_word c,C_word *av) C_noret;
C_noret_decl(f_3764)
static void C_ccall f_3764(C_word c,C_word *av) C_noret;
C_noret_decl(f_3770)
static void C_ccall f_3770(C_word c,C_word *av) C_noret;
C_noret_decl(f_3778)
static void C_ccall f_3778(C_word c,C_word *av) C_noret;
C_noret_decl(f_3803)
static void C_ccall f_3803(C_word c,C_word *av) C_noret;
C_noret_decl(f_3813)
static void C_ccall f_3813(C_word c,C_word *av) C_noret;
C_noret_decl(f_3815)
static void C_ccall f_3815(C_word c,C_word *av) C_noret;
C_noret_decl(f_3822)
static void C_ccall f_3822(C_word c,C_word *av) C_noret;
C_noret_decl(f_3825)
static void C_ccall f_3825(C_word c,C_word *av) C_noret;
C_noret_decl(f_3836)
static void C_ccall f_3836(C_word c,C_word *av) C_noret;
C_noret_decl(f_3854)
static void C_ccall f_3854(C_word c,C_word *av) C_noret;
C_noret_decl(f_3870)
static void C_ccall f_3870(C_word c,C_word *av) C_noret;
C_noret_decl(f_3873)
static void C_ccall f_3873(C_word c,C_word *av) C_noret;
C_noret_decl(f_3889)
static void C_ccall f_3889(C_word c,C_word *av) C_noret;
C_noret_decl(f_3892)
static void C_ccall f_3892(C_word c,C_word *av) C_noret;
C_noret_decl(f_3899)
static void C_ccall f_3899(C_word c,C_word *av) C_noret;
C_noret_decl(f_3914)
static void C_ccall f_3914(C_word c,C_word *av) C_noret;
C_noret_decl(f_3937)
static void C_ccall f_3937(C_word c,C_word *av) C_noret;
C_noret_decl(C_extras_toplevel)
C_externexport void C_ccall C_extras_toplevel(C_word c,C_word *av) C_noret;

C_noret_decl(trf_1119)
static void C_ccall trf_1119(C_word c,C_word *av) C_noret;
static void C_ccall trf_1119(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_1119(t0,t1,t2,t3,t4);}

C_noret_decl(trf_1201)
static void C_ccall trf_1201(C_word c,C_word *av) C_noret;
static void C_ccall trf_1201(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_1201(t0,t1);}

C_noret_decl(trf_1211)
static void C_ccall trf_1211(C_word c,C_word *av) C_noret;
static void C_ccall trf_1211(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_1211(t0,t1,t2);}

C_noret_decl(trf_1229)
static void C_ccall trf_1229(C_word c,C_word *av) C_noret;
static void C_ccall trf_1229(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_1229(t0,t1,t2);}

C_noret_decl(trf_1369)
static void C_ccall trf_1369(C_word c,C_word *av) C_noret;
static void C_ccall trf_1369(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_1369(t0,t1,t2,t3);}

C_noret_decl(trf_1474)
static void C_ccall trf_1474(C_word c,C_word *av) C_noret;
static void C_ccall trf_1474(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_1474(t0,t1,t2,t3,t4);}

C_noret_decl(trf_1533)
static void C_ccall trf_1533(C_word c,C_word *av) C_noret;
static void C_ccall trf_1533(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_1533(t0,t1,t2,t3,t4);}

C_noret_decl(trf_1540)
static void C_ccall trf_1540(C_word c,C_word *av) C_noret;
static void C_ccall trf_1540(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_1540(t0,t1);}

C_noret_decl(trf_1616)
static void C_ccall trf_1616(C_word c,C_word *av) C_noret;
static void C_ccall trf_1616(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_1616(t0,t1);}

C_noret_decl(trf_1707)
static void C_ccall trf_1707(C_word c,C_word *av) C_noret;
static void C_ccall trf_1707(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_1707(t0,t1);}

C_noret_decl(trf_1832)
static void C_ccall trf_1832(C_word c,C_word *av) C_noret;
static void C_ccall trf_1832(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_1832(t0,t1);}

C_noret_decl(trf_1878)
static void C_ccall trf_1878(C_word c,C_word *av) C_noret;
static void C_ccall trf_1878(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_1878(t0,t1,t2,t3);}

C_noret_decl(trf_1916)
static void C_ccall trf_1916(C_word c,C_word *av) C_noret;
static void C_ccall trf_1916(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_1916(t0,t1,t2);}

C_noret_decl(trf_1921)
static void C_ccall trf_1921(C_word c,C_word *av) C_noret;
static void C_ccall trf_1921(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_1921(t0,t1);}

C_noret_decl(trf_2008)
static void C_ccall trf_2008(C_word c,C_word *av) C_noret;
static void C_ccall trf_2008(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_2008(t0,t1,t2,t3,t4);}

C_noret_decl(trf_2011)
static void C_ccall trf_2011(C_word c,C_word *av) C_noret;
static void C_ccall trf_2011(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_2011(t0,t1);}

C_noret_decl(trf_2039)
static void C_ccall trf_2039(C_word c,C_word *av) C_noret;
static void C_ccall trf_2039(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_2039(t0,t1);}

C_noret_decl(trf_2112)
static void C_ccall trf_2112(C_word c,C_word *av) C_noret;
static void C_ccall trf_2112(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_2112(t0,t1,t2,t3);}

C_noret_decl(trf_2131)
static void C_ccall trf_2131(C_word c,C_word *av) C_noret;
static void C_ccall trf_2131(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_2131(t0,t1,t2,t3);}

C_noret_decl(trf_2134)
static void C_ccall trf_2134(C_word c,C_word *av) C_noret;
static void C_ccall trf_2134(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_2134(t0,t1,t2,t3);}

C_noret_decl(trf_2161)
static void C_ccall trf_2161(C_word c,C_word *av) C_noret;
static void C_ccall trf_2161(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_2161(t0,t1,t2,t3);}

C_noret_decl(trf_2179)
static void C_ccall trf_2179(C_word c,C_word *av) C_noret;
static void C_ccall trf_2179(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_2179(t0,t1,t2,t3);}

C_noret_decl(trf_2365)
static void C_ccall trf_2365(C_word c,C_word *av) C_noret;
static void C_ccall trf_2365(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_2365(t0,t1,t2,t3,t4);}

C_noret_decl(trf_2430)
static void C_ccall trf_2430(C_word c,C_word *av) C_noret;
static void C_ccall trf_2430(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_2430(t0,t1,t2);}

C_noret_decl(trf_2531)
static void C_ccall trf_2531(C_word c,C_word *av) C_noret;
static void C_ccall trf_2531(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_2531(t0,t1,t2);}

C_noret_decl(trf_2669)
static void C_ccall trf_2669(C_word c,C_word *av) C_noret;
static void C_ccall trf_2669(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_2669(t0,t1,t2);}

C_noret_decl(trf_2726)
static void C_ccall trf_2726(C_word c,C_word *av) C_noret;
static void C_ccall trf_2726(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_2726(t0,t1,t2,t3);}

C_noret_decl(trf_2729)
static void C_ccall trf_2729(C_word c,C_word *av) C_noret;
static void C_ccall trf_2729(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_2729(t0,t1,t2,t3);}

C_noret_decl(trf_2762)
static void C_ccall trf_2762(C_word c,C_word *av) C_noret;
static void C_ccall trf_2762(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_2762(t0,t1,t2,t3);}

C_noret_decl(trf_2796)
static void C_ccall trf_2796(C_word c,C_word *av) C_noret;
static void C_ccall trf_2796(C_word c,C_word *av){
C_word t0=av[5];
C_word t1=av[4];
C_word t2=av[3];
C_word t3=av[2];
C_word t4=av[1];
C_word t5=av[0];
f_2796(t0,t1,t2,t3,t4,t5);}

C_noret_decl(trf_2948)
static void C_ccall trf_2948(C_word c,C_word *av) C_noret;
static void C_ccall trf_2948(C_word c,C_word *av){
C_word t0=av[5];
C_word t1=av[4];
C_word t2=av[3];
C_word t3=av[2];
C_word t4=av[1];
C_word t5=av[0];
f_2948(t0,t1,t2,t3,t4,t5);}

C_noret_decl(trf_2974)
static void C_ccall trf_2974(C_word c,C_word *av) C_noret;
static void C_ccall trf_2974(C_word c,C_word *av){
C_word t0=av[5];
C_word t1=av[4];
C_word t2=av[3];
C_word t3=av[2];
C_word t4=av[1];
C_word t5=av[0];
f_2974(t0,t1,t2,t3,t4,t5);}

C_noret_decl(trf_2983)
static void C_ccall trf_2983(C_word c,C_word *av) C_noret;
static void C_ccall trf_2983(C_word c,C_word *av){
C_word t0=av[6];
C_word t1=av[5];
C_word t2=av[4];
C_word t3=av[3];
C_word t4=av[2];
C_word t5=av[1];
C_word t6=av[0];
f_2983(t0,t1,t2,t3,t4,t5,t6);}

C_noret_decl(trf_2989)
static void C_ccall trf_2989(C_word c,C_word *av) C_noret;
static void C_ccall trf_2989(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_2989(t0,t1,t2,t3);}

C_noret_decl(trf_3056)
static void C_ccall trf_3056(C_word c,C_word *av) C_noret;
static void C_ccall trf_3056(C_word c,C_word *av){
C_word t0=av[8];
C_word t1=av[7];
C_word t2=av[6];
C_word t3=av[5];
C_word t4=av[4];
C_word t5=av[3];
C_word t6=av[2];
C_word t7=av[1];
C_word t8=av[0];
f_3056(t0,t1,t2,t3,t4,t5,t6,t7,t8);}

C_noret_decl(trf_3059)
static void C_ccall trf_3059(C_word c,C_word *av) C_noret;
static void C_ccall trf_3059(C_word c,C_word *av){
C_word t0=av[5];
C_word t1=av[4];
C_word t2=av[3];
C_word t3=av[2];
C_word t4=av[1];
C_word t5=av[0];
f_3059(t0,t1,t2,t3,t4,t5);}

C_noret_decl(trf_3098)
static void C_ccall trf_3098(C_word c,C_word *av) C_noret;
static void C_ccall trf_3098(C_word c,C_word *av){
C_word t0=av[5];
C_word t1=av[4];
C_word t2=av[3];
C_word t3=av[2];
C_word t4=av[1];
C_word t5=av[0];
f_3098(t0,t1,t2,t3,t4,t5);}

C_noret_decl(trf_3137)
static void C_ccall trf_3137(C_word c,C_word *av) C_noret;
static void C_ccall trf_3137(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_3137(t0,t1,t2,t3,t4);}

C_noret_decl(trf_3267)
static void C_ccall trf_3267(C_word c,C_word *av) C_noret;
static void C_ccall trf_3267(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3267(t0,t1,t2);}

C_noret_decl(trf_3277)
static void C_ccall trf_3277(C_word c,C_word *av) C_noret;
static void C_ccall trf_3277(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_3277(t0,t1);}

C_noret_decl(trf_3432)
static void C_ccall trf_3432(C_word c,C_word *av) C_noret;
static void C_ccall trf_3432(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_3432(t0,t1,t2,t3,t4);}

C_noret_decl(trf_3470)
static void C_ccall trf_3470(C_word c,C_word *av) C_noret;
static void C_ccall trf_3470(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_3470(t0,t1,t2,t3);}

C_noret_decl(trf_3486)
static void C_ccall trf_3486(C_word c,C_word *av) C_noret;
static void C_ccall trf_3486(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_3486(t0,t1);}

C_noret_decl(trf_3505)
static void C_ccall trf_3505(C_word c,C_word *av) C_noret;
static void C_ccall trf_3505(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_3505(t0,t1);}

/* k1081 */
static void C_ccall f_1083(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_1083,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1086,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_library_toplevel(2,av2);}}

/* k1084 in k1081 */
static void C_ccall f_1086(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(70,c,6)))){
C_save_and_reclaim((void *)f_1086,c,av);}
a=C_alloc(70);
t2=C_a_i_provide(&a,1,lf[0]);
t3=C_a_i_provide(&a,1,lf[1]);
t4=*((C_word*)lf[2]+1);
t5=C_mutate((C_word*)lf[3]+1 /* (set! chicken.io#read-list ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1088,a[2]=t4,a[3]=((C_word)li1),tmp=(C_word)a,a+=4,tmp));
t6=C_mutate((C_word*)lf[7]+1 /* (set! chicken.io#read-line ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1191,a[2]=((C_word)li4),tmp=(C_word)a,a+=3,tmp));
t7=C_mutate((C_word*)lf[15]+1 /* (set! chicken.io#read-lines ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1344,a[2]=((C_word)li6),tmp=(C_word)a,a+=3,tmp));
t8=C_mutate((C_word*)lf[18]+1 /* (set! chicken.io#write-line ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1428,a[2]=((C_word)li7),tmp=(C_word)a,a+=3,tmp));
t9=C_mutate((C_word*)lf[22]+1 /* (set! chicken.io#read-string!/port ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1456,a[2]=((C_word)li10),tmp=(C_word)a,a+=3,tmp));
t10=C_mutate((C_word*)lf[23]+1 /* (set! chicken.io#read-string! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1588,a[2]=((C_word)li11),tmp=(C_word)a,a+=3,tmp));
t11=C_mutate((C_word*)lf[26]+1 /* (set! chicken.io#read-string/port ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1665,a[2]=((C_word)li13),tmp=(C_word)a,a+=3,tmp));
t12=C_mutate((C_word*)lf[32]+1 /* (set! chicken.io#read-string ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1736,a[2]=((C_word)li14),tmp=(C_word)a,a+=3,tmp));
t13=C_mutate((C_word*)lf[26]+1 /* (set! chicken.io#read-string/port ...) */,*((C_word*)lf[26]+1));
t14=C_mutate((C_word*)lf[22]+1 /* (set! chicken.io#read-string!/port ...) */,*((C_word*)lf[22]+1));
t15=C_mutate((C_word*)lf[34]+1 /* (set! chicken.io#read-buffered ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1789,a[2]=((C_word)li15),tmp=(C_word)a,a+=3,tmp));
t16=C_mutate((C_word*)lf[37]+1 /* (set! chicken.io#read-token ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1817,a[2]=((C_word)li17),tmp=(C_word)a,a+=3,tmp));
t17=C_mutate((C_word*)lf[29]+1 /* (set! chicken.io#write-string ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1873,a[2]=((C_word)li21),tmp=(C_word)a,a+=3,tmp));
t18=C_mutate((C_word*)lf[40]+1 /* (set! chicken.io#read-byte ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1956,a[2]=((C_word)li22),tmp=(C_word)a,a+=3,tmp));
t19=C_mutate((C_word*)lf[42]+1 /* (set! chicken.io#write-byte ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1983,a[2]=((C_word)li23),tmp=(C_word)a,a+=3,tmp));
t20=C_a_i_provide(&a,1,lf[44]);
t21=C_mutate(&lf[45] /* (set! chicken.pretty-print#generic-write ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2008,a[2]=((C_word)li59),tmp=(C_word)a,a+=3,tmp));
t22=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3406,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* extras.scm:551: chicken.base#make-parameter */
t23=*((C_word*)lf[164]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t23;
av2[1]=t22;
av2[2]=C_fix(79);
((C_proc)(void*)(*((C_word*)t23+1)))(3,av2);}}

/* chicken.io#read-list in k1084 in k1081 */
static void C_ccall f_1088(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_1088,c,av);}
a=C_alloc(6);
t2=C_rest_nullp(c,2);
t3=(C_truep(t2)?*((C_word*)lf[4]+1):C_get_rest_arg(c,2,av,2,t0));
t4=C_rest_nullp(c,2);
t5=C_rest_nullp(c,3);
t6=(C_truep(t5)?((C_word*)t0)[2]:C_get_rest_arg(c,3,av,2,t0));
t7=C_rest_nullp(c,3);
t8=C_rest_nullp(c,4);
t9=(C_truep(t8)?C_SCHEME_FALSE:C_get_rest_arg(c,4,av,2,t0));
t10=C_rest_nullp(c,4);
t11=C_i_check_port_2(t3,C_fix(1),C_SCHEME_TRUE,lf[5]);
t12=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1117,a[2]=t9,a[3]=t6,a[4]=t3,a[5]=t1,tmp=(C_word)a,a+=6,tmp);
/* extras.scm:48: reader */
t13=t6;{
C_word av2[3];
av2[0]=t13;
av2[1]=t12;
av2[2]=t3;
((C_proc)C_fast_retrieve_proc(t13))(3,av2);}}

/* k1115 in chicken.io#read-list in k1084 in k1081 */
static void C_ccall f_1117(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,5)))){
C_save_and_reclaim((void *)f_1117,c,av);}
a=C_alloc(9);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_1119,a[2]=((C_word*)t0)[2],a[3]=t3,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word)li0),tmp=(C_word)a,a+=7,tmp));
t5=((C_word*)t3)[1];
f_1119(t5,((C_word*)t0)[5],t1,C_fix(0),C_SCHEME_END_OF_LIST);}

/* doloop96 in k1115 in chicken.io#read-list in k1084 in k1081 */
static void C_fcall f_1119(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,2)))){
C_save_and_reclaim_args((void *)trf_1119,5,t0,t1,t2,t3,t4);}
a=C_alloc(7);
t5=C_eofp(t2);
t6=(C_truep(t5)?t5:(C_truep(((C_word*)t0)[2])?C_fixnum_greater_or_equal_p(t3,((C_word*)t0)[2]):C_SCHEME_FALSE));
if(C_truep(t6)){
/* extras.scm:52: ##sys#fast-reverse */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[6]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[6]+1);
av2[1]=t1;
av2[2]=t4;
tp(3,av2);}}
else{
t7=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_1139,a[2]=t3,a[3]=t2,a[4]=t4,a[5]=((C_word*)t0)[3],a[6]=t1,tmp=(C_word)a,a+=7,tmp);
/* extras.scm:48: reader */
t8=((C_word*)t0)[4];{
C_word av2[3];
av2[0]=t8;
av2[1]=t7;
av2[2]=((C_word*)t0)[5];
((C_proc)C_fast_retrieve_proc(t8))(3,av2);}}}

/* k1137 in doloop96 in k1115 in chicken.io#read-list in k1084 in k1081 */
static void C_ccall f_1139(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_1139,c,av);}
a=C_alloc(3);
t2=C_fixnum_plus(((C_word*)t0)[2],C_fix(1));
t3=C_a_i_cons(&a,2,((C_word*)t0)[3],((C_word*)t0)[4]);
t4=((C_word*)((C_word*)t0)[5])[1];
f_1119(t4,((C_word*)t0)[6],t1,t2,t3);}

/* chicken.io#read-line in k1084 in k1081 */
static void C_ccall f_1191(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +4,c,2)))){
C_save_and_reclaim((void*)f_1191,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+4);
t2=C_build_rest(&a,c,2,av);
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
t3=C_i_pairp(t2);
t4=(C_truep(t3)?C_get_rest_arg(c,2,av,2,t0):*((C_word*)lf[4]+1));
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1201,a[2]=t4,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
if(C_truep(t3)){
t6=C_i_cdr(t2);
t7=C_i_pairp(t6);
t8=t5;
f_1201(t8,(C_truep(t7)?C_i_cadr(t2):C_SCHEME_FALSE));}
else{
t6=t5;
f_1201(t6,C_SCHEME_FALSE);}}

/* k1199 in chicken.io#read-line in k1084 in k1081 */
static void C_fcall f_1201(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,3)))){
C_save_and_reclaim_args((void *)trf_1201,2,t0,t1);}
a=C_alloc(8);
t2=C_i_check_port_2(((C_word*)t0)[2],C_fix(1),C_SCHEME_TRUE,lf[8]);
t3=C_slot(((C_word*)t0)[2],C_fix(2));
t4=C_slot(t3,C_fix(8));
if(C_truep(t4)){
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1211,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word)li2),tmp=(C_word)a,a+=5,tmp);
/* extras.scm:64: g120 */
t6=t5;
f_1211(t6,((C_word*)t0)[3],t4);}
else{
t5=(C_truep(t1)?t1:C_fix(256));
t6=t5;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1224,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t7,a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
/* extras.scm:67: ##sys#make-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[14]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[14]+1);
av2[1]=t8;
av2[2]=((C_word*)t7)[1];
tp(3,av2);}}}

/* g120 in k1199 in chicken.io#read-line in k1084 in k1081 */
static void C_fcall f_1211(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,3)))){
C_save_and_reclaim_args((void *)trf_1211,3,t0,t1,t2);}
/* extras.scm:64: rl */
t3=t2;{
C_word av2[4];
av2[0]=t3;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
av2[3]=((C_word*)t0)[3];
((C_proc)C_fast_retrieve_proc(t3))(4,av2);}}

/* k1222 in k1199 in chicken.io#read-line in k1084 in k1081 */
static void C_ccall f_1224(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_1224,c,av);}
a=C_alloc(12);
t2=t1;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_1229,a[2]=((C_word*)t0)[2],a[3]=t3,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=t5,a[7]=((C_word)li3),tmp=(C_word)a,a+=8,tmp));
t7=((C_word*)t5)[1];
f_1229(t7,((C_word*)t0)[5],C_fix(0));}

/* loop in k1222 in k1199 in chicken.io#read-line in k1084 in k1081 */
static void C_fcall f_1229(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,4)))){
C_save_and_reclaim_args((void *)trf_1229,3,t0,t1,t2);}
a=C_alloc(8);
t3=(C_truep(((C_word*)t0)[2])?C_fixnum_greater_or_equal_p(t2,((C_word*)t0)[2]):C_SCHEME_FALSE);
if(C_truep(t3)){
/* extras.scm:70: ##sys#substring */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[9]+1));
C_word av2[5];
av2[0]=*((C_word*)lf[9]+1);
av2[1]=t1;
av2[2]=((C_word*)((C_word*)t0)[3])[1];
av2[3]=C_fix(0);
av2[4]=t2;
tp(5,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_1242,a[2]=t2,a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* extras.scm:71: ##sys#read-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[10]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[10]+1);
av2[1]=t4;
av2[2]=((C_word*)t0)[4];
tp(3,av2);}}}

/* k1240 in loop in k1222 in k1199 in chicken.io#read-line in k1084 in k1081 */
static void C_ccall f_1242(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,4)))){
C_save_and_reclaim((void *)f_1242,c,av);}
a=C_alloc(12);
if(C_truep(C_eofp(t1))){
t2=C_eqp(((C_word*)t0)[2],C_fix(0));
if(C_truep(t2)){
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
/* extras.scm:75: ##sys#substring */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[9]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[9]+1);
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)((C_word*)t0)[4])[1];
av2[3]=C_fix(0);
av2[4]=((C_word*)t0)[2];
tp(5,av2);}}}
else{
switch(t1){
case C_make_character(10):
/* extras.scm:77: ##sys#substring */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[9]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[9]+1);
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)((C_word*)t0)[4])[1];
av2[3]=C_fix(0);
av2[4]=((C_word*)t0)[2];
tp(5,av2);}
case C_make_character(13):
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1275,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* extras.scm:79: scheme#peek-char */
t3=*((C_word*)lf[11]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}
default:
if(C_truep(C_fixnum_greater_or_equal_p(((C_word*)t0)[2],((C_word*)((C_word*)t0)[6])[1]))){
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_1307,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[2],a[5]=t1,a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[3],tmp=(C_word)a,a+=8,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1315,a[2]=t2,a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
/* extras.scm:87: scheme#make-string */
t4=*((C_word*)lf[13]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)((C_word*)t0)[6])[1];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t2=C_setsubchar(((C_word*)((C_word*)t0)[4])[1],((C_word*)t0)[2],t1);
/* extras.scm:90: loop */
t3=((C_word*)((C_word*)t0)[7])[1];
f_1229(t3,((C_word*)t0)[3],C_fixnum_plus(((C_word*)t0)[2],C_fix(1)));}}}}

/* k1273 in k1240 in loop in k1222 in k1199 in chicken.io#read-line in k1084 in k1081 */
static void C_ccall f_1275(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_1275,c,av);}
a=C_alloc(5);
if(C_truep(C_i_char_equalp(t1,C_make_character(10)))){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1284,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* extras.scm:81: ##sys#read-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[10]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[10]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
tp(3,av2);}}
else{
/* extras.scm:83: ##sys#substring */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[9]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[9]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)((C_word*)t0)[3])[1];
av2[3]=C_fix(0);
av2[4]=((C_word*)t0)[4];
tp(5,av2);}}}

/* k1282 in k1273 in k1240 in loop in k1222 in k1199 in chicken.io#read-line in k1084 in k1081 */
static void C_ccall f_1284(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_1284,c,av);}
/* extras.scm:82: ##sys#substring */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[9]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[9]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)((C_word*)t0)[3])[1];
av2[3]=C_fix(0);
av2[4]=((C_word*)t0)[4];
tp(5,av2);}}

/* k1305 in k1240 in loop in k1222 in k1199 in chicken.io#read-line in k1084 in k1081 */
static void C_ccall f_1307(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_1307,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=C_fixnum_plus(((C_word*)((C_word*)t0)[3])[1],((C_word*)((C_word*)t0)[3])[1]);
t4=C_set_block_item(((C_word*)t0)[3],0,t3);
t5=C_setsubchar(((C_word*)((C_word*)t0)[2])[1],((C_word*)t0)[4],((C_word*)t0)[5]);
/* extras.scm:90: loop */
t6=((C_word*)((C_word*)t0)[6])[1];
f_1229(t6,((C_word*)t0)[7],C_fixnum_plus(((C_word*)t0)[4],C_fix(1)));}

/* k1313 in k1240 in loop in k1222 in k1199 in chicken.io#read-line in k1084 in k1081 */
static void C_ccall f_1315(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_1315,c,av);}
/* extras.scm:87: ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[12]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[12]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)((C_word*)t0)[3])[1];
av2[3]=t1;
tp(4,av2);}}

/* chicken.io#read-lines in k1084 in k1081 */
static void C_ccall f_1344(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,4)))){
C_save_and_reclaim((void *)f_1344,c,av);}
a=C_alloc(7);
t2=C_rest_nullp(c,2);
t3=(C_truep(t2)?*((C_word*)lf[4]+1):C_get_rest_arg(c,2,av,2,t0));
t4=C_rest_nullp(c,2);
t5=C_rest_nullp(c,3);
t6=(C_truep(t5)?*((C_word*)lf[16]+1):C_get_rest_arg(c,3,av,2,t0));
t7=C_rest_nullp(c,3);
t8=C_i_check_port_2(t3,C_fix(1),C_SCHEME_TRUE,lf[17]);
t9=(C_truep(t6)?t6:C_fix(1000000000));
t10=C_SCHEME_UNDEFINED;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_set_block_item(t11,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1369,a[2]=t11,a[3]=t3,a[4]=((C_word)li5),tmp=(C_word)a,a+=5,tmp));
t13=((C_word*)t11)[1];
f_1369(t13,t1,C_SCHEME_END_OF_LIST,t9);}

/* loop in chicken.io#read-lines in k1084 in k1081 */
static void C_fcall f_1369(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_1369,4,t0,t1,t2,t3);}
a=C_alloc(6);
t4=C_eqp(t3,C_fix(0));
if(C_truep(t4)){
/* extras.scm:98: ##sys#fast-reverse */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[6]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[6]+1);
av2[1]=t1;
av2[2]=t2;
tp(3,av2);}}
else{
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1382,a[2]=t1,a[3]=t2,a[4]=((C_word*)t0)[2],a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* extras.scm:99: read-line */
t6=*((C_word*)lf[7]+1);{
C_word av2[3];
av2[0]=t6;
av2[1]=t5;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}}

/* k1380 in loop in chicken.io#read-lines in k1084 in k1081 */
static void C_ccall f_1382(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_1382,c,av);}
a=C_alloc(3);
if(C_truep(C_eofp(t1))){
/* extras.scm:101: ##sys#fast-reverse */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[6]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[6]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
tp(3,av2);}}
else{
t2=C_a_i_cons(&a,2,t1,((C_word*)t0)[3]);
/* extras.scm:102: loop */
t3=((C_word*)((C_word*)t0)[4])[1];
f_1369(t3,((C_word*)t0)[2],t2,C_fixnum_difference(((C_word*)t0)[5],C_fix(1)));}}

/* chicken.io#write-line in k1084 in k1081 */
static void C_ccall f_1428(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +4,c,3)))){
C_save_and_reclaim((void*)f_1428,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+4);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
t4=(C_truep(C_eqp(t3,C_SCHEME_END_OF_LIST))?*((C_word*)lf[19]+1):C_slot(t3,C_fix(0)));
t5=C_i_check_port_2(t4,C_fix(2),C_SCHEME_TRUE,lf[20]);
t6=C_i_check_string_2(t2,lf[20]);
t7=C_slot(t4,C_fix(2));
t8=C_slot(t7,C_fix(3));
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1444,a[2]=t1,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* extras.scm:106: g171 */
t10=t8;{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t10;
av2[1]=t9;
av2[2]=t4;
av2[3]=t2;
((C_proc)C_fast_retrieve_proc(t10))(4,av2);}}

/* k1442 in chicken.io#write-line in k1084 in k1081 */
static void C_ccall f_1444(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_1444,c,av);}
/* extras.scm:112: ##sys#write-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[21]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[21]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=C_make_character(10);
av2[3]=((C_word*)t0)[3];
tp(4,av2);}}

/* chicken.io#read-string!/port in k1084 in k1081 */
static void C_ccall f_1456(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(c!=6) C_bad_argc_2(c,6,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,5)))){
C_save_and_reclaim((void *)f_1456,c,av);}
a=C_alloc(9);
t6=C_eqp(t2,C_fix(0));
if(C_truep(t6)){
t7=t1;{
C_word *av2=av;
av2[0]=t7;
av2[1]=C_fix(0);
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}
else{
t7=C_slot(t4,C_fix(2));
t8=C_slot(t7,C_fix(7));
if(C_truep(t8)){
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_1474,a[2]=t4,a[3]=t10,a[4]=t8,a[5]=t3,a[6]=((C_word)li8),tmp=(C_word)a,a+=7,tmp));
t12=((C_word*)t10)[1];
f_1474(t12,t1,t5,t2,C_fix(0));}
else{
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1533,a[2]=t10,a[3]=t3,a[4]=t4,a[5]=((C_word)li9),tmp=(C_word)a,a+=6,tmp));
t12=((C_word*)t10)[1];
f_1533(t12,t1,t5,t2,C_fix(0));}}}

/* loop in chicken.io#read-string!/port in k1084 in k1081 */
static void C_fcall f_1474(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,5)))){
C_save_and_reclaim_args((void *)trf_1474,5,t0,t1,t2,t3,t4);}
a=C_alloc(8);
t5=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_1478,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t4,a[5]=t3,a[6]=t2,a[7]=((C_word*)t0)[3],tmp=(C_word)a,a+=8,tmp);
/* extras.scm:123: rdstring */
t6=((C_word*)t0)[4];{
C_word av2[6];
av2[0]=t6;
av2[1]=t5;
av2[2]=((C_word*)t0)[2];
av2[3]=t3;
av2[4]=((C_word*)t0)[5];
av2[5]=t2;
((C_proc)C_fast_retrieve_proc(t6))(6,av2);}}

/* k1476 in loop in chicken.io#read-string!/port in k1084 in k1081 */
static void C_ccall f_1478(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_1478,c,av);}
t2=C_slot(((C_word*)t0)[2],C_fix(5));
t3=C_fixnum_plus(t2,t1);
t4=C_i_set_i_slot(((C_word*)t0)[2],C_fix(5),t3);
t5=C_eqp(t1,C_fix(0));
if(C_truep(t5)){
t6=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t6;
av2[1]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
t6=C_i_not(((C_word*)t0)[5]);
t7=(C_truep(t6)?t6:C_fixnum_lessp(t1,((C_word*)t0)[5]));
if(C_truep(t7)){
t8=C_fixnum_plus(((C_word*)t0)[6],t1);
t9=(C_truep(((C_word*)t0)[5])?C_fixnum_difference(((C_word*)t0)[5],t1):C_SCHEME_FALSE);
/* extras.scm:128: loop */
t10=((C_word*)((C_word*)t0)[7])[1];
f_1474(t10,((C_word*)t0)[3],t8,t9,C_fixnum_plus(((C_word*)t0)[4],t1));}
else{
t8=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t8;
av2[1]=C_fixnum_plus(t1,((C_word*)t0)[4]);
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}}}

/* loop in chicken.io#read-string!/port in k1084 in k1081 */
static void C_fcall f_1533(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_1533,5,t0,t1,t2,t3,t4);}
a=C_alloc(8);
t5=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_1537,a[2]=t1,a[3]=t4,a[4]=t3,a[5]=t2,a[6]=((C_word*)t0)[2],a[7]=((C_word*)t0)[3],tmp=(C_word)a,a+=8,tmp);
/* extras.scm:131: ##sys#read-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[10]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[10]+1);
av2[1]=t5;
av2[2]=((C_word*)t0)[4];
tp(3,av2);}}

/* k1535 in loop in chicken.io#read-string!/port in k1084 in k1081 */
static void C_ccall f_1537(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_1537,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_1540,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
if(C_truep(C_eofp(t1))){
t3=t2;
f_1540(t3,C_fix(0));}
else{
t3=C_setsubchar(((C_word*)t0)[7],((C_word*)t0)[5],t1);
t4=t2;
f_1540(t4,C_fix(1));}}

/* k1538 in k1535 in loop in chicken.io#read-string!/port in k1084 in k1081 */
static void C_fcall f_1540(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,4)))){
C_save_and_reclaim_args((void *)trf_1540,2,t0,t1);}
t2=C_eqp(t1,C_fix(0));
if(C_truep(t2)){
t3=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_i_not(((C_word*)t0)[4]);
t4=(C_truep(t3)?t3:C_fixnum_lessp(t1,((C_word*)t0)[4]));
if(C_truep(t4)){
t5=C_fixnum_plus(((C_word*)t0)[5],t1);
t6=(C_truep(((C_word*)t0)[4])?C_fixnum_difference(((C_word*)t0)[4],t1):C_SCHEME_FALSE);
/* extras.scm:139: loop */
t7=((C_word*)((C_word*)t0)[6])[1];
f_1533(t7,((C_word*)t0)[2],t5,t6,C_fixnum_plus(((C_word*)t0)[3],t1));}
else{
t5=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t5;
av2[1]=C_fixnum_plus(t1,((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}}

/* chicken.io#read-string! in k1084 in k1081 */
static void C_ccall f_1588(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
if(c<4) C_bad_min_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_1588,c,av);}
a=C_alloc(9);
t4=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t5=C_rest_nullp(c,4);
t6=(C_truep(t5)?*((C_word*)lf[4]+1):C_get_rest_arg(c,4,av,4,t0));
t7=C_rest_nullp(c,4);
t8=C_rest_nullp(c,5);
t9=(C_truep(t8)?C_fix(0):C_get_rest_arg(c,5,av,4,t0));
t10=C_rest_nullp(c,5);
t11=C_i_check_port_2(t6,C_fix(1),C_SCHEME_TRUE,lf[24]);
t12=C_i_check_string_2(t3,lf[24]);
t13=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_1610,a[2]=t3,a[3]=t1,a[4]=t4,a[5]=t6,a[6]=t9,tmp=(C_word)a,a+=7,tmp);
if(C_truep(((C_word*)t4)[1])){
/* extras.scm:145: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[25]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[25]+1);
av2[1]=t13;
av2[2]=((C_word*)t4)[1];
av2[3]=lf[24];
tp(4,av2);}}
else{
t14=t13;{
C_word *av2=av;
av2[0]=t14;
av2[1]=C_SCHEME_UNDEFINED;
f_1610(2,av2);}}}

/* k1608 in chicken.io#read-string! in k1084 in k1081 */
static void C_ccall f_1610(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_1610,c,av);}
a=C_alloc(7);
t2=C_block_size(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_1616,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
t4=(C_truep(((C_word*)((C_word*)t0)[4])[1])?C_fixnum_less_or_equal_p(C_fixnum_plus(((C_word*)t0)[6],((C_word*)((C_word*)t0)[4])[1]),t2):C_SCHEME_FALSE);
if(C_truep(t4)){
t5=t3;
f_1616(t5,C_SCHEME_UNDEFINED);}
else{
t5=C_fixnum_difference(t2,((C_word*)t0)[6]);
t6=C_set_block_item(((C_word*)t0)[4],0,t5);
t7=t3;
f_1616(t7,t6);}}

/* k1614 in k1608 in chicken.io#read-string! in k1084 in k1081 */
static void C_fcall f_1616(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,3)))){
C_save_and_reclaim_args((void *)trf_1616,2,t0,t1);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_1619,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* extras.scm:149: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[25]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[25]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[6];
av2[3]=lf[24];
tp(4,av2);}}

/* k1617 in k1614 in k1608 in chicken.io#read-string! in k1084 in k1081 */
static void C_ccall f_1619(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_1619,c,av);}
/* extras.scm:150: read-string!/port */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[22]+1));
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=*((C_word*)lf[22]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)((C_word*)t0)[3])[1];
av2[3]=((C_word*)t0)[4];
av2[4]=((C_word*)t0)[5];
av2[5]=((C_word*)t0)[6];
tp(6,av2);}}

/* chicken.io#read-string/port in k1084 in k1081 */
static void C_ccall f_1665(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_1665,c,av);}
a=C_alloc(5);
t4=C_eqp(t2,C_fix(0));
if(C_truep(t4)){
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=lf[27];
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1734,a[2]=t1,a[3]=t2,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* extras.scm:157: ##sys#peek-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[31]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[31]+1);
av2[1]=t5;
av2[2]=t3;
tp(3,av2);}}}

/* k1682 in k1732 in chicken.io#read-string/port in k1084 in k1081 */
static void C_ccall f_1684(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,5)))){
C_save_and_reclaim((void *)f_1684,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1687,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* extras.scm:159: read-string!/port */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[22]+1));
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=*((C_word*)lf[22]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[2];
av2[3]=t1;
av2[4]=((C_word*)t0)[4];
av2[5]=C_fix(0);
tp(6,av2);}}

/* k1685 in k1682 in k1732 in chicken.io#read-string/port in k1084 in k1081 */
static void C_ccall f_1687(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_1687,c,av);}
t2=C_eqp(((C_word*)t0)[2],t1);
if(C_truep(t2)){
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
/* extras.scm:162: ##sys#substring */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[9]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[9]+1);
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=C_fix(0);
av2[4]=t1;
tp(5,av2);}}}

/* k1697 in k1732 in chicken.io#read-string/port in k1084 in k1081 */
static void C_ccall f_1699(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_1699,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1702,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* ##sys#make-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[14]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[14]+1);
av2[1]=t2;
av2[2]=C_fix(2048);
av2[3]=C_make_character(32);
tp(4,av2);}}

/* k1700 in k1697 in k1732 in chicken.io#read-string/port in k1084 in k1081 */
static void C_ccall f_1702(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_1702,c,av);}
a=C_alloc(9);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_1707,a[2]=((C_word*)t0)[2],a[3]=t3,a[4]=t1,a[5]=((C_word*)t0)[3],a[6]=((C_word)li12),tmp=(C_word)a,a+=7,tmp));
t5=((C_word*)t3)[1];
f_1707(t5,((C_word*)t0)[4]);}

/* loop in k1700 in k1697 in k1732 in chicken.io#read-string/port in k1084 in k1081 */
static void C_fcall f_1707(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,2)))){
C_save_and_reclaim_args((void *)trf_1707,2,t0,t1);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_1711,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* extras.scm:167: scheme#peek-char */
t3=*((C_word*)lf[11]+1);{
C_word av2[3];
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k1709 in loop in k1700 in k1697 in k1732 in chicken.io#read-string/port in k1084 in k1081 */
static void C_ccall f_1711(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,5)))){
C_save_and_reclaim((void *)f_1711,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1714,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* extras.scm:168: read-string!/port */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[22]+1));
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=*((C_word*)lf[22]+1);
av2[1]=t2;
av2[2]=C_fix(2048);
av2[3]=((C_word*)t0)[5];
av2[4]=((C_word*)t0)[6];
av2[5]=C_fix(0);
tp(6,av2);}}

/* k1712 in k1709 in loop in k1700 in k1697 in k1732 in chicken.io#read-string/port in k1084 in k1081 */
static void C_ccall f_1714(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_1714,c,av);}
a=C_alloc(4);
t2=C_eqp(t1,C_fix(0));
if(C_truep(t2)){
/* extras.scm:170: chicken.base#get-output-string */
t3=*((C_word*)lf[28]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1726,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* extras.scm:172: write-string */
t4=*((C_word*)lf[29]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[5];
av2[3]=t1;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}}

/* k1724 in k1712 in k1709 in loop in k1700 in k1697 in k1732 in chicken.io#read-string/port in k1084 in k1081 */
static void C_ccall f_1726(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1726,c,av);}
/* extras.scm:173: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_1707(t2,((C_word*)t0)[3]);}

/* k1732 in chicken.io#read-string/port in k1084 in k1081 */
static void C_ccall f_1734(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_1734,c,av);}
a=C_alloc(5);
if(C_truep(C_eofp(t1))){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_END_OF_FILE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
if(C_truep(((C_word*)t0)[3])){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1684,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* extras.scm:158: ##sys#make-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[14]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[14]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
tp(3,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1699,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* extras.scm:164: chicken.base#open-output-string */
t3=*((C_word*)lf[30]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}}

/* chicken.io#read-string in k1084 in k1081 */
static void C_ccall f_1736(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_1736,c,av);}
a=C_alloc(5);
t2=C_rest_nullp(c,2);
t3=(C_truep(t2)?C_SCHEME_FALSE:C_get_rest_arg(c,2,av,2,t0));
t4=C_rest_nullp(c,2);
t5=C_rest_nullp(c,3);
t6=(C_truep(t5)?*((C_word*)lf[4]+1):C_get_rest_arg(c,3,av,2,t0));
t7=C_rest_nullp(c,3);
t8=C_i_check_port_2(t6,C_fix(1),C_SCHEME_TRUE,lf[33]);
t9=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1755,a[2]=t1,a[3]=t3,a[4]=t6,tmp=(C_word)a,a+=5,tmp);
if(C_truep(t3)){
/* extras.scm:177: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[25]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[25]+1);
av2[1]=t9;
av2[2]=t3;
av2[3]=lf[33];
tp(4,av2);}}
else{
/* extras.scm:178: read-string/port */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[26]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[26]+1);
av2[1]=t1;
av2[2]=t3;
av2[3]=t6;
tp(4,av2);}}}

/* k1753 in chicken.io#read-string in k1084 in k1081 */
static void C_ccall f_1755(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_1755,c,av);}
/* extras.scm:178: read-string/port */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[26]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[26]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
tp(4,av2);}}

/* chicken.io#read-buffered in k1084 in k1081 */
static void C_ccall f_1789(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_1789,c,av);}
t2=C_rest_nullp(c,2);
t3=(C_truep(t2)?*((C_word*)lf[4]+1):C_get_rest_arg(c,2,av,2,t0));
t4=C_i_check_port_2(t3,C_fix(1),C_SCHEME_TRUE,lf[35]);
t5=C_slot(t3,C_fix(2));
t6=C_slot(t5,C_fix(9));
if(C_truep(t6)){
/* extras.scm:190: rb */
t7=t6;{
C_word av2[3];
av2[0]=t7;
av2[1]=t1;
av2[2]=t3;
((C_proc)C_fast_retrieve_proc(t7))(3,av2);}}
else{
t7=t1;{
C_word *av2=av;
av2[0]=t7;
av2[1]=lf[36];
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}}

/* chicken.io#read-token in k1084 in k1081 */
static void C_ccall f_1817(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_1817,c,av);}
a=C_alloc(5);
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?*((C_word*)lf[4]+1):C_get_rest_arg(c,3,av,3,t0));
t5=C_i_check_port_2(t4,C_fix(1),C_SCHEME_TRUE,lf[38]);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1827,a[2]=t4,a[3]=t2,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* extras.scm:200: chicken.base#open-output-string */
t7=*((C_word*)lf[30]+1);{
C_word *av2=av;
av2[0]=t7;
av2[1]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}

/* k1825 in chicken.io#read-token in k1084 in k1081 */
static void C_ccall f_1827(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_1827,c,av);}
a=C_alloc(9);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_1832,a[2]=t3,a[3]=t1,a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],a[6]=((C_word)li16),tmp=(C_word)a,a+=7,tmp));
t5=((C_word*)t3)[1];
f_1832(t5,((C_word*)t0)[4]);}

/* loop in k1825 in chicken.io#read-token in k1084 in k1081 */
static void C_fcall f_1832(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,2)))){
C_save_and_reclaim_args((void *)trf_1832,2,t0,t1);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_1836,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* extras.scm:202: ##sys#peek-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[31]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[31]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
tp(3,av2);}}

/* k1834 in loop in k1825 in chicken.io#read-token in k1084 in k1081 */
static void C_ccall f_1836(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_1836,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1842,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t3=C_eofp(t1);
if(C_truep(C_i_not(t3))){
/* extras.scm:203: pred */
t4=((C_word*)t0)[6];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=t1;
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}
else{
t4=t2;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
f_1842(2,av2);}}}

/* k1840 in k1834 in loop in k1825 in chicken.io#read-token in k1084 in k1081 */
static void C_ccall f_1842(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_1842,c,av);}
a=C_alloc(8);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1845,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1852,a[2]=t2,a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
/* extras.scm:205: ##sys#read-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[10]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[10]+1);
av2[1]=t3;
av2[2]=((C_word*)t0)[5];
tp(3,av2);}}
else{
/* extras.scm:207: chicken.base#get-output-string */
t2=*((C_word*)lf[28]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}}

/* k1843 in k1840 in k1834 in loop in k1825 in chicken.io#read-token in k1084 in k1081 */
static void C_ccall f_1845(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1845,c,av);}
/* extras.scm:206: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_1832(t2,((C_word*)t0)[3]);}

/* k1850 in k1840 in k1834 in loop in k1825 in chicken.io#read-token in k1084 in k1081 */
static void C_ccall f_1852(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_1852,c,av);}
/* extras.scm:205: ##sys#write-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[21]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[21]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
tp(4,av2);}}

/* chicken.io#write-string in k1084 in k1081 */
static void C_ccall f_1873(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,4)))){
C_save_and_reclaim((void *)f_1873,c,av);}
a=C_alloc(12);
t3=C_i_check_string_2(t2,lf[39]);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1878,a[2]=t2,a[3]=((C_word)li18),tmp=(C_word)a,a+=4,tmp);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1916,a[2]=t4,a[3]=((C_word)li19),tmp=(C_word)a,a+=4,tmp);
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1921,a[2]=t5,a[3]=((C_word)li20),tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_rest_nullp(c,3))){
/* extras.scm:212: def-n317 */
t7=t6;
f_1921(t7,t1);}
else{
t7=C_get_rest_arg(c,3,av,3,t0);
if(C_truep(C_rest_nullp(c,4))){
/* extras.scm:212: def-port318 */
t8=t5;
f_1916(t8,t1,t7);}
else{
t8=C_get_rest_arg(c,4,av,3,t0);
/* extras.scm:212: body315 */
t9=t4;
f_1878(t9,t1,t7,t8);}}}

/* body315 in chicken.io#write-string in k1084 in k1081 */
static void C_fcall f_1878(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_1878,4,t0,t1,t2,t3);}
a=C_alloc(6);
t4=C_i_check_port_2(t3,C_fix(2),C_SCHEME_TRUE,lf[39]);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1885,a[2]=t3,a[3]=t1,a[4]=t2,a[5]=((C_word*)t0)[2],tmp=(C_word)a,a+=6,tmp);
if(C_truep(t2)){
/* extras.scm:214: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[25]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[25]+1);
av2[1]=t5;
av2[2]=t2;
av2[3]=lf[39];
tp(4,av2);}}
else{
t6=t5;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_SCHEME_UNDEFINED;
f_1885(2,av2);}}}

/* k1883 in body315 in chicken.io#write-string in k1084 in k1081 */
static void C_ccall f_1885(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_1885,c,av);}
a=C_alloc(5);
t2=C_slot(((C_word*)t0)[2],C_fix(2));
t3=C_slot(t2,C_fix(3));
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1895,a[2]=t3,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
t5=(C_truep(((C_word*)t0)[4])?C_fixnum_lessp(((C_word*)t0)[4],C_block_size(((C_word*)t0)[5])):C_SCHEME_FALSE);
if(C_truep(t5)){
/* extras.scm:218: ##sys#substring */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[9]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[9]+1);
av2[1]=t4;
av2[2]=((C_word*)t0)[5];
av2[3]=C_fix(0);
av2[4]=((C_word*)t0)[4];
tp(5,av2);}}
else{
/* extras.scm:212: g327 */
t6=t3;{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[2];
av2[3]=((C_word*)t0)[5];
((C_proc)C_fast_retrieve_proc(t6))(4,av2);}}}

/* k1893 in k1883 in body315 in chicken.io#write-string in k1084 in k1081 */
static void C_ccall f_1895(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_1895,c,av);}
/* extras.scm:212: g327 */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=t1;
((C_proc)C_fast_retrieve_proc(t2))(4,av2);}}

/* def-port318 in chicken.io#write-string in k1084 in k1081 */
static void C_fcall f_1916(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,3)))){
C_save_and_reclaim_args((void *)trf_1916,3,t0,t1,t2);}
/* extras.scm:212: body315 */
t3=((C_word*)t0)[2];
f_1878(t3,t1,t2,*((C_word*)lf[19]+1));}

/* def-n317 in chicken.io#write-string in k1084 in k1081 */
static void C_fcall f_1921(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,2)))){
C_save_and_reclaim_args((void *)trf_1921,2,t0,t1);}
/* extras.scm:212: def-port318 */
t2=((C_word*)t0)[2];
f_1916(t2,t1,C_SCHEME_FALSE);}

/* chicken.io#read-byte in k1084 in k1081 */
static void C_ccall f_1956(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_1956,c,av);}
a=C_alloc(3);
t2=C_rest_nullp(c,2);
t3=(C_truep(t2)?*((C_word*)lf[4]+1):C_get_rest_arg(c,2,av,2,t0));
t4=C_i_check_port_2(t3,C_fix(1),C_SCHEME_TRUE,lf[41]);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1966,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* extras.scm:226: ##sys#read-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[10]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[10]+1);
av2[1]=t5;
av2[2]=t3;
tp(3,av2);}}

/* k1964 in chicken.io#read-byte in k1084 in k1081 */
static void C_ccall f_1966(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1966,c,av);}
t2=C_eofp(t1);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=(C_truep(t2)?t1:C_fix(C_character_code(t1)));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.io#write-byte in k1084 in k1081 */
static void C_ccall f_1983(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_1983,c,av);}
a=C_alloc(5);
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?*((C_word*)lf[19]+1):C_get_rest_arg(c,3,av,3,t0));
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1990,a[2]=t4,a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* extras.scm:232: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[25]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[25]+1);
av2[1]=t5;
av2[2]=t2;
av2[3]=lf[43];
tp(4,av2);}}

/* k1988 in chicken.io#write-byte in k1084 in k1081 */
static void C_ccall f_1990(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_1990,c,av);}
t2=C_i_check_port_2(((C_word*)t0)[2],C_fix(2),C_SCHEME_TRUE,lf[43]);
/* extras.scm:234: ##sys#write-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[21]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[21]+1);
av2[1]=((C_word*)t0)[3];
av2[2]=C_make_character(C_unfix(((C_word*)t0)[4]));
av2[3]=((C_word*)t0)[2];
tp(4,av2);}}

/* chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_fcall f_2008(C_word t1,C_word t2,C_word t3,C_word t4,C_word t5){
C_word tmp;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(43,0,4)))){
C_save_and_reclaim_args((void *)trf_2008,5,t1,t2,t3,t4,t5);}
a=C_alloc(43);
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_SCHEME_UNDEFINED;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_SCHEME_UNDEFINED;
t15=(*a=C_VECTOR_TYPE|1,a[1]=t14,tmp=(C_word)a,a+=2,tmp);
t16=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2011,a[2]=((C_word)li24),tmp=(C_word)a,a+=3,tmp));
t17=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2073,a[2]=((C_word)li25),tmp=(C_word)a,a+=3,tmp));
t18=C_set_block_item(t11,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2112,a[2]=t5,a[3]=((C_word)li26),tmp=(C_word)a,a+=4,tmp));
t19=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_2131,a[2]=t13,a[3]=t9,a[4]=t11,a[5]=t7,a[6]=t3,a[7]=((C_word)li34),tmp=(C_word)a,a+=8,tmp));
t20=C_set_block_item(t15,0,(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_2726,a[2]=t11,a[3]=t3,a[4]=t4,a[5]=t13,a[6]=t9,a[7]=t7,a[8]=((C_word)li58),tmp=(C_word)a,a+=9,tmp));
if(C_truep(t4)){
t21=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3393,a[2]=t11,a[3]=t1,a[4]=t15,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* ##sys#make-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[14]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[14]+1);
av2[1]=t21;
av2[2]=C_fix(1);
av2[3]=C_make_character(10);
tp(4,av2);}}
else{
/* extras.scm:546: wr */
t21=((C_word*)t13)[1];
f_2131(t21,t1,t2,C_fix(0));}}

/* read-macro? in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_fcall f_2011(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_2011,2,t1,t2);}
a=C_alloc(4);
t3=C_i_car(t2);
t4=C_u_i_cdr(t2);
t5=C_eqp(t3,lf[46]);
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2039,a[2]=t4,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
if(C_truep(t5)){
t7=t6;
f_2039(t7,t5);}
else{
t7=C_eqp(t3,lf[47]);
if(C_truep(t7)){
t8=t6;
f_2039(t8,t7);}
else{
t8=C_eqp(t3,lf[48]);
t9=t6;
f_2039(t9,(C_truep(t8)?t8:C_eqp(t3,lf[49])));}}}

/* k2037 in read-macro? in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_fcall f_2039(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,1)))){
C_save_and_reclaim_args((void *)trf_2039,2,t0,t1);}
if(C_truep(t1)){
t2=C_i_pairp(((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word av2[2];
av2[0]=t3;
av2[1]=(C_truep(t2)?C_i_nullp(C_u_i_cdr(((C_word*)t0)[2])):C_SCHEME_FALSE);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word av2[2];
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* read-macro-prefix in chicken.pretty-print#generic-write in k1084 in k1081 */
static C_word C_fcall f_2073(C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_stack_overflow_check;{}
t2=C_i_car(t1);
t3=C_u_i_cdr(t1);
t4=C_eqp(t2,lf[46]);
if(C_truep(t4)){
return(lf[50]);}
else{
t5=C_eqp(t2,lf[47]);
if(C_truep(t5)){
return(lf[51]);}
else{
t6=C_eqp(t2,lf[48]);
if(C_truep(t6)){
return(lf[52]);}
else{
t7=C_eqp(t2,lf[49]);
return((C_truep(t7)?lf[53]:C_SCHEME_UNDEFINED));}}}}

/* out in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_fcall f_2112(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_2112,4,t0,t1,t2,t3);}
a=C_alloc(5);
if(C_truep(t3)){
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2122,a[2]=t2,a[3]=t1,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* extras.scm:275: output */
t5=((C_word*)t0)[2];{
C_word av2[3];
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)C_fast_retrieve_proc(t5))(3,av2);}}
else{
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k2120 in out in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2122(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(29,c,1)))){
C_save_and_reclaim((void *)f_2122,c,av);}
a=C_alloc(29);
if(C_truep(t1)){
t2=C_i_string_length(((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_s_a_i_plus(&a,2,((C_word*)t0)[4],t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_fcall f_2131(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(22,0,4)))){
C_save_and_reclaim_args((void *)trf_2131,4,t0,t1,t2,t3);}
a=C_alloc(22);
t4=C_SCHEME_UNDEFINED;
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=t4=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_2134,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t6,a[6]=((C_word*)t0)[5],a[7]=((C_word)li27),tmp=(C_word)a,a+=8,tmp);
t8=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2161,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[4],a[4]=((C_word)li29),tmp=(C_word)a,a+=5,tmp));
if(C_truep(C_i_pairp(t2))){
/* extras.scm:295: wr-expr */
t9=t4;
f_2134(t9,t1,t2,t3);}
else{
if(C_truep(C_i_nullp(t2))){
/* extras.scm:296: wr-lst */
t9=((C_word*)t6)[1];
f_2161(t9,t1,t2,t3);}
else{
if(C_truep(C_eofp(t2))){
/* extras.scm:297: out */
t9=((C_word*)((C_word*)t0)[4])[1];
f_2112(t9,t1,lf[60],t3);}
else{
if(C_truep(C_i_vectorp(t2))){
t9=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2279,a[2]=t6,a[3]=t1,a[4]=((C_word*)t0)[4],a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* extras.scm:298: scheme#vector->list */
t10=*((C_word*)lf[62]+1);{
C_word av2[3];
av2[0]=t10;
av2[1]=t9;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t10+1)))(3,av2);}}
else{
if(C_truep(C_booleanp(t2))){
if(C_truep(t2)){
/* extras.scm:299: out */
t9=((C_word*)((C_word*)t0)[4])[1];
f_2112(t9,t1,lf[63],t3);}
else{
/* extras.scm:299: out */
t9=((C_word*)((C_word*)t0)[4])[1];
f_2112(t9,t1,lf[64],t3);}}
else{
t9=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_2302,a[2]=((C_word*)t0)[4],a[3]=t1,a[4]=t3,a[5]=t2,a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* extras.scm:300: ##sys#number? */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[97]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[97]+1);
av2[1]=t9;
av2[2]=t2;
tp(3,av2);}}}}}}}

/* wr-expr in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_fcall f_2134(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,2)))){
C_save_and_reclaim_args((void *)trf_2134,4,t0,t1,t2,t3);}
a=C_alloc(9);
t4=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_2141,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=t1,a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[4],a[7]=t3,a[8]=((C_word*)t0)[5],tmp=(C_word)a,a+=9,tmp);
/* extras.scm:280: read-macro? */
f_2011(t4,t2);}

/* k2139 in wr-expr in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2141(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_2141,c,av);}
a=C_alloc(5);
if(C_truep(t1)){
t2=C_i_cadr(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2152,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t4=(
/* extras.scm:281: read-macro-prefix */
  f_2073(((C_word*)t0)[2])
);
/* extras.scm:281: out */
t5=((C_word*)((C_word*)t0)[6])[1];
f_2112(t5,t3,t4,((C_word*)t0)[7]);}
else{
/* extras.scm:282: wr-lst */
t2=((C_word*)((C_word*)t0)[8])[1];
f_2161(t2,((C_word*)t0)[4],((C_word*)t0)[2],((C_word*)t0)[7]);}}

/* k2150 in k2139 in wr-expr in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2152(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2152,c,av);}
/* extras.scm:281: wr */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2131(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* wr-lst in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_fcall f_2161(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,0,3)))){
C_save_and_reclaim_args((void *)trf_2161,4,t0,t1,t2,t3);}
a=C_alloc(11);
if(C_truep(C_i_pairp(t2))){
t4=C_u_i_cdr(t2);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2177,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t4,tmp=(C_word)a,a+=6,tmp);
if(C_truep(t3)){
t6=C_u_i_car(t2);
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2236,a[2]=((C_word*)t0)[2],a[3]=t5,a[4]=t6,tmp=(C_word)a,a+=5,tmp);
/* extras.scm:287: out */
t8=((C_word*)((C_word*)t0)[3])[1];
f_2112(t8,t7,lf[58],t3);}
else{
t6=t5;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_SCHEME_FALSE;
f_2177(2,av2);}}}
else{
/* extras.scm:293: out */
t4=((C_word*)((C_word*)t0)[3])[1];
f_2112(t4,t1,lf[59],t3);}}

/* k2175 in wr-lst in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2177(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_2177,c,av);}
a=C_alloc(8);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2179,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word)li28),tmp=(C_word)a,a+=6,tmp));
t5=((C_word*)t3)[1];
f_2179(t5,((C_word*)t0)[4],((C_word*)t0)[5],t1);}

/* loop in k2175 in wr-lst in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_fcall f_2179(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,3)))){
C_save_and_reclaim_args((void *)trf_2179,4,t0,t1,t2,t3);}
a=C_alloc(10);
if(C_truep(C_i_not(t3))){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
if(C_truep(C_i_pairp(t2))){
t4=C_u_i_cdr(t2);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2201,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
t6=C_u_i_car(t2);
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2207,a[2]=((C_word*)t0)[3],a[3]=t5,a[4]=t6,tmp=(C_word)a,a+=5,tmp);
/* extras.scm:290: out */
t8=((C_word*)((C_word*)t0)[4])[1];
f_2112(t8,t7,lf[54],t3);}
else{
if(C_truep(C_i_nullp(t2))){
/* extras.scm:291: out */
t4=((C_word*)((C_word*)t0)[4])[1];
f_2112(t4,t1,lf[55],t3);}
else{
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2223,a[2]=((C_word*)t0)[4],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2227,a[2]=((C_word*)t0)[3],a[3]=t4,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* extras.scm:292: out */
t6=((C_word*)((C_word*)t0)[4])[1];
f_2112(t6,t5,lf[57],t3);}}}}

/* k2199 in loop in k2175 in wr-lst in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2201(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2201,c,av);}
/* extras.scm:290: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2179(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* k2205 in loop in k2175 in wr-lst in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2207(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2207,c,av);}
/* extras.scm:290: wr */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2131(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* k2221 in loop in k2175 in wr-lst in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2223(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2223,c,av);}
/* extras.scm:292: out */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2112(t2,((C_word*)t0)[3],lf[56],t1);}

/* k2225 in loop in k2175 in wr-lst in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2227(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2227,c,av);}
/* extras.scm:292: wr */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2131(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* k2234 in wr-lst in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2236(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2236,c,av);}
/* extras.scm:287: wr */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2131(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* k2277 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2279(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_2279,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2283,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* extras.scm:298: out */
t3=((C_word*)((C_word*)t0)[4])[1];
f_2112(t3,t2,lf[61],((C_word*)t0)[5]);}

/* k2281 in k2277 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2283(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2283,c,av);}
/* extras.scm:298: wr-lst */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2161(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2302(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_2302,c,av);}
a=C_alloc(7);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2309,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* extras.scm:300: ##sys#number->string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[65]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[65]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
tp(3,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_2315,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* extras.scm:301: chicken.keyword#keyword? */
t3=*((C_word*)lf[96]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}

/* k2307 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2309(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2309,c,av);}
/* extras.scm:300: out */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2112(t2,((C_word*)t0)[3],t1,((C_word*)t0)[4]);}

/* k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2315(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_2315,c,av);}
a=C_alloc(7);
t2=(C_truep(t1)?t1:C_i_symbolp(((C_word*)t0)[2]));
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2321,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[2],tmp=(C_word)a,a+=6,tmp);
/* extras.scm:302: chicken.base#open-output-string */
t4=*((C_word*)lf[30]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
if(C_truep(C_i_closurep(((C_word*)t0)[2]))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2344,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp);
/* extras.scm:305: ##sys#procedure->string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[67]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[67]+1);
av2[1]=t3;
av2[2]=((C_word*)t0)[2];
tp(3,av2);}}
else{
if(C_truep(C_i_stringp(((C_word*)t0)[2]))){
if(C_truep(((C_word*)t0)[6])){
/* extras.scm:308: out */
t3=((C_word*)((C_word*)t0)[3])[1];
f_2112(t3,((C_word*)t0)[4],((C_word*)t0)[2],((C_word*)t0)[5]);}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2363,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* extras.scm:309: out */
t4=((C_word*)((C_word*)t0)[3])[1];
f_2112(t4,t3,lf[75],((C_word*)t0)[5]);}}
else{
if(C_truep(C_charp(((C_word*)t0)[2]))){
if(C_truep(((C_word*)t0)[6])){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2516,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp);
/* ##sys#make-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[14]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[14]+1);
av2[1]=t3;
av2[2]=C_fix(1);
av2[3]=((C_word*)t0)[2];
tp(4,av2);}}
else{
t3=C_fix(C_character_code(((C_word*)t0)[2]));
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_2524,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[4],a[5]=t3,a[6]=((C_word*)t0)[2],tmp=(C_word)a,a+=7,tmp);
/* extras.scm:346: out */
t5=((C_word*)((C_word*)t0)[3])[1];
f_2112(t5,t4,lf[80],((C_word*)t0)[5]);}}
else{
if(C_truep(C_undefinedp(((C_word*)t0)[2]))){
/* extras.scm:357: out */
t3=((C_word*)((C_word*)t0)[3])[1];
f_2112(t3,((C_word*)t0)[4],lf[81],((C_word*)t0)[5]);}
else{
if(C_truep(C_anypointerp(((C_word*)t0)[2]))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2610,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp);
/* extras.scm:358: ##sys#pointer->string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[82]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[82]+1);
av2[1]=t3;
av2[2]=((C_word*)t0)[2];
tp(3,av2);}}
else{
if(C_truep(C_unboundvaluep(((C_word*)t0)[2]))){
/* extras.scm:360: out */
t3=((C_word*)((C_word*)t0)[3])[1];
f_2112(t3,((C_word*)t0)[4],lf[83],((C_word*)t0)[5]);}
else{
if(C_truep(C_structurep(((C_word*)t0)[2]))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2625,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[2],tmp=(C_word)a,a+=6,tmp);
/* extras.scm:362: chicken.base#open-output-string */
t4=*((C_word*)lf[30]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2641,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[2],tmp=(C_word)a,a+=6,tmp);
/* extras.scm:365: chicken.base#port? */
t4=*((C_word*)lf[95]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}}}}}}}}}

/* k2319 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2321(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_2321,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2324,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
/* extras.scm:303: ##sys#print */
t3=*((C_word*)lf[66]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=C_SCHEME_TRUE;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k2322 in k2319 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2324(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_2324,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2331,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* extras.scm:304: chicken.base#get-output-string */
t3=*((C_word*)lf[28]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k2329 in k2322 in k2319 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2331(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2331,c,av);}
/* extras.scm:304: out */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2112(t2,((C_word*)t0)[3],t1,((C_word*)t0)[4]);}

/* k2342 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2344(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2344,c,av);}
/* extras.scm:305: out */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2112(t2,((C_word*)t0)[3],t1,((C_word*)t0)[4]);}

/* k2361 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2363(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,5)))){
C_save_and_reclaim((void *)f_2363,c,av);}
a=C_alloc(8);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2365,a[2]=((C_word*)t0)[2],a[3]=t3,a[4]=((C_word*)t0)[3],a[5]=((C_word)li31),tmp=(C_word)a,a+=6,tmp));
t5=((C_word*)t3)[1];
f_2365(t5,((C_word*)t0)[4],C_fix(0),C_fix(0),t1);}

/* loop in k2361 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_fcall f_2365(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word *a;
loop:
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(20,0,4)))){
C_save_and_reclaim_args((void *)trf_2365,5,t0,t1,t2,t3,t4);}
a=C_alloc(20);
t5=(C_truep(t4)?C_fixnum_lessp(t3,C_i_string_length(((C_word*)t0)[2])):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_i_string_ref(((C_word*)t0)[2],t3);
t7=C_u_i_char_equalp(t6,C_make_character(92));
t8=(C_truep(t7)?t7:C_u_i_char_equalp(t6,C_make_character(34)));
if(C_truep(t8)){
t9=C_a_i_fixnum_plus(&a,2,t3,C_fix(1));
t10=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2393,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t3,a[5]=t9,tmp=(C_word)a,a+=6,tmp);
t11=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2397,a[2]=((C_word*)t0)[4],a[3]=t10,tmp=(C_word)a,a+=4,tmp);
t12=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2401,a[2]=((C_word*)t0)[4],a[3]=t11,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
/* extras.scm:318: ##sys#substring */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[9]+1));
C_word av2[5];
av2[0]=*((C_word*)lf[9]+1);
av2[1]=t12;
av2[2]=((C_word*)t0)[2];
av2[3]=t2;
av2[4]=t3;
tp(5,av2);}}
else{
t9=C_u_i_char_lessp(t6,C_make_character(32));
t10=(C_truep(t9)?t9:C_u_i_char_equalp(t6,C_make_character(127)));
if(C_truep(t10)){
t11=C_fixnum_plus(t3,C_fix(1));
t12=C_fixnum_plus(t3,C_fix(1));
t13=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_2424,a[2]=t6,a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t11,a[6]=t12,a[7]=((C_word*)t0)[4],tmp=(C_word)a,a+=8,tmp);
t14=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2471,a[2]=((C_word*)t0)[4],a[3]=t13,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
/* extras.scm:325: ##sys#substring */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[9]+1));
C_word av2[5];
av2[0]=*((C_word*)lf[9]+1);
av2[1]=t14;
av2[2]=((C_word*)t0)[2];
av2[3]=t2;
av2[4]=t3;
tp(5,av2);}}
else{
/* extras.scm:340: loop */
t16=t1;
t17=t2;
t18=C_fixnum_plus(t3,C_fix(1));
t19=t4;
t1=t16;
t2=t17;
t3=t18;
t4=t19;
goto loop;}}}
else{
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2489,a[2]=((C_word*)t0)[4],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2493,a[2]=((C_word*)t0)[4],a[3]=t6,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
/* extras.scm:342: ##sys#substring */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[9]+1));
C_word av2[5];
av2[0]=*((C_word*)lf[9]+1);
av2[1]=t7;
av2[2]=((C_word*)t0)[2];
av2[3]=t2;
av2[4]=t3;
tp(5,av2);}}}

/* k2391 in loop in k2361 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2393(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_2393,c,av);}
/* extras.scm:315: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2365(t2,((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],t1);}

/* k2395 in loop in k2361 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2397(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2397,c,av);}
/* extras.scm:317: out */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2112(t2,((C_word*)t0)[3],lf[68],t1);}

/* k2399 in loop in k2361 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2401(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2401,c,av);}
/* extras.scm:318: out */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2112(t2,((C_word*)t0)[3],t1,((C_word*)t0)[4]);}

/* k2422 in loop in k2361 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2424(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_2424,c,av);}
a=C_alloc(12);
t2=C_u_i_assq(((C_word*)t0)[2],lf[69]);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2429,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],tmp=(C_word)a,a+=6,tmp);
if(C_truep(t2)){
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2430,a[2]=((C_word*)t0)[7],a[3]=t1,a[4]=((C_word)li30),tmp=(C_word)a,a+=5,tmp);
/* extras.scm:326: g500 */
t5=t4;
f_2430(t5,t3,t2);}
else{
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2448,a[2]=((C_word*)t0)[7],a[3]=t3,a[4]=((C_word*)t0)[2],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
/* ##sys#fixnum->string */
t5=*((C_word*)lf[73]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=C_fix(C_character_code(((C_word*)t0)[2]));
av2[3]=C_fix(16);
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}}

/* k2427 in k2422 in loop in k2361 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2429(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_2429,c,av);}
/* extras.scm:322: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2365(t2,((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],t1);}

/* g500 in k2422 in loop in k2361 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_fcall f_2430(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,3)))){
C_save_and_reclaim_args((void *)trf_2430,3,t0,t1,t2);}
/* extras.scm:335: out */
t3=((C_word*)((C_word*)t0)[2])[1];
f_2112(t3,t1,C_i_cdr(t2),((C_word*)t0)[3]);}

/* k2446 in k2422 in loop in k2361 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2448(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_2448,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2452,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t3=(C_truep(C_u_i_char_lessp(((C_word*)t0)[4],C_make_character(16)))?lf[70]:lf[71]);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2460,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* extras.scm:339: out */
t5=((C_word*)((C_word*)t0)[2])[1];
f_2112(t5,t4,lf[72],((C_word*)t0)[5]);}

/* k2450 in k2446 in k2422 in loop in k2361 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2452(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2452,c,av);}
/* extras.scm:337: out */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2112(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* k2458 in k2446 in k2422 in loop in k2361 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2460(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2460,c,av);}
/* extras.scm:338: out */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2112(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* k2469 in loop in k2361 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2471(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2471,c,av);}
/* extras.scm:325: out */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2112(t2,((C_word*)t0)[3],t1,((C_word*)t0)[4]);}

/* k2487 in loop in k2361 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2489(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2489,c,av);}
/* extras.scm:341: out */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2112(t2,((C_word*)t0)[3],lf[74],t1);}

/* k2491 in loop in k2361 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2493(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2493,c,av);}
/* extras.scm:342: out */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2112(t2,((C_word*)t0)[3],t1,((C_word*)t0)[4]);}

/* k2514 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2516(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2516,c,av);}
/* extras.scm:344: out */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2112(t2,((C_word*)t0)[3],t1,((C_word*)t0)[4]);}

/* k2522 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2524(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_2524,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_2527,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* extras.scm:347: chicken.base#char-name */
t3=*((C_word*)lf[79]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k2525 in k2522 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2527(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2527,c,av);}
a=C_alloc(6);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2531,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word)li32),tmp=(C_word)a,a+=5,tmp);
/* extras.scm:347: g511 */
t3=t2;
f_2531(t3,((C_word*)t0)[4],t1);}
else{
if(C_truep(C_fixnum_lessp(((C_word*)t0)[5],C_fix(32)))){
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2551,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* extras.scm:351: out */
t3=((C_word*)((C_word*)t0)[2])[1];
f_2112(t3,t2,lf[76],((C_word*)t0)[3]);}
else{
if(C_truep(C_fixnum_greaterp(((C_word*)t0)[5],C_fix(255)))){
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2569,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_fixnum_greaterp(((C_word*)t0)[5],C_fix(65535)))){
/* extras.scm:354: out */
t3=((C_word*)((C_word*)t0)[2])[1];
f_2112(t3,t2,lf[77],((C_word*)t0)[3]);}
else{
/* extras.scm:354: out */
t3=((C_word*)((C_word*)t0)[2])[1];
f_2112(t3,t2,lf[78],((C_word*)t0)[3]);}}
else{
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2592,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* ##sys#make-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[14]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[14]+1);
av2[1]=t2;
av2[2]=C_fix(1);
av2[3]=((C_word*)t0)[6];
tp(4,av2);}}}}}

/* g511 in k2525 in k2522 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_fcall f_2531(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,3)))){
C_save_and_reclaim_args((void *)trf_2531,3,t0,t1,t2);}
/* extras.scm:349: out */
t3=((C_word*)((C_word*)t0)[2])[1];
f_2112(t3,t1,C_slot(t2,C_fix(1)),((C_word*)t0)[3]);}

/* k2549 in k2525 in k2522 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2551(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_2551,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2558,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* ##sys#fixnum->string */
t3=*((C_word*)lf[73]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=C_fix(16);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k2556 in k2549 in k2525 in k2522 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2558(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2558,c,av);}
/* extras.scm:352: out */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2112(t2,((C_word*)t0)[3],t1,((C_word*)t0)[4]);}

/* k2567 in k2525 in k2522 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2569(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_2569,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2576,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* ##sys#fixnum->string */
t3=*((C_word*)lf[73]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=C_fix(16);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k2574 in k2567 in k2525 in k2522 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2576(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2576,c,av);}
/* extras.scm:355: out */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2112(t2,((C_word*)t0)[3],t1,((C_word*)t0)[4]);}

/* k2590 in k2525 in k2522 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2592(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2592,c,av);}
/* extras.scm:356: out */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2112(t2,((C_word*)t0)[3],t1,((C_word*)t0)[4]);}

/* k2608 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2610(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2610,c,av);}
/* extras.scm:358: out */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2112(t2,((C_word*)t0)[3],t1,((C_word*)t0)[4]);}

/* k2623 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2625(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_2625,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2628,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
/* extras.scm:363: ##sys#user-print-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[84]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[84]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=C_SCHEME_TRUE;
av2[4]=t1;
tp(5,av2);}}

/* k2626 in k2623 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2628(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_2628,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2635,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* extras.scm:364: chicken.base#get-output-string */
t3=*((C_word*)lf[28]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k2633 in k2626 in k2623 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2635(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2635,c,av);}
/* extras.scm:364: out */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2112(t2,((C_word*)t0)[3],t1,((C_word*)t0)[4]);}

/* k2639 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2641(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_2641,c,av);}
a=C_alloc(6);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2648,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* extras.scm:365: scheme#string-append */
t3=*((C_word*)lf[85]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[86];
av2[3]=C_slot(((C_word*)t0)[5],C_fix(3));
av2[4]=lf[87];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}
else{
if(C_truep(C_bytevectorp(((C_word*)t0)[5]))){
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2658,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* extras.scm:367: out */
t3=((C_word*)((C_word*)t0)[2])[1];
f_2112(t3,t2,lf[90],((C_word*)t0)[4]);}
else{
if(C_truep(C_lambdainfop(((C_word*)t0)[5]))){
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2708,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* extras.scm:377: out */
t3=((C_word*)((C_word*)t0)[2])[1];
f_2112(t3,t2,lf[93],((C_word*)t0)[4]);}
else{
/* extras.scm:380: out */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2112(t2,((C_word*)t0)[3],lf[94],((C_word*)t0)[4]);}}}}

/* k2646 in k2639 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2648(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2648,c,av);}
/* extras.scm:365: out */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2112(t2,((C_word*)t0)[3],t1,((C_word*)t0)[4]);}

/* k2656 in k2639 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2658(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,3)))){
C_save_and_reclaim((void *)f_2658,c,av);}
a=C_alloc(15);
t2=C_block_size(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2664,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_2669,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=t5,a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[5],a[7]=((C_word)li33),tmp=(C_word)a,a+=8,tmp));
t7=((C_word*)t5)[1];
f_2669(t7,t3,C_fix(0));}

/* k2662 in k2656 in k2639 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2664(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2664,c,av);}
/* extras.scm:375: out */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2112(t2,((C_word*)t0)[3],lf[88],((C_word*)t0)[4]);}

/* doloop520 in k2656 in k2639 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_fcall f_2669(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,3)))){
C_save_and_reclaim_args((void *)trf_2669,3,t0,t1,t2);}
a=C_alloc(8);
if(C_truep(C_fixnum_greater_or_equal_p(t2,((C_word*)t0)[2]))){
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=C_subbyte(((C_word*)t0)[3],t2);
t4=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_2682,a[2]=((C_word*)t0)[4],a[3]=t1,a[4]=t2,a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t3,tmp=(C_word)a,a+=8,tmp);
if(C_truep(C_fixnum_lessp(t3,C_fix(16)))){
/* extras.scm:373: out */
t5=((C_word*)((C_word*)t0)[5])[1];
f_2112(t5,t4,lf[89],((C_word*)t0)[6]);}
else{
t5=t4;{
C_word av2[2];
av2[0]=t5;
av2[1]=C_SCHEME_UNDEFINED;
f_2682(2,av2);}}}}

/* k2680 in doloop520 in k2656 in k2639 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2682(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_2682,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2685,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2696,a[2]=((C_word*)t0)[5],a[3]=t2,a[4]=((C_word*)t0)[6],tmp=(C_word)a,a+=5,tmp);
/* extras.scm:374: ##sys#number->string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[65]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[65]+1);
av2[1]=t3;
av2[2]=((C_word*)t0)[7];
av2[3]=C_fix(16);
tp(4,av2);}}

/* k2683 in k2680 in doloop520 in k2656 in k2639 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2685(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_2685,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_2669(t2,((C_word*)t0)[3],C_fixnum_plus(((C_word*)t0)[4],C_fix(1)));}

/* k2694 in k2680 in doloop520 in k2656 in k2639 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2696(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2696,c,av);}
/* extras.scm:374: out */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2112(t2,((C_word*)t0)[3],t1,((C_word*)t0)[4]);}

/* k2706 in k2639 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2708(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_2708,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2711,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2718,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* extras.scm:378: ##sys#lambda-info->string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[92]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[92]+1);
av2[1]=t3;
av2[2]=((C_word*)t0)[5];
tp(3,av2);}}

/* k2709 in k2706 in k2639 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2711(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2711,c,av);}
/* extras.scm:379: out */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2112(t2,((C_word*)t0)[3],lf[91],((C_word*)t0)[4]);}

/* k2716 in k2706 in k2639 in k2313 in k2300 in wr in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2718(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2718,c,av);}
/* extras.scm:378: out */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2112(t2,((C_word*)t0)[3],t1,((C_word*)t0)[4]);}

/* pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_fcall f_2726(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word t24;
C_word t25;
C_word t26;
C_word t27;
C_word t28;
C_word t29;
C_word t30;
C_word t31;
C_word t32;
C_word t33;
C_word t34;
C_word t35;
C_word t36;
C_word t37;
C_word t38;
C_word t39;
C_word t40;
C_word t41;
C_word t42;
C_word t43;
C_word t44;
C_word t45;
C_word t46;
C_word t47;
C_word t48;
C_word t49;
C_word t50;
C_word t51;
C_word t52;
C_word t53;
C_word t54;
C_word t55;
C_word t56;
C_word t57;
C_word t58;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(152,0,9)))){
C_save_and_reclaim_args((void *)trf_2726,4,t0,t1,t2,t3);}
a=C_alloc(152);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_SCHEME_UNDEFINED;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_SCHEME_UNDEFINED;
t15=(*a=C_VECTOR_TYPE|1,a[1]=t14,tmp=(C_word)a,a+=2,tmp);
t16=C_SCHEME_UNDEFINED;
t17=(*a=C_VECTOR_TYPE|1,a[1]=t16,tmp=(C_word)a,a+=2,tmp);
t18=C_SCHEME_UNDEFINED;
t19=(*a=C_VECTOR_TYPE|1,a[1]=t18,tmp=(C_word)a,a+=2,tmp);
t20=C_SCHEME_UNDEFINED;
t21=(*a=C_VECTOR_TYPE|1,a[1]=t20,tmp=(C_word)a,a+=2,tmp);
t22=C_SCHEME_UNDEFINED;
t23=(*a=C_VECTOR_TYPE|1,a[1]=t22,tmp=(C_word)a,a+=2,tmp);
t24=C_SCHEME_UNDEFINED;
t25=(*a=C_VECTOR_TYPE|1,a[1]=t24,tmp=(C_word)a,a+=2,tmp);
t26=C_SCHEME_UNDEFINED;
t27=(*a=C_VECTOR_TYPE|1,a[1]=t26,tmp=(C_word)a,a+=2,tmp);
t28=C_SCHEME_UNDEFINED;
t29=(*a=C_VECTOR_TYPE|1,a[1]=t28,tmp=(C_word)a,a+=2,tmp);
t30=C_SCHEME_UNDEFINED;
t31=(*a=C_VECTOR_TYPE|1,a[1]=t30,tmp=(C_word)a,a+=2,tmp);
t32=C_SCHEME_UNDEFINED;
t33=(*a=C_VECTOR_TYPE|1,a[1]=t32,tmp=(C_word)a,a+=2,tmp);
t34=C_SCHEME_UNDEFINED;
t35=(*a=C_VECTOR_TYPE|1,a[1]=t34,tmp=(C_word)a,a+=2,tmp);
t36=C_SCHEME_UNDEFINED;
t37=(*a=C_VECTOR_TYPE|1,a[1]=t36,tmp=(C_word)a,a+=2,tmp);
t38=C_SCHEME_UNDEFINED;
t39=(*a=C_VECTOR_TYPE|1,a[1]=t38,tmp=(C_word)a,a+=2,tmp);
t40=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2729,a[2]=t5,a[3]=((C_word*)t0)[2],a[4]=((C_word)li35),tmp=(C_word)a,a+=5,tmp));
t41=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2762,a[2]=t5,a[3]=((C_word*)t0)[2],a[4]=((C_word)li36),tmp=(C_word)a,a+=5,tmp));
t42=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_2796,a[2]=((C_word*)t0)[2],a[3]=t15,a[4]=t11,a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[5],a[8]=((C_word)li38),tmp=(C_word)a,a+=9,tmp));
t43=C_set_block_item(t11,0,(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_2883,a[2]=t9,a[3]=t11,a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[2],a[6]=t19,a[7]=t13,a[8]=t39,a[9]=t15,a[10]=((C_word*)t0)[7],a[11]=((C_word)li39),tmp=(C_word)a,a+=12,tmp));
t44=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2948,a[2]=t17,a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[2],a[5]=((C_word)li40),tmp=(C_word)a,a+=6,tmp));
t45=C_set_block_item(t15,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2974,a[2]=t17,a[3]=((C_word*)t0)[2],a[4]=((C_word)li41),tmp=(C_word)a,a+=5,tmp));
t46=C_set_block_item(t17,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2983,a[2]=t9,a[3]=t7,a[4]=((C_word*)t0)[2],a[5]=((C_word)li43),tmp=(C_word)a,a+=6,tmp));
t47=C_set_block_item(t19,0,(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3056,a[2]=t9,a[3]=t7,a[4]=t17,a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[2],a[7]=((C_word)li47),tmp=(C_word)a,a+=8,tmp));
t48=C_set_block_item(t21,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3196,a[2]=t15,a[3]=t11,a[4]=((C_word)li48),tmp=(C_word)a,a+=5,tmp));
t49=C_set_block_item(t23,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3202,a[2]=t19,a[3]=t21,a[4]=t11,a[5]=((C_word)li49),tmp=(C_word)a,a+=6,tmp));
t50=C_set_block_item(t25,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3208,a[2]=t19,a[3]=t11,a[4]=((C_word)li50),tmp=(C_word)a,a+=5,tmp));
t51=C_set_block_item(t27,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3214,a[2]=t13,a[3]=t21,a[4]=((C_word)li51),tmp=(C_word)a,a+=5,tmp));
t52=C_set_block_item(t29,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3220,a[2]=t19,a[3]=t11,a[4]=t21,a[5]=((C_word)li52),tmp=(C_word)a,a+=6,tmp));
t53=C_set_block_item(t31,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3226,a[2]=t13,a[3]=t11,a[4]=((C_word)li53),tmp=(C_word)a,a+=5,tmp));
t54=C_set_block_item(t33,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3232,a[2]=t19,a[3]=t21,a[4]=t11,a[5]=((C_word)li54),tmp=(C_word)a,a+=6,tmp));
t55=C_set_block_item(t35,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3252,a[2]=t19,a[3]=t11,a[4]=((C_word)li55),tmp=(C_word)a,a+=5,tmp));
t56=C_set_block_item(t37,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3258,a[2]=t19,a[3]=t21,a[4]=t11,a[5]=((C_word)li56),tmp=(C_word)a,a+=6,tmp));
t57=C_set_block_item(t39,0,(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_3267,a[2]=t23,a[3]=t25,a[4]=t27,a[5]=t29,a[6]=t31,a[7]=t33,a[8]=t35,a[9]=t37,a[10]=((C_word)li57),tmp=(C_word)a,a+=11,tmp));
/* extras.scm:542: pr */
t58=((C_word*)t9)[1];
f_2796(t58,t1,t2,t3,C_fix(0),((C_word*)t11)[1]);}

/* spaces in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_fcall f_2729(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(34,0,4)))){
C_save_and_reclaim_args((void *)trf_2729,4,t0,t1,t2,t3);}
a=C_alloc(34);
if(C_truep(C_i_greaterp(t2,C_fix(0)))){
if(C_truep(C_i_greaterp(t2,C_fix(7)))){
t4=C_s_a_i_minus(&a,2,t2,C_fix(8));
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2753,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
/* extras.scm:387: out */
t6=((C_word*)((C_word*)t0)[3])[1];
f_2112(t6,t5,lf[98],t3);}
else{
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2760,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* extras.scm:388: ##sys#substring */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[9]+1));
C_word av2[5];
av2[0]=*((C_word*)lf[9]+1);
av2[1]=t4;
av2[2]=lf[99];
av2[3]=C_fix(0);
av2[4]=t2;
tp(5,av2);}}}
else{
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k2751 in spaces in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2753(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2753,c,av);}
/* extras.scm:387: spaces */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2729(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* k2758 in spaces in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2760(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2760,c,av);}
/* extras.scm:388: out */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2112(t2,((C_word*)t0)[3],t1,((C_word*)t0)[4]);}

/* indent in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_fcall f_2762(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(29,0,3)))){
C_save_and_reclaim_args((void *)trf_2762,4,t0,t1,t2,t3);}
a=C_alloc(29);
if(C_truep(t3)){
if(C_truep(C_i_lessp(t2,t3))){
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2778,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2785,a[2]=((C_word*)t0)[3],a[3]=t4,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* ##sys#make-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[14]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[14]+1);
av2[1]=t5;
av2[2]=C_fix(1);
av2[3]=C_make_character(10);
tp(4,av2);}}
else{
t4=C_s_a_i_minus(&a,2,t2,t3);
/* extras.scm:395: spaces */
t5=((C_word*)((C_word*)t0)[2])[1];
f_2729(t5,t1,t4,t3);}}
else{
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k2776 in indent in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2778(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2778,c,av);}
if(C_truep(t1)){
/* extras.scm:394: spaces */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2729(t2,((C_word*)t0)[3],((C_word*)t0)[4],C_fix(0));}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k2783 in indent in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2785(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2785,c,av);}
/* extras.scm:394: out */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2112(t2,((C_word*)t0)[3],t1,((C_word*)t0)[4]);}

/* pr in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_fcall f_2796(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5){
C_word tmp;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(101,0,3)))){
C_save_and_reclaim_args((void *)trf_2796,6,t0,t1,t2,t3,t4,t5);}
a=C_alloc(101);
t6=C_i_pairp(t2);
t7=(C_truep(t6)?t6:C_i_vectorp(t2));
if(C_truep(t7)){
t8=C_SCHEME_END_OF_LIST;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_2809,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t3,a[5]=t9,a[6]=t2,a[7]=t5,a[8]=t4,a[9]=((C_word*)t0)[3],a[10]=((C_word*)t0)[4],a[11]=((C_word*)t0)[5],tmp=(C_word)a,a+=12,tmp);
t11=C_s_a_i_minus(&a,2,((C_word*)t0)[6],t3);
t12=C_s_a_i_minus(&a,2,t11,t4);
t13=C_s_a_i_plus(&a,2,t12,C_fix(1));
/* extras.scm:400: scheme#max */
t14=*((C_word*)lf[102]+1);{
C_word av2[4];
av2[0]=t14;
av2[1]=t10;
av2[2]=t13;
av2[3]=C_fix(50);
((C_proc)(void*)(*((C_word*)t14+1)))(4,av2);}}
else{
/* extras.scm:411: wr */
t8=((C_word*)((C_word*)t0)[7])[1];
f_2131(t8,t1,t2,t3);}}

/* k2807 in pr in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2809(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(19,c,5)))){
C_save_and_reclaim((void *)f_2809,c,av);}
a=C_alloc(19);
t2=t1;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_2812,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],tmp=(C_word)a,a+=12,tmp);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2847,a[2]=((C_word*)t0)[5],a[3]=t3,a[4]=((C_word)li37),tmp=(C_word)a,a+=5,tmp);
/* extras.scm:401: generic-write */
f_2008(t4,((C_word*)t0)[6],((C_word*)t0)[11],C_SCHEME_FALSE,t5);}

/* k2810 in k2807 in pr in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2812(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_2812,c,av);}
a=C_alloc(8);
if(C_truep(C_i_greaterp(((C_word*)((C_word*)t0)[2])[1],C_fix(0)))){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2825,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp);
/* extras.scm:407: chicken.string#reverse-string-append */
t3=*((C_word*)lf[100]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[6])[1];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
if(C_truep(C_i_pairp(((C_word*)t0)[7]))){
/* extras.scm:409: pp-pair */
t2=((C_word*)t0)[8];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[4];
av2[2]=((C_word*)t0)[7];
av2[3]=((C_word*)t0)[5];
av2[4]=((C_word*)t0)[9];
((C_proc)C_fast_retrieve_proc(t2))(5,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_2841,a[2]=((C_word*)t0)[10],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[9],a[5]=((C_word*)t0)[11],a[6]=((C_word*)t0)[3],a[7]=((C_word*)t0)[5],tmp=(C_word)a,a+=8,tmp);
/* extras.scm:410: scheme#vector->list */
t3=*((C_word*)lf[62]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}}

/* k2823 in k2810 in k2807 in pr in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2825(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2825,c,av);}
/* extras.scm:407: out */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2112(t2,((C_word*)t0)[3],t1,((C_word*)t0)[4]);}

/* k2839 in k2810 in k2807 in pr in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2841(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_2841,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_2845,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* extras.scm:410: out */
t3=((C_word*)((C_word*)t0)[6])[1];
f_2112(t3,t2,lf[101],((C_word*)t0)[7]);}

/* k2843 in k2839 in k2810 in k2807 in pr in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2845(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_2845,c,av);}
/* extras.scm:410: pp-list */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2974(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1,((C_word*)t0)[5],((C_word*)((C_word*)t0)[6])[1]);}

/* a2846 in k2807 in pr in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2847(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(32,c,1)))){
C_save_and_reclaim((void *)f_2847,c,av);}
a=C_alloc(32);
t3=C_a_i_cons(&a,2,t2,((C_word*)((C_word*)t0)[2])[1]);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t3);
t5=C_i_string_length(t2);
t6=C_s_a_i_minus(&a,2,((C_word*)((C_word*)t0)[3])[1],t5);
t7=C_mutate(((C_word *)((C_word*)t0)[3])+1,t6);
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=C_i_greaterp(((C_word*)((C_word*)t0)[3])[1],C_fix(0));
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}

/* pp-expr in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2883(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,2)))){
C_save_and_reclaim((void *)f_2883,c,av);}
a=C_alloc(14);
t5=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_2890,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=t1,a[5]=t4,a[6]=((C_word*)t0)[3],a[7]=((C_word*)t0)[4],a[8]=((C_word*)t0)[5],a[9]=t3,a[10]=((C_word*)t0)[6],a[11]=((C_word*)t0)[7],a[12]=((C_word*)t0)[8],a[13]=((C_word*)t0)[9],tmp=(C_word)a,a+=14,tmp);
/* extras.scm:414: read-macro? */
f_2011(t5,t2);}

/* k2888 in pp-expr in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2890(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_2890,c,av);}
a=C_alloc(10);
if(C_truep(t1)){
t2=C_i_cadr(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_2901,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t2,a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
t4=(
/* extras.scm:416: read-macro-prefix */
  f_2073(((C_word*)t0)[2])
);
/* extras.scm:416: out */
t5=((C_word*)((C_word*)t0)[8])[1];
f_2112(t5,t3,t4,((C_word*)t0)[9]);}
else{
t2=C_i_car(((C_word*)t0)[2]);
if(C_truep(C_i_symbolp(t2))){
t3=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_2917,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[9],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[10],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[11],a[9]=t2,tmp=(C_word)a,a+=10,tmp);
/* extras.scm:421: style */
t4=((C_word*)((C_word*)t0)[12])[1];
f_3267(t4,t3,t2);}
else{
/* extras.scm:428: pp-list */
t3=((C_word*)((C_word*)t0)[13])[1];
f_2974(t3,((C_word*)t0)[4],((C_word*)t0)[2],((C_word*)t0)[9],((C_word*)t0)[5],((C_word*)((C_word*)t0)[6])[1]);}}}

/* k2899 in k2888 in pp-expr in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2901(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_2901,c,av);}
/* extras.scm:415: pr */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2796(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1,((C_word*)t0)[5],((C_word*)((C_word*)t0)[6])[1]);}

/* k2915 in k2888 in pp-expr in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2917(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_2917,c,av);}
a=C_alloc(9);
if(C_truep(t1)){
/* extras.scm:423: proc */
t2=t1;{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
av2[4]=((C_word*)t0)[5];
((C_proc)C_fast_retrieve_proc(t2))(5,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_2943,a[2]=((C_word*)t0)[6],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
/* extras.scm:424: ##sys#symbol->string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[103]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[103]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[9];
tp(3,av2);}}}

/* k2941 in k2915 in k2888 in pp-expr in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2943(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,8)))){
C_save_and_reclaim((void *)f_2943,c,av);}
t2=C_i_string_length(t1);
if(C_truep(C_i_greaterp(t2,C_fix(5)))){
/* extras.scm:426: pp-general */
t3=((C_word*)((C_word*)t0)[2])[1];
f_3056(t3,((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6],C_SCHEME_FALSE,C_SCHEME_FALSE,C_SCHEME_FALSE,((C_word*)((C_word*)t0)[7])[1]);}
else{
/* extras.scm:427: pp-call */
t3=((C_word*)((C_word*)t0)[8])[1];
f_2948(t3,((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6],((C_word*)((C_word*)t0)[7])[1]);}}

/* pp-call in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_fcall f_2948(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5){
C_word tmp;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(13,0,3)))){
C_save_and_reclaim_args((void *)trf_2948,6,t0,t1,t2,t3,t4,t5);}
a=C_alloc(13);
t6=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_2952,a[2]=t3,a[3]=t2,a[4]=((C_word*)t0)[2],a[5]=t1,a[6]=t4,a[7]=t5,tmp=(C_word)a,a+=8,tmp);
t7=C_i_car(t2);
t8=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2972,a[2]=((C_word*)t0)[3],a[3]=t6,a[4]=t7,tmp=(C_word)a,a+=5,tmp);
/* extras.scm:434: out */
t9=((C_word*)((C_word*)t0)[4])[1];
f_2112(t9,t8,lf[104],t3);}

/* k2950 in pp-call in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2952(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(29,c,6)))){
C_save_and_reclaim((void *)f_2952,c,av);}
a=C_alloc(29);
if(C_truep(((C_word*)t0)[2])){
t2=C_u_i_cdr(((C_word*)t0)[3]);
t3=C_s_a_i_plus(&a,2,t1,C_fix(1));
/* extras.scm:436: pp-down */
t4=((C_word*)((C_word*)t0)[4])[1];
f_2983(t4,((C_word*)t0)[5],t2,t1,t3,((C_word*)t0)[6],((C_word*)t0)[7]);}
else{
t2=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k2970 in pp-call in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2972(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2972,c,av);}
/* extras.scm:434: wr */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2131(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* pp-list in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_fcall f_2974(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5){
C_word tmp;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,3)))){
C_save_and_reclaim_args((void *)trf_2974,6,t0,t1,t2,t3,t4,t5);}
a=C_alloc(7);
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_2978,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,a[5]=t4,a[6]=t5,tmp=(C_word)a,a+=7,tmp);
/* extras.scm:442: out */
t7=((C_word*)((C_word*)t0)[3])[1];
f_2112(t7,t6,lf[105],t3);}

/* k2976 in pp-list in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_2978(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_2978,c,av);}
/* extras.scm:443: pp-down */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2983(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1,t1,((C_word*)t0)[5],((C_word*)t0)[6]);}

/* pp-down in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_fcall f_2983(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6){
C_word tmp;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,4)))){
C_save_and_reclaim_args((void *)trf_2983,7,t0,t1,t2,t3,t4,t5,t6);}
a=C_alloc(12);
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_2989,a[2]=t5,a[3]=t8,a[4]=((C_word*)t0)[2],a[5]=t6,a[6]=((C_word*)t0)[3],a[7]=t4,a[8]=((C_word*)t0)[4],a[9]=((C_word)li42),tmp=(C_word)a,a+=10,tmp));
t10=((C_word*)t8)[1];
f_2989(t10,t1,t2,t3);}

/* loop in pp-down in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_fcall f_2989(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(41,0,3)))){
C_save_and_reclaim_args((void *)trf_2989,4,t0,t1,t2,t3);}
a=C_alloc(41);
if(C_truep(t3)){
if(C_truep(C_i_pairp(t2))){
t4=C_u_i_cdr(t2);
t5=C_i_nullp(t4);
t6=(C_truep(t5)?C_s_a_i_plus(&a,2,((C_word*)t0)[2],C_fix(1)):C_fix(0));
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3010,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
t8=C_u_i_car(t2);
t9=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3016,a[2]=((C_word*)t0)[4],a[3]=t7,a[4]=t8,a[5]=t6,a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* extras.scm:452: indent */
t10=((C_word*)((C_word*)t0)[6])[1];
f_2762(t10,t9,((C_word*)t0)[7],t3);}
else{
if(C_truep(C_i_nullp(t2))){
/* extras.scm:454: out */
t4=((C_word*)((C_word*)t0)[8])[1];
f_2112(t4,t1,lf[106],t3);}
else{
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3038,a[2]=((C_word*)t0)[8],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t5=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3042,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[4],a[4]=t4,a[5]=t2,a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3050,a[2]=((C_word*)t0)[6],a[3]=t5,a[4]=((C_word*)t0)[7],tmp=(C_word)a,a+=5,tmp);
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3054,a[2]=((C_word*)t0)[8],a[3]=t6,tmp=(C_word)a,a+=4,tmp);
/* extras.scm:458: indent */
t8=((C_word*)((C_word*)t0)[6])[1];
f_2762(t8,t7,((C_word*)t0)[7],t3);}}}
else{
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k3008 in loop in pp-down in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_3010(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3010,c,av);}
/* extras.scm:451: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2989(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* k3014 in loop in pp-down in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_3016(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_3016,c,av);}
/* extras.scm:452: pr */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2796(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1,((C_word*)t0)[5],((C_word*)t0)[6]);}

/* k3036 in loop in pp-down in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_3038(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3038,c,av);}
/* extras.scm:456: out */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2112(t2,((C_word*)t0)[3],lf[107],t1);}

/* k3040 in loop in pp-down in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_3042(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(29,c,5)))){
C_save_and_reclaim((void *)f_3042,c,av);}
a=C_alloc(29);
t2=C_s_a_i_plus(&a,2,((C_word*)t0)[2],C_fix(1));
/* extras.scm:457: pr */
t3=((C_word*)((C_word*)t0)[3])[1];
f_2796(t3,((C_word*)t0)[4],((C_word*)t0)[5],t1,t2,((C_word*)t0)[6]);}

/* k3048 in loop in pp-down in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_3050(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3050,c,av);}
/* extras.scm:458: indent */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2762(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* k3052 in loop in pp-down in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_3054(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3054,c,av);}
/* extras.scm:458: out */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2112(t2,((C_word*)t0)[3],lf[108],t1);}

/* pp-general in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_fcall f_3056(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6,C_word t7,C_word t8){
C_word tmp;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(42,0,6)))){
C_save_and_reclaim_args((void *)trf_3056,9,t0,t1,t2,t3,t4,t5,t6,t7,t8);}
a=C_alloc(42);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_SCHEME_UNDEFINED;
t12=(*a=C_VECTOR_TYPE|1,a[1]=t11,tmp=(C_word)a,a+=2,tmp);
t13=C_SCHEME_UNDEFINED;
t14=(*a=C_VECTOR_TYPE|1,a[1]=t13,tmp=(C_word)a,a+=2,tmp);
t15=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3059,a[2]=t6,a[3]=t4,a[4]=t12,a[5]=((C_word*)t0)[2],a[6]=((C_word*)t0)[3],a[7]=((C_word)li44),tmp=(C_word)a,a+=8,tmp));
t16=C_set_block_item(t12,0,(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3098,a[2]=t7,a[3]=t4,a[4]=t14,a[5]=((C_word*)t0)[2],a[6]=((C_word*)t0)[3],a[7]=((C_word)li45),tmp=(C_word)a,a+=8,tmp));
t17=C_set_block_item(t14,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3137,a[2]=((C_word*)t0)[4],a[3]=t4,a[4]=t8,a[5]=((C_word)li46),tmp=(C_word)a,a+=6,tmp));
t18=C_i_car(t2);
t19=C_u_i_cdr(t2);
t20=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_3148,a[2]=t5,a[3]=t19,a[4]=t3,a[5]=t10,a[6]=t1,a[7]=((C_word*)t0)[5],a[8]=((C_word*)t0)[6],tmp=(C_word)a,a+=9,tmp);
t21=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3194,a[2]=((C_word*)t0)[5],a[3]=t20,a[4]=t18,tmp=(C_word)a,a+=5,tmp);
/* extras.scm:485: out */
t22=((C_word*)((C_word*)t0)[6])[1];
f_2112(t22,t21,lf[110],t3);}

/* tail1 in pp-general in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_fcall f_3059(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5){
C_word tmp;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(43,0,5)))){
C_save_and_reclaim_args((void *)trf_3059,6,t0,t1,t2,t3,t4,t5);}
a=C_alloc(43);
t6=(C_truep(((C_word*)t0)[2])?C_i_pairp(t2):C_SCHEME_FALSE);
if(C_truep(t6)){
t7=C_i_car(t2);
t8=C_u_i_cdr(t2);
t9=C_i_nullp(t8);
t10=(C_truep(t9)?C_s_a_i_plus(&a,2,((C_word*)t0)[3],C_fix(1)):C_fix(0));
t11=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3080,a[2]=((C_word*)t0)[4],a[3]=t1,a[4]=t8,a[5]=t3,a[6]=t5,tmp=(C_word)a,a+=7,tmp);
t12=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3084,a[2]=((C_word*)t0)[5],a[3]=t11,a[4]=t7,a[5]=t10,a[6]=((C_word*)t0)[2],tmp=(C_word)a,a+=7,tmp);
/* extras.scm:469: indent */
t13=((C_word*)((C_word*)t0)[6])[1];
f_2762(t13,t12,t5,t4);}
else{
/* extras.scm:470: tail2 */
t7=((C_word*)((C_word*)t0)[4])[1];
f_3098(t7,t1,t2,t3,t4,t5);}}

/* k3078 in tail1 in pp-general in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_3080(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_3080,c,av);}
/* extras.scm:469: tail2 */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3098(t2,((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],t1,((C_word*)t0)[6]);}

/* k3082 in tail1 in pp-general in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_3084(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_3084,c,av);}
/* extras.scm:469: pr */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2796(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1,((C_word*)t0)[5],((C_word*)t0)[6]);}

/* tail2 in pp-general in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_fcall f_3098(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5){
C_word tmp;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(42,0,4)))){
C_save_and_reclaim_args((void *)trf_3098,6,t0,t1,t2,t3,t4,t5);}
a=C_alloc(42);
t6=(C_truep(((C_word*)t0)[2])?C_i_pairp(t2):C_SCHEME_FALSE);
if(C_truep(t6)){
t7=C_i_car(t2);
t8=C_u_i_cdr(t2);
t9=C_i_nullp(t8);
t10=(C_truep(t9)?C_s_a_i_plus(&a,2,((C_word*)t0)[3],C_fix(1)):C_fix(0));
t11=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3119,a[2]=((C_word*)t0)[4],a[3]=t1,a[4]=t8,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
t12=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3123,a[2]=((C_word*)t0)[5],a[3]=t11,a[4]=t7,a[5]=t10,a[6]=((C_word*)t0)[2],tmp=(C_word)a,a+=7,tmp);
/* extras.scm:477: indent */
t13=((C_word*)((C_word*)t0)[6])[1];
f_2762(t13,t12,t5,t4);}
else{
/* extras.scm:478: tail3 */
t7=((C_word*)((C_word*)t0)[4])[1];
f_3137(t7,t1,t2,t3,t4);}}

/* k3117 in tail2 in pp-general in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_3119(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_3119,c,av);}
/* extras.scm:477: tail3 */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3137(t2,((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],t1);}

/* k3121 in tail2 in pp-general in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_3123(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_3123,c,av);}
/* extras.scm:477: pr */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2796(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1,((C_word*)t0)[5],((C_word*)t0)[6]);}

/* tail3 in pp-general in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_fcall f_3137(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,6)))){
C_save_and_reclaim_args((void *)trf_3137,5,t0,t1,t2,t3,t4);}
/* extras.scm:481: pp-down */
t5=((C_word*)((C_word*)t0)[2])[1];
f_2983(t5,t1,t2,t4,t3,((C_word*)t0)[3],((C_word*)t0)[4]);}

/* k3146 in pp-general in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_3148(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(58,c,5)))){
C_save_and_reclaim((void *)f_3148,c,av);}
a=C_alloc(58);
t2=(C_truep(((C_word*)t0)[2])?C_i_pairp(((C_word*)t0)[3]):C_SCHEME_FALSE);
if(C_truep(t2)){
t3=C_i_car(((C_word*)t0)[3]);
t4=C_u_i_cdr(((C_word*)t0)[3]);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3161,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=t4,tmp=(C_word)a,a+=6,tmp);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3176,a[2]=((C_word*)t0)[7],a[3]=t5,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* extras.scm:489: out */
t7=((C_word*)((C_word*)t0)[8])[1];
f_2112(t7,t6,lf[109],t1);}
else{
t3=C_s_a_i_plus(&a,2,((C_word*)t0)[4],C_fix(2));
t4=C_s_a_i_plus(&a,2,t1,C_fix(1));
/* extras.scm:491: tail1 */
t5=((C_word*)((C_word*)t0)[5])[1];
f_3059(t5,((C_word*)t0)[6],((C_word*)t0)[3],t3,t1,t4);}}

/* k3159 in k3146 in pp-general in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_3161(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(58,c,5)))){
C_save_and_reclaim((void *)f_3161,c,av);}
a=C_alloc(58);
t2=C_s_a_i_plus(&a,2,((C_word*)t0)[2],C_fix(2));
t3=C_s_a_i_plus(&a,2,t1,C_fix(1));
/* extras.scm:490: tail1 */
t4=((C_word*)((C_word*)t0)[3])[1];
f_3059(t4,((C_word*)t0)[4],((C_word*)t0)[5],t2,t1,t3);}

/* k3174 in k3146 in pp-general in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_3176(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3176,c,av);}
/* extras.scm:489: wr */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2131(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* k3192 in pp-general in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_3194(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3194,c,av);}
/* extras.scm:485: wr */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2131(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* pp-expr-list in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_3196(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_3196,c,av);}
/* extras.scm:494: pp-list */
t5=((C_word*)((C_word*)t0)[2])[1];
f_2974(t5,t1,t2,t3,t4,((C_word*)((C_word*)t0)[3])[1]);}

/* pp-lambda in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_3202(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,8)))){
C_save_and_reclaim((void *)f_3202,c,av);}
/* extras.scm:497: pp-general */
t5=((C_word*)((C_word*)t0)[2])[1];
f_3056(t5,t1,t2,t3,t4,C_SCHEME_FALSE,((C_word*)((C_word*)t0)[3])[1],C_SCHEME_FALSE,((C_word*)((C_word*)t0)[4])[1]);}

/* pp-if in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_3208(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,8)))){
C_save_and_reclaim((void *)f_3208,c,av);}
/* extras.scm:500: pp-general */
t5=((C_word*)((C_word*)t0)[2])[1];
f_3056(t5,t1,t2,t3,t4,C_SCHEME_FALSE,((C_word*)((C_word*)t0)[3])[1],C_SCHEME_FALSE,((C_word*)((C_word*)t0)[3])[1]);}

/* pp-cond in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_3214(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_3214,c,av);}
/* extras.scm:503: pp-call */
t5=((C_word*)((C_word*)t0)[2])[1];
f_2948(t5,t1,t2,t3,t4,((C_word*)((C_word*)t0)[3])[1]);}

/* pp-case in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_3220(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,8)))){
C_save_and_reclaim((void *)f_3220,c,av);}
/* extras.scm:506: pp-general */
t5=((C_word*)((C_word*)t0)[2])[1];
f_3056(t5,t1,t2,t3,t4,C_SCHEME_FALSE,((C_word*)((C_word*)t0)[3])[1],C_SCHEME_FALSE,((C_word*)((C_word*)t0)[4])[1]);}

/* pp-and in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_3226(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_3226,c,av);}
/* extras.scm:509: pp-call */
t5=((C_word*)((C_word*)t0)[2])[1];
f_2948(t5,t1,t2,t3,t4,((C_word*)((C_word*)t0)[3])[1]);}

/* pp-let in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_3232(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,8)))){
C_save_and_reclaim((void *)f_3232,c,av);}
t5=C_i_cdr(t2);
if(C_truep(C_i_pairp(t5))){
/* extras.scm:514: pp-general */
t6=((C_word*)((C_word*)t0)[2])[1];
f_3056(t6,t1,t2,t3,t4,C_i_symbolp(C_u_i_car(t5)),((C_word*)((C_word*)t0)[3])[1],C_SCHEME_FALSE,((C_word*)((C_word*)t0)[4])[1]);}
else{
/* extras.scm:514: pp-general */
t6=((C_word*)((C_word*)t0)[2])[1];
f_3056(t6,t1,t2,t3,t4,C_SCHEME_FALSE,((C_word*)((C_word*)t0)[3])[1],C_SCHEME_FALSE,((C_word*)((C_word*)t0)[4])[1]);}}

/* pp-begin in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_3252(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,8)))){
C_save_and_reclaim((void *)f_3252,c,av);}
/* extras.scm:517: pp-general */
t5=((C_word*)((C_word*)t0)[2])[1];
f_3056(t5,t1,t2,t3,t4,C_SCHEME_FALSE,C_SCHEME_FALSE,C_SCHEME_FALSE,((C_word*)((C_word*)t0)[3])[1]);}

/* pp-do in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_3258(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,8)))){
C_save_and_reclaim((void *)f_3258,c,av);}
/* extras.scm:520: pp-general */
t5=((C_word*)((C_word*)t0)[2])[1];
f_3056(t5,t1,t2,t3,t4,C_SCHEME_FALSE,((C_word*)((C_word*)t0)[3])[1],((C_word*)((C_word*)t0)[3])[1],((C_word*)((C_word*)t0)[4])[1]);}

/* style in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_fcall f_3267(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,2)))){
C_save_and_reclaim_args((void *)trf_3267,3,t0,t1,t2);}
a=C_alloc(12);
t3=C_eqp(t2,lf[111]);
t4=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_3277,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t2,a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[5],a[8]=((C_word*)t0)[6],a[9]=((C_word*)t0)[7],a[10]=((C_word*)t0)[8],a[11]=((C_word*)t0)[9],tmp=(C_word)a,a+=12,tmp);
if(C_truep(t3)){
t5=t4;
f_3277(t5,t3);}
else{
t5=C_eqp(t2,lf[121]);
if(C_truep(t5)){
t6=t4;
f_3277(t6,t5);}
else{
t6=C_eqp(t2,lf[122]);
if(C_truep(t6)){
t7=t4;
f_3277(t7,t6);}
else{
t7=C_eqp(t2,lf[123]);
t8=t4;
f_3277(t8,(C_truep(t7)?t7:C_eqp(t2,lf[124])));}}}}

/* k3275 in style in pp in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_fcall f_3277(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,1)))){
C_save_and_reclaim_args((void *)trf_3277,2,t0,t1);}
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t2;
av2[1]=((C_word*)((C_word*)t0)[3])[1];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_eqp(((C_word*)t0)[4],lf[112]);
t3=(C_truep(t2)?t2:C_eqp(((C_word*)t0)[4],lf[113]));
if(C_truep(t3)){
t4=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t4;
av2[1]=((C_word*)((C_word*)t0)[5])[1];
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=C_eqp(((C_word*)t0)[4],lf[114]);
if(C_truep(t4)){
t5=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t5;
av2[1]=((C_word*)((C_word*)t0)[6])[1];
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t5=C_eqp(((C_word*)t0)[4],lf[115]);
if(C_truep(t5)){
t6=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t6;
av2[1]=((C_word*)((C_word*)t0)[7])[1];
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
t6=C_eqp(((C_word*)t0)[4],lf[116]);
t7=(C_truep(t6)?t6:C_eqp(((C_word*)t0)[4],lf[117]));
if(C_truep(t7)){
t8=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t8;
av2[1]=((C_word*)((C_word*)t0)[8])[1];
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}
else{
t8=C_eqp(((C_word*)t0)[4],lf[118]);
if(C_truep(t8)){
t9=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t9;
av2[1]=((C_word*)((C_word*)t0)[9])[1];
((C_proc)(void*)(*((C_word*)t9+1)))(2,av2);}}
else{
t9=C_eqp(((C_word*)t0)[4],lf[119]);
if(C_truep(t9)){
t10=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t10;
av2[1]=((C_word*)((C_word*)t0)[10])[1];
((C_proc)(void*)(*((C_word*)t10+1)))(2,av2);}}
else{
t10=C_eqp(((C_word*)t0)[4],lf[120]);
t11=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t11;
av2[1]=(C_truep(t10)?((C_word*)((C_word*)t0)[11])[1]:C_SCHEME_FALSE);
((C_proc)(void*)(*((C_word*)t11+1)))(2,av2);}}}}}}}}}

/* k3391 in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_3393(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_3393,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3397,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* extras.scm:545: pp */
t3=((C_word*)((C_word*)t0)[4])[1];
f_2726(t3,t2,((C_word*)t0)[5],C_fix(0));}

/* k3395 in k3391 in chicken.pretty-print#generic-write in k1084 in k1081 */
static void C_ccall f_3397(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3397,c,av);}
/* extras.scm:545: out */
t2=((C_word*)((C_word*)t0)[2])[1];
f_2112(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* k3404 in k1084 in k1081 */
static void C_ccall f_3406(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(29,c,6)))){
C_save_and_reclaim((void *)f_3406,c,av);}
a=C_alloc(29);
t2=C_mutate((C_word*)lf[125]+1 /* (set! chicken.pretty-print#pretty-print-width ...) */,t1);
t3=C_mutate((C_word*)lf[126]+1 /* (set! chicken.pretty-print#pretty-print ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3408,a[2]=((C_word)li61),tmp=(C_word)a,a+=3,tmp));
t4=C_mutate((C_word*)lf[128]+1 /* (set! chicken.pretty-print#pp ...) */,*((C_word*)lf[126]+1));
t5=C_a_i_provide(&a,1,lf[129]);
t6=C_mutate(&lf[130] /* (set! chicken.format#fprintf0 ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3432,a[2]=((C_word)li66),tmp=(C_word)a,a+=3,tmp));
t7=C_mutate((C_word*)lf[138]+1 /* (set! chicken.format#fprintf ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3752,a[2]=((C_word)li67),tmp=(C_word)a,a+=3,tmp));
t8=C_mutate((C_word*)lf[140]+1 /* (set! chicken.format#printf ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3758,a[2]=((C_word)li68),tmp=(C_word)a,a+=3,tmp));
t9=C_mutate((C_word*)lf[142]+1 /* (set! chicken.format#sprintf ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3764,a[2]=((C_word)li69),tmp=(C_word)a,a+=3,tmp));
t10=C_mutate((C_word*)lf[144]+1 /* (set! chicken.format#format ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3770,a[2]=((C_word)li70),tmp=(C_word)a,a+=3,tmp));
t11=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3813,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* extras.scm:640: chicken.platform#register-feature! */
t12=*((C_word*)lf[162]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t12;
av2[1]=t11;
av2[2]=lf[163];
((C_proc)(void*)(*((C_word*)t12+1)))(3,av2);}}

/* chicken.pretty-print#pretty-print in k3404 in k1084 in k1081 */
static void C_ccall f_3408(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +8,c,2)))){
C_save_and_reclaim((void*)f_3408,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+8);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
t4=C_i_pairp(t3);
t5=(C_truep(t4)?C_get_rest_arg(c,3,av,3,t0):*((C_word*)lf[19]+1));
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3415,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3419,a[2]=t5,a[3]=t6,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* extras.scm:555: pretty-print-width */
t8=*((C_word*)lf[125]+1);{
C_word *av2=av;
av2[0]=t8;
av2[1]=t7;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}

/* k3413 in chicken.pretty-print#pretty-print in k3404 in k1084 in k1081 */
static void C_ccall f_3415(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3415,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k3417 in chicken.pretty-print#pretty-print in k3404 in k1084 in k1081 */
static void C_ccall f_3419(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,5)))){
C_save_and_reclaim((void *)f_3419,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3421,a[2]=((C_word*)t0)[2],a[3]=((C_word)li60),tmp=(C_word)a,a+=4,tmp);
/* extras.scm:555: generic-write */
f_2008(((C_word*)t0)[3],((C_word*)t0)[4],C_SCHEME_FALSE,t1,t2);}

/* a3420 in k3417 in chicken.pretty-print#pretty-print in k3404 in k1084 in k1081 */
static void C_ccall f_3421(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_3421,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3425,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* extras.scm:555: scheme#display */
t4=*((C_word*)lf[127]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k3423 in a3420 in k3417 in chicken.pretty-print#pretty-print in k3404 in k1084 in k1081 */
static void C_ccall f_3425(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3425,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.format#fprintf0 in k3404 in k1084 in k1081 */
static void C_fcall f_3432(C_word t1,C_word t2,C_word t3,C_word t4,C_word t5){
C_word tmp;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,0,2)))){
C_save_and_reclaim_args((void *)trf_3432,5,t1,t2,t3,t4,t5);}
a=C_alloc(11);
t6=(C_truep(t3)?C_i_check_port_2(t3,C_fix(2),C_SCHEME_TRUE,t2):C_SCHEME_UNDEFINED);
t7=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3439,a[2]=t3,a[3]=t1,a[4]=t2,a[5]=t4,a[6]=t5,tmp=(C_word)a,a+=7,tmp);
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3741,a[2]=t7,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
if(C_truep(t3)){
/* extras.scm:571: ##sys#tty-port? */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[137]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[137]+1);
av2[1]=t8;
av2[2]=t3;
tp(3,av2);}}
else{
/* extras.scm:573: chicken.base#open-output-string */
t9=*((C_word*)lf[30]+1);{
C_word av2[2];
av2[0]=t9;
av2[1]=t7;
((C_proc)(void*)(*((C_word*)t9+1)))(2,av2);}}}

/* k3437 in chicken.format#fprintf0 in k3404 in k1084 in k1081 */
static void C_ccall f_3439(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,4)))){
C_save_and_reclaim((void *)f_3439,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3442,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3470,a[2]=((C_word*)t0)[4],a[3]=t1,a[4]=t4,a[5]=((C_word)li65),tmp=(C_word)a,a+=6,tmp));
t6=((C_word*)t4)[1];
f_3470(t6,t2,((C_word*)t0)[5],((C_word*)t0)[6]);}

/* k3440 in k3437 in chicken.format#fprintf0 in k3404 in k1084 in k1081 */
static void C_ccall f_3442(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_3442,c,av);}
a=C_alloc(4);
if(C_truep(C_i_not(((C_word*)t0)[2]))){
/* extras.scm:617: chicken.base#get-output-string */
t2=*((C_word*)lf[28]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}
else{
t2=C_eqp(((C_word*)t0)[4],((C_word*)t0)[2]);
if(C_truep(C_i_not(t2))){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3464,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* extras.scm:619: chicken.base#get-output-string */
t4=*((C_word*)lf[28]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}}

/* k3462 in k3440 in k3437 in chicken.format#fprintf0 in k3404 in k1084 in k1081 */
static void C_ccall f_3464(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_3464,c,av);}
/* extras.scm:619: ##sys#print */
t2=*((C_word*)lf[66]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* rec in k3437 in chicken.format#fprintf0 in k3404 in k1084 in k1081 */
static void C_fcall f_3470(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(30,0,2)))){
C_save_and_reclaim_args((void *)trf_3470,4,t0,t1,t2,t3);}
a=C_alloc(30);
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_i_check_string_2(t2,((C_word*)t0)[2]);
t6=C_fix(0);
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_block_size(t2);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_SCHEME_UNDEFINED;
t12=(*a=C_VECTOR_TYPE|1,a[1]=t11,tmp=(C_word)a,a+=2,tmp);
t13=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3479,a[2]=t2,a[3]=t7,tmp=(C_word)a,a+=4,tmp));
t14=C_set_block_item(t12,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3486,a[2]=t4,a[3]=((C_word*)t0)[2],a[4]=((C_word)li62),tmp=(C_word)a,a+=5,tmp));
t15=C_SCHEME_UNDEFINED;
t16=(*a=C_VECTOR_TYPE|1,a[1]=t15,tmp=(C_word)a,a+=2,tmp);
t17=C_set_block_item(t16,0,(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_3505,a[2]=t7,a[3]=t8,a[4]=t10,a[5]=t16,a[6]=((C_word*)t0)[3],a[7]=t12,a[8]=((C_word*)t0)[2],a[9]=((C_word*)t0)[4],a[10]=((C_word)li64),tmp=(C_word)a,a+=11,tmp));
t18=((C_word*)t16)[1];
f_3505(t18,t1);}

/* fetch in rec in k3437 in chicken.format#fprintf0 in k3404 in k1084 in k1081 */
static C_word C_fcall f_3479(C_word t0){
C_word tmp;
C_word t1;
C_word t2;
C_word t3;
C_word t4;
C_stack_overflow_check;{}
t1=C_subchar(((C_word*)t0)[2],((C_word*)((C_word*)t0)[3])[1]);
t2=C_fixnum_plus(((C_word*)((C_word*)t0)[3])[1],C_fix(1));
t3=C_set_block_item(((C_word*)t0)[3],0,t2);
return(t1);}

/* next in rec in k3437 in chicken.format#fprintf0 in k3404 in k1084 in k1081 */
static void C_fcall f_3486(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,3)))){
C_save_and_reclaim_args((void *)trf_3486,2,t0,t1);}
if(C_truep(C_eqp(((C_word*)((C_word*)t0)[2])[1],C_SCHEME_END_OF_LIST))){
/* extras.scm:584: ##sys#error */
t2=*((C_word*)lf[131]+1);{
C_word av2[4];
av2[0]=t2;
av2[1]=t1;
av2[2]=((C_word*)t0)[3];
av2[3]=lf[132];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}
else{
t2=C_slot(((C_word*)((C_word*)t0)[2])[1],C_fix(0));
t3=C_slot(((C_word*)((C_word*)t0)[2])[1],C_fix(1));
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t3);
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* loop in rec in k3437 in chicken.format#fprintf0 in k3404 in k1084 in k1081 */
static void C_fcall f_3505(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word t24;
C_word *a;
loop:
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,0,4)))){
C_save_and_reclaim_args((void *)trf_3505,2,t0,t1);}
a=C_alloc(11);
if(C_truep(C_fixnum_greater_or_equal_p(((C_word*)((C_word*)t0)[2])[1],((C_word*)t0)[3]))){
t2=C_SCHEME_UNDEFINED;
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=(
/* extras.scm:590: fetch */
  f_3479(((C_word*)((C_word*)t0)[4])[1])
);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3518,a[2]=((C_word*)t0)[5],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t4=C_eqp(t2,C_make_character(126));
t5=(C_truep(t4)?C_fixnum_lessp(((C_word*)((C_word*)t0)[2])[1],((C_word*)t0)[3]):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=(
/* extras.scm:592: fetch */
  f_3479(((C_word*)((C_word*)t0)[4])[1])
);
t7=C_u_i_char_upcase(t6);
switch(t7){
case C_make_character(83):
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3543,a[2]=t3,a[3]=((C_word*)t0)[6],tmp=(C_word)a,a+=4,tmp);
/* extras.scm:594: next */
t9=((C_word*)((C_word*)t0)[7])[1];
f_3486(t9,t8);
case C_make_character(65):
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3556,a[2]=t3,a[3]=((C_word*)t0)[6],tmp=(C_word)a,a+=4,tmp);
/* extras.scm:595: next */
t9=((C_word*)((C_word*)t0)[7])[1];
f_3486(t9,t8);
case C_make_character(67):
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3569,a[2]=t3,a[3]=((C_word*)t0)[6],tmp=(C_word)a,a+=4,tmp);
/* extras.scm:596: next */
t9=((C_word*)((C_word*)t0)[7])[1];
f_3486(t9,t8);
case C_make_character(66):
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3582,a[2]=t3,a[3]=((C_word*)t0)[6],tmp=(C_word)a,a+=4,tmp);
t9=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3586,a[2]=t8,tmp=(C_word)a,a+=3,tmp);
/* extras.scm:597: next */
t10=((C_word*)((C_word*)t0)[7])[1];
f_3486(t10,t9);
case C_make_character(79):
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3599,a[2]=t3,a[3]=((C_word*)t0)[6],tmp=(C_word)a,a+=4,tmp);
t9=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3603,a[2]=t8,tmp=(C_word)a,a+=3,tmp);
/* extras.scm:598: next */
t10=((C_word*)((C_word*)t0)[7])[1];
f_3486(t10,t9);
case C_make_character(88):
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3616,a[2]=t3,a[3]=((C_word*)t0)[6],tmp=(C_word)a,a+=4,tmp);
t9=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3620,a[2]=t8,tmp=(C_word)a,a+=3,tmp);
/* extras.scm:599: next */
t10=((C_word*)((C_word*)t0)[7])[1];
f_3486(t10,t9);
case C_make_character(33):
/* extras.scm:600: ##sys#flush-output */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[134]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[134]+1);
av2[1]=t3;
av2[2]=((C_word*)t0)[6];
tp(3,av2);}
case C_make_character(63):
t8=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3638,a[2]=((C_word*)t0)[8],a[3]=((C_word*)t0)[5],a[4]=t1,a[5]=((C_word*)t0)[9],a[6]=((C_word*)t0)[7],tmp=(C_word)a,a+=7,tmp);
/* extras.scm:602: next */
t9=((C_word*)((C_word*)t0)[7])[1];
f_3486(t9,t8);
case C_make_character(126):
/* extras.scm:606: ##sys#write-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[21]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[21]+1);
av2[1]=t3;
av2[2]=C_make_character(126);
av2[3]=((C_word*)t0)[6];
tp(4,av2);}
default:
t8=C_eqp(t7,C_make_character(37));
t9=(C_truep(t8)?t8:C_eqp(t7,C_make_character(78)));
if(C_truep(t9)){
/* extras.scm:607: scheme#newline */
t10=*((C_word*)lf[135]+1);{
C_word av2[3];
av2[0]=t10;
av2[1]=t3;
av2[2]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t10+1)))(3,av2);}}
else{
if(C_truep(C_u_i_char_whitespacep(t6))){
t10=(
/* extras.scm:610: fetch */
  f_3479(((C_word*)((C_word*)t0)[4])[1])
);
t11=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3683,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=((C_word)li63),tmp=(C_word)a,a+=5,tmp);
t12=(
  f_3683(t11,t10)
);
/* extras.scm:616: loop */
t24=t1;
t1=t24;
goto loop;}
else{
/* extras.scm:614: ##sys#error */
t10=*((C_word*)lf[131]+1);{
C_word av2[5];
av2[0]=t10;
av2[1]=t3;
av2[2]=((C_word*)t0)[8];
av2[3]=lf[136];
av2[4]=t6;
((C_proc)(void*)(*((C_word*)t10+1)))(5,av2);}}}}}
else{
/* extras.scm:615: ##sys#write-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[21]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[21]+1);
av2[1]=t3;
av2[2]=t2;
av2[3]=((C_word*)t0)[6];
tp(4,av2);}}}}

/* k3516 in loop in rec in k3437 in chicken.format#fprintf0 in k3404 in k1084 in k1081 */
static void C_ccall f_3518(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3518,c,av);}
/* extras.scm:616: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3505(t2,((C_word*)t0)[3]);}

/* k3541 in loop in rec in k3437 in chicken.format#fprintf0 in k3404 in k1084 in k1081 */
static void C_ccall f_3543(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3543,c,av);}
/* extras.scm:594: scheme#write */
t2=*((C_word*)lf[133]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k3554 in loop in rec in k3437 in chicken.format#fprintf0 in k3404 in k1084 in k1081 */
static void C_ccall f_3556(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3556,c,av);}
/* extras.scm:595: scheme#display */
t2=*((C_word*)lf[127]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k3567 in loop in rec in k3437 in chicken.format#fprintf0 in k3404 in k1084 in k1081 */
static void C_ccall f_3569(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3569,c,av);}
/* extras.scm:596: ##sys#write-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[21]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[21]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
tp(4,av2);}}

/* k3580 in loop in rec in k3437 in chicken.format#fprintf0 in k3404 in k1084 in k1081 */
static void C_ccall f_3582(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3582,c,av);}
/* extras.scm:597: scheme#display */
t2=*((C_word*)lf[127]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k3584 in loop in rec in k3437 in chicken.format#fprintf0 in k3404 in k1084 in k1081 */
static void C_ccall f_3586(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3586,c,av);}
/* extras.scm:597: ##sys#number->string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[65]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[65]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_fix(2);
tp(4,av2);}}

/* k3597 in loop in rec in k3437 in chicken.format#fprintf0 in k3404 in k1084 in k1081 */
static void C_ccall f_3599(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3599,c,av);}
/* extras.scm:598: scheme#display */
t2=*((C_word*)lf[127]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k3601 in loop in rec in k3437 in chicken.format#fprintf0 in k3404 in k1084 in k1081 */
static void C_ccall f_3603(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3603,c,av);}
/* extras.scm:598: ##sys#number->string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[65]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[65]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_fix(8);
tp(4,av2);}}

/* k3614 in loop in rec in k3437 in chicken.format#fprintf0 in k3404 in k1084 in k1081 */
static void C_ccall f_3616(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3616,c,av);}
/* extras.scm:599: scheme#display */
t2=*((C_word*)lf[127]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k3618 in loop in rec in k3437 in chicken.format#fprintf0 in k3404 in k1084 in k1081 */
static void C_ccall f_3620(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3620,c,av);}
/* extras.scm:599: ##sys#number->string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[65]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[65]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_fix(16);
tp(4,av2);}}

/* k3636 in loop in rec in k3437 in chicken.format#fprintf0 in k3404 in k1084 in k1081 */
static void C_ccall f_3638(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_3638,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3641,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,tmp=(C_word)a,a+=7,tmp);
/* extras.scm:603: next */
t3=((C_word*)((C_word*)t0)[6])[1];
f_3486(t3,t2);}

/* k3639 in k3636 in loop in rec in k3437 in chicken.format#fprintf0 in k3404 in k1084 in k1081 */
static void C_ccall f_3641(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_3641,c,av);}
a=C_alloc(4);
t2=C_i_check_list_2(t1,((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3647,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
/* extras.scm:605: rec */
t4=((C_word*)((C_word*)t0)[5])[1];
f_3470(t4,t3,((C_word*)t0)[6],t1);}

/* k3645 in k3639 in k3636 in loop in rec in k3437 in chicken.format#fprintf0 in k3404 in k1084 in k1081 */
static void C_ccall f_3647(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3647,c,av);}
/* extras.scm:616: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3505(t2,((C_word*)t0)[3]);}

/* skip in loop in rec in k3437 in chicken.format#fprintf0 in k3404 in k1084 in k1081 */
static C_word C_fcall f_3683(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_stack_overflow_check;
loop:{}
if(C_truep(C_u_i_char_whitespacep(t1))){
t2=(
/* extras.scm:612: fetch */
  f_3479(((C_word*)((C_word*)t0)[2])[1])
);
t5=t2;
t1=t5;
goto loop;}
else{
t2=C_fixnum_difference(((C_word*)((C_word*)t0)[3])[1],C_fix(1));
t3=C_set_block_item(((C_word*)t0)[3],0,t2);
return(t3);}}

/* k3739 in chicken.format#fprintf0 in k3404 in k1084 in k1081 */
static void C_ccall f_3741(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3741,c,av);}
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
f_3439(2,av2);}}
else{
/* extras.scm:573: chicken.base#open-output-string */
t2=*((C_word*)lf[30]+1);{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* chicken.format#fprintf in k3404 in k1084 in k1081 */
static void C_ccall f_3752(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c<4) C_bad_min_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand((c-4)*C_SIZEOF_PAIR +0,c,5)))){
C_save_and_reclaim((void*)f_3752,c,av);}
a=C_alloc((c-4)*C_SIZEOF_PAIR+0);
t4=C_build_rest(&a,c,4,av);
C_word t5;
/* extras.scm:622: fprintf0 */
f_3432(t1,lf[139],t2,t3,t4);}

/* chicken.format#printf in k3404 in k1084 in k1081 */
static void C_ccall f_3758(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +0,c,5)))){
C_save_and_reclaim((void*)f_3758,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+0);
t3=C_build_rest(&a,c,3,av);
C_word t4;
/* extras.scm:625: fprintf0 */
f_3432(t1,lf[141],*((C_word*)lf[19]+1),t2,t3);}

/* chicken.format#sprintf in k3404 in k1084 in k1081 */
static void C_ccall f_3764(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +0,c,5)))){
C_save_and_reclaim((void*)f_3764,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+0);
t3=C_build_rest(&a,c,3,av);
C_word t4;
/* extras.scm:628: fprintf0 */
f_3432(t1,lf[143],C_SCHEME_FALSE,t2,t3);}

/* chicken.format#format in k3404 in k1084 in k1081 */
static void C_ccall f_3770(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +12,c,3)))){
C_save_and_reclaim((void*)f_3770,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+12);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3778,a[2]=t1,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_i_not(t2))){{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=t1;
av2[2]=*((C_word*)lf[142]+1);
av2[3]=((C_word*)t4)[1];
C_apply(4,av2);}}
else{
if(C_truep(C_booleanp(t2))){{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=t1;
av2[2]=*((C_word*)lf[140]+1);
av2[3]=((C_word*)t4)[1];
C_apply(4,av2);}}
else{
if(C_truep(C_i_stringp(t2))){
t6=C_a_i_cons(&a,2,t2,((C_word*)t4)[1]);
t7=C_set_block_item(t4,0,t6);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=t1;
av2[2]=*((C_word*)lf[142]+1);
av2[3]=((C_word*)t4)[1];
C_apply(4,av2);}}
else{
t6=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3803,a[2]=t2,a[3]=t4,a[4]=t1,a[5]=t5,tmp=(C_word)a,a+=6,tmp);
/* extras.scm:635: scheme#output-port? */
t7=*((C_word*)lf[147]+1);{
C_word *av2=av;
av2[0]=t7;
av2[1]=t6;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}}}}

/* k3776 in chicken.format#format in k3404 in k1084 in k1081 */
static void C_ccall f_3778(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3778,c,av);}{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)((C_word*)t0)[3])[1];
C_apply(4,av2);}}

/* k3801 in chicken.format#format in k3404 in k1084 in k1081 */
static void C_ccall f_3803(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_3803,c,av);}
a=C_alloc(3);
if(C_truep(t1)){
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],((C_word*)((C_word*)t0)[3])[1]);
t3=C_mutate(((C_word *)((C_word*)t0)[3])+1,t2);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[4];
av2[2]=*((C_word*)lf[138]+1);
av2[3]=((C_word*)((C_word*)t0)[3])[1];
C_apply(4,av2);}}
else{
/* extras.scm:637: ##sys#error */
t2=*((C_word*)lf[131]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[5];
av2[2]=lf[145];
av2[3]=lf[146];
av2[4]=((C_word*)t0)[2];
av2[5]=((C_word*)((C_word*)t0)[3])[1];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}}

/* k3811 in k3404 in k1084 in k1081 */
static void C_ccall f_3813(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(21,c,3)))){
C_save_and_reclaim((void *)f_3813,c,av);}
a=C_alloc(21);
t2=C_a_i_provide(&a,1,lf[148]);
t3=C_mutate((C_word*)lf[149]+1 /* (set! chicken.random#set-pseudo-random-seed! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3815,a[2]=((C_word)li71),tmp=(C_word)a,a+=3,tmp));
t4=C_mutate((C_word*)lf[153]+1 /* (set! chicken.random#pseudo-random-integer ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3854,a[2]=((C_word)li72),tmp=(C_word)a,a+=3,tmp));
t5=C_mutate((C_word*)lf[156]+1 /* (set! chicken.random#pseudo-random-real ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3870,a[2]=((C_word)li73),tmp=(C_word)a,a+=3,tmp));
t6=C_fix(C_MOST_POSITIVE_FIXNUM&(C_word)C_RANDOM_STATE_SIZE);
t7=C_mutate((C_word*)lf[157]+1 /* (set! chicken.random#random-bytes ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3873,a[2]=t6,a[3]=((C_word)li74),tmp=(C_word)a,a+=4,tmp));
t8=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t8;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}

/* chicken.random#set-pseudo-random-seed! in k3811 in k3404 in k1084 in k1081 */
static void C_ccall f_3815(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,3)))){
C_save_and_reclaim((void *)f_3815,c,av);}
a=C_alloc(11);
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,3,t0));
t5=t4;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3822,a[2]=t2,a[3]=t1,a[4]=t6,tmp=(C_word)a,a+=5,tmp);
if(C_truep(((C_word*)t6)[1])){
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3836,a[2]=t6,a[3]=t7,tmp=(C_word)a,a+=4,tmp);
/* extras.scm:651: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[25]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[25]+1);
av2[1]=t8;
av2[2]=((C_word*)t6)[1];
av2[3]=lf[150];
tp(4,av2);}}
else{
t8=C_block_size(t2);
t9=C_set_block_item(t6,0,t8);
t10=t7;{
C_word *av2=av;
av2[0]=t10;
av2[1]=t9;
f_3822(2,av2);}}}

/* k3820 in chicken.random#set-pseudo-random-seed! in k3811 in k3404 in k1084 in k1081 */
static void C_ccall f_3822(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_3822,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3825,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_byteblockp(((C_word*)t0)[2]))){
t3=C_block_size(((C_word*)t0)[2]);
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_set_random_seed(((C_word*)t0)[2],C_i_fixnum_min(((C_word*)((C_word*)t0)[4])[1],t3));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
/* extras.scm:656: ##sys#error */
t3=*((C_word*)lf[131]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[150];
av2[3]=lf[151];
av2[4]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}}

/* k3823 in k3820 in chicken.random#set-pseudo-random-seed! in k3811 in k3404 in k1084 in k1081 */
static void C_ccall f_3825(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3825,c,av);}
t2=C_block_size(((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_set_random_seed(((C_word*)t0)[2],C_i_fixnum_min(((C_word*)((C_word*)t0)[4])[1],t2));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k3834 in chicken.random#set-pseudo-random-seed! in k3811 in k3404 in k1084 in k1081 */
static void C_ccall f_3836(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_3836,c,av);}
if(C_truep(C_fixnum_lessp(((C_word*)((C_word*)t0)[2])[1],C_fix(0)))){
/* extras.scm:653: ##sys#error */
t2=*((C_word*)lf[131]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[150];
av2[3]=lf[152];
av2[4]=((C_word*)((C_word*)t0)[2])[1];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
f_3822(2,av2);}}}

/* chicken.random#pseudo-random-integer in k3811 in k3404 in k1084 in k1081 */
static void C_ccall f_3854(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(2,c,4)))){
C_save_and_reclaim((void *)f_3854,c,av);}
a=C_alloc(2);
if(C_truep(C_fixnump(t2))){
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_random_fixnum(t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
if(C_truep(C_i_not(C_i_bignump(t2)))){
/* extras.scm:666: ##sys#error */
t3=*((C_word*)lf[131]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=lf[154];
av2[3]=lf[155];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}
else{
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_s_a_u_i_random_int(&a,1,t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}}

/* chicken.random#pseudo-random-real in k3811 in k3404 in k1084 in k1081 */
static void C_ccall f_3870(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(2,c,1)))){
C_save_and_reclaim((void *)f_3870,c,av);}
a=C_alloc(2);
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_random_real(&a,0);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.random#random-bytes in k3811 in k3404 in k1084 in k1081 */
static void C_ccall f_3873(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_3873,c,av);}
a=C_alloc(10);
t2=C_rest_nullp(c,2);
t3=(C_truep(t2)?C_SCHEME_FALSE:C_get_rest_arg(c,2,av,2,t0));
t4=C_rest_nullp(c,2);
t5=C_rest_nullp(c,3);
t6=(C_truep(t5)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,2,t0));
t7=C_rest_nullp(c,3);
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3889,a[2]=t6,a[3]=t3,a[4]=t1,a[5]=((C_word*)t0)[2],tmp=(C_word)a,a+=6,tmp);
if(C_truep(t6)){
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3937,a[2]=t6,a[3]=t8,tmp=(C_word)a,a+=4,tmp);
/* extras.scm:677: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[25]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[25]+1);
av2[1]=t9;
av2[2]=t6;
av2[3]=lf[158];
tp(4,av2);}}
else{
t9=t8;{
C_word *av2=av;
av2[0]=t9;
av2[1]=C_SCHEME_UNDEFINED;
f_3889(2,av2);}}}

/* k3887 in chicken.random#random-bytes in k3811 in k3404 in k1084 in k1081 */
static void C_ccall f_3889(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_3889,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3892,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
if(C_truep(((C_word*)t0)[3])){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3914,a[2]=t2,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t4=C_immp(((C_word*)t0)[3]);
if(C_truep(t4)){
if(C_truep(t4)){
/* extras.scm:683: ##sys#error */
t5=*((C_word*)lf[131]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=lf[158];
av2[3]=lf[160];
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}
else{
t5=t2;{
C_word *av2=av;
av2[0]=t5;
av2[1]=((C_word*)t0)[3];
f_3892(2,av2);}}}
else{
if(C_truep(C_i_not(C_byteblockp(((C_word*)t0)[3])))){
/* extras.scm:683: ##sys#error */
t5=*((C_word*)lf[131]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=lf[158];
av2[3]=lf[160];
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}
else{
t5=t2;{
C_word *av2=av;
av2[0]=t5;
av2[1]=((C_word*)t0)[3];
f_3892(2,av2);}}}}
else{
if(C_truep(((C_word*)t0)[2])){
/* extras.scm:686: scheme#make-string */
t3=*((C_word*)lf[13]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
/* extras.scm:686: scheme#make-string */
t3=*((C_word*)lf[13]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}}

/* k3890 in k3887 in chicken.random#random-bytes in k3811 in k3404 in k1084 in k1081 */
static void C_ccall f_3892(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_3892,c,av);}
a=C_alloc(5);
t2=(C_truep(((C_word*)t0)[2])?((C_word*)t0)[2]:C_block_size(t1));
t3=C_random_bytes(t1,t2);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3899,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
if(C_truep(t3)){
t5=t4;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_UNDEFINED;
f_3899(2,av2);}}
else{
/* extras.scm:690: ##sys#error */
t5=*((C_word*)lf[131]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=lf[158];
av2[3]=lf[159];
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}}

/* k3897 in k3890 in k3887 in chicken.random#random-bytes in k3811 in k3404 in k1084 in k1081 */
static void C_ccall f_3899(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3899,c,av);}
t2=C_eqp(((C_word*)t0)[2],((C_word*)t0)[3]);
if(C_truep(t2)){
t3=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_string_to_bytevector(((C_word*)t0)[3]);
t4=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t4;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k3912 in k3887 in chicken.random#random-bytes in k3811 in k3404 in k1084 in k1081 */
static void C_ccall f_3914(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3914,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
f_3892(2,av2);}}

/* k3935 in chicken.random#random-bytes in k3811 in k3404 in k1084 in k1081 */
static void C_ccall f_3937(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_3937,c,av);}
if(C_truep(C_i_lessp(((C_word*)t0)[2],C_fix(0)))){
/* extras.scm:679: ##sys#error */
t2=*((C_word*)lf[131]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[158];
av2[3]=lf[161];
av2[4]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
f_3889(2,av2);}}}

/* toplevel */
static C_TLS int toplevel_initialized=0;

void C_ccall C_extras_toplevel(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(toplevel_initialized) {C_kontinue(t1,C_SCHEME_UNDEFINED);}
else C_toplevel_entry(C_text("extras"));
C_check_nursery_minimum(C_calculate_demand(3,c,2));
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void*)C_extras_toplevel,c,av);}
toplevel_initialized=1;
if(C_unlikely(!C_demand_2(791))){
C_save(t1);
C_rereclaim2(791*sizeof(C_word),1);
t1=C_restore;}
a=C_alloc(3);
C_initialize_lf(lf,165);
lf[0]=C_h_intern(&lf[0],6, C_text("extras"));
lf[1]=C_h_intern(&lf[1],11, C_text("chicken.io#"));
lf[2]=C_h_intern(&lf[2],11, C_text("scheme#read"));
lf[3]=C_h_intern(&lf[3],20, C_text("chicken.io#read-list"));
lf[4]=C_h_intern(&lf[4],20, C_text("##sys#standard-input"));
lf[5]=C_h_intern(&lf[5],9, C_text("read-list"));
lf[6]=C_h_intern(&lf[6],18, C_text("##sys#fast-reverse"));
lf[7]=C_h_intern(&lf[7],20, C_text("chicken.io#read-line"));
lf[8]=C_h_intern(&lf[8],9, C_text("read-line"));
lf[9]=C_h_intern(&lf[9],15, C_text("##sys#substring"));
lf[10]=C_h_intern(&lf[10],17, C_text("##sys#read-char-0"));
lf[11]=C_h_intern(&lf[11],16, C_text("scheme#peek-char"));
lf[12]=C_h_intern(&lf[12],19, C_text("##sys#string-append"));
lf[13]=C_h_intern(&lf[13],18, C_text("scheme#make-string"));
lf[14]=C_h_intern(&lf[14],17, C_text("##sys#make-string"));
lf[15]=C_h_intern(&lf[15],21, C_text("chicken.io#read-lines"));
lf[16]=C_h_intern(&lf[16],35, C_text("chicken.fixnum#most-positive-fixnum"));
lf[17]=C_h_intern(&lf[17],10, C_text("read-lines"));
lf[18]=C_h_intern(&lf[18],21, C_text("chicken.io#write-line"));
lf[19]=C_h_intern(&lf[19],21, C_text("##sys#standard-output"));
lf[20]=C_h_intern(&lf[20],10, C_text("write-line"));
lf[21]=C_h_intern(&lf[21],18, C_text("##sys#write-char-0"));
lf[22]=C_h_intern(&lf[22],28, C_text("chicken.io#read-string!/port"));
lf[23]=C_h_intern(&lf[23],23, C_text("chicken.io#read-string!"));
lf[24]=C_h_intern(&lf[24],12, C_text("read-string!"));
lf[25]=C_h_intern(&lf[25],18, C_text("##sys#check-fixnum"));
lf[26]=C_h_intern(&lf[26],27, C_text("chicken.io#read-string/port"));
lf[27]=C_decode_literal(C_heaptop,C_text("\376B\000\000\000"));
lf[28]=C_h_intern(&lf[28],30, C_text("chicken.base#get-output-string"));
lf[29]=C_h_intern(&lf[29],23, C_text("chicken.io#write-string"));
lf[30]=C_h_intern(&lf[30],31, C_text("chicken.base#open-output-string"));
lf[31]=C_h_intern(&lf[31],17, C_text("##sys#peek-char-0"));
lf[32]=C_h_intern(&lf[32],22, C_text("chicken.io#read-string"));
lf[33]=C_h_intern(&lf[33],11, C_text("read-string"));
lf[34]=C_h_intern(&lf[34],24, C_text("chicken.io#read-buffered"));
lf[35]=C_h_intern(&lf[35],13, C_text("read-buffered"));
lf[36]=C_decode_literal(C_heaptop,C_text("\376B\000\000\000"));
lf[37]=C_h_intern(&lf[37],21, C_text("chicken.io#read-token"));
lf[38]=C_h_intern(&lf[38],10, C_text("read-token"));
lf[39]=C_h_intern(&lf[39],12, C_text("write-string"));
lf[40]=C_h_intern(&lf[40],20, C_text("chicken.io#read-byte"));
lf[41]=C_h_intern(&lf[41],9, C_text("read-byte"));
lf[42]=C_h_intern(&lf[42],21, C_text("chicken.io#write-byte"));
lf[43]=C_h_intern(&lf[43],10, C_text("write-byte"));
lf[44]=C_h_intern(&lf[44],21, C_text("chicken.pretty-print#"));
lf[46]=C_h_intern(&lf[46],5, C_text("quote"));
lf[47]=C_h_intern(&lf[47],10, C_text("quasiquote"));
lf[48]=C_h_intern(&lf[48],7, C_text("unquote"));
lf[49]=C_h_intern(&lf[49],16, C_text("unquote-splicing"));
lf[50]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001\047"));
lf[51]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001`"));
lf[52]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001,"));
lf[53]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002,@"));
lf[54]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001 "));
lf[55]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001)"));
lf[56]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001)"));
lf[57]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003 . "));
lf[58]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001("));
lf[59]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002()"));
lf[60]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005#!eof"));
lf[61]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001#"));
lf[62]=C_h_intern(&lf[62],19, C_text("scheme#vector->list"));
lf[63]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002#t"));
lf[64]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002#f"));
lf[65]=C_h_intern(&lf[65],20, C_text("##sys#number->string"));
lf[66]=C_h_intern(&lf[66],11, C_text("##sys#print"));
lf[67]=C_h_intern(&lf[67],23, C_text("##sys#procedure->string"));
lf[68]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001\134"));
lf[69]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\377\012\000\000\011\376B\000\000\002\134t\376\003\000\000\002\376\003\000\000\002\376\377\012\000\000\012\376B\000\000\002\134n\376\003\000\000\002\376\003\000\000\002\376\377\012\000\000\015\376B\000\000\002\134r\376\003\000\000\002\376\003\000\000\002\376"
"\377\012\000\000\013\376B\000\000\002\134v\376\003\000\000\002\376\003\000\000\002\376\377\012\000\000\014\376B\000\000\002\134f\376\003\000\000\002\376\003\000\000\002\376\377\012\000\000\007\376B\000\000\002\134a\376\003\000\000\002\376\003\000\000\002\376\377\012\000\000\010\376B\000\000\002\134"
"b\376\377\016"));
lf[70]=C_decode_literal(C_heaptop,C_text("\376B\000\000\0010"));
lf[71]=C_decode_literal(C_heaptop,C_text("\376B\000\000\000"));
lf[72]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002\134x"));
lf[73]=C_h_intern(&lf[73],20, C_text("##sys#fixnum->string"));
lf[74]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001\042"));
lf[75]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001\042"));
lf[76]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001x"));
lf[77]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001U"));
lf[78]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001u"));
lf[79]=C_h_intern(&lf[79],22, C_text("chicken.base#char-name"));
lf[80]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002#\134"));
lf[81]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016#<unspecified>"));
lf[82]=C_h_intern(&lf[82],21, C_text("##sys#pointer->string"));
lf[83]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020#<unbound value>"));
lf[84]=C_h_intern(&lf[84],21, C_text("##sys#user-print-hook"));
lf[85]=C_h_intern(&lf[85],20, C_text("scheme#string-append"));
lf[86]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007#<port "));
lf[87]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001>"));
lf[88]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001}"));
lf[89]=C_decode_literal(C_heaptop,C_text("\376B\000\000\0010"));
lf[90]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003#${"));
lf[91]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001>"));
lf[92]=C_h_intern(&lf[92],25, C_text("##sys#lambda-info->string"));
lf[93]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016#<lambda info "));
lf[94]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025#<unprintable object>"));
lf[95]=C_h_intern(&lf[95],18, C_text("chicken.base#port\077"));
lf[96]=C_h_intern(&lf[96],24, C_text("chicken.keyword#keyword\077"));
lf[97]=C_h_intern(&lf[97],13, C_text("##sys#number\077"));
lf[98]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010        "));
lf[99]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010        "));
lf[100]=C_h_intern(&lf[100],36, C_text("chicken.string#reverse-string-append"));
lf[101]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001#"));
lf[102]=C_h_intern(&lf[102],10, C_text("scheme#max"));
lf[103]=C_h_intern(&lf[103],20, C_text("##sys#symbol->string"));
lf[104]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001("));
lf[105]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001("));
lf[106]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001)"));
lf[107]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001)"));
lf[108]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001."));
lf[109]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001 "));
lf[110]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001("));
lf[111]=C_h_intern(&lf[111],6, C_text("lambda"));
lf[112]=C_h_intern(&lf[112],2, C_text("if"));
lf[113]=C_h_intern(&lf[113],4, C_text("set!"));
lf[114]=C_h_intern(&lf[114],4, C_text("cond"));
lf[115]=C_h_intern(&lf[115],4, C_text("case"));
lf[116]=C_h_intern(&lf[116],3, C_text("and"));
lf[117]=C_h_intern(&lf[117],2, C_text("or"));
lf[118]=C_h_intern(&lf[118],3, C_text("let"));
lf[119]=C_h_intern(&lf[119],5, C_text("begin"));
lf[120]=C_h_intern(&lf[120],2, C_text("do"));
lf[121]=C_h_intern(&lf[121],4, C_text("let\052"));
lf[122]=C_h_intern(&lf[122],6, C_text("letrec"));
lf[123]=C_h_intern(&lf[123],7, C_text("letrec\052"));
lf[124]=C_h_intern(&lf[124],6, C_text("define"));
lf[125]=C_h_intern(&lf[125],39, C_text("chicken.pretty-print#pretty-print-width"));
lf[126]=C_h_intern(&lf[126],33, C_text("chicken.pretty-print#pretty-print"));
lf[127]=C_h_intern(&lf[127],14, C_text("scheme#display"));
lf[128]=C_h_intern(&lf[128],23, C_text("chicken.pretty-print#pp"));
lf[129]=C_h_intern(&lf[129],15, C_text("chicken.format#"));
lf[131]=C_h_intern(&lf[131],11, C_text("##sys#error"));
lf[132]=C_decode_literal(C_heaptop,C_text("\376B\000\000/too few arguments to formatted output procedure"));
lf[133]=C_h_intern(&lf[133],12, C_text("scheme#write"));
lf[134]=C_h_intern(&lf[134],18, C_text("##sys#flush-output"));
lf[135]=C_h_intern(&lf[135],14, C_text("scheme#newline"));
lf[136]=C_decode_literal(C_heaptop,C_text("\376B\000\000\037illegal format-string character"));
lf[137]=C_h_intern(&lf[137],15, C_text("##sys#tty-port\077"));
lf[138]=C_h_intern(&lf[138],22, C_text("chicken.format#fprintf"));
lf[139]=C_h_intern(&lf[139],7, C_text("fprintf"));
lf[140]=C_h_intern(&lf[140],21, C_text("chicken.format#printf"));
lf[141]=C_h_intern(&lf[141],6, C_text("printf"));
lf[142]=C_h_intern(&lf[142],22, C_text("chicken.format#sprintf"));
lf[143]=C_h_intern(&lf[143],7, C_text("sprintf"));
lf[144]=C_h_intern(&lf[144],21, C_text("chicken.format#format"));
lf[145]=C_h_intern(&lf[145],6, C_text("format"));
lf[146]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023illegal destination"));
lf[147]=C_h_intern(&lf[147],19, C_text("scheme#output-port\077"));
lf[148]=C_h_intern(&lf[148],15, C_text("chicken.random#"));
lf[149]=C_h_intern(&lf[149],38, C_text("chicken.random#set-pseudo-random-seed!"));
lf[150]=C_h_intern(&lf[150],23, C_text("set-pseudo-random-seed!"));
lf[151]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023invalid buffer type"));
lf[152]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014invalid size"));
lf[153]=C_h_intern(&lf[153],36, C_text("chicken.random#pseudo-random-integer"));
lf[154]=C_h_intern(&lf[154],21, C_text("pseudo-random-integer"));
lf[155]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021bad argument type"));
lf[156]=C_h_intern(&lf[156],33, C_text("chicken.random#pseudo-random-real"));
lf[157]=C_h_intern(&lf[157],27, C_text("chicken.random#random-bytes"));
lf[158]=C_h_intern(&lf[158],12, C_text("random-bytes"));
lf[159]=C_decode_literal(C_heaptop,C_text("\376B\000\000\033unable to read random bytes"));
lf[160]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023invalid buffer type"));
lf[161]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014invalid size"));
lf[162]=C_h_intern(&lf[162],34, C_text("chicken.platform#register-feature!"));
lf[163]=C_h_intern(&lf[163],7, C_text("srfi-28"));
lf[164]=C_h_intern(&lf[164],27, C_text("chicken.base#make-parameter"));
C_register_lf2(lf,165,create_ptable());{}
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1083,a[2]=t1,tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_data_2dstructures_toplevel(2,av2);}}

#ifdef C_ENABLE_PTABLES
static C_PTABLE_ENTRY ptable[233] = {
{C_text("f_1083:extras_2escm"),(void*)f_1083},
{C_text("f_1086:extras_2escm"),(void*)f_1086},
{C_text("f_1088:extras_2escm"),(void*)f_1088},
{C_text("f_1117:extras_2escm"),(void*)f_1117},
{C_text("f_1119:extras_2escm"),(void*)f_1119},
{C_text("f_1139:extras_2escm"),(void*)f_1139},
{C_text("f_1191:extras_2escm"),(void*)f_1191},
{C_text("f_1201:extras_2escm"),(void*)f_1201},
{C_text("f_1211:extras_2escm"),(void*)f_1211},
{C_text("f_1224:extras_2escm"),(void*)f_1224},
{C_text("f_1229:extras_2escm"),(void*)f_1229},
{C_text("f_1242:extras_2escm"),(void*)f_1242},
{C_text("f_1275:extras_2escm"),(void*)f_1275},
{C_text("f_1284:extras_2escm"),(void*)f_1284},
{C_text("f_1307:extras_2escm"),(void*)f_1307},
{C_text("f_1315:extras_2escm"),(void*)f_1315},
{C_text("f_1344:extras_2escm"),(void*)f_1344},
{C_text("f_1369:extras_2escm"),(void*)f_1369},
{C_text("f_1382:extras_2escm"),(void*)f_1382},
{C_text("f_1428:extras_2escm"),(void*)f_1428},
{C_text("f_1444:extras_2escm"),(void*)f_1444},
{C_text("f_1456:extras_2escm"),(void*)f_1456},
{C_text("f_1474:extras_2escm"),(void*)f_1474},
{C_text("f_1478:extras_2escm"),(void*)f_1478},
{C_text("f_1533:extras_2escm"),(void*)f_1533},
{C_text("f_1537:extras_2escm"),(void*)f_1537},
{C_text("f_1540:extras_2escm"),(void*)f_1540},
{C_text("f_1588:extras_2escm"),(void*)f_1588},
{C_text("f_1610:extras_2escm"),(void*)f_1610},
{C_text("f_1616:extras_2escm"),(void*)f_1616},
{C_text("f_1619:extras_2escm"),(void*)f_1619},
{C_text("f_1665:extras_2escm"),(void*)f_1665},
{C_text("f_1684:extras_2escm"),(void*)f_1684},
{C_text("f_1687:extras_2escm"),(void*)f_1687},
{C_text("f_1699:extras_2escm"),(void*)f_1699},
{C_text("f_1702:extras_2escm"),(void*)f_1702},
{C_text("f_1707:extras_2escm"),(void*)f_1707},
{C_text("f_1711:extras_2escm"),(void*)f_1711},
{C_text("f_1714:extras_2escm"),(void*)f_1714},
{C_text("f_1726:extras_2escm"),(void*)f_1726},
{C_text("f_1734:extras_2escm"),(void*)f_1734},
{C_text("f_1736:extras_2escm"),(void*)f_1736},
{C_text("f_1755:extras_2escm"),(void*)f_1755},
{C_text("f_1789:extras_2escm"),(void*)f_1789},
{C_text("f_1817:extras_2escm"),(void*)f_1817},
{C_text("f_1827:extras_2escm"),(void*)f_1827},
{C_text("f_1832:extras_2escm"),(void*)f_1832},
{C_text("f_1836:extras_2escm"),(void*)f_1836},
{C_text("f_1842:extras_2escm"),(void*)f_1842},
{C_text("f_1845:extras_2escm"),(void*)f_1845},
{C_text("f_1852:extras_2escm"),(void*)f_1852},
{C_text("f_1873:extras_2escm"),(void*)f_1873},
{C_text("f_1878:extras_2escm"),(void*)f_1878},
{C_text("f_1885:extras_2escm"),(void*)f_1885},
{C_text("f_1895:extras_2escm"),(void*)f_1895},
{C_text("f_1916:extras_2escm"),(void*)f_1916},
{C_text("f_1921:extras_2escm"),(void*)f_1921},
{C_text("f_1956:extras_2escm"),(void*)f_1956},
{C_text("f_1966:extras_2escm"),(void*)f_1966},
{C_text("f_1983:extras_2escm"),(void*)f_1983},
{C_text("f_1990:extras_2escm"),(void*)f_1990},
{C_text("f_2008:extras_2escm"),(void*)f_2008},
{C_text("f_2011:extras_2escm"),(void*)f_2011},
{C_text("f_2039:extras_2escm"),(void*)f_2039},
{C_text("f_2073:extras_2escm"),(void*)f_2073},
{C_text("f_2112:extras_2escm"),(void*)f_2112},
{C_text("f_2122:extras_2escm"),(void*)f_2122},
{C_text("f_2131:extras_2escm"),(void*)f_2131},
{C_text("f_2134:extras_2escm"),(void*)f_2134},
{C_text("f_2141:extras_2escm"),(void*)f_2141},
{C_text("f_2152:extras_2escm"),(void*)f_2152},
{C_text("f_2161:extras_2escm"),(void*)f_2161},
{C_text("f_2177:extras_2escm"),(void*)f_2177},
{C_text("f_2179:extras_2escm"),(void*)f_2179},
{C_text("f_2201:extras_2escm"),(void*)f_2201},
{C_text("f_2207:extras_2escm"),(void*)f_2207},
{C_text("f_2223:extras_2escm"),(void*)f_2223},
{C_text("f_2227:extras_2escm"),(void*)f_2227},
{C_text("f_2236:extras_2escm"),(void*)f_2236},
{C_text("f_2279:extras_2escm"),(void*)f_2279},
{C_text("f_2283:extras_2escm"),(void*)f_2283},
{C_text("f_2302:extras_2escm"),(void*)f_2302},
{C_text("f_2309:extras_2escm"),(void*)f_2309},
{C_text("f_2315:extras_2escm"),(void*)f_2315},
{C_text("f_2321:extras_2escm"),(void*)f_2321},
{C_text("f_2324:extras_2escm"),(void*)f_2324},
{C_text("f_2331:extras_2escm"),(void*)f_2331},
{C_text("f_2344:extras_2escm"),(void*)f_2344},
{C_text("f_2363:extras_2escm"),(void*)f_2363},
{C_text("f_2365:extras_2escm"),(void*)f_2365},
{C_text("f_2393:extras_2escm"),(void*)f_2393},
{C_text("f_2397:extras_2escm"),(void*)f_2397},
{C_text("f_2401:extras_2escm"),(void*)f_2401},
{C_text("f_2424:extras_2escm"),(void*)f_2424},
{C_text("f_2429:extras_2escm"),(void*)f_2429},
{C_text("f_2430:extras_2escm"),(void*)f_2430},
{C_text("f_2448:extras_2escm"),(void*)f_2448},
{C_text("f_2452:extras_2escm"),(void*)f_2452},
{C_text("f_2460:extras_2escm"),(void*)f_2460},
{C_text("f_2471:extras_2escm"),(void*)f_2471},
{C_text("f_2489:extras_2escm"),(void*)f_2489},
{C_text("f_2493:extras_2escm"),(void*)f_2493},
{C_text("f_2516:extras_2escm"),(void*)f_2516},
{C_text("f_2524:extras_2escm"),(void*)f_2524},
{C_text("f_2527:extras_2escm"),(void*)f_2527},
{C_text("f_2531:extras_2escm"),(void*)f_2531},
{C_text("f_2551:extras_2escm"),(void*)f_2551},
{C_text("f_2558:extras_2escm"),(void*)f_2558},
{C_text("f_2569:extras_2escm"),(void*)f_2569},
{C_text("f_2576:extras_2escm"),(void*)f_2576},
{C_text("f_2592:extras_2escm"),(void*)f_2592},
{C_text("f_2610:extras_2escm"),(void*)f_2610},
{C_text("f_2625:extras_2escm"),(void*)f_2625},
{C_text("f_2628:extras_2escm"),(void*)f_2628},
{C_text("f_2635:extras_2escm"),(void*)f_2635},
{C_text("f_2641:extras_2escm"),(void*)f_2641},
{C_text("f_2648:extras_2escm"),(void*)f_2648},
{C_text("f_2658:extras_2escm"),(void*)f_2658},
{C_text("f_2664:extras_2escm"),(void*)f_2664},
{C_text("f_2669:extras_2escm"),(void*)f_2669},
{C_text("f_2682:extras_2escm"),(void*)f_2682},
{C_text("f_2685:extras_2escm"),(void*)f_2685},
{C_text("f_2696:extras_2escm"),(void*)f_2696},
{C_text("f_2708:extras_2escm"),(void*)f_2708},
{C_text("f_2711:extras_2escm"),(void*)f_2711},
{C_text("f_2718:extras_2escm"),(void*)f_2718},
{C_text("f_2726:extras_2escm"),(void*)f_2726},
{C_text("f_2729:extras_2escm"),(void*)f_2729},
{C_text("f_2753:extras_2escm"),(void*)f_2753},
{C_text("f_2760:extras_2escm"),(void*)f_2760},
{C_text("f_2762:extras_2escm"),(void*)f_2762},
{C_text("f_2778:extras_2escm"),(void*)f_2778},
{C_text("f_2785:extras_2escm"),(void*)f_2785},
{C_text("f_2796:extras_2escm"),(void*)f_2796},
{C_text("f_2809:extras_2escm"),(void*)f_2809},
{C_text("f_2812:extras_2escm"),(void*)f_2812},
{C_text("f_2825:extras_2escm"),(void*)f_2825},
{C_text("f_2841:extras_2escm"),(void*)f_2841},
{C_text("f_2845:extras_2escm"),(void*)f_2845},
{C_text("f_2847:extras_2escm"),(void*)f_2847},
{C_text("f_2883:extras_2escm"),(void*)f_2883},
{C_text("f_2890:extras_2escm"),(void*)f_2890},
{C_text("f_2901:extras_2escm"),(void*)f_2901},
{C_text("f_2917:extras_2escm"),(void*)f_2917},
{C_text("f_2943:extras_2escm"),(void*)f_2943},
{C_text("f_2948:extras_2escm"),(void*)f_2948},
{C_text("f_2952:extras_2escm"),(void*)f_2952},
{C_text("f_2972:extras_2escm"),(void*)f_2972},
{C_text("f_2974:extras_2escm"),(void*)f_2974},
{C_text("f_2978:extras_2escm"),(void*)f_2978},
{C_text("f_2983:extras_2escm"),(void*)f_2983},
{C_text("f_2989:extras_2escm"),(void*)f_2989},
{C_text("f_3010:extras_2escm"),(void*)f_3010},
{C_text("f_3016:extras_2escm"),(void*)f_3016},
{C_text("f_3038:extras_2escm"),(void*)f_3038},
{C_text("f_3042:extras_2escm"),(void*)f_3042},
{C_text("f_3050:extras_2escm"),(void*)f_3050},
{C_text("f_3054:extras_2escm"),(void*)f_3054},
{C_text("f_3056:extras_2escm"),(void*)f_3056},
{C_text("f_3059:extras_2escm"),(void*)f_3059},
{C_text("f_3080:extras_2escm"),(void*)f_3080},
{C_text("f_3084:extras_2escm"),(void*)f_3084},
{C_text("f_3098:extras_2escm"),(void*)f_3098},
{C_text("f_3119:extras_2escm"),(void*)f_3119},
{C_text("f_3123:extras_2escm"),(void*)f_3123},
{C_text("f_3137:extras_2escm"),(void*)f_3137},
{C_text("f_3148:extras_2escm"),(void*)f_3148},
{C_text("f_3161:extras_2escm"),(void*)f_3161},
{C_text("f_3176:extras_2escm"),(void*)f_3176},
{C_text("f_3194:extras_2escm"),(void*)f_3194},
{C_text("f_3196:extras_2escm"),(void*)f_3196},
{C_text("f_3202:extras_2escm"),(void*)f_3202},
{C_text("f_3208:extras_2escm"),(void*)f_3208},
{C_text("f_3214:extras_2escm"),(void*)f_3214},
{C_text("f_3220:extras_2escm"),(void*)f_3220},
{C_text("f_3226:extras_2escm"),(void*)f_3226},
{C_text("f_3232:extras_2escm"),(void*)f_3232},
{C_text("f_3252:extras_2escm"),(void*)f_3252},
{C_text("f_3258:extras_2escm"),(void*)f_3258},
{C_text("f_3267:extras_2escm"),(void*)f_3267},
{C_text("f_3277:extras_2escm"),(void*)f_3277},
{C_text("f_3393:extras_2escm"),(void*)f_3393},
{C_text("f_3397:extras_2escm"),(void*)f_3397},
{C_text("f_3406:extras_2escm"),(void*)f_3406},
{C_text("f_3408:extras_2escm"),(void*)f_3408},
{C_text("f_3415:extras_2escm"),(void*)f_3415},
{C_text("f_3419:extras_2escm"),(void*)f_3419},
{C_text("f_3421:extras_2escm"),(void*)f_3421},
{C_text("f_3425:extras_2escm"),(void*)f_3425},
{C_text("f_3432:extras_2escm"),(void*)f_3432},
{C_text("f_3439:extras_2escm"),(void*)f_3439},
{C_text("f_3442:extras_2escm"),(void*)f_3442},
{C_text("f_3464:extras_2escm"),(void*)f_3464},
{C_text("f_3470:extras_2escm"),(void*)f_3470},
{C_text("f_3479:extras_2escm"),(void*)f_3479},
{C_text("f_3486:extras_2escm"),(void*)f_3486},
{C_text("f_3505:extras_2escm"),(void*)f_3505},
{C_text("f_3518:extras_2escm"),(void*)f_3518},
{C_text("f_3543:extras_2escm"),(void*)f_3543},
{C_text("f_3556:extras_2escm"),(void*)f_3556},
{C_text("f_3569:extras_2escm"),(void*)f_3569},
{C_text("f_3582:extras_2escm"),(void*)f_3582},
{C_text("f_3586:extras_2escm"),(void*)f_3586},
{C_text("f_3599:extras_2escm"),(void*)f_3599},
{C_text("f_3603:extras_2escm"),(void*)f_3603},
{C_text("f_3616:extras_2escm"),(void*)f_3616},
{C_text("f_3620:extras_2escm"),(void*)f_3620},
{C_text("f_3638:extras_2escm"),(void*)f_3638},
{C_text("f_3641:extras_2escm"),(void*)f_3641},
{C_text("f_3647:extras_2escm"),(void*)f_3647},
{C_text("f_3683:extras_2escm"),(void*)f_3683},
{C_text("f_3741:extras_2escm"),(void*)f_3741},
{C_text("f_3752:extras_2escm"),(void*)f_3752},
{C_text("f_3758:extras_2escm"),(void*)f_3758},
{C_text("f_3764:extras_2escm"),(void*)f_3764},
{C_text("f_3770:extras_2escm"),(void*)f_3770},
{C_text("f_3778:extras_2escm"),(void*)f_3778},
{C_text("f_3803:extras_2escm"),(void*)f_3803},
{C_text("f_3813:extras_2escm"),(void*)f_3813},
{C_text("f_3815:extras_2escm"),(void*)f_3815},
{C_text("f_3822:extras_2escm"),(void*)f_3822},
{C_text("f_3825:extras_2escm"),(void*)f_3825},
{C_text("f_3836:extras_2escm"),(void*)f_3836},
{C_text("f_3854:extras_2escm"),(void*)f_3854},
{C_text("f_3870:extras_2escm"),(void*)f_3870},
{C_text("f_3873:extras_2escm"),(void*)f_3873},
{C_text("f_3889:extras_2escm"),(void*)f_3889},
{C_text("f_3892:extras_2escm"),(void*)f_3892},
{C_text("f_3899:extras_2escm"),(void*)f_3899},
{C_text("f_3914:extras_2escm"),(void*)f_3914},
{C_text("f_3937:extras_2escm"),(void*)f_3937},
{C_text("toplevel:extras_2escm"),(void*)C_extras_toplevel},
{NULL,NULL}};
#endif

static C_PTABLE_ENTRY *create_ptable(void){
#ifdef C_ENABLE_PTABLES
return ptable;
#else
return NULL;
#endif
}

/*
o|hiding unexported module binding: chicken.io#d 
o|hiding unexported module binding: chicken.io#define-alias 
o|hiding unexported module binding: chicken.io#read-string!/port 
o|hiding unexported module binding: chicken.io#read-string/port 
o|hiding unexported module binding: chicken.pretty-print#generic-write 
o|hiding unexported module binding: chicken.format#fprintf0 
o|eliminated procedure checks: 27 
o|specializations:
o|  11 (scheme#eqv? (or eof null fixnum char boolean symbol keyword) *)
o|  1 (scheme#current-output-port)
o|  4 (scheme#make-string fixnum char)
o|  3 (scheme#number->string fixnum fixnum)
o|  1 (scheme#assq * (list-of pair))
o|  2 (scheme#char<? char char)
o|  1 (scheme#+ fixnum fixnum)
o|  3 (scheme#char=? char char)
o|  5 (scheme#car pair)
o|  13 (scheme#cdr pair)
o|  1 (scheme#make-string fixnum)
o|  4 (##sys#check-output-port * * *)
o|  24 (scheme#eqv? * (or eof null fixnum char boolean symbol keyword))
o|  8 (##sys#check-input-port * * *)
(o e)|safe calls: 338 
(o e)|assignments to immediate values: 8 
o|safe globals: (chicken.pretty-print#generic-write chicken.io#write-byte chicken.io#read-byte chicken.io#write-string chicken.io#read-token chicken.io#read-buffered chicken.io#read-string chicken.io#read-string/port chicken.io#read-string! chicken.io#read-string!/port chicken.io#write-line chicken.io#read-lines chicken.io#read-line chicken.io#read-list) 
o|substituted constant variable: a1109 
o|substituted constant variable: a1110 
o|inlining procedure: k1121 
o|inlining procedure: k1121 
o|inlining procedure: k1148 
o|inlining procedure: k1148 
o|substituted constant variable: a1203 
o|substituted constant variable: a1204 
o|inlining procedure: k1208 
o|inlining procedure: k1208 
o|inlining procedure: k1231 
o|inlining procedure: k1231 
o|inlining procedure: k1249 
o|inlining procedure: k1249 
o|inlining procedure: k1258 
o|inlining procedure: k1258 
o|inlining procedure: k1276 
o|inlining procedure: k1276 
o|substituted constant variable: a1317 
o|substituted constant variable: a1319 
o|inlining procedure: k1327 
o|inlining procedure: k1327 
o|substituted constant variable: a1359 
o|substituted constant variable: a1360 
o|inlining procedure: k1371 
o|inlining procedure: k1371 
o|substituted constant variable: a1434 
o|substituted constant variable: a1435 
o|inlining procedure: k1458 
o|inlining procedure: k1458 
o|inlining procedure: k1482 
o|inlining procedure: k1482 
o|inlining procedure: k1541 
o|inlining procedure: k1541 
o|substituted constant variable: a1603 
o|substituted constant variable: a1604 
o|inlining procedure: k1667 
o|inlining procedure: k1667 
o|inlining procedure: k1679 
o|inlining procedure: k1679 
o|inlining procedure: k1715 
o|inlining procedure: k1715 
o|substituted constant variable: a1730 
o|substituted constant variable: a1751 
o|substituted constant variable: a1752 
o|inlining procedure: k1753 
o|inlining procedure: k1753 
o|substituted constant variable: a1795 
o|substituted constant variable: a1796 
o|inlining procedure: k1800 
o|inlining procedure: k1800 
o|substituted constant variable: a1823 
o|substituted constant variable: a1824 
o|inlining procedure: k1837 
o|inlining procedure: k1837 
o|substituted constant variable: a1881 
o|substituted constant variable: a1882 
o|inlining procedure: k1893 
o|inlining procedure: k1893 
o|inlining procedure: k1926 
o|inlining procedure: k1926 
o|substituted constant variable: a1962 
o|substituted constant variable: a1963 
o|inlining procedure: k1967 
o|inlining procedure: k1967 
o|substituted constant variable: a1992 
o|substituted constant variable: a1993 
o|inlining procedure: k2031 
o|contracted procedure: "(extras.scm:260) length1?416" 
o|inlining procedure: k2016 
o|inlining procedure: k2016 
o|inlining procedure: k2031 
o|inlining procedure: k2046 
o|inlining procedure: k2046 
o|substituted constant variable: a2059 
o|substituted constant variable: a2061 
o|substituted constant variable: a2063 
o|substituted constant variable: a2065 
o|inlining procedure: k2079 
o|inlining procedure: k2079 
o|inlining procedure: k2091 
o|inlining procedure: k2091 
o|substituted constant variable: a2104 
o|substituted constant variable: a2106 
o|substituted constant variable: a2108 
o|substituted constant variable: a2110 
o|inlining procedure: k2114 
o|inlining procedure: k2114 
o|inlining procedure: k2136 
o|inlining procedure: "(extras.scm:281) read-macro-body410" 
o|inlining procedure: k2136 
o|inlining procedure: k2163 
o|inlining procedure: k2181 
o|inlining procedure: k2181 
o|inlining procedure: k2208 
o|inlining procedure: k2208 
o|inlining procedure: k2163 
o|inlining procedure: k2240 
o|inlining procedure: k2240 
o|inlining procedure: k2258 
o|inlining procedure: k2258 
o|inlining procedure: k2284 
o|inlining procedure: k2294 
o|inlining procedure: k2294 
o|inlining procedure: k2284 
o|inlining procedure: k2310 
o|inlining procedure: k2310 
o|inlining procedure: k2345 
o|inlining procedure: k2367 
o|substituted constant variable: a2380 
o|substituted constant variable: a2389 
o|substituted constant variable: a2406 
o|inlining procedure: k2402 
o|substituted constant variable: a2426 
o|inlining procedure: k2427 
o|inlining procedure: k2427 
o|substituted constant variable: a2462 
o|substituted constant variable: a2467 
o|inlining procedure: k2402 
o|substituted constant variable: a2480 
o|substituted constant variable: a2482 
o|inlining procedure: k2367 
o|inlining procedure: k2345 
o|inlining procedure: k2507 
o|substituted constant variable: a2517 
o|inlining procedure: k2507 
o|inlining procedure: k2543 
o|substituted constant variable: a2560 
o|inlining procedure: k2543 
o|substituted constant variable: a2578 
o|inlining procedure: k2580 
o|inlining procedure: k2580 
o|substituted constant variable: a2593 
o|inlining procedure: k2595 
o|inlining procedure: k2595 
o|inlining procedure: k2611 
o|inlining procedure: k2611 
o|inlining procedure: k2636 
o|inlining procedure: k2636 
o|inlining procedure: k2671 
o|inlining procedure: k2671 
o|inlining procedure: k2703 
o|inlining procedure: k2703 
o|inlining procedure: k2731 
o|inlining procedure: k2731 
o|inlining procedure: k2764 
o|inlining procedure: k2773 
o|inlining procedure: k2773 
o|substituted constant variable: a2786 
o|substituted constant variable: a2787 
o|inlining procedure: k2764 
o|inlining procedure: k2798 
o|inlining procedure: k2826 
o|inlining procedure: k2826 
o|substituted constant variable: max-expr-width554 
o|inlining procedure: k2798 
o|inlining procedure: k2885 
o|inlining procedure: "(extras.scm:415) read-macro-body410" 
o|inlining procedure: k2885 
o|inlining procedure: k2918 
o|inlining procedure: k2918 
o|substituted constant variable: max-call-head-width553 
o|inlining procedure: k2953 
o|inlining procedure: k2953 
o|inlining procedure: k2991 
o|inlining procedure: k3023 
o|inlining procedure: k3023 
o|inlining procedure: k2991 
o|inlining procedure: k3061 
o|inlining procedure: k3061 
o|inlining procedure: k3100 
o|inlining procedure: k3100 
o|inlining procedure: k3149 
o|substituted constant variable: indent-general552 
o|inlining procedure: k3149 
o|substituted constant variable: indent-general552 
o|inlining procedure: k3269 
o|inlining procedure: k3269 
o|inlining procedure: k3287 
o|inlining procedure: k3287 
o|inlining procedure: k3299 
o|inlining procedure: k3299 
o|inlining procedure: k3314 
o|inlining procedure: k3314 
o|substituted constant variable: a3327 
o|substituted constant variable: a3329 
o|substituted constant variable: a3331 
o|substituted constant variable: a3336 
o|substituted constant variable: a3338 
o|substituted constant variable: a3340 
o|substituted constant variable: a3342 
o|substituted constant variable: a3347 
o|substituted constant variable: a3349 
o|inlining procedure: k3353 
o|inlining procedure: k3353 
o|inlining procedure: k3365 
o|inlining procedure: k3365 
o|substituted constant variable: a3372 
o|substituted constant variable: a3374 
o|substituted constant variable: a3376 
o|substituted constant variable: a3378 
o|substituted constant variable: a3380 
o|inlining procedure: k3384 
o|substituted constant variable: a3398 
o|substituted constant variable: a3399 
o|inlining procedure: k3384 
o|inlining procedure: k3443 
o|inlining procedure: k3443 
o|inlining procedure: k3488 
o|inlining procedure: k3488 
o|inlining procedure: k3507 
o|inlining procedure: k3507 
o|inlining procedure: k3531 
o|inlining procedure: k3531 
o|inlining procedure: k3557 
o|inlining procedure: k3557 
o|inlining procedure: k3587 
o|inlining procedure: k3587 
o|inlining procedure: k3621 
o|inlining procedure: k3621 
o|inlining procedure: k3648 
o|inlining procedure: k3648 
o|inlining procedure: k3669 
o|inlining procedure: k3685 
o|inlining procedure: k3685 
o|inlining procedure: k3669 
o|substituted constant variable: a3709 
o|substituted constant variable: a3711 
o|substituted constant variable: a3713 
o|substituted constant variable: a3715 
o|substituted constant variable: a3717 
o|substituted constant variable: a3719 
o|substituted constant variable: a3721 
o|substituted constant variable: a3723 
o|substituted constant variable: a3725 
o|substituted constant variable: a3727 
o|substituted constant variable: a3729 
o|substituted constant variable: a3749 
o|inlining procedure: k3776 
o|propagated global variable: r37774153 chicken.format#sprintf 
o|inlining procedure: k3776 
o|inlining procedure: k3788 
o|propagated global variable: r37894157 chicken.format#sprintf 
o|inlining procedure: k3788 
o|inlining procedure: k3837 
o|inlining procedure: k3837 
o|inlining procedure: k3856 
o|inlining procedure: k3856 
o|inlining procedure: k3900 
o|inlining procedure: k3900 
o|inlining procedure: k3912 
o|inlining procedure: k3912 
o|inlining procedure: k3932 
o|inlining procedure: k3932 
o|inlining procedure: k3938 
o|inlining procedure: k3938 
o|replaced variables: 930 
o|removed binding forms: 137 
o|substituted constant variable: r11493974 
o|substituted constant variable: r13283986 
o|substituted constant variable: r14593989 
o|substituted constant variable: r16683995 
o|substituted constant variable: r18014012 
o|substituted constant variable: r20174025 
o|substituted constant variable: r20324026 
o|removed side-effect free assignment to unused variable: read-macro-body410 
o|substituted constant variable: r20804029 
o|substituted constant variable: r20924031 
o|substituted constant variable: r21154034 
o|substituted constant variable: r22954053 
o|substituted constant variable: r22954053 
o|substituted constant variable: r22954055 
o|substituted constant variable: r22954055 
o|substituted constant variable: r25814074 
o|substituted constant variable: r25814074 
o|substituted constant variable: r25814076 
o|substituted constant variable: r25814076 
o|substituted constant variable: r27744092 
o|substituted constant variable: r27654093 
o|substituted constant variable: r29544108 
o|substituted constant variable: r29924112 
o|removed side-effect free assignment to unused variable: indent-general552 
o|removed side-effect free assignment to unused variable: max-call-head-width553 
o|removed side-effect free assignment to unused variable: max-expr-width554 
o|inlining procedure: k3516 
o|propagated global variable: a37754154 chicken.format#sprintf 
o|inlining procedure: k3776 
o|propagated global variable: r37774238 chicken.format#printf 
o|propagated global variable: r37774238 chicken.format#printf 
o|inlining procedure: k3776 
o|propagated global variable: r37774240 chicken.format#sprintf 
o|propagated global variable: r37774240 chicken.format#sprintf 
o|inlining procedure: k3776 
o|propagated global variable: r37774242 chicken.format#fprintf 
o|propagated global variable: r37774242 chicken.format#fprintf 
o|replaced variables: 32 
o|removed binding forms: 750 
o|inlining procedure: k3237 
o|inlining procedure: k3739 
o|removed binding forms: 72 
o|substituted constant variable: r32384266 
o|substituted constant variable: r37404273 
o|inlining procedure: k3918 
o|removed conditional forms: 1 
o|replaced variables: 1 
o|removed binding forms: 2 
o|removed binding forms: 1 
o|simplifications: ((let . 6) (if . 63) (##core#call . 282)) 
o|  call simplifications:
o|    ##sys#immediate?
o|    scheme#apply	5
o|    scheme#char-upcase
o|    scheme#char-whitespace?	2
o|    ##sys#check-list
o|    scheme#<	2
o|    scheme#>	5
o|    scheme#-	5
o|    scheme#vector?	2
o|    scheme#boolean?	2
o|    scheme#symbol?	3
o|    scheme#procedure?
o|    scheme#string?	2
o|    scheme#char?
o|    ##sys#generic-structure?
o|    ##sys#byte
o|    chicken.fixnum#fx>	2
o|    scheme#string-ref
o|    scheme#string-length	4
o|    scheme#+	11
o|    scheme#integer->char
o|    scheme#char->integer	3
o|    ##sys#size	7
o|    chicken.fixnum#fx<=
o|    ##sys#setislot
o|    scheme#not	9
o|    chicken.fixnum#fx<	7
o|    ##sys#check-string	4
o|    chicken.fixnum#fx-	5
o|    scheme#pair?	14
o|    scheme#cadr	3
o|    ##sys#slot	16
o|    scheme#eq?	45
o|    scheme#char=?
o|    chicken.fixnum#fx=
o|    scheme#car	27
o|    scheme#null?	36
o|    scheme#cdr	14
o|    scheme#eof-object?	8
o|    chicken.fixnum#fx>=	5
o|    chicken.fixnum#fx+	16
o|    scheme#cons	5
o|contracted procedure: k1184 
o|contracted procedure: k1090 
o|contracted procedure: k1178 
o|contracted procedure: k1093 
o|contracted procedure: k1172 
o|contracted procedure: k1096 
o|contracted procedure: k1166 
o|contracted procedure: k1099 
o|contracted procedure: k1160 
o|contracted procedure: k1102 
o|contracted procedure: k1154 
o|contracted procedure: k1105 
o|contracted procedure: k1124 
o|contracted procedure: k1127 
o|contracted procedure: k1141 
o|contracted procedure: k1145 
o|contracted procedure: k1193 
o|contracted procedure: k1196 
o|contracted procedure: k1324 
o|contracted procedure: k1205 
o|contracted procedure: k1219 
o|contracted procedure: k1234 
o|contracted procedure: k1246 
o|contracted procedure: k1252 
o|contracted procedure: k1261 
o|contracted procedure: k1270 
o|contracted procedure: k1279 
o|contracted procedure: k1298 
o|contracted procedure: k1301 
o|contracted procedure: k1309 
o|contracted procedure: k1337 
o|contracted procedure: k1330 
o|contracted procedure: k1421 
o|contracted procedure: k1346 
o|contracted procedure: k1415 
o|contracted procedure: k1349 
o|contracted procedure: k1409 
o|contracted procedure: k1352 
o|contracted procedure: k1403 
o|contracted procedure: k1355 
o|contracted procedure: k1365 
o|contracted procedure: k1374 
o|contracted procedure: k1386 
o|contracted procedure: k1396 
o|contracted procedure: k1400 
o|contracted procedure: k1430 
o|contracted procedure: k1436 
o|contracted procedure: k1449 
o|contracted procedure: k1439 
o|contracted procedure: k1461 
o|contracted procedure: k1584 
o|contracted procedure: k1464 
o|contracted procedure: k1526 
o|contracted procedure: k1522 
o|contracted procedure: k1479 
o|contracted procedure: k1485 
o|contracted procedure: k1491 
o|contracted procedure: k1494 
o|contracted procedure: k1501 
o|contracted procedure: k1505 
o|contracted procedure: k1509 
o|contracted procedure: k1544 
o|contracted procedure: k1550 
o|contracted procedure: k1553 
o|contracted procedure: k1560 
o|contracted procedure: k1564 
o|contracted procedure: k1568 
o|contracted procedure: k1580 
o|contracted procedure: k1658 
o|contracted procedure: k1590 
o|contracted procedure: k1652 
o|contracted procedure: k1593 
o|contracted procedure: k1646 
o|contracted procedure: k1596 
o|contracted procedure: k1640 
o|contracted procedure: k1599 
o|contracted procedure: k1605 
o|contracted procedure: k1611 
o|contracted procedure: k1627 
o|contracted procedure: k1634 
o|contracted procedure: k1670 
o|contracted procedure: k1676 
o|contracted procedure: k1691 
o|contracted procedure: k1718 
o|contracted procedure: k1780 
o|contracted procedure: k1738 
o|contracted procedure: k1774 
o|contracted procedure: k1741 
o|contracted procedure: k1768 
o|contracted procedure: k1744 
o|contracted procedure: k1762 
o|contracted procedure: k1747 
o|contracted procedure: k1810 
o|contracted procedure: k1791 
o|contracted procedure: k1807 
o|contracted procedure: k1797 
o|contracted procedure: k1866 
o|contracted procedure: k1819 
o|contracted procedure: k1863 
o|contracted procedure: k1856 
o|contracted procedure: k1875 
o|contracted procedure: k1910 
o|contracted procedure: k1886 
o|contracted procedure: k1906 
o|contracted procedure: k1929 
o|contracted procedure: k1935 
o|contracted procedure: k1942 
o|contracted procedure: k1948 
o|contracted procedure: k1976 
o|contracted procedure: k1958 
o|contracted procedure: k1970 
o|contracted procedure: k2001 
o|contracted procedure: k1985 
o|contracted procedure: k1998 
o|contracted procedure: k2027 
o|contracted procedure: k2034 
o|contracted procedure: k2019 
o|contracted procedure: k2043 
o|contracted procedure: k2049 
o|contracted procedure: k2075 
o|contracted procedure: k2082 
o|contracted procedure: k2088 
o|contracted procedure: k2094 
o|contracted procedure: k2100 
o|contracted procedure: k2127 
o|contracted procedure: k2146 
o|contracted procedure: k2166 
o|contracted procedure: k2184 
o|contracted procedure: k2190 
o|contracted procedure: k2211 
o|contracted procedure: k2243 
o|contracted procedure: k2252 
o|contracted procedure: k2261 
o|contracted procedure: k2270 
o|contracted procedure: k2287 
o|contracted procedure: k2316 
o|contracted procedure: k2335 
o|contracted procedure: k2348 
o|contracted procedure: k2373 
o|contracted procedure: k2381 
o|contracted procedure: k2407 
o|contracted procedure: k2414 
o|contracted procedure: k2418 
o|contracted procedure: k2436 
o|contracted procedure: k2454 
o|contracted procedure: k2464 
o|contracted procedure: k2476 
o|contracted procedure: k2498 
o|contracted procedure: k2504 
o|contracted procedure: k2519 
o|contracted procedure: k2537 
o|contracted procedure: k2546 
o|contracted procedure: k2564 
o|contracted procedure: k2583 
o|contracted procedure: k2620 
o|contracted procedure: k2650 
o|contracted procedure: k2659 
o|contracted procedure: k2674 
o|contracted procedure: k2677 
o|contracted procedure: k2690 
o|contracted procedure: k2697 
o|contracted procedure: k2734 
o|contracted procedure: k2740 
o|contracted procedure: k2747 
o|contracted procedure: k2770 
o|contracted procedure: k2792 
o|contracted procedure: k2801 
o|contracted procedure: k2804 
o|contracted procedure: k2816 
o|contracted procedure: k2829 
o|contracted procedure: k2850 
o|contracted procedure: k2861 
o|contracted procedure: k2854 
o|contracted procedure: k2873 
o|contracted procedure: k2869 
o|contracted procedure: k2865 
o|contracted procedure: k2895 
o|contracted procedure: k2906 
o|contracted procedure: k2912 
o|contracted procedure: k2937 
o|contracted procedure: k2927 
o|contracted procedure: k2962 
o|contracted procedure: k2966 
o|contracted procedure: k2997 
o|contracted procedure: k3017 
o|contracted procedure: k3001 
o|contracted procedure: k3026 
o|contracted procedure: k3044 
o|contracted procedure: k3064 
o|contracted procedure: k3067 
o|contracted procedure: k3085 
o|contracted procedure: k3071 
o|contracted procedure: k3103 
o|contracted procedure: k3106 
o|contracted procedure: k3124 
o|contracted procedure: k3110 
o|contracted procedure: k3142 
o|contracted procedure: k3152 
o|contracted procedure: k3155 
o|contracted procedure: k3166 
o|contracted procedure: k3170 
o|contracted procedure: k3181 
o|contracted procedure: k3185 
o|contracted procedure: k3234 
o|contracted procedure: k3243 
o|contracted procedure: k3237 
o|contracted procedure: k3272 
o|contracted procedure: k3281 
o|contracted procedure: k3284 
o|contracted procedure: k3290 
o|contracted procedure: k3296 
o|contracted procedure: k3302 
o|contracted procedure: k3305 
o|contracted procedure: k3311 
o|contracted procedure: k3317 
o|contracted procedure: k3323 
o|contracted procedure: k3350 
o|contracted procedure: k3356 
o|contracted procedure: k3362 
o|contracted procedure: k3426 
o|contracted procedure: k3410 
o|contracted procedure: k3434 
o|contracted procedure: k3446 
o|contracted procedure: k3466 
o|contracted procedure: k3455 
o|contracted procedure: k3472 
o|contracted procedure: k3475 
o|contracted procedure: k3482 
o|contracted procedure: k3494 
o|contracted procedure: k3498 
o|contracted procedure: k3510 
o|contracted procedure: k3733 
o|contracted procedure: k3522 
o|contracted procedure: k3528 
o|contracted procedure: k3534 
o|contracted procedure: k3547 
o|contracted procedure: k3560 
o|contracted procedure: k3573 
o|contracted procedure: k3590 
o|contracted procedure: k3607 
o|contracted procedure: k3624 
o|contracted procedure: k3633 
o|contracted procedure: k3642 
o|contracted procedure: k3651 
o|contracted procedure: k3660 
o|contracted procedure: k3663 
o|contracted procedure: k3672 
o|contracted procedure: k3688 
o|contracted procedure: k3699 
o|contracted procedure: k3779 
o|contracted procedure: k3785 
o|contracted procedure: k3791 
o|contracted procedure: k3795 
o|contracted procedure: k3805 
o|contracted procedure: k3847 
o|contracted procedure: k3817 
o|contracted procedure: k3828 
o|contracted procedure: k3844 
o|contracted procedure: k3862 
o|contracted procedure: k3965 
o|contracted procedure: k3875 
o|contracted procedure: k3959 
o|contracted procedure: k3878 
o|contracted procedure: k3953 
o|contracted procedure: k3881 
o|contracted procedure: k3947 
o|contracted procedure: k3884 
o|contracted procedure: k3894 
o|contracted procedure: k3903 
o|contracted procedure: k3915 
o|contracted procedure: k3918 
o|contracted procedure: k3941 
o|simplifications: ((if . 3) (let . 72)) 
o|removed binding forms: 272 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest8283 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest8283 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest8283 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest8283 0 
(o x)|known list op on rest arg sublist: ##core#rest-car args108 0 
o|inlining procedure: k1291 
o|inlining procedure: k1291 
(o x)|known list op on rest arg sublist: ##core#rest-cdr args108 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest146147 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest146147 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest146147 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest146147 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest221224 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest221224 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest221224 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest221224 0 
o|contracted procedure: k1623 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest266267 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest266267 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest266267 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest266267 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest286287 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest286287 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? port297 0 
(o x)|known list op on rest arg sublist: ##core#rest-car port297 0 
o|contracted procedure: k1896 
(o x)|known list op on rest arg sublist: ##core#rest-null? more312 0 
(o x)|known list op on rest arg sublist: ##core#rest-car more312 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr more312 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest344345 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest344345 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest357359 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest357359 0 
o|contracted procedure: k2370 
(o x)|known list op on rest arg sublist: ##core#rest-car opt731 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest855857 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest855857 0 
o|inlining procedure: k3823 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest882883 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest882883 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest882883 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest882883 0 
o|removed binding forms: 3 
(o x)|known list op on rest arg sublist: ##core#rest-null? r1094 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r1094 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r1094 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r1094 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r1350 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r1350 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r1350 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r1350 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r1594 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r1594 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r1594 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r1594 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r1742 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r1742 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r1742 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r1742 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? _%rest316336 1 
(o x)|known list op on rest arg sublist: ##core#rest-car _%rest316336 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr _%rest316336 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r3879 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r3879 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r3879 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r3879 1 
o|removed binding forms: 4 
(o x)|known list op on rest arg sublist: ##core#rest-null? r1100 2 
(o x)|known list op on rest arg sublist: ##core#rest-car r1100 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r1100 2 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r1100 2 
o|removed binding forms: 11 
o|removed binding forms: 2 
o|direct leaf routine/allocation: read-macro-prefix411 0 
o|direct leaf routine/allocation: fetch770 0 
o|contracted procedure: "(extras.scm:281) k2154" 
o|contracted procedure: "(extras.scm:416) k2903" 
o|contracted procedure: "(extras.scm:590) k3513" 
o|contracted procedure: "(extras.scm:592) k3525" 
o|contracted procedure: "(extras.scm:610) k3679" 
o|contracted procedure: "(extras.scm:612) k3695" 
o|removed binding forms: 6 
o|direct leaf routine/allocation: skip793 0 
o|inlining procedure: k3516 
o|converted assignments to bindings: (skip793) 
o|simplifications: ((let . 1)) 
o|customizable procedures: (chicken.format#fprintf0 rec765 next771 loop776 pp414 k3275 tail1614 tail3616 tail2615 indent536 loop596 pp-down541 style555 pp-call539 pp-general542 pr537 chicken.pretty-print#generic-write pp-list540 spaces535 doloop520521 g511512 g500501 loop477 wr-expr457 loop463 read-macro?409 wr-lst458 out412 wr413 k2037 def-n317334 def-port318332 body315324 loop304 loop253 k1614 k1538 loop201 loop186 loop158 k1199 loop125 g120121 doloop9697) 
o|calls to known targets: 194 
o|unused rest argument: rest8283 f_1088 
o|unused rest argument: rest146147 f_1344 
o|unused rest argument: rest221224 f_1588 
o|unused rest argument: rest266267 f_1736 
o|unused rest argument: rest286287 f_1789 
o|unused rest argument: port297 f_1817 
o|unused rest argument: more312 f_1873 
o|unused rest argument: rest344345 f_1956 
o|unused rest argument: rest357359 f_1983 
o|identified direct recursive calls: f_2365 1 
o|identified direct recursive calls: f_3683 1 
o|identified direct recursive calls: f_3505 1 
o|unused rest argument: rest855857 f_3815 
o|unused rest argument: rest882883 f_3873 
o|fast box initializations: 43 
o|fast global references: 5 
o|fast global assignments: 2 
o|dropping unused closure argument: f_2008 
o|dropping unused closure argument: f_2011 
o|dropping unused closure argument: f_2073 
o|dropping unused closure argument: f_3432 
*/
/* end of file */
