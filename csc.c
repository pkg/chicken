/* Generated from csc.scm by the CHICKEN compiler
   http://www.call-cc.org
   Version 5.2.0 (rev 317468e4)
   linux-unix-gnu-x86-64 [ 64bit dload ptables ]
   command line: csc.scm -optimize-level 2 -include-path . -include-path ./ -inline -ignore-repository -feature chicken-bootstrap -no-warnings -specialize -consult-types-file ./types.db -no-lambda-info -no-trace -output-file csc.c
   uses: library eval expand file extras pathname posix data-structures
*/
#include "chicken.h"

#ifndef STATICBUILD
# define STATIC_CHICKEN 0
#else
# define STATIC_CHICKEN 1
#endif
#ifndef DEBUGBUILD
# define DEBUG_CHICKEN 0
#else
# define DEBUG_CHICKEN 1
#endif

static C_PTABLE_ENTRY *create_ptable(void);
C_noret_decl(C_library_toplevel)
C_externimport void C_ccall C_library_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_eval_toplevel)
C_externimport void C_ccall C_eval_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_expand_toplevel)
C_externimport void C_ccall C_expand_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_file_toplevel)
C_externimport void C_ccall C_file_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_extras_toplevel)
C_externimport void C_ccall C_extras_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_pathname_toplevel)
C_externimport void C_ccall C_pathname_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_posix_toplevel)
C_externimport void C_ccall C_posix_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_data_2dstructures_toplevel)
C_externimport void C_ccall C_data_2dstructures_toplevel(C_word c,C_word *av) C_noret;

static C_TLS C_word lf[443];
static double C_possibly_force_alignment;


C_noret_decl(f8620)
static void C_ccall f8620(C_word c,C_word *av) C_noret;
C_noret_decl(f8626)
static void C_ccall f8626(C_word c,C_word *av) C_noret;
C_noret_decl(f8630)
static void C_ccall f8630(C_word c,C_word *av) C_noret;
C_noret_decl(f8662)
static void C_ccall f8662(C_word c,C_word *av) C_noret;
C_noret_decl(f8710)
static void C_ccall f8710(C_word c,C_word *av) C_noret;
C_noret_decl(f9025)
static void C_ccall f9025(C_word c,C_word *av) C_noret;
C_noret_decl(f9029)
static void C_ccall f9029(C_word c,C_word *av) C_noret;
C_noret_decl(f_2268)
static void C_ccall f_2268(C_word c,C_word *av) C_noret;
C_noret_decl(f_2271)
static void C_ccall f_2271(C_word c,C_word *av) C_noret;
C_noret_decl(f_2274)
static void C_ccall f_2274(C_word c,C_word *av) C_noret;
C_noret_decl(f_2277)
static void C_ccall f_2277(C_word c,C_word *av) C_noret;
C_noret_decl(f_2280)
static void C_ccall f_2280(C_word c,C_word *av) C_noret;
C_noret_decl(f_2283)
static void C_ccall f_2283(C_word c,C_word *av) C_noret;
C_noret_decl(f_2286)
static void C_ccall f_2286(C_word c,C_word *av) C_noret;
C_noret_decl(f_2289)
static void C_ccall f_2289(C_word c,C_word *av) C_noret;
C_noret_decl(f_2292)
static void C_ccall f_2292(C_word c,C_word *av) C_noret;
C_noret_decl(f_2298)
static void C_ccall f_2298(C_word c,C_word *av) C_noret;
C_noret_decl(f_2304)
static void C_ccall f_2304(C_word c,C_word *av) C_noret;
C_noret_decl(f_2308)
static void C_ccall f_2308(C_word c,C_word *av) C_noret;
C_noret_decl(f_2312)
static void C_ccall f_2312(C_word c,C_word *av) C_noret;
C_noret_decl(f_2316)
static void C_ccall f_2316(C_word c,C_word *av) C_noret;
C_noret_decl(f_2320)
static void C_ccall f_2320(C_word c,C_word *av) C_noret;
C_noret_decl(f_2324)
static void C_ccall f_2324(C_word c,C_word *av) C_noret;
C_noret_decl(f_2328)
static void C_ccall f_2328(C_word c,C_word *av) C_noret;
C_noret_decl(f_2332)
static void C_ccall f_2332(C_word c,C_word *av) C_noret;
C_noret_decl(f_2336)
static void C_ccall f_2336(C_word c,C_word *av) C_noret;
C_noret_decl(f_2340)
static void C_ccall f_2340(C_word c,C_word *av) C_noret;
C_noret_decl(f_2344)
static void C_ccall f_2344(C_word c,C_word *av) C_noret;
C_noret_decl(f_2348)
static void C_ccall f_2348(C_word c,C_word *av) C_noret;
C_noret_decl(f_2352)
static void C_ccall f_2352(C_word c,C_word *av) C_noret;
C_noret_decl(f_2356)
static void C_ccall f_2356(C_word c,C_word *av) C_noret;
C_noret_decl(f_2364)
static void C_ccall f_2364(C_word c,C_word *av) C_noret;
C_noret_decl(f_2368)
static void C_ccall f_2368(C_word c,C_word *av) C_noret;
C_noret_decl(f_2372)
static void C_ccall f_2372(C_word c,C_word *av) C_noret;
C_noret_decl(f_2376)
static void C_ccall f_2376(C_word c,C_word *av) C_noret;
C_noret_decl(f_2380)
static void C_ccall f_2380(C_word c,C_word *av) C_noret;
C_noret_decl(f_2384)
static void C_ccall f_2384(C_word c,C_word *av) C_noret;
C_noret_decl(f_2388)
static void C_ccall f_2388(C_word c,C_word *av) C_noret;
C_noret_decl(f_2392)
static void C_ccall f_2392(C_word c,C_word *av) C_noret;
C_noret_decl(f_2396)
static void C_ccall f_2396(C_word c,C_word *av) C_noret;
C_noret_decl(f_2400)
static void C_ccall f_2400(C_word c,C_word *av) C_noret;
C_noret_decl(f_2404)
static void C_ccall f_2404(C_word c,C_word *av) C_noret;
C_noret_decl(f_2408)
static void C_ccall f_2408(C_word c,C_word *av) C_noret;
C_noret_decl(f_2412)
static void C_ccall f_2412(C_word c,C_word *av) C_noret;
C_noret_decl(f_2416)
static void C_ccall f_2416(C_word c,C_word *av) C_noret;
C_noret_decl(f_2420)
static void C_ccall f_2420(C_word c,C_word *av) C_noret;
C_noret_decl(f_2424)
static void C_ccall f_2424(C_word c,C_word *av) C_noret;
C_noret_decl(f_2428)
static void C_ccall f_2428(C_word c,C_word *av) C_noret;
C_noret_decl(f_2432)
static void C_ccall f_2432(C_word c,C_word *av) C_noret;
C_noret_decl(f_2436)
static void C_ccall f_2436(C_word c,C_word *av) C_noret;
C_noret_decl(f_2505)
static void C_ccall f_2505(C_word c,C_word *av) C_noret;
C_noret_decl(f_2508)
static void C_ccall f_2508(C_word c,C_word *av) C_noret;
C_noret_decl(f_2943)
static void C_fcall f_2943(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_2949)
static void C_fcall f_2949(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_2963)
static void C_ccall f_2963(C_word c,C_word *av) C_noret;
C_noret_decl(f_3005)
static void C_fcall f_3005(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3032)
static void C_ccall f_3032(C_word c,C_word *av) C_noret;
C_noret_decl(f_3080)
static void C_fcall f_3080(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3094)
static void C_ccall f_3094(C_word c,C_word *av) C_noret;
C_noret_decl(f_3107)
static void C_ccall f_3107(C_word c,C_word *av) C_noret;
C_noret_decl(f_3128)
static void C_fcall f_3128(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3136)
static C_word C_fcall f_3136(C_word *a,C_word t0,C_word t1,C_word t2);
C_noret_decl(f_3157)
static void C_ccall f_3157(C_word c,C_word *av) C_noret;
C_noret_decl(f_3172)
static void C_fcall f_3172(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3184)
static void C_ccall f_3184(C_word c,C_word *av) C_noret;
C_noret_decl(f_3188)
static C_word C_fcall f_3188(C_word *a,C_word t0,C_word t1);
C_noret_decl(f_3206)
static void C_ccall f_3206(C_word c,C_word *av) C_noret;
C_noret_decl(f_3285)
static C_word C_fcall f_3285(C_word t0);
C_noret_decl(f_3385)
static void C_fcall f_3385(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_3407)
static C_word C_fcall f_3407(C_word t0,C_word t1);
C_noret_decl(f_3418)
static void C_ccall f_3418(C_word c,C_word *av) C_noret;
C_noret_decl(f_3790)
static void C_ccall f_3790(C_word c,C_word *av) C_noret;
C_noret_decl(f_3816)
static void C_ccall f_3816(C_word c,C_word *av) C_noret;
C_noret_decl(f_3819)
static void C_fcall f_3819(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3826)
static void C_ccall f_3826(C_word c,C_word *av) C_noret;
C_noret_decl(f_3829)
static void C_ccall f_3829(C_word c,C_word *av) C_noret;
C_noret_decl(f_3832)
static void C_ccall f_3832(C_word c,C_word *av) C_noret;
C_noret_decl(f_3835)
static void C_ccall f_3835(C_word c,C_word *av) C_noret;
C_noret_decl(f_3842)
static void C_ccall f_3842(C_word c,C_word *av) C_noret;
C_noret_decl(f_3846)
static void C_ccall f_3846(C_word c,C_word *av) C_noret;
C_noret_decl(f_3850)
static void C_ccall f_3850(C_word c,C_word *av) C_noret;
C_noret_decl(f_3868)
static void C_ccall f_3868(C_word c,C_word *av) C_noret;
C_noret_decl(f_3876)
static void C_ccall f_3876(C_word c,C_word *av) C_noret;
C_noret_decl(f_3880)
static void C_ccall f_3880(C_word c,C_word *av) C_noret;
C_noret_decl(f_3882)
static void C_ccall f_3882(C_word c,C_word *av) C_noret;
C_noret_decl(f_3890)
static void C_ccall f_3890(C_word c,C_word *av) C_noret;
C_noret_decl(f_3898)
static void C_ccall f_3898(C_word c,C_word *av) C_noret;
C_noret_decl(f_3902)
static void C_ccall f_3902(C_word c,C_word *av) C_noret;
C_noret_decl(f_3906)
static void C_ccall f_3906(C_word c,C_word *av) C_noret;
C_noret_decl(f_3910)
static void C_ccall f_3910(C_word c,C_word *av) C_noret;
C_noret_decl(f_3914)
static void C_ccall f_3914(C_word c,C_word *av) C_noret;
C_noret_decl(f_3918)
static void C_ccall f_3918(C_word c,C_word *av) C_noret;
C_noret_decl(f_3931)
static void C_ccall f_3931(C_word c,C_word *av) C_noret;
C_noret_decl(f_3935)
static void C_ccall f_3935(C_word c,C_word *av) C_noret;
C_noret_decl(f_3940)
static void C_fcall f_3940(C_word t0,C_word t1) C_noret;
C_noret_decl(f_3943)
static void C_fcall f_3943(C_word t0) C_noret;
C_noret_decl(f_3951)
static void C_ccall f_3951(C_word c,C_word *av) C_noret;
C_noret_decl(f_3986)
static void C_ccall f_3986(C_word c,C_word *av) C_noret;
C_noret_decl(f_3990)
static void C_ccall f_3990(C_word c,C_word *av) C_noret;
C_noret_decl(f_3995)
static void C_ccall f_3995(C_word c,C_word *av) C_noret;
C_noret_decl(f_4000)
static void C_ccall f_4000(C_word c,C_word *av) C_noret;
C_noret_decl(f_4007)
static void C_ccall f_4007(C_word c,C_word *av) C_noret;
C_noret_decl(f_4037)
static void C_ccall f_4037(C_word c,C_word *av) C_noret;
C_noret_decl(f_4051)
static void C_ccall f_4051(C_word c,C_word *av) C_noret;
C_noret_decl(f_4055)
static void C_ccall f_4055(C_word c,C_word *av) C_noret;
C_noret_decl(f_4071)
static void C_ccall f_4071(C_word c,C_word *av) C_noret;
C_noret_decl(f_4090)
static void C_ccall f_4090(C_word c,C_word *av) C_noret;
C_noret_decl(f_4105)
static void C_fcall f_4105(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4109)
static void C_fcall f_4109(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4113)
static void C_ccall f_4113(C_word c,C_word *av) C_noret;
C_noret_decl(f_4116)
static void C_ccall f_4116(C_word c,C_word *av) C_noret;
C_noret_decl(f_4129)
static void C_ccall f_4129(C_word c,C_word *av) C_noret;
C_noret_decl(f_4134)
static void C_fcall f_4134(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4159)
static void C_ccall f_4159(C_word c,C_word *av) C_noret;
C_noret_decl(f_4179)
static void C_ccall f_4179(C_word c,C_word *av) C_noret;
C_noret_decl(f_4187)
static void C_ccall f_4187(C_word c,C_word *av) C_noret;
C_noret_decl(f_4191)
static void C_ccall f_4191(C_word c,C_word *av) C_noret;
C_noret_decl(f_4195)
static void C_ccall f_4195(C_word c,C_word *av) C_noret;
C_noret_decl(f_4211)
static void C_ccall f_4211(C_word c,C_word *av) C_noret;
C_noret_decl(f_4218)
static void C_ccall f_4218(C_word c,C_word *av) C_noret;
C_noret_decl(f_4228)
static void C_fcall f_4228(C_word t0) C_noret;
C_noret_decl(f_4240)
static void C_ccall f_4240(C_word c,C_word *av) C_noret;
C_noret_decl(f_4244)
static void C_ccall f_4244(C_word c,C_word *av) C_noret;
C_noret_decl(f_4247)
static void C_ccall f_4247(C_word c,C_word *av) C_noret;
C_noret_decl(f_4250)
static void C_ccall f_4250(C_word c,C_word *av) C_noret;
C_noret_decl(f_4253)
static void C_ccall f_4253(C_word c,C_word *av) C_noret;
C_noret_decl(f_4256)
static void C_ccall f_4256(C_word c,C_word *av) C_noret;
C_noret_decl(f_4262)
static void C_ccall f_4262(C_word c,C_word *av) C_noret;
C_noret_decl(f_4268)
static void C_ccall f_4268(C_word c,C_word *av) C_noret;
C_noret_decl(f_4280)
static void C_ccall f_4280(C_word c,C_word *av) C_noret;
C_noret_decl(f_4290)
static void C_ccall f_4290(C_word c,C_word *av) C_noret;
C_noret_decl(f_4294)
static void C_ccall f_4294(C_word c,C_word *av) C_noret;
C_noret_decl(f_4300)
static void C_ccall f_4300(C_word c,C_word *av) C_noret;
C_noret_decl(f_4312)
static void C_ccall f_4312(C_word c,C_word *av) C_noret;
C_noret_decl(f_4319)
static void C_ccall f_4319(C_word c,C_word *av) C_noret;
C_noret_decl(f_4352)
static void C_fcall f_4352(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4357)
static void C_ccall f_4357(C_word c,C_word *av) C_noret;
C_noret_decl(f_4359)
static void C_fcall f_4359(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_4385)
static void C_fcall f_4385(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4390)
static void C_ccall f_4390(C_word c,C_word *av) C_noret;
C_noret_decl(f_4394)
static void C_ccall f_4394(C_word c,C_word *av) C_noret;
C_noret_decl(f_4398)
static void C_ccall f_4398(C_word c,C_word *av) C_noret;
C_noret_decl(f_4415)
static void C_fcall f_4415(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4431)
static void C_fcall f_4431(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4442)
static void C_ccall f_4442(C_word c,C_word *av) C_noret;
C_noret_decl(f_4446)
static void C_ccall f_4446(C_word c,C_word *av) C_noret;
C_noret_decl(f_4449)
static void C_ccall f_4449(C_word c,C_word *av) C_noret;
C_noret_decl(f_4452)
static void C_ccall f_4452(C_word c,C_word *av) C_noret;
C_noret_decl(f_4455)
static void C_ccall f_4455(C_word c,C_word *av) C_noret;
C_noret_decl(f_4461)
static void C_ccall f_4461(C_word c,C_word *av) C_noret;
C_noret_decl(f_4467)
static void C_fcall f_4467(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4470)
static void C_ccall f_4470(C_word c,C_word *av) C_noret;
C_noret_decl(f_4482)
static void C_ccall f_4482(C_word c,C_word *av) C_noret;
C_noret_decl(f_4485)
static void C_ccall f_4485(C_word c,C_word *av) C_noret;
C_noret_decl(f_4488)
static void C_ccall f_4488(C_word c,C_word *av) C_noret;
C_noret_decl(f_4491)
static void C_ccall f_4491(C_word c,C_word *av) C_noret;
C_noret_decl(f_4494)
static void C_ccall f_4494(C_word c,C_word *av) C_noret;
C_noret_decl(f_4497)
static void C_ccall f_4497(C_word c,C_word *av) C_noret;
C_noret_decl(f_4504)
static void C_ccall f_4504(C_word c,C_word *av) C_noret;
C_noret_decl(f_4510)
static void C_ccall f_4510(C_word c,C_word *av) C_noret;
C_noret_decl(f_4513)
static void C_ccall f_4513(C_word c,C_word *av) C_noret;
C_noret_decl(f_4516)
static void C_ccall f_4516(C_word c,C_word *av) C_noret;
C_noret_decl(f_4519)
static void C_ccall f_4519(C_word c,C_word *av) C_noret;
C_noret_decl(f_4522)
static void C_ccall f_4522(C_word c,C_word *av) C_noret;
C_noret_decl(f_4525)
static void C_ccall f_4525(C_word c,C_word *av) C_noret;
C_noret_decl(f_4532)
static void C_ccall f_4532(C_word c,C_word *av) C_noret;
C_noret_decl(f_4536)
static void C_ccall f_4536(C_word c,C_word *av) C_noret;
C_noret_decl(f_4554)
static void C_ccall f_4554(C_word c,C_word *av) C_noret;
C_noret_decl(f_4558)
static void C_ccall f_4558(C_word c,C_word *av) C_noret;
C_noret_decl(f_4564)
static void C_ccall f_4564(C_word c,C_word *av) C_noret;
C_noret_decl(f_4571)
static void C_ccall f_4571(C_word c,C_word *av) C_noret;
C_noret_decl(f_4588)
static void C_ccall f_4588(C_word c,C_word *av) C_noret;
C_noret_decl(f_4598)
static void C_ccall f_4598(C_word c,C_word *av) C_noret;
C_noret_decl(f_4602)
static void C_ccall f_4602(C_word c,C_word *av) C_noret;
C_noret_decl(f_4611)
static void C_fcall f_4611(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4614)
static void C_fcall f_4614(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4621)
static void C_ccall f_4621(C_word c,C_word *av) C_noret;
C_noret_decl(f_4654)
static void C_ccall f_4654(C_word c,C_word *av) C_noret;
C_noret_decl(f_4657)
static void C_ccall f_4657(C_word c,C_word *av) C_noret;
C_noret_decl(f_4660)
static void C_ccall f_4660(C_word c,C_word *av) C_noret;
C_noret_decl(f_4663)
static void C_ccall f_4663(C_word c,C_word *av) C_noret;
C_noret_decl(f_4673)
static void C_ccall f_4673(C_word c,C_word *av) C_noret;
C_noret_decl(f_4680)
static void C_ccall f_4680(C_word c,C_word *av) C_noret;
C_noret_decl(f_4687)
static void C_ccall f_4687(C_word c,C_word *av) C_noret;
C_noret_decl(f_4691)
static void C_ccall f_4691(C_word c,C_word *av) C_noret;
C_noret_decl(f_4698)
static void C_ccall f_4698(C_word c,C_word *av) C_noret;
C_noret_decl(f_4701)
static void C_ccall f_4701(C_word c,C_word *av) C_noret;
C_noret_decl(f_4713)
static void C_ccall f_4713(C_word c,C_word *av) C_noret;
C_noret_decl(f_4725)
static void C_ccall f_4725(C_word c,C_word *av) C_noret;
C_noret_decl(f_4732)
static void C_ccall f_4732(C_word c,C_word *av) C_noret;
C_noret_decl(f_4741)
static void C_ccall f_4741(C_word c,C_word *av) C_noret;
C_noret_decl(f_4748)
static void C_ccall f_4748(C_word c,C_word *av) C_noret;
C_noret_decl(f_4754)
static void C_ccall f_4754(C_word c,C_word *av) C_noret;
C_noret_decl(f_4757)
static void C_ccall f_4757(C_word c,C_word *av) C_noret;
C_noret_decl(f_4760)
static void C_ccall f_4760(C_word c,C_word *av) C_noret;
C_noret_decl(f_4763)
static void C_ccall f_4763(C_word c,C_word *av) C_noret;
C_noret_decl(f_4820)
static void C_ccall f_4820(C_word c,C_word *av) C_noret;
C_noret_decl(f_4832)
static void C_ccall f_4832(C_word c,C_word *av) C_noret;
C_noret_decl(f_4844)
static void C_ccall f_4844(C_word c,C_word *av) C_noret;
C_noret_decl(f_4856)
static void C_ccall f_4856(C_word c,C_word *av) C_noret;
C_noret_decl(f_4879)
static void C_fcall f_4879(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4882)
static void C_ccall f_4882(C_word c,C_word *av) C_noret;
C_noret_decl(f_4894)
static void C_ccall f_4894(C_word c,C_word *av) C_noret;
C_noret_decl(f_4984)
static void C_ccall f_4984(C_word c,C_word *av) C_noret;
C_noret_decl(f_4987)
static void C_ccall f_4987(C_word c,C_word *av) C_noret;
C_noret_decl(f_4991)
static void C_ccall f_4991(C_word c,C_word *av) C_noret;
C_noret_decl(f_4999)
static void C_ccall f_4999(C_word c,C_word *av) C_noret;
C_noret_decl(f_5016)
static void C_ccall f_5016(C_word c,C_word *av) C_noret;
C_noret_decl(f_5036)
static void C_ccall f_5036(C_word c,C_word *av) C_noret;
C_noret_decl(f_5039)
static void C_ccall f_5039(C_word c,C_word *av) C_noret;
C_noret_decl(f_5105)
static void C_ccall f_5105(C_word c,C_word *av) C_noret;
C_noret_decl(f_5109)
static void C_ccall f_5109(C_word c,C_word *av) C_noret;
C_noret_decl(f_5125)
static void C_ccall f_5125(C_word c,C_word *av) C_noret;
C_noret_decl(f_5136)
static void C_ccall f_5136(C_word c,C_word *av) C_noret;
C_noret_decl(f_5152)
static void C_ccall f_5152(C_word c,C_word *av) C_noret;
C_noret_decl(f_5173)
static void C_ccall f_5173(C_word c,C_word *av) C_noret;
C_noret_decl(f_5183)
static void C_ccall f_5183(C_word c,C_word *av) C_noret;
C_noret_decl(f_5193)
static void C_ccall f_5193(C_word c,C_word *av) C_noret;
C_noret_decl(f_5203)
static void C_ccall f_5203(C_word c,C_word *av) C_noret;
C_noret_decl(f_5213)
static void C_ccall f_5213(C_word c,C_word *av) C_noret;
C_noret_decl(f_5223)
static void C_ccall f_5223(C_word c,C_word *av) C_noret;
C_noret_decl(f_5233)
static void C_ccall f_5233(C_word c,C_word *av) C_noret;
C_noret_decl(f_5243)
static void C_ccall f_5243(C_word c,C_word *av) C_noret;
C_noret_decl(f_5253)
static void C_ccall f_5253(C_word c,C_word *av) C_noret;
C_noret_decl(f_5263)
static void C_ccall f_5263(C_word c,C_word *av) C_noret;
C_noret_decl(f_5272)
static void C_ccall f_5272(C_word c,C_word *av) C_noret;
C_noret_decl(f_5275)
static void C_ccall f_5275(C_word c,C_word *av) C_noret;
C_noret_decl(f_5287)
static void C_ccall f_5287(C_word c,C_word *av) C_noret;
C_noret_decl(f_5314)
static void C_fcall f_5314(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5338)
static void C_ccall f_5338(C_word c,C_word *av) C_noret;
C_noret_decl(f_5355)
static void C_ccall f_5355(C_word c,C_word *av) C_noret;
C_noret_decl(f_5372)
static void C_ccall f_5372(C_word c,C_word *av) C_noret;
C_noret_decl(f_5389)
static void C_ccall f_5389(C_word c,C_word *av) C_noret;
C_noret_decl(f_5406)
static void C_ccall f_5406(C_word c,C_word *av) C_noret;
C_noret_decl(f_5410)
static void C_ccall f_5410(C_word c,C_word *av) C_noret;
C_noret_decl(f_5427)
static void C_ccall f_5427(C_word c,C_word *av) C_noret;
C_noret_decl(f_5431)
static void C_ccall f_5431(C_word c,C_word *av) C_noret;
C_noret_decl(f_5439)
static void C_ccall f_5439(C_word c,C_word *av) C_noret;
C_noret_decl(f_5453)
static void C_ccall f_5453(C_word c,C_word *av) C_noret;
C_noret_decl(f_5466)
static void C_ccall f_5466(C_word c,C_word *av) C_noret;
C_noret_decl(f_5470)
static void C_ccall f_5470(C_word c,C_word *av) C_noret;
C_noret_decl(f_5478)
static void C_ccall f_5478(C_word c,C_word *av) C_noret;
C_noret_decl(f_5491)
static void C_ccall f_5491(C_word c,C_word *av) C_noret;
C_noret_decl(f_5505)
static void C_fcall f_5505(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5509)
static void C_ccall f_5509(C_word c,C_word *av) C_noret;
C_noret_decl(f_5517)
static void C_ccall f_5517(C_word c,C_word *av) C_noret;
C_noret_decl(f_5521)
static void C_ccall f_5521(C_word c,C_word *av) C_noret;
C_noret_decl(f_5546)
static void C_ccall f_5546(C_word c,C_word *av) C_noret;
C_noret_decl(f_5549)
static void C_ccall f_5549(C_word c,C_word *av) C_noret;
C_noret_decl(f_5566)
static void C_ccall f_5566(C_word c,C_word *av) C_noret;
C_noret_decl(f_5569)
static void C_ccall f_5569(C_word c,C_word *av) C_noret;
C_noret_decl(f_5587)
static void C_ccall f_5587(C_word c,C_word *av) C_noret;
C_noret_decl(f_5594)
static void C_ccall f_5594(C_word c,C_word *av) C_noret;
C_noret_decl(f_5597)
static void C_fcall f_5597(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5600)
static void C_fcall f_5600(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5607)
static C_word C_fcall f_5607(C_word *a,C_word t0,C_word t1);
C_noret_decl(f_5637)
static void C_ccall f_5637(C_word c,C_word *av) C_noret;
C_noret_decl(f_5640)
static void C_ccall f_5640(C_word c,C_word *av) C_noret;
C_noret_decl(f_5654)
static void C_fcall f_5654(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5673)
static void C_ccall f_5673(C_word c,C_word *av) C_noret;
C_noret_decl(f_5677)
static void C_ccall f_5677(C_word c,C_word *av) C_noret;
C_noret_decl(f_5700)
static void C_ccall f_5700(C_word c,C_word *av) C_noret;
C_noret_decl(f_5704)
static void C_ccall f_5704(C_word c,C_word *av) C_noret;
C_noret_decl(f_5731)
static void C_ccall f_5731(C_word c,C_word *av) C_noret;
C_noret_decl(f_5745)
static void C_ccall f_5745(C_word c,C_word *av) C_noret;
C_noret_decl(f_5755)
static void C_fcall f_5755(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5759)
static void C_ccall f_5759(C_word c,C_word *av) C_noret;
C_noret_decl(f_5782)
static void C_ccall f_5782(C_word c,C_word *av) C_noret;
C_noret_decl(f_5799)
static void C_ccall f_5799(C_word c,C_word *av) C_noret;
C_noret_decl(f_5801)
static void C_fcall f_5801(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5826)
static void C_ccall f_5826(C_word c,C_word *av) C_noret;
C_noret_decl(f_5840)
static void C_ccall f_5840(C_word c,C_word *av) C_noret;
C_noret_decl(f_5844)
static void C_ccall f_5844(C_word c,C_word *av) C_noret;
C_noret_decl(f_5861)
static void C_ccall f_5861(C_word c,C_word *av) C_noret;
C_noret_decl(f_5873)
static void C_ccall f_5873(C_word c,C_word *av) C_noret;
C_noret_decl(f_5878)
static void C_ccall f_5878(C_word c,C_word *av) C_noret;
C_noret_decl(f_5884)
static void C_ccall f_5884(C_word c,C_word *av) C_noret;
C_noret_decl(f_5895)
static void C_ccall f_5895(C_word c,C_word *av) C_noret;
C_noret_decl(f_5909)
static void C_ccall f_5909(C_word c,C_word *av) C_noret;
C_noret_decl(f_5923)
static void C_ccall f_5923(C_word c,C_word *av) C_noret;
C_noret_decl(f_5936)
static void C_fcall f_5936(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5941)
static void C_ccall f_5941(C_word c,C_word *av) C_noret;
C_noret_decl(f_5960)
static void C_ccall f_5960(C_word c,C_word *av) C_noret;
C_noret_decl(f_5972)
static void C_fcall f_5972(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5976)
static void C_ccall f_5976(C_word c,C_word *av) C_noret;
C_noret_decl(f_5984)
static void C_ccall f_5984(C_word c,C_word *av) C_noret;
C_noret_decl(f_5993)
static void C_ccall f_5993(C_word c,C_word *av) C_noret;
C_noret_decl(f_5999)
static void C_ccall f_5999(C_word c,C_word *av) C_noret;
C_noret_decl(f_6029)
static void C_ccall f_6029(C_word c,C_word *av) C_noret;
C_noret_decl(f_6236)
static void C_ccall f_6236(C_word c,C_word *av) C_noret;
C_noret_decl(f_6239)
static void C_ccall f_6239(C_word c,C_word *av) C_noret;
C_noret_decl(f_6242)
static void C_ccall f_6242(C_word c,C_word *av) C_noret;
C_noret_decl(f_6245)
static void C_fcall f_6245(C_word t0,C_word t1) C_noret;
C_noret_decl(f_6249)
static void C_ccall f_6249(C_word c,C_word *av) C_noret;
C_noret_decl(f_6253)
static void C_ccall f_6253(C_word c,C_word *av) C_noret;
C_noret_decl(f_6272)
static void C_ccall f_6272(C_word c,C_word *av) C_noret;
C_noret_decl(f_6276)
static void C_ccall f_6276(C_word c,C_word *av) C_noret;
C_noret_decl(f_6280)
static void C_ccall f_6280(C_word c,C_word *av) C_noret;
C_noret_decl(f_6284)
static void C_ccall f_6284(C_word c,C_word *av) C_noret;
C_noret_decl(f_6288)
static void C_ccall f_6288(C_word c,C_word *av) C_noret;
C_noret_decl(f_6292)
static void C_fcall f_6292(C_word t0,C_word t1) C_noret;
C_noret_decl(f_6303)
static void C_ccall f_6303(C_word c,C_word *av) C_noret;
C_noret_decl(f_6309)
static void C_ccall f_6309(C_word c,C_word *av) C_noret;
C_noret_decl(f_6311)
static void C_fcall f_6311(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6336)
static void C_ccall f_6336(C_word c,C_word *av) C_noret;
C_noret_decl(f_6347)
static void C_fcall f_6347(C_word t0,C_word t1) C_noret;
C_noret_decl(f_6364)
static void C_ccall f_6364(C_word c,C_word *av) C_noret;
C_noret_decl(f_6378)
static void C_ccall f_6378(C_word c,C_word *av) C_noret;
C_noret_decl(f_6407)
static void C_fcall f_6407(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6432)
static void C_fcall f_6432(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6436)
static void C_ccall f_6436(C_word c,C_word *av) C_noret;
C_noret_decl(f_6439)
static void C_ccall f_6439(C_word c,C_word *av) C_noret;
C_noret_decl(f_6442)
static void C_ccall f_6442(C_word c,C_word *av) C_noret;
C_noret_decl(f_6454)
static void C_ccall f_6454(C_word c,C_word *av) C_noret;
C_noret_decl(f_6466)
static void C_ccall f_6466(C_word c,C_word *av) C_noret;
C_noret_decl(f_6470)
static void C_ccall f_6470(C_word c,C_word *av) C_noret;
C_noret_decl(f_6474)
static void C_fcall f_6474(C_word t0,C_word t1) C_noret;
C_noret_decl(f_6478)
static void C_ccall f_6478(C_word c,C_word *av) C_noret;
C_noret_decl(f_6489)
static void C_ccall f_6489(C_word c,C_word *av) C_noret;
C_noret_decl(f_6518)
static void C_ccall f_6518(C_word c,C_word *av) C_noret;
C_noret_decl(f_6521)
static void C_fcall f_6521(C_word t0,C_word t1) C_noret;
C_noret_decl(f_6522)
static void C_fcall f_6522(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6526)
static void C_ccall f_6526(C_word c,C_word *av) C_noret;
C_noret_decl(f_6529)
static void C_ccall f_6529(C_word c,C_word *av) C_noret;
C_noret_decl(f_6541)
static void C_ccall f_6541(C_word c,C_word *av) C_noret;
C_noret_decl(f_6549)
static void C_ccall f_6549(C_word c,C_word *av) C_noret;
C_noret_decl(f_6553)
static void C_ccall f_6553(C_word c,C_word *av) C_noret;
C_noret_decl(f_6559)
static void C_ccall f_6559(C_word c,C_word *av) C_noret;
C_noret_decl(f_6563)
static void C_ccall f_6563(C_word c,C_word *av) C_noret;
C_noret_decl(f_6572)
static void C_ccall f_6572(C_word c,C_word *av) C_noret;
C_noret_decl(f_6580)
static void C_fcall f_6580(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6590)
static void C_ccall f_6590(C_word c,C_word *av) C_noret;
C_noret_decl(f_6603)
static void C_fcall f_6603(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6613)
static void C_ccall f_6613(C_word c,C_word *av) C_noret;
C_noret_decl(f_6628)
static void C_ccall f_6628(C_word c,C_word *av) C_noret;
C_noret_decl(f_6630)
static void C_fcall f_6630(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6640)
static void C_ccall f_6640(C_word c,C_word *av) C_noret;
C_noret_decl(f_6654)
static void C_fcall f_6654(C_word t0,C_word t1) C_noret;
C_noret_decl(f_6657)
static void C_ccall f_6657(C_word c,C_word *av) C_noret;
C_noret_decl(f_6660)
static void C_ccall f_6660(C_word c,C_word *av) C_noret;
C_noret_decl(f_6672)
static void C_ccall f_6672(C_word c,C_word *av) C_noret;
C_noret_decl(f_6679)
static void C_ccall f_6679(C_word c,C_word *av) C_noret;
C_noret_decl(f_6681)
static void C_fcall f_6681(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6691)
static void C_ccall f_6691(C_word c,C_word *av) C_noret;
C_noret_decl(f_6704)
static void C_fcall f_6704(C_word t0) C_noret;
C_noret_decl(f_6715)
static void C_ccall f_6715(C_word c,C_word *av) C_noret;
C_noret_decl(f_6721)
static void C_ccall f_6721(C_word c,C_word *av) C_noret;
C_noret_decl(f_6723)
static void C_fcall f_6723(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6748)
static void C_ccall f_6748(C_word c,C_word *av) C_noret;
C_noret_decl(f_6762)
static void C_ccall f_6762(C_word c,C_word *av) C_noret;
C_noret_decl(f_6771)
static void C_ccall f_6771(C_word c,C_word *av) C_noret;
C_noret_decl(f_6774)
static void C_ccall f_6774(C_word c,C_word *av) C_noret;
C_noret_decl(f_6777)
static void C_ccall f_6777(C_word c,C_word *av) C_noret;
C_noret_decl(f_6780)
static void C_ccall f_6780(C_word c,C_word *av) C_noret;
C_noret_decl(f_6786)
static void C_ccall f_6786(C_word c,C_word *av) C_noret;
C_noret_decl(f_6794)
static void C_fcall f_6794(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6804)
static void C_ccall f_6804(C_word c,C_word *av) C_noret;
C_noret_decl(f_6821)
static void C_ccall f_6821(C_word c,C_word *av) C_noret;
C_noret_decl(f_6831)
static void C_ccall f_6831(C_word c,C_word *av) C_noret;
C_noret_decl(f_6835)
static void C_ccall f_6835(C_word c,C_word *av) C_noret;
C_noret_decl(f_6839)
static void C_ccall f_6839(C_word c,C_word *av) C_noret;
C_noret_decl(f_6843)
static void C_ccall f_6843(C_word c,C_word *av) C_noret;
C_noret_decl(f_6847)
static void C_ccall f_6847(C_word c,C_word *av) C_noret;
C_noret_decl(f_6850)
static void C_ccall f_6850(C_word c,C_word *av) C_noret;
C_noret_decl(f_6860)
static void C_ccall f_6860(C_word c,C_word *av) C_noret;
C_noret_decl(f_6867)
static void C_ccall f_6867(C_word c,C_word *av) C_noret;
C_noret_decl(f_6872)
static void C_ccall f_6872(C_word c,C_word *av) C_noret;
C_noret_decl(f_6876)
static void C_ccall f_6876(C_word c,C_word *av) C_noret;
C_noret_decl(f_6884)
static void C_ccall f_6884(C_word c,C_word *av) C_noret;
C_noret_decl(f_6892)
static void C_ccall f_6892(C_word c,C_word *av) C_noret;
C_noret_decl(f_6896)
static void C_ccall f_6896(C_word c,C_word *av) C_noret;
C_noret_decl(f_6900)
static void C_ccall f_6900(C_word c,C_word *av) C_noret;
C_noret_decl(f_6905)
static void C_ccall f_6905(C_word c,C_word *av) C_noret;
C_noret_decl(f_6907)
static void C_fcall f_6907(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6932)
static void C_ccall f_6932(C_word c,C_word *av) C_noret;
C_noret_decl(f_6948)
static void C_ccall f_6948(C_word c,C_word *av) C_noret;
C_noret_decl(f_6951)
static void C_ccall f_6951(C_word c,C_word *av) C_noret;
C_noret_decl(f_6958)
static void C_ccall f_6958(C_word c,C_word *av) C_noret;
C_noret_decl(f_6972)
static void C_ccall f_6972(C_word c,C_word *av) C_noret;
C_noret_decl(f_6987)
static void C_ccall f_6987(C_word c,C_word *av) C_noret;
C_noret_decl(f_6995)
static void C_fcall f_6995(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7005)
static void C_fcall f_7005(C_word t0,C_word t1) C_noret;
C_noret_decl(f_7020)
static void C_ccall f_7020(C_word c,C_word *av) C_noret;
C_noret_decl(f_7029)
static void C_fcall f_7029(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7054)
static void C_ccall f_7054(C_word c,C_word *av) C_noret;
C_noret_decl(f_7066)
static void C_fcall f_7066(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_7080)
static void C_ccall f_7080(C_word c,C_word *av) C_noret;
C_noret_decl(f_7086)
static void C_fcall f_7086(C_word t0,C_word t1) C_noret;
C_noret_decl(f_7089)
static void C_ccall f_7089(C_word c,C_word *av) C_noret;
C_noret_decl(f_7092)
static void C_ccall f_7092(C_word c,C_word *av) C_noret;
C_noret_decl(f_7099)
static void C_ccall f_7099(C_word c,C_word *av) C_noret;
C_noret_decl(f_7103)
static void C_ccall f_7103(C_word c,C_word *av) C_noret;
C_noret_decl(f_7112)
static void C_ccall f_7112(C_word c,C_word *av) C_noret;
C_noret_decl(f_7191)
static void C_fcall f_7191(C_word t0) C_noret;
C_noret_decl(f_7202)
static void C_ccall f_7202(C_word c,C_word *av) C_noret;
C_noret_decl(f_7208)
static void C_ccall f_7208(C_word c,C_word *av) C_noret;
C_noret_decl(f_7210)
static void C_fcall f_7210(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7235)
static void C_ccall f_7235(C_word c,C_word *av) C_noret;
C_noret_decl(f_7244)
static void C_fcall f_7244(C_word t0) C_noret;
C_noret_decl(f_7252)
static void C_ccall f_7252(C_word c,C_word *av) C_noret;
C_noret_decl(f_7279)
static void C_ccall f_7279(C_word c,C_word *av) C_noret;
C_noret_decl(f_7289)
static void C_ccall f_7289(C_word c,C_word *av) C_noret;
C_noret_decl(f_7293)
static void C_ccall f_7293(C_word c,C_word *av) C_noret;
C_noret_decl(f_7297)
static void C_ccall f_7297(C_word c,C_word *av) C_noret;
C_noret_decl(f_7299)
static void C_fcall f_7299(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7322)
static void C_ccall f_7322(C_word c,C_word *av) C_noret;
C_noret_decl(f_7342)
static void C_fcall f_7342(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7351)
static void C_fcall f_7351(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7368)
static void C_ccall f_7368(C_word c,C_word *av) C_noret;
C_noret_decl(f_7376)
static void C_ccall f_7376(C_word c,C_word *av) C_noret;
C_noret_decl(f_7383)
static void C_ccall f_7383(C_word c,C_word *av) C_noret;
C_noret_decl(f_7389)
static void C_ccall f_7389(C_word c,C_word *av) C_noret;
C_noret_decl(f_7394)
static void C_ccall f_7394(C_word c,C_word *av) C_noret;
C_noret_decl(f_7406)
static void C_ccall f_7406(C_word c,C_word *av) C_noret;
C_noret_decl(f_7417)
static void C_ccall f_7417(C_word c,C_word *av) C_noret;
C_noret_decl(f_7420)
static void C_ccall f_7420(C_word c,C_word *av) C_noret;
C_noret_decl(f_7423)
static void C_ccall f_7423(C_word c,C_word *av) C_noret;
C_noret_decl(f_7426)
static void C_ccall f_7426(C_word c,C_word *av) C_noret;
C_noret_decl(f_7443)
static void C_ccall f_7443(C_word c,C_word *av) C_noret;
C_noret_decl(f_7446)
static void C_ccall f_7446(C_word c,C_word *av) C_noret;
C_noret_decl(f_7449)
static void C_ccall f_7449(C_word c,C_word *av) C_noret;
C_noret_decl(f_7452)
static void C_ccall f_7452(C_word c,C_word *av) C_noret;
C_noret_decl(f_7468)
static void C_ccall f_7468(C_word c,C_word *av) C_noret;
C_noret_decl(f_7472)
static void C_fcall f_7472(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7480)
static void C_ccall f_7480(C_word c,C_word *av) C_noret;
C_noret_decl(f_7488)
static void C_fcall f_7488(C_word t0,C_word t1) C_noret;
C_noret_decl(f_7501)
static void C_ccall f_7501(C_word c,C_word *av) C_noret;
C_noret_decl(f_7505)
static void C_ccall f_7505(C_word c,C_word *av) C_noret;
C_noret_decl(f_7524)
static void C_ccall f_7524(C_word c,C_word *av) C_noret;
C_noret_decl(f_7530)
static void C_ccall f_7530(C_word c,C_word *av) C_noret;
C_noret_decl(f_7533)
static void C_ccall f_7533(C_word c,C_word *av) C_noret;
C_noret_decl(f_7536)
static void C_ccall f_7536(C_word c,C_word *av) C_noret;
C_noret_decl(f_7539)
static void C_ccall f_7539(C_word c,C_word *av) C_noret;
C_noret_decl(f_7542)
static void C_ccall f_7542(C_word c,C_word *av) C_noret;
C_noret_decl(f_7546)
static void C_ccall f_7546(C_word c,C_word *av) C_noret;
C_noret_decl(f_7550)
static void C_ccall f_7550(C_word c,C_word *av) C_noret;
C_noret_decl(f_7554)
static void C_ccall f_7554(C_word c,C_word *av) C_noret;
C_noret_decl(f_7560)
static void C_ccall f_7560(C_word c,C_word *av) C_noret;
C_noret_decl(f_7565)
static void C_ccall f_7565(C_word c,C_word *av) C_noret;
C_noret_decl(f_7573)
static void C_ccall f_7573(C_word c,C_word *av) C_noret;
C_noret_decl(f_7591)
static void C_ccall f_7591(C_word c,C_word *av) C_noret;
C_noret_decl(f_7597)
static void C_ccall f_7597(C_word c,C_word *av) C_noret;
C_noret_decl(f_7601)
static void C_ccall f_7601(C_word c,C_word *av) C_noret;
C_noret_decl(f_7605)
static void C_ccall f_7605(C_word c,C_word *av) C_noret;
C_noret_decl(f_7609)
static void C_ccall f_7609(C_word c,C_word *av) C_noret;
C_noret_decl(f_7616)
static void C_fcall f_7616(C_word t0,C_word t1) C_noret;
C_noret_decl(f_7620)
static void C_ccall f_7620(C_word c,C_word *av) C_noret;
C_noret_decl(f_7623)
static void C_ccall f_7623(C_word c,C_word *av) C_noret;
C_noret_decl(f_7639)
static void C_ccall f_7639(C_word c,C_word *av) C_noret;
C_noret_decl(f_7642)
static void C_ccall f_7642(C_word c,C_word *av) C_noret;
C_noret_decl(f_7650)
static void C_fcall f_7650(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7675)
static void C_ccall f_7675(C_word c,C_word *av) C_noret;
C_noret_decl(f_7684)
static void C_fcall f_7684(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7709)
static void C_ccall f_7709(C_word c,C_word *av) C_noret;
C_noret_decl(f_7726)
static void C_ccall f_7726(C_word c,C_word *av) C_noret;
C_noret_decl(f_7746)
static void C_ccall f_7746(C_word c,C_word *av) C_noret;
C_noret_decl(f_7750)
static void C_ccall f_7750(C_word c,C_word *av) C_noret;
C_noret_decl(f_7775)
static void C_ccall f_7775(C_word c,C_word *av) C_noret;
C_noret_decl(f_7793)
static void C_ccall f_7793(C_word c,C_word *av) C_noret;
C_noret_decl(f_7797)
static void C_ccall f_7797(C_word c,C_word *av) C_noret;
C_noret_decl(f_7804)
static void C_ccall f_7804(C_word c,C_word *av) C_noret;
C_noret_decl(f_7808)
static void C_ccall f_7808(C_word c,C_word *av) C_noret;
C_noret_decl(f_7812)
static void C_ccall f_7812(C_word c,C_word *av) C_noret;
C_noret_decl(f_7816)
static void C_ccall f_7816(C_word c,C_word *av) C_noret;
C_noret_decl(f_7827)
static void C_ccall f_7827(C_word c,C_word *av) C_noret;
C_noret_decl(f_7830)
static void C_ccall f_7830(C_word c,C_word *av) C_noret;
C_noret_decl(f_7837)
static void C_ccall f_7837(C_word c,C_word *av) C_noret;
C_noret_decl(f_7842)
static void C_ccall f_7842(C_word c,C_word *av) C_noret;
C_noret_decl(f_7847)
static void C_ccall f_7847(C_word c,C_word *av) C_noret;
C_noret_decl(f_7851)
static void C_ccall f_7851(C_word c,C_word *av) C_noret;
C_noret_decl(f_7855)
static void C_ccall f_7855(C_word c,C_word *av) C_noret;
C_noret_decl(f_7862)
static void C_ccall f_7862(C_word c,C_word *av) C_noret;
C_noret_decl(f_7864)
static void C_ccall f_7864(C_word c,C_word *av) C_noret;
C_noret_decl(C_toplevel)
C_externexport void C_ccall C_toplevel(C_word c,C_word *av) C_noret;

C_noret_decl(trf_2943)
static void C_ccall trf_2943(C_word c,C_word *av) C_noret;
static void C_ccall trf_2943(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_2943(t0,t1,t2);}

C_noret_decl(trf_2949)
static void C_ccall trf_2949(C_word c,C_word *av) C_noret;
static void C_ccall trf_2949(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_2949(t0,t1,t2,t3);}

C_noret_decl(trf_3005)
static void C_ccall trf_3005(C_word c,C_word *av) C_noret;
static void C_ccall trf_3005(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3005(t0,t1,t2);}

C_noret_decl(trf_3080)
static void C_ccall trf_3080(C_word c,C_word *av) C_noret;
static void C_ccall trf_3080(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3080(t0,t1,t2);}

C_noret_decl(trf_3128)
static void C_ccall trf_3128(C_word c,C_word *av) C_noret;
static void C_ccall trf_3128(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3128(t0,t1,t2);}

C_noret_decl(trf_3172)
static void C_ccall trf_3172(C_word c,C_word *av) C_noret;
static void C_ccall trf_3172(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3172(t0,t1,t2);}

C_noret_decl(trf_3385)
static void C_ccall trf_3385(C_word c,C_word *av) C_noret;
static void C_ccall trf_3385(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_3385(t0,t1,t2,t3);}

C_noret_decl(trf_3819)
static void C_ccall trf_3819(C_word c,C_word *av) C_noret;
static void C_ccall trf_3819(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3819(t0,t1,t2);}

C_noret_decl(trf_3940)
static void C_ccall trf_3940(C_word c,C_word *av) C_noret;
static void C_ccall trf_3940(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_3940(t0,t1);}

C_noret_decl(trf_3943)
static void C_ccall trf_3943(C_word c,C_word *av) C_noret;
static void C_ccall trf_3943(C_word c,C_word *av){
C_word t0=av[0];
f_3943(t0);}

C_noret_decl(trf_4105)
static void C_ccall trf_4105(C_word c,C_word *av) C_noret;
static void C_ccall trf_4105(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4105(t0,t1);}

C_noret_decl(trf_4109)
static void C_ccall trf_4109(C_word c,C_word *av) C_noret;
static void C_ccall trf_4109(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4109(t0,t1);}

C_noret_decl(trf_4134)
static void C_ccall trf_4134(C_word c,C_word *av) C_noret;
static void C_ccall trf_4134(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4134(t0,t1,t2);}

C_noret_decl(trf_4228)
static void C_ccall trf_4228(C_word c,C_word *av) C_noret;
static void C_ccall trf_4228(C_word c,C_word *av){
C_word t0=av[0];
f_4228(t0);}

C_noret_decl(trf_4352)
static void C_ccall trf_4352(C_word c,C_word *av) C_noret;
static void C_ccall trf_4352(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4352(t0,t1);}

C_noret_decl(trf_4359)
static void C_ccall trf_4359(C_word c,C_word *av) C_noret;
static void C_ccall trf_4359(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_4359(t0,t1,t2,t3);}

C_noret_decl(trf_4385)
static void C_ccall trf_4385(C_word c,C_word *av) C_noret;
static void C_ccall trf_4385(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4385(t0,t1);}

C_noret_decl(trf_4415)
static void C_ccall trf_4415(C_word c,C_word *av) C_noret;
static void C_ccall trf_4415(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4415(t0,t1);}

C_noret_decl(trf_4431)
static void C_ccall trf_4431(C_word c,C_word *av) C_noret;
static void C_ccall trf_4431(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4431(t0,t1,t2);}

C_noret_decl(trf_4467)
static void C_ccall trf_4467(C_word c,C_word *av) C_noret;
static void C_ccall trf_4467(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4467(t0,t1);}

C_noret_decl(trf_4611)
static void C_ccall trf_4611(C_word c,C_word *av) C_noret;
static void C_ccall trf_4611(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4611(t0,t1);}

C_noret_decl(trf_4614)
static void C_ccall trf_4614(C_word c,C_word *av) C_noret;
static void C_ccall trf_4614(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4614(t0,t1);}

C_noret_decl(trf_4879)
static void C_ccall trf_4879(C_word c,C_word *av) C_noret;
static void C_ccall trf_4879(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4879(t0,t1);}

C_noret_decl(trf_5314)
static void C_ccall trf_5314(C_word c,C_word *av) C_noret;
static void C_ccall trf_5314(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5314(t0,t1);}

C_noret_decl(trf_5505)
static void C_ccall trf_5505(C_word c,C_word *av) C_noret;
static void C_ccall trf_5505(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5505(t0,t1);}

C_noret_decl(trf_5597)
static void C_ccall trf_5597(C_word c,C_word *av) C_noret;
static void C_ccall trf_5597(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5597(t0,t1);}

C_noret_decl(trf_5600)
static void C_ccall trf_5600(C_word c,C_word *av) C_noret;
static void C_ccall trf_5600(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5600(t0,t1);}

C_noret_decl(trf_5654)
static void C_ccall trf_5654(C_word c,C_word *av) C_noret;
static void C_ccall trf_5654(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5654(t0,t1);}

C_noret_decl(trf_5755)
static void C_ccall trf_5755(C_word c,C_word *av) C_noret;
static void C_ccall trf_5755(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5755(t0,t1);}

C_noret_decl(trf_5801)
static void C_ccall trf_5801(C_word c,C_word *av) C_noret;
static void C_ccall trf_5801(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5801(t0,t1,t2);}

C_noret_decl(trf_5936)
static void C_ccall trf_5936(C_word c,C_word *av) C_noret;
static void C_ccall trf_5936(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5936(t0,t1);}

C_noret_decl(trf_5972)
static void C_ccall trf_5972(C_word c,C_word *av) C_noret;
static void C_ccall trf_5972(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5972(t0,t1);}

C_noret_decl(trf_6245)
static void C_ccall trf_6245(C_word c,C_word *av) C_noret;
static void C_ccall trf_6245(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_6245(t0,t1);}

C_noret_decl(trf_6292)
static void C_ccall trf_6292(C_word c,C_word *av) C_noret;
static void C_ccall trf_6292(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_6292(t0,t1);}

C_noret_decl(trf_6311)
static void C_ccall trf_6311(C_word c,C_word *av) C_noret;
static void C_ccall trf_6311(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6311(t0,t1,t2);}

C_noret_decl(trf_6347)
static void C_ccall trf_6347(C_word c,C_word *av) C_noret;
static void C_ccall trf_6347(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_6347(t0,t1);}

C_noret_decl(trf_6407)
static void C_ccall trf_6407(C_word c,C_word *av) C_noret;
static void C_ccall trf_6407(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6407(t0,t1,t2);}

C_noret_decl(trf_6432)
static void C_ccall trf_6432(C_word c,C_word *av) C_noret;
static void C_ccall trf_6432(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6432(t0,t1,t2);}

C_noret_decl(trf_6474)
static void C_ccall trf_6474(C_word c,C_word *av) C_noret;
static void C_ccall trf_6474(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_6474(t0,t1);}

C_noret_decl(trf_6521)
static void C_ccall trf_6521(C_word c,C_word *av) C_noret;
static void C_ccall trf_6521(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_6521(t0,t1);}

C_noret_decl(trf_6522)
static void C_ccall trf_6522(C_word c,C_word *av) C_noret;
static void C_ccall trf_6522(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6522(t0,t1,t2);}

C_noret_decl(trf_6580)
static void C_ccall trf_6580(C_word c,C_word *av) C_noret;
static void C_ccall trf_6580(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6580(t0,t1,t2);}

C_noret_decl(trf_6603)
static void C_ccall trf_6603(C_word c,C_word *av) C_noret;
static void C_ccall trf_6603(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6603(t0,t1,t2);}

C_noret_decl(trf_6630)
static void C_ccall trf_6630(C_word c,C_word *av) C_noret;
static void C_ccall trf_6630(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6630(t0,t1,t2);}

C_noret_decl(trf_6654)
static void C_ccall trf_6654(C_word c,C_word *av) C_noret;
static void C_ccall trf_6654(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_6654(t0,t1);}

C_noret_decl(trf_6681)
static void C_ccall trf_6681(C_word c,C_word *av) C_noret;
static void C_ccall trf_6681(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6681(t0,t1,t2);}

C_noret_decl(trf_6704)
static void C_ccall trf_6704(C_word c,C_word *av) C_noret;
static void C_ccall trf_6704(C_word c,C_word *av){
C_word t0=av[0];
f_6704(t0);}

C_noret_decl(trf_6723)
static void C_ccall trf_6723(C_word c,C_word *av) C_noret;
static void C_ccall trf_6723(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6723(t0,t1,t2);}

C_noret_decl(trf_6794)
static void C_ccall trf_6794(C_word c,C_word *av) C_noret;
static void C_ccall trf_6794(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6794(t0,t1,t2);}

C_noret_decl(trf_6907)
static void C_ccall trf_6907(C_word c,C_word *av) C_noret;
static void C_ccall trf_6907(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6907(t0,t1,t2);}

C_noret_decl(trf_6995)
static void C_ccall trf_6995(C_word c,C_word *av) C_noret;
static void C_ccall trf_6995(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6995(t0,t1,t2);}

C_noret_decl(trf_7005)
static void C_ccall trf_7005(C_word c,C_word *av) C_noret;
static void C_ccall trf_7005(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_7005(t0,t1);}

C_noret_decl(trf_7029)
static void C_ccall trf_7029(C_word c,C_word *av) C_noret;
static void C_ccall trf_7029(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7029(t0,t1,t2);}

C_noret_decl(trf_7066)
static void C_ccall trf_7066(C_word c,C_word *av) C_noret;
static void C_ccall trf_7066(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_7066(t0,t1,t2,t3);}

C_noret_decl(trf_7086)
static void C_ccall trf_7086(C_word c,C_word *av) C_noret;
static void C_ccall trf_7086(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_7086(t0,t1);}

C_noret_decl(trf_7191)
static void C_ccall trf_7191(C_word c,C_word *av) C_noret;
static void C_ccall trf_7191(C_word c,C_word *av){
C_word t0=av[0];
f_7191(t0);}

C_noret_decl(trf_7210)
static void C_ccall trf_7210(C_word c,C_word *av) C_noret;
static void C_ccall trf_7210(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7210(t0,t1,t2);}

C_noret_decl(trf_7244)
static void C_ccall trf_7244(C_word c,C_word *av) C_noret;
static void C_ccall trf_7244(C_word c,C_word *av){
C_word t0=av[0];
f_7244(t0);}

C_noret_decl(trf_7299)
static void C_ccall trf_7299(C_word c,C_word *av) C_noret;
static void C_ccall trf_7299(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7299(t0,t1,t2);}

C_noret_decl(trf_7342)
static void C_ccall trf_7342(C_word c,C_word *av) C_noret;
static void C_ccall trf_7342(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7342(t0,t1,t2);}

C_noret_decl(trf_7351)
static void C_ccall trf_7351(C_word c,C_word *av) C_noret;
static void C_ccall trf_7351(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7351(t0,t1,t2);}

C_noret_decl(trf_7472)
static void C_ccall trf_7472(C_word c,C_word *av) C_noret;
static void C_ccall trf_7472(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7472(t0,t1,t2);}

C_noret_decl(trf_7488)
static void C_ccall trf_7488(C_word c,C_word *av) C_noret;
static void C_ccall trf_7488(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_7488(t0,t1);}

C_noret_decl(trf_7616)
static void C_ccall trf_7616(C_word c,C_word *av) C_noret;
static void C_ccall trf_7616(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_7616(t0,t1);}

C_noret_decl(trf_7650)
static void C_ccall trf_7650(C_word c,C_word *av) C_noret;
static void C_ccall trf_7650(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7650(t0,t1,t2);}

C_noret_decl(trf_7684)
static void C_ccall trf_7684(C_word c,C_word *av) C_noret;
static void C_ccall trf_7684(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7684(t0,t1,t2);}

/* f8620 in k6946 in k7084 in loop in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in ... */
static void C_ccall f8620(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f8620,c,av);}
/* csc.scm:1008: chicken.file#file-exists? */
t2=C_fast_retrieve(lf[131]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* f8626 in k4511 in k4508 in k4502 in k4495 in k4492 in k4489 in k4486 in k4483 in k4480 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in ... */
static void C_ccall f8626(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f8626,c,av);}
/* csc.scm:608: ##sys#print */
t2=*((C_word*)lf[44]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* f8630 in k4511 in k4508 in k4502 in k4495 in k4492 in k4489 in k4486 in k4483 in k4480 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in ... */
static void C_ccall f8630(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f8630,c,av);}
/* csc.scm:608: ##sys#print */
t2=*((C_word*)lf[44]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* f8662 in k4655 in k4652 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in ... */
static void C_ccall f8662(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f8662,c,av);}
/* csc.scm:572: chicken.base#exit */
t2=C_fast_retrieve(lf[41]);{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* f8710 in k4511 in k4508 in k4502 in k4495 in k4492 in k4489 in k4486 in k4483 in k4480 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in ... */
static void C_ccall f8710(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f8710,c,av);}
/* csc.scm:92: chicken.process#qs */
t2=C_fast_retrieve(lf[51]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* f9025 in fold in k7295 in k7387 in k7381 in main#quote-option in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in ... */
static void C_ccall f9025(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f9025,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* f9029 in fold in k7295 in k7387 in k7381 in main#quote-option in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in ... */
static void C_ccall f9029(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f9029,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k2266 */
static void C_ccall f_2268(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2268,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2271,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_eval_toplevel(2,av2);}}

/* k2269 in k2266 */
static void C_ccall f_2271(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2271,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2274,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_expand_toplevel(2,av2);}}

/* k2272 in k2269 in k2266 */
static void C_ccall f_2274(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2274,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2277,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_file_toplevel(2,av2);}}

/* k2275 in k2272 in k2269 in k2266 */
static void C_ccall f_2277(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2277,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2280,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_extras_toplevel(2,av2);}}

/* k2278 in k2275 in k2272 in k2269 in k2266 */
static void C_ccall f_2280(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2280,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2283,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_pathname_toplevel(2,av2);}}

/* k2281 in k2278 in k2275 in k2272 in k2269 in k2266 */
static void C_ccall f_2283(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2283,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2286,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_posix_toplevel(2,av2);}}

/* k2284 in k2281 in k2278 in k2275 in k2272 in k2269 in k2266 */
static void C_ccall f_2286(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2286,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2289,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_data_2dstructures_toplevel(2,av2);}}

/* k2287 in k2284 in k2281 in k2278 in k2275 in k2272 in k2269 in k2266 */
static void C_ccall f_2289(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_2289,c,av);}
a=C_alloc(13);
t2=C_a_i_provide(&a,1,lf[0]);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2292,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7864,tmp=(C_word)a,a+=2,tmp);
/* csc.scm:28: ##sys#with-environment */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[442]);
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[442]+1);
av2[1]=t3;
av2[2]=t4;
tp(3,av2);}}

/* k2290 in k2287 in k2284 in k2281 in k2278 in k2275 in k2272 in k2269 in k2266 */
static void C_ccall f_2292(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2292,c,av);}
a=C_alloc(3);
t2=C_mutate(&lf[1] /* (set! main#staticbuild ...) */,C_mk_bool(STATIC_CHICKEN));
t3=C_mutate(&lf[2] /* (set! main#debugbuild ...) */,C_mk_bool(DEBUG_CHICKEN));
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2298,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* egg-environment.scm:43: chicken.platform#feature? */
t5=C_fast_retrieve(lf[428]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=lf[429];
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k2296 in k2290 in k2287 in k2284 in k2281 in k2278 in k2275 in k2272 in k2269 in k2266 */
static void C_ccall f_2298(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2298,c,av);}
a=C_alloc(6);
t2=lf[3] /* main#cross-chicken */ =t1;;
t3=lf[4] /* main#binary-version */ =C_fix((C_word)C_BINARY_VERSION);;
t4=lf[5] /* main#major-version */ =C_fix((C_word)C_MAJOR_VERSION);;
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2304,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t6=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=C_mpointer(&a,(void*)C_TARGET_CC);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* k2302 in k2296 in k2290 in k2287 in k2284 in k2281 in k2278 in k2275 in k2272 in k2269 in k2266 */
static void C_ccall f_2304(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2304,c,av);}
a=C_alloc(6);
t2=C_mutate(&lf[6] /* (set! main#default-cc ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2308,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_mpointer(&a,(void*)C_TARGET_CXX);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k2306 in k2302 in k2296 in k2290 in k2287 in k2284 in k2281 in k2278 in k2275 in k2272 in k2269 in k2266 */
static void C_ccall f_2308(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2308,c,av);}
a=C_alloc(6);
t2=C_mutate(&lf[7] /* (set! main#default-cxx ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2312,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_mpointer(&a,(void*)C_TARGET_INSTALL_PROGRAM);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k2310 in k2306 in k2302 in k2296 in k2290 in k2287 in k2284 in k2281 in k2278 in k2275 in k2272 in k2269 in k2266 */
static void C_ccall f_2312(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2312,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2316,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_mpointer(&a,(void*)C_TARGET_CFLAGS);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k2314 in k2310 in k2306 in k2302 in k2296 in k2290 in k2287 in k2284 in k2281 in k2278 in k2275 in k2272 in k2269 in k2266 */
static void C_ccall f_2316(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2316,c,av);}
a=C_alloc(6);
t2=C_mutate(&lf[8] /* (set! main#default-cflags ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2320,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_mpointer(&a,(void*)C_TARGET_LDFLAGS);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k2318 in k2314 in k2310 in k2306 in k2302 in k2296 in k2290 in k2287 in k2284 in k2281 in k2278 in k2275 in k2272 in k2269 in k2266 */
static void C_ccall f_2320(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2320,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2324,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_mpointer(&a,(void*)C_TARGET_INSTALL_PROGRAM_EXECUTABLE_OPTIONS);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k2322 in k2318 in k2314 in k2310 in k2306 in k2302 in k2296 in k2290 in k2287 in k2284 in k2281 in k2278 in k2275 in k2272 in k2269 in k2266 */
static void C_ccall f_2324(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2324,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2328,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_mpointer(&a,(void*)C_TARGET_INSTALL_PROGRAM_FILE_OPTIONS);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k2326 in k2322 in k2318 in k2314 in k2310 in k2306 in k2302 in k2296 in k2290 in k2287 in k2284 in k2281 in k2278 in k2275 in k2272 in k2269 in k2266 */
static void C_ccall f_2328(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2328,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2332,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_mpointer(&a,(void*)C_TARGET_MORE_LIBS);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k2330 in k2326 in k2322 in k2318 in k2314 in k2310 in k2306 in k2302 in k2296 in k2290 in k2287 in k2284 in k2281 in k2278 in k2275 in k2272 in k2269 in k2266 */
static void C_ccall f_2332(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2332,c,av);}
a=C_alloc(6);
t2=C_mutate(&lf[9] /* (set! main#default-libs ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2336,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_mpointer(&a,(void*)C_TARGET_LIB_HOME);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k2334 in k2330 in k2326 in k2322 in k2318 in k2314 in k2310 in k2306 in k2302 in k2296 in k2290 in k2287 in k2284 in k2281 in k2278 in k2275 in k2272 in k2269 in k2266 */
static void C_ccall f_2336(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2336,c,av);}
a=C_alloc(6);
t2=C_mutate(&lf[10] /* (set! main#default-libdir ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2340,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_mpointer(&a,(void*)C_TARGET_RUN_LIB_HOME);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k2338 in k2334 in k2330 in k2326 in k2322 in k2318 in k2314 in k2310 in k2306 in k2302 in k2296 in k2290 in k2287 in k2284 in k2281 in k2278 in k2275 in k2272 in k2269 in k2266 */
static void C_ccall f_2340(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2340,c,av);}
a=C_alloc(6);
t2=C_mutate(&lf[11] /* (set! main#default-runlibdir ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2344,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_mpointer(&a,(void*)C_TARGET_STATIC_LIB_HOME);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k2342 in k2338 in k2334 in k2330 in k2326 in k2322 in k2318 in k2314 in k2310 in k2306 in k2302 in k2296 in k2290 in k2287 in k2284 in k2281 in k2278 in k2275 in k2272 in k2269 in k2266 */
static void C_ccall f_2344(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2344,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2348,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_mpointer(&a,(void*)C_TARGET_INCLUDE_HOME);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k2346 in k2342 in k2338 in k2334 in k2330 in k2326 in k2322 in k2318 in k2314 in k2310 in k2306 in k2302 in k2296 in k2290 in k2287 in k2284 in k2281 in k2278 in k2275 in k2272 in k2269 in k2266 in ... */
static void C_ccall f_2348(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2348,c,av);}
a=C_alloc(6);
t2=C_mutate(&lf[12] /* (set! main#default-incdir ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2352,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_mpointer(&a,(void*)C_TARGET_BIN_HOME);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k2350 in k2346 in k2342 in k2338 in k2334 in k2330 in k2326 in k2322 in k2318 in k2314 in k2310 in k2306 in k2302 in k2296 in k2290 in k2287 in k2284 in k2281 in k2278 in k2275 in k2272 in k2269 in ... */
static void C_ccall f_2352(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2352,c,av);}
a=C_alloc(6);
t2=C_mutate(&lf[13] /* (set! main#default-bindir ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2356,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_mpointer(&a,(void*)C_TARGET_SHARE_HOME);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k2354 in k2350 in k2346 in k2342 in k2338 in k2334 in k2330 in k2326 in k2322 in k2318 in k2314 in k2310 in k2306 in k2302 in k2296 in k2290 in k2287 in k2284 in k2281 in k2278 in k2275 in k2272 in ... */
static void C_ccall f_2356(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2356,c,av);}
a=C_alloc(3);
t2=C_mutate(&lf[14] /* (set! main#default-sharedir ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7862,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* egg-environment.scm:63: chicken.platform#software-type */
t4=C_fast_retrieve(lf[218]);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k2362 in k7860 in k2354 in k2350 in k2346 in k2342 in k2338 in k2334 in k2330 in k2326 in k2322 in k2318 in k2314 in k2310 in k2306 in k2302 in k2296 in k2290 in k2287 in k2284 in k2281 in k2278 in ... */
static void C_ccall f_2364(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2364,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2368,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_mpointer(&a,(void*)C_INSTALL_BIN_HOME);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k2366 in k2362 in k7860 in k2354 in k2350 in k2346 in k2342 in k2338 in k2334 in k2330 in k2326 in k2322 in k2318 in k2314 in k2310 in k2306 in k2302 in k2296 in k2290 in k2287 in k2284 in k2281 in ... */
static void C_ccall f_2368(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_2368,c,av);}
a=C_alloc(9);
t2=C_mutate(&lf[13] /* (set! main#default-bindir ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2372,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7855,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t5=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=C_mpointer(&a,(void*)C_CSC_PROGRAM);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k2370 in k2366 in k2362 in k7860 in k2354 in k2350 in k2346 in k2342 in k2338 in k2334 in k2330 in k2326 in k2322 in k2318 in k2314 in k2310 in k2306 in k2302 in k2296 in k2290 in k2287 in k2284 in ... */
static void C_ccall f_2372(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_2372,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2376,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7851,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_mpointer(&a,(void*)C_CSI_PROGRAM);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k2374 in k2370 in k2366 in k2362 in k7860 in k2354 in k2350 in k2346 in k2342 in k2338 in k2334 in k2330 in k2326 in k2322 in k2318 in k2314 in k2310 in k2306 in k2302 in k2296 in k2290 in k2287 in ... */
static void C_ccall f_2376(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_2376,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2380,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7847,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_mpointer(&a,(void*)C_CHICKEN_DO_PROGRAM);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k2378 in k2374 in k2370 in k2366 in k2362 in k7860 in k2354 in k2350 in k2346 in k2342 in k2338 in k2334 in k2330 in k2326 in k2322 in k2318 in k2314 in k2310 in k2306 in k2302 in k2296 in k2290 in ... */
static void C_ccall f_2380(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2380,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2384,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_mpointer(&a,(void*)C_TARGET_LIBRARIAN);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k2382 in k2378 in k2374 in k2370 in k2366 in k2362 in k7860 in k2354 in k2350 in k2346 in k2342 in k2338 in k2334 in k2330 in k2326 in k2322 in k2318 in k2314 in k2310 in k2306 in k2302 in k2296 in ... */
static void C_ccall f_2384(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2384,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2388,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_mpointer(&a,(void*)C_TARGET_LIBRARIAN_FLAGS);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k2386 in k2382 in k2378 in k2374 in k2370 in k2366 in k2362 in k7860 in k2354 in k2350 in k2346 in k2342 in k2338 in k2334 in k2330 in k2326 in k2322 in k2318 in k2314 in k2310 in k2306 in k2302 in ... */
static void C_ccall f_2388(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2388,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2392,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_mpointer(&a,(void*)C_INSTALL_EGG_HOME);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k2390 in k2386 in k2382 in k2378 in k2374 in k2370 in k2366 in k2362 in k7860 in k2354 in k2350 in k2346 in k2342 in k2338 in k2334 in k2330 in k2326 in k2322 in k2318 in k2314 in k2310 in k2306 in ... */
static void C_ccall f_2392(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2392,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2396,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_mpointer(&a,(void*)C_INSTALL_LIB_HOME);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k2394 in k2390 in k2386 in k2382 in k2378 in k2374 in k2370 in k2366 in k2362 in k7860 in k2354 in k2350 in k2346 in k2342 in k2338 in k2334 in k2330 in k2326 in k2322 in k2318 in k2314 in k2310 in ... */
static void C_ccall f_2396(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2396,c,av);}
a=C_alloc(6);
t2=C_mutate(&lf[17] /* (set! main#host-libdir ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2400,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_mpointer(&a,(void*)C_INSTALL_BIN_HOME);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k2398 in k2394 in k2390 in k2386 in k2382 in k2378 in k2374 in k2370 in k2366 in k2362 in k7860 in k2354 in k2350 in k2346 in k2342 in k2338 in k2334 in k2330 in k2326 in k2322 in k2318 in k2314 in ... */
static void C_ccall f_2400(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2400,c,av);}
a=C_alloc(6);
t2=C_mutate(&lf[18] /* (set! main#host-bindir ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2404,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_mpointer(&a,(void*)C_INSTALL_INCLUDE_HOME);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k2402 in k2398 in k2394 in k2390 in k2386 in k2382 in k2378 in k2374 in k2370 in k2366 in k2362 in k7860 in k2354 in k2350 in k2346 in k2342 in k2338 in k2334 in k2330 in k2326 in k2322 in k2318 in ... */
static void C_ccall f_2404(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2404,c,av);}
a=C_alloc(6);
t2=C_mutate(&lf[19] /* (set! main#host-incdir ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2408,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_mpointer(&a,(void*)C_INSTALL_SHARE_HOME);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k2406 in k2402 in k2398 in k2394 in k2390 in k2386 in k2382 in k2378 in k2374 in k2370 in k2366 in k2362 in k7860 in k2354 in k2350 in k2346 in k2342 in k2338 in k2334 in k2330 in k2326 in k2322 in ... */
static void C_ccall f_2408(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2408,c,av);}
a=C_alloc(6);
t2=C_mutate(&lf[20] /* (set! main#host-sharedir ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2412,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_mpointer(&a,(void*)C_INSTALL_MORE_LIBS);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k2410 in k2406 in k2402 in k2398 in k2394 in k2390 in k2386 in k2382 in k2378 in k2374 in k2370 in k2366 in k2362 in k7860 in k2354 in k2350 in k2346 in k2342 in k2338 in k2334 in k2330 in k2326 in ... */
static void C_ccall f_2412(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2412,c,av);}
a=C_alloc(6);
t2=C_mutate(&lf[21] /* (set! main#host-libs ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2416,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_mpointer(&a,(void*)C_INSTALL_CFLAGS);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k2414 in k2410 in k2406 in k2402 in k2398 in k2394 in k2390 in k2386 in k2382 in k2378 in k2374 in k2370 in k2366 in k2362 in k7860 in k2354 in k2350 in k2346 in k2342 in k2338 in k2334 in k2330 in ... */
static void C_ccall f_2416(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2416,c,av);}
a=C_alloc(6);
t2=C_mutate(&lf[22] /* (set! main#host-cflags ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2420,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_mpointer(&a,(void*)C_INSTALL_LDFLAGS);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in k2394 in k2390 in k2386 in k2382 in k2378 in k2374 in k2370 in k2366 in k2362 in k7860 in k2354 in k2350 in k2346 in k2342 in k2338 in k2334 in ... */
static void C_ccall f_2420(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2420,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2424,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t3=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_mpointer(&a,(void*)C_INSTALL_CC);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in k2394 in k2390 in k2386 in k2382 in k2378 in k2374 in k2370 in k2366 in k2362 in k7860 in k2354 in k2350 in k2346 in k2342 in k2338 in ... */
static void C_ccall f_2424(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2424,c,av);}
a=C_alloc(6);
t2=C_mutate(&lf[23] /* (set! main#host-cc ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2428,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_mpointer(&a,(void*)C_INSTALL_CXX);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in k2394 in k2390 in k2386 in k2382 in k2378 in k2374 in k2370 in k2366 in k2362 in k7860 in k2354 in k2350 in k2346 in k2342 in ... */
static void C_ccall f_2428(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2428,c,av);}
a=C_alloc(6);
t2=C_mutate(&lf[24] /* (set! main#host-cxx ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2432,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7842,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
t5=C_retrieve2(lf[4],C_text("main#binary-version"));
/* ##sys#fixnum->string */
t6=C_fast_retrieve(lf[436]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t4;
av2[2]=C_retrieve2(lf[4],C_text("main#binary-version"));
av2[3]=C_fix(10);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in k2394 in k2390 in k2386 in k2382 in k2378 in k2374 in k2370 in k2366 in k2362 in k7860 in k2354 in k2350 in k2346 in ... */
static void C_ccall f_2432(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2432,c,av);}
a=C_alloc(6);
t2=C_mutate(&lf[25] /* (set! main#target-repo ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2436,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7837,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
t5=C_retrieve2(lf[4],C_text("main#binary-version"));
/* ##sys#fixnum->string */
t6=C_fast_retrieve(lf[436]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t4;
av2[2]=C_retrieve2(lf[4],C_text("main#binary-version"));
av2[3]=C_fix(10);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in k2394 in k2390 in k2386 in k2382 in k2378 in k2374 in k2370 in k2366 in k2362 in k7860 in k2354 in k2350 in ... */
static void C_ccall f_2436(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2436,c,av);}
a=C_alloc(3);
t2=C_mutate(&lf[26] /* (set! main#target-run-repo ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2505,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* egg-environment.scm:120: chicken.process-context#get-environment-variable */
t4=C_fast_retrieve(lf[162]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[434];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in k2394 in k2390 in k2386 in k2382 in k2378 in k2374 in k2370 in k2366 in k2362 in k7860 in k2354 in ... */
static void C_ccall f_2505(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_2505,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2508,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(t1)){
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=t1;
f_2508(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7827,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* egg-environment.scm:121: chicken.platform#system-cache-directory */
t4=C_fast_retrieve(lf[433]);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k2506 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in k2394 in k2390 in k2386 in k2382 in k2378 in k2374 in k2370 in k2366 in k2362 in k7860 in ... */
static void C_ccall f_2508(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_2508,c,av);}
a=C_alloc(8);
t2=C_mutate(&lf[27] /* (set! main#cons* ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_2943,tmp=(C_word)a,a+=2,tmp));
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3790,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_mpointer(&a,(void*)C_INSTALL_MORE_LIBS);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* main#cons* in k2506 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in k2394 in k2390 in k2386 in k2382 in k2378 in k2374 in k2370 in k2366 in k2362 in ... */
static void C_fcall f_2943(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,4)))){
C_save_and_reclaim_args((void *)trf_2943,3,t1,t2,t3);}
a=C_alloc(5);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2949,a[2]=t5,tmp=(C_word)a,a+=3,tmp));
t7=((C_word*)t5)[1];
f_2949(t7,t1,t2,t3);}

/* loop in main#cons* in k2506 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in k2394 in k2390 in k2386 in k2382 in k2378 in k2374 in k2370 in k2366 in ... */
static void C_fcall f_2949(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
loop:
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,3)))){
C_save_and_reclaim_args((void *)trf_2949,4,t0,t1,t2,t3);}
a=C_alloc(4);
if(C_truep(C_i_nullp(t3))){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2963,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* mini-srfi-1.scm:95: loop */
t6=t4;
t7=C_i_car(t3);
t8=C_u_i_cdr(t3);
t1=t6;
t2=t7;
t3=t8;
goto loop;}}

/* k2961 in loop in main#cons* in k2506 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in k2394 in k2390 in k2386 in k2382 in k2378 in k2374 in k2370 in ... */
static void C_ccall f_2963(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_2963,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* loop in loop in k7078 in loop in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in ... */
static void C_fcall f_3005(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
loop:
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_3005,3,t0,t1,t2);}
a=C_alloc(4);
if(C_truep(C_i_nullp(t2))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_i_car(t2);
if(C_truep(C_i_string_equal_p(((C_word*)t0)[2],t3))){
/* mini-srfi-1.scm:107: loop */
t7=t1;
t8=C_u_i_cdr(t2);
t1=t7;
t2=t8;
goto loop;}
else{
t4=C_u_i_car(t2);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3032,a[2]=t1,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* mini-srfi-1.scm:109: loop */
t7=t5;
t8=C_u_i_cdr(t2);
t1=t7;
t2=t8;
goto loop;}}}

/* k3030 in loop in loop in k7078 in loop in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in ... */
static void C_ccall f_3032(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_3032,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* loop in k7078 in loop in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in ... */
static void C_fcall f_3080(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(16,0,3)))){
C_save_and_reclaim_args((void *)trf_3080,3,t0,t1,t2);}
a=C_alloc(16);
if(C_truep(C_i_nullp(t2))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_i_car(t2);
t4=C_u_i_cdr(t2);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3094,a[2]=t4,a[3]=t1,a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3107,a[2]=((C_word*)t0)[2],a[3]=t5,tmp=(C_word)a,a+=4,tmp);
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3005,a[2]=t3,a[3]=t8,tmp=(C_word)a,a+=4,tmp));
t10=((C_word*)t8)[1];
f_3005(t10,t6,t4);}}

/* k3092 in loop in k7078 in loop in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in ... */
static void C_ccall f_3094(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_3094,c,av);}
a=C_alloc(3);
t2=C_i_equalp(((C_word*)t0)[2],t1);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=(C_truep(t2)?((C_word*)t0)[4]:C_a_i_cons(&a,2,((C_word*)t0)[5],t1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k3105 in loop in k7078 in loop in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in ... */
static void C_ccall f_3107(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_3107,c,av);}
/* mini-srfi-1.scm:123: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3080(t2,((C_word*)t0)[3],t1);}

/* foldr457 in foldl563 in k5842 in k5753 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in ... */
static void C_fcall f_3128(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
loop:
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,3)))){
C_save_and_reclaim_args((void *)trf_3128,3,t0,t1,t2);}
a=C_alloc(8);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3136,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=C_slot(t2,C_fix(0));
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3157,a[2]=t1,a[3]=t3,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
t7=t5;
t8=C_slot(t2,C_fix(1));
t1=t7;
t2=t8;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* g462 in foldr457 in foldl563 in k5842 in k5753 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in ... */
static C_word C_fcall f_3136(C_word *a,C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_stack_overflow_check;{}
t3=(
/* mini-srfi-1.scm:131: pred */
  f_3407(((C_word*)t0)[2],t1)
);
return((C_truep(t3)?C_a_i_cons(&a,2,t1,t2):t2));}

/* k3155 in foldr457 in foldl563 in k5842 in k5753 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in ... */
static void C_ccall f_3157(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_3157,c,av);}
a=C_alloc(3);
/* mini-srfi-1.scm:131: g462 */
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(
/* mini-srfi-1.scm:131: g462 */
  f_3136(C_a_i(&a,3),((C_word*)t0)[3],((C_word*)t0)[4],t1)
);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* foldr475 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in ... */
static void C_fcall f_3172(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
loop:
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_3172,3,t0,t1,t2);}
a=C_alloc(4);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3206,a[2]=t1,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
t6=t4;
t7=C_slot(t2,C_fix(1));
t1=t6;
t2=t7;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k3182 in k3204 in foldr475 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in ... */
static void C_ccall f_3184(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_3184,c,av);}
a=C_alloc(6);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3188,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* mini-srfi-1.scm:135: g490 */
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=(
/* mini-srfi-1.scm:135: g490 */
  f_3188(C_a_i(&a,3),t2,t1)
);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* g490 in k3182 in k3204 in foldr475 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in ... */
static C_word C_fcall f_3188(C_word *a,C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_stack_overflow_check;{}
return(C_a_i_cons(&a,2,t1,((C_word*)t0)[2]));}

/* k3204 in foldr475 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in ... */
static void C_ccall f_3206(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_3206,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3184,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* mini-srfi-1.scm:135: pred */
t3=C_retrieve2(lf[128],C_text("main#find-object-file"));{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
f_4240(3,av2);}}

/* loop in k4562 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in ... */
static C_word C_fcall f_3285(C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_stack_overflow_check;
loop:{}
t2=C_i_cdr(t1);
if(C_truep(C_i_nullp(t2))){
return(C_u_i_car(t1));}
else{
t4=C_u_i_cdr(t1);
t1=t4;
goto loop;}}

/* foldl563 in k5842 in k5753 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in ... */
static void C_fcall f_3385(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(14,0,3)))){
C_save_and_reclaim_args((void *)trf_3385,4,t0,t1,t2,t3);}
a=C_alloc(14);
if(C_truep(C_i_pairp(t2))){
t4=C_slot(t2,C_fix(1));
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3418,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
t6=C_slot(t2,C_fix(0));
t7=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3407,a[2]=t6,tmp=(C_word)a,a+=3,tmp);
t8=C_i_check_list_2(t3,lf[392]);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3128,a[2]=t7,a[3]=t10,tmp=(C_word)a,a+=4,tmp));
t12=((C_word*)t10)[1];
f_3128(t12,t5,t3);}
else{
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* a3406 in foldl563 in k5842 in k5753 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in ... */
static C_word C_fcall f_3407(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_stack_overflow_check;{}
return(C_i_not(C_i_memq(t1,((C_word*)t0)[2])));}

/* k3416 in foldl563 in k5842 in k5753 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in ... */
static void C_ccall f_3418(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3418,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_3385(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* k3788 in k2506 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in k2394 in k2390 in k2386 in k2382 in k2378 in k2374 in k2370 in k2366 in k2362 in ... */
static void C_ccall f_3790(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_3790,c,av);}
a=C_alloc(3);
t2=C_mutate(&lf[21] /* (set! main#host-libs ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7816,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csc.scm:67: chicken.platform#software-version */
t4=C_fast_retrieve(lf[247]);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in k2394 in k2390 in k2386 in k2382 in ... */
static void C_ccall f_3816(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_3816,c,av);}
a=C_alloc(5);
t2=C_mutate(&lf[36] /* (set! main#elf ...) */,C_u_i_memq(t1,lf[37]));
t3=C_mutate(&lf[38] /* (set! main#stop ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_3819,tmp=(C_word)a,a+=2,tmp));
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3846,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csc.scm:79: chicken.process-context#command-line-arguments */
t5=C_fast_retrieve(lf[430]);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* main#stop in k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in k2394 in k2390 in k2386 in ... */
static void C_fcall f_3819(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(13,0,3)))){
C_save_and_reclaim_args((void *)trf_3819,3,t1,t2,t3);}
a=C_alloc(13);
t4=*((C_word*)lf[39]+1);
t5=*((C_word*)lf[39]+1);
t6=C_i_check_port_2(*((C_word*)lf[39]+1),C_fix(2),C_SCHEME_TRUE,lf[40]);
t7=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3826,a[2]=t1,a[3]=t4,a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3842,a[2]=t7,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* ##sys#peek-c-string */
t9=*((C_word*)lf[46]+1);{
C_word av2[4];
av2[0]=t9;
av2[1]=t8;
av2[2]=C_mpointer(&a,(void*)C_CSC_PROGRAM);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t9+1)))(4,av2);}}

/* k3824 in main#stop in k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in k2394 in k2390 in ... */
static void C_ccall f_3826(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_3826,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3829,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* csc.scm:76: ##sys#print */
t3=*((C_word*)lf[44]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[45];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k3827 in k3824 in main#stop in k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in k2394 in ... */
static void C_ccall f_3829(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,5)))){
C_save_and_reclaim((void *)f_3829,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3832,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=0;
av2[1]=t2;
av2[2]=*((C_word*)lf[43]+1);
av2[3]=((C_word*)t0)[3];
av2[4]=((C_word*)t0)[4];
av2[5]=((C_word*)t0)[5];
C_apply(6,av2);}}

/* k3830 in k3827 in k3824 in main#stop in k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in ... */
static void C_ccall f_3832(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_3832,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3835,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csc.scm:76: ##sys#write-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[42]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[42]+1);
av2[1]=t2;
av2[2]=C_make_character(10);
av2[3]=((C_word*)t0)[3];
tp(4,av2);}}

/* k3833 in k3830 in k3827 in k3824 in main#stop in k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in ... */
static void C_ccall f_3835(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_3835,c,av);}
/* csc.scm:77: chicken.base#exit */
t2=C_fast_retrieve(lf[41]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_fix(64);
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k3840 in main#stop in k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in k2394 in k2390 in ... */
static void C_ccall f_3842(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_3842,c,av);}
/* csc.scm:76: ##sys#print */
t2=*((C_word*)lf[44]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k3844 in k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in k2394 in k2390 in k2386 in ... */
static void C_ccall f_3846(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_3846,c,av);}
a=C_alloc(3);
t2=C_mutate(&lf[47] /* (set! main#arguments ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3850,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csc.scm:80: chicken.platform#feature? */
t4=C_fast_retrieve(lf[428]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[429];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in k2394 in k2390 in ... */
static void C_ccall f_3850(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,3)))){
C_save_and_reclaim((void *)f_3850,c,av);}
a=C_alloc(16);
t2=lf[3] /* main#cross-chicken */ =t1;;
t3=C_i_not(C_retrieve2(lf[3],C_text("main#cross-chicken")));
t4=(C_truep(t3)?t3:C_i_member(lf[48],C_retrieve2(lf[47],C_text("main#arguments"))));
t5=C_mutate(&lf[49] /* (set! main#host-mode ...) */,t4);
t6=C_mutate(&lf[50] /* (set! main#quotewrap ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_3868,tmp=(C_word)a,a+=2,tmp));
t7=C_mutate(&lf[54] /* (set! main#quotewrap-no-slash-trans ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_3882,tmp=(C_word)a,a+=2,tmp));
t8=(C_truep(C_retrieve2(lf[49],C_text("main#host-mode")))?C_retrieve2(lf[20],C_text("main#host-sharedir")):C_retrieve2(lf[14],C_text("main#default-sharedir")));
t9=C_mutate(&lf[55] /* (set! main#home ...) */,t8);
t10=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3898,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t11=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7793,a[2]=t10,tmp=(C_word)a,a+=3,tmp);
t12=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7797,a[2]=t11,tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t13=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t13;
av2[1]=t12;
av2[2]=C_mpointer(&a,(void*)C_CHICKEN_PROGRAM);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t13+1)))(4,av2);}}

/* main#quotewrap in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in k2394 in ... */
static void C_ccall f_3868(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_3868,c,av);}
a=C_alloc(7);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3876,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3880,a[2]=t3,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* csc.scm:89: chicken.pathname#normalize-pathname */
t5=C_fast_retrieve(lf[53]);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k3874 in main#quotewrap in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in ... */
static void C_ccall f_3876(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_3876,c,av);}
/* csc.scm:89: chicken.process#qs */
t2=C_fast_retrieve(lf[51]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k3878 in main#quotewrap in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in ... */
static void C_ccall f_3880(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_3880,c,av);}
if(C_truep(C_mk_bool(C_WINDOWS_SHELL))){
/* csc.scm:85: chicken.string#string-translate */
t2=C_fast_retrieve(lf[52]);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_make_character(92);
av2[4]=C_make_character(47);
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}
else{
/* csc.scm:89: chicken.process#qs */
t2=C_fast_retrieve(lf[51]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}}

/* main#quotewrap-no-slash-trans in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in k2394 in ... */
static void C_ccall f_3882(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_3882,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3890,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:92: chicken.pathname#normalize-pathname */
t4=C_fast_retrieve(lf[53]);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k3888 in main#quotewrap-no-slash-trans in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in ... */
static void C_ccall f_3890(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_3890,c,av);}
/* csc.scm:92: chicken.process#qs */
t2=C_fast_retrieve(lf[51]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in k2394 in ... */
static void C_ccall f_3898(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_3898,c,av);}
a=C_alloc(3);
t2=C_mutate(&lf[56] /* (set! main#translator ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3902,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_retrieve2(lf[49],C_text("main#host-mode")))){
t4=C_retrieve2(lf[23],C_text("main#host-cc"));
t5=C_retrieve2(lf[23],C_text("main#host-cc"));
/* csc.scm:100: quotewrap */
t6=C_retrieve2(lf[50],C_text("main#quotewrap"));{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t3;
av2[2]=C_retrieve2(lf[23],C_text("main#host-cc"));
f_3868(3,av2);}}
else{
t4=C_retrieve2(lf[6],C_text("main#default-cc"));
t5=C_retrieve2(lf[6],C_text("main#default-cc"));
/* csc.scm:100: quotewrap */
t6=C_retrieve2(lf[50],C_text("main#quotewrap"));{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t3;
av2[2]=C_retrieve2(lf[6],C_text("main#default-cc"));
f_3868(3,av2);}}}

/* k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in ... */
static void C_ccall f_3902(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_3902,c,av);}
a=C_alloc(3);
t2=C_mutate(&lf[57] /* (set! main#compiler ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3906,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_retrieve2(lf[49],C_text("main#host-mode")))){
t4=C_retrieve2(lf[24],C_text("main#host-cxx"));
t5=C_retrieve2(lf[24],C_text("main#host-cxx"));
/* csc.scm:101: quotewrap */
t6=C_retrieve2(lf[50],C_text("main#quotewrap"));{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t3;
av2[2]=C_retrieve2(lf[24],C_text("main#host-cxx"));
f_3868(3,av2);}}
else{
t4=C_retrieve2(lf[7],C_text("main#default-cxx"));
t5=C_retrieve2(lf[7],C_text("main#default-cxx"));
/* csc.scm:101: quotewrap */
t6=C_retrieve2(lf[50],C_text("main#quotewrap"));{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t3;
av2[2]=C_retrieve2(lf[7],C_text("main#default-cxx"));
f_3868(3,av2);}}}

/* k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in ... */
static void C_ccall f_3906(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_3906,c,av);}
a=C_alloc(9);
t2=C_mutate(&lf[58] /* (set! main#c++-compiler ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3910,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7775,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_retrieve2(lf[49],C_text("main#host-mode")))){
/* ##sys#peek-c-string */
t5=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=C_mpointer(&a,(void*)C_INSTALL_RC_COMPILER);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}
else{
/* ##sys#peek-c-string */
t5=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=C_mpointer(&a,(void*)C_TARGET_RC_COMPILER);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}}

/* k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in ... */
static void C_ccall f_3910(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_3910,c,av);}
a=C_alloc(3);
t2=C_mutate(&lf[59] /* (set! main#rc-compiler ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3914,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_retrieve2(lf[49],C_text("main#host-mode")))){
t4=C_retrieve2(lf[23],C_text("main#host-cc"));
t5=C_retrieve2(lf[23],C_text("main#host-cc"));
/* csc.scm:103: quotewrap */
t6=C_retrieve2(lf[50],C_text("main#quotewrap"));{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t3;
av2[2]=C_retrieve2(lf[23],C_text("main#host-cc"));
f_3868(3,av2);}}
else{
t4=C_retrieve2(lf[6],C_text("main#default-cc"));
t5=C_retrieve2(lf[6],C_text("main#default-cc"));
/* csc.scm:103: quotewrap */
t6=C_retrieve2(lf[50],C_text("main#quotewrap"));{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t3;
av2[2]=C_retrieve2(lf[6],C_text("main#default-cc"));
f_3868(3,av2);}}}

/* k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in ... */
static void C_ccall f_3914(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_3914,c,av);}
a=C_alloc(3);
t2=C_mutate(&lf[60] /* (set! main#linker ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3918,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_retrieve2(lf[49],C_text("main#host-mode")))){
t4=C_retrieve2(lf[24],C_text("main#host-cxx"));
t5=C_retrieve2(lf[24],C_text("main#host-cxx"));
/* csc.scm:104: quotewrap */
t6=C_retrieve2(lf[50],C_text("main#quotewrap"));{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t3;
av2[2]=C_retrieve2(lf[24],C_text("main#host-cxx"));
f_3868(3,av2);}}
else{
t4=C_retrieve2(lf[7],C_text("main#default-cxx"));
t5=C_retrieve2(lf[7],C_text("main#default-cxx"));
/* csc.scm:104: quotewrap */
t6=C_retrieve2(lf[50],C_text("main#quotewrap"));{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t3;
av2[2]=C_retrieve2(lf[7],C_text("main#default-cxx"));
f_3868(3,av2);}}}

/* k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in ... */
static void C_ccall f_3918(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_3918,c,av);}
a=C_alloc(3);
t2=C_mutate(&lf[61] /* (set! main#c++-linker ...) */,t1);
t3=(C_truep(C_retrieve2(lf[29],C_text("main#mingw")))?lf[62]:lf[63]);
t4=C_mutate(&lf[64] /* (set! main#object-extension ...) */,t3);
t5=C_mutate(&lf[65] /* (set! main#library-extension ...) */,lf[66]);
t6=C_mutate(&lf[67] /* (set! main#executable-extension ...) */,lf[68]);
t7=C_mutate(&lf[69] /* (set! main#shared-library-extension ...) */,C_fast_retrieve(lf[70]));
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3931,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csc.scm:111: ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[186]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[186]+1);
av2[1]=t8;
av2[2]=lf[427];
av2[3]=C_retrieve2(lf[64],C_text("main#object-extension"));
tp(4,av2);}}

/* k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in ... */
static void C_ccall f_3931(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_3931,c,av);}
a=C_alloc(3);
t2=C_mutate(&lf[71] /* (set! main#static-object-extension ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3935,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csc.scm:112: ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[186]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[186]+1);
av2[1]=t3;
av2[2]=lf[426];
av2[3]=C_retrieve2(lf[65],C_text("main#library-extension"));
tp(4,av2);}}

/* k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in k2434 in k2430 in k2426 in k2422 in ... */
static void C_ccall f_3935(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_3935,c,av);}
a=C_alloc(3);
t2=C_mutate(&lf[72] /* (set! main#static-library-extension ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3940,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=C_retrieve2(lf[29],C_text("main#mingw"));
if(C_truep(C_retrieve2(lf[29],C_text("main#mingw")))){
t5=C_retrieve2(lf[29],C_text("main#mingw"));
t6=t3;
f_3940(t6,(C_truep(C_retrieve2(lf[29],C_text("main#mingw")))?lf[424]:lf[425]));}
else{
t5=C_retrieve2(lf[33],C_text("main#cygwin"));
t6=t3;
f_3940(t6,(C_truep(C_retrieve2(lf[33],C_text("main#cygwin")))?lf[424]:lf[425]));}}

/* k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in k2434 in k2430 in k2426 in ... */
static void C_fcall f_3940(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_3940,2,t0,t1);}
a=C_alloc(5);
t2=C_mutate(&lf[73] /* (set! main#pic-options ...) */,t1);
t3=lf[74] /* main#generate-manifest */ =C_SCHEME_FALSE;;
t4=C_mutate(&lf[75] /* (set! main#libchicken ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_3943,tmp=(C_word)a,a+=2,tmp));
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3990,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_retrieve2(lf[49],C_text("main#host-mode")))){
t6=C_retrieve2(lf[22],C_text("main#host-cflags"));
t7=C_retrieve2(lf[22],C_text("main#host-cflags"));
/* csc.scm:132: chicken.string#string-split */
t8=C_fast_retrieve(lf[240]);{
C_word av2[3];
av2[0]=t8;
av2[1]=t5;
av2[2]=C_retrieve2(lf[22],C_text("main#host-cflags"));
((C_proc)(void*)(*((C_word*)t8+1)))(3,av2);}}
else{
t6=C_retrieve2(lf[8],C_text("main#default-cflags"));
t7=C_retrieve2(lf[8],C_text("main#default-cflags"));
/* csc.scm:132: chicken.string#string-split */
t8=C_fast_retrieve(lf[240]);{
C_word av2[3];
av2[0]=t8;
av2[1]=t5;
av2[2]=C_retrieve2(lf[8],C_text("main#default-cflags"));
((C_proc)(void*)(*((C_word*)t8+1)))(3,av2);}}}

/* main#libchicken in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in k2434 in k2430 in ... */
static void C_fcall f_3943(C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_3943,1,t1);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3951,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_i_not(C_retrieve2(lf[49],C_text("main#host-mode"))))){
/* ##sys#peek-c-string */
t3=*((C_word*)lf[46]+1);{
C_word av2[4];
av2[0]=t3;
av2[1]=t2;
av2[2]=C_mpointer(&a,(void*)C_TARGET_LIB_NAME);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}
else{
/* ##sys#peek-c-string */
t3=*((C_word*)lf[46]+1);{
C_word av2[4];
av2[0]=t3;
av2[1]=t2;
av2[2]=C_mpointer(&a,(void*)C_INSTALL_LIB_NAME);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}}

/* k3949 in main#libchicken in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in k2434 in ... */
static void C_ccall f_3951(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3951,c,av);}
/* csc.scm:118: scheme#string-append */
t2=*((C_word*)lf[76]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[77];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k3984 in main#linker-libraries in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in ... */
static void C_ccall f_3986(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_3986,c,av);}
/* csc.scm:129: chicken.pathname#make-pathname */
t2=C_fast_retrieve(lf[132]);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_retrieve2(lf[106],C_text("main#library-dir"));
av2[3]=t1;
av2[4]=C_retrieve2(lf[65],C_text("main#library-extension"));
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in k2434 in k2430 in ... */
static void C_ccall f_3990(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_3990,c,av);}
a=C_alloc(9);
t2=C_mutate(&lf[78] /* (set! main#default-compilation-optimization-options ...) */,t1);
t3=C_mutate(&lf[79] /* (set! main#best-compilation-optimization-options ...) */,C_retrieve2(lf[78],C_text("main#default-compilation-optimization-options")));
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3995,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7750,a[2]=t4,tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_retrieve2(lf[49],C_text("main#host-mode")))){
/* ##sys#peek-c-string */
t6=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=C_mpointer(&a,(void*)C_INSTALL_LDFLAGS);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}
else{
/* ##sys#peek-c-string */
t6=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=C_mpointer(&a,(void*)C_TARGET_LDFLAGS);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}}

/* k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in k2434 in ... */
static void C_ccall f_3995(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_3995,c,av);}
a=C_alloc(9);
t2=C_mutate(&lf[80] /* (set! main#default-linking-optimization-options ...) */,t1);
t3=C_mutate(&lf[81] /* (set! main#best-linking-optimization-options ...) */,C_retrieve2(lf[80],C_text("main#default-linking-optimization-options")));
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4000,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_retrieve2(lf[49],C_text("main#host-mode")))){
t5=t4;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_END_OF_LIST;
f_4000(2,av2);}}
else{
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7746,a[2]=t4,tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t6=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=C_mpointer(&a,(void*)C_TARGET_FEATURES);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}}

/* k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in ... */
static void C_ccall f_4000(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4000,c,av);}
a=C_alloc(3);
t2=C_mutate(&lf[82] /* (set! main#extra-features ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4007,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* ##sys#string->list */
t4=C_fast_retrieve(lf[149]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[423];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in ... */
static void C_ccall f_4007(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word t24;
C_word t25;
C_word t26;
C_word t27;
C_word t28;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_4007,c,av);}
a=C_alloc(6);
t2=C_mutate(&lf[83] /* (set! main#short-options ...) */,t1);
t3=lf[84] /* main#scheme-files */ =C_SCHEME_END_OF_LIST;;
t4=lf[85] /* main#c-files */ =C_SCHEME_END_OF_LIST;;
t5=lf[86] /* main#rc-files */ =C_SCHEME_END_OF_LIST;;
t6=lf[87] /* main#generated-c-files */ =C_SCHEME_END_OF_LIST;;
t7=lf[88] /* main#generated-rc-files */ =C_SCHEME_END_OF_LIST;;
t8=lf[89] /* main#object-files */ =C_SCHEME_END_OF_LIST;;
t9=lf[90] /* main#generated-object-files */ =C_SCHEME_END_OF_LIST;;
t10=lf[91] /* main#transient-link-files */ =C_SCHEME_END_OF_LIST;;
t11=lf[92] /* main#linked-extensions */ =C_SCHEME_END_OF_LIST;;
t12=lf[93] /* main#cpp-mode */ =C_SCHEME_FALSE;;
t13=lf[94] /* main#objc-mode */ =C_SCHEME_FALSE;;
t14=lf[95] /* main#embedded */ =C_SCHEME_FALSE;;
t15=lf[96] /* main#inquiry-only */ =C_SCHEME_FALSE;;
t16=lf[97] /* main#show-cflags */ =C_SCHEME_FALSE;;
t17=lf[98] /* main#show-ldflags */ =C_SCHEME_FALSE;;
t18=lf[99] /* main#show-libs */ =C_SCHEME_FALSE;;
t19=lf[100] /* main#dry-run */ =C_SCHEME_FALSE;;
t20=lf[101] /* main#gui */ =C_SCHEME_FALSE;;
t21=lf[102] /* main#deployed */ =C_SCHEME_FALSE;;
t22=lf[103] /* main#rpath */ =C_SCHEME_FALSE;;
t23=lf[104] /* main#ignore-repository */ =C_SCHEME_FALSE;;
t24=lf[105] /* main#show-debugging-help */ =C_SCHEME_FALSE;;
t25=(C_truep(C_retrieve2(lf[49],C_text("main#host-mode")))?C_retrieve2(lf[17],C_text("main#host-libdir")):C_retrieve2(lf[10],C_text("main#default-libdir")));
t26=C_mutate(&lf[106] /* (set! main#library-dir ...) */,t25);
t27=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4037,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_retrieve2(lf[49],C_text("main#host-mode")))){
/* ##sys#peek-c-string */
t28=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t28;
av2[1]=t27;
av2[2]=C_mpointer(&a,(void*)C_INSTALL_MORE_STATIC_LIBS);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t28+1)))(4,av2);}}
else{
/* ##sys#peek-c-string */
t28=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t28;
av2[1]=t27;
av2[2]=C_mpointer(&a,(void*)C_TARGET_MORE_STATIC_LIBS);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t28+1)))(4,av2);}}}

/* k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in ... */
static void C_ccall f_4037(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_4037,c,av);}
a=C_alloc(9);
t2=C_mutate(&lf[107] /* (set! main#extra-libraries ...) */,t1);
t3=(C_truep(C_retrieve2(lf[49],C_text("main#host-mode")))?C_retrieve2(lf[21],C_text("main#host-libs")):C_retrieve2(lf[9],C_text("main#default-libs")));
t4=C_mutate(&lf[108] /* (set! main#extra-shared-libraries ...) */,t3);
t5=lf[109] /* main#translate-options */ =C_SCHEME_END_OF_LIST;;
t6=(C_truep(C_retrieve2(lf[49],C_text("main#host-mode")))?C_retrieve2(lf[19],C_text("main#host-incdir")):C_retrieve2(lf[12],C_text("main#default-incdir")));
t7=C_i_member(t6,lf[110]);
t8=C_i_not(t7);
t9=(C_truep(t8)?t6:C_SCHEME_FALSE);
t10=C_mutate(&lf[111] /* (set! main#include-dir ...) */,t9);
t11=lf[112] /* main#compile-options */ =C_SCHEME_END_OF_LIST;;
t12=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4090,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t13=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7616,a[2]=t12,tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_retrieve2(lf[111],C_text("main#include-dir")))){
t14=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7726,a[2]=t13,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:247: chicken.string#conc */
t15=C_fast_retrieve(lf[248]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t15;
av2[1]=t14;
av2[2]=lf[422];
av2[3]=C_retrieve2(lf[111],C_text("main#include-dir"));
((C_proc)(void*)(*((C_word*)t15+1)))(4,av2);}}
else{
t14=t13;
f_7616(t14,C_SCHEME_END_OF_LIST);}}

/* k4049 in main#linker-libraries in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in ... */
static void C_ccall f_4051(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_4051,c,av);}
a=C_alloc(6);
t2=C_a_i_list1(&a,1,t1);
t3=(C_truep(C_retrieve2(lf[125],C_text("main#static")))?C_a_i_list1(&a,1,C_retrieve2(lf[107],C_text("main#extra-libraries"))):C_a_i_list1(&a,1,C_retrieve2(lf[108],C_text("main#extra-shared-libraries"))));
/* csc.scm:1041: scheme#append */
t4=*((C_word*)lf[137]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[2];
av2[2]=t2;
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k4053 in main#linker-libraries in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in ... */
static void C_ccall f_4055(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4055,c,av);}
/* csc.scm:231: scheme#string-append */
t2=*((C_word*)lf[76]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[140];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k4069 in main#linker-libraries in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in ... */
static void C_ccall f_4071(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_4071,c,av);}
a=C_alloc(6);
t2=C_a_i_list1(&a,1,t1);
t3=(C_truep(C_retrieve2(lf[125],C_text("main#static")))?C_a_i_list1(&a,1,C_retrieve2(lf[107],C_text("main#extra-libraries"))):C_a_i_list1(&a,1,C_retrieve2(lf[108],C_text("main#extra-shared-libraries"))));
/* csc.scm:1041: scheme#append */
t4=*((C_word*)lf[137]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[2];
av2[2]=t2;
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in k7814 in ... */
static void C_ccall f_4090(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word t24;
C_word t25;
C_word t26;
C_word t27;
C_word t28;
C_word t29;
C_word t30;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(30,c,4)))){
C_save_and_reclaim((void *)f_4090,c,av);}
a=C_alloc(30);
t2=C_mutate(&lf[113] /* (set! main#builtin-compile-options ...) */,t1);
t3=lf[114] /* main#translation-optimization-options */ =C_SCHEME_END_OF_LIST;;
t4=C_mutate(&lf[115] /* (set! main#compilation-optimization-options ...) */,C_retrieve2(lf[78],C_text("main#default-compilation-optimization-options")));
t5=C_mutate(&lf[116] /* (set! main#linking-optimization-options ...) */,C_retrieve2(lf[80],C_text("main#default-linking-optimization-options")));
t6=lf[117] /* main#link-options */ =C_SCHEME_END_OF_LIST;;
t7=lf[118] /* main#target-filename */ =C_SCHEME_FALSE;;
t8=lf[119] /* main#verbose */ =C_SCHEME_FALSE;;
t9=lf[120] /* main#keep-files */ =C_SCHEME_FALSE;;
t10=lf[121] /* main#translate-only */ =C_SCHEME_FALSE;;
t11=lf[122] /* main#compile-only */ =C_SCHEME_FALSE;;
t12=lf[123] /* main#to-stdout */ =C_SCHEME_FALSE;;
t13=lf[124] /* main#shared */ =C_SCHEME_FALSE;;
t14=lf[125] /* main#static */ =C_SCHEME_FALSE;;
t15=C_mutate(&lf[126] /* (set! main#repo-path ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_4228,tmp=(C_word)a,a+=2,tmp));
t16=C_mutate(&lf[128] /* (set! main#find-object-file ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_4240,tmp=(C_word)a,a+=2,tmp));
t17=C_mutate(&lf[133] /* (set! main#compiler-options ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_6704,tmp=(C_word)a,a+=2,tmp));
t18=C_mutate(&lf[138] /* (set! main#linker-options ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7191,tmp=(C_word)a,a+=2,tmp));
t19=C_mutate(&lf[139] /* (set! main#linker-libraries ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7244,tmp=(C_word)a,a+=2,tmp));
t20=C_mutate(&lf[141] /* (set! main#constant1665 ...) */,lf[142]);
t21=C_mutate(&lf[143] /* (set! main#string-any ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7342,tmp=(C_word)a,a+=2,tmp));
t22=C_mutate(&lf[134] /* (set! main#quote-option ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7376,tmp=(C_word)a,a+=2,tmp));
t23=lf[150] /* main#last-exit-code */ =C_SCHEME_FALSE;;
t24=C_mutate(&lf[151] /* (set! main#command ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7488,tmp=(C_word)a,a+=2,tmp));
t25=C_mutate(&lf[164] /* (set! main#$delete-file ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7501,tmp=(C_word)a,a+=2,tmp));
t26=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7591,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t27=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7601,a[2]=t26,tmp=(C_word)a,a+=3,tmp);
t28=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7605,a[2]=t27,tmp=(C_word)a,a+=3,tmp);
t29=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7609,a[2]=t28,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:1148: chicken.process-context#get-environment-variable */
t30=C_fast_retrieve(lf[162]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t30;
av2[1]=t29;
av2[2]=lf[418];
((C_proc)(void*)(*((C_word*)t30+1)))(3,av2);}}

/* k4103 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_fcall f_4105(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,2)))){
C_save_and_reclaim_args((void *)trf_4105,2,t0,t1);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4109,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_retrieve2(lf[102],C_text("main#deployed")))){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4179,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:275: chicken.platform#software-version */
t4=C_fast_retrieve(lf[247]);{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=t2;
f_4109(t3,C_SCHEME_END_OF_LIST);}}

/* k4107 in k4103 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_fcall f_4109(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_4109,2,t0,t1);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4113,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* csc.scm:278: chicken.process-context#get-environment-variable */
t3=C_fast_retrieve(lf[162]);{
C_word av2[3];
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[242];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4111 in k4107 in k4103 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in ... */
static void C_ccall f_4113(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,4)))){
C_save_and_reclaim((void *)f_4113,c,av);}
a=C_alloc(15);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4116,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
if(C_truep(t1)){
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4129,a[2]=t5,a[3]=t6,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* csc.scm:280: chicken.string#string-split */
t8=C_fast_retrieve(lf[240]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t8;
av2[1]=t7;
av2[2]=t1;
av2[3]=lf[241];
((C_proc)(void*)(*((C_word*)t8+1)))(4,av2);}}
else{
/* csc.scm:261: scheme#append */
t3=*((C_word*)lf[137]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
av2[4]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}}

/* k4114 in k4111 in k4107 in k4103 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in ... */
static void C_ccall f_4116(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_4116,c,av);}
/* csc.scm:261: scheme#append */
t2=*((C_word*)lf[137]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k4127 in k4111 in k4107 in k4103 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in ... */
static void C_ccall f_4129(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_4129,c,av);}
a=C_alloc(7);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4134,a[2]=((C_word*)t0)[2],a[3]=t3,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp));
t5=((C_word*)t3)[1];
f_4134(t5,((C_word*)t0)[4],t1);}

/* map-loop941 in k4127 in k4111 in k4107 in k4103 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in ... */
static void C_fcall f_4134(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_4134,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4159,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* csc.scm:280: g964 */
t4=*((C_word*)lf[76]+1);{
C_word av2[4];
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[239];
av2[3]=C_slot(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k4157 in map-loop941 in k4127 in k4111 in k4107 in k4103 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in ... */
static void C_ccall f_4159(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4159,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_4134(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k4177 in k4103 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_ccall f_4179(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_4179,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];
f_4109(t2,(C_truep((C_truep(C_eqp(t1,lf[243]))?C_SCHEME_TRUE:(C_truep(C_eqp(t1,lf[244]))?C_SCHEME_TRUE:(C_truep(C_eqp(t1,lf[245]))?C_SCHEME_TRUE:C_SCHEME_FALSE))))?C_a_i_list1(&a,1,lf[246]):C_SCHEME_END_OF_LIST));}

/* k4185 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_4187(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_4187,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4191,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4195,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_retrieve2(lf[102],C_text("main#deployed")))){
/* csc.scm:265: chicken.string#conc */
t4=C_fast_retrieve(lf[248]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=lf[249];
av2[3]=lf[250];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
if(C_truep(C_retrieve2(lf[49],C_text("main#host-mode")))){
t4=C_retrieve2(lf[17],C_text("main#host-libdir"));
t5=C_retrieve2(lf[17],C_text("main#host-libdir"));
/* csc.scm:265: chicken.string#conc */
t6=C_fast_retrieve(lf[248]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t2;
av2[2]=lf[249];
av2[3]=C_retrieve2(lf[17],C_text("main#host-libdir"));
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}
else{
/* ##sys#peek-c-string */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_mpointer(&a,(void*)C_TARGET_RUN_LIB_HOME);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}}}

/* k4189 in k4185 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_ccall f_4191(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_4191,c,av);}
a=C_alloc(6);
t2=((C_word*)t0)[2];
f_4105(t2,C_a_i_list2(&a,2,((C_word*)t0)[3],t1));}

/* k4193 in k4185 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_ccall f_4195(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4195,c,av);}
/* csc.scm:265: chicken.string#conc */
t2=C_fast_retrieve(lf[248]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[249];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k4209 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_4211(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_4211,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];
f_4105(t2,C_a_i_list1(&a,1,t1));}

/* k4216 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_4218(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_4218,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];
f_4105(t2,C_a_i_list1(&a,1,t1));}

/* main#repo-path in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in ... */
static void C_fcall f_4228(C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,1)))){
C_save_and_reclaim_args((void *)trf_4228,1,t1);}
if(C_truep(C_retrieve2(lf[49],C_text("main#host-mode")))){
/* csc.scm:297: chicken.platform#repository-path */
t2=C_fast_retrieve(lf[127]);{
C_word av2[2];
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_SCHEME_END_OF_LIST;
if(C_truep(C_i_nullp(t2))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_retrieve2(lf[25],C_text("main#target-repo"));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_i_car(t2);
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=(C_truep(t3)?C_retrieve2(lf[26],C_text("main#target-run-repo")):C_retrieve2(lf[25],C_text("main#target-repo")));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}}

/* main#find-object-file in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in ... */
static void C_ccall f_4240(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_4240,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4244,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* csc.scm:301: chicken.pathname#make-pathname */
t4=C_fast_retrieve(lf[132]);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_SCHEME_FALSE;
av2[3]=t2;
av2[4]=C_retrieve2(lf[64],C_text("main#object-extension"));
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k4242 in main#find-object-file in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in ... */
static void C_ccall f_4244(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_4244,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4247,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* csc.scm:302: chicken.pathname#make-pathname */
t3=C_fast_retrieve(lf[132]);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_SCHEME_FALSE;
av2[3]=((C_word*)t0)[3];
av2[4]=C_retrieve2(lf[65],C_text("main#library-extension"));
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4245 in k4242 in main#find-object-file in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in ... */
static void C_ccall f_4247(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_4247,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4250,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* csc.scm:304: chicken.pathname#make-pathname */
t3=C_fast_retrieve(lf[132]);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_SCHEME_FALSE;
av2[3]=((C_word*)t0)[4];
av2[4]=C_retrieve2(lf[72],C_text("main#static-library-extension"));
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4248 in k4245 in k4242 in main#find-object-file in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_4250(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_4250,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4253,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
/* csc.scm:305: chicken.pathname#make-pathname */
t3=C_fast_retrieve(lf[132]);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_SCHEME_FALSE;
av2[3]=((C_word*)t0)[5];
av2[4]=C_retrieve2(lf[71],C_text("main#static-object-extension"));
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4251 in k4248 in k4245 in k4242 in main#find-object-file in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_ccall f_4253(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_4253,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4256,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* csc.scm:306: chicken.file#file-exists? */
t3=C_fast_retrieve(lf[131]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4254 in k4251 in k4248 in k4245 in k4242 in main#find-object-file in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in ... */
static void C_ccall f_4256(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_4256,c,av);}
a=C_alloc(7);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4262,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* csc.scm:307: chicken.file#file-exists? */
t3=C_fast_retrieve(lf[131]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}

/* k4260 in k4254 in k4251 in k4248 in k4245 in k4242 in main#find-object-file in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in ... */
static void C_ccall f_4262(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_4262,c,av);}
a=C_alloc(9);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4268,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=C_eqp(C_fast_retrieve(lf[130]),C_SCHEME_TRUE);
if(C_truep(t3)){
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4300,a[2]=t2,a[3]=((C_word*)t0)[5],tmp=(C_word)a,a+=4,tmp);
/* csc.scm:309: chicken.file#file-exists? */
t5=C_fast_retrieve(lf[131]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}
else{
t4=t2;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
f_4268(2,av2);}}}}

/* k4266 in k4260 in k4254 in k4251 in k4248 in k4245 in k4242 in main#find-object-file in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in ... */
static void C_ccall f_4268(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_4268,c,av);}
a=C_alloc(8);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
if(C_truep(C_i_not(C_retrieve2(lf[104],C_text("main#ignore-repository"))))){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4280,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4294,a[2]=t2,a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
/* csc.scm:312: repo-path */
f_4228(t3);}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}}

/* k4278 in k4266 in k4260 in k4254 in k4251 in k4248 in k4245 in k4242 in main#find-object-file in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in ... */
static void C_ccall f_4280(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_4280,c,av);}
a=C_alloc(4);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4290,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* csc.scm:313: repo-path */
f_4228(t2);}}

/* k4288 in k4278 in k4266 in k4260 in k4254 in k4251 in k4248 in k4245 in k4242 in main#find-object-file in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in ... */
static void C_ccall f_4290(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4290,c,av);}
/* csc.scm:313: chicken.load#find-file */
t2=C_fast_retrieve(lf[129]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k4292 in k4266 in k4260 in k4254 in k4251 in k4248 in k4245 in k4242 in main#find-object-file in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in ... */
static void C_ccall f_4294(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4294,c,av);}
/* csc.scm:312: chicken.load#find-file */
t2=C_fast_retrieve(lf[129]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k4298 in k4260 in k4254 in k4251 in k4248 in k4245 in k4242 in main#find-object-file in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in ... */
static void C_ccall f_4300(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4300,c,av);}
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
f_4268(2,av2);}}
else{
/* csc.scm:310: chicken.file#file-exists? */
t2=C_fast_retrieve(lf[131]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}}

/* k4310 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_4312(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,2)))){
C_save_and_reclaim((void *)f_4312,c,av);}
a=C_alloc(24);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4319,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=C_a_i_cons(&a,2,lf[257],C_SCHEME_END_OF_LIST);
t4=C_a_i_cons(&a,2,t1,t3);
t5=C_a_i_cons(&a,2,lf[258],t4);
t6=C_a_i_cons(&a,2,t1,t5);
t7=C_a_i_cons(&a,2,lf[259],t6);
t8=C_a_i_cons(&a,2,t1,t7);
t9=C_a_i_cons(&a,2,lf[260],t8);
/* csc.scm:28: ##sys#print-to-string */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[213]);
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[213]+1);
av2[1]=t2;
av2[2]=t9;
tp(3,av2);}}

/* k4317 in k4310 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_ccall f_4319(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4319,c,av);}
/* csc.scm:320: chicken.base#print */
t2=*((C_word*)lf[157]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* t-options in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in ... */
static void C_fcall f_4352(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,0,3)))){
C_save_and_reclaim_args((void *)trf_4352,2,t1,t2);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4357,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:535: scheme#append */
t4=*((C_word*)lf[137]+1);{
C_word av2[4];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_retrieve2(lf[109],C_text("main#translate-options"));
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k4355 in t-options in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in ... */
static void C_ccall f_4357(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4357,c,av);}
t2=C_mutate(&lf[109] /* (set! main#translate-options ...) */,t1);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* check in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in ... */
static void C_fcall f_4359(C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,0,3)))){
C_save_and_reclaim_args((void *)trf_4359,4,t1,t2,t3,t4);}
a=C_alloc(3);
t5=C_i_length(t3);
if(C_truep(C_i_nullp(t4))){
if(C_truep(C_i_greater_or_equalp(t5,C_fix(1)))){
t6=C_SCHEME_UNDEFINED;
t7=t1;{
C_word av2[2];
av2[0]=t7;
av2[1]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}
else{
/* csc.scm:539: stop */
f_3819(t1,lf[168],C_a_i_list(&a,1,t2));}}
else{
t6=C_i_car(t4);
if(C_truep(C_i_greater_or_equalp(t5,t6))){
t7=C_SCHEME_UNDEFINED;
t8=t1;{
C_word av2[2];
av2[0]=t8;
av2[1]=t7;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}
else{
/* csc.scm:539: stop */
f_3819(t1,lf[168],C_a_i_list(&a,1,t2));}}}

/* shared-build in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in ... */
static void C_fcall f_4385(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,3)))){
C_save_and_reclaim_args((void *)trf_4385,2,t1,t2);}
a=C_alloc(10);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4390,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* csc.scm:542: cons* */
f_2943(t3,lf[173],C_a_i_list(&a,2,lf[174],C_retrieve2(lf[109],C_text("main#translate-options"))));}

/* k4388 in shared-build in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in ... */
static void C_ccall f_4390(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_4390,c,av);}
a=C_alloc(4);
t2=C_mutate(&lf[109] /* (set! main#translate-options ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4394,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* csc.scm:543: scheme#append */
t4=*((C_word*)lf[137]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_retrieve2(lf[73],C_text("main#pic-options"));
av2[3]=lf[172];
av2[4]=C_retrieve2(lf[112],C_text("main#compile-options"));
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k4392 in k4388 in shared-build in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_4394(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_4394,c,av);}
a=C_alloc(3);
t2=C_mutate(&lf[112] /* (set! main#compile-options ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4398,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_retrieve2(lf[31],C_text("main#osx")))){
if(C_truep(((C_word*)t0)[3])){
/* csc.scm:545: scheme#append */
t4=*((C_word*)lf[137]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[169];
av2[3]=C_retrieve2(lf[117],C_text("main#link-options"));
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
/* csc.scm:545: scheme#append */
t4=*((C_word*)lf[137]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[170];
av2[3]=C_retrieve2(lf[117],C_text("main#link-options"));
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}}
else{
/* csc.scm:545: scheme#append */
t4=*((C_word*)lf[137]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[171];
av2[3]=C_retrieve2(lf[117],C_text("main#link-options"));
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}}

/* k4396 in k4392 in k4388 in shared-build in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_ccall f_4398(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4398,c,av);}
t2=C_mutate(&lf[117] /* (set! main#link-options ...) */,t1);
t3=lf[124] /* main#shared */ =C_SCHEME_TRUE;;
t4=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* generate-target-filename in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in ... */
static void C_fcall f_4415(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,3)))){
C_save_and_reclaim_args((void *)trf_4415,2,t1,t2);}
if(C_truep(C_retrieve2(lf[124],C_text("main#shared")))){
t3=C_retrieve2(lf[69],C_text("main#shared-library-extension"));
t4=C_retrieve2(lf[69],C_text("main#shared-library-extension"));
/* csc.scm:555: chicken.pathname#pathname-replace-extension */
t5=C_fast_retrieve(lf[175]);{
C_word av2[4];
av2[0]=t5;
av2[1]=t1;
av2[2]=t2;
av2[3]=C_retrieve2(lf[69],C_text("main#shared-library-extension"));
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}
else{
if(C_truep(C_retrieve2(lf[122],C_text("main#compile-only")))){
t3=C_retrieve2(lf[64],C_text("main#object-extension"));
t4=C_retrieve2(lf[64],C_text("main#object-extension"));
/* csc.scm:555: chicken.pathname#pathname-replace-extension */
t5=C_fast_retrieve(lf[175]);{
C_word av2[4];
av2[0]=t5;
av2[1]=t1;
av2[2]=t2;
av2[3]=C_retrieve2(lf[64],C_text("main#object-extension"));
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}
else{
t3=C_retrieve2(lf[67],C_text("main#executable-extension"));
/* csc.scm:555: chicken.pathname#pathname-replace-extension */
t4=C_fast_retrieve(lf[175]);{
C_word av2[4];
av2[0]=t4;
av2[1]=t1;
av2[2]=t2;
av2[3]=C_retrieve2(lf[67],C_text("main#executable-extension"));
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}}}

/* loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in ... */
static void C_fcall f_4431(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,0,3)))){
C_save_and_reclaim_args((void *)trf_4431,3,t0,t1,t2);}
a=C_alloc(11);
if(C_truep(C_i_nullp(t2))){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4442,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* csc.scm:564: scheme#append */
t4=*((C_word*)lf[137]+1);{
C_word av2[4];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_retrieve2(lf[112],C_text("main#compile-options"));
av2[3]=C_retrieve2(lf[113],C_text("main#builtin-compile-options"));
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t3=C_i_car(t2);
t4=C_u_i_cdr(t2);
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_4698,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t5,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=t3,a[8]=((C_word*)t0)[6],tmp=(C_word)a,a+=9,tmp);
/* csc.scm:617: scheme#string->symbol */
t7=*((C_word*)lf[416]+1);{
C_word av2[3];
av2[0]=t7;
av2[1]=t6;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}}

/* k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in ... */
static void C_ccall f_4442(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,4)))){
C_save_and_reclaim((void *)f_4442,c,av);}
a=C_alloc(13);
t2=C_mutate(&lf[112] /* (set! main#compile-options ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4446,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4691,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4105,a[2]=t4,tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_retrieve2(lf[36],C_text("main#elf")))){
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4187,a[2]=t5,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:264: chicken.string#conc */
t7=C_fast_retrieve(lf[248]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t7;
av2[1]=t6;
av2[2]=lf[251];
av2[3]=C_retrieve2(lf[106],C_text("main#library-dir"));
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}
else{
if(C_truep(C_retrieve2(lf[35],C_text("main#aix")))){
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4211,a[2]=t5,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:272: chicken.string#conc */
t7=C_fast_retrieve(lf[248]);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t7;
av2[1]=t6;
av2[2]=lf[252];
av2[3]=C_retrieve2(lf[106],C_text("main#library-dir"));
av2[4]=lf[253];
((C_proc)(void*)(*((C_word*)t7+1)))(5,av2);}}
else{
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4218,a[2]=t5,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:274: chicken.string#conc */
t7=C_fast_retrieve(lf[248]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t7;
av2[1]=t6;
av2[2]=lf[254];
av2[3]=C_retrieve2(lf[106],C_text("main#library-dir"));
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}}}

/* k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_4446(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_4446,c,av);}
a=C_alloc(10);
t2=C_mutate(&lf[117] /* (set! main#link-options ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4449,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_retrieve2(lf[96],C_text("main#inquiry-only")))){
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4654,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_retrieve2(lf[97],C_text("main#show-cflags")))){
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4687,a[2]=t4,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:568: compiler-options */
f_6704(t5);}
else{
t5=t4;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_UNDEFINED;
f_4654(2,av2);}}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_4449(2,av2);}}}

/* k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_ccall f_4449(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_4449,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4452,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_retrieve2(lf[122],C_text("main#compile-only")))){
t3=C_i_length(C_retrieve2(lf[84],C_text("main#scheme-files")));
t4=C_i_length(C_retrieve2(lf[85],C_text("main#c-files")));
t5=C_a_i_fixnum_plus(&a,2,t3,t4);
if(C_truep(C_i_integer_greaterp(t5,C_fix(1)))){
/* csc.scm:577: stop */
f_3819(t2,lf[236],C_SCHEME_END_OF_LIST);}
else{
t6=t2;{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_SCHEME_UNDEFINED;
f_4452(2,av2);}}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_4452(2,av2);}}}

/* k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in ... */
static void C_ccall f_4452(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(22,c,3)))){
C_save_and_reclaim((void *)f_4452,c,av);}
a=C_alloc(22);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4455,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_i_nullp(C_retrieve2(lf[84],C_text("main#scheme-files"))))){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4564,a[2]=t2,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t4=C_i_nullp(C_retrieve2(lf[85],C_text("main#c-files")));
t5=(C_truep(t4)?C_i_nullp(C_retrieve2(lf[89],C_text("main#object-files"))):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4588,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_retrieve2(lf[105],C_text("main#show-debugging-help")))){
t7=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4598,a[2]=t6,tmp=(C_word)a,a+=3,tmp);
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4602,a[2]=t7,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:584: cons* */
f_2943(t8,C_retrieve2(lf[56],C_text("main#translator")),C_a_i_list(&a,2,lf[220],C_retrieve2(lf[109],C_text("main#translate-options"))));}
else{
/* csc.scm:585: stop */
f_3819(t3,lf[219],C_SCHEME_END_OF_LIST);}}
else{
t6=t3;{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_SCHEME_UNDEFINED;
f_4564(2,av2);}}}
else{
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4611,a[2]=t2,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t4=(C_truep(C_retrieve2(lf[124],C_text("main#shared")))?C_i_not(C_retrieve2(lf[95],C_text("main#embedded"))):C_SCHEME_FALSE);
if(C_truep(t4)){
t5=C_a_i_cons(&a,2,lf[235],C_retrieve2(lf[109],C_text("main#translate-options")));
t6=C_mutate(&lf[109] /* (set! main#translate-options ...) */,t5);
t7=t3;
f_4611(t7,t6);}
else{
t5=t3;
f_4611(t5,C_SCHEME_UNDEFINED);}}}

/* k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in ... */
static void C_ccall f_4455(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,3)))){
C_save_and_reclaim((void *)f_4455,c,av);}
a=C_alloc(18);
if(C_truep(C_retrieve2(lf[121],C_text("main#translate-only")))){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4461,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=C_SCHEME_END_OF_LIST;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6432,a[2]=t4,tmp=(C_word)a,a+=3,tmp);
t6=C_retrieve2(lf[85],C_text("main#c-files"));
t7=C_i_check_list_2(C_retrieve2(lf[85],C_text("main#c-files")),lf[176]);
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6518,a[2]=t4,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6681,a[2]=t10,a[3]=t5,tmp=(C_word)a,a+=4,tmp));
t12=((C_word*)t10)[1];
f_6681(t12,t8,C_retrieve2(lf[85],C_text("main#c-files")));}}

/* k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in ... */
static void C_ccall f_4461(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,3)))){
C_save_and_reclaim((void *)f_4461,c,av);}
a=C_alloc(14);
if(C_truep(C_retrieve2(lf[122],C_text("main#compile-only")))){
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4467,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_i_pairp(C_retrieve2(lf[92],C_text("main#linked-extensions"))))){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4554,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4558,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
t5=C_retrieve2(lf[128],C_text("main#find-object-file"));
t6=C_retrieve2(lf[92],C_text("main#linked-extensions"));
t7=C_retrieve2(lf[92],C_text("main#linked-extensions"));
t8=C_i_noop2(C_retrieve2(lf[92],C_text("main#linked-extensions")),C_SCHEME_UNDEFINED);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3172,a[2]=t10,tmp=(C_word)a,a+=3,tmp));
t12=((C_word*)t10)[1];
f_3172(t12,t4,C_retrieve2(lf[92],C_text("main#linked-extensions")));}
else{
t3=t2;
f_4467(t3,C_SCHEME_UNDEFINED);}}}

/* k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in ... */
static void C_fcall f_4467(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,4)))){
C_save_and_reclaim_args((void *)trf_4467,2,t0,t1);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4470,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_i_member(C_retrieve2(lf[118],C_text("main#target-filename")),C_retrieve2(lf[84],C_text("main#scheme-files"))))){
t3=*((C_word*)lf[39]+1);
t4=*((C_word*)lf[39]+1);
t5=C_i_check_port_2(*((C_word*)lf[39]+1),C_fix(2),C_SCHEME_TRUE,lf[40]);
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4482,a[2]=t2,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* csc.scm:604: ##sys#print */
t7=*((C_word*)lf[44]+1);{
C_word av2[5];
av2[0]=t7;
av2[1]=t6;
av2[2]=lf[203];
av2[3]=C_SCHEME_FALSE;
av2[4]=*((C_word*)lf[39]+1);
((C_proc)(void*)(*((C_word*)t7+1)))(5,av2);}}
else{
t3=t2;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_4470(2,av2);}}}

/* k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in ... */
static void C_ccall f_4470(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_4470,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6762,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=C_retrieve2(lf[89],C_text("main#object-files"));
t4=C_retrieve2(lf[90],C_text("main#generated-object-files"));
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7066,a[2]=t6,a[3]=t4,tmp=(C_word)a,a+=4,tmp));
t8=((C_word*)t6)[1];
f_7066(t8,t2,C_retrieve2(lf[89],C_text("main#object-files")),C_retrieve2(lf[89],C_text("main#object-files")));}

/* k4480 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in ... */
static void C_ccall f_4482(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_4482,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4485,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* csc.scm:604: ##sys#print */
t3=*((C_word*)lf[44]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_retrieve2(lf[118],C_text("main#target-filename"));
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4483 in k4480 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in ... */
static void C_ccall f_4485(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_4485,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4488,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* csc.scm:604: ##sys#print */
t3=*((C_word*)lf[44]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[202];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4486 in k4483 in k4480 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in ... */
static void C_ccall f_4488(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_4488,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4491,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* csc.scm:604: ##sys#print */
t3=*((C_word*)lf[44]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_retrieve2(lf[118],C_text("main#target-filename"));
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4489 in k4486 in k4483 in k4480 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in ... */
static void C_ccall f_4491(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_4491,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4494,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* csc.scm:604: ##sys#print */
t3=*((C_word*)lf[44]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[201];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4492 in k4489 in k4486 in k4483 in k4480 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in ... */
static void C_ccall f_4494(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_4494,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4497,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csc.scm:604: ##sys#write-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[42]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[42]+1);
av2[1]=t2;
av2[2]=C_make_character(10);
av2[3]=((C_word*)t0)[3];
tp(4,av2);}}

/* k4495 in k4492 in k4489 in k4486 in k4483 in k4480 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in ... */
static void C_ccall f_4497(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4497,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4504,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csc.scm:608: chicken.base#open-output-string */
t3=C_fast_retrieve(lf[181]);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k4502 in k4495 in k4492 in k4489 in k4486 in k4483 in k4480 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in ... */
static void C_ccall f_4504(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_4504,c,av);}
a=C_alloc(4);
t2=C_i_check_port_2(t1,C_fix(2),C_SCHEME_TRUE,lf[177]);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4510,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_mk_bool(C_WINDOWS_SHELL))){
/* csc.scm:608: ##sys#print */
t4=*((C_word*)lf[44]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[199];
av2[3]=C_SCHEME_FALSE;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}
else{
/* csc.scm:608: ##sys#print */
t4=*((C_word*)lf[44]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[200];
av2[3]=C_SCHEME_FALSE;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}}

/* k4508 in k4502 in k4495 in k4492 in k4489 in k4486 in k4483 in k4480 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in ... */
static void C_ccall f_4510(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_4510,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4513,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* csc.scm:608: ##sys#write-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[42]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[42]+1);
av2[1]=t2;
av2[2]=C_make_character(32);
av2[3]=((C_word*)t0)[3];
tp(4,av2);}}

/* k4511 in k4508 in k4502 in k4495 in k4492 in k4489 in k4486 in k4483 in k4480 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in ... */
static void C_ccall f_4513(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_4513,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4516,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_mk_bool(C_WINDOWS_SHELL))){
t3=C_retrieve2(lf[54],C_text("main#quotewrap-no-slash-trans"));
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f8626,a[2]=t2,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t5=C_retrieve2(lf[118],C_text("main#target-filename"));
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f8710,a[2]=t4,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:92: chicken.pathname#normalize-pathname */
t7=C_fast_retrieve(lf[53]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t7;
av2[1]=t6;
av2[2]=C_retrieve2(lf[118],C_text("main#target-filename"));
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}
else{
t3=C_retrieve2(lf[50],C_text("main#quotewrap"));
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f8630,a[2]=t2,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* csc.scm:608: g1072 */
t5=C_retrieve2(lf[50],C_text("main#quotewrap"));{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=C_retrieve2(lf[118],C_text("main#target-filename"));
f_3868(3,av2);}}}

/* k4514 in k4511 in k4508 in k4502 in k4495 in k4492 in k4489 in k4486 in k4483 in k4480 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in ... */
static void C_ccall f_4516(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_4516,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4519,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* csc.scm:608: ##sys#write-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[42]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[42]+1);
av2[1]=t2;
av2[2]=C_make_character(32);
av2[3]=((C_word*)t0)[3];
tp(4,av2);}}

/* k4517 in k4514 in k4511 in k4508 in k4502 in k4495 in k4492 in k4489 in k4486 in k4483 in k4480 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in ... */
static void C_ccall f_4519(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_4519,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4522,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(C_truep(C_mk_bool(C_WINDOWS_SHELL))?C_retrieve2(lf[54],C_text("main#quotewrap-no-slash-trans")):C_retrieve2(lf[50],C_text("main#quotewrap")));
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4532,a[2]=t2,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4536,a[2]=t3,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* csc.scm:612: scheme#string-append */
t6=*((C_word*)lf[76]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=C_retrieve2(lf[118],C_text("main#target-filename"));
av2[3]=lf[198];
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* k4520 in k4517 in k4514 in k4511 in k4508 in k4502 in k4495 in k4492 in k4489 in k4486 in k4483 in k4480 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in ... */
static void C_ccall f_4522(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4522,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4525,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csc.scm:608: chicken.base#get-output-string */
t3=C_fast_retrieve(lf[178]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4523 in k4520 in k4517 in k4514 in k4511 in k4508 in k4502 in k4495 in k4492 in k4489 in k4486 in k4483 in k4480 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in ... */
static void C_ccall f_4525(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4525,c,av);}
/* csc.scm:607: command */
f_7488(((C_word*)t0)[2],t1);}

/* k4530 in k4517 in k4514 in k4511 in k4508 in k4502 in k4495 in k4492 in k4489 in k4486 in k4483 in k4480 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in ... */
static void C_ccall f_4532(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_4532,c,av);}
/* csc.scm:608: ##sys#print */
t2=*((C_word*)lf[44]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k4534 in k4517 in k4514 in k4511 in k4508 in k4502 in k4495 in k4492 in k4489 in k4486 in k4483 in k4480 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in ... */
static void C_ccall f_4536(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4536,c,av);}
/* csc.scm:608: g1074 */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k4552 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in ... */
static void C_ccall f_4554(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4554,c,av);}
t2=C_mutate(&lf[89] /* (set! main#object-files ...) */,t1);
t3=((C_word*)t0)[2];
f_4467(t3,t2);}

/* k4556 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in ... */
static void C_ccall f_4558(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4558,c,av);}
/* csc.scm:602: scheme#append */
t2=*((C_word*)lf[137]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_retrieve2(lf[89],C_text("main#object-files"));
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k4562 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in ... */
static void C_ccall f_4564(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_4564,c,av);}
a=C_alloc(5);
if(C_truep(C_retrieve2(lf[118],C_text("main#target-filename")))){
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
f_4455(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4571,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=C_i_nullp(C_retrieve2(lf[85],C_text("main#c-files")));
t4=(C_truep(t3)?C_retrieve2(lf[89],C_text("main#object-files")):C_retrieve2(lf[85],C_text("main#c-files")));
t5=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_3285,tmp=(C_word)a,a+=2,tmp);
t6=(
  f_3285(t4)
);
/* csc.scm:588: generate-target-filename */
f_4415(t2,t6);}}

/* k4569 in k4562 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in ... */
static void C_ccall f_4571(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4571,c,av);}
t2=C_mutate(&lf[118] /* (set! main#target-filename ...) */,t1);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
f_4455(2,av2);}}

/* k4586 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in ... */
static void C_ccall f_4588(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4588,c,av);}
/* csc.scm:585: stop */
f_3819(((C_word*)t0)[2],lf[219],C_SCHEME_END_OF_LIST);}

/* k4596 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in ... */
static void C_ccall f_4598(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4598,c,av);}
/* csc.scm:582: command */
f_7488(((C_word*)t0)[2],t1);}

/* k4600 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in ... */
static void C_ccall f_4602(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4602,c,av);}
/* csc.scm:583: chicken.string#string-intersperse */
t2=C_fast_retrieve(lf[136]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k4609 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in ... */
static void C_fcall f_4611(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_4611,2,t0,t1);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4614,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_retrieve2(lf[118],C_text("main#target-filename")))){
t3=t2;
f_4614(t3,C_SCHEME_UNDEFINED);}
else{
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4621,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=C_retrieve2(lf[84],C_text("main#scheme-files"));
/* csc.scm:595: generate-target-filename */
f_4415(t3,C_i_car(C_retrieve2(lf[84],C_text("main#scheme-files"))));}}

/* k4612 in k4609 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in ... */
static void C_fcall f_4614(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,3)))){
C_save_and_reclaim_args((void *)trf_4614,2,t0,t1);}
a=C_alloc(5);
t2=C_retrieve2(lf[84],C_text("main#scheme-files"));
t3=C_i_check_list_2(C_retrieve2(lf[84],C_text("main#scheme-files")),lf[176]);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6407,a[2]=t5,tmp=(C_word)a,a+=3,tmp));
t7=((C_word*)t5)[1];
f_6407(t7,((C_word*)t0)[2],C_retrieve2(lf[84],C_text("main#scheme-files")));}

/* k4619 in k4609 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in ... */
static void C_ccall f_4621(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4621,c,av);}
t2=C_mutate(&lf[118] /* (set! main#target-filename ...) */,t1);
t3=((C_word*)t0)[2];
f_4614(t3,t2);}

/* k4652 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_ccall f_4654(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_4654,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4657,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_retrieve2(lf[98],C_text("main#show-ldflags")))){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4680,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:569: linker-options */
f_7191(t3);}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_4657(2,av2);}}}

/* k4655 in k4652 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in ... */
static void C_ccall f_4657(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_4657,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4660,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_retrieve2(lf[99],C_text("main#show-libs")))){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4673,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:570: linker-libraries */
f_7244(t3);}
else{
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f8662,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csc.scm:571: scheme#newline */
t4=*((C_word*)lf[237]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k4658 in k4655 in k4652 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in ... */
static void C_ccall f_4660(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4660,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4663,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csc.scm:571: scheme#newline */
t3=*((C_word*)lf[237]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k4661 in k4658 in k4655 in k4652 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in ... */
static void C_ccall f_4663(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4663,c,av);}
/* csc.scm:572: chicken.base#exit */
t2=C_fast_retrieve(lf[41]);{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k4671 in k4655 in k4652 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in ... */
static void C_ccall f_4673(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4673,c,av);}
/* csc.scm:570: chicken.base#print* */
t2=*((C_word*)lf[238]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_make_character(32);
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k4678 in k4652 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in ... */
static void C_ccall f_4680(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4680,c,av);}
/* csc.scm:569: chicken.base#print* */
t2=*((C_word*)lf[238]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_make_character(32);
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k4685 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_ccall f_4687(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4687,c,av);}
/* csc.scm:568: chicken.base#print* */
t2=*((C_word*)lf[238]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_make_character(32);
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k4689 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_4691(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4691,c,av);}
/* csc.scm:565: scheme#append */
t2=*((C_word*)lf[137]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_retrieve2(lf[117],C_text("main#link-options"));
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in ... */
static void C_ccall f_4698(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word t24;
C_word t25;
C_word t26;
C_word t27;
C_word t28;
C_word t29;
C_word t30;
C_word t31;
C_word t32;
C_word t33;
C_word t34;
C_word t35;
C_word t36;
C_word t37;
C_word t38;
C_word t39;
C_word t40;
C_word t41;
C_word t42;
C_word t43;
C_word t44;
C_word t45;
C_word t46;
C_word t47;
C_word t48;
C_word t49;
C_word t50;
C_word t51;
C_word t52;
C_word t53;
C_word t54;
C_word t55;
C_word t56;
C_word t57;
C_word t58;
C_word t59;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(20,c,4)))){
C_save_and_reclaim((void *)f_4698,c,av);}
a=C_alloc(20);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4701,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=C_eqp(t1,lf[255]);
t4=(C_truep(t3)?t3:C_eqp(t1,lf[256]));
if(C_truep(t4)){
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4713,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4312,a[2]=t5,tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t7=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t7;
av2[1]=t6;
av2[2]=C_mpointer(&a,(void*)C_CSC_PROGRAM);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}
else{
t5=C_eqp(t1,lf[261]);
if(C_truep(t5)){
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4725,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t7=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4732,a[2]=t6,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:623: chicken.platform#chicken-version */
t8=C_fast_retrieve(lf[262]);{
C_word *av2=av;
av2[0]=t8;
av2[1]=t7;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}
else{
t6=C_eqp(t1,lf[263]);
if(C_truep(t6)){
t7=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4741,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4748,a[2]=t7,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:626: chicken.base#open-output-string */
t9=C_fast_retrieve(lf[181]);{
C_word *av2=av;
av2[0]=t9;
av2[1]=t8;
((C_proc)(void*)(*((C_word*)t9+1)))(2,av2);}}
else{
t7=C_eqp(t1,lf[265]);
if(C_truep(t7)){
t8=lf[93] /* main#cpp-mode */ =C_SCHEME_TRUE;;
if(C_truep(C_retrieve2(lf[31],C_text("main#osx")))){
t9=C_a_i_cons(&a,2,lf[266],C_retrieve2(lf[112],C_text("main#compile-options")));
t10=C_mutate(&lf[112] /* (set! main#compile-options ...) */,t9);
/* csc.scm:857: loop */
t11=((C_word*)((C_word*)t0)[2])[1];
f_4431(t11,((C_word*)t0)[3],((C_word*)((C_word*)t0)[4])[1]);}
else{
/* csc.scm:857: loop */
t9=((C_word*)((C_word*)t0)[2])[1];
f_4431(t9,((C_word*)t0)[3],((C_word*)((C_word*)t0)[4])[1]);}}
else{
t8=C_eqp(t1,lf[267]);
if(C_truep(t8)){
t9=lf[94] /* main#objc-mode */ =C_SCHEME_TRUE;;
/* csc.scm:857: loop */
t10=((C_word*)((C_word*)t0)[2])[1];
f_4431(t10,((C_word*)t0)[3],((C_word*)((C_word*)t0)[4])[1]);}
else{
t9=C_eqp(t1,lf[268]);
if(C_truep(t9)){
t10=C_a_i_cons(&a,2,lf[269],C_retrieve2(lf[109],C_text("main#translate-options")));
t11=C_mutate(&lf[109] /* (set! main#translate-options ...) */,t10);
t12=lf[125] /* main#static */ =C_SCHEME_TRUE;;
/* csc.scm:857: loop */
t13=((C_word*)((C_word*)t0)[2])[1];
f_4431(t13,((C_word*)t0)[3],((C_word*)((C_word*)t0)[4])[1]);}
else{
t10=C_eqp(t1,lf[270]);
if(C_truep(t10)){
t11=lf[96] /* main#inquiry-only */ =C_SCHEME_TRUE;;
t12=lf[97] /* main#show-cflags */ =C_SCHEME_TRUE;;
/* csc.scm:857: loop */
t13=((C_word*)((C_word*)t0)[2])[1];
f_4431(t13,((C_word*)t0)[3],((C_word*)((C_word*)t0)[4])[1]);}
else{
t11=C_eqp(t1,lf[271]);
if(C_truep(t11)){
t12=lf[96] /* main#inquiry-only */ =C_SCHEME_TRUE;;
t13=lf[98] /* main#show-ldflags */ =C_SCHEME_TRUE;;
/* csc.scm:857: loop */
t14=((C_word*)((C_word*)t0)[2])[1];
f_4431(t14,((C_word*)t0)[3],((C_word*)((C_word*)t0)[4])[1]);}
else{
t12=C_eqp(t1,lf[272]);
if(C_truep(t12)){
t13=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4820,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:642: chicken.base#print */
t14=*((C_word*)lf[157]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t14;
av2[1]=t13;
av2[2]=C_retrieve2(lf[57],C_text("main#compiler"));
((C_proc)(void*)(*((C_word*)t14+1)))(3,av2);}}
else{
t13=C_eqp(t1,lf[273]);
if(C_truep(t13)){
t14=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4832,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:643: chicken.base#print */
t15=*((C_word*)lf[157]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t15;
av2[1]=t14;
av2[2]=C_retrieve2(lf[58],C_text("main#c++-compiler"));
((C_proc)(void*)(*((C_word*)t15+1)))(3,av2);}}
else{
t14=C_eqp(t1,lf[274]);
if(C_truep(t14)){
t15=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4844,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:644: chicken.base#print */
t16=*((C_word*)lf[157]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t16;
av2[1]=t15;
av2[2]=C_retrieve2(lf[60],C_text("main#linker"));
((C_proc)(void*)(*((C_word*)t16+1)))(3,av2);}}
else{
t15=C_eqp(t1,lf[275]);
if(C_truep(t15)){
t16=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4856,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:645: chicken.base#print */
t17=*((C_word*)lf[157]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t17;
av2[1]=t16;
av2[2]=C_retrieve2(lf[55],C_text("main#home"));
((C_proc)(void*)(*((C_word*)t17+1)))(3,av2);}}
else{
t16=C_eqp(t1,lf[276]);
if(C_truep(t16)){
t17=lf[96] /* main#inquiry-only */ =C_SCHEME_TRUE;;
t18=lf[99] /* main#show-libs */ =C_SCHEME_TRUE;;
/* csc.scm:857: loop */
t19=((C_word*)((C_word*)t0)[2])[1];
f_4431(t19,((C_word*)t0)[3],((C_word*)((C_word*)t0)[4])[1]);}
else{
t17=C_eqp(t1,lf[277]);
t18=(C_truep(t17)?t17:C_eqp(t1,lf[278]));
if(C_truep(t18)){
t19=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4879,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_i_numberp(C_retrieve2(lf[119],C_text("main#verbose"))))){
t20=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4894,a[2]=t19,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:651: cons* */
f_2943(t20,lf[281],C_a_i_list(&a,2,lf[282],C_retrieve2(lf[112],C_text("main#compile-options"))));}
else{
t20=t19;
f_4879(t20,C_SCHEME_UNDEFINED);}}
else{
t19=C_eqp(t1,lf[283]);
t20=(C_truep(t19)?t19:C_eqp(t1,lf[284]));
if(C_truep(t20)){
t21=C_a_i_cons(&a,2,lf[285],C_retrieve2(lf[112],C_text("main#compile-options")));
t22=C_mutate(&lf[112] /* (set! main#compile-options ...) */,t21);
/* csc.scm:659: t-options */
f_4352(t2,C_a_i_list(&a,1,lf[286]));}
else{
t21=C_eqp(t1,lf[287]);
t22=(C_truep(t21)?t21:C_eqp(t1,lf[288]));
if(C_truep(t22)){
t23=lf[121] /* main#translate-only */ =C_SCHEME_TRUE;;
/* csc.scm:662: t-options */
f_4352(t2,C_a_i_list(&a,1,lf[289]));}
else{
t23=C_eqp(t1,lf[290]);
t24=(C_truep(t23)?t23:C_eqp(t1,lf[291]));
if(C_truep(t24)){
t25=lf[121] /* main#translate-only */ =C_SCHEME_TRUE;;
/* csc.scm:665: t-options */
f_4352(t2,C_a_i_list(&a,1,lf[292]));}
else{
t25=C_eqp(t1,lf[293]);
if(C_truep(t25)){
t26=lf[120] /* main#keep-files */ =C_SCHEME_TRUE;;
/* csc.scm:857: loop */
t27=((C_word*)((C_word*)t0)[2])[1];
f_4431(t27,((C_word*)t0)[3],((C_word*)((C_word*)t0)[4])[1]);}
else{
t26=C_eqp(t1,lf[294]);
if(C_truep(t26)){
t27=lf[122] /* main#compile-only */ =C_SCHEME_TRUE;;
/* csc.scm:857: loop */
t28=((C_word*)((C_word*)t0)[2])[1];
f_4431(t28,((C_word*)t0)[3],((C_word*)((C_word*)t0)[4])[1]);}
else{
t27=C_eqp(t1,lf[295]);
if(C_truep(t27)){
t28=lf[121] /* main#translate-only */ =C_SCHEME_TRUE;;
/* csc.scm:857: loop */
t29=((C_word*)((C_word*)t0)[2])[1];
f_4431(t29,((C_word*)t0)[3],((C_word*)((C_word*)t0)[4])[1]);}
else{
t28=C_eqp(t1,lf[296]);
t29=(C_truep(t28)?t28:C_eqp(t1,lf[297]));
if(C_truep(t29)){
t30=lf[95] /* main#embedded */ =C_SCHEME_TRUE;;
t31=C_a_i_cons(&a,2,lf[298],C_retrieve2(lf[112],C_text("main#compile-options")));
t32=C_mutate(&lf[112] /* (set! main#compile-options ...) */,t31);
/* csc.scm:857: loop */
t33=((C_word*)((C_word*)t0)[2])[1];
f_4431(t33,((C_word*)t0)[3],((C_word*)((C_word*)t0)[4])[1]);}
else{
t30=C_eqp(t1,lf[299]);
if(C_truep(t30)){
t31=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4984,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* csc.scm:673: check */
f_4359(t31,t1,((C_word*)((C_word*)t0)[4])[1],C_SCHEME_END_OF_LIST);}
else{
t31=C_eqp(t1,lf[302]);
if(C_truep(t31)){
t32=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5016,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* csc.scm:679: check */
f_4359(t32,t1,((C_word*)((C_word*)t0)[4])[1],C_SCHEME_END_OF_LIST);}
else{
t32=C_eqp(t1,lf[303]);
t33=(C_truep(t32)?t32:C_eqp(t1,lf[304]));
if(C_truep(t33)){
t34=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5036,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* csc.scm:683: check */
f_4359(t34,t1,((C_word*)((C_word*)t0)[4])[1],C_SCHEME_END_OF_LIST);}
else{
t34=C_eqp(t1,lf[306]);
if(C_truep(t34)){
t35=C_a_i_cons(&a,2,lf[307],C_retrieve2(lf[112],C_text("main#compile-options")));
t36=C_mutate(&lf[112] /* (set! main#compile-options ...) */,t35);
/* csc.scm:857: loop */
t37=((C_word*)((C_word*)t0)[2])[1];
f_4431(t37,((C_word*)t0)[3],((C_word*)((C_word*)t0)[4])[1]);}
else{
t35=C_eqp(t1,lf[308]);
if(C_truep(t35)){
t36=lf[104] /* main#ignore-repository */ =C_SCHEME_TRUE;;
/* csc.scm:690: t-options */
f_4352(t2,C_a_i_list(&a,1,((C_word*)t0)[7]));}
else{
t36=C_eqp(t1,lf[309]);
if(C_truep(t36)){
t37=C_set_block_item(lf[130] /* ##sys#setup-mode */,0,C_SCHEME_TRUE);
/* csc.scm:693: t-options */
f_4352(t2,C_a_i_list(&a,1,((C_word*)t0)[7]));}
else{
t37=C_eqp(t1,lf[310]);
if(C_truep(t37)){
t38=lf[74] /* main#generate-manifest */ =C_SCHEME_TRUE;;
/* csc.scm:857: loop */
t39=((C_word*)((C_word*)t0)[2])[1];
f_4431(t39,((C_word*)t0)[3],((C_word*)((C_word*)t0)[4])[1]);}
else{
t38=C_eqp(t1,lf[311]);
if(C_truep(t38)){
t39=lf[101] /* main#gui */ =C_SCHEME_TRUE;;
t40=C_a_i_cons(&a,2,lf[312],C_retrieve2(lf[112],C_text("main#compile-options")));
t41=C_mutate(&lf[112] /* (set! main#compile-options ...) */,t40);
if(C_truep(C_retrieve2(lf[29],C_text("main#mingw")))){
t42=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5109,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* csc.scm:701: chicken.pathname#make-pathname */
t43=C_fast_retrieve(lf[132]);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t43;
av2[1]=t42;
av2[2]=C_retrieve2(lf[20],C_text("main#host-sharedir"));
av2[3]=lf[317];
av2[4]=C_retrieve2(lf[64],C_text("main#object-extension"));
((C_proc)(void*)(*((C_word*)t43+1)))(5,av2);}}
else{
/* csc.scm:857: loop */
t42=((C_word*)((C_word*)t0)[2])[1];
f_4431(t42,((C_word*)t0)[3],((C_word*)((C_word*)t0)[4])[1]);}}
else{
t39=C_eqp(t1,lf[318]);
if(C_truep(t39)){
t40=lf[102] /* main#deployed */ =C_SCHEME_TRUE;;
/* csc.scm:857: loop */
t41=((C_word*)((C_word*)t0)[2])[1];
f_4431(t41,((C_word*)t0)[3],((C_word*)((C_word*)t0)[4])[1]);}
else{
t40=C_eqp(t1,lf[319]);
if(C_truep(t40)){
t41=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5125,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* csc.scm:711: check */
f_4359(t41,t1,((C_word*)((C_word*)t0)[4])[1],C_SCHEME_END_OF_LIST);}
else{
t41=C_eqp(t1,lf[321]);
t42=(C_truep(t41)?t41:C_eqp(t1,lf[322]));
if(C_truep(t42)){
t43=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5152,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* csc.scm:716: check */
f_4359(t43,t1,((C_word*)((C_word*)t0)[4])[1],C_SCHEME_END_OF_LIST);}
else{
t43=C_eqp(t1,lf[323]);
t44=(C_truep(t43)?t43:C_eqp(t1,lf[324]));
if(C_truep(t44)){
t45=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5173,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* csc.scm:720: cons* */
f_2943(t45,lf[325],C_a_i_list(&a,2,lf[326],((C_word*)((C_word*)t0)[4])[1]));}
else{
t45=C_eqp(t1,lf[327]);
if(C_truep(t45)){
t46=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5183,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* csc.scm:721: cons* */
f_2943(t46,lf[328],C_a_i_list(&a,2,lf[329],((C_word*)((C_word*)t0)[4])[1]));}
else{
t46=C_eqp(t1,lf[330]);
if(C_truep(t46)){
t47=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5193,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* csc.scm:722: cons* */
f_2943(t47,lf[331],C_a_i_list(&a,2,lf[332],((C_word*)((C_word*)t0)[4])[1]));}
else{
t47=C_eqp(t1,lf[333]);
if(C_truep(t47)){
t48=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5203,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* csc.scm:723: cons* */
f_2943(t48,lf[334],C_a_i_list(&a,2,lf[335],((C_word*)((C_word*)t0)[4])[1]));}
else{
t48=C_eqp(t1,lf[336]);
if(C_truep(t48)){
t49=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5213,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* csc.scm:724: cons* */
f_2943(t49,lf[337],C_a_i_list(&a,2,lf[338],((C_word*)((C_word*)t0)[4])[1]));}
else{
t49=C_eqp(t1,lf[339]);
if(C_truep(t49)){
t50=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5223,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* csc.scm:726: cons* */
f_2943(t50,lf[340],C_a_i_list(&a,2,lf[341],((C_word*)((C_word*)t0)[4])[1]));}
else{
t50=C_eqp(t1,lf[342]);
if(C_truep(t50)){
t51=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5233,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* csc.scm:727: cons* */
f_2943(t51,lf[343],C_a_i_list(&a,2,lf[344],((C_word*)((C_word*)t0)[4])[1]));}
else{
t51=C_eqp(t1,lf[345]);
if(C_truep(t51)){
t52=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5243,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* csc.scm:728: cons* */
f_2943(t52,lf[346],C_a_i_list(&a,2,lf[347],((C_word*)((C_word*)t0)[4])[1]));}
else{
t52=C_eqp(t1,lf[348]);
if(C_truep(t52)){
t53=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5253,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* csc.scm:729: cons* */
f_2943(t53,lf[349],C_a_i_list(&a,2,lf[350],((C_word*)((C_word*)t0)[4])[1]));}
else{
t53=C_eqp(t1,lf[351]);
if(C_truep(t53)){
t54=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5263,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* csc.scm:730: cons* */
f_2943(t54,lf[352],C_a_i_list(&a,2,lf[353],((C_word*)((C_word*)t0)[4])[1]));}
else{
t54=C_eqp(t1,lf[354]);
if(C_truep(t54)){
t55=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5272,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[7],tmp=(C_word)a,a+=7,tmp);
/* csc.scm:732: check */
f_4359(t55,t1,((C_word*)((C_word*)t0)[4])[1],C_SCHEME_END_OF_LIST);}
else{
t55=C_eqp(t1,lf[355]);
if(C_truep(t55)){
t56=lf[119] /* main#verbose */ =C_SCHEME_TRUE;;
t57=lf[100] /* main#dry-run */ =C_SCHEME_TRUE;;
/* csc.scm:857: loop */
t58=((C_word*)((C_word*)t0)[2])[1];
f_4431(t58,((C_word*)t0)[3],((C_word*)((C_word*)t0)[4])[1]);}
else{
t56=C_eqp(t1,lf[356]);
t57=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_5314,a[2]=((C_word*)t0)[8],a[3]=t2,a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[2],a[7]=((C_word*)t0)[3],a[8]=((C_word*)t0)[6],a[9]=((C_word*)t0)[5],a[10]=((C_word*)t0)[7],tmp=(C_word)a,a+=11,tmp);
if(C_truep(t56)){
t58=t57;
f_5314(t58,t56);}
else{
t58=C_eqp(t1,lf[414]);
t59=t57;
f_5314(t59,(C_truep(t58)?t58:C_eqp(t1,lf[415])));}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}

/* k4699 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_4701(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4701,c,av);}
/* csc.scm:857: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_4431(t2,((C_word*)t0)[3],((C_word*)((C_word*)t0)[4])[1]);}

/* k4711 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_4713(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4713,c,av);}
/* csc.scm:621: chicken.base#exit */
t2=C_fast_retrieve(lf[41]);{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k4723 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_4725(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4725,c,av);}
/* csc.scm:624: chicken.base#exit */
t2=C_fast_retrieve(lf[41]);{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k4730 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_4732(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4732,c,av);}
/* csc.scm:623: chicken.base#print */
t2=*((C_word*)lf[157]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k4739 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_4741(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4741,c,av);}
/* csc.scm:627: chicken.base#exit */
t2=C_fast_retrieve(lf[41]);{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k4746 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_4748(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_4748,c,av);}
a=C_alloc(4);
t2=C_i_check_port_2(t1,C_fix(2),C_SCHEME_TRUE,lf[177]);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4754,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* csc.scm:626: ##sys#print */
t4=*((C_word*)lf[44]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_retrieve2(lf[56],C_text("main#translator"));
av2[3]=C_SCHEME_FALSE;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k4752 in k4746 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_ccall f_4754(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_4754,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4757,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* csc.scm:626: ##sys#write-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[42]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[42]+1);
av2[1]=t2;
av2[2]=C_make_character(32);
av2[3]=((C_word*)t0)[3];
tp(4,av2);}}

/* k4755 in k4752 in k4746 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in ... */
static void C_ccall f_4757(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_4757,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4760,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* csc.scm:626: ##sys#print */
t3=*((C_word*)lf[44]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[264];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4758 in k4755 in k4752 in k4746 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in ... */
static void C_ccall f_4760(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4760,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4763,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csc.scm:626: chicken.base#get-output-string */
t3=C_fast_retrieve(lf[178]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4761 in k4758 in k4755 in k4752 in k4746 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in ... */
static void C_ccall f_4763(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4763,c,av);}
/* csc.scm:626: chicken.process#system */
t2=C_fast_retrieve(lf[156]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k4818 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_4820(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4820,c,av);}
/* csc.scm:642: chicken.base#exit */
t2=C_fast_retrieve(lf[41]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_fix(0);
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k4830 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_4832(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4832,c,av);}
/* csc.scm:643: chicken.base#exit */
t2=C_fast_retrieve(lf[41]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_fix(0);
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k4842 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_4844(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4844,c,av);}
/* csc.scm:644: chicken.base#exit */
t2=C_fast_retrieve(lf[41]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_fix(0);
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k4854 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_4856(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4856,c,av);}
/* csc.scm:645: chicken.base#exit */
t2=C_fast_retrieve(lf[41]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_fix(0);
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k4877 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_fcall f_4879(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_4879,2,t0,t1);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4882,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* csc.scm:653: t-options */
f_4352(t2,C_a_i_list(&a,1,lf[279]));}

/* k4880 in k4877 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_ccall f_4882(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4882,c,av);}
if(C_truep(C_retrieve2(lf[119],C_text("main#verbose")))){
t2=lf[119] /* main#verbose */ =C_fix(2);;
/* csc.scm:857: loop */
t3=((C_word*)((C_word*)t0)[2])[1];
f_4431(t3,((C_word*)t0)[3],((C_word*)((C_word*)t0)[4])[1]);}
else{
t2=lf[119] /* main#verbose */ =C_SCHEME_TRUE;;
/* csc.scm:857: loop */
t3=((C_word*)((C_word*)t0)[2])[1];
f_4431(t3,((C_word*)t0)[3],((C_word*)((C_word*)t0)[4])[1]);}}

/* k4892 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_4894(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_4894,c,av);}
a=C_alloc(3);
t2=C_mutate(&lf[112] /* (set! main#compile-options ...) */,t1);
t3=C_a_i_cons(&a,2,lf[280],C_retrieve2(lf[117],C_text("main#link-options")));
t4=C_mutate(&lf[117] /* (set! main#link-options ...) */,t3);
t5=((C_word*)t0)[2];
f_4879(t5,t4);}

/* k4982 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_4984(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_4984,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4987,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=C_i_car(((C_word*)((C_word*)t0)[2])[1]);
/* csc.scm:674: t-options */
f_4352(t2,C_a_i_list(&a,2,lf[301],t3));}

/* k4985 in k4982 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_ccall f_4987(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_4987,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4991,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4999,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:676: chicken.string#string-split */
t4=C_fast_retrieve(lf[240]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_i_car(((C_word*)((C_word*)t0)[2])[1]);
av2[3]=lf[300];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k4989 in k4985 in k4982 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in ... */
static void C_ccall f_4991(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4991,c,av);}
t2=C_mutate(&lf[92] /* (set! main#linked-extensions ...) */,t1);
t3=C_i_cdr(((C_word*)((C_word*)t0)[2])[1]);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t3);
/* csc.scm:857: loop */
t5=((C_word*)((C_word*)t0)[3])[1];
f_4431(t5,((C_word*)t0)[4],((C_word*)((C_word*)t0)[2])[1]);}

/* k4997 in k4985 in k4982 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in ... */
static void C_ccall f_4999(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4999,c,av);}
/* csc.scm:676: scheme#append */
t2=*((C_word*)lf[137]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_retrieve2(lf[92],C_text("main#linked-extensions"));
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k5014 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_5016(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5016,c,av);}
t2=C_i_car(((C_word*)((C_word*)t0)[2])[1]);
t3=C_mutate(&lf[106] /* (set! main#library-dir ...) */,t2);
t4=C_i_cdr(((C_word*)((C_word*)t0)[2])[1]);
t5=C_mutate(((C_word *)((C_word*)t0)[2])+1,t4);
/* csc.scm:857: loop */
t6=((C_word*)((C_word*)t0)[3])[1];
f_4431(t6,((C_word*)t0)[4],((C_word*)((C_word*)t0)[2])[1]);}

/* k5034 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_5036(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_5036,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5039,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=C_i_car(((C_word*)((C_word*)t0)[2])[1]);
/* csc.scm:684: t-options */
f_4352(t2,C_a_i_list(&a,2,lf[305],t3));}

/* k5037 in k5034 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_ccall f_5039(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5039,c,av);}
t2=C_i_cdr(((C_word*)((C_word*)t0)[2])[1]);
t3=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
/* csc.scm:857: loop */
t4=((C_word*)((C_word*)t0)[3])[1];
f_4431(t4,((C_word*)t0)[4],((C_word*)((C_word*)t0)[2])[1]);}

/* k5103 in k5107 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_ccall f_5105(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5105,c,av);}
t2=C_mutate(&lf[117] /* (set! main#link-options ...) */,t1);
/* csc.scm:857: loop */
t3=((C_word*)((C_word*)t0)[2])[1];
f_4431(t3,((C_word*)t0)[3],((C_word*)((C_word*)t0)[4])[1]);}

/* k5107 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_5109(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(20,c,3)))){
C_save_and_reclaim((void *)f_5109,c,av);}
a=C_alloc(20);
t2=C_a_i_cons(&a,2,t1,C_retrieve2(lf[89],C_text("main#object-files")));
t3=C_mutate(&lf[89] /* (set! main#object-files ...) */,t2);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5105,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* csc.scm:706: cons* */
f_2943(t4,lf[313],C_a_i_list(&a,4,lf[314],lf[315],lf[316],C_retrieve2(lf[117],C_text("main#link-options"))));}

/* k5123 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_5125(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,3)))){
C_save_and_reclaim((void *)f_5125,c,av);}
a=C_alloc(11);
if(C_truep(C_retrieve2(lf[31],C_text("main#osx")))){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5136,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=C_i_car(((C_word*)((C_word*)t0)[2])[1]);
/* csc.scm:713: cons* */
f_2943(t2,lf[320],C_a_i_list(&a,2,t3,C_retrieve2(lf[117],C_text("main#link-options"))));}
else{
t2=C_i_cdr(((C_word*)((C_word*)t0)[2])[1]);
t3=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
/* csc.scm:857: loop */
t4=((C_word*)((C_word*)t0)[3])[1];
f_4431(t4,((C_word*)t0)[4],((C_word*)((C_word*)t0)[2])[1]);}}

/* k5134 in k5123 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_ccall f_5136(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5136,c,av);}
t2=C_mutate(&lf[117] /* (set! main#link-options ...) */,t1);
t3=C_i_cdr(((C_word*)((C_word*)t0)[2])[1]);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t3);
/* csc.scm:857: loop */
t5=((C_word*)((C_word*)t0)[3])[1];
f_4431(t5,((C_word*)t0)[4],((C_word*)((C_word*)t0)[2])[1]);}

/* k5150 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_5152(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5152,c,av);}
t2=C_i_car(((C_word*)((C_word*)t0)[2])[1]);
t3=C_i_cdr(((C_word*)((C_word*)t0)[2])[1]);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t3);
t5=C_mutate(&lf[118] /* (set! main#target-filename ...) */,t2);
/* csc.scm:857: loop */
t6=((C_word*)((C_word*)t0)[3])[1];
f_4431(t6,((C_word*)t0)[4],((C_word*)((C_word*)t0)[2])[1]);}

/* k5171 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_5173(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5173,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
/* csc.scm:857: loop */
t3=((C_word*)((C_word*)t0)[3])[1];
f_4431(t3,((C_word*)t0)[4],((C_word*)((C_word*)t0)[2])[1]);}

/* k5181 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_5183(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5183,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
/* csc.scm:857: loop */
t3=((C_word*)((C_word*)t0)[3])[1];
f_4431(t3,((C_word*)t0)[4],((C_word*)((C_word*)t0)[2])[1]);}

/* k5191 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_5193(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5193,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
/* csc.scm:857: loop */
t3=((C_word*)((C_word*)t0)[3])[1];
f_4431(t3,((C_word*)t0)[4],((C_word*)((C_word*)t0)[2])[1]);}

/* k5201 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_5203(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5203,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
/* csc.scm:857: loop */
t3=((C_word*)((C_word*)t0)[3])[1];
f_4431(t3,((C_word*)t0)[4],((C_word*)((C_word*)t0)[2])[1]);}

/* k5211 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_5213(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5213,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
/* csc.scm:857: loop */
t3=((C_word*)((C_word*)t0)[3])[1];
f_4431(t3,((C_word*)t0)[4],((C_word*)((C_word*)t0)[2])[1]);}

/* k5221 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_5223(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5223,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
/* csc.scm:857: loop */
t3=((C_word*)((C_word*)t0)[3])[1];
f_4431(t3,((C_word*)t0)[4],((C_word*)((C_word*)t0)[2])[1]);}

/* k5231 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_5233(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5233,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
/* csc.scm:857: loop */
t3=((C_word*)((C_word*)t0)[3])[1];
f_4431(t3,((C_word*)t0)[4],((C_word*)((C_word*)t0)[2])[1]);}

/* k5241 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_5243(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5243,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
/* csc.scm:857: loop */
t3=((C_word*)((C_word*)t0)[3])[1];
f_4431(t3,((C_word*)t0)[4],((C_word*)((C_word*)t0)[2])[1]);}

/* k5251 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_5253(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5253,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
/* csc.scm:857: loop */
t3=((C_word*)((C_word*)t0)[3])[1];
f_4431(t3,((C_word*)t0)[4],((C_word*)((C_word*)t0)[2])[1]);}

/* k5261 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_5263(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5263,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
/* csc.scm:857: loop */
t3=((C_word*)((C_word*)t0)[3])[1];
f_4431(t3,((C_word*)t0)[4],((C_word*)((C_word*)t0)[2])[1]);}

/* k5270 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_5272(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_5272,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5275,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=C_i_car(((C_word*)((C_word*)t0)[2])[1]);
/* csc.scm:733: t-options */
f_4352(t2,C_a_i_list(&a,2,((C_word*)t0)[6],t3));}

/* k5273 in k5270 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_ccall f_5275(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_5275,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5287,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* ##sys#string->list */
t3=C_fast_retrieve(lf[149]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_i_car(((C_word*)((C_word*)t0)[2])[1]);
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k5285 in k5273 in k5270 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in ... */
static void C_ccall f_5287(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5287,c,av);}
if(C_truep(C_u_i_memq(C_make_character(104),t1))){
t2=lf[105] /* main#show-debugging-help */ =C_SCHEME_TRUE;;
t3=lf[121] /* main#translate-only */ =C_SCHEME_TRUE;;
t4=C_i_cdr(((C_word*)((C_word*)t0)[2])[1]);
t5=C_mutate(((C_word *)((C_word*)t0)[2])+1,t4);
/* csc.scm:857: loop */
t6=((C_word*)((C_word*)t0)[3])[1];
f_4431(t6,((C_word*)t0)[4],((C_word*)((C_word*)t0)[2])[1]);}
else{
t2=C_i_cdr(((C_word*)((C_word*)t0)[2])[1]);
t3=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
/* csc.scm:857: loop */
t4=((C_word*)((C_word*)t0)[3])[1];
f_4431(t4,((C_word*)t0)[4],((C_word*)((C_word*)t0)[2])[1]);}}

/* k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_fcall f_5314(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,4)))){
C_save_and_reclaim_args((void *)trf_5314,2,t0,t1);}
a=C_alloc(10);
if(C_truep(t1)){
/* csc.scm:742: shared-build */
f_4385(((C_word*)t0)[3],C_SCHEME_FALSE);}
else{
t2=C_eqp(((C_word*)t0)[4],lf[357]);
t3=(C_truep(t2)?t2:C_eqp(((C_word*)t0)[4],lf[358]));
if(C_truep(t3)){
/* csc.scm:744: shared-build */
f_4385(((C_word*)t0)[3],C_SCHEME_TRUE);}
else{
t4=C_eqp(((C_word*)t0)[4],lf[359]);
if(C_truep(t4)){
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5338,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[7],tmp=(C_word)a,a+=5,tmp);
/* csc.scm:746: check */
f_4359(t5,((C_word*)t0)[4],((C_word*)((C_word*)t0)[5])[1],C_SCHEME_END_OF_LIST);}
else{
t5=C_eqp(((C_word*)t0)[4],lf[360]);
if(C_truep(t5)){
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5355,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[7],tmp=(C_word)a,a+=5,tmp);
/* csc.scm:750: check */
f_4359(t6,((C_word*)t0)[4],((C_word*)((C_word*)t0)[5])[1],C_SCHEME_END_OF_LIST);}
else{
t6=C_eqp(((C_word*)t0)[4],lf[361]);
if(C_truep(t6)){
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5372,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[7],tmp=(C_word)a,a+=5,tmp);
/* csc.scm:754: check */
f_4359(t7,((C_word*)t0)[4],((C_word*)((C_word*)t0)[5])[1],C_SCHEME_END_OF_LIST);}
else{
t7=C_eqp(((C_word*)t0)[4],lf[362]);
if(C_truep(t7)){
t8=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5389,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[7],tmp=(C_word)a,a+=5,tmp);
/* csc.scm:758: check */
f_4359(t8,((C_word*)t0)[4],((C_word*)((C_word*)t0)[5])[1],C_SCHEME_END_OF_LIST);}
else{
t8=C_eqp(((C_word*)t0)[4],lf[363]);
if(C_truep(t8)){
t9=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5406,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[7],tmp=(C_word)a,a+=5,tmp);
/* csc.scm:762: check */
f_4359(t9,((C_word*)t0)[4],((C_word*)((C_word*)t0)[5])[1],C_SCHEME_END_OF_LIST);}
else{
t9=C_eqp(((C_word*)t0)[4],lf[365]);
if(C_truep(t9)){
t10=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5427,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[7],tmp=(C_word)a,a+=5,tmp);
/* csc.scm:765: check */
f_4359(t10,((C_word*)t0)[4],((C_word*)((C_word*)t0)[5])[1],C_SCHEME_END_OF_LIST);}
else{
t10=C_eqp(((C_word*)t0)[4],lf[366]);
if(C_truep(t10)){
t11=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5453,a[2]=((C_word*)t0)[6],a[3]=((C_word*)t0)[7],a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp);
t12=C_a_i_list1(&a,1,lf[367]);
/* csc.scm:769: scheme#append */
t13=*((C_word*)lf[137]+1);{
C_word av2[4];
av2[0]=t13;
av2[1]=t11;
av2[2]=C_retrieve2(lf[117],C_text("main#link-options"));
av2[3]=t12;
((C_proc)(void*)(*((C_word*)t13+1)))(4,av2);}}
else{
t11=C_eqp(((C_word*)t0)[4],lf[368]);
if(C_truep(t11)){
t12=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5466,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[7],tmp=(C_word)a,a+=5,tmp);
/* csc.scm:771: check */
f_4359(t12,((C_word*)t0)[4],((C_word*)((C_word*)t0)[5])[1],C_SCHEME_END_OF_LIST);}
else{
t12=C_eqp(((C_word*)t0)[4],lf[369]);
if(C_truep(t12)){
t13=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5491,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[7],tmp=(C_word)a,a+=5,tmp);
/* csc.scm:775: check */
f_4359(t13,((C_word*)t0)[4],((C_word*)((C_word*)t0)[5])[1],C_SCHEME_END_OF_LIST);}
else{
t13=C_eqp(((C_word*)t0)[4],lf[374]);
if(C_truep(t13)){
/* csc.scm:857: loop */
t14=((C_word*)((C_word*)t0)[6])[1];
f_4431(t14,((C_word*)t0)[7],((C_word*)((C_word*)t0)[5])[1]);}
else{
t14=C_eqp(((C_word*)t0)[4],lf[375]);
if(C_truep(t14)){
t15=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5546,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[7],a[5]=((C_word*)t0)[9],tmp=(C_word)a,a+=6,tmp);
/* csc.scm:783: check */
f_4359(t15,((C_word*)t0)[4],((C_word*)((C_word*)t0)[5])[1],C_SCHEME_END_OF_LIST);}
else{
t15=C_eqp(((C_word*)t0)[4],lf[377]);
if(C_truep(t15)){
t16=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5566,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[7],a[5]=((C_word*)t0)[9],tmp=(C_word)a,a+=6,tmp);
/* csc.scm:787: check */
f_4359(t16,((C_word*)t0)[4],((C_word*)((C_word*)t0)[5])[1],C_SCHEME_END_OF_LIST);}
else{
t16=C_eqp(((C_word*)t0)[4],lf[379]);
if(C_truep(t16)){
t17=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5587,a[2]=((C_word*)t0)[6],a[3]=((C_word*)t0)[7],a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp);
/* csc.scm:791: scheme#append */
t18=*((C_word*)lf[137]+1);{
C_word av2[4];
av2[0]=t18;
av2[1]=t17;
av2[2]=C_retrieve2(lf[84],C_text("main#scheme-files"));
av2[3]=lf[381];
((C_proc)(void*)(*((C_word*)t18+1)))(4,av2);}}
else{
t17=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_5597,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[7],a[6]=((C_word*)t0)[9],a[7]=((C_word*)t0)[3],a[8]=((C_word*)t0)[10],a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
t18=C_eqp(((C_word*)t0)[4],lf[413]);
if(C_truep(t18)){
t19=lf[123] /* main#to-stdout */ =C_SCHEME_TRUE;;
t20=lf[121] /* main#translate-only */ =C_SCHEME_TRUE;;
t21=t17;
f_5597(t21,t20);}
else{
t19=t17;
f_5597(t19,C_SCHEME_UNDEFINED);}}}}}}}}}}}}}}}}}

/* k5336 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_ccall f_5338(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5338,c,av);}
t2=C_i_car(((C_word*)((C_word*)t0)[2])[1]);
t3=C_mutate(&lf[56] /* (set! main#translator ...) */,t2);
t4=C_i_cdr(((C_word*)((C_word*)t0)[2])[1]);
t5=C_mutate(((C_word *)((C_word*)t0)[2])+1,t4);
/* csc.scm:857: loop */
t6=((C_word*)((C_word*)t0)[3])[1];
f_4431(t6,((C_word*)t0)[4],((C_word*)((C_word*)t0)[2])[1]);}

/* k5353 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_ccall f_5355(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5355,c,av);}
t2=C_i_car(((C_word*)((C_word*)t0)[2])[1]);
t3=C_mutate(&lf[57] /* (set! main#compiler ...) */,t2);
t4=C_i_cdr(((C_word*)((C_word*)t0)[2])[1]);
t5=C_mutate(((C_word *)((C_word*)t0)[2])+1,t4);
/* csc.scm:857: loop */
t6=((C_word*)((C_word*)t0)[3])[1];
f_4431(t6,((C_word*)t0)[4],((C_word*)((C_word*)t0)[2])[1]);}

/* k5370 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_ccall f_5372(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5372,c,av);}
t2=C_i_car(((C_word*)((C_word*)t0)[2])[1]);
t3=C_mutate(&lf[58] /* (set! main#c++-compiler ...) */,t2);
t4=C_i_cdr(((C_word*)((C_word*)t0)[2])[1]);
t5=C_mutate(((C_word *)((C_word*)t0)[2])+1,t4);
/* csc.scm:857: loop */
t6=((C_word*)((C_word*)t0)[3])[1];
f_4431(t6,((C_word*)t0)[4],((C_word*)((C_word*)t0)[2])[1]);}

/* k5387 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_ccall f_5389(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5389,c,av);}
t2=C_i_car(((C_word*)((C_word*)t0)[2])[1]);
t3=C_mutate(&lf[60] /* (set! main#linker ...) */,t2);
t4=C_i_cdr(((C_word*)((C_word*)t0)[2])[1]);
t5=C_mutate(((C_word *)((C_word*)t0)[2])+1,t4);
/* csc.scm:857: loop */
t6=((C_word*)((C_word*)t0)[3])[1];
f_4431(t6,((C_word*)t0)[4],((C_word*)((C_word*)t0)[2])[1]);}

/* k5404 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_ccall f_5406(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,3)))){
C_save_and_reclaim((void *)f_5406,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5410,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=C_i_car(((C_word*)((C_word*)t0)[2])[1]);
t4=C_i_cdr(((C_word*)((C_word*)t0)[2])[1]);
/* csc.scm:763: cons* */
f_2943(t2,lf[364],C_a_i_list(&a,2,t3,t4));}

/* k5408 in k5404 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in ... */
static void C_ccall f_5410(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5410,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
/* csc.scm:857: loop */
t3=((C_word*)((C_word*)t0)[3])[1];
f_4431(t3,((C_word*)t0)[4],((C_word*)((C_word*)t0)[2])[1]);}

/* k5425 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_ccall f_5427(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_5427,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5431,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5439,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:766: chicken.string#string-split */
t4=C_fast_retrieve(lf[240]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_i_car(((C_word*)((C_word*)t0)[2])[1]);
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k5429 in k5425 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in ... */
static void C_ccall f_5431(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5431,c,av);}
t2=C_mutate(&lf[112] /* (set! main#compile-options ...) */,t1);
t3=C_i_cdr(((C_word*)((C_word*)t0)[2])[1]);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t3);
/* csc.scm:857: loop */
t5=((C_word*)((C_word*)t0)[3])[1];
f_4431(t5,((C_word*)t0)[4],((C_word*)((C_word*)t0)[2])[1]);}

/* k5437 in k5425 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in ... */
static void C_ccall f_5439(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5439,c,av);}
/* csc.scm:766: scheme#append */
t2=*((C_word*)lf[137]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_retrieve2(lf[112],C_text("main#compile-options"));
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k5451 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_ccall f_5453(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5453,c,av);}
t2=C_mutate(&lf[117] /* (set! main#link-options ...) */,t1);
/* csc.scm:857: loop */
t3=((C_word*)((C_word*)t0)[2])[1];
f_4431(t3,((C_word*)t0)[3],((C_word*)((C_word*)t0)[4])[1]);}

/* k5464 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_ccall f_5466(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_5466,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5470,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5478,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:772: chicken.string#string-split */
t4=C_fast_retrieve(lf[240]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_i_car(((C_word*)((C_word*)t0)[2])[1]);
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k5468 in k5464 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in ... */
static void C_ccall f_5470(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5470,c,av);}
t2=C_mutate(&lf[117] /* (set! main#link-options ...) */,t1);
t3=C_i_cdr(((C_word*)((C_word*)t0)[2])[1]);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t3);
/* csc.scm:857: loop */
t5=((C_word*)((C_word*)t0)[3])[1];
f_4431(t5,((C_word*)t0)[4],((C_word*)((C_word*)t0)[2])[1]);}

/* k5476 in k5464 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in ... */
static void C_ccall f_5478(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5478,c,av);}
/* csc.scm:772: scheme#append */
t2=*((C_word*)lf[137]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_retrieve2(lf[117],C_text("main#link-options"));
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k5489 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_ccall f_5491(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_5491,c,av);}
a=C_alloc(8);
t2=C_i_car(((C_word*)((C_word*)t0)[2])[1]);
t3=C_mutate(&lf[103] /* (set! main#rpath ...) */,t2);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5505,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5521,a[2]=t4,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:777: chicken.platform#build-platform */
t6=C_fast_retrieve(lf[373]);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}

/* k5503 in k5489 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in ... */
static void C_fcall f_5505(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,3)))){
C_save_and_reclaim_args((void *)trf_5505,2,t0,t1);}
a=C_alloc(8);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5509,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5517,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:779: scheme#string-append */
t4=*((C_word*)lf[76]+1);{
C_word av2[4];
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[370];
av2[3]=C_retrieve2(lf[103],C_text("main#rpath"));
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t2=C_i_cdr(((C_word*)((C_word*)t0)[2])[1]);
t3=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
/* csc.scm:857: loop */
t4=((C_word*)((C_word*)t0)[3])[1];
f_4431(t4,((C_word*)t0)[4],((C_word*)((C_word*)t0)[2])[1]);}}

/* k5507 in k5503 in k5489 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in ... */
static void C_ccall f_5509(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5509,c,av);}
t2=C_mutate(&lf[117] /* (set! main#link-options ...) */,t1);
t3=C_i_cdr(((C_word*)((C_word*)t0)[2])[1]);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t3);
/* csc.scm:857: loop */
t5=((C_word*)((C_word*)t0)[3])[1];
f_4431(t5,((C_word*)t0)[4],((C_word*)((C_word*)t0)[2])[1]);}

/* k5515 in k5503 in k5489 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in ... */
static void C_ccall f_5517(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_5517,c,av);}
a=C_alloc(3);
t2=C_a_i_list1(&a,1,t1);
/* csc.scm:779: scheme#append */
t3=*((C_word*)lf[137]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=C_retrieve2(lf[117],C_text("main#link-options"));
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k5519 in k5489 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in ... */
static void C_ccall f_5521(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5521,c,av);}
if(C_truep((C_truep(C_eqp(t1,lf[371]))?C_SCHEME_TRUE:(C_truep(C_eqp(t1,lf[372]))?C_SCHEME_TRUE:C_SCHEME_FALSE)))){
t2=C_i_not(C_retrieve2(lf[29],C_text("main#mingw")));
t3=((C_word*)t0)[2];
f_5505(t3,(C_truep(t2)?C_i_not(C_retrieve2(lf[31],C_text("main#osx"))):C_SCHEME_FALSE));}
else{
t2=((C_word*)t0)[2];
f_5505(t2,C_SCHEME_FALSE);}}

/* k5544 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_ccall f_5546(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_5546,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5549,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=C_i_car(((C_word*)((C_word*)t0)[2])[1]);
/* csc.scm:784: t-options */
f_4352(t2,C_a_i_list(&a,2,lf[376],t3));}

/* k5547 in k5544 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in ... */
static void C_ccall f_5549(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5549,c,av);}
t2=C_i_cdr(((C_word*)((C_word*)t0)[2])[1]);
t3=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
/* csc.scm:857: loop */
t4=((C_word*)((C_word*)t0)[3])[1];
f_4431(t4,((C_word*)t0)[4],((C_word*)((C_word*)t0)[2])[1]);}

/* k5564 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_ccall f_5566(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_5566,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5569,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=C_i_car(((C_word*)((C_word*)t0)[2])[1]);
/* csc.scm:788: t-options */
f_4352(t2,C_a_i_list(&a,2,lf[378],t3));}

/* k5567 in k5564 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in ... */
static void C_ccall f_5569(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5569,c,av);}
t2=C_i_cdr(((C_word*)((C_word*)t0)[2])[1]);
t3=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
/* csc.scm:857: loop */
t4=((C_word*)((C_word*)t0)[3])[1];
f_4431(t4,((C_word*)t0)[4],((C_word*)((C_word*)t0)[2])[1]);}

/* k5585 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_ccall f_5587(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_5587,c,av);}
a=C_alloc(5);
t2=C_mutate(&lf[84] /* (set! main#scheme-files ...) */,t1);
if(C_truep(C_retrieve2(lf[118],C_text("main#target-filename")))){
/* csc.scm:857: loop */
t3=((C_word*)((C_word*)t0)[2])[1];
f_4431(t3,((C_word*)t0)[3],((C_word*)((C_word*)t0)[4])[1]);}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5594,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* csc.scm:793: chicken.pathname#make-pathname */
t4=C_fast_retrieve(lf[132]);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_SCHEME_FALSE;
av2[3]=lf[380];
av2[4]=C_retrieve2(lf[67],C_text("main#executable-extension"));
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}}

/* k5592 in k5585 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in ... */
static void C_ccall f_5594(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5594,c,av);}
t2=C_mutate(&lf[118] /* (set! main#target-filename ...) */,t1);
/* csc.scm:857: loop */
t3=((C_word*)((C_word*)t0)[2])[1];
f_4431(t3,((C_word*)t0)[3],((C_word*)((C_word*)t0)[4])[1]);}

/* k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_fcall f_5597(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,2)))){
C_save_and_reclaim_args((void *)trf_5597,2,t0,t1);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_5600,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],tmp=(C_word)a,a+=10,tmp);
if(C_truep((C_truep(C_eqp(((C_word*)t0)[2],lf[411]))?C_SCHEME_TRUE:(C_truep(C_eqp(((C_word*)t0)[2],lf[412]))?C_SCHEME_TRUE:C_SCHEME_FALSE)))){
t3=C_mutate(&lf[115] /* (set! main#compilation-optimization-options ...) */,C_retrieve2(lf[79],C_text("main#best-compilation-optimization-options")));
t4=C_mutate(&lf[116] /* (set! main#linking-optimization-options ...) */,C_retrieve2(lf[81],C_text("main#best-linking-optimization-options")));
t5=t2;
f_5600(t5,t4);}
else{
t3=t2;
f_5600(t3,C_SCHEME_UNDEFINED);}}

/* k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in ... */
static void C_fcall f_5600(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,4)))){
C_save_and_reclaim_args((void *)trf_5600,2,t0,t1);}
a=C_alloc(12);
t2=C_i_assq(((C_word*)t0)[2],lf[382]);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5607,a[2]=((C_word*)t0)[3],tmp=(C_word)a,a+=3,tmp);
t4=(
/* csc.scm:801: g1220 */
  f_5607(C_a_i(&a,3),t3,t2)
);
/* csc.scm:857: loop */
t5=((C_word*)((C_word*)t0)[4])[1];
f_4431(t5,((C_word*)t0)[5],((C_word*)((C_word*)t0)[3])[1]);}
else{
if(C_truep(C_i_memq(((C_word*)t0)[2],lf[383]))){
/* csc.scm:802: t-options */
f_4352(((C_word*)t0)[7],C_a_i_list(&a,1,((C_word*)t0)[8]));}
else{
if(C_truep(C_i_memq(((C_word*)t0)[2],lf[384]))){
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5637,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[8],tmp=(C_word)a,a+=7,tmp);
/* csc.scm:804: check */
f_4359(t3,((C_word*)t0)[2],((C_word*)((C_word*)t0)[3])[1],C_SCHEME_END_OF_LIST);}
else{
t3=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_5654,a[2]=((C_word*)t0)[6],a[3]=((C_word*)t0)[7],a[4]=((C_word*)t0)[8],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[3],a[8]=((C_word*)t0)[2],tmp=(C_word)a,a+=9,tmp);
t4=C_block_size(((C_word*)t0)[8]);
if(C_truep(C_fixnum_greaterp(t4,C_fix(2)))){
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6029,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:807: scheme#substring */
t6=*((C_word*)lf[389]+1);{
C_word av2[5];
av2[0]=t6;
av2[1]=t5;
av2[2]=((C_word*)t0)[8];
av2[3]=C_fix(0);
av2[4]=C_fix(2);
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}
else{
t5=t3;
f_5654(t5,C_SCHEME_FALSE);}}}}}

/* g1220 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in ... */
static C_word C_fcall f_5607(C_word *a,C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_stack_overflow_check;{}
t2=C_i_cadr(t1);
t3=C_a_i_cons(&a,2,t2,((C_word*)((C_word*)t0)[2])[1]);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t3);
return(t4);}

/* k5635 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in ... */
static void C_ccall f_5637(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_5637,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5640,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=C_i_car(((C_word*)((C_word*)t0)[2])[1]);
/* csc.scm:805: t-options */
f_4352(t2,C_a_i_list(&a,2,((C_word*)t0)[6],t3));}

/* k5638 in k5635 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in ... */
static void C_ccall f_5640(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5640,c,av);}
t2=C_i_cdr(((C_word*)((C_word*)t0)[2])[1]);
t3=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
/* csc.scm:857: loop */
t4=((C_word*)((C_word*)t0)[3])[1];
f_4431(t4,((C_word*)t0)[4],((C_word*)((C_word*)t0)[2])[1]);}

/* k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in ... */
static void C_fcall f_5654(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,0,4)))){
C_save_and_reclaim_args((void *)trf_5654,2,t0,t1);}
a=C_alloc(11);
if(C_truep(t1)){
/* csc.scm:808: t-options */
f_4352(((C_word*)t0)[3],C_a_i_list(&a,1,((C_word*)t0)[4]));}
else{
t2=C_block_size(((C_word*)t0)[4]);
t3=C_fixnum_greaterp(t2,C_fix(1));
t4=(C_truep(t3)?C_u_i_char_equalp(C_make_character(45),C_i_string_ref(((C_word*)t0)[4],C_fix(0))):C_SCHEME_FALSE);
if(C_truep(t4)){
t5=C_i_string_ref(((C_word*)t0)[4],C_fix(1));
if(C_truep(C_u_i_char_equalp(C_make_character(76),t5))){
t6=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5673,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[7],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_u_i_char_whitespacep(C_i_string_ref(((C_word*)t0)[4],C_fix(2))))){
/* csc.scm:813: chicken.base#error */
t7=*((C_word*)lf[385]+1);{
C_word av2[4];
av2[0]=t7;
av2[1]=t6;
av2[2]=lf[386];
av2[3]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}
else{
t7=t6;{
C_word av2[2];
av2[0]=t7;
av2[1]=C_SCHEME_UNDEFINED;
f_5673(2,av2);}}}
else{
t6=C_i_string_ref(((C_word*)t0)[4],C_fix(1));
if(C_truep(C_u_i_char_equalp(C_make_character(73),t6))){
t7=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5700,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[7],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_u_i_char_whitespacep(C_i_string_ref(((C_word*)t0)[4],C_fix(2))))){
/* csc.scm:817: chicken.base#error */
t8=*((C_word*)lf[385]+1);{
C_word av2[4];
av2[0]=t8;
av2[1]=t7;
av2[2]=lf[387];
av2[3]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t8+1)))(4,av2);}}
else{
t8=t7;{
C_word av2[2];
av2[0]=t8;
av2[1]=C_SCHEME_UNDEFINED;
f_5700(2,av2);}}}
else{
t7=C_i_string_ref(((C_word*)t0)[4],C_fix(1));
if(C_truep(C_u_i_char_equalp(C_make_character(68),t7))){
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5731,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* csc.scm:820: scheme#substring */
t9=*((C_word*)lf[389]+1);{
C_word av2[4];
av2[0]=t9;
av2[1]=t8;
av2[2]=((C_word*)t0)[4];
av2[3]=C_fix(2);
((C_proc)(void*)(*((C_word*)t9+1)))(4,av2);}}
else{
t8=C_i_string_ref(((C_word*)t0)[4],C_fix(1));
if(C_truep(C_u_i_char_equalp(C_make_character(70),t8))){
if(C_truep(C_retrieve2(lf[31],C_text("main#osx")))){
t9=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5745,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[7],tmp=(C_word)a,a+=5,tmp);
t10=C_a_i_list1(&a,1,((C_word*)t0)[4]);
/* csc.scm:823: scheme#append */
t11=*((C_word*)lf[137]+1);{
C_word av2[4];
av2[0]=t11;
av2[1]=t9;
av2[2]=C_retrieve2(lf[112],C_text("main#compile-options"));
av2[3]=t10;
((C_proc)(void*)(*((C_word*)t11+1)))(4,av2);}}
else{
/* csc.scm:857: loop */
t9=((C_word*)((C_word*)t0)[5])[1];
f_4431(t9,((C_word*)t0)[6],((C_word*)((C_word*)t0)[7])[1]);}}
else{
t9=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_5755,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[7],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[3],a[7]=((C_word*)t0)[8],tmp=(C_word)a,a+=8,tmp);
t10=C_block_size(((C_word*)t0)[4]);
if(C_truep(C_fixnum_greaterp(t10,C_fix(3)))){
t11=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5861,a[2]=t9,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:824: scheme#substring */
t12=*((C_word*)lf[389]+1);{
C_word av2[5];
av2[0]=t12;
av2[1]=t11;
av2[2]=((C_word*)t0)[4];
av2[3]=C_fix(0);
av2[4]=C_fix(4);
((C_proc)(void*)(*((C_word*)t12+1)))(5,av2);}}
else{
t11=t9;
f_5755(t11,C_SCHEME_FALSE);}}}}}}
else{
t5=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5873,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[7],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* csc.scm:833: chicken.file#file-exists? */
t6=C_fast_retrieve(lf[131]);{
C_word av2[3];
av2[0]=t6;
av2[1]=t5;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}}}

/* k5671 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in ... */
static void C_ccall f_5673(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_5673,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5677,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=C_a_i_list1(&a,1,((C_word*)t0)[5]);
/* csc.scm:814: scheme#append */
t4=*((C_word*)lf[137]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=C_retrieve2(lf[117],C_text("main#link-options"));
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k5675 in k5671 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in ... */
static void C_ccall f_5677(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5677,c,av);}
t2=C_mutate(&lf[117] /* (set! main#link-options ...) */,t1);
/* csc.scm:857: loop */
t3=((C_word*)((C_word*)t0)[2])[1];
f_4431(t3,((C_word*)t0)[3],((C_word*)((C_word*)t0)[4])[1]);}

/* k5698 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in ... */
static void C_ccall f_5700(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_5700,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5704,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=C_a_i_list1(&a,1,((C_word*)t0)[5]);
/* csc.scm:818: scheme#append */
t4=*((C_word*)lf[137]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=C_retrieve2(lf[112],C_text("main#compile-options"));
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k5702 in k5698 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in ... */
static void C_ccall f_5704(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5704,c,av);}
t2=C_mutate(&lf[112] /* (set! main#compile-options ...) */,t1);
/* csc.scm:857: loop */
t3=((C_word*)((C_word*)t0)[2])[1];
f_4431(t3,((C_word*)t0)[3],((C_word*)((C_word*)t0)[4])[1]);}

/* k5729 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in ... */
static void C_ccall f_5731(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_5731,c,av);}
a=C_alloc(6);
/* csc.scm:820: t-options */
f_4352(((C_word*)t0)[3],C_a_i_list(&a,2,lf[388],t1));}

/* k5743 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in ... */
static void C_ccall f_5745(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5745,c,av);}
t2=C_mutate(&lf[112] /* (set! main#compile-options ...) */,t1);
/* csc.scm:857: loop */
t3=((C_word*)((C_word*)t0)[2])[1];
f_4431(t3,((C_word*)t0)[3],((C_word*)((C_word*)t0)[4])[1]);}

/* k5753 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in ... */
static void C_fcall f_5755(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,3)))){
C_save_and_reclaim_args((void *)trf_5755,2,t0,t1);}
a=C_alloc(8);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5759,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=C_a_i_list1(&a,1,((C_word*)t0)[5]);
/* csc.scm:825: scheme#append */
t4=*((C_word*)lf[137]+1);{
C_word av2[4];
av2[0]=t4;
av2[1]=t2;
av2[2]=C_retrieve2(lf[117],C_text("main#link-options"));
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t2=C_block_size(((C_word*)t0)[5]);
if(C_truep(C_fixnum_greaterp(t2,C_fix(2)))){
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5844,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* ##sys#string->list */
t4=C_fast_retrieve(lf[149]);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
/* csc.scm:832: stop */
f_3819(((C_word*)t0)[6],lf[393],C_a_i_list(&a,1,((C_word*)t0)[7]));}}}

/* k5757 in k5753 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in ... */
static void C_ccall f_5759(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5759,c,av);}
t2=C_mutate(&lf[117] /* (set! main#link-options ...) */,t1);
/* csc.scm:857: loop */
t3=((C_word*)((C_word*)t0)[2])[1];
f_4431(t3,((C_word*)t0)[3],((C_word*)((C_word*)t0)[4])[1]);}

/* k5780 in k5838 in k5842 in k5753 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in ... */
static void C_ccall f_5782(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5782,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
/* csc.scm:857: loop */
t3=((C_word*)((C_word*)t0)[3])[1];
f_4431(t3,((C_word*)t0)[4],((C_word*)((C_word*)t0)[2])[1]);}

/* k5797 in k5838 in k5842 in k5753 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in ... */
static void C_ccall f_5799(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5799,c,av);}
/* csc.scm:830: scheme#append */
t2=*((C_word*)lf[137]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)((C_word*)t0)[3])[1];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* map-loop1237 in k5838 in k5842 in k5753 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in ... */
static void C_fcall f_5801(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,3)))){
C_save_and_reclaim_args((void *)trf_5801,3,t0,t1,t2);}
a=C_alloc(8);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5826,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
t4=C_slot(t2,C_fix(0));
t5=C_a_i_string(&a,1,t4);
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[186]);
C_word av2[4];
av2[0]=*((C_word*)lf[186]+1);
av2[1]=t3;
av2[2]=lf[390];
av2[3]=t5;
tp(4,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k5824 in map-loop1237 in k5838 in k5842 in k5753 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in ... */
static void C_ccall f_5826(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_5826,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_5801(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k5838 in k5842 in k5753 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in ... */
static void C_ccall f_5840(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(21,c,3)))){
C_save_and_reclaim((void *)f_5840,c,av);}
a=C_alloc(21);
if(C_truep(C_i_nullp(t1))){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5782,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5799,a[2]=t2,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5801,a[2]=t5,a[3]=t9,a[4]=t6,tmp=(C_word)a,a+=5,tmp));
t11=((C_word*)t9)[1];
f_5801(t11,t7,((C_word*)t0)[5]);}
else{
/* csc.scm:831: stop */
f_3819(((C_word*)t0)[6],lf[391],C_a_i_list(&a,1,((C_word*)t0)[7]));}}

/* k5842 in k5753 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in ... */
static void C_ccall f_5844(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,4)))){
C_save_and_reclaim((void *)f_5844,c,av);}
a=C_alloc(16);
t2=C_i_cdr(t1);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_5840,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t2,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
t4=C_retrieve2(lf[83],C_text("main#short-options"));
t5=C_a_i_list(&a,1,C_retrieve2(lf[83],C_text("main#short-options")));
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3385,a[2]=t7,tmp=(C_word)a,a+=3,tmp));
t9=((C_word*)t7)[1];
f_3385(t9,t3,t5,t2);}

/* k5859 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in ... */
static void C_ccall f_5861(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5861,c,av);}
t2=((C_word*)t0)[2];
f_5755(t2,C_u_i_string_equal_p(lf[394],t1));}

/* k5871 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in ... */
static void C_ccall f_5873(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,5)))){
C_save_and_reclaim((void *)f_5873,c,av);}
a=C_alloc(7);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5878,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5884,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csc.scm:834: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[3];
av2[2]=t2;
av2[3]=t3;
C_call_with_values(4,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5993,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[2],tmp=(C_word)a,a+=7,tmp);
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[186]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[186]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[2];
av2[3]=lf[409];
tp(4,av2);}}}

/* a5877 in k5871 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in ... */
static void C_ccall f_5878(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5878,c,av);}
/* csc.scm:834: chicken.pathname#decompose-pathname */
t2=C_fast_retrieve(lf[395]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* a5883 in k5871 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in ... */
static void C_ccall f_5884(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_5884,c,av);}
a=C_alloc(7);
if(C_truep(C_i_not(t4))){
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5895,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t6=C_a_i_list1(&a,1,((C_word*)t0)[2]);
/* csc.scm:836: scheme#append */
t7=*((C_word*)lf[137]+1);{
C_word *av2=av;
av2[0]=t7;
av2[1]=t5;
av2[2]=C_retrieve2(lf[84],C_text("main#scheme-files"));
av2[3]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}
else{
if(C_truep((C_truep(C_i_equalp(t4,lf[396]))?C_SCHEME_TRUE:(C_truep(C_i_equalp(t4,lf[397]))?C_SCHEME_TRUE:C_SCHEME_FALSE)))){
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5909,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t6=C_a_i_list1(&a,1,((C_word*)t0)[2]);
/* csc.scm:838: scheme#append */
t7=*((C_word*)lf[137]+1);{
C_word *av2=av;
av2[0]=t7;
av2[1]=t5;
av2[2]=C_retrieve2(lf[85],C_text("main#c-files"));
av2[3]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}
else{
if(C_truep(C_i_string_ci_equal_p(t4,lf[398]))){
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5923,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t6=C_a_i_list1(&a,1,((C_word*)t0)[2]);
/* csc.scm:840: scheme#append */
t7=*((C_word*)lf[137]+1);{
C_word *av2=av;
av2[0]=t7;
av2[1]=t5;
av2[2]=C_retrieve2(lf[86],C_text("main#rc-files"));
av2[3]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}
else{
if(C_truep((C_truep(C_i_equalp(t4,lf[399]))?C_SCHEME_TRUE:(C_truep(C_i_equalp(t4,lf[400]))?C_SCHEME_TRUE:(C_truep(C_i_equalp(t4,lf[401]))?C_SCHEME_TRUE:(C_truep(C_i_equalp(t4,lf[402]))?C_SCHEME_TRUE:(C_truep(C_i_equalp(t4,lf[403]))?C_SCHEME_TRUE:C_SCHEME_FALSE))))))){
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5936,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_retrieve2(lf[31],C_text("main#osx")))){
t6=C_a_i_cons(&a,2,lf[404],C_retrieve2(lf[112],C_text("main#compile-options")));
t7=C_mutate(&lf[112] /* (set! main#compile-options ...) */,t6);
t8=t5;
f_5936(t8,t7);}
else{
t6=t5;
f_5936(t6,C_SCHEME_UNDEFINED);}}
else{
if(C_truep((C_truep(C_i_equalp(t4,lf[405]))?C_SCHEME_TRUE:(C_truep(C_i_equalp(t4,lf[406]))?C_SCHEME_TRUE:(C_truep(C_i_equalp(t4,lf[407]))?C_SCHEME_TRUE:C_SCHEME_FALSE))))){
t5=lf[94] /* main#objc-mode */ =C_SCHEME_TRUE;;
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5960,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t7=C_a_i_list1(&a,1,((C_word*)t0)[2]);
/* csc.scm:847: scheme#append */
t8=*((C_word*)lf[137]+1);{
C_word *av2=av;
av2[0]=t8;
av2[1]=t6;
av2[2]=C_retrieve2(lf[85],C_text("main#c-files"));
av2[3]=t7;
((C_proc)(void*)(*((C_word*)t8+1)))(4,av2);}}
else{
t5=C_retrieve2(lf[64],C_text("main#object-extension"));
t6=C_u_i_string_equal_p(t4,C_retrieve2(lf[64],C_text("main#object-extension")));
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5972,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
if(C_truep(t6)){
t8=t7;
f_5972(t8,t6);}
else{
t8=C_retrieve2(lf[65],C_text("main#library-extension"));
t9=t7;
f_5972(t9,C_u_i_string_equal_p(t4,C_retrieve2(lf[65],C_text("main#library-extension"))));}}}}}}}

/* k5893 in a5883 in k5871 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in ... */
static void C_ccall f_5895(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5895,c,av);}
t2=C_mutate(&lf[84] /* (set! main#scheme-files ...) */,t1);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k5907 in a5883 in k5871 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in ... */
static void C_ccall f_5909(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5909,c,av);}
t2=C_mutate(&lf[85] /* (set! main#c-files ...) */,t1);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k5921 in a5883 in k5871 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in ... */
static void C_ccall f_5923(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5923,c,av);}
t2=C_mutate(&lf[86] /* (set! main#rc-files ...) */,t1);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k5934 in a5883 in k5871 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in ... */
static void C_fcall f_5936(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_5936,2,t0,t1);}
a=C_alloc(6);
t2=lf[93] /* main#cpp-mode */ =C_SCHEME_TRUE;;
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5941,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=C_a_i_list1(&a,1,((C_word*)t0)[3]);
/* csc.scm:844: scheme#append */
t5=*((C_word*)lf[137]+1);{
C_word av2[4];
av2[0]=t5;
av2[1]=t3;
av2[2]=C_retrieve2(lf[85],C_text("main#c-files"));
av2[3]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k5939 in k5934 in a5883 in k5871 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in ... */
static void C_ccall f_5941(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5941,c,av);}
t2=C_mutate(&lf[85] /* (set! main#c-files ...) */,t1);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k5958 in a5883 in k5871 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in ... */
static void C_ccall f_5960(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5960,c,av);}
t2=C_mutate(&lf[85] /* (set! main#c-files ...) */,t1);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k5970 in a5883 in k5871 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in ... */
static void C_fcall f_5972(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_5972,2,t0,t1);}
a=C_alloc(6);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5976,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=C_a_i_list1(&a,1,((C_word*)t0)[3]);
/* csc.scm:850: scheme#append */
t4=*((C_word*)lf[137]+1);{
C_word av2[4];
av2[0]=t4;
av2[1]=t2;
av2[2]=C_retrieve2(lf[89],C_text("main#object-files"));
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5984,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=C_a_i_list1(&a,1,((C_word*)t0)[3]);
/* csc.scm:851: scheme#append */
t4=*((C_word*)lf[137]+1);{
C_word av2[4];
av2[0]=t4;
av2[1]=t2;
av2[2]=C_retrieve2(lf[84],C_text("main#scheme-files"));
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}}

/* k5974 in k5970 in a5883 in k5871 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in ... */
static void C_ccall f_5976(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5976,c,av);}
t2=C_mutate(&lf[89] /* (set! main#object-files ...) */,t1);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k5982 in k5970 in a5883 in k5871 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in ... */
static void C_ccall f_5984(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5984,c,av);}
t2=C_mutate(&lf[84] /* (set! main#scheme-files ...) */,t1);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k5991 in k5871 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in ... */
static void C_ccall f_5993(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_5993,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_5999,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* csc.scm:854: chicken.file#file-exists? */
t3=C_fast_retrieve(lf[131]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k5997 in k5991 in k5871 in k5652 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in ... */
static void C_ccall f_5999(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_5999,c,av);}
a=C_alloc(3);
if(C_truep(t1)){
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],((C_word*)((C_word*)t0)[3])[1]);
t3=C_mutate(((C_word *)((C_word*)t0)[3])+1,t2);
/* csc.scm:857: loop */
t4=((C_word*)((C_word*)t0)[4])[1];
f_4431(t4,((C_word*)t0)[5],((C_word*)((C_word*)t0)[3])[1]);}
else{
/* csc.scm:856: stop */
f_3819(((C_word*)t0)[6],lf[408],C_a_i_list(&a,1,((C_word*)t0)[7]));}}

/* k6027 in k5598 in k5595 in k5312 in k4696 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in ... */
static void C_ccall f_6029(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6029,c,av);}
t2=((C_word*)t0)[2];
f_5654(t2,C_u_i_string_equal_p(lf[410],t1));}

/* k6234 in for-each-loop1297 in k4612 in k4609 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in ... */
static void C_ccall f_6236(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,3)))){
C_save_and_reclaim((void *)f_6236,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6239,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
if(C_truep(C_i_member(t1,C_retrieve2(lf[85],C_text("main#c-files"))))){
/* csc.scm:874: stop */
f_3819(t2,lf[231],C_a_i_list(&a,2,((C_word*)t0)[5],t1));}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_6239(2,av2);}}}

/* k6237 in k6234 in for-each-loop1297 in k4612 in k4609 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in ... */
static void C_ccall f_6239(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(17,c,2)))){
C_save_and_reclaim((void *)f_6239,c,av);}
a=C_alloc(17);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6242,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6276,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6280,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6284,a[2]=t4,a[3]=((C_word*)t0)[5],tmp=(C_word)a,a+=4,tmp);
/* csc.scm:878: quotewrap */
t6=C_retrieve2(lf[50],C_text("main#quotewrap"));{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=((C_word*)t0)[6];
f_3868(3,av2);}}

/* k6240 in k6237 in k6234 in for-each-loop1297 in k4612 in k4609 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in ... */
static void C_ccall f_6242(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_6242,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6245,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t3=(C_truep(C_retrieve2(lf[125],C_text("main#static")))?C_retrieve2(lf[122],C_text("main#compile-only")):C_SCHEME_FALSE);
if(C_truep(t3)){
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6272,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:903: chicken.pathname#pathname-replace-extension */
t5=C_fast_retrieve(lf[175]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[6];
av2[3]=lf[221];
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}
else{
t4=t2;
f_6245(t4,C_SCHEME_UNDEFINED);}}

/* k6243 in k6240 in k6237 in k6234 in for-each-loop1297 in k4612 in k4609 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in ... */
static void C_fcall f_6245(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,3)))){
C_save_and_reclaim_args((void *)trf_6245,2,t0,t1);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6249,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t3=C_a_i_list1(&a,1,((C_word*)t0)[5]);
/* csc.scm:905: scheme#append */
t4=*((C_word*)lf[137]+1);{
C_word av2[4];
av2[0]=t4;
av2[1]=t2;
av2[2]=t3;
av2[3]=C_retrieve2(lf[85],C_text("main#c-files"));
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k6247 in k6243 in k6240 in k6237 in k6234 in for-each-loop1297 in k4612 in k4609 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in ... */
static void C_ccall f_6249(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_6249,c,av);}
a=C_alloc(8);
t2=C_mutate(&lf[85] /* (set! main#c-files ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6253,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t4=C_a_i_list1(&a,1,((C_word*)t0)[5]);
/* csc.scm:906: scheme#append */
t5=*((C_word*)lf[137]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
av2[3]=C_retrieve2(lf[87],C_text("main#generated-c-files"));
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k6251 in k6247 in k6243 in k6240 in k6237 in k6234 in for-each-loop1297 in k4612 in k4609 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in ... */
static void C_ccall f_6253(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6253,c,av);}
t2=C_mutate(&lf[87] /* (set! main#generated-c-files ...) */,t1);
t3=((C_word*)((C_word*)t0)[2])[1];
f_6407(t3,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k6270 in k6240 in k6237 in k6234 in for-each-loop1297 in k4612 in k4609 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in ... */
static void C_ccall f_6272(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_6272,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_retrieve2(lf[91],C_text("main#transient-link-files")));
t3=C_mutate(&lf[91] /* (set! main#transient-link-files ...) */,t2);
t4=((C_word*)t0)[2];
f_6245(t4,t3);}

/* k6274 in k6237 in k6234 in for-each-loop1297 in k4612 in k4609 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in ... */
static void C_ccall f_6276(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6276,c,av);}
/* csc.scm:876: command */
f_7488(((C_word*)t0)[2],t1);}

/* k6278 in k6237 in k6234 in for-each-loop1297 in k4612 in k4609 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in ... */
static void C_ccall f_6280(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6280,c,av);}
/* csc.scm:877: chicken.string#string-intersperse */
t2=C_fast_retrieve(lf[136]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=lf[222];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k6282 in k6237 in k6234 in for-each-loop1297 in k4612 in k4609 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in ... */
static void C_ccall f_6284(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_6284,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6288,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6292,a[2]=t2,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_retrieve2(lf[123],C_text("main#to-stdout")))){
t4=t3;
f_6292(t4,lf[229]);}
else{
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6378,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:882: quotewrap */
t5=C_retrieve2(lf[50],C_text("main#quotewrap"));{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[3];
f_3868(3,av2);}}}

/* k6286 in k6282 in k6237 in k6234 in for-each-loop1297 in k4612 in k4609 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in ... */
static void C_ccall f_6288(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_6288,c,av);}
a=C_alloc(6);
/* csc.scm:878: cons* */
f_2943(((C_word*)t0)[2],C_retrieve2(lf[56],C_text("main#translator")),C_a_i_list(&a,2,((C_word*)t0)[3],t1));}

/* k6290 in k6282 in k6237 in k6234 in for-each-loop1297 in k4612 in k4609 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in ... */
static void C_fcall f_6292(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(18,0,3)))){
C_save_and_reclaim_args((void *)trf_6292,2,t0,t1);}
a=C_alloc(18);
t2=(C_truep(C_i_debug_modep())?lf[223]:C_SCHEME_END_OF_LIST);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=C_retrieve2(lf[134],C_text("main#quote-option"));
t8=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6303,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,a[5]=t5,a[6]=t6,tmp=(C_word)a,a+=7,tmp);
t9=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6347,a[2]=t8,tmp=(C_word)a,a+=3,tmp);
t10=(C_truep(C_retrieve2(lf[125],C_text("main#static")))?C_i_not(C_i_member(lf[226],C_retrieve2(lf[109],C_text("main#translate-options")))):C_SCHEME_FALSE);
if(C_truep(t10)){
t11=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6364,a[2]=t9,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:894: chicken.pathname#pathname-replace-extension */
t12=C_fast_retrieve(lf[175]);{
C_word av2[4];
av2[0]=t12;
av2[1]=t11;
av2[2]=((C_word*)t0)[3];
av2[3]=lf[228];
((C_proc)(void*)(*((C_word*)t12+1)))(4,av2);}}
else{
t11=t9;
f_6347(t11,C_SCHEME_END_OF_LIST);}}

/* k6301 in k6290 in k6282 in k6237 in k6234 in for-each-loop1297 in k4612 in k4609 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in ... */
static void C_ccall f_6303(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_6303,c,av);}
a=C_alloc(12);
t2=C_i_check_list_2(t1,lf[135]);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6309,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6311,a[2]=((C_word*)t0)[5],a[3]=t5,a[4]=((C_word*)t0)[6],tmp=(C_word)a,a+=5,tmp));
t7=((C_word*)t5)[1];
f_6311(t7,t3,t1);}

/* k6307 in k6301 in k6290 in k6282 in k6237 in k6234 in for-each-loop1297 in k4612 in k4609 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in ... */
static void C_ccall f_6309(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6309,c,av);}
/* csc.scm:879: scheme#append */
t2=*((C_word*)lf[137]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* map-loop1318 in k6301 in k6290 in k6282 in k6237 in k6234 in for-each-loop1297 in k4612 in k4609 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in ... */
static void C_fcall f_6311(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_6311,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6336,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* csc.scm:886: g1324 */
t4=C_retrieve2(lf[134],C_text("main#quote-option"));{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
f_7376(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k6334 in map-loop1318 in k6301 in k6290 in k6282 in k6237 in k6234 in for-each-loop1297 in k4612 in k4609 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in ... */
static void C_ccall f_6336(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_6336,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_6311(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k6345 in k6290 in k6282 in k6237 in k6234 in for-each-loop1297 in k4612 in k4609 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in ... */
static void C_fcall f_6347(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,6)))){
C_save_and_reclaim_args((void *)trf_6347,2,t0,t1);}
if(C_truep(C_retrieve2(lf[93],C_text("main#cpp-mode")))){
/* csc.scm:887: scheme#append */
t2=*((C_word*)lf[137]+1);{
C_word av2[7];
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_retrieve2(lf[82],C_text("main#extra-features"));
av2[3]=C_retrieve2(lf[109],C_text("main#translate-options"));
av2[4]=t1;
av2[5]=lf[224];
av2[6]=C_retrieve2(lf[114],C_text("main#translation-optimization-options"));
((C_proc)(void*)(*((C_word*)t2+1)))(7,av2);}}
else{
if(C_truep(C_retrieve2(lf[94],C_text("main#objc-mode")))){
/* csc.scm:887: scheme#append */
t2=*((C_word*)lf[137]+1);{
C_word av2[7];
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_retrieve2(lf[82],C_text("main#extra-features"));
av2[3]=C_retrieve2(lf[109],C_text("main#translate-options"));
av2[4]=t1;
av2[5]=lf[225];
av2[6]=C_retrieve2(lf[114],C_text("main#translation-optimization-options"));
((C_proc)(void*)(*((C_word*)t2+1)))(7,av2);}}
else{
/* csc.scm:887: scheme#append */
t2=*((C_word*)lf[137]+1);{
C_word av2[7];
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_retrieve2(lf[82],C_text("main#extra-features"));
av2[3]=C_retrieve2(lf[109],C_text("main#translate-options"));
av2[4]=t1;
av2[5]=C_SCHEME_END_OF_LIST;
av2[6]=C_retrieve2(lf[114],C_text("main#translation-optimization-options"));
((C_proc)(void*)(*((C_word*)t2+1)))(7,av2);}}}}

/* k6362 in k6290 in k6282 in k6237 in k6234 in for-each-loop1297 in k4612 in k4609 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in ... */
static void C_ccall f_6364(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_6364,c,av);}
a=C_alloc(6);
t2=((C_word*)t0)[2];
f_6347(t2,C_a_i_list2(&a,2,lf[227],t1));}

/* k6376 in k6282 in k6237 in k6234 in for-each-loop1297 in k4612 in k4609 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in ... */
static void C_ccall f_6378(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_6378,c,av);}
a=C_alloc(6);
t2=((C_word*)t0)[2];
f_6292(t2,C_a_i_list(&a,2,lf[230],t1));}

/* for-each-loop1297 in k4612 in k4609 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in ... */
static void C_fcall f_6407(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_6407,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_length(C_retrieve2(lf[84],C_text("main#scheme-files")));
t5=C_eqp(C_fix(1),t4);
t6=(C_truep(t5)?C_retrieve2(lf[118],C_text("main#target-filename")):t3);
t7=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6236,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_retrieve2(lf[93],C_text("main#cpp-mode")))){
/* csc.scm:868: chicken.pathname#pathname-replace-extension */
t8=C_fast_retrieve(lf[175]);{
C_word av2[4];
av2[0]=t8;
av2[1]=t7;
av2[2]=t6;
av2[3]=lf[232];
((C_proc)(void*)(*((C_word*)t8+1)))(4,av2);}}
else{
if(C_truep(C_retrieve2(lf[94],C_text("main#objc-mode")))){
/* csc.scm:868: chicken.pathname#pathname-replace-extension */
t8=C_fast_retrieve(lf[175]);{
C_word av2[4];
av2[0]=t8;
av2[1]=t7;
av2[2]=t6;
av2[3]=lf[233];
((C_proc)(void*)(*((C_word*)t8+1)))(4,av2);}}
else{
/* csc.scm:868: chicken.pathname#pathname-replace-extension */
t8=C_fast_retrieve(lf[175]);{
C_word av2[4];
av2[0]=t8;
av2[1]=t7;
av2[2]=t6;
av2[3]=lf[234];
((C_proc)(void*)(*((C_word*)t8+1)))(4,av2);}}}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* g1361 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in ... */
static void C_fcall f_6432(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,3)))){
C_save_and_reclaim_args((void *)trf_6432,3,t0,t1,t2);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6436,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_retrieve2(lf[122],C_text("main#compile-only")))){
if(C_truep(C_retrieve2(lf[118],C_text("main#target-filename")))){
t4=C_i_length(C_retrieve2(lf[85],C_text("main#c-files")));
t5=C_eqp(C_fix(1),t4);
if(C_truep(t5)){
t6=t3;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_retrieve2(lf[118],C_text("main#target-filename"));
f_6436(2,av2);}}
else{
/* csc.scm:920: chicken.pathname#pathname-replace-extension */
t6=C_fast_retrieve(lf[175]);{
C_word av2[4];
av2[0]=t6;
av2[1]=t3;
av2[2]=t2;
av2[3]=C_retrieve2(lf[64],C_text("main#object-extension"));
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}}
else{
/* csc.scm:920: chicken.pathname#pathname-replace-extension */
t4=C_fast_retrieve(lf[175]);{
C_word av2[4];
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=C_retrieve2(lf[64],C_text("main#object-extension"));
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}}
else{
/* csc.scm:920: chicken.pathname#pathname-replace-extension */
t4=C_fast_retrieve(lf[175]);{
C_word av2[4];
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=C_retrieve2(lf[64],C_text("main#object-extension"));
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}}

/* k6434 in g1361 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in ... */
static void C_ccall f_6436(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_6436,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6439,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_i_member(t1,C_retrieve2(lf[89],C_text("main#object-files"))))){
/* csc.scm:922: stop */
f_3819(t2,lf[209],C_a_i_list(&a,2,((C_word*)t0)[4],t1));}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_6439(2,av2);}}}

/* k6437 in k6434 in g1361 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in ... */
static void C_ccall f_6439(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_6439,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6442,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6454,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(C_truep(C_retrieve2(lf[93],C_text("main#cpp-mode")))?C_retrieve2(lf[58],C_text("main#c++-compiler")):C_retrieve2(lf[57],C_text("main#compiler")));
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6466,a[2]=t4,a[3]=t3,a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
/* csc.scm:928: quotewrap */
t6=C_retrieve2(lf[50],C_text("main#quotewrap"));{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=((C_word*)t0)[5];
f_3868(3,av2);}}

/* k6440 in k6437 in k6434 in g1361 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in ... */
static void C_ccall f_6442(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_6442,c,av);}
a=C_alloc(6);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],C_retrieve2(lf[90],C_text("main#generated-object-files")));
t3=C_mutate(&lf[90] /* (set! main#generated-object-files ...) */,t2);
t4=C_a_i_cons(&a,2,((C_word*)t0)[2],((C_word*)((C_word*)t0)[3])[1]);
t5=C_mutate(((C_word *)((C_word*)t0)[3])+1,t4);
t6=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}

/* k6452 in k6437 in k6434 in g1361 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in ... */
static void C_ccall f_6454(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6454,c,av);}
/* csc.scm:924: command */
f_7488(((C_word*)t0)[2],t1);}

/* k6464 in k6437 in k6434 in g1361 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in ... */
static void C_ccall f_6466(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_6466,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6470,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6489,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:929: quotewrap */
t4=C_retrieve2(lf[50],C_text("main#quotewrap"));{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[4];
f_3868(3,av2);}}

/* k6468 in k6464 in k6437 in k6434 in g1361 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in ... */
static void C_ccall f_6470(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_6470,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6474,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_retrieve2(lf[93],C_text("main#cpp-mode")))){
t3=C_i_string_equal_p(lf[205],C_retrieve2(lf[58],C_text("main#c++-compiler")));
t4=t2;
f_6474(t4,(C_truep(t3)?lf[206]:lf[207]));}
else{
t3=t2;
f_6474(t3,lf[207]);}}

/* k6472 in k6468 in k6464 in k6437 in k6434 in g1361 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in ... */
static void C_fcall f_6474(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,2)))){
C_save_and_reclaim_args((void *)trf_6474,2,t0,t1);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6478,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* csc.scm:934: compiler-options */
f_6704(t2);}

/* k6476 in k6472 in k6468 in k6464 in k6437 in k6434 in g1361 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in ... */
static void C_ccall f_6478(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,2)))){
C_save_and_reclaim((void *)f_6478,c,av);}
a=C_alloc(18);
t2=C_a_i_list6(&a,6,((C_word*)t0)[2],((C_word*)t0)[3],((C_word*)t0)[4],lf[204],((C_word*)t0)[5],t1);
/* csc.scm:925: chicken.string#string-intersperse */
t3=C_fast_retrieve(lf[136]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[6];
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k6487 in k6464 in k6437 in k6434 in g1361 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in ... */
static void C_ccall f_6489(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6489,c,av);}
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[186]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[186]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=lf[208];
av2[3]=t1;
tp(4,av2);}}

/* k6516 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in ... */
static void C_ccall f_6518(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_6518,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6521,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6654,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_retrieve2(lf[74],C_text("main#generate-manifest")))){
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6679,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:938: chicken.platform#software-type */
t5=C_fast_retrieve(lf[218]);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t4=t3;
f_6654(t4,C_SCHEME_FALSE);}}

/* k6519 in k6516 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in ... */
static void C_fcall f_6521(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(13,0,3)))){
C_save_and_reclaim_args((void *)trf_6521,2,t0,t1);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6522,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=C_retrieve2(lf[86],C_text("main#rc-files"));
t4=C_i_check_list_2(C_retrieve2(lf[86],C_text("main#rc-files")),lf[176]);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6559,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6630,a[2]=t7,a[3]=t2,tmp=(C_word)a,a+=4,tmp));
t9=((C_word*)t7)[1];
f_6630(t9,t5,C_retrieve2(lf[86],C_text("main#rc-files")));}

/* g1371 in k6519 in k6516 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in ... */
static void C_fcall f_6522(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,4)))){
C_save_and_reclaim_args((void *)trf_6522,3,t0,t1,t2);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6526,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* csc.scm:945: scheme#string-append */
t4=*((C_word*)lf[76]+1);{
C_word av2[5];
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=lf[210];
av2[4]=C_retrieve2(lf[64],C_text("main#object-extension"));
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k6524 in g1371 in k6519 in k6516 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in ... */
static void C_ccall f_6526(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_6526,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6529,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6541,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6549,a[2]=t3,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* csc.scm:948: quotewrap */
t5=C_retrieve2(lf[50],C_text("main#quotewrap"));{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[4];
f_3868(3,av2);}}

/* k6527 in k6524 in g1371 in k6519 in k6516 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in ... */
static void C_ccall f_6529(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_6529,c,av);}
a=C_alloc(6);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],C_retrieve2(lf[90],C_text("main#generated-object-files")));
t3=C_mutate(&lf[90] /* (set! main#generated-object-files ...) */,t2);
t4=C_a_i_cons(&a,2,((C_word*)t0)[2],((C_word*)((C_word*)t0)[3])[1]);
t5=C_mutate(((C_word *)((C_word*)t0)[3])+1,t4);
t6=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}

/* k6539 in k6524 in g1371 in k6519 in k6516 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in ... */
static void C_ccall f_6541(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6541,c,av);}
/* csc.scm:946: command */
f_7488(((C_word*)t0)[2],t1);}

/* k6547 in k6524 in g1371 in k6519 in k6516 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in ... */
static void C_ccall f_6549(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_6549,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6553,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* csc.scm:948: quotewrap */
t3=C_retrieve2(lf[50],C_text("main#quotewrap"));{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
f_3868(3,av2);}}

/* k6551 in k6547 in k6524 in g1371 in k6519 in k6516 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in ... */
static void C_ccall f_6553(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_6553,c,av);}
a=C_alloc(9);
t2=C_a_i_list3(&a,3,C_retrieve2(lf[59],C_text("main#rc-compiler")),((C_word*)t0)[2],t1);
/* csc.scm:947: chicken.string#string-intersperse */
t3=C_fast_retrieve(lf[136]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k6557 in k6519 in k6516 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in ... */
static void C_ccall f_6559(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_6559,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6563,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6628,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:952: scheme#reverse */
t4=*((C_word*)lf[189]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)((C_word*)t0)[3])[1];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k6561 in k6557 in k6519 in k6516 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in ... */
static void C_ccall f_6563(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_6563,c,av);}
a=C_alloc(8);
t2=C_mutate(&lf[89] /* (set! main#object-files ...) */,t1);
if(C_truep(C_retrieve2(lf[120],C_text("main#keep-files")))){
t3=C_SCHEME_UNDEFINED;
t4=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
f_4461(2,av2);}}
else{
t3=C_retrieve2(lf[164],C_text("main#$delete-file"));
t4=C_retrieve2(lf[87],C_text("main#generated-c-files"));
t5=C_i_check_list_2(C_retrieve2(lf[87],C_text("main#generated-c-files")),lf[176]);
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6572,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6603,a[2]=t8,tmp=(C_word)a,a+=3,tmp));
t10=((C_word*)t8)[1];
f_6603(t10,t6,C_retrieve2(lf[87],C_text("main#generated-c-files")));}}

/* k6570 in k6561 in k6557 in k6519 in k6516 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in ... */
static void C_ccall f_6572(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_6572,c,av);}
a=C_alloc(5);
t2=C_retrieve2(lf[164],C_text("main#$delete-file"));
t3=C_retrieve2(lf[88],C_text("main#generated-rc-files"));
t4=C_i_check_list_2(C_retrieve2(lf[88],C_text("main#generated-rc-files")),lf[176]);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6580,a[2]=t6,tmp=(C_word)a,a+=3,tmp));
t8=((C_word*)t6)[1];
f_6580(t8,((C_word*)t0)[2],C_retrieve2(lf[88],C_text("main#generated-rc-files")));}

/* for-each-loop1430 in k6570 in k6561 in k6557 in k6519 in k6516 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in ... */
static void C_fcall f_6580(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_6580,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6590,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* csc.scm:955: g1431 */
t4=C_retrieve2(lf[164],C_text("main#$delete-file"));{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
f_7501(3,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k6588 in for-each-loop1430 in k6570 in k6561 in k6557 in k6519 in k6516 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in ... */
static void C_ccall f_6590(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6590,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_6580(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* for-each-loop1413 in k6561 in k6557 in k6519 in k6516 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in ... */
static void C_fcall f_6603(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_6603,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6613,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* csc.scm:954: g1414 */
t4=C_retrieve2(lf[164],C_text("main#$delete-file"));{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
f_7501(3,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k6611 in for-each-loop1413 in k6561 in k6557 in k6519 in k6516 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in ... */
static void C_ccall f_6613(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6613,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_6603(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k6626 in k6557 in k6519 in k6516 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in ... */
static void C_ccall f_6628(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6628,c,av);}
/* csc.scm:952: scheme#append */
t2=*((C_word*)lf[137]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_retrieve2(lf[89],C_text("main#object-files"));
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* for-each-loop1370 in k6519 in k6516 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in ... */
static void C_fcall f_6630(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_6630,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6640,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* csc.scm:943: g1371 */
t4=((C_word*)t0)[3];
f_6522(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k6638 in for-each-loop1370 in k6519 in k6516 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in ... */
static void C_ccall f_6640(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6640,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_6630(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k6652 in k6516 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in ... */
static void C_fcall f_6654(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,0,3)))){
C_save_and_reclaim_args((void *)trf_6654,2,t0,t1);}
a=C_alloc(3);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6657,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csc.scm:939: chicken.pathname#pathname-replace-extension */
t3=C_fast_retrieve(lf[175]);{
C_word av2[4];
av2[0]=t3;
av2[1]=t2;
av2[2]=C_retrieve2(lf[118],C_text("main#target-filename"));
av2[3]=lf[217];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}
else{
t2=((C_word*)t0)[2];
f_6521(t2,C_SCHEME_UNDEFINED);}}

/* k6655 in k6652 in k6516 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in ... */
static void C_ccall f_6657(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_6657,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6660,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6672,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* csc.scm:940: chicken.pathname#pathname-file */
t4=C_fast_retrieve(lf[216]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_retrieve2(lf[118],C_text("main#target-filename"));
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k6658 in k6655 in k6652 in k6516 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in ... */
static void C_ccall f_6660(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_6660,c,av);}
a=C_alloc(6);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],C_retrieve2(lf[86],C_text("main#rc-files")));
t3=C_mutate(&lf[86] /* (set! main#rc-files ...) */,t2);
t4=C_a_i_cons(&a,2,((C_word*)t0)[2],C_retrieve2(lf[88],C_text("main#generated-rc-files")));
t5=C_mutate(&lf[88] /* (set! main#generated-rc-files ...) */,t4);
t6=((C_word*)t0)[3];
f_6521(t6,t5);}

/* k6670 in k6655 in k6652 in k6516 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in ... */
static void C_ccall f_6672(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_6672,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7560,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_retrieve2(lf[119],C_text("main#verbose")))){
/* csc.scm:1122: chicken.base#print */
t3=*((C_word*)lf[157]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[215];
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_7560(2,av2);}}}

/* k6677 in k6516 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in ... */
static void C_ccall f_6679(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6679,c,av);}
t2=((C_word*)t0)[2];
f_6654(t2,C_eqp(lf[15],t1));}

/* for-each-loop1360 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in ... */
static void C_fcall f_6681(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_6681,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6691,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* csc.scm:914: g1361 */
t4=((C_word*)t0)[3];
f_6432(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k6689 in for-each-loop1360 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in ... */
static void C_ccall f_6691(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6691,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_6681(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* main#compiler-options in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in ... */
static void C_fcall f_6704(C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,3)))){
C_save_and_reclaim_args((void *)trf_6704,1,t1);}
a=C_alloc(10);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=C_retrieve2(lf[134],C_text("main#quote-option"));
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6715,a[2]=t1,a[3]=t4,a[4]=t5,tmp=(C_word)a,a+=5,tmp);
/* csc.scm:960: scheme#append */
t8=*((C_word*)lf[137]+1);{
C_word av2[4];
av2[0]=t8;
av2[1]=t7;
av2[2]=C_retrieve2(lf[115],C_text("main#compilation-optimization-options"));
av2[3]=C_retrieve2(lf[112],C_text("main#compile-options"));
((C_proc)(void*)(*((C_word*)t8+1)))(4,av2);}}

/* k6713 in main#compiler-options in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in ... */
static void C_ccall f_6715(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_6715,c,av);}
a=C_alloc(10);
t2=C_i_check_list_2(t1,lf[135]);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6721,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6723,a[2]=((C_word*)t0)[3],a[3]=t5,a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp));
t7=((C_word*)t5)[1];
f_6723(t7,t3,t1);}

/* k6719 in k6713 in main#compiler-options in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in ... */
static void C_ccall f_6721(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6721,c,av);}
/* csc.scm:958: chicken.string#string-intersperse */
t2=C_fast_retrieve(lf[136]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* map-loop1453 in k6713 in main#compiler-options in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in ... */
static void C_fcall f_6723(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_6723,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6748,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* csc.scm:959: g1459 */
t4=C_retrieve2(lf[134],C_text("main#quote-option"));{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
f_7376(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k6746 in map-loop1453 in k6713 in main#compiler-options in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_6748(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_6748,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_6723(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in ... */
static void C_ccall f_6762(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,3)))){
C_save_and_reclaim((void *)f_6762,c,av);}
a=C_alloc(15);
t2=C_mutate(&lf[89] /* (set! main#object-files ...) */,t1);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=C_retrieve2(lf[50],C_text("main#quotewrap"));
t8=C_retrieve2(lf[89],C_text("main#object-files"));
t9=C_i_check_list_2(C_retrieve2(lf[89],C_text("main#object-files")),lf[135]);
t10=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6771,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t11=C_SCHEME_UNDEFINED;
t12=(*a=C_VECTOR_TYPE|1,a[1]=t11,tmp=(C_word)a,a+=2,tmp);
t13=C_set_block_item(t12,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6907,a[2]=t5,a[3]=t12,a[4]=t6,tmp=(C_word)a,a+=5,tmp));
t14=((C_word*)t12)[1];
f_6907(t14,t10,C_retrieve2(lf[89],C_text("main#object-files")));}

/* k6769 in k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in ... */
static void C_ccall f_6771(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_6771,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6774,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* csc.scm:971: quotewrap */
t3=C_retrieve2(lf[50],C_text("main#quotewrap"));{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_retrieve2(lf[118],C_text("main#target-filename"));
f_3868(3,av2);}}

/* k6772 in k6769 in k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in ... */
static void C_ccall f_6774(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(21,c,2)))){
C_save_and_reclaim((void *)f_6774,c,av);}
a=C_alloc(21);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6777,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6872,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6876,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
t5=(C_truep(C_retrieve2(lf[93],C_text("main#cpp-mode")))?C_retrieve2(lf[61],C_text("main#c++-linker")):C_retrieve2(lf[60],C_text("main#linker")));
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6884,a[2]=t4,a[3]=t5,tmp=(C_word)a,a+=4,tmp);
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6892,a[2]=t6,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6905,a[2]=t7,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:979: quotewrap */
t9=C_retrieve2(lf[50],C_text("main#quotewrap"));{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t9;
av2[1]=t8;
av2[2]=C_retrieve2(lf[118],C_text("main#target-filename"));
f_3868(3,av2);}}

/* k6775 in k6772 in k6769 in k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in ... */
static void C_ccall f_6777(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(17,c,3)))){
C_save_and_reclaim((void *)f_6777,c,av);}
a=C_alloc(17);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6780,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(C_truep(C_retrieve2(lf[31],C_text("main#osx")))?C_retrieve2(lf[49],C_text("main#host-mode")):C_SCHEME_FALSE);
if(C_truep(t3)){
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6821,a[2]=t2,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6831,a[2]=t4,tmp=(C_word)a,a+=3,tmp);
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6835,a[2]=t5,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* ##sys#peek-c-string */
t7=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t7;
av2[1]=t6;
av2[2]=C_mpointer(&a,(void*)C_INSTALL_POSTINSTALL_PROGRAM);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}
else{
t4=t2;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_6780(2,av2);}}}

/* k6778 in k6775 in k6772 in k6769 in k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in ... */
static void C_ccall f_6780(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_6780,c,av);}
a=C_alloc(3);
if(C_truep(C_retrieve2(lf[120],C_text("main#keep-files")))){
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=C_retrieve2(lf[164],C_text("main#$delete-file"));
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6786,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csc.scm:1000: scheme#append */
t4=*((C_word*)lf[137]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_retrieve2(lf[90],C_text("main#generated-object-files"));
av2[3]=C_retrieve2(lf[91],C_text("main#transient-link-files"));
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}}

/* k6784 in k6778 in k6775 in k6772 in k6769 in k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in ... */
static void C_ccall f_6786(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_6786,c,av);}
a=C_alloc(5);
t2=C_i_check_list_2(t1,lf[176]);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6794,a[2]=t4,tmp=(C_word)a,a+=3,tmp));
t6=((C_word*)t4)[1];
f_6794(t6,((C_word*)t0)[2],t1);}

/* for-each-loop1515 in k6784 in k6778 in k6775 in k6772 in k6769 in k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in ... */
static void C_fcall f_6794(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_6794,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6804,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* csc.scm:999: g1516 */
t4=C_retrieve2(lf[164],C_text("main#$delete-file"));{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
f_7501(3,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k6802 in for-each-loop1515 in k6784 in k6778 in k6775 in k6772 in k6769 in k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in ... */
static void C_ccall f_6804(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6804,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_6794(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k6819 in k6775 in k6772 in k6769 in k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in ... */
static void C_ccall f_6821(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_6821,c,av);}
a=C_alloc(4);
if(C_truep(C_retrieve2(lf[101],C_text("main#gui")))){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7524,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* csc.scm:1117: chicken.base#open-output-string */
t3=C_fast_retrieve(lf[181]);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
f_6780(2,av2);}}}

/* k6829 in k6775 in k6772 in k6769 in k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in ... */
static void C_ccall f_6831(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6831,c,av);}
/* csc.scm:983: command */
f_7488(((C_word*)t0)[2],t1);}

/* k6833 in k6775 in k6772 in k6769 in k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in ... */
static void C_ccall f_6835(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_6835,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6839,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* csc.scm:985: libchicken */
f_3943(t2);}

/* k6837 in k6833 in k6775 in k6772 in k6769 in k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in ... */
static void C_ccall f_6839(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_6839,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6843,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6847,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6867,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:987: libchicken */
f_3943(t4);}

/* k6841 in k6837 in k6833 in k6775 in k6772 in k6769 in k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in ... */
static void C_ccall f_6843(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,8)))){
C_save_and_reclaim((void *)f_6843,c,av);}
/* csc.scm:984: scheme#string-append */
t2=*((C_word*)lf[76]+1);{
C_word *av2;
if(c >= 9) {
  av2=av;
} else {
  av2=C_alloc(9);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=lf[182];
av2[4]=((C_word*)t0)[4];
av2[5]=lf[183];
av2[6]=t1;
av2[7]=lf[184];
av2[8]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t2+1)))(9,av2);}}

/* k6845 in k6837 in k6833 in k6775 in k6772 in k6769 in k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in ... */
static void C_ccall f_6847(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_6847,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6850,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_retrieve2(lf[102],C_text("main#deployed")))){
/* csc.scm:989: chicken.pathname#make-pathname */
t3=C_fast_retrieve(lf[132]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[185];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6860,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_retrieve2(lf[49],C_text("main#host-mode")))){
t4=C_retrieve2(lf[17],C_text("main#host-libdir"));
t5=C_retrieve2(lf[17],C_text("main#host-libdir"));
/* csc.scm:990: chicken.pathname#make-pathname */
t6=C_fast_retrieve(lf[132]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t2;
av2[2]=C_retrieve2(lf[17],C_text("main#host-libdir"));
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}
else{
/* ##sys#peek-c-string */
t4=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_mpointer(&a,(void*)C_TARGET_RUN_LIB_HOME);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}}}

/* k6848 in k6845 in k6837 in k6833 in k6775 in k6772 in k6769 in k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in ... */
static void C_ccall f_6850(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6850,c,av);}
/* csc.scm:986: quotewrap */
t2=C_retrieve2(lf[50],C_text("main#quotewrap"));{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
f_3868(3,av2);}}

/* k6858 in k6845 in k6837 in k6833 in k6775 in k6772 in k6769 in k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in ... */
static void C_ccall f_6860(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6860,c,av);}
/* csc.scm:990: chicken.pathname#make-pathname */
t2=C_fast_retrieve(lf[132]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k6865 in k6837 in k6833 in k6775 in k6772 in k6769 in k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in ... */
static void C_ccall f_6867(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6867,c,av);}
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[186]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[186]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=lf[187];
tp(4,av2);}}

/* k6870 in k6772 in k6769 in k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in ... */
static void C_ccall f_6872(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6872,c,av);}
/* csc.scm:973: command */
f_7488(((C_word*)t0)[2],t1);}

/* k6874 in k6772 in k6769 in k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in ... */
static void C_ccall f_6876(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6876,c,av);}
/* csc.scm:974: chicken.string#string-intersperse */
t2=C_fast_retrieve(lf[136]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k6882 in k6772 in k6769 in k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in ... */
static void C_ccall f_6884(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_6884,c,av);}
a=C_alloc(3);
/* csc.scm:975: cons* */
f_2943(((C_word*)t0)[2],((C_word*)t0)[3],C_a_i_list(&a,1,t1));}

/* k6890 in k6772 in k6769 in k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in ... */
static void C_ccall f_6892(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_6892,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6896,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* csc.scm:980: linker-options */
f_7191(t2);}

/* k6894 in k6890 in k6772 in k6769 in k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in ... */
static void C_ccall f_6896(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_6896,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6900,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* csc.scm:981: linker-libraries */
f_7244(t2);}

/* k6898 in k6894 in k6890 in k6772 in k6769 in k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in ... */
static void C_ccall f_6900(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_6900,c,av);}
a=C_alloc(9);
t2=C_a_i_list3(&a,3,((C_word*)t0)[2],((C_word*)t0)[3],t1);
/* csc.scm:977: scheme#append */
t3=*((C_word*)lf[137]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[4];
av2[2]=((C_word*)t0)[5];
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k6903 in k6772 in k6769 in k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in ... */
static void C_ccall f_6905(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6905,c,av);}
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[186]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[186]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=lf[188];
av2[3]=t1;
tp(4,av2);}}

/* map-loop1481 in k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in ... */
static void C_fcall f_6907(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_6907,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6932,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* csc.scm:970: g1487 */
t4=C_retrieve2(lf[50],C_text("main#quotewrap"));{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
f_3868(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k6930 in map-loop1481 in k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in ... */
static void C_ccall f_6932(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_6932,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_6907(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k6946 in k7084 in loop in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in ... */
static void C_ccall f_6948(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_6948,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6951,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_fast_retrieve(lf[130]))){
/* csc.scm:1007: chicken.string#string-chomp */
t3=C_fast_retrieve(lf[195]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=t1;
av2[3]=lf[196];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f8620,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csc.scm:1008: chicken.pathname#make-pathname */
t4=C_fast_retrieve(lf[132]);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_SCHEME_FALSE;
av2[3]=t1;
av2[4]=lf[194];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}}

/* k6949 in k6946 in k7084 in loop in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in ... */
static void C_ccall f_6951(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_6951,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6958,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csc.scm:1008: chicken.pathname#make-pathname */
t3=C_fast_retrieve(lf[132]);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_SCHEME_FALSE;
av2[3]=t1;
av2[4]=lf[194];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k6956 in k6949 in k6946 in k7084 in loop in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in ... */
static void C_ccall f_6958(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6958,c,av);}
/* csc.scm:1008: chicken.file#file-exists? */
t2=C_fast_retrieve(lf[131]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k6970 in map-loop1544 in k6985 in k7110 in k7087 in k7084 in loop in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in ... */
static void C_ccall f_6972(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_6972,c,av);}
a=C_alloc(3);
if(C_truep(t1)){
t2=((C_word*)t0)[2];
f_7005(t2,C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST));}
else{
/* csc.scm:1012: stop */
f_3819(((C_word*)t0)[3],lf[191],C_a_i_list(&a,1,((C_word*)t0)[4]));}}

/* k6985 in k7110 in k7087 in k7084 in loop in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in ... */
static void C_ccall f_6987(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_6987,c,av);}
a=C_alloc(7);
t2=C_i_check_list_2(t1,lf[135]);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6995,a[2]=((C_word*)t0)[2],a[3]=t4,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp));
t6=((C_word*)t4)[1];
f_6995(t6,((C_word*)t0)[4],t1);}

/* map-loop1544 in k6985 in k7110 in k7087 in k7084 in loop in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in ... */
static void C_fcall f_6995(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(14,0,2)))){
C_save_and_reclaim_args((void *)trf_6995,3,t0,t1,t2);}
a=C_alloc(14);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7005,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7020,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
t5=C_slot(t2,C_fix(0));
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6972,a[2]=t3,a[3]=t4,a[4]=t5,tmp=(C_word)a,a+=5,tmp);
/* csc.scm:1011: find-object-file */
t7=C_retrieve2(lf[128],C_text("main#find-object-file"));{
C_word av2[3];
av2[0]=t7;
av2[1]=t6;
av2[2]=t5;
f_4240(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k7003 in map-loop1544 in k6985 in k7110 in k7087 in k7084 in loop in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in ... */
static void C_fcall f_7005(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,2)))){
C_save_and_reclaim_args((void *)trf_7005,2,t0,t1);}
t2=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t1);
t3=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t4=((C_word*)((C_word*)t0)[3])[1];
f_6995(t4,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k7018 in map-loop1544 in k6985 in k7110 in k7087 in k7084 in loop in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in ... */
static void C_ccall f_7020(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_7020,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];
f_7005(t2,C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST));}

/* map-loop1567 in k7110 in k7087 in k7084 in loop in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in ... */
static void C_fcall f_7029(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_7029,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7054,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* csc.scm:1013: g1573 */
t4=((C_word*)t0)[4];{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k7052 in map-loop1567 in k7110 in k7087 in k7084 in loop in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in ... */
static void C_ccall f_7054(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7054,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_7029(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* loop in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in ... */
static void C_fcall f_7066(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_7066,4,t0,t1,t2,t3);}
a=C_alloc(6);
if(C_truep(C_i_nullp(t2))){
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7080,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:1016: scheme#reverse */
t5=*((C_word*)lf[189]+1);{
C_word av2[3];
av2[0]=t5;
av2[1]=t4;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}
else{
t4=C_retrieve2(lf[125],C_text("main#static"));
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7086,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t3,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_retrieve2(lf[125],C_text("main#static")))){
t6=t5;
f_7086(t6,C_retrieve2(lf[125],C_text("main#static")));}
else{
t6=C_i_car(t2);
t7=t5;
f_7086(t7,C_i_not(C_i_member(t6,((C_word*)t0)[3])));}}}

/* k7078 in loop in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in ... */
static void C_ccall f_7080(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_7080,c,av);}
a=C_alloc(5);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3080,a[2]=t3,tmp=(C_word)a,a+=3,tmp));
t5=((C_word*)t3)[1];
f_3080(t5,((C_word*)t0)[2],t1);}

/* k7084 in loop in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in ... */
static void C_fcall f_7086(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,3)))){
C_save_and_reclaim_args((void *)trf_7086,2,t0,t1);}
a=C_alloc(9);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7089,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t3=C_i_car(((C_word*)t0)[5]);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6948,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:1005: chicken.pathname#pathname-strip-extension */
t5=C_fast_retrieve(lf[197]);{
C_word av2[3];
av2[0]=t5;
av2[1]=t4;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}
else{
t2=C_i_cdr(((C_word*)t0)[5]);
t3=C_u_i_car(((C_word*)t0)[5]);
t4=C_a_i_cons(&a,2,t3,((C_word*)t0)[4]);
/* csc.scm:1023: loop */
t5=((C_word*)((C_word*)t0)[2])[1];
f_7066(t5,((C_word*)t0)[3],t2,t4);}}

/* k7087 in k7084 in loop in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in ... */
static void C_ccall f_7089(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_7089,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7092,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
if(C_truep(t1)){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7112,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:1020: scheme#with-input-from-file */
t4=C_fast_retrieve(lf[192]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t1;
av2[3]=*((C_word*)lf[193]+1);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
f_7092(2,av2);}}}

/* k7090 in k7087 in k7084 in loop in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in ... */
static void C_ccall f_7092(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_7092,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7099,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* csc.scm:1022: scheme#append */
t3=*((C_word*)lf[137]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=t1;
av2[3]=C_u_i_cdr(((C_word*)t0)[5]);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k7097 in k7090 in k7087 in k7084 in loop in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in ... */
static void C_ccall f_7099(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_7099,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7103,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* csc.scm:1022: scheme#append */
t3=*((C_word*)lf[137]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k7101 in k7097 in k7090 in k7087 in k7084 in loop in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in ... */
static void C_ccall f_7103(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7103,c,av);}
/* csc.scm:1022: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_7066(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* k7110 in k7087 in k7084 in loop in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in ... */
static void C_ccall f_7112(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(23,c,3)))){
C_save_and_reclaim((void *)f_7112,c,av);}
a=C_alloc(23);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t7=t6;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=((C_word*)t8)[1];
t10=C_fast_retrieve(lf[190]);
t11=C_i_check_list_2(t1,lf[135]);
t12=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6987,a[2]=t4,a[3]=t5,a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
t13=C_SCHEME_UNDEFINED;
t14=(*a=C_VECTOR_TYPE|1,a[1]=t13,tmp=(C_word)a,a+=2,tmp);
t15=C_set_block_item(t14,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7029,a[2]=t8,a[3]=t14,a[4]=t10,a[5]=t9,tmp=(C_word)a,a+=6,tmp));
t16=((C_word*)t14)[1];
f_7029(t16,t12,t1);}

/* main#linker-options in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in ... */
static void C_fcall f_7191(C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,3)))){
C_save_and_reclaim_args((void *)trf_7191,1,t1);}
a=C_alloc(10);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=C_retrieve2(lf[134],C_text("main#quote-option"));
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7202,a[2]=t1,a[3]=t4,a[4]=t5,tmp=(C_word)a,a+=5,tmp);
/* csc.scm:1037: scheme#append */
t8=*((C_word*)lf[137]+1);{
C_word av2[4];
av2[0]=t8;
av2[1]=t7;
av2[2]=C_retrieve2(lf[116],C_text("main#linking-optimization-options"));
av2[3]=C_retrieve2(lf[117],C_text("main#link-options"));
((C_proc)(void*)(*((C_word*)t8+1)))(4,av2);}}

/* k7200 in main#linker-options in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in ... */
static void C_ccall f_7202(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_7202,c,av);}
a=C_alloc(10);
t2=C_i_check_list_2(t1,lf[135]);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7208,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7210,a[2]=((C_word*)t0)[3],a[3]=t5,a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp));
t7=((C_word*)t5)[1];
f_7210(t7,t3,t1);}

/* k7206 in k7200 in main#linker-options in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in ... */
static void C_ccall f_7208(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7208,c,av);}
/* csc.scm:1035: chicken.string#string-intersperse */
t2=C_fast_retrieve(lf[136]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* map-loop1637 in k7200 in main#linker-options in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in ... */
static void C_fcall f_7210(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_7210,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7235,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* csc.scm:1036: g1643 */
t4=C_retrieve2(lf[134],C_text("main#quote-option"));{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
f_7376(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k7233 in map-loop1637 in k7200 in main#linker-options in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_7235(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7235,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_7210(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* main#linker-libraries in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in ... */
static void C_fcall f_7244(C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,3)))){
C_save_and_reclaim_args((void *)trf_7244,1,t1);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7252,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_retrieve2(lf[125],C_text("main#static")))){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4071,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3986,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:129: libchicken */
f_3943(t4);}
else{
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4051,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4055,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_retrieve2(lf[49],C_text("main#host-mode")))){
/* ##sys#peek-c-string */
t5=*((C_word*)lf[46]+1);{
C_word av2[4];
av2[0]=t5;
av2[1]=t4;
av2[2]=C_mpointer(&a,(void*)C_INSTALL_LIB_NAME);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}
else{
/* ##sys#peek-c-string */
t5=*((C_word*)lf[46]+1);{
C_word av2[4];
av2[0]=t5;
av2[1]=t4;
av2[2]=C_mpointer(&a,(void*)C_TARGET_LIB_NAME);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}}}

/* k7250 in main#linker-libraries in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in ... */
static void C_ccall f_7252(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7252,c,av);}
/* csc.scm:1040: chicken.string#string-intersperse */
t2=C_fast_retrieve(lf[136]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k7277 in k7387 in k7381 in main#quote-option in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_7279(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_7279,c,av);}
a=C_alloc(3);
if(C_truep(((C_word*)((C_word*)t0)[2])[1])){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7289,a[2]=((C_word*)t0)[3],tmp=(C_word)a,a+=3,tmp);
/* csc.scm:1066: chicken.string#string-translate* */
t3=C_fast_retrieve(lf[146]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=t1;
av2[3]=lf[147];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k7287 in k7277 in k7387 in k7381 in main#quote-option in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_ccall f_7289(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_7289,c,av);}
/* csc.scm:1066: scheme#string-append */
t2=*((C_word*)lf[76]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[144];
av2[3]=t1;
av2[4]=lf[145];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k7291 in k7387 in k7381 in main#quote-option in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_7293(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7293,c,av);}
/* ##sys#list->string */
t2=C_fast_retrieve(lf[148]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k7295 in k7387 in k7381 in main#quote-option in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_7297(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_7297,c,av);}
a=C_alloc(6);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7299,a[2]=t3,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp));
t5=((C_word*)t3)[1];
f_7299(t5,((C_word*)t0)[3],t1);}

/* fold in k7295 in k7387 in k7381 in main#quote-option in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_fcall f_7299(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
loop:
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_7299,3,t0,t1,t2);}
a=C_alloc(4);
if(C_truep(C_i_nullp(t2))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_i_car(t2);
if(C_truep(C_i_memq(t3,C_retrieve2(lf[141],C_text("main#constant1665"))))){
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7322,a[2]=t1,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* csc.scm:1061: fold */
t7=t4;
t8=C_u_i_cdr(t2);
t1=t7;
t2=t8;
goto loop;}
else{
if(C_truep(C_u_i_char_whitespacep(t3))){
t4=C_set_block_item(((C_word*)t0)[3],0,C_SCHEME_TRUE);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f9025,a[2]=t1,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* csc.scm:1064: fold */
t7=t5;
t8=C_u_i_cdr(t2);
t1=t7;
t2=t8;
goto loop;}
else{
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f9029,a[2]=t1,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* csc.scm:1064: fold */
t7=t4;
t8=C_u_i_cdr(t2);
t1=t7;
t2=t8;
goto loop;}}}}

/* k7320 in fold in k7295 in k7387 in k7381 in main#quote-option in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in ... */
static void C_ccall f_7322(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_7322,c,av);}
a=C_alloc(6);
/* csc.scm:1061: cons* */
f_2943(((C_word*)t0)[2],C_make_character(92),C_a_i_list(&a,2,((C_word*)t0)[3],t1));}

/* main#string-any in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in ... */
static void C_fcall f_7342(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,3)))){
C_save_and_reclaim_args((void *)trf_7342,3,t1,t2,t3);}
a=C_alloc(8);
t4=C_i_string_length(t3);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7351,a[2]=t3,a[3]=t4,a[4]=t2,a[5]=t6,tmp=(C_word)a,a+=6,tmp));
t8=((C_word*)t6)[1];
f_7351(t8,t1,C_fix(0));}

/* lp in main#string-any in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in ... */
static void C_fcall f_7351(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,2)))){
C_save_and_reclaim_args((void *)trf_7351,3,t0,t1,t2);}
a=C_alloc(10);
t3=C_i_string_ref(((C_word*)t0)[2],t2);
t4=C_a_i_fixnum_plus(&a,2,t2,C_fix(1));
if(C_truep(C_i_integer_equalp(t4,((C_word*)t0)[3]))){
/* csc.scm:1075: criteria */
t5=((C_word*)t0)[4];{
C_word av2[3];
av2[0]=t5;
av2[1]=t1;
av2[2]=t3;
((C_proc)C_fast_retrieve_proc(t5))(3,av2);}}
else{
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7368,a[2]=t1,a[3]=((C_word*)t0)[5],a[4]=t4,tmp=(C_word)a,a+=5,tmp);
/* csc.scm:1076: criteria */
t6=((C_word*)t0)[4];{
C_word av2[3];
av2[0]=t6;
av2[1]=t5;
av2[2]=t3;
((C_proc)C_fast_retrieve_proc(t6))(3,av2);}}}

/* k7366 in lp in main#string-any in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in ... */
static void C_ccall f_7368(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7368,c,av);}
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
/* csc.scm:1077: lp */
t2=((C_word*)((C_word*)t0)[3])[1];
f_7351(t2,((C_word*)t0)[2],((C_word*)t0)[4]);}}

/* main#quote-option in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in ... */
static void C_ccall f_7376(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_7376,c,av);}
a=C_alloc(6);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7383,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7406,tmp=(C_word)a,a+=2,tmp);
/* csc.scm:1080: string-any */
f_7342(t3,t4,t2);}

/* k7381 in main#quote-option in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in ... */
static void C_ccall f_7383(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_7383,c,av);}
a=C_alloc(6);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7389,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7394,tmp=(C_word)a,a+=2,tmp);
/* csc.scm:1081: string-any */
f_7342(t2,t3,((C_word*)t0)[3]);}}

/* k7387 in k7381 in main#quote-option in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in ... */
static void C_ccall f_7389(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_7389,c,av);}
a=C_alloc(13);
if(C_truep(t1)){
t2=C_SCHEME_FALSE;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7279,a[2]=t3,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7293,a[2]=t4,tmp=(C_word)a,a+=3,tmp);
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7297,a[2]=t3,a[3]=t5,tmp=(C_word)a,a+=4,tmp);
/* ##sys#string->list */
t7=C_fast_retrieve(lf[149]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t7;
av2[1]=t6;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* a7393 in k7381 in main#quote-option in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in ... */
static void C_ccall f_7394(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7394,c,av);}
t3=C_u_i_char_whitespacep(t2);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=(C_truep(t3)?t3:C_i_memq(t2,C_retrieve2(lf[141],C_text("main#constant1665"))));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* a7405 in main#quote-option in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in ... */
static void C_ccall f_7406(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7406,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_char_equalp(C_make_character(34),t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k7415 in main#command in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in ... */
static void C_ccall f_7417(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_7417,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7420,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_retrieve2(lf[119],C_text("main#verbose")))){
/* csc.scm:1097: chicken.base#print */
t3=*((C_word*)lf[157]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_7420(2,av2);}}}

/* k7418 in k7415 in main#command in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in ... */
static void C_ccall f_7420(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_7420,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7423,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_retrieve2(lf[100],C_text("main#dry-run")))){
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_fix(0);
f_7423(2,av2);}}
else{
/* csc.scm:1098: chicken.process#system */
t3=C_fast_retrieve(lf[156]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}

/* k7421 in k7418 in k7415 in main#command in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_7423(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,4)))){
C_save_and_reclaim((void *)f_7423,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7426,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
t3=C_eqp(t1,C_fix(0));
if(C_truep(t3)){
t4=t2;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_7426(2,av2);}}
else{
t4=*((C_word*)lf[152]+1);
t5=*((C_word*)lf[152]+1);
t6=C_i_check_port_2(*((C_word*)lf[152]+1),C_fix(2),C_SCHEME_TRUE,lf[153]);
t7=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7443,a[2]=t2,a[3]=t4,a[4]=((C_word*)t0)[3],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
/* csc.scm:1100: ##sys#print */
t8=*((C_word*)lf[44]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t8;
av2[1]=t7;
av2[2]=lf[155];
av2[3]=C_SCHEME_FALSE;
av2[4]=*((C_word*)lf[152]+1);
((C_proc)(void*)(*((C_word*)t8+1)))(5,av2);}}}

/* k7424 in k7421 in k7418 in k7415 in main#command in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_ccall f_7426(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7426,c,av);}
t2=C_eqp(((C_word*)t0)[2],C_fix(0));
if(C_truep(t2)){
t3=lf[150] /* main#last-exit-code */ =C_fix(0);;
t4=C_retrieve2(lf[150],C_text("main#last-exit-code"));
if(C_truep(C_i_zerop(C_retrieve2(lf[150],C_text("main#last-exit-code"))))){
t5=C_SCHEME_UNDEFINED;
t6=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
/* csc.scm:1107: chicken.base#exit */
t5=C_fast_retrieve(lf[41]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=((C_word*)t0)[3];
av2[2]=C_retrieve2(lf[150],C_text("main#last-exit-code"));
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}}
else{
t3=lf[150] /* main#last-exit-code */ =C_fix(1);;
t4=C_retrieve2(lf[150],C_text("main#last-exit-code"));
if(C_truep(C_i_zerop(C_retrieve2(lf[150],C_text("main#last-exit-code"))))){
t5=C_SCHEME_UNDEFINED;
t6=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
/* csc.scm:1107: chicken.base#exit */
t5=C_fast_retrieve(lf[41]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=((C_word*)t0)[3];
av2[2]=C_retrieve2(lf[150],C_text("main#last-exit-code"));
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}}}

/* k7441 in k7421 in k7418 in k7415 in main#command in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_ccall f_7443(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_7443,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7446,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* csc.scm:1100: ##sys#print */
t3=*((C_word*)lf[44]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k7444 in k7441 in k7421 in k7418 in k7415 in main#command in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in ... */
static void C_ccall f_7446(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_7446,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7449,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* csc.scm:1100: ##sys#print */
t3=*((C_word*)lf[44]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[154];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k7447 in k7444 in k7441 in k7421 in k7418 in k7415 in main#command in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in ... */
static void C_ccall f_7449(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_7449,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7452,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* csc.scm:1100: ##sys#print */
t3=*((C_word*)lf[44]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k7450 in k7447 in k7444 in k7441 in k7421 in k7418 in k7415 in main#command in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in ... */
static void C_ccall f_7452(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7452,c,av);}
/* csc.scm:1100: ##sys#write-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[42]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[42]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=C_make_character(10);
av2[3]=((C_word*)t0)[3];
tp(4,av2);}}

/* k7466 in main#command in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in ... */
static void C_ccall f_7468(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_7468,c,av);}
a=C_alloc(3);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7472,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csc.scm:1090: g1721 */
t3=t2;
f_7472(t3,((C_word*)t0)[3],t1);}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
f_7417(2,av2);}}}

/* g1721 in k7466 in main#command in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in ... */
static void C_fcall f_7472(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_7472,3,t0,t1,t2);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7480,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* csc.scm:1095: chicken.process#qs */
t4=C_fast_retrieve(lf[51]);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k7478 in g1721 in k7466 in main#command in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_7480(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_7480,c,av);}
/* csc.scm:1094: scheme#string-append */
t2=*((C_word*)lf[76]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[160];
av2[3]=t1;
av2[4]=lf[161];
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* main#command in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in ... */
static void C_fcall f_7488(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,4)))){
C_save_and_reclaim_args((void *)trf_7488,2,t1,t2);}
a=C_alloc(7);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7417,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_mk_bool(C_WINDOWS_SHELL))){
/* csc.scm:1091: scheme#string-append */
t4=*((C_word*)lf[76]+1);{
C_word av2[5];
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[158];
av2[3]=t2;
av2[4]=lf[159];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7468,a[2]=t2,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_retrieve2(lf[31],C_text("main#osx")))){
/* csc.scm:1092: chicken.process-context#get-environment-variable */
t5=C_fast_retrieve(lf[162]);{
C_word av2[3];
av2[0]=t5;
av2[1]=t4;
av2[2]=lf[163];
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}
else{
t5=t4;{
C_word av2[2];
av2[0]=t5;
av2[1]=C_SCHEME_FALSE;
f_7468(2,av2);}}}}

/* main#$delete-file in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in ... */
static void C_ccall f_7501(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_7501,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7505,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_retrieve2(lf[119],C_text("main#verbose")))){
/* csc.scm:1111: chicken.base#print */
t4=*((C_word*)lf[157]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[166];
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
if(C_truep(C_retrieve2(lf[100],C_text("main#dry-run")))){
t4=C_SCHEME_UNDEFINED;
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
/* csc.scm:1112: chicken.file#delete-file */
t4=C_fast_retrieve(lf[165]);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t1;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}}}

/* k7503 in main#$delete-file in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in ... */
static void C_ccall f_7505(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7505,c,av);}
if(C_truep(C_retrieve2(lf[100],C_text("main#dry-run")))){
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
/* csc.scm:1112: chicken.file#delete-file */
t2=C_fast_retrieve(lf[165]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}}

/* k7522 in k6819 in k6775 in k6772 in k6769 in k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in ... */
static void C_ccall f_7524(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_7524,c,av);}
a=C_alloc(5);
t2=C_i_check_port_2(t1,C_fix(2),C_SCHEME_TRUE,lf[177]);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7530,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* csc.scm:1117: ##sys#print */
t4=*((C_word*)lf[44]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[180];
av2[3]=C_SCHEME_FALSE;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k7528 in k7522 in k6819 in k6775 in k6772 in k6769 in k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in ... */
static void C_ccall f_7530(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_7530,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7533,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7554,a[2]=t2,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* csc.scm:1118: quotewrap */
t4=C_retrieve2(lf[50],C_text("main#quotewrap"));{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[4];
f_3868(3,av2);}}

/* k7531 in k7528 in k7522 in k6819 in k6775 in k6772 in k6769 in k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in ... */
static void C_ccall f_7533(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_7533,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7536,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* csc.scm:1117: ##sys#write-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[42]);
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[42]+1);
av2[1]=t2;
av2[2]=C_make_character(32);
av2[3]=((C_word*)t0)[3];
tp(4,av2);}}

/* k7534 in k7531 in k7528 in k7522 in k6819 in k6775 in k6772 in k6769 in k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in ... */
static void C_ccall f_7536(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,3)))){
C_save_and_reclaim((void *)f_7536,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7539,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7546,a[2]=t2,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7550,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* csc.scm:1119: chicken.pathname#make-pathname */
t5=C_fast_retrieve(lf[132]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=C_retrieve2(lf[55],C_text("main#home"));
av2[3]=lf[179];
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k7537 in k7534 in k7531 in k7528 in k7522 in k6819 in k6775 in k6772 in k6769 in k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in ... */
static void C_ccall f_7539(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7539,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7542,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csc.scm:1117: chicken.base#get-output-string */
t3=C_fast_retrieve(lf[178]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7540 in k7537 in k7534 in k7531 in k7528 in k7522 in k6819 in k6775 in k6772 in k6769 in k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in ... */
static void C_ccall f_7542(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7542,c,av);}
/* csc.scm:1116: command */
f_7488(((C_word*)t0)[2],t1);}

/* k7544 in k7534 in k7531 in k7528 in k7522 in k6819 in k6775 in k6772 in k6769 in k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in ... */
static void C_ccall f_7546(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_7546,c,av);}
/* csc.scm:1117: ##sys#print */
t2=*((C_word*)lf[44]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k7548 in k7534 in k7531 in k7528 in k7522 in k6819 in k6775 in k6772 in k6769 in k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in ... */
static void C_ccall f_7550(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7550,c,av);}
/* csc.scm:1119: quotewrap */
t2=C_retrieve2(lf[50],C_text("main#quotewrap"));{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
f_3868(3,av2);}}

/* k7552 in k7528 in k7522 in k6819 in k6775 in k6772 in k6769 in k6760 in k4468 in k4465 in k4459 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in ... */
static void C_ccall f_7554(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_7554,c,av);}
/* csc.scm:1117: ##sys#print */
t2=*((C_word*)lf[44]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k7558 in k6670 in k6655 in k6652 in k6516 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in ... */
static void C_ccall f_7560(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_7560,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7565,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csc.scm:1123: scheme#with-output-to-file */
t3=C_fast_retrieve(lf[214]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* a7564 in k7558 in k6670 in k6655 in k6652 in k6516 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in ... */
static void C_ccall f_7565(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_7565,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7573,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t3=C_a_i_cons(&a,2,lf[211],C_SCHEME_END_OF_LIST);
t4=C_a_i_cons(&a,2,((C_word*)t0)[2],t3);
t5=C_a_i_cons(&a,2,lf[212],t4);
/* csc.scm:28: ##sys#print-to-string */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[213]);
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[213]+1);
av2[1]=t2;
av2[2]=t5;
tp(3,av2);}}

/* k7571 in a7564 in k7558 in k6670 in k6655 in k6652 in k6516 in k4453 in k4450 in k4447 in k4444 in k4440 in loop in k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in ... */
static void C_ccall f_7573(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7573,c,av);}
/* csc.scm:1125: chicken.base#print */
t2=*((C_word*)lf[157]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k7589 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in ... */
static void C_ccall f_7591(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7591,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7597,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* chicken.base#implicit-exit-handler */
t3=C_fast_retrieve(lf[167]);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k7595 in k7589 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in ... */
static void C_ccall f_7597(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7597,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k7599 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in ... */
static void C_ccall f_7601(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(25,c,5)))){
C_save_and_reclaim((void *)f_7601,c,av);}
a=C_alloc(25);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_4352,tmp=(C_word)a,a+=2,tmp));
t11=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_4359,tmp=(C_word)a,a+=2,tmp));
t12=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_4385,tmp=(C_word)a,a+=2,tmp));
t13=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_4415,tmp=(C_word)a,a+=2,tmp));
t14=C_SCHEME_UNDEFINED;
t15=(*a=C_VECTOR_TYPE|1,a[1]=t14,tmp=(C_word)a,a+=2,tmp);
t16=C_set_block_item(t15,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4431,a[2]=t9,a[3]=t15,a[4]=t3,a[5]=t5,a[6]=t7,tmp=(C_word)a,a+=7,tmp));
t17=((C_word*)t15)[1];
f_4431(t17,((C_word*)t0)[2],t1);}

/* k7603 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in ... */
static void C_ccall f_7605(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7605,c,av);}
/* csc.scm:1147: scheme#append */
t2=*((C_word*)lf[137]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_retrieve2(lf[47],C_text("main#arguments"));
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k7607 in k4088 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in ... */
static void C_ccall f_7609(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7609,c,av);}
if(C_truep(t1)){
/* csc.scm:1148: chicken.string#string-split */
t2=C_fast_retrieve(lf[240]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}
else{
/* csc.scm:1148: chicken.string#string-split */
t2=C_fast_retrieve(lf[240]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[417];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}}

/* k7614 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in k7814 in ... */
static void C_fcall f_7616(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_7616,2,t0,t1);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7620,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* csc.scm:248: chicken.process-context#get-environment-variable */
t3=C_fast_retrieve(lf[162]);{
C_word av2[3];
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[421];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7618 in k7614 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in ... */
static void C_ccall f_7620(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(21,c,3)))){
C_save_and_reclaim((void *)f_7620,c,av);}
a=C_alloc(21);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7623,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
if(C_truep(t1)){
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t8=t7;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=((C_word*)t9)[1];
t11=C_retrieve2(lf[50],C_text("main#quotewrap"));
t12=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7639,a[2]=t5,a[3]=t6,a[4]=t2,a[5]=t9,a[6]=t10,tmp=(C_word)a,a+=7,tmp);
/* csc.scm:250: chicken.string#string-split */
t13=C_fast_retrieve(lf[240]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t13;
av2[1]=t12;
av2[2]=t1;
av2[3]=lf[420];
((C_proc)(void*)(*((C_word*)t13+1)))(4,av2);}}
else{
/* csc.scm:246: scheme#append */
t3=*((C_word*)lf[137]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}}

/* k7621 in k7618 in k7614 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in ... */
static void C_ccall f_7623(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7623,c,av);}
/* csc.scm:246: scheme#append */
t2=*((C_word*)lf[137]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k7637 in k7618 in k7614 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in ... */
static void C_ccall f_7639(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_7639,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7642,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7684,a[2]=((C_word*)t0)[5],a[3]=t4,a[4]=((C_word*)t0)[6],tmp=(C_word)a,a+=5,tmp));
t6=((C_word*)t4)[1];
f_7684(t6,t2,t1);}

/* k7640 in k7637 in k7618 in k7614 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in ... */
static void C_ccall f_7642(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_7642,c,av);}
a=C_alloc(7);
t2=C_i_check_list_2(t1,lf[135]);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7650,a[2]=((C_word*)t0)[2],a[3]=t4,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp));
t6=((C_word*)t4)[1];
f_7650(t6,((C_word*)t0)[4],t1);}

/* map-loop862 in k7640 in k7637 in k7618 in k7614 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_fcall f_7650(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_7650,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7675,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* csc.scm:250: g885 */
t4=*((C_word*)lf[76]+1);{
C_word av2[4];
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[419];
av2[3]=C_slot(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k7673 in map-loop862 in k7640 in k7637 in k7618 in k7614 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in ... */
static void C_ccall f_7675(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7675,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_7650(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* map-loop889 in k7637 in k7618 in k7614 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in ... */
static void C_fcall f_7684(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_7684,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7709,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* csc.scm:250: g895 */
t4=C_retrieve2(lf[50],C_text("main#quotewrap"));{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
f_3868(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k7707 in map-loop889 in k7637 in k7618 in k7614 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in ... */
static void C_ccall f_7709(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7709,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_7684(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k7724 in k4035 in k4005 in k3998 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in k7814 in ... */
static void C_ccall f_7726(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_7726,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];
f_7616(t2,C_a_i_list1(&a,1,t1));}

/* k7744 in k3993 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in ... */
static void C_ccall f_7746(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7746,c,av);}
/* csc.scm:143: chicken.string#string-split */
t2=C_fast_retrieve(lf[240]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k7748 in k3988 in k3938 in k3933 in k3929 in k3916 in k3912 in k3908 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in k2434 in ... */
static void C_ccall f_7750(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7750,c,av);}
/* csc.scm:138: chicken.string#string-split */
t2=C_fast_retrieve(lf[240]);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k7773 in k3904 in k3900 in k3896 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in ... */
static void C_ccall f_7775(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7775,c,av);}
/* csc.scm:102: quotewrap */
t2=C_retrieve2(lf[50],C_text("main#quotewrap"));{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
f_3868(3,av2);}}

/* k7791 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in k2394 in ... */
static void C_ccall f_7793(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7793,c,av);}
/* csc.scm:98: quotewrap */
t2=C_retrieve2(lf[50],C_text("main#quotewrap"));{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
f_3868(3,av2);}}

/* k7795 in k3848 in k3844 in k3814 in k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in k2394 in ... */
static void C_ccall f_7797(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7797,c,av);}
/* csc.scm:98: chicken.pathname#make-pathname */
t2=C_fast_retrieve(lf[132]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_retrieve2(lf[18],C_text("main#host-bindir"));
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k7802 in k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in k2394 in k2390 in k2386 in k2382 in k2378 in ... */
static void C_ccall f_7804(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7804,c,av);}
a=C_alloc(3);
t2=C_eqp(t1,lf[34]);
t3=lf[35] /* main#aix */ =t2;;
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3816,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csc.scm:73: chicken.platform#software-version */
t5=C_fast_retrieve(lf[247]);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* k7806 in k7810 in k7814 in k3788 in k2506 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in k2394 in k2390 in k2386 in k2382 in k2378 in k2374 in ... */
static void C_ccall f_7808(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7808,c,av);}
a=C_alloc(3);
t2=C_eqp(t1,lf[32]);
t3=lf[33] /* main#cygwin */ =t2;;
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7804,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csc.scm:70: chicken.platform#build-platform */
t5=C_fast_retrieve(lf[373]);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* k7810 in k7814 in k3788 in k2506 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in k2394 in k2390 in k2386 in k2382 in k2378 in k2374 in k2370 in ... */
static void C_ccall f_7812(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7812,c,av);}
a=C_alloc(3);
t2=C_eqp(t1,lf[30]);
t3=lf[31] /* main#osx */ =t2;;
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7808,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csc.scm:69: chicken.platform#software-version */
t5=C_fast_retrieve(lf[247]);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* k7814 in k3788 in k2506 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in k2394 in k2390 in k2386 in k2382 in k2378 in k2374 in k2370 in k2366 in ... */
static void C_ccall f_7816(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7816,c,av);}
a=C_alloc(3);
t2=C_eqp(t1,lf[28]);
t3=lf[29] /* main#mingw */ =t2;;
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7812,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* csc.scm:68: chicken.platform#software-version */
t5=C_fast_retrieve(lf[247]);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* k7825 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in k2394 in k2390 in k2386 in k2382 in k2378 in k2374 in k2370 in k2366 in k2362 in k7860 in ... */
static void C_ccall f_7827(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_7827,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7830,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(t1)){
/* egg-environment.scm:121: chicken.pathname#make-pathname */
t3=C_fast_retrieve(lf[132]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=lf[431];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}
else{
/* egg-environment.scm:122: chicken.process-context#current-directory */
t3=C_fast_retrieve(lf[432]);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k7828 in k7825 in k2503 in k2434 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in k2394 in k2390 in k2386 in k2382 in k2378 in k2374 in k2370 in k2366 in k2362 in ... */
static void C_ccall f_7830(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7830,c,av);}
/* egg-environment.scm:121: chicken.pathname#make-pathname */
t2=C_fast_retrieve(lf[132]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=lf[431];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k7835 in k2430 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in k2394 in k2390 in k2386 in k2382 in k2378 in k2374 in k2370 in k2366 in k2362 in k7860 in k2354 in k2350 in ... */
static void C_ccall f_7837(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_7837,c,av);}
/* egg-environment.scm:97: scheme#string-append */
t2=*((C_word*)lf[76]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_retrieve2(lf[11],C_text("main#default-runlibdir"));
av2[3]=lf[435];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k7840 in k2426 in k2422 in k2418 in k2414 in k2410 in k2406 in k2402 in k2398 in k2394 in k2390 in k2386 in k2382 in k2378 in k2374 in k2370 in k2366 in k2362 in k7860 in k2354 in k2350 in k2346 in ... */
static void C_ccall f_7842(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_7842,c,av);}
/* egg-environment.scm:94: scheme#string-append */
t2=*((C_word*)lf[76]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_retrieve2(lf[10],C_text("main#default-libdir"));
av2[3]=lf[437];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k7845 in k2374 in k2370 in k2366 in k2362 in k7860 in k2354 in k2350 in k2346 in k2342 in k2338 in k2334 in k2330 in k2326 in k2322 in k2318 in k2314 in k2310 in k2306 in k2302 in k2296 in k2290 in ... */
static void C_ccall f_7847(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7847,c,av);}
/* egg-environment.scm:77: chicken.pathname#make-pathname */
t2=C_fast_retrieve(lf[132]);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_retrieve2(lf[13],C_text("main#default-bindir"));
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k7849 in k2370 in k2366 in k2362 in k7860 in k2354 in k2350 in k2346 in k2342 in k2338 in k2334 in k2330 in k2326 in k2322 in k2318 in k2314 in k2310 in k2306 in k2302 in k2296 in k2290 in k2287 in ... */
static void C_ccall f_7851(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_7851,c,av);}
/* egg-environment.scm:74: scheme#string-append */
t2=*((C_word*)lf[76]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_retrieve2(lf[13],C_text("main#default-bindir"));
av2[3]=lf[438];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k7853 in k2366 in k2362 in k7860 in k2354 in k2350 in k2346 in k2342 in k2338 in k2334 in k2330 in k2326 in k2322 in k2318 in k2314 in k2310 in k2306 in k2302 in k2296 in k2290 in k2287 in k2284 in ... */
static void C_ccall f_7855(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_7855,c,av);}
/* egg-environment.scm:71: scheme#string-append */
t2=*((C_word*)lf[76]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_retrieve2(lf[13],C_text("main#default-bindir"));
av2[3]=lf[439];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k7860 in k2354 in k2350 in k2346 in k2342 in k2338 in k2334 in k2330 in k2326 in k2322 in k2318 in k2314 in k2310 in k2306 in k2302 in k2296 in k2290 in k2287 in k2284 in k2281 in k2278 in k2275 in ... */
static void C_ccall f_7862(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_7862,c,av);}
a=C_alloc(6);
t2=C_eqp(t1,lf[15]);
t3=(C_truep(t2)?C_mk_bool(C_WINDOWS_SHELL):lf[16]);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2364,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t5=*((C_word*)lf[46]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=C_mpointer(&a,(void*)C_INSTALL_PREFIX);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* a7863 in k2287 in k2284 in k2281 in k2278 in k2275 in k2272 in k2269 in k2266 */
static void C_ccall f_7864(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_7864,c,av);}
/* csc.scm:28: ##sys#register-compiled-module */
{C_proc tp=(C_proc)C_fast_retrieve_symbol_proc(lf[440]);
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=*((C_word*)lf[440]+1);
av2[1]=t1;
av2[2]=lf[441];
av2[3]=lf[441];
av2[4]=C_SCHEME_END_OF_LIST;
av2[5]=C_SCHEME_END_OF_LIST;
av2[6]=C_SCHEME_END_OF_LIST;
av2[7]=C_SCHEME_END_OF_LIST;
tp(8,av2);}}

/* toplevel */
static C_TLS int toplevel_initialized=0;
C_main_entry_point

void C_ccall C_toplevel(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(toplevel_initialized) {C_kontinue(t1,C_SCHEME_UNDEFINED);}
else C_toplevel_entry(C_text("toplevel"));
C_check_nursery_minimum(C_calculate_demand(3,c,2));
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void*)C_toplevel,c,av);}
toplevel_initialized=1;
if(C_unlikely(!C_demand_2(2241))){
C_save(t1);
C_rereclaim2(2241*sizeof(C_word),1);
t1=C_restore;}
a=C_alloc(3);
C_initialize_lf(lf,443);
lf[0]=C_h_intern(&lf[0],5, C_text("main#"));
lf[15]=C_h_intern(&lf[15],7, C_text("windows"));
lf[16]=C_h_intern(&lf[16],4, C_text("unix"));
lf[28]=C_h_intern(&lf[28],7, C_text("mingw32"));
lf[30]=C_h_intern(&lf[30],6, C_text("macosx"));
lf[32]=C_h_intern(&lf[32],6, C_text("cygwin"));
lf[34]=C_h_intern(&lf[34],3, C_text("aix"));
lf[37]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\005\001linux\376\003\000\000\002\376\001\000\000\006\001netbsd\376\003\000\000\002\376\001\000\000\007\001freebsd\376\003\000\000\002\376\001\000\000\007\001solaris\376\003\000\000\002\376\001\000\000\007\001"
"openbsd\376\003\000\000\002\376\001\000\000\004\001hurd\376\003\000\000\002\376\001\000\000\005\001haiku\376\377\016"));
lf[39]=C_h_intern(&lf[39],20, C_text("##sys#standard-error"));
lf[40]=C_h_intern(&lf[40],7, C_text("fprintf"));
lf[41]=C_h_intern(&lf[41],17, C_text("chicken.base#exit"));
lf[42]=C_h_intern(&lf[42],18, C_text("##sys#write-char-0"));
lf[43]=C_h_intern(&lf[43],22, C_text("chicken.format#fprintf"));
lf[44]=C_h_intern(&lf[44],11, C_text("##sys#print"));
lf[45]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002: "));
lf[46]=C_h_intern(&lf[46],19, C_text("##sys#peek-c-string"));
lf[48]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005-host"));
lf[51]=C_h_intern(&lf[51],18, C_text("chicken.process#qs"));
lf[52]=C_h_intern(&lf[52],31, C_text("chicken.string#string-translate"));
lf[53]=C_h_intern(&lf[53],35, C_text("chicken.pathname#normalize-pathname"));
lf[62]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003obj"));
lf[63]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001o"));
lf[66]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001a"));
lf[68]=C_decode_literal(C_heaptop,C_text("\376B\000\000\000"));
lf[70]=C_h_intern(&lf[70],28, C_text("##sys#load-dynamic-extension"));
lf[76]=C_h_intern(&lf[76],20, C_text("scheme#string-append"));
lf[77]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003lib"));
lf[110]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\014/usr/include\376\003\000\000\002\376B\000\000\000\376\377\016"));
lf[127]=C_h_intern(&lf[127],32, C_text("chicken.platform#repository-path"));
lf[129]=C_h_intern(&lf[129],22, C_text("chicken.load#find-file"));
lf[130]=C_h_intern(&lf[130],16, C_text("##sys#setup-mode"));
lf[131]=C_h_intern(&lf[131],25, C_text("chicken.file#file-exists\077"));
lf[132]=C_h_intern(&lf[132],30, C_text("chicken.pathname#make-pathname"));
lf[135]=C_h_intern(&lf[135],3, C_text("map"));
lf[136]=C_h_intern(&lf[136],33, C_text("chicken.string#string-intersperse"));
lf[137]=C_h_intern(&lf[137],13, C_text("scheme#append"));
lf[140]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002-l"));
lf[142]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\377\012\000\000\134\376\003\000\000\002\376\377\012\000\000#\376\377\016"));
lf[144]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001\042"));
lf[145]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001\042"));
lf[146]=C_h_intern(&lf[146],32, C_text("chicken.string#string-translate\052"));
lf[147]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376B\000\000\001\042\376B\000\000\002\134\042\376\377\016"));
lf[148]=C_h_intern(&lf[148],18, C_text("##sys#list->string"));
lf[149]=C_h_intern(&lf[149],18, C_text("##sys#string->list"));
lf[152]=C_h_intern(&lf[152],21, C_text("##sys#standard-output"));
lf[153]=C_h_intern(&lf[153],6, C_text("printf"));
lf[154]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002: "));
lf[155]=C_decode_literal(C_heaptop,C_text("\376B\000\000;\012Error: shell command terminated with non-zero exit status "));
lf[156]=C_h_intern(&lf[156],22, C_text("chicken.process#system"));
lf[157]=C_h_intern(&lf[157],18, C_text("chicken.base#print"));
lf[158]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001\042"));
lf[159]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001\042"));
lf[160]=C_decode_literal(C_heaptop,C_text("\376B\000\000\037/usr/bin/env DYLD_LIBRARY_PATH="));
lf[161]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001 "));
lf[162]=C_h_intern(&lf[162],48, C_text("chicken.process-context#get-environment-variable"));
lf[163]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021DYLD_LIBRARY_PATH"));
lf[165]=C_h_intern(&lf[165],24, C_text("chicken.file#delete-file"));
lf[166]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003rm "));
lf[167]=C_h_intern(&lf[167],34, C_text("chicken.base#implicit-exit-handler"));
lf[168]=C_decode_literal(C_heaptop,C_text("\376B\000\000#not enough arguments to option `~A\047"));
lf[169]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\013-dynamiclib\376\377\016"));
lf[170]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\007-bundle\376\003\000\000\002\376B\000\000\034-headerpad_max_install_names\376\377\016"));
lf[171]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\007-shared\376\377\016"));
lf[172]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\012-DC_SHARED\376\377\016"));
lf[173]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010-feature"));
lf[174]=C_decode_literal(C_heaptop,C_text("\376B\000\000\026chicken-compile-shared"));
lf[175]=C_h_intern(&lf[175],43, C_text("chicken.pathname#pathname-replace-extension"));
lf[176]=C_h_intern(&lf[176],8, C_text("for-each"));
lf[177]=C_h_intern(&lf[177],6, C_text("format"));
lf[178]=C_h_intern(&lf[178],30, C_text("chicken.base#get-output-string"));
lf[179]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005mac.r"));
lf[180]=C_decode_literal(C_heaptop,C_text("\376B\000\000 /Developer/Tools/Rez -t APPL -o "));
lf[181]=C_h_intern(&lf[181],31, C_text("chicken.base#open-output-string"));
lf[182]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011 -change "));
lf[183]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007.dylib "));
lf[184]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001 "));
lf[185]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020@executable_path"));
lf[186]=C_h_intern(&lf[186],19, C_text("##sys#string-append"));
lf[187]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006.dylib"));
lf[188]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003-o "));
lf[189]=C_h_intern(&lf[189],14, C_text("scheme#reverse"));
lf[190]=C_h_intern(&lf[190],23, C_text("chicken.string#->string"));
lf[191]=C_decode_literal(C_heaptop,C_text("\376B\000\000#could not find linked extension: ~A"));
lf[192]=C_h_intern(&lf[192],27, C_text("scheme#with-input-from-file"));
lf[193]=C_h_intern(&lf[193],11, C_text("scheme#read"));
lf[194]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004link"));
lf[195]=C_h_intern(&lf[195],27, C_text("chicken.string#string-chomp"));
lf[196]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007.static"));
lf[197]=C_h_intern(&lf[197],41, C_text("chicken.pathname#pathname-strip-extension"));
lf[198]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004.old"));
lf[199]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004move"));
lf[200]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002mv"));
lf[201]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005.old\047"));
lf[202]=C_decode_literal(C_heaptop,C_text("\376B\000\000\030\047 - renaming source to `"));
lf[203]=C_decode_literal(C_heaptop,C_text("\376B\000\0001Warning: output file will overwrite source file `"));
lf[204]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002-c"));
lf[205]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003g++"));
lf[206]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022-Wno-write-strings"));
lf[207]=C_decode_literal(C_heaptop,C_text("\376B\000\000\000"));
lf[208]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003-o "));
lf[209]=C_decode_literal(C_heaptop,C_text("\376B\000\000Pobject file generated from `~a\047 will overwrite explicitly given object file"
" `~a\047"));
lf[210]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001."));
lf[211]=C_decode_literal(C_heaptop,C_text("\376B\000\001\232\042\042 type=\042\042win32\042\042/>\134r\134n\042\012  \042  <ms_asmv2:trustInfo xmlns:ms_asmv2=\042\042urn:sche"
"mas-microsoft-com:asm.v2\042\042>\134r\134n\042\012  \042    <ms_asmv2:security>\134r\134n\042\012  \042      <ms_as"
"mv2:requestedPrivileges>\134r\134n\042\012  \042        <ms_asmv2:requestedExecutionLevel level"
"=\042\042asInvoker\042\042 uiAccess=\042\042false\042\042/>\134r\134n\042\012  \042      </ms_asmv2:requestedPrivileges"
">\134r\134n\042\012  \042    </ms_asmv2:security>\134r\134n\042\012  \042  </ms_asmv2:trustInfo>\134r\134n\042\012  \042</ass"
"embly>\134r\134n\042\012END"));
lf[212]=C_decode_literal(C_heaptop,C_text("\376B\000\001\0031 24 MOVEABLE PURE\012BEGIN\012  \042<\077xml version=\042\0421.0\042\042 encoding=\042\042UTF-8\042\042 standa"
"lone=\042\042yes\042\042\077>\134r\134n\042\012  \042<assembly xmlns=\042\042urn:schemas-microsoft-com:asm.v1\042\042 mani"
"festVersion=\042\0421.0\042\042>\134r\134n\042\012  \042  <assemblyIdentity version=\042\0421.0.0.0\042\042 processorAr"
"chitecture=\042\042\052\042\042 name=\042\042"));
lf[213]=C_h_intern(&lf[213],21, C_text("##sys#print-to-string"));
lf[214]=C_h_intern(&lf[214],26, C_text("scheme#with-output-to-file"));
lf[215]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013generating "));
lf[216]=C_h_intern(&lf[216],30, C_text("chicken.pathname#pathname-file"));
lf[217]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002rc"));
lf[218]=C_h_intern(&lf[218],30, C_text("chicken.platform#software-type"));
lf[219]=C_decode_literal(C_heaptop,C_text("\376B\000\000\031no source files specified"));
lf[220]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011bogus.scm"));
lf[221]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004link"));
lf[222]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001 "));
lf[223]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\003-:d\376\377\016"));
lf[224]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\010-feature\376\003\000\000\002\376B\000\000\025chicken-scheme-to-c++\376\377\016"));
lf[225]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\010-feature\376\003\000\000\002\376B\000\000\026chicken-scheme-to-objc\376\377\016"));
lf[226]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017-emit-link-file"));
lf[227]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017-emit-link-file"));
lf[228]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004link"));
lf[229]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\012-to-stdout\376\377\016"));
lf[230]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014-output-file"));
lf[231]=C_decode_literal(C_heaptop,C_text("\376B\000\000KC file generated from `~a\047 will overwrite explicitly given source file `~a\047"
));
lf[232]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003cpp"));
lf[233]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001m"));
lf[234]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001c"));
lf[235]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010-dynamic"));
lf[236]=C_decode_literal(C_heaptop,C_text("\376B\000\000Gthe `-c\047 option cannot be used in combination with multiple input files"));
lf[237]=C_h_intern(&lf[237],14, C_text("scheme#newline"));
lf[238]=C_h_intern(&lf[238],19, C_text("chicken.base#print\052"));
lf[239]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002-L"));
lf[240]=C_h_intern(&lf[240],27, C_text("chicken.string#string-split"));
lf[241]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002:;"));
lf[242]=C_decode_literal(C_heaptop,C_text("\376B\000\000\026CHICKEN_C_LIBRARY_PATH"));
lf[243]=C_h_intern(&lf[243],7, C_text("freebsd"));
lf[244]=C_h_intern(&lf[244],7, C_text("openbsd"));
lf[245]=C_h_intern(&lf[245],6, C_text("netbsd"));
lf[246]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015-Wl,-z,origin"));
lf[247]=C_h_intern(&lf[247],33, C_text("chicken.platform#software-version"));
lf[248]=C_h_intern(&lf[248],19, C_text("chicken.string#conc"));
lf[249]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006-Wl,-R"));
lf[250]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007$ORIGIN"));
lf[251]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002-L"));
lf[252]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007-Wl,-R\042"));
lf[253]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001\042"));
lf[254]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002-L"));
lf[255]=C_h_intern(&lf[255],5, C_text("-help"));
lf[256]=C_h_intern(&lf[256],6, C_text("--help"));
lf[257]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003\047.\012"));
lf[258]=C_decode_literal(C_heaptop,C_text("\376B\000\052+\047 is a driver program for the CHICKEN compiler. Files given on the\012  comman"
"d line are translated, compiled or linked as needed.\012\012  FILENAME is a Scheme sou"
"rce file name with optional extension or a\012  C/C++/Objective-C source, object or"
" library file name with extension. OPTION\012  may be one of the following:\012\012  Gene"
"ral options:\012\012    -h  -help                      display this text and exit\012    "
"-v  -verbose                   show compiler notes and tool-invocations\012    -vv "
"                           display information about translation\012               "
"                     progress\012    -vvv                           display informa"
"tion about all compilation\012                                    stages\012    -versi"
"on                       display Scheme compiler version and exit\012    -release  "
"                     display release number and exit\012\012  File and pathname option"
"s:\012\012    -o -output-file FILENAME       specifies target executable name\012    -I -"
"include-path PATHNAME      specifies alternative path for included\012             "
"                       files\012    -to-stdout                     write compiler t"
"o stdout (implies -t)\012    -s -shared -dynamic            generate dynamically lo"
"adable shared object\012                                    file\012\012  Language option"
"s:\012\012    -D  -DSYMBOL  -feature SYMBOL  register feature identifier\012    -no-featu"
"re SYMBOL             disable builtin feature identifier\012    -c++               "
"            compile via a C++ source file (.cpp) \012    -objc                     "
"     compile via Objective-C source file (.m)\012\012  Syntax related options:\012\012    -i"
" -case-insensitive           don\047t preserve case of read symbols    \012    -K -key"
"word-style STYLE        enable alternative keyword-syntax\012                      "
"              (prefix, suffix or none)\012       -no-parentheses-synonyms    disabl"
"es list delimiter synonyms\012       -no-symbol-escape           disables support f"
"or escaped symbols\012       -r5rs-syntax                disables the CHICKEN exten"
"sions to\012                                    R5RS syntax\012    -compile-syntax    "
"            macros are made available at run-time\012    -j -emit-import-library MO"
"DULE write compile-time module information into\012                                "
"    separate file\012    -J -emit-all-import-libraries  emit import-libraries for a"
"ll defined modules\012    -no-compiler-syntax            disable expansion of compi"
"ler-macros\012    -m -module NAME                wrap compiled code in a module\012   "
" -M -module-registration        always generate module registration code\012    -N "
"-no-module-registration     never generate module registration code\012            "
"                        (overrides `-M\047)\012\012  Translation options:\012\012    -x  -expli"
"cit-use              do not use units `library\047 and `eval\047 by\012                  "
"                  default\012    -P  -check-syntax              stop compilation af"
"ter macro-expansion\012    -A  -analyze-only              stop compilation after fi"
"rst analysis pass\012\012  Debugging options:\012\012    -w  -no-warnings               disa"
"ble warnings\012    -d0 -d1 -d2 -d3 -debug-level NUMBER\012                           "
"        set level of available debugging information\012    -no-trace              "
"        disable rudimentary debugging information\012    -debug-info               "
"     enable debug-information in compiled code for use\012                         "
"           with an external debugger\012    -profile                       executab"
"le emits profiling information \012    -accumulate-profile            executable em"
"its profiling information in\012                                    append mode\012   "
" -profile-name FILENAME         name of the generated profile information\012      "
"                              file\012    -consult-types-file FILENAME   load addit"
"ional type database\012\012  Optimization options:\012\012    -O -O0 -O1 -O2 -O3 -O4 -O5 -op"
"timize-level NUMBER\012                                   enable certain sets of op"
"timization options\012    -optimize-leaf-routines        enable leaf routine optimi"
"zation\012    -no-usual-integrations         standard procedures may be redefined\012 "
"   -u  -unsafe                    disable safety checks\012    -local              "
"           assume globals are only modified in current\012                         "
"           file\012    -b  -block                     enable block-compilation\012    "
"-disable-interrupts            disable interrupts in compiled code\012    -f  -fixn"
"um-arithmetic         assume all numbers are fixnums\012    -disable-stack-overflow"
"-checks disables detection of stack-overflows\012    -inline                       "
" enable inlining\012    -inline-limit LIMIT            set inlining threshold\012    -"
"inline-global                 enable cross-module inlining\012    -specialize      "
"              perform type-based specialization of primitive calls\012    -oi -emit"
"-inline-file FILENAME generate file with globally inlinable\012                    "
"                procedures (implies -inline -local)\012    -consult-inline-file FIL"
"ENAME  explicitly load inline file\012    -ot  -emit-types-file FILENAME write type"
"-declaration information into file\012    -no-argc-checks                disable ar"
"gument count checks\012    -no-bound-checks               disable bound variable ch"
"ecks\012    -no-procedure-checks           disable procedure call checks\012    -no-pr"
"ocedure-checks-for-usual-bindings\012                                   disable pro"
"cedure call checks only for usual\012                                    bindings\012 "
"   -no-procedure-checks-for-toplevel-bindings\012                                  "
" disable procedure call checks for toplevel\012                                    "
"bindings\012    -strict-types                  assume variable do not change their "
"type\012    -clustering                    combine groups of local procedures into "
"dispatch\012                                     loop\012    -lfa2                    "
"      perform additional lightweight flow-analysis pass\012    -unroll-limit LIMIT "
"         specifies inlining limit for self-recursive calls\012\012  Configuration opti"
"ons:\012\012    -unit NAME                     compile file as a library unit\012    -use"
"s NAME                     declare library unit as used.\012    -heap-size NUMBER  "
"            specifies heap-size of compiled executable\012    -nursery NUMBER  -sta"
"ck-size NUMBER\012                                   specifies nursery size of comp"
"iled\012                                   executable\012    -X -extend FILENAME      "
"      load file before compilation commences\012    -prelude EXPRESSION            "
"add expression to beginning of source file\012    -postlude EXPRESSION           ad"
"d expression to end of source file\012    -prologue FILENAME             include fi"
"le before main source file\012    -epilogue FILENAME             include file after"
" main source file\012\012    -e  -embedded                  compile as embedded\012      "
"                              (don\047t generate `main()\047)\012    -gui                "
"           compile as GUI application\012    -link NAME                     link ex"
"tension with compiled executable\012                                    (implies -u"
"ses)\012    -R  -require-extension NAME    require extension and import in compiled"
"\012                                    code\012    -dll -library                  com"
"pile multiple units into a dynamic\012                                    library\012 "
"   -libdir DIRECTORY              override directory for runtime library\012\012  Opti"
"ons to other passes:\012\012    -C OPTION                      pass option to C compil"
"er\012    -L OPTION                      pass option to linker\012    -I<DIR>         "
"               pass \134\042-I<DIR>\134\042 to C compiler\012                                  "
"  (add include path)\012    -L<DIR>                        pass \134\042-L<DIR>\134\042 to link"
"er\012                                    (add library path)\012    -k                "
"             keep intermediate files\012    -c                             stop aft"
"er compilation to object files\012    -t                             stop after tra"
"nslation to C\012    -cc COMPILER                   select other C compiler than th"
"e default\012    -cxx COMPILER                  select other C++ compiler than the "
"default\012    -ld COMPILER                   select other linker than the default "
"\012    -static                        link with static CHICKEN libraries and\012     "
"                               extensions (if possible)\012    -F<DIR>             "
"           pass \134\042-F<DIR>\134\042 to C compiler\012                                    (a"
"dd framework header path on Mac OS X)\012    -framework NAME                passed "
"to linker on Mac OS X\012    -rpath PATHNAME                add directory to runtim"
"e library search path\012    -Wl,...                        pass linker options\012   "
" -strip                         strip resulting binary\012\012  Inquiry options:\012\012    "
"-home                          show home-directory (where support files go)\012    "
"-cflags                        show required C-compiler flags and exit\012    -ldfl"
"ags                       show required linker flags and exit\012    -libs         "
"                 show required libraries and exit\012    -cc-name                  "
"     show name of default C compiler used\012    -cxx-name                      sho"
"w name of default C++ compiler used\012    -ld-name                       show name"
" of default linker used\012    -dry-run                       just show commands ex"
"ecuted, don\047t run them\012                                    (implies `-v\047)\012\012  Obs"
"cure options:\012\012    -debug MODES                   display debugging output for t"
"he given modes\012    -compiler PATHNAME             use other compiler than defaul"
"t `chicken\047\012    -raw                           do not generate implicit init- an"
"d exit code\012    -emit-external-prototypes-first\012                                "
"   emit prototypes for callbacks before foreign\012                                "
"    declarations\012    -regenerate-import-libraries   emit import libraries even w"
"hen unchanged\012    -ignore-repository             do not refer to repository for "
"extensions\012    -keep-shadowed-macros          do not remove shadowed macro\012    -"
"host                          compile for host when configured for\012             "
"                       cross-compiling\012    -private-repository            load e"
"xtensions from executable path\012    -deployed                      link support f"
"ile to be used from a deployed \012                                    executable ("
"sets `rpath\047 accordingly, if supported\012                                    on th"
"is platform)\012    -no-elevation                  embed manifest on Windows to sup"
"ress elevation\012                                    warnings for programs named `"
"install\047 or `setup\047\012\012  Options can be collapsed if unambiguous, so\012\012    -vkfO\012\012 "
" is the same as\012\012    -v -k -fixnum-arithmetic -optimize\012\012  The contents of the e"
"nvironment variable CSC_OPTIONS are implicitly passed to\012  every invocation of `"
));
lf[259]=C_decode_literal(C_heaptop,C_text("\376B\000\000! [OPTION ...] [FILENAME ...]\012\012  `"));
lf[260]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007Usage: "));
lf[261]=C_h_intern(&lf[261],8, C_text("-release"));
lf[262]=C_h_intern(&lf[262],32, C_text("chicken.platform#chicken-version"));
lf[263]=C_h_intern(&lf[263],8, C_text("-version"));
lf[264]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011 -version"));
lf[265]=C_h_intern(&lf[265],4, C_text("-c++"));
lf[266]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017-no-cpp-precomp"));
lf[267]=C_h_intern(&lf[267],5, C_text("-objc"));
lf[268]=C_h_intern(&lf[268],7, C_text("-static"));
lf[269]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007-static"));
lf[270]=C_h_intern(&lf[270],7, C_text("-cflags"));
lf[271]=C_h_intern(&lf[271],8, C_text("-ldflags"));
lf[272]=C_h_intern(&lf[272],8, C_text("-cc-name"));
lf[273]=C_h_intern(&lf[273],9, C_text("-cxx-name"));
lf[274]=C_h_intern(&lf[274],8, C_text("-ld-name"));
lf[275]=C_h_intern(&lf[275],5, C_text("-home"));
lf[276]=C_h_intern(&lf[276],5, C_text("-libs"));
lf[277]=C_h_intern(&lf[277],2, C_text("-v"));
lf[278]=C_h_intern(&lf[278],8, C_text("-verbose"));
lf[279]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010-verbose"));
lf[280]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002-v"));
lf[281]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002-v"));
lf[282]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002-Q"));
lf[283]=C_h_intern(&lf[283],2, C_text("-w"));
lf[284]=C_h_intern(&lf[284],12, C_text("-no-warnings"));
lf[285]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002-w"));
lf[286]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014-no-warnings"));
lf[287]=C_h_intern(&lf[287],2, C_text("-A"));
lf[288]=C_h_intern(&lf[288],13, C_text("-analyze-only"));
lf[289]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015-analyze-only"));
lf[290]=C_h_intern(&lf[290],2, C_text("-P"));
lf[291]=C_h_intern(&lf[291],13, C_text("-check-syntax"));
lf[292]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015-check-syntax"));
lf[293]=C_h_intern(&lf[293],2, C_text("-k"));
lf[294]=C_h_intern(&lf[294],2, C_text("-c"));
lf[295]=C_h_intern(&lf[295],2, C_text("-t"));
lf[296]=C_h_intern(&lf[296],2, C_text("-e"));
lf[297]=C_h_intern(&lf[297],9, C_text("-embedded"));
lf[298]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014-DC_EMBEDDED"));
lf[299]=C_h_intern(&lf[299],5, C_text("-link"));
lf[300]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002, "));
lf[301]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005-uses"));
lf[302]=C_h_intern(&lf[302],7, C_text("-libdir"));
lf[303]=C_h_intern(&lf[303],18, C_text("-require-extension"));
lf[304]=C_h_intern(&lf[304],2, C_text("-R"));
lf[305]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022-require-extension"));
lf[306]=C_h_intern(&lf[306],19, C_text("-private-repository"));
lf[307]=C_decode_literal(C_heaptop,C_text("\376B\000\000\026-DC_PRIVATE_REPOSITORY"));
lf[308]=C_h_intern(&lf[308],18, C_text("-ignore-repository"));
lf[309]=C_h_intern(&lf[309],11, C_text("-setup-mode"));
lf[310]=C_h_intern(&lf[310],13, C_text("-no-elevation"));
lf[311]=C_h_intern(&lf[311],4, C_text("-gui"));
lf[312]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007-DC_GUI"));
lf[313]=C_decode_literal(C_heaptop,C_text("\376B\000\000\012-lkernel32"));
lf[314]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010-luser32"));
lf[315]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007-lgdi32"));
lf[316]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011-mwindows"));
lf[317]=C_decode_literal(C_heaptop,C_text("\376B\000\000\012chicken.rc"));
lf[318]=C_h_intern(&lf[318],9, C_text("-deployed"));
lf[319]=C_h_intern(&lf[319],10, C_text("-framework"));
lf[320]=C_decode_literal(C_heaptop,C_text("\376B\000\000\012-framework"));
lf[321]=C_h_intern(&lf[321],2, C_text("-o"));
lf[322]=C_h_intern(&lf[322],12, C_text("-output-file"));
lf[323]=C_h_intern(&lf[323],2, C_text("-O"));
lf[324]=C_h_intern(&lf[324],3, C_text("-O1"));
lf[325]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017-optimize-level"));
lf[326]=C_decode_literal(C_heaptop,C_text("\376B\000\000\0011"));
lf[327]=C_h_intern(&lf[327],3, C_text("-O0"));
lf[328]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017-optimize-level"));
lf[329]=C_decode_literal(C_heaptop,C_text("\376B\000\000\0010"));
lf[330]=C_h_intern(&lf[330],3, C_text("-O2"));
lf[331]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017-optimize-level"));
lf[332]=C_decode_literal(C_heaptop,C_text("\376B\000\000\0012"));
lf[333]=C_h_intern(&lf[333],3, C_text("-O3"));
lf[334]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017-optimize-level"));
lf[335]=C_decode_literal(C_heaptop,C_text("\376B\000\000\0013"));
lf[336]=C_h_intern(&lf[336],3, C_text("-O4"));
lf[337]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017-optimize-level"));
lf[338]=C_decode_literal(C_heaptop,C_text("\376B\000\000\0014"));
lf[339]=C_h_intern(&lf[339],3, C_text("-O5"));
lf[340]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017-optimize-level"));
lf[341]=C_decode_literal(C_heaptop,C_text("\376B\000\000\0015"));
lf[342]=C_h_intern(&lf[342],3, C_text("-d0"));
lf[343]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014-debug-level"));
lf[344]=C_decode_literal(C_heaptop,C_text("\376B\000\000\0010"));
lf[345]=C_h_intern(&lf[345],3, C_text("-d1"));
lf[346]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014-debug-level"));
lf[347]=C_decode_literal(C_heaptop,C_text("\376B\000\000\0011"));
lf[348]=C_h_intern(&lf[348],3, C_text("-d2"));
lf[349]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014-debug-level"));
lf[350]=C_decode_literal(C_heaptop,C_text("\376B\000\000\0012"));
lf[351]=C_h_intern(&lf[351],3, C_text("-d3"));
lf[352]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014-debug-level"));
lf[353]=C_decode_literal(C_heaptop,C_text("\376B\000\000\0013"));
lf[354]=C_h_intern(&lf[354],6, C_text("-debug"));
lf[355]=C_h_intern(&lf[355],8, C_text("-dry-run"));
lf[356]=C_h_intern(&lf[356],2, C_text("-s"));
lf[357]=C_h_intern(&lf[357],4, C_text("-dll"));
lf[358]=C_h_intern(&lf[358],8, C_text("-library"));
lf[359]=C_h_intern(&lf[359],9, C_text("-compiler"));
lf[360]=C_h_intern(&lf[360],3, C_text("-cc"));
lf[361]=C_h_intern(&lf[361],4, C_text("-cxx"));
lf[362]=C_h_intern(&lf[362],3, C_text("-ld"));
lf[363]=C_h_intern(&lf[363],2, C_text("-I"));
lf[364]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015-include-path"));
lf[365]=C_h_intern(&lf[365],2, C_text("-C"));
lf[366]=C_h_intern(&lf[366],6, C_text("-strip"));
lf[367]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002-s"));
lf[368]=C_h_intern(&lf[368],2, C_text("-L"));
lf[369]=C_h_intern(&lf[369],6, C_text("-rpath"));
lf[370]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006-Wl,-R"));
lf[371]=C_h_intern(&lf[371],3, C_text("gnu"));
lf[372]=C_h_intern(&lf[372],5, C_text("clang"));
lf[373]=C_h_intern(&lf[373],31, C_text("chicken.platform#build-platform"));
lf[374]=C_h_intern(&lf[374],5, C_text("-host"));
lf[375]=C_h_intern(&lf[375],3, C_text("-oi"));
lf[376]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021-emit-inline-file"));
lf[377]=C_h_intern(&lf[377],3, C_text("-ot"));
lf[378]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020-emit-types-file"));
lf[379]=C_h_intern(&lf[379],1, C_text("-"));
lf[380]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001a"));
lf[381]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\001-\376\377\016"));
lf[382]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\002\001-h\376\003\000\000\002\376B\000\000\005-help\376\377\016\376\003\000\000\002\376\003\000\000\002\376\001\000\000\002\001-s\376\003\000\000\002\376B\000\000\007-shared\376\377\016\376\003\000\000\002\376"
"\003\000\000\002\376\001\000\000\002\001-m\376\003\000\000\002\376B\000\000\007-module\376\377\016\376\003\000\000\002\376\003\000\000\002\376\001\000\000\002\001-P\376\003\000\000\002\376B\000\000\015-check-syntax\376\377\016\376\003\000\000"
"\002\376\003\000\000\002\376\001\000\000\002\001-f\376\003\000\000\002\376B\000\000\022-fixnum-arithmetic\376\377\016\376\003\000\000\002\376\003\000\000\002\376\001\000\000\002\001-D\376\003\000\000\002\376B\000\000\010-featur"
"e\376\377\016\376\003\000\000\002\376\003\000\000\002\376\016\000\000\002\376\377\001\000\000\000\000\376\377\001\377\377\377\377\376\003\000\000\002\376B\000\000\021-case-insensitive\376\377\016\376\003\000\000\002\376\003\000\000\002\376\001\000\000\002\001-"
"K\376\003\000\000\002\376B\000\000\016-keyword-style\376\377\016\376\003\000\000\002\376\003\000\000\002\376\001\000\000\002\001-X\376\003\000\000\002\376B\000\000\007-extend\376\377\016\376\003\000\000\002\376\003\000\000\002\376\001\000\000"
"\002\001-J\376\003\000\000\002\376B\000\000\032-emit-all-import-libraries\376\377\016\376\003\000\000\002\376\003\000\000\002\376\001\000\000\002\001-M\376\003\000\000\002\376B\000\000\024-module-r"
"egistration\376\377\016\376\003\000\000\002\376\003\000\000\002\376\001\000\000\002\001-N\376\003\000\000\002\376B\000\000\027-no-module-registration\376\377\016\376\003\000\000\002\376\003\000\000\002\376\001"
"\000\000\002\001-x\376\003\000\000\002\376B\000\000\015-explicit-use\376\377\016\376\003\000\000\002\376\003\000\000\002\376\001\000\000\002\001-u\376\003\000\000\002\376B\000\000\007-unsafe\376\377\016\376\003\000\000\002\376\003\000\000\002"
"\376\001\000\000\002\001-j\376\003\000\000\002\376B\000\000\024-emit-import-library\376\377\016\376\003\000\000\002\376\003\000\000\002\376\001\000\000\002\001-b\376\003\000\000\002\376B\000\000\006-block\376\377\016\376\003"
"\000\000\002\376\003\000\000\002\376\001\000\000\006\001-types\376\003\000\000\002\376B\000\000\023-consult-types-file\376\377\016\376\377\016"));
lf[383]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\015\001-explicit-use\376\003\000\000\002\376\001\000\000\011\001-no-trace\376\003\000\000\002\376\001\000\000\014\001-no-warnings\376\003\000\000\002\376\001\000\000\026\001-n"
"o-usual-integrations\376\003\000\000\002\376\001\000\000\027\001-optimize-leaf-routines\376\003\000\000\002\376\001\000\000\007\001-unsafe\376\003\000\000\002\376\001\000"
"\000\006\001-block\376\003\000\000\002\376\001\000\000\023\001-disable-interrupts\376\003\000\000\002\376\001\000\000\022\001-fixnum-arithmetic\376\003\000\000\002\376\001\000\000\012\001-"
"to-stdout\376\003\000\000\002\376\001\000\000\010\001-profile\376\003\000\000\002\376\001\000\000\004\001-raw\376\003\000\000\002\376\001\000\000\023\001-accumulate-profile\376\003\000\000\002\376\001"
"\000\000\015\001-check-syntax\376\003\000\000\002\376\001\000\000\021\001-case-insensitive\376\003\000\000\002\376\001\000\000\007\001-shared\376\003\000\000\002\376\001\000\000\017\001-compi"
"le-syntax\376\003\000\000\002\376\001\000\000\017\001-no-lambda-info\376\003\000\000\002\376\001\000\000\010\001-dynamic\376\003\000\000\002\376\001\000\000\036\001-disable-stack-"
"overflow-checks\376\003\000\000\002\376\001\000\000\006\001-local\376\003\000\000\002\376\001\000\000\037\001-emit-external-prototypes-first\376\003\000\000\002\376"
"\001\000\000\007\001-inline\376\003\000\000\002\376\001\000\000\010\001-release\376\003\000\000\002\376\001\000\000\015\001-analyze-only\376\003\000\000\002\376\001\000\000\025\001-keep-shadowed"
"-macros\376\003\000\000\002\376\001\000\000\016\001-inline-global\376\003\000\000\002\376\001\000\000\022\001-ignore-repository\376\003\000\000\002\376\001\000\000\021\001-no-symb"
"ol-escape\376\003\000\000\002\376\001\000\000\030\001-no-parentheses-synonyms\376\003\000\000\002\376\001\000\000\014\001-r5rs-syntax\376\003\000\000\002\376\001\000\000\017\001-n"
"o-argc-checks\376\003\000\000\002\376\001\000\000\020\001-no-bound-checks\376\003\000\000\002\376\001\000\000\024\001-no-procedure-checks\376\003\000\000\002\376\001\000\000"
"\023\001-no-compiler-syntax\376\003\000\000\002\376\001\000\000\032\001-emit-all-import-libraries\376\003\000\000\002\376\001\000\000\015\001-no-elevati"
"on\376\003\000\000\002\376\001\000\000\024\001-module-registration\376\003\000\000\002\376\001\000\000\027\001-no-module-registration\376\003\000\000\002\376\001\000\000\047\001-n"
"o-procedure-checks-for-usual-bindings\376\003\000\000\002\376\001\000\000\034\001-regenerate-import-libraries\376\003\000\000"
"\002\376\001\000\000\013\001-specialize\376\003\000\000\002\376\001\000\000\015\001-strict-types\376\003\000\000\002\376\001\000\000\013\001-clustering\376\003\000\000\002\376\001\000\000\005\001-lfa2"
"\376\003\000\000\002\376\001\000\000\013\001-debug-info\376\003\000\000\002\376\001\000\000\052\001-no-procedure-checks-for-toplevel-bindings\376\377\016"));
lf[384]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\006\001-debug\376\003\000\000\002\376\001\000\000\012\001-heap-size\376\003\000\000\002\376\001\000\000\010\001-nursery\376\003\000\000\002\376\001\000\000\013\001-stack-size\376"
"\003\000\000\002\376\001\000\000\011\001-compiler\376\003\000\000\002\376\001\000\000\005\001-unit\376\003\000\000\002\376\001\000\000\005\001-uses\376\003\000\000\002\376\001\000\000\016\001-keyword-style\376\003\000\000"
"\002\376\001\000\000\017\001-optimize-level\376\003\000\000\002\376\001\000\000\015\001-include-path\376\003\000\000\002\376\001\000\000\016\001-database-size\376\003\000\000\002\376\001\000\000"
"\007\001-extend\376\003\000\000\002\376\001\000\000\010\001-prelude\376\003\000\000\002\376\001\000\000\011\001-postlude\376\003\000\000\002\376\001\000\000\011\001-prologue\376\003\000\000\002\376\001\000\000\011\001-"
"epilogue\376\003\000\000\002\376\001\000\000\017\001-emit-link-file\376\003\000\000\002\376\001\000\000\015\001-inline-limit\376\003\000\000\002\376\001\000\000\015\001-profile-na"
"me\376\003\000\000\002\376\001\000\000\015\001-unroll-limit\376\003\000\000\002\376\001\000\000\021\001-emit-inline-file\376\003\000\000\002\376\001\000\000\024\001-consult-inline"
"-file\376\003\000\000\002\376\001\000\000\020\001-emit-types-file\376\003\000\000\002\376\001\000\000\023\001-consult-types-file\376\003\000\000\002\376\001\000\000\010\001-featur"
"e\376\003\000\000\002\376\001\000\000\014\001-debug-level\376\003\000\000\002\376\001\000\000\024\001-emit-import-library\376\003\000\000\002\376\001\000\000\007\001-module\376\003\000\000\002\376\001"
"\000\000\005\001-link\376\003\000\000\002\376\001\000\000\013\001-no-feature\376\377\016"));
lf[385]=C_h_intern(&lf[385],18, C_text("chicken.base#error"));
lf[386]=C_decode_literal(C_heaptop,C_text("\376B\000\000-bad -L argument, <DIR> starts with whitespace"));
lf[387]=C_decode_literal(C_heaptop,C_text("\376B\000\000-bad -I argument: <DIR> starts with whitespace"));
lf[388]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010-feature"));
lf[389]=C_h_intern(&lf[389],16, C_text("scheme#substring"));
lf[390]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001-"));
lf[391]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023invalid option `~A\047"));
lf[392]=C_h_intern(&lf[392],5, C_text("foldr"));
lf[393]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023invalid option `~A\047"));
lf[394]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004-Wl,"));
lf[395]=C_h_intern(&lf[395],35, C_text("chicken.pathname#decompose-pathname"));
lf[396]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001h"));
lf[397]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001c"));
lf[398]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002rc"));
lf[399]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003cpp"));
lf[400]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001C"));
lf[401]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002cc"));
lf[402]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003cxx"));
lf[403]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003hpp"));
lf[404]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017-no-cpp-precomp"));
lf[405]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001m"));
lf[406]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001M"));
lf[407]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002mm"));
lf[408]=C_decode_literal(C_heaptop,C_text("\376B\000\000\030file `~A\047 does not exist"));
lf[409]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004.scm"));
lf[410]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002-:"));
lf[411]=C_h_intern(&lf[411],15, C_text("-optimize-level"));
lf[412]=C_h_intern(&lf[412],15, C_text("-benchmark-mode"));
lf[413]=C_h_intern(&lf[413],10, C_text("-to-stdout"));
lf[414]=C_h_intern(&lf[414],7, C_text("-shared"));
lf[415]=C_h_intern(&lf[415],8, C_text("-dynamic"));
lf[416]=C_h_intern(&lf[416],21, C_text("scheme#string->symbol"));
lf[417]=C_decode_literal(C_heaptop,C_text("\376B\000\000\000"));
lf[418]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013CSC_OPTIONS"));
lf[419]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002-I"));
lf[420]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002:;"));
lf[421]=C_decode_literal(C_heaptop,C_text("\376B\000\000\026CHICKEN_C_INCLUDE_PATH"));
lf[422]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002-I"));
lf[423]=C_decode_literal(C_heaptop,C_text("\376B\000\000\030PHhsfiENxubvwAOeWkctgSJM"));
lf[424]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\005-DPIC\376\377\016"));
lf[425]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\005-fPIC\376\003\000\000\002\376B\000\000\005-DPIC\376\377\016"));
lf[426]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007static."));
lf[427]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007static."));
lf[428]=C_h_intern(&lf[428],25, C_text("chicken.platform#feature\077"));
lf[429]=C_h_intern_kw(&lf[429],13, C_text("cross-chicken"));
lf[430]=C_h_intern(&lf[430],46, C_text("chicken.process-context#command-line-arguments"));
lf[431]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017chicken-install"));
lf[432]=C_h_intern(&lf[432],41, C_text("chicken.process-context#current-directory"));
lf[433]=C_h_intern(&lf[433],39, C_text("chicken.platform#system-cache-directory"));
lf[434]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021CHICKEN_EGG_CACHE"));
lf[435]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011/chicken/"));
lf[436]=C_h_intern(&lf[436],20, C_text("##sys#fixnum->string"));
lf[437]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011/chicken/"));
lf[438]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001/"));
lf[439]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001/"));
lf[440]=C_h_intern(&lf[440],30, C_text("##sys#register-compiled-module"));
lf[441]=C_h_intern(&lf[441],4, C_text("main"));
lf[442]=C_h_intern(&lf[442],22, C_text("##sys#with-environment"));
C_register_lf2(lf,443,create_ptable());{}
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2268,a[2]=t1,tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_library_toplevel(2,av2);}}

#ifdef C_ENABLE_PTABLES
static C_PTABLE_ENTRY ptable[475] = {
{C_text("f8620:csc_2escm"),(void*)f8620},
{C_text("f8626:csc_2escm"),(void*)f8626},
{C_text("f8630:csc_2escm"),(void*)f8630},
{C_text("f8662:csc_2escm"),(void*)f8662},
{C_text("f8710:csc_2escm"),(void*)f8710},
{C_text("f9025:csc_2escm"),(void*)f9025},
{C_text("f9029:csc_2escm"),(void*)f9029},
{C_text("f_2268:csc_2escm"),(void*)f_2268},
{C_text("f_2271:csc_2escm"),(void*)f_2271},
{C_text("f_2274:csc_2escm"),(void*)f_2274},
{C_text("f_2277:csc_2escm"),(void*)f_2277},
{C_text("f_2280:csc_2escm"),(void*)f_2280},
{C_text("f_2283:csc_2escm"),(void*)f_2283},
{C_text("f_2286:csc_2escm"),(void*)f_2286},
{C_text("f_2289:csc_2escm"),(void*)f_2289},
{C_text("f_2292:csc_2escm"),(void*)f_2292},
{C_text("f_2298:csc_2escm"),(void*)f_2298},
{C_text("f_2304:csc_2escm"),(void*)f_2304},
{C_text("f_2308:csc_2escm"),(void*)f_2308},
{C_text("f_2312:csc_2escm"),(void*)f_2312},
{C_text("f_2316:csc_2escm"),(void*)f_2316},
{C_text("f_2320:csc_2escm"),(void*)f_2320},
{C_text("f_2324:csc_2escm"),(void*)f_2324},
{C_text("f_2328:csc_2escm"),(void*)f_2328},
{C_text("f_2332:csc_2escm"),(void*)f_2332},
{C_text("f_2336:csc_2escm"),(void*)f_2336},
{C_text("f_2340:csc_2escm"),(void*)f_2340},
{C_text("f_2344:csc_2escm"),(void*)f_2344},
{C_text("f_2348:csc_2escm"),(void*)f_2348},
{C_text("f_2352:csc_2escm"),(void*)f_2352},
{C_text("f_2356:csc_2escm"),(void*)f_2356},
{C_text("f_2364:csc_2escm"),(void*)f_2364},
{C_text("f_2368:csc_2escm"),(void*)f_2368},
{C_text("f_2372:csc_2escm"),(void*)f_2372},
{C_text("f_2376:csc_2escm"),(void*)f_2376},
{C_text("f_2380:csc_2escm"),(void*)f_2380},
{C_text("f_2384:csc_2escm"),(void*)f_2384},
{C_text("f_2388:csc_2escm"),(void*)f_2388},
{C_text("f_2392:csc_2escm"),(void*)f_2392},
{C_text("f_2396:csc_2escm"),(void*)f_2396},
{C_text("f_2400:csc_2escm"),(void*)f_2400},
{C_text("f_2404:csc_2escm"),(void*)f_2404},
{C_text("f_2408:csc_2escm"),(void*)f_2408},
{C_text("f_2412:csc_2escm"),(void*)f_2412},
{C_text("f_2416:csc_2escm"),(void*)f_2416},
{C_text("f_2420:csc_2escm"),(void*)f_2420},
{C_text("f_2424:csc_2escm"),(void*)f_2424},
{C_text("f_2428:csc_2escm"),(void*)f_2428},
{C_text("f_2432:csc_2escm"),(void*)f_2432},
{C_text("f_2436:csc_2escm"),(void*)f_2436},
{C_text("f_2505:csc_2escm"),(void*)f_2505},
{C_text("f_2508:csc_2escm"),(void*)f_2508},
{C_text("f_2943:csc_2escm"),(void*)f_2943},
{C_text("f_2949:csc_2escm"),(void*)f_2949},
{C_text("f_2963:csc_2escm"),(void*)f_2963},
{C_text("f_3005:csc_2escm"),(void*)f_3005},
{C_text("f_3032:csc_2escm"),(void*)f_3032},
{C_text("f_3080:csc_2escm"),(void*)f_3080},
{C_text("f_3094:csc_2escm"),(void*)f_3094},
{C_text("f_3107:csc_2escm"),(void*)f_3107},
{C_text("f_3128:csc_2escm"),(void*)f_3128},
{C_text("f_3136:csc_2escm"),(void*)f_3136},
{C_text("f_3157:csc_2escm"),(void*)f_3157},
{C_text("f_3172:csc_2escm"),(void*)f_3172},
{C_text("f_3184:csc_2escm"),(void*)f_3184},
{C_text("f_3188:csc_2escm"),(void*)f_3188},
{C_text("f_3206:csc_2escm"),(void*)f_3206},
{C_text("f_3285:csc_2escm"),(void*)f_3285},
{C_text("f_3385:csc_2escm"),(void*)f_3385},
{C_text("f_3407:csc_2escm"),(void*)f_3407},
{C_text("f_3418:csc_2escm"),(void*)f_3418},
{C_text("f_3790:csc_2escm"),(void*)f_3790},
{C_text("f_3816:csc_2escm"),(void*)f_3816},
{C_text("f_3819:csc_2escm"),(void*)f_3819},
{C_text("f_3826:csc_2escm"),(void*)f_3826},
{C_text("f_3829:csc_2escm"),(void*)f_3829},
{C_text("f_3832:csc_2escm"),(void*)f_3832},
{C_text("f_3835:csc_2escm"),(void*)f_3835},
{C_text("f_3842:csc_2escm"),(void*)f_3842},
{C_text("f_3846:csc_2escm"),(void*)f_3846},
{C_text("f_3850:csc_2escm"),(void*)f_3850},
{C_text("f_3868:csc_2escm"),(void*)f_3868},
{C_text("f_3876:csc_2escm"),(void*)f_3876},
{C_text("f_3880:csc_2escm"),(void*)f_3880},
{C_text("f_3882:csc_2escm"),(void*)f_3882},
{C_text("f_3890:csc_2escm"),(void*)f_3890},
{C_text("f_3898:csc_2escm"),(void*)f_3898},
{C_text("f_3902:csc_2escm"),(void*)f_3902},
{C_text("f_3906:csc_2escm"),(void*)f_3906},
{C_text("f_3910:csc_2escm"),(void*)f_3910},
{C_text("f_3914:csc_2escm"),(void*)f_3914},
{C_text("f_3918:csc_2escm"),(void*)f_3918},
{C_text("f_3931:csc_2escm"),(void*)f_3931},
{C_text("f_3935:csc_2escm"),(void*)f_3935},
{C_text("f_3940:csc_2escm"),(void*)f_3940},
{C_text("f_3943:csc_2escm"),(void*)f_3943},
{C_text("f_3951:csc_2escm"),(void*)f_3951},
{C_text("f_3986:csc_2escm"),(void*)f_3986},
{C_text("f_3990:csc_2escm"),(void*)f_3990},
{C_text("f_3995:csc_2escm"),(void*)f_3995},
{C_text("f_4000:csc_2escm"),(void*)f_4000},
{C_text("f_4007:csc_2escm"),(void*)f_4007},
{C_text("f_4037:csc_2escm"),(void*)f_4037},
{C_text("f_4051:csc_2escm"),(void*)f_4051},
{C_text("f_4055:csc_2escm"),(void*)f_4055},
{C_text("f_4071:csc_2escm"),(void*)f_4071},
{C_text("f_4090:csc_2escm"),(void*)f_4090},
{C_text("f_4105:csc_2escm"),(void*)f_4105},
{C_text("f_4109:csc_2escm"),(void*)f_4109},
{C_text("f_4113:csc_2escm"),(void*)f_4113},
{C_text("f_4116:csc_2escm"),(void*)f_4116},
{C_text("f_4129:csc_2escm"),(void*)f_4129},
{C_text("f_4134:csc_2escm"),(void*)f_4134},
{C_text("f_4159:csc_2escm"),(void*)f_4159},
{C_text("f_4179:csc_2escm"),(void*)f_4179},
{C_text("f_4187:csc_2escm"),(void*)f_4187},
{C_text("f_4191:csc_2escm"),(void*)f_4191},
{C_text("f_4195:csc_2escm"),(void*)f_4195},
{C_text("f_4211:csc_2escm"),(void*)f_4211},
{C_text("f_4218:csc_2escm"),(void*)f_4218},
{C_text("f_4228:csc_2escm"),(void*)f_4228},
{C_text("f_4240:csc_2escm"),(void*)f_4240},
{C_text("f_4244:csc_2escm"),(void*)f_4244},
{C_text("f_4247:csc_2escm"),(void*)f_4247},
{C_text("f_4250:csc_2escm"),(void*)f_4250},
{C_text("f_4253:csc_2escm"),(void*)f_4253},
{C_text("f_4256:csc_2escm"),(void*)f_4256},
{C_text("f_4262:csc_2escm"),(void*)f_4262},
{C_text("f_4268:csc_2escm"),(void*)f_4268},
{C_text("f_4280:csc_2escm"),(void*)f_4280},
{C_text("f_4290:csc_2escm"),(void*)f_4290},
{C_text("f_4294:csc_2escm"),(void*)f_4294},
{C_text("f_4300:csc_2escm"),(void*)f_4300},
{C_text("f_4312:csc_2escm"),(void*)f_4312},
{C_text("f_4319:csc_2escm"),(void*)f_4319},
{C_text("f_4352:csc_2escm"),(void*)f_4352},
{C_text("f_4357:csc_2escm"),(void*)f_4357},
{C_text("f_4359:csc_2escm"),(void*)f_4359},
{C_text("f_4385:csc_2escm"),(void*)f_4385},
{C_text("f_4390:csc_2escm"),(void*)f_4390},
{C_text("f_4394:csc_2escm"),(void*)f_4394},
{C_text("f_4398:csc_2escm"),(void*)f_4398},
{C_text("f_4415:csc_2escm"),(void*)f_4415},
{C_text("f_4431:csc_2escm"),(void*)f_4431},
{C_text("f_4442:csc_2escm"),(void*)f_4442},
{C_text("f_4446:csc_2escm"),(void*)f_4446},
{C_text("f_4449:csc_2escm"),(void*)f_4449},
{C_text("f_4452:csc_2escm"),(void*)f_4452},
{C_text("f_4455:csc_2escm"),(void*)f_4455},
{C_text("f_4461:csc_2escm"),(void*)f_4461},
{C_text("f_4467:csc_2escm"),(void*)f_4467},
{C_text("f_4470:csc_2escm"),(void*)f_4470},
{C_text("f_4482:csc_2escm"),(void*)f_4482},
{C_text("f_4485:csc_2escm"),(void*)f_4485},
{C_text("f_4488:csc_2escm"),(void*)f_4488},
{C_text("f_4491:csc_2escm"),(void*)f_4491},
{C_text("f_4494:csc_2escm"),(void*)f_4494},
{C_text("f_4497:csc_2escm"),(void*)f_4497},
{C_text("f_4504:csc_2escm"),(void*)f_4504},
{C_text("f_4510:csc_2escm"),(void*)f_4510},
{C_text("f_4513:csc_2escm"),(void*)f_4513},
{C_text("f_4516:csc_2escm"),(void*)f_4516},
{C_text("f_4519:csc_2escm"),(void*)f_4519},
{C_text("f_4522:csc_2escm"),(void*)f_4522},
{C_text("f_4525:csc_2escm"),(void*)f_4525},
{C_text("f_4532:csc_2escm"),(void*)f_4532},
{C_text("f_4536:csc_2escm"),(void*)f_4536},
{C_text("f_4554:csc_2escm"),(void*)f_4554},
{C_text("f_4558:csc_2escm"),(void*)f_4558},
{C_text("f_4564:csc_2escm"),(void*)f_4564},
{C_text("f_4571:csc_2escm"),(void*)f_4571},
{C_text("f_4588:csc_2escm"),(void*)f_4588},
{C_text("f_4598:csc_2escm"),(void*)f_4598},
{C_text("f_4602:csc_2escm"),(void*)f_4602},
{C_text("f_4611:csc_2escm"),(void*)f_4611},
{C_text("f_4614:csc_2escm"),(void*)f_4614},
{C_text("f_4621:csc_2escm"),(void*)f_4621},
{C_text("f_4654:csc_2escm"),(void*)f_4654},
{C_text("f_4657:csc_2escm"),(void*)f_4657},
{C_text("f_4660:csc_2escm"),(void*)f_4660},
{C_text("f_4663:csc_2escm"),(void*)f_4663},
{C_text("f_4673:csc_2escm"),(void*)f_4673},
{C_text("f_4680:csc_2escm"),(void*)f_4680},
{C_text("f_4687:csc_2escm"),(void*)f_4687},
{C_text("f_4691:csc_2escm"),(void*)f_4691},
{C_text("f_4698:csc_2escm"),(void*)f_4698},
{C_text("f_4701:csc_2escm"),(void*)f_4701},
{C_text("f_4713:csc_2escm"),(void*)f_4713},
{C_text("f_4725:csc_2escm"),(void*)f_4725},
{C_text("f_4732:csc_2escm"),(void*)f_4732},
{C_text("f_4741:csc_2escm"),(void*)f_4741},
{C_text("f_4748:csc_2escm"),(void*)f_4748},
{C_text("f_4754:csc_2escm"),(void*)f_4754},
{C_text("f_4757:csc_2escm"),(void*)f_4757},
{C_text("f_4760:csc_2escm"),(void*)f_4760},
{C_text("f_4763:csc_2escm"),(void*)f_4763},
{C_text("f_4820:csc_2escm"),(void*)f_4820},
{C_text("f_4832:csc_2escm"),(void*)f_4832},
{C_text("f_4844:csc_2escm"),(void*)f_4844},
{C_text("f_4856:csc_2escm"),(void*)f_4856},
{C_text("f_4879:csc_2escm"),(void*)f_4879},
{C_text("f_4882:csc_2escm"),(void*)f_4882},
{C_text("f_4894:csc_2escm"),(void*)f_4894},
{C_text("f_4984:csc_2escm"),(void*)f_4984},
{C_text("f_4987:csc_2escm"),(void*)f_4987},
{C_text("f_4991:csc_2escm"),(void*)f_4991},
{C_text("f_4999:csc_2escm"),(void*)f_4999},
{C_text("f_5016:csc_2escm"),(void*)f_5016},
{C_text("f_5036:csc_2escm"),(void*)f_5036},
{C_text("f_5039:csc_2escm"),(void*)f_5039},
{C_text("f_5105:csc_2escm"),(void*)f_5105},
{C_text("f_5109:csc_2escm"),(void*)f_5109},
{C_text("f_5125:csc_2escm"),(void*)f_5125},
{C_text("f_5136:csc_2escm"),(void*)f_5136},
{C_text("f_5152:csc_2escm"),(void*)f_5152},
{C_text("f_5173:csc_2escm"),(void*)f_5173},
{C_text("f_5183:csc_2escm"),(void*)f_5183},
{C_text("f_5193:csc_2escm"),(void*)f_5193},
{C_text("f_5203:csc_2escm"),(void*)f_5203},
{C_text("f_5213:csc_2escm"),(void*)f_5213},
{C_text("f_5223:csc_2escm"),(void*)f_5223},
{C_text("f_5233:csc_2escm"),(void*)f_5233},
{C_text("f_5243:csc_2escm"),(void*)f_5243},
{C_text("f_5253:csc_2escm"),(void*)f_5253},
{C_text("f_5263:csc_2escm"),(void*)f_5263},
{C_text("f_5272:csc_2escm"),(void*)f_5272},
{C_text("f_5275:csc_2escm"),(void*)f_5275},
{C_text("f_5287:csc_2escm"),(void*)f_5287},
{C_text("f_5314:csc_2escm"),(void*)f_5314},
{C_text("f_5338:csc_2escm"),(void*)f_5338},
{C_text("f_5355:csc_2escm"),(void*)f_5355},
{C_text("f_5372:csc_2escm"),(void*)f_5372},
{C_text("f_5389:csc_2escm"),(void*)f_5389},
{C_text("f_5406:csc_2escm"),(void*)f_5406},
{C_text("f_5410:csc_2escm"),(void*)f_5410},
{C_text("f_5427:csc_2escm"),(void*)f_5427},
{C_text("f_5431:csc_2escm"),(void*)f_5431},
{C_text("f_5439:csc_2escm"),(void*)f_5439},
{C_text("f_5453:csc_2escm"),(void*)f_5453},
{C_text("f_5466:csc_2escm"),(void*)f_5466},
{C_text("f_5470:csc_2escm"),(void*)f_5470},
{C_text("f_5478:csc_2escm"),(void*)f_5478},
{C_text("f_5491:csc_2escm"),(void*)f_5491},
{C_text("f_5505:csc_2escm"),(void*)f_5505},
{C_text("f_5509:csc_2escm"),(void*)f_5509},
{C_text("f_5517:csc_2escm"),(void*)f_5517},
{C_text("f_5521:csc_2escm"),(void*)f_5521},
{C_text("f_5546:csc_2escm"),(void*)f_5546},
{C_text("f_5549:csc_2escm"),(void*)f_5549},
{C_text("f_5566:csc_2escm"),(void*)f_5566},
{C_text("f_5569:csc_2escm"),(void*)f_5569},
{C_text("f_5587:csc_2escm"),(void*)f_5587},
{C_text("f_5594:csc_2escm"),(void*)f_5594},
{C_text("f_5597:csc_2escm"),(void*)f_5597},
{C_text("f_5600:csc_2escm"),(void*)f_5600},
{C_text("f_5607:csc_2escm"),(void*)f_5607},
{C_text("f_5637:csc_2escm"),(void*)f_5637},
{C_text("f_5640:csc_2escm"),(void*)f_5640},
{C_text("f_5654:csc_2escm"),(void*)f_5654},
{C_text("f_5673:csc_2escm"),(void*)f_5673},
{C_text("f_5677:csc_2escm"),(void*)f_5677},
{C_text("f_5700:csc_2escm"),(void*)f_5700},
{C_text("f_5704:csc_2escm"),(void*)f_5704},
{C_text("f_5731:csc_2escm"),(void*)f_5731},
{C_text("f_5745:csc_2escm"),(void*)f_5745},
{C_text("f_5755:csc_2escm"),(void*)f_5755},
{C_text("f_5759:csc_2escm"),(void*)f_5759},
{C_text("f_5782:csc_2escm"),(void*)f_5782},
{C_text("f_5799:csc_2escm"),(void*)f_5799},
{C_text("f_5801:csc_2escm"),(void*)f_5801},
{C_text("f_5826:csc_2escm"),(void*)f_5826},
{C_text("f_5840:csc_2escm"),(void*)f_5840},
{C_text("f_5844:csc_2escm"),(void*)f_5844},
{C_text("f_5861:csc_2escm"),(void*)f_5861},
{C_text("f_5873:csc_2escm"),(void*)f_5873},
{C_text("f_5878:csc_2escm"),(void*)f_5878},
{C_text("f_5884:csc_2escm"),(void*)f_5884},
{C_text("f_5895:csc_2escm"),(void*)f_5895},
{C_text("f_5909:csc_2escm"),(void*)f_5909},
{C_text("f_5923:csc_2escm"),(void*)f_5923},
{C_text("f_5936:csc_2escm"),(void*)f_5936},
{C_text("f_5941:csc_2escm"),(void*)f_5941},
{C_text("f_5960:csc_2escm"),(void*)f_5960},
{C_text("f_5972:csc_2escm"),(void*)f_5972},
{C_text("f_5976:csc_2escm"),(void*)f_5976},
{C_text("f_5984:csc_2escm"),(void*)f_5984},
{C_text("f_5993:csc_2escm"),(void*)f_5993},
{C_text("f_5999:csc_2escm"),(void*)f_5999},
{C_text("f_6029:csc_2escm"),(void*)f_6029},
{C_text("f_6236:csc_2escm"),(void*)f_6236},
{C_text("f_6239:csc_2escm"),(void*)f_6239},
{C_text("f_6242:csc_2escm"),(void*)f_6242},
{C_text("f_6245:csc_2escm"),(void*)f_6245},
{C_text("f_6249:csc_2escm"),(void*)f_6249},
{C_text("f_6253:csc_2escm"),(void*)f_6253},
{C_text("f_6272:csc_2escm"),(void*)f_6272},
{C_text("f_6276:csc_2escm"),(void*)f_6276},
{C_text("f_6280:csc_2escm"),(void*)f_6280},
{C_text("f_6284:csc_2escm"),(void*)f_6284},
{C_text("f_6288:csc_2escm"),(void*)f_6288},
{C_text("f_6292:csc_2escm"),(void*)f_6292},
{C_text("f_6303:csc_2escm"),(void*)f_6303},
{C_text("f_6309:csc_2escm"),(void*)f_6309},
{C_text("f_6311:csc_2escm"),(void*)f_6311},
{C_text("f_6336:csc_2escm"),(void*)f_6336},
{C_text("f_6347:csc_2escm"),(void*)f_6347},
{C_text("f_6364:csc_2escm"),(void*)f_6364},
{C_text("f_6378:csc_2escm"),(void*)f_6378},
{C_text("f_6407:csc_2escm"),(void*)f_6407},
{C_text("f_6432:csc_2escm"),(void*)f_6432},
{C_text("f_6436:csc_2escm"),(void*)f_6436},
{C_text("f_6439:csc_2escm"),(void*)f_6439},
{C_text("f_6442:csc_2escm"),(void*)f_6442},
{C_text("f_6454:csc_2escm"),(void*)f_6454},
{C_text("f_6466:csc_2escm"),(void*)f_6466},
{C_text("f_6470:csc_2escm"),(void*)f_6470},
{C_text("f_6474:csc_2escm"),(void*)f_6474},
{C_text("f_6478:csc_2escm"),(void*)f_6478},
{C_text("f_6489:csc_2escm"),(void*)f_6489},
{C_text("f_6518:csc_2escm"),(void*)f_6518},
{C_text("f_6521:csc_2escm"),(void*)f_6521},
{C_text("f_6522:csc_2escm"),(void*)f_6522},
{C_text("f_6526:csc_2escm"),(void*)f_6526},
{C_text("f_6529:csc_2escm"),(void*)f_6529},
{C_text("f_6541:csc_2escm"),(void*)f_6541},
{C_text("f_6549:csc_2escm"),(void*)f_6549},
{C_text("f_6553:csc_2escm"),(void*)f_6553},
{C_text("f_6559:csc_2escm"),(void*)f_6559},
{C_text("f_6563:csc_2escm"),(void*)f_6563},
{C_text("f_6572:csc_2escm"),(void*)f_6572},
{C_text("f_6580:csc_2escm"),(void*)f_6580},
{C_text("f_6590:csc_2escm"),(void*)f_6590},
{C_text("f_6603:csc_2escm"),(void*)f_6603},
{C_text("f_6613:csc_2escm"),(void*)f_6613},
{C_text("f_6628:csc_2escm"),(void*)f_6628},
{C_text("f_6630:csc_2escm"),(void*)f_6630},
{C_text("f_6640:csc_2escm"),(void*)f_6640},
{C_text("f_6654:csc_2escm"),(void*)f_6654},
{C_text("f_6657:csc_2escm"),(void*)f_6657},
{C_text("f_6660:csc_2escm"),(void*)f_6660},
{C_text("f_6672:csc_2escm"),(void*)f_6672},
{C_text("f_6679:csc_2escm"),(void*)f_6679},
{C_text("f_6681:csc_2escm"),(void*)f_6681},
{C_text("f_6691:csc_2escm"),(void*)f_6691},
{C_text("f_6704:csc_2escm"),(void*)f_6704},
{C_text("f_6715:csc_2escm"),(void*)f_6715},
{C_text("f_6721:csc_2escm"),(void*)f_6721},
{C_text("f_6723:csc_2escm"),(void*)f_6723},
{C_text("f_6748:csc_2escm"),(void*)f_6748},
{C_text("f_6762:csc_2escm"),(void*)f_6762},
{C_text("f_6771:csc_2escm"),(void*)f_6771},
{C_text("f_6774:csc_2escm"),(void*)f_6774},
{C_text("f_6777:csc_2escm"),(void*)f_6777},
{C_text("f_6780:csc_2escm"),(void*)f_6780},
{C_text("f_6786:csc_2escm"),(void*)f_6786},
{C_text("f_6794:csc_2escm"),(void*)f_6794},
{C_text("f_6804:csc_2escm"),(void*)f_6804},
{C_text("f_6821:csc_2escm"),(void*)f_6821},
{C_text("f_6831:csc_2escm"),(void*)f_6831},
{C_text("f_6835:csc_2escm"),(void*)f_6835},
{C_text("f_6839:csc_2escm"),(void*)f_6839},
{C_text("f_6843:csc_2escm"),(void*)f_6843},
{C_text("f_6847:csc_2escm"),(void*)f_6847},
{C_text("f_6850:csc_2escm"),(void*)f_6850},
{C_text("f_6860:csc_2escm"),(void*)f_6860},
{C_text("f_6867:csc_2escm"),(void*)f_6867},
{C_text("f_6872:csc_2escm"),(void*)f_6872},
{C_text("f_6876:csc_2escm"),(void*)f_6876},
{C_text("f_6884:csc_2escm"),(void*)f_6884},
{C_text("f_6892:csc_2escm"),(void*)f_6892},
{C_text("f_6896:csc_2escm"),(void*)f_6896},
{C_text("f_6900:csc_2escm"),(void*)f_6900},
{C_text("f_6905:csc_2escm"),(void*)f_6905},
{C_text("f_6907:csc_2escm"),(void*)f_6907},
{C_text("f_6932:csc_2escm"),(void*)f_6932},
{C_text("f_6948:csc_2escm"),(void*)f_6948},
{C_text("f_6951:csc_2escm"),(void*)f_6951},
{C_text("f_6958:csc_2escm"),(void*)f_6958},
{C_text("f_6972:csc_2escm"),(void*)f_6972},
{C_text("f_6987:csc_2escm"),(void*)f_6987},
{C_text("f_6995:csc_2escm"),(void*)f_6995},
{C_text("f_7005:csc_2escm"),(void*)f_7005},
{C_text("f_7020:csc_2escm"),(void*)f_7020},
{C_text("f_7029:csc_2escm"),(void*)f_7029},
{C_text("f_7054:csc_2escm"),(void*)f_7054},
{C_text("f_7066:csc_2escm"),(void*)f_7066},
{C_text("f_7080:csc_2escm"),(void*)f_7080},
{C_text("f_7086:csc_2escm"),(void*)f_7086},
{C_text("f_7089:csc_2escm"),(void*)f_7089},
{C_text("f_7092:csc_2escm"),(void*)f_7092},
{C_text("f_7099:csc_2escm"),(void*)f_7099},
{C_text("f_7103:csc_2escm"),(void*)f_7103},
{C_text("f_7112:csc_2escm"),(void*)f_7112},
{C_text("f_7191:csc_2escm"),(void*)f_7191},
{C_text("f_7202:csc_2escm"),(void*)f_7202},
{C_text("f_7208:csc_2escm"),(void*)f_7208},
{C_text("f_7210:csc_2escm"),(void*)f_7210},
{C_text("f_7235:csc_2escm"),(void*)f_7235},
{C_text("f_7244:csc_2escm"),(void*)f_7244},
{C_text("f_7252:csc_2escm"),(void*)f_7252},
{C_text("f_7279:csc_2escm"),(void*)f_7279},
{C_text("f_7289:csc_2escm"),(void*)f_7289},
{C_text("f_7293:csc_2escm"),(void*)f_7293},
{C_text("f_7297:csc_2escm"),(void*)f_7297},
{C_text("f_7299:csc_2escm"),(void*)f_7299},
{C_text("f_7322:csc_2escm"),(void*)f_7322},
{C_text("f_7342:csc_2escm"),(void*)f_7342},
{C_text("f_7351:csc_2escm"),(void*)f_7351},
{C_text("f_7368:csc_2escm"),(void*)f_7368},
{C_text("f_7376:csc_2escm"),(void*)f_7376},
{C_text("f_7383:csc_2escm"),(void*)f_7383},
{C_text("f_7389:csc_2escm"),(void*)f_7389},
{C_text("f_7394:csc_2escm"),(void*)f_7394},
{C_text("f_7406:csc_2escm"),(void*)f_7406},
{C_text("f_7417:csc_2escm"),(void*)f_7417},
{C_text("f_7420:csc_2escm"),(void*)f_7420},
{C_text("f_7423:csc_2escm"),(void*)f_7423},
{C_text("f_7426:csc_2escm"),(void*)f_7426},
{C_text("f_7443:csc_2escm"),(void*)f_7443},
{C_text("f_7446:csc_2escm"),(void*)f_7446},
{C_text("f_7449:csc_2escm"),(void*)f_7449},
{C_text("f_7452:csc_2escm"),(void*)f_7452},
{C_text("f_7468:csc_2escm"),(void*)f_7468},
{C_text("f_7472:csc_2escm"),(void*)f_7472},
{C_text("f_7480:csc_2escm"),(void*)f_7480},
{C_text("f_7488:csc_2escm"),(void*)f_7488},
{C_text("f_7501:csc_2escm"),(void*)f_7501},
{C_text("f_7505:csc_2escm"),(void*)f_7505},
{C_text("f_7524:csc_2escm"),(void*)f_7524},
{C_text("f_7530:csc_2escm"),(void*)f_7530},
{C_text("f_7533:csc_2escm"),(void*)f_7533},
{C_text("f_7536:csc_2escm"),(void*)f_7536},
{C_text("f_7539:csc_2escm"),(void*)f_7539},
{C_text("f_7542:csc_2escm"),(void*)f_7542},
{C_text("f_7546:csc_2escm"),(void*)f_7546},
{C_text("f_7550:csc_2escm"),(void*)f_7550},
{C_text("f_7554:csc_2escm"),(void*)f_7554},
{C_text("f_7560:csc_2escm"),(void*)f_7560},
{C_text("f_7565:csc_2escm"),(void*)f_7565},
{C_text("f_7573:csc_2escm"),(void*)f_7573},
{C_text("f_7591:csc_2escm"),(void*)f_7591},
{C_text("f_7597:csc_2escm"),(void*)f_7597},
{C_text("f_7601:csc_2escm"),(void*)f_7601},
{C_text("f_7605:csc_2escm"),(void*)f_7605},
{C_text("f_7609:csc_2escm"),(void*)f_7609},
{C_text("f_7616:csc_2escm"),(void*)f_7616},
{C_text("f_7620:csc_2escm"),(void*)f_7620},
{C_text("f_7623:csc_2escm"),(void*)f_7623},
{C_text("f_7639:csc_2escm"),(void*)f_7639},
{C_text("f_7642:csc_2escm"),(void*)f_7642},
{C_text("f_7650:csc_2escm"),(void*)f_7650},
{C_text("f_7675:csc_2escm"),(void*)f_7675},
{C_text("f_7684:csc_2escm"),(void*)f_7684},
{C_text("f_7709:csc_2escm"),(void*)f_7709},
{C_text("f_7726:csc_2escm"),(void*)f_7726},
{C_text("f_7746:csc_2escm"),(void*)f_7746},
{C_text("f_7750:csc_2escm"),(void*)f_7750},
{C_text("f_7775:csc_2escm"),(void*)f_7775},
{C_text("f_7793:csc_2escm"),(void*)f_7793},
{C_text("f_7797:csc_2escm"),(void*)f_7797},
{C_text("f_7804:csc_2escm"),(void*)f_7804},
{C_text("f_7808:csc_2escm"),(void*)f_7808},
{C_text("f_7812:csc_2escm"),(void*)f_7812},
{C_text("f_7816:csc_2escm"),(void*)f_7816},
{C_text("f_7827:csc_2escm"),(void*)f_7827},
{C_text("f_7830:csc_2escm"),(void*)f_7830},
{C_text("f_7837:csc_2escm"),(void*)f_7837},
{C_text("f_7842:csc_2escm"),(void*)f_7842},
{C_text("f_7847:csc_2escm"),(void*)f_7847},
{C_text("f_7851:csc_2escm"),(void*)f_7851},
{C_text("f_7855:csc_2escm"),(void*)f_7855},
{C_text("f_7862:csc_2escm"),(void*)f_7862},
{C_text("f_7864:csc_2escm"),(void*)f_7864},
{C_text("toplevel:csc_2escm"),(void*)C_toplevel},
{NULL,NULL}};
#endif

static C_PTABLE_ENTRY *create_ptable(void){
#ifdef C_ENABLE_PTABLES
return ptable;
#else
return NULL;
#endif
}

/*
o|hiding unexported module binding: main#staticbuild 
o|hiding unexported module binding: main#debugbuild 
o|hiding unexported module binding: main#cross-chicken 
o|hiding unexported module binding: main#binary-version 
o|hiding unexported module binding: main#major-version 
o|hiding unexported module binding: main#default-cc 
o|hiding unexported module binding: main#default-cxx 
o|hiding unexported module binding: main#default-install-program 
o|hiding unexported module binding: main#default-cflags 
o|hiding unexported module binding: main#default-ldflags 
o|hiding unexported module binding: main#default-install-program-executable-flags 
o|hiding unexported module binding: main#default-install-program-data-flags 
o|hiding unexported module binding: main#default-libs 
o|hiding unexported module binding: main#default-libdir 
o|hiding unexported module binding: main#default-runlibdir 
o|hiding unexported module binding: main#default-slibdir 
o|hiding unexported module binding: main#default-incdir 
o|hiding unexported module binding: main#default-bindir 
o|hiding unexported module binding: main#default-sharedir 
o|hiding unexported module binding: main#default-platform 
o|hiding unexported module binding: main#default-prefix 
o|hiding unexported module binding: main#default-bindir 
o|hiding unexported module binding: main#default-csc 
o|hiding unexported module binding: main#default-csi 
o|hiding unexported module binding: main#default-builder 
o|hiding unexported module binding: main#target-librarian 
o|hiding unexported module binding: main#target-librarian-options 
o|hiding unexported module binding: main#host-repo 
o|hiding unexported module binding: main#host-libdir 
o|hiding unexported module binding: main#host-bindir 
o|hiding unexported module binding: main#host-incdir 
o|hiding unexported module binding: main#host-sharedir 
o|hiding unexported module binding: main#host-libs 
o|hiding unexported module binding: main#host-cflags 
o|hiding unexported module binding: main#host-ldflags 
o|hiding unexported module binding: main#host-cc 
o|hiding unexported module binding: main#host-cxx 
o|hiding unexported module binding: main#target-repo 
o|hiding unexported module binding: main#target-run-repo 
o|hiding unexported module binding: main#+egg-info-extension+ 
o|hiding unexported module binding: main#+version-file+ 
o|hiding unexported module binding: main#+timestamp-file+ 
o|hiding unexported module binding: main#+status-file+ 
o|hiding unexported module binding: main#+egg-extension+ 
o|hiding unexported module binding: main#validate-environment 
o|hiding unexported module binding: main#destination-repository 
o|hiding unexported module binding: main#probe-dir 
o|hiding unexported module binding: main#cache-directory 
o|hiding unexported module binding: main#partition 
o|hiding unexported module binding: main#span 
o|hiding unexported module binding: main#take 
o|hiding unexported module binding: main#drop 
o|hiding unexported module binding: main#split-at 
o|hiding unexported module binding: main#append-map 
o|hiding unexported module binding: main#every 
o|hiding unexported module binding: main#any 
o|hiding unexported module binding: main#cons* 
o|hiding unexported module binding: main#concatenate 
o|hiding unexported module binding: main#delete 
o|hiding unexported module binding: main#first 
o|hiding unexported module binding: main#second 
o|hiding unexported module binding: main#third 
o|hiding unexported module binding: main#fourth 
o|hiding unexported module binding: main#fifth 
o|hiding unexported module binding: main#delete-duplicates 
o|hiding unexported module binding: main#alist-cons 
o|hiding unexported module binding: main#filter 
o|hiding unexported module binding: main#filter-map 
o|hiding unexported module binding: main#remove 
o|hiding unexported module binding: main#unzip1 
o|hiding unexported module binding: main#last 
o|hiding unexported module binding: main#list-index 
o|hiding unexported module binding: main#lset-adjoin/eq? 
o|hiding unexported module binding: main#lset-difference/eq? 
o|hiding unexported module binding: main#lset-union/eq? 
o|hiding unexported module binding: main#lset-intersection/eq? 
o|hiding unexported module binding: main#list-tabulate 
o|hiding unexported module binding: main#lset<=/eq? 
o|hiding unexported module binding: main#lset=/eq? 
o|hiding unexported module binding: main#length+ 
o|hiding unexported module binding: main#find 
o|hiding unexported module binding: main#find-tail 
o|hiding unexported module binding: main#iota 
o|hiding unexported module binding: main#make-list 
o|hiding unexported module binding: main#posq 
o|hiding unexported module binding: main#posv 
o|hiding unexported module binding: main#host-libs 
o|hiding unexported module binding: main#TARGET_CC 
o|hiding unexported module binding: main#windows 
o|hiding unexported module binding: main#mingw 
o|hiding unexported module binding: main#osx 
o|hiding unexported module binding: main#cygwin 
o|hiding unexported module binding: main#aix 
o|hiding unexported module binding: main#elf 
o|hiding unexported module binding: main#stop 
o|hiding unexported module binding: main#arguments 
o|hiding unexported module binding: main#cross-chicken 
o|hiding unexported module binding: main#host-mode 
o|hiding unexported module binding: main#back-slash->forward-slash 
o|hiding unexported module binding: main#quotewrap 
o|hiding unexported module binding: main#quotewrap-no-slash-trans 
o|hiding unexported module binding: main#home 
o|hiding unexported module binding: main#translator 
o|hiding unexported module binding: main#compiler 
o|hiding unexported module binding: main#c++-compiler 
o|hiding unexported module binding: main#rc-compiler 
o|hiding unexported module binding: main#linker 
o|hiding unexported module binding: main#c++-linker 
o|hiding unexported module binding: main#object-extension 
o|hiding unexported module binding: main#library-extension 
o|hiding unexported module binding: main#link-output-flag 
o|hiding unexported module binding: main#executable-extension 
o|hiding unexported module binding: main#compile-output-flag 
o|hiding unexported module binding: main#shared-library-extension 
o|hiding unexported module binding: main#static-object-extension 
o|hiding unexported module binding: main#static-library-extension 
o|hiding unexported module binding: main#default-translation-optimization-options 
o|hiding unexported module binding: main#pic-options 
o|hiding unexported module binding: main#generate-manifest 
o|hiding unexported module binding: main#libchicken 
o|hiding unexported module binding: main#dynamic-libchicken 
o|hiding unexported module binding: main#default-library 
o|hiding unexported module binding: main#default-compilation-optimization-options 
o|hiding unexported module binding: main#best-compilation-optimization-options 
o|hiding unexported module binding: main#default-linking-optimization-options 
o|hiding unexported module binding: main#best-linking-optimization-options 
o|hiding unexported module binding: main#extra-features 
o|hiding unexported module binding: main#constant807 
o|hiding unexported module binding: main#constant810 
o|hiding unexported module binding: main#constant814 
o|hiding unexported module binding: main#short-options 
o|hiding unexported module binding: main#scheme-files 
o|hiding unexported module binding: main#c-files 
o|hiding unexported module binding: main#rc-files 
o|hiding unexported module binding: main#generated-c-files 
o|hiding unexported module binding: main#generated-rc-files 
o|hiding unexported module binding: main#object-files 
o|hiding unexported module binding: main#generated-object-files 
o|hiding unexported module binding: main#transient-link-files 
o|hiding unexported module binding: main#linked-extensions 
o|hiding unexported module binding: main#cpp-mode 
o|hiding unexported module binding: main#objc-mode 
o|hiding unexported module binding: main#embedded 
o|hiding unexported module binding: main#inquiry-only 
o|hiding unexported module binding: main#show-cflags 
o|hiding unexported module binding: main#show-ldflags 
o|hiding unexported module binding: main#show-libs 
o|hiding unexported module binding: main#dry-run 
o|hiding unexported module binding: main#gui 
o|hiding unexported module binding: main#deployed 
o|hiding unexported module binding: main#rpath 
o|hiding unexported module binding: main#ignore-repository 
o|hiding unexported module binding: main#show-debugging-help 
o|hiding unexported module binding: main#library-dir 
o|hiding unexported module binding: main#extra-libraries 
o|hiding unexported module binding: main#extra-shared-libraries 
o|hiding unexported module binding: main#default-library-files 
o|hiding unexported module binding: main#library-files 
o|hiding unexported module binding: main#shared-library-files 
o|hiding unexported module binding: main#translate-options 
o|hiding unexported module binding: main#include-dir 
o|hiding unexported module binding: main#compile-options 
o|hiding unexported module binding: main#builtin-compile-options 
o|hiding unexported module binding: main#compile-only-flag 
o|hiding unexported module binding: main#translation-optimization-options 
o|hiding unexported module binding: main#compilation-optimization-options 
o|hiding unexported module binding: main#linking-optimization-options 
o|hiding unexported module binding: main#link-options 
o|hiding unexported module binding: main#builtin-link-options 
o|hiding unexported module binding: main#target-filename 
o|hiding unexported module binding: main#verbose 
o|hiding unexported module binding: main#keep-files 
o|hiding unexported module binding: main#translate-only 
o|hiding unexported module binding: main#compile-only 
o|hiding unexported module binding: main#to-stdout 
o|hiding unexported module binding: main#shared 
o|hiding unexported module binding: main#static 
o|hiding unexported module binding: main#repo-path 
o|hiding unexported module binding: main#find-object-file 
o|hiding unexported module binding: main#usage 
o|hiding unexported module binding: main#run 
o|hiding unexported module binding: main#run-translation 
o|hiding unexported module binding: main#run-compilation 
o|hiding unexported module binding: main#compiler-options 
o|hiding unexported module binding: main#run-linking 
o|hiding unexported module binding: main#collect-linked-objects 
o|hiding unexported module binding: main#copy-files 
o|hiding unexported module binding: main#linker-options 
o|hiding unexported module binding: main#linker-libraries 
o|hiding unexported module binding: main#constant1665 
o|hiding unexported module binding: main#cleanup 
o|hiding unexported module binding: main#string-any 
o|hiding unexported module binding: main#quote-option 
o|hiding unexported module binding: main#last-exit-code 
o|hiding unexported module binding: main#$system 
o|hiding unexported module binding: main#command 
o|hiding unexported module binding: main#$delete-file 
o|hiding unexported module binding: main#rez 
o|hiding unexported module binding: main#create-win-manifest 
S|applied compiler syntax:
S|  chicken.format#printf		1
S|  scheme#for-each		6
S|  chicken.format#sprintf		4
S|  chicken.format#fprintf		2
S|  chicken.base#foldl		3
S|  scheme#map		13
S|  chicken.base#foldr		3
o|eliminated procedure checks: 150 
o|specializations:
o|  1 (scheme#zero? *)
o|  2 (scheme#zero? integer)
o|  1 (scheme#= integer integer)
o|  1 (##sys#debug-mode?)
o|  2 (scheme#= fixnum fixnum)
o|  5 (scheme#string-append string string)
o|  5 (scheme#char=? char char)
o|  7 (scheme#string-ref string fixnum)
o|  4 (scheme#string=? string string)
o|  4 (scheme#> fixnum fixnum)
o|  4 (scheme#string-length string)
o|  1 (scheme#memv (or symbol keyword procedure eof null fixnum char boolean) list)
o|  71 (scheme#eqv? (or eof null fixnum char boolean symbol keyword) *)
o|  1 (scheme#> integer integer)
o|  2 (scheme#+ fixnum fixnum)
o|  7 (##sys#check-output-port * * *)
o|  2 (chicken.base#current-error-port)
o|  4 (scheme#memq * list)
o|  1 (scheme#eqv? * *)
o|  6 (##sys#check-list (or pair list) *)
o|  26 (scheme#cdr pair)
o|  8 (scheme#car pair)
o|  2 (scheme#number->string fixnum)
(o e)|safe calls: 816 
(o e)|assignments to immediate values: 25 
o|removed side-effect free assignment to unused variable: main#default-install-program 
o|removed side-effect free assignment to unused variable: main#default-ldflags 
o|removed side-effect free assignment to unused variable: main#default-install-program-executable-flags 
o|removed side-effect free assignment to unused variable: main#default-install-program-data-flags 
o|removed side-effect free assignment to unused variable: main#default-slibdir 
o|removed side-effect free assignment to unused variable: main#default-platform 
o|removed side-effect free assignment to unused variable: main#default-prefix 
o|removed side-effect free assignment to unused variable: main#default-csc 
o|removed side-effect free assignment to unused variable: main#default-csi 
o|removed side-effect free assignment to unused variable: main#default-builder 
o|removed side-effect free assignment to unused variable: main#target-librarian 
o|removed side-effect free assignment to unused variable: main#target-librarian-options 
o|removed side-effect free assignment to unused variable: main#host-ldflags 
o|removed side-effect free assignment to unused variable: main#+egg-info-extension+ 
o|removed side-effect free assignment to unused variable: main#+version-file+ 
o|removed side-effect free assignment to unused variable: main#+timestamp-file+ 
o|removed side-effect free assignment to unused variable: main#+status-file+ 
o|removed side-effect free assignment to unused variable: main#+egg-extension+ 
o|removed side-effect free assignment to unused variable: main#validate-environment 
o|removed side-effect free assignment to unused variable: main#probe-dir 
o|removed side-effect free assignment to unused variable: main#cache-directory 
o|removed side-effect free assignment to unused variable: main#partition 
o|removed side-effect free assignment to unused variable: main#span 
o|removed side-effect free assignment to unused variable: main#drop 
o|removed side-effect free assignment to unused variable: main#split-at 
o|removed side-effect free assignment to unused variable: main#append-map 
o|inlining procedure: k2890 
o|inlining procedure: k2890 
o|inlining procedure: k2921 
o|inlining procedure: k2921 
o|merged explicitly consed rest parameter: xs406 
o|inlining procedure: k2951 
o|inlining procedure: k2951 
o|removed side-effect free assignment to unused variable: main#concatenate 
o|removed side-effect free assignment to unused variable: main#second 
o|removed side-effect free assignment to unused variable: main#third 
o|removed side-effect free assignment to unused variable: main#fourth 
o|removed side-effect free assignment to unused variable: main#fifth 
o|removed side-effect free assignment to unused variable: main#alist-cons 
o|inlining procedure: k3138 
o|inlining procedure: k3138 
o|inlining procedure: k3130 
o|inlining procedure: k3130 
o|removed side-effect free assignment to unused variable: main#remove 
o|removed side-effect free assignment to unused variable: main#unzip1 
o|removed side-effect free assignment to unused variable: main#list-index 
o|removed side-effect free assignment to unused variable: main#lset-adjoin/eq? 
o|removed side-effect free assignment to unused variable: main#lset-union/eq? 
o|removed side-effect free assignment to unused variable: main#lset-intersection/eq? 
o|inlining procedure: k3529 
o|inlining procedure: k3529 
o|removed side-effect free assignment to unused variable: main#lset<=/eq? 
o|removed side-effect free assignment to unused variable: main#lset=/eq? 
o|removed side-effect free assignment to unused variable: main#length+ 
o|removed side-effect free assignment to unused variable: main#find 
o|removed side-effect free assignment to unused variable: main#find-tail 
o|removed side-effect free assignment to unused variable: main#iota 
o|removed side-effect free assignment to unused variable: main#make-list 
o|removed side-effect free assignment to unused variable: main#posq 
o|removed side-effect free assignment to unused variable: main#posv 
o|removed side-effect free assignment to unused variable: main#TARGET_CC 
o|removed side-effect free assignment to unused variable: main#windows 
o|substituted constant variable: a3817 
o|merged explicitly consed rest parameter: args756 
o|propagated global variable: out757760 ##sys#standard-error 
o|substituted constant variable: a3822 
o|substituted constant variable: a3823 
o|contracted procedure: "(csc.scm:89) main#back-slash->forward-slash" 
o|inlining procedure: k3861 
o|inlining procedure: k3861 
o|inlining procedure: k3949 
o|inlining procedure: k3949 
o|removed side-effect free assignment to unused variable: main#dynamic-libchicken 
o|substituted constant variable: main#default-translation-optimization-options 
o|inlining procedure: k4230 
o|inlining procedure: k4230 
o|contracted procedure: "(csc.scm:298) main#destination-repository" 
o|inlining procedure: k2469 
o|inlining procedure: k2469 
o|inlining procedure: k4257 
o|inlining procedure: k4257 
o|inlining procedure: k4269 
o|inlining procedure: k4269 
o|inlining procedure: k4281 
o|inlining procedure: k4281 
o|inlining procedure: k4301 
o|inlining procedure: k4301 
o|inlining procedure: k6725 
o|inlining procedure: k6725 
o|removed side-effect free assignment to unused variable: main#copy-files 
o|inlining procedure: k7212 
o|inlining procedure: k7212 
o|inlining procedure: k7258 
o|inlining procedure: k7258 
o|contracted procedure: "(csc.scm:1043) main#library-files" 
o|contracted procedure: "(csc.scm:233) main#default-library" 
o|contracted procedure: "(csc.scm:1044) main#shared-library-files" 
o|contracted procedure: "(csc.scm:234) main#default-library-files" 
o|inlining procedure: k4053 
o|inlining procedure: k4053 
o|substituted constant variable: a7357 
o|inlining procedure: k7358 
o|inlining procedure: k7358 
o|inlining procedure: k7378 
o|inlining procedure: k7378 
o|contracted procedure: "(csc.scm:1084) main#cleanup" 
o|inlining procedure: k7280 
o|inlining procedure: k7280 
o|inlining procedure: k7301 
o|inlining procedure: k7301 
o|consed rest parameter at call site: "(csc.scm:1061) main#cons*" 2 
o|inlining procedure: k7399 
o|inlining procedure: k7399 
o|inlining procedure: k7490 
o|inlining procedure: k7490 
o|contracted procedure: "(csc.scm:1106) main#$system" 
o|inlining procedure: k7428 
o|inlining procedure: k7428 
o|propagated global variable: out17251728 ##sys#standard-output 
o|substituted constant variable: a7439 
o|substituted constant variable: a7440 
o|propagated global variable: out17251728 ##sys#standard-output 
o|inlining procedure: k7469 
o|inlining procedure: k7469 
o|inlining procedure: k7506 
o|inlining procedure: k7506 
o|contracted procedure: "(csc.scm:1146) main#run" 
o|merged explicitly consed rest parameter: os1014 
o|merged explicitly consed rest parameter: n1017 
o|inlining procedure: k4361 
o|inlining procedure: k4361 
o|consed rest parameter at call site: "(csc.scm:539) main#stop" 2 
o|inlining procedure: k4375 
o|inlining procedure: k4375 
o|inlining procedure: k4401 
o|inlining procedure: k4401 
o|consed rest parameter at call site: "(csc.scm:542) main#cons*" 2 
o|inlining procedure: k4421 
o|propagated global variable: r44227955 main#shared-library-extension 
o|inlining procedure: k4421 
o|inlining procedure: k4433 
o|inlining procedure: k4462 
o|inlining procedure: k4462 
o|contracted procedure: "(csc.scm:613) main#run-linking" 
o|inlining procedure: k6781 
o|inlining procedure: k6781 
o|inlining procedure: k6796 
o|inlining procedure: k6796 
o|inlining procedure: k6822 
o|contracted procedure: "(csc.scm:997) main#rez" 
o|substituted constant variable: a7526 
o|substituted constant variable: a7527 
o|inlining procedure: k6822 
o|inlining procedure: k6848 
o|inlining procedure: k6848 
o|inlining procedure: k6858 
o|propagated global variable: r68597972 main#host-libdir 
o|inlining procedure: k6858 
o|substituted constant variable: a6868 
o|consed rest parameter at call site: "(csc.scm:975) main#cons*" 2 
o|substituted constant variable: main#link-output-flag 
o|substituted constant variable: main#link-output-flag 
o|inlining procedure: k6909 
o|inlining procedure: k6909 
o|propagated global variable: g14931497 main#object-files 
o|contracted procedure: "(csc.scm:969) main#collect-linked-objects" 
o|inlining procedure: k7068 
o|contracted procedure: "(csc.scm:1016) main#delete-duplicates" 
o|inlining procedure: k3082 
o|inlining procedure: k3082 
o|contracted procedure: "(mini-srfi-1.scm:123) main#delete" 
o|inlining procedure: k3007 
o|inlining procedure: k3007 
o|inlining procedure: k7068 
o|contracted procedure: "(csc.scm:1020) locate-objects1537" 
o|inlining procedure: k6997 
o|contracted procedure: "(csc.scm:1010) g15501559" 
o|inlining procedure: k6973 
o|inlining procedure: k6973 
o|consed rest parameter at call site: "(csc.scm:1012) main#stop" 2 
o|inlining procedure: k6997 
o|inlining procedure: k7031 
o|inlining procedure: k7031 
o|contracted procedure: "(csc.scm:1018) locate-link-file1536" 
o|propagated global variable: tmp16051607 main#static 
o|propagated global variable: tmp16051607 main#static 
o|propagated global variable: ofiles1534 main#object-files 
o|propagated global variable: ofiles1534 main#object-files 
o|propagated global variable: out10541057 ##sys#standard-error 
o|substituted constant variable: a4478 
o|substituted constant variable: a4479 
o|substituted constant variable: a4506 
o|substituted constant variable: a4507 
o|inlining procedure: k4545 
o|inlining procedure: k4545 
o|propagated global variable: out10541057 ##sys#standard-error 
o|contracted procedure: "(csc.scm:602) main#filter-map" 
o|propagated global variable: lst470 main#linked-extensions 
o|inlining procedure: k3185 
o|inlining procedure: k3185 
o|inlining procedure: k3174 
o|inlining procedure: k3174 
o|contracted procedure: "(csc.scm:598) main#run-compilation" 
o|substituted constant variable: main#compile-only-flag 
o|inlining procedure: k6479 
o|inlining procedure: k6479 
o|substituted constant variable: main#compile-output-flag 
o|substituted constant variable: main#compile-output-flag 
o|consed rest parameter at call site: "(csc.scm:922) main#stop" 2 
o|inlining procedure: k6502 
o|substituted constant variable: a6508 
o|inlining procedure: k6502 
o|inlining procedure: k6564 
o|inlining procedure: k6564 
o|inlining procedure: k6582 
o|inlining procedure: k6582 
o|propagated global variable: g14371439 main#generated-rc-files 
o|inlining procedure: k6605 
o|inlining procedure: k6605 
o|propagated global variable: g14201422 main#generated-c-files 
o|inlining procedure: k6632 
o|inlining procedure: k6632 
o|propagated global variable: g13771401 main#rc-files 
o|contracted procedure: "(csc.scm:940) main#create-win-manifest" 
o|inlining procedure: k6683 
o|inlining procedure: k6683 
o|propagated global variable: g13671379 main#c-files 
o|inlining procedure: k4565 
o|inlining procedure: k4565 
o|contracted procedure: "(csc.scm:589) main#last" 
o|inlining procedure: k3287 
o|inlining procedure: k3287 
o|consed rest parameter at call site: "(csc.scm:585) main#stop" 2 
o|inlining procedure: k4586 
o|consed rest parameter at call site: "(csc.scm:585) main#stop" 2 
o|consed rest parameter at call site: "(csc.scm:584) main#cons*" 2 
o|inlining procedure: k4586 
o|consed rest parameter at call site: "(csc.scm:585) main#stop" 2 
o|contracted procedure: "(csc.scm:596) main#run-translation" 
o|inlining procedure: k6409 
o|contracted procedure: "(csc.scm:863) g12981305" 
o|consed rest parameter at call site: "(csc.scm:878) main#cons*" 2 
o|inlining procedure: k6313 
o|inlining procedure: k6313 
o|inlining procedure: k6349 
o|inlining procedure: k6349 
o|consed rest parameter at call site: "(csc.scm:874) main#stop" 2 
o|inlining procedure: k6386 
o|inlining procedure: k6386 
o|substituted constant variable: a6395 
o|inlining procedure: k6409 
o|propagated global variable: g13041306 main#scheme-files 
o|contracted procedure: "(csc.scm:595) main#first" 
o|propagated global variable: x428 main#scheme-files 
o|consed rest parameter at call site: "(csc.scm:577) main#stop" 2 
o|substituted constant variable: a4651 
o|contracted procedure: "(csc.scm:565) main#builtin-link-options" 
o|inlining procedure: k4114 
o|contracted procedure: "(csc.scm:278) g936937" 
o|inlining procedure: k4136 
o|contracted procedure: "(csc.scm:280) g947956" 
o|inlining procedure: k4136 
o|inlining procedure: k4114 
o|substituted constant variable: a4180 
o|inlining procedure: k4193 
o|inlining procedure: k4193 
o|inlining procedure: k4202 
o|inlining procedure: k4202 
o|inlining procedure: k4433 
o|contracted procedure: "(csc.scm:620) main#usage" 
o|inlining procedure: k4717 
o|inlining procedure: k4717 
o|substituted constant variable: a4750 
o|substituted constant variable: a4751 
o|inlining procedure: k4764 
o|inlining procedure: k4764 
o|inlining procedure: k4785 
o|inlining procedure: k4785 
o|inlining procedure: k4804 
o|inlining procedure: k4804 
o|inlining procedure: k4824 
o|inlining procedure: k4824 
o|inlining procedure: k4848 
o|inlining procedure: k4848 
o|inlining procedure: k4868 
o|consed rest parameter at call site: "(csc.scm:653) t-options1009" 1 
o|consed rest parameter at call site: "(csc.scm:651) main#cons*" 2 
o|inlining procedure: k4868 
o|consed rest parameter at call site: "(csc.scm:659) t-options1009" 1 
o|inlining procedure: k4915 
o|consed rest parameter at call site: "(csc.scm:662) t-options1009" 1 
o|inlining procedure: k4915 
o|consed rest parameter at call site: "(csc.scm:665) t-options1009" 1 
o|inlining procedure: k4941 
o|inlining procedure: k4941 
o|inlining procedure: k4955 
o|inlining procedure: k4955 
o|inlining procedure: k4976 
o|consed rest parameter at call site: "(csc.scm:674) t-options1009" 1 
o|consed rest parameter at call site: "(csc.scm:673) check1010" 3 
o|inlining procedure: k4976 
o|consed rest parameter at call site: "(csc.scm:679) check1010" 3 
o|inlining procedure: k5025 
o|consed rest parameter at call site: "(csc.scm:684) t-options1009" 1 
o|consed rest parameter at call site: "(csc.scm:683) check1010" 3 
o|inlining procedure: k5025 
o|contracted procedure: "(csc.scm:687) use-private-repository1012" 
o|inlining procedure: k5057 
o|consed rest parameter at call site: "(csc.scm:690) t-options1009" 1 
o|inlining procedure: k5057 
o|consed rest parameter at call site: "(csc.scm:693) t-options1009" 1 
o|inlining procedure: k5077 
o|inlining procedure: k5077 
o|inlining procedure: k5095 
o|consed rest parameter at call site: "(csc.scm:706) main#cons*" 2 
o|inlining procedure: k5095 
o|inlining procedure: k5110 
o|inlining procedure: k5110 
o|consed rest parameter at call site: "(csc.scm:713) main#cons*" 2 
o|consed rest parameter at call site: "(csc.scm:711) check1010" 3 
o|inlining procedure: k5141 
o|consed rest parameter at call site: "(csc.scm:716) check1010" 3 
o|inlining procedure: k5141 
o|consed rest parameter at call site: "(csc.scm:720) main#cons*" 2 
o|inlining procedure: k5174 
o|consed rest parameter at call site: "(csc.scm:721) main#cons*" 2 
o|inlining procedure: k5174 
o|consed rest parameter at call site: "(csc.scm:722) main#cons*" 2 
o|inlining procedure: k5194 
o|consed rest parameter at call site: "(csc.scm:723) main#cons*" 2 
o|inlining procedure: k5194 
o|consed rest parameter at call site: "(csc.scm:724) main#cons*" 2 
o|inlining procedure: k5214 
o|consed rest parameter at call site: "(csc.scm:726) main#cons*" 2 
o|inlining procedure: k5214 
o|consed rest parameter at call site: "(csc.scm:727) main#cons*" 2 
o|inlining procedure: k5234 
o|consed rest parameter at call site: "(csc.scm:728) main#cons*" 2 
o|inlining procedure: k5234 
o|consed rest parameter at call site: "(csc.scm:729) main#cons*" 2 
o|inlining procedure: k5254 
o|consed rest parameter at call site: "(csc.scm:730) main#cons*" 2 
o|inlining procedure: k5254 
o|substituted constant variable: a5283 
o|consed rest parameter at call site: "(csc.scm:733) t-options1009" 1 
o|consed rest parameter at call site: "(csc.scm:732) check1010" 3 
o|inlining procedure: k5298 
o|inlining procedure: k5298 
o|inlining procedure: k5318 
o|inlining procedure: k5318 
o|consed rest parameter at call site: "(csc.scm:746) check1010" 3 
o|inlining procedure: k5347 
o|consed rest parameter at call site: "(csc.scm:750) check1010" 3 
o|inlining procedure: k5347 
o|consed rest parameter at call site: "(csc.scm:754) check1010" 3 
o|inlining procedure: k5381 
o|consed rest parameter at call site: "(csc.scm:758) check1010" 3 
o|inlining procedure: k5381 
o|consed rest parameter at call site: "(csc.scm:763) main#cons*" 2 
o|consed rest parameter at call site: "(csc.scm:762) check1010" 3 
o|inlining procedure: k5419 
o|consed rest parameter at call site: "(csc.scm:765) check1010" 3 
o|inlining procedure: k5419 
o|inlining procedure: k5458 
o|consed rest parameter at call site: "(csc.scm:771) check1010" 3 
o|inlining procedure: k5458 
o|substituted constant variable: a5522 
o|inlining procedure: k5523 
o|inlining procedure: k5523 
o|consed rest parameter at call site: "(csc.scm:775) check1010" 3 
o|inlining procedure: k5532 
o|inlining procedure: k5532 
o|consed rest parameter at call site: "(csc.scm:784) t-options1009" 1 
o|consed rest parameter at call site: "(csc.scm:783) check1010" 3 
o|inlining procedure: k5558 
o|consed rest parameter at call site: "(csc.scm:788) t-options1009" 1 
o|consed rest parameter at call site: "(csc.scm:787) check1010" 3 
o|inlining procedure: k5558 
o|inlining procedure: k5588 
o|inlining procedure: k5588 
o|inlining procedure: k5604 
o|inlining procedure: k5604 
o|consed rest parameter at call site: "(csc.scm:802) t-options1009" 1 
o|inlining procedure: k5629 
o|consed rest parameter at call site: "(csc.scm:805) t-options1009" 1 
o|consed rest parameter at call site: "(csc.scm:804) check1010" 3 
o|inlining procedure: k5629 
o|consed rest parameter at call site: "(csc.scm:808) t-options1009" 1 
o|inlining procedure: k5658 
o|substituted constant variable: a5670 
o|substituted constant variable: a5667 
o|substituted constant variable: a5690 
o|substituted constant variable: a5697 
o|substituted constant variable: a5694 
o|inlining procedure: k5691 
o|substituted constant variable: a5717 
o|inlining procedure: k5691 
o|substituted constant variable: a5724 
o|substituted constant variable: a5721 
o|consed rest parameter at call site: "(csc.scm:820) t-options1009" 1 
o|substituted constant variable: a5738 
o|substituted constant variable: a5735 
o|inlining procedure: k5732 
o|inlining procedure: k5732 
o|inlining procedure: k5764 
o|inlining procedure: k5803 
o|contracted procedure: "(csc.scm:830) g12431252" 
o|substituted constant variable: a5792 
o|inlining procedure: k5803 
o|consed rest parameter at call site: "(csc.scm:831) main#stop" 2 
o|contracted procedure: "(csc.scm:828) main#lset-difference/eq?" 
o|inlining procedure: k3387 
o|contracted procedure: "(mini-srfi-1.scm:164) g570571" 
o|inlining procedure: k3387 
o|inlining procedure: k5764 
o|consed rest parameter at call site: "(csc.scm:832) main#stop" 2 
o|substituted constant variable: a5852 
o|substituted constant variable: a5857 
o|substituted constant variable: a5866 
o|inlining procedure: k5658 
o|inlining procedure: k5886 
o|inlining procedure: k5886 
o|inlining procedure: k5914 
o|inlining procedure: k5914 
o|inlining procedure: k5950 
o|inlining procedure: k5950 
o|inlining procedure: k5994 
o|inlining procedure: k5994 
o|consed rest parameter at call site: "(csc.scm:856) main#stop" 2 
o|substituted constant variable: a6008 
o|substituted constant variable: a6015 
o|substituted constant variable: a6012 
o|substituted constant variable: a6020 
o|substituted constant variable: a6025 
o|substituted constant variable: a6034 
o|substituted constant variable: main#constant810 
o|substituted constant variable: main#constant807 
o|substituted constant variable: main#constant814 
o|substituted constant variable: a6037 
o|substituted constant variable: a6046 
o|substituted constant variable: a6048 
o|substituted constant variable: a6050 
o|substituted constant variable: a6052 
o|substituted constant variable: a6054 
o|substituted constant variable: a6056 
o|substituted constant variable: a6058 
o|substituted constant variable: a6060 
o|substituted constant variable: a6062 
o|substituted constant variable: a6064 
o|substituted constant variable: a6066 
o|substituted constant variable: a6068 
o|substituted constant variable: a6070 
o|substituted constant variable: a6075 
o|substituted constant variable: a6077 
o|inlining procedure: k6081 
o|inlining procedure: k6081 
o|substituted constant variable: a6088 
o|substituted constant variable: a6090 
o|substituted constant variable: a6092 
o|substituted constant variable: a6094 
o|substituted constant variable: a6096 
o|substituted constant variable: a6098 
o|substituted constant variable: a6100 
o|substituted constant variable: a6102 
o|substituted constant variable: a6104 
o|substituted constant variable: a6106 
o|substituted constant variable: a6108 
o|substituted constant variable: a6110 
o|substituted constant variable: a6112 
o|substituted constant variable: a6114 
o|substituted constant variable: a6119 
o|substituted constant variable: a6121 
o|substituted constant variable: a6126 
o|substituted constant variable: a6128 
o|substituted constant variable: a6130 
o|substituted constant variable: a6132 
o|substituted constant variable: a6134 
o|substituted constant variable: a6136 
o|substituted constant variable: a6138 
o|substituted constant variable: a6140 
o|substituted constant variable: a6142 
o|substituted constant variable: a6147 
o|substituted constant variable: a6149 
o|substituted constant variable: a6151 
o|substituted constant variable: a6153 
o|substituted constant variable: a6158 
o|substituted constant variable: a6160 
o|substituted constant variable: a6162 
o|substituted constant variable: a6164 
o|substituted constant variable: a6166 
o|substituted constant variable: a6171 
o|substituted constant variable: a6173 
o|substituted constant variable: a6178 
o|substituted constant variable: a6180 
o|substituted constant variable: a6185 
o|substituted constant variable: a6187 
o|substituted constant variable: a6192 
o|substituted constant variable: a6194 
o|substituted constant variable: a6196 
o|substituted constant variable: a6198 
o|substituted constant variable: a6200 
o|substituted constant variable: a6202 
o|substituted constant variable: a6204 
o|substituted constant variable: a6206 
o|substituted constant variable: a6208 
o|substituted constant variable: a6210 
o|substituted constant variable: a6212 
o|substituted constant variable: a6214 
o|substituted constant variable: a6216 
o|substituted constant variable: a6218 
o|substituted constant variable: a6223 
o|substituted constant variable: a6225 
o|inlining procedure: k7610 
o|inlining procedure: k7610 
o|inlining procedure: k7621 
o|contracted procedure: "(csc.scm:248) g857858" 
o|inlining procedure: k7652 
o|contracted procedure: "(csc.scm:250) g868877" 
o|inlining procedure: k7652 
o|inlining procedure: k7686 
o|inlining procedure: k7686 
o|inlining procedure: k7621 
o|inlining procedure: k7748 
o|inlining procedure: k7748 
o|inlining procedure: k7758 
o|propagated global variable: r77598160 main#host-cflags 
o|inlining procedure: k7758 
o|propagated global variable: r77598162 main#default-cflags 
o|inlining procedure: k7761 
o|inlining procedure: k7761 
o|propagated global variable: r77628165 main#cygwin 
o|inlining procedure: k7765 
o|propagated global variable: r77668166 main#host-cxx 
o|inlining procedure: k7765 
o|propagated global variable: r77668168 main#default-cxx 
o|inlining procedure: k7769 
o|propagated global variable: r77708170 main#host-cc 
o|inlining procedure: k7769 
o|propagated global variable: r77708172 main#default-cc 
o|inlining procedure: k7773 
o|inlining procedure: k7773 
o|inlining procedure: k7783 
o|propagated global variable: r77848178 main#host-cxx 
o|inlining procedure: k7783 
o|propagated global variable: r77848180 main#default-cxx 
o|inlining procedure: k7787 
o|propagated global variable: r77888182 main#host-cc 
o|inlining procedure: k7787 
o|propagated global variable: r77888184 main#default-cc 
o|inlining procedure: k7828 
o|inlining procedure: k7828 
o|simplifications: ((if . 2)) 
o|replaced variables: 1180 
o|removed binding forms: 453 
o|removed side-effect free assignment to unused variable: main#every 
o|removed side-effect free assignment to unused variable: main#any 
o|removed side-effect free assignment to unused variable: main#list-tabulate 
o|propagated global variable: out757760 ##sys#standard-error 
o|removed side-effect free assignment to unused variable: main#link-output-flag 
o|removed side-effect free assignment to unused variable: main#compile-output-flag 
o|removed side-effect free assignment to unused variable: main#default-translation-optimization-options 
o|removed side-effect free assignment to unused variable: main#constant807 
o|removed side-effect free assignment to unused variable: main#constant810 
o|removed side-effect free assignment to unused variable: main#constant814 
o|removed side-effect free assignment to unused variable: main#compile-only-flag 
o|substituted constant variable: mode193 
o|folded constant expression: (scheme#eq? (quote target) (quote target)) 
o|substituted constant variable: r73027929 
o|substituted constant variable: r74297935 
o|substituted constant variable: r74297938 
o|propagated global variable: out17251728 ##sys#standard-output 
o|substituted constant variable: r43767947 
o|substituted constant variable: r43767947 
o|inlining procedure: k4401 
o|substituted constant variable: r44027953 
o|substituted constant variable: r44027953 
o|inlining procedure: k4421 
o|propagated global variable: r44228210 main#object-extension 
o|propagated global variable: r44228210 main#object-extension 
o|propagated global variable: out10541057 ##sys#standard-error 
o|substituted constant variable: r45467990 
o|substituted constant variable: r45467990 
o|substituted constant variable: r45467992 
o|substituted constant variable: r45467992 
o|substituted constant variable: r31757997 
o|propagated global variable: lst470 main#linked-extensions 
o|substituted constant variable: r64807999 
o|substituted constant variable: r65038001 
o|substituted constant variable: r63508029 
o|substituted constant variable: r63508029 
o|inlining procedure: k6349 
o|substituted constant variable: r63878033 
o|substituted constant variable: r63878033 
o|inlining procedure: k6386 
o|substituted constant variable: r41158042 
o|substituted constant variable: r41158042 
o|substituted constant variable: r41948044 
o|substituted constant variable: r41948044 
o|inlining procedure: k4193 
o|propagated global variable: r41948274 main#host-libdir 
o|propagated global variable: r41948274 main#host-libdir 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|substituted constant variable: r55248108 
o|inlining procedure: k4699 
o|substituted constant variable: r55338110 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|inlining procedure: k4699 
o|contracted procedure: "(mini-srfi-1.scm:166) main#filter" 
o|substituted constant variable: r31317890 
o|inlining procedure: k4699 
o|substituted constant variable: r76118146 
o|substituted constant variable: r76118146 
o|substituted constant variable: r76228154 
o|substituted constant variable: r76228154 
o|removed call to pure procedure with unused result: "(csc.scm:66) scheme#eq?" 
o|replaced variables: 146 
o|removed binding forms: 1025 
o|removed conditional forms: 1 
o|contracted procedure: k3793 
o|inlining procedure: k3874 
o|contracted procedure: k2472 
o|inlining procedure: k7494 
o|propagated global variable: r74958591 main#last-exit-code 
o|inlining procedure: k7494 
o|propagated global variable: r74958593 main#last-exit-code 
o|inlining procedure: k7503 
o|contracted procedure: k4401 
o|contracted procedure: k4421 
o|propagated global variable: r4422 main#executable-extension 
o|inlining procedure: k7018 
o|inlining procedure: k6949 
o|inlining procedure: k4538 
o|propagated global variable: r45398624 main#quotewrap-no-slash-trans 
o|inlining procedure: k4538 
o|propagated global variable: r45398628 main#quotewrap 
o|contracted procedure: "(mini-srfi-1.scm:134) g480481" 
o|inlining procedure: k6496 
o|inlining procedure: k6496 
o|inlining procedure: k6415 
o|contracted procedure: k6349 
o|contracted procedure: k6386 
o|inlining procedure: k4636 
o|inlining procedure: k4636 
o|inlining procedure: k4658 
o|inlining procedure: k4170 
o|inlining procedure: k4170 
o|inlining procedure: k4699 
o|removed call to pure procedure with unused result: "(csc.scm:66) chicken.platform#software-type" 
o|replaced variables: 4 
o|removed binding forms: 275 
o|contracted procedure: k7818 
o|substituted constant variable: r2473 
o|substituted constant variable: r44028208 
o|substituted constant variable: r4402 
o|inlining procedure: "(csc.scm:608) main#quotewrap-no-slash-trans" 
o|propagated global variable: str7778708 main#target-filename 
o|substituted constant variable: r64978634 
o|substituted constant variable: r64978635 
o|substituted constant variable: r63508260 
o|substituted constant variable: r6350 
o|substituted constant variable: r63878262 
o|substituted constant variable: r6387 
o|substituted constant variable: r46378659 
o|substituted constant variable: r41718667 
o|simplifications: ((if . 1) (let . 2)) 
o|replaced variables: 8 
o|removed binding forms: 17 
o|removed conditional forms: 5 
o|removed side-effect free assignment to unused variable: main#host-repo 
o|inlining procedure: k2466 
o|replaced variables: 4 
o|removed binding forms: 18 
o|substituted constant variable: r24678745 
o|removed binding forms: 4 
o|removed conditional forms: 1 
o|removed binding forms: 1 
o|simplifications: ((if . 40) (let . 11) (##core#call . 391)) 
o|  call simplifications:
o|    scheme#assq
o|    ##sys#call-with-values
o|    scheme#string-ci=?
o|    ##sys#size	4
o|    chicken.fixnum#fx>	4
o|    scheme#string
o|    scheme#cadr
o|    scheme#number?
o|    ##sys#list
o|    scheme#member	10
o|    scheme#cdr	20
o|    scheme#string=?	2
o|    scheme#equal?
o|    scheme#length	5
o|    scheme#>=	2
o|    scheme#char=?
o|    scheme#string->list	4
o|    scheme#memq	5
o|    scheme#char-whitespace?	4
o|    scheme#list->string
o|    scheme#string-length
o|    scheme#string-ref
o|    scheme#list	31
o|    ##sys#check-list	15
o|    scheme#pair?	20
o|    ##sys#setslot	10
o|    ##sys#slot	48
o|    scheme#eq?	83
o|    scheme#not	11
o|    ##sys#apply
o|    scheme#null?	14
o|    scheme#car	29
o|    scheme#cons	57
o|contracted procedure: k7856 
o|contracted procedure: k2358 
o|contracted procedure: k2954 
o|contracted procedure: k2965 
o|contracted procedure: k3797 
o|contracted procedure: k3801 
o|contracted procedure: k3805 
o|contracted procedure: k3809 
o|contracted procedure: k3852 
o|contracted procedure: k3855 
o|contracted procedure: k3892 
o|contracted procedure: k3920 
o|contracted procedure: k3952 
o|contracted procedure: k4031 
o|contracted procedure: k4039 
o|contracted procedure: k4080 
o|contracted procedure: k7731 
o|contracted procedure: k7727 
o|contracted procedure: k4083 
o|contracted procedure: k2484 
o|contracted procedure: k2466 
o|contracted procedure: k4275 
o|contracted procedure: k4295 
o|contracted procedure: k6710 
o|contracted procedure: k6716 
o|contracted procedure: k6728 
o|contracted procedure: k6731 
o|contracted procedure: k6734 
o|contracted procedure: k6742 
o|contracted procedure: k6750 
o|contracted procedure: k7197 
o|contracted procedure: k7203 
o|contracted procedure: k7215 
o|contracted procedure: k7218 
o|contracted procedure: k7221 
o|contracted procedure: k7229 
o|contracted procedure: k7237 
o|contracted procedure: k7258 
o|contracted procedure: k7344 
o|contracted procedure: k7353 
o|contracted procedure: k7304 
o|contracted procedure: k7307 
o|contracted procedure: k7313 
o|contracted procedure: k7337 
o|inlining procedure: k7325 
o|inlining procedure: k7325 
o|contracted procedure: k7396 
o|contracted procedure: k7431 
o|contracted procedure: k7435 
o|contracted procedure: k4371 
o|contracted procedure: k4378 
o|inlining procedure: k4364 
o|contracted procedure: k4375 
o|inlining procedure: k4364 
o|contracted procedure: k4436 
o|contracted procedure: k6763 
o|contracted procedure: k6766 
o|contracted procedure: k6787 
o|contracted procedure: k6799 
o|contracted procedure: k6809 
o|contracted procedure: k6813 
o|contracted procedure: k6816 
o|contracted procedure: k6878 
o|contracted procedure: k6886 
o|contracted procedure: k6912 
o|contracted procedure: k6915 
o|contracted procedure: k6918 
o|contracted procedure: k6926 
o|contracted procedure: k6934 
o|propagated global variable: g14931497 main#object-files 
o|contracted procedure: k7071 
o|contracted procedure: k3085 
o|contracted procedure: k3088 
o|contracted procedure: k3098 
o|contracted procedure: k3010 
o|contracted procedure: k3036 
o|contracted procedure: k3016 
o|contracted procedure: k6965 
o|contracted procedure: k6979 
o|contracted procedure: k6982 
o|contracted procedure: k6988 
o|contracted procedure: k7000 
o|contracted procedure: k7006 
o|contracted procedure: k7014 
o|contracted procedure: k7022 
o|contracted procedure: k7034 
o|contracted procedure: k7037 
o|contracted procedure: k7040 
o|contracted procedure: k7048 
o|contracted procedure: k7056 
o|contracted procedure: k7114 
o|contracted procedure: k7121 
o|contracted procedure: k7125 
o|contracted procedure: k7138 
o|contracted procedure: k7134 
o|contracted procedure: k4474 
o|contracted procedure: k4527 
o|contracted procedure: k4548 
o|contracted procedure: k3165 
o|contracted procedure: k3177 
o|contracted procedure: k3200 
o|contracted procedure: k3208 
o|propagated global variable: lst470 main#linked-extensions 
o|contracted procedure: k6444 
o|contracted procedure: k6448 
o|contracted procedure: k6460 
o|contracted procedure: k6456 
o|contracted procedure: k6479 
o|contracted procedure: k6490 
o|contracted procedure: k6510 
o|contracted procedure: k6496 
o|contracted procedure: k6513 
o|contracted procedure: k6531 
o|contracted procedure: k6535 
o|contracted procedure: k6543 
o|contracted procedure: k6554 
o|contracted procedure: k6567 
o|contracted procedure: k6573 
o|contracted procedure: k6585 
o|contracted procedure: k6595 
o|contracted procedure: k6599 
o|propagated global variable: g14371439 main#generated-rc-files 
o|contracted procedure: k6608 
o|contracted procedure: k6618 
o|contracted procedure: k6622 
o|propagated global variable: g14201422 main#generated-c-files 
o|contracted procedure: k6635 
o|contracted procedure: k6645 
o|contracted procedure: k6649 
o|propagated global variable: g13771401 main#rc-files 
o|contracted procedure: k6662 
o|contracted procedure: k6666 
o|contracted procedure: k7583 
o|contracted procedure: k7579 
o|contracted procedure: k7575 
o|contracted procedure: k6686 
o|contracted procedure: k6696 
o|contracted procedure: k6700 
o|propagated global variable: g13671379 main#c-files 
o|contracted procedure: k4559 
o|contracted procedure: k4580 
o|contracted procedure: k4577 
o|contracted procedure: k3300 
o|contracted procedure: k3290 
o|contracted procedure: k4603 
o|contracted procedure: k4583 
o|contracted procedure: k6400 
o|contracted procedure: k6412 
o|contracted procedure: k6426 
o|contracted procedure: k6397 
o|contracted procedure: k6392 
o|contracted procedure: k6231 
o|contracted procedure: k64228653 
o|contracted procedure: k6255 
o|contracted procedure: k6259 
o|contracted procedure: k6262 
o|contracted procedure: k6266 
o|contracted procedure: k6294 
o|contracted procedure: k6298 
o|contracted procedure: k6304 
o|contracted procedure: k6316 
o|contracted procedure: k6319 
o|contracted procedure: k6322 
o|contracted procedure: k6330 
o|contracted procedure: k6338 
o|contracted procedure: k6369 
o|contracted procedure: k6379 
o|propagated global variable: g13041306 main#scheme-files 
o|contracted procedure: k4623 
o|contracted procedure: k4626 
o|contracted procedure: k4630 
o|contracted procedure: k4644 
o|contracted procedure: k4648 
o|contracted procedure: k4119 
o|contracted procedure: k4139 
o|contracted procedure: k4142 
o|contracted procedure: k4145 
o|contracted procedure: k4153 
o|contracted procedure: k4161 
o|contracted procedure: k4692 
o|contracted procedure: k4705 
o|contracted procedure: k4708 
o|contracted procedure: k4345 
o|contracted procedure: k4341 
o|contracted procedure: k4337 
o|contracted procedure: k4333 
o|contracted procedure: k4329 
o|contracted procedure: k4325 
o|contracted procedure: k4321 
o|contracted procedure: k4720 
o|contracted procedure: k4736 
o|contracted procedure: k4767 
o|contracted procedure: k4775 
o|contracted procedure: k4781 
o|contracted procedure: k4788 
o|contracted procedure: k4792 
o|contracted procedure: k4799 
o|contracted procedure: k4807 
o|contracted procedure: k4815 
o|contracted procedure: k4827 
o|contracted procedure: k4839 
o|contracted procedure: k4851 
o|contracted procedure: k4863 
o|contracted procedure: k4871 
o|contracted procedure: k4874 
o|contracted procedure: k4888 
o|contracted procedure: k4896 
o|contracted procedure: k4902 
o|contracted procedure: k4905 
o|contracted procedure: k4909 
o|contracted procedure: k4918 
o|contracted procedure: k4921 
o|contracted procedure: k4931 
o|contracted procedure: k4934 
o|contracted procedure: k4944 
o|contracted procedure: k4951 
o|contracted procedure: k4958 
o|contracted procedure: k4965 
o|contracted procedure: k4968 
o|contracted procedure: k4973 
o|contracted procedure: k4979 
o|contracted procedure: k4993 
o|contracted procedure: k5001 
o|contracted procedure: k5005 
o|contracted procedure: k5011 
o|contracted procedure: k5018 
o|contracted procedure: k5022 
o|contracted procedure: k5028 
o|contracted procedure: k5031 
o|contracted procedure: k5041 
o|contracted procedure: k5045 
o|contracted procedure: k5051 
o|contracted procedure: k4411 
o|contracted procedure: k5060 
o|contracted procedure: k5070 
o|contracted procedure: k5080 
o|contracted procedure: k5087 
o|contracted procedure: k5092 
o|contracted procedure: k5099 
o|contracted procedure: k5113 
o|contracted procedure: k5120 
o|contracted procedure: k5130 
o|contracted procedure: k5138 
o|contracted procedure: k5144 
o|contracted procedure: k5147 
o|contracted procedure: k5153 
o|contracted procedure: k5157 
o|contracted procedure: k5164 
o|contracted procedure: k5167 
o|contracted procedure: k5177 
o|contracted procedure: k5187 
o|contracted procedure: k5197 
o|contracted procedure: k5207 
o|contracted procedure: k5217 
o|contracted procedure: k5227 
o|contracted procedure: k5237 
o|contracted procedure: k5247 
o|contracted procedure: k5257 
o|contracted procedure: k5267 
o|contracted procedure: k5280 
o|contracted procedure: k5291 
o|contracted procedure: k5295 
o|contracted procedure: k5301 
o|contracted procedure: k5309 
o|contracted procedure: k5321 
o|contracted procedure: k5324 
o|contracted procedure: k5333 
o|contracted procedure: k5340 
o|contracted procedure: k5344 
o|contracted procedure: k5350 
o|contracted procedure: k5357 
o|contracted procedure: k5361 
o|contracted procedure: k5367 
o|contracted procedure: k5374 
o|contracted procedure: k5378 
o|contracted procedure: k5384 
o|contracted procedure: k5391 
o|contracted procedure: k5395 
o|contracted procedure: k5401 
o|contracted procedure: k5412 
o|contracted procedure: k5416 
o|contracted procedure: k5422 
o|contracted procedure: k5433 
o|contracted procedure: k5441 
o|contracted procedure: k5447 
o|contracted procedure: k5455 
o|contracted procedure: k5461 
o|contracted procedure: k5472 
o|contracted procedure: k5480 
o|contracted procedure: k5486 
o|contracted procedure: k5493 
o|contracted procedure: k5500 
o|contracted procedure: k5511 
o|contracted procedure: k5526 
o|contracted procedure: k5535 
o|contracted procedure: k5541 
o|contracted procedure: k5551 
o|contracted procedure: k5555 
o|contracted procedure: k5561 
o|contracted procedure: k5571 
o|contracted procedure: k5575 
o|contracted procedure: k5581 
o|contracted procedure: k5601 
o|contracted procedure: k5614 
o|contracted procedure: k5610 
o|contracted procedure: k5623 
o|contracted procedure: k5632 
o|contracted procedure: k5642 
o|contracted procedure: k5646 
o|contracted procedure: k6017 
o|contracted procedure: k6009 
o|contracted procedure: k5661 
o|contracted procedure: k5679 
o|contracted procedure: k5682 
o|contracted procedure: k5706 
o|contracted procedure: k5709 
o|contracted procedure: k5747 
o|contracted procedure: k5761 
o|contracted procedure: k5849 
o|contracted procedure: k5767 
o|contracted procedure: k5770 
o|contracted procedure: k5776 
o|contracted procedure: k5784 
o|contracted procedure: k5806 
o|contracted procedure: k5809 
o|contracted procedure: k5812 
o|contracted procedure: k5820 
o|contracted procedure: k5828 
o|contracted procedure: k5794 
o|contracted procedure: k3390 
o|contracted procedure: k3397 
o|contracted procedure: k3420 
o|contracted procedure: k3413 
o|contracted procedure: k3121 
o|contracted procedure: k3133 
o|contracted procedure: k3151 
o|contracted procedure: k3159 
o|contracted procedure: k5863 
o|contracted procedure: k5854 
o|contracted procedure: k5889 
o|contracted procedure: k5897 
o|contracted procedure: k5903 
o|contracted procedure: k5911 
o|contracted procedure: k5917 
o|contracted procedure: k5925 
o|contracted procedure: k5931 
o|contracted procedure: k5943 
o|contracted procedure: k5947 
o|contracted procedure: k5953 
o|contracted procedure: k5962 
o|contracted procedure: k5978 
o|contracted procedure: k5986 
o|contracted procedure: k6001 
o|contracted procedure: k6031 
o|contracted procedure: k6022 
o|contracted procedure: k6040 
o|contracted procedure: k6078 
o|contracted procedure: k7626 
o|contracted procedure: k7634 
o|contracted procedure: k7643 
o|contracted procedure: k7655 
o|contracted procedure: k7658 
o|contracted procedure: k7661 
o|contracted procedure: k7669 
o|contracted procedure: k7677 
o|contracted procedure: k7689 
o|contracted procedure: k7692 
o|contracted procedure: k7695 
o|contracted procedure: k7703 
o|contracted procedure: k7711 
o|contracted procedure: k7870 
o|contracted procedure: k7874 
o|contracted procedure: k7878 
o|simplifications: ((if . 4) (let . 92)) 
o|removed binding forms: 362 
o|inlining procedure: k7254 
o|inlining procedure: k7254 
o|contracted procedure: k6355 
o|inlining procedure: k5126 
o|inlining procedure: k5126 
o|inlining procedure: k5276 
o|inlining procedure: k5276 
o|inlining procedure: k5496 
o|inlining procedure: k5496 
o|substituted constant variable: r7871 
o|substituted constant variable: r7875 
o|substituted constant variable: r7879 
o|simplifications: ((let . 2)) 
o|replaced variables: 10 
o|removed binding forms: 5 
o|removed binding forms: 16 
o|direct leaf routine/allocation: g490491 3 
o|direct leaf routine/allocation: loop530 0 
o|direct leaf routine/allocation: g12201221 3 
o|direct leaf routine/allocation: a3406 0 
o|contracted procedure: k4573 
o|converted assignments to bindings: (loop530) 
o|inlining procedure: "(csc.scm:801) k4699" 
o|contracted procedure: "(mini-srfi-1.scm:131) k3141" 
o|simplifications: ((let . 1)) 
o|removed binding forms: 2 
o|direct leaf routine/allocation: g462463 3 
x|eliminated type checks:
x|  C_i_check_list_2:	1
o|customizable procedures: (k3938 k7614 map-loop889906 map-loop862913 k5312 k5595 k5598 k5652 k5970 k5934 k5753 foldr457460 foldl563567 map-loop12371255 k5503 shared-build1011 check1010 k4877 t-options1009 loop1033 k4103 k4107 map-loop941966 k4609 k4612 k6290 k6345 map-loop13181339 k6243 for-each-loop12971351 generate-target-filename1013 g13611378 for-each-loop13601391 k6652 k6519 g13711400 for-each-loop13701406 for-each-loop14131423 for-each-loop14301440 k6472 main#compiler-options foldr475478 k4465 k7084 map-loop15671584 k7003 map-loop15441591 loop1598 loop420 loop440 map-loop14811498 main#linker-options main#linker-libraries main#command for-each-loop15151525 main#stop g17211722 main#string-any fold1671 main#cons* lp1684 main#libchicken map-loop16371654 map-loop14531470 main#repo-path loop407) 
o|calls to known targets: 316 
o|identified direct recursive calls: f_2949 1 
o|identified direct recursive calls: f_7299 3 
o|identified direct recursive calls: f_3005 2 
o|identified direct recursive calls: f_3172 1 
o|identified direct recursive calls: f_3285 1 
o|identified direct recursive calls: f_3128 1 
o|fast box initializations: 30 
o|fast global references: 432 
o|fast global assignments: 202 
o|dropping unused closure argument: f_2943 
o|dropping unused closure argument: f_3285 
o|dropping unused closure argument: f_3819 
o|dropping unused closure argument: f_3943 
o|dropping unused closure argument: f_4228 
o|dropping unused closure argument: f_4352 
o|dropping unused closure argument: f_4359 
o|dropping unused closure argument: f_4385 
o|dropping unused closure argument: f_4415 
o|dropping unused closure argument: f_6704 
o|dropping unused closure argument: f_7191 
o|dropping unused closure argument: f_7244 
o|dropping unused closure argument: f_7342 
o|dropping unused closure argument: f_7488 
*/
/* end of file */
