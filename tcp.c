/* Generated from tcp.scm by the CHICKEN compiler
   http://www.call-cc.org
   Version 5.2.0 (rev 317468e4)
   linux-unix-gnu-x86-64 [ 64bit dload ptables ]
   command line: tcp.scm -optimize-level 2 -include-path . -include-path ./ -inline -ignore-repository -feature chicken-bootstrap -no-warnings -specialize -consult-types-file ./types.db -explicit-use -no-trace -output-file tcp.c -emit-import-library chicken.tcp
   unit: tcp
   uses: port scheduler library
*/
#include "chicken.h"

#ifdef _WIN32
# include <winsock2.h>
# include <ws2tcpip.h>
/* Beware: winsock2.h must come BEFORE windows.h */
# define socklen_t	 int
static WSADATA wsa;
# ifndef SHUT_RD
#  define SHUT_RD	  SD_RECEIVE
# endif
# ifndef SHUT_WR
#  define SHUT_WR	  SD_SEND
# endif

# define typecorrect_getsockopt(socket, level, optname, optval, optlen)	\
    getsockopt(socket, level, optname, (char *)optval, optlen)

static C_word make_socket_nonblocking (C_word sock) {
  int fd = C_unfix(sock);
  C_return(C_mk_bool(ioctlsocket(fd, FIONBIO, (void *)&fd) != SOCKET_ERROR)) ;
}

/* This is a bit of a hack, but it keeps things simple */
static C_TLS char *last_wsa_errorstring = NULL;

static char *errormsg_from_code(int code) {
  int bufsize;
  if (last_wsa_errorstring != NULL) {
    LocalFree(last_wsa_errorstring);
    last_wsa_errorstring = NULL;
  }
  bufsize = FormatMessage(
	FORMAT_MESSAGE_ALLOCATE_BUFFER |
	FORMAT_MESSAGE_FROM_SYSTEM |
	FORMAT_MESSAGE_IGNORE_INSERTS,
	NULL, code, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
	(LPTSTR) &last_wsa_errorstring, 0, NULL);
  if (bufsize == 0) return "ERROR WHILE FETCHING ERROR";
  return last_wsa_errorstring;
}

# define get_last_socket_error()  WSAGetLastError()
# define should_retry_call()      (WSAGetLastError() == WSAEWOULDBLOCK)
/* Not EINPROGRESS in winsock.  Nonblocking connect returns EWOULDBLOCK... */
# define call_in_progress()       (WSAGetLastError() == WSAEWOULDBLOCK)
# define call_was_interrupted()   (WSAGetLastError() == WSAEINTR) /* ? */

#else
# include <errno.h>
# include <fcntl.h>
# include <sys/socket.h>
# include <sys/time.h>
# include <netinet/in.h>
# include <netdb.h>
# include <signal.h>
# define closesocket     close
# define INVALID_SOCKET  -1
# define SOCKET_ERROR    -1
# define typecorrect_getsockopt getsockopt

static C_word make_socket_nonblocking (C_word sock) {
  int fd = C_unfix(sock);
  int val = fcntl(fd, F_GETFL, 0);
  if(val == -1) C_return(C_SCHEME_FALSE);
  C_return(C_mk_bool(fcntl(fd, F_SETFL, val | O_NONBLOCK) != -1));
}

# define get_last_socket_error()  errno
# define errormsg_from_code(e)    strerror(e)

# define should_retry_call()      (errno == EAGAIN || errno == EWOULDBLOCK)
# define call_was_interrupted()   (errno == EINTR)
# define call_in_progress()       (errno == EINPROGRESS)
#endif

#ifdef ECOS
#include <sys/sockio.h>
#endif

#ifndef h_addr
# define h_addr  h_addr_list[ 0 ]
#endif

static char addr_buffer[ 20 ];

static int C_set_socket_options(int socket)
{
  int yes = 1; 
  int r;

  r = setsockopt(socket, SOL_SOCKET, SO_REUSEADDR, (const char *)&yes, sizeof(int));
  
  if(r != 0) return r;

#ifdef SO_NOSIGPIPE
  /*
   * Avoid SIGPIPE (iOS uses *only* SIGPIPE otherwise, not returning EPIPE).
   * For consistency we do this everywhere the option is supported.
   */
  r = setsockopt(socket, SOL_SOCKET, SO_NOSIGPIPE, (const char *)&yes, sizeof(int));
#endif

  return r;
}


static C_PTABLE_ENTRY *create_ptable(void);
C_noret_decl(C_port_toplevel)
C_externimport void C_ccall C_port_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_scheduler_toplevel)
C_externimport void C_ccall C_scheduler_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_library_toplevel)
C_externimport void C_ccall C_library_toplevel(C_word c,C_word *av) C_noret;

static C_TLS C_word lf[109];
static double C_possibly_force_alignment;
static C_char C_TLS li0[] C_aligned={C_lihdr(0,0,50),40,99,104,105,99,107,101,110,46,116,99,112,35,103,101,116,104,111,115,116,97,100,100,114,32,115,97,100,100,114,50,54,48,32,104,111,115,116,50,54,49,32,112,111,114,116,50,54,50,41,0,0,0,0,0,0};
static C_char C_TLS li1[] C_aligned={C_lihdr(0,0,36),40,99,104,105,99,107,101,110,46,116,99,112,35,116,99,112,45,108,105,115,116,101,110,32,112,111,114,116,32,46,32,114,101,115,116,41,0,0,0,0};
static C_char C_TLS li2[] C_aligned={C_lihdr(0,0,29),40,99,104,105,99,107,101,110,46,116,99,112,35,116,99,112,45,108,105,115,116,101,110,101,114,63,32,120,41,0,0,0};
static C_char C_TLS li3[] C_aligned={C_lihdr(0,0,28),40,99,104,105,99,107,101,110,46,116,99,112,35,116,99,112,45,99,108,111,115,101,32,116,99,112,108,41,0,0,0,0};
static C_char C_TLS li4[] C_aligned={C_lihdr(0,0,10),40,102,95,49,54,50,52,32,120,41,0,0,0,0,0,0};
static C_char C_TLS li5[] C_aligned={C_lihdr(0,0,11),40,99,104,101,99,107,32,108,111,99,41,0,0,0,0,0};
static C_char C_TLS li6[] C_aligned={C_lihdr(0,0,6),40,108,111,111,112,41,0,0};
static C_char C_TLS li7[] C_aligned={C_lihdr(0,0,12),40,114,101,97,100,45,105,110,112,117,116,41,0,0,0,0};
static C_char C_TLS li8[] C_aligned={C_lihdr(0,0,7),40,97,49,56,51,56,41,0};
static C_char C_TLS li9[] C_aligned={C_lihdr(0,0,21),40,108,111,111,112,32,108,101,110,32,111,102,102,115,101,116,32,100,108,119,41,0,0,0};
static C_char C_TLS li10[] C_aligned={C_lihdr(0,0,10),40,111,117,116,112,117,116,32,115,41,0,0,0,0,0,0};
static C_char C_TLS li11[] C_aligned={C_lihdr(0,0,10),40,102,95,50,48,50,49,32,115,41,0,0,0,0,0,0};
static C_char C_TLS li12[] C_aligned={C_lihdr(0,0,10),40,102,95,50,48,52,49,32,115,41,0,0,0,0,0,0};
static C_char C_TLS li13[] C_aligned={C_lihdr(0,0,7),40,97,49,57,51,52,41,0};
static C_char C_TLS li14[] C_aligned={C_lihdr(0,0,8),40,102,95,50,48,48,53,41};
static C_char C_TLS li15[] C_aligned={C_lihdr(0,0,7),40,97,50,48,53,54,41,0};
static C_char C_TLS li16[] C_aligned={C_lihdr(0,0,7),40,97,50,48,55,56,41,0};
static C_char C_TLS li17[] C_aligned={C_lihdr(0,0,7),40,97,50,49,50,48,41,0};
static C_char C_TLS li18[] C_aligned={C_lihdr(0,0,7),40,97,50,49,55,48,41,0};
static C_char C_TLS li19[] C_aligned={C_lihdr(0,0,16),40,108,111,111,112,32,110,32,109,32,115,116,97,114,116,41};
static C_char C_TLS li20[] C_aligned={C_lihdr(0,0,22),40,97,50,49,56,56,32,112,32,110,32,100,101,115,116,32,115,116,97,114,116,41,0,0};
static C_char C_TLS li21[] C_aligned={C_lihdr(0,0,11),40,97,50,50,56,49,32,112,111,115,41,0,0,0,0,0};
static C_char C_TLS li22[] C_aligned={C_lihdr(0,0,7),40,97,50,50,55,49,41,0};
static C_char C_TLS li23[] C_aligned={C_lihdr(0,0,28),40,97,50,51,50,55,32,110,101,120,116,32,108,105,110,101,32,102,117,108,108,45,108,105,110,101,63,41,0,0,0,0};
static C_char C_TLS li24[] C_aligned={C_lihdr(0,0,15),40,97,50,50,53,51,32,112,32,108,105,109,105,116,41,0};
static C_char C_TLS li25[] C_aligned={C_lihdr(0,0,9),40,97,50,51,55,50,32,112,41,0,0,0,0,0,0,0};
static C_char C_TLS li26[] C_aligned={C_lihdr(0,0,29),40,99,104,105,99,107,101,110,46,116,99,112,35,105,111,45,112,111,114,116,115,32,108,111,99,32,102,100,41,0,0,0};
static C_char C_TLS li27[] C_aligned={C_lihdr(0,0,6),40,108,111,111,112,41,0,0};
static C_char C_TLS li28[] C_aligned={C_lihdr(0,0,29),40,99,104,105,99,107,101,110,46,116,99,112,35,116,99,112,45,97,99,99,101,112,116,32,116,99,112,108,41,0,0,0};
static C_char C_TLS li29[] C_aligned={C_lihdr(0,0,36),40,99,104,105,99,107,101,110,46,116,99,112,35,116,99,112,45,97,99,99,101,112,116,45,114,101,97,100,121,63,32,116,99,112,108,41,0,0,0,0};
static C_char C_TLS li30[] C_aligned={C_lihdr(0,0,6),40,108,111,111,112,41,0,0};
static C_char C_TLS li31[] C_aligned={C_lihdr(0,0,8),40,108,111,111,112,32,105,41};
static C_char C_TLS li32[] C_aligned={C_lihdr(0,0,7),40,97,50,56,49,56,41,0};
static C_char C_TLS li33[] C_aligned={C_lihdr(0,0,23),40,97,50,56,50,52,32,104,111,115,116,55,48,48,32,112,111,114,116,55,48,49,41,0};
static C_char C_TLS li34[] C_aligned={C_lihdr(0,0,37),40,99,104,105,99,107,101,110,46,116,99,112,35,116,99,112,45,99,111,110,110,101,99,116,32,104,111,115,116,32,46,32,109,111,114,101,41,0,0,0};
static C_char C_TLS li35[] C_aligned={C_lihdr(0,0,36),40,99,104,105,99,107,101,110,46,116,99,112,35,116,99,112,45,112,111,114,116,45,62,102,105,108,101,110,111,32,112,32,108,111,99,41,0,0,0,0};
static C_char C_TLS li36[] C_aligned={C_lihdr(0,0,29),40,99,104,105,99,107,101,110,46,116,99,112,35,116,99,112,45,97,100,100,114,101,115,115,101,115,32,112,41,0,0,0};
static C_char C_TLS li37[] C_aligned={C_lihdr(0,0,32),40,99,104,105,99,107,101,110,46,116,99,112,35,116,99,112,45,112,111,114,116,45,110,117,109,98,101,114,115,32,112,41};
static C_char C_TLS li38[] C_aligned={C_lihdr(0,0,36),40,99,104,105,99,107,101,110,46,116,99,112,35,116,99,112,45,108,105,115,116,101,110,101,114,45,112,111,114,116,32,116,99,112,108,41,0,0,0,0};
static C_char C_TLS li39[] C_aligned={C_lihdr(0,0,32),40,99,104,105,99,107,101,110,46,116,99,112,35,116,99,112,45,97,98,97,110,100,111,110,45,112,111,114,116,32,112,41};
static C_char C_TLS li40[] C_aligned={C_lihdr(0,0,35),40,99,104,105,99,107,101,110,46,116,99,112,35,116,99,112,45,108,105,115,116,101,110,101,114,45,102,105,108,101,110,111,32,108,41,0,0,0,0,0};
static C_char C_TLS li41[] C_aligned={C_lihdr(0,0,10),40,116,111,112,108,101,118,101,108,41,0,0,0,0,0,0};


#define return(x) C_cblock C_r = (C_fix((C_word)(x))); goto C_ret; C_cblockend
C_regparm static C_word C_fcall stub685(C_word C_buf,C_word C_a0){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int socket=(int )C_unfix(C_a0);
int err, optlen;
optlen = sizeof(err);
if (typecorrect_getsockopt(socket, SOL_SOCKET, SO_ERROR, &err, (socklen_t *)&optlen) == SOCKET_ERROR)
  C_return(SOCKET_ERROR);
C_return(err);
C_ret:
#undef return

return C_r;}

#define return(x) C_cblock C_r = (((C_word)(x))); goto C_ret; C_cblockend
C_regparm static C_word C_fcall stub394(C_word C_buf,C_word C_a0,C_word C_a1){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
void * saddr=(void * )C_data_pointer_or_null(C_a0);
unsigned short port=(unsigned short )(unsigned short)C_unfix(C_a1);
struct sockaddr_in *addr = (struct sockaddr_in *)saddr;
memset(addr, 0, sizeof(struct sockaddr_in));
addr->sin_family = AF_INET;
addr->sin_port = htons(port);
addr->sin_addr.s_addr = htonl(INADDR_ANY);
C_ret:
#undef return

return C_r;}

#define return(x) C_cblock C_r = (C_mk_bool((x))); goto C_ret; C_cblockend
C_regparm static C_word C_fcall stub263(C_word C_buf,C_word C_a0,C_word C_a1,C_word C_a2){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
void * saddr=(void * )C_data_pointer_or_null(C_a0);
char * host=(char * )C_string_or_null(C_a1);
unsigned short port=(unsigned short )(unsigned short)C_unfix(C_a2);
struct hostent *he = gethostbyname(host);
struct sockaddr_in *addr = (struct sockaddr_in *)saddr;
if(he == NULL) C_return(0);
memset(addr, 0, sizeof(struct sockaddr_in));
addr->sin_family = AF_INET;
addr->sin_port = htons((short)port);
addr->sin_addr = *((struct in_addr *)he->h_addr);
C_return(1);
C_ret:
#undef return

return C_r;}

#define return(x) C_cblock C_r = (C_fix((C_word)(x))); goto C_ret; C_cblockend
C_regparm static C_word C_fcall stub251(C_word C_buf,C_word C_a0,C_word C_a1){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
char * serv=(char * )C_string_or_null(C_a0);
char * proto=(char * )C_string_or_null(C_a1);
struct servent *se;
     if((se = getservbyname(serv, proto)) == NULL) C_return(0);
     else C_return(ntohs(se->s_port));
C_ret:
#undef return

return C_r;}

#define return(x) C_cblock C_r = (C_mk_bool((x))); goto C_ret; C_cblockend
C_regparm static C_word C_fcall stub246(C_word C_buf){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
#ifdef _WIN32
     C_return(WSAStartup(MAKEWORD(1, 1), &wsa) == 0);
#else
     signal(SIGPIPE, SIG_IGN);
     C_return(1);
#endif
C_ret:
#undef return

return C_r;}

#define return(x) C_cblock C_r = (C_mpointer(&C_a,(void*)(x))); goto C_ret; C_cblockend
C_regparm static C_word C_fcall stub241(C_word C_buf,C_word C_a0){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int s=(int )C_unfix(C_a0);
struct sockaddr_in sa;
unsigned char *ptr;
unsigned int len = sizeof(struct sockaddr_in);
if(getpeername(s, (struct sockaddr *)&sa, ((socklen_t *)&len)) != 0) C_return(NULL);
ptr = (unsigned char *)&sa.sin_addr;
C_snprintf(addr_buffer, sizeof(addr_buffer), "%d.%d.%d.%d", ptr[ 0 ], ptr[ 1 ], ptr[ 2 ], ptr[ 3 ]);
C_return(addr_buffer);
C_ret:
#undef return

return C_r;}

#define return(x) C_cblock C_r = (C_fix((C_word)(x))); goto C_ret; C_cblockend
C_regparm static C_word C_fcall stub236(C_word C_buf,C_word C_a0){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int s=(int )C_unfix(C_a0);
struct sockaddr_in sa;
int len = sizeof(struct sockaddr_in);
if(getpeername(s, (struct sockaddr *)&sa, (socklen_t *)(&len)) != 0) C_return(-1);
else C_return(ntohs(sa.sin_port));
C_ret:
#undef return

return C_r;}

#define return(x) C_cblock C_r = (C_fix((C_word)(x))); goto C_ret; C_cblockend
C_regparm static C_word C_fcall stub231(C_word C_buf,C_word C_a0){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int s=(int )C_unfix(C_a0);
struct sockaddr_in sa;
int len = sizeof(struct sockaddr_in);
if(getsockname(s, (struct sockaddr *)&sa, (socklen_t *)(&len)) != 0) C_return(-1);
else C_return(ntohs(sa.sin_port));
C_ret:
#undef return

return C_r;}

#define return(x) C_cblock C_r = (C_mpointer(&C_a,(void*)(x))); goto C_ret; C_cblockend
C_regparm static C_word C_fcall stub225(C_word C_buf,C_word C_a0){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int s=(int )C_unfix(C_a0);
struct sockaddr_in sa;
unsigned char *ptr;
int len = sizeof(struct sockaddr_in);
if(getsockname(s, (struct sockaddr *)&sa, (socklen_t *)&len) != 0) C_return(NULL);
ptr = (unsigned char *)&sa.sin_addr;
C_snprintf(addr_buffer, sizeof(addr_buffer), "%d.%d.%d.%d", ptr[ 0 ], ptr[ 1 ], ptr[ 2 ], ptr[ 3 ]);
C_return(addr_buffer);
C_ret:
#undef return

return C_r;}

#define return(x) C_cblock C_r = (C_fix((C_word)(x))); goto C_ret; C_cblockend
C_regparm static C_word C_fcall stub214(C_word C_buf,C_word C_a0,C_word C_a1,C_word C_a2,C_word C_a3,C_word C_a4){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int s=(int )C_unfix(C_a0);
void * msg=(void * )C_data_pointer_or_null(C_a1);
int offset=(int )C_unfix(C_a2);
int len=(int )C_unfix(C_a3);
int flags=(int )C_unfix(C_a4);
C_return(send(s, (char *)msg+offset, len, flags));
C_ret:
#undef return

return C_r;}

/* from k1151 */
C_regparm static C_word C_fcall stub205(C_word C_buf,C_word C_a0){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int t0=(int )C_unfix(C_a0);
C_r=C_fix((C_word)C_set_socket_options(t0));
return C_r;}

/* from k1144 */
C_regparm static C_word C_fcall stub200(C_word C_buf,C_word C_a0){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int t0=(int )C_unfix(C_a0);
C_r=C_fix((C_word)C_check_fd_ready(t0));
return C_r;}

/* from k1134 */
C_regparm static C_word C_fcall stub191(C_word C_buf,C_word C_a0,C_word C_a1,C_word C_a2){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int t0=(int )C_unfix(C_a0);
void * t1=(void * )C_data_pointer_or_null(C_a1);
int t2=(int )C_unfix(C_a2);
C_r=C_fix((C_word)connect(t0,t1,t2));
return C_r;}

/* from k1119 */
C_regparm static C_word C_fcall stub183(C_word C_buf,C_word C_a0,C_word C_a1){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int t0=(int )C_unfix(C_a0);
int t1=(int )C_unfix(C_a1);
C_r=C_fix((C_word)shutdown(t0,t1));
return C_r;}

/* from k1105 */
C_regparm static C_word C_fcall stub172(C_word C_buf,C_word C_a0,C_word C_a1,C_word C_a2,C_word C_a3){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int t0=(int )C_unfix(C_a0);
void * t1=(void * )C_data_pointer_or_null(C_a1);
int t2=(int )C_unfix(C_a2);
int t3=(int )C_unfix(C_a3);
C_r=C_fix((C_word)recv(t0,t1,t2,t3));
return C_r;}

/* from k1086 */
C_regparm static C_word C_fcall stub164(C_word C_buf,C_word C_a0){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int t0=(int )C_unfix(C_a0);
C_r=C_fix((C_word)closesocket(t0));
return C_r;}

/* from k1073 */
C_regparm static C_word C_fcall stub153(C_word C_buf,C_word C_a0,C_word C_a1,C_word C_a2){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int t0=(int )C_unfix(C_a0);
void * t1=(void * )C_c_pointer_or_null(C_a1);
void * t2=(void * )C_c_pointer_or_null(C_a2);
C_r=C_fix((C_word)accept(t0,t1,t2));
return C_r;}

/* from k1058 */
C_regparm static C_word C_fcall stub145(C_word C_buf,C_word C_a0,C_word C_a1){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int t0=(int )C_unfix(C_a0);
int t1=(int )C_unfix(C_a1);
C_r=C_fix((C_word)listen(t0,t1));
return C_r;}

/* from k1044 */
C_regparm static C_word C_fcall stub135(C_word C_buf,C_word C_a0,C_word C_a1,C_word C_a2){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int t0=(int )C_unfix(C_a0);
void * t1=(void * )C_data_pointer_or_null(C_a1);
int t2=(int )C_unfix(C_a2);
C_r=C_fix((C_word)bind(t0,t1,t2));
return C_r;}

/* from k1029 */
C_regparm static C_word C_fcall stub126(C_word C_buf,C_word C_a0,C_word C_a1,C_word C_a2){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int t0=(int )C_unfix(C_a0);
int t1=(int )C_unfix(C_a1);
int t2=(int )C_unfix(C_a2);
C_r=C_fix((C_word)socket(t0,t1,t2));
return C_r;}

/* from chicken.tcp#interrupted? */
C_regparm static C_word C_fcall stub120(C_word C_buf){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
C_r=C_mk_bool(call_was_interrupted());
return C_r;}

/* from chicken.tcp#in-progress? */
C_regparm static C_word C_fcall stub117(C_word C_buf){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
C_r=C_mk_bool(call_in_progress());
return C_r;}

/* from chicken.tcp#retry? */
C_regparm static C_word C_fcall stub114(C_word C_buf){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
C_r=C_mk_bool(should_retry_call());
return C_r;}

/* from k1005 */
C_regparm static C_word C_fcall stub109(C_word C_buf,C_word C_a0){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
int t0=(int )C_unfix(C_a0);
C_r=C_mpointer(&C_a,(void*)errormsg_from_code(t0));
return C_r;}

/* from chicken.tcp#last-error-code */
C_regparm static C_word C_fcall stub105(C_word C_buf){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
C_r=C_fix((C_word)get_last_socket_error());
return C_r;}

C_noret_decl(f3432)
static void C_ccall f3432(C_word c,C_word *av) C_noret;
C_noret_decl(f_1221)
static void C_ccall f_1221(C_word c,C_word *av) C_noret;
C_noret_decl(f_1228)
static void C_ccall f_1228(C_word c,C_word *av) C_noret;
C_noret_decl(f_1232)
static void C_ccall f_1232(C_word c,C_word *av) C_noret;
C_noret_decl(f_1248)
static void C_fcall f_1248(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_1257)
static void C_ccall f_1257(C_word c,C_word *av) C_noret;
C_noret_decl(f_1283)
static void C_fcall f_1283(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_1306)
static void C_ccall f_1306(C_word c,C_word *av) C_noret;
C_noret_decl(f_1310)
static void C_ccall f_1310(C_word c,C_word *av) C_noret;
C_noret_decl(f_1313)
static void C_fcall f_1313(C_word t0,C_word t1) C_noret;
C_noret_decl(f_1316)
static void C_ccall f_1316(C_word c,C_word *av) C_noret;
C_noret_decl(f_1326)
static void C_ccall f_1326(C_word c,C_word *av) C_noret;
C_noret_decl(f_1330)
static void C_ccall f_1330(C_word c,C_word *av) C_noret;
C_noret_decl(f_1334)
static void C_ccall f_1334(C_word c,C_word *av) C_noret;
C_noret_decl(f_1372)
static void C_ccall f_1372(C_word c,C_word *av) C_noret;
C_noret_decl(f_1375)
static void C_ccall f_1375(C_word c,C_word *av) C_noret;
C_noret_decl(f_1381)
static void C_ccall f_1381(C_word c,C_word *av) C_noret;
C_noret_decl(f_1384)
static void C_ccall f_1384(C_word c,C_word *av) C_noret;
C_noret_decl(f_1387)
static void C_ccall f_1387(C_word c,C_word *av) C_noret;
C_noret_decl(f_1403)
static void C_ccall f_1403(C_word c,C_word *av) C_noret;
C_noret_decl(f_1407)
static void C_ccall f_1407(C_word c,C_word *av) C_noret;
C_noret_decl(f_1411)
static void C_ccall f_1411(C_word c,C_word *av) C_noret;
C_noret_decl(f_1427)
static void C_ccall f_1427(C_word c,C_word *av) C_noret;
C_noret_decl(f_1431)
static void C_ccall f_1431(C_word c,C_word *av) C_noret;
C_noret_decl(f_1435)
static void C_ccall f_1435(C_word c,C_word *av) C_noret;
C_noret_decl(f_1457)
static void C_ccall f_1457(C_word c,C_word *av) C_noret;
C_noret_decl(f_1466)
static void C_ccall f_1466(C_word c,C_word *av) C_noret;
C_noret_decl(f_1482)
static void C_ccall f_1482(C_word c,C_word *av) C_noret;
C_noret_decl(f_1485)
static void C_ccall f_1485(C_word c,C_word *av) C_noret;
C_noret_decl(f_1488)
static void C_ccall f_1488(C_word c,C_word *av) C_noret;
C_noret_decl(f_1491)
static void C_fcall f_1491(C_word t0,C_word t1) C_noret;
C_noret_decl(f_1494)
static void C_ccall f_1494(C_word c,C_word *av) C_noret;
C_noret_decl(f_1513)
static void C_ccall f_1513(C_word c,C_word *av) C_noret;
C_noret_decl(f_1517)
static void C_ccall f_1517(C_word c,C_word *av) C_noret;
C_noret_decl(f_1521)
static void C_ccall f_1521(C_word c,C_word *av) C_noret;
C_noret_decl(f_1565)
static void C_ccall f_1565(C_word c,C_word *av) C_noret;
C_noret_decl(f_1574)
static void C_ccall f_1574(C_word c,C_word *av) C_noret;
C_noret_decl(f_1594)
static void C_ccall f_1594(C_word c,C_word *av) C_noret;
C_noret_decl(f_1598)
static void C_ccall f_1598(C_word c,C_word *av) C_noret;
C_noret_decl(f_1602)
static void C_ccall f_1602(C_word c,C_word *av) C_noret;
C_noret_decl(f_1616)
static void C_ccall f_1616(C_word c,C_word *av) C_noret;
C_noret_decl(f_1622)
static void C_fcall f_1622(C_word t0,C_word t1) C_noret;
C_noret_decl(f_1624)
static void C_ccall f_1624(C_word c,C_word *av) C_noret;
C_noret_decl(f_1628)
static void C_ccall f_1628(C_word c,C_word *av) C_noret;
C_noret_decl(f_1639)
static void C_ccall f_1639(C_word c,C_word *av) C_noret;
C_noret_decl(f_1643)
static void C_ccall f_1643(C_word c,C_word *av) C_noret;
C_noret_decl(f_1647)
static void C_ccall f_1647(C_word c,C_word *av) C_noret;
C_noret_decl(f_1651)
static void C_ccall f_1651(C_word c,C_word *av) C_noret;
C_noret_decl(f_1653)
static void C_fcall f_1653(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_1657)
static void C_ccall f_1657(C_word c,C_word *av) C_noret;
C_noret_decl(f_1660)
static void C_ccall f_1660(C_word c,C_word *av) C_noret;
C_noret_decl(f_1666)
static void C_ccall f_1666(C_word c,C_word *av) C_noret;
C_noret_decl(f_1669)
static void C_fcall f_1669(C_word t0,C_word t1) C_noret;
C_noret_decl(f_1670)
static void C_fcall f_1670(C_word t0,C_word t1) C_noret;
C_noret_decl(f_1674)
static void C_ccall f_1674(C_word c,C_word *av) C_noret;
C_noret_decl(f_1677)
static void C_fcall f_1677(C_word t0,C_word t1) C_noret;
C_noret_decl(f_1682)
static void C_ccall f_1682(C_word c,C_word *av) C_noret;
C_noret_decl(f_1701)
static void C_ccall f_1701(C_word c,C_word *av) C_noret;
C_noret_decl(f_1704)
static void C_ccall f_1704(C_word c,C_word *av) C_noret;
C_noret_decl(f_1707)
static void C_ccall f_1707(C_word c,C_word *av) C_noret;
C_noret_decl(f_1710)
static void C_ccall f_1710(C_word c,C_word *av) C_noret;
C_noret_decl(f_1738)
static void C_ccall f_1738(C_word c,C_word *av) C_noret;
C_noret_decl(f_1742)
static void C_ccall f_1742(C_word c,C_word *av) C_noret;
C_noret_decl(f_1746)
static void C_ccall f_1746(C_word c,C_word *av) C_noret;
C_noret_decl(f_1764)
static void C_ccall f_1764(C_word c,C_word *av) C_noret;
C_noret_decl(f_1767)
static void C_ccall f_1767(C_word c,C_word *av) C_noret;
C_noret_decl(f_1768)
static void C_fcall f_1768(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_1772)
static void C_ccall f_1772(C_word c,C_word *av) C_noret;
C_noret_decl(f_1783)
static void C_fcall f_1783(C_word t0,C_word t1) C_noret;
C_noret_decl(f_1785)
static void C_fcall f_1785(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_1807)
static void C_ccall f_1807(C_word c,C_word *av) C_noret;
C_noret_decl(f_1810)
static void C_ccall f_1810(C_word c,C_word *av) C_noret;
C_noret_decl(f_1813)
static void C_ccall f_1813(C_word c,C_word *av) C_noret;
C_noret_decl(f_1816)
static void C_ccall f_1816(C_word c,C_word *av) C_noret;
C_noret_decl(f_1839)
static void C_ccall f_1839(C_word c,C_word *av) C_noret;
C_noret_decl(f_1850)
static void C_ccall f_1850(C_word c,C_word *av) C_noret;
C_noret_decl(f_1854)
static void C_ccall f_1854(C_word c,C_word *av) C_noret;
C_noret_decl(f_1858)
static void C_ccall f_1858(C_word c,C_word *av) C_noret;
C_noret_decl(f_1898)
static void C_ccall f_1898(C_word c,C_word *av) C_noret;
C_noret_decl(f_1905)
static void C_ccall f_1905(C_word c,C_word *av) C_noret;
C_noret_decl(f_1908)
static void C_ccall f_1908(C_word c,C_word *av) C_noret;
C_noret_decl(f_1923)
static void C_ccall f_1923(C_word c,C_word *av) C_noret;
C_noret_decl(f_1926)
static void C_ccall f_1926(C_word c,C_word *av) C_noret;
C_noret_decl(f_1935)
static void C_ccall f_1935(C_word c,C_word *av) C_noret;
C_noret_decl(f_1943)
static void C_fcall f_1943(C_word t0,C_word t1) C_noret;
C_noret_decl(f_1946)
static void C_fcall f_1946(C_word t0,C_word t1) C_noret;
C_noret_decl(f_1952)
static void C_fcall f_1952(C_word t0,C_word t1) C_noret;
C_noret_decl(f_1959)
static void C_ccall f_1959(C_word c,C_word *av) C_noret;
C_noret_decl(f_1963)
static void C_ccall f_1963(C_word c,C_word *av) C_noret;
C_noret_decl(f_1967)
static void C_ccall f_1967(C_word c,C_word *av) C_noret;
C_noret_decl(f_1992)
static void C_ccall f_1992(C_word c,C_word *av) C_noret;
C_noret_decl(f_2005)
static void C_ccall f_2005(C_word c,C_word *av) C_noret;
C_noret_decl(f_2015)
static void C_ccall f_2015(C_word c,C_word *av) C_noret;
C_noret_decl(f_2021)
static void C_ccall f_2021(C_word c,C_word *av) C_noret;
C_noret_decl(f_2026)
static void C_ccall f_2026(C_word c,C_word *av) C_noret;
C_noret_decl(f_2035)
static void C_ccall f_2035(C_word c,C_word *av) C_noret;
C_noret_decl(f_2041)
static void C_ccall f_2041(C_word c,C_word *av) C_noret;
C_noret_decl(f_2057)
static void C_ccall f_2057(C_word c,C_word *av) C_noret;
C_noret_decl(f_2061)
static void C_ccall f_2061(C_word c,C_word *av) C_noret;
C_noret_decl(f_2079)
static void C_ccall f_2079(C_word c,C_word *av) C_noret;
C_noret_decl(f_2092)
static void C_ccall f_2092(C_word c,C_word *av) C_noret;
C_noret_decl(f_2105)
static void C_ccall f_2105(C_word c,C_word *av) C_noret;
C_noret_decl(f_2109)
static void C_ccall f_2109(C_word c,C_word *av) C_noret;
C_noret_decl(f_2113)
static void C_ccall f_2113(C_word c,C_word *av) C_noret;
C_noret_decl(f_2121)
static void C_ccall f_2121(C_word c,C_word *av) C_noret;
C_noret_decl(f_2129)
static void C_fcall f_2129(C_word t0,C_word t1) C_noret;
C_noret_decl(f_2135)
static void C_fcall f_2135(C_word t0,C_word t1) C_noret;
C_noret_decl(f_2142)
static void C_ccall f_2142(C_word c,C_word *av) C_noret;
C_noret_decl(f_2146)
static void C_ccall f_2146(C_word c,C_word *av) C_noret;
C_noret_decl(f_2150)
static void C_ccall f_2150(C_word c,C_word *av) C_noret;
C_noret_decl(f_2171)
static void C_ccall f_2171(C_word c,C_word *av) C_noret;
C_noret_decl(f_2175)
static void C_ccall f_2175(C_word c,C_word *av) C_noret;
C_noret_decl(f_2189)
static void C_ccall f_2189(C_word c,C_word *av) C_noret;
C_noret_decl(f_2195)
static void C_fcall f_2195(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_2243)
static void C_ccall f_2243(C_word c,C_word *av) C_noret;
C_noret_decl(f_2254)
static void C_ccall f_2254(C_word c,C_word *av) C_noret;
C_noret_decl(f_2258)
static void C_ccall f_2258(C_word c,C_word *av) C_noret;
C_noret_decl(f_2272)
static void C_ccall f_2272(C_word c,C_word *av) C_noret;
C_noret_decl(f_2282)
static void C_ccall f_2282(C_word c,C_word *av) C_noret;
C_noret_decl(f_2298)
static void C_ccall f_2298(C_word c,C_word *av) C_noret;
C_noret_decl(f_2328)
static void C_ccall f_2328(C_word c,C_word *av) C_noret;
C_noret_decl(f_2373)
static void C_ccall f_2373(C_word c,C_word *av) C_noret;
C_noret_decl(f_2383)
static void C_ccall f_2383(C_word c,C_word *av) C_noret;
C_noret_decl(f_2404)
static void C_ccall f_2404(C_word c,C_word *av) C_noret;
C_noret_decl(f_2408)
static void C_ccall f_2408(C_word c,C_word *av) C_noret;
C_noret_decl(f_2412)
static void C_ccall f_2412(C_word c,C_word *av) C_noret;
C_noret_decl(f_2416)
static void C_ccall f_2416(C_word c,C_word *av) C_noret;
C_noret_decl(f_2426)
static void C_ccall f_2426(C_word c,C_word *av) C_noret;
C_noret_decl(f_2429)
static void C_fcall f_2429(C_word t0,C_word t1) C_noret;
C_noret_decl(f_2434)
static void C_ccall f_2434(C_word c,C_word *av) C_noret;
C_noret_decl(f_2438)
static void C_ccall f_2438(C_word c,C_word *av) C_noret;
C_noret_decl(f_2441)
static void C_ccall f_2441(C_word c,C_word *av) C_noret;
C_noret_decl(f_2444)
static void C_ccall f_2444(C_word c,C_word *av) C_noret;
C_noret_decl(f_2447)
static void C_ccall f_2447(C_word c,C_word *av) C_noret;
C_noret_decl(f_2475)
static void C_ccall f_2475(C_word c,C_word *av) C_noret;
C_noret_decl(f_2479)
static void C_ccall f_2479(C_word c,C_word *av) C_noret;
C_noret_decl(f_2483)
static void C_ccall f_2483(C_word c,C_word *av) C_noret;
C_noret_decl(f_2509)
static void C_ccall f_2509(C_word c,C_word *av) C_noret;
C_noret_decl(f_2511)
static void C_ccall f_2511(C_word c,C_word *av) C_noret;
C_noret_decl(f_2521)
static void C_ccall f_2521(C_word c,C_word *av) C_noret;
C_noret_decl(f_2534)
static void C_ccall f_2534(C_word c,C_word *av) C_noret;
C_noret_decl(f_2538)
static void C_ccall f_2538(C_word c,C_word *av) C_noret;
C_noret_decl(f_2542)
static void C_ccall f_2542(C_word c,C_word *av) C_noret;
C_noret_decl(f_2561)
static void C_ccall f_2561(C_word c,C_word *av) C_noret;
C_noret_decl(f_2568)
static void C_ccall f_2568(C_word c,C_word *av) C_noret;
C_noret_decl(f_2571)
static void C_fcall f_2571(C_word t0,C_word t1) C_noret;
C_noret_decl(f_2574)
static void C_ccall f_2574(C_word c,C_word *av) C_noret;
C_noret_decl(f_2580)
static void C_ccall f_2580(C_word c,C_word *av) C_noret;
C_noret_decl(f_2583)
static void C_ccall f_2583(C_word c,C_word *av) C_noret;
C_noret_decl(f_2586)
static void C_ccall f_2586(C_word c,C_word *av) C_noret;
C_noret_decl(f_2592)
static void C_ccall f_2592(C_word c,C_word *av) C_noret;
C_noret_decl(f_2595)
static void C_ccall f_2595(C_word c,C_word *av) C_noret;
C_noret_decl(f_2598)
static void C_ccall f_2598(C_word c,C_word *av) C_noret;
C_noret_decl(f_2601)
static void C_ccall f_2601(C_word c,C_word *av) C_noret;
C_noret_decl(f_2607)
static void C_ccall f_2607(C_word c,C_word *av) C_noret;
C_noret_decl(f_2626)
static void C_ccall f_2626(C_word c,C_word *av) C_noret;
C_noret_decl(f_2630)
static void C_ccall f_2630(C_word c,C_word *av) C_noret;
C_noret_decl(f_2634)
static void C_ccall f_2634(C_word c,C_word *av) C_noret;
C_noret_decl(f_2652)
static void C_ccall f_2652(C_word c,C_word *av) C_noret;
C_noret_decl(f_2656)
static void C_ccall f_2656(C_word c,C_word *av) C_noret;
C_noret_decl(f_2660)
static void C_ccall f_2660(C_word c,C_word *av) C_noret;
C_noret_decl(f_2664)
static void C_ccall f_2664(C_word c,C_word *av) C_noret;
C_noret_decl(f_2680)
static void C_ccall f_2680(C_word c,C_word *av) C_noret;
C_noret_decl(f_2683)
static void C_ccall f_2683(C_word c,C_word *av) C_noret;
C_noret_decl(f_2711)
static void C_ccall f_2711(C_word c,C_word *av) C_noret;
C_noret_decl(f_2715)
static void C_ccall f_2715(C_word c,C_word *av) C_noret;
C_noret_decl(f_2719)
static void C_ccall f_2719(C_word c,C_word *av) C_noret;
C_noret_decl(f_2738)
static void C_ccall f_2738(C_word c,C_word *av) C_noret;
C_noret_decl(f_2742)
static void C_ccall f_2742(C_word c,C_word *av) C_noret;
C_noret_decl(f_2746)
static void C_ccall f_2746(C_word c,C_word *av) C_noret;
C_noret_decl(f_2764)
static void C_ccall f_2764(C_word c,C_word *av) C_noret;
C_noret_decl(f_2768)
static void C_ccall f_2768(C_word c,C_word *av) C_noret;
C_noret_decl(f_2772)
static void C_ccall f_2772(C_word c,C_word *av) C_noret;
C_noret_decl(f_2788)
static void C_ccall f_2788(C_word c,C_word *av) C_noret;
C_noret_decl(f_2792)
static void C_ccall f_2792(C_word c,C_word *av) C_noret;
C_noret_decl(f_2796)
static void C_ccall f_2796(C_word c,C_word *av) C_noret;
C_noret_decl(f_2805)
static void C_ccall f_2805(C_word c,C_word *av) C_noret;
C_noret_decl(f_2811)
static void C_ccall f_2811(C_word c,C_word *av) C_noret;
C_noret_decl(f_2819)
static void C_ccall f_2819(C_word c,C_word *av) C_noret;
C_noret_decl(f_2825)
static void C_ccall f_2825(C_word c,C_word *av) C_noret;
C_noret_decl(f_2836)
static void C_ccall f_2836(C_word c,C_word *av) C_noret;
C_noret_decl(f_2844)
static void C_fcall f_2844(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_2848)
static void C_ccall f_2848(C_word c,C_word *av) C_noret;
C_noret_decl(f_2862)
static void C_ccall f_2862(C_word c,C_word *av) C_noret;
C_noret_decl(f_2868)
static void C_ccall f_2868(C_word c,C_word *av) C_noret;
C_noret_decl(f_2875)
static void C_ccall f_2875(C_word c,C_word *av) C_noret;
C_noret_decl(f_2878)
static void C_ccall f_2878(C_word c,C_word *av) C_noret;
C_noret_decl(f_2882)
static void C_ccall f_2882(C_word c,C_word *av) C_noret;
C_noret_decl(f_2885)
static void C_ccall f_2885(C_word c,C_word *av) C_noret;
C_noret_decl(f_2892)
static void C_ccall f_2892(C_word c,C_word *av) C_noret;
C_noret_decl(f_2896)
static void C_ccall f_2896(C_word c,C_word *av) C_noret;
C_noret_decl(f_2900)
static void C_ccall f_2900(C_word c,C_word *av) C_noret;
C_noret_decl(f_2913)
static void C_ccall f_2913(C_word c,C_word *av) C_noret;
C_noret_decl(f_2917)
static void C_ccall f_2917(C_word c,C_word *av) C_noret;
C_noret_decl(f_2921)
static void C_ccall f_2921(C_word c,C_word *av) C_noret;
C_noret_decl(f_2929)
static void C_ccall f_2929(C_word c,C_word *av) C_noret;
C_noret_decl(f_2935)
static void C_ccall f_2935(C_word c,C_word *av) C_noret;
C_noret_decl(f_2944)
static void C_ccall f_2944(C_word c,C_word *av) C_noret;
C_noret_decl(f_2947)
static void C_ccall f_2947(C_word c,C_word *av) C_noret;
C_noret_decl(f_2960)
static void C_ccall f_2960(C_word c,C_word *av) C_noret;
C_noret_decl(f_2964)
static void C_ccall f_2964(C_word c,C_word *av) C_noret;
C_noret_decl(f_2968)
static void C_ccall f_2968(C_word c,C_word *av) C_noret;
C_noret_decl(f_2984)
static void C_ccall f_2984(C_word c,C_word *av) C_noret;
C_noret_decl(f_2988)
static void C_ccall f_2988(C_word c,C_word *av) C_noret;
C_noret_decl(f_2992)
static void C_ccall f_2992(C_word c,C_word *av) C_noret;
C_noret_decl(f_3000)
static void C_ccall f_3000(C_word c,C_word *av) C_noret;
C_noret_decl(f_3013)
static void C_ccall f_3013(C_word c,C_word *av) C_noret;
C_noret_decl(f_3023)
static void C_ccall f_3023(C_word c,C_word *av) C_noret;
C_noret_decl(f_3027)
static void C_ccall f_3027(C_word c,C_word *av) C_noret;
C_noret_decl(f_3031)
static void C_ccall f_3031(C_word c,C_word *av) C_noret;
C_noret_decl(f_3039)
static void C_ccall f_3039(C_word c,C_word *av) C_noret;
C_noret_decl(f_3049)
static void C_ccall f_3049(C_word c,C_word *av) C_noret;
C_noret_decl(f_3055)
static void C_ccall f_3055(C_word c,C_word *av) C_noret;
C_noret_decl(f_3066)
static void C_ccall f_3066(C_word c,C_word *av) C_noret;
C_noret_decl(f_3070)
static void C_ccall f_3070(C_word c,C_word *av) C_noret;
C_noret_decl(f_3074)
static void C_ccall f_3074(C_word c,C_word *av) C_noret;
C_noret_decl(f_3078)
static void C_ccall f_3078(C_word c,C_word *av) C_noret;
C_noret_decl(f_987)
static void C_ccall f_987(C_word c,C_word *av) C_noret;
C_noret_decl(f_990)
static void C_ccall f_990(C_word c,C_word *av) C_noret;
C_noret_decl(f_993)
static void C_ccall f_993(C_word c,C_word *av) C_noret;
C_noret_decl(C_tcp_toplevel)
C_externexport void C_ccall C_tcp_toplevel(C_word c,C_word *av) C_noret;

C_noret_decl(trf_1248)
static void C_ccall trf_1248(C_word c,C_word *av) C_noret;
static void C_ccall trf_1248(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_1248(t0,t1,t2,t3);}

C_noret_decl(trf_1283)
static void C_ccall trf_1283(C_word c,C_word *av) C_noret;
static void C_ccall trf_1283(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_1283(t0,t1,t2);}

C_noret_decl(trf_1313)
static void C_ccall trf_1313(C_word c,C_word *av) C_noret;
static void C_ccall trf_1313(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_1313(t0,t1);}

C_noret_decl(trf_1491)
static void C_ccall trf_1491(C_word c,C_word *av) C_noret;
static void C_ccall trf_1491(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_1491(t0,t1);}

C_noret_decl(trf_1622)
static void C_ccall trf_1622(C_word c,C_word *av) C_noret;
static void C_ccall trf_1622(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_1622(t0,t1);}

C_noret_decl(trf_1653)
static void C_ccall trf_1653(C_word c,C_word *av) C_noret;
static void C_ccall trf_1653(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_1653(t0,t1,t2,t3);}

C_noret_decl(trf_1669)
static void C_ccall trf_1669(C_word c,C_word *av) C_noret;
static void C_ccall trf_1669(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_1669(t0,t1);}

C_noret_decl(trf_1670)
static void C_ccall trf_1670(C_word c,C_word *av) C_noret;
static void C_ccall trf_1670(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_1670(t0,t1);}

C_noret_decl(trf_1677)
static void C_ccall trf_1677(C_word c,C_word *av) C_noret;
static void C_ccall trf_1677(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_1677(t0,t1);}

C_noret_decl(trf_1768)
static void C_ccall trf_1768(C_word c,C_word *av) C_noret;
static void C_ccall trf_1768(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_1768(t0,t1,t2);}

C_noret_decl(trf_1783)
static void C_ccall trf_1783(C_word c,C_word *av) C_noret;
static void C_ccall trf_1783(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_1783(t0,t1);}

C_noret_decl(trf_1785)
static void C_ccall trf_1785(C_word c,C_word *av) C_noret;
static void C_ccall trf_1785(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_1785(t0,t1,t2,t3,t4);}

C_noret_decl(trf_1943)
static void C_ccall trf_1943(C_word c,C_word *av) C_noret;
static void C_ccall trf_1943(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_1943(t0,t1);}

C_noret_decl(trf_1946)
static void C_ccall trf_1946(C_word c,C_word *av) C_noret;
static void C_ccall trf_1946(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_1946(t0,t1);}

C_noret_decl(trf_1952)
static void C_ccall trf_1952(C_word c,C_word *av) C_noret;
static void C_ccall trf_1952(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_1952(t0,t1);}

C_noret_decl(trf_2129)
static void C_ccall trf_2129(C_word c,C_word *av) C_noret;
static void C_ccall trf_2129(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_2129(t0,t1);}

C_noret_decl(trf_2135)
static void C_ccall trf_2135(C_word c,C_word *av) C_noret;
static void C_ccall trf_2135(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_2135(t0,t1);}

C_noret_decl(trf_2195)
static void C_ccall trf_2195(C_word c,C_word *av) C_noret;
static void C_ccall trf_2195(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_2195(t0,t1,t2,t3,t4);}

C_noret_decl(trf_2429)
static void C_ccall trf_2429(C_word c,C_word *av) C_noret;
static void C_ccall trf_2429(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_2429(t0,t1);}

C_noret_decl(trf_2571)
static void C_ccall trf_2571(C_word c,C_word *av) C_noret;
static void C_ccall trf_2571(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_2571(t0,t1);}

C_noret_decl(trf_2844)
static void C_ccall trf_2844(C_word c,C_word *av) C_noret;
static void C_ccall trf_2844(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_2844(t0,t1,t2);}

/* f3432 in loop in k2596 in k2593 in k2590 in k2584 in k2581 in k2578 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f3432(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f3432,c,av);}
/* tcp.scm:610: ##sys#thread-yield! */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[36]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[36]+1);
av2[1]=((C_word*)t0)[2];
tp(2,av2);}}

/* k1219 in k991 in k988 in k985 */
static void C_ccall f_1221(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,5)))){
C_save_and_reclaim((void *)f_1221,c,av);}
a=C_alloc(15);
t2=C_mutate(&lf[2] /* (set! chicken.tcp#gethostaddr ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1248,a[2]=((C_word)li0),tmp=(C_word)a,a+=3,tmp));
t3=C_mutate((C_word*)lf[4]+1 /* (set! chicken.tcp#tcp-listen ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1466,a[2]=((C_word)li1),tmp=(C_word)a,a+=3,tmp));
t4=C_mutate((C_word*)lf[23]+1 /* (set! chicken.tcp#tcp-listener? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1565,a[2]=((C_word)li2),tmp=(C_word)a,a+=3,tmp));
t5=C_mutate((C_word*)lf[24]+1 /* (set! chicken.tcp#tcp-close ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1574,a[2]=((C_word)li3),tmp=(C_word)a,a+=3,tmp));
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1616,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* tcp.scm:345: chicken.base#make-parameter */
t7=*((C_word*)lf[103]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t7;
av2[1]=t6;
av2[2]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}

/* k1226 in k1308 in k1304 in loop in a2818 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1228(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_1228,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1232,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
if(C_truep(((C_word*)t0)[3])){
/* tcp.scm:240: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[3]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[3]+1);
av2[1]=t2;
av2[2]=C_i_foreign_string_argumentp(((C_word*)t0)[3]);
tp(3,av2);}}
else{
t3=((C_word*)t0)[2];
f_1313(t3,stub251(C_SCHEME_UNDEFINED,t1,C_SCHEME_FALSE));}}

/* k1230 in k1226 in k1308 in k1304 in loop in a2818 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1232(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1232,c,av);}
t2=((C_word*)t0)[2];
f_1313(t2,stub251(C_SCHEME_UNDEFINED,((C_word*)t0)[3],t1));}

/* chicken.tcp#gethostaddr in k1219 in k991 in k988 in k985 */
static void C_fcall f_1248(C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_1248,4,t1,t2,t3,t4);}
a=C_alloc(5);
t5=(C_truep(t2)?C_i_foreign_block_argumentp(t2):C_SCHEME_FALSE);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1257,a[2]=t1,a[3]=t5,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
if(C_truep(t3)){
/* tcp.scm:246: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[3]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[3]+1);
av2[1]=t6;
av2[2]=C_i_foreign_string_argumentp(t3);
tp(3,av2);}}
else{
t7=C_fix((C_word)sizeof(unsigned short) * CHAR_BIT);
t8=t1;{
C_word av2[2];
av2[0]=t8;
av2[1]=stub263(C_SCHEME_UNDEFINED,t5,C_SCHEME_FALSE,C_i_foreign_unsigned_ranged_integer_argumentp(t4,t7));
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}}

/* k1255 in chicken.tcp#gethostaddr in k1219 in k991 in k988 in k985 */
static void C_ccall f_1257(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1257,c,av);}
t2=C_fix((C_word)sizeof(unsigned short) * CHAR_BIT);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=stub263(C_SCHEME_UNDEFINED,((C_word*)t0)[3],t1,C_i_foreign_unsigned_ranged_integer_argumentp(((C_word*)t0)[4],t2));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* loop in a2818 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_fcall f_1283(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(6,0,4)))){
C_save_and_reclaim_args((void *)trf_1283,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_fixnum_greater_or_equal_p(t2,((C_word*)t0)[2]))){
/* tcp.scm:282: scheme#values */{
C_word av2[4];
av2[0]=0;
av2[1]=t1;
av2[2]=((C_word*)t0)[3];
av2[3]=C_SCHEME_FALSE;
C_values(4,av2);}}
else{
t3=C_subchar(((C_word*)t0)[3],t2);
if(C_truep(C_i_char_equalp(t3,C_make_character(58)))){
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1306,a[2]=t1,a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[3],a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* tcp.scm:286: substring */
t5=*((C_word*)lf[83]+1);{
C_word av2[5];
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[3];
av2[3]=C_fixnum_plus(t2,C_fix(1));
av2[4]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}
else{
/* tcp.scm:292: loop */
t6=t1;
t7=C_fixnum_plus(t2,C_fix(1));
t1=t6;
t2=t7;
goto loop;}}}

/* k1304 in loop in a2818 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1306(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_1306,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1310,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* tcp.scm:287: substring */
t3=*((C_word*)lf[83]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=C_fix(0);
av2[4]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k1308 in k1304 in loop in a2818 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1310(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_1310,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1313,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1228,a[2]=t2,a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
if(C_truep(t1)){
/* tcp.scm:240: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[3]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[3]+1);
av2[1]=t3;
av2[2]=C_i_foreign_string_argumentp(t1);
tp(3,av2);}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
f_1228(2,av2);}}}

/* k1311 in k1308 in k1304 in loop in a2818 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_fcall f_1313(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,3)))){
C_save_and_reclaim_args((void *)trf_1313,2,t0,t1);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1316,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t3=C_eqp(C_fix(0),t1);
if(C_truep(t3)){
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1326,a[2]=t2,a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1330,a[2]=t4,tmp=(C_word)a,a+=3,tmp);
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[11]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[11]+1);
av2[1]=t5;
av2[2]=lf[82];
av2[3]=lf[13];
tp(4,av2);}}
else{
/* tcp.scm:285: scheme#values */{
C_word av2[4];
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
C_values(4,av2);}}}

/* k1314 in k1311 in k1308 in k1304 in loop in a2818 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1316(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_1316,c,av);}
/* tcp.scm:285: scheme#values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
C_values(4,av2);}}

/* k1324 in k1311 in k1308 in k1304 in loop in a2818 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1326(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_1326,c,av);}
/* tcp.scm:290: ##sys#signal-hook */
t2=*((C_word*)lf[6]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[7];
av2[3]=lf[72];
av2[4]=t1;
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* k1328 in k1311 in k1308 in k1304 in loop in a2818 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1330(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_1330,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1334,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=stub105(C_SCHEME_UNDEFINED);
t4=C_a_i_bytevector(&a,1,C_fix(3));
t5=C_i_foreign_fixnum_argumentp(t3);
/* tcp.scm:171: ##sys#peek-c-string */
t6=*((C_word*)lf[10]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t2;
av2[2]=stub109(t4,t5);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* k1332 in k1328 in k1311 in k1308 in k1304 in loop in a2818 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1334(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_1334,c,av);}
/* tcp.scm:290: scheme#string-append */
t2=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k1370 in k1486 in k1483 in k1480 in chicken.tcp#tcp-listen in k1219 in k991 in k988 in k985 */
static void C_ccall f_1372(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,4)))){
C_save_and_reclaim((void *)f_1372,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_1375,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
if(C_truep(((C_word*)t0)[4])){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1457,a[2]=t2,a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp);
/* tcp.scm:305: gethostaddr */
f_1248(t3,t1,((C_word*)t0)[4],((C_word*)t0)[5]);}
else{
t3=(C_truep(t1)?C_i_foreign_block_argumentp(t1):C_SCHEME_FALSE);
t4=C_fix((C_word)sizeof(unsigned short) * CHAR_BIT);
t5=t2;{
C_word *av2=av;
av2[0]=t5;
av2[1]=stub394(C_SCHEME_UNDEFINED,t3,C_i_foreign_unsigned_ranged_integer_argumentp(((C_word*)t0)[5],t4));
f_1375(2,av2);}}}

/* k1373 in k1370 in k1486 in k1483 in k1480 in chicken.tcp#tcp-listen in k1219 in k991 in k988 in k985 */
static void C_ccall f_1375(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_1375,c,av);}
a=C_alloc(7);
t2=C_fix((C_word)AF_INET);
t3=stub126(C_SCHEME_UNDEFINED,C_i_foreign_fixnum_argumentp(t2),C_i_foreign_fixnum_argumentp(((C_word*)t0)[2]),C_fix(0));
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_1381,a[2]=((C_word*)t0)[3],a[3]=t3,a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
t5=C_eqp(C_fix((C_word)INVALID_SOCKET),t3);
if(C_truep(t5)){
/* tcp.scm:312: ##sys#error */
t6=*((C_word*)lf[16]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t4;
av2[2]=lf[17];
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}
else{
t6=t4;{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_SCHEME_UNDEFINED;
f_1381(2,av2);}}}

/* k1379 in k1373 in k1370 in k1486 in k1483 in k1480 in chicken.tcp#tcp-listen in k1219 in k991 in k988 in k985 */
static void C_ccall f_1381(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,3)))){
C_save_and_reclaim((void *)f_1381,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_1384,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
t3=C_i_foreign_fixnum_argumentp(((C_word*)t0)[3]);
t4=C_eqp(C_fix((C_word)SOCKET_ERROR),stub205(C_SCHEME_UNDEFINED,t3));
if(C_truep(t4)){
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1427,a[2]=t2,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1431,a[2]=t5,tmp=(C_word)a,a+=3,tmp);
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[11]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[11]+1);
av2[1]=t6;
av2[2]=lf[15];
av2[3]=lf[13];
tp(4,av2);}}
else{
t5=t2;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_UNDEFINED;
f_1384(2,av2);}}}

/* k1382 in k1379 in k1373 in k1370 in k1486 in k1483 in k1480 in chicken.tcp#tcp-listen in k1219 in k991 in k988 in k985 */
static void C_ccall f_1384(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,3)))){
C_save_and_reclaim((void *)f_1384,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1387,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=C_fix((C_word)sizeof(struct sockaddr_in));
t4=C_i_foreign_fixnum_argumentp(((C_word*)t0)[3]);
t5=(C_truep(((C_word*)t0)[4])?C_i_foreign_block_argumentp(((C_word*)t0)[4]):C_SCHEME_FALSE);
t6=C_i_foreign_fixnum_argumentp(t3);
t7=C_eqp(C_fix((C_word)SOCKET_ERROR),stub135(C_SCHEME_UNDEFINED,t4,t5,t6));
if(C_truep(t7)){
t8=stub105(C_SCHEME_UNDEFINED);
t9=C_i_foreign_fixnum_argumentp(((C_word*)t0)[3]);
t10=stub164(C_SCHEME_UNDEFINED,t9);
t11=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1403,a[2]=t2,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],tmp=(C_word)a,a+=6,tmp);
t12=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1407,a[2]=t11,a[3]=t8,tmp=(C_word)a,a+=4,tmp);
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[11]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[11]+1);
av2[1]=t12;
av2[2]=lf[14];
av2[3]=lf[13];
tp(4,av2);}}
else{
t8=((C_word*)t0)[2];
f_1491(t8,((C_word*)t0)[3]);}}

/* k1385 in k1382 in k1379 in k1373 in k1370 in k1486 in k1483 in k1480 in chicken.tcp#tcp-listen in k1219 in k991 in k988 in k985 */
static void C_ccall f_1387(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1387,c,av);}
t2=((C_word*)t0)[2];
f_1491(t2,((C_word*)t0)[3]);}

/* k1401 in k1382 in k1379 in k1373 in k1370 in k1486 in k1483 in k1480 in chicken.tcp#tcp-listen in k1219 in k991 in k988 in k985 */
static void C_ccall f_1403(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_1403,c,av);}
/* tcp.scm:317: ##sys#signal-hook */
t2=*((C_word*)lf[6]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[7];
av2[3]=lf[8];
av2[4]=t1;
av2[5]=((C_word*)t0)[3];
av2[6]=((C_word*)t0)[4];
av2[7]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t2+1)))(8,av2);}}

/* k1405 in k1382 in k1379 in k1373 in k1370 in k1486 in k1483 in k1480 in chicken.tcp#tcp-listen in k1219 in k991 in k988 in k985 */
static void C_ccall f_1407(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_1407,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1411,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=C_a_i_bytevector(&a,1,C_fix(3));
t4=C_i_foreign_fixnum_argumentp(((C_word*)t0)[3]);
/* tcp.scm:171: ##sys#peek-c-string */
t5=*((C_word*)lf[10]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=stub109(t3,t4);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k1409 in k1405 in k1382 in k1379 in k1373 in k1370 in k1486 in k1483 in k1480 in chicken.tcp#tcp-listen in k1219 in k991 in k988 in k985 */
static void C_ccall f_1411(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_1411,c,av);}
/* tcp.scm:317: scheme#string-append */
t2=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k1425 in k1379 in k1373 in k1370 in k1486 in k1483 in k1480 in chicken.tcp#tcp-listen in k1219 in k991 in k988 in k985 */
static void C_ccall f_1427(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_1427,c,av);}
/* tcp.scm:315: ##sys#signal-hook */
t2=*((C_word*)lf[6]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[7];
av2[3]=lf[8];
av2[4]=t1;
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* k1429 in k1379 in k1373 in k1370 in k1486 in k1483 in k1480 in chicken.tcp#tcp-listen in k1219 in k991 in k988 in k985 */
static void C_ccall f_1431(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_1431,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1435,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=stub105(C_SCHEME_UNDEFINED);
t4=C_a_i_bytevector(&a,1,C_fix(3));
t5=C_i_foreign_fixnum_argumentp(t3);
/* tcp.scm:171: ##sys#peek-c-string */
t6=*((C_word*)lf[10]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t2;
av2[2]=stub109(t4,t5);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* k1433 in k1429 in k1379 in k1373 in k1370 in k1486 in k1483 in k1480 in chicken.tcp#tcp-listen in k1219 in k991 in k988 in k985 */
static void C_ccall f_1435(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_1435,c,av);}
/* tcp.scm:315: scheme#string-append */
t2=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k1455 in k1370 in k1486 in k1483 in k1480 in chicken.tcp#tcp-listen in k1219 in k991 in k988 in k985 */
static void C_ccall f_1457(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_1457,c,av);}
if(C_truep(t1)){
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
f_1375(2,av2);}}
else{
/* tcp.scm:306: ##sys#signal-hook */
t2=*((C_word*)lf[6]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[7];
av2[3]=lf[8];
av2[4]=lf[18];
av2[5]=((C_word*)t0)[3];
av2[6]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(7,av2);}}}

/* chicken.tcp#tcp-listen in k1219 in k991 in k988 in k985 */
static void C_ccall f_1466(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_1466,c,av);}
a=C_alloc(6);
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_fix(100):C_get_rest_arg(c,3,av,3,t0));
t5=C_rest_nullp(c,3);
t6=C_rest_nullp(c,4);
t7=(C_truep(t6)?C_SCHEME_FALSE:C_get_rest_arg(c,4,av,3,t0));
t8=C_rest_nullp(c,4);
t9=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1482,a[2]=t1,a[3]=t4,a[4]=t2,a[5]=t7,tmp=(C_word)a,a+=6,tmp);
/* tcp.scm:323: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[20]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[20]+1);
av2[1]=t9;
av2[2]=t2;
tp(3,av2);}}

/* k1480 in chicken.tcp#tcp-listen in k1219 in k991 in k988 in k985 */
static void C_ccall f_1482(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,5)))){
C_save_and_reclaim((void *)f_1482,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1485,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t3=C_fixnum_lessp(((C_word*)t0)[4],C_fix(0));
if(C_truep(t3)){
if(C_truep(t3)){
/* tcp.scm:325: ##sys#signal-hook */
t4=*((C_word*)lf[6]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=lf[21];
av2[3]=lf[8];
av2[4]=lf[22];
av2[5]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(6,av2);}}
else{
t4=t2;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_1485(2,av2);}}}
else{
if(C_truep(C_fixnum_greaterp(((C_word*)t0)[4],C_fix(65535)))){
/* tcp.scm:325: ##sys#signal-hook */
t4=*((C_word*)lf[6]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=lf[21];
av2[3]=lf[8];
av2[4]=lf[22];
av2[5]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(6,av2);}}
else{
t4=t2;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_1485(2,av2);}}}}

/* k1483 in k1480 in chicken.tcp#tcp-listen in k1219 in k991 in k988 in k985 */
static void C_ccall f_1485(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_1485,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1488,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* tcp.scm:326: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[20]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[20]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
tp(3,av2);}}

/* k1486 in k1483 in k1480 in chicken.tcp#tcp-listen in k1219 in k991 in k988 in k985 */
static void C_ccall f_1488(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,3)))){
C_save_and_reclaim((void *)f_1488,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1491,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=C_fix((C_word)SOCK_STREAM);
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1372,a[2]=t3,a[3]=t2,a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
t5=C_fix((C_word)sizeof(struct sockaddr_in));
/* ##sys#make-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[19]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[19]+1);
av2[1]=t4;
av2[2]=t5;
av2[3]=C_make_character(32);
tp(4,av2);}}

/* k1489 in k1486 in k1483 in k1480 in chicken.tcp#tcp-listen in k1219 in k991 in k988 in k985 */
static void C_fcall f_1491(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,0,3)))){
C_save_and_reclaim_args((void *)trf_1491,2,t0,t1);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1494,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=C_i_foreign_fixnum_argumentp(t1);
t4=C_i_foreign_fixnum_argumentp(((C_word*)t0)[3]);
t5=C_eqp(C_fix((C_word)SOCKET_ERROR),stub145(C_SCHEME_UNDEFINED,t3,t4));
if(C_truep(t5)){
t6=stub105(C_SCHEME_UNDEFINED);
t7=C_i_foreign_fixnum_argumentp(t1);
t8=stub164(C_SCHEME_UNDEFINED,t7);
t9=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1513,a[2]=t2,a[3]=t1,a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t10=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1517,a[2]=t9,a[3]=t6,tmp=(C_word)a,a+=4,tmp);
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[11]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[11]+1);
av2[1]=t10;
av2[2]=lf[12];
av2[3]=lf[13];
tp(4,av2);}}
else{
t6=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t6;
av2[1]=C_a_i_record2(&a,2,lf[5],t1);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* k1492 in k1489 in k1486 in k1483 in k1480 in chicken.tcp#tcp-listen in k1219 in k991 in k988 in k985 */
static void C_ccall f_1494(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_1494,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_record2(&a,2,lf[5],((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k1511 in k1489 in k1486 in k1483 in k1480 in chicken.tcp#tcp-listen in k1219 in k991 in k988 in k985 */
static void C_ccall f_1513(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_1513,c,av);}
/* tcp.scm:329: ##sys#signal-hook */
t2=*((C_word*)lf[6]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[7];
av2[3]=lf[8];
av2[4]=t1;
av2[5]=((C_word*)t0)[3];
av2[6]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(7,av2);}}

/* k1515 in k1489 in k1486 in k1483 in k1480 in chicken.tcp#tcp-listen in k1219 in k991 in k988 in k985 */
static void C_ccall f_1517(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_1517,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1521,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=C_a_i_bytevector(&a,1,C_fix(3));
t4=C_i_foreign_fixnum_argumentp(((C_word*)t0)[3]);
/* tcp.scm:171: ##sys#peek-c-string */
t5=*((C_word*)lf[10]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=stub109(t3,t4);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k1519 in k1515 in k1489 in k1486 in k1483 in k1480 in chicken.tcp#tcp-listen in k1219 in k991 in k988 in k985 */
static void C_ccall f_1521(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_1521,c,av);}
/* tcp.scm:329: scheme#string-append */
t2=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.tcp#tcp-listener? in k1219 in k991 in k988 in k985 */
static void C_ccall f_1565(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1565,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=(C_truep(C_blockp(t2))?C_i_structurep(t2,lf[5]):C_SCHEME_FALSE);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.tcp#tcp-close in k1219 in k991 in k988 in k985 */
static void C_ccall f_1574(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_1574,c,av);}
a=C_alloc(7);
t3=C_i_check_structure(t2,lf[5]);
t4=C_slot(t2,C_fix(1));
t5=C_i_foreign_fixnum_argumentp(t4);
t6=C_eqp(C_fix((C_word)SOCKET_ERROR),stub164(C_SCHEME_UNDEFINED,t5));
if(C_truep(t6)){
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1594,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1598,a[2]=t7,tmp=(C_word)a,a+=3,tmp);
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[11]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[11]+1);
av2[1]=t8;
av2[2]=lf[26];
av2[3]=lf[13];
tp(4,av2);}}
else{
t7=C_SCHEME_UNDEFINED;
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=t7;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}}

/* k1592 in chicken.tcp#tcp-close in k1219 in k991 in k988 in k985 */
static void C_ccall f_1594(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_1594,c,av);}
/* tcp.scm:340: ##sys#signal-hook */
t2=*((C_word*)lf[6]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[7];
av2[3]=lf[25];
av2[4]=t1;
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* k1596 in chicken.tcp#tcp-close in k1219 in k991 in k988 in k985 */
static void C_ccall f_1598(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_1598,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1602,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=stub105(C_SCHEME_UNDEFINED);
t4=C_a_i_bytevector(&a,1,C_fix(3));
t5=C_i_foreign_fixnum_argumentp(t3);
/* tcp.scm:171: ##sys#peek-c-string */
t6=*((C_word*)lf[10]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t2;
av2[2]=stub109(t4,t5);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* k1600 in k1596 in chicken.tcp#tcp-close in k1219 in k991 in k988 in k985 */
static void C_ccall f_1602(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_1602,c,av);}
/* tcp.scm:340: scheme#string-append */
t2=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1616(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,3)))){
C_save_and_reclaim((void *)f_1616,c,av);}
a=C_alloc(16);
t2=C_mutate((C_word*)lf[27]+1 /* (set! chicken.tcp#tcp-buffer-size ...) */,t1);
t3=C_set_block_item(lf[28] /* chicken.tcp#tcp-read-timeout */,0,C_SCHEME_UNDEFINED);
t4=C_set_block_item(lf[29] /* chicken.tcp#tcp-write-timeout */,0,C_SCHEME_UNDEFINED);
t5=C_set_block_item(lf[30] /* chicken.tcp#tcp-connect-timeout */,0,C_SCHEME_UNDEFINED);
t6=C_set_block_item(lf[31] /* chicken.tcp#tcp-accept-timeout */,0,C_SCHEME_UNDEFINED);
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1622,a[2]=((C_word)li5),tmp=(C_word)a,a+=3,tmp));
t12=C_fixnum_times(C_fix(60),C_fix(1000));
t13=C_set_block_item(t10,0,t12);
t14=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1639,a[2]=((C_word*)t0)[2],a[3]=t8,a[4]=t10,tmp=(C_word)a,a+=5,tmp);
t15=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3078,a[2]=t14,a[3]=t10,tmp=(C_word)a,a+=4,tmp);
/* tcp.scm:356: check */
f_1622(t15,lf[107]);}

/* check in k1614 in k1219 in k991 in k988 in k985 */
static void C_fcall f_1622(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,3)))){
C_save_and_reclaim_args((void *)trf_1622,2,t1,t2);}
a=C_alloc(4);
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1624,a[2]=t2,a[3]=((C_word)li4),tmp=(C_word)a,a+=4,tmp);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* f_1624 in check in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1624(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_1624,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1628,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
if(C_truep(t2)){
/* tcp.scm:353: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[20]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[20]+1);
av2[1]=t3;
av2[2]=t2;
av2[3]=((C_word*)t0)[2];
tp(4,av2);}}
else{
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k1626 */
static void C_ccall f_1628(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1628,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1639(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_1639,c,av);}
a=C_alloc(8);
t2=C_mutate((C_word*)lf[28]+1 /* (set! chicken.tcp#tcp-read-timeout ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1643,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3074,a[2]=t3,a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
/* tcp.scm:357: check */
f_1622(t4,lf[106]);}

/* k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1643(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_1643,c,av);}
a=C_alloc(7);
t2=C_mutate((C_word*)lf[29]+1 /* (set! chicken.tcp#tcp-write-timeout ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1647,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3070,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* tcp.scm:358: check */
f_1622(t4,lf[105]);}

/* k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1647(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_1647,c,av);}
a=C_alloc(6);
t2=C_mutate((C_word*)lf[30]+1 /* (set! chicken.tcp#tcp-connect-timeout ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1651,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3066,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* tcp.scm:359: check */
f_1622(t4,lf[104]);}

/* k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1651(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(31,c,4)))){
C_save_and_reclaim((void *)f_1651,c,av);}
a=C_alloc(31);
t2=C_mutate((C_word*)lf[31]+1 /* (set! chicken.tcp#tcp-accept-timeout ...) */,t1);
t3=*((C_word*)lf[27]+1);
t4=C_mutate(&lf[32] /* (set! chicken.tcp#io-ports ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1653,a[2]=t3,a[3]=((C_word)li26),tmp=(C_word)a,a+=4,tmp));
t5=C_mutate((C_word*)lf[64]+1 /* (set! chicken.tcp#tcp-accept ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2416,a[2]=((C_word)li28),tmp=(C_word)a,a+=3,tmp));
t6=C_mutate((C_word*)lf[68]+1 /* (set! chicken.tcp#tcp-accept-ready? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2511,a[2]=((C_word)li29),tmp=(C_word)a,a+=3,tmp));
t7=C_mutate((C_word*)lf[71]+1 /* (set! chicken.tcp#tcp-connect ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2561,a[2]=((C_word)li34),tmp=(C_word)a,a+=3,tmp));
t8=C_mutate(&lf[84] /* (set! chicken.tcp#tcp-port->fileno ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2844,a[2]=((C_word)li35),tmp=(C_word)a,a+=3,tmp));
t9=C_mutate((C_word*)lf[88]+1 /* (set! chicken.tcp#tcp-addresses ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2862,a[2]=((C_word)li36),tmp=(C_word)a,a+=3,tmp));
t10=C_mutate((C_word*)lf[92]+1 /* (set! chicken.tcp#tcp-port-numbers ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2929,a[2]=((C_word)li37),tmp=(C_word)a,a+=3,tmp));
t11=C_mutate((C_word*)lf[96]+1 /* (set! chicken.tcp#tcp-listener-port ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3000,a[2]=((C_word)li38),tmp=(C_word)a,a+=3,tmp));
t12=C_mutate((C_word*)lf[99]+1 /* (set! chicken.tcp#tcp-abandon-port ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3039,a[2]=((C_word)li39),tmp=(C_word)a,a+=3,tmp));
t13=C_mutate((C_word*)lf[101]+1 /* (set! chicken.tcp#tcp-listener-fileno ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3055,a[2]=((C_word)li40),tmp=(C_word)a,a+=3,tmp));
t14=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t14;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t14+1)))(2,av2);}}

/* chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_fcall f_1653(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,0,3)))){
C_save_and_reclaim_args((void *)trf_1653,4,t0,t1,t2,t3);}
a=C_alloc(14);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1657,a[2]=t3,a[3]=t1,a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
if(C_truep(make_socket_nonblocking(t3))){
t5=t4;{
C_word av2[2];
av2[0]=t5;
av2[1]=C_SCHEME_UNDEFINED;
f_1657(2,av2);}}
else{
t5=stub105(C_SCHEME_UNDEFINED);
t6=C_i_foreign_fixnum_argumentp(t3);
t7=stub164(C_SCHEME_UNDEFINED,t6);
t8=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2404,a[2]=t4,a[3]=t2,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2408,a[2]=t8,a[3]=t5,tmp=(C_word)a,a+=4,tmp);
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[11]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[11]+1);
av2[1]=t9;
av2[2]=lf[63];
av2[3]=lf[13];
tp(4,av2);}}}

/* k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1657(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_1657,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1660,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* ##sys#make-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[19]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[19]+1);
av2[1]=t2;
av2[2]=C_fix(1024);
av2[3]=C_make_character(32);
tp(4,av2);}}

/* k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1660(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,2)))){
C_save_and_reclaim((void *)f_1660,c,av);}
a=C_alloc(24);
t2=C_a_i_vector5(&a,5,((C_word*)t0)[2],C_SCHEME_FALSE,C_SCHEME_FALSE,t1,C_fix(0));
t3=C_fix(0);
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_fix(0);
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_SCHEME_FALSE;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_SCHEME_FALSE;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_1666,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t4,a[5]=t2,a[6]=t6,a[7]=((C_word*)t0)[3],a[8]=t10,a[9]=t8,tmp=(C_word)a,a+=10,tmp);
/* tcp.scm:372: tbs */
t12=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t12;
av2[1]=t11;
((C_proc)(void*)(*((C_word*)t12+1)))(2,av2);}}

/* k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1666(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_1666,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_1669,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=t1,a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],tmp=(C_word)a,a+=11,tmp);
if(C_truep(t1)){
t3=C_fixnum_greaterp(t1,C_fix(0));
t4=t2;
f_1669(t4,(C_truep(t3)?lf[62]:C_SCHEME_FALSE));}
else{
t3=t2;
f_1669(t3,C_SCHEME_FALSE);}}

/* k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_fcall f_1669(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(66,0,8)))){
C_save_and_reclaim_args((void *)trf_1669,2,t0,t1);}
a=C_alloc(66);
t2=t1;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_1670,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word)li7),tmp=(C_word)a,a+=8,tmp);
t5=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_1767,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[7],a[4]=((C_word*)t0)[5],a[5]=t3,a[6]=((C_word*)t0)[8],a[7]=((C_word*)t0)[9],a[8]=((C_word*)t0)[10],tmp=(C_word)a,a+=9,tmp);
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_2057,a[2]=((C_word*)t0)[6],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[3],a[5]=t4,a[6]=((C_word)li15),tmp=(C_word)a,a+=7,tmp);
t7=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2079,a[2]=((C_word*)t0)[6],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[2],a[5]=((C_word)li16),tmp=(C_word)a,a+=6,tmp);
t8=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_2121,a[2]=((C_word*)t0)[10],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[9],a[5]=((C_word*)t0)[5],a[6]=((C_word)li17),tmp=(C_word)a,a+=7,tmp);
t9=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_2171,a[2]=((C_word*)t0)[6],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[3],a[5]=t4,a[6]=((C_word)li18),tmp=(C_word)a,a+=7,tmp);
t10=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_2189,a[2]=((C_word*)t0)[6],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[3],a[5]=t4,a[6]=((C_word)li20),tmp=(C_word)a,a+=7,tmp);
t11=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_2254,a[2]=((C_word*)t0)[6],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[3],a[5]=t4,a[6]=((C_word)li24),tmp=(C_word)a,a+=7,tmp);
t12=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2373,a[2]=((C_word*)t0)[6],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[3],a[5]=((C_word)li25),tmp=(C_word)a,a+=6,tmp);
/* tcp.scm:401: chicken.port#make-input-port */
t13=*((C_word*)lf[61]+1);{
C_word av2[9];
av2[0]=t13;
av2[1]=t5;
av2[2]=t6;
av2[3]=t7;
av2[4]=t8;
av2[5]=t9;
av2[6]=t10;
av2[7]=t11;
av2[8]=t12;
((C_proc)(void*)(*((C_word*)t13+1)))(9,av2);}}

/* read-input in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_fcall f_1670(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_1670,2,t0,t1);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_1674,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t1,tmp=(C_word)a,a+=8,tmp);
/* tcp.scm:376: tcp-read-timeout */
t3=*((C_word*)lf[28]+1);{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k1672 in read-input in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1674(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_1674,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_1677,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
if(C_truep(t1)){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1764,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* tcp.scm:377: chicken.time#current-milliseconds */
t4=*((C_word*)lf[42]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=t2;
f_1677(t3,C_SCHEME_FALSE);}}

/* k1675 in k1672 in read-input in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_fcall f_1677(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,0,2)))){
C_save_and_reclaim_args((void *)trf_1677,2,t0,t1);}
a=C_alloc(13);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_1682,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t3,a[5]=((C_word*)t0)[4],a[6]=t1,a[7]=((C_word*)t0)[5],a[8]=((C_word*)t0)[6],a[9]=((C_word*)t0)[7],a[10]=((C_word)li6),tmp=(C_word)a,a+=11,tmp));
t5=((C_word*)t3)[1];{
C_word av2[2];
av2[0]=t5;
av2[1]=((C_word*)t0)[8];
f_1682(2,av2);}}

/* loop in k1675 in k1672 in read-input in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1682(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_1682,c,av);}
a=C_alloc(7);
t2=C_i_foreign_fixnum_argumentp(((C_word*)t0)[2]);
t3=(C_truep(((C_word*)t0)[3])?C_i_foreign_block_argumentp(((C_word*)t0)[3]):C_SCHEME_FALSE);
t4=C_fix(1024);
t5=C_fix(0);
t6=stub172(C_SCHEME_UNDEFINED,t2,t3,t4,t5);
t7=C_eqp(C_fix((C_word)SOCKET_ERROR),t6);
if(C_truep(t7)){
if(C_truep(stub114(C_SCHEME_UNDEFINED))){
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1701,a[2]=((C_word*)t0)[4],a[3]=t1,a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[2],tmp=(C_word)a,a+=6,tmp);
if(C_truep(((C_word*)t0)[6])){
/* tcp.scm:383: ##sys#thread-block-for-timeout! */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[39]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[39]+1);
av2[1]=t8;
av2[2]=*((C_word*)lf[33]+1);
av2[3]=((C_word*)t0)[6];
tp(4,av2);}}
else{
t9=t8;{
C_word *av2=av;
av2[0]=t9;
av2[1]=C_SCHEME_UNDEFINED;
f_1701(2,av2);}}}
else{
if(C_truep(stub120(C_SCHEME_UNDEFINED))){
/* tcp.scm:393: ##sys#dispatch-interrupt */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[40]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[40]+1);
av2[1]=t1;
av2[2]=((C_word*)((C_word*)t0)[4])[1];
tp(3,av2);}}
else{
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1738,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
t9=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1742,a[2]=t8,tmp=(C_word)a,a+=3,tmp);
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[11]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[11]+1);
av2[1]=t9;
av2[2]=lf[41];
av2[3]=lf[13];
tp(4,av2);}}}}
else{
t8=C_set_block_item(((C_word*)t0)[7],0,t6);
t9=C_i_set_i_slot(((C_word*)t0)[8],C_fix(4),t6);
t10=C_set_block_item(((C_word*)t0)[9],0,C_fix(0));
t11=t1;{
C_word *av2=av;
av2[0]=t11;
av2[1]=t10;
((C_proc)(void*)(*((C_word*)t11+1)))(2,av2);}}}

/* k1699 in loop in k1675 in k1672 in read-input in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1701(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_1701,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1704,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* tcp.scm:385: ##sys#thread-block-for-i/o! */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[37]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[37]+1);
av2[1]=t2;
av2[2]=*((C_word*)lf[33]+1);
av2[3]=((C_word*)t0)[5];
av2[4]=lf[38];
tp(5,av2);}}

/* k1702 in k1699 in loop in k1675 in k1672 in read-input in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1704(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_1704,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1707,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* tcp.scm:386: ##sys#thread-yield! */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[36]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[36]+1);
av2[1]=t2;
tp(2,av2);}}

/* k1705 in k1702 in k1699 in loop in k1675 in k1672 in read-input in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1707(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,5)))){
C_save_and_reclaim((void *)f_1707,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1710,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_slot(*((C_word*)lf[33]+1),C_fix(13)))){
/* tcp.scm:388: ##sys#signal-hook */
t3=*((C_word*)lf[6]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[34];
av2[3]=lf[35];
av2[4]=((C_word*)t0)[4];
av2[5]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}
else{
/* tcp.scm:391: loop */
t3=((C_word*)((C_word*)t0)[2])[1];{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
f_1682(2,av2);}}}

/* k1708 in k1705 in k1702 in k1699 in loop in k1675 in k1672 in read-input in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 in ... */
static void C_ccall f_1710(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1710,c,av);}
/* tcp.scm:391: loop */
t2=((C_word*)((C_word*)t0)[2])[1];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
f_1682(2,av2);}}

/* k1736 in loop in k1675 in k1672 in read-input in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1738(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_1738,c,av);}
/* tcp.scm:395: ##sys#signal-hook */
t2=*((C_word*)lf[6]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[7];
av2[3]=C_SCHEME_FALSE;
av2[4]=t1;
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* k1740 in loop in k1675 in k1672 in read-input in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1742(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_1742,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1746,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=stub105(C_SCHEME_UNDEFINED);
t4=C_a_i_bytevector(&a,1,C_fix(3));
t5=C_i_foreign_fixnum_argumentp(t3);
/* tcp.scm:171: ##sys#peek-c-string */
t6=*((C_word*)lf[10]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t2;
av2[2]=stub109(t4,t5);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* k1744 in k1740 in loop in k1675 in k1672 in read-input in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1746(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_1746,c,av);}
/* tcp.scm:395: scheme#string-append */
t2=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k1762 in k1672 in read-input in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1764(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(29,c,1)))){
C_save_and_reclaim((void *)f_1764,c,av);}
a=C_alloc(29);
t2=((C_word*)t0)[2];
f_1677(t2,C_s_a_i_plus(&a,2,t1,((C_word*)t0)[3]));}

/* k1765 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1767(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(29,c,4)))){
C_save_and_reclaim((void *)f_1767,c,av);}
a=C_alloc(29);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1768,a[2]=((C_word*)t0)[2],a[3]=((C_word)li10),tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1908,a[2]=t1,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t4=(C_truep(((C_word*)((C_word*)t0)[5])[1])?(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2021,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],a[4]=t2,a[5]=((C_word)li11),tmp=(C_word)a,a+=6,tmp):(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2041,a[2]=t2,a[3]=((C_word)li12),tmp=(C_word)a,a+=4,tmp));
t5=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_1935,a[2]=((C_word*)t0)[7],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[8],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=t2,a[8]=((C_word)li13),tmp=(C_word)a,a+=9,tmp);
if(C_truep(((C_word*)((C_word*)t0)[5])[1])){
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2005,a[2]=((C_word*)t0)[5],a[3]=t2,a[4]=((C_word)li14),tmp=(C_word)a,a+=5,tmp);
/* tcp.scm:514: chicken.port#make-output-port */
t7=*((C_word*)lf[54]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t7;
av2[1]=t3;
av2[2]=t4;
av2[3]=t5;
av2[4]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(5,av2);}}
else{
/* tcp.scm:514: chicken.port#make-output-port */
t6=*((C_word*)lf[54]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t6;
av2[1]=t3;
av2[2]=t4;
av2[3]=t5;
av2[4]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}}

/* output in k1765 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_fcall f_1768(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_1768,3,t0,t1,t2);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1772,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* tcp.scm:484: tcp-write-timeout */
t4=*((C_word*)lf[29]+1);{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k1770 in output in k1765 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1772(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_1772,c,av);}
a=C_alloc(11);
t2=C_block_size(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_1783,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=t2,tmp=(C_word)a,a+=7,tmp);
if(C_truep(t1)){
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1905,a[2]=t3,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* tcp.scm:487: chicken.time#current-milliseconds */
t5=*((C_word*)lf[42]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t4=t3;
f_1783(t4,C_SCHEME_FALSE);}}

/* k1781 in k1770 in output in k1765 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_fcall f_1783(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,5)))){
C_save_and_reclaim_args((void *)trf_1783,2,t0,t1);}
a=C_alloc(9);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_1785,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t3,a[5]=((C_word*)t0)[4],a[6]=((C_word)li9),tmp=(C_word)a,a+=7,tmp));
t5=((C_word*)t3)[1];
f_1785(t5,((C_word*)t0)[5],((C_word*)t0)[6],C_fix(0),t1);}

/* loop in k1781 in k1770 in output in k1765 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_fcall f_1785(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(9,0,4)))){
C_save_and_reclaim_args((void *)trf_1785,5,t0,t1,t2,t3,t4);}
a=C_alloc(9);
t5=C_i_fixnum_min(C_fix(8192),t2);
t6=C_i_foreign_fixnum_argumentp(((C_word*)t0)[2]);
t7=(C_truep(((C_word*)t0)[3])?C_i_foreign_block_argumentp(((C_word*)t0)[3]):C_SCHEME_FALSE);
t8=C_i_foreign_fixnum_argumentp(t3);
t9=C_i_foreign_fixnum_argumentp(t5);
t10=C_fix(0);
t11=stub214(C_SCHEME_UNDEFINED,t6,t7,t8,t9,t10);
t12=C_eqp(C_fix((C_word)SOCKET_ERROR),t11);
if(C_truep(t12)){
if(C_truep(stub114(C_SCHEME_UNDEFINED))){
t13=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_1807,a[2]=((C_word*)t0)[4],a[3]=t1,a[4]=t2,a[5]=t3,a[6]=t4,a[7]=((C_word*)t0)[5],a[8]=((C_word*)t0)[2],tmp=(C_word)a,a+=9,tmp);
if(C_truep(t4)){
/* tcp.scm:493: ##sys#thread-block-for-timeout! */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[39]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[39]+1);
av2[1]=t13;
av2[2]=*((C_word*)lf[33]+1);
av2[3]=t4;
tp(4,av2);}}
else{
t14=t13;{
C_word av2[2];
av2[0]=t14;
av2[1]=C_SCHEME_UNDEFINED;
f_1807(2,av2);}}}
else{
if(C_truep(stub120(C_SCHEME_UNDEFINED))){
t13=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_1839,a[2]=((C_word*)t0)[4],a[3]=t2,a[4]=t3,a[5]=t4,a[6]=((C_word)li8),tmp=(C_word)a,a+=7,tmp);
/* tcp.scm:503: ##sys#dispatch-interrupt */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[40]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[40]+1);
av2[1]=t1;
av2[2]=t13;
tp(3,av2);}}
else{
t13=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1850,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
t14=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1854,a[2]=t13,tmp=(C_word)a,a+=3,tmp);
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[11]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[11]+1);
av2[1]=t14;
av2[2]=lf[45];
av2[3]=lf[13];
tp(4,av2);}}}}
else{
if(C_truep(C_fixnum_lessp(t11,t2))){
t13=C_fixnum_difference(t2,t11);
t14=C_fixnum_plus(t3,t11);
t15=C_eqp(t11,C_fix(0));
if(C_truep(t15)){
/* tcp.scm:508: loop */
t18=t1;
t19=t13;
t20=t14;
t21=((C_word*)t0)[5];
t1=t18;
t2=t19;
t3=t20;
t4=t21;
goto loop;}
else{
if(C_truep(((C_word*)t0)[5])){
t16=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_1898,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[4],a[4]=t1,a[5]=t13,a[6]=t14,tmp=(C_word)a,a+=7,tmp);
/* tcp.scm:512: chicken.time#current-milliseconds */
t17=*((C_word*)lf[42]+1);{
C_word av2[2];
av2[0]=t17;
av2[1]=t16;
((C_proc)(void*)(*((C_word*)t17+1)))(2,av2);}}
else{
/* tcp.scm:508: loop */
t18=t1;
t19=t13;
t20=t14;
t21=C_SCHEME_FALSE;
t1=t18;
t2=t19;
t3=t20;
t4=t21;
goto loop;}}}
else{
t13=t1;{
C_word av2[2];
av2[0]=t13;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t13+1)))(2,av2);}}}}

/* k1805 in loop in k1781 in k1770 in output in k1765 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1807(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_1807,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_1810,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
/* tcp.scm:495: ##sys#thread-block-for-i/o! */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[37]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[37]+1);
av2[1]=t2;
av2[2]=*((C_word*)lf[33]+1);
av2[3]=((C_word*)t0)[8];
av2[4]=lf[44];
tp(5,av2);}}

/* k1808 in k1805 in loop in k1781 in k1770 in output in k1765 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1810(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_1810,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_1813,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
/* tcp.scm:496: ##sys#thread-yield! */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[36]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[36]+1);
av2[1]=t2;
tp(2,av2);}}

/* k1811 in k1808 in k1805 in loop in k1781 in k1770 in output in k1765 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 in ... */
static void C_ccall f_1813(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,5)))){
C_save_and_reclaim((void *)f_1813,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_1816,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
if(C_truep(C_slot(*((C_word*)lf[33]+1),C_fix(13)))){
/* tcp.scm:498: ##sys#signal-hook */
t3=*((C_word*)lf[6]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[34];
av2[3]=lf[43];
av2[4]=((C_word*)t0)[7];
av2[5]=((C_word*)t0)[8];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}
else{
/* tcp.scm:501: loop */
t3=((C_word*)((C_word*)t0)[2])[1];
f_1785(t3,((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6]);}}

/* k1814 in k1811 in k1808 in k1805 in loop in k1781 in k1770 in output in k1765 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in ... */
static void C_ccall f_1816(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_1816,c,av);}
/* tcp.scm:501: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_1785(t2,((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6]);}

/* a1838 in loop in k1781 in k1770 in output in k1765 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1839(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_1839,c,av);}
/* tcp.scm:504: g619 */
t2=((C_word*)((C_word*)t0)[2])[1];
f_1785(t2,t1,((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5]);}

/* k1848 in loop in k1781 in k1770 in output in k1765 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1850(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_1850,c,av);}
/* tcp.scm:506: ##sys#signal-hook */
t2=*((C_word*)lf[6]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[7];
av2[3]=C_SCHEME_FALSE;
av2[4]=t1;
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* k1852 in loop in k1781 in k1770 in output in k1765 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1854(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_1854,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1858,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=stub105(C_SCHEME_UNDEFINED);
t4=C_a_i_bytevector(&a,1,C_fix(3));
t5=C_i_foreign_fixnum_argumentp(t3);
/* tcp.scm:171: ##sys#peek-c-string */
t6=*((C_word*)lf[10]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t2;
av2[2]=stub109(t4,t5);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* k1856 in k1852 in loop in k1781 in k1770 in output in k1765 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1858(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_1858,c,av);}
/* tcp.scm:506: scheme#string-append */
t2=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k1896 in loop in k1781 in k1770 in output in k1765 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1898(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(29,c,4)))){
C_save_and_reclaim((void *)f_1898,c,av);}
a=C_alloc(29);
t2=C_s_a_i_plus(&a,2,t1,((C_word*)t0)[2]);
/* tcp.scm:508: loop */
t3=((C_word*)((C_word*)t0)[3])[1];
f_1785(t3,((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6],t2);}

/* k1903 in k1770 in output in k1765 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1905(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(29,c,1)))){
C_save_and_reclaim((void *)f_1905,c,av);}
a=C_alloc(29);
t2=((C_word*)t0)[2];
f_1783(t2,C_s_a_i_plus(&a,2,t1,((C_word*)t0)[3]));}

/* k1906 in k1765 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1908(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_1908,c,av);}
a=C_alloc(6);
t2=C_i_setslot(((C_word*)t0)[2],C_fix(3),lf[46]);
t3=C_i_setslot(t1,C_fix(3),lf[47]);
t4=C_i_setslot(((C_word*)t0)[2],C_fix(7),lf[48]);
t5=C_i_setslot(t1,C_fix(7),lf[48]);
t6=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1923,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],a[4]=t1,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* tcp.scm:542: ##sys#set-port-data! */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[49]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[49]+1);
av2[1]=t6;
av2[2]=((C_word*)t0)[2];
av2[3]=((C_word*)t0)[4];
tp(4,av2);}}

/* k1921 in k1906 in k1765 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1923(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_1923,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1926,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* tcp.scm:543: ##sys#set-port-data! */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[49]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[49]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=((C_word*)t0)[5];
tp(4,av2);}}

/* k1924 in k1921 in k1906 in k1765 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1926(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_1926,c,av);}
/* tcp.scm:544: scheme#values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
C_values(4,av2);}}

/* a1934 in k1765 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1935(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_1935,c,av);}
a=C_alloc(10);
if(C_truep(((C_word*)((C_word*)t0)[2])[1])){
t2=C_SCHEME_UNDEFINED;
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=C_set_block_item(((C_word*)t0)[2],0,C_SCHEME_TRUE);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1943,a[2]=t1,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t4=(C_truep(((C_word*)((C_word*)t0)[6])[1])?C_fixnum_greaterp(C_block_size(((C_word*)((C_word*)t0)[6])[1]),C_fix(0)):C_SCHEME_FALSE);
if(C_truep(t4)){
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1992,a[2]=((C_word*)t0)[6],a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* tcp.scm:528: output */
t6=((C_word*)t0)[7];
f_1768(t6,t5,((C_word*)((C_word*)t0)[6])[1]);}
else{
t5=t3;
f_1943(t5,C_SCHEME_UNDEFINED);}}}

/* k1941 in a1934 in k1765 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_fcall f_1943(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_1943,2,t0,t1);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1946,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_slot(((C_word*)t0)[5],C_fix(2)))){
t3=t2;
f_1946(t3,C_SCHEME_UNDEFINED);}
else{
t3=C_fix((C_word)SHUT_WR);
t4=t2;
f_1946(t4,stub183(C_SCHEME_UNDEFINED,C_i_foreign_fixnum_argumentp(((C_word*)t0)[3]),C_i_foreign_fixnum_argumentp(t3)));}}

/* k1944 in k1941 in a1934 in k1765 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_fcall f_1946(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_1946,2,t0,t1);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1952,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
if(C_truep(((C_word*)((C_word*)t0)[4])[1])){
t3=C_i_foreign_fixnum_argumentp(((C_word*)t0)[3]);
t4=t2;
f_1952(t4,C_eqp(C_fix((C_word)SOCKET_ERROR),stub164(C_SCHEME_UNDEFINED,t3)));}
else{
t3=t2;
f_1952(t3,C_SCHEME_FALSE);}}

/* k1950 in k1944 in k1941 in a1934 in k1765 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_fcall f_1952(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,3)))){
C_save_and_reclaim_args((void *)trf_1952,2,t0,t1);}
a=C_alloc(7);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1959,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1963,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[11]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[11]+1);
av2[1]=t3;
av2[2]=lf[51];
av2[3]=lf[13];
tp(4,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k1957 in k1950 in k1944 in k1941 in a1934 in k1765 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1959(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_1959,c,av);}
/* tcp.scm:532: ##sys#signal-hook */
t2=*((C_word*)lf[6]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[7];
av2[3]=C_SCHEME_FALSE;
av2[4]=t1;
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* k1961 in k1950 in k1944 in k1941 in a1934 in k1765 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1963(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_1963,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1967,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=stub105(C_SCHEME_UNDEFINED);
t4=C_a_i_bytevector(&a,1,C_fix(3));
t5=C_i_foreign_fixnum_argumentp(t3);
/* tcp.scm:171: ##sys#peek-c-string */
t6=*((C_word*)lf[10]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t2;
av2[2]=stub109(t4,t5);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* k1965 in k1961 in k1950 in k1944 in k1941 in a1934 in k1765 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1967(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_1967,c,av);}
/* tcp.scm:532: scheme#string-append */
t2=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k1990 in a1934 in k1765 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_1992(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1992,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,lf[52]);
t3=((C_word*)t0)[3];
f_1943(t3,t2);}

/* f_2005 in k1765 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2005(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_2005,c,av);}
a=C_alloc(4);
t2=C_block_size(((C_word*)((C_word*)t0)[2])[1]);
if(C_truep(C_fixnum_greaterp(t2,C_fix(0)))){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2015,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* tcp.scm:536: output */
t4=((C_word*)t0)[3];
f_1768(t4,t3,((C_word*)((C_word*)t0)[2])[1]);}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k2013 */
static void C_ccall f_2015(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2015,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,lf[53]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* f_2021 in k1765 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2021(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_2021,c,av);}
a=C_alloc(6);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2026,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* tcp.scm:517: ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[11]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[11]+1);
av2[1]=t3;
av2[2]=((C_word*)((C_word*)t0)[2])[1];
av2[3]=t2;
tp(4,av2);}}

/* k2024 */
static void C_ccall f_2026(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_2026,c,av);}
a=C_alloc(4);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=C_block_size(((C_word*)((C_word*)t0)[2])[1]);
if(C_truep(C_fixnum_greater_or_equal_p(t3,((C_word*)t0)[3]))){
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2035,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
/* tcp.scm:519: output */
t5=((C_word*)t0)[5];
f_1768(t5,t4,((C_word*)((C_word*)t0)[2])[1]);}
else{
t4=C_SCHEME_UNDEFINED;
t5=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* k2033 in k2024 */
static void C_ccall f_2035(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2035,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,lf[50]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* f_2041 in k1765 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2041(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_2041,c,av);}
t3=C_block_size(t2);
if(C_truep(C_fixnum_greaterp(t3,C_fix(0)))){
/* tcp.scm:523: output */
t4=((C_word*)t0)[2];
f_1768(t4,t1,t2);}
else{
t4=C_SCHEME_UNDEFINED;
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* a2056 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2057(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_2057,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2061,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_fixnum_greater_or_equal_p(((C_word*)((C_word*)t0)[2])[1],((C_word*)((C_word*)t0)[3])[1]))){
/* tcp.scm:404: read-input */
t3=((C_word*)t0)[5];
f_1670(t3,t2);}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_2061(2,av2);}}}

/* k2059 in a2056 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2061(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2061,c,av);}
if(C_truep(C_fixnum_greater_or_equal_p(((C_word*)((C_word*)t0)[2])[1],((C_word*)((C_word*)t0)[3])[1]))){
t2=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_END_OF_FILE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_subchar(((C_word*)t0)[5],((C_word*)((C_word*)t0)[2])[1]);
t3=C_fixnum_plus(((C_word*)((C_word*)t0)[2])[1],C_fix(1));
t4=C_set_block_item(((C_word*)t0)[2],0,t3);
t5=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* a2078 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2079(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(11,c,3)))){
C_save_and_reclaim((void *)f_2079,c,av);}
a=C_alloc(11);
t2=C_fixnum_lessp(((C_word*)((C_word*)t0)[2])[1],((C_word*)((C_word*)t0)[3])[1]);
if(C_truep(t2)){
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_i_foreign_fixnum_argumentp(((C_word*)t0)[4]);
t4=stub200(C_SCHEME_UNDEFINED,t3);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2092,a[2]=t1,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
t6=C_eqp(C_fix((C_word)SOCKET_ERROR),t4);
if(C_truep(t6)){
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2105,a[2]=t5,a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2109,a[2]=t7,tmp=(C_word)a,a+=3,tmp);
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[11]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[11]+1);
av2[1]=t8;
av2[2]=lf[55];
av2[3]=lf[13];
tp(4,av2);}}
else{
t7=t1;{
C_word *av2=av;
av2[0]=t7;
av2[1]=C_eqp(t4,C_fix(1));
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}}}

/* k2090 in a2078 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2092(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2092,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_eqp(((C_word*)t0)[3],C_fix(1));
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k2103 in a2078 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2105(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_2105,c,av);}
/* tcp.scm:416: ##sys#signal-hook */
t2=*((C_word*)lf[6]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[7];
av2[3]=C_SCHEME_FALSE;
av2[4]=t1;
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* k2107 in a2078 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2109(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_2109,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2113,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=stub105(C_SCHEME_UNDEFINED);
t4=C_a_i_bytevector(&a,1,C_fix(3));
t5=C_i_foreign_fixnum_argumentp(t3);
/* tcp.scm:171: ##sys#peek-c-string */
t6=*((C_word*)lf[10]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t2;
av2[2]=stub109(t4,t5);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* k2111 in k2107 in a2078 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2113(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2113,c,av);}
/* tcp.scm:416: scheme#string-append */
t2=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* a2120 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2121(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_2121,c,av);}
a=C_alloc(5);
if(C_truep(((C_word*)((C_word*)t0)[2])[1])){
t2=C_SCHEME_UNDEFINED;
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=C_set_block_item(((C_word*)t0)[2],0,C_SCHEME_TRUE);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2129,a[2]=t1,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_slot(((C_word*)t0)[5],C_fix(1)))){
t4=t3;
f_2129(t4,C_SCHEME_UNDEFINED);}
else{
t4=C_fix((C_word)SHUT_RD);
t5=t3;
f_2129(t5,stub183(C_SCHEME_UNDEFINED,C_i_foreign_fixnum_argumentp(((C_word*)t0)[3]),C_i_foreign_fixnum_argumentp(t4)));}}}

/* k2127 in a2120 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_fcall f_2129(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_2129,2,t0,t1);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2135,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
if(C_truep(((C_word*)((C_word*)t0)[4])[1])){
t3=C_i_foreign_fixnum_argumentp(((C_word*)t0)[3]);
t4=t2;
f_2135(t4,C_eqp(C_fix((C_word)SOCKET_ERROR),stub164(C_SCHEME_UNDEFINED,t3)));}
else{
t3=t2;
f_2135(t3,C_SCHEME_FALSE);}}

/* k2133 in k2127 in a2120 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_fcall f_2135(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,3)))){
C_save_and_reclaim_args((void *)trf_2135,2,t0,t1);}
a=C_alloc(7);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2142,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2146,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[11]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[11]+1);
av2[1]=t3;
av2[2]=lf[56];
av2[3]=lf[13];
tp(4,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k2140 in k2133 in k2127 in a2120 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2142(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_2142,c,av);}
/* tcp.scm:423: ##sys#signal-hook */
t2=*((C_word*)lf[6]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[7];
av2[3]=C_SCHEME_FALSE;
av2[4]=t1;
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* k2144 in k2133 in k2127 in a2120 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2146(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_2146,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2150,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=stub105(C_SCHEME_UNDEFINED);
t4=C_a_i_bytevector(&a,1,C_fix(3));
t5=C_i_foreign_fixnum_argumentp(t3);
/* tcp.scm:171: ##sys#peek-c-string */
t6=*((C_word*)lf[10]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t2;
av2[2]=stub109(t4,t5);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* k2148 in k2144 in k2133 in k2127 in a2120 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2150(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2150,c,av);}
/* tcp.scm:423: scheme#string-append */
t2=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* a2170 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2171(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_2171,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2175,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_fixnum_greater_or_equal_p(((C_word*)((C_word*)t0)[2])[1],((C_word*)((C_word*)t0)[3])[1]))){
/* tcp.scm:426: read-input */
t3=((C_word*)t0)[5];
f_1670(t3,t2);}
else{
t3=C_fixnum_lessp(((C_word*)((C_word*)t0)[2])[1],((C_word*)((C_word*)t0)[3])[1]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=(C_truep(t3)?C_subchar(((C_word*)t0)[4],((C_word*)((C_word*)t0)[2])[1]):C_SCHEME_END_OF_FILE);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k2173 in a2170 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2175(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2175,c,av);}
t2=C_fixnum_lessp(((C_word*)((C_word*)t0)[2])[1],((C_word*)((C_word*)t0)[3])[1]);
t3=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t3;
av2[1]=(C_truep(t2)?C_subchar(((C_word*)t0)[5],((C_word*)((C_word*)t0)[2])[1]):C_SCHEME_END_OF_FILE);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* a2188 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2189(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(c!=6) C_bad_argc_2(c,6,t0);
if(C_unlikely(!C_demand(C_calculate_demand(11,c,5)))){
C_save_and_reclaim((void *)f_2189,c,av);}
a=C_alloc(11);
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_2195,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t4,a[6]=t7,a[7]=((C_word*)t0)[5],a[8]=((C_word)li19),tmp=(C_word)a,a+=9,tmp));
t9=((C_word*)t7)[1];
f_2195(t9,t1,t3,C_fix(0),t5);}

/* loop in a2188 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_fcall f_2195(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(8,0,4)))){
C_save_and_reclaim_args((void *)trf_2195,5,t0,t1,t2,t3,t4);}
a=C_alloc(8);
t5=C_eqp(t2,C_fix(0));
if(C_truep(t5)){
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
if(C_truep(C_fixnum_lessp(((C_word*)((C_word*)t0)[2])[1],((C_word*)((C_word*)t0)[3])[1]))){
t6=C_fixnum_difference(((C_word*)((C_word*)t0)[3])[1],((C_word*)((C_word*)t0)[2])[1]);
t7=C_fixnum_lessp(t2,t6);
t8=(C_truep(t7)?t2:t6);
t9=C_fixnum_plus(((C_word*)((C_word*)t0)[2])[1],t8);
t10=C_substring_copy(((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)((C_word*)t0)[2])[1],t9,t4);
t11=C_fixnum_plus(((C_word*)((C_word*)t0)[2])[1],t8);
t12=C_set_block_item(((C_word*)t0)[2],0,t11);
/* tcp.scm:438: loop */
t14=t1;
t15=C_fixnum_difference(t2,t8);
t16=C_fixnum_plus(t3,t8);
t17=C_fixnum_plus(t4,t8);
t1=t14;
t2=t15;
t3=t16;
t4=t17;
goto loop;}
else{
t6=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_2243,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t3,a[5]=((C_word*)t0)[6],a[6]=t2,a[7]=t4,tmp=(C_word)a,a+=8,tmp);
/* tcp.scm:440: read-input */
t7=((C_word*)t0)[7];
f_1670(t7,t6);}}}

/* k2241 in loop in a2188 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2243(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_2243,c,av);}
t2=C_eqp(((C_word*)((C_word*)t0)[2])[1],C_fix(0));
if(C_truep(t2)){
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
/* tcp.scm:443: loop */
t3=((C_word*)((C_word*)t0)[5])[1];
f_2195(t3,((C_word*)t0)[3],((C_word*)t0)[6],((C_word*)t0)[4],((C_word*)t0)[7]);}}

/* a2253 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2254(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_2254,c,av);}
a=C_alloc(9);
t4=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_2258,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t3,a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[5],a[8]=t2,tmp=(C_word)a,a+=9,tmp);
if(C_truep(C_fixnum_greater_or_equal_p(((C_word*)((C_word*)t0)[2])[1],((C_word*)((C_word*)t0)[3])[1]))){
/* tcp.scm:446: read-input */
t5=((C_word*)t0)[5];
f_1670(t5,t4);}
else{
t5=t4;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_UNDEFINED;
f_2258(2,av2);}}}

/* k2256 in a2253 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2258(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,5)))){
C_save_and_reclaim((void *)f_2258,c,av);}
a=C_alloc(15);
if(C_truep(C_fixnum_greater_or_equal_p(((C_word*)((C_word*)t0)[2])[1],((C_word*)((C_word*)t0)[3])[1]))){
t2=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_END_OF_FILE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(C_truep(((C_word*)t0)[5])?((C_word*)t0)[5]:C_fixnum_difference(*((C_word*)lf[57]+1),((C_word*)((C_word*)t0)[2])[1]));
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_2272,a[2]=((C_word*)t0)[2],a[3]=t4,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word)li22),tmp=(C_word)a,a+=8,tmp);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2328,a[2]=((C_word*)t0)[8],a[3]=((C_word*)t0)[2],a[4]=((C_word)li23),tmp=(C_word)a,a+=5,tmp);
/* tcp.scm:450: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[4];
av2[2]=t5;
av2[3]=t6;
C_call_with_values(4,av2);}}}

/* a2271 in k2256 in a2253 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2272(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(8,c,5)))){
C_save_and_reclaim((void *)f_2272,c,av);}
a=C_alloc(8);
t2=C_fixnum_plus(((C_word*)((C_word*)t0)[2])[1],((C_word*)((C_word*)t0)[3])[1]);
t3=C_i_fixnum_min(((C_word*)((C_word*)t0)[4])[1],t2);
t4=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_2282,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word)li21),tmp=(C_word)a,a+=8,tmp);
/* tcp.scm:451: ##sys#scan-buffer-line */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[58]+1));
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=*((C_word*)lf[58]+1);
av2[1]=t1;
av2[2]=((C_word*)t0)[5];
av2[3]=t3;
av2[4]=((C_word*)((C_word*)t0)[2])[1];
av2[5]=t4;
tp(6,av2);}}

/* a2281 in a2271 in k2256 in a2253 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2282(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_2282,c,av);}
a=C_alloc(8);
t3=C_fixnum_difference(t2,((C_word*)((C_word*)t0)[2])[1]);
if(C_truep(C_fixnum_greater_or_equal_p(t3,((C_word*)((C_word*)t0)[3])[1]))){
/* tcp.scm:458: scheme#values */{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=t1;
av2[2]=C_SCHEME_FALSE;
av2[3]=t2;
av2[4]=C_SCHEME_FALSE;
C_values(5,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_2298,a[2]=((C_word*)t0)[3],a[3]=t3,a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[4],a[6]=t1,a[7]=((C_word*)t0)[5],tmp=(C_word)a,a+=8,tmp);
/* tcp.scm:459: read-input */
t5=((C_word*)t0)[6];
f_1670(t5,t4);}}

/* k2296 in a2281 in a2271 in k2256 in a2253 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2298(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_2298,c,av);}
t2=C_fixnum_difference(((C_word*)((C_word*)t0)[2])[1],((C_word*)t0)[3]);
t3=C_set_block_item(((C_word*)t0)[2],0,t2);
if(C_truep(C_fixnum_lessp(((C_word*)((C_word*)t0)[4])[1],((C_word*)((C_word*)t0)[5])[1]))){
t4=C_fixnum_plus(((C_word*)((C_word*)t0)[4])[1],((C_word*)((C_word*)t0)[2])[1]);
/* tcp.scm:462: scheme#values */{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=((C_word*)t0)[6];
av2[2]=((C_word*)t0)[7];
av2[3]=((C_word*)((C_word*)t0)[4])[1];
av2[4]=C_i_fixnum_min(((C_word*)((C_word*)t0)[5])[1],t4);
C_values(5,av2);}}
else{
/* tcp.scm:465: scheme#values */{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=((C_word*)t0)[6];
av2[2]=C_SCHEME_FALSE;
av2[3]=((C_word*)((C_word*)t0)[4])[1];
av2[4]=C_SCHEME_FALSE;
C_values(5,av2);}}}

/* a2327 in k2256 in a2253 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2328(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2328,c,av);}
if(C_truep(t4)){
t5=C_slot(((C_word*)t0)[2],C_fix(4));
t6=C_fixnum_plus(t5,C_fix(1));
t7=C_i_set_i_slot(((C_word*)t0)[2],C_fix(4),t6);
t8=C_i_set_i_slot(((C_word*)t0)[2],C_fix(5),C_fix(0));
t9=C_mutate(((C_word *)((C_word*)t0)[3])+1,t2);
t10=t1;{
C_word *av2=av;
av2[0]=t10;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t10+1)))(2,av2);}}
else{
t5=C_slot(((C_word*)t0)[2],C_fix(5));
t6=C_block_size(t3);
t7=C_fixnum_plus(t5,t6);
t8=C_i_set_i_slot(((C_word*)t0)[2],C_fix(5),t7);
t9=C_mutate(((C_word *)((C_word*)t0)[3])+1,t2);
t10=t1;{
C_word *av2=av;
av2[0]=t10;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t10+1)))(2,av2);}}}

/* a2372 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2373(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_2373,c,av);}
a=C_alloc(5);
if(C_truep(C_fixnum_greater_or_equal_p(((C_word*)((C_word*)t0)[2])[1],((C_word*)((C_word*)t0)[3])[1]))){
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=lf[59];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2383,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* tcp.scm:478: ##sys#substring */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[60]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[60]+1);
av2[1]=t3;
av2[2]=((C_word*)t0)[4];
av2[3]=((C_word*)((C_word*)t0)[2])[1];
av2[4]=((C_word*)((C_word*)t0)[3])[1];
tp(5,av2);}}}

/* k2381 in a2372 in k1667 in k1664 in k1658 in k1655 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2383(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2383,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,((C_word*)((C_word*)t0)[3])[1]);
t3=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k2402 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2404(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_2404,c,av);}
/* tcp.scm:365: ##sys#signal-hook */
t2=*((C_word*)lf[6]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[7];
av2[3]=((C_word*)t0)[3];
av2[4]=t1;
av2[5]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* k2406 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2408(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_2408,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2412,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=C_a_i_bytevector(&a,1,C_fix(3));
t4=C_i_foreign_fixnum_argumentp(((C_word*)t0)[3]);
/* tcp.scm:171: ##sys#peek-c-string */
t5=*((C_word*)lf[10]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=stub109(t3,t4);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k2410 in k2406 in chicken.tcp#io-ports in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2412(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2412,c,av);}
/* tcp.scm:365: scheme#string-append */
t2=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.tcp#tcp-accept in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2416(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_2416,c,av);}
a=C_alloc(5);
t3=C_i_check_structure(t2,lf[5]);
t4=C_slot(t2,C_fix(1));
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2426,a[2]=t4,a[3]=t2,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* tcp.scm:549: tcp-accept-timeout */
t6=*((C_word*)lf[31]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}

/* k2424 in chicken.tcp#tcp-accept in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2426(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_2426,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2429,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(t1)){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2509,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* tcp.scm:550: chicken.time#current-milliseconds */
t4=*((C_word*)lf[42]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=t2;
f_2429(t3,C_SCHEME_FALSE);}}

/* k2427 in k2424 in chicken.tcp#tcp-accept in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_fcall f_2429(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,2)))){
C_save_and_reclaim_args((void *)trf_2429,2,t0,t1);}
a=C_alloc(10);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_2434,a[2]=((C_word*)t0)[2],a[3]=t3,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=t1,a[7]=((C_word)li27),tmp=(C_word)a,a+=8,tmp));
t5=((C_word*)t3)[1];{
C_word av2[2];
av2[0]=t5;
av2[1]=((C_word*)t0)[5];
f_2434(2,av2);}}

/* loop in k2427 in k2424 in chicken.tcp#tcp-accept in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2434(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_2434,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_2438,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
if(C_truep(((C_word*)t0)[6])){
/* tcp.scm:553: ##sys#thread-block-for-timeout! */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[39]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[39]+1);
av2[1]=t2;
av2[2]=*((C_word*)lf[33]+1);
av2[3]=((C_word*)t0)[6];
tp(4,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_2438(2,av2);}}}

/* k2436 in loop in k2427 in k2424 in chicken.tcp#tcp-accept in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2438(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,4)))){
C_save_and_reclaim((void *)f_2438,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_2441,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* tcp.scm:554: ##sys#thread-block-for-i/o! */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[37]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[37]+1);
av2[1]=t2;
av2[2]=*((C_word*)lf[33]+1);
av2[3]=((C_word*)t0)[2];
av2[4]=lf[38];
tp(5,av2);}}

/* k2439 in k2436 in loop in k2427 in k2424 in chicken.tcp#tcp-accept in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2441(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_2441,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_2444,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* tcp.scm:555: ##sys#thread-yield! */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[36]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[36]+1);
av2[1]=t2;
tp(2,av2);}}

/* k2442 in k2439 in k2436 in loop in k2427 in k2424 in chicken.tcp#tcp-accept in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2444(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,6)))){
C_save_and_reclaim((void *)f_2444,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2447,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_slot(*((C_word*)lf[33]+1),C_fix(13)))){
/* tcp.scm:557: ##sys#signal-hook */
t3=*((C_word*)lf[6]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[34];
av2[3]=lf[65];
av2[4]=lf[67];
av2[5]=((C_word*)t0)[6];
av2[6]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(7,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_2447(2,av2);}}}

/* k2445 in k2442 in k2439 in k2436 in loop in k2427 in k2424 in chicken.tcp#tcp-accept in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2447(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_2447,c,av);}
a=C_alloc(7);
t2=C_i_foreign_fixnum_argumentp(((C_word*)t0)[2]);
t3=stub153(C_SCHEME_UNDEFINED,t2,C_SCHEME_FALSE,C_SCHEME_FALSE);
t4=C_eqp(C_fix((C_word)INVALID_SOCKET),t3);
if(C_truep(C_i_not(t4))){
/* tcp.scm:563: io-ports */
t5=lf[32];
f_1653(t5,((C_word*)t0)[3],lf[65],t3);}
else{
if(C_truep(stub120(C_SCHEME_UNDEFINED))){
/* tcp.scm:565: ##sys#dispatch-interrupt */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[40]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[40]+1);
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)((C_word*)t0)[4])[1];
tp(3,av2);}}
else{
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2475,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[5],tmp=(C_word)a,a+=4,tmp);
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2479,a[2]=t5,tmp=(C_word)a,a+=3,tmp);
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[11]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[11]+1);
av2[1]=t6;
av2[2]=lf[66];
av2[3]=lf[13];
tp(4,av2);}}}}

/* k2473 in k2445 in k2442 in k2439 in k2436 in loop in k2427 in k2424 in chicken.tcp#tcp-accept in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2475(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_2475,c,av);}
/* tcp.scm:567: ##sys#signal-hook */
t2=*((C_word*)lf[6]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[7];
av2[3]=lf[65];
av2[4]=t1;
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* k2477 in k2445 in k2442 in k2439 in k2436 in loop in k2427 in k2424 in chicken.tcp#tcp-accept in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2479(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_2479,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2483,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=stub105(C_SCHEME_UNDEFINED);
t4=C_a_i_bytevector(&a,1,C_fix(3));
t5=C_i_foreign_fixnum_argumentp(t3);
/* tcp.scm:171: ##sys#peek-c-string */
t6=*((C_word*)lf[10]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t2;
av2[2]=stub109(t4,t5);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* k2481 in k2477 in k2445 in k2442 in k2439 in k2436 in loop in k2427 in k2424 in chicken.tcp#tcp-accept in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2483(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2483,c,av);}
/* tcp.scm:567: scheme#string-append */
t2=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k2507 in k2424 in chicken.tcp#tcp-accept in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2509(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(29,c,1)))){
C_save_and_reclaim((void *)f_2509,c,av);}
a=C_alloc(29);
t2=((C_word*)t0)[2];
f_2429(t2,C_s_a_i_plus(&a,2,((C_word*)t0)[3],t1));}

/* chicken.tcp#tcp-accept-ready? in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2511(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(11,c,3)))){
C_save_and_reclaim((void *)f_2511,c,av);}
a=C_alloc(11);
t3=C_i_check_structure_2(t2,lf[5],lf[69]);
t4=C_slot(t2,C_fix(1));
t5=C_i_foreign_fixnum_argumentp(t4);
t6=stub200(C_SCHEME_UNDEFINED,t5);
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2521,a[2]=t1,a[3]=t6,tmp=(C_word)a,a+=4,tmp);
t8=C_eqp(C_fix((C_word)SOCKET_ERROR),t6);
if(C_truep(t8)){
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2534,a[2]=t7,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t10=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2538,a[2]=t9,tmp=(C_word)a,a+=3,tmp);
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[11]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[11]+1);
av2[1]=t10;
av2[2]=lf[70];
av2[3]=lf[13];
tp(4,av2);}}
else{
t9=t1;{
C_word *av2=av;
av2[0]=t9;
av2[1]=C_eqp(C_fix(1),t6);
((C_proc)(void*)(*((C_word*)t9+1)))(2,av2);}}}

/* k2519 in chicken.tcp#tcp-accept-ready? in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2521(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2521,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_eqp(C_fix(1),((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k2532 in chicken.tcp#tcp-accept-ready? in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2534(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_2534,c,av);}
/* tcp.scm:574: ##sys#signal-hook */
t2=*((C_word*)lf[6]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[7];
av2[3]=lf[69];
av2[4]=t1;
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* k2536 in chicken.tcp#tcp-accept-ready? in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2538(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_2538,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2542,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=stub105(C_SCHEME_UNDEFINED);
t4=C_a_i_bytevector(&a,1,C_fix(3));
t5=C_i_foreign_fixnum_argumentp(t3);
/* tcp.scm:171: ##sys#peek-c-string */
t6=*((C_word*)lf[10]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t2;
av2[2]=stub109(t4,t5);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* k2540 in k2536 in chicken.tcp#tcp-accept-ready? in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2542(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2542,c,av);}
/* tcp.scm:574: scheme#string-append */
t2=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2561(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_2561,c,av);}
a=C_alloc(9);
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_rest_nullp(c,3);
t5=(C_truep(t4)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,3,t0));
t6=t5;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2568,a[2]=t3,a[3]=t1,a[4]=t7,tmp=(C_word)a,a+=5,tmp);
/* tcp.scm:587: tcp-connect-timeout */
t9=*((C_word*)lf[30]+1);{
C_word *av2=av;
av2[0]=t9;
av2[1]=t8;
((C_proc)(void*)(*((C_word*)t9+1)))(2,av2);}}

/* k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2568(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_2568,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2571,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
if(C_truep(t1)){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2836,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* tcp.scm:588: chicken.time#current-milliseconds */
t4=*((C_word*)lf[42]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=t2;
f_2571(t3,C_SCHEME_FALSE);}}

/* k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_fcall f_2571(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_2571,2,t0,t1);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2574,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
t3=C_fix((C_word)sizeof(struct sockaddr_in));
/* ##sys#make-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[19]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[19]+1);
av2[1]=t2;
av2[2]=t3;
av2[3]=C_make_character(32);
tp(4,av2);}}

/* k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2574(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(21,c,4)))){
C_save_and_reclaim((void *)f_2574,c,av);}
a=C_alloc(21);
t2=C_i_check_string(((C_word*)((C_word*)t0)[2])[1]);
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_2580,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[2],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
if(C_truep(((C_word*)((C_word*)t0)[5])[1])){
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_2580(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2811,a[2]=((C_word*)t0)[5],a[3]=t3,a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2819,a[2]=((C_word*)t0)[2],a[3]=((C_word)li32),tmp=(C_word)a,a+=4,tmp);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2825,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[5],a[4]=((C_word)li33),tmp=(C_word)a,a+=5,tmp);
/* tcp.scm:592: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=t4;
av2[2]=t5;
av2[3]=t6;
C_call_with_values(4,av2);}}}

/* k2578 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2580(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_2580,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_2583,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* tcp.scm:594: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[20]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[20]+1);
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[6])[1];
tp(3,av2);}}

/* k2581 in k2578 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2583(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,4)))){
C_save_and_reclaim((void *)f_2583,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_2586,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2805,a[2]=t2,a[3]=((C_word*)t0)[5],tmp=(C_word)a,a+=4,tmp);
/* tcp.scm:595: gethostaddr */
f_1248(t3,((C_word*)t0)[3],((C_word*)((C_word*)t0)[5])[1],((C_word*)((C_word*)t0)[6])[1]);}

/* k2584 in k2581 in k2578 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2586(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,3)))){
C_save_and_reclaim((void *)f_2586,c,av);}
a=C_alloc(16);
t2=C_fix((C_word)AF_INET);
t3=C_fix((C_word)SOCK_STREAM);
t4=stub126(C_SCHEME_UNDEFINED,C_i_foreign_fixnum_argumentp(t2),C_i_foreign_fixnum_argumentp(t3),C_fix(0));
t5=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_2592,a[2]=t4,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
t6=C_eqp(C_fix((C_word)INVALID_SOCKET),t4);
if(C_truep(t6)){
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2788,a[2]=t5,a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],tmp=(C_word)a,a+=5,tmp);
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2792,a[2]=t7,tmp=(C_word)a,a+=3,tmp);
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[11]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[11]+1);
av2[1]=t8;
av2[2]=lf[78];
av2[3]=lf[13];
tp(4,av2);}}
else{
t7=t5;{
C_word *av2=av;
av2[0]=t7;
av2[1]=C_SCHEME_UNDEFINED;
f_2592(2,av2);}}}

/* k2590 in k2584 in k2581 in k2578 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2592(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,3)))){
C_save_and_reclaim((void *)f_2592,c,av);}
a=C_alloc(16);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_2595,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
t3=C_i_foreign_fixnum_argumentp(((C_word*)t0)[2]);
t4=C_eqp(C_fix((C_word)SOCKET_ERROR),stub205(C_SCHEME_UNDEFINED,t3));
if(C_truep(t4)){
t5=stub105(C_SCHEME_UNDEFINED);
t6=C_i_foreign_fixnum_argumentp(((C_word*)t0)[2]);
t7=stub164(C_SCHEME_UNDEFINED,t6);
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2764,a[2]=t2,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2768,a[2]=t8,a[3]=t5,tmp=(C_word)a,a+=4,tmp);
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[11]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[11]+1);
av2[1]=t9;
av2[2]=lf[77];
av2[3]=lf[13];
tp(4,av2);}}
else{
t5=t2;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_UNDEFINED;
f_2595(2,av2);}}}

/* k2593 in k2590 in k2584 in k2581 in k2578 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2595(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,3)))){
C_save_and_reclaim((void *)f_2595,c,av);}
a=C_alloc(16);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_2598,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
if(C_truep(make_socket_nonblocking(((C_word*)t0)[2]))){
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_2598(2,av2);}}
else{
t3=stub105(C_SCHEME_UNDEFINED);
t4=C_i_foreign_fixnum_argumentp(((C_word*)t0)[2]);
t5=stub164(C_SCHEME_UNDEFINED,t4);
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2738,a[2]=t2,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2742,a[2]=t6,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[11]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[11]+1);
av2[1]=t7;
av2[2]=lf[76];
av2[3]=lf[13];
tp(4,av2);}}}

/* k2596 in k2593 in k2590 in k2584 in k2581 in k2578 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2598(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,2)))){
C_save_and_reclaim((void *)f_2598,c,av);}
a=C_alloc(15);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2601,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_2664,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=t4,a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word)li30),tmp=(C_word)a,a+=9,tmp));
t6=((C_word*)t4)[1];{
C_word *av2=av;
av2[0]=t6;
av2[1]=t2;
f_2664(2,av2);}}

/* k2599 in k2596 in k2593 in k2590 in k2584 in k2581 in k2578 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2601(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_2601,c,av);}
a=C_alloc(12);
t2=C_i_foreign_fixnum_argumentp(((C_word*)t0)[2]);
t3=stub685(C_SCHEME_UNDEFINED,t2);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2607,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
t5=C_eqp(C_fix((C_word)SOCKET_ERROR),t3);
if(C_truep(t5)){
t6=stub105(C_SCHEME_UNDEFINED);
t7=C_i_foreign_fixnum_argumentp(((C_word*)t0)[2]);
t8=stub164(C_SCHEME_UNDEFINED,t7);
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2626,a[2]=t4,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
t10=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2630,a[2]=t9,a[3]=t6,tmp=(C_word)a,a+=4,tmp);
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[11]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[11]+1);
av2[1]=t10;
av2[2]=lf[73];
av2[3]=lf[13];
tp(4,av2);}}
else{
if(C_truep(C_fixnum_greaterp(t3,C_fix(0)))){
t6=C_i_foreign_fixnum_argumentp(((C_word*)t0)[2]);
t7=stub164(C_SCHEME_UNDEFINED,t6);
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2652,a[2]=t4,tmp=(C_word)a,a+=3,tmp);
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2656,a[2]=t8,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[11]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[11]+1);
av2[1]=t9;
av2[2]=lf[74];
av2[3]=lf[13];
tp(4,av2);}}
else{
/* tcp.scm:622: io-ports */
t6=lf[32];
f_1653(t6,((C_word*)t0)[3],lf[72],((C_word*)t0)[2]);}}}

/* k2605 in k2599 in k2596 in k2593 in k2590 in k2584 in k2581 in k2578 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2607(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2607,c,av);}
/* tcp.scm:622: io-ports */
t2=lf[32];
f_1653(t2,((C_word*)t0)[2],lf[72],((C_word*)t0)[3]);}

/* k2624 in k2599 in k2596 in k2593 in k2590 in k2584 in k2581 in k2578 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2626(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_2626,c,av);}
/* tcp.scm:618: ##sys#signal-hook */
t2=*((C_word*)lf[6]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[7];
av2[3]=lf[72];
av2[4]=t1;
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* k2628 in k2599 in k2596 in k2593 in k2590 in k2584 in k2581 in k2578 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2630(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_2630,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2634,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=C_a_i_bytevector(&a,1,C_fix(3));
t4=C_i_foreign_fixnum_argumentp(((C_word*)t0)[3]);
/* tcp.scm:171: ##sys#peek-c-string */
t5=*((C_word*)lf[10]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=stub109(t3,t4);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k2632 in k2628 in k2599 in k2596 in k2593 in k2590 in k2584 in k2581 in k2578 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 in ... */
static void C_ccall f_2634(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2634,c,av);}
/* tcp.scm:618: scheme#string-append */
t2=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k2650 in k2599 in k2596 in k2593 in k2590 in k2584 in k2581 in k2578 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2652(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_2652,c,av);}
/* tcp.scm:621: ##sys#signal-hook */
t2=*((C_word*)lf[6]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[7];
av2[3]=lf[72];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k2654 in k2599 in k2596 in k2593 in k2590 in k2584 in k2581 in k2578 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2656(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_2656,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2660,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=C_a_i_bytevector(&a,1,C_fix(3));
t4=C_i_foreign_fixnum_argumentp(((C_word*)t0)[3]);
/* tcp.scm:171: ##sys#peek-c-string */
t5=*((C_word*)lf[10]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=stub109(t3,t4);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k2658 in k2654 in k2599 in k2596 in k2593 in k2590 in k2584 in k2581 in k2578 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 in ... */
static void C_ccall f_2660(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2660,c,av);}
/* tcp.scm:621: scheme#string-append */
t2=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* loop in k2596 in k2593 in k2590 in k2584 in k2581 in k2578 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2664(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(10,c,4)))){
C_save_and_reclaim((void *)f_2664,c,av);}
a=C_alloc(10);
t2=C_fix((C_word)sizeof(struct sockaddr_in));
t3=C_i_foreign_fixnum_argumentp(((C_word*)t0)[2]);
t4=(C_truep(((C_word*)t0)[3])?C_i_foreign_block_argumentp(((C_word*)t0)[3]):C_SCHEME_FALSE);
t5=C_i_foreign_fixnum_argumentp(t2);
t6=C_eqp(C_fix((C_word)SOCKET_ERROR),stub191(C_SCHEME_UNDEFINED,t3,t4,t5));
if(C_truep(t6)){
if(C_truep(stub117(C_SCHEME_UNDEFINED))){
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2680,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
if(C_truep(((C_word*)t0)[4])){
/* tcp.scm:608: ##sys#thread-block-for-timeout! */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[39]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[39]+1);
av2[1]=t7;
av2[2]=*((C_word*)lf[33]+1);
av2[3]=((C_word*)t0)[4];
tp(4,av2);}}
else{
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f3432,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* tcp.scm:609: ##sys#thread-block-for-i/o! */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[37]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[37]+1);
av2[1]=t8;
av2[2]=*((C_word*)lf[33]+1);
av2[3]=((C_word*)t0)[2];
av2[4]=lf[44];
tp(5,av2);}}}
else{
if(C_truep(stub120(C_SCHEME_UNDEFINED))){
/* tcp.scm:612: ##sys#dispatch-interrupt */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[40]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[40]+1);
av2[1]=t1;
av2[2]=((C_word*)((C_word*)t0)[5])[1];
tp(3,av2);}}
else{
t7=stub105(C_SCHEME_UNDEFINED);
t8=C_i_foreign_fixnum_argumentp(((C_word*)t0)[2]);
t9=stub164(C_SCHEME_UNDEFINED,t8);
t10=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2711,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[7],tmp=(C_word)a,a+=6,tmp);
t11=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2715,a[2]=t10,a[3]=t7,tmp=(C_word)a,a+=4,tmp);
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[11]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[11]+1);
av2[1]=t11;
av2[2]=lf[75];
av2[3]=lf[13];
tp(4,av2);}}}}
else{
t7=C_SCHEME_UNDEFINED;
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=t7;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}}

/* k2678 in loop in k2596 in k2593 in k2590 in k2584 in k2581 in k2578 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2680(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_2680,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2683,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* tcp.scm:609: ##sys#thread-block-for-i/o! */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[37]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[37]+1);
av2[1]=t2;
av2[2]=*((C_word*)lf[33]+1);
av2[3]=((C_word*)t0)[3];
av2[4]=lf[44];
tp(5,av2);}}

/* k2681 in k2678 in loop in k2596 in k2593 in k2590 in k2584 in k2581 in k2578 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 in ... */
static void C_ccall f_2683(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2683,c,av);}
/* tcp.scm:610: ##sys#thread-yield! */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[36]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[36]+1);
av2[1]=((C_word*)t0)[2];
tp(2,av2);}}

/* k2709 in loop in k2596 in k2593 in k2590 in k2584 in k2581 in k2578 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2711(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_2711,c,av);}
/* tcp.scm:614: ##sys#signal-hook */
t2=*((C_word*)lf[6]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[7];
av2[3]=lf[72];
av2[4]=t1;
av2[5]=((C_word*)t0)[3];
av2[6]=((C_word*)((C_word*)t0)[4])[1];
av2[7]=((C_word*)((C_word*)t0)[5])[1];
((C_proc)(void*)(*((C_word*)t2+1)))(8,av2);}}

/* k2713 in loop in k2596 in k2593 in k2590 in k2584 in k2581 in k2578 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2715(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_2715,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2719,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=C_a_i_bytevector(&a,1,C_fix(3));
t4=C_i_foreign_fixnum_argumentp(((C_word*)t0)[3]);
/* tcp.scm:171: ##sys#peek-c-string */
t5=*((C_word*)lf[10]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=stub109(t3,t4);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k2717 in k2713 in loop in k2596 in k2593 in k2590 in k2584 in k2581 in k2578 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 in ... */
static void C_ccall f_2719(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2719,c,av);}
/* tcp.scm:614: scheme#string-append */
t2=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k2736 in k2593 in k2590 in k2584 in k2581 in k2578 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2738(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_2738,c,av);}
/* tcp.scm:603: ##sys#signal-hook */
t2=*((C_word*)lf[6]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[7];
av2[3]=lf[72];
av2[4]=t1;
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* k2740 in k2593 in k2590 in k2584 in k2581 in k2578 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2742(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_2742,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2746,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=C_a_i_bytevector(&a,1,C_fix(3));
t4=C_i_foreign_fixnum_argumentp(((C_word*)t0)[3]);
/* tcp.scm:171: ##sys#peek-c-string */
t5=*((C_word*)lf[10]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=stub109(t3,t4);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k2744 in k2740 in k2593 in k2590 in k2584 in k2581 in k2578 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2746(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2746,c,av);}
/* tcp.scm:603: scheme#string-append */
t2=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k2762 in k2590 in k2584 in k2581 in k2578 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2764(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_2764,c,av);}
/* tcp.scm:601: ##sys#signal-hook */
t2=*((C_word*)lf[6]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[7];
av2[3]=lf[72];
av2[4]=t1;
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* k2766 in k2590 in k2584 in k2581 in k2578 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2768(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_2768,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2772,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=C_a_i_bytevector(&a,1,C_fix(3));
t4=C_i_foreign_fixnum_argumentp(((C_word*)t0)[3]);
/* tcp.scm:171: ##sys#peek-c-string */
t5=*((C_word*)lf[10]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=stub109(t3,t4);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k2770 in k2766 in k2590 in k2584 in k2581 in k2578 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2772(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2772,c,av);}
/* tcp.scm:601: scheme#string-append */
t2=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k2786 in k2584 in k2581 in k2578 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2788(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_2788,c,av);}
/* tcp.scm:599: ##sys#signal-hook */
t2=*((C_word*)lf[6]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[7];
av2[3]=lf[72];
av2[4]=t1;
av2[5]=((C_word*)((C_word*)t0)[3])[1];
av2[6]=((C_word*)((C_word*)t0)[4])[1];
((C_proc)(void*)(*((C_word*)t2+1)))(7,av2);}}

/* k2790 in k2584 in k2581 in k2578 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2792(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_2792,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2796,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=stub105(C_SCHEME_UNDEFINED);
t4=C_a_i_bytevector(&a,1,C_fix(3));
t5=C_i_foreign_fixnum_argumentp(t3);
/* tcp.scm:171: ##sys#peek-c-string */
t6=*((C_word*)lf[10]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t2;
av2[2]=stub109(t4,t5);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* k2794 in k2790 in k2584 in k2581 in k2578 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2796(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2796,c,av);}
/* tcp.scm:599: scheme#string-append */
t2=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k2803 in k2581 in k2578 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2805(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_2805,c,av);}
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
f_2586(2,av2);}}
else{
/* tcp.scm:596: ##sys#signal-hook */
t2=*((C_word*)lf[6]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[7];
av2[3]=lf[72];
av2[4]=lf[79];
av2[5]=((C_word*)((C_word*)t0)[3])[1];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}}

/* k2809 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2811(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_2811,c,av);}
if(C_truep(((C_word*)((C_word*)t0)[2])[1])){
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
f_2580(2,av2);}}
else{
/* tcp.scm:593: ##sys#signal-hook */
t2=*((C_word*)lf[6]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[21];
av2[3]=lf[72];
av2[4]=lf[80];
av2[5]=((C_word*)((C_word*)t0)[4])[1];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}}

/* a2818 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2819(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_2819,c,av);}
a=C_alloc(9);
t2=((C_word*)((C_word*)t0)[2])[1];
t3=lf[81];
t4=C_block_size(t2);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_1283,a[2]=t4,a[3]=t2,a[4]=t3,a[5]=t6,a[6]=((C_word)li31),tmp=(C_word)a,a+=7,tmp));
t8=((C_word*)t6)[1];
f_1283(t8,t1,C_fix(0));}

/* a2824 in k2572 in k2569 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2825(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_2825,c,av);}
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=C_mutate(((C_word *)((C_word*)t0)[3])+1,t3);
t6=t1;{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}

/* k2834 in k2566 in chicken.tcp#tcp-connect in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2836(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(29,c,1)))){
C_save_and_reclaim((void *)f_2836,c,av);}
a=C_alloc(29);
t2=((C_word*)t0)[2];
f_2571(t2,C_s_a_i_plus(&a,2,t1,((C_word*)t0)[3]));}

/* chicken.tcp#tcp-port->fileno in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_fcall f_2844(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_2844,3,t1,t2,t3);}
a=C_alloc(5);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2848,a[2]=t1,a[3]=t3,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* tcp.scm:625: ##sys#port-data */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[87]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[87]+1);
av2[1]=t4;
av2[2]=t2;
tp(3,av2);}}

/* k2846 in chicken.tcp#tcp-port->fileno in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2848(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_2848,c,av);}
if(C_truep(C_i_vectorp(t1))){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_slot(t1,C_fix(0));
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
/* tcp.scm:628: chicken.base#error */
t2=*((C_word*)lf[85]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=lf[86];
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}}

/* chicken.tcp#tcp-addresses in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2862(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_2862,c,av);}
a=C_alloc(4);
t3=C_i_check_port_2(t2,C_fix(0),C_SCHEME_TRUE,lf[89]);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2868,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* tcp.scm:632: tcp-port->fileno */
f_2844(t4,t2,lf[89]);}

/* k2866 in chicken.tcp#tcp-addresses in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2868(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_2868,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2875,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t3=C_a_i_bytevector(&a,1,C_fix(3));
t4=C_i_foreign_fixnum_argumentp(t1);
/* tcp.scm:192: ##sys#peek-c-string */
t5=*((C_word*)lf[10]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=stub225(t3,t4);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k2873 in k2866 in chicken.tcp#tcp-addresses in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2875(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_2875,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2878,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
if(C_truep(t1)){
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=t1;
f_2878(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2913,a[2]=t2,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2917,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[11]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[11]+1);
av2[1]=t4;
av2[2]=lf[91];
av2[3]=lf[13];
tp(4,av2);}}}

/* k2876 in k2873 in k2866 in chicken.tcp#tcp-addresses in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2878(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_2878,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2882,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
t3=C_a_i_bytevector(&a,1,C_fix(3));
t4=C_i_foreign_fixnum_argumentp(((C_word*)t0)[4]);
/* tcp.scm:216: ##sys#peek-c-string */
t5=*((C_word*)lf[10]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=stub241(t3,t4);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k2880 in k2876 in k2873 in k2866 in chicken.tcp#tcp-addresses in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2882(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,3)))){
C_save_and_reclaim((void *)f_2882,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2885,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
if(C_truep(t1)){
/* tcp.scm:633: scheme#values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
C_values(4,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2892,a[2]=t2,a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2896,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[11]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[11]+1);
av2[1]=t4;
av2[2]=lf[90];
av2[3]=lf[13];
tp(4,av2);}}}

/* k2883 in k2880 in k2876 in k2873 in k2866 in chicken.tcp#tcp-addresses in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2885(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2885,c,av);}
/* tcp.scm:633: scheme#values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
C_values(4,av2);}}

/* k2890 in k2880 in k2876 in k2873 in k2866 in chicken.tcp#tcp-addresses in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2892(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_2892,c,av);}
/* tcp.scm:637: ##sys#signal-hook */
t2=*((C_word*)lf[6]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[7];
av2[3]=lf[89];
av2[4]=t1;
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* k2894 in k2880 in k2876 in k2873 in k2866 in chicken.tcp#tcp-addresses in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2896(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_2896,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2900,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=stub105(C_SCHEME_UNDEFINED);
t4=C_a_i_bytevector(&a,1,C_fix(3));
t5=C_i_foreign_fixnum_argumentp(t3);
/* tcp.scm:171: ##sys#peek-c-string */
t6=*((C_word*)lf[10]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t2;
av2[2]=stub109(t4,t5);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* k2898 in k2894 in k2880 in k2876 in k2873 in k2866 in chicken.tcp#tcp-addresses in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2900(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2900,c,av);}
/* tcp.scm:637: scheme#string-append */
t2=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k2911 in k2873 in k2866 in chicken.tcp#tcp-addresses in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2913(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_2913,c,av);}
/* tcp.scm:635: ##sys#signal-hook */
t2=*((C_word*)lf[6]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[7];
av2[3]=lf[89];
av2[4]=t1;
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* k2915 in k2873 in k2866 in chicken.tcp#tcp-addresses in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2917(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_2917,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2921,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=stub105(C_SCHEME_UNDEFINED);
t4=C_a_i_bytevector(&a,1,C_fix(3));
t5=C_i_foreign_fixnum_argumentp(t3);
/* tcp.scm:171: ##sys#peek-c-string */
t6=*((C_word*)lf[10]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t2;
av2[2]=stub109(t4,t5);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* k2919 in k2915 in k2873 in k2866 in chicken.tcp#tcp-addresses in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2921(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2921,c,av);}
/* tcp.scm:635: scheme#string-append */
t2=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.tcp#tcp-port-numbers in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2929(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_2929,c,av);}
a=C_alloc(4);
t3=C_i_check_port_2(t2,C_fix(0),C_SCHEME_TRUE,lf[93]);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2935,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* tcp.scm:641: tcp-port->fileno */
f_2844(t4,t2,lf[93]);}

/* k2933 in chicken.tcp#tcp-port-numbers in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2935(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,3)))){
C_save_and_reclaim((void *)f_2935,c,av);}
a=C_alloc(13);
t2=C_i_foreign_fixnum_argumentp(t1);
t3=stub231(C_SCHEME_UNDEFINED,t2);
t4=C_i_foreign_fixnum_argumentp(t1);
t5=stub236(C_SCHEME_UNDEFINED,t4);
t6=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2944,a[2]=((C_word*)t0)[2],a[3]=t3,a[4]=t5,a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
t7=C_eqp(C_fix(-1),t3);
if(C_truep(t7)){
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2984,a[2]=t6,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t9=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2988,a[2]=t8,tmp=(C_word)a,a+=3,tmp);
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[11]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[11]+1);
av2[1]=t9;
av2[2]=lf[95];
av2[3]=lf[13];
tp(4,av2);}}
else{
t8=t6;{
C_word *av2=av;
av2[0]=t8;
av2[1]=C_SCHEME_UNDEFINED;
f_2944(2,av2);}}}

/* k2942 in k2933 in chicken.tcp#tcp-port-numbers in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2944(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_2944,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2947,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=C_eqp(C_fix(-1),((C_word*)t0)[4]);
if(C_truep(t3)){
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2960,a[2]=t2,a[3]=((C_word*)t0)[5],tmp=(C_word)a,a+=4,tmp);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2964,a[2]=t4,tmp=(C_word)a,a+=3,tmp);
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[11]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[11]+1);
av2[1]=t5;
av2[2]=lf[94];
av2[3]=lf[13];
tp(4,av2);}}
else{
/* tcp.scm:648: scheme#values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
C_values(4,av2);}}}

/* k2945 in k2942 in k2933 in chicken.tcp#tcp-port-numbers in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2947(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2947,c,av);}
/* tcp.scm:648: scheme#values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
C_values(4,av2);}}

/* k2958 in k2942 in k2933 in chicken.tcp#tcp-port-numbers in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2960(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_2960,c,av);}
/* tcp.scm:647: ##sys#signal-hook */
t2=*((C_word*)lf[6]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[7];
av2[3]=lf[93];
av2[4]=t1;
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* k2962 in k2942 in k2933 in chicken.tcp#tcp-port-numbers in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2964(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_2964,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2968,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=stub105(C_SCHEME_UNDEFINED);
t4=C_a_i_bytevector(&a,1,C_fix(3));
t5=C_i_foreign_fixnum_argumentp(t3);
/* tcp.scm:171: ##sys#peek-c-string */
t6=*((C_word*)lf[10]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t2;
av2[2]=stub109(t4,t5);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* k2966 in k2962 in k2942 in k2933 in chicken.tcp#tcp-port-numbers in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2968(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2968,c,av);}
/* tcp.scm:647: scheme#string-append */
t2=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k2982 in k2933 in chicken.tcp#tcp-port-numbers in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2984(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_2984,c,av);}
/* tcp.scm:645: ##sys#signal-hook */
t2=*((C_word*)lf[6]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[7];
av2[3]=lf[93];
av2[4]=t1;
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* k2986 in k2933 in chicken.tcp#tcp-port-numbers in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2988(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_2988,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2992,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=stub105(C_SCHEME_UNDEFINED);
t4=C_a_i_bytevector(&a,1,C_fix(3));
t5=C_i_foreign_fixnum_argumentp(t3);
/* tcp.scm:171: ##sys#peek-c-string */
t6=*((C_word*)lf[10]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t2;
av2[2]=stub109(t4,t5);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* k2990 in k2986 in k2933 in chicken.tcp#tcp-port-numbers in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_2992(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2992,c,av);}
/* tcp.scm:645: scheme#string-append */
t2=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.tcp#tcp-listener-port in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_3000(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_3000,c,av);}
a=C_alloc(12);
t3=C_i_check_structure_2(t2,lf[5],lf[97]);
t4=C_slot(t2,C_fix(1));
t5=C_i_foreign_fixnum_argumentp(t4);
t6=stub231(C_SCHEME_UNDEFINED,t5);
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3013,a[2]=t1,a[3]=t6,tmp=(C_word)a,a+=4,tmp);
t8=C_eqp(C_fix(-1),t6);
if(C_truep(t8)){
t9=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3023,a[2]=t7,a[3]=t2,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
t10=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3027,a[2]=t9,tmp=(C_word)a,a+=3,tmp);
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[11]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[11]+1);
av2[1]=t10;
av2[2]=lf[98];
av2[3]=lf[13];
tp(4,av2);}}
else{
t9=t1;{
C_word *av2=av;
av2[0]=t9;
av2[1]=t6;
((C_proc)(void*)(*((C_word*)t9+1)))(2,av2);}}}

/* k3011 in chicken.tcp#tcp-listener-port in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_3013(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3013,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k3021 in chicken.tcp#tcp-listener-port in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_3023(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_3023,c,av);}
/* tcp.scm:655: ##sys#signal-hook */
t2=*((C_word*)lf[6]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[7];
av2[3]=lf[97];
av2[4]=t1;
av2[5]=((C_word*)t0)[3];
av2[6]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(7,av2);}}

/* k3025 in chicken.tcp#tcp-listener-port in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_3027(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_3027,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3031,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=stub105(C_SCHEME_UNDEFINED);
t4=C_a_i_bytevector(&a,1,C_fix(3));
t5=C_i_foreign_fixnum_argumentp(t3);
/* tcp.scm:171: ##sys#peek-c-string */
t6=*((C_word*)lf[10]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t2;
av2[2]=stub109(t4,t5);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* k3029 in k3025 in chicken.tcp#tcp-listener-port in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_3031(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3031,c,av);}
/* tcp.scm:655: scheme#string-append */
t2=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.tcp#tcp-abandon-port in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_3039(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_3039,c,av);}
a=C_alloc(4);
t3=C_i_check_port_2(t2,C_fix(0),C_SCHEME_TRUE,lf[100]);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3049,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* tcp.scm:660: ##sys#port-data */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[87]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[87]+1);
av2[1]=t4;
av2[2]=t2;
tp(3,av2);}}

/* k3047 in chicken.tcp#tcp-abandon-port in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_3049(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3049,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_i_set_i_slot(t1,C_slot(((C_word*)t0)[3],C_fix(1)),C_SCHEME_TRUE);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.tcp#tcp-listener-fileno in k1649 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_3055(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3055,c,av);}
t3=C_i_check_structure_2(t2,lf[5],lf[102]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_slot(t2,C_fix(1));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k3064 in k1645 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_3066(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3066,c,av);}
/* tcp.scm:359: chicken.base#make-parameter */
t2=*((C_word*)lf[103]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_SCHEME_FALSE;
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k3068 in k1641 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_3070(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3070,c,av);}
/* tcp.scm:358: chicken.base#make-parameter */
t2=*((C_word*)lf[103]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_SCHEME_FALSE;
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k3072 in k1637 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_3074(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3074,c,av);}
/* tcp.scm:357: chicken.base#make-parameter */
t2=*((C_word*)lf[103]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)((C_word*)t0)[3])[1];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k3076 in k1614 in k1219 in k991 in k988 in k985 */
static void C_ccall f_3078(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3078,c,av);}
/* tcp.scm:356: chicken.base#make-parameter */
t2=*((C_word*)lf[103]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)((C_word*)t0)[3])[1];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k985 */
static void C_ccall f_987(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_987,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_990,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_scheduler_toplevel(2,av2);}}

/* k988 in k985 */
static void C_ccall f_990(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_990,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_993,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_library_toplevel(2,av2);}}

/* k991 in k988 in k985 */
static void C_ccall f_993(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(19,c,3)))){
C_save_and_reclaim((void *)f_993,c,av);}
a=C_alloc(19);
t2=C_a_i_provide(&a,1,lf[0]);
t3=C_a_i_provide(&a,1,lf[1]);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1221,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(stub246(C_SCHEME_UNDEFINED))){
t5=t4;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_UNDEFINED;
f_1221(2,av2);}}
else{
/* tcp.scm:237: ##sys#signal-hook */
t5=*((C_word*)lf[6]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=lf[7];
av2[3]=lf[108];
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}}

/* toplevel */
static C_TLS int toplevel_initialized=0;

void C_ccall C_tcp_toplevel(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(toplevel_initialized) {C_kontinue(t1,C_SCHEME_UNDEFINED);}
else C_toplevel_entry(C_text("tcp"));
C_check_nursery_minimum(C_calculate_demand(3,c,2));
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void*)C_tcp_toplevel,c,av);}
toplevel_initialized=1;
if(C_unlikely(!C_demand_2(441))){
C_save(t1);
C_rereclaim2(441*sizeof(C_word),1);
t1=C_restore;}
a=C_alloc(3);
C_initialize_lf(lf,109);
lf[0]=C_h_intern(&lf[0],3, C_text("tcp"));
lf[1]=C_h_intern(&lf[1],12, C_text("chicken.tcp#"));
lf[3]=C_h_intern(&lf[3],19, C_text("##sys#make-c-string"));
lf[4]=C_h_intern(&lf[4],22, C_text("chicken.tcp#tcp-listen"));
lf[5]=C_h_intern(&lf[5],12, C_text("tcp-listener"));
lf[6]=C_h_intern(&lf[6],17, C_text("##sys#signal-hook"));
lf[7]=C_h_intern_kw(&lf[7],13, C_text("network-error"));
lf[8]=C_h_intern(&lf[8],10, C_text("tcp-listen"));
lf[9]=C_h_intern(&lf[9],20, C_text("scheme#string-append"));
lf[10]=C_h_intern(&lf[10],19, C_text("##sys#peek-c-string"));
lf[11]=C_h_intern(&lf[11],19, C_text("##sys#string-append"));
lf[12]=C_decode_literal(C_heaptop,C_text("\376B\000\000\027cannot listen on socket"));
lf[13]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003 - "));
lf[14]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025cannot bind to socket"));
lf[15]=C_decode_literal(C_heaptop,C_text("\376B\000\000\035error while setting up socket"));
lf[16]=C_h_intern(&lf[16],11, C_text("##sys#error"));
lf[17]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024cannot create socket"));
lf[18]=C_decode_literal(C_heaptop,C_text("\376B\000\000\037getting listener host IP failed"));
lf[19]=C_h_intern(&lf[19],17, C_text("##sys#make-string"));
lf[20]=C_h_intern(&lf[20],18, C_text("##sys#check-fixnum"));
lf[21]=C_h_intern_kw(&lf[21],12, C_text("domain-error"));
lf[22]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023invalid port number"));
lf[23]=C_h_intern(&lf[23],25, C_text("chicken.tcp#tcp-listener\077"));
lf[24]=C_h_intern(&lf[24],21, C_text("chicken.tcp#tcp-close"));
lf[25]=C_h_intern(&lf[25],9, C_text("tcp-close"));
lf[26]=C_decode_literal(C_heaptop,C_text("\376B\000\000\027cannot close TCP socket"));
lf[27]=C_h_intern(&lf[27],27, C_text("chicken.tcp#tcp-buffer-size"));
lf[28]=C_h_intern(&lf[28],28, C_text("chicken.tcp#tcp-read-timeout"));
lf[29]=C_h_intern(&lf[29],29, C_text("chicken.tcp#tcp-write-timeout"));
lf[30]=C_h_intern(&lf[30],31, C_text("chicken.tcp#tcp-connect-timeout"));
lf[31]=C_h_intern(&lf[31],30, C_text("chicken.tcp#tcp-accept-timeout"));
lf[33]=C_h_intern(&lf[33],20, C_text("##sys#current-thread"));
lf[34]=C_h_intern_kw(&lf[34],21, C_text("network-timeout-error"));
lf[35]=C_decode_literal(C_heaptop,C_text("\376B\000\000\030read operation timed out"));
lf[36]=C_h_intern(&lf[36],19, C_text("##sys#thread-yield!"));
lf[37]=C_h_intern(&lf[37],27, C_text("##sys#thread-block-for-i/o!"));
lf[38]=C_h_intern_kw(&lf[38],5, C_text("input"));
lf[39]=C_h_intern(&lf[39],31, C_text("##sys#thread-block-for-timeout!"));
lf[40]=C_h_intern(&lf[40],24, C_text("##sys#dispatch-interrupt"));
lf[41]=C_decode_literal(C_heaptop,C_text("\376B\000\000\027cannot read from socket"));
lf[42]=C_h_intern(&lf[42],33, C_text("chicken.time#current-milliseconds"));
lf[43]=C_decode_literal(C_heaptop,C_text("\376B\000\000\031write operation timed out"));
lf[44]=C_h_intern_kw(&lf[44],6, C_text("output"));
lf[45]=C_decode_literal(C_heaptop,C_text("\376B\000\000\026cannot write to socket"));
lf[46]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005(tcp)"));
lf[47]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005(tcp)"));
lf[48]=C_h_intern(&lf[48],6, C_text("socket"));
lf[49]=C_h_intern(&lf[49],20, C_text("##sys#set-port-data!"));
lf[50]=C_decode_literal(C_heaptop,C_text("\376B\000\000\000"));
lf[51]=C_decode_literal(C_heaptop,C_text("\376B\000\000\037cannot close socket output port"));
lf[52]=C_decode_literal(C_heaptop,C_text("\376B\000\000\000"));
lf[53]=C_decode_literal(C_heaptop,C_text("\376B\000\000\000"));
lf[54]=C_h_intern(&lf[54],29, C_text("chicken.port#make-output-port"));
lf[55]=C_decode_literal(C_heaptop,C_text("\376B\000\000\035cannot check socket for input"));
lf[56]=C_decode_literal(C_heaptop,C_text("\376B\000\000\036cannot close socket input port"));
lf[57]=C_h_intern(&lf[57],35, C_text("chicken.fixnum#most-positive-fixnum"));
lf[58]=C_h_intern(&lf[58],22, C_text("##sys#scan-buffer-line"));
lf[59]=C_decode_literal(C_heaptop,C_text("\376B\000\000\000"));
lf[60]=C_h_intern(&lf[60],15, C_text("##sys#substring"));
lf[61]=C_h_intern(&lf[61],28, C_text("chicken.port#make-input-port"));
lf[62]=C_decode_literal(C_heaptop,C_text("\376B\000\000\000"));
lf[63]=C_decode_literal(C_heaptop,C_text("\376B\000\000\027cannot create TCP ports"));
lf[64]=C_h_intern(&lf[64],22, C_text("chicken.tcp#tcp-accept"));
lf[65]=C_h_intern(&lf[65],10, C_text("tcp-accept"));
lf[66]=C_decode_literal(C_heaptop,C_text("\376B\000\000\036could not accept from listener"));
lf[67]=C_decode_literal(C_heaptop,C_text("\376B\000\000\032accept operation timed out"));
lf[68]=C_h_intern(&lf[68],29, C_text("chicken.tcp#tcp-accept-ready\077"));
lf[69]=C_h_intern(&lf[69],17, C_text("tcp-accept-ready\077"));
lf[70]=C_decode_literal(C_heaptop,C_text("\376B\000\000\035cannot check socket for input"));
lf[71]=C_h_intern(&lf[71],23, C_text("chicken.tcp#tcp-connect"));
lf[72]=C_h_intern(&lf[72],11, C_text("tcp-connect"));
lf[73]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023getsockopt() failed"));
lf[74]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024cannot create socket"));
lf[75]=C_decode_literal(C_heaptop,C_text("\376B\000\000\030cannot connect to socket"));
lf[76]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016fcntl() failed"));
lf[77]=C_decode_literal(C_heaptop,C_text("\376B\000\000\035error while setting up socket"));
lf[78]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024cannot create socket"));
lf[79]=C_decode_literal(C_heaptop,C_text("\376B\000\000\030cannot find host address"));
lf[80]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021no port specified"));
lf[81]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003tcp"));
lf[82]=C_decode_literal(C_heaptop,C_text("\376B\000\000 cannot compute port from service"));
lf[83]=C_h_intern(&lf[83],16, C_text("scheme#substring"));
lf[85]=C_h_intern(&lf[85],18, C_text("chicken.base#error"));
lf[86]=C_decode_literal(C_heaptop,C_text("\376B\000\000)argument does not appear to be a TCP port"));
lf[87]=C_h_intern(&lf[87],15, C_text("##sys#port-data"));
lf[88]=C_h_intern(&lf[88],25, C_text("chicken.tcp#tcp-addresses"));
lf[89]=C_h_intern(&lf[89],13, C_text("tcp-addresses"));
lf[90]=C_decode_literal(C_heaptop,C_text("\376B\000\000\035cannot compute remote address"));
lf[91]=C_decode_literal(C_heaptop,C_text("\376B\000\000\034cannot compute local address"));
lf[92]=C_h_intern(&lf[92],28, C_text("chicken.tcp#tcp-port-numbers"));
lf[93]=C_h_intern(&lf[93],16, C_text("tcp-port-numbers"));
lf[94]=C_decode_literal(C_heaptop,C_text("\376B\000\000\032cannot compute remote port"));
lf[95]=C_decode_literal(C_heaptop,C_text("\376B\000\000\031cannot compute local port"));
lf[96]=C_h_intern(&lf[96],29, C_text("chicken.tcp#tcp-listener-port"));
lf[97]=C_h_intern(&lf[97],17, C_text("tcp-listener-port"));
lf[98]=C_decode_literal(C_heaptop,C_text("\376B\000\000\033cannot obtain listener port"));
lf[99]=C_h_intern(&lf[99],28, C_text("chicken.tcp#tcp-abandon-port"));
lf[100]=C_h_intern(&lf[100],16, C_text("tcp-abandon-port"));
lf[101]=C_h_intern(&lf[101],31, C_text("chicken.tcp#tcp-listener-fileno"));
lf[102]=C_h_intern(&lf[102],19, C_text("tcp-listener-fileno"));
lf[103]=C_h_intern(&lf[103],27, C_text("chicken.base#make-parameter"));
lf[104]=C_h_intern(&lf[104],18, C_text("tcp-accept-timeout"));
lf[105]=C_h_intern(&lf[105],19, C_text("tcp-connect-timeout"));
lf[106]=C_h_intern(&lf[106],17, C_text("tcp-write-timeout"));
lf[107]=C_h_intern(&lf[107],16, C_text("tcp-read-timeout"));
lf[108]=C_decode_literal(C_heaptop,C_text("\376B\000\000\031cannot initialize Winsock"));
C_register_lf2(lf,109,create_ptable());{}
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_987,a[2]=t1,tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_port_toplevel(2,av2);}}

#ifdef C_ENABLE_PTABLES
static C_PTABLE_ENTRY ptable[222] = {
{C_text("f3432:tcp_2escm"),(void*)f3432},
{C_text("f_1221:tcp_2escm"),(void*)f_1221},
{C_text("f_1228:tcp_2escm"),(void*)f_1228},
{C_text("f_1232:tcp_2escm"),(void*)f_1232},
{C_text("f_1248:tcp_2escm"),(void*)f_1248},
{C_text("f_1257:tcp_2escm"),(void*)f_1257},
{C_text("f_1283:tcp_2escm"),(void*)f_1283},
{C_text("f_1306:tcp_2escm"),(void*)f_1306},
{C_text("f_1310:tcp_2escm"),(void*)f_1310},
{C_text("f_1313:tcp_2escm"),(void*)f_1313},
{C_text("f_1316:tcp_2escm"),(void*)f_1316},
{C_text("f_1326:tcp_2escm"),(void*)f_1326},
{C_text("f_1330:tcp_2escm"),(void*)f_1330},
{C_text("f_1334:tcp_2escm"),(void*)f_1334},
{C_text("f_1372:tcp_2escm"),(void*)f_1372},
{C_text("f_1375:tcp_2escm"),(void*)f_1375},
{C_text("f_1381:tcp_2escm"),(void*)f_1381},
{C_text("f_1384:tcp_2escm"),(void*)f_1384},
{C_text("f_1387:tcp_2escm"),(void*)f_1387},
{C_text("f_1403:tcp_2escm"),(void*)f_1403},
{C_text("f_1407:tcp_2escm"),(void*)f_1407},
{C_text("f_1411:tcp_2escm"),(void*)f_1411},
{C_text("f_1427:tcp_2escm"),(void*)f_1427},
{C_text("f_1431:tcp_2escm"),(void*)f_1431},
{C_text("f_1435:tcp_2escm"),(void*)f_1435},
{C_text("f_1457:tcp_2escm"),(void*)f_1457},
{C_text("f_1466:tcp_2escm"),(void*)f_1466},
{C_text("f_1482:tcp_2escm"),(void*)f_1482},
{C_text("f_1485:tcp_2escm"),(void*)f_1485},
{C_text("f_1488:tcp_2escm"),(void*)f_1488},
{C_text("f_1491:tcp_2escm"),(void*)f_1491},
{C_text("f_1494:tcp_2escm"),(void*)f_1494},
{C_text("f_1513:tcp_2escm"),(void*)f_1513},
{C_text("f_1517:tcp_2escm"),(void*)f_1517},
{C_text("f_1521:tcp_2escm"),(void*)f_1521},
{C_text("f_1565:tcp_2escm"),(void*)f_1565},
{C_text("f_1574:tcp_2escm"),(void*)f_1574},
{C_text("f_1594:tcp_2escm"),(void*)f_1594},
{C_text("f_1598:tcp_2escm"),(void*)f_1598},
{C_text("f_1602:tcp_2escm"),(void*)f_1602},
{C_text("f_1616:tcp_2escm"),(void*)f_1616},
{C_text("f_1622:tcp_2escm"),(void*)f_1622},
{C_text("f_1624:tcp_2escm"),(void*)f_1624},
{C_text("f_1628:tcp_2escm"),(void*)f_1628},
{C_text("f_1639:tcp_2escm"),(void*)f_1639},
{C_text("f_1643:tcp_2escm"),(void*)f_1643},
{C_text("f_1647:tcp_2escm"),(void*)f_1647},
{C_text("f_1651:tcp_2escm"),(void*)f_1651},
{C_text("f_1653:tcp_2escm"),(void*)f_1653},
{C_text("f_1657:tcp_2escm"),(void*)f_1657},
{C_text("f_1660:tcp_2escm"),(void*)f_1660},
{C_text("f_1666:tcp_2escm"),(void*)f_1666},
{C_text("f_1669:tcp_2escm"),(void*)f_1669},
{C_text("f_1670:tcp_2escm"),(void*)f_1670},
{C_text("f_1674:tcp_2escm"),(void*)f_1674},
{C_text("f_1677:tcp_2escm"),(void*)f_1677},
{C_text("f_1682:tcp_2escm"),(void*)f_1682},
{C_text("f_1701:tcp_2escm"),(void*)f_1701},
{C_text("f_1704:tcp_2escm"),(void*)f_1704},
{C_text("f_1707:tcp_2escm"),(void*)f_1707},
{C_text("f_1710:tcp_2escm"),(void*)f_1710},
{C_text("f_1738:tcp_2escm"),(void*)f_1738},
{C_text("f_1742:tcp_2escm"),(void*)f_1742},
{C_text("f_1746:tcp_2escm"),(void*)f_1746},
{C_text("f_1764:tcp_2escm"),(void*)f_1764},
{C_text("f_1767:tcp_2escm"),(void*)f_1767},
{C_text("f_1768:tcp_2escm"),(void*)f_1768},
{C_text("f_1772:tcp_2escm"),(void*)f_1772},
{C_text("f_1783:tcp_2escm"),(void*)f_1783},
{C_text("f_1785:tcp_2escm"),(void*)f_1785},
{C_text("f_1807:tcp_2escm"),(void*)f_1807},
{C_text("f_1810:tcp_2escm"),(void*)f_1810},
{C_text("f_1813:tcp_2escm"),(void*)f_1813},
{C_text("f_1816:tcp_2escm"),(void*)f_1816},
{C_text("f_1839:tcp_2escm"),(void*)f_1839},
{C_text("f_1850:tcp_2escm"),(void*)f_1850},
{C_text("f_1854:tcp_2escm"),(void*)f_1854},
{C_text("f_1858:tcp_2escm"),(void*)f_1858},
{C_text("f_1898:tcp_2escm"),(void*)f_1898},
{C_text("f_1905:tcp_2escm"),(void*)f_1905},
{C_text("f_1908:tcp_2escm"),(void*)f_1908},
{C_text("f_1923:tcp_2escm"),(void*)f_1923},
{C_text("f_1926:tcp_2escm"),(void*)f_1926},
{C_text("f_1935:tcp_2escm"),(void*)f_1935},
{C_text("f_1943:tcp_2escm"),(void*)f_1943},
{C_text("f_1946:tcp_2escm"),(void*)f_1946},
{C_text("f_1952:tcp_2escm"),(void*)f_1952},
{C_text("f_1959:tcp_2escm"),(void*)f_1959},
{C_text("f_1963:tcp_2escm"),(void*)f_1963},
{C_text("f_1967:tcp_2escm"),(void*)f_1967},
{C_text("f_1992:tcp_2escm"),(void*)f_1992},
{C_text("f_2005:tcp_2escm"),(void*)f_2005},
{C_text("f_2015:tcp_2escm"),(void*)f_2015},
{C_text("f_2021:tcp_2escm"),(void*)f_2021},
{C_text("f_2026:tcp_2escm"),(void*)f_2026},
{C_text("f_2035:tcp_2escm"),(void*)f_2035},
{C_text("f_2041:tcp_2escm"),(void*)f_2041},
{C_text("f_2057:tcp_2escm"),(void*)f_2057},
{C_text("f_2061:tcp_2escm"),(void*)f_2061},
{C_text("f_2079:tcp_2escm"),(void*)f_2079},
{C_text("f_2092:tcp_2escm"),(void*)f_2092},
{C_text("f_2105:tcp_2escm"),(void*)f_2105},
{C_text("f_2109:tcp_2escm"),(void*)f_2109},
{C_text("f_2113:tcp_2escm"),(void*)f_2113},
{C_text("f_2121:tcp_2escm"),(void*)f_2121},
{C_text("f_2129:tcp_2escm"),(void*)f_2129},
{C_text("f_2135:tcp_2escm"),(void*)f_2135},
{C_text("f_2142:tcp_2escm"),(void*)f_2142},
{C_text("f_2146:tcp_2escm"),(void*)f_2146},
{C_text("f_2150:tcp_2escm"),(void*)f_2150},
{C_text("f_2171:tcp_2escm"),(void*)f_2171},
{C_text("f_2175:tcp_2escm"),(void*)f_2175},
{C_text("f_2189:tcp_2escm"),(void*)f_2189},
{C_text("f_2195:tcp_2escm"),(void*)f_2195},
{C_text("f_2243:tcp_2escm"),(void*)f_2243},
{C_text("f_2254:tcp_2escm"),(void*)f_2254},
{C_text("f_2258:tcp_2escm"),(void*)f_2258},
{C_text("f_2272:tcp_2escm"),(void*)f_2272},
{C_text("f_2282:tcp_2escm"),(void*)f_2282},
{C_text("f_2298:tcp_2escm"),(void*)f_2298},
{C_text("f_2328:tcp_2escm"),(void*)f_2328},
{C_text("f_2373:tcp_2escm"),(void*)f_2373},
{C_text("f_2383:tcp_2escm"),(void*)f_2383},
{C_text("f_2404:tcp_2escm"),(void*)f_2404},
{C_text("f_2408:tcp_2escm"),(void*)f_2408},
{C_text("f_2412:tcp_2escm"),(void*)f_2412},
{C_text("f_2416:tcp_2escm"),(void*)f_2416},
{C_text("f_2426:tcp_2escm"),(void*)f_2426},
{C_text("f_2429:tcp_2escm"),(void*)f_2429},
{C_text("f_2434:tcp_2escm"),(void*)f_2434},
{C_text("f_2438:tcp_2escm"),(void*)f_2438},
{C_text("f_2441:tcp_2escm"),(void*)f_2441},
{C_text("f_2444:tcp_2escm"),(void*)f_2444},
{C_text("f_2447:tcp_2escm"),(void*)f_2447},
{C_text("f_2475:tcp_2escm"),(void*)f_2475},
{C_text("f_2479:tcp_2escm"),(void*)f_2479},
{C_text("f_2483:tcp_2escm"),(void*)f_2483},
{C_text("f_2509:tcp_2escm"),(void*)f_2509},
{C_text("f_2511:tcp_2escm"),(void*)f_2511},
{C_text("f_2521:tcp_2escm"),(void*)f_2521},
{C_text("f_2534:tcp_2escm"),(void*)f_2534},
{C_text("f_2538:tcp_2escm"),(void*)f_2538},
{C_text("f_2542:tcp_2escm"),(void*)f_2542},
{C_text("f_2561:tcp_2escm"),(void*)f_2561},
{C_text("f_2568:tcp_2escm"),(void*)f_2568},
{C_text("f_2571:tcp_2escm"),(void*)f_2571},
{C_text("f_2574:tcp_2escm"),(void*)f_2574},
{C_text("f_2580:tcp_2escm"),(void*)f_2580},
{C_text("f_2583:tcp_2escm"),(void*)f_2583},
{C_text("f_2586:tcp_2escm"),(void*)f_2586},
{C_text("f_2592:tcp_2escm"),(void*)f_2592},
{C_text("f_2595:tcp_2escm"),(void*)f_2595},
{C_text("f_2598:tcp_2escm"),(void*)f_2598},
{C_text("f_2601:tcp_2escm"),(void*)f_2601},
{C_text("f_2607:tcp_2escm"),(void*)f_2607},
{C_text("f_2626:tcp_2escm"),(void*)f_2626},
{C_text("f_2630:tcp_2escm"),(void*)f_2630},
{C_text("f_2634:tcp_2escm"),(void*)f_2634},
{C_text("f_2652:tcp_2escm"),(void*)f_2652},
{C_text("f_2656:tcp_2escm"),(void*)f_2656},
{C_text("f_2660:tcp_2escm"),(void*)f_2660},
{C_text("f_2664:tcp_2escm"),(void*)f_2664},
{C_text("f_2680:tcp_2escm"),(void*)f_2680},
{C_text("f_2683:tcp_2escm"),(void*)f_2683},
{C_text("f_2711:tcp_2escm"),(void*)f_2711},
{C_text("f_2715:tcp_2escm"),(void*)f_2715},
{C_text("f_2719:tcp_2escm"),(void*)f_2719},
{C_text("f_2738:tcp_2escm"),(void*)f_2738},
{C_text("f_2742:tcp_2escm"),(void*)f_2742},
{C_text("f_2746:tcp_2escm"),(void*)f_2746},
{C_text("f_2764:tcp_2escm"),(void*)f_2764},
{C_text("f_2768:tcp_2escm"),(void*)f_2768},
{C_text("f_2772:tcp_2escm"),(void*)f_2772},
{C_text("f_2788:tcp_2escm"),(void*)f_2788},
{C_text("f_2792:tcp_2escm"),(void*)f_2792},
{C_text("f_2796:tcp_2escm"),(void*)f_2796},
{C_text("f_2805:tcp_2escm"),(void*)f_2805},
{C_text("f_2811:tcp_2escm"),(void*)f_2811},
{C_text("f_2819:tcp_2escm"),(void*)f_2819},
{C_text("f_2825:tcp_2escm"),(void*)f_2825},
{C_text("f_2836:tcp_2escm"),(void*)f_2836},
{C_text("f_2844:tcp_2escm"),(void*)f_2844},
{C_text("f_2848:tcp_2escm"),(void*)f_2848},
{C_text("f_2862:tcp_2escm"),(void*)f_2862},
{C_text("f_2868:tcp_2escm"),(void*)f_2868},
{C_text("f_2875:tcp_2escm"),(void*)f_2875},
{C_text("f_2878:tcp_2escm"),(void*)f_2878},
{C_text("f_2882:tcp_2escm"),(void*)f_2882},
{C_text("f_2885:tcp_2escm"),(void*)f_2885},
{C_text("f_2892:tcp_2escm"),(void*)f_2892},
{C_text("f_2896:tcp_2escm"),(void*)f_2896},
{C_text("f_2900:tcp_2escm"),(void*)f_2900},
{C_text("f_2913:tcp_2escm"),(void*)f_2913},
{C_text("f_2917:tcp_2escm"),(void*)f_2917},
{C_text("f_2921:tcp_2escm"),(void*)f_2921},
{C_text("f_2929:tcp_2escm"),(void*)f_2929},
{C_text("f_2935:tcp_2escm"),(void*)f_2935},
{C_text("f_2944:tcp_2escm"),(void*)f_2944},
{C_text("f_2947:tcp_2escm"),(void*)f_2947},
{C_text("f_2960:tcp_2escm"),(void*)f_2960},
{C_text("f_2964:tcp_2escm"),(void*)f_2964},
{C_text("f_2968:tcp_2escm"),(void*)f_2968},
{C_text("f_2984:tcp_2escm"),(void*)f_2984},
{C_text("f_2988:tcp_2escm"),(void*)f_2988},
{C_text("f_2992:tcp_2escm"),(void*)f_2992},
{C_text("f_3000:tcp_2escm"),(void*)f_3000},
{C_text("f_3013:tcp_2escm"),(void*)f_3013},
{C_text("f_3023:tcp_2escm"),(void*)f_3023},
{C_text("f_3027:tcp_2escm"),(void*)f_3027},
{C_text("f_3031:tcp_2escm"),(void*)f_3031},
{C_text("f_3039:tcp_2escm"),(void*)f_3039},
{C_text("f_3049:tcp_2escm"),(void*)f_3049},
{C_text("f_3055:tcp_2escm"),(void*)f_3055},
{C_text("f_3066:tcp_2escm"),(void*)f_3066},
{C_text("f_3070:tcp_2escm"),(void*)f_3070},
{C_text("f_3074:tcp_2escm"),(void*)f_3074},
{C_text("f_3078:tcp_2escm"),(void*)f_3078},
{C_text("f_987:tcp_2escm"),(void*)f_987},
{C_text("f_990:tcp_2escm"),(void*)f_990},
{C_text("f_993:tcp_2escm"),(void*)f_993},
{C_text("toplevel:tcp_2escm"),(void*)C_tcp_toplevel},
{NULL,NULL}};
#endif

static C_PTABLE_ENTRY *create_ptable(void){
#ifdef C_ENABLE_PTABLES
return ptable;
#else
return NULL;
#endif
}

/*
o|hiding unexported module binding: chicken.tcp#d 
o|hiding unexported module binding: chicken.tcp#define-alias 
o|hiding unexported module binding: chicken.tcp#last-error-code 
o|hiding unexported module binding: chicken.tcp#error-code->message 
o|hiding unexported module binding: chicken.tcp#retry? 
o|hiding unexported module binding: chicken.tcp#in-progress? 
o|hiding unexported module binding: chicken.tcp#interrupted? 
o|hiding unexported module binding: chicken.tcp#socket 
o|hiding unexported module binding: chicken.tcp#bind 
o|hiding unexported module binding: chicken.tcp#listen 
o|hiding unexported module binding: chicken.tcp#accept 
o|hiding unexported module binding: chicken.tcp#close 
o|hiding unexported module binding: chicken.tcp#recv 
o|hiding unexported module binding: chicken.tcp#shutdown 
o|hiding unexported module binding: chicken.tcp#connect 
o|hiding unexported module binding: chicken.tcp#check-fd-ready 
o|hiding unexported module binding: chicken.tcp#set-socket-options 
o|hiding unexported module binding: chicken.tcp#send 
o|hiding unexported module binding: chicken.tcp#getsockname 
o|hiding unexported module binding: chicken.tcp#getsockport 
o|hiding unexported module binding: chicken.tcp#getpeerport 
o|hiding unexported module binding: chicken.tcp#getpeername 
o|hiding unexported module binding: chicken.tcp#startup 
o|hiding unexported module binding: chicken.tcp#getservbyname 
o|hiding unexported module binding: chicken.tcp#gethostaddr 
o|hiding unexported module binding: chicken.tcp#network-error 
o|hiding unexported module binding: chicken.tcp#network-error/close 
o|hiding unexported module binding: chicken.tcp#network-error/code 
o|hiding unexported module binding: chicken.tcp#parse-host 
o|hiding unexported module binding: chicken.tcp#fresh-addr 
o|hiding unexported module binding: chicken.tcp#bind-socket 
o|hiding unexported module binding: chicken.tcp#io-ports 
o|hiding unexported module binding: chicken.tcp#get-socket-error 
o|hiding unexported module binding: chicken.tcp#tcp-port->fileno 
o|eliminated procedure checks: 64 
o|specializations:
o|  3 (##sys#check-open-port * *)
o|  3 (scheme#make-string fixnum)
o|  24 (scheme#string-append string string)
(o e)|safe calls: 343 
(o e)|assignments to immediate values: 6 
o|safe globals: (chicken.tcp#startup chicken.tcp#getpeername chicken.tcp#getpeerport chicken.tcp#getsockport chicken.tcp#getsockname chicken.tcp#send chicken.tcp#set-socket-options chicken.tcp#check-fd-ready chicken.tcp#connect chicken.tcp#shutdown chicken.tcp#recv chicken.tcp#close chicken.tcp#accept chicken.tcp#listen chicken.tcp#bind chicken.tcp#socket chicken.tcp#interrupted? chicken.tcp#in-progress? chicken.tcp#retry? chicken.tcp#error-code->message chicken.tcp#last-error-code) 
o|inlining procedure: k1285 
o|inlining procedure: k1285 
o|inlining procedure: k1314 
o|inlining procedure: "(tcp.scm:290) chicken.tcp#last-error-code" 
o|substituted constant variable: a1339 
o|substituted constant variable: a1340 
o|inlining procedure: k1314 
o|contracted procedure: "(tcp.scm:288) chicken.tcp#getservbyname" 
o|inlining procedure: k1230 
o|inlining procedure: k1230 
o|inlining procedure: k1492 
o|substituted constant variable: a1522 
o|substituted constant variable: a1523 
o|inlining procedure: "(tcp.scm:329) chicken.tcp#close" 
o|inlining procedure: "(tcp.scm:329) chicken.tcp#last-error-code" 
o|inlining procedure: k1492 
o|contracted procedure: "(tcp.scm:328) chicken.tcp#listen" 
o|contracted procedure: "(tcp.scm:327) chicken.tcp#bind-socket" 
o|inlining procedure: k1385 
o|substituted constant variable: a1412 
o|substituted constant variable: a1413 
o|inlining procedure: "(tcp.scm:317) chicken.tcp#close" 
o|inlining procedure: "(tcp.scm:317) chicken.tcp#last-error-code" 
o|inlining procedure: k1385 
o|contracted procedure: "(tcp.scm:316) chicken.tcp#bind" 
o|inlining procedure: "(tcp.scm:315) chicken.tcp#last-error-code" 
o|substituted constant variable: a1440 
o|substituted constant variable: a1441 
o|inlining procedure: "(tcp.scm:314) chicken.tcp#set-socket-options" 
o|inlining procedure: k1452 
o|inlining procedure: k1452 
o|contracted procedure: "(tcp.scm:309) chicken.tcp#fresh-addr" 
o|inlining procedure: k1567 
o|inlining procedure: k1567 
o|inlining procedure: k1582 
o|inlining procedure: "(tcp.scm:340) chicken.tcp#last-error-code" 
o|substituted constant variable: a1607 
o|substituted constant variable: a1608 
o|inlining procedure: k1582 
o|inlining procedure: "(tcp.scm:339) chicken.tcp#close" 
o|inlining procedure: k1626 
o|inlining procedure: k1626 
o|inlining procedure: k1687 
o|inlining procedure: k1708 
o|inlining procedure: k1708 
o|inlining procedure: k1723 
o|inlining procedure: k1723 
o|inlining procedure: "(tcp.scm:395) chicken.tcp#last-error-code" 
o|substituted constant variable: a1751 
o|substituted constant variable: a1752 
o|inlining procedure: "(tcp.scm:392) chicken.tcp#interrupted?" 
o|inlining procedure: "(tcp.scm:381) chicken.tcp#retry?" 
o|inlining procedure: k1687 
o|contracted procedure: "(tcp.scm:379) chicken.tcp#recv" 
o|inlining procedure: k1793 
o|inlining procedure: k1829 
o|inlining procedure: k1829 
o|inlining procedure: "(tcp.scm:506) chicken.tcp#last-error-code" 
o|substituted constant variable: a1863 
o|substituted constant variable: a1864 
o|inlining procedure: "(tcp.scm:502) chicken.tcp#interrupted?" 
o|inlining procedure: "(tcp.scm:491) chicken.tcp#retry?" 
o|inlining procedure: k1793 
o|inlining procedure: k1883 
o|inlining procedure: k1883 
o|contracted procedure: "(tcp.scm:489) chicken.tcp#send" 
o|inlining procedure: k1937 
o|inlining procedure: k1937 
o|inlining procedure: "(tcp.scm:532) chicken.tcp#last-error-code" 
o|substituted constant variable: a1972 
o|substituted constant variable: a1973 
o|inlining procedure: "(tcp.scm:531) chicken.tcp#close" 
o|inlining procedure: k2002 
o|inlining procedure: k2007 
o|inlining procedure: k2007 
o|inlining procedure: k2002 
o|inlining procedure: k2027 
o|inlining procedure: k2027 
o|inlining procedure: k2043 
o|inlining procedure: k2043 
o|inlining procedure: k2062 
o|inlining procedure: k2062 
o|inlining procedure: k2084 
o|inlining procedure: k2084 
o|inlining procedure: "(tcp.scm:416) chicken.tcp#last-error-code" 
o|substituted constant variable: a2118 
o|substituted constant variable: a2119 
o|inlining procedure: "(tcp.scm:414) chicken.tcp#check-fd-ready" 
o|inlining procedure: k2123 
o|inlining procedure: k2123 
o|inlining procedure: "(tcp.scm:423) chicken.tcp#last-error-code" 
o|substituted constant variable: a2155 
o|substituted constant variable: a2156 
o|inlining procedure: "(tcp.scm:422) chicken.tcp#close" 
o|inlining procedure: k2176 
o|inlining procedure: k2176 
o|inlining procedure: k2197 
o|inlining procedure: k2197 
o|inlining procedure: k2244 
o|inlining procedure: k2244 
o|inlining procedure: k2259 
o|inlining procedure: k2259 
o|inlining procedure: k2287 
o|inlining procedure: k2287 
o|inlining procedure: k2330 
o|inlining procedure: k2330 
o|inlining procedure: k2375 
o|inlining procedure: k2375 
o|inlining procedure: k2385 
o|inlining procedure: k2385 
o|substituted constant variable: a2391 
o|substituted constant variable: a2413 
o|substituted constant variable: a2414 
o|inlining procedure: "(tcp.scm:365) chicken.tcp#close" 
o|inlining procedure: "(tcp.scm:365) chicken.tcp#last-error-code" 
o|inlining procedure: k2451 
o|inlining procedure: k2451 
o|inlining procedure: "(tcp.scm:567) chicken.tcp#last-error-code" 
o|substituted constant variable: a2488 
o|substituted constant variable: a2489 
o|inlining procedure: "(tcp.scm:564) chicken.tcp#interrupted?" 
o|contracted procedure: "(tcp.scm:561) chicken.tcp#accept" 
o|inlining procedure: k1073 
o|inlining procedure: k1073 
o|inlining procedure: k2519 
o|inlining procedure: "(tcp.scm:574) chicken.tcp#last-error-code" 
o|substituted constant variable: a2547 
o|substituted constant variable: a2548 
o|inlining procedure: k2519 
o|inlining procedure: "(tcp.scm:572) chicken.tcp#check-fd-ready" 
o|inlining procedure: k2605 
o|substituted constant variable: a2635 
o|substituted constant variable: a2636 
o|inlining procedure: "(tcp.scm:618) chicken.tcp#close" 
o|inlining procedure: "(tcp.scm:618) chicken.tcp#last-error-code" 
o|inlining procedure: k2605 
o|substituted constant variable: a2661 
o|substituted constant variable: a2662 
o|inlining procedure: "(tcp.scm:620) chicken.tcp#close" 
o|contracted procedure: "(tcp.scm:616) chicken.tcp#get-socket-error" 
o|inlining procedure: k2666 
o|inlining procedure: k2690 
o|inlining procedure: k2690 
o|substituted constant variable: a2720 
o|substituted constant variable: a2721 
o|inlining procedure: "(tcp.scm:614) chicken.tcp#close" 
o|inlining procedure: "(tcp.scm:614) chicken.tcp#last-error-code" 
o|inlining procedure: "(tcp.scm:611) chicken.tcp#interrupted?" 
o|contracted procedure: "(tcp.scm:606) chicken.tcp#in-progress?" 
o|inlining procedure: k2666 
o|contracted procedure: "(tcp.scm:605) chicken.tcp#connect" 
o|substituted constant variable: a2747 
o|substituted constant variable: a2748 
o|inlining procedure: "(tcp.scm:603) chicken.tcp#close" 
o|inlining procedure: "(tcp.scm:603) chicken.tcp#last-error-code" 
o|substituted constant variable: a2773 
o|substituted constant variable: a2774 
o|inlining procedure: "(tcp.scm:601) chicken.tcp#close" 
o|inlining procedure: "(tcp.scm:601) chicken.tcp#last-error-code" 
o|inlining procedure: "(tcp.scm:600) chicken.tcp#set-socket-options" 
o|inlining procedure: "(tcp.scm:599) chicken.tcp#last-error-code" 
o|substituted constant variable: a2801 
o|substituted constant variable: a2802 
o|inlining procedure: k2812 
o|inlining procedure: k2812 
o|inlining procedure: k2849 
o|inlining procedure: k2849 
o|substituted constant variable: a2865 
o|inlining procedure: k2883 
o|inlining procedure: k2883 
o|inlining procedure: "(tcp.scm:637) chicken.tcp#last-error-code" 
o|substituted constant variable: a2905 
o|substituted constant variable: a2906 
o|contracted procedure: "(tcp.scm:636) chicken.tcp#getpeername" 
o|inlining procedure: "(tcp.scm:635) chicken.tcp#last-error-code" 
o|substituted constant variable: a2926 
o|substituted constant variable: a2927 
o|contracted procedure: "(tcp.scm:634) chicken.tcp#getsockname" 
o|substituted constant variable: a2932 
o|inlining procedure: k2945 
o|inlining procedure: "(tcp.scm:647) chicken.tcp#last-error-code" 
o|substituted constant variable: a2973 
o|substituted constant variable: a2974 
o|inlining procedure: k2945 
o|inlining procedure: "(tcp.scm:645) chicken.tcp#last-error-code" 
o|substituted constant variable: a2997 
o|substituted constant variable: a2998 
o|contracted procedure: "(tcp.scm:643) chicken.tcp#getpeerport" 
o|inlining procedure: "(tcp.scm:642) chicken.tcp#getsockport" 
o|inlining procedure: k3011 
o|inlining procedure: "(tcp.scm:655) chicken.tcp#last-error-code" 
o|substituted constant variable: a3036 
o|substituted constant variable: a3037 
o|inlining procedure: k3011 
o|inlining procedure: "(tcp.scm:653) chicken.tcp#getsockport" 
o|substituted constant variable: a3042 
o|contracted procedure: "(tcp.scm:236) chicken.tcp#startup" 
o|replaced variables: 553 
o|removed binding forms: 145 
o|removed side-effect free assignment to unused variable: chicken.tcp#last-error-code 
o|removed side-effect free assignment to unused variable: chicken.tcp#retry? 
o|removed side-effect free assignment to unused variable: chicken.tcp#interrupted? 
o|removed side-effect free assignment to unused variable: chicken.tcp#close 
o|removed side-effect free assignment to unused variable: chicken.tcp#check-fd-ready 
o|removed side-effect free assignment to unused variable: chicken.tcp#set-socket-options 
o|removed side-effect free assignment to unused variable: chicken.tcp#getsockport 
o|substituted constant variable: r15683136 
o|substituted constant variable: int171178 
o|substituted constant variable: int170177 
o|inlining procedure: k1883 
o|substituted constant variable: flags213221 
o|substituted constant variable: r20033191 
o|substituted constant variable: r20033191 
o|substituted constant variable: r20633197 
o|inlining procedure: k2090 
o|substituted constant variable: r21773218 
o|substituted constant variable: r22603223 
o|substituted constant variable: r23763235 
o|substituted constant variable: r23863237 
o|substituted constant variable: r23863238 
o|substituted constant variable: c-pointer152159 
o|substituted constant variable: c-pointer152159 
o|substituted constant variable: r10743252 
o|substituted constant variable: r10743252 
o|substituted constant variable: c-pointer151158 
o|substituted constant variable: c-pointer151158 
o|inlining procedure: k2605 
o|contracted procedure: "(tcp.scm:592) chicken.tcp#parse-host" 
o|substituted constant variable: r12313096 
o|substituted constant variable: r12313096 
o|replaced variables: 152 
o|removed binding forms: 480 
o|removed conditional forms: 2 
o|inlining procedure: k1525 
o|inlining procedure: k1415 
o|inlining procedure: k1437 
o|inlining procedure: k1443 
o|inlining procedure: k1531 
o|inlining procedure: k1604 
o|inlining procedure: k1610 
o|inlining procedure: k1748 
o|inlining procedure: k1814 
o|inlining procedure: k1860 
o|substituted constant variable: r18843372 
o|inlining procedure: k1969 
o|inlining procedure: k1978 
o|inlining procedure: k2115 
o|inlining procedure: k2152 
o|inlining procedure: k2161 
o|inlining procedure: k2485 
o|contracted procedure: k1069 
o|inlining procedure: k2544 
o|inlining procedure: k2678 
o|inlining procedure: k2723 
o|inlining procedure: k2776 
o|inlining procedure: k2798 
o|inlining procedure: k1336 
o|inlining procedure: k2902 
o|inlining procedure: k2923 
o|inlining procedure: k2970 
o|inlining procedure: k2994 
o|inlining procedure: k3033 
o|inlining procedure: k3079 
o|replaced variables: 1 
o|removed binding forms: 177 
o|contracted procedure: k1501 
o|contracted procedure: k1504 
o|contracted procedure: k1391 
o|contracted procedure: k1394 
o|contracted procedure: k1684 
o|contracted procedure: k1696 
o|contracted procedure: k1726 
o|contracted procedure: k1790 
o|contracted procedure: k1802 
o|contracted procedure: k1832 
o|contracted procedure: k2087 
o|contracted procedure: k2392 
o|contracted procedure: k2395 
o|contracted procedure: k2448 
o|substituted constant variable: r1070 
o|contracted procedure: k2463 
o|contracted procedure: k2516 
o|contracted procedure: k2602 
o|contracted procedure: k2614 
o|contracted procedure: k2617 
o|contracted procedure: k2643 
o|contracted procedure: k2675 
o|contracted procedure: k2693 
o|contracted procedure: k2699 
o|contracted procedure: k2702 
o|contracted procedure: k2726 
o|contracted procedure: k2729 
o|contracted procedure: k2752 
o|contracted procedure: k2755 
o|contracted procedure: k2936 
o|contracted procedure: k2939 
o|contracted procedure: k3008 
o|simplifications: ((let . 8)) 
o|replaced variables: 1 
o|removed binding forms: 60 
o|removed binding forms: 2 
o|simplifications: ((if . 17) (##core#call . 194) (let . 28)) 
o|  call simplifications:
o|    chicken.fixnum#fx*
o|    scheme#vector?
o|    ##sys#check-string
o|    scheme#char=?
o|    scheme#not
o|    scheme#vector
o|    ##sys#call-with-values	2
o|    chicken.fixnum#fx>=	9
o|    ##sys#setslot	4
o|    scheme#values	11
o|    ##sys#size	7
o|    chicken.fixnum#fxmin	3
o|    chicken.fixnum#fx-	6
o|    chicken.fixnum#fx+	12
o|    chicken.fixnum#fx=
o|    scheme#+	5
o|    ##sys#setislot	5
o|    ##sys#check-structure	5
o|    ##sys#slot	14
o|    ##sys#structure?
o|    scheme#car	3
o|    scheme#null?	5
o|    scheme#cdr	2
o|    chicken.fixnum#fx<	7
o|    chicken.fixnum#fx>	6
o|    scheme#eq?	26
o|    ##sys#make-structure	2
o|    ##sys#foreign-block-argument	6
o|    ##sys#foreign-string-argument	3
o|    ##sys#foreign-unsigned-ranged-integer-argument	2
o|    ##sys#foreign-fixnum-argument	41
o|contracted procedure: k1005 
o|contracted procedure: k1021 
o|contracted procedure: k1025 
o|contracted procedure: k1029 
o|contracted procedure: k1115 
o|contracted procedure: k1119 
o|contracted procedure: k1251 
o|contracted procedure: k1259 
o|contracted procedure: k1267 
o|contracted procedure: k1558 
o|contracted procedure: k1468 
o|contracted procedure: k1552 
o|contracted procedure: k1471 
o|contracted procedure: k1546 
o|contracted procedure: k1474 
o|contracted procedure: k1540 
o|contracted procedure: k1477 
o|contracted procedure: k1054 
o|contracted procedure: k1058 
o|contracted procedure: k1498 
o|contracted procedure: k10863108 
o|contracted procedure: k1036 
o|contracted procedure: k1040 
o|contracted procedure: k1044 
o|contracted procedure: k1388 
o|contracted procedure: k10863122 
o|contracted procedure: k11513132 
o|contracted procedure: k1418 
o|contracted procedure: k1446 
o|contracted procedure: k1356 
o|contracted procedure: k1360 
o|contracted procedure: k1528 
o|contracted procedure: k1531 
o|contracted procedure: k1576 
o|contracted procedure: k1579 
o|contracted procedure: k10863145 
o|contracted procedure: k1585 
o|contracted procedure: k1633 
o|contracted procedure: k1661 
o|contracted procedure: k1093 
o|contracted procedure: k1097 
o|contracted procedure: k1101 
o|contracted procedure: k1105 
o|contracted procedure: k1690 
o|contracted procedure: k1714 
o|contracted procedure: k1754 
o|contracted procedure: k1777 
o|contracted procedure: k1787 
o|contracted procedure: k1158 
o|contracted procedure: k1162 
o|contracted procedure: k1166 
o|contracted procedure: k1170 
o|contracted procedure: k1174 
o|contracted procedure: k1796 
o|contracted procedure: k1820 
o|contracted procedure: k1868 
o|contracted procedure: k1875 
o|contracted procedure: k1879 
o|contracted procedure: k1886 
o|contracted procedure: k1883 
o|contracted procedure: k1909 
o|contracted procedure: k1912 
o|contracted procedure: k1915 
o|contracted procedure: k1918 
o|contracted procedure: k1931 
o|contracted procedure: k2038 
o|contracted procedure: k2030 
o|contracted procedure: k2053 
o|contracted procedure: k2046 
o|contracted procedure: k10863186 
o|contracted procedure: k1981 
o|contracted procedure: k1998 
o|contracted procedure: k2018 
o|contracted procedure: k2010 
o|contracted procedure: k2065 
o|contracted procedure: k2069 
o|contracted procedure: k2072 
o|contracted procedure: k2081 
o|contracted procedure: k11443207 
o|contracted procedure: k2096 
o|contracted procedure: k10863216 
o|contracted procedure: k2164 
o|contracted procedure: k2179 
o|contracted procedure: k2182 
o|contracted procedure: k2200 
o|contracted procedure: k2206 
o|contracted procedure: k2209 
o|contracted procedure: k2238 
o|contracted procedure: k2212 
o|contracted procedure: k2216 
o|contracted procedure: k2220 
o|contracted procedure: k2227 
o|contracted procedure: k2231 
o|contracted procedure: k2235 
o|contracted procedure: k2247 
o|contracted procedure: k2262 
o|contracted procedure: k2265 
o|contracted procedure: k2324 
o|contracted procedure: k2278 
o|contracted procedure: k2284 
o|contracted procedure: k2290 
o|contracted procedure: k2300 
o|contracted procedure: k2306 
o|contracted procedure: k2317 
o|contracted procedure: k2313 
o|contracted procedure: k2345 
o|contracted procedure: k2341 
o|contracted procedure: k2334 
o|inlining procedure: k2330 
o|contracted procedure: k2356 
o|contracted procedure: k2360 
o|contracted procedure: k2352 
o|inlining procedure: k2330 
o|contracted procedure: k2366 
o|contracted procedure: k2378 
o|contracted procedure: k2388 
o|contracted procedure: k10863244 
o|contracted procedure: k2418 
o|contracted procedure: k2421 
o|contracted procedure: k1065 
o|contracted procedure: k2491 
o|contracted procedure: k2454 
o|contracted procedure: k2494 
o|contracted procedure: k2513 
o|contracted procedure: k2550 
o|contracted procedure: k11443270 
o|contracted procedure: k2525 
o|contracted procedure: k2837 
o|contracted procedure: k2563 
o|contracted procedure: k2575 
o|contracted procedure: k2557 
o|contracted procedure: k2611 
o|contracted procedure: k10863281 
o|contracted procedure: k2640 
o|contracted procedure: k10863293 
o|contracted procedure: k1126 
o|contracted procedure: k1130 
o|contracted procedure: k1134 
o|contracted procedure: k2669 
o|contracted procedure: k10863302 
o|contracted procedure: k10863311 
o|contracted procedure: k11513325 
o|contracted procedure: k2749 
o|contracted procedure: k10863318 
o|contracted procedure: k2779 
o|contracted procedure: k1276 
o|contracted procedure: k1288 
o|contracted procedure: k1297 
o|contracted procedure: k1317 
o|contracted procedure: k1237 
o|contracted procedure: k1244 
o|contracted procedure: k1342 
o|contracted procedure: k1349 
o|contracted procedure: k2852 
o|contracted procedure: k1213 
o|contracted procedure: k1188 
o|contracted procedure: k11953354 
o|contracted procedure: k1202 
o|contracted procedure: k2951 
o|contracted procedure: k2975 
o|contracted procedure: k3002 
o|contracted procedure: k3005 
o|contracted procedure: k11953365 
o|contracted procedure: k3014 
o|contracted procedure: k3051 
o|contracted procedure: k3057 
o|simplifications: ((if . 1) (let . 42)) 
o|removed binding forms: 164 
o|inlining procedure: k1255 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest428430 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest428430 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest428430 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest428430 0 
o|inlining procedure: "(tcp.scm:329) chicken.tcp#error-code->message" 
o|inlining procedure: "(tcp.scm:317) chicken.tcp#error-code->message" 
o|inlining procedure: "(tcp.scm:315) chicken.tcp#error-code->message" 
o|inlining procedure: "(tcp.scm:310) chicken.tcp#socket" 
o|inlining procedure: "(tcp.scm:340) chicken.tcp#error-code->message" 
o|inlining procedure: "(tcp.scm:395) chicken.tcp#error-code->message" 
o|inlining procedure: "(tcp.scm:506) chicken.tcp#error-code->message" 
o|inlining procedure: "(tcp.scm:532) chicken.tcp#error-code->message" 
o|inlining procedure: "(tcp.scm:530) chicken.tcp#shutdown" 
o|contracted procedure: k1987 
o|inlining procedure: "(tcp.scm:416) chicken.tcp#error-code->message" 
o|inlining procedure: "(tcp.scm:423) chicken.tcp#error-code->message" 
o|inlining procedure: "(tcp.scm:421) chicken.tcp#shutdown" 
o|inlining procedure: k2173 
o|inlining procedure: "(tcp.scm:365) chicken.tcp#error-code->message" 
o|inlining procedure: "(tcp.scm:567) chicken.tcp#error-code->message" 
o|inlining procedure: "(tcp.scm:574) chicken.tcp#error-code->message" 
(o x)|known list op on rest arg sublist: ##core#rest-null? more690 0 
(o x)|known list op on rest arg sublist: ##core#rest-car more690 0 
o|inlining procedure: "(tcp.scm:618) chicken.tcp#error-code->message" 
o|inlining procedure: "(tcp.scm:621) chicken.tcp#error-code->message" 
o|inlining procedure: "(tcp.scm:614) chicken.tcp#error-code->message" 
o|inlining procedure: "(tcp.scm:603) chicken.tcp#error-code->message" 
o|inlining procedure: "(tcp.scm:601) chicken.tcp#error-code->message" 
o|inlining procedure: "(tcp.scm:599) chicken.tcp#error-code->message" 
o|inlining procedure: "(tcp.scm:597) chicken.tcp#socket" 
o|inlining procedure: "(tcp.scm:290) chicken.tcp#error-code->message" 
o|inlining procedure: "(tcp.scm:637) chicken.tcp#error-code->message" 
o|inlining procedure: "(tcp.scm:635) chicken.tcp#error-code->message" 
o|inlining procedure: "(tcp.scm:647) chicken.tcp#error-code->message" 
o|inlining procedure: "(tcp.scm:645) chicken.tcp#error-code->message" 
o|inlining procedure: "(tcp.scm:655) chicken.tcp#error-code->message" 
o|removed binding forms: 2 
o|removed side-effect free assignment to unused variable: chicken.tcp#error-code->message 
o|removed side-effect free assignment to unused variable: chicken.tcp#socket 
o|removed side-effect free assignment to unused variable: chicken.tcp#shutdown 
o|substituted constant variable: r12563512 
(o x)|known list op on rest arg sublist: ##core#rest-null? r1472 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r1472 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r1472 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r1472 1 
o|substituted constant variable: int1251303529 
o|substituted constant variable: int1251303611 
o|replaced variables: 39 
o|removed binding forms: 1 
o|removed binding forms: 47 
o|contracted procedure: k1376 
o|contracted procedure: k2587 
o|removed binding forms: 2 
o|customizable procedures: (check474 chicken.tcp#tcp-port->fileno k2569 loop380 k1311 k2427 chicken.tcp#io-ports k1667 loop557 k2127 k2133 read-input507 k1941 k1944 k1950 output595 k1781 loop598 k1675 chicken.tcp#gethostaddr k1489) 
o|calls to known targets: 84 
o|unused rest argument: rest428430 f_1466 
o|identified direct recursive calls: f_1785 2 
o|identified direct recursive calls: f_2195 1 
o|identified direct recursive calls: f_1283 1 
o|unused rest argument: more690 f_2561 
o|fast box initializations: 7 
o|fast global references: 7 
o|fast global assignments: 3 
o|dropping unused closure argument: f_1248 
o|dropping unused closure argument: f_1622 
o|dropping unused closure argument: f_2844 
*/
/* end of file */
