/* Generated from modules.scm by the CHICKEN compiler
   http://www.call-cc.org
   Version 5.2.0 (rev 317468e4)
   linux-unix-gnu-x86-64 [ 64bit dload ptables ]
   command line: modules.scm -optimize-level 2 -include-path . -include-path ./ -inline -ignore-repository -feature chicken-bootstrap -no-warnings -specialize -consult-types-file ./types.db -explicit-use -no-trace -output-file modules.c
   unit: modules
   uses: chicken-syntax library internal expand
*/
#include "chicken.h"

static C_PTABLE_ENTRY *create_ptable(void);
C_noret_decl(C_chicken_2dsyntax_toplevel)
C_externimport void C_ccall C_chicken_2dsyntax_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_library_toplevel)
C_externimport void C_ccall C_library_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_internal_toplevel)
C_externimport void C_ccall C_internal_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_expand_toplevel)
C_externimport void C_ccall C_expand_toplevel(C_word c,C_word *av) C_noret;

static C_TLS C_word lf[252];
static double C_possibly_force_alignment;
static C_char C_TLS li0[] C_aligned={C_lihdr(0,0,10),40,108,111,111,112,32,108,115,116,41,0,0,0,0,0,0};
static C_char C_TLS li1[] C_aligned={C_lihdr(0,0,19),40,100,101,108,101,116,101,32,120,32,108,115,116,32,116,101,115,116,41,0,0,0,0,0};
static C_char C_TLS li2[] C_aligned={C_lihdr(0,0,15),40,109,111,100,117,108,101,45,110,97,109,101,32,120,41,0};
static C_char C_TLS li3[] C_aligned={C_lihdr(0,0,25),40,109,111,100,117,108,101,45,117,110,100,101,102,105,110,101,100,45,108,105,115,116,32,120,41,0,0,0,0,0,0,0};
static C_char C_TLS li4[] C_aligned={C_lihdr(0,0,32),40,115,101,116,45,109,111,100,117,108,101,45,117,110,100,101,102,105,110,101,100,45,108,105,115,116,33,32,120,32,121,41};
static C_char C_TLS li5[] C_aligned={C_lihdr(0,0,24),40,35,35,115,121,115,35,109,111,100,117,108,101,45,101,120,112,111,114,116,115,32,109,41};
static C_char C_TLS li6[] C_aligned={C_lihdr(0,0,40),40,35,35,115,121,115,35,114,101,103,105,115,116,101,114,45,109,111,100,117,108,101,45,97,108,105,97,115,32,97,108,105,97,115,32,110,97,109,101,41};
static C_char C_TLS li7[] C_aligned={C_lihdr(0,0,7),40,97,53,54,49,51,41,0};
static C_char C_TLS li8[] C_aligned={C_lihdr(0,0,7),40,97,53,54,51,48,41,0};
static C_char C_TLS li9[] C_aligned={C_lihdr(0,0,7),40,97,53,54,51,54,41,0};
static C_char C_TLS li10[] C_aligned={C_lihdr(0,0,18),40,109,97,112,45,108,111,111,112,56,57,57,32,103,57,49,49,41,0,0,0,0,0,0};
static C_char C_TLS li11[] C_aligned={C_lihdr(0,0,42),40,35,35,115,121,115,35,119,105,116,104,45,109,111,100,117,108,101,45,97,108,105,97,115,101,115,32,98,105,110,100,105,110,103,115,32,116,104,117,110,107,41,0,0,0,0,0,0};
static C_char C_TLS li12[] C_aligned={C_lihdr(0,0,8),40,103,57,52,51,32,97,41};
static C_char C_TLS li13[] C_aligned={C_lihdr(0,0,13),40,108,111,111,112,32,110,32,100,111,110,101,41,0,0,0};
static C_char C_TLS li14[] C_aligned={C_lihdr(0,0,36),40,35,35,115,121,115,35,114,101,115,111,108,118,101,45,109,111,100,117,108,101,45,110,97,109,101,32,110,97,109,101,32,108,111,99,41,0,0,0,0};
static C_char C_TLS li15[] C_aligned={C_lihdr(0,0,31),40,35,35,115,121,115,35,102,105,110,100,45,109,111,100,117,108,101,32,110,97,109,101,32,46,32,114,101,115,116,41,0};
static C_char C_TLS li16[] C_aligned={C_lihdr(0,0,8),40,103,57,56,49,32,109,41};
static C_char C_TLS li17[] C_aligned={C_lihdr(0,0,25),40,35,35,115,121,115,35,115,119,105,116,99,104,45,109,111,100,117,108,101,32,109,111,100,41,0,0,0,0,0,0,0};
static C_char C_TLS li18[] C_aligned={C_lihdr(0,0,7),40,103,49,48,49,52,41,0};
static C_char C_TLS li19[] C_aligned={C_lihdr(0,0,6),40,103,57,57,56,41,0,0};
static C_char C_TLS li20[] C_aligned={C_lihdr(0,0,24),40,102,111,114,45,101,97,99,104,45,108,111,111,112,57,57,55,32,103,49,48,48,52,41};
static C_char C_TLS li21[] C_aligned={C_lihdr(0,0,35),40,35,35,115,121,115,35,97,100,100,45,116,111,45,101,120,112,111,114,116,45,108,105,115,116,32,109,111,100,32,101,120,112,115,41,0,0,0,0,0};
static C_char C_TLS li22[] C_aligned={C_lihdr(0,0,54),40,35,35,115,121,115,35,116,111,112,108,101,118,101,108,45,100,101,102,105,110,105,116,105,111,110,45,104,111,111,107,32,115,121,109,32,114,101,110,97,109,101,100,32,101,120,112,111,114,116,101,100,63,41,0,0};
static C_char C_TLS li23[] C_aligned={C_lihdr(0,0,36),40,35,35,115,121,115,35,114,101,103,105,115,116,101,114,45,109,101,116,97,45,101,120,112,114,101,115,115,105,111,110,32,101,120,112,41,0,0,0,0};
static C_char C_TLS li24[] C_aligned={C_lihdr(0,0,30),40,99,104,101,99,107,45,102,111,114,45,114,101,100,101,102,32,115,121,109,32,101,110,118,32,115,101,110,118,41,0,0};
static C_char C_TLS li25[] C_aligned={C_lihdr(0,0,31),40,35,35,115,121,115,35,114,101,103,105,115,116,101,114,45,101,120,112,111,114,116,32,115,121,109,32,109,111,100,41,0};
static C_char C_TLS li26[] C_aligned={C_lihdr(0,0,42),40,35,35,115,121,115,35,114,101,103,105,115,116,101,114,45,115,121,110,116,97,120,45,101,120,112,111,114,116,32,115,121,109,32,109,111,100,32,118,97,108,41,0,0,0,0,0,0};
static C_char C_TLS li27[] C_aligned={C_lihdr(0,0,11),40,97,54,49,56,49,32,120,32,121,41,0,0,0,0,0};
static C_char C_TLS li28[] C_aligned={C_lihdr(0,0,40),40,35,35,115,121,115,35,117,110,114,101,103,105,115,116,101,114,45,115,121,110,116,97,120,45,101,120,112,111,114,116,32,115,121,109,32,109,111,100,41};
static C_char C_TLS li29[] C_aligned={C_lihdr(0,0,47),40,35,35,115,121,115,35,114,101,103,105,115,116,101,114,45,109,111,100,117,108,101,32,110,97,109,101,32,108,105,98,32,101,120,112,108,105,115,116,32,46,32,114,101,115,116,41,0};
static C_char C_TLS li30[] C_aligned={C_lihdr(0,0,9),40,103,49,49,56,54,32,101,41,0,0,0,0,0,0,0};
static C_char C_TLS li31[] C_aligned={C_lihdr(0,0,25),40,102,111,114,45,101,97,99,104,45,108,111,111,112,49,49,56,53,32,103,49,49,57,50,41,0,0,0,0,0,0,0};
static C_char C_TLS li32[] C_aligned={C_lihdr(0,0,11),40,108,112,32,115,101,32,115,101,50,41,0,0,0,0,0};
static C_char C_TLS li33[] C_aligned={C_lihdr(0,0,22),40,108,111,111,112,32,115,101,115,32,108,97,115,116,45,115,101,32,115,101,50,41,0,0};
static C_char C_TLS li34[] C_aligned={C_lihdr(0,0,15),40,109,101,114,103,101,45,115,101,32,115,101,115,42,41,0};
static C_char C_TLS li35[] C_aligned={C_lihdr(0,0,15),40,103,49,50,55,56,32,115,101,120,112,111,114,116,41,0};
static C_char C_TLS li36[] C_aligned={C_lihdr(0,0,9),40,108,111,111,112,32,115,100,41,0,0,0,0,0,0,0};
static C_char C_TLS li37[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,49,50,55,50,32,103,49,50,56,52,41,0,0,0,0};
static C_char C_TLS li38[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,49,50,51,57,32,103,49,50,53,49,41,0,0,0,0};
static C_char C_TLS li39[] C_aligned={C_lihdr(0,0,40),40,35,35,115,121,115,35,99,111,109,112,105,108,101,100,45,109,111,100,117,108,101,45,114,101,103,105,115,116,114,97,116,105,111,110,32,109,111,100,41};
static C_char C_TLS li40[] C_aligned={C_lihdr(0,0,12),40,103,49,52,48,55,32,115,101,120,112,41,0,0,0,0};
static C_char C_TLS li41[] C_aligned={C_lihdr(0,0,12),40,103,49,52,49,55,32,110,101,120,112,41,0,0,0,0};
static C_char C_TLS li42[] C_aligned={C_lihdr(0,0,25),40,102,111,114,45,101,97,99,104,45,108,111,111,112,49,52,49,54,32,103,49,52,50,51,41,0,0,0,0,0,0,0};
static C_char C_TLS li43[] C_aligned={C_lihdr(0,0,25),40,102,111,114,45,101,97,99,104,45,108,111,111,112,49,52,48,54,32,103,49,52,49,51,41,0,0,0,0,0,0,0};
static C_char C_TLS li44[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,49,51,55,52,32,103,49,51,56,54,41,0,0,0,0};
static C_char C_TLS li45[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,49,51,52,54,32,103,49,51,53,56,41,0,0,0,0};
static C_char C_TLS li46[] C_aligned={C_lihdr(0,0,75),40,35,35,115,121,115,35,114,101,103,105,115,116,101,114,45,99,111,109,112,105,108,101,100,45,109,111,100,117,108,101,32,110,97,109,101,32,108,105,98,32,105,101,120,112,111,114,116,115,32,118,101,120,112,111,114,116,115,32,115,101,120,112,111,114,116,115,32,46,32,114,101,115,116,41,0,0,0,0,0};
static C_char C_TLS li47[] C_aligned={C_lihdr(0,0,10),40,103,49,52,55,53,32,115,101,41,0,0,0,0,0,0};
static C_char C_TLS li48[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,49,52,54,57,32,103,49,52,56,49,41,0,0,0,0};
static C_char C_TLS li49[] C_aligned={C_lihdr(0,0,53),40,35,35,115,121,115,35,114,101,103,105,115,116,101,114,45,99,111,114,101,45,109,111,100,117,108,101,32,110,97,109,101,32,108,105,98,32,118,101,120,112,111,114,116,115,32,46,32,114,101,115,116,41,0,0,0};
static C_char C_TLS li50[] C_aligned={C_lihdr(0,0,54),40,35,35,115,121,115,35,114,101,103,105,115,116,101,114,45,112,114,105,109,105,116,105,118,101,45,109,111,100,117,108,101,32,110,97,109,101,32,118,101,120,112,111,114,116,115,32,46,32,114,101,115,116,41,0,0};
static C_char C_TLS li51[] C_aligned={C_lihdr(0,0,6),40,108,111,111,112,41,0,0};
static C_char C_TLS li52[] C_aligned={C_lihdr(0,0,30),40,102,105,110,100,45,101,120,112,111,114,116,32,115,121,109,32,109,111,100,32,105,110,100,105,114,101,99,116,41,0,0};
static C_char C_TLS li53[] C_aligned={C_lihdr(0,0,12),40,102,95,56,51,49,56,32,46,32,95,41,0,0,0,0};
static C_char C_TLS li54[] C_aligned={C_lihdr(0,0,9),40,103,49,54,55,50,32,97,41,0,0,0,0,0,0,0};
static C_char C_TLS li55[] C_aligned={C_lihdr(0,0,25),40,102,111,114,45,101,97,99,104,45,108,111,111,112,49,54,55,49,32,103,49,54,55,56,41,0,0,0,0,0,0,0};
static C_char C_TLS li56[] C_aligned={C_lihdr(0,0,11),40,103,49,54,52,49,32,115,121,109,41,0,0,0,0,0};
static C_char C_TLS li57[] C_aligned={C_lihdr(0,0,25),40,102,111,114,45,101,97,99,104,45,108,111,111,112,49,54,52,48,32,103,49,54,52,55,41,0,0,0,0,0,0,0};
static C_char C_TLS li58[] C_aligned={C_lihdr(0,0,9),40,103,49,54,50,53,32,117,41,0,0,0,0,0,0,0};
static C_char C_TLS li59[] C_aligned={C_lihdr(0,0,9),40,103,49,55,52,49,32,109,41,0,0,0,0,0,0,0};
static C_char C_TLS li60[] C_aligned={C_lihdr(0,0,25),40,102,111,114,45,101,97,99,104,45,108,111,111,112,49,55,52,48,32,103,49,55,52,55,41,0,0,0,0,0,0,0};
static C_char C_TLS li61[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,49,55,48,53,32,103,49,55,49,55,41,0,0,0,0};
static C_char C_TLS li62[] C_aligned={C_lihdr(0,0,13),40,119,97,114,110,32,109,115,103,32,105,100,41,0,0,0};
static C_char C_TLS li63[] C_aligned={C_lihdr(0,0,9),40,103,49,49,52,56,32,97,41,0,0,0,0,0,0,0};
static C_char C_TLS li64[] C_aligned={C_lihdr(0,0,9),40,103,49,49,53,53,32,97,41,0,0,0,0,0,0,0};
static C_char C_TLS li65[] C_aligned={C_lihdr(0,0,16),40,108,111,111,112,50,32,105,101,120,112,111,114,116,115,41};
static C_char C_TLS li66[] C_aligned={C_lihdr(0,0,14),40,108,111,111,112,32,101,120,112,111,114,116,115,41,0,0};
static C_char C_TLS li67[] C_aligned={C_lihdr(0,0,25),40,102,111,114,45,101,97,99,104,45,108,111,111,112,49,54,50,52,32,103,49,54,51,49,41,0,0,0,0,0,0,0};
static C_char C_TLS li68[] C_aligned={C_lihdr(0,0,10),40,102,97,105,108,32,109,115,103,41,0,0,0,0,0,0};
static C_char C_TLS li69[] C_aligned={C_lihdr(0,0,11),40,105,100,45,115,116,114,105,110,103,41,0,0,0,0,0};
static C_char C_TLS li70[] C_aligned={C_lihdr(0,0,12),40,103,49,54,49,54,32,116,121,112,101,41,0,0,0,0};
static C_char C_TLS li71[] C_aligned={C_lihdr(0,0,9),40,108,111,111,112,32,120,108,41,0,0,0,0,0,0,0};
static C_char C_TLS li72[] C_aligned={C_lihdr(0,0,9),40,108,111,111,112,32,109,101,41,0,0,0,0,0,0,0};
static C_char C_TLS li73[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,49,53,53,55,32,103,49,53,54,57,41,0,0,0,0};
static C_char C_TLS li74[] C_aligned={C_lihdr(0,0,34),40,35,35,115,121,115,35,102,105,110,97,108,105,122,101,45,109,111,100,117,108,101,32,109,111,100,32,46,32,114,101,115,116,41,0,0,0,0,0,0};
static C_char C_TLS li75[] C_aligned={C_lihdr(0,0,7),40,97,56,51,51,54,41,0};
static C_char C_TLS li76[] C_aligned={C_lihdr(0,0,7),40,97,56,51,57,50,41,0};
static C_char C_TLS li77[] C_aligned={C_lihdr(0,0,7),40,97,56,51,57,56,41,0};
static C_char C_TLS li78[] C_aligned={C_lihdr(0,0,30),40,35,35,115,121,115,35,119,105,116,104,45,101,110,118,105,114,111,110,109,101,110,116,32,116,104,117,110,107,41,0,0};
static C_char C_TLS li79[] C_aligned={C_lihdr(0,0,7),40,97,56,52,52,55,41,0};
static C_char C_TLS li80[] C_aligned={C_lihdr(0,0,7),40,97,56,52,53,50,41,0};
static C_char C_TLS li81[] C_aligned={C_lihdr(0,0,7),40,97,56,52,54,49,41,0};
static C_char C_TLS li82[] C_aligned={C_lihdr(0,0,7),40,97,56,52,52,49,41,0};
static C_char C_TLS li83[] C_aligned={C_lihdr(0,0,33),40,35,35,115,121,115,35,105,109,112,111,114,116,45,108,105,98,114,97,114,121,45,104,111,111,107,32,109,110,97,109,101,41,0,0,0,0,0,0,0};
static C_char C_TLS li84[] C_aligned={C_lihdr(0,0,36),40,102,105,110,100,45,109,111,100,117,108,101,47,105,109,112,111,114,116,45,108,105,98,114,97,114,121,32,108,105,98,32,108,111,99,41,0,0,0,0};
static C_char C_TLS li85[] C_aligned={C_lihdr(0,0,17),40,119,97,114,110,32,109,115,103,32,109,111,100,32,105,100,41,0,0,0,0,0,0,0};
static C_char C_TLS li86[] C_aligned={C_lihdr(0,0,9),40,116,111,115,116,114,32,120,41,0,0,0,0,0,0,0};
static C_char C_TLS li87[] C_aligned={C_lihdr(0,0,21),40,109,111,100,117,108,101,45,105,109,112,111,114,116,115,32,110,97,109,101,41,0,0,0};
static C_char C_TLS li88[] C_aligned={C_lihdr(0,0,7),40,97,56,54,53,55,41,0};
static C_char C_TLS li89[] C_aligned={C_lihdr(0,0,10),40,103,49,57,50,55,32,105,100,41,0,0,0,0,0,0};
static C_char C_TLS li90[] C_aligned={C_lihdr(0,0,25),40,102,111,114,45,101,97,99,104,45,108,111,111,112,49,57,50,54,32,103,49,57,51,51,41,0,0,0,0,0,0,0};
static C_char C_TLS li91[] C_aligned={C_lihdr(0,0,9),40,103,49,57,52,55,32,97,41,0,0,0,0,0,0,0};
static C_char C_TLS li92[] C_aligned={C_lihdr(0,0,9),40,103,49,57,53,49,32,97,41,0,0,0,0,0,0,0};
static C_char C_TLS li93[] C_aligned={C_lihdr(0,0,22),40,108,111,111,112,32,105,100,115,32,118,32,115,32,109,105,115,115,105,110,103,41,0,0};
static C_char C_TLS li94[] C_aligned={C_lihdr(0,0,60),40,97,56,54,54,55,32,110,97,109,101,49,56,56,56,32,108,105,98,49,56,57,48,32,115,112,101,99,49,56,57,50,32,105,109,112,118,49,56,57,52,32,105,109,112,115,49,56,57,54,32,105,109,112,105,49,56,57,56,41,0,0,0,0};
static C_char C_TLS li95[] C_aligned={C_lihdr(0,0,7),40,97,56,56,48,55,41,0};
static C_char C_TLS li96[] C_aligned={C_lihdr(0,0,10),40,103,50,48,48,53,32,105,100,41,0,0,0,0,0,0};
static C_char C_TLS li97[] C_aligned={C_lihdr(0,0,25),40,102,111,114,45,101,97,99,104,45,108,111,111,112,50,48,48,52,32,103,50,48,49,49,41,0,0,0,0,0,0,0};
static C_char C_TLS li98[] C_aligned={C_lihdr(0,0,10),40,103,50,48,50,53,32,105,100,41,0,0,0,0,0,0};
static C_char C_TLS li99[] C_aligned={C_lihdr(0,0,17),40,108,111,111,112,32,105,109,112,115,32,115,32,105,100,115,41,0,0,0,0,0,0,0};
static C_char C_TLS li100[] C_aligned={C_lihdr(0,0,10),40,103,50,48,51,48,32,105,100,41,0,0,0,0,0,0};
static C_char C_TLS li101[] C_aligned={C_lihdr(0,0,17),40,108,111,111,112,32,105,109,112,118,32,118,32,105,100,115,41,0,0,0,0,0,0,0};
static C_char C_TLS li102[] C_aligned={C_lihdr(0,0,60),40,97,56,56,49,55,32,110,97,109,101,49,57,53,56,32,108,105,98,49,57,54,48,32,115,112,101,99,49,57,54,50,32,105,109,112,118,49,57,54,52,32,105,109,112,115,49,57,54,54,32,105,109,112,105,49,57,54,56,41,0,0,0,0};
static C_char C_TLS li103[] C_aligned={C_lihdr(0,0,7),40,97,56,57,57,48,41,0};
static C_char C_TLS li104[] C_aligned={C_lihdr(0,0,10),40,103,50,48,56,52,32,105,100,41,0,0,0,0,0,0};
static C_char C_TLS li105[] C_aligned={C_lihdr(0,0,25),40,102,111,114,45,101,97,99,104,45,108,111,111,112,50,48,56,51,32,103,50,48,57,48,41,0,0,0,0,0,0,0};
static C_char C_TLS li106[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,48,57,54,32,103,50,49,48,56,41,0,0,0,0};
static C_char C_TLS li107[] C_aligned={C_lihdr(0,0,9),40,103,50,49,51,48,32,97,41,0,0,0,0,0,0,0};
static C_char C_TLS li108[] C_aligned={C_lihdr(0,0,17),40,108,111,111,112,32,105,109,112,115,32,115,32,105,100,115,41,0,0,0,0,0,0,0};
static C_char C_TLS li109[] C_aligned={C_lihdr(0,0,9),40,103,50,49,51,53,32,97,41,0,0,0,0,0,0,0};
static C_char C_TLS li110[] C_aligned={C_lihdr(0,0,17),40,108,111,111,112,32,105,109,112,118,32,118,32,105,100,115,41,0,0,0,0,0,0,0};
static C_char C_TLS li111[] C_aligned={C_lihdr(0,0,60),40,97,57,48,48,48,32,110,97,109,101,50,48,51,55,32,108,105,98,50,48,51,57,32,115,112,101,99,50,48,52,49,32,105,109,112,118,50,48,52,51,32,105,109,112,115,50,48,52,53,32,105,109,112,105,50,48,52,55,41,0,0,0,0};
static C_char C_TLS li112[] C_aligned={C_lihdr(0,0,7),40,97,57,50,52,48,41,0};
static C_char C_TLS li113[] C_aligned={C_lihdr(0,0,12),40,114,101,110,97,109,101,32,105,109,112,41,0,0,0,0};
static C_char C_TLS li114[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,50,48,49,32,103,50,50,49,51,41,0,0,0,0};
static C_char C_TLS li115[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,49,55,53,32,103,50,49,56,55,41,0,0,0,0};
static C_char C_TLS li116[] C_aligned={C_lihdr(0,0,60),40,97,57,50,53,48,32,110,97,109,101,50,49,52,50,32,108,105,98,50,49,52,52,32,115,112,101,99,50,49,52,54,32,105,109,112,118,50,49,52,56,32,105,109,112,115,50,49,53,48,32,105,109,112,105,50,49,53,50,41,0,0,0,0};
static C_char C_TLS li117[] C_aligned={C_lihdr(0,0,8),40,108,111,111,112,32,120,41};
static C_char C_TLS li118[] C_aligned={C_lihdr(0,0,9),40,97,56,53,54,54,32,107,41,0,0,0,0,0,0,0};
static C_char C_TLS li119[] C_aligned={C_lihdr(0,0,34),40,35,35,115,121,115,35,100,101,99,111,109,112,111,115,101,45,105,109,112,111,114,116,32,120,32,114,32,99,32,108,111,99,41,0,0,0,0,0,0};
static C_char C_TLS li120[] C_aligned={C_lihdr(0,0,7),40,97,57,52,48,53,41,0};
static C_char C_TLS li121[] C_aligned={C_lihdr(0,0,49),40,97,57,52,49,49,32,110,97,109,101,50,50,53,51,32,95,50,50,53,53,32,115,112,101,99,50,50,53,55,32,118,50,50,53,57,32,115,50,50,54,49,32,105,50,50,54,51,41,0,0,0,0,0,0,0};
static C_char C_TLS li122[] C_aligned={C_lihdr(0,0,9),40,103,50,50,52,51,32,120,41,0,0,0,0,0,0,0};
static C_char C_TLS li123[] C_aligned={C_lihdr(0,0,25),40,102,111,114,45,101,97,99,104,45,108,111,111,112,50,50,52,50,32,103,50,50,52,57,41,0,0,0,0,0,0,0};
static C_char C_TLS li124[] C_aligned={C_lihdr(0,0,65),40,35,35,115,121,115,35,101,120,112,97,110,100,45,105,109,112,111,114,116,32,120,32,114,32,99,32,105,109,112,111,114,116,45,101,110,118,32,109,97,99,114,111,45,101,110,118,32,109,101,116,97,63,32,114,101,101,120,112,63,32,108,111,99,41,0,0,0,0,0,0,0};
static C_char C_TLS li125[] C_aligned={C_lihdr(0,0,11),40,103,50,51,48,52,32,105,109,112,41,0,0,0,0,0};
static C_char C_TLS li126[] C_aligned={C_lihdr(0,0,11),40,103,50,51,49,52,32,105,109,112,41,0,0,0,0,0};
static C_char C_TLS li127[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,51,56,50,32,103,50,51,57,52,41,0,0,0,0};
static C_char C_TLS li128[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,51,53,54,32,103,50,51,54,56,41,0,0,0,0};
static C_char C_TLS li129[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,52,51,54,32,103,50,52,52,56,41,0,0,0,0};
static C_char C_TLS li130[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,52,49,48,32,103,50,52,50,50,41,0,0,0,0};
static C_char C_TLS li131[] C_aligned={C_lihdr(0,0,25),40,102,111,114,45,101,97,99,104,45,108,111,111,112,50,51,49,51,32,103,50,51,50,48,41,0,0,0,0,0,0,0};
static C_char C_TLS li132[] C_aligned={C_lihdr(0,0,25),40,102,111,114,45,101,97,99,104,45,108,111,111,112,50,51,48,51,32,103,50,51,49,48,41,0,0,0,0,0,0,0};
static C_char C_TLS li133[] C_aligned={C_lihdr(0,0,69),40,35,35,115,121,115,35,105,109,112,111,114,116,32,115,112,101,99,32,118,115,118,32,118,115,115,32,118,115,105,32,105,109,112,111,114,116,45,101,110,118,32,109,97,99,114,111,45,101,110,118,32,109,101,116,97,63,32,114,101,101,120,112,63,32,108,111,99,41,0,0,0};
static C_char C_TLS li134[] C_aligned={C_lihdr(0,0,26),40,109,111,100,117,108,101,45,114,101,110,97,109,101,32,115,121,109,32,112,114,101,102,105,120,41,0,0,0,0,0,0};
static C_char C_TLS li135[] C_aligned={C_lihdr(0,0,9),40,103,49,48,57,48,32,97,41,0,0,0,0,0,0,0};
static C_char C_TLS li136[] C_aligned={C_lihdr(0,0,11),40,103,50,52,57,51,32,109,111,100,41,0,0,0,0,0};
static C_char C_TLS li137[] C_aligned={C_lihdr(0,0,13),40,109,114,101,110,97,109,101,32,115,121,109,41,0,0,0};
static C_char C_TLS li138[] C_aligned={C_lihdr(0,0,9),40,103,50,53,48,52,32,97,41,0,0,0,0,0,0,0};
static C_char C_TLS li139[] C_aligned={C_lihdr(0,0,42),40,35,35,115,121,115,35,97,108,105,97,115,45,103,108,111,98,97,108,45,104,111,111,107,32,115,121,109,32,97,115,115,105,103,110,32,119,104,101,114,101,41,0,0,0,0,0,0};
static C_char C_TLS li140[] C_aligned={C_lihdr(0,0,10),40,101,114,114,32,97,114,103,115,41,0,0,0,0,0,0};
static C_char C_TLS li141[] C_aligned={C_lihdr(0,0,12),40,105,102,97,99,101,32,110,97,109,101,41,0,0,0,0};
static C_char C_TLS li142[] C_aligned={C_lihdr(0,0,11),40,108,111,111,112,50,32,108,115,116,41,0,0,0,0,0};
static C_char C_TLS li143[] C_aligned={C_lihdr(0,0,10),40,108,111,111,112,32,120,112,115,41,0,0,0,0,0,0};
static C_char C_TLS li144[] C_aligned={C_lihdr(0,0,33),40,35,35,115,121,115,35,118,97,108,105,100,97,116,101,45,101,120,112,111,114,116,115,32,101,120,112,115,32,108,111,99,41,0,0,0,0,0,0,0};
static C_char C_TLS li145[] C_aligned={C_lihdr(0,0,46),40,35,35,115,121,115,35,114,101,103,105,115,116,101,114,45,102,117,110,99,116,111,114,32,110,97,109,101,32,102,97,114,103,115,32,102,101,120,112,115,32,98,111,100,121,41,0,0};
static C_char C_TLS li146[] C_aligned={C_lihdr(0,0,10),40,101,114,114,32,97,114,103,115,41,0,0,0,0,0,0};
static C_char C_TLS li147[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,53,56,50,32,103,50,53,57,52,41,0,0,0,0};
static C_char C_TLS li148[] C_aligned={C_lihdr(0,0,6),40,109,101,114,114,41,0,0};
static C_char C_TLS li149[] C_aligned={C_lihdr(0,0,11),40,108,111,111,112,50,32,102,97,115,41,0,0,0,0,0};
static C_char C_TLS li150[] C_aligned={C_lihdr(0,0,13),40,108,111,111,112,32,97,115,32,102,97,115,41,0,0,0};
static C_char C_TLS li151[] C_aligned={C_lihdr(0,0,43),40,35,35,115,121,115,35,105,110,115,116,97,110,116,105,97,116,101,45,102,117,110,99,116,111,114,32,110,97,109,101,32,102,110,97,109,101,32,97,114,103,115,41,0,0,0,0,0};
static C_char C_TLS li152[] C_aligned={C_lihdr(0,0,11),40,103,50,54,52,51,32,101,120,112,41,0,0,0,0,0};
static C_char C_TLS li153[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,54,54,52,32,103,50,54,55,54,41,0,0,0,0};
static C_char C_TLS li154[] C_aligned={C_lihdr(0,0,25),40,102,111,114,45,101,97,99,104,45,108,111,111,112,50,54,52,50,32,103,50,54,52,57,41,0,0,0,0,0,0,0};
static C_char C_TLS li155[] C_aligned={C_lihdr(0,0,46),40,109,97,116,99,104,45,102,117,110,99,116,111,114,45,97,114,103,117,109,101,110,116,32,110,97,109,101,32,109,110,97,109,101,32,101,120,112,115,32,102,110,97,109,101,41,0,0};
static C_char C_TLS li156[] C_aligned={C_lihdr(0,0,48),40,99,104,105,99,107,101,110,46,109,111,100,117,108,101,35,109,111,100,117,108,101,45,101,110,118,105,114,111,110,109,101,110,116,32,109,110,97,109,101,32,46,32,114,101,115,116,41};
static C_char C_TLS li157[] C_aligned={C_lihdr(0,0,7),40,103,51,49,54,51,41,0};
static C_char C_TLS li158[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,51,49,53,55,32,103,51,49,54,57,41,0,0,0,0};
static C_char C_TLS li159[] C_aligned={C_lihdr(0,0,7),40,103,51,49,50,53,41,0};
static C_char C_TLS li160[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,51,49,49,57,32,103,51,49,51,49,41,0,0,0,0};
static C_char C_TLS li161[] C_aligned={C_lihdr(0,0,7),40,103,51,48,56,55,41,0};
static C_char C_TLS li162[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,51,48,56,49,32,103,51,48,57,51,41,0,0,0,0};
static C_char C_TLS li163[] C_aligned={C_lihdr(0,0,7),40,103,51,48,52,57,41,0};
static C_char C_TLS li164[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,51,48,52,51,32,103,51,48,53,53,41,0,0,0,0};
static C_char C_TLS li165[] C_aligned={C_lihdr(0,0,7),40,103,51,48,49,49,41,0};
static C_char C_TLS li166[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,51,48,48,53,32,103,51,48,49,55,41,0,0,0,0};
static C_char C_TLS li167[] C_aligned={C_lihdr(0,0,7),40,103,50,57,55,51,41,0};
static C_char C_TLS li168[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,57,54,55,32,103,50,57,55,57,41,0,0,0,0};
static C_char C_TLS li169[] C_aligned={C_lihdr(0,0,7),40,103,50,57,51,53,41,0};
static C_char C_TLS li170[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,57,50,57,32,103,50,57,52,49,41,0,0,0,0};
static C_char C_TLS li171[] C_aligned={C_lihdr(0,0,7),40,103,50,56,57,55,41,0};
static C_char C_TLS li172[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,56,57,49,32,103,50,57,48,51,41,0,0,0,0};
static C_char C_TLS li173[] C_aligned={C_lihdr(0,0,7),40,103,50,56,53,57,41,0};
static C_char C_TLS li174[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,56,53,51,32,103,50,56,54,53,41,0,0,0,0};
static C_char C_TLS li175[] C_aligned={C_lihdr(0,0,7),40,103,50,56,50,49,41,0};
static C_char C_TLS li176[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,56,49,53,32,103,50,56,50,55,41,0,0,0,0};
static C_char C_TLS li177[] C_aligned={C_lihdr(0,0,7),40,103,50,55,56,51,41,0};
static C_char C_TLS li178[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,55,55,55,32,103,50,55,56,57,41,0,0,0,0};
static C_char C_TLS li179[] C_aligned={C_lihdr(0,0,7),40,103,50,55,52,53,41,0};
static C_char C_TLS li180[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,55,51,57,32,103,50,55,53,49,41,0,0,0,0};
static C_char C_TLS li181[] C_aligned={C_lihdr(0,0,7),40,103,50,55,48,55,41,0};
static C_char C_TLS li182[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,55,48,49,32,103,50,55,49,51,41,0,0,0,0};
static C_char C_TLS li183[] C_aligned={C_lihdr(0,0,10),40,116,111,112,108,101,118,101,108,41,0,0,0,0,0,0};


C_noret_decl(f12022)
static void C_ccall f12022(C_word c,C_word *av) C_noret;
C_noret_decl(f12847)
static void C_ccall f12847(C_word c,C_word *av) C_noret;
C_noret_decl(f12851)
static void C_ccall f12851(C_word c,C_word *av) C_noret;
C_noret_decl(f_10002)
static void C_fcall f_10002(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10008)
static void C_fcall f_10008(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10049)
static void C_fcall f_10049(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10081)
static void C_ccall f_10081(C_word c,C_word *av) C_noret;
C_noret_decl(f_10107)
static void C_ccall f_10107(C_word c,C_word *av) C_noret;
C_noret_decl(f_10128)
static void C_ccall f_10128(C_word c,C_word *av) C_noret;
C_noret_decl(f_10132)
static void C_ccall f_10132(C_word c,C_word *av) C_noret;
C_noret_decl(f_10158)
static void C_fcall f_10158(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10172)
static void C_ccall f_10172(C_word c,C_word *av) C_noret;
C_noret_decl(f_10214)
static void C_ccall f_10214(C_word c,C_word *av) C_noret;
C_noret_decl(f_10230)
static void C_ccall f_10230(C_word c,C_word *av) C_noret;
C_noret_decl(f_10238)
static void C_fcall f_10238(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10245)
static void C_ccall f_10245(C_word c,C_word *av) C_noret;
C_noret_decl(f_10255)
static void C_fcall f_10255(C_word t0,C_word t1) C_noret;
C_noret_decl(f_10277)
static void C_ccall f_10277(C_word c,C_word *av) C_noret;
C_noret_decl(f_10279)
static void C_fcall f_10279(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10318)
static void C_ccall f_10318(C_word c,C_word *av) C_noret;
C_noret_decl(f_10339)
static void C_fcall f_10339(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_10351)
static void C_fcall f_10351(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10374)
static void C_ccall f_10374(C_word c,C_word *av) C_noret;
C_noret_decl(f_10377)
static void C_ccall f_10377(C_word c,C_word *av) C_noret;
C_noret_decl(f_10388)
static void C_ccall f_10388(C_word c,C_word *av) C_noret;
C_noret_decl(f_10394)
static void C_ccall f_10394(C_word c,C_word *av) C_noret;
C_noret_decl(f_10426)
static void C_ccall f_10426(C_word c,C_word *av) C_noret;
C_noret_decl(f_10429)
static void C_ccall f_10429(C_word c,C_word *av) C_noret;
C_noret_decl(f_10440)
static void C_ccall f_10440(C_word c,C_word *av) C_noret;
C_noret_decl(f_10456)
static void C_fcall f_10456(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_10460)
static void C_ccall f_10460(C_word c,C_word *av) C_noret;
C_noret_decl(f_10467)
static void C_fcall f_10467(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10480)
static void C_fcall f_10480(C_word t0,C_word t1) C_noret;
C_noret_decl(f_10507)
static void C_ccall f_10507(C_word c,C_word *av) C_noret;
C_noret_decl(f_10520)
static void C_ccall f_10520(C_word c,C_word *av) C_noret;
C_noret_decl(f_10524)
static void C_ccall f_10524(C_word c,C_word *av) C_noret;
C_noret_decl(f_10528)
static void C_ccall f_10528(C_word c,C_word *av) C_noret;
C_noret_decl(f_10532)
static void C_ccall f_10532(C_word c,C_word *av) C_noret;
C_noret_decl(f_10546)
static void C_ccall f_10546(C_word c,C_word *av) C_noret;
C_noret_decl(f_10552)
static void C_ccall f_10552(C_word c,C_word *av) C_noret;
C_noret_decl(f_10554)
static void C_fcall f_10554(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10579)
static void C_ccall f_10579(C_word c,C_word *av) C_noret;
C_noret_decl(f_10588)
static void C_fcall f_10588(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10598)
static void C_ccall f_10598(C_word c,C_word *av) C_noret;
C_noret_decl(f_10613)
static void C_ccall f_10613(C_word c,C_word *av) C_noret;
C_noret_decl(f_10616)
static void C_ccall f_10616(C_word c,C_word *av) C_noret;
C_noret_decl(f_10619)
static void C_ccall f_10619(C_word c,C_word *av) C_noret;
C_noret_decl(f_10622)
static void C_ccall f_10622(C_word c,C_word *av) C_noret;
C_noret_decl(f_10625)
static void C_ccall f_10625(C_word c,C_word *av) C_noret;
C_noret_decl(f_10628)
static void C_ccall f_10628(C_word c,C_word *av) C_noret;
C_noret_decl(f_10631)
static void C_ccall f_10631(C_word c,C_word *av) C_noret;
C_noret_decl(f_10634)
static void C_ccall f_10634(C_word c,C_word *av) C_noret;
C_noret_decl(f_10637)
static void C_ccall f_10637(C_word c,C_word *av) C_noret;
C_noret_decl(f_10640)
static void C_ccall f_10640(C_word c,C_word *av) C_noret;
C_noret_decl(f_10643)
static void C_ccall f_10643(C_word c,C_word *av) C_noret;
C_noret_decl(f_10646)
static void C_ccall f_10646(C_word c,C_word *av) C_noret;
C_noret_decl(f_10649)
static void C_ccall f_10649(C_word c,C_word *av) C_noret;
C_noret_decl(f_10652)
static void C_ccall f_10652(C_word c,C_word *av) C_noret;
C_noret_decl(f_10655)
static void C_ccall f_10655(C_word c,C_word *av) C_noret;
C_noret_decl(f_10658)
static void C_ccall f_10658(C_word c,C_word *av) C_noret;
C_noret_decl(f_10661)
static void C_ccall f_10661(C_word c,C_word *av) C_noret;
C_noret_decl(f_10664)
static void C_ccall f_10664(C_word c,C_word *av) C_noret;
C_noret_decl(f_10667)
static void C_ccall f_10667(C_word c,C_word *av) C_noret;
C_noret_decl(f_10670)
static void C_ccall f_10670(C_word c,C_word *av) C_noret;
C_noret_decl(f_10673)
static void C_ccall f_10673(C_word c,C_word *av) C_noret;
C_noret_decl(f_10676)
static void C_ccall f_10676(C_word c,C_word *av) C_noret;
C_noret_decl(f_10679)
static void C_ccall f_10679(C_word c,C_word *av) C_noret;
C_noret_decl(f_10682)
static void C_ccall f_10682(C_word c,C_word *av) C_noret;
C_noret_decl(f_10685)
static void C_ccall f_10685(C_word c,C_word *av) C_noret;
C_noret_decl(f_10688)
static void C_ccall f_10688(C_word c,C_word *av) C_noret;
C_noret_decl(f_10691)
static void C_ccall f_10691(C_word c,C_word *av) C_noret;
C_noret_decl(f_10694)
static void C_ccall f_10694(C_word c,C_word *av) C_noret;
C_noret_decl(f_10697)
static void C_ccall f_10697(C_word c,C_word *av) C_noret;
C_noret_decl(f_10699)
static void C_ccall f_10699(C_word c,C_word *av) C_noret;
C_noret_decl(f_10706)
static void C_ccall f_10706(C_word c,C_word *av) C_noret;
C_noret_decl(f_10735)
static void C_ccall f_10735(C_word c,C_word *av) C_noret;
C_noret_decl(f_10738)
static void C_ccall f_10738(C_word c,C_word *av) C_noret;
C_noret_decl(f_10745)
static C_word C_fcall f_10745(C_word t0,C_word t1);
C_noret_decl(f_10757)
static void C_fcall f_10757(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10792)
static void C_ccall f_10792(C_word c,C_word *av) C_noret;
C_noret_decl(f_10799)
static C_word C_fcall f_10799(C_word t0,C_word t1);
C_noret_decl(f_10811)
static void C_fcall f_10811(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10846)
static void C_ccall f_10846(C_word c,C_word *av) C_noret;
C_noret_decl(f_10853)
static C_word C_fcall f_10853(C_word t0,C_word t1);
C_noret_decl(f_10865)
static void C_fcall f_10865(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10900)
static void C_ccall f_10900(C_word c,C_word *av) C_noret;
C_noret_decl(f_10907)
static C_word C_fcall f_10907(C_word t0,C_word t1);
C_noret_decl(f_10919)
static void C_fcall f_10919(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10954)
static void C_ccall f_10954(C_word c,C_word *av) C_noret;
C_noret_decl(f_10961)
static C_word C_fcall f_10961(C_word t0,C_word t1);
C_noret_decl(f_10973)
static void C_fcall f_10973(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11008)
static void C_ccall f_11008(C_word c,C_word *av) C_noret;
C_noret_decl(f_11015)
static C_word C_fcall f_11015(C_word t0,C_word t1);
C_noret_decl(f_11027)
static void C_fcall f_11027(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11062)
static void C_ccall f_11062(C_word c,C_word *av) C_noret;
C_noret_decl(f_11069)
static C_word C_fcall f_11069(C_word t0,C_word t1);
C_noret_decl(f_11081)
static void C_fcall f_11081(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11116)
static void C_ccall f_11116(C_word c,C_word *av) C_noret;
C_noret_decl(f_11123)
static C_word C_fcall f_11123(C_word t0,C_word t1);
C_noret_decl(f_11135)
static void C_fcall f_11135(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11170)
static void C_ccall f_11170(C_word c,C_word *av) C_noret;
C_noret_decl(f_11177)
static C_word C_fcall f_11177(C_word t0,C_word t1);
C_noret_decl(f_11189)
static void C_fcall f_11189(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11224)
static void C_ccall f_11224(C_word c,C_word *av) C_noret;
C_noret_decl(f_11231)
static C_word C_fcall f_11231(C_word t0,C_word t1);
C_noret_decl(f_11243)
static void C_fcall f_11243(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11278)
static void C_ccall f_11278(C_word c,C_word *av) C_noret;
C_noret_decl(f_11285)
static C_word C_fcall f_11285(C_word t0,C_word t1);
C_noret_decl(f_11297)
static void C_fcall f_11297(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11332)
static void C_ccall f_11332(C_word c,C_word *av) C_noret;
C_noret_decl(f_11339)
static C_word C_fcall f_11339(C_word t0,C_word t1);
C_noret_decl(f_11351)
static void C_fcall f_11351(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11386)
static void C_ccall f_11386(C_word c,C_word *av) C_noret;
C_noret_decl(f_11393)
static C_word C_fcall f_11393(C_word t0,C_word t1);
C_noret_decl(f_11405)
static void C_fcall f_11405(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11440)
static void C_ccall f_11440(C_word c,C_word *av) C_noret;
C_noret_decl(f_11444)
static void C_ccall f_11444(C_word c,C_word *av) C_noret;
C_noret_decl(f_11448)
static void C_ccall f_11448(C_word c,C_word *av) C_noret;
C_noret_decl(f_4018)
static void C_ccall f_4018(C_word c,C_word *av) C_noret;
C_noret_decl(f_4021)
static void C_ccall f_4021(C_word c,C_word *av) C_noret;
C_noret_decl(f_4024)
static void C_ccall f_4024(C_word c,C_word *av) C_noret;
C_noret_decl(f_4027)
static void C_ccall f_4027(C_word c,C_word *av) C_noret;
C_noret_decl(f_4518)
static void C_fcall f_4518(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_4524)
static void C_fcall f_4524(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4537)
static void C_ccall f_4537(C_word c,C_word *av) C_noret;
C_noret_decl(f_4551)
static void C_ccall f_4551(C_word c,C_word *av) C_noret;
C_noret_decl(f_5309)
static void C_ccall f_5309(C_word c,C_word *av) C_noret;
C_noret_decl(f_5313)
static void C_ccall f_5313(C_word c,C_word *av) C_noret;
C_noret_decl(f_5328)
static void C_ccall f_5328(C_word c,C_word *av) C_noret;
C_noret_decl(f_5418)
static void C_ccall f_5418(C_word c,C_word *av) C_noret;
C_noret_decl(f_5427)
static void C_ccall f_5427(C_word c,C_word *av) C_noret;
C_noret_decl(f_5563)
static void C_ccall f_5563(C_word c,C_word *av) C_noret;
C_noret_decl(f_5587)
static void C_ccall f_5587(C_word c,C_word *av) C_noret;
C_noret_decl(f_5603)
static void C_ccall f_5603(C_word c,C_word *av) C_noret;
C_noret_decl(f_5605)
static void C_ccall f_5605(C_word c,C_word *av) C_noret;
C_noret_decl(f_5609)
static void C_ccall f_5609(C_word c,C_word *av) C_noret;
C_noret_decl(f_5614)
static void C_ccall f_5614(C_word c,C_word *av) C_noret;
C_noret_decl(f_5618)
static void C_ccall f_5618(C_word c,C_word *av) C_noret;
C_noret_decl(f_5622)
static void C_ccall f_5622(C_word c,C_word *av) C_noret;
C_noret_decl(f_5625)
static void C_ccall f_5625(C_word c,C_word *av) C_noret;
C_noret_decl(f_5631)
static void C_ccall f_5631(C_word c,C_word *av) C_noret;
C_noret_decl(f_5637)
static void C_ccall f_5637(C_word c,C_word *av) C_noret;
C_noret_decl(f_5641)
static void C_ccall f_5641(C_word c,C_word *av) C_noret;
C_noret_decl(f_5644)
static void C_ccall f_5644(C_word c,C_word *av) C_noret;
C_noret_decl(f_5668)
static void C_ccall f_5668(C_word c,C_word *av) C_noret;
C_noret_decl(f_5672)
static void C_ccall f_5672(C_word c,C_word *av) C_noret;
C_noret_decl(f_5674)
static void C_fcall f_5674(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5708)
static void C_ccall f_5708(C_word c,C_word *av) C_noret;
C_noret_decl(f_5716)
static void C_ccall f_5716(C_word c,C_word *av) C_noret;
C_noret_decl(f_5718)
static void C_fcall f_5718(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_5726)
static void C_fcall f_5726(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5753)
static void C_ccall f_5753(C_word c,C_word *av) C_noret;
C_noret_decl(f_5755)
static void C_ccall f_5755(C_word c,C_word *av) C_noret;
C_noret_decl(f_5809)
static void C_ccall f_5809(C_word c,C_word *av) C_noret;
C_noret_decl(f_5816)
static void C_ccall f_5816(C_word c,C_word *av) C_noret;
C_noret_decl(f_5819)
static void C_ccall f_5819(C_word c,C_word *av) C_noret;
C_noret_decl(f_5822)
static void C_fcall f_5822(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5825)
static void C_ccall f_5825(C_word c,C_word *av) C_noret;
C_noret_decl(f_5831)
static void C_ccall f_5831(C_word c,C_word *av) C_noret;
C_noret_decl(f_5844)
static void C_fcall f_5844(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5856)
static void C_ccall f_5856(C_word c,C_word *av) C_noret;
C_noret_decl(f_5860)
static void C_ccall f_5860(C_word c,C_word *av) C_noret;
C_noret_decl(f_5862)
static void C_ccall f_5862(C_word c,C_word *av) C_noret;
C_noret_decl(f_5878)
static void C_ccall f_5878(C_word c,C_word *av) C_noret;
C_noret_decl(f_5879)
static C_word C_fcall f_5879(C_word *a,C_word t0,C_word t1);
C_noret_decl(f_5887)
static C_word C_fcall f_5887(C_word *a,C_word t0,C_word t1);
C_noret_decl(f_5901)
static void C_ccall f_5901(C_word c,C_word *av) C_noret;
C_noret_decl(f_5904)
static void C_ccall f_5904(C_word c,C_word *av) C_noret;
C_noret_decl(f_5911)
static void C_ccall f_5911(C_word c,C_word *av) C_noret;
C_noret_decl(f_5915)
static void C_ccall f_5915(C_word c,C_word *av) C_noret;
C_noret_decl(f_5921)
static void C_fcall f_5921(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5949)
static void C_ccall f_5949(C_word c,C_word *av) C_noret;
C_noret_decl(f_5951)
static void C_ccall f_5951(C_word c,C_word *av) C_noret;
C_noret_decl(f_5954)
static void C_ccall f_5954(C_word c,C_word *av) C_noret;
C_noret_decl(f_5958)
static void C_ccall f_5958(C_word c,C_word *av) C_noret;
C_noret_decl(f_5974)
static void C_fcall f_5974(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_5981)
static void C_ccall f_5981(C_word c,C_word *av) C_noret;
C_noret_decl(f_5995)
static void C_ccall f_5995(C_word c,C_word *av) C_noret;
C_noret_decl(f_6005)
static void C_ccall f_6005(C_word c,C_word *av) C_noret;
C_noret_decl(f_6008)
static void C_ccall f_6008(C_word c,C_word *av) C_noret;
C_noret_decl(f_6011)
static void C_ccall f_6011(C_word c,C_word *av) C_noret;
C_noret_decl(f_6017)
static void C_ccall f_6017(C_word c,C_word *av) C_noret;
C_noret_decl(f_6020)
static void C_ccall f_6020(C_word c,C_word *av) C_noret;
C_noret_decl(f_6023)
static void C_ccall f_6023(C_word c,C_word *av) C_noret;
C_noret_decl(f_6056)
static void C_ccall f_6056(C_word c,C_word *av) C_noret;
C_noret_decl(f_6060)
static void C_ccall f_6060(C_word c,C_word *av) C_noret;
C_noret_decl(f_6067)
static void C_ccall f_6067(C_word c,C_word *av) C_noret;
C_noret_decl(f_6071)
static void C_ccall f_6071(C_word c,C_word *av) C_noret;
C_noret_decl(f_6084)
static void C_ccall f_6084(C_word c,C_word *av) C_noret;
C_noret_decl(f_6094)
static void C_ccall f_6094(C_word c,C_word *av) C_noret;
C_noret_decl(f_6097)
static void C_ccall f_6097(C_word c,C_word *av) C_noret;
C_noret_decl(f_6103)
static void C_ccall f_6103(C_word c,C_word *av) C_noret;
C_noret_decl(f_6106)
static void C_ccall f_6106(C_word c,C_word *av) C_noret;
C_noret_decl(f_6112)
static void C_ccall f_6112(C_word c,C_word *av) C_noret;
C_noret_decl(f_6146)
static void C_ccall f_6146(C_word c,C_word *av) C_noret;
C_noret_decl(f_6150)
static void C_ccall f_6150(C_word c,C_word *av) C_noret;
C_noret_decl(f_6165)
static void C_ccall f_6165(C_word c,C_word *av) C_noret;
C_noret_decl(f_6176)
static void C_ccall f_6176(C_word c,C_word *av) C_noret;
C_noret_decl(f_6182)
static void C_ccall f_6182(C_word c,C_word *av) C_noret;
C_noret_decl(f_6199)
static void C_ccall f_6199(C_word c,C_word *av) C_noret;
C_noret_decl(f_6206)
static void C_fcall f_6206(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6213)
static void C_fcall f_6213(C_word t0,C_word t1) C_noret;
C_noret_decl(f_6258)
static void C_ccall f_6258(C_word c,C_word *av) C_noret;
C_noret_decl(f_6364)
static void C_fcall f_6364(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_6372)
static void C_ccall f_6372(C_word c,C_word *av) C_noret;
C_noret_decl(f_6376)
static void C_ccall f_6376(C_word c,C_word *av) C_noret;
C_noret_decl(f_6387)
static void C_fcall f_6387(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6414)
static void C_fcall f_6414(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6437)
static void C_ccall f_6437(C_word c,C_word *av) C_noret;
C_noret_decl(f_6451)
static void C_fcall f_6451(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6476)
static void C_ccall f_6476(C_word c,C_word *av) C_noret;
C_noret_decl(f_6491)
static void C_fcall f_6491(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6509)
static void C_ccall f_6509(C_word c,C_word *av) C_noret;
C_noret_decl(f_6520)
static void C_ccall f_6520(C_word c,C_word *av) C_noret;
C_noret_decl(f_6539)
static void C_ccall f_6539(C_word c,C_word *av) C_noret;
C_noret_decl(f_6552)
static void C_ccall f_6552(C_word c,C_word *av) C_noret;
C_noret_decl(f_6562)
static void C_ccall f_6562(C_word c,C_word *av) C_noret;
C_noret_decl(f_6568)
static void C_fcall f_6568(C_word t0,C_word t1) C_noret;
C_noret_decl(f_6572)
static void C_ccall f_6572(C_word c,C_word *av) C_noret;
C_noret_decl(f_6575)
static void C_ccall f_6575(C_word c,C_word *av) C_noret;
C_noret_decl(f_6586)
static void C_fcall f_6586(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_6614)
static void C_fcall f_6614(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6628)
static void C_ccall f_6628(C_word c,C_word *av) C_noret;
C_noret_decl(f_6633)
static void C_fcall f_6633(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6643)
static void C_ccall f_6643(C_word c,C_word *av) C_noret;
C_noret_decl(f_6661)
static void C_fcall f_6661(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_6683)
static void C_ccall f_6683(C_word c,C_word *av) C_noret;
C_noret_decl(f_6691)
static void C_ccall f_6691(C_word c,C_word *av) C_noret;
C_noret_decl(f_6721)
static void C_ccall f_6721(C_word c,C_word *av) C_noret;
C_noret_decl(f_6756)
static void C_ccall f_6756(C_word c,C_word *av) C_noret;
C_noret_decl(f_6760)
static void C_fcall f_6760(C_word t0,C_word t1) C_noret;
C_noret_decl(f_6764)
static void C_ccall f_6764(C_word c,C_word *av) C_noret;
C_noret_decl(f_6768)
static void C_fcall f_6768(C_word t0,C_word t1) C_noret;
C_noret_decl(f_6772)
static void C_ccall f_6772(C_word c,C_word *av) C_noret;
C_noret_decl(f_6776)
static void C_ccall f_6776(C_word c,C_word *av) C_noret;
C_noret_decl(f_6808)
static void C_fcall f_6808(C_word t0,C_word t1) C_noret;
C_noret_decl(f_6812)
static void C_ccall f_6812(C_word c,C_word *av) C_noret;
C_noret_decl(f_6824)
static void C_fcall f_6824(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6856)
static void C_ccall f_6856(C_word c,C_word *av) C_noret;
C_noret_decl(f_6866)
static void C_ccall f_6866(C_word c,C_word *av) C_noret;
C_noret_decl(f_6881)
static void C_fcall f_6881(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6905)
static void C_ccall f_6905(C_word c,C_word *av) C_noret;
C_noret_decl(f_6921)
static void C_ccall f_6921(C_word c,C_word *av) C_noret;
C_noret_decl(f_6923)
static void C_fcall f_6923(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6948)
static void C_ccall f_6948(C_word c,C_word *av) C_noret;
C_noret_decl(f_7010)
static void C_ccall f_7010(C_word c,C_word *av) C_noret;
C_noret_decl(f_7012)
static void C_fcall f_7012(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7037)
static void C_fcall f_7037(C_word t0,C_word t1) C_noret;
C_noret_decl(f_7067)
static void C_ccall f_7067(C_word c,C_word *av) C_noret;
C_noret_decl(f_7088)
static void C_ccall f_7088(C_word c,C_word *av) C_noret;
C_noret_decl(f_7116)
static void C_ccall f_7116(C_word c,C_word *av) C_noret;
C_noret_decl(f_7124)
static void C_ccall f_7124(C_word c,C_word *av) C_noret;
C_noret_decl(f_7154)
static void C_ccall f_7154(C_word c,C_word *av) C_noret;
C_noret_decl(f_7179)
static void C_ccall f_7179(C_word c,C_word *av) C_noret;
C_noret_decl(f_7189)
static void C_ccall f_7189(C_word c,C_word *av) C_noret;
C_noret_decl(f_7205)
static void C_ccall f_7205(C_word c,C_word *av) C_noret;
C_noret_decl(f_7215)
static void C_ccall f_7215(C_word c,C_word *av) C_noret;
C_noret_decl(f_7221)
static void C_ccall f_7221(C_word c,C_word *av) C_noret;
C_noret_decl(f_7222)
static void C_fcall f_7222(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7234)
static void C_ccall f_7234(C_word c,C_word *av) C_noret;
C_noret_decl(f_7247)
static void C_ccall f_7247(C_word c,C_word *av) C_noret;
C_noret_decl(f_7248)
static void C_fcall f_7248(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7260)
static void C_ccall f_7260(C_word c,C_word *av) C_noret;
C_noret_decl(f_7273)
static void C_ccall f_7273(C_word c,C_word *av) C_noret;
C_noret_decl(f_7276)
static void C_ccall f_7276(C_word c,C_word *av) C_noret;
C_noret_decl(f_7292)
static void C_ccall f_7292(C_word c,C_word *av) C_noret;
C_noret_decl(f_7296)
static void C_ccall f_7296(C_word c,C_word *av) C_noret;
C_noret_decl(f_7300)
static void C_ccall f_7300(C_word c,C_word *av) C_noret;
C_noret_decl(f_7302)
static void C_fcall f_7302(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7312)
static void C_ccall f_7312(C_word c,C_word *av) C_noret;
C_noret_decl(f_7325)
static void C_fcall f_7325(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7335)
static void C_ccall f_7335(C_word c,C_word *av) C_noret;
C_noret_decl(f_7359)
static void C_ccall f_7359(C_word c,C_word *av) C_noret;
C_noret_decl(f_7363)
static void C_ccall f_7363(C_word c,C_word *av) C_noret;
C_noret_decl(f_7376)
static void C_fcall f_7376(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7410)
static void C_fcall f_7410(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7420)
static void C_fcall f_7420(C_word t0,C_word t1) C_noret;
C_noret_decl(f_7435)
static void C_ccall f_7435(C_word c,C_word *av) C_noret;
C_noret_decl(f_7450)
static void C_ccall f_7450(C_word c,C_word *av) C_noret;
C_noret_decl(f_7457)
static void C_ccall f_7457(C_word c,C_word *av) C_noret;
C_noret_decl(f_7463)
static void C_ccall f_7463(C_word c,C_word *av) C_noret;
C_noret_decl(f_7479)
static void C_ccall f_7479(C_word c,C_word *av) C_noret;
C_noret_decl(f_7483)
static void C_ccall f_7483(C_word c,C_word *av) C_noret;
C_noret_decl(f_7487)
static void C_ccall f_7487(C_word c,C_word *av) C_noret;
C_noret_decl(f_7500)
static void C_fcall f_7500(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7522)
static void C_ccall f_7522(C_word c,C_word *av) C_noret;
C_noret_decl(f_7524)
static void C_fcall f_7524(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7549)
static void C_ccall f_7549(C_word c,C_word *av) C_noret;
C_noret_decl(f_7564)
static void C_ccall f_7564(C_word c,C_word *av) C_noret;
C_noret_decl(f_7579)
static void C_fcall f_7579(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_7590)
static void C_fcall f_7590(C_word t0,C_word t1) C_noret;
C_noret_decl(f_7592)
static C_word C_fcall f_7592(C_word t0,C_word t1);
C_noret_decl(f_7657)
static void C_ccall f_7657(C_word c,C_word *av) C_noret;
C_noret_decl(f_7689)
static void C_ccall f_7689(C_word c,C_word *av) C_noret;
C_noret_decl(f_7698)
static void C_ccall f_7698(C_word c,C_word *av) C_noret;
C_noret_decl(f_7701)
static void C_ccall f_7701(C_word c,C_word *av) C_noret;
C_noret_decl(f_7704)
static void C_ccall f_7704(C_word c,C_word *av) C_noret;
C_noret_decl(f_7705)
static void C_fcall f_7705(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7719)
static void C_ccall f_7719(C_word c,C_word *av) C_noret;
C_noret_decl(f_7723)
static void C_ccall f_7723(C_word c,C_word *av) C_noret;
C_noret_decl(f_7726)
static void C_ccall f_7726(C_word c,C_word *av) C_noret;
C_noret_decl(f_7729)
static void C_ccall f_7729(C_word c,C_word *av) C_noret;
C_noret_decl(f_7732)
static void C_ccall f_7732(C_word c,C_word *av) C_noret;
C_noret_decl(f_7740)
static void C_ccall f_7740(C_word c,C_word *av) C_noret;
C_noret_decl(f_7747)
static void C_ccall f_7747(C_word c,C_word *av) C_noret;
C_noret_decl(f_7756)
static void C_ccall f_7756(C_word c,C_word *av) C_noret;
C_noret_decl(f_7759)
static void C_ccall f_7759(C_word c,C_word *av) C_noret;
C_noret_decl(f_7766)
static void C_ccall f_7766(C_word c,C_word *av) C_noret;
C_noret_decl(f_7769)
static void C_ccall f_7769(C_word c,C_word *av) C_noret;
C_noret_decl(f_7770)
static void C_fcall f_7770(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7774)
static void C_ccall f_7774(C_word c,C_word *av) C_noret;
C_noret_decl(f_7777)
static void C_ccall f_7777(C_word c,C_word *av) C_noret;
C_noret_decl(f_7789)
static void C_fcall f_7789(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7799)
static void C_ccall f_7799(C_word c,C_word *av) C_noret;
C_noret_decl(f_7821)
static void C_ccall f_7821(C_word c,C_word *av) C_noret;
C_noret_decl(f_7822)
static void C_fcall f_7822(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7826)
static void C_ccall f_7826(C_word c,C_word *av) C_noret;
C_noret_decl(f_7834)
static void C_fcall f_7834(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7844)
static void C_ccall f_7844(C_word c,C_word *av) C_noret;
C_noret_decl(f_7858)
static void C_ccall f_7858(C_word c,C_word *av) C_noret;
C_noret_decl(f_7861)
static void C_ccall f_7861(C_word c,C_word *av) C_noret;
C_noret_decl(f_7864)
static void C_ccall f_7864(C_word c,C_word *av) C_noret;
C_noret_decl(f_7892)
static void C_ccall f_7892(C_word c,C_word *av) C_noret;
C_noret_decl(f_7899)
static void C_ccall f_7899(C_word c,C_word *av) C_noret;
C_noret_decl(f_7905)
static void C_ccall f_7905(C_word c,C_word *av) C_noret;
C_noret_decl(f_7908)
static void C_ccall f_7908(C_word c,C_word *av) C_noret;
C_noret_decl(f_7909)
static void C_fcall f_7909(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7913)
static void C_ccall f_7913(C_word c,C_word *av) C_noret;
C_noret_decl(f_7931)
static void C_ccall f_7931(C_word c,C_word *av) C_noret;
C_noret_decl(f_7937)
static void C_ccall f_7937(C_word c,C_word *av) C_noret;
C_noret_decl(f_7940)
static void C_ccall f_7940(C_word c,C_word *av) C_noret;
C_noret_decl(f_7943)
static void C_ccall f_7943(C_word c,C_word *av) C_noret;
C_noret_decl(f_7954)
static void C_ccall f_7954(C_word c,C_word *av) C_noret;
C_noret_decl(f_7958)
static void C_ccall f_7958(C_word c,C_word *av) C_noret;
C_noret_decl(f_7962)
static void C_ccall f_7962(C_word c,C_word *av) C_noret;
C_noret_decl(f_7966)
static void C_ccall f_7966(C_word c,C_word *av) C_noret;
C_noret_decl(f_7972)
static void C_fcall f_7972(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7982)
static void C_ccall f_7982(C_word c,C_word *av) C_noret;
C_noret_decl(f_7997)
static void C_ccall f_7997(C_word c,C_word *av) C_noret;
C_noret_decl(f_8001)
static void C_ccall f_8001(C_word c,C_word *av) C_noret;
C_noret_decl(f_8003)
static void C_fcall f_8003(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8013)
static void C_fcall f_8013(C_word t0,C_word t1) C_noret;
C_noret_decl(f_8028)
static void C_ccall f_8028(C_word c,C_word *av) C_noret;
C_noret_decl(f_8040)
static void C_fcall f_8040(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8050)
static void C_ccall f_8050(C_word c,C_word *av) C_noret;
C_noret_decl(f_8065)
static void C_ccall f_8065(C_word c,C_word *av) C_noret;
C_noret_decl(f_8071)
static void C_fcall f_8071(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8106)
static void C_ccall f_8106(C_word c,C_word *av) C_noret;
C_noret_decl(f_8115)
static void C_ccall f_8115(C_word c,C_word *av) C_noret;
C_noret_decl(f_8126)
static void C_fcall f_8126(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8130)
static void C_ccall f_8130(C_word c,C_word *av) C_noret;
C_noret_decl(f_8133)
static void C_fcall f_8133(C_word t0,C_word t1) C_noret;
C_noret_decl(f_8141)
static void C_ccall f_8141(C_word c,C_word *av) C_noret;
C_noret_decl(f_8162)
static void C_ccall f_8162(C_word c,C_word *av) C_noret;
C_noret_decl(f_8166)
static void C_fcall f_8166(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8174)
static void C_ccall f_8174(C_word c,C_word *av) C_noret;
C_noret_decl(f_8178)
static void C_ccall f_8178(C_word c,C_word *av) C_noret;
C_noret_decl(f_8194)
static void C_ccall f_8194(C_word c,C_word *av) C_noret;
C_noret_decl(f_8198)
static void C_ccall f_8198(C_word c,C_word *av) C_noret;
C_noret_decl(f_8212)
static void C_ccall f_8212(C_word c,C_word *av) C_noret;
C_noret_decl(f_8245)
static void C_ccall f_8245(C_word c,C_word *av) C_noret;
C_noret_decl(f_8247)
static void C_fcall f_8247(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8260)
static void C_ccall f_8260(C_word c,C_word *av) C_noret;
C_noret_decl(f_8269)
static void C_ccall f_8269(C_word c,C_word *av) C_noret;
C_noret_decl(f_8282)
static void C_fcall f_8282(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8318)
static void C_ccall f_8318(C_word c,C_word *av) C_noret;
C_noret_decl(f_8325)
static void C_ccall f_8325(C_word c,C_word *av) C_noret;
C_noret_decl(f_8329)
static void C_ccall f_8329(C_word c,C_word *av) C_noret;
C_noret_decl(f_8332)
static void C_ccall f_8332(C_word c,C_word *av) C_noret;
C_noret_decl(f_8337)
static void C_ccall f_8337(C_word c,C_word *av) C_noret;
C_noret_decl(f_8341)
static void C_ccall f_8341(C_word c,C_word *av) C_noret;
C_noret_decl(f_8344)
static void C_ccall f_8344(C_word c,C_word *av) C_noret;
C_noret_decl(f_8347)
static void C_ccall f_8347(C_word c,C_word *av) C_noret;
C_noret_decl(f_8350)
static void C_ccall f_8350(C_word c,C_word *av) C_noret;
C_noret_decl(f_8354)
static void C_ccall f_8354(C_word c,C_word *av) C_noret;
C_noret_decl(f_8358)
static void C_ccall f_8358(C_word c,C_word *av) C_noret;
C_noret_decl(f_8362)
static void C_ccall f_8362(C_word c,C_word *av) C_noret;
C_noret_decl(f_8366)
static void C_ccall f_8366(C_word c,C_word *av) C_noret;
C_noret_decl(f_8369)
static void C_ccall f_8369(C_word c,C_word *av) C_noret;
C_noret_decl(f_8372)
static void C_ccall f_8372(C_word c,C_word *av) C_noret;
C_noret_decl(f_8375)
static void C_ccall f_8375(C_word c,C_word *av) C_noret;
C_noret_decl(f_8378)
static void C_ccall f_8378(C_word c,C_word *av) C_noret;
C_noret_decl(f_8393)
static void C_ccall f_8393(C_word c,C_word *av) C_noret;
C_noret_decl(f_8399)
static void C_ccall f_8399(C_word c,C_word *av) C_noret;
C_noret_decl(f_8403)
static void C_ccall f_8403(C_word c,C_word *av) C_noret;
C_noret_decl(f_8406)
static void C_ccall f_8406(C_word c,C_word *av) C_noret;
C_noret_decl(f_8409)
static void C_ccall f_8409(C_word c,C_word *av) C_noret;
C_noret_decl(f_8412)
static void C_ccall f_8412(C_word c,C_word *av) C_noret;
C_noret_decl(f_8415)
static void C_ccall f_8415(C_word c,C_word *av) C_noret;
C_noret_decl(f_8418)
static void C_ccall f_8418(C_word c,C_word *av) C_noret;
C_noret_decl(f_8421)
static void C_ccall f_8421(C_word c,C_word *av) C_noret;
C_noret_decl(f_8424)
static void C_ccall f_8424(C_word c,C_word *av) C_noret;
C_noret_decl(f_8430)
static void C_ccall f_8430(C_word c,C_word *av) C_noret;
C_noret_decl(f_8434)
static void C_ccall f_8434(C_word c,C_word *av) C_noret;
C_noret_decl(f_8442)
static void C_ccall f_8442(C_word c,C_word *av) C_noret;
C_noret_decl(f_8448)
static void C_ccall f_8448(C_word c,C_word *av) C_noret;
C_noret_decl(f_8453)
static void C_ccall f_8453(C_word c,C_word *av) C_noret;
C_noret_decl(f_8457)
static void C_ccall f_8457(C_word c,C_word *av) C_noret;
C_noret_decl(f_8462)
static void C_ccall f_8462(C_word c,C_word *av) C_noret;
C_noret_decl(f_8469)
static void C_ccall f_8469(C_word c,C_word *av) C_noret;
C_noret_decl(f_8473)
static void C_ccall f_8473(C_word c,C_word *av) C_noret;
C_noret_decl(f_8476)
static void C_fcall f_8476(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8480)
static void C_ccall f_8480(C_word c,C_word *av) C_noret;
C_noret_decl(f_8483)
static void C_ccall f_8483(C_word c,C_word *av) C_noret;
C_noret_decl(f_8491)
static void C_ccall f_8491(C_word c,C_word *av) C_noret;
C_noret_decl(f_8495)
static void C_ccall f_8495(C_word c,C_word *av) C_noret;
C_noret_decl(f_8498)
static void C_ccall f_8498(C_word c,C_word *av) C_noret;
C_noret_decl(f_8501)
static void C_ccall f_8501(C_word c,C_word *av) C_noret;
C_noret_decl(f_8504)
static void C_ccall f_8504(C_word c,C_word *av) C_noret;
C_noret_decl(f_8506)
static void C_fcall f_8506(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_8514)
static void C_ccall f_8514(C_word c,C_word *av) C_noret;
C_noret_decl(f_8518)
static void C_ccall f_8518(C_word c,C_word *av) C_noret;
C_noret_decl(f_8520)
static void C_fcall f_8520(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8533)
static void C_ccall f_8533(C_word c,C_word *av) C_noret;
C_noret_decl(f_8540)
static void C_ccall f_8540(C_word c,C_word *av) C_noret;
C_noret_decl(f_8567)
static void C_ccall f_8567(C_word c,C_word *av) C_noret;
C_noret_decl(f_8570)
static void C_fcall f_8570(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8574)
static void C_ccall f_8574(C_word c,C_word *av) C_noret;
C_noret_decl(f_8577)
static void C_ccall f_8577(C_word c,C_word *av) C_noret;
C_noret_decl(f_8618)
static void C_fcall f_8618(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8632)
static void C_ccall f_8632(C_word c,C_word *av) C_noret;
C_noret_decl(f_8650)
static void C_ccall f_8650(C_word c,C_word *av) C_noret;
C_noret_decl(f_8653)
static void C_ccall f_8653(C_word c,C_word *av) C_noret;
C_noret_decl(f_8658)
static void C_ccall f_8658(C_word c,C_word *av) C_noret;
C_noret_decl(f_8668)
static void C_ccall f_8668(C_word c,C_word *av) C_noret;
C_noret_decl(f_8672)
static void C_ccall f_8672(C_word c,C_word *av) C_noret;
C_noret_decl(f_8677)
static void C_fcall f_8677(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5) C_noret;
C_noret_decl(f_8685)
static void C_fcall f_8685(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8695)
static void C_ccall f_8695(C_word c,C_word *av) C_noret;
C_noret_decl(f_8708)
static void C_fcall f_8708(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8718)
static void C_ccall f_8718(C_word c,C_word *av) C_noret;
C_noret_decl(f_8736)
static void C_fcall f_8736(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8758)
static void C_fcall f_8758(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8800)
static void C_ccall f_8800(C_word c,C_word *av) C_noret;
C_noret_decl(f_8803)
static void C_ccall f_8803(C_word c,C_word *av) C_noret;
C_noret_decl(f_8808)
static void C_ccall f_8808(C_word c,C_word *av) C_noret;
C_noret_decl(f_8818)
static void C_ccall f_8818(C_word c,C_word *av) C_noret;
C_noret_decl(f_8822)
static void C_ccall f_8822(C_word c,C_word *av) C_noret;
C_noret_decl(f_8827)
static void C_fcall f_8827(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_8839)
static void C_fcall f_8839(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_8847)
static void C_fcall f_8847(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8857)
static void C_ccall f_8857(C_word c,C_word *av) C_noret;
C_noret_decl(f_8870)
static void C_fcall f_8870(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8880)
static void C_ccall f_8880(C_word c,C_word *av) C_noret;
C_noret_decl(f_8898)
static void C_fcall f_8898(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8910)
static void C_ccall f_8910(C_word c,C_word *av) C_noret;
C_noret_decl(f_8939)
static void C_fcall f_8939(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8951)
static void C_ccall f_8951(C_word c,C_word *av) C_noret;
C_noret_decl(f_8983)
static void C_ccall f_8983(C_word c,C_word *av) C_noret;
C_noret_decl(f_8986)
static void C_ccall f_8986(C_word c,C_word *av) C_noret;
C_noret_decl(f_8991)
static void C_ccall f_8991(C_word c,C_word *av) C_noret;
C_noret_decl(f_9001)
static void C_ccall f_9001(C_word c,C_word *av) C_noret;
C_noret_decl(f_9005)
static void C_ccall f_9005(C_word c,C_word *av) C_noret;
C_noret_decl(f_9010)
static void C_fcall f_9010(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_9022)
static void C_fcall f_9022(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_9030)
static void C_fcall f_9030(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9043)
static void C_ccall f_9043(C_word c,C_word *av) C_noret;
C_noret_decl(f_9049)
static void C_ccall f_9049(C_word c,C_word *av) C_noret;
C_noret_decl(f_9062)
static void C_fcall f_9062(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9072)
static void C_ccall f_9072(C_word c,C_word *av) C_noret;
C_noret_decl(f_9085)
static void C_fcall f_9085(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9124)
static void C_fcall f_9124(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9140)
static void C_ccall f_9140(C_word c,C_word *av) C_noret;
C_noret_decl(f_9177)
static void C_fcall f_9177(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9193)
static void C_ccall f_9193(C_word c,C_word *av) C_noret;
C_noret_decl(f_9233)
static void C_ccall f_9233(C_word c,C_word *av) C_noret;
C_noret_decl(f_9236)
static void C_ccall f_9236(C_word c,C_word *av) C_noret;
C_noret_decl(f_9241)
static void C_ccall f_9241(C_word c,C_word *av) C_noret;
C_noret_decl(f_9251)
static void C_ccall f_9251(C_word c,C_word *av) C_noret;
C_noret_decl(f_9255)
static void C_ccall f_9255(C_word c,C_word *av) C_noret;
C_noret_decl(f_9257)
static void C_fcall f_9257(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9265)
static void C_ccall f_9265(C_word c,C_word *av) C_noret;
C_noret_decl(f_9271)
static void C_ccall f_9271(C_word c,C_word *av) C_noret;
C_noret_decl(f_9275)
static void C_ccall f_9275(C_word c,C_word *av) C_noret;
C_noret_decl(f_9279)
static void C_ccall f_9279(C_word c,C_word *av) C_noret;
C_noret_decl(f_9300)
static void C_ccall f_9300(C_word c,C_word *av) C_noret;
C_noret_decl(f_9310)
static void C_ccall f_9310(C_word c,C_word *av) C_noret;
C_noret_decl(f_9312)
static void C_fcall f_9312(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9337)
static void C_ccall f_9337(C_word c,C_word *av) C_noret;
C_noret_decl(f_9346)
static void C_fcall f_9346(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9371)
static void C_ccall f_9371(C_word c,C_word *av) C_noret;
C_noret_decl(f_9389)
static void C_ccall f_9389(C_word c,C_word *av) C_noret;
C_noret_decl(f_9395)
static void C_ccall f_9395(C_word c,C_word *av) C_noret;
C_noret_decl(f_9399)
static void C_ccall f_9399(C_word c,C_word *av) C_noret;
C_noret_decl(f_9400)
static void C_fcall f_9400(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9406)
static void C_ccall f_9406(C_word c,C_word *av) C_noret;
C_noret_decl(f_9412)
static void C_ccall f_9412(C_word c,C_word *av) C_noret;
C_noret_decl(f_9434)
static void C_ccall f_9434(C_word c,C_word *av) C_noret;
C_noret_decl(f_9436)
static void C_fcall f_9436(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9446)
static void C_ccall f_9446(C_word c,C_word *av) C_noret;
C_noret_decl(f_9459)
static void C_ccall f_9459(C_word c,C_word *av) C_noret;
C_noret_decl(f_9463)
static void C_ccall f_9463(C_word c,C_word *av) C_noret;
C_noret_decl(f_9466)
static void C_ccall f_9466(C_word c,C_word *av) C_noret;
C_noret_decl(f_9476)
static void C_fcall f_9476(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9514)
static void C_ccall f_9514(C_word c,C_word *av) C_noret;
C_noret_decl(f_9520)
static void C_ccall f_9520(C_word c,C_word *av) C_noret;
C_noret_decl(f_9521)
static void C_fcall f_9521(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9557)
static void C_ccall f_9557(C_word c,C_word *av) C_noret;
C_noret_decl(f_9563)
static void C_ccall f_9563(C_word c,C_word *av) C_noret;
C_noret_decl(f_9566)
static void C_fcall f_9566(C_word t0,C_word t1) C_noret;
C_noret_decl(f_9569)
static void C_ccall f_9569(C_word c,C_word *av) C_noret;
C_noret_decl(f_9576)
static void C_ccall f_9576(C_word c,C_word *av) C_noret;
C_noret_decl(f_9580)
static void C_ccall f_9580(C_word c,C_word *av) C_noret;
C_noret_decl(f_9584)
static void C_ccall f_9584(C_word c,C_word *av) C_noret;
C_noret_decl(f_9588)
static void C_ccall f_9588(C_word c,C_word *av) C_noret;
C_noret_decl(f_9591)
static void C_ccall f_9591(C_word c,C_word *av) C_noret;
C_noret_decl(f_9597)
static void C_ccall f_9597(C_word c,C_word *av) C_noret;
C_noret_decl(f_9600)
static void C_ccall f_9600(C_word c,C_word *av) C_noret;
C_noret_decl(f_9607)
static void C_ccall f_9607(C_word c,C_word *av) C_noret;
C_noret_decl(f_9617)
static void C_ccall f_9617(C_word c,C_word *av) C_noret;
C_noret_decl(f_9624)
static void C_ccall f_9624(C_word c,C_word *av) C_noret;
C_noret_decl(f_9635)
static void C_ccall f_9635(C_word c,C_word *av) C_noret;
C_noret_decl(f_9642)
static void C_ccall f_9642(C_word c,C_word *av) C_noret;
C_noret_decl(f_9644)
static void C_fcall f_9644(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9678)
static void C_fcall f_9678(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9714)
static void C_ccall f_9714(C_word c,C_word *av) C_noret;
C_noret_decl(f_9725)
static void C_ccall f_9725(C_word c,C_word *av) C_noret;
C_noret_decl(f_9739)
static void C_ccall f_9739(C_word c,C_word *av) C_noret;
C_noret_decl(f_9746)
static void C_ccall f_9746(C_word c,C_word *av) C_noret;
C_noret_decl(f_9748)
static void C_fcall f_9748(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9782)
static void C_fcall f_9782(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9822)
static void C_fcall f_9822(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9832)
static void C_ccall f_9832(C_word c,C_word *av) C_noret;
C_noret_decl(f_9845)
static void C_fcall f_9845(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9855)
static void C_ccall f_9855(C_word c,C_word *av) C_noret;
C_noret_decl(f_9876)
static void C_ccall f_9876(C_word c,C_word *av) C_noret;
C_noret_decl(f_9891)
static void C_ccall f_9891(C_word c,C_word *av) C_noret;
C_noret_decl(f_9901)
static void C_fcall f_9901(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9909)
static void C_ccall f_9909(C_word c,C_word *av) C_noret;
C_noret_decl(f_9919)
static void C_ccall f_9919(C_word c,C_word *av) C_noret;
C_noret_decl(f_9922)
static void C_fcall f_9922(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9926)
static void C_ccall f_9926(C_word c,C_word *av) C_noret;
C_noret_decl(f_9930)
static void C_fcall f_9930(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9937)
static void C_ccall f_9937(C_word c,C_word *av) C_noret;
C_noret_decl(f_9956)
static void C_ccall f_9956(C_word c,C_word *av) C_noret;
C_noret_decl(f_9971)
static void C_fcall f_9971(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9997)
static void C_ccall f_9997(C_word c,C_word *av) C_noret;
C_noret_decl(f_9999)
static void C_ccall f_9999(C_word c,C_word *av) C_noret;
C_noret_decl(C_modules_toplevel)
C_externexport void C_ccall C_modules_toplevel(C_word c,C_word *av) C_noret;

C_noret_decl(trf_10002)
static void C_ccall trf_10002(C_word c,C_word *av) C_noret;
static void C_ccall trf_10002(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10002(t0,t1,t2);}

C_noret_decl(trf_10008)
static void C_ccall trf_10008(C_word c,C_word *av) C_noret;
static void C_ccall trf_10008(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10008(t0,t1,t2);}

C_noret_decl(trf_10049)
static void C_ccall trf_10049(C_word c,C_word *av) C_noret;
static void C_ccall trf_10049(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10049(t0,t1,t2);}

C_noret_decl(trf_10158)
static void C_ccall trf_10158(C_word c,C_word *av) C_noret;
static void C_ccall trf_10158(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10158(t0,t1,t2);}

C_noret_decl(trf_10238)
static void C_ccall trf_10238(C_word c,C_word *av) C_noret;
static void C_ccall trf_10238(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10238(t0,t1,t2);}

C_noret_decl(trf_10255)
static void C_ccall trf_10255(C_word c,C_word *av) C_noret;
static void C_ccall trf_10255(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_10255(t0,t1);}

C_noret_decl(trf_10279)
static void C_ccall trf_10279(C_word c,C_word *av) C_noret;
static void C_ccall trf_10279(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10279(t0,t1,t2);}

C_noret_decl(trf_10339)
static void C_ccall trf_10339(C_word c,C_word *av) C_noret;
static void C_ccall trf_10339(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_10339(t0,t1,t2,t3);}

C_noret_decl(trf_10351)
static void C_ccall trf_10351(C_word c,C_word *av) C_noret;
static void C_ccall trf_10351(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10351(t0,t1,t2);}

C_noret_decl(trf_10456)
static void C_ccall trf_10456(C_word c,C_word *av) C_noret;
static void C_ccall trf_10456(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_10456(t0,t1,t2,t3,t4);}

C_noret_decl(trf_10467)
static void C_ccall trf_10467(C_word c,C_word *av) C_noret;
static void C_ccall trf_10467(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10467(t0,t1,t2);}

C_noret_decl(trf_10480)
static void C_ccall trf_10480(C_word c,C_word *av) C_noret;
static void C_ccall trf_10480(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_10480(t0,t1);}

C_noret_decl(trf_10554)
static void C_ccall trf_10554(C_word c,C_word *av) C_noret;
static void C_ccall trf_10554(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10554(t0,t1,t2);}

C_noret_decl(trf_10588)
static void C_ccall trf_10588(C_word c,C_word *av) C_noret;
static void C_ccall trf_10588(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10588(t0,t1,t2);}

C_noret_decl(trf_10757)
static void C_ccall trf_10757(C_word c,C_word *av) C_noret;
static void C_ccall trf_10757(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10757(t0,t1,t2);}

C_noret_decl(trf_10811)
static void C_ccall trf_10811(C_word c,C_word *av) C_noret;
static void C_ccall trf_10811(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10811(t0,t1,t2);}

C_noret_decl(trf_10865)
static void C_ccall trf_10865(C_word c,C_word *av) C_noret;
static void C_ccall trf_10865(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10865(t0,t1,t2);}

C_noret_decl(trf_10919)
static void C_ccall trf_10919(C_word c,C_word *av) C_noret;
static void C_ccall trf_10919(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10919(t0,t1,t2);}

C_noret_decl(trf_10973)
static void C_ccall trf_10973(C_word c,C_word *av) C_noret;
static void C_ccall trf_10973(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10973(t0,t1,t2);}

C_noret_decl(trf_11027)
static void C_ccall trf_11027(C_word c,C_word *av) C_noret;
static void C_ccall trf_11027(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11027(t0,t1,t2);}

C_noret_decl(trf_11081)
static void C_ccall trf_11081(C_word c,C_word *av) C_noret;
static void C_ccall trf_11081(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11081(t0,t1,t2);}

C_noret_decl(trf_11135)
static void C_ccall trf_11135(C_word c,C_word *av) C_noret;
static void C_ccall trf_11135(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11135(t0,t1,t2);}

C_noret_decl(trf_11189)
static void C_ccall trf_11189(C_word c,C_word *av) C_noret;
static void C_ccall trf_11189(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11189(t0,t1,t2);}

C_noret_decl(trf_11243)
static void C_ccall trf_11243(C_word c,C_word *av) C_noret;
static void C_ccall trf_11243(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11243(t0,t1,t2);}

C_noret_decl(trf_11297)
static void C_ccall trf_11297(C_word c,C_word *av) C_noret;
static void C_ccall trf_11297(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11297(t0,t1,t2);}

C_noret_decl(trf_11351)
static void C_ccall trf_11351(C_word c,C_word *av) C_noret;
static void C_ccall trf_11351(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11351(t0,t1,t2);}

C_noret_decl(trf_11405)
static void C_ccall trf_11405(C_word c,C_word *av) C_noret;
static void C_ccall trf_11405(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11405(t0,t1,t2);}

C_noret_decl(trf_4518)
static void C_ccall trf_4518(C_word c,C_word *av) C_noret;
static void C_ccall trf_4518(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_4518(t0,t1,t2,t3);}

C_noret_decl(trf_4524)
static void C_ccall trf_4524(C_word c,C_word *av) C_noret;
static void C_ccall trf_4524(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4524(t0,t1,t2);}

C_noret_decl(trf_5674)
static void C_ccall trf_5674(C_word c,C_word *av) C_noret;
static void C_ccall trf_5674(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5674(t0,t1,t2);}

C_noret_decl(trf_5718)
static void C_ccall trf_5718(C_word c,C_word *av) C_noret;
static void C_ccall trf_5718(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_5718(t0,t1,t2,t3);}

C_noret_decl(trf_5726)
static void C_ccall trf_5726(C_word c,C_word *av) C_noret;
static void C_ccall trf_5726(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5726(t0,t1,t2);}

C_noret_decl(trf_5822)
static void C_ccall trf_5822(C_word c,C_word *av) C_noret;
static void C_ccall trf_5822(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5822(t0,t1);}

C_noret_decl(trf_5844)
static void C_ccall trf_5844(C_word c,C_word *av) C_noret;
static void C_ccall trf_5844(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5844(t0,t1,t2);}

C_noret_decl(trf_5921)
static void C_ccall trf_5921(C_word c,C_word *av) C_noret;
static void C_ccall trf_5921(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5921(t0,t1,t2);}

C_noret_decl(trf_5974)
static void C_ccall trf_5974(C_word c,C_word *av) C_noret;
static void C_ccall trf_5974(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_5974(t0,t1,t2,t3);}

C_noret_decl(trf_6206)
static void C_ccall trf_6206(C_word c,C_word *av) C_noret;
static void C_ccall trf_6206(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6206(t0,t1,t2);}

C_noret_decl(trf_6213)
static void C_ccall trf_6213(C_word c,C_word *av) C_noret;
static void C_ccall trf_6213(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_6213(t0,t1);}

C_noret_decl(trf_6364)
static void C_ccall trf_6364(C_word c,C_word *av) C_noret;
static void C_ccall trf_6364(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_6364(t0,t1,t2,t3);}

C_noret_decl(trf_6387)
static void C_ccall trf_6387(C_word c,C_word *av) C_noret;
static void C_ccall trf_6387(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6387(t0,t1,t2);}

C_noret_decl(trf_6414)
static void C_ccall trf_6414(C_word c,C_word *av) C_noret;
static void C_ccall trf_6414(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6414(t0,t1,t2);}

C_noret_decl(trf_6451)
static void C_ccall trf_6451(C_word c,C_word *av) C_noret;
static void C_ccall trf_6451(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6451(t0,t1,t2);}

C_noret_decl(trf_6491)
static void C_ccall trf_6491(C_word c,C_word *av) C_noret;
static void C_ccall trf_6491(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6491(t0,t1,t2);}

C_noret_decl(trf_6568)
static void C_ccall trf_6568(C_word c,C_word *av) C_noret;
static void C_ccall trf_6568(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_6568(t0,t1);}

C_noret_decl(trf_6586)
static void C_ccall trf_6586(C_word c,C_word *av) C_noret;
static void C_ccall trf_6586(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_6586(t0,t1,t2,t3,t4);}

C_noret_decl(trf_6614)
static void C_ccall trf_6614(C_word c,C_word *av) C_noret;
static void C_ccall trf_6614(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6614(t0,t1,t2);}

C_noret_decl(trf_6633)
static void C_ccall trf_6633(C_word c,C_word *av) C_noret;
static void C_ccall trf_6633(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6633(t0,t1,t2);}

C_noret_decl(trf_6661)
static void C_ccall trf_6661(C_word c,C_word *av) C_noret;
static void C_ccall trf_6661(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_6661(t0,t1,t2,t3);}

C_noret_decl(trf_6760)
static void C_ccall trf_6760(C_word c,C_word *av) C_noret;
static void C_ccall trf_6760(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_6760(t0,t1);}

C_noret_decl(trf_6768)
static void C_ccall trf_6768(C_word c,C_word *av) C_noret;
static void C_ccall trf_6768(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_6768(t0,t1);}

C_noret_decl(trf_6808)
static void C_ccall trf_6808(C_word c,C_word *av) C_noret;
static void C_ccall trf_6808(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_6808(t0,t1);}

C_noret_decl(trf_6824)
static void C_ccall trf_6824(C_word c,C_word *av) C_noret;
static void C_ccall trf_6824(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6824(t0,t1,t2);}

C_noret_decl(trf_6881)
static void C_ccall trf_6881(C_word c,C_word *av) C_noret;
static void C_ccall trf_6881(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6881(t0,t1,t2);}

C_noret_decl(trf_6923)
static void C_ccall trf_6923(C_word c,C_word *av) C_noret;
static void C_ccall trf_6923(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6923(t0,t1,t2);}

C_noret_decl(trf_7012)
static void C_ccall trf_7012(C_word c,C_word *av) C_noret;
static void C_ccall trf_7012(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7012(t0,t1,t2);}

C_noret_decl(trf_7037)
static void C_ccall trf_7037(C_word c,C_word *av) C_noret;
static void C_ccall trf_7037(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_7037(t0,t1);}

C_noret_decl(trf_7222)
static void C_ccall trf_7222(C_word c,C_word *av) C_noret;
static void C_ccall trf_7222(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7222(t0,t1,t2);}

C_noret_decl(trf_7248)
static void C_ccall trf_7248(C_word c,C_word *av) C_noret;
static void C_ccall trf_7248(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7248(t0,t1,t2);}

C_noret_decl(trf_7302)
static void C_ccall trf_7302(C_word c,C_word *av) C_noret;
static void C_ccall trf_7302(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7302(t0,t1,t2);}

C_noret_decl(trf_7325)
static void C_ccall trf_7325(C_word c,C_word *av) C_noret;
static void C_ccall trf_7325(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7325(t0,t1,t2);}

C_noret_decl(trf_7376)
static void C_ccall trf_7376(C_word c,C_word *av) C_noret;
static void C_ccall trf_7376(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7376(t0,t1,t2);}

C_noret_decl(trf_7410)
static void C_ccall trf_7410(C_word c,C_word *av) C_noret;
static void C_ccall trf_7410(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7410(t0,t1,t2);}

C_noret_decl(trf_7420)
static void C_ccall trf_7420(C_word c,C_word *av) C_noret;
static void C_ccall trf_7420(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_7420(t0,t1);}

C_noret_decl(trf_7500)
static void C_ccall trf_7500(C_word c,C_word *av) C_noret;
static void C_ccall trf_7500(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7500(t0,t1,t2);}

C_noret_decl(trf_7524)
static void C_ccall trf_7524(C_word c,C_word *av) C_noret;
static void C_ccall trf_7524(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7524(t0,t1,t2);}

C_noret_decl(trf_7579)
static void C_ccall trf_7579(C_word c,C_word *av) C_noret;
static void C_ccall trf_7579(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_7579(t0,t1,t2,t3);}

C_noret_decl(trf_7590)
static void C_ccall trf_7590(C_word c,C_word *av) C_noret;
static void C_ccall trf_7590(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_7590(t0,t1);}

C_noret_decl(trf_7705)
static void C_ccall trf_7705(C_word c,C_word *av) C_noret;
static void C_ccall trf_7705(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7705(t0,t1,t2);}

C_noret_decl(trf_7770)
static void C_ccall trf_7770(C_word c,C_word *av) C_noret;
static void C_ccall trf_7770(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7770(t0,t1,t2);}

C_noret_decl(trf_7789)
static void C_ccall trf_7789(C_word c,C_word *av) C_noret;
static void C_ccall trf_7789(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7789(t0,t1,t2);}

C_noret_decl(trf_7822)
static void C_ccall trf_7822(C_word c,C_word *av) C_noret;
static void C_ccall trf_7822(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7822(t0,t1,t2);}

C_noret_decl(trf_7834)
static void C_ccall trf_7834(C_word c,C_word *av) C_noret;
static void C_ccall trf_7834(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7834(t0,t1,t2);}

C_noret_decl(trf_7909)
static void C_ccall trf_7909(C_word c,C_word *av) C_noret;
static void C_ccall trf_7909(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7909(t0,t1,t2);}

C_noret_decl(trf_7972)
static void C_ccall trf_7972(C_word c,C_word *av) C_noret;
static void C_ccall trf_7972(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7972(t0,t1,t2);}

C_noret_decl(trf_8003)
static void C_ccall trf_8003(C_word c,C_word *av) C_noret;
static void C_ccall trf_8003(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8003(t0,t1,t2);}

C_noret_decl(trf_8013)
static void C_ccall trf_8013(C_word c,C_word *av) C_noret;
static void C_ccall trf_8013(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_8013(t0,t1);}

C_noret_decl(trf_8040)
static void C_ccall trf_8040(C_word c,C_word *av) C_noret;
static void C_ccall trf_8040(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8040(t0,t1,t2);}

C_noret_decl(trf_8071)
static void C_ccall trf_8071(C_word c,C_word *av) C_noret;
static void C_ccall trf_8071(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8071(t0,t1,t2);}

C_noret_decl(trf_8126)
static void C_ccall trf_8126(C_word c,C_word *av) C_noret;
static void C_ccall trf_8126(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8126(t0,t1,t2);}

C_noret_decl(trf_8133)
static void C_ccall trf_8133(C_word c,C_word *av) C_noret;
static void C_ccall trf_8133(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_8133(t0,t1);}

C_noret_decl(trf_8166)
static void C_ccall trf_8166(C_word c,C_word *av) C_noret;
static void C_ccall trf_8166(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8166(t0,t1,t2);}

C_noret_decl(trf_8247)
static void C_ccall trf_8247(C_word c,C_word *av) C_noret;
static void C_ccall trf_8247(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8247(t0,t1,t2);}

C_noret_decl(trf_8282)
static void C_ccall trf_8282(C_word c,C_word *av) C_noret;
static void C_ccall trf_8282(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8282(t0,t1,t2);}

C_noret_decl(trf_8476)
static void C_ccall trf_8476(C_word c,C_word *av) C_noret;
static void C_ccall trf_8476(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8476(t0,t1,t2);}

C_noret_decl(trf_8506)
static void C_ccall trf_8506(C_word c,C_word *av) C_noret;
static void C_ccall trf_8506(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_8506(t0,t1,t2,t3);}

C_noret_decl(trf_8520)
static void C_ccall trf_8520(C_word c,C_word *av) C_noret;
static void C_ccall trf_8520(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8520(t0,t1,t2);}

C_noret_decl(trf_8570)
static void C_ccall trf_8570(C_word c,C_word *av) C_noret;
static void C_ccall trf_8570(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8570(t0,t1,t2);}

C_noret_decl(trf_8618)
static void C_ccall trf_8618(C_word c,C_word *av) C_noret;
static void C_ccall trf_8618(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8618(t0,t1,t2);}

C_noret_decl(trf_8677)
static void C_ccall trf_8677(C_word c,C_word *av) C_noret;
static void C_ccall trf_8677(C_word c,C_word *av){
C_word t0=av[5];
C_word t1=av[4];
C_word t2=av[3];
C_word t3=av[2];
C_word t4=av[1];
C_word t5=av[0];
f_8677(t0,t1,t2,t3,t4,t5);}

C_noret_decl(trf_8685)
static void C_ccall trf_8685(C_word c,C_word *av) C_noret;
static void C_ccall trf_8685(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8685(t0,t1,t2);}

C_noret_decl(trf_8708)
static void C_ccall trf_8708(C_word c,C_word *av) C_noret;
static void C_ccall trf_8708(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8708(t0,t1,t2);}

C_noret_decl(trf_8736)
static void C_ccall trf_8736(C_word c,C_word *av) C_noret;
static void C_ccall trf_8736(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8736(t0,t1,t2);}

C_noret_decl(trf_8758)
static void C_ccall trf_8758(C_word c,C_word *av) C_noret;
static void C_ccall trf_8758(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8758(t0,t1,t2);}

C_noret_decl(trf_8827)
static void C_ccall trf_8827(C_word c,C_word *av) C_noret;
static void C_ccall trf_8827(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_8827(t0,t1,t2,t3,t4);}

C_noret_decl(trf_8839)
static void C_ccall trf_8839(C_word c,C_word *av) C_noret;
static void C_ccall trf_8839(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_8839(t0,t1,t2,t3,t4);}

C_noret_decl(trf_8847)
static void C_ccall trf_8847(C_word c,C_word *av) C_noret;
static void C_ccall trf_8847(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8847(t0,t1,t2);}

C_noret_decl(trf_8870)
static void C_ccall trf_8870(C_word c,C_word *av) C_noret;
static void C_ccall trf_8870(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8870(t0,t1,t2);}

C_noret_decl(trf_8898)
static void C_ccall trf_8898(C_word c,C_word *av) C_noret;
static void C_ccall trf_8898(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8898(t0,t1,t2);}

C_noret_decl(trf_8939)
static void C_ccall trf_8939(C_word c,C_word *av) C_noret;
static void C_ccall trf_8939(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8939(t0,t1,t2);}

C_noret_decl(trf_9010)
static void C_ccall trf_9010(C_word c,C_word *av) C_noret;
static void C_ccall trf_9010(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_9010(t0,t1,t2,t3,t4);}

C_noret_decl(trf_9022)
static void C_ccall trf_9022(C_word c,C_word *av) C_noret;
static void C_ccall trf_9022(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_9022(t0,t1,t2,t3,t4);}

C_noret_decl(trf_9030)
static void C_ccall trf_9030(C_word c,C_word *av) C_noret;
static void C_ccall trf_9030(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9030(t0,t1,t2);}

C_noret_decl(trf_9062)
static void C_ccall trf_9062(C_word c,C_word *av) C_noret;
static void C_ccall trf_9062(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9062(t0,t1,t2);}

C_noret_decl(trf_9085)
static void C_ccall trf_9085(C_word c,C_word *av) C_noret;
static void C_ccall trf_9085(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9085(t0,t1,t2);}

C_noret_decl(trf_9124)
static void C_ccall trf_9124(C_word c,C_word *av) C_noret;
static void C_ccall trf_9124(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9124(t0,t1,t2);}

C_noret_decl(trf_9177)
static void C_ccall trf_9177(C_word c,C_word *av) C_noret;
static void C_ccall trf_9177(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9177(t0,t1,t2);}

C_noret_decl(trf_9257)
static void C_ccall trf_9257(C_word c,C_word *av) C_noret;
static void C_ccall trf_9257(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9257(t0,t1,t2);}

C_noret_decl(trf_9312)
static void C_ccall trf_9312(C_word c,C_word *av) C_noret;
static void C_ccall trf_9312(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9312(t0,t1,t2);}

C_noret_decl(trf_9346)
static void C_ccall trf_9346(C_word c,C_word *av) C_noret;
static void C_ccall trf_9346(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9346(t0,t1,t2);}

C_noret_decl(trf_9400)
static void C_ccall trf_9400(C_word c,C_word *av) C_noret;
static void C_ccall trf_9400(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9400(t0,t1,t2);}

C_noret_decl(trf_9436)
static void C_ccall trf_9436(C_word c,C_word *av) C_noret;
static void C_ccall trf_9436(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9436(t0,t1,t2);}

C_noret_decl(trf_9476)
static void C_ccall trf_9476(C_word c,C_word *av) C_noret;
static void C_ccall trf_9476(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9476(t0,t1,t2);}

C_noret_decl(trf_9521)
static void C_ccall trf_9521(C_word c,C_word *av) C_noret;
static void C_ccall trf_9521(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9521(t0,t1,t2);}

C_noret_decl(trf_9566)
static void C_ccall trf_9566(C_word c,C_word *av) C_noret;
static void C_ccall trf_9566(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_9566(t0,t1);}

C_noret_decl(trf_9644)
static void C_ccall trf_9644(C_word c,C_word *av) C_noret;
static void C_ccall trf_9644(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9644(t0,t1,t2);}

C_noret_decl(trf_9678)
static void C_ccall trf_9678(C_word c,C_word *av) C_noret;
static void C_ccall trf_9678(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9678(t0,t1,t2);}

C_noret_decl(trf_9748)
static void C_ccall trf_9748(C_word c,C_word *av) C_noret;
static void C_ccall trf_9748(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9748(t0,t1,t2);}

C_noret_decl(trf_9782)
static void C_ccall trf_9782(C_word c,C_word *av) C_noret;
static void C_ccall trf_9782(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9782(t0,t1,t2);}

C_noret_decl(trf_9822)
static void C_ccall trf_9822(C_word c,C_word *av) C_noret;
static void C_ccall trf_9822(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9822(t0,t1,t2);}

C_noret_decl(trf_9845)
static void C_ccall trf_9845(C_word c,C_word *av) C_noret;
static void C_ccall trf_9845(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9845(t0,t1,t2);}

C_noret_decl(trf_9901)
static void C_ccall trf_9901(C_word c,C_word *av) C_noret;
static void C_ccall trf_9901(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9901(t0,t1,t2);}

C_noret_decl(trf_9922)
static void C_ccall trf_9922(C_word c,C_word *av) C_noret;
static void C_ccall trf_9922(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9922(t0,t1,t2);}

C_noret_decl(trf_9930)
static void C_ccall trf_9930(C_word c,C_word *av) C_noret;
static void C_ccall trf_9930(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9930(t0,t1,t2);}

C_noret_decl(trf_9971)
static void C_ccall trf_9971(C_word c,C_word *av) C_noret;
static void C_ccall trf_9971(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9971(t0,t1,t2);}

/* f12022 in k7730 in k7727 in k7724 in k7721 in k7717 in g1625 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f12022(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f12022,c,av);}
/* modules.scm:534: ##sys#warn */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[38]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[38]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
tp(3,av2);}}

/* f12847 in k6474 in g1148 in k6560 in loop2 in loop in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f12847(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f12847,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* f12851 in g1148 in k6560 in loop2 in loop in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f12851(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f12851,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* err in ##sys#validate-exports in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_10002(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,4)))){
C_save_and_reclaim_args((void *)trf_10002,3,t0,t1,t2);}{
C_word av2[5];
av2[0]=0;
av2[1]=t1;
av2[2]=*((C_word*)lf[127]+1);
av2[3]=((C_word*)t0)[2];
av2[4]=t2;
C_apply(5,av2);}}

/* iface in ##sys#validate-exports in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_10008(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,2)))){
C_save_and_reclaim_args((void *)trf_10008,3,t0,t1,t2);}
a=C_alloc(9);
t3=C_i_getprop(t2,lf[157],C_SCHEME_FALSE);
if(C_truep(t3)){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
/* modules.scm:811: err */
t4=((C_word*)((C_word*)t0)[2])[1];
f_10002(t4,t1,C_a_i_list(&a,3,lf[158],t2,((C_word*)t0)[3]));}}

/* loop in ##sys#validate-exports in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_10049(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(11,0,3)))){
C_save_and_reclaim_args((void *)trf_10049,3,t0,t1,t2);}
a=C_alloc(11);
if(C_truep(C_i_nullp(t2))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_i_pairp(t2);
if(C_truep(C_i_not(t3))){
/* modules.scm:820: err */
t4=((C_word*)((C_word*)t0)[2])[1];
f_10002(t4,t1,C_a_i_list(&a,2,lf[161],((C_word*)t0)[3]));}
else{
t4=C_i_car(t2);
if(C_truep(C_i_symbolp(t4))){
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10081,a[2]=t1,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* modules.scm:823: loop */
t13=t5;
t14=C_u_i_cdr(t2);
t1=t13;
t2=t14;
goto loop;}
else{
t5=C_i_listp(t4);
if(C_truep(C_i_not(t5))){
/* modules.scm:825: err */
t6=((C_word*)((C_word*)t0)[2])[1];
f_10002(t6,t1,C_a_i_list(&a,3,lf[162],t4,((C_word*)t0)[3]));}
else{
t6=C_i_car(t4);
t7=C_eqp(lf[163],t6);
if(C_truep(t7)){
t8=C_u_i_cdr(t4);
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10107,a[2]=t1,a[3]=t8,tmp=(C_word)a,a+=4,tmp);
/* modules.scm:827: loop */
t13=t9;
t14=C_u_i_cdr(t2);
t1=t13;
t2=t14;
goto loop;}
else{
t8=C_eqp(lf[164],C_u_i_car(t4));
if(C_truep(t8)){
t9=C_i_pairp(C_u_i_cdr(t4));
t10=(C_truep(t9)?C_i_symbolp(C_i_cadr(t4)):C_SCHEME_FALSE);
if(C_truep(t10)){
t11=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10128,a[2]=t1,a[3]=((C_word*)t0)[4],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* modules.scm:830: iface */
t12=((C_word*)((C_word*)t0)[5])[1];
f_10008(t12,t11,C_i_cadr(t4));}
else{
/* modules.scm:831: err */
t11=((C_word*)((C_word*)t0)[2])[1];
f_10002(t11,t1,C_a_i_list(&a,3,lf[165],t4,((C_word*)t0)[3]));}}
else{
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_10158,a[2]=t4,a[3]=((C_word*)t0)[4],a[4]=t2,a[5]=t10,a[6]=((C_word*)t0)[2],a[7]=((C_word*)t0)[3],a[8]=((C_word)li142),tmp=(C_word)a,a+=9,tmp));
t12=((C_word*)t10)[1];
f_10158(t12,t1,t4);}}}}}}}

/* k10079 in loop in ##sys#validate-exports in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10081(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_10081,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k10105 in loop in ##sys#validate-exports in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10107(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_10107,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k10126 in loop in ##sys#validate-exports in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10128(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_10128,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10132,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* modules.scm:830: loop */
t3=((C_word*)((C_word*)t0)[3])[1];
f_10049(t3,t2,C_u_i_cdr(((C_word*)t0)[4]));}

/* k10130 in k10126 in loop in ##sys#validate-exports in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10132(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_10132,c,av);}
/* modules.scm:830: scheme#append */
t2=*((C_word*)lf[19]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* loop2 in loop in ##sys#validate-exports in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_10158(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(9,0,2)))){
C_save_and_reclaim_args((void *)trf_10158,3,t0,t1,t2);}
a=C_alloc(9);
if(C_truep(C_i_nullp(t2))){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10172,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* modules.scm:834: loop */
t4=((C_word*)((C_word*)t0)[3])[1];
f_10049(t4,t3,C_i_cdr(((C_word*)t0)[4]));}
else{
t3=C_i_car(t2);
if(C_truep(C_i_symbolp(t3))){
/* modules.scm:835: loop2 */
t5=t1;
t6=C_u_i_cdr(t2);
t1=t5;
t2=t6;
goto loop;}
else{
/* modules.scm:836: err */
t4=((C_word*)((C_word*)t0)[6])[1];
f_10002(t4,t1,C_a_i_list(&a,3,lf[166],((C_word*)t0)[2],((C_word*)t0)[7]));}}}

/* k10170 in loop2 in loop in ##sys#validate-exports in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10172(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_10172,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* ##sys#register-functor in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10214(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(c!=6) C_bad_argc_2(c,6,t0);
if(C_unlikely(!C_demand(C_calculate_demand(14,c,1)))){
C_save_and_reclaim((void *)f_10214,c,av);}
a=C_alloc(14);
t6=C_a_i_cons(&a,2,t4,t5);
t7=C_a_i_cons(&a,2,t3,t6);
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=C_a_i_putprop(&a,3,t2,lf[69],t7);
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}

/* ##sys#instantiate-functor in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10230(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(18,c,3)))){
C_save_and_reclaim((void *)f_10230,c,av);}
a=C_alloc(18);
t5=C_i_getprop(t3,lf[69],C_SCHEME_FALSE);
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10238,a[2]=t2,a[3]=((C_word)li146),tmp=(C_word)a,a+=4,tmp);
t7=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_10245,a[2]=t5,a[3]=t2,a[4]=t4,a[5]=t3,a[6]=t6,a[7]=t1,tmp=(C_word)a,a+=8,tmp);
if(C_truep(t5)){
t8=t7;{
C_word *av2=av;
av2[0]=t8;
av2[1]=C_SCHEME_UNDEFINED;
f_10245(2,av2);}}
else{
/* modules.scm:845: err */
t8=t6;
f_10238(t8,t7,C_a_i_list(&a,2,lf[173],t3));}}

/* err in ##sys#instantiate-functor in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_10238(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,4)))){
C_save_and_reclaim_args((void *)trf_10238,3,t0,t1,t2);}{
C_word av2[5];
av2[0]=0;
av2[1]=t1;
av2[2]=*((C_word*)lf[127]+1);
av2[3]=((C_word*)t0)[2];
av2[4]=t2;
C_apply(5,av2);}}

/* k10243 in ##sys#instantiate-functor in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10245(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(23,c,4)))){
C_save_and_reclaim((void *)f_10245,c,av);}
a=C_alloc(23);
t2=C_i_car(((C_word*)t0)[2]);
t3=C_i_cadr(((C_word*)t0)[2]);
t4=C_u_i_cdr(((C_word*)t0)[2]);
t5=C_u_i_cdr(t4);
t6=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_10255,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t2,a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word)li148),tmp=(C_word)a,a+=8,tmp);
t7=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10318,a[2]=t3,a[3]=t5,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[7],tmp=(C_word)a,a+=6,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10339,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[5],a[4]=t6,a[5]=t9,a[6]=((C_word)li150),tmp=(C_word)a,a+=7,tmp));
t11=((C_word*)t9)[1];
f_10339(t11,t7,((C_word*)t0)[4],t2);}

/* merr in k10243 in ##sys#instantiate-functor in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_10255(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(22,0,3)))){
C_save_and_reclaim_args((void *)trf_10255,2,t0,t1);}
a=C_alloc(22);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],((C_word*)t0)[3]);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=C_i_check_list_2(((C_word*)t0)[4],lf[18]);
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10277,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10279,a[2]=t5,a[3]=t10,a[4]=t6,a[5]=((C_word)li147),tmp=(C_word)a,a+=6,tmp));
t12=((C_word*)t10)[1];
f_10279(t12,t8,((C_word*)t0)[4]);}

/* k10275 in merr in k10243 in ##sys#instantiate-functor in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10277(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_10277,c,av);}
a=C_alloc(12);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
/* modules.scm:850: err */
t3=((C_word*)t0)[3];
f_10238(t3,((C_word*)t0)[4],C_a_i_list(&a,3,lf[169],((C_word*)t0)[5],t2));}

/* map-loop2582 in merr in k10243 in ##sys#instantiate-functor in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_10279(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_10279,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_car(t3);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k10316 in k10243 in ##sys#instantiate-functor in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10318(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,1)))){
C_save_and_reclaim((void *)f_10318,c,av);}
a=C_alloc(18);
t2=C_eqp(lf[159],((C_word*)t0)[2]);
t3=(C_truep(t2)?C_a_i_cons(&a,2,C_SCHEME_TRUE,((C_word*)t0)[3]):C_a_i_cons(&a,2,((C_word*)t0)[2],((C_word*)t0)[3]));
t4=C_a_i_cons(&a,2,((C_word*)t0)[4],t3);
t5=C_a_i_cons(&a,2,lf[170],t4);
t6=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_a_i_list(&a,3,lf[171],t1,t5);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}

/* loop in k10243 in ##sys#instantiate-functor in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_10339(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,3)))){
C_save_and_reclaim_args((void *)trf_10339,4,t0,t1,t2,t3);}
a=C_alloc(10);
if(C_truep(C_i_nullp(t2))){
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10351,a[2]=t5,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word)li149),tmp=(C_word)a,a+=7,tmp));
t7=((C_word*)t5)[1];
f_10351(t7,t1,t3);}
else{
if(C_truep(C_i_nullp(t3))){
/* modules.scm:869: merr */
t4=((C_word*)t0)[4];
f_10255(t4,t1);}
else{
t4=C_i_car(t3);
t5=C_i_car(t4);
t6=C_u_i_cdr(t4);
t7=C_i_pairp(t5);
t8=(C_truep(t7)?C_i_car(t5):t5);
t9=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_10426,a[2]=t8,a[3]=t1,a[4]=((C_word*)t0)[5],a[5]=t2,a[6]=t3,a[7]=((C_word*)t0)[2],a[8]=t6,a[9]=((C_word*)t0)[3],tmp=(C_word)a,a+=10,tmp);
/* modules.scm:877: chicken.internal#library-id */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[23]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[23]+1);
av2[1]=t9;
av2[2]=C_i_car(t2);
tp(3,av2);}}}}

/* loop2 in loop in k10243 in ##sys#instantiate-functor in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_10351(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,2)))){
C_save_and_reclaim_args((void *)trf_10351,3,t0,t1,t2);}
a=C_alloc(12);
if(C_truep(C_i_nullp(t2))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_i_car(t2);
t4=C_i_car(t3);
if(C_truep(C_i_pairp(t4))){
t5=C_u_i_cdr(t3);
t6=C_i_caar(t3);
t7=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_10374,a[2]=t6,a[3]=t1,a[4]=((C_word*)t0)[2],a[5]=t2,a[6]=((C_word*)t0)[3],a[7]=t5,a[8]=((C_word*)t0)[4],tmp=(C_word)a,a+=9,tmp);
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10394,a[2]=t7,tmp=(C_word)a,a+=3,tmp);
/* modules.scm:863: scheme#cadar */
t9=*((C_word*)lf[85]+1);{
C_word av2[3];
av2[0]=t9;
av2[1]=t8;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}
else{
/* modules.scm:867: merr */
t5=((C_word*)t0)[5];
f_10255(t5,t1);}}}

/* k10372 in loop2 in loop in k10243 in ##sys#instantiate-functor in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10374(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,5)))){
C_save_and_reclaim((void *)f_10374,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10377,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* modules.scm:864: match-functor-argument */
f_10456(t2,((C_word*)t0)[6],t1,((C_word*)t0)[7],((C_word*)t0)[8]);}

/* k10375 in k10372 in loop2 in loop in k10243 in ##sys#instantiate-functor in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10377(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_10377,c,av);}
a=C_alloc(10);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],((C_word*)t0)[3]);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10388,a[2]=((C_word*)t0)[4],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* modules.scm:865: loop2 */
t4=((C_word*)((C_word*)t0)[5])[1];
f_10351(t4,t3,C_u_i_cdr(((C_word*)t0)[6]));}

/* k10386 in k10375 in k10372 in loop2 in loop in k10243 in ##sys#instantiate-functor in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10388(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_10388,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k10392 in loop2 in loop in k10243 in ##sys#instantiate-functor in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10394(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_10394,c,av);}
/* modules.scm:863: chicken.internal#library-id */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[23]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[23]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
tp(3,av2);}}

/* k10424 in loop in k10243 in ##sys#instantiate-functor in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10426(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,5)))){
C_save_and_reclaim((void *)f_10426,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_10429,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* modules.scm:878: match-functor-argument */
f_10456(t2,((C_word*)t0)[7],t1,((C_word*)t0)[8],((C_word*)t0)[9]);}

/* k10427 in k10424 in loop in k10243 in ##sys#instantiate-functor in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10429(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_10429,c,av);}
a=C_alloc(10);
t2=C_a_i_list2(&a,2,((C_word*)t0)[2],((C_word*)t0)[3]);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10440,a[2]=((C_word*)t0)[4],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* modules.scm:880: loop */
t4=((C_word*)((C_word*)t0)[5])[1];
f_10339(t4,t3,C_u_i_cdr(((C_word*)t0)[6]),C_u_i_cdr(((C_word*)t0)[7]));}

/* k10438 in k10427 in k10424 in loop in k10243 in ##sys#instantiate-functor in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10440(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_10440,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* match-functor-argument in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_10456(C_word t1,C_word t2,C_word t3,C_word t4,C_word t5){
C_word tmp;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,3)))){
C_save_and_reclaim_args((void *)trf_10456,5,t1,t2,t3,t4,t5);}
a=C_alloc(10);
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10460,a[2]=t4,a[3]=t1,a[4]=t5,a[5]=t2,a[6]=t3,tmp=(C_word)a,a+=7,tmp);
t7=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10613,a[2]=t6,tmp=(C_word)a,a+=3,tmp);
/* modules.scm:887: ##sys#resolve-module-name */
t8=*((C_word*)lf[20]+1);{
C_word av2[4];
av2[0]=t8;
av2[1]=t7;
av2[2]=t3;
av2[3]=lf[4];
((C_proc)(void*)(*((C_word*)t8+1)))(4,av2);}}

/* k10458 in match-functor-argument in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10460(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(21,c,3)))){
C_save_and_reclaim((void *)f_10460,c,av);}
a=C_alloc(21);
t2=C_eqp(((C_word*)t0)[2],lf[159]);
if(C_truep(t2)){
t3=C_SCHEME_UNDEFINED;
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=C_SCHEME_END_OF_LIST;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10467,a[2]=t1,a[3]=t4,a[4]=((C_word)li152),tmp=(C_word)a,a+=5,tmp);
t6=C_i_check_list_2(((C_word*)t0)[2],lf[33]);
t7=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10507,a[2]=t4,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10588,a[2]=t9,a[3]=t5,a[4]=((C_word)li154),tmp=(C_word)a,a+=5,tmp));
t11=((C_word*)t9)[1];
f_10588(t11,t7,((C_word*)t0)[2]);}}

/* g2643 in k10458 in match-functor-argument in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_10467(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_10467,3,t0,t1,t2);}
a=C_alloc(5);
t3=C_i_symbolp(t2);
t4=(C_truep(t3)?t2:C_i_car(t2));
t5=C_i_check_structure_2(((C_word*)t0)[2],lf[4],lf[13]);
t6=C_i_block_ref(((C_word*)t0)[2],C_fix(11));
t7=C_i_assq(t4,t6);
t8=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10480,a[2]=t1,a[3]=t4,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
if(C_truep(t7)){
t9=t8;
f_10480(t9,t7);}
else{
t9=C_i_check_structure_2(((C_word*)t0)[2],lf[4],lf[14]);
t10=t8;
f_10480(t10,C_i_assq(t4,C_i_block_ref(((C_word*)t0)[2],C_fix(12))));}}

/* k10478 in g2643 in k10458 in match-functor-argument in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_10480(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,0,1)))){
C_save_and_reclaim_args((void *)trf_10480,2,t0,t1);}
a=C_alloc(3);
if(C_truep(t1)){
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=C_a_i_cons(&a,2,((C_word*)t0)[3],((C_word*)((C_word*)t0)[4])[1]);
t3=C_mutate(((C_word *)((C_word*)t0)[4])+1,t2);
t4=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k10505 in k10458 in match-functor-argument in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10507(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_10507,c,av);}
a=C_alloc(9);
if(C_truep(C_i_pairp(((C_word*)((C_word*)t0)[2])[1]))){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10520,a[2]=((C_word*)t0)[3],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10524,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* modules.scm:902: scheme#symbol->string */
t4=*((C_word*)lf[98]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k10518 in k10505 in k10458 in match-functor-argument in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10520(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_10520,c,av);}
/* modules.scm:898: ##sys#syntax-error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[127]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[127]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=lf[4];
av2[3]=t1;
tp(4,av2);}}

/* k10522 in k10505 in k10458 in match-functor-argument in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10524(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_10524,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10528,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* modules.scm:903: scheme#symbol->string */
t3=*((C_word*)lf[98]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k10526 in k10522 in k10505 in k10458 in match-functor-argument in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10528(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_10528,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10532,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
/* modules.scm:904: scheme#symbol->string */
t3=*((C_word*)lf[98]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k10530 in k10526 in k10522 in k10505 in k10458 in match-functor-argument in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10532(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(19,c,3)))){
C_save_and_reclaim((void *)f_10532,c,av);}
a=C_alloc(19);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=((C_word*)((C_word*)t0)[2])[1];
t7=C_i_check_list_2(t6,lf[18]);
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10552,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10554,a[2]=t4,a[3]=t10,a[4]=t5,a[5]=((C_word)li153),tmp=(C_word)a,a+=6,tmp));
t12=((C_word*)t10)[1];
f_10554(t12,t8,t6);}

/* k10544 in map-loop2664 in k10530 in k10526 in k10522 in k10505 in k10458 in match-functor-argument in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10546(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_10546,c,av);}
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[118]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[118]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=lf[179];
av2[3]=t1;
tp(4,av2);}}

/* k10550 in k10530 in k10526 in k10522 in k10505 in k10458 in match-functor-argument in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10552(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,11)))){
C_save_and_reclaim((void *)f_10552,c,av);}{
C_word *av2;
if(c >= 12) {
  av2=av;
} else {
  av2=C_alloc(12);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=*((C_word*)lf[95]+1);
av2[3]=lf[174];
av2[4]=((C_word*)t0)[3];
av2[5]=lf[175];
av2[6]=lf[176];
av2[7]=((C_word*)t0)[4];
av2[8]=lf[177];
av2[9]=((C_word*)t0)[5];
av2[10]=lf[178];
av2[11]=t1;
C_apply(12,av2);}}

/* map-loop2664 in k10530 in k10526 in k10522 in k10505 in k10458 in match-functor-argument in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_10554(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,2)))){
C_save_and_reclaim_args((void *)trf_10554,3,t0,t1,t2);}
a=C_alloc(9);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10579,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
t4=C_slot(t2,C_fix(0));
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10546,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* modules.scm:905: scheme#symbol->string */
t6=*((C_word*)lf[98]+1);{
C_word av2[3];
av2[0]=t6;
av2[1]=t5;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k10577 in map-loop2664 in k10530 in k10526 in k10522 in k10505 in k10458 in match-functor-argument in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10579(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_10579,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_10554(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* for-each-loop2642 in k10458 in match-functor-argument in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_10588(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_10588,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10598,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* modules.scm:890: g2643 */
t4=((C_word*)t0)[3];
f_10467(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k10596 in for-each-loop2642 in k10458 in match-functor-argument in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10598(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_10598,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_10588(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k10611 in match-functor-argument in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10613(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_10613,c,av);}
/* modules.scm:887: ##sys#find-module */
t2=*((C_word*)lf[24]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_SCHEME_TRUE;
av2[4]=lf[4];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10616(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_10616,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10619,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11448,a[2]=t2,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* modules.scm:1040: scheme#append */
t4=*((C_word*)lf[19]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[249];
av2[3]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10619(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,5)))){
C_save_and_reclaim((void *)f_10619,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10622,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* modules.scm:1046: ##sys#register-core-module */
t3=*((C_word*)lf[77]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[248];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_END_OF_LIST;
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10622(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_10622,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10625,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* modules.scm:1047: ##sys#register-core-module */
t3=*((C_word*)lf[77]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[247];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_END_OF_LIST;
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10625(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_10625,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10628,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* modules.scm:1049: ##sys#register-module-alias */
t3=*((C_word*)lf[15]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[245];
av2[3]=lf[246];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10628(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_10628,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10631,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* modules.scm:1050: ##sys#register-module-alias */
t3=*((C_word*)lf[15]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[197];
av2[3]=lf[244];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10631(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_10631,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10634,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11444,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* modules.scm:1056: ##sys#macro-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[28]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[28]+1);
av2[1]=t3;
tp(2,av2);}}

/* k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10634(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_10634,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10637,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* modules.scm:1058: ##sys#register-core-module */
t3=*((C_word*)lf[77]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[241];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_END_OF_LIST;
av2[5]=*((C_word*)lf[242]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10637(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_10637,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10640,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* modules.scm:1061: ##sys#register-core-module */
t3=*((C_word*)lf[77]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[239];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_END_OF_LIST;
av2[5]=*((C_word*)lf[240]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10640(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,3)))){
C_save_and_reclaim((void *)f_10640,c,av);}
a=C_alloc(24);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10643,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11440,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=lf[238];
t5=*((C_word*)lf[215]+1);
t6=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t7=t6;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=((C_word*)t8)[1];
t10=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11393,a[2]=t5,a[3]=((C_word)li181),tmp=(C_word)a,a+=4,tmp);
t11=C_SCHEME_UNDEFINED;
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11405,a[2]=t10,a[3]=t8,a[4]=t13,a[5]=t9,a[6]=((C_word)li182),tmp=(C_word)a,a+=7,tmp));
t15=((C_word*)t13)[1];
f_11405(t15,t3,t4);}

/* k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10643(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,3)))){
C_save_and_reclaim((void *)f_10643,c,av);}
a=C_alloc(24);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10646,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11386,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=lf[236];
t5=*((C_word*)lf[193]+1);
t6=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t7=t6;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=((C_word*)t8)[1];
t10=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11339,a[2]=t5,a[3]=((C_word)li179),tmp=(C_word)a,a+=4,tmp);
t11=C_SCHEME_UNDEFINED;
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11351,a[2]=t10,a[3]=t8,a[4]=t13,a[5]=t9,a[6]=((C_word)li180),tmp=(C_word)a,a+=7,tmp));
t15=((C_word*)t13)[1];
f_11351(t15,t3,t4);}

/* k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10646(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_10646,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10649,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* modules.scm:1070: ##sys#register-core-module */
t3=*((C_word*)lf[77]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[233];
av2[3]=lf[191];
av2[4]=lf[234];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10649(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,3)))){
C_save_and_reclaim((void *)f_10649,c,av);}
a=C_alloc(24);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10652,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11332,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=lf[232];
t5=*((C_word*)lf[193]+1);
t6=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t7=t6;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=((C_word*)t8)[1];
t10=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11285,a[2]=t5,a[3]=((C_word)li177),tmp=(C_word)a,a+=4,tmp);
t11=C_SCHEME_UNDEFINED;
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11297,a[2]=t10,a[3]=t8,a[4]=t13,a[5]=t9,a[6]=((C_word)li178),tmp=(C_word)a,a+=7,tmp));
t15=((C_word*)t13)[1];
f_11297(t15,t3,t4);}

/* k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10652(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,3)))){
C_save_and_reclaim((void *)f_10652,c,av);}
a=C_alloc(24);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10655,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11278,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=lf[230];
t5=*((C_word*)lf[193]+1);
t6=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t7=t6;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=((C_word*)t8)[1];
t10=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11231,a[2]=t5,a[3]=((C_word)li175),tmp=(C_word)a,a+=4,tmp);
t11=C_SCHEME_UNDEFINED;
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11243,a[2]=t10,a[3]=t8,a[4]=t13,a[5]=t9,a[6]=((C_word)li176),tmp=(C_word)a,a+=7,tmp));
t15=((C_word*)t13)[1];
f_11243(t15,t3,t4);}

/* k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10655(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_10655,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10658,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* modules.scm:1082: ##sys#register-core-module */
t3=*((C_word*)lf[77]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[226];
av2[3]=lf[227];
av2[4]=lf[228];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_10658(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,3)))){
C_save_and_reclaim((void *)f_10658,c,av);}
a=C_alloc(24);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10661,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11224,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=lf[225];
t5=*((C_word*)lf[193]+1);
t6=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t7=t6;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=((C_word*)t8)[1];
t10=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11177,a[2]=t5,a[3]=((C_word)li173),tmp=(C_word)a,a+=4,tmp);
t11=C_SCHEME_UNDEFINED;
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11189,a[2]=t10,a[3]=t8,a[4]=t13,a[5]=t9,a[6]=((C_word)li174),tmp=(C_word)a,a+=7,tmp));
t15=((C_word*)t13)[1];
f_11189(t15,t3,t4);}

/* k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 in ... */
static void C_ccall f_10661(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,3)))){
C_save_and_reclaim((void *)f_10661,c,av);}
a=C_alloc(24);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10664,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11170,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=lf[222];
t5=*((C_word*)lf[223]+1);
t6=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t7=t6;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=((C_word*)t8)[1];
t10=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11123,a[2]=t5,a[3]=((C_word)li171),tmp=(C_word)a,a+=4,tmp);
t11=C_SCHEME_UNDEFINED;
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11135,a[2]=t10,a[3]=t8,a[4]=t13,a[5]=t9,a[6]=((C_word)li172),tmp=(C_word)a,a+=7,tmp));
t15=((C_word*)t13)[1];
f_11135(t15,t3,t4);}

/* k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in ... */
static void C_ccall f_10664(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,3)))){
C_save_and_reclaim((void *)f_10664,c,av);}
a=C_alloc(24);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10667,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11116,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=lf[219];
t5=*((C_word*)lf[193]+1);
t6=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t7=t6;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=((C_word*)t8)[1];
t10=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11069,a[2]=t5,a[3]=((C_word)li169),tmp=(C_word)a,a+=4,tmp);
t11=C_SCHEME_UNDEFINED;
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11081,a[2]=t10,a[3]=t8,a[4]=t13,a[5]=t9,a[6]=((C_word)li170),tmp=(C_word)a,a+=7,tmp));
t15=((C_word*)t13)[1];
f_11081(t15,t3,t4);}

/* k10665 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in ... */
static void C_ccall f_10667(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,3)))){
C_save_and_reclaim((void *)f_10667,c,av);}
a=C_alloc(24);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10670,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11062,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=lf[217];
t5=*((C_word*)lf[193]+1);
t6=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t7=t6;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=((C_word*)t8)[1];
t10=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11015,a[2]=t5,a[3]=((C_word)li167),tmp=(C_word)a,a+=4,tmp);
t11=C_SCHEME_UNDEFINED;
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11027,a[2]=t10,a[3]=t8,a[4]=t13,a[5]=t9,a[6]=((C_word)li168),tmp=(C_word)a,a+=7,tmp));
t15=((C_word*)t13)[1];
f_11027(t15,t3,t4);}

/* k10668 in k10665 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in ... */
static void C_ccall f_10670(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,3)))){
C_save_and_reclaim((void *)f_10670,c,av);}
a=C_alloc(24);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10673,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11008,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=lf[214];
t5=*((C_word*)lf[215]+1);
t6=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t7=t6;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=((C_word*)t8)[1];
t10=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10961,a[2]=t5,a[3]=((C_word)li165),tmp=(C_word)a,a+=4,tmp);
t11=C_SCHEME_UNDEFINED;
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10973,a[2]=t10,a[3]=t8,a[4]=t13,a[5]=t9,a[6]=((C_word)li166),tmp=(C_word)a,a+=7,tmp));
t15=((C_word*)t13)[1];
f_10973(t15,t3,t4);}

/* k10671 in k10668 in k10665 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in ... */
static void C_ccall f_10673(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_10673,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10676,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* modules.scm:1110: ##sys#register-core-module */
t3=*((C_word*)lf[77]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[211];
av2[3]=lf[191];
av2[4]=lf[212];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k10674 in k10671 in k10668 in k10665 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in ... */
static void C_ccall f_10676(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,3)))){
C_save_and_reclaim((void *)f_10676,c,av);}
a=C_alloc(24);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10679,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10954,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=lf[210];
t5=*((C_word*)lf[193]+1);
t6=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t7=t6;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=((C_word*)t8)[1];
t10=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10907,a[2]=t5,a[3]=((C_word)li163),tmp=(C_word)a,a+=4,tmp);
t11=C_SCHEME_UNDEFINED;
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10919,a[2]=t10,a[3]=t8,a[4]=t13,a[5]=t9,a[6]=((C_word)li164),tmp=(C_word)a,a+=7,tmp));
t15=((C_word*)t13)[1];
f_10919(t15,t3,t4);}

/* k10677 in k10674 in k10671 in k10668 in k10665 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in ... */
static void C_ccall f_10679(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_10679,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10682,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* modules.scm:1116: ##sys#register-core-module */
t3=*((C_word*)lf[77]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[206];
av2[3]=lf[207];
av2[4]=lf[208];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k10680 in k10677 in k10674 in k10671 in k10668 in k10665 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in ... */
static void C_ccall f_10682(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,3)))){
C_save_and_reclaim((void *)f_10682,c,av);}
a=C_alloc(24);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10685,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10900,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=lf[205];
t5=*((C_word*)lf[193]+1);
t6=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t7=t6;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=((C_word*)t8)[1];
t10=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10853,a[2]=t5,a[3]=((C_word)li161),tmp=(C_word)a,a+=4,tmp);
t11=C_SCHEME_UNDEFINED;
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10865,a[2]=t10,a[3]=t8,a[4]=t13,a[5]=t9,a[6]=((C_word)li162),tmp=(C_word)a,a+=7,tmp));
t15=((C_word*)t13)[1];
f_10865(t15,t3,t4);}

/* k10683 in k10680 in k10677 in k10674 in k10671 in k10668 in k10665 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in ... */
static void C_ccall f_10685(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,3)))){
C_save_and_reclaim((void *)f_10685,c,av);}
a=C_alloc(24);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10688,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10846,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=lf[203];
t5=*((C_word*)lf[193]+1);
t6=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t7=t6;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=((C_word*)t8)[1];
t10=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10799,a[2]=t5,a[3]=((C_word)li159),tmp=(C_word)a,a+=4,tmp);
t11=C_SCHEME_UNDEFINED;
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10811,a[2]=t10,a[3]=t8,a[4]=t13,a[5]=t9,a[6]=((C_word)li160),tmp=(C_word)a,a+=7,tmp));
t15=((C_word*)t13)[1];
f_10811(t15,t3,t4);}

/* k10686 in k10683 in k10680 in k10677 in k10674 in k10671 in k10668 in k10665 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in ... */
static void C_ccall f_10688(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,3)))){
C_save_and_reclaim((void *)f_10688,c,av);}
a=C_alloc(24);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10691,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10792,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=lf[200];
t5=*((C_word*)lf[193]+1);
t6=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t7=t6;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=((C_word*)t8)[1];
t10=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10745,a[2]=t5,a[3]=((C_word)li157),tmp=(C_word)a,a+=4,tmp);
t11=C_SCHEME_UNDEFINED;
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10757,a[2]=t10,a[3]=t8,a[4]=t13,a[5]=t9,a[6]=((C_word)li158),tmp=(C_word)a,a+=7,tmp));
t15=((C_word*)t13)[1];
f_10757(t15,t3,t4);}

/* k10689 in k10686 in k10683 in k10680 in k10677 in k10674 in k10671 in k10668 in k10665 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in ... */
static void C_ccall f_10691(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_10691,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10694,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* modules.scm:1129: ##sys#register-core-module */
t3=*((C_word*)lf[77]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[197];
av2[3]=lf[191];
av2[4]=lf[198];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k10692 in k10689 in k10686 in k10683 in k10680 in k10677 in k10674 in k10671 in k10668 in k10665 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in ... */
static void C_ccall f_10694(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_10694,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10697,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* modules.scm:1135: ##sys#register-core-module */
t3=*((C_word*)lf[77]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[194];
av2[3]=lf[195];
av2[4]=lf[196];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k10695 in k10692 in k10689 in k10686 in k10683 in k10680 in k10677 in k10674 in k10671 in k10668 in k10665 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in ... */
static void C_ccall f_10697(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,5)))){
C_save_and_reclaim((void *)f_10697,c,av);}
a=C_alloc(6);
t2=C_mutate((C_word*)lf[182]+1 /* (set! chicken.module#module-environment ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10699,a[2]=((C_word)li156),tmp=(C_word)a,a+=3,tmp));
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10735,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* chicken.base.import.scm:26: ##sys#register-core-module */
t4=*((C_word*)lf[77]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[190];
av2[3]=lf[191];
av2[4]=lf[192];
av2[5]=*((C_word*)lf[193]+1);
((C_proc)(void*)(*((C_word*)t4+1)))(6,av2);}}

/* chicken.module#module-environment in k10695 in k10692 in k10689 in k10686 in k10683 in k10680 in k10677 in k10674 in k10671 in k10668 in k10665 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in ... */
static void C_ccall f_10699(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_10699,c,av);}
a=C_alloc(5);
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?t2:C_get_rest_arg(c,3,av,3,t0));
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10706,a[2]=t1,a[3]=t2,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
/* modules.scm:1141: find-module/import-library */
f_8476(t5,t2,lf[183]);}

/* k10704 in chicken.module#module-environment in k10695 in k10692 in k10689 in k10686 in k10683 in k10680 in k10677 in k10674 in k10671 in k10668 in k10665 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in ... */
static void C_ccall f_10706(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_10706,c,av);}
a=C_alloc(5);
if(C_truep(C_i_not(t1))){
/* modules.scm:1143: ##sys#syntax-error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[127]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[127]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=lf[183];
av2[3]=lf[184];
av2[4]=((C_word*)t0)[3];
tp(5,av2);}}
else{
t2=C_i_check_structure_2(t1,lf[4],lf[30]);
t3=C_i_block_ref(t1,C_fix(14));
t4=C_i_car(t3);
t5=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_a_i_record4(&a,4,lf[185],((C_word*)t0)[4],t4,C_SCHEME_TRUE);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* k10733 in k10695 in k10692 in k10689 in k10686 in k10683 in k10680 in k10677 in k10674 in k10671 in k10668 in k10665 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in ... */
static void C_ccall f_10735(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_10735,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10738,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* chicken.syntax.import.scm:30: ##sys#register-core-module */
t3=*((C_word*)lf[77]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[186];
av2[3]=lf[187];
av2[4]=lf[188];
av2[5]=*((C_word*)lf[189]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k10736 in k10733 in k10695 in k10692 in k10689 in k10686 in k10683 in k10680 in k10677 in k10674 in k10671 in k10668 in k10665 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in ... */
static void C_ccall f_10738(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_10738,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* g3163 in k10686 in k10683 in k10680 in k10677 in k10674 in k10671 in k10668 in k10665 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in ... */
static C_word C_fcall f_10745(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_stack_overflow_check;{}
return(C_i_assq(t1,((C_word*)t0)[2]));}

/* map-loop3157 in k10686 in k10683 in k10680 in k10677 in k10674 in k10671 in k10668 in k10665 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in ... */
static void C_fcall f_10757(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_10757,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=(
/* modules.scm:1052: g3163 */
  f_10745(((C_word*)t0)[2],C_slot(t2,C_fix(0)))
);
t4=C_a_i_cons(&a,2,t3,C_SCHEME_END_OF_LIST);
t5=C_i_setslot(((C_word*)((C_word*)t0)[3])[1],C_fix(1),t4);
t6=C_mutate(((C_word *)((C_word*)t0)[3])+1,t4);
t8=t1;
t9=C_slot(t2,C_fix(1));
t1=t8;
t2=t9;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k10790 in k10686 in k10683 in k10680 in k10677 in k10674 in k10671 in k10668 in k10665 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in ... */
static void C_ccall f_10792(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_10792,c,av);}
/* modules.scm:1126: ##sys#register-primitive-module */
t2=*((C_word*)lf[79]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[199];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* g3125 in k10683 in k10680 in k10677 in k10674 in k10671 in k10668 in k10665 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in ... */
static C_word C_fcall f_10799(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_stack_overflow_check;{}
return(C_i_assq(t1,((C_word*)t0)[2]));}

/* map-loop3119 in k10683 in k10680 in k10677 in k10674 in k10671 in k10668 in k10665 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in ... */
static void C_fcall f_10811(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_10811,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=(
/* modules.scm:1052: g3125 */
  f_10799(((C_word*)t0)[2],C_slot(t2,C_fix(0)))
);
t4=C_a_i_cons(&a,2,t3,C_SCHEME_END_OF_LIST);
t5=C_i_setslot(((C_word*)((C_word*)t0)[3])[1],C_fix(1),t4);
t6=C_mutate(((C_word *)((C_word*)t0)[3])+1,t4);
t8=t1;
t9=C_slot(t2,C_fix(1));
t1=t8;
t2=t9;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k10844 in k10683 in k10680 in k10677 in k10674 in k10671 in k10668 in k10665 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in ... */
static void C_ccall f_10846(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_10846,c,av);}
/* modules.scm:1122: ##sys#register-core-module */
t2=*((C_word*)lf[77]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[201];
av2[3]=lf[191];
av2[4]=lf[202];
av2[5]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* g3087 in k10680 in k10677 in k10674 in k10671 in k10668 in k10665 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in ... */
static C_word C_fcall f_10853(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_stack_overflow_check;{}
return(C_i_assq(t1,((C_word*)t0)[2]));}

/* map-loop3081 in k10680 in k10677 in k10674 in k10671 in k10668 in k10665 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in ... */
static void C_fcall f_10865(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_10865,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=(
/* modules.scm:1052: g3087 */
  f_10853(((C_word*)t0)[2],C_slot(t2,C_fix(0)))
);
t4=C_a_i_cons(&a,2,t3,C_SCHEME_END_OF_LIST);
t5=C_i_setslot(((C_word*)((C_word*)t0)[3])[1],C_fix(1),t4);
t6=C_mutate(((C_word *)((C_word*)t0)[3])+1,t4);
t8=t1;
t9=C_slot(t2,C_fix(1));
t1=t8;
t2=t9;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k10898 in k10680 in k10677 in k10674 in k10671 in k10668 in k10665 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in ... */
static void C_ccall f_10900(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_10900,c,av);}
/* modules.scm:1119: ##sys#register-primitive-module */
t2=*((C_word*)lf[79]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[204];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* g3049 in k10674 in k10671 in k10668 in k10665 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in ... */
static C_word C_fcall f_10907(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_stack_overflow_check;{}
return(C_i_assq(t1,((C_word*)t0)[2]));}

/* map-loop3043 in k10674 in k10671 in k10668 in k10665 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in ... */
static void C_fcall f_10919(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_10919,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=(
/* modules.scm:1052: g3049 */
  f_10907(((C_word*)t0)[2],C_slot(t2,C_fix(0)))
);
t4=C_a_i_cons(&a,2,t3,C_SCHEME_END_OF_LIST);
t5=C_i_setslot(((C_word*)((C_word*)t0)[3])[1],C_fix(1),t4);
t6=C_mutate(((C_word *)((C_word*)t0)[3])+1,t4);
t8=t1;
t9=C_slot(t2,C_fix(1));
t1=t8;
t2=t9;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k10952 in k10674 in k10671 in k10668 in k10665 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in ... */
static void C_ccall f_10954(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_10954,c,av);}
/* modules.scm:1113: ##sys#register-primitive-module */
t2=*((C_word*)lf[79]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[209];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* g3011 in k10668 in k10665 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in ... */
static C_word C_fcall f_10961(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_stack_overflow_check;{}
return(C_i_assq(t1,((C_word*)t0)[2]));}

/* map-loop3005 in k10668 in k10665 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in ... */
static void C_fcall f_10973(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_10973,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=(
/* modules.scm:1052: g3011 */
  f_10961(((C_word*)t0)[2],C_slot(t2,C_fix(0)))
);
t4=C_a_i_cons(&a,2,t3,C_SCHEME_END_OF_LIST);
t5=C_i_setslot(((C_word*)((C_word*)t0)[3])[1],C_fix(1),t4);
t6=C_mutate(((C_word *)((C_word*)t0)[3])+1,t4);
t8=t1;
t9=C_slot(t2,C_fix(1));
t1=t8;
t2=t9;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k11006 in k10668 in k10665 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in ... */
static void C_ccall f_11008(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11008,c,av);}
/* modules.scm:1107: ##sys#register-primitive-module */
t2=*((C_word*)lf[79]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[213];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* g2973 in k10665 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in ... */
static C_word C_fcall f_11015(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_stack_overflow_check;{}
return(C_i_assq(t1,((C_word*)t0)[2]));}

/* map-loop2967 in k10665 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in ... */
static void C_fcall f_11027(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_11027,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=(
/* modules.scm:1052: g2973 */
  f_11015(((C_word*)t0)[2],C_slot(t2,C_fix(0)))
);
t4=C_a_i_cons(&a,2,t3,C_SCHEME_END_OF_LIST);
t5=C_i_setslot(((C_word*)((C_word*)t0)[3])[1],C_fix(1),t4);
t6=C_mutate(((C_word *)((C_word*)t0)[3])+1,t4);
t8=t1;
t9=C_slot(t2,C_fix(1));
t1=t8;
t2=t9;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k11060 in k10665 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in ... */
static void C_ccall f_11062(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11062,c,av);}
/* modules.scm:1104: ##sys#register-primitive-module */
t2=*((C_word*)lf[79]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[216];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* g2935 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in ... */
static C_word C_fcall f_11069(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_stack_overflow_check;{}
return(C_i_assq(t1,((C_word*)t0)[2]));}

/* map-loop2929 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in ... */
static void C_fcall f_11081(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_11081,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=(
/* modules.scm:1052: g2935 */
  f_11069(((C_word*)t0)[2],C_slot(t2,C_fix(0)))
);
t4=C_a_i_cons(&a,2,t3,C_SCHEME_END_OF_LIST);
t5=C_i_setslot(((C_word*)((C_word*)t0)[3])[1],C_fix(1),t4);
t6=C_mutate(((C_word *)((C_word*)t0)[3])+1,t4);
t8=t1;
t9=C_slot(t2,C_fix(1));
t1=t8;
t2=t9;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k11114 in k10662 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in ... */
static void C_ccall f_11116(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11116,c,av);}
/* modules.scm:1101: ##sys#register-primitive-module */
t2=*((C_word*)lf[79]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[218];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* g2897 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in ... */
static C_word C_fcall f_11123(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_stack_overflow_check;{}
return(C_i_assq(t1,((C_word*)t0)[2]));}

/* map-loop2891 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in ... */
static void C_fcall f_11135(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_11135,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=(
/* modules.scm:1052: g2897 */
  f_11123(((C_word*)t0)[2],C_slot(t2,C_fix(0)))
);
t4=C_a_i_cons(&a,2,t3,C_SCHEME_END_OF_LIST);
t5=C_i_setslot(((C_word*)((C_word*)t0)[3])[1],C_fix(1),t4);
t6=C_mutate(((C_word *)((C_word*)t0)[3])+1,t4);
t8=t1;
t9=C_slot(t2,C_fix(1));
t1=t8;
t2=t9;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k11168 in k10659 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in ... */
static void C_ccall f_11170(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_11170,c,av);}
/* modules.scm:1088: ##sys#register-core-module */
t2=*((C_word*)lf[77]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[220];
av2[3]=lf[191];
av2[4]=lf[221];
av2[5]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* g2859 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 in ... */
static C_word C_fcall f_11177(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_stack_overflow_check;{}
return(C_i_assq(t1,((C_word*)t0)[2]));}

/* map-loop2853 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 in ... */
static void C_fcall f_11189(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_11189,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=(
/* modules.scm:1052: g2859 */
  f_11177(((C_word*)t0)[2],C_slot(t2,C_fix(0)))
);
t4=C_a_i_cons(&a,2,t3,C_SCHEME_END_OF_LIST);
t5=C_i_setslot(((C_word*)((C_word*)t0)[3])[1],C_fix(1),t4);
t6=C_mutate(((C_word *)((C_word*)t0)[3])+1,t4);
t8=t1;
t9=C_slot(t2,C_fix(1));
t1=t8;
t2=t9;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k11222 in k10656 in k10653 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 in ... */
static void C_ccall f_11224(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11224,c,av);}
/* modules.scm:1085: ##sys#register-primitive-module */
t2=*((C_word*)lf[79]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[224];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* g2821 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static C_word C_fcall f_11231(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_stack_overflow_check;{}
return(C_i_assq(t1,((C_word*)t0)[2]));}

/* map-loop2815 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_11243(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_11243,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=(
/* modules.scm:1052: g2821 */
  f_11231(((C_word*)t0)[2],C_slot(t2,C_fix(0)))
);
t4=C_a_i_cons(&a,2,t3,C_SCHEME_END_OF_LIST);
t5=C_i_setslot(((C_word*)((C_word*)t0)[3])[1],C_fix(1),t4);
t6=C_mutate(((C_word *)((C_word*)t0)[3])+1,t4);
t8=t1;
t9=C_slot(t2,C_fix(1));
t1=t8;
t2=t9;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k11276 in k10650 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_11278(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11278,c,av);}
/* modules.scm:1079: ##sys#register-primitive-module */
t2=*((C_word*)lf[79]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[229];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* g2783 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static C_word C_fcall f_11285(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_stack_overflow_check;{}
return(C_i_assq(t1,((C_word*)t0)[2]));}

/* map-loop2777 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_11297(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_11297,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=(
/* modules.scm:1052: g2783 */
  f_11285(((C_word*)t0)[2],C_slot(t2,C_fix(0)))
);
t4=C_a_i_cons(&a,2,t3,C_SCHEME_END_OF_LIST);
t5=C_i_setslot(((C_word*)((C_word*)t0)[3])[1],C_fix(1),t4);
t6=C_mutate(((C_word *)((C_word*)t0)[3])+1,t4);
t8=t1;
t9=C_slot(t2,C_fix(1));
t1=t8;
t2=t9;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k11330 in k10647 in k10644 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_11332(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11332,c,av);}
/* modules.scm:1076: ##sys#register-primitive-module */
t2=*((C_word*)lf[79]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[231];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* g2745 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static C_word C_fcall f_11339(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_stack_overflow_check;{}
return(C_i_assq(t1,((C_word*)t0)[2]));}

/* map-loop2739 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_11351(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_11351,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=(
/* modules.scm:1052: g2745 */
  f_11339(((C_word*)t0)[2],C_slot(t2,C_fix(0)))
);
t4=C_a_i_cons(&a,2,t3,C_SCHEME_END_OF_LIST);
t5=C_i_setslot(((C_word*)((C_word*)t0)[3])[1],C_fix(1),t4);
t6=C_mutate(((C_word *)((C_word*)t0)[3])+1,t4);
t8=t1;
t9=C_slot(t2,C_fix(1));
t1=t8;
t2=t9;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k11384 in k10641 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_11386(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11386,c,av);}
/* modules.scm:1067: ##sys#register-primitive-module */
t2=*((C_word*)lf[79]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[235];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* g2707 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static C_word C_fcall f_11393(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_stack_overflow_check;{}
return(C_i_assq(t1,((C_word*)t0)[2]));}

/* map-loop2701 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_11405(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_11405,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=(
/* modules.scm:1052: g2707 */
  f_11393(((C_word*)t0)[2],C_slot(t2,C_fix(0)))
);
t4=C_a_i_cons(&a,2,t3,C_SCHEME_END_OF_LIST);
t5=C_i_setslot(((C_word*)((C_word*)t0)[3])[1],C_fix(1),t4);
t6=C_mutate(((C_word *)((C_word*)t0)[3])+1,t4);
t8=t1;
t9=C_slot(t2,C_fix(1));
t1=t8;
t2=t9;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k11438 in k10638 in k10635 in k10632 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_11440(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11440,c,av);}
/* modules.scm:1064: ##sys#register-primitive-module */
t2=*((C_word*)lf[79]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[237];
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k11442 in k10629 in k10626 in k10623 in k10620 in k10617 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_11444(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_11444,c,av);}
/* modules.scm:1055: ##sys#register-core-module */
t2=*((C_word*)lf[77]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[243];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_END_OF_LIST;
av2[5]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* k11446 in k10614 in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_11448(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_11448,c,av);}
/* modules.scm:1038: ##sys#register-core-module */
t2=*((C_word*)lf[77]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[246];
av2[3]=lf[191];
av2[4]=t1;
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* k4016 */
static void C_ccall f_4018(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4018,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4021,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_library_toplevel(2,av2);}}

/* k4019 in k4016 */
static void C_ccall f_4021(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4021,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4024,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_internal_toplevel(2,av2);}}

/* k4022 in k4019 in k4016 */
static void C_ccall f_4024(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4024,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4027,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_expand_toplevel(2,av2);}}

/* k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_4027(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,5)))){
C_save_and_reclaim((void *)f_4027,c,av);}
a=C_alloc(14);
t2=C_a_i_provide(&a,1,lf[0]);
t3=C_mutate(&lf[1] /* (set! delete ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4518,a[2]=((C_word)li1),tmp=(C_word)a,a+=3,tmp));
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5309,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* modules.scm:75: chicken.base#make-parameter */
t5=*((C_word*)lf[251]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* delete in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_4518(C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,3)))){
C_save_and_reclaim_args((void *)trf_4518,4,t1,t2,t3,t4);}
a=C_alloc(8);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4524,a[2]=t6,a[3]=t4,a[4]=t2,a[5]=((C_word)li0),tmp=(C_word)a,a+=6,tmp));
t8=((C_word*)t6)[1];
f_4524(t8,t1,t3);}

/* loop in delete in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_4524(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,3)))){
C_save_and_reclaim_args((void *)trf_4524,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_nullp(t2))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4537,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* mini-srfi-1.scm:106: test */
t4=((C_word*)t0)[3];{
C_word av2[4];
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[4];
av2[3]=C_i_car(t2);
((C_proc)C_fast_retrieve_proc(t4))(4,av2);}}}

/* k4535 in loop in delete in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_4537(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_4537,c,av);}
a=C_alloc(4);
if(C_truep(t1)){
/* mini-srfi-1.scm:107: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_4524(t2,((C_word*)t0)[3],C_u_i_cdr(((C_word*)t0)[4]));}
else{
t2=C_u_i_car(((C_word*)t0)[4]);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4551,a[2]=((C_word*)t0)[3],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* mini-srfi-1.scm:109: loop */
t4=((C_word*)((C_word*)t0)[2])[1];
f_4524(t4,t3,C_u_i_cdr(((C_word*)t0)[4]));}}

/* k4549 in k4535 in loop in delete in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_4551(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_4551,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5309(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_5309,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[2]+1 /* (set! ##sys#current-module ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5313,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* modules.scm:76: chicken.base#make-parameter */
t4=*((C_word*)lf[251]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5313(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word t24;
C_word t25;
C_word t26;
C_word t27;
C_word t28;
C_word t29;
C_word t30;
C_word t31;
C_word t32;
C_word t33;
C_word t34;
C_word t35;
C_word t36;
C_word t37;
C_word t38;
C_word t39;
C_word t40;
C_word t41;
C_word t42;
C_word t43;
C_word t44;
C_word t45;
C_word t46;
C_word t47;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(116,c,11)))){
C_save_and_reclaim((void *)f_5313,c,av);}
a=C_alloc(116);
t2=C_mutate((C_word*)lf[3]+1 /* (set! ##sys#module-alias-environment ...) */,t1);
t3=C_mutate((C_word*)lf[4]+1 /* (set! module ...) */,lf[4]);
t4=C_mutate(&lf[5] /* (set! module-name ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5328,a[2]=((C_word)li2),tmp=(C_word)a,a+=3,tmp));
t5=C_mutate((C_word*)lf[7]+1 /* (set! module-undefined-list ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5418,a[2]=((C_word)li3),tmp=(C_word)a,a+=3,tmp));
t6=C_mutate((C_word*)lf[8]+1 /* (set! set-module-undefined-list! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5427,a[2]=((C_word)li4),tmp=(C_word)a,a+=3,tmp));
t7=C_mutate((C_word*)lf[10]+1 /* (set! ##sys#module-name ...) */,lf[5]);
t8=C_mutate((C_word*)lf[11]+1 /* (set! ##sys#module-exports ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5563,a[2]=((C_word)li5),tmp=(C_word)a,a+=3,tmp));
t9=C_mutate((C_word*)lf[15]+1 /* (set! ##sys#register-module-alias ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5587,a[2]=((C_word)li6),tmp=(C_word)a,a+=3,tmp));
t10=C_mutate((C_word*)lf[16]+1 /* (set! ##sys#with-module-aliases ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5605,a[2]=((C_word)li11),tmp=(C_word)a,a+=3,tmp));
t11=C_mutate((C_word*)lf[20]+1 /* (set! ##sys#resolve-module-name ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5708,a[2]=((C_word)li14),tmp=(C_word)a,a+=3,tmp));
t12=C_mutate((C_word*)lf[24]+1 /* (set! ##sys#find-module ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5755,a[2]=((C_word)li15),tmp=(C_word)a,a+=3,tmp));
t13=C_SCHEME_FALSE;
t14=(*a=C_VECTOR_TYPE|1,a[1]=t13,tmp=(C_word)a,a+=2,tmp);
t15=C_mutate((C_word*)lf[27]+1 /* (set! ##sys#switch-module ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5809,a[2]=t14,a[3]=((C_word)li17),tmp=(C_word)a,a+=4,tmp));
t16=C_mutate((C_word*)lf[31]+1 /* (set! ##sys#add-to-export-list ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5862,a[2]=((C_word)li21),tmp=(C_word)a,a+=3,tmp));
t17=C_mutate((C_word*)lf[34]+1 /* (set! ##sys#toplevel-definition-hook ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5951,a[2]=((C_word)li22),tmp=(C_word)a,a+=3,tmp));
t18=C_mutate((C_word*)lf[35]+1 /* (set! ##sys#register-meta-expression ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5954,a[2]=((C_word)li23),tmp=(C_word)a,a+=3,tmp));
t19=C_mutate(&lf[37] /* (set! check-for-redef ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5974,a[2]=((C_word)li24),tmp=(C_word)a,a+=3,tmp));
t20=C_mutate((C_word*)lf[41]+1 /* (set! ##sys#register-export ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5995,a[2]=((C_word)li25),tmp=(C_word)a,a+=3,tmp));
t21=C_mutate((C_word*)lf[46]+1 /* (set! ##sys#register-syntax-export ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6084,a[2]=((C_word)li26),tmp=(C_word)a,a+=3,tmp));
t22=C_mutate((C_word*)lf[49]+1 /* (set! ##sys#unregister-syntax-export ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6165,a[2]=((C_word)li28),tmp=(C_word)a,a+=3,tmp));
t23=C_mutate((C_word*)lf[50]+1 /* (set! ##sys#register-module ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6258,a[2]=((C_word)li29),tmp=(C_word)a,a+=3,tmp));
t24=C_mutate(&lf[51] /* (set! merge-se ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6568,a[2]=((C_word)li34),tmp=(C_word)a,a+=3,tmp));
t25=C_mutate((C_word*)lf[56]+1 /* (set! ##sys#compiled-module-registration ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6721,a[2]=((C_word)li39),tmp=(C_word)a,a+=3,tmp));
t26=C_mutate((C_word*)lf[68]+1 /* (set! ##sys#register-compiled-module ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7124,a[2]=((C_word)li46),tmp=(C_word)a,a+=3,tmp));
t27=C_mutate((C_word*)lf[77]+1 /* (set! ##sys#register-core-module ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7450,a[2]=((C_word)li49),tmp=(C_word)a,a+=3,tmp));
t28=C_mutate((C_word*)lf[79]+1 /* (set! ##sys#register-primitive-module ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7564,a[2]=((C_word)li50),tmp=(C_word)a,a+=3,tmp));
t29=C_mutate(&lf[45] /* (set! find-export ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7579,a[2]=((C_word)li52),tmp=(C_word)a,a+=3,tmp));
t30=C_mutate((C_word*)lf[80]+1 /* (set! ##sys#finalize-module ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7657,a[2]=((C_word)li74),tmp=(C_word)a,a+=3,tmp));
t31=C_set_block_item(lf[25] /* ##sys#module-table */,0,C_SCHEME_END_OF_LIST);
t32=C_mutate((C_word*)lf[60]+1 /* (set! ##sys#with-environment ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8325,a[2]=((C_word)li78),tmp=(C_word)a,a+=3,tmp));
t33=C_mutate((C_word*)lf[114]+1 /* (set! ##sys#import-library-hook ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8430,a[2]=((C_word)li83),tmp=(C_word)a,a+=3,tmp));
t34=C_mutate(&lf[120] /* (set! find-module/import-library ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8476,a[2]=((C_word)li84),tmp=(C_word)a,a+=3,tmp));
t35=C_mutate((C_word*)lf[121]+1 /* (set! ##sys#decompose-import ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8491,a[2]=((C_word)li119),tmp=(C_word)a,a+=3,tmp));
t36=C_mutate((C_word*)lf[145]+1 /* (set! ##sys#expand-import ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9395,a[2]=((C_word)li124),tmp=(C_word)a,a+=3,tmp));
t37=C_mutate((C_word*)lf[147]+1 /* (set! ##sys#import ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9459,a[2]=((C_word)li133),tmp=(C_word)a,a+=3,tmp));
t38=C_mutate(&lf[44] /* (set! module-rename ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9901,a[2]=((C_word)li134),tmp=(C_word)a,a+=3,tmp));
t39=C_mutate((C_word*)lf[155]+1 /* (set! ##sys#alias-global-hook ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9919,a[2]=((C_word)li139),tmp=(C_word)a,a+=3,tmp));
t40=C_mutate((C_word*)lf[156]+1 /* (set! ##sys#validate-exports ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9999,a[2]=((C_word)li144),tmp=(C_word)a,a+=3,tmp));
t41=C_mutate((C_word*)lf[167]+1 /* (set! ##sys#register-functor ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10214,a[2]=((C_word)li145),tmp=(C_word)a,a+=3,tmp));
t42=C_mutate((C_word*)lf[168]+1 /* (set! ##sys#instantiate-functor ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10230,a[2]=((C_word)li151),tmp=(C_word)a,a+=3,tmp));
t43=C_mutate(&lf[172] /* (set! match-functor-argument ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10456,a[2]=((C_word)li155),tmp=(C_word)a,a+=3,tmp));
t44=lf[180];
t45=*((C_word*)lf[181]+1);
t46=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10616,a[2]=((C_word*)t0)[2],a[3]=t45,a[4]=t44,tmp=(C_word)a,a+=5,tmp);
/* modules.scm:1037: ##sys#register-core-module */
t47=*((C_word*)lf[77]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t47;
av2[1]=t46;
av2[2]=lf[250];
av2[3]=lf[191];
av2[4]=t44;
av2[5]=*((C_word*)lf[181]+1);
((C_proc)(void*)(*((C_word*)t47+1)))(6,av2);}}

/* module-name in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5328(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5328,c,av);}
t3=C_i_check_structure_2(t2,lf[4],lf[6]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_i_block_ref(t2,C_fix(1));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* module-undefined-list in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5418(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5418,c,av);}
t3=C_i_check_structure_2(t2,lf[4],lf[7]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_i_block_ref(t2,C_fix(7));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* set-module-undefined-list! in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5427(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_5427,c,av);}
t4=C_i_check_structure_2(t2,lf[4],C_SCHEME_FALSE);
/* modules.scm:93: ##sys#block-set! */
t5=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t1;
av2[2]=t2;
av2[3]=C_fix(7);
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}

/* ##sys#module-exports in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5563(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_5563,c,av);}
t3=C_i_check_structure_2(t2,lf[4],lf[12]);
t4=C_i_block_ref(t2,C_fix(3));
t5=C_i_check_structure_2(t2,lf[4],lf[13]);
t6=C_i_block_ref(t2,C_fix(11));
t7=C_i_check_structure_2(t2,lf[4],lf[14]);
/* modules.scm:117: scheme#values */{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=t1;
av2[2]=t4;
av2[3]=t6;
av2[4]=C_i_block_ref(t2,C_fix(12));
C_values(5,av2);}}

/* ##sys#register-module-alias in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5587(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_5587,c,av);}
a=C_alloc(7);
t4=C_a_i_cons(&a,2,t2,t3);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5603,a[2]=t4,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* modules.scm:127: ##sys#module-alias-environment */
t6=*((C_word*)lf[3]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}

/* k5601 in ##sys#register-module-alias in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5603(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_5603,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
/* modules.scm:126: ##sys#module-alias-environment */
t3=*((C_word*)lf[3]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* ##sys#with-module-aliases in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5605(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(21,c,3)))){
C_save_and_reclaim((void *)f_5605,c,av);}
a=C_alloc(21);
t4=*((C_word*)lf[3]+1);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5609,a[2]=t4,a[3]=t3,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t6=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t7=t6;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=((C_word*)t8)[1];
t10=C_i_check_list_2(t2,lf[18]);
t11=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5668,a[2]=t5,tmp=(C_word)a,a+=3,tmp);
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5674,a[2]=t8,a[3]=t13,a[4]=t9,a[5]=((C_word)li10),tmp=(C_word)a,a+=6,tmp));
t15=((C_word*)t13)[1];
f_5674(t15,t11,t2);}

/* k5607 in ##sys#with-module-aliases in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5609(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(19,c,4)))){
C_save_and_reclaim((void *)f_5609,c,av);}
a=C_alloc(19);
t2=t1;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_SCHEME_TRUE;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5614,a[2]=t3,a[3]=t5,a[4]=((C_word*)t0)[2],a[5]=((C_word)li7),tmp=(C_word)a,a+=6,tmp);
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5631,a[2]=((C_word*)t0)[3],a[3]=((C_word)li8),tmp=(C_word)a,a+=4,tmp);
t8=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5637,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=((C_word)li9),tmp=(C_word)a,a+=5,tmp);
/* modules.scm:130: ##sys#dynamic-wind */
t9=*((C_word*)lf[17]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t9;
av2[1]=((C_word*)t0)[4];
av2[2]=t6;
av2[3]=t7;
av2[4]=t8;
((C_proc)(void*)(*((C_word*)t9+1)))(5,av2);}}

/* a5613 in k5607 in ##sys#with-module-aliases in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5614(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_5614,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5618,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(((C_word*)((C_word*)t0)[3])[1])){
/* modules.scm:130: ##sys#module-alias-environment890 */
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[2])[1];
av2[3]=C_SCHEME_TRUE;
av2[4]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)((C_word*)t0)[2])[1];
f_5618(2,av2);}}}

/* k5616 in a5613 in k5607 in ##sys#with-module-aliases in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5618(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_5618,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5622,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,tmp=(C_word)a,a+=7,tmp);
/* modules.scm:130: ##sys#module-alias-environment890 */
t3=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)C_fast_retrieve_proc(t3))(2,av2);}}

/* k5620 in k5616 in a5613 in k5607 in ##sys#with-module-aliases in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5622(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_5622,c,av);}
a=C_alloc(4);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5625,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
/* modules.scm:130: ##sys#module-alias-environment890 */
t4=((C_word*)t0)[5];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[6];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t4))(5,av2);}}

/* k5623 in k5620 in k5616 in a5613 in k5607 in ##sys#with-module-aliases in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5625(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5625,c,av);}
t2=C_set_block_item(((C_word*)t0)[2],0,C_SCHEME_FALSE);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* a5630 in k5607 in ##sys#with-module-aliases in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5631(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5631,c,av);}
/* modules.scm:134: thunk */
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)C_fast_retrieve_proc(t2))(2,av2);}}

/* a5636 in k5607 in ##sys#with-module-aliases in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5637(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_5637,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5641,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* modules.scm:130: ##sys#module-alias-environment890 */
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)C_fast_retrieve_proc(t3))(2,av2);}}

/* k5639 in a5636 in k5607 in ##sys#with-module-aliases in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5641(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_5641,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5644,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* modules.scm:130: ##sys#module-alias-environment890 */
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[2])[1];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}

/* k5642 in k5639 in a5636 in k5607 in ##sys#with-module-aliases in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5644(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5644,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,((C_word*)t0)[3]);
t3=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k5666 in ##sys#with-module-aliases in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5668(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_5668,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5672,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* modules.scm:133: ##sys#module-alias-environment */
t3=*((C_word*)lf[3]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k5670 in k5666 in ##sys#with-module-aliases in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5672(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5672,c,av);}
/* modules.scm:131: scheme#append */
t2=*((C_word*)lf[19]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* map-loop899 in ##sys#with-module-aliases in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_5674(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_5674,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_car(t3);
t5=C_i_cadr(t3);
t6=C_a_i_cons(&a,2,t4,t5);
t7=C_a_i_cons(&a,2,t6,C_SCHEME_END_OF_LIST);
t8=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t7);
t9=C_mutate(((C_word *)((C_word*)t0)[2])+1,t7);
t11=t1;
t12=C_slot(t2,C_fix(1));
t1=t11;
t2=t12;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* ##sys#resolve-module-name in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5708(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_5708,c,av);}
a=C_alloc(5);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5716,a[2]=t3,a[3]=t2,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* modules.scm:137: chicken.internal#library-id */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[23]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[23]+1);
av2[1]=t4;
av2[2]=t2;
tp(3,av2);}}

/* k5714 in ##sys#resolve-module-name in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5716(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_5716,c,av);}
a=C_alloc(8);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5718,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t3,a[5]=((C_word)li13),tmp=(C_word)a,a+=6,tmp));
t5=((C_word*)t3)[1];
f_5718(t5,((C_word*)t0)[4],t1,C_SCHEME_END_OF_LIST);}

/* loop in k5714 in ##sys#resolve-module-name in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_5718(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_5718,4,t0,t1,t2,t3);}
a=C_alloc(8);
t4=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_5753,a[2]=t2,a[3]=t3,a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[4],a[7]=t1,tmp=(C_word)a,a+=8,tmp);
/* modules.scm:138: ##sys#module-alias-environment */
t5=*((C_word*)lf[3]+1);{
C_word av2[2];
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* g943 in k5751 in loop in k5714 in ##sys#resolve-module-name in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_5726(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,0,4)))){
C_save_and_reclaim_args((void *)trf_5726,3,t0,t1,t2);}
a=C_alloc(3);
t3=C_i_cdr(t2);
if(C_truep(C_i_memq(t3,((C_word*)t0)[2]))){
/* modules.scm:142: chicken.base#error */
t4=*((C_word*)lf[21]+1);{
C_word av2[5];
av2[0]=t4;
av2[1]=t1;
av2[2]=((C_word*)t0)[3];
av2[3]=lf[22];
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}
else{
t4=C_a_i_cons(&a,2,t3,((C_word*)t0)[2]);
/* modules.scm:143: loop */
t5=((C_word*)((C_word*)t0)[5])[1];
f_5718(t5,t1,t3,t4);}}

/* k5751 in loop in k5714 in ##sys#resolve-module-name in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5753(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_5753,c,av);}
a=C_alloc(7);
t2=C_i_assq(((C_word*)t0)[2],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5726,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word)li12),tmp=(C_word)a,a+=7,tmp);
/* modules.scm:138: g943 */
t4=t3;
f_5726(t4,((C_word*)t0)[7],t2);}
else{
t3=((C_word*)t0)[7];{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* ##sys#find-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5755(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_5755,c,av);}
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_SCHEME_TRUE:C_get_rest_arg(c,3,av,3,t0));
t5=C_rest_nullp(c,3);
t6=C_rest_nullp(c,4);
t7=(C_truep(t6)?C_SCHEME_FALSE:C_get_rest_arg(c,4,av,3,t0));
t8=C_rest_nullp(c,4);
t9=C_i_assq(t2,*((C_word*)lf[25]+1));
if(C_truep(t9)){
t10=t1;{
C_word *av2=av;
av2[0]=t10;
av2[1]=C_i_cdr(t9);
((C_proc)(void*)(*((C_word*)t10+1)))(2,av2);}}
else{
if(C_truep(t4)){
/* modules.scm:148: chicken.base#error */
t10=*((C_word*)lf[21]+1);{
C_word av2[5];
av2[0]=t10;
av2[1]=t1;
av2[2]=t7;
av2[3]=lf[26];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t10+1)))(5,av2);}}
else{
t10=t1;{
C_word *av2=av;
av2[0]=t10;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t10+1)))(2,av2);}}}}

/* ##sys#switch-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5809(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_5809,c,av);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5856,a[2]=t1,a[3]=t2,a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
/* modules.scm:154: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[29]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[29]+1);
av2[1]=t3;
tp(2,av2);}}

/* k5814 in k5858 in k5854 in ##sys#switch-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5816(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_5816,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5819,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
if(C_truep(t1)){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5844,a[2]=((C_word*)t0)[5],a[3]=((C_word)li16),tmp=(C_word)a,a+=4,tmp);
/* modules.scm:155: g981 */
t4=t3;
f_5844(t4,t2,t1);}
else{
t3=C_mutate(((C_word *)((C_word*)t0)[4])+1,((C_word*)t0)[5]);
t4=t2;{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
f_5819(2,av2);}}}

/* k5817 in k5814 in k5858 in k5854 in ##sys#switch-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5819(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_5819,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5822,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
if(C_truep(((C_word*)t0)[3])){
t3=C_i_check_structure_2(((C_word*)t0)[3],lf[4],lf[30]);
t4=t2;
f_5822(t4,C_i_block_ref(((C_word*)t0)[3],C_fix(14)));}
else{
t3=t2;
f_5822(t3,((C_word*)((C_word*)t0)[4])[1]);}}

/* k5820 in k5817 in k5814 in k5858 in k5854 in ##sys#switch-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_5822(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_5822,2,t0,t1);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5825,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
if(C_truep(t1)){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5831,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* modules.scm:162: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[29]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[29]+1);
av2[1]=t3;
av2[2]=C_i_car(t1);
tp(3,av2);}}
else{
/* modules.scm:164: ##sys#current-module */
t3=*((C_word*)lf[2]+1);{
C_word av2[3];
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}

/* k5823 in k5820 in k5817 in k5814 in k5858 in k5854 in ##sys#switch-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5825(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5825,c,av);}
/* modules.scm:164: ##sys#current-module */
t2=*((C_word*)lf[2]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k5829 in k5820 in k5817 in k5814 in k5858 in k5854 in ##sys#switch-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5831(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5831,c,av);}
/* modules.scm:163: ##sys#macro-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[28]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[28]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=C_u_i_cdr(((C_word*)t0)[3]);
tp(3,av2);}}

/* g981 in k5814 in k5858 in k5854 in ##sys#switch-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_5844(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,4)))){
C_save_and_reclaim_args((void *)trf_5844,3,t0,t1,t2);}
t3=C_i_check_structure_2(t2,lf[4],C_SCHEME_FALSE);
/* modules.scm:93: ##sys#block-set! */
t4=*((C_word*)lf[9]+1);{
C_word av2[5];
av2[0]=t4;
av2[1]=t1;
av2[2]=t2;
av2[3]=C_fix(14);
av2[4]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k5854 in ##sys#switch-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5856(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_5856,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5860,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* modules.scm:154: ##sys#macro-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[28]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[28]+1);
av2[1]=t2;
tp(2,av2);}}

/* k5858 in k5854 in ##sys#switch-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5860(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_5860,c,av);}
a=C_alloc(9);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5816,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* modules.scm:155: ##sys#current-module */
t4=*((C_word*)lf[2]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* ##sys#add-to-export-list in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5862(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_5862,c,av);}
a=C_alloc(6);
t4=C_i_check_structure_2(t2,lf[4],lf[12]);
t5=C_i_block_ref(t2,C_fix(3));
t6=C_eqp(t5,C_SCHEME_TRUE);
if(C_truep(t6)){
t7=C_i_check_structure_2(t2,lf[4],lf[32]);
t8=C_i_block_ref(t2,C_fix(5));
t9=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5878,a[2]=t3,a[3]=t2,a[4]=t1,a[5]=t8,tmp=(C_word)a,a+=6,tmp);
/* modules.scm:170: ##sys#macro-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[28]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[28]+1);
av2[1]=t9;
tp(2,av2);}}
else{
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5949,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* modules.scm:180: scheme#append */
t8=*((C_word*)lf[19]+1);{
C_word *av2=av;
av2[0]=t8;
av2[1]=t7;
av2[2]=t5;
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t8+1)))(4,av2);}}}

/* k5876 in ##sys#add-to-export-list in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5878(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(25,c,3)))){
C_save_and_reclaim((void *)f_5878,c,av);}
a=C_alloc(25);
t2=C_SCHEME_END_OF_LIST;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5887,a[2]=t3,a[3]=((C_word)li18),tmp=(C_word)a,a+=4,tmp);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5879,a[2]=t1,a[3]=t4,a[4]=((C_word)li19),tmp=(C_word)a,a+=5,tmp);
t6=C_i_check_list_2(((C_word*)t0)[2],lf[33]);
t7=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5901,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[2],a[6]=t3,tmp=(C_word)a,a+=7,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5921,a[2]=t5,a[3]=t9,a[4]=((C_word)li20),tmp=(C_word)a,a+=5,tmp));
t11=((C_word*)t9)[1];
f_5921(t11,t7,((C_word*)t0)[2]);}

/* g998 in k5876 in ##sys#add-to-export-list in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static C_word C_fcall f_5879(C_word *a,C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_stack_overflow_check;{}
t2=C_i_assq(t1,((C_word*)t0)[2]);
if(C_truep(t2)){
return((
/* modules.scm:174: g1014 */
  f_5887(C_a_i(&a,3),((C_word*)t0)[3],t2)
));}
else{
t3=C_SCHEME_UNDEFINED;
return(t3);}}

/* g1014 in k5876 in ##sys#add-to-export-list in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static C_word C_fcall f_5887(C_word *a,C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_stack_overflow_check;{}
t2=C_a_i_cons(&a,2,t1,((C_word*)((C_word*)t0)[2])[1]);
t3=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
return(t3);}

/* k5899 in k5876 in ##sys#add-to-export-list in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5901(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_5901,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5904,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5915,a[2]=((C_word*)t0)[2],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t4=C_i_check_structure_2(((C_word*)t0)[2],lf[4],lf[14]);
/* modules.scm:178: scheme#append */
t5=*((C_word*)lf[19]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=((C_word*)((C_word*)t0)[6])[1];
av2[3]=C_i_block_ref(((C_word*)t0)[2],C_fix(12));
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k5902 in k5899 in k5876 in ##sys#add-to-export-list in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5904(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_5904,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5911,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* modules.scm:179: scheme#append */
t3=*((C_word*)lf[19]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k5909 in k5902 in k5899 in k5876 in ##sys#add-to-export-list in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5911(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_5911,c,av);}
t2=C_i_check_structure_2(((C_word*)t0)[2],lf[4],C_SCHEME_FALSE);
/* modules.scm:93: ##sys#block-set! */
t3=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[2];
av2[3]=C_fix(5);
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k5913 in k5899 in k5876 in ##sys#add-to-export-list in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5915(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_5915,c,av);}
t2=C_i_check_structure_2(((C_word*)t0)[2],lf[4],C_SCHEME_FALSE);
/* modules.scm:93: ##sys#block-set! */
t3=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[2];
av2[3]=C_fix(12);
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* for-each-loop997 in k5876 in ##sys#add-to-export-list in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_5921(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_5921,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=(
/* modules.scm:172: g998 */
  f_5879(C_a_i(&a,3),((C_word*)t0)[2],C_slot(t2,C_fix(0)))
);
t5=t1;
t6=C_slot(t2,C_fix(1));
t1=t5;
t2=t6;
goto loop;}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k5947 in ##sys#add-to-export-list in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5949(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_5949,c,av);}
t2=C_i_check_structure_2(((C_word*)t0)[2],lf[4],C_SCHEME_FALSE);
/* modules.scm:93: ##sys#block-set! */
t3=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[2];
av2[3]=C_fix(3);
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* ##sys#toplevel-definition-hook in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5951(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5951,c,av);}
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* ##sys#register-meta-expression in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5954(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_5954,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5958,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* modules.scm:185: ##sys#current-module */
t4=*((C_word*)lf[2]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k5956 in ##sys#register-meta-expression in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5958(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_5958,c,av);}
a=C_alloc(3);
if(C_truep(t1)){
t2=C_i_check_structure_2(t1,lf[4],lf[36]);
t3=C_i_block_ref(t1,C_fix(10));
t4=C_a_i_cons(&a,2,((C_word*)t0)[2],t3);
t5=C_i_check_structure_2(t1,lf[4],C_SCHEME_FALSE);
/* modules.scm:93: ##sys#block-set! */
t6=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t6;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
av2[3]=C_fix(10);
av2[4]=t4;
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* check-for-redef in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_5974(C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,3)))){
C_save_and_reclaim_args((void *)trf_5974,4,t1,t2,t3,t4);}
a=C_alloc(5);
t5=C_i_assq(t2,t3);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5981,a[2]=t2,a[3]=t4,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
if(C_truep(t5)){
/* modules.scm:190: ##sys#warn */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[38]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[38]+1);
av2[1]=t6;
av2[2]=lf[40];
av2[3]=t2;
tp(4,av2);}}
else{
t7=t6;{
C_word av2[2];
av2[0]=t7;
av2[1]=C_SCHEME_FALSE;
f_5981(2,av2);}}}

/* k5979 in check-for-redef in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5981(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5981,c,av);}
if(C_truep(C_i_assq(((C_word*)t0)[2],((C_word*)t0)[3]))){
/* modules.scm:192: ##sys#warn */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[38]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[38]+1);
av2[1]=((C_word*)t0)[4];
av2[2]=lf[39];
av2[3]=((C_word*)t0)[2];
tp(4,av2);}}
else{
t2=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* ##sys#register-export in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_5995(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_5995,c,av);}
a=C_alloc(5);
if(C_truep(t3)){
t4=C_i_check_structure_2(t3,lf[4],lf[12]);
t5=C_i_block_ref(t3,C_fix(3));
t6=C_eqp(C_SCHEME_TRUE,t5);
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6005,a[2]=t2,a[3]=t3,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
if(C_truep(t6)){
t8=t7;{
C_word *av2=av;
av2[0]=t8;
av2[1]=t6;
f_6005(2,av2);}}
else{
/* modules.scm:197: find-export */
f_7579(t7,t2,t3,C_SCHEME_TRUE);}}
else{
t4=C_SCHEME_UNDEFINED;
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* k6003 in ##sys#register-export in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6005(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_6005,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6008,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* modules.scm:198: module-undefined-list */
t3=*((C_word*)lf[7]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k6006 in k6003 in ##sys#register-export in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6008(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_6008,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6011,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6071,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
t4=C_i_check_structure_2(((C_word*)t0)[4],lf[4],lf[6]);
/* modules.scm:200: module-rename */
f_9901(t3,((C_word*)t0)[2],C_i_block_ref(((C_word*)t0)[4],C_fix(1)));}

/* k6009 in k6006 in k6003 in ##sys#register-export in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6011(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,4)))){
C_save_and_reclaim((void *)f_6011,c,av);}
a=C_alloc(10);
t2=C_i_assq(((C_word*)t0)[2],((C_word*)t0)[3]);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6017,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],tmp=(C_word)a,a+=6,tmp);
if(C_truep(t2)){
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6067,a[2]=t3,a[3]=((C_word*)t0)[5],tmp=(C_word)a,a+=4,tmp);
/* modules.scm:202: delete */
f_4518(t4,t2,((C_word*)t0)[3],*((C_word*)lf[43]+1));}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
f_6017(2,av2);}}}

/* k6015 in k6009 in k6006 in k6003 in ##sys#register-export in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6017(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_6017,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6020,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6056,a[2]=t2,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* modules.scm:203: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[29]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[29]+1);
av2[1]=t3;
tp(2,av2);}}

/* k6018 in k6015 in k6009 in k6006 in k6003 in ##sys#register-export in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6020(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_6020,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6023,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t3=C_i_check_structure_2(((C_word*)t0)[4],lf[4],lf[32]);
t4=C_i_block_ref(((C_word*)t0)[4],C_fix(5));
t5=C_a_i_cons(&a,2,((C_word*)t0)[3],t4);
t6=C_i_check_structure_2(((C_word*)t0)[4],lf[4],C_SCHEME_FALSE);
/* modules.scm:93: ##sys#block-set! */
t7=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t7;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=C_fix(5);
av2[4]=t5;
((C_proc)(void*)(*((C_word*)t7+1)))(5,av2);}}

/* k6021 in k6018 in k6015 in k6009 in k6006 in k6003 in ##sys#register-export in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6023(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_6023,c,av);}
a=C_alloc(6);
if(C_truep(((C_word*)t0)[2])){
t2=C_a_i_cons(&a,2,((C_word*)t0)[3],C_SCHEME_FALSE);
t3=C_i_check_structure_2(((C_word*)t0)[4],lf[4],lf[42]);
t4=C_i_block_ref(((C_word*)t0)[4],C_fix(4));
t5=C_a_i_cons(&a,2,t2,t4);
t6=C_i_check_structure_2(((C_word*)t0)[4],lf[4],C_SCHEME_FALSE);
/* modules.scm:93: ##sys#block-set! */
t7=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t7;
av2[1]=((C_word*)t0)[5];
av2[2]=((C_word*)t0)[4];
av2[3]=C_fix(4);
av2[4]=t5;
((C_proc)(void*)(*((C_word*)t7+1)))(5,av2);}}
else{
t2=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k6054 in k6015 in k6009 in k6006 in k6003 in ##sys#register-export in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6056(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_6056,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6060,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* modules.scm:203: ##sys#macro-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[28]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[28]+1);
av2[1]=t2;
tp(2,av2);}}

/* k6058 in k6054 in k6015 in k6009 in k6006 in k6003 in ##sys#register-export in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6060(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6060,c,av);}
/* modules.scm:203: check-for-redef */
f_5974(((C_word*)t0)[2],((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* k6065 in k6009 in k6006 in k6003 in ##sys#register-export in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6067(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6067,c,av);}
/* modules.scm:202: set-module-undefined-list! */
t2=*((C_word*)lf[8]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k6069 in k6006 in k6003 in ##sys#register-export in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6071(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6071,c,av);}
/* modules.scm:199: ##sys#toplevel-definition-hook */
t2=*((C_word*)lf[34]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* ##sys#register-syntax-export in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6084(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_6084,c,av);}
a=C_alloc(6);
if(C_truep(t3)){
t5=C_i_check_structure_2(t3,lf[4],lf[12]);
t6=C_i_block_ref(t3,C_fix(3));
t7=C_eqp(C_SCHEME_TRUE,t6);
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6094,a[2]=t3,a[3]=t2,a[4]=t4,a[5]=t1,tmp=(C_word)a,a+=6,tmp);
if(C_truep(t7)){
t9=t8;{
C_word *av2=av;
av2[0]=t9;
av2[1]=t7;
f_6094(2,av2);}}
else{
/* modules.scm:215: find-export */
f_7579(t8,t2,t3,C_SCHEME_TRUE);}}
else{
t5=C_SCHEME_UNDEFINED;
t6=t1;{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* k6092 in ##sys#register-syntax-export in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6094(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_6094,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6097,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,tmp=(C_word)a,a+=7,tmp);
/* modules.scm:216: module-undefined-list */
t3=*((C_word*)lf[7]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k6095 in k6092 in ##sys#register-syntax-export in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6097(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_6097,c,av);}
a=C_alloc(7);
t2=C_i_check_structure_2(((C_word*)t0)[2],lf[4],lf[6]);
t3=C_i_block_ref(((C_word*)t0)[2],C_fix(1));
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6103,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
if(C_truep(C_i_assq(((C_word*)t0)[3],t1))){
/* modules.scm:219: ##sys#warn */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[38]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[38]+1);
av2[1]=t4;
av2[2]=lf[48];
av2[3]=((C_word*)t0)[3];
tp(4,av2);}}
else{
t5=t4;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_UNDEFINED;
f_6103(2,av2);}}}

/* k6101 in k6095 in k6092 in ##sys#register-syntax-export in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6103(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_6103,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6106,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6146,a[2]=t2,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* modules.scm:220: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[29]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[29]+1);
av2[1]=t3;
tp(2,av2);}}

/* k6104 in k6101 in k6095 in k6092 in ##sys#register-syntax-export in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6106(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,4)))){
C_save_and_reclaim((void *)f_6106,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6112,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
if(C_truep(((C_word*)t0)[6])){
t3=C_a_i_cons(&a,2,((C_word*)t0)[2],((C_word*)t0)[3]);
t4=C_i_check_structure_2(((C_word*)t0)[4],lf[4],lf[42]);
t5=C_i_block_ref(((C_word*)t0)[4],C_fix(4));
t6=C_a_i_cons(&a,2,t3,t5);
t7=C_i_check_structure_2(((C_word*)t0)[4],lf[4],C_SCHEME_FALSE);
/* modules.scm:93: ##sys#block-set! */
t8=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t8;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=C_fix(4);
av2[4]=t6;
((C_proc)(void*)(*((C_word*)t8+1)))(5,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_6112(2,av2);}}}

/* k6110 in k6104 in k6101 in k6095 in k6092 in ##sys#register-syntax-export in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6112(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_6112,c,av);}
a=C_alloc(6);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],((C_word*)t0)[3]);
t3=C_i_check_structure_2(((C_word*)t0)[4],lf[4],lf[47]);
t4=C_i_block_ref(((C_word*)t0)[4],C_fix(6));
t5=C_a_i_cons(&a,2,t2,t4);
t6=C_i_check_structure_2(((C_word*)t0)[4],lf[4],C_SCHEME_FALSE);
/* modules.scm:93: ##sys#block-set! */
t7=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t7;
av2[1]=((C_word*)t0)[5];
av2[2]=((C_word*)t0)[4];
av2[3]=C_fix(6);
av2[4]=t5;
((C_proc)(void*)(*((C_word*)t7+1)))(5,av2);}}

/* k6144 in k6101 in k6095 in k6092 in ##sys#register-syntax-export in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6146(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_6146,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6150,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* modules.scm:220: ##sys#macro-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[28]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[28]+1);
av2[1]=t2;
tp(2,av2);}}

/* k6148 in k6144 in k6101 in k6095 in k6092 in ##sys#register-syntax-export in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6150(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6150,c,av);}
/* modules.scm:220: check-for-redef */
f_5974(((C_word*)t0)[2],((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* ##sys#unregister-syntax-export in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6165(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(7,c,4)))){
C_save_and_reclaim((void *)f_6165,c,av);}
a=C_alloc(7);
if(C_truep(t3)){
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6176,a[2]=t3,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t5=C_i_check_structure_2(t3,lf[4],lf[47]);
t6=C_i_block_ref(t3,C_fix(6));
t7=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6182,a[2]=((C_word)li27),tmp=(C_word)a,a+=3,tmp);
/* modules.scm:235: delete */
f_4518(t4,t2,t6,t7);}
else{
t4=C_SCHEME_UNDEFINED;
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* k6174 in ##sys#unregister-syntax-export in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6176(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6176,c,av);}
t2=C_i_check_structure_2(((C_word*)t0)[2],lf[4],C_SCHEME_FALSE);
/* modules.scm:93: ##sys#block-set! */
t3=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[2];
av2[3]=C_fix(6);
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* a6181 in ##sys#unregister-syntax-export in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6182(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6182,c,av);}
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_eqp(t2,C_i_car(t3));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k6197 in g2493 in k9924 in mrename in ##sys#alias-global-hook in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6199(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_6199,c,av);}
a=C_alloc(9);
t2=C_i_assq(((C_word*)t0)[2],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6206,a[2]=((C_word*)t0)[3],a[3]=((C_word)li135),tmp=(C_word)a,a+=4,tmp);
/* modules.scm:240: g1090 */
t4=t3;
f_6206(t4,((C_word*)t0)[4],t2);}
else{
if(C_truep(((C_word*)t0)[3])){
t3=C_a_i_list1(&a,1,((C_word*)t0)[3]);
t4=C_a_i_cons(&a,2,((C_word*)t0)[2],t3);
t5=C_a_i_cons(&a,2,t4,t1);
/* modules.scm:245: set-module-undefined-list! */
t6=*((C_word*)lf[8]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=((C_word*)t0)[4];
av2[2]=((C_word*)t0)[5];
av2[3]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}
else{
t3=C_a_i_cons(&a,2,((C_word*)t0)[2],C_SCHEME_END_OF_LIST);
t4=C_a_i_cons(&a,2,t3,t1);
/* modules.scm:245: set-module-undefined-list! */
t5=*((C_word*)lf[8]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=((C_word*)t0)[4];
av2[2]=((C_word*)t0)[5];
av2[3]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}}}

/* g1090 in k6197 in g2493 in k9924 in mrename in ##sys#alias-global-hook in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_6206(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_6206,3,t0,t1,t2);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6213,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
if(C_truep(((C_word*)t0)[2])){
t4=C_i_cdr(t2);
t5=t3;
f_6213(t5,C_i_not(C_i_memq(((C_word*)t0)[2],t4)));}
else{
t4=t3;
f_6213(t4,C_SCHEME_FALSE);}}

/* k6211 in g1090 in k6197 in g2493 in k9924 in mrename in ##sys#alias-global-hook in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_6213(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,0,1)))){
C_save_and_reclaim_args((void *)trf_6213,2,t0,t1);}
a=C_alloc(3);
if(C_truep(t1)){
t2=C_i_cdr(((C_word*)t0)[2]);
t3=C_a_i_cons(&a,2,((C_word*)t0)[3],t2);
t4=((C_word*)t0)[4];{
C_word av2[2];
av2[0]=t4;
av2[1]=C_i_set_cdr(((C_word*)t0)[2],t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[4];{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* ##sys#register-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6258(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(c<5) C_bad_min_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(22,c,1)))){
C_save_and_reclaim((void *)f_6258,c,av);}
a=C_alloc(22);
t5=C_rest_nullp(c,5);
t6=(C_truep(t5)?C_SCHEME_END_OF_LIST:C_get_rest_arg(c,5,av,5,t0));
t7=C_rest_nullp(c,5);
t8=C_rest_nullp(c,6);
t9=(C_truep(t8)?C_SCHEME_END_OF_LIST:C_get_rest_arg(c,6,av,5,t0));
t10=C_rest_nullp(c,6);
t11=C_a_i_record(&a,15,lf[4],t2,t3,t4,C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST,t6,t9,C_SCHEME_END_OF_LIST,C_SCHEME_FALSE);
t12=C_a_i_cons(&a,2,t2,t11);
t13=C_a_i_cons(&a,2,t12,*((C_word*)lf[25]+1));
t14=C_mutate((C_word*)lf[25]+1 /* (set! ##sys#module-table ...) */,t13);
t15=t1;{
C_word *av2=av;
av2[0]=t15;
av2[1]=t11;
((C_proc)(void*)(*((C_word*)t15+1)))(2,av2);}}

/* warn in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_6364(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_6364,4,t0,t1,t2,t3);}
a=C_alloc(8);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6372,a[2]=t1,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6376,a[2]=t4,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* modules.scm:266: scheme#symbol->string */
t6=*((C_word*)lf[98]+1);{
C_word av2[3];
av2[0]=t6;
av2[1]=t5;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k6370 in warn in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6372(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6372,c,av);}
/* modules.scm:265: ##sys#warn */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[38]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[38]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
tp(4,av2);}}

/* k6374 in warn in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6376(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_6376,c,av);}
/* modules.scm:266: scheme#string-append */
t2=*((C_word*)lf[95]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=lf[96];
av2[4]=t1;
av2[5]=lf[97];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* loop in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_6387(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(11,0,3)))){
C_save_and_reclaim_args((void *)trf_6387,3,t0,t1,t2);}
a=C_alloc(11);
if(C_truep(C_i_nullp(t2))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_i_car(t2);
if(C_truep(C_i_symbolp(t3))){
/* modules.scm:272: loop */
t9=t1;
t10=C_u_i_cdr(t2);
t1=t9;
t2=t10;
goto loop;}
else{
t4=C_i_cdar(t2);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_6414,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=t6,a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[5],a[8]=((C_word)li65),tmp=(C_word)a,a+=9,tmp));
t8=((C_word*)t6)[1];
f_6414(t8,t1,t4);}}}

/* loop2 in loop in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_6414(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,2)))){
C_save_and_reclaim_args((void *)trf_6414,3,t0,t1,t2);}
a=C_alloc(9);
if(C_truep(C_i_nullp(t2))){
/* modules.scm:275: loop */
t3=((C_word*)((C_word*)t0)[2])[1];
f_6387(t3,t1,C_i_cdr(((C_word*)t0)[3]));}
else{
t3=C_i_car(t2);
t4=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_6562,a[2]=t3,a[3]=((C_word*)t0)[4],a[4]=t1,a[5]=t2,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
/* modules.scm:276: ##sys#macro-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[28]+1));
C_word av2[2];
av2[0]=*((C_word*)lf[28]+1);
av2[1]=t4;
tp(2,av2);}}}

/* k6435 in k6560 in loop2 in loop in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6437(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6437,c,av);}
/* modules.scm:278: loop2 */
t2=((C_word*)((C_word*)t0)[2])[1];
f_6414(t2,((C_word*)t0)[3],C_u_i_cdr(((C_word*)t0)[4]));}

/* g1148 in k6560 in loop2 in loop in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_6451(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,0,3)))){
C_save_and_reclaim_args((void *)trf_6451,3,t0,t1,t2);}
a=C_alloc(13);
t3=C_i_car(((C_word*)t0)[2]);
t4=C_i_cdr(t2);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6476,a[2]=t3,a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[2],tmp=(C_word)a,a+=6,tmp);
if(C_truep(t4)){
t6=C_a_i_cons(&a,2,t3,t4);
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f12851,a[2]=t1,a[3]=t6,tmp=(C_word)a,a+=4,tmp);
/* modules.scm:285: loop2 */
t8=((C_word*)((C_word*)t0)[3])[1];
f_6414(t8,t7,C_u_i_cdr(((C_word*)t0)[2]));}
else{
/* modules.scm:284: module-rename */
f_9901(t5,C_u_i_car(((C_word*)t0)[2]),((C_word*)t0)[4]);}}

/* k6474 in g1148 in k6560 in loop2 in loop in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6476(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_6476,c,av);}
a=C_alloc(7);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f12847,a[2]=((C_word*)t0)[3],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* modules.scm:285: loop2 */
t4=((C_word*)((C_word*)t0)[4])[1];
f_6414(t4,t3,C_u_i_cdr(((C_word*)t0)[5]));}

/* g1155 in k6550 in k6560 in loop2 in loop in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_6491(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,3)))){
C_save_and_reclaim_args((void *)trf_6491,3,t0,t1,t2);}
a=C_alloc(7);
t3=C_i_cdr(t2);
if(C_truep(C_i_symbolp(t3))){
t4=C_i_car(((C_word*)t0)[2]);
t5=C_u_i_cdr(t2);
t6=C_a_i_cons(&a,2,t4,t5);
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6509,a[2]=t1,a[3]=t6,tmp=(C_word)a,a+=4,tmp);
/* modules.scm:289: loop2 */
t8=((C_word*)((C_word*)t0)[3])[1];
f_6414(t8,t7,C_u_i_cdr(((C_word*)t0)[2]));}
else{
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6520,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
/* modules.scm:291: warn */
t5=((C_word*)t0)[4];
f_6364(t5,t4,lf[100],C_i_car(((C_word*)t0)[2]));}}

/* k6507 in g1155 in k6550 in k6560 in loop2 in loop in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6509(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_6509,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k6518 in g1155 in k6550 in k6560 in loop2 in loop in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6520(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6520,c,av);}
/* modules.scm:292: loop2 */
t2=((C_word*)((C_word*)t0)[2])[1];
f_6414(t2,((C_word*)t0)[3],C_u_i_cdr(((C_word*)t0)[4]));}

/* k6537 in k6550 in k6560 in loop2 in loop in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6539(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6539,c,av);}
/* modules.scm:295: loop2 */
t2=((C_word*)((C_word*)t0)[2])[1];
f_6414(t2,((C_word*)t0)[3],C_u_i_cdr(((C_word*)t0)[4]));}

/* k6550 in k6560 in loop2 in loop in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6552(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_6552,c,av);}
a=C_alloc(6);
t2=C_i_assq(((C_word*)t0)[2],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6491,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word)li64),tmp=(C_word)a,a+=6,tmp);
/* modules.scm:275: g1155 */
t4=t3;
f_6491(t4,((C_word*)t0)[6],t2);}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6539,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* modules.scm:294: warn */
t4=((C_word*)t0)[5];
f_6364(t4,t3,lf[101],C_u_i_car(((C_word*)t0)[3]));}}

/* k6560 in loop2 in loop in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6562(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_6562,c,av);}
a=C_alloc(7);
if(C_truep(C_i_assq(((C_word*)t0)[2],t1))){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6437,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp);
/* modules.scm:277: warn */
t3=((C_word*)t0)[6];
f_6364(t3,t2,lf[99],C_u_i_car(((C_word*)t0)[5]));}
else{
t2=C_i_assq(C_u_i_car(((C_word*)t0)[5]),((C_word*)t0)[7]);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6451,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[8],a[5]=((C_word)li63),tmp=(C_word)a,a+=6,tmp);
/* modules.scm:275: g1148 */
t4=t3;
f_6451(t4,((C_word*)t0)[4],t2);}
else{
t3=C_u_i_car(((C_word*)t0)[5]);
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6552,a[2]=t3,a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[4],tmp=(C_word)a,a+=7,tmp);
/* modules.scm:286: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[29]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[29]+1);
av2[1]=t4;
tp(2,av2);}}}}

/* merge-se in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_6568(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_6568,2,t1,t2);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6572,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* modules.scm:298: chicken.internal#make-hash-table */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[55]+1));
C_word av2[2];
av2[0]=*((C_word*)lf[55]+1);
av2[1]=t3;
tp(2,av2);}}

/* k6570 in merge-se in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6572(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_6572,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6575,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* modules.scm:298: scheme#reverse */
t3=*((C_word*)lf[54]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k6573 in k6570 in merge-se in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6575(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,5)))){
C_save_and_reclaim((void *)f_6575,c,av);}
a=C_alloc(7);
t2=C_i_cdr(t1);
t3=C_u_i_car(t1);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6586,a[2]=t5,a[3]=((C_word*)t0)[2],a[4]=((C_word)li33),tmp=(C_word)a,a+=5,tmp));
t7=((C_word*)t5)[1];
f_6586(t7,((C_word*)t0)[3],t2,C_SCHEME_FALSE,t3);}

/* loop in k6573 in k6570 in merge-se in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_6586(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(17,0,4)))){
C_save_and_reclaim_args((void *)trf_6586,5,t0,t1,t2,t3,t4);}
a=C_alloc(17);
if(C_truep(C_i_nullp(t2))){
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t5=C_i_car(t2);
t6=C_eqp(t3,t5);
t7=(C_truep(t6)?t6:C_i_nullp(C_u_i_car(t2)));
if(C_truep(t7)){
/* modules.scm:302: loop */
t15=t1;
t16=C_u_i_cdr(t2);
t17=t3;
t18=t4;
t1=t15;
t2=t16;
t3=t17;
t4=t18;
goto loop;}
else{
if(C_truep(C_i_not(t3))){
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6614,a[2]=((C_word*)t0)[3],a[3]=((C_word)li30),tmp=(C_word)a,a+=4,tmp);
t9=C_i_check_list_2(t4,lf[33]);
t10=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6628,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,a[5]=t4,tmp=(C_word)a,a+=6,tmp);
t11=C_SCHEME_UNDEFINED;
t12=(*a=C_VECTOR_TYPE|1,a[1]=t11,tmp=(C_word)a,a+=2,tmp);
t13=C_set_block_item(t12,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6633,a[2]=t12,a[3]=t8,a[4]=((C_word)li31),tmp=(C_word)a,a+=5,tmp));
t14=((C_word*)t12)[1];
f_6633(t14,t10,t4);}
else{
t8=C_u_i_car(t2);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6661,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=t10,a[5]=((C_word*)t0)[3],a[6]=((C_word)li32),tmp=(C_word)a,a+=7,tmp));
t12=((C_word*)t10)[1];
f_6661(t12,t1,t8,t4);}}}}

/* g1186 in loop in k6573 in k6570 in merge-se in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_6614(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,4)))){
C_save_and_reclaim_args((void *)trf_6614,3,t0,t1,t2);}
/* modules.scm:305: chicken.internal#hash-table-set! */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[52]+1));
C_word av2[5];
av2[0]=*((C_word*)lf[52]+1);
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
av2[3]=C_i_car(t2);
av2[4]=C_SCHEME_TRUE;
tp(5,av2);}}

/* k6626 in loop in k6573 in k6570 in merge-se in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6628(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6628,c,av);}
/* modules.scm:306: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_6586(t2,((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[5]);}

/* for-each-loop1185 in loop in k6573 in k6570 in merge-se in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_6633(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_6633,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6643,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* modules.scm:305: g1186 */
t4=((C_word*)t0)[3];
f_6614(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k6641 in for-each-loop1185 in loop in k6573 in k6570 in merge-se in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6643(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6643,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_6633(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* lp in loop in k6573 in k6570 in merge-se in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_6661(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,4)))){
C_save_and_reclaim_args((void *)trf_6661,4,t0,t1,t2,t3);}
a=C_alloc(7);
if(C_truep(C_i_nullp(t2))){
/* modules.scm:308: loop */
t4=((C_word*)((C_word*)t0)[2])[1];
f_6586(t4,t1,C_i_cdr(((C_word*)t0)[3]),C_u_i_car(((C_word*)t0)[3]),t3);}
else{
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6683,a[2]=((C_word*)t0)[4],a[3]=t1,a[4]=t2,a[5]=t3,a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* modules.scm:309: chicken.internal#hash-table-ref */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[53]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[53]+1);
av2[1]=t4;
av2[2]=((C_word*)t0)[5];
av2[3]=C_i_caar(t2);
tp(4,av2);}}}

/* k6681 in lp in loop in k6573 in k6570 in merge-se in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6683(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_6683,c,av);}
a=C_alloc(6);
if(C_truep(t1)){
/* modules.scm:310: lp */
t2=((C_word*)((C_word*)t0)[2])[1];
f_6661(t2,((C_word*)t0)[3],C_u_i_cdr(((C_word*)t0)[4]),((C_word*)t0)[5]);}
else{
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6691,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
/* modules.scm:311: chicken.internal#hash-table-set! */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[52]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[52]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[6];
av2[3]=C_i_caar(((C_word*)t0)[4]);
av2[4]=C_SCHEME_TRUE;
tp(5,av2);}}}

/* k6689 in k6681 in lp in loop in k6573 in k6570 in merge-se in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6691(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_6691,c,av);}
a=C_alloc(3);
t2=C_u_i_cdr(((C_word*)t0)[2]);
t3=C_u_i_car(((C_word*)t0)[2]);
t4=C_a_i_cons(&a,2,t3,((C_word*)t0)[3]);
/* modules.scm:312: lp */
t5=((C_word*)((C_word*)t0)[4])[1];
f_6661(t5,((C_word*)t0)[5],t2,t4);}

/* ##sys#compiled-module-registration in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6721(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(14,c,2)))){
C_save_and_reclaim((void *)f_6721,c,av);}
a=C_alloc(14);
t3=C_i_check_structure_2(t2,lf[4],lf[42]);
t4=C_i_block_ref(t2,C_fix(4));
t5=C_i_check_structure_2(t2,lf[4],lf[6]);
t6=C_i_block_ref(t2,C_fix(1));
t7=C_i_check_structure_2(t2,lf[4],lf[57]);
t8=C_i_block_ref(t2,C_fix(8));
t9=C_i_check_structure_2(t2,lf[4],lf[14]);
t10=C_i_block_ref(t2,C_fix(12));
t11=C_i_check_structure_2(t2,lf[4],lf[58]);
t12=C_i_block_ref(t2,C_fix(9));
t13=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6756,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t14=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6760,a[2]=t13,a[3]=t2,a[4]=t4,a[5]=t10,a[6]=t6,a[7]=t12,tmp=(C_word)a,a+=8,tmp);
t15=C_i_pairp(t8);
t16=(C_truep(t15)?C_i_pairp(t10):C_SCHEME_FALSE);
if(C_truep(t16)){
t17=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7116,a[2]=t14,tmp=(C_word)a,a+=3,tmp);
/* modules.scm:323: chicken.syntax#strip-syntax */
t18=*((C_word*)lf[67]+1);{
C_word *av2=av;
av2[0]=t18;
av2[1]=t17;
av2[2]=t8;
((C_proc)(void*)(*((C_word*)t18+1)))(3,av2);}}
else{
t17=t14;
f_6760(t17,C_SCHEME_END_OF_LIST);}}

/* k6754 in ##sys#compiled-module-registration in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6756(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,1)))){
C_save_and_reclaim((void *)f_6756,c,av);}
a=C_alloc(15);
t2=C_a_i_cons(&a,2,C_SCHEME_END_OF_LIST,t1);
t3=C_a_i_cons(&a,2,lf[59],t2);
t4=C_a_i_list(&a,2,lf[60],t3);
t5=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_a_i_list(&a,1,t4);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* k6758 in ##sys#compiled-module-registration in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_6760(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,0,2)))){
C_save_and_reclaim_args((void *)trf_6760,2,t0,t1);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6764,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6768,a[2]=t2,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
t4=C_i_pairp(((C_word*)t0)[7]);
t5=(C_truep(t4)?C_i_pairp(((C_word*)t0)[5]):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7088,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* modules.scm:326: chicken.syntax#strip-syntax */
t7=*((C_word*)lf[67]+1);{
C_word av2[3];
av2[0]=t7;
av2[1]=t6;
av2[2]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}
else{
t6=t3;
f_6768(t6,C_SCHEME_END_OF_LIST);}}

/* k6762 in k6758 in ##sys#compiled-module-registration in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6764(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6764,c,av);}
/* modules.scm:320: ##sys#append */
t2=*((C_word*)lf[61]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k6766 in k6758 in ##sys#compiled-module-registration in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_6768(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,0,2)))){
C_save_and_reclaim_args((void *)trf_6768,2,t0,t1);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6772,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6776,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=t2,tmp=(C_word)a,a+=6,tmp);
t4=C_i_getprop(((C_word*)t0)[6],lf[69],C_SCHEME_FALSE);
t5=(C_truep(t4)?t4:C_i_pairp(((C_word*)t0)[5]));
if(C_truep(t5)){
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7067,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
t7=C_i_check_structure_2(((C_word*)t0)[3],lf[4],lf[36]);
/* modules.scm:329: chicken.syntax#strip-syntax */
t8=*((C_word*)lf[67]+1);{
C_word av2[3];
av2[0]=t8;
av2[1]=t6;
av2[2]=C_i_block_ref(((C_word*)t0)[3],C_fix(10));
((C_proc)(void*)(*((C_word*)t8+1)))(3,av2);}}
else{
t6=t3;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_SCHEME_END_OF_LIST;
f_6776(2,av2);}}}

/* k6770 in k6766 in k6758 in ##sys#compiled-module-registration in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6772(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6772,c,av);}
/* modules.scm:320: ##sys#append */
t2=*((C_word*)lf[61]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k6774 in k6766 in k6758 in ##sys#compiled-module-registration in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6776(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(34,c,3)))){
C_save_and_reclaim((void *)f_6776,c,av);}
a=C_alloc(34);
t2=C_i_check_structure_2(((C_word*)t0)[2],lf[4],lf[6]);
t3=C_i_block_ref(((C_word*)t0)[2],C_fix(1));
t4=C_a_i_list(&a,2,lf[62],t3);
t5=C_i_check_structure_2(((C_word*)t0)[2],lf[4],lf[63]);
t6=C_i_block_ref(((C_word*)t0)[2],C_fix(2));
t7=C_a_i_list(&a,2,lf[62],t6);
t8=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t9=t8;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=((C_word*)t10)[1];
t12=C_i_check_structure_2(((C_word*)t0)[2],lf[4],lf[64]);
t13=C_i_block_ref(((C_word*)t0)[2],C_fix(13));
t14=C_i_check_list_2(t13,lf[18]);
t15=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_7010,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t4,a[6]=t7,a[7]=((C_word*)t0)[5],a[8]=t1,tmp=(C_word)a,a+=9,tmp);
t16=C_SCHEME_UNDEFINED;
t17=(*a=C_VECTOR_TYPE|1,a[1]=t16,tmp=(C_word)a,a+=2,tmp);
t18=C_set_block_item(t17,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7012,a[2]=t10,a[3]=t17,a[4]=t11,a[5]=((C_word)li38),tmp=(C_word)a,a+=6,tmp));
t19=((C_word*)t17)[1];
f_7012(t19,t15,t13);}

/* k6806 in k6919 in k7008 in k6774 in k6766 in k6758 in ##sys#compiled-module-registration in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_6808(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,0,3)))){
C_save_and_reclaim_args((void *)trf_6808,2,t0,t1);}
a=C_alloc(24);
t2=C_a_i_list(&a,7,lf[68],((C_word*)t0)[2],((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6],t1);
t3=C_a_i_list(&a,1,t2);
/* modules.scm:320: ##sys#append */
t4=*((C_word*)lf[61]+1);{
C_word av2[4];
av2[0]=t4;
av2[1]=((C_word*)t0)[7];
av2[2]=((C_word*)t0)[8];
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k6810 in k6919 in k7008 in k6774 in k6766 in k6758 in ##sys#compiled-module-registration in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6812(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_6812,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];
f_6808(t2,C_a_i_cons(&a,2,lf[65],t1));}

/* loop in k6919 in k7008 in k6774 in k6766 in k6758 in ##sys#compiled-module-registration in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_6824(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(12,0,2)))){
C_save_and_reclaim_args((void *)trf_6824,3,t0,t1,t2);}
a=C_alloc(12);
if(C_truep(C_i_nullp(t2))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_i_caar(t2);
if(C_truep(C_i_assq(t3,((C_word*)t0)[2]))){
/* modules.scm:356: loop */
t11=t1;
t12=C_u_i_cdr(t2);
t1=t11;
t2=t12;
goto loop;}
else{
t4=C_i_caar(t2);
t5=C_u_i_car(t2);
t6=C_u_i_car(t5);
t7=C_a_i_list(&a,2,lf[62],t6);
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6866,a[2]=t7,a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=t2,tmp=(C_word)a,a+=6,tmp);
t9=C_u_i_car(t2);
/* modules.scm:359: chicken.syntax#strip-syntax */
t10=*((C_word*)lf[67]+1);{
C_word av2[3];
av2[0]=t10;
av2[1]=t8;
av2[2]=C_u_i_cdr(t9);
((C_proc)(void*)(*((C_word*)t10+1)))(3,av2);}}}}

/* k6854 in k6864 in loop in k6919 in k7008 in k6774 in k6766 in k6758 in ##sys#compiled-module-registration in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6856(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_6856,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k6864 in loop in k6919 in k7008 in k6774 in k6766 in k6758 in ##sys#compiled-module-registration in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6866(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_6866,c,av);}
a=C_alloc(13);
t2=C_a_i_list(&a,3,lf[66],((C_word*)t0)[2],t1);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6856,a[2]=((C_word*)t0)[3],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* modules.scm:360: loop */
t4=((C_word*)((C_word*)t0)[4])[1];
f_6824(t4,t3,C_u_i_cdr(((C_word*)t0)[5]));}

/* g1278 in k7008 in k6774 in k6766 in k6758 in ##sys#compiled-module-registration in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_6881(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,2)))){
C_save_and_reclaim_args((void *)trf_6881,3,t0,t1,t2);}
a=C_alloc(10);
t3=C_i_car(t2);
t4=C_i_assq(t3,((C_word*)t0)[2]);
if(C_truep(C_i_pairp(t4))){
t5=C_u_i_car(t2);
t6=C_a_i_list(&a,2,lf[62],t5);
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6905,a[2]=t1,a[3]=t6,tmp=(C_word)a,a+=4,tmp);
/* modules.scm:346: chicken.syntax#strip-syntax */
t8=*((C_word*)lf[67]+1);{
C_word av2[3];
av2[0]=t8;
av2[1]=t7;
av2[2]=C_u_i_cdr(t4);
((C_proc)(void*)(*((C_word*)t8+1)))(3,av2);}}
else{
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=C_a_i_list(&a,2,lf[62],t3);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* k6903 in g1278 in k7008 in k6774 in k6766 in k6758 in ##sys#compiled-module-registration in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6905(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_6905,c,av);}
a=C_alloc(9);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_list(&a,3,lf[66],((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k6919 in k7008 in k6774 in k6766 in k6758 in ##sys#compiled-module-registration in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6921(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(22,c,3)))){
C_save_and_reclaim((void *)f_6921,c,av);}
a=C_alloc(22);
t2=C_a_i_cons(&a,2,lf[65],t1);
t3=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_6808,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t2,a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6812,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_i_nullp(((C_word*)t0)[8]))){
t5=t3;
f_6808(t5,C_a_i_cons(&a,2,lf[65],C_SCHEME_END_OF_LIST));}
else{
t5=C_i_check_structure_2(((C_word*)t0)[9],lf[4],lf[47]);
t6=C_i_block_ref(((C_word*)t0)[9],C_fix(6));
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6824,a[2]=((C_word*)t0)[8],a[3]=t8,a[4]=((C_word)li36),tmp=(C_word)a,a+=5,tmp));
t10=((C_word*)t8)[1];
f_6824(t10,t4,t6);}}

/* map-loop1272 in k7008 in k6774 in k6766 in k6758 in ##sys#compiled-module-registration in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_6923(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_6923,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6948,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* modules.scm:342: g1278 */
t4=((C_word*)t0)[4];
f_6881(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k6946 in map-loop1272 in k7008 in k6774 in k6766 in k6758 in ##sys#compiled-module-registration in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_6948(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_6948,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_6923(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k7008 in k6774 in k6766 in k6758 in ##sys#compiled-module-registration in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7010(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(37,c,3)))){
C_save_and_reclaim((void *)f_7010,c,av);}
a=C_alloc(37);
t2=C_a_i_cons(&a,2,lf[65],t1);
t3=C_i_check_structure_2(((C_word*)t0)[2],lf[4],lf[13]);
t4=C_i_block_ref(((C_word*)t0)[2],C_fix(11));
t5=C_a_i_list(&a,2,lf[62],t4);
t6=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t7=t6;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=((C_word*)t8)[1];
t10=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6881,a[2]=((C_word*)t0)[3],a[3]=((C_word)li35),tmp=(C_word)a,a+=4,tmp);
t11=C_i_check_list_2(((C_word*)t0)[4],lf[18]);
t12=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_6921,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],a[4]=t2,a[5]=t5,a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[4],a[9]=((C_word*)t0)[2],tmp=(C_word)a,a+=10,tmp);
t13=C_SCHEME_UNDEFINED;
t14=(*a=C_VECTOR_TYPE|1,a[1]=t13,tmp=(C_word)a,a+=2,tmp);
t15=C_set_block_item(t14,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6923,a[2]=t8,a[3]=t14,a[4]=t10,a[5]=t9,a[6]=((C_word)li37),tmp=(C_word)a,a+=7,tmp));
t16=((C_word*)t14)[1];
f_6923(t16,t12,((C_word*)t0)[4]);}

/* map-loop1239 in k6774 in k6766 in k6758 in ##sys#compiled-module-registration in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_7012(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(30,0,2)))){
C_save_and_reclaim_args((void *)trf_7012,3,t0,t1,t2);}
a=C_alloc(30);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7037,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
t4=C_slot(t2,C_fix(0));
t5=C_i_cdr(t4);
if(C_truep(C_i_symbolp(t5))){
t6=C_u_i_car(t4);
t7=C_u_i_cdr(t4);
t8=C_a_i_cons(&a,2,t6,t7);
t9=t3;
f_7037(t9,C_a_i_list(&a,2,lf[62],t8));}
else{
t6=C_u_i_car(t4);
t7=C_a_i_list(&a,2,lf[62],t6);
t8=C_a_i_list(&a,2,lf[62],C_SCHEME_END_OF_LIST);
t9=C_u_i_cdr(t4);
t10=t3;
f_7037(t10,C_a_i_list(&a,4,lf[65],t7,t8,t9));}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k7035 in map-loop1239 in k6774 in k6766 in k6758 in ##sys#compiled-module-registration in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_7037(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_7037,2,t0,t1);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_7012(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k7065 in k6766 in k6758 in ##sys#compiled-module-registration in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7067(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7067,c,av);}
/* modules.scm:329: ##sys#fast-reverse */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[70]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[70]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
tp(3,av2);}}

/* k7086 in k6758 in ##sys#compiled-module-registration in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7088(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_7088,c,av);}
a=C_alloc(6);
t2=C_a_i_cons(&a,2,lf[71],t1);
t3=((C_word*)t0)[2];
f_6768(t3,C_a_i_list(&a,1,t2));}

/* k7114 in ##sys#compiled-module-registration in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7116(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,1)))){
C_save_and_reclaim((void *)f_7116,c,av);}
a=C_alloc(18);
t2=C_a_i_cons(&a,2,lf[71],t1);
t3=C_a_i_list(&a,2,lf[62],t2);
t4=C_a_i_list(&a,2,lf[72],t3);
t5=((C_word*)t0)[2];
f_6760(t5,C_a_i_list(&a,1,t4));}

/* ##sys#register-compiled-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7124(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6=av[6];
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word *a;
if(c<7) C_bad_min_argc_2(c,7,t0);
if(C_unlikely(!C_demand(C_calculate_demand(21,c,3)))){
C_save_and_reclaim((void *)f_7124,c,av);}
a=C_alloc(21);
t7=C_rest_nullp(c,7);
t8=(C_truep(t7)?C_SCHEME_END_OF_LIST:C_get_rest_arg(c,7,av,7,t0));
t9=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t10=t9;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=((C_word*)t11)[1];
t13=C_i_check_list_2(t6,lf[18]);
t14=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_7189,a[2]=t8,a[3]=t2,a[4]=t3,a[5]=t5,a[6]=t4,a[7]=t1,tmp=(C_word)a,a+=8,tmp);
t15=C_SCHEME_UNDEFINED;
t16=(*a=C_VECTOR_TYPE|1,a[1]=t15,tmp=(C_word)a,a+=2,tmp);
t17=C_set_block_item(t16,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7410,a[2]=t11,a[3]=t16,a[4]=t12,a[5]=((C_word)li45),tmp=(C_word)a,a+=6,tmp));
t18=((C_word*)t16)[1];
f_7410(t18,t14,t6);}

/* k7152 in map-loop1346 in ##sys#register-compiled-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7154(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_7154,c,av);}
a=C_alloc(3);
t2=C_i_assq(((C_word*)t0)[2],t1);
t3=(C_truep(t2)?C_i_pairp(C_i_cdr(t2)):C_SCHEME_FALSE);
if(C_truep(t3)){
t4=((C_word*)t0)[3];
f_7420(t4,C_a_i_cons(&a,2,t2,C_SCHEME_END_OF_LIST));}
else{
/* modules.scm:372: ##sys#error */
t4=*((C_word*)lf[74]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[4];
av2[2]=lf[75];
av2[3]=lf[76];
av2[4]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}}

/* k7177 in map-loop1346 in ##sys#register-compiled-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7179(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,1)))){
C_save_and_reclaim((void *)f_7179,c,av);}
a=C_alloc(12);
t2=C_a_i_list3(&a,3,((C_word*)t0)[2],C_SCHEME_FALSE,t1);
t3=((C_word*)t0)[3];
f_7420(t3,C_a_i_cons(&a,2,t2,C_SCHEME_END_OF_LIST));}

/* k7187 in ##sys#register-compiled-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7189(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(21,c,3)))){
C_save_and_reclaim((void *)f_7189,c,av);}
a=C_alloc(21);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=C_i_check_list_2(((C_word*)t0)[2],lf[18]);
t7=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_7215,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=t1,a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7376,a[2]=t4,a[3]=t9,a[4]=t5,a[5]=((C_word)li44),tmp=(C_word)a,a+=6,tmp));
t11=((C_word*)t9)[1];
f_7376(t11,t7,((C_word*)t0)[2]);}

/* k7203 in map-loop1374 in k7187 in ##sys#register-compiled-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7205(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_7205,c,av);}
a=C_alloc(12);
t2=C_a_i_list3(&a,3,((C_word*)t0)[2],C_SCHEME_FALSE,t1);
t3=C_a_i_cons(&a,2,t2,C_SCHEME_END_OF_LIST);
t4=C_i_setslot(((C_word*)((C_word*)t0)[3])[1],C_fix(1),t3);
t5=C_mutate(((C_word *)((C_word*)t0)[3])+1,t3);
t6=((C_word*)((C_word*)t0)[4])[1];
f_7376(t6,((C_word*)t0)[5],C_slot(((C_word*)t0)[6],C_fix(1)));}

/* k7213 in k7187 in ##sys#register-compiled-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7215(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(31,c,2)))){
C_save_and_reclaim((void *)f_7215,c,av);}
a=C_alloc(31);
t2=C_a_i_record(&a,15,lf[4],((C_word*)t0)[2],((C_word*)t0)[3],C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST,((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6],C_SCHEME_FALSE);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_7221,a[2]=((C_word*)t0)[5],a[3]=t1,a[4]=((C_word*)t0)[2],a[5]=t2,a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[4],tmp=(C_word)a,a+=8,tmp);
t4=C_i_nullp(((C_word*)t0)[5]);
t5=C_i_not(t4);
t6=(C_truep(t5)?t5:C_i_not(C_i_nullp(t1)));
if(C_truep(t6)){
t7=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7359,a[2]=t3,a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,tmp=(C_word)a,a+=7,tmp);
/* modules.scm:389: ##sys#macro-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[28]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[28]+1);
av2[1]=t7;
tp(2,av2);}}
else{
t7=t3;{
C_word *av2=av;
av2[0]=t7;
av2[1]=C_SCHEME_END_OF_LIST;
f_7221(2,av2);}}}

/* k7219 in k7213 in k7187 in ##sys#register-compiled-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7221(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(20,c,3)))){
C_save_and_reclaim((void *)f_7221,c,av);}
a=C_alloc(20);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7222,a[2]=t1,a[3]=((C_word)li40),tmp=(C_word)a,a+=4,tmp);
t3=C_i_check_list_2(((C_word*)t0)[2],lf[33]);
t4=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_7247,a[2]=t1,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[2],tmp=(C_word)a,a+=9,tmp);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7325,a[2]=t6,a[3]=t2,a[4]=((C_word)li43),tmp=(C_word)a,a+=5,tmp));
t8=((C_word*)t6)[1];
f_7325(t8,t4,((C_word*)t0)[2]);}

/* g1407 in k7219 in k7213 in k7187 in ##sys#register-compiled-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_7222(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,2)))){
C_save_and_reclaim_args((void *)trf_7222,3,t0,t1,t2);}
a=C_alloc(10);
t3=C_i_cdr(t2);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7234,a[2]=t1,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
t5=C_i_cadr(t2);
if(C_truep(t5)){
/* modules.scm:395: merge-se */
f_6568(t4,C_a_i_list(&a,2,t5,((C_word*)t0)[2]));}
else{
/* modules.scm:395: merge-se */
f_6568(t4,C_a_i_list(&a,2,C_SCHEME_END_OF_LIST,((C_word*)t0)[2]));}}

/* k7232 in g1407 in k7219 in k7213 in k7187 in ##sys#register-compiled-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7234(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7234,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_i_set_car(((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k7245 in k7219 in k7213 in k7187 in ##sys#register-compiled-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7247(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,3)))){
C_save_and_reclaim((void *)f_7247,c,av);}
a=C_alloc(18);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7248,a[2]=((C_word*)t0)[2],a[3]=((C_word)li41),tmp=(C_word)a,a+=4,tmp);
t3=C_i_check_list_2(((C_word*)t0)[3],lf[33]);
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7273,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[7],a[6]=((C_word*)t0)[8],tmp=(C_word)a,a+=7,tmp);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7302,a[2]=t6,a[3]=t2,a[4]=((C_word)li42),tmp=(C_word)a,a+=5,tmp));
t8=((C_word*)t6)[1];
f_7302(t8,t4,((C_word*)t0)[3]);}

/* g1417 in k7245 in k7219 in k7213 in k7187 in ##sys#register-compiled-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_7248(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,2)))){
C_save_and_reclaim_args((void *)trf_7248,3,t0,t1,t2);}
a=C_alloc(10);
t3=C_i_cdr(t2);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7260,a[2]=t1,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
t5=C_i_cadr(t2);
if(C_truep(t5)){
/* modules.scm:399: merge-se */
f_6568(t4,C_a_i_list(&a,2,t5,((C_word*)t0)[2]));}
else{
/* modules.scm:399: merge-se */
f_6568(t4,C_a_i_list(&a,2,C_SCHEME_END_OF_LIST,((C_word*)t0)[2]));}}

/* k7258 in g1417 in k7245 in k7219 in k7213 in k7187 in ##sys#register-compiled-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7260(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7260,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_i_set_car(((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k7271 in k7245 in k7219 in k7213 in k7187 in ##sys#register-compiled-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7273(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,2)))){
C_save_and_reclaim((void *)f_7273,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7276,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7292,a[2]=((C_word*)t0)[3],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7300,a[2]=t3,a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],tmp=(C_word)a,a+=5,tmp);
/* modules.scm:403: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[29]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[29]+1);
av2[1]=t4;
tp(2,av2);}}

/* k7274 in k7271 in k7245 in k7219 in k7213 in k7187 in ##sys#register-compiled-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7276(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_7276,c,av);}
a=C_alloc(6);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],((C_word*)t0)[3]);
t3=C_a_i_cons(&a,2,t2,*((C_word*)lf[25]+1));
t4=C_mutate((C_word*)lf[25]+1 /* (set! ##sys#module-table ...) */,t3);
t5=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t5;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* k7290 in k7271 in k7245 in k7219 in k7213 in k7187 in ##sys#register-compiled-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7292(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_7292,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7296,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* modules.scm:404: ##sys#macro-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[28]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[28]+1);
av2[1]=t2;
tp(2,av2);}}

/* k7294 in k7290 in k7271 in k7245 in k7219 in k7213 in k7187 in ##sys#register-compiled-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7296(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_7296,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_i_check_structure_2(((C_word*)t0)[3],lf[4],C_SCHEME_FALSE);
/* modules.scm:93: ##sys#block-set! */
t4=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[4];
av2[2]=((C_word*)t0)[3];
av2[3]=C_fix(14);
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k7298 in k7271 in k7245 in k7219 in k7213 in k7187 in ##sys#register-compiled-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7300(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_7300,c,av);}
a=C_alloc(9);
/* modules.scm:403: merge-se */
f_6568(((C_word*)t0)[2],C_a_i_list(&a,3,t1,((C_word*)t0)[3],((C_word*)t0)[4]));}

/* for-each-loop1416 in k7245 in k7219 in k7213 in k7187 in ##sys#register-compiled-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_7302(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_7302,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7312,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* modules.scm:397: g1417 */
t4=((C_word*)t0)[3];
f_7248(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k7310 in for-each-loop1416 in k7245 in k7219 in k7213 in k7187 in ##sys#register-compiled-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7312(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7312,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_7302(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* for-each-loop1406 in k7219 in k7213 in k7187 in ##sys#register-compiled-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_7325(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_7325,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7335,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* modules.scm:393: g1407 */
t4=((C_word*)t0)[3];
f_7222(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k7333 in for-each-loop1406 in k7219 in k7213 in k7187 in ##sys#register-compiled-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7335(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7335,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_7325(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k7357 in k7213 in k7187 in ##sys#register-compiled-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7359(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_7359,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_7363,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* modules.scm:390: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[29]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[29]+1);
av2[1]=t2;
tp(2,av2);}}

/* k7361 in k7357 in k7213 in k7187 in ##sys#register-compiled-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7363(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,2)))){
C_save_and_reclaim((void *)f_7363,c,av);}
a=C_alloc(18);
/* modules.scm:388: merge-se */
f_6568(((C_word*)t0)[2],C_a_i_list(&a,6,((C_word*)t0)[3],t1,((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6],((C_word*)t0)[7]));}

/* map-loop1374 in k7187 in ##sys#register-compiled-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_7376(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,3)))){
C_save_and_reclaim_args((void *)trf_7376,3,t0,t1,t2);}
a=C_alloc(7);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_car(t3);
t5=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7205,a[2]=t4,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=t1,a[6]=t2,tmp=(C_word)a,a+=7,tmp);
/* modules.scm:383: ##sys#ensure-transformer */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[73]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[73]+1);
av2[1]=t5;
av2[2]=C_u_i_cdr(t3);
av2[3]=C_u_i_car(t3);
tp(4,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* map-loop1346 in ##sys#register-compiled-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_7410(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,0,3)))){
C_save_and_reclaim_args((void *)trf_7410,3,t0,t1,t2);}
a=C_alloc(14);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7420,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7435,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
t5=C_slot(t2,C_fix(0));
if(C_truep(C_i_symbolp(t5))){
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7154,a[2]=t5,a[3]=t3,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
/* modules.scm:369: ##sys#macro-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[28]+1));
C_word av2[2];
av2[0]=*((C_word*)lf[28]+1);
av2[1]=t6;
tp(2,av2);}}
else{
t6=C_i_car(t5);
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7179,a[2]=t6,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* modules.scm:379: ##sys#ensure-transformer */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[73]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[73]+1);
av2[1]=t7;
av2[2]=C_u_i_cdr(t5);
av2[3]=C_u_i_car(t5);
tp(4,av2);}}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k7418 in map-loop1346 in ##sys#register-compiled-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_7420(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,2)))){
C_save_and_reclaim_args((void *)trf_7420,2,t0,t1);}
t2=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t1);
t3=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t4=((C_word*)((C_word*)t0)[3])[1];
f_7410(t4,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k7433 in map-loop1346 in ##sys#register-compiled-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7435(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_7435,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];
f_7420(t2,C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST));}

/* ##sys#register-core-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7450(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(c<5) C_bad_min_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_7450,c,av);}
a=C_alloc(7);
t5=C_rest_nullp(c,5);
t6=(C_truep(t5)?C_SCHEME_END_OF_LIST:C_get_rest_arg(c,5,av,5,t0));
t7=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7457,a[2]=t2,a[3]=t6,a[4]=t3,a[5]=t4,a[6]=t1,tmp=(C_word)a,a+=7,tmp);
/* modules.scm:409: ##sys#macro-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[28]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[28]+1);
av2[1]=t7;
tp(2,av2);}}

/* k7455 in ##sys#register-core-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7457(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(25,c,3)))){
C_save_and_reclaim((void *)f_7457,c,av);}
a=C_alloc(25);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7500,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word)li47),tmp=(C_word)a,a+=5,tmp);
t7=C_i_check_list_2(((C_word*)t0)[3],lf[18]);
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7522,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],tmp=(C_word)a,a+=6,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7524,a[2]=t4,a[3]=t10,a[4]=t6,a[5]=t5,a[6]=((C_word)li48),tmp=(C_word)a,a+=7,tmp));
t12=((C_word*)t10)[1];
f_7524(t12,t8,((C_word*)t0)[3]);}

/* k7461 in k7520 in k7455 in ##sys#register-core-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7463(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,1)))){
C_save_and_reclaim((void *)f_7463,c,av);}
a=C_alloc(6);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],((C_word*)t0)[3]);
t3=C_a_i_cons(&a,2,t2,*((C_word*)lf[25]+1));
t4=C_mutate((C_word*)lf[25]+1 /* (set! ##sys#module-table ...) */,t3);
t5=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t5;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* k7477 in k7520 in k7455 in ##sys#register-core-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7479(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_7479,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7483,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* modules.scm:427: ##sys#macro-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[28]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[28]+1);
av2[1]=t2;
tp(2,av2);}}

/* k7481 in k7477 in k7520 in k7455 in ##sys#register-core-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7483(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_7483,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_i_check_structure_2(((C_word*)t0)[3],lf[4],C_SCHEME_FALSE);
/* modules.scm:93: ##sys#block-set! */
t4=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[4];
av2[2]=((C_word*)t0)[3];
av2[3]=C_fix(14);
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k7485 in k7520 in k7455 in ##sys#register-core-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7487(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_7487,c,av);}
a=C_alloc(9);
t2=C_i_check_structure_2(((C_word*)t0)[2],lf[4],lf[13]);
t3=C_i_block_ref(((C_word*)t0)[2],C_fix(11));
t4=C_i_check_structure_2(((C_word*)t0)[2],lf[4],lf[14]);
t5=C_i_block_ref(((C_word*)t0)[2],C_fix(12));
/* modules.scm:424: merge-se */
f_6568(((C_word*)t0)[3],C_a_i_list(&a,3,t1,t3,t5));}

/* g1475 in k7455 in ##sys#register-core-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_7500(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,4)))){
C_save_and_reclaim_args((void *)trf_7500,3,t0,t1,t2);}
if(C_truep(C_i_symbolp(t2))){
t3=C_i_assq(t2,((C_word*)t0)[2]);
if(C_truep(t3)){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
/* modules.scm:416: ##sys#error */
t4=*((C_word*)lf[74]+1);{
C_word av2[5];
av2[0]=t4;
av2[1]=t1;
av2[2]=lf[78];
av2[3]=t2;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k7520 in k7455 in ##sys#register-core-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7522(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(29,c,2)))){
C_save_and_reclaim((void *)f_7522,c,av);}
a=C_alloc(29);
t2=C_a_i_record(&a,15,lf[4],((C_word*)t0)[2],((C_word*)t0)[3],C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST,((C_word*)t0)[4],t1,C_SCHEME_END_OF_LIST,C_SCHEME_FALSE);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7463,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7479,a[2]=t2,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7487,a[2]=t2,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* modules.scm:424: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[29]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[29]+1);
av2[1]=t5;
tp(2,av2);}}

/* map-loop1469 in k7455 in ##sys#register-core-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_7524(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_7524,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7549,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* modules.scm:413: g1475 */
t4=((C_word*)t0)[4];
f_7500(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k7547 in map-loop1469 in k7455 in ##sys#register-core-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7549(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7549,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_7524(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* ##sys#register-primitive-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7564(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c<4) C_bad_min_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_7564,c,av);}
if(C_truep(C_rest_nullp(c,4))){
/* modules.scm:433: ##sys#register-core-module */
t4=*((C_word*)lf[77]+1);{
C_word av2[6];
av2[0]=t4;
av2[1]=t1;
av2[2]=t2;
av2[3]=t2;
av2[4]=t3;
av2[5]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t4+1)))(6,av2);}}
else{
/* modules.scm:433: ##sys#register-core-module */
t4=*((C_word*)lf[77]+1);{
C_word av2[6];
av2[0]=t4;
av2[1]=t1;
av2[2]=t2;
av2[3]=t2;
av2[4]=t3;
av2[5]=C_get_rest_arg(c,4,av,4,t0);
((C_proc)(void*)(*((C_word*)t4+1)))(6,av2);}}}

/* find-export in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_7579(C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_7579,4,t1,t2,t3,t4);}
a=C_alloc(5);
t5=C_i_check_structure_2(t3,lf[4],lf[12]);
t6=C_i_block_ref(t3,C_fix(3));
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7590,a[2]=t2,a[3]=t4,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t8=C_eqp(C_SCHEME_TRUE,t6);
if(C_truep(t8)){
t9=C_i_check_structure_2(t3,lf[4],lf[32]);
t10=t7;
f_7590(t10,C_i_block_ref(t3,C_fix(5)));}
else{
t9=t7;
f_7590(t9,t6);}}

/* k7588 in find-export in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_7590(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_7590,2,t0,t1);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7592,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word)li51),tmp=(C_word)a,a+=5,tmp);
t3=((C_word*)t0)[4];{
C_word av2[2];
av2[0]=t3;
av2[1]=(
  f_7592(t2,t1)
);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* loop in k7588 in find-export in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static C_word C_fcall f_7592(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_stack_overflow_check;
loop:{}
if(C_truep(C_i_nullp(t1))){
return(C_SCHEME_FALSE);}
else{
t2=C_i_car(t1);
t3=C_eqp(((C_word*)t0)[2],t2);
if(C_truep(t3)){
return(t3);}
else{
if(C_truep(C_i_pairp(C_u_i_car(t1)))){
t4=C_i_caar(t1);
t5=C_eqp(((C_word*)t0)[2],t4);
if(C_truep(t5)){
return(t5);}
else{
if(C_truep(((C_word*)t0)[3])){
t6=C_u_i_car(t1);
t7=C_i_memq(((C_word*)t0)[2],C_u_i_cdr(t6));
if(C_truep(t7)){
return(t7);}
else{
t9=C_u_i_cdr(t1);
t1=t9;
goto loop;}}
else{
t9=C_u_i_cdr(t1);
t1=t9;
goto loop;}}}
else{
t9=C_u_i_cdr(t1);
t1=t9;
goto loop;}}}}

/* ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7657(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word t24;
C_word t25;
C_word t26;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(28,c,3)))){
C_save_and_reclaim((void *)f_7657,c,av);}
a=C_alloc(28);
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8318,a[2]=((C_word)li53),tmp=(C_word)a,a+=3,tmp):C_get_rest_arg(c,3,av,3,t0));
t5=C_i_check_structure_2(t2,lf[4],lf[12]);
t6=C_i_block_ref(t2,C_fix(3));
t7=C_i_check_structure_2(t2,lf[4],lf[6]);
t8=C_i_block_ref(t2,C_fix(1));
t9=C_i_check_structure_2(t2,lf[4],lf[42]);
t10=C_i_block_ref(t2,C_fix(4));
t11=C_i_check_structure_2(t2,lf[4],lf[32]);
t12=C_i_block_ref(t2,C_fix(5));
t13=C_SCHEME_FALSE;
t14=(*a=C_VECTOR_TYPE|1,a[1]=t13,tmp=(C_word)a,a+=2,tmp);
t15=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t16=t15;
t17=(*a=C_VECTOR_TYPE|1,a[1]=t16,tmp=(C_word)a,a+=2,tmp);
t18=((C_word*)t17)[1];
t19=C_i_check_structure_2(t2,lf[4],lf[47]);
t20=C_i_block_ref(t2,C_fix(6));
t21=C_i_check_list_2(t20,lf[18]);
t22=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_7698,a[2]=t12,a[3]=t14,a[4]=t2,a[5]=t1,a[6]=t8,a[7]=t6,a[8]=t10,a[9]=t4,tmp=(C_word)a,a+=10,tmp);
t23=C_SCHEME_UNDEFINED;
t24=(*a=C_VECTOR_TYPE|1,a[1]=t23,tmp=(C_word)a,a+=2,tmp);
t25=C_set_block_item(t24,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8282,a[2]=t17,a[3]=t24,a[4]=t18,a[5]=((C_word)li73),tmp=(C_word)a,a+=6,tmp));
t26=((C_word*)t24)[1];
f_8282(t26,t22,t20);}

/* k7687 in map-loop1557 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7689(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7689,c,av);}
a=C_alloc(3);
t2=C_i_assq(((C_word*)t0)[2],t1);
t3=C_a_i_cons(&a,2,t2,C_SCHEME_END_OF_LIST);
t4=C_i_setslot(((C_word*)((C_word*)t0)[3])[1],C_fix(1),t3);
t5=C_mutate(((C_word *)((C_word*)t0)[3])+1,t3);
t6=((C_word*)((C_word*)t0)[4])[1];
f_8282(t6,((C_word*)t0)[5],C_slot(((C_word*)t0)[6],C_fix(1)));}

/* k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7698(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(17,c,2)))){
C_save_and_reclaim((void *)f_7698,c,av);}
a=C_alloc(17);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_7701,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],tmp=(C_word)a,a+=11,tmp);
t3=C_eqp(C_SCHEME_TRUE,((C_word*)t0)[7]);
if(C_truep(t3)){
t4=C_i_check_structure_2(((C_word*)t0)[4],lf[4],lf[14]);
t5=C_i_block_ref(((C_word*)t0)[4],C_fix(12));
/* modules.scm:462: merge-se */
f_6568(t2,C_a_i_list(&a,2,t5,t1));}
else{
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8245,a[2]=((C_word*)t0)[4],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* modules.scm:463: ##sys#macro-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[28]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[28]+1);
av2[1]=t4;
tp(2,av2);}}}

/* k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7701(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(20,c,3)))){
C_save_and_reclaim((void *)f_7701,c,av);}
a=C_alloc(20);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_7704,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t1,a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
t3=C_eqp(C_SCHEME_TRUE,((C_word*)t0)[8]);
t4=(C_truep(t3)?((C_word*)t0)[2]:((C_word*)t0)[8]);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_8071,a[2]=t1,a[3]=t6,a[4]=((C_word*)t0)[9],a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[10],a[8]=((C_word)li71),tmp=(C_word)a,a+=9,tmp));
t8=((C_word*)t6)[1];
f_8071(t8,t2,t4);}

/* k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7704(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,3)))){
C_save_and_reclaim((void *)f_7704,c,av);}
a=C_alloc(18);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7705,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word)li58),tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_7858,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=t1,a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[3],a[8]=((C_word*)t0)[8],a[9]=t2,tmp=(C_word)a,a+=10,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8065,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* modules.scm:535: module-undefined-list */
t5=*((C_word*)lf[7]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* g1625 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_7705(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_7705,3,t0,t1,t2);}
a=C_alloc(6);
t3=C_i_cdr(t2);
t4=C_u_i_car(t2);
if(C_truep(C_i_memq(t4,((C_word*)t0)[2]))){
t5=C_SCHEME_UNDEFINED;
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7719,a[2]=((C_word*)t0)[3],a[3]=t4,a[4]=t1,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* modules.scm:509: chicken.base#open-output-string */
t6=*((C_word*)lf[93]+1);{
C_word av2[2];
av2[0]=t6;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* k7717 in g1625 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7719(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_7719,c,av);}
a=C_alloc(6);
t2=C_set_block_item(((C_word*)t0)[2],0,C_SCHEME_TRUE);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7723,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t1,a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* modules.scm:511: display */
t4=*((C_word*)lf[83]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[92];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k7721 in k7717 in g1625 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7723(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_7723,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7726,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* modules.scm:512: display */
t3=*((C_word*)lf[83]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[2];
av2[3]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k7724 in k7721 in k7717 in g1625 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7726(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_7726,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7729,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* ##sys#write-char/port */
t3=*((C_word*)lf[87]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_make_character(39);
av2[3]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k7727 in k7724 in k7721 in k7717 in g1625 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7729(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_7729,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7732,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_i_pairp(((C_word*)t0)[5]))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7821,a[2]=((C_word*)t0)[4],a[3]=t2,a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp);
/* modules.scm:515: display */
t4=*((C_word*)lf[83]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[91];
av2[3]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_7732(2,av2);}}}

/* k7730 in k7727 in k7724 in k7721 in k7717 in g1625 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7732(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_7732,c,av);}
a=C_alloc(9);
t2=C_i_getprop(((C_word*)t0)[2],lf[81],C_SCHEME_FALSE);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7740,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
if(C_truep(t2)){
t4=C_i_length(t2);
t5=C_eqp(C_fix(1),t4);
if(C_truep(t5)){
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7756,a[2]=t3,a[3]=((C_word*)t0)[4],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* modules.scm:523: display */
t7=*((C_word*)lf[83]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t7;
av2[1]=t6;
av2[2]=lf[86];
av2[3]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}
else{
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7769,a[2]=((C_word*)t0)[4],a[3]=t3,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* modules.scm:527: display */
t7=*((C_word*)lf[83]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t7;
av2[1]=t6;
av2[2]=lf[89];
av2[3]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}}
else{
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f12022,a[2]=((C_word*)t0)[3],tmp=(C_word)a,a+=3,tmp);
/* modules.scm:534: chicken.base#get-output-string */
t5=*((C_word*)lf[82]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}}

/* k7738 in k7730 in k7727 in k7724 in k7721 in k7717 in g1625 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7740(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7740,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7747,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* modules.scm:534: chicken.base#get-output-string */
t3=*((C_word*)lf[82]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7745 in k7738 in k7730 in k7727 in k7724 in k7721 in k7717 in g1625 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7747(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7747,c,av);}
/* modules.scm:534: ##sys#warn */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[38]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[38]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
tp(3,av2);}}

/* k7754 in k7730 in k7727 in k7724 in k7721 in k7717 in g1625 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7756(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_7756,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7759,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7766,a[2]=t2,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* modules.scm:524: scheme#cadar */
t4=*((C_word*)lf[85]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k7757 in k7754 in k7730 in k7727 in k7724 in k7721 in k7717 in g1625 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7759(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7759,c,av);}
/* modules.scm:525: display */
t2=*((C_word*)lf[83]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[84];
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k7764 in k7754 in k7730 in k7727 in k7724 in k7721 in k7717 in g1625 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7766(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7766,c,av);}
/* modules.scm:524: display */
t2=*((C_word*)lf[83]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k7767 in k7730 in k7727 in k7724 in k7721 in k7717 in g1625 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7769(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,3)))){
C_save_and_reclaim((void *)f_7769,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7770,a[2]=((C_word*)t0)[2],a[3]=((C_word)li54),tmp=(C_word)a,a+=4,tmp);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7789,a[2]=t4,a[3]=t2,a[4]=((C_word)li55),tmp=(C_word)a,a+=5,tmp));
t6=((C_word*)t4)[1];
f_7789(t6,((C_word*)t0)[3],((C_word*)t0)[4]);}

/* g1672 in k7767 in k7730 in k7727 in k7724 in k7721 in k7717 in g1625 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_7770(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,3)))){
C_save_and_reclaim_args((void *)trf_7770,3,t0,t1,t2);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7774,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* modules.scm:530: display */
t4=*((C_word*)lf[83]+1);{
C_word av2[4];
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[88];
av2[3]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k7772 in g1672 in k7767 in k7730 in k7727 in k7724 in k7721 in k7717 in g1625 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7774(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_7774,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7777,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* modules.scm:531: display */
t3=*((C_word*)lf[83]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_i_cadr(((C_word*)t0)[4]);
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k7775 in k7772 in g1672 in k7767 in k7730 in k7727 in k7724 in k7721 in k7717 in g1625 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7777(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7777,c,av);}
/* ##sys#write-char/port */
t2=*((C_word*)lf[87]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_make_character(41);
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* for-each-loop1671 in k7767 in k7730 in k7727 in k7724 in k7721 in k7717 in g1625 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_7789(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_7789,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7799,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* modules.scm:528: g1672 */
t4=((C_word*)t0)[3];
f_7770(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k7797 in for-each-loop1671 in k7767 in k7730 in k7727 in k7724 in k7721 in k7717 in g1625 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7799(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7799,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_7789(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k7819 in k7727 in k7724 in k7721 in k7717 in g1625 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7821(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,3)))){
C_save_and_reclaim((void *)f_7821,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7822,a[2]=((C_word*)t0)[2],a[3]=((C_word)li56),tmp=(C_word)a,a+=4,tmp);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7834,a[2]=t4,a[3]=t2,a[4]=((C_word)li57),tmp=(C_word)a,a+=5,tmp));
t6=((C_word*)t4)[1];
f_7834(t6,((C_word*)t0)[3],((C_word*)t0)[4]);}

/* g1641 in k7819 in k7727 in k7724 in k7721 in k7717 in g1625 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_7822(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,3)))){
C_save_and_reclaim_args((void *)trf_7822,3,t0,t1,t2);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7826,a[2]=t1,a[3]=t2,a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
/* modules.scm:518: display */
t4=*((C_word*)lf[83]+1);{
C_word av2[4];
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[90];
av2[3]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k7824 in g1641 in k7819 in k7727 in k7724 in k7721 in k7717 in g1625 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7826(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7826,c,av);}
/* modules.scm:519: display */
t2=*((C_word*)lf[83]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* for-each-loop1640 in k7819 in k7727 in k7724 in k7721 in k7717 in g1625 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_7834(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_7834,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7844,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* modules.scm:516: g1641 */
t4=((C_word*)t0)[3];
f_7822(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k7842 in for-each-loop1640 in k7819 in k7727 in k7724 in k7721 in k7717 in g1625 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7844(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7844,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_7834(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7858(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,3)))){
C_save_and_reclaim((void *)f_7858,c,av);}
a=C_alloc(16);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_7861,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8040,a[2]=t4,a[3]=((C_word*)t0)[9],a[4]=((C_word)li67),tmp=(C_word)a,a+=5,tmp));
t6=((C_word*)t4)[1];
f_8040(t6,t2,t1);}

/* k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7861(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_7861,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7864,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
if(C_truep(((C_word*)((C_word*)t0)[7])[1])){
/* modules.scm:537: ##sys#error */
t3=*((C_word*)lf[74]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[102];
av2[3]=((C_word*)t0)[8];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_7864(2,av2);}}}

/* k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7864(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(27,c,4)))){
C_save_and_reclaim((void *)f_7864,c,av);}
a=C_alloc(27);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_7899,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t4,a[8]=t5,tmp=(C_word)a,a+=9,tmp);
t7=C_i_check_structure_2(((C_word*)t0)[3],lf[4],lf[12]);
t8=C_i_block_ref(((C_word*)t0)[3],C_fix(3));
t9=C_i_check_structure_2(((C_word*)t0)[3],lf[4],lf[6]);
t10=C_i_block_ref(((C_word*)t0)[3],C_fix(1));
t11=C_i_check_structure_2(((C_word*)t0)[3],lf[4],lf[42]);
t12=C_i_block_ref(((C_word*)t0)[3],C_fix(4));
t13=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6364,a[2]=t10,a[3]=((C_word)li62),tmp=(C_word)a,a+=4,tmp);
t14=C_eqp(C_SCHEME_TRUE,t8);
if(C_truep(t14)){
t15=t6;{
C_word *av2=av;
av2[0]=t15;
av2[1]=C_SCHEME_END_OF_LIST;
f_7899(2,av2);}}
else{
t15=C_SCHEME_UNDEFINED;
t16=(*a=C_VECTOR_TYPE|1,a[1]=t15,tmp=(C_word)a,a+=2,tmp);
t17=C_set_block_item(t16,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6387,a[2]=t16,a[3]=t13,a[4]=t12,a[5]=t10,a[6]=((C_word)li66),tmp=(C_word)a,a+=7,tmp));
t18=((C_word*)t16)[1];
f_6387(t18,t6,t8);}}

/* k7890 in map-loop1705 in k7897 in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7892(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_7892,c,av);}
a=C_alloc(3);
t2=C_i_assq(((C_word*)t0)[2],t1);
if(C_truep(t2)){
t3=((C_word*)t0)[3];
f_8013(t3,C_a_i_cons(&a,2,t2,C_SCHEME_END_OF_LIST));}
else{
/* modules.scm:542: ##sys#error */
t3=*((C_word*)lf[74]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[4];
av2[2]=lf[94];
av2[3]=C_u_i_car(((C_word*)t0)[5]);
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}}

/* k7897 in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7899(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,3)))){
C_save_and_reclaim((void *)f_7899,c,av);}
a=C_alloc(15);
t2=C_i_check_list_2(t1,lf[18]);
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7905,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8003,a[2]=((C_word*)t0)[7],a[3]=t5,a[4]=((C_word*)t0)[8],a[5]=((C_word)li61),tmp=(C_word)a,a+=6,tmp));
t7=((C_word*)t5)[1];
f_8003(t7,t3,t1);}

/* k7903 in k7897 in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7905(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,2)))){
C_save_and_reclaim((void *)f_7905,c,av);}
a=C_alloc(15);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_7908,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t1,tmp=(C_word)a,a+=8,tmp);
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7997,a[2]=t2,a[3]=t1,a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[2],tmp=(C_word)a,a+=7,tmp);
/* modules.scm:545: ##sys#macro-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[28]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[28]+1);
av2[1]=t3;
tp(2,av2);}}

/* k7906 in k7903 in k7897 in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7908(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,3)))){
C_save_and_reclaim((void *)f_7908,c,av);}
a=C_alloc(18);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7909,a[2]=t1,a[3]=((C_word)li59),tmp=(C_word)a,a+=4,tmp);
t3=C_i_check_list_2(((C_word*)t0)[2],lf[33]);
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7931,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],tmp=(C_word)a,a+=7,tmp);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7972,a[2]=t6,a[3]=t2,a[4]=((C_word)li60),tmp=(C_word)a,a+=5,tmp));
t8=((C_word*)t6)[1];
f_7972(t8,t4,((C_word*)t0)[2]);}

/* g1741 in k7906 in k7903 in k7897 in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_7909(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,2)))){
C_save_and_reclaim_args((void *)trf_7909,3,t0,t1,t2);}
a=C_alloc(10);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7913,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t4=C_i_cadr(t2);
/* modules.scm:550: merge-se */
f_6568(t3,C_a_i_list(&a,2,t4,((C_word*)t0)[2]));}

/* k7911 in g1741 in k7906 in k7903 in k7897 in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7913(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7913,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_i_set_car(C_u_i_cdr(((C_word*)t0)[3]),t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k7929 in k7906 in k7903 in k7897 in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7931(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,4)))){
C_save_and_reclaim((void *)f_7931,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7937,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
t3=C_i_check_structure_2(((C_word*)t0)[2],lf[4],C_SCHEME_FALSE);
/* modules.scm:93: ##sys#block-set! */
t4=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=((C_word*)t0)[2];
av2[3]=C_fix(11);
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k7935 in k7929 in k7906 in k7903 in k7897 in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7937(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,4)))){
C_save_and_reclaim((void *)f_7937,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7940,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
t3=C_i_check_structure_2(((C_word*)t0)[2],lf[4],C_SCHEME_FALSE);
/* modules.scm:93: ##sys#block-set! */
t4=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=((C_word*)t0)[2];
av2[3]=C_fix(12);
av2[4]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k7938 in k7935 in k7929 in k7906 in k7903 in k7897 in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7940(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,2)))){
C_save_and_reclaim((void *)f_7940,c,av);}
a=C_alloc(16);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7943,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7966,a[2]=((C_word*)t0)[2],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t4=C_i_check_structure_2(((C_word*)t0)[2],lf[4],lf[64]);
t5=C_i_block_ref(((C_word*)t0)[2],C_fix(13));
/* modules.scm:565: merge-se */
f_6568(t3,C_a_i_list(&a,2,t5,((C_word*)t0)[6]));}

/* k7941 in k7938 in k7935 in k7929 in k7906 in k7903 in k7897 in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7943(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_7943,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7954,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7962,a[2]=t2,a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp);
/* modules.scm:568: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[29]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[29]+1);
av2[1]=t3;
tp(2,av2);}}

/* k7952 in k7941 in k7938 in k7935 in k7929 in k7906 in k7903 in k7897 in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7954(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_7954,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7958,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* modules.scm:569: ##sys#macro-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[28]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[28]+1);
av2[1]=t2;
tp(2,av2);}}

/* k7956 in k7952 in k7941 in k7938 in k7935 in k7929 in k7906 in k7903 in k7897 in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 in ... */
static void C_ccall f_7958(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_7958,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_i_check_structure_2(((C_word*)t0)[3],lf[4],C_SCHEME_FALSE);
/* modules.scm:93: ##sys#block-set! */
t4=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[4];
av2[2]=((C_word*)t0)[3];
av2[3]=C_fix(14);
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k7960 in k7941 in k7938 in k7935 in k7929 in k7906 in k7903 in k7897 in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7962(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_7962,c,av);}
a=C_alloc(9);
/* modules.scm:568: merge-se */
f_6568(((C_word*)t0)[2],C_a_i_list(&a,3,t1,((C_word*)t0)[3],((C_word*)t0)[4]));}

/* k7964 in k7938 in k7935 in k7929 in k7906 in k7903 in k7897 in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7966(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_7966,c,av);}
t2=C_i_check_structure_2(((C_word*)t0)[2],lf[4],C_SCHEME_FALSE);
/* modules.scm:93: ##sys#block-set! */
t3=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[2];
av2[3]=C_fix(13);
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* for-each-loop1740 in k7906 in k7903 in k7897 in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_7972(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_7972,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7982,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* modules.scm:548: g1741 */
t4=((C_word*)t0)[3];
f_7909(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k7980 in for-each-loop1740 in k7906 in k7903 in k7897 in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7982(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7982,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_7972(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k7995 in k7903 in k7897 in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_7997(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_7997,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_8001,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* modules.scm:546: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[29]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[29]+1);
av2[1]=t2;
tp(2,av2);}}

/* k7999 in k7995 in k7903 in k7897 in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8001(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,2)))){
C_save_and_reclaim((void *)f_8001,c,av);}
a=C_alloc(18);
/* modules.scm:544: merge-se */
f_6568(((C_word*)t0)[2],C_a_i_list(&a,6,((C_word*)t0)[3],t1,((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6],((C_word*)t0)[7]));}

/* map-loop1705 in k7897 in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_8003(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,0,2)))){
C_save_and_reclaim_args((void *)trf_8003,3,t0,t1,t2);}
a=C_alloc(15);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8013,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8028,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
t5=C_slot(t2,C_fix(0));
t6=C_i_cdr(t5);
if(C_truep(C_i_symbolp(t6))){
t7=t3;
f_8013(t7,C_a_i_cons(&a,2,t5,C_SCHEME_END_OF_LIST));}
else{
t7=C_u_i_car(t5);
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7892,a[2]=t7,a[3]=t3,a[4]=t4,a[5]=t5,tmp=(C_word)a,a+=6,tmp);
/* modules.scm:541: ##sys#macro-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[28]+1));
C_word av2[2];
av2[0]=*((C_word*)lf[28]+1);
av2[1]=t8;
tp(2,av2);}}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k8011 in map-loop1705 in k7897 in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_8013(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,2)))){
C_save_and_reclaim_args((void *)trf_8013,2,t0,t1);}
t2=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t1);
t3=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t4=((C_word*)((C_word*)t0)[3])[1];
f_8003(t4,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k8026 in map-loop1705 in k7897 in k7862 in k7859 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8028(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_8028,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];
f_8013(t2,C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST));}

/* for-each-loop1624 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_8040(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_8040,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8050,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* modules.scm:504: g1625 */
t4=((C_word*)t0)[3];
f_7705(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k8048 in for-each-loop1624 in k7856 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8050(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8050,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_8040(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k8063 in k7702 in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8065(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8065,c,av);}
/* modules.scm:535: scheme#reverse */
t2=*((C_word*)lf[54]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* loop in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_8071(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(14,0,2)))){
C_save_and_reclaim_args((void *)trf_8071,3,t0,t1,t2);}
a=C_alloc(14);
if(C_truep(C_i_nullp(t2))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_i_car(t2);
t4=C_i_symbolp(t3);
t5=(C_truep(t4)?t3:C_i_car(t3));
if(C_truep(C_i_assq(t5,((C_word*)t0)[2]))){
/* modules.scm:474: loop */
t11=t1;
t12=C_u_i_cdr(t2);
t1=t11;
t2=t12;
goto loop;}
else{
t6=C_i_assq(t5,((C_word*)t0)[4]);
t7=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8115,a[2]=t5,a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=t2,tmp=(C_word)a,a+=6,tmp);
t8=(C_truep(t6)?C_i_symbolp(C_i_cdr(t6)):C_SCHEME_FALSE);
if(C_truep(t8)){
t9=t7;{
C_word av2[2];
av2[0]=t9;
av2[1]=C_i_cdr(t6);
f_8115(2,av2);}}
else{
t9=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_8212,a[2]=t5,a[3]=((C_word*)t0)[5],a[4]=t7,a[5]=t6,a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
/* modules.scm:482: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[29]+1));
C_word av2[2];
av2[0]=*((C_word*)lf[29]+1);
av2[1]=t9;
tp(2,av2);}}}}}

/* k8104 in k8113 in loop in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8106(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_8106,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k8113 in loop in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8115(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_8115,c,av);}
a=C_alloc(7);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8106,a[2]=((C_word*)t0)[3],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* modules.scm:503: loop */
t4=((C_word*)((C_word*)t0)[4])[1];
f_8071(t4,t3,C_u_i_cdr(((C_word*)t0)[5]));}

/* fail in k8210 in loop in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_8126(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_8126,3,t0,t1,t2);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8130,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* modules.scm:484: ##sys#warn */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[38]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[38]+1);
av2[1]=t3;
av2[2]=t2;
tp(3,av2);}}

/* k8128 in fail in k8210 in loop in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8130(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_8130,c,av);}
t2=C_set_block_item(((C_word*)t0)[2],0,C_SCHEME_TRUE);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* id-string in k8210 in loop in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_8133(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_8133,2,t0,t1);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8141,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* modules.scm:487: scheme#symbol->string */
t3=*((C_word*)lf[98]+1);{
C_word av2[3];
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k8139 in id-string in k8210 in loop in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8141(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_8141,c,av);}
/* modules.scm:487: scheme#string-append */
t2=*((C_word*)lf[95]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[103];
av2[3]=t1;
av2[4]=lf[104];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k8160 in k8210 in loop in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8162(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_8162,c,av);}
a=C_alloc(7);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8166,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word)li70),tmp=(C_word)a,a+=5,tmp);
/* modules.scm:488: g1616 */
t3=t2;
f_8166(t3,((C_word*)t0)[4],t1);}
else{
if(C_truep(C_i_not(((C_word*)t0)[5]))){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8194,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8198,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* modules.scm:500: id-string */
t4=((C_word*)((C_word*)t0)[3])[1];
f_8133(t4,t3);}
else{
/* modules.scm:502: bomb */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[110]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[110]+1);
av2[1]=((C_word*)t0)[4];
av2[2]=lf[111];
tp(3,av2);}}}}

/* g1616 in k8160 in k8210 in loop in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_8166(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_8166,3,t0,t1,t2);}
a=C_alloc(8);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8174,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8178,a[2]=t3,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* modules.scm:496: id-string */
t5=((C_word*)((C_word*)t0)[3])[1];
f_8133(t5,t4);}

/* k8172 in g1616 in k8160 in k8210 in loop in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8174(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8174,c,av);}
/* modules.scm:495: fail */
t2=((C_word*)((C_word*)t0)[2])[1];
f_8126(t2,((C_word*)t0)[3],t1);}

/* k8176 in g1616 in k8160 in k8210 in loop in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8178(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_8178,c,av);}
/* modules.scm:495: scheme#string-append */
t2=*((C_word*)lf[95]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[105];
av2[3]=t1;
av2[4]=lf[106];
av2[5]=((C_word*)t0)[3];
av2[6]=lf[107];
((C_proc)(void*)(*((C_word*)t2+1)))(7,av2);}}

/* k8192 in k8160 in k8210 in loop in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8194(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8194,c,av);}
/* modules.scm:499: fail */
t2=((C_word*)((C_word*)t0)[2])[1];
f_8126(t2,((C_word*)t0)[3],t1);}

/* k8196 in k8160 in k8210 in loop in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8198(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_8198,c,av);}
/* modules.scm:499: scheme#string-append */
t2=*((C_word*)lf[95]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[108];
av2[3]=t1;
av2[4]=lf[109];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k8210 in loop in k7699 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8212(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,3)))){
C_save_and_reclaim((void *)f_8212,c,av);}
a=C_alloc(18);
t2=C_i_assq(((C_word*)t0)[2],t1);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8126,a[2]=((C_word*)t0)[3],a[3]=((C_word)li68),tmp=(C_word)a,a+=4,tmp));
t8=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8133,a[2]=((C_word*)t0)[2],a[3]=((C_word)li69),tmp=(C_word)a,a+=4,tmp));
t9=(C_truep(t2)?C_i_symbolp(C_i_cdr(t2)):C_SCHEME_FALSE);
if(C_truep(t9)){
t10=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t10;
av2[1]=C_i_cdr(t2);
f_8115(2,av2);}}
else{
if(C_truep(((C_word*)t0)[5])){
/* modules.scm:491: module-rename */
f_9901(((C_word*)t0)[4],((C_word*)t0)[2],((C_word*)t0)[6]);}
else{
t10=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8162,a[2]=t4,a[3]=t6,a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* modules.scm:492: invalid-export */
t11=((C_word*)t0)[7];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t11;
av2[1]=t10;
av2[2]=((C_word*)t0)[2];
((C_proc)C_fast_retrieve_proc(t11))(3,av2);}}}}

/* k8243 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8245(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_8245,c,av);}
a=C_alloc(7);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8247,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=((C_word)li72),tmp=(C_word)a,a+=5,tmp));
t5=((C_word*)t3)[1];
f_8247(t5,((C_word*)t0)[3],t1);}

/* loop in k8243 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_8247(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,4)))){
C_save_and_reclaim_args((void *)trf_8247,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_nullp(t2))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8260,a[2]=t2,a[3]=t1,a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
/* modules.scm:465: find-export */
f_7579(t3,C_i_caar(t2),((C_word*)t0)[3],C_SCHEME_FALSE);}}

/* k8258 in loop in k8243 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8260(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_8260,c,av);}
a=C_alloc(4);
if(C_truep(t1)){
t2=C_u_i_car(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8269,a[2]=((C_word*)t0)[3],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* modules.scm:466: loop */
t4=((C_word*)((C_word*)t0)[4])[1];
f_8247(t4,t3,C_u_i_cdr(((C_word*)t0)[2]));}
else{
/* modules.scm:467: loop */
t2=((C_word*)((C_word*)t0)[4])[1];
f_8247(t2,((C_word*)t0)[3],C_u_i_cdr(((C_word*)t0)[2]));}}

/* k8267 in k8258 in loop in k8243 in k7696 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8269(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_8269,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* map-loop1557 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_8282(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,2)))){
C_save_and_reclaim_args((void *)trf_8282,3,t0,t1,t2);}
a=C_alloc(7);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_car(t3);
t5=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7689,a[2]=t4,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=t1,a[6]=t2,tmp=(C_word)a,a+=7,tmp);
/* modules.scm:458: ##sys#macro-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[28]+1));
C_word av2[2];
av2[0]=*((C_word*)lf[28]+1);
av2[1]=t5;
tp(2,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* f_8318 in ##sys#finalize-module in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8318(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_8318,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* ##sys#with-environment in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8325(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_8325,c,av);}
a=C_alloc(8);
t3=*((C_word*)lf[2]+1);
t4=*((C_word*)lf[29]+1);
t5=*((C_word*)lf[112]+1);
t6=*((C_word*)lf[28]+1);
t7=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_8329,a[2]=t6,a[3]=t5,a[4]=t4,a[5]=t3,a[6]=t2,a[7]=t1,tmp=(C_word)a,a+=8,tmp);
/* modules.scm:580: ##sys#current-meta-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[112]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[112]+1);
av2[1]=t7;
tp(2,av2);}}

/* k8327 in ##sys#with-environment in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8329(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_8329,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_8332,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
/* modules.scm:582: ##sys#meta-macro-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[113]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[113]+1);
av2[1]=t2;
tp(2,av2);}}

/* k8330 in k8327 in ##sys#with-environment in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8332(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(37,c,4)))){
C_save_and_reclaim((void *)f_8332,c,av);}
a=C_alloc(37);
t2=C_SCHEME_FALSE;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_SCHEME_END_OF_LIST;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t0)[2];
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=t1;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_SCHEME_TRUE;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_8337,a[2]=t3,a[3]=t5,a[4]=t7,a[5]=t9,a[6]=t11,a[7]=((C_word*)t0)[3],a[8]=((C_word*)t0)[4],a[9]=((C_word*)t0)[5],a[10]=((C_word*)t0)[6],a[11]=((C_word)li75),tmp=(C_word)a,a+=12,tmp);
t13=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8393,a[2]=((C_word*)t0)[7],a[3]=((C_word)li76),tmp=(C_word)a,a+=4,tmp);
t14=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_8399,a[2]=t3,a[3]=t5,a[4]=t7,a[5]=t9,a[6]=((C_word*)t0)[3],a[7]=((C_word*)t0)[4],a[8]=((C_word*)t0)[5],a[9]=((C_word*)t0)[6],a[10]=((C_word)li77),tmp=(C_word)a,a+=11,tmp);
/* modules.scm:577: ##sys#dynamic-wind */
t15=*((C_word*)lf[17]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t15;
av2[1]=((C_word*)t0)[8];
av2[2]=t12;
av2[3]=t13;
av2[4]=t14;
((C_proc)(void*)(*((C_word*)t15+1)))(5,av2);}}

/* a8336 in k8330 in k8327 in ##sys#with-environment in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8337(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(12,c,4)))){
C_save_and_reclaim((void *)f_8337,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_8341,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t1,a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],tmp=(C_word)a,a+=12,tmp);
if(C_truep(((C_word*)((C_word*)t0)[6])[1])){
/* modules.scm:577: ##sys#current-module1773 */
t3=((C_word*)t0)[10];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[2])[1];
av2[3]=C_SCHEME_TRUE;
av2[4]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)((C_word*)t0)[2])[1];
f_8341(2,av2);}}}

/* k8339 in a8336 in k8330 in k8327 in ##sys#with-environment in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8341(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,4)))){
C_save_and_reclaim((void *)f_8341,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_8344,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=t1,tmp=(C_word)a,a+=13,tmp);
if(C_truep(((C_word*)((C_word*)t0)[6])[1])){
/* modules.scm:577: ##sys#current-environment1774 */
t3=((C_word*)t0)[10];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[3])[1];
av2[3]=C_SCHEME_TRUE;
av2[4]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)((C_word*)t0)[3])[1];
f_8344(2,av2);}}}

/* k8342 in k8339 in a8336 in k8330 in k8327 in ##sys#with-environment in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8344(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,4)))){
C_save_and_reclaim((void *)f_8344,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_8347,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=t1,a[12]=((C_word*)t0)[11],a[13]=((C_word*)t0)[12],tmp=(C_word)a,a+=14,tmp);
if(C_truep(((C_word*)((C_word*)t0)[6])[1])){
/* modules.scm:577: ##sys#current-meta-environment1775 */
t3=((C_word*)t0)[9];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[4])[1];
av2[3]=C_SCHEME_TRUE;
av2[4]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)((C_word*)t0)[4])[1];
f_8347(2,av2);}}}

/* k8345 in k8342 in k8339 in a8336 in k8330 in k8327 in ##sys#with-environment in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8347(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,4)))){
C_save_and_reclaim((void *)f_8347,c,av);}
a=C_alloc(15);
t2=(*a=C_CLOSURE_TYPE|14,a[1]=(C_word)f_8350,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=t1,a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],a[13]=((C_word*)t0)[12],a[14]=((C_word*)t0)[13],tmp=(C_word)a,a+=15,tmp);
if(C_truep(((C_word*)((C_word*)t0)[6])[1])){
/* modules.scm:577: ##sys#macro-environment1776 */
t3=((C_word*)t0)[8];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[5])[1];
av2[3]=C_SCHEME_TRUE;
av2[4]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)((C_word*)t0)[5])[1];
f_8350(2,av2);}}}

/* k8348 in k8345 in k8342 in k8339 in a8336 in k8330 in k8327 in ##sys#with-environment in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8350(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,2)))){
C_save_and_reclaim((void *)f_8350,c,av);}
a=C_alloc(16);
t2=(*a=C_CLOSURE_TYPE|15,a[1]=(C_word)f_8354,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=t1,a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],a[13]=((C_word*)t0)[12],a[14]=((C_word*)t0)[13],a[15]=((C_word*)t0)[14],tmp=(C_word)a,a+=16,tmp);
/* modules.scm:577: ##sys#current-module1773 */
t3=((C_word*)t0)[13];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)C_fast_retrieve_proc(t3))(2,av2);}}

/* k8352 in k8348 in k8345 in k8342 in k8339 in a8336 in k8330 in k8327 in ##sys#with-environment in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8354(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,2)))){
C_save_and_reclaim((void *)f_8354,c,av);}
a=C_alloc(15);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|14,a[1]=(C_word)f_8358,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],a[12]=((C_word*)t0)[13],a[13]=((C_word*)t0)[14],a[14]=((C_word*)t0)[15],tmp=(C_word)a,a+=15,tmp);
/* modules.scm:577: ##sys#current-environment1774 */
t4=((C_word*)t0)[12];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)C_fast_retrieve_proc(t4))(2,av2);}}

/* k8356 in k8352 in k8348 in k8345 in k8342 in k8339 in a8336 in k8330 in k8327 in ##sys#with-environment in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8358(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,2)))){
C_save_and_reclaim((void *)f_8358,c,av);}
a=C_alloc(14);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_8362,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],a[12]=((C_word*)t0)[13],a[13]=((C_word*)t0)[14],tmp=(C_word)a,a+=14,tmp);
/* modules.scm:577: ##sys#current-meta-environment1775 */
t4=((C_word*)t0)[9];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)C_fast_retrieve_proc(t4))(2,av2);}}

/* k8360 in k8356 in k8352 in k8348 in k8345 in k8342 in k8339 in a8336 in k8330 in k8327 in ##sys#with-environment in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8362(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_8362,c,av);}
a=C_alloc(13);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_8366,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],a[12]=((C_word*)t0)[13],tmp=(C_word)a,a+=13,tmp);
/* modules.scm:577: ##sys#macro-environment1776 */
t4=((C_word*)t0)[6];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)C_fast_retrieve_proc(t4))(2,av2);}}

/* k8364 in k8360 in k8356 in k8352 in k8348 in k8345 in k8342 in k8339 in a8336 in k8330 in k8327 in ##sys#with-environment in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8366(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,4)))){
C_save_and_reclaim((void *)f_8366,c,av);}
a=C_alloc(10);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_8369,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],tmp=(C_word)a,a+=10,tmp);
/* modules.scm:577: ##sys#current-module1773 */
t4=((C_word*)t0)[11];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[12];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t4))(5,av2);}}

/* k8367 in k8364 in k8360 in k8356 in k8352 in k8348 in k8345 in k8342 in k8339 in a8336 in k8330 in k8327 in ##sys#with-environment in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8369(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_8369,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_8372,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
/* modules.scm:577: ##sys#current-environment1774 */
t3=((C_word*)t0)[8];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[9];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}

/* k8370 in k8367 in k8364 in k8360 in k8356 in k8352 in k8348 in k8345 in k8342 in k8339 in a8336 in k8330 in k8327 in ##sys#with-environment in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8372(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_8372,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8375,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* modules.scm:577: ##sys#current-meta-environment1775 */
t3=((C_word*)t0)[6];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[7];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}

/* k8373 in k8370 in k8367 in k8364 in k8360 in k8356 in k8352 in k8348 in k8345 in k8342 in k8339 in a8336 in k8330 in k8327 in ##sys#with-environment in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8375(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_8375,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8378,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* modules.scm:577: ##sys#macro-environment1776 */
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}

/* k8376 in k8373 in k8370 in k8367 in k8364 in k8360 in k8356 in k8352 in k8348 in k8345 in k8342 in k8339 in a8336 in k8330 in k8327 in ##sys#with-environment in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 in ... */
static void C_ccall f_8378(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_8378,c,av);}
t2=C_set_block_item(((C_word*)t0)[2],0,C_SCHEME_FALSE);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* a8392 in k8330 in k8327 in ##sys#with-environment in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8393(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_8393,c,av);}
/* modules.scm:583: thunk */
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)C_fast_retrieve_proc(t2))(2,av2);}}

/* a8398 in k8330 in k8327 in ##sys#with-environment in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8399(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_8399,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_8403,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],tmp=(C_word)a,a+=11,tmp);
/* modules.scm:577: ##sys#current-module1773 */
t3=((C_word*)t0)[9];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)C_fast_retrieve_proc(t3))(2,av2);}}

/* k8401 in a8398 in k8330 in k8327 in ##sys#with-environment in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8403(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_8403,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_8406,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],tmp=(C_word)a,a+=12,tmp);
/* modules.scm:577: ##sys#current-environment1774 */
t3=((C_word*)t0)[9];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)C_fast_retrieve_proc(t3))(2,av2);}}

/* k8404 in k8401 in a8398 in k8330 in k8327 in ##sys#with-environment in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8406(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_8406,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_8409,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],tmp=(C_word)a,a+=13,tmp);
/* modules.scm:577: ##sys#current-meta-environment1775 */
t3=((C_word*)t0)[9];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)C_fast_retrieve_proc(t3))(2,av2);}}

/* k8407 in k8404 in k8401 in a8398 in k8330 in k8327 in ##sys#with-environment in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8409(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,2)))){
C_save_and_reclaim((void *)f_8409,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_8412,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t1,a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],a[13]=((C_word*)t0)[12],tmp=(C_word)a,a+=14,tmp);
/* modules.scm:577: ##sys#macro-environment1776 */
t3=((C_word*)t0)[9];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)C_fast_retrieve_proc(t3))(2,av2);}}

/* k8410 in k8407 in k8404 in k8401 in a8398 in k8330 in k8327 in ##sys#with-environment in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8412(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,4)))){
C_save_and_reclaim((void *)f_8412,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_8415,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=t1,a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],a[13]=((C_word*)t0)[12],tmp=(C_word)a,a+=14,tmp);
/* modules.scm:577: ##sys#current-module1773 */
t3=((C_word*)t0)[13];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[2])[1];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}

/* k8413 in k8410 in k8407 in k8404 in k8401 in a8398 in k8330 in k8327 in ##sys#with-environment in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8415(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,4)))){
C_save_and_reclaim((void *)f_8415,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_8418,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],tmp=(C_word)a,a+=13,tmp);
/* modules.scm:577: ##sys#current-environment1774 */
t3=((C_word*)t0)[13];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[4])[1];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}

/* k8416 in k8413 in k8410 in k8407 in k8404 in k8401 in a8398 in k8330 in k8327 in ##sys#with-environment in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8418(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,4)))){
C_save_and_reclaim((void *)f_8418,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_8421,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],tmp=(C_word)a,a+=12,tmp);
/* modules.scm:577: ##sys#current-meta-environment1775 */
t3=((C_word*)t0)[12];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[6])[1];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}

/* k8419 in k8416 in k8413 in k8410 in k8407 in k8404 in k8401 in a8398 in k8330 in k8327 in ##sys#with-environment in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8421(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,4)))){
C_save_and_reclaim((void *)f_8421,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_8424,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],tmp=(C_word)a,a+=11,tmp);
/* modules.scm:577: ##sys#macro-environment1776 */
t3=((C_word*)t0)[11];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[8])[1];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}

/* k8422 in k8419 in k8416 in k8413 in k8410 in k8407 in k8404 in k8401 in a8398 in k8330 in k8327 in ##sys#with-environment in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8424(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_8424,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,((C_word*)t0)[3]);
t3=C_mutate(((C_word *)((C_word*)t0)[4])+1,((C_word*)t0)[5]);
t4=C_mutate(((C_word *)((C_word*)t0)[6])+1,((C_word*)t0)[7]);
t5=C_mutate(((C_word *)((C_word*)t0)[8])+1,((C_word*)t0)[9]);
t6=((C_word*)t0)[10];{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}

/* ##sys#import-library-hook in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8430(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_8430,c,av);}
a=C_alloc(10);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8434,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8469,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8473,a[2]=t4,tmp=(C_word)a,a+=3,tmp);
/* modules.scm:587: scheme#symbol->string */
t6=*((C_word*)lf[98]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k8432 in ##sys#import-library-hook in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8434(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_8434,c,av);}
a=C_alloc(5);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8442,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word)li82),tmp=(C_word)a,a+=5,tmp);
/* modules.scm:589: ##sys#with-environment */
t3=*((C_word*)lf[60]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* a8441 in k8432 in ##sys#import-library-hook in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8442(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(19,c,4)))){
C_save_and_reclaim((void *)f_8442,c,av);}
a=C_alloc(19);
t2=C_SCHEME_FALSE;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_SCHEME_FALSE;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8448,a[2]=t5,a[3]=t3,a[4]=((C_word)li79),tmp=(C_word)a,a+=5,tmp);
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8453,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word)li80),tmp=(C_word)a,a+=5,tmp);
t8=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8462,a[2]=t3,a[3]=t5,a[4]=((C_word)li81),tmp=(C_word)a,a+=5,tmp);
/* modules.scm:591: ##sys#dynamic-wind */
t9=*((C_word*)lf[17]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t9;
av2[1]=t1;
av2[2]=t6;
av2[3]=t7;
av2[4]=t8;
((C_proc)(void*)(*((C_word*)t9+1)))(5,av2);}}

/* a8447 in a8441 in k8432 in ##sys#import-library-hook in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8448(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_8448,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,*((C_word*)lf[115]+1));
t3=C_mutate((C_word*)lf[115]+1 /* (set! ##sys#notices-enabled ...) */,((C_word*)((C_word*)t0)[3])[1]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* a8452 in a8441 in k8432 in ##sys#import-library-hook in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8453(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_8453,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8457,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* modules.scm:592: scheme#load */
t3=*((C_word*)lf[116]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k8455 in a8452 in a8441 in k8432 in ##sys#import-library-hook in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8457(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_8457,c,av);}
/* modules.scm:593: ##sys#find-module */
t2=*((C_word*)lf[24]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=lf[75];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* a8461 in a8441 in k8432 in ##sys#import-library-hook in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8462(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_8462,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,*((C_word*)lf[115]+1));
t3=C_mutate((C_word*)lf[115]+1 /* (set! ##sys#notices-enabled ...) */,((C_word*)((C_word*)t0)[3])[1]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k8467 in ##sys#import-library-hook in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8469(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_8469,c,av);}
/* modules.scm:586: chicken.load#find-dynamic-extension */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[117]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[117]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_SCHEME_TRUE;
tp(4,av2);}}

/* k8471 in ##sys#import-library-hook in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8473(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_8473,c,av);}
/* ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[118]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[118]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=lf[119];
tp(4,av2);}}

/* find-module/import-library in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_8476(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,3)))){
C_save_and_reclaim_args((void *)trf_8476,3,t1,t2,t3);}
a=C_alloc(4);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8480,a[2]=t1,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* modules.scm:596: ##sys#resolve-module-name */
t5=*((C_word*)lf[20]+1);{
C_word av2[4];
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k8478 in find-module/import-library in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8480(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_8480,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8483,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* modules.scm:597: ##sys#find-module */
t3=*((C_word*)lf[24]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=t1;
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k8481 in k8478 in find-module/import-library in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8483(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8483,c,av);}
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
/* modules.scm:598: ##sys#import-library-hook */
t2=*((C_word*)lf[114]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}}

/* ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8491(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word *a;
if(c!=6) C_bad_argc_2(c,6,t0);
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_8491,c,av);}
a=C_alloc(7);
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_8495,a[2]=t5,a[3]=t4,a[4]=t2,a[5]=t1,a[6]=t3,tmp=(C_word)a,a+=7,tmp);
/* modules.scm:601: r */
t7=t3;{
C_word *av2=av;
av2[0]=t7;
av2[1]=t6;
av2[2]=lf[144];
((C_proc)C_fast_retrieve_proc(t7))(3,av2);}}

/* k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8495(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_8495,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_8498,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* modules.scm:602: r */
t3=((C_word*)t0)[6];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[143];
((C_proc)C_fast_retrieve_proc(t3))(3,av2);}}

/* k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8498(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_8498,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_8501,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
/* modules.scm:603: r */
t3=((C_word*)t0)[7];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[142];
((C_proc)C_fast_retrieve_proc(t3))(3,av2);}}

/* k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8501(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_8501,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_8504,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
/* modules.scm:604: r */
t3=((C_word*)t0)[8];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[141];
((C_proc)C_fast_retrieve_proc(t3))(3,av2);}}

/* k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8504(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(23,c,5)))){
C_save_and_reclaim((void *)f_8504,c,av);}
a=C_alloc(23);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8506,a[2]=((C_word)li85),tmp=(C_word)a,a+=3,tmp));
t7=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8520,a[2]=((C_word*)t0)[2],a[3]=((C_word)li86),tmp=(C_word)a,a+=4,tmp));
t8=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_8567,a[2]=((C_word*)t0)[2],a[3]=t3,a[4]=t5,a[5]=((C_word*)t0)[3],a[6]=t1,a[7]=((C_word*)t0)[4],a[8]=((C_word*)t0)[5],a[9]=((C_word*)t0)[6],a[10]=((C_word*)t0)[7],a[11]=((C_word)li118),tmp=(C_word)a,a+=12,tmp);
/* modules.scm:613: scheme#call-with-current-continuation */
t9=*((C_word*)lf[140]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t9;
av2[1]=((C_word*)t0)[8];
av2[2]=t8;
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}

/* warn in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_8506(C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_8506,4,t1,t2,t3,t4);}
a=C_alloc(8);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8514,a[2]=t1,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8518,a[2]=t5,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* modules.scm:606: scheme#symbol->string */
t7=*((C_word*)lf[98]+1);{
C_word av2[3];
av2[0]=t7;
av2[1]=t6;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}

/* k8512 in warn in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8514(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_8514,c,av);}
/* modules.scm:606: ##sys#warn */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[38]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[38]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
tp(4,av2);}}

/* k8516 in warn in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8518(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_8518,c,av);}
/* modules.scm:606: scheme#string-append */
t2=*((C_word*)lf[95]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=lf[122];
av2[4]=t1;
av2[5]=lf[123];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* tostr in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_8520(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_8520,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_stringp(t2))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8533,a[2]=t1,a[3]=t2,a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
/* modules.scm:609: chicken.keyword#keyword? */
t4=*((C_word*)lf[129]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}}

/* k8531 in tostr in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8533(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_8533,c,av);}
a=C_alloc(3);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8540,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* modules.scm:609: ##sys#symbol->string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[125]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[125]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
tp(3,av2);}}
else{
if(C_truep(C_i_symbolp(((C_word*)t0)[3]))){
/* modules.scm:610: ##sys#symbol->string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[125]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[125]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
tp(3,av2);}}
else{
if(C_truep(C_i_numberp(((C_word*)t0)[3]))){
/* ##sys#number->string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[126]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[126]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=C_fix(10);
tp(4,av2);}}
else{
/* modules.scm:612: ##sys#syntax-error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[127]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[127]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[4];
av2[3]=lf[128];
tp(4,av2);}}}}}

/* k8538 in k8531 in tostr in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8540(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_8540,c,av);}
/* modules.scm:609: ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[118]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[118]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=lf[124];
tp(4,av2);}}

/* a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8567(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(20,c,3)))){
C_save_and_reclaim((void *)f_8567,c,av);}
a=C_alloc(20);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8570,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word)li87),tmp=(C_word)a,a+=5,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_8618,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=t5,a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[5],a[8]=((C_word*)t0)[6],a[9]=((C_word*)t0)[7],a[10]=((C_word*)t0)[8],a[11]=((C_word*)t0)[9],a[12]=((C_word)li117),tmp=(C_word)a,a+=13,tmp));
t7=((C_word*)t5)[1];
f_8618(t7,t1,((C_word*)t0)[10]);}

/* module-imports in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_8570(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_8570,3,t0,t1,t2);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8574,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* modules.scm:616: chicken.internal#library-id */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[23]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[23]+1);
av2[1]=t3;
av2[2]=t2;
tp(3,av2);}}

/* k8572 in module-imports in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8574(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_8574,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8577,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* modules.scm:617: find-module/import-library */
f_8476(t2,t1,((C_word*)t0)[4]);}

/* k8575 in k8572 in module-imports in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8577(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_8577,c,av);}
if(C_truep(C_i_not(t1))){
/* modules.scm:619: k */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=((C_word*)t0)[4];
av2[4]=C_SCHEME_FALSE;
av2[5]=C_SCHEME_FALSE;
av2[6]=C_SCHEME_FALSE;
av2[7]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t2))(8,av2);}}
else{
t2=C_i_check_structure_2(t1,lf[4],lf[6]);
t3=C_i_block_ref(t1,C_fix(1));
t4=C_i_check_structure_2(t1,lf[4],lf[63]);
t5=C_i_block_ref(t1,C_fix(2));
t6=C_i_check_structure_2(t1,lf[4],lf[6]);
t7=C_i_block_ref(t1,C_fix(1));
t8=C_i_check_structure_2(t1,lf[4],lf[13]);
t9=C_i_block_ref(t1,C_fix(11));
t10=C_i_check_structure_2(t1,lf[4],lf[14]);
t11=C_i_block_ref(t1,C_fix(12));
t12=C_i_check_structure_2(t1,lf[4],lf[64]);
/* modules.scm:620: scheme#values */{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=0;
av2[1]=((C_word*)t0)[3];
av2[2]=t3;
av2[3]=t5;
av2[4]=t7;
av2[5]=t9;
av2[6]=t11;
av2[7]=C_i_block_ref(t1,C_fix(13));
C_values(8,av2);}}}

/* loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_8618(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,0,4)))){
C_save_and_reclaim_args((void *)trf_8618,3,t0,t1,t2);}
a=C_alloc(14);
if(C_truep(C_i_symbolp(t2))){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8632,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* modules.scm:628: chicken.syntax#strip-syntax */
t4=*((C_word*)lf[67]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=C_i_pairp(t2);
if(C_truep(C_i_not(t3))){
/* modules.scm:630: ##sys#syntax-error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[127]+1));
C_word av2[5];
av2[0]=*((C_word*)lf[127]+1);
av2[1]=t1;
av2[2]=((C_word*)t0)[3];
av2[3]=lf[130];
av2[4]=t2;
tp(5,av2);}}
else{
t4=C_i_car(t2);
t5=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_8650,a[2]=((C_word*)t0)[4],a[3]=t2,a[4]=((C_word*)t0)[5],a[5]=t4,a[6]=t1,a[7]=((C_word*)t0)[3],a[8]=((C_word*)t0)[6],a[9]=((C_word*)t0)[2],a[10]=((C_word*)t0)[7],a[11]=((C_word*)t0)[8],a[12]=((C_word*)t0)[9],a[13]=((C_word*)t0)[10],tmp=(C_word)a,a+=14,tmp);
/* modules.scm:633: c */
t6=((C_word*)t0)[7];{
C_word av2[4];
av2[0]=t6;
av2[1]=t5;
av2[2]=((C_word*)t0)[11];
av2[3]=t4;
((C_proc)C_fast_retrieve_proc(t6))(4,av2);}}}}

/* k8630 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8632(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8632,c,av);}
/* modules.scm:628: module-imports */
t2=((C_word*)t0)[2];
f_8570(t2,((C_word*)t0)[3],t1);}

/* k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8650(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,4)))){
C_save_and_reclaim((void *)f_8650,c,av);}
a=C_alloc(13);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_8653,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* modules.scm:634: ##sys#check-syntax */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[132]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[132]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[7];
av2[3]=((C_word*)t0)[3];
av2[4]=lf[133];
tp(5,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_8800,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],tmp=(C_word)a,a+=13,tmp);
/* modules.scm:652: c */
t3=((C_word*)t0)[10];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[13];
av2[3]=((C_word*)t0)[5];
((C_proc)C_fast_retrieve_proc(t3))(4,av2);}}}

/* k8651 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8653(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,8)))){
C_save_and_reclaim((void *)f_8653,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8658,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word)li88),tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8668,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[3],a[5]=((C_word)li94),tmp=(C_word)a,a+=6,tmp);
/* modules.scm:635: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[6];
av2[2]=t2;
av2[3]=t3;
C_call_with_values(4,av2);}}

/* a8657 in k8651 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8658(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8658,c,av);}
/* modules.scm:635: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_8618(t2,t1,C_i_cadr(((C_word*)t0)[3]));}

/* a8667 in k8651 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8668(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6=av[6];
C_word t7=av[7];
C_word t8;
C_word t9;
C_word *a;
if(c!=8) C_bad_argc_2(c,8,t0);
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_8668,c,av);}
a=C_alloc(11);
t8=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_8672,a[2]=((C_word*)t0)[2],a[3]=t4,a[4]=((C_word*)t0)[3],a[5]=t2,a[6]=t3,a[7]=t7,a[8]=t5,a[9]=t6,a[10]=t1,tmp=(C_word)a,a+=11,tmp);
/* modules.scm:636: chicken.syntax#strip-syntax */
t9=*((C_word*)lf[67]+1);{
C_word *av2=av;
av2[0]=t9;
av2[1]=t8;
av2[2]=C_i_cddr(((C_word*)t0)[4]);
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}

/* k8670 in a8667 in k8651 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8672(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,6)))){
C_save_and_reclaim((void *)f_8672,c,av);}
a=C_alloc(15);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_8677,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=t3,a[11]=((C_word*)t0)[9],a[12]=((C_word)li93),tmp=(C_word)a,a+=13,tmp));
t5=((C_word*)t3)[1];
f_8677(t5,((C_word*)t0)[10],t1,C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST,C_SCHEME_END_OF_LIST);}

/* loop in k8670 in a8667 in k8651 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_8677(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5){
C_word tmp;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(23,0,5)))){
C_save_and_reclaim_args((void *)trf_8677,6,t0,t1,t2,t3,t4,t5);}
a=C_alloc(23);
if(C_truep(C_i_nullp(t2))){
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8685,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word)li89),tmp=(C_word)a,a+=5,tmp);
t7=C_i_check_list_2(t5,lf[33]);
t8=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_8695,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=t1,a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=t3,a[9]=t4,a[10]=((C_word*)t0)[8],tmp=(C_word)a,a+=11,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8708,a[2]=t10,a[3]=t6,a[4]=((C_word)li90),tmp=(C_word)a,a+=5,tmp));
t12=((C_word*)t10)[1];
f_8708(t12,t8,t5);}
else{
t6=C_i_car(t2);
t7=C_i_assq(t6,((C_word*)t0)[9]);
if(C_truep(t7)){
t8=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_8736,a[2]=t2,a[3]=t3,a[4]=((C_word*)t0)[10],a[5]=t4,a[6]=t5,a[7]=((C_word)li91),tmp=(C_word)a,a+=8,tmp);
/* modules.scm:638: g1947 */
t9=t8;
f_8736(t9,t1,t7);}
else{
t8=C_i_assq(C_u_i_car(t2),((C_word*)t0)[11]);
if(C_truep(t8)){
t9=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_8758,a[2]=t2,a[3]=t4,a[4]=((C_word*)t0)[10],a[5]=t3,a[6]=t5,a[7]=((C_word)li92),tmp=(C_word)a,a+=8,tmp);
/* modules.scm:638: g1951 */
t10=t9;
f_8758(t10,t1,t8);}
else{
t9=C_u_i_cdr(t2);
t10=C_u_i_car(t2);
t11=C_a_i_cons(&a,2,t10,t5);
/* modules.scm:651: loop */
t13=t1;
t14=t9;
t15=t3;
t16=t4;
t17=t11;
t1=t13;
t2=t14;
t3=t15;
t4=t16;
t5=t17;
goto loop;}}}}

/* g1927 in loop in k8670 in a8667 in k8651 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_8685(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,4)))){
C_save_and_reclaim_args((void *)trf_8685,3,t0,t1,t2);}
/* modules.scm:641: warn */
f_8506(t1,lf[131],((C_word*)t0)[3],t2);}

/* k8693 in loop in k8670 in a8667 in k8651 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8695(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,7)))){
C_save_and_reclaim((void *)f_8695,c,av);}
a=C_alloc(6);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],((C_word*)t0)[3]);
t3=C_a_i_cons(&a,2,((C_word*)t0)[4],t2);
/* modules.scm:643: scheme#values */{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=0;
av2[1]=((C_word*)t0)[5];
av2[2]=((C_word*)t0)[6];
av2[3]=((C_word*)t0)[7];
av2[4]=t3;
av2[5]=((C_word*)t0)[8];
av2[6]=((C_word*)t0)[9];
av2[7]=((C_word*)t0)[10];
C_values(8,av2);}}

/* for-each-loop1926 in loop in k8670 in a8667 in k8651 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_8708(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_8708,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8718,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* modules.scm:639: g1927 */
t4=((C_word*)t0)[3];
f_8685(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k8716 in for-each-loop1926 in loop in k8670 in a8667 in k8651 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8718(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8718,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_8708(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* g1947 in loop in k8670 in a8667 in k8651 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_8736(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,0,5)))){
C_save_and_reclaim_args((void *)trf_8736,3,t0,t1,t2);}
a=C_alloc(3);
t3=C_i_cdr(((C_word*)t0)[2]);
t4=C_a_i_cons(&a,2,t2,((C_word*)t0)[3]);
/* modules.scm:646: loop */
t5=((C_word*)((C_word*)t0)[4])[1];
f_8677(t5,t1,t3,t4,((C_word*)t0)[5],((C_word*)t0)[6]);}

/* g1951 in loop in k8670 in a8667 in k8651 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_8758(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,0,5)))){
C_save_and_reclaim_args((void *)trf_8758,3,t0,t1,t2);}
a=C_alloc(3);
t3=C_i_cdr(((C_word*)t0)[2]);
t4=C_a_i_cons(&a,2,t2,((C_word*)t0)[3]);
/* modules.scm:649: loop */
t5=((C_word*)((C_word*)t0)[4])[1];
f_8677(t5,t1,t3,((C_word*)t0)[5],t4,((C_word*)t0)[6]);}

/* k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8800(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,4)))){
C_save_and_reclaim((void *)f_8800,c,av);}
a=C_alloc(12);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_8803,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* modules.scm:653: ##sys#check-syntax */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[132]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[132]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[7];
av2[3]=((C_word*)t0)[3];
av2[4]=lf[135];
tp(5,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_8983,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],tmp=(C_word)a,a+=12,tmp);
/* modules.scm:675: c */
t3=((C_word*)t0)[10];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[12];
av2[3]=((C_word*)t0)[5];
((C_proc)C_fast_retrieve_proc(t3))(4,av2);}}}

/* k8801 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8803(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,8)))){
C_save_and_reclaim((void *)f_8803,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8808,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word)li95),tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8818,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[3],a[5]=((C_word)li102),tmp=(C_word)a,a+=6,tmp);
/* modules.scm:654: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[6];
av2[2]=t2;
av2[3]=t3;
C_call_with_values(4,av2);}}

/* a8807 in k8801 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8808(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8808,c,av);}
/* modules.scm:654: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_8618(t2,t1,C_i_cadr(((C_word*)t0)[3]));}

/* a8817 in k8801 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8818(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6=av[6];
C_word t7=av[7];
C_word t8;
C_word t9;
C_word *a;
if(c!=8) C_bad_argc_2(c,8,t0);
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_8818,c,av);}
a=C_alloc(11);
t8=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_8822,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=t4,a[5]=((C_word*)t0)[3],a[6]=t3,a[7]=t7,a[8]=t6,a[9]=t1,a[10]=t5,tmp=(C_word)a,a+=11,tmp);
/* modules.scm:655: chicken.syntax#strip-syntax */
t9=*((C_word*)lf[67]+1);{
C_word *av2=av;
av2[0]=t9;
av2[1]=t8;
av2[2]=C_i_cddr(((C_word*)t0)[4]);
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}

/* k8820 in a8817 in k8801 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8822(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,5)))){
C_save_and_reclaim((void *)f_8822,c,av);}
a=C_alloc(14);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_8827,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=t3,a[11]=((C_word)li101),tmp=(C_word)a,a+=12,tmp));
t5=((C_word*)t3)[1];
f_8827(t5,((C_word*)t0)[9],((C_word*)t0)[10],C_SCHEME_END_OF_LIST,t1);}

/* loop in k8820 in a8817 in k8801 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_8827(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(14,0,5)))){
C_save_and_reclaim_args((void *)trf_8827,5,t0,t1,t2,t3,t4);}
a=C_alloc(14);
if(C_truep(C_i_nullp(t2))){
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_8839,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=t3,a[9]=((C_word*)t0)[8],a[10]=t6,a[11]=((C_word)li99),tmp=(C_word)a,a+=12,tmp));
t8=((C_word*)t6)[1];
f_8839(t8,t1,((C_word*)t0)[9],C_SCHEME_END_OF_LIST,t4);}
else{
t5=C_i_caar(t2);
t6=C_i_memq(t5,t4);
if(C_truep(t6)){
t7=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_8939,a[2]=t2,a[3]=((C_word*)t0)[10],a[4]=t3,a[5]=t4,a[6]=((C_word)li100),tmp=(C_word)a,a+=7,tmp);
/* modules.scm:657: g2030 */
t8=t7;
f_8939(t8,t1,t6);}
else{
t7=C_u_i_cdr(t2);
t8=C_u_i_car(t2);
t9=C_a_i_cons(&a,2,t8,t3);
/* modules.scm:674: loop */
t11=t1;
t12=t7;
t13=t9;
t14=t4;
t1=t11;
t2=t12;
t3=t13;
t4=t14;
goto loop;}}}

/* loop in loop in k8820 in a8817 in k8801 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_8839(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(23,0,4)))){
C_save_and_reclaim_args((void *)trf_8839,5,t0,t1,t2,t3,t4);}
a=C_alloc(23);
if(C_truep(C_i_nullp(t2))){
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8847,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word)li96),tmp=(C_word)a,a+=5,tmp);
t6=C_i_check_list_2(t4,lf[33]);
t7=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_8857,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=t1,a[6]=((C_word*)t0)[3],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=t3,a[10]=((C_word*)t0)[9],tmp=(C_word)a,a+=11,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8870,a[2]=t9,a[3]=t5,a[4]=((C_word)li97),tmp=(C_word)a,a+=5,tmp));
t11=((C_word*)t9)[1];
f_8870(t11,t7,t4);}
else{
t5=C_i_caar(t2);
t6=C_i_memq(t5,t4);
if(C_truep(t6)){
t7=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_8898,a[2]=t2,a[3]=((C_word*)t0)[10],a[4]=t3,a[5]=t4,a[6]=((C_word)li98),tmp=(C_word)a,a+=7,tmp);
/* modules.scm:659: g2025 */
t8=t7;
f_8898(t8,t1,t6);}
else{
t7=C_u_i_cdr(t2);
t8=C_u_i_car(t2);
t9=C_a_i_cons(&a,2,t8,t3);
/* modules.scm:669: loop */
t12=t1;
t13=t7;
t14=t9;
t15=t4;
t1=t12;
t2=t13;
t3=t14;
t4=t15;
goto loop;}}}

/* g2005 in loop in loop in k8820 in a8817 in k8801 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_8847(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,4)))){
C_save_and_reclaim_args((void *)trf_8847,3,t0,t1,t2);}
/* modules.scm:662: warn */
f_8506(t1,lf[134],((C_word*)t0)[3],t2);}

/* k8855 in loop in loop in k8820 in a8817 in k8801 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8857(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,7)))){
C_save_and_reclaim((void *)f_8857,c,av);}
a=C_alloc(6);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],((C_word*)t0)[3]);
t3=C_a_i_cons(&a,2,((C_word*)t0)[4],t2);
/* modules.scm:664: scheme#values */{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=0;
av2[1]=((C_word*)t0)[5];
av2[2]=((C_word*)t0)[6];
av2[3]=((C_word*)t0)[7];
av2[4]=t3;
av2[5]=((C_word*)t0)[8];
av2[6]=((C_word*)t0)[9];
av2[7]=((C_word*)t0)[10];
C_values(8,av2);}}

/* for-each-loop2004 in loop in loop in k8820 in a8817 in k8801 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_8870(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_8870,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8880,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* modules.scm:660: g2005 */
t4=((C_word*)t0)[3];
f_8847(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k8878 in for-each-loop2004 in loop in loop in k8820 in a8817 in k8801 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 in ... */
static void C_ccall f_8880(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8880,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_8870(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* g2025 in loop in loop in k8820 in a8817 in k8801 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_8898(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,4)))){
C_save_and_reclaim_args((void *)trf_8898,3,t0,t1,t2);}
a=C_alloc(6);
t3=C_i_cdr(((C_word*)t0)[2]);
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8910,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t3,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* modules.scm:667: delete */
f_4518(t4,C_i_car(t2),((C_word*)t0)[5],*((C_word*)lf[43]+1));}

/* k8908 in g2025 in loop in loop in k8820 in a8817 in k8801 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 in ... */
static void C_ccall f_8910(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_8910,c,av);}
/* modules.scm:667: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_8839(t2,((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],t1);}

/* g2030 in loop in k8820 in a8817 in k8801 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_8939(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,4)))){
C_save_and_reclaim_args((void *)trf_8939,3,t0,t1,t2);}
a=C_alloc(6);
t3=C_i_cdr(((C_word*)t0)[2]);
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8951,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t3,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* modules.scm:672: delete */
f_4518(t4,C_i_car(t2),((C_word*)t0)[5],*((C_word*)lf[43]+1));}

/* k8949 in g2030 in loop in k8820 in a8817 in k8801 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8951(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_8951,c,av);}
/* modules.scm:672: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_8827(t2,((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],t1);}

/* k8981 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8983(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_8983,c,av);}
a=C_alloc(9);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_8986,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* modules.scm:676: ##sys#check-syntax */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[132]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[132]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[7];
av2[3]=((C_word*)t0)[3];
av2[4]=lf[137];
tp(5,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_9233,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[8],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[9],tmp=(C_word)a,a+=9,tmp);
/* modules.scm:702: c */
t3=((C_word*)t0)[10];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[11];
av2[3]=((C_word*)t0)[5];
((C_proc)C_fast_retrieve_proc(t3))(4,av2);}}}

/* k8984 in k8981 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8986(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,8)))){
C_save_and_reclaim((void *)f_8986,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8991,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word)li103),tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9001,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[3],a[5]=((C_word)li111),tmp=(C_word)a,a+=6,tmp);
/* modules.scm:677: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[6];
av2[2]=t2;
av2[3]=t3;
C_call_with_values(4,av2);}}

/* a8990 in k8984 in k8981 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_8991(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8991,c,av);}
/* modules.scm:677: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_8618(t2,t1,C_i_cadr(((C_word*)t0)[3]));}

/* a9000 in k8984 in k8981 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9001(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6=av[6];
C_word t7=av[7];
C_word t8;
C_word t9;
C_word *a;
if(c!=8) C_bad_argc_2(c,8,t0);
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_9001,c,av);}
a=C_alloc(11);
t8=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_9005,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=t4,a[5]=((C_word*)t0)[3],a[6]=t3,a[7]=t7,a[8]=t6,a[9]=t1,a[10]=t5,tmp=(C_word)a,a+=11,tmp);
/* modules.scm:678: chicken.syntax#strip-syntax */
t9=*((C_word*)lf[67]+1);{
C_word *av2=av;
av2[0]=t9;
av2[1]=t8;
av2[2]=C_i_cddr(((C_word*)t0)[4]);
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}

/* k9003 in a9000 in k8984 in k8981 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9005(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,5)))){
C_save_and_reclaim((void *)f_9005,c,av);}
a=C_alloc(14);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_9010,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=t3,a[11]=((C_word)li110),tmp=(C_word)a,a+=12,tmp));
t5=((C_word*)t3)[1];
f_9010(t5,((C_word*)t0)[9],((C_word*)t0)[10],C_SCHEME_END_OF_LIST,t1);}

/* loop in k9003 in a9000 in k8984 in k8981 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_9010(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(14,0,5)))){
C_save_and_reclaim_args((void *)trf_9010,5,t0,t1,t2,t3,t4);}
a=C_alloc(14);
if(C_truep(C_i_nullp(t2))){
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_9022,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=t3,a[9]=((C_word*)t0)[8],a[10]=t6,a[11]=((C_word)li108),tmp=(C_word)a,a+=12,tmp));
t8=((C_word*)t6)[1];
f_9022(t8,t1,((C_word*)t0)[9],C_SCHEME_END_OF_LIST,t4);}
else{
t5=C_i_caar(t2);
t6=C_i_assq(t5,t4);
if(C_truep(t6)){
t7=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_9177,a[2]=t2,a[3]=t3,a[4]=((C_word*)t0)[10],a[5]=t4,a[6]=((C_word)li109),tmp=(C_word)a,a+=7,tmp);
/* modules.scm:680: g2135 */
t8=t7;
f_9177(t8,t1,t6);}
else{
t7=C_u_i_cdr(t2);
t8=C_u_i_car(t2);
t9=C_a_i_cons(&a,2,t8,t3);
/* modules.scm:701: loop */
t11=t1;
t12=t7;
t13=t9;
t14=t4;
t1=t11;
t2=t12;
t3=t13;
t4=t14;
goto loop;}}}

/* loop in loop in k9003 in a9000 in k8984 in k8981 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_9022(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(30,0,4)))){
C_save_and_reclaim_args((void *)trf_9022,5,t0,t1,t2,t3,t4);}
a=C_alloc(30);
if(C_truep(C_i_nullp(t2))){
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9030,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word)li104),tmp=(C_word)a,a+=5,tmp);
t6=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t7=t6;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=((C_word*)t8)[1];
t10=C_i_check_list_2(t4,lf[18]);
t11=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_9043,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=t1,a[6]=((C_word*)t0)[3],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=t3,a[10]=((C_word*)t0)[9],a[11]=t5,tmp=(C_word)a,a+=12,tmp);
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9085,a[2]=t8,a[3]=t13,a[4]=t9,a[5]=((C_word)li106),tmp=(C_word)a,a+=6,tmp));
t15=((C_word*)t13)[1];
f_9085(t15,t11,t4);}
else{
t5=C_i_caar(t2);
t6=C_i_assq(t5,t4);
if(C_truep(t6)){
t7=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_9124,a[2]=t2,a[3]=t3,a[4]=((C_word*)t0)[10],a[5]=t4,a[6]=((C_word)li107),tmp=(C_word)a,a+=7,tmp);
/* modules.scm:682: g2130 */
t8=t7;
f_9124(t8,t1,t6);}
else{
t7=C_u_i_cdr(t2);
t8=C_u_i_car(t2);
t9=C_a_i_cons(&a,2,t8,t3);
/* modules.scm:694: loop */
t16=t1;
t17=t7;
t18=t9;
t19=t4;
t1=t16;
t2=t17;
t3=t18;
t4=t19;
goto loop;}}}

/* g2084 in loop in loop in k9003 in a9000 in k8984 in k8981 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 in ... */
static void C_fcall f_9030(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,4)))){
C_save_and_reclaim_args((void *)trf_9030,3,t0,t1,t2);}
/* modules.scm:685: warn */
f_8506(t1,lf[136],((C_word*)t0)[3],t2);}

/* k9041 in loop in loop in k9003 in a9000 in k8984 in k8981 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 in ... */
static void C_ccall f_9043(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,3)))){
C_save_and_reclaim((void *)f_9043,c,av);}
a=C_alloc(18);
t2=C_i_check_list_2(t1,lf[33]);
t3=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_9049,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],tmp=(C_word)a,a+=11,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9062,a[2]=t5,a[3]=((C_word*)t0)[11],a[4]=((C_word)li105),tmp=(C_word)a,a+=5,tmp));
t7=((C_word*)t5)[1];
f_9062(t7,t3,t1);}

/* k9047 in k9041 in loop in loop in k9003 in a9000 in k8984 in k8981 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in ... */
static void C_ccall f_9049(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,7)))){
C_save_and_reclaim((void *)f_9049,c,av);}
a=C_alloc(6);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],((C_word*)t0)[3]);
t3=C_a_i_cons(&a,2,((C_word*)t0)[4],t2);
/* modules.scm:687: scheme#values */{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=0;
av2[1]=((C_word*)t0)[5];
av2[2]=((C_word*)t0)[6];
av2[3]=((C_word*)t0)[7];
av2[4]=t3;
av2[5]=((C_word*)t0)[8];
av2[6]=((C_word*)t0)[9];
av2[7]=((C_word*)t0)[10];
C_values(8,av2);}}

/* for-each-loop2083 in k9041 in loop in loop in k9003 in a9000 in k8984 in k8981 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in ... */
static void C_fcall f_9062(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_9062,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9072,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* modules.scm:683: g2084 */
t4=((C_word*)t0)[3];
f_9030(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k9070 in for-each-loop2083 in k9041 in loop in loop in k9003 in a9000 in k8984 in k8981 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in ... */
static void C_ccall f_9072(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_9072,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_9062(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* map-loop2096 in loop in loop in k9003 in a9000 in k8984 in k8981 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 in ... */
static void C_fcall f_9085(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_9085,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_car(t3);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* g2130 in loop in loop in k9003 in a9000 in k8984 in k8981 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 in ... */
static void C_fcall f_9124(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,4)))){
C_save_and_reclaim_args((void *)trf_9124,3,t0,t1,t2);}
a=C_alloc(12);
t3=C_i_cdr(((C_word*)t0)[2]);
t4=C_i_cadr(t2);
t5=C_i_cdar(((C_word*)t0)[2]);
t6=C_a_i_cons(&a,2,t4,t5);
t7=C_a_i_cons(&a,2,t6,((C_word*)t0)[3]);
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9140,a[2]=((C_word*)t0)[4],a[3]=t1,a[4]=t3,a[5]=t7,tmp=(C_word)a,a+=6,tmp);
/* modules.scm:692: delete */
f_4518(t8,t2,((C_word*)t0)[5],*((C_word*)lf[43]+1));}

/* k9138 in g2130 in loop in loop in k9003 in a9000 in k8984 in k8981 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in ... */
static void C_ccall f_9140(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_9140,c,av);}
/* modules.scm:690: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_9022(t2,((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],t1);}

/* g2135 in loop in k9003 in a9000 in k8984 in k8981 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_9177(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,4)))){
C_save_and_reclaim_args((void *)trf_9177,3,t0,t1,t2);}
a=C_alloc(12);
t3=C_i_cdr(((C_word*)t0)[2]);
t4=C_i_cadr(t2);
t5=C_i_cdar(((C_word*)t0)[2]);
t6=C_a_i_cons(&a,2,t4,t5);
t7=C_a_i_cons(&a,2,t6,((C_word*)t0)[3]);
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9193,a[2]=((C_word*)t0)[4],a[3]=t1,a[4]=t3,a[5]=t7,tmp=(C_word)a,a+=6,tmp);
/* modules.scm:699: delete */
f_4518(t8,t2,((C_word*)t0)[5],*((C_word*)lf[43]+1));}

/* k9191 in g2135 in loop in k9003 in a9000 in k8984 in k8981 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 in ... */
static void C_ccall f_9193(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_9193,c,av);}
/* modules.scm:697: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_9010(t2,((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],t1);}

/* k9231 in k8981 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9233(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,4)))){
C_save_and_reclaim((void *)f_9233,c,av);}
a=C_alloc(7);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_9236,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* modules.scm:703: ##sys#check-syntax */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[132]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[132]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[7];
av2[3]=((C_word*)t0)[3];
av2[4]=lf[139];
tp(5,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9389,a[2]=((C_word*)t0)[8],a[3]=((C_word*)t0)[6],tmp=(C_word)a,a+=4,tmp);
/* modules.scm:713: chicken.syntax#strip-syntax */
t3=*((C_word*)lf[67]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}

/* k9234 in k9231 in k8981 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9236(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,8)))){
C_save_and_reclaim((void *)f_9236,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9241,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word)li112),tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9251,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[3],a[5]=((C_word)li116),tmp=(C_word)a,a+=6,tmp);
/* modules.scm:704: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[6];
av2[2]=t2;
av2[3]=t3;
C_call_with_values(4,av2);}}

/* a9240 in k9234 in k9231 in k8981 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9241(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_9241,c,av);}
/* modules.scm:704: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_8618(t2,t1,C_i_cadr(((C_word*)t0)[3]));}

/* a9250 in k9234 in k9231 in k8981 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9251(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6=av[6];
C_word t7=av[7];
C_word t8;
C_word t9;
C_word *a;
if(c!=8) C_bad_argc_2(c,8,t0);
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_9251,c,av);}
a=C_alloc(11);
t8=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_9255,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t4,a[5]=t5,a[6]=t6,a[7]=t1,a[8]=t2,a[9]=t3,a[10]=t7,tmp=(C_word)a,a+=11,tmp);
/* modules.scm:705: chicken.syntax#strip-syntax */
t9=*((C_word*)lf[67]+1);{
C_word *av2=av;
av2[0]=t9;
av2[1]=t8;
av2[2]=C_i_caddr(((C_word*)t0)[4]);
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}

/* k9253 in a9250 in k9234 in k9231 in k8981 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9255(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(37,c,3)))){
C_save_and_reclaim((void *)f_9255,c,av);}
a=C_alloc(37);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9257,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word)li113),tmp=(C_word)a,a+=5,tmp);
t3=C_a_i_list(&a,3,((C_word*)t0)[3],((C_word*)t0)[4],t1);
t4=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t5=t4;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=((C_word*)t6)[1];
t8=C_i_check_list_2(((C_word*)t0)[5],lf[18]);
t9=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_9300,a[2]=((C_word*)t0)[6],a[3]=((C_word*)t0)[7],a[4]=((C_word*)t0)[8],a[5]=((C_word*)t0)[9],a[6]=t3,a[7]=((C_word*)t0)[10],a[8]=t2,tmp=(C_word)a,a+=9,tmp);
t10=C_SCHEME_UNDEFINED;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_set_block_item(t11,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_9346,a[2]=t6,a[3]=t11,a[4]=t2,a[5]=t7,a[6]=((C_word)li115),tmp=(C_word)a,a+=7,tmp));
t13=((C_word*)t11)[1];
f_9346(t13,t9,((C_word*)t0)[5]);}

/* rename in k9253 in a9250 in k9234 in k9231 in k8981 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_9257(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,0,2)))){
C_save_and_reclaim_args((void *)trf_9257,3,t0,t1,t2);}
a=C_alloc(11);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9265,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9271,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9275,a[2]=t4,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* modules.scm:709: tostr */
t6=((C_word*)((C_word*)t0)[2])[1];
f_8520(t6,t5,((C_word*)t0)[3]);}

/* k9263 in rename in k9253 in a9250 in k9234 in k9231 in k8981 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 in ... */
static void C_ccall f_9265(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_9265,c,av);}
a=C_alloc(3);
t2=C_u_i_cdr(((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_cons(&a,2,t1,t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k9269 in rename in k9253 in a9250 in k9234 in k9231 in k8981 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 in ... */
static void C_ccall f_9271(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_9271,c,av);}
/* modules.scm:708: ##sys#string->symbol */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[138]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[138]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
tp(3,av2);}}

/* k9273 in rename in k9253 in a9250 in k9234 in k9231 in k8981 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 in ... */
static void C_ccall f_9275(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_9275,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9279,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* modules.scm:709: ##sys#symbol->string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[125]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[125]+1);
av2[1]=t2;
av2[2]=C_i_car(((C_word*)t0)[3]);
tp(3,av2);}}

/* k9277 in k9273 in rename in k9253 in a9250 in k9234 in k9231 in k8981 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in ... */
static void C_ccall f_9279(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_9279,c,av);}
/* modules.scm:709: ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[118]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[118]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
tp(4,av2);}}

/* k9298 in k9253 in a9250 in k9234 in k9231 in k8981 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9300(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(22,c,3)))){
C_save_and_reclaim((void *)f_9300,c,av);}
a=C_alloc(22);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=C_i_check_list_2(((C_word*)t0)[2],lf[18]);
t7=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_9310,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=t1,a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_9312,a[2]=t4,a[3]=t9,a[4]=((C_word*)t0)[8],a[5]=t5,a[6]=((C_word)li114),tmp=(C_word)a,a+=7,tmp));
t11=((C_word*)t9)[1];
f_9312(t11,t7,((C_word*)t0)[2]);}

/* k9308 in k9298 in k9253 in a9250 in k9234 in k9231 in k8981 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 in ... */
static void C_ccall f_9310(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_9310,c,av);}
/* modules.scm:711: scheme#values */{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
av2[4]=((C_word*)t0)[5];
av2[5]=((C_word*)t0)[6];
av2[6]=t1;
av2[7]=((C_word*)t0)[7];
C_values(8,av2);}}

/* map-loop2201 in k9298 in k9253 in a9250 in k9234 in k9231 in k8981 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 in ... */
static void C_fcall f_9312(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_9312,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9337,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* modules.scm:711: g2207 */
t4=((C_word*)t0)[4];
f_9257(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k9335 in map-loop2201 in k9298 in k9253 in a9250 in k9234 in k9231 in k8981 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in ... */
static void C_ccall f_9337(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_9337,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_9312(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* map-loop2175 in k9253 in a9250 in k9234 in k9231 in k8981 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_9346(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_9346,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9371,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* modules.scm:711: g2181 */
t4=((C_word*)t0)[4];
f_9257(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k9369 in map-loop2175 in k9253 in a9250 in k9234 in k9231 in k8981 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 in ... */
static void C_ccall f_9371(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_9371,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_9346(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k9387 in k9231 in k8981 in k8798 in k8648 in loop in a8566 in k8502 in k8499 in k8496 in k8493 in ##sys#decompose-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9389(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_9389,c,av);}
/* modules.scm:713: module-imports */
t2=((C_word*)t0)[2];
f_8570(t2,((C_word*)t0)[3],t1);}

/* ##sys#expand-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9395(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6=av[6];
C_word t7=av[7];
C_word t8=av[8];
C_word t9=av[9];
C_word t10;
C_word t11;
C_word *a;
if(c!=10) C_bad_argc_2(c,10,t0);
if(C_unlikely(!C_demand(C_calculate_demand(11,c,4)))){
C_save_and_reclaim((void *)f_9395,c,av);}
a=C_alloc(11);
t10=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_9399,a[2]=t3,a[3]=t4,a[4]=t9,a[5]=t5,a[6]=t6,a[7]=t7,a[8]=t8,a[9]=t2,a[10]=t1,tmp=(C_word)a,a+=11,tmp);
/* modules.scm:716: ##sys#check-syntax */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[132]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[132]+1);
av2[1]=t10;
av2[2]=t9;
av2[3]=t2;
av2[4]=lf[149];
tp(5,av2);}}

/* k9397 in ##sys#expand-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9399(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(20,c,3)))){
C_save_and_reclaim((void *)f_9399,c,av);}
a=C_alloc(20);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_9400,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word)li122),tmp=(C_word)a,a+=10,tmp);
t3=C_i_cdr(((C_word*)t0)[9]);
t4=C_i_check_list_2(t3,lf[33]);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9434,a[2]=((C_word*)t0)[10],tmp=(C_word)a,a+=3,tmp);
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9436,a[2]=t7,a[3]=t2,a[4]=((C_word)li123),tmp=(C_word)a,a+=5,tmp));
t9=((C_word*)t7)[1];
f_9436(t9,t5,t3);}

/* g2243 in k9397 in ##sys#expand-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_9400(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,0,8)))){
C_save_and_reclaim_args((void *)trf_9400,3,t0,t1,t2);}
a=C_alloc(16);
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_9406,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word)li120),tmp=(C_word)a,a+=7,tmp);
t4=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_9412,a[2]=((C_word*)t0)[4],a[3]=t2,a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word)li121),tmp=(C_word)a,a+=9,tmp);
/* modules.scm:719: ##sys#call-with-values */{
C_word av2[4];
av2[0]=0;
av2[1]=t1;
av2[2]=t3;
av2[3]=t4;
C_call_with_values(4,av2);}}

/* a9405 in g2243 in k9397 in ##sys#expand-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9406(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_9406,c,av);}
/* modules.scm:719: ##sys#decompose-import */
t2=*((C_word*)lf[121]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
av2[3]=((C_word*)t0)[3];
av2[4]=((C_word*)t0)[4];
av2[5]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* a9411 in g2243 in k9397 in ##sys#expand-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9412(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6=av[6];
C_word t7=av[7];
C_word t8;
C_word *a;
if(c!=8) C_bad_argc_2(c,8,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,10)))){
C_save_and_reclaim((void *)f_9412,c,av);}
if(C_truep(C_i_not(t4))){
/* modules.scm:721: ##sys#syntax-error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[127]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[127]+1);
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
av2[3]=lf[146];
av2[4]=t2;
av2[5]=((C_word*)t0)[3];
tp(6,av2);}}
else{
/* modules.scm:722: ##sys#import */
t8=*((C_word*)lf[147]+1);{
C_word *av2;
if(c >= 11) {
  av2=av;
} else {
  av2=C_alloc(11);
}
av2[0]=t8;
av2[1]=t1;
av2[2]=t4;
av2[3]=t5;
av2[4]=t6;
av2[5]=t7;
av2[6]=((C_word*)t0)[4];
av2[7]=((C_word*)t0)[5];
av2[8]=((C_word*)t0)[6];
av2[9]=((C_word*)t0)[7];
av2[10]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t8+1)))(11,av2);}}}

/* k9432 in k9397 in ##sys#expand-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9434(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_9434,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=lf[148];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* for-each-loop2242 in k9397 in ##sys#expand-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_9436(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_9436,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9446,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* modules.scm:717: g2243 */
t4=((C_word*)t0)[3];
f_9400(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k9444 in for-each-loop2242 in k9397 in ##sys#expand-import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9446(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_9446,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_9436(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9459(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6=av[6];
C_word t7=av[7];
C_word t8=av[8];
C_word t9=av[9];
C_word t10=av[10];
C_word t11;
C_word t12;
C_word *a;
if(c!=11) C_bad_argc_2(c,11,t0);
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_9459,c,av);}
a=C_alloc(12);
t11=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_9463,a[2]=t6,a[3]=t3,a[4]=t7,a[5]=t4,a[6]=t1,a[7]=t9,a[8]=t5,a[9]=t10,a[10]=t8,a[11]=t2,tmp=(C_word)a,a+=12,tmp);
/* modules.scm:727: ##sys#current-module */
t12=*((C_word*)lf[2]+1);{
C_word *av2=av;
av2[0]=t12;
av2[1]=t11;
((C_proc)(void*)(*((C_word*)t12+1)))(2,av2);}}

/* k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9463(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,3)))){
C_save_and_reclaim((void *)f_9463,c,av);}
a=C_alloc(18);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_9466,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=t1,a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],tmp=(C_word)a,a+=11,tmp);
if(C_truep(t1)){
if(C_truep(((C_word*)t0)[10])){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9876,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t4=C_i_check_structure_2(t1,lf[4],lf[58]);
t5=C_i_block_ref(t1,C_fix(9));
t6=C_a_i_list1(&a,1,((C_word*)t0)[11]);
/* modules.scm:732: scheme#append */
t7=*((C_word*)lf[19]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t7;
av2[1]=t3;
av2[2]=t5;
av2[3]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9891,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t4=C_i_check_structure_2(t1,lf[4],lf[57]);
t5=C_i_block_ref(t1,C_fix(8));
t6=C_a_i_list1(&a,1,((C_word*)t0)[11]);
/* modules.scm:735: scheme#append */
t7=*((C_word*)lf[19]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t7;
av2[1]=t3;
av2[2]=t5;
av2[3]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_9466(2,av2);}}}

/* k9464 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9466(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(22,c,3)))){
C_save_and_reclaim((void *)f_9466,c,av);}
a=C_alloc(22);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9476,a[2]=((C_word*)t0)[2],a[3]=((C_word)li125),tmp=(C_word)a,a+=4,tmp);
t3=C_i_check_list_2(((C_word*)t0)[3],lf[33]);
t4=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_9520,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[2],a[6]=((C_word*)t0)[3],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],tmp=(C_word)a,a+=11,tmp);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9845,a[2]=t6,a[3]=t2,a[4]=((C_word)li132),tmp=(C_word)a,a+=5,tmp));
t8=((C_word*)t6)[1];
f_9845(t8,t4,((C_word*)t0)[3]);}

/* g2304 in k9464 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_9476(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_9476,3,t0,t1,t2);}
a=C_alloc(5);
t3=C_i_car(t2);
if(C_truep(t3)){
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9514,a[2]=t3,a[3]=t2,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* modules.scm:742: import-env */
t5=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t5;
av2[1]=t4;
((C_proc)C_fast_retrieve_proc(t5))(2,av2);}}
else{
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k9512 in g2304 in k9464 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9514(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_9514,c,av);}
t2=C_i_assq(((C_word*)t0)[2],t1);
if(C_truep(t2)){
t3=C_u_i_cdr(((C_word*)t0)[3]);
if(C_truep(t3)){
t4=C_i_cdr(t2);
t5=C_eqp(t3,t4);
if(C_truep(C_i_not(t5))){
/* modules.scm:745: ##sys#notice */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[150]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[150]+1);
av2[1]=((C_word*)t0)[4];
av2[2]=lf[151];
av2[3]=((C_word*)t0)[2];
tp(4,av2);}}
else{
t6=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}
else{
t4=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}
else{
t3=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k9518 in k9464 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9520(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(22,c,3)))){
C_save_and_reclaim((void *)f_9520,c,av);}
a=C_alloc(22);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9521,a[2]=((C_word*)t0)[2],a[3]=((C_word)li126),tmp=(C_word)a,a+=4,tmp);
t3=C_i_check_list_2(((C_word*)t0)[3],lf[33]);
t4=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_9563,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],tmp=(C_word)a,a+=11,tmp);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9822,a[2]=t6,a[3]=t2,a[4]=((C_word)li131),tmp=(C_word)a,a+=5,tmp));
t8=((C_word*)t6)[1];
f_9822(t8,t4,((C_word*)t0)[3]);}

/* g2314 in k9518 in k9464 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_9521(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_9521,3,t0,t1,t2);}
a=C_alloc(5);
t3=C_i_car(t2);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9557,a[2]=t3,a[3]=t2,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* modules.scm:749: macro-env */
t5=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t5;
av2[1]=t4;
((C_proc)C_fast_retrieve_proc(t5))(2,av2);}}

/* k9555 in g2314 in k9518 in k9464 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9557(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_9557,c,av);}
t2=C_i_assq(((C_word*)t0)[2],t1);
if(C_truep(t2)){
t3=C_u_i_cdr(((C_word*)t0)[3]);
t4=C_i_cdr(t2);
t5=C_eqp(t3,t4);
if(C_truep(C_i_not(t5))){
/* modules.scm:751: ##sys#notice */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[150]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[150]+1);
av2[1]=((C_word*)t0)[4];
av2[2]=lf[152];
av2[3]=C_u_i_car(((C_word*)t0)[3]);
tp(4,av2);}}
else{
t6=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}
else{
t3=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k9561 in k9518 in k9464 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9563(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,3)))){
C_save_and_reclaim((void *)f_9563,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_9566,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
if(C_truep(((C_word*)t0)[7])){
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_9591,a[2]=((C_word*)t0)[8],a[3]=t2,a[4]=((C_word*)t0)[9],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
if(C_truep(((C_word*)t0)[8])){
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_9591(2,av2);}}
else{
/* modules.scm:755: ##sys#syntax-error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[127]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[127]+1);
av2[1]=t3;
av2[2]=((C_word*)t0)[10];
av2[3]=lf[153];
tp(4,av2);}}}
else{
t3=t2;
f_9566(t3,C_SCHEME_UNDEFINED);}}

/* k9564 in k9561 in k9518 in k9464 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_9566(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,0,2)))){
C_save_and_reclaim_args((void *)trf_9566,2,t0,t1);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9569,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9584,a[2]=((C_word*)t0)[5],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9588,a[2]=t3,a[3]=((C_word*)t0)[6],tmp=(C_word)a,a+=4,tmp);
/* modules.scm:776: import-env */
t5=((C_word*)t0)[5];{
C_word av2[2];
av2[0]=t5;
av2[1]=t4;
((C_proc)C_fast_retrieve_proc(t5))(2,av2);}}

/* k9567 in k9564 in k9561 in k9518 in k9464 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9569(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_9569,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9576,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9580,a[2]=t2,a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
/* modules.scm:777: macro-env */
t4=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)C_fast_retrieve_proc(t4))(2,av2);}}

/* k9574 in k9567 in k9564 in k9561 in k9518 in k9464 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9576(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_9576,c,av);}
/* modules.scm:777: macro-env */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
((C_proc)C_fast_retrieve_proc(t2))(3,av2);}}

/* k9578 in k9567 in k9564 in k9561 in k9518 in k9464 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9580(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_9580,c,av);}
/* modules.scm:777: scheme#append */
t2=*((C_word*)lf[19]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k9582 in k9564 in k9561 in k9518 in k9464 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9584(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_9584,c,av);}
/* modules.scm:776: import-env */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
((C_proc)C_fast_retrieve_proc(t2))(3,av2);}}

/* k9586 in k9564 in k9561 in k9518 in k9464 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9588(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_9588,c,av);}
/* modules.scm:776: scheme#append */
t2=*((C_word*)lf[19]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k9589 in k9561 in k9518 in k9464 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9591(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(27,c,3)))){
C_save_and_reclaim((void *)f_9591,c,av);}
a=C_alloc(27);
t2=C_i_check_structure_2(((C_word*)t0)[2],lf[4],lf[12]);
t3=C_i_block_ref(((C_word*)t0)[2],C_fix(3));
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9597,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t5=C_eqp(C_SCHEME_TRUE,t3);
if(C_truep(t5)){
t6=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9617,a[2]=((C_word*)t0)[2],a[3]=t4,a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],tmp=(C_word)a,a+=6,tmp);
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9714,a[2]=((C_word*)t0)[2],a[3]=t6,tmp=(C_word)a,a+=4,tmp);
t8=C_i_check_structure_2(((C_word*)t0)[2],lf[4],lf[14]);
/* modules.scm:758: scheme#append */
t9=*((C_word*)lf[19]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t9;
av2[1]=t7;
av2[2]=((C_word*)t0)[5];
av2[3]=C_i_block_ref(((C_word*)t0)[2],C_fix(12));
((C_proc)(void*)(*((C_word*)t9+1)))(4,av2);}}
else{
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9725,a[2]=((C_word*)t0)[2],a[3]=t4,tmp=(C_word)a,a+=4,tmp);
t7=C_i_check_structure_2(((C_word*)t0)[2],lf[4],lf[12]);
t8=C_i_block_ref(((C_word*)t0)[2],C_fix(3));
t9=C_eqp(C_SCHEME_TRUE,t8);
t10=(C_truep(t9)?C_SCHEME_END_OF_LIST:t8);
t11=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t12=t11;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=((C_word*)t13)[1];
t15=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9739,a[2]=t6,a[3]=t10,a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp);
t16=C_SCHEME_UNDEFINED;
t17=(*a=C_VECTOR_TYPE|1,a[1]=t16,tmp=(C_word)a,a+=2,tmp);
t18=C_set_block_item(t17,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9782,a[2]=t13,a[3]=t17,a[4]=t14,a[5]=((C_word)li130),tmp=(C_word)a,a+=6,tmp));
t19=((C_word*)t17)[1];
f_9782(t19,t15,((C_word*)t0)[6]);}}

/* k9595 in k9589 in k9561 in k9518 in k9464 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9597(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_9597,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9600,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9607,a[2]=((C_word*)t0)[3],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t4=C_i_check_structure_2(((C_word*)t0)[3],lf[4],lf[64]);
t5=C_i_block_ref(((C_word*)t0)[3],C_fix(13));
/* modules.scm:774: merge-se */
f_6568(t3,C_a_i_list(&a,2,t5,((C_word*)t0)[4]));}

/* k9598 in k9595 in k9589 in k9561 in k9518 in k9464 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9600(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_9600,c,av);}
t2=((C_word*)t0)[2];
f_9566(t2,C_SCHEME_UNDEFINED);}

/* k9605 in k9595 in k9589 in k9561 in k9518 in k9464 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9607(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_9607,c,av);}
t2=C_i_check_structure_2(((C_word*)t0)[2],lf[4],C_SCHEME_FALSE);
/* modules.scm:93: ##sys#block-set! */
t3=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[2];
av2[3]=C_fix(13);
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k9615 in k9589 in k9561 in k9518 in k9464 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9617(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(22,c,3)))){
C_save_and_reclaim((void *)f_9617,c,av);}
a=C_alloc(22);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9624,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=C_i_check_structure_2(((C_word*)t0)[2],lf[4],lf[32]);
t4=C_i_block_ref(((C_word*)t0)[2],C_fix(5));
t5=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t6=t5;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=((C_word*)t7)[1];
t9=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9635,a[2]=t2,a[3]=t4,a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t10=C_SCHEME_UNDEFINED;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_set_block_item(t11,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9678,a[2]=t7,a[3]=t11,a[4]=t8,a[5]=((C_word)li128),tmp=(C_word)a,a+=6,tmp));
t13=((C_word*)t11)[1];
f_9678(t13,t9,((C_word*)t0)[5]);}

/* k9622 in k9615 in k9589 in k9561 in k9518 in k9464 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9624(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_9624,c,av);}
t2=C_i_check_structure_2(((C_word*)t0)[2],lf[4],C_SCHEME_FALSE);
/* modules.scm:93: ##sys#block-set! */
t3=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[2];
av2[3]=C_fix(5);
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k9633 in k9615 in k9589 in k9561 in k9518 in k9464 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9635(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,3)))){
C_save_and_reclaim((void *)f_9635,c,av);}
a=C_alloc(18);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9642,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9644,a[2]=t4,a[3]=t8,a[4]=t5,a[5]=((C_word)li127),tmp=(C_word)a,a+=6,tmp));
t10=((C_word*)t8)[1];
f_9644(t10,t6,((C_word*)t0)[4]);}

/* k9640 in k9633 in k9615 in k9589 in k9561 in k9518 in k9464 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9642(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_9642,c,av);}
/* modules.scm:761: scheme#append */
t2=*((C_word*)lf[19]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* map-loop2382 in k9633 in k9615 in k9589 in k9561 in k9518 in k9464 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_9644(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_9644,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_car(t3);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* map-loop2356 in k9615 in k9589 in k9561 in k9518 in k9464 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_9678(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_9678,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_car(t3);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k9712 in k9589 in k9561 in k9518 in k9464 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9714(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_9714,c,av);}
t2=C_i_check_structure_2(((C_word*)t0)[2],lf[4],C_SCHEME_FALSE);
/* modules.scm:93: ##sys#block-set! */
t3=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[2];
av2[3]=C_fix(12);
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k9723 in k9589 in k9561 in k9518 in k9464 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9725(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_9725,c,av);}
t2=C_i_check_structure_2(((C_word*)t0)[2],lf[4],C_SCHEME_FALSE);
/* modules.scm:93: ##sys#block-set! */
t3=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[2];
av2[3]=C_fix(3);
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k9737 in k9589 in k9561 in k9518 in k9464 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9739(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,3)))){
C_save_and_reclaim((void *)f_9739,c,av);}
a=C_alloc(18);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9746,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t7=C_SCHEME_UNDEFINED;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_set_block_item(t8,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9748,a[2]=t4,a[3]=t8,a[4]=t5,a[5]=((C_word)li129),tmp=(C_word)a,a+=6,tmp));
t10=((C_word*)t8)[1];
f_9748(t10,t6,((C_word*)t0)[4]);}

/* k9744 in k9737 in k9589 in k9561 in k9518 in k9464 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9746(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_9746,c,av);}
/* modules.scm:767: scheme#append */
t2=*((C_word*)lf[19]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* map-loop2436 in k9737 in k9589 in k9561 in k9518 in k9464 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_9748(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_9748,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_car(t3);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* map-loop2410 in k9589 in k9561 in k9518 in k9464 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_9782(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_9782,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_car(t3);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* for-each-loop2313 in k9518 in k9464 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_9822(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_9822,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9832,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* modules.scm:747: g2314 */
t4=((C_word*)t0)[3];
f_9521(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k9830 in for-each-loop2313 in k9518 in k9464 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9832(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_9832,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_9822(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* for-each-loop2303 in k9464 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_9845(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_9845,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9855,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* modules.scm:739: g2304 */
t4=((C_word*)t0)[3];
f_9476(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k9853 in for-each-loop2303 in k9464 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9855(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_9855,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_9845(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k9874 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9876(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_9876,c,av);}
t2=C_i_check_structure_2(((C_word*)t0)[2],lf[4],C_SCHEME_FALSE);
/* modules.scm:93: ##sys#block-set! */
t3=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[2];
av2[3]=C_fix(9);
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k9889 in k9461 in ##sys#import in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9891(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_9891,c,av);}
t2=C_i_check_structure_2(((C_word*)t0)[2],lf[4],C_SCHEME_FALSE);
/* modules.scm:93: ##sys#block-set! */
t3=*((C_word*)lf[9]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[2];
av2[3]=C_fix(8);
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* module-rename in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_9901(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,0,4)))){
C_save_and_reclaim_args((void *)trf_9901,3,t1,t2,t3);}
a=C_alloc(3);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9909,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* modules.scm:781: scheme#string-append */
t5=*((C_word*)lf[95]+1);{
C_word av2[5];
av2[0]=t5;
av2[1]=t4;
av2[2]=C_slot(t3,C_fix(1));
av2[3]=lf[154];
av2[4]=C_slot(t2,C_fix(1));
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}

/* k9907 in module-rename in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9909(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_9909,c,av);}
/* modules.scm:780: ##sys#string->symbol */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[138]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[138]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
tp(3,av2);}}

/* ##sys#alias-global-hook in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9919(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_9919,c,av);}
a=C_alloc(10);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9922,a[2]=t3,a[3]=t4,a[4]=((C_word)li137),tmp=(C_word)a,a+=5,tmp);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9956,a[2]=t1,a[3]=t2,a[4]=t5,tmp=(C_word)a,a+=5,tmp);
/* modules.scm:795: chicken.keyword#keyword? */
t7=*((C_word*)lf[129]+1);{
C_word *av2=av;
av2[0]=t7;
av2[1]=t6;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}

/* mrename in ##sys#alias-global-hook in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_9922(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_9922,3,t0,t1,t2);}
a=C_alloc(6);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9926,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
/* modules.scm:788: ##sys#current-module */
t4=*((C_word*)lf[2]+1);{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k9924 in mrename in ##sys#alias-global-hook in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9926(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_9926,c,av);}
a=C_alloc(6);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9930,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word)li136),tmp=(C_word)a,a+=6,tmp);
/* modules.scm:788: g2493 */
t3=t2;
f_9930(t3,((C_word*)t0)[5],t1);}
else{
t2=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* g2493 in k9924 in mrename in ##sys#alias-global-hook in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_9930(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,0,3)))){
C_save_and_reclaim_args((void *)trf_9930,3,t0,t1,t2);}
a=C_alloc(11);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9937,a[2]=t2,a[3]=t1,a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
if(C_truep(((C_word*)t0)[3])){
t4=C_i_check_structure_2(t2,lf[4],lf[6]);
/* modules.scm:793: module-rename */
f_9901(t1,((C_word*)t0)[2],C_i_block_ref(t2,C_fix(1)));}
else{
if(C_truep(t2)){
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6199,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[4],a[4]=t3,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* modules.scm:239: module-undefined-list */
t5=*((C_word*)lf[7]+1);{
C_word av2[3];
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}
else{
t4=C_i_check_structure_2(t2,lf[4],lf[6]);
/* modules.scm:793: module-rename */
f_9901(t1,((C_word*)t0)[2],C_i_block_ref(t2,C_fix(1)));}}}

/* k9935 in g2493 in k9924 in mrename in ##sys#alias-global-hook in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9937(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_9937,c,av);}
t2=C_i_check_structure_2(((C_word*)t0)[2],lf[4],lf[6]);
/* modules.scm:793: module-rename */
f_9901(((C_word*)t0)[3],((C_word*)t0)[4],C_i_block_ref(((C_word*)t0)[2],C_fix(1)));}

/* k9954 in ##sys#alias-global-hook in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9956(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_9956,c,av);}
a=C_alloc(5);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
if(C_truep(C_u_i_namespaced_symbolp(((C_word*)t0)[3]))){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9997,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
/* modules.scm:797: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[29]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[29]+1);
av2[1]=t2;
tp(2,av2);}}}}

/* g2504 in k9995 in k9954 in ##sys#alias-global-hook in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_fcall f_9971(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,2)))){
C_save_and_reclaim_args((void *)trf_9971,3,t0,t1,t2);}
t3=C_i_cdr(t2);
if(C_truep(C_i_pairp(t3))){
/* modules.scm:802: mrename */
t4=((C_word*)t0)[2];
f_9922(t4,t1,((C_word*)t0)[3]);}
else{
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k9995 in k9954 in ##sys#alias-global-hook in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9997(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_9997,c,av);}
a=C_alloc(5);
t2=C_i_assq(((C_word*)t0)[2],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9971,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],a[4]=((C_word)li138),tmp=(C_word)a,a+=5,tmp);
/* modules.scm:795: g2504 */
t4=t3;
f_9971(t4,((C_word*)t0)[4],t2);}
else{
/* modules.scm:803: mrename */
t3=((C_word*)t0)[3];
f_9922(t3,((C_word*)t0)[4],((C_word*)t0)[2]);}}

/* ##sys#validate-exports in k5311 in k5307 in k4025 in k4022 in k4019 in k4016 */
static void C_ccall f_9999(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(22,c,3)))){
C_save_and_reclaim((void *)f_9999,c,av);}
a=C_alloc(22);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10002,a[2]=t3,a[3]=((C_word)li140),tmp=(C_word)a,a+=4,tmp));
t9=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10008,a[2]=t5,a[3]=t2,a[4]=((C_word)li141),tmp=(C_word)a,a+=5,tmp));
t10=C_eqp(lf[159],t2);
if(C_truep(t10)){
t11=t1;{
C_word *av2=av;
av2[0]=t11;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t11+1)))(2,av2);}}
else{
if(C_truep(C_i_symbolp(t2))){
/* modules.scm:813: iface */
t11=((C_word*)t7)[1];
f_10008(t11,t1,t2);}
else{
t11=C_i_listp(t2);
if(C_truep(C_i_not(t11))){
/* modules.scm:815: err */
t12=((C_word*)t5)[1];
f_10002(t12,t1,C_a_i_list(&a,2,lf[160],t2));}
else{
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10049,a[2]=t5,a[3]=t2,a[4]=t13,a[5]=t7,a[6]=((C_word)li143),tmp=(C_word)a,a+=7,tmp));
t15=((C_word*)t13)[1];
f_10049(t15,t1,t2);}}}}

/* toplevel */
static C_TLS int toplevel_initialized=0;

void C_ccall C_modules_toplevel(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(toplevel_initialized) {C_kontinue(t1,C_SCHEME_UNDEFINED);}
else C_toplevel_entry(C_text("modules"));
C_check_nursery_minimum(C_calculate_demand(3,c,2));
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void*)C_modules_toplevel,c,av);}
toplevel_initialized=1;
if(C_unlikely(!C_demand_2(7604))){
C_save(t1);
C_rereclaim2(7604*sizeof(C_word),1);
t1=C_restore;}
a=C_alloc(3);
C_initialize_lf(lf,252);
lf[0]=C_h_intern(&lf[0],7, C_text("modules"));
lf[2]=C_h_intern(&lf[2],20, C_text("##sys#current-module"));
lf[3]=C_h_intern(&lf[3],30, C_text("##sys#module-alias-environment"));
lf[4]=C_h_intern(&lf[4],6, C_text("module"));
lf[6]=C_h_intern(&lf[6],11, C_text("module-name"));
lf[7]=C_h_intern(&lf[7],21, C_text("module-undefined-list"));
lf[8]=C_h_intern(&lf[8],26, C_text("set-module-undefined-list!"));
lf[9]=C_h_intern(&lf[9],16, C_text("##sys#block-set!"));
lf[10]=C_h_intern(&lf[10],17, C_text("##sys#module-name"));
lf[11]=C_h_intern(&lf[11],20, C_text("##sys#module-exports"));
lf[12]=C_h_intern(&lf[12],18, C_text("module-export-list"));
lf[13]=C_h_intern(&lf[13],15, C_text("module-vexports"));
lf[14]=C_h_intern(&lf[14],15, C_text("module-sexports"));
lf[15]=C_h_intern(&lf[15],27, C_text("##sys#register-module-alias"));
lf[16]=C_h_intern(&lf[16],25, C_text("##sys#with-module-aliases"));
lf[17]=C_h_intern(&lf[17],18, C_text("##sys#dynamic-wind"));
lf[18]=C_h_intern(&lf[18],3, C_text("map"));
lf[19]=C_h_intern(&lf[19],13, C_text("scheme#append"));
lf[20]=C_h_intern(&lf[20],25, C_text("##sys#resolve-module-name"));
lf[21]=C_h_intern(&lf[21],18, C_text("chicken.base#error"));
lf[22]=C_decode_literal(C_heaptop,C_text("\376B\000\000\035module alias refers to itself"));
lf[23]=C_h_intern(&lf[23],27, C_text("chicken.internal#library-id"));
lf[24]=C_h_intern(&lf[24],17, C_text("##sys#find-module"));
lf[25]=C_h_intern(&lf[25],18, C_text("##sys#module-table"));
lf[26]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020module not found"));
lf[27]=C_h_intern(&lf[27],19, C_text("##sys#switch-module"));
lf[28]=C_h_intern(&lf[28],23, C_text("##sys#macro-environment"));
lf[29]=C_h_intern(&lf[29],25, C_text("##sys#current-environment"));
lf[30]=C_h_intern(&lf[30],25, C_text("module-saved-environments"));
lf[31]=C_h_intern(&lf[31],24, C_text("##sys#add-to-export-list"));
lf[32]=C_h_intern(&lf[32],17, C_text("module-exist-list"));
lf[33]=C_h_intern(&lf[33],8, C_text("for-each"));
lf[34]=C_h_intern(&lf[34],30, C_text("##sys#toplevel-definition-hook"));
lf[35]=C_h_intern(&lf[35],30, C_text("##sys#register-meta-expression"));
lf[36]=C_h_intern(&lf[36],23, C_text("module-meta-expressions"));
lf[38]=C_h_intern(&lf[38],10, C_text("##sys#warn"));
lf[39]=C_decode_literal(C_heaptop,C_text("\376B\000\000\047redefinition of imported syntax binding"));
lf[40]=C_decode_literal(C_heaptop,C_text("\376B\000\000&redefinition of imported value binding"));
lf[41]=C_h_intern(&lf[41],21, C_text("##sys#register-export"));
lf[42]=C_h_intern(&lf[42],19, C_text("module-defined-list"));
lf[43]=C_h_intern(&lf[43],10, C_text("scheme#eq\077"));
lf[46]=C_h_intern(&lf[46],28, C_text("##sys#register-syntax-export"));
lf[47]=C_h_intern(&lf[47],26, C_text("module-defined-syntax-list"));
lf[48]=C_decode_literal(C_heaptop,C_text("\376B\000\000!use of syntax precedes definition"));
lf[49]=C_h_intern(&lf[49],30, C_text("##sys#unregister-syntax-export"));
lf[50]=C_h_intern(&lf[50],21, C_text("##sys#register-module"));
lf[52]=C_h_intern(&lf[52],32, C_text("chicken.internal#hash-table-set!"));
lf[53]=C_h_intern(&lf[53],31, C_text("chicken.internal#hash-table-ref"));
lf[54]=C_h_intern(&lf[54],14, C_text("scheme#reverse"));
lf[55]=C_h_intern(&lf[55],32, C_text("chicken.internal#make-hash-table"));
lf[56]=C_h_intern(&lf[56],34, C_text("##sys#compiled-module-registration"));
lf[57]=C_h_intern(&lf[57],19, C_text("module-import-forms"));
lf[58]=C_h_intern(&lf[58],24, C_text("module-meta-import-forms"));
lf[59]=C_h_intern(&lf[59],6, C_text("lambda"));
lf[60]=C_h_intern(&lf[60],22, C_text("##sys#with-environment"));
lf[61]=C_h_intern(&lf[61],12, C_text("##sys#append"));
lf[62]=C_h_intern(&lf[62],5, C_text("quote"));
lf[63]=C_h_intern(&lf[63],14, C_text("module-library"));
lf[64]=C_h_intern(&lf[64],15, C_text("module-iexports"));
lf[65]=C_h_intern(&lf[65],11, C_text("scheme#list"));
lf[66]=C_h_intern(&lf[66],11, C_text("scheme#cons"));
lf[67]=C_h_intern(&lf[67],27, C_text("chicken.syntax#strip-syntax"));
lf[68]=C_h_intern(&lf[68],30, C_text("##sys#register-compiled-module"));
lf[69]=C_h_intern(&lf[69],14, C_text("##core#functor"));
lf[70]=C_h_intern(&lf[70],18, C_text("##sys#fast-reverse"));
lf[71]=C_h_intern(&lf[71],13, C_text("import-syntax"));
lf[72]=C_h_intern(&lf[72],11, C_text("scheme#eval"));
lf[73]=C_h_intern(&lf[73],24, C_text("##sys#ensure-transformer"));
lf[74]=C_h_intern(&lf[74],11, C_text("##sys#error"));
lf[75]=C_h_intern(&lf[75],6, C_text("import"));
lf[76]=C_decode_literal(C_heaptop,C_text("\376B\000\0000cannot find implementation of re-exported syntax"));
lf[77]=C_h_intern(&lf[77],26, C_text("##sys#register-core-module"));
lf[78]=C_decode_literal(C_heaptop,C_text("\376B\000\0002unknown syntax referenced while registering module"));
lf[79]=C_h_intern(&lf[79],31, C_text("##sys#register-primitive-module"));
lf[80]=C_h_intern(&lf[80],21, C_text("##sys#finalize-module"));
lf[81]=C_h_intern(&lf[81],9, C_text("##core#db"));
lf[82]=C_h_intern(&lf[82],30, C_text("chicken.base#get-output-string"));
lf[83]=C_h_intern(&lf[83],14, C_text("scheme#display"));
lf[84]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002)\047"));
lf[85]=C_h_intern(&lf[85],12, C_text("scheme#cadar"));
lf[86]=C_decode_literal(C_heaptop,C_text("\376B\000\000\042\012Warning:    suggesting: `(import "));
lf[87]=C_h_intern(&lf[87],21, C_text("##sys#write-char/port"));
lf[88]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025\012Warning:    (import "));
lf[89]=C_decode_literal(C_heaptop,C_text("\376B\000\000\037\012Warning:    suggesting one of:"));
lf[90]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015\012Warning:    "));
lf[91]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004 in:"));
lf[92]=C_decode_literal(C_heaptop,C_text("\376B\000\000\052reference to possibly unbound identifier `"));
lf[93]=C_h_intern(&lf[93],31, C_text("chicken.base#open-output-string"));
lf[94]=C_decode_literal(C_heaptop,C_text("\376B\000\000$(internal) indirect export not found"));
lf[95]=C_h_intern(&lf[95],20, C_text("scheme#string-append"));
lf[96]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014 in module `"));
lf[97]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001\047"));
lf[98]=C_h_intern(&lf[98],21, C_text("scheme#symbol->string"));
lf[99]=C_decode_literal(C_heaptop,C_text("\376B\000\000!indirect export of syntax binding"));
lf[100]=C_decode_literal(C_heaptop,C_text("\376B\000\000\033indirect reexport of syntax"));
lf[101]=C_decode_literal(C_heaptop,C_text("\376B\000\000\042indirect export of unknown binding"));
lf[102]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021module unresolved"));
lf[103]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001`"));
lf[104]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001\047"));
lf[105]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016Cannot export "));
lf[106]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017 because it is "));
lf[107]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001."));
lf[108]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024Exported identifier "));
lf[109]=C_decode_literal(C_heaptop,C_text("\376B\000\000\026 has not been defined."));
lf[110]=C_h_intern(&lf[110],4, C_text("bomb"));
lf[111]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004fail"));
lf[112]=C_h_intern(&lf[112],30, C_text("##sys#current-meta-environment"));
lf[113]=C_h_intern(&lf[113],28, C_text("##sys#meta-macro-environment"));
lf[114]=C_h_intern(&lf[114],25, C_text("##sys#import-library-hook"));
lf[115]=C_h_intern(&lf[115],21, C_text("##sys#notices-enabled"));
lf[116]=C_h_intern(&lf[116],11, C_text("scheme#load"));
lf[117]=C_h_intern(&lf[117],35, C_text("chicken.load#find-dynamic-extension"));
lf[118]=C_h_intern(&lf[118],19, C_text("##sys#string-append"));
lf[119]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007.import"));
lf[121]=C_h_intern(&lf[121],22, C_text("##sys#decompose-import"));
lf[122]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014 in module `"));
lf[123]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001\047"));
lf[124]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001:"));
lf[125]=C_h_intern(&lf[125],20, C_text("##sys#symbol->string"));
lf[126]=C_h_intern(&lf[126],20, C_text("##sys#number->string"));
lf[127]=C_h_intern(&lf[127],23, C_text("##sys#syntax-error-hook"));
lf[128]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016invalid prefix"));
lf[129]=C_h_intern(&lf[129],24, C_text("chicken.keyword#keyword\077"));
lf[130]=C_decode_literal(C_heaptop,C_text("\376B\000\000\034invalid import specification"));
lf[131]=C_decode_literal(C_heaptop,C_text("\376B\000\000!imported identifier doesn\047t exist"));
lf[132]=C_h_intern(&lf[132],18, C_text("##sys#check-syntax"));
lf[133]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\000\000\000\002\376\001\000\000\006\001symbol\376\377\001\000\000\000\000"));
lf[134]=C_decode_literal(C_heaptop,C_text("\376B\000\000!excluded identifier doesn\047t exist"));
lf[135]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\000\000\000\002\376\001\000\000\006\001symbol\376\377\001\000\000\000\000"));
lf[136]=C_decode_literal(C_heaptop,C_text("\376B\000\000 renamed identifier doesn\047t exist"));
lf[137]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\000\000\000\002\376\003\000\000\002\376\001\000\000\006\001symbol\376\003\000\000\002\376\001\000\000\006\001symbol\376\377\016\376\377\001\000\000\000\000"));
lf[138]=C_h_intern(&lf[138],20, C_text("##sys#string->symbol"));
lf[139]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\001\001_\376\377\016"));
lf[140]=C_h_intern(&lf[140],37, C_text("scheme#call-with-current-continuation"));
lf[141]=C_h_intern(&lf[141],6, C_text("prefix"));
lf[142]=C_h_intern(&lf[142],6, C_text("except"));
lf[143]=C_h_intern(&lf[143],6, C_text("rename"));
lf[144]=C_h_intern(&lf[144],4, C_text("only"));
lf[145]=C_h_intern(&lf[145],19, C_text("##sys#expand-import"));
lf[146]=C_decode_literal(C_heaptop,C_text("\376B\000\000#cannot import from undefined module"));
lf[147]=C_h_intern(&lf[147],12, C_text("##sys#import"));
lf[148]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016"));
lf[149]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000\000\001"));
lf[150]=C_h_intern(&lf[150],12, C_text("##sys#notice"));
lf[151]=C_decode_literal(C_heaptop,C_text("\376B\000\000(re-importing already imported identifier"));
lf[152]=C_decode_literal(C_heaptop,C_text("\376B\000\000$re-importing already imported syntax"));
lf[153]=C_decode_literal(C_heaptop,C_text("\376B\000\000%`reexport\047 only valid inside a module"));
lf[154]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001#"));
lf[155]=C_h_intern(&lf[155],23, C_text("##sys#alias-global-hook"));
lf[156]=C_h_intern(&lf[156],22, C_text("##sys#validate-exports"));
lf[157]=C_h_intern(&lf[157],16, C_text("##core#interface"));
lf[158]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021unknown interface"));
lf[159]=C_h_intern(&lf[159],1, C_text("\052"));
lf[160]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017invalid exports"));
lf[161]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017invalid exports"));
lf[162]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016invalid export"));
lf[163]=C_h_intern_kw(&lf[163],6, C_text("syntax"));
lf[164]=C_h_intern_kw(&lf[164],9, C_text("interface"));
lf[165]=C_decode_literal(C_heaptop,C_text("\376B\000\000\037invalid interface specification"));
lf[166]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016invalid export"));
lf[167]=C_h_intern(&lf[167],22, C_text("##sys#register-functor"));
lf[168]=C_h_intern(&lf[168],25, C_text("##sys#instantiate-functor"));
lf[169]=C_decode_literal(C_heaptop,C_text("\376B\000\000/argument list mismatch in functor instantiation"));
lf[170]=C_h_intern(&lf[170],13, C_text("##core#module"));
lf[171]=C_h_intern(&lf[171],23, C_text("##core#let-module-alias"));
lf[173]=C_decode_literal(C_heaptop,C_text("\376B\000\000!instantation of undefined functor"));
lf[174]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021argument module `"));
lf[175]=C_decode_literal(C_heaptop,C_text("\376B\000\000$\047 does not match required signature\012"));
lf[176]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022in instantiation `"));
lf[177]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016\047 of functor `"));
lf[178]=C_decode_literal(C_heaptop,C_text("\376B\000\0007\047, because the following required exports are missing:\012"));
lf[179]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003\012  "));
lf[180]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\003\001not\376\001\000\000\012\001scheme#not\376\003\000\000\002\376\003\000\000\002\376\001\000\000\010\001boolean\077\376\001\000\000\017\001scheme#boolean\077"
"\376\003\000\000\002\376\003\000\000\002\376\001\000\000\003\001eq\077\376\001\000\000\012\001scheme#eq\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\004\001eqv\077\376\001\000\000\013\001scheme#eqv\077\376\003\000\000\002\376\003\000"
"\000\002\376\001\000\000\006\001equal\077\376\001\000\000\015\001scheme#equal\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001pair\077\376\001\000\000\014\001scheme#pair\077\376\003\000\000\002\376\003\000"
"\000\002\376\001\000\000\004\001cons\376\001\000\000\013\001scheme#cons\376\003\000\000\002\376\003\000\000\002\376\001\000\000\003\001car\376\001\000\000\012\001scheme#car\376\003\000\000\002\376\003\000\000\002\376\001\000\000\003\001"
"cdr\376\001\000\000\012\001scheme#cdr\376\003\000\000\002\376\003\000\000\002\376\001\000\000\004\001caar\376\001\000\000\013\001scheme#caar\376\003\000\000\002\376\003\000\000\002\376\001\000\000\004\001cadr\376\001\000\000"
"\013\001scheme#cadr\376\003\000\000\002\376\003\000\000\002\376\001\000\000\004\001cdar\376\001\000\000\013\001scheme#cdar\376\003\000\000\002\376\003\000\000\002\376\001\000\000\004\001cddr\376\001\000\000\013\001sche"
"me#cddr\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001caaar\376\001\000\000\014\001scheme#caaar\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001caadr\376\001\000\000\014\001scheme#"
"caadr\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001cadar\376\001\000\000\014\001scheme#cadar\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001caddr\376\001\000\000\014\001scheme#ca"
"ddr\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001cdaar\376\001\000\000\014\001scheme#cdaar\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001cdadr\376\001\000\000\014\001scheme#cdad"
"r\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001cddar\376\001\000\000\014\001scheme#cddar\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001cdddr\376\001\000\000\014\001scheme#cdddr\376"
"\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001caaaar\376\001\000\000\015\001scheme#caaaar\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001caaadr\376\001\000\000\015\001scheme#caaad"
"r\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001caadar\376\001\000\000\015\001scheme#caadar\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001caaddr\376\001\000\000\015\001scheme#caa"
"ddr\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001cadaar\376\001\000\000\015\001scheme#cadaar\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001cadadr\376\001\000\000\015\001scheme#c"
"adadr\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001caddar\376\001\000\000\015\001scheme#caddar\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001cadddr\376\001\000\000\015\001scheme"
"#cadddr\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001cdaaar\376\001\000\000\015\001scheme#cdaaar\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001cdaadr\376\001\000\000\015\001sche"
"me#cdaadr\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001cdadar\376\001\000\000\015\001scheme#cdadar\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001cdaddr\376\001\000\000\015\001sc"
"heme#cdaddr\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001cddaar\376\001\000\000\015\001scheme#cddaar\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001cddadr\376\001\000\000\015\001"
"scheme#cddadr\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001cdddar\376\001\000\000\015\001scheme#cdddar\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001cddddr\376\001\000\000"
"\015\001scheme#cddddr\376\003\000\000\002\376\003\000\000\002\376\001\000\000\010\001set-car!\376\001\000\000\017\001scheme#set-car!\376\003\000\000\002\376\003\000\000\002\376\001\000\000\010\001set-"
"cdr!\376\001\000\000\017\001scheme#set-cdr!\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001null\077\376\001\000\000\014\001scheme#null\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001"
"list\077\376\001\000\000\014\001scheme#list\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\004\001list\376\001\000\000\013\001scheme#list\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001leng"
"th\376\001\000\000\015\001scheme#length\376\003\000\000\002\376\003\000\000\002\376\001\000\000\011\001list-tail\376\001\000\000\020\001scheme#list-tail\376\003\000\000\002\376\003\000\000\002\376\001"
"\000\000\010\001list-ref\376\001\000\000\017\001scheme#list-ref\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001append\376\001\000\000\015\001scheme#append\376\003\000\000\002\376"
"\003\000\000\002\376\001\000\000\007\001reverse\376\001\000\000\016\001scheme#reverse\376\003\000\000\002\376\003\000\000\002\376\001\000\000\004\001memq\376\001\000\000\013\001scheme#memq\376\003\000\000\002\376"
"\003\000\000\002\376\001\000\000\004\001memv\376\001\000\000\013\001scheme#memv\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001member\376\001\000\000\015\001scheme#member\376\003\000\000\002\376\003\000"
"\000\002\376\001\000\000\004\001assq\376\001\000\000\013\001scheme#assq\376\003\000\000\002\376\003\000\000\002\376\001\000\000\004\001assv\376\001\000\000\013\001scheme#assv\376\003\000\000\002\376\003\000\000\002\376\001\000\000"
"\005\001assoc\376\001\000\000\014\001scheme#assoc\376\003\000\000\002\376\003\000\000\002\376\001\000\000\007\001symbol\077\376\001\000\000\016\001scheme#symbol\077\376\003\000\000\002\376\003\000\000\002\376\001"
"\000\000\016\001symbol->string\376\001\000\000\025\001scheme#symbol->string\376\003\000\000\002\376\003\000\000\002\376\001\000\000\016\001string->symbol\376\001\000\000\025"
"\001scheme#string->symbol\376\003\000\000\002\376\003\000\000\002\376\001\000\000\007\001number\077\376\001\000\000\016\001scheme#number\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\010"
"\001integer\077\376\001\000\000\017\001scheme#integer\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001exact\077\376\001\000\000\015\001scheme#exact\077\376\003\000\000\002\376\003\000\000"
"\002\376\001\000\000\005\001real\077\376\001\000\000\014\001scheme#real\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\010\001complex\077\376\001\000\000\017\001scheme#complex\077\376\003\000\000\002"
"\376\003\000\000\002\376\001\000\000\010\001inexact\077\376\001\000\000\017\001scheme#inexact\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\011\001rational\077\376\001\000\000\020\001scheme#ra"
"tional\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001zero\077\376\001\000\000\014\001scheme#zero\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\004\001odd\077\376\001\000\000\013\001scheme#o"
"dd\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001even\077\376\001\000\000\014\001scheme#even\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\011\001positive\077\376\001\000\000\020\001scheme#"
"positive\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\011\001negative\077\376\001\000\000\020\001scheme#negative\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\003\001max\376\001\000\000\012"
"\001scheme#max\376\003\000\000\002\376\003\000\000\002\376\001\000\000\003\001min\376\001\000\000\012\001scheme#min\376\003\000\000\002\376\003\000\000\002\376\001\000\000\001\001+\376\001\000\000\010\001scheme#+\376\003\000"
"\000\002\376\003\000\000\002\376\001\000\000\001\001-\376\001\000\000\010\001scheme#-\376\003\000\000\002\376\003\000\000\002\376\001\000\000\001\001\052\376\001\000\000\010\001scheme#\052\376\003\000\000\002\376\003\000\000\002\376\001\000\000\001\001/\376\001\000\000"
"\010\001scheme#/\376\003\000\000\002\376\003\000\000\002\376\001\000\000\001\001=\376\001\000\000\010\001scheme#=\376\003\000\000\002\376\003\000\000\002\376\001\000\000\001\001>\376\001\000\000\010\001scheme#>\376\003\000\000\002\376\003\000"
"\000\002\376\001\000\000\001\001<\376\001\000\000\010\001scheme#<\376\003\000\000\002\376\003\000\000\002\376\001\000\000\002\001>=\376\001\000\000\011\001scheme#>=\376\003\000\000\002\376\003\000\000\002\376\001\000\000\002\001<=\376\001\000\000\011\001"
"scheme#<=\376\003\000\000\002\376\003\000\000\002\376\001\000\000\010\001quotient\376\001\000\000\017\001scheme#quotient\376\003\000\000\002\376\003\000\000\002\376\001\000\000\011\001remainder\376"
"\001\000\000\020\001scheme#remainder\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001modulo\376\001\000\000\015\001scheme#modulo\376\003\000\000\002\376\003\000\000\002\376\001\000\000\003\001gc"
"d\376\001\000\000\012\001scheme#gcd\376\003\000\000\002\376\003\000\000\002\376\001\000\000\003\001lcm\376\001\000\000\012\001scheme#lcm\376\003\000\000\002\376\003\000\000\002\376\001\000\000\003\001abs\376\001\000\000\012\001sch"
"eme#abs\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001floor\376\001\000\000\014\001scheme#floor\376\003\000\000\002\376\003\000\000\002\376\001\000\000\007\001ceiling\376\001\000\000\016\001schem"
"e#ceiling\376\003\000\000\002\376\003\000\000\002\376\001\000\000\010\001truncate\376\001\000\000\017\001scheme#truncate\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001round\376\001\000\000\014"
"\001scheme#round\376\003\000\000\002\376\003\000\000\002\376\001\000\000\013\001rationalize\376\001\000\000\022\001scheme#rationalize\376\003\000\000\002\376\003\000\000\002\376\001\000\000\016\001"
"exact->inexact\376\001\000\000\025\001scheme#exact->inexact\376\003\000\000\002\376\003\000\000\002\376\001\000\000\016\001inexact->exact\376\001\000\000\025\001sch"
"eme#inexact->exact\376\003\000\000\002\376\003\000\000\002\376\001\000\000\003\001exp\376\001\000\000\012\001scheme#exp\376\003\000\000\002\376\003\000\000\002\376\001\000\000\003\001log\376\001\000\000\012\001sc"
"heme#log\376\003\000\000\002\376\003\000\000\002\376\001\000\000\004\001expt\376\001\000\000\013\001scheme#expt\376\003\000\000\002\376\003\000\000\002\376\001\000\000\004\001sqrt\376\001\000\000\013\001scheme#sq"
"rt\376\003\000\000\002\376\003\000\000\002\376\001\000\000\003\001sin\376\001\000\000\012\001scheme#sin\376\003\000\000\002\376\003\000\000\002\376\001\000\000\003\001cos\376\001\000\000\012\001scheme#cos\376\003\000\000\002\376\003\000"
"\000\002\376\001\000\000\003\001tan\376\001\000\000\012\001scheme#tan\376\003\000\000\002\376\003\000\000\002\376\001\000\000\004\001asin\376\001\000\000\013\001scheme#asin\376\003\000\000\002\376\003\000\000\002\376\001\000\000\004\001"
"acos\376\001\000\000\013\001scheme#acos\376\003\000\000\002\376\003\000\000\002\376\001\000\000\004\001atan\376\001\000\000\013\001scheme#atan\376\003\000\000\002\376\003\000\000\002\376\001\000\000\016\001number"
"->string\376\001\000\000\025\001scheme#number->string\376\003\000\000\002\376\003\000\000\002\376\001\000\000\016\001string->number\376\001\000\000\025\001scheme#st"
"ring->number\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001char\077\376\001\000\000\014\001scheme#char\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001char=\077\376\001\000\000\015\001s"
"cheme#char=\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001char>\077\376\001\000\000\015\001scheme#char>\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001char<\077\376\001\000\000\015"
"\001scheme#char<\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\007\001char>=\077\376\001\000\000\016\001scheme#char>=\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\007\001char<=\077"
"\376\001\000\000\016\001scheme#char<=\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\011\001char-ci=\077\376\001\000\000\020\001scheme#char-ci=\077\376\003\000\000\002\376\003\000\000\002\376\001\000"
"\000\011\001char-ci<\077\376\001\000\000\020\001scheme#char-ci<\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\011\001char-ci>\077\376\001\000\000\020\001scheme#char-ci>"
"\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\012\001char-ci>=\077\376\001\000\000\021\001scheme#char-ci>=\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\012\001char-ci<=\077\376\001\000\000"
"\021\001scheme#char-ci<=\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\020\001char-alphabetic\077\376\001\000\000\027\001scheme#char-alphabetic\077"
"\376\003\000\000\002\376\003\000\000\002\376\001\000\000\020\001char-whitespace\077\376\001\000\000\027\001scheme#char-whitespace\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\015\001cha"
"r-numeric\077\376\001\000\000\024\001scheme#char-numeric\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\020\001char-upper-case\077\376\001\000\000\027\001scheme"
"#char-upper-case\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\020\001char-lower-case\077\376\001\000\000\027\001scheme#char-lower-case\077\376\003"
"\000\000\002\376\003\000\000\002\376\001\000\000\013\001char-upcase\376\001\000\000\022\001scheme#char-upcase\376\003\000\000\002\376\003\000\000\002\376\001\000\000\015\001char-downcase\376\001"
"\000\000\024\001scheme#char-downcase\376\003\000\000\002\376\003\000\000\002\376\001\000\000\015\001char->integer\376\001\000\000\024\001scheme#char->integer\376"
"\003\000\000\002\376\003\000\000\002\376\001\000\000\015\001integer->char\376\001\000\000\024\001scheme#integer->char\376\003\000\000\002\376\003\000\000\002\376\001\000\000\007\001string\077\376\001\000"
"\000\016\001scheme#string\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\010\001string=\077\376\001\000\000\017\001scheme#string=\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\010\001st"
"ring>\077\376\001\000\000\017\001scheme#string>\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\010\001string<\077\376\001\000\000\017\001scheme#string<\077\376\003\000\000\002\376\003\000"
"\000\002\376\001\000\000\011\001string>=\077\376\001\000\000\020\001scheme#string>=\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\011\001string<=\077\376\001\000\000\020\001scheme#str"
"ing<=\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\013\001string-ci=\077\376\001\000\000\022\001scheme#string-ci=\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\013\001string-"
"ci<\077\376\001\000\000\022\001scheme#string-ci<\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\013\001string-ci>\077\376\001\000\000\022\001scheme#string-ci>\077\376"
"\003\000\000\002\376\003\000\000\002\376\001\000\000\014\001string-ci>=\077\376\001\000\000\023\001scheme#string-ci>=\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\014\001string-ci<=\077"
"\376\001\000\000\023\001scheme#string-ci<=\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\013\001make-string\376\001\000\000\022\001scheme#make-string\376\003\000\000"
"\002\376\003\000\000\002\376\001\000\000\015\001string-length\376\001\000\000\024\001scheme#string-length\376\003\000\000\002\376\003\000\000\002\376\001\000\000\012\001string-ref\376\001\000"
"\000\021\001scheme#string-ref\376\003\000\000\002\376\003\000\000\002\376\001\000\000\013\001string-set!\376\001\000\000\022\001scheme#string-set!\376\003\000\000\002\376\003\000\000"
"\002\376\001\000\000\015\001string-append\376\001\000\000\024\001scheme#string-append\376\003\000\000\002\376\003\000\000\002\376\001\000\000\013\001string-copy\376\001\000\000\022\001s"
"cheme#string-copy\376\003\000\000\002\376\003\000\000\002\376\001\000\000\014\001string->list\376\001\000\000\023\001scheme#string->list\376\003\000\000\002\376\003\000\000\002"
"\376\001\000\000\014\001list->string\376\001\000\000\023\001scheme#list->string\376\003\000\000\002\376\003\000\000\002\376\001\000\000\011\001substring\376\001\000\000\020\001scheme"
"#substring\376\003\000\000\002\376\003\000\000\002\376\001\000\000\014\001string-fill!\376\001\000\000\023\001scheme#string-fill!\376\003\000\000\002\376\003\000\000\002\376\001\000\000\007\001v"
"ector\077\376\001\000\000\016\001scheme#vector\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\013\001make-vector\376\001\000\000\022\001scheme#make-vector\376\003\000"
"\000\002\376\003\000\000\002\376\001\000\000\012\001vector-ref\376\001\000\000\021\001scheme#vector-ref\376\003\000\000\002\376\003\000\000\002\376\001\000\000\013\001vector-set!\376\001\000\000\022\001s"
"cheme#vector-set!\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001string\376\001\000\000\015\001scheme#string\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001vector"
"\376\001\000\000\015\001scheme#vector\376\003\000\000\002\376\003\000\000\002\376\001\000\000\015\001vector-length\376\001\000\000\024\001scheme#vector-length\376\003\000\000\002\376"
"\003\000\000\002\376\001\000\000\014\001vector->list\376\001\000\000\023\001scheme#vector->list\376\003\000\000\002\376\003\000\000\002\376\001\000\000\014\001list->vector\376\001\000\000\023"
"\001scheme#list->vector\376\003\000\000\002\376\003\000\000\002\376\001\000\000\014\001vector-fill!\376\001\000\000\023\001scheme#vector-fill!\376\003\000\000\002\376\003"
"\000\000\002\376\001\000\000\012\001procedure\077\376\001\000\000\021\001scheme#procedure\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\003\001map\376\001\000\000\012\001scheme#map\376\003\000"
"\000\002\376\003\000\000\002\376\001\000\000\010\001for-each\376\001\000\000\017\001scheme#for-each\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001apply\376\001\000\000\014\001scheme#appl"
"y\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001force\376\001\000\000\014\001scheme#force\376\003\000\000\002\376\003\000\000\002\376\001\000\000\036\001call-with-current-contin"
"uation\376\001\000\000%\001scheme#call-with-current-continuation\376\003\000\000\002\376\003\000\000\002\376\001\000\000\013\001input-port\077\376\001\000\000"
"\022\001scheme#input-port\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\014\001output-port\077\376\001\000\000\023\001scheme#output-port\077\376\003\000\000\002\376\003"
"\000\000\002\376\001\000\000\022\001current-input-port\376\001\000\000\031\001scheme#current-input-port\376\003\000\000\002\376\003\000\000\002\376\001\000\000\023\001curren"
"t-output-port\376\001\000\000\032\001scheme#current-output-port\376\003\000\000\002\376\003\000\000\002\376\001\000\000\024\001call-with-input-fil"
"e\376\001\000\000\033\001scheme#call-with-input-file\376\003\000\000\002\376\003\000\000\002\376\001\000\000\025\001call-with-output-file\376\001\000\000\034\001sch"
"eme#call-with-output-file\376\003\000\000\002\376\003\000\000\002\376\001\000\000\017\001open-input-file\376\001\000\000\026\001scheme#open-input-"
"file\376\003\000\000\002\376\003\000\000\002\376\001\000\000\020\001open-output-file\376\001\000\000\027\001scheme#open-output-file\376\003\000\000\002\376\003\000\000\002\376\001\000\000\020"
"\001close-input-port\376\001\000\000\027\001scheme#close-input-port\376\003\000\000\002\376\003\000\000\002\376\001\000\000\021\001close-output-port\376"
"\001\000\000\030\001scheme#close-output-port\376\003\000\000\002\376\003\000\000\002\376\001\000\000\004\001load\376\001\000\000\013\001scheme#load\376\003\000\000\002\376\003\000\000\002\376\001\000\000"
"\004\001read\376\001\000\000\013\001scheme#read\376\003\000\000\002\376\003\000\000\002\376\001\000\000\011\001read-char\376\001\000\000\020\001scheme#read-char\376\003\000\000\002\376\003\000\000\002"
"\376\001\000\000\011\001peek-char\376\001\000\000\020\001scheme#peek-char\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001write\376\001\000\000\014\001scheme#write\376\003\000\000"
"\002\376\003\000\000\002\376\001\000\000\007\001display\376\001\000\000\016\001scheme#display\376\003\000\000\002\376\003\000\000\002\376\001\000\000\012\001write-char\376\001\000\000\021\001scheme#wr"
"ite-char\376\003\000\000\002\376\003\000\000\002\376\001\000\000\007\001newline\376\001\000\000\016\001scheme#newline\376\003\000\000\002\376\003\000\000\002\376\001\000\000\013\001eof-object\077\376\001"
"\000\000\022\001scheme#eof-object\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\024\001with-input-from-file\376\001\000\000\033\001scheme#with-inpu"
"t-from-file\376\003\000\000\002\376\003\000\000\002\376\001\000\000\023\001with-output-to-file\376\001\000\000\032\001scheme#with-output-to-file\376\003"
"\000\000\002\376\003\000\000\002\376\001\000\000\013\001char-ready\077\376\001\000\000\022\001scheme#char-ready\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\011\001imag-part\376\001\000\000\020\001"
"scheme#imag-part\376\003\000\000\002\376\003\000\000\002\376\001\000\000\011\001real-part\376\001\000\000\020\001scheme#real-part\376\003\000\000\002\376\003\000\000\002\376\001\000\000\020\001m"
"ake-rectangular\376\001\000\000\027\001scheme#make-rectangular\376\003\000\000\002\376\003\000\000\002\376\001\000\000\012\001make-polar\376\001\000\000\021\001sche"
"me#make-polar\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001angle\376\001\000\000\014\001scheme#angle\376\003\000\000\002\376\003\000\000\002\376\001\000\000\011\001magnitude\376\001\000"
"\000\020\001scheme#magnitude\376\003\000\000\002\376\003\000\000\002\376\001\000\000\011\001numerator\376\001\000\000\020\001scheme#numerator\376\003\000\000\002\376\003\000\000\002\376\001\000\000"
"\013\001denominator\376\001\000\000\022\001scheme#denominator\376\003\000\000\002\376\003\000\000\002\376\001\000\000\031\001scheme-report-environment\376\001"
"\000\000 \001scheme#scheme-report-environment\376\003\000\000\002\376\003\000\000\002\376\001\000\000\020\001null-environment\376\001\000\000\027\001scheme"
"#null-environment\376\003\000\000\002\376\003\000\000\002\376\001\000\000\027\001interaction-environment\376\001\000\000\036\001scheme#interaction"
"-environment\376\377\016"));
lf[181]=C_h_intern(&lf[181],30, C_text("##sys#scheme-macro-environment"));
lf[182]=C_h_intern(&lf[182],33, C_text("chicken.module#module-environment"));
lf[183]=C_h_intern(&lf[183],18, C_text("module-environment"));
lf[184]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020undefined module"));
lf[185]=C_h_intern(&lf[185],11, C_text("environment"));
lf[186]=C_h_intern(&lf[186],14, C_text("chicken.syntax"));
lf[187]=C_h_intern(&lf[187],6, C_text("expand"));
lf[188]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001expand\376\001\000\000\025\001chicken.syntax#expand\376\003\000\000\002\376\003\000\000\002\376\001\000\000\017\001get-line-number"
"\376\001\000\000\036\001chicken.syntax#get-line-number\376\003\000\000\002\376\003\000\000\002\376\001\000\000\014\001strip-syntax\376\001\000\000\033\001chicken.sy"
"ntax#strip-syntax\376\003\000\000\002\376\003\000\000\002\376\001\000\000\014\001syntax-error\376\001\000\000\033\001chicken.syntax#syntax-error\376\003"
"\000\000\002\376\003\000\000\002\376\001\000\000\024\001er-macro-transformer\376\001\000\000#\001chicken.syntax#er-macro-transformer\376\003\000\000\002"
"\376\003\000\000\002\376\001\000\000\024\001ir-macro-transformer\376\001\000\000#\001chicken.syntax#ir-macro-transformer\376\377\016"));
lf[189]=C_h_intern(&lf[189],38, C_text("##sys#chicken.syntax-macro-environment"));
lf[190]=C_h_intern(&lf[190],12, C_text("chicken.base"));
lf[191]=C_h_intern(&lf[191],7, C_text("library"));
lf[192]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\004\001add1\376\001\000\000\021\001chicken.base#add1\376\003\000\000\002\376\003\000\000\002\376\001\000\000\011\001alist-ref\376\001\000\000\026\001chicke"
"n.base#alist-ref\376\003\000\000\002\376\003\000\000\002\376\001\000\000\014\001alist-update\376\001\000\000\031\001chicken.base#alist-update\376\003\000\000\002"
"\376\003\000\000\002\376\001\000\000\015\001alist-update!\376\001\000\000\032\001chicken.base#alist-update!\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001atom\077\376\001\000"
"\000\022\001chicken.base#atom\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\007\001bignum\077\376\001\000\000\024\001chicken.base#bignum\077\376\003\000\000\002\376\003\000\000\002"
"\376\001\000\000\007\001butlast\376\001\000\000\024\001chicken.base#butlast\376\003\000\000\002\376\003\000\000\002\376\001\000\000\007\001call/cc\376\001\000\000\024\001chicken.base"
"#call/cc\376\003\000\000\002\376\003\000\000\002\376\001\000\000\016\001case-sensitive\376\001\000\000\033\001chicken.base#case-sensitive\376\003\000\000\002\376\003\000\000"
"\002\376\001\000\000\011\001char-name\376\001\000\000\026\001chicken.base#char-name\376\003\000\000\002\376\003\000\000\002\376\001\000\000\004\001chop\376\001\000\000\021\001chicken.ba"
"se#chop\376\003\000\000\002\376\003\000\000\002\376\001\000\000\012\001complement\376\001\000\000\027\001chicken.base#complement\376\003\000\000\002\376\003\000\000\002\376\001\000\000\007\001co"
"mpose\376\001\000\000\024\001chicken.base#compose\376\003\000\000\002\376\003\000\000\002\376\001\000\000\010\001compress\376\001\000\000\025\001chicken.base#compre"
"ss\376\003\000\000\002\376\003\000\000\002\376\001\000\000\007\001conjoin\376\001\000\000\024\001chicken.base#conjoin\376\003\000\000\002\376\003\000\000\002\376\001\000\000\012\001constantly\376\001\000"
"\000\027\001chicken.base#constantly\376\003\000\000\002\376\003\000\000\002\376\001\000\000\010\001cplxnum\077\376\001\000\000\025\001chicken.base#cplxnum\077\376\003\000"
"\000\002\376\003\000\000\002\376\001\000\000\022\001current-error-port\376\001\000\000\037\001chicken.base#current-error-port\376\003\000\000\002\376\003\000\000\002\376\001"
"\000\000\007\001disjoin\376\001\000\000\024\001chicken.base#disjoin\376\003\000\000\002\376\003\000\000\002\376\001\000\000\004\001each\376\001\000\000\021\001chicken.base#each"
"\376\003\000\000\002\376\003\000\000\002\376\001\000\000\016\001emergency-exit\376\001\000\000\033\001chicken.base#emergency-exit\376\003\000\000\002\376\003\000\000\002\376\001\000\000\017\001e"
"nable-warnings\376\001\000\000\034\001chicken.base#enable-warnings\376\003\000\000\002\376\003\000\000\002\376\001\000\000\007\001equal=\077\376\001\000\000\024\001chi"
"cken.base#equal=\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001error\376\001\000\000\022\001chicken.base#error\376\003\000\000\002\376\003\000\000\002\376\001\000\000\016\001ex"
"act-integer\077\376\001\000\000\033\001chicken.base#exact-integer\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\026\001exact-integer-nth-r"
"oot\376\001\000\000#\001chicken.base#exact-integer-nth-root\376\003\000\000\002\376\003\000\000\002\376\001\000\000\022\001exact-integer-sqrt\376\001"
"\000\000\037\001chicken.base#exact-integer-sqrt\376\003\000\000\002\376\003\000\000\002\376\001\000\000\004\001exit\376\001\000\000\021\001chicken.base#exit\376\003"
"\000\000\002\376\003\000\000\002\376\001\000\000\014\001exit-handler\376\001\000\000\031\001chicken.base#exit-handler\376\003\000\000\002\376\003\000\000\002\376\001\000\000\007\001finite\077"
"\376\001\000\000\024\001chicken.base#finite\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\007\001fixnum\077\376\001\000\000\024\001chicken.base#fixnum\077\376\003\000\000\002"
"\376\003\000\000\002\376\001\000\000\007\001flatten\376\001\000\000\024\001chicken.base#flatten\376\003\000\000\002\376\003\000\000\002\376\001\000\000\004\001flip\376\001\000\000\021\001chicken.ba"
"se#flip\376\003\000\000\002\376\003\000\000\002\376\001\000\000\007\001flonum\077\376\001\000\000\024\001chicken.base#flonum\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\014\001flush-ou"
"tput\376\001\000\000\031\001chicken.base#flush-output\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001foldl\376\001\000\000\022\001chicken.base#foldl"
"\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001foldr\376\001\000\000\022\001chicken.base#foldr\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001gensym\376\001\000\000\023\001chicken"
".base#gensym\376\003\000\000\002\376\003\000\000\002\376\001\000\000\016\001get-call-chain\376\001\000\000\033\001chicken.base#get-call-chain\376\003\000\000\002"
"\376\003\000\000\002\376\001\000\000\021\001get-output-string\376\001\000\000\036\001chicken.base#get-output-string\376\003\000\000\002\376\003\000\000\002\376\001\000\000\022\001"
"getter-with-setter\376\001\000\000\037\001chicken.base#getter-with-setter\376\003\000\000\002\376\003\000\000\002\376\001\000\000\010\001identity\376"
"\001\000\000\025\001chicken.base#identity\376\003\000\000\002\376\003\000\000\002\376\001\000\000\025\001implicit-exit-handler\376\001\000\000\042\001chicken.bas"
"e#implicit-exit-handler\376\003\000\000\002\376\003\000\000\002\376\001\000\000\011\001infinite\077\376\001\000\000\026\001chicken.base#infinite\077\376\003\000\000"
"\002\376\003\000\000\002\376\001\000\000\020\001input-port-open\077\376\001\000\000\035\001chicken.base#input-port-open\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\013\001i"
"ntersperse\376\001\000\000\030\001chicken.base#intersperse\376\003\000\000\002\376\003\000\000\002\376\001\000\000\004\001join\376\001\000\000\021\001chicken.base#j"
"oin\376\003\000\000\002\376\003\000\000\002\376\001\000\000\015\001keyword-style\376\001\000\000\032\001chicken.base#keyword-style\376\003\000\000\002\376\003\000\000\002\376\001\000\000\010\001"
"list-of\077\376\001\000\000\025\001chicken.base#list-of\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\016\001make-parameter\376\001\000\000\033\001chicken.b"
"ase#make-parameter\376\003\000\000\002\376\003\000\000\002\376\001\000\000\014\001make-promise\376\001\000\000\031\001chicken.base#make-promise\376\003\000"
"\000\002\376\003\000\000\002\376\001\000\000\004\001nan\077\376\001\000\000\021\001chicken.base#nan\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001notice\376\001\000\000\023\001chicken.base"
"#notice\376\003\000\000\002\376\003\000\000\002\376\001\000\000\001\001o\376\001\000\000\016\001chicken.base#o\376\003\000\000\002\376\003\000\000\002\376\001\000\000\007\001on-exit\376\001\000\000\024\001chicken"
".base#on-exit\376\003\000\000\002\376\003\000\000\002\376\001\000\000\021\001open-input-string\376\001\000\000\036\001chicken.base#open-input-stri"
"ng\376\003\000\000\002\376\003\000\000\002\376\001\000\000\022\001open-output-string\376\001\000\000\037\001chicken.base#open-output-string\376\003\000\000\002\376\003"
"\000\000\002\376\001\000\000\021\001output-port-open\077\376\001\000\000\036\001chicken.base#output-port-open\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\024\001pa"
"rentheses-synonyms\376\001\000\000!\001chicken.base#parentheses-synonyms\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001port\077\376\001"
"\000\000\022\001chicken.base#port\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\014\001port-closed\077\376\001\000\000\031\001chicken.base#port-closed"
"\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\020\001print-call-chain\376\001\000\000\035\001chicken.base#print-call-chain\376\003\000\000\002\376\003\000\000\002\376\001"
"\000\000\005\001print\376\001\000\000\022\001chicken.base#print\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001print\052\376\001\000\000\023\001chicken.base#print\052"
"\376\003\000\000\002\376\003\000\000\002\376\001\000\000\025\001procedure-information\376\001\000\000\042\001chicken.base#procedure-information\376\003\000"
"\000\002\376\003\000\000\002\376\001\000\000\010\001promise\077\376\001\000\000\025\001chicken.base#promise\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\017\001quotient&modulo\376"
"\001\000\000\034\001chicken.base#quotient&modulo\376\003\000\000\002\376\003\000\000\002\376\001\000\000\022\001quotient&remainder\376\001\000\000\037\001chicken"
".base#quotient&remainder\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001rassoc\376\001\000\000\023\001chicken.base#rassoc\376\003\000\000\002\376\003\000\000"
"\002\376\001\000\000\007\001ratnum\077\376\001\000\000\024\001chicken.base#ratnum\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\016\001record-printer\376\001\000\000\033\001chic"
"ken.base#record-printer\376\003\000\000\002\376\003\000\000\002\376\001\000\000\023\001set-record-printer!\376\001\000\000 \001chicken.base#set"
"-record-printer!\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001setter\376\001\000\000\023\001chicken.base#setter\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001s"
"ignum\376\001\000\000\023\001chicken.base#signum\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001sleep\376\001\000\000\022\001chicken.base#sleep\376\003\000\000\002"
"\376\003\000\000\002\376\001\000\000\031\001string->uninterned-symbol\376\001\000\000&\001chicken.base#string->uninterned-symbol"
"\376\003\000\000\002\376\003\000\000\002\376\001\000\000\004\001sub1\376\001\000\000\021\001chicken.base#sub1\376\003\000\000\002\376\003\000\000\002\376\001\000\000\011\001subvector\376\001\000\000\026\001chicke"
"n.base#subvector\376\003\000\000\002\376\003\000\000\002\376\001\000\000\015\001symbol-append\376\001\000\000\032\001chicken.base#symbol-append\376\003\000"
"\000\002\376\003\000\000\002\376\001\000\000\015\001symbol-escape\376\001\000\000\032\001chicken.base#symbol-escape\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001tail\077\376"
"\001\000\000\022\001chicken.base#tail\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\014\001vector-copy!\376\001\000\000\031\001chicken.base#vector-cop"
"y!\376\003\000\000\002\376\003\000\000\002\376\001\000\000\015\001vector-resize\376\001\000\000\032\001chicken.base#vector-resize\376\003\000\000\002\376\003\000\000\002\376\001\000\000\004\001v"
"oid\376\001\000\000\021\001chicken.base#void\376\003\000\000\002\376\003\000\000\002\376\001\000\000\007\001warning\376\001\000\000\024\001chicken.base#warning\376\377\016"));
lf[193]=C_h_intern(&lf[193],36, C_text("##sys#chicken.base-macro-environment"));
lf[194]=C_h_intern(&lf[194],7, C_text("srfi-98"));
lf[195]=C_h_intern(&lf[195],5, C_text("posix"));
lf[196]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\030\001get-environment-variable\376\001\000\0000\001chicken.process-context#get-enviro"
"nment-variable\376\003\000\000\002\376\003\000\000\002\376\001\000\000\031\001get-environment-variables\376\001\000\0001\001chicken.process-con"
"text#get-environment-variables\376\377\016"));
lf[197]=C_h_intern(&lf[197],7, C_text("srfi-88"));
lf[198]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\010\001keyword\077\376\001\000\000\030\001chicken.keyword#keyword\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\017\001keyword->s"
"tring\376\003\000\000\002\376\001\000\000\037\001chicken.keyword#keyword->string\376\377\016\376\003\000\000\002\376\003\000\000\002\376\001\000\000\017\001string->keywor"
"d\376\003\000\000\002\376\001\000\000\037\001chicken.keyword#string->keyword\376\377\016\376\377\016"));
lf[199]=C_h_intern(&lf[199],7, C_text("srfi-55"));
lf[200]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\021\001require-extension\376\377\016"));
lf[201]=C_h_intern(&lf[201],7, C_text("srfi-39"));
lf[202]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\016\001make-parameter\376\001\000\000\033\001chicken.base#make-parameter\376\377\016"));
lf[203]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\014\001parameterize\376\377\016"));
lf[204]=C_h_intern(&lf[204],7, C_text("srfi-31"));
lf[205]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\003\001rec\376\377\016"));
lf[206]=C_h_intern(&lf[206],7, C_text("srfi-28"));
lf[207]=C_h_intern(&lf[207],6, C_text("extras"));
lf[208]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001format\376\001\000\000\025\001chicken.format#format\376\377\016"));
lf[209]=C_h_intern(&lf[209],7, C_text("srfi-26"));
lf[210]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\003\001cut\376\003\000\000\002\376\001\000\000\004\001cute\376\377\016"));
lf[211]=C_h_intern(&lf[211],7, C_text("srfi-23"));
lf[212]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001error\376\001\000\000\022\001chicken.base#error\376\377\016"));
lf[213]=C_h_intern(&lf[213],7, C_text("srfi-17"));
lf[214]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\004\001set!\376\377\016"));
lf[215]=C_h_intern(&lf[215],31, C_text("##sys#default-macro-environment"));
lf[216]=C_h_intern(&lf[216],7, C_text("srfi-16"));
lf[217]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\013\001case-lambda\376\377\016"));
lf[218]=C_h_intern(&lf[218],7, C_text("srfi-15"));
lf[219]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\011\001fluid-let\376\377\016"));
lf[220]=C_h_intern(&lf[220],7, C_text("srfi-12"));
lf[221]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\005\001abort\376\001\000\000\027\001chicken.condition#abort\376\003\000\000\002\376\003\000\000\002\376\001\000\000\012\001condition\077\376\001\000\000"
"\034\001chicken.condition#condition\077\376\003\000\000\002\376\003\000\000\002\376\001\000\000\023\001condition-predicate\376\001\000\000%\001chicken.c"
"ondition#condition-predicate\376\003\000\000\002\376\003\000\000\002\376\001\000\000\033\001condition-property-accessor\376\001\000\000-\001chi"
"cken.condition#condition-property-accessor\376\003\000\000\002\376\003\000\000\002\376\001\000\000\031\001current-exception-hand"
"ler\376\001\000\000+\001chicken.condition#current-exception-handler\376\003\000\000\002\376\003\000\000\002\376\001\000\000\030\001make-composi"
"te-condition\376\001\000\000\052\001chicken.condition#make-composite-condition\376\003\000\000\002\376\003\000\000\002\376\001\000\000\027\001make"
"-property-condition\376\001\000\000)\001chicken.condition#make-property-condition\376\003\000\000\002\376\003\000\000\002\376\001\000\000"
"\006\001signal\376\001\000\000\030\001chicken.condition#signal\376\003\000\000\002\376\003\000\000\002\376\001\000\000\026\001with-exception-handler\376\001\000\000"
"(\001chicken.condition#with-exception-handler\376\377\016"));
lf[222]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\021\001handle-exceptions\376\377\016"));
lf[223]=C_h_intern(&lf[223],41, C_text("##sys#chicken.condition-macro-environment"));
lf[224]=C_h_intern(&lf[224],7, C_text("srfi-11"));
lf[225]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\012\001let-values\376\003\000\000\002\376\001\000\000\013\001let\052-values\376\377\016"));
lf[226]=C_h_intern(&lf[226],7, C_text("srfi-10"));
lf[227]=C_h_intern(&lf[227],11, C_text("read-syntax"));
lf[228]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\022\001define-reader-ctor\376\001\000\000&\001chicken.read-syntax#define-reader-ctor\376\377"
"\016"));
lf[229]=C_h_intern(&lf[229],6, C_text("srfi-9"));
lf[230]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\022\001define-record-type\376\377\016"));
lf[231]=C_h_intern(&lf[231],6, C_text("srfi-8"));
lf[232]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\007\001receive\376\377\016"));
lf[233]=C_h_intern(&lf[233],6, C_text("srfi-6"));
lf[234]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\021\001get-output-string\376\001\000\000\036\001chicken.base#get-output-string\376\003\000\000\002\376\003\000\000\002\376"
"\001\000\000\021\001open-input-string\376\001\000\000\036\001chicken.base#open-input-string\376\003\000\000\002\376\003\000\000\002\376\001\000\000\022\001open-o"
"utput-string\376\001\000\000\036\001chicken.base#open-input-string\376\377\016"));
lf[235]=C_h_intern(&lf[235],6, C_text("srfi-2"));
lf[236]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\010\001and-let\052\376\377\016"));
lf[237]=C_h_intern(&lf[237],6, C_text("srfi-0"));
lf[238]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\013\001cond-expand\376\377\016"));
lf[239]=C_h_intern(&lf[239],12, C_text("chicken.type"));
lf[240]=C_h_intern(&lf[240],36, C_text("##sys#chicken.type-macro-environment"));
lf[241]=C_h_intern(&lf[241],14, C_text("chicken.module"));
lf[242]=C_h_intern(&lf[242],38, C_text("##sys#chicken.module-macro-environment"));
lf[243]=C_h_intern(&lf[243],23, C_text("chicken.internal.syntax"));
lf[244]=C_h_intern(&lf[244],15, C_text("chicken.keyword"));
lf[245]=C_h_intern(&lf[245],4, C_text("r5rs"));
lf[246]=C_h_intern(&lf[246],6, C_text("scheme"));
lf[247]=C_h_intern(&lf[247],9, C_text("r5rs-null"));
lf[248]=C_h_intern(&lf[248],9, C_text("r4rs-null"));
lf[249]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\014\001dynamic-wind\376\001\000\000\023\001scheme#dynamic-wind\376\003\000\000\002\376\003\000\000\002\376\001\000\000\004\001eval\376\001\000\000\013\001s"
"cheme#eval\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001values\376\001\000\000\015\001scheme#values\376\003\000\000\002\376\003\000\000\002\376\001\000\000\020\001call-with-val"
"ues\376\001\000\000\027\001scheme#call-with-values\376\377\016"));
lf[250]=C_h_intern(&lf[250],4, C_text("r4rs"));
lf[251]=C_h_intern(&lf[251],27, C_text("chicken.base#make-parameter"));
C_register_lf2(lf,252,create_ptable());{}
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4018,a[2]=t1,tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_chicken_2dsyntax_toplevel(2,av2);}}

#ifdef C_ENABLE_PTABLES
static C_PTABLE_ENTRY ptable[540] = {
{C_text("f12022:modules_2escm"),(void*)f12022},
{C_text("f12847:modules_2escm"),(void*)f12847},
{C_text("f12851:modules_2escm"),(void*)f12851},
{C_text("f_10002:modules_2escm"),(void*)f_10002},
{C_text("f_10008:modules_2escm"),(void*)f_10008},
{C_text("f_10049:modules_2escm"),(void*)f_10049},
{C_text("f_10081:modules_2escm"),(void*)f_10081},
{C_text("f_10107:modules_2escm"),(void*)f_10107},
{C_text("f_10128:modules_2escm"),(void*)f_10128},
{C_text("f_10132:modules_2escm"),(void*)f_10132},
{C_text("f_10158:modules_2escm"),(void*)f_10158},
{C_text("f_10172:modules_2escm"),(void*)f_10172},
{C_text("f_10214:modules_2escm"),(void*)f_10214},
{C_text("f_10230:modules_2escm"),(void*)f_10230},
{C_text("f_10238:modules_2escm"),(void*)f_10238},
{C_text("f_10245:modules_2escm"),(void*)f_10245},
{C_text("f_10255:modules_2escm"),(void*)f_10255},
{C_text("f_10277:modules_2escm"),(void*)f_10277},
{C_text("f_10279:modules_2escm"),(void*)f_10279},
{C_text("f_10318:modules_2escm"),(void*)f_10318},
{C_text("f_10339:modules_2escm"),(void*)f_10339},
{C_text("f_10351:modules_2escm"),(void*)f_10351},
{C_text("f_10374:modules_2escm"),(void*)f_10374},
{C_text("f_10377:modules_2escm"),(void*)f_10377},
{C_text("f_10388:modules_2escm"),(void*)f_10388},
{C_text("f_10394:modules_2escm"),(void*)f_10394},
{C_text("f_10426:modules_2escm"),(void*)f_10426},
{C_text("f_10429:modules_2escm"),(void*)f_10429},
{C_text("f_10440:modules_2escm"),(void*)f_10440},
{C_text("f_10456:modules_2escm"),(void*)f_10456},
{C_text("f_10460:modules_2escm"),(void*)f_10460},
{C_text("f_10467:modules_2escm"),(void*)f_10467},
{C_text("f_10480:modules_2escm"),(void*)f_10480},
{C_text("f_10507:modules_2escm"),(void*)f_10507},
{C_text("f_10520:modules_2escm"),(void*)f_10520},
{C_text("f_10524:modules_2escm"),(void*)f_10524},
{C_text("f_10528:modules_2escm"),(void*)f_10528},
{C_text("f_10532:modules_2escm"),(void*)f_10532},
{C_text("f_10546:modules_2escm"),(void*)f_10546},
{C_text("f_10552:modules_2escm"),(void*)f_10552},
{C_text("f_10554:modules_2escm"),(void*)f_10554},
{C_text("f_10579:modules_2escm"),(void*)f_10579},
{C_text("f_10588:modules_2escm"),(void*)f_10588},
{C_text("f_10598:modules_2escm"),(void*)f_10598},
{C_text("f_10613:modules_2escm"),(void*)f_10613},
{C_text("f_10616:modules_2escm"),(void*)f_10616},
{C_text("f_10619:modules_2escm"),(void*)f_10619},
{C_text("f_10622:modules_2escm"),(void*)f_10622},
{C_text("f_10625:modules_2escm"),(void*)f_10625},
{C_text("f_10628:modules_2escm"),(void*)f_10628},
{C_text("f_10631:modules_2escm"),(void*)f_10631},
{C_text("f_10634:modules_2escm"),(void*)f_10634},
{C_text("f_10637:modules_2escm"),(void*)f_10637},
{C_text("f_10640:modules_2escm"),(void*)f_10640},
{C_text("f_10643:modules_2escm"),(void*)f_10643},
{C_text("f_10646:modules_2escm"),(void*)f_10646},
{C_text("f_10649:modules_2escm"),(void*)f_10649},
{C_text("f_10652:modules_2escm"),(void*)f_10652},
{C_text("f_10655:modules_2escm"),(void*)f_10655},
{C_text("f_10658:modules_2escm"),(void*)f_10658},
{C_text("f_10661:modules_2escm"),(void*)f_10661},
{C_text("f_10664:modules_2escm"),(void*)f_10664},
{C_text("f_10667:modules_2escm"),(void*)f_10667},
{C_text("f_10670:modules_2escm"),(void*)f_10670},
{C_text("f_10673:modules_2escm"),(void*)f_10673},
{C_text("f_10676:modules_2escm"),(void*)f_10676},
{C_text("f_10679:modules_2escm"),(void*)f_10679},
{C_text("f_10682:modules_2escm"),(void*)f_10682},
{C_text("f_10685:modules_2escm"),(void*)f_10685},
{C_text("f_10688:modules_2escm"),(void*)f_10688},
{C_text("f_10691:modules_2escm"),(void*)f_10691},
{C_text("f_10694:modules_2escm"),(void*)f_10694},
{C_text("f_10697:modules_2escm"),(void*)f_10697},
{C_text("f_10699:modules_2escm"),(void*)f_10699},
{C_text("f_10706:modules_2escm"),(void*)f_10706},
{C_text("f_10735:modules_2escm"),(void*)f_10735},
{C_text("f_10738:modules_2escm"),(void*)f_10738},
{C_text("f_10745:modules_2escm"),(void*)f_10745},
{C_text("f_10757:modules_2escm"),(void*)f_10757},
{C_text("f_10792:modules_2escm"),(void*)f_10792},
{C_text("f_10799:modules_2escm"),(void*)f_10799},
{C_text("f_10811:modules_2escm"),(void*)f_10811},
{C_text("f_10846:modules_2escm"),(void*)f_10846},
{C_text("f_10853:modules_2escm"),(void*)f_10853},
{C_text("f_10865:modules_2escm"),(void*)f_10865},
{C_text("f_10900:modules_2escm"),(void*)f_10900},
{C_text("f_10907:modules_2escm"),(void*)f_10907},
{C_text("f_10919:modules_2escm"),(void*)f_10919},
{C_text("f_10954:modules_2escm"),(void*)f_10954},
{C_text("f_10961:modules_2escm"),(void*)f_10961},
{C_text("f_10973:modules_2escm"),(void*)f_10973},
{C_text("f_11008:modules_2escm"),(void*)f_11008},
{C_text("f_11015:modules_2escm"),(void*)f_11015},
{C_text("f_11027:modules_2escm"),(void*)f_11027},
{C_text("f_11062:modules_2escm"),(void*)f_11062},
{C_text("f_11069:modules_2escm"),(void*)f_11069},
{C_text("f_11081:modules_2escm"),(void*)f_11081},
{C_text("f_11116:modules_2escm"),(void*)f_11116},
{C_text("f_11123:modules_2escm"),(void*)f_11123},
{C_text("f_11135:modules_2escm"),(void*)f_11135},
{C_text("f_11170:modules_2escm"),(void*)f_11170},
{C_text("f_11177:modules_2escm"),(void*)f_11177},
{C_text("f_11189:modules_2escm"),(void*)f_11189},
{C_text("f_11224:modules_2escm"),(void*)f_11224},
{C_text("f_11231:modules_2escm"),(void*)f_11231},
{C_text("f_11243:modules_2escm"),(void*)f_11243},
{C_text("f_11278:modules_2escm"),(void*)f_11278},
{C_text("f_11285:modules_2escm"),(void*)f_11285},
{C_text("f_11297:modules_2escm"),(void*)f_11297},
{C_text("f_11332:modules_2escm"),(void*)f_11332},
{C_text("f_11339:modules_2escm"),(void*)f_11339},
{C_text("f_11351:modules_2escm"),(void*)f_11351},
{C_text("f_11386:modules_2escm"),(void*)f_11386},
{C_text("f_11393:modules_2escm"),(void*)f_11393},
{C_text("f_11405:modules_2escm"),(void*)f_11405},
{C_text("f_11440:modules_2escm"),(void*)f_11440},
{C_text("f_11444:modules_2escm"),(void*)f_11444},
{C_text("f_11448:modules_2escm"),(void*)f_11448},
{C_text("f_4018:modules_2escm"),(void*)f_4018},
{C_text("f_4021:modules_2escm"),(void*)f_4021},
{C_text("f_4024:modules_2escm"),(void*)f_4024},
{C_text("f_4027:modules_2escm"),(void*)f_4027},
{C_text("f_4518:modules_2escm"),(void*)f_4518},
{C_text("f_4524:modules_2escm"),(void*)f_4524},
{C_text("f_4537:modules_2escm"),(void*)f_4537},
{C_text("f_4551:modules_2escm"),(void*)f_4551},
{C_text("f_5309:modules_2escm"),(void*)f_5309},
{C_text("f_5313:modules_2escm"),(void*)f_5313},
{C_text("f_5328:modules_2escm"),(void*)f_5328},
{C_text("f_5418:modules_2escm"),(void*)f_5418},
{C_text("f_5427:modules_2escm"),(void*)f_5427},
{C_text("f_5563:modules_2escm"),(void*)f_5563},
{C_text("f_5587:modules_2escm"),(void*)f_5587},
{C_text("f_5603:modules_2escm"),(void*)f_5603},
{C_text("f_5605:modules_2escm"),(void*)f_5605},
{C_text("f_5609:modules_2escm"),(void*)f_5609},
{C_text("f_5614:modules_2escm"),(void*)f_5614},
{C_text("f_5618:modules_2escm"),(void*)f_5618},
{C_text("f_5622:modules_2escm"),(void*)f_5622},
{C_text("f_5625:modules_2escm"),(void*)f_5625},
{C_text("f_5631:modules_2escm"),(void*)f_5631},
{C_text("f_5637:modules_2escm"),(void*)f_5637},
{C_text("f_5641:modules_2escm"),(void*)f_5641},
{C_text("f_5644:modules_2escm"),(void*)f_5644},
{C_text("f_5668:modules_2escm"),(void*)f_5668},
{C_text("f_5672:modules_2escm"),(void*)f_5672},
{C_text("f_5674:modules_2escm"),(void*)f_5674},
{C_text("f_5708:modules_2escm"),(void*)f_5708},
{C_text("f_5716:modules_2escm"),(void*)f_5716},
{C_text("f_5718:modules_2escm"),(void*)f_5718},
{C_text("f_5726:modules_2escm"),(void*)f_5726},
{C_text("f_5753:modules_2escm"),(void*)f_5753},
{C_text("f_5755:modules_2escm"),(void*)f_5755},
{C_text("f_5809:modules_2escm"),(void*)f_5809},
{C_text("f_5816:modules_2escm"),(void*)f_5816},
{C_text("f_5819:modules_2escm"),(void*)f_5819},
{C_text("f_5822:modules_2escm"),(void*)f_5822},
{C_text("f_5825:modules_2escm"),(void*)f_5825},
{C_text("f_5831:modules_2escm"),(void*)f_5831},
{C_text("f_5844:modules_2escm"),(void*)f_5844},
{C_text("f_5856:modules_2escm"),(void*)f_5856},
{C_text("f_5860:modules_2escm"),(void*)f_5860},
{C_text("f_5862:modules_2escm"),(void*)f_5862},
{C_text("f_5878:modules_2escm"),(void*)f_5878},
{C_text("f_5879:modules_2escm"),(void*)f_5879},
{C_text("f_5887:modules_2escm"),(void*)f_5887},
{C_text("f_5901:modules_2escm"),(void*)f_5901},
{C_text("f_5904:modules_2escm"),(void*)f_5904},
{C_text("f_5911:modules_2escm"),(void*)f_5911},
{C_text("f_5915:modules_2escm"),(void*)f_5915},
{C_text("f_5921:modules_2escm"),(void*)f_5921},
{C_text("f_5949:modules_2escm"),(void*)f_5949},
{C_text("f_5951:modules_2escm"),(void*)f_5951},
{C_text("f_5954:modules_2escm"),(void*)f_5954},
{C_text("f_5958:modules_2escm"),(void*)f_5958},
{C_text("f_5974:modules_2escm"),(void*)f_5974},
{C_text("f_5981:modules_2escm"),(void*)f_5981},
{C_text("f_5995:modules_2escm"),(void*)f_5995},
{C_text("f_6005:modules_2escm"),(void*)f_6005},
{C_text("f_6008:modules_2escm"),(void*)f_6008},
{C_text("f_6011:modules_2escm"),(void*)f_6011},
{C_text("f_6017:modules_2escm"),(void*)f_6017},
{C_text("f_6020:modules_2escm"),(void*)f_6020},
{C_text("f_6023:modules_2escm"),(void*)f_6023},
{C_text("f_6056:modules_2escm"),(void*)f_6056},
{C_text("f_6060:modules_2escm"),(void*)f_6060},
{C_text("f_6067:modules_2escm"),(void*)f_6067},
{C_text("f_6071:modules_2escm"),(void*)f_6071},
{C_text("f_6084:modules_2escm"),(void*)f_6084},
{C_text("f_6094:modules_2escm"),(void*)f_6094},
{C_text("f_6097:modules_2escm"),(void*)f_6097},
{C_text("f_6103:modules_2escm"),(void*)f_6103},
{C_text("f_6106:modules_2escm"),(void*)f_6106},
{C_text("f_6112:modules_2escm"),(void*)f_6112},
{C_text("f_6146:modules_2escm"),(void*)f_6146},
{C_text("f_6150:modules_2escm"),(void*)f_6150},
{C_text("f_6165:modules_2escm"),(void*)f_6165},
{C_text("f_6176:modules_2escm"),(void*)f_6176},
{C_text("f_6182:modules_2escm"),(void*)f_6182},
{C_text("f_6199:modules_2escm"),(void*)f_6199},
{C_text("f_6206:modules_2escm"),(void*)f_6206},
{C_text("f_6213:modules_2escm"),(void*)f_6213},
{C_text("f_6258:modules_2escm"),(void*)f_6258},
{C_text("f_6364:modules_2escm"),(void*)f_6364},
{C_text("f_6372:modules_2escm"),(void*)f_6372},
{C_text("f_6376:modules_2escm"),(void*)f_6376},
{C_text("f_6387:modules_2escm"),(void*)f_6387},
{C_text("f_6414:modules_2escm"),(void*)f_6414},
{C_text("f_6437:modules_2escm"),(void*)f_6437},
{C_text("f_6451:modules_2escm"),(void*)f_6451},
{C_text("f_6476:modules_2escm"),(void*)f_6476},
{C_text("f_6491:modules_2escm"),(void*)f_6491},
{C_text("f_6509:modules_2escm"),(void*)f_6509},
{C_text("f_6520:modules_2escm"),(void*)f_6520},
{C_text("f_6539:modules_2escm"),(void*)f_6539},
{C_text("f_6552:modules_2escm"),(void*)f_6552},
{C_text("f_6562:modules_2escm"),(void*)f_6562},
{C_text("f_6568:modules_2escm"),(void*)f_6568},
{C_text("f_6572:modules_2escm"),(void*)f_6572},
{C_text("f_6575:modules_2escm"),(void*)f_6575},
{C_text("f_6586:modules_2escm"),(void*)f_6586},
{C_text("f_6614:modules_2escm"),(void*)f_6614},
{C_text("f_6628:modules_2escm"),(void*)f_6628},
{C_text("f_6633:modules_2escm"),(void*)f_6633},
{C_text("f_6643:modules_2escm"),(void*)f_6643},
{C_text("f_6661:modules_2escm"),(void*)f_6661},
{C_text("f_6683:modules_2escm"),(void*)f_6683},
{C_text("f_6691:modules_2escm"),(void*)f_6691},
{C_text("f_6721:modules_2escm"),(void*)f_6721},
{C_text("f_6756:modules_2escm"),(void*)f_6756},
{C_text("f_6760:modules_2escm"),(void*)f_6760},
{C_text("f_6764:modules_2escm"),(void*)f_6764},
{C_text("f_6768:modules_2escm"),(void*)f_6768},
{C_text("f_6772:modules_2escm"),(void*)f_6772},
{C_text("f_6776:modules_2escm"),(void*)f_6776},
{C_text("f_6808:modules_2escm"),(void*)f_6808},
{C_text("f_6812:modules_2escm"),(void*)f_6812},
{C_text("f_6824:modules_2escm"),(void*)f_6824},
{C_text("f_6856:modules_2escm"),(void*)f_6856},
{C_text("f_6866:modules_2escm"),(void*)f_6866},
{C_text("f_6881:modules_2escm"),(void*)f_6881},
{C_text("f_6905:modules_2escm"),(void*)f_6905},
{C_text("f_6921:modules_2escm"),(void*)f_6921},
{C_text("f_6923:modules_2escm"),(void*)f_6923},
{C_text("f_6948:modules_2escm"),(void*)f_6948},
{C_text("f_7010:modules_2escm"),(void*)f_7010},
{C_text("f_7012:modules_2escm"),(void*)f_7012},
{C_text("f_7037:modules_2escm"),(void*)f_7037},
{C_text("f_7067:modules_2escm"),(void*)f_7067},
{C_text("f_7088:modules_2escm"),(void*)f_7088},
{C_text("f_7116:modules_2escm"),(void*)f_7116},
{C_text("f_7124:modules_2escm"),(void*)f_7124},
{C_text("f_7154:modules_2escm"),(void*)f_7154},
{C_text("f_7179:modules_2escm"),(void*)f_7179},
{C_text("f_7189:modules_2escm"),(void*)f_7189},
{C_text("f_7205:modules_2escm"),(void*)f_7205},
{C_text("f_7215:modules_2escm"),(void*)f_7215},
{C_text("f_7221:modules_2escm"),(void*)f_7221},
{C_text("f_7222:modules_2escm"),(void*)f_7222},
{C_text("f_7234:modules_2escm"),(void*)f_7234},
{C_text("f_7247:modules_2escm"),(void*)f_7247},
{C_text("f_7248:modules_2escm"),(void*)f_7248},
{C_text("f_7260:modules_2escm"),(void*)f_7260},
{C_text("f_7273:modules_2escm"),(void*)f_7273},
{C_text("f_7276:modules_2escm"),(void*)f_7276},
{C_text("f_7292:modules_2escm"),(void*)f_7292},
{C_text("f_7296:modules_2escm"),(void*)f_7296},
{C_text("f_7300:modules_2escm"),(void*)f_7300},
{C_text("f_7302:modules_2escm"),(void*)f_7302},
{C_text("f_7312:modules_2escm"),(void*)f_7312},
{C_text("f_7325:modules_2escm"),(void*)f_7325},
{C_text("f_7335:modules_2escm"),(void*)f_7335},
{C_text("f_7359:modules_2escm"),(void*)f_7359},
{C_text("f_7363:modules_2escm"),(void*)f_7363},
{C_text("f_7376:modules_2escm"),(void*)f_7376},
{C_text("f_7410:modules_2escm"),(void*)f_7410},
{C_text("f_7420:modules_2escm"),(void*)f_7420},
{C_text("f_7435:modules_2escm"),(void*)f_7435},
{C_text("f_7450:modules_2escm"),(void*)f_7450},
{C_text("f_7457:modules_2escm"),(void*)f_7457},
{C_text("f_7463:modules_2escm"),(void*)f_7463},
{C_text("f_7479:modules_2escm"),(void*)f_7479},
{C_text("f_7483:modules_2escm"),(void*)f_7483},
{C_text("f_7487:modules_2escm"),(void*)f_7487},
{C_text("f_7500:modules_2escm"),(void*)f_7500},
{C_text("f_7522:modules_2escm"),(void*)f_7522},
{C_text("f_7524:modules_2escm"),(void*)f_7524},
{C_text("f_7549:modules_2escm"),(void*)f_7549},
{C_text("f_7564:modules_2escm"),(void*)f_7564},
{C_text("f_7579:modules_2escm"),(void*)f_7579},
{C_text("f_7590:modules_2escm"),(void*)f_7590},
{C_text("f_7592:modules_2escm"),(void*)f_7592},
{C_text("f_7657:modules_2escm"),(void*)f_7657},
{C_text("f_7689:modules_2escm"),(void*)f_7689},
{C_text("f_7698:modules_2escm"),(void*)f_7698},
{C_text("f_7701:modules_2escm"),(void*)f_7701},
{C_text("f_7704:modules_2escm"),(void*)f_7704},
{C_text("f_7705:modules_2escm"),(void*)f_7705},
{C_text("f_7719:modules_2escm"),(void*)f_7719},
{C_text("f_7723:modules_2escm"),(void*)f_7723},
{C_text("f_7726:modules_2escm"),(void*)f_7726},
{C_text("f_7729:modules_2escm"),(void*)f_7729},
{C_text("f_7732:modules_2escm"),(void*)f_7732},
{C_text("f_7740:modules_2escm"),(void*)f_7740},
{C_text("f_7747:modules_2escm"),(void*)f_7747},
{C_text("f_7756:modules_2escm"),(void*)f_7756},
{C_text("f_7759:modules_2escm"),(void*)f_7759},
{C_text("f_7766:modules_2escm"),(void*)f_7766},
{C_text("f_7769:modules_2escm"),(void*)f_7769},
{C_text("f_7770:modules_2escm"),(void*)f_7770},
{C_text("f_7774:modules_2escm"),(void*)f_7774},
{C_text("f_7777:modules_2escm"),(void*)f_7777},
{C_text("f_7789:modules_2escm"),(void*)f_7789},
{C_text("f_7799:modules_2escm"),(void*)f_7799},
{C_text("f_7821:modules_2escm"),(void*)f_7821},
{C_text("f_7822:modules_2escm"),(void*)f_7822},
{C_text("f_7826:modules_2escm"),(void*)f_7826},
{C_text("f_7834:modules_2escm"),(void*)f_7834},
{C_text("f_7844:modules_2escm"),(void*)f_7844},
{C_text("f_7858:modules_2escm"),(void*)f_7858},
{C_text("f_7861:modules_2escm"),(void*)f_7861},
{C_text("f_7864:modules_2escm"),(void*)f_7864},
{C_text("f_7892:modules_2escm"),(void*)f_7892},
{C_text("f_7899:modules_2escm"),(void*)f_7899},
{C_text("f_7905:modules_2escm"),(void*)f_7905},
{C_text("f_7908:modules_2escm"),(void*)f_7908},
{C_text("f_7909:modules_2escm"),(void*)f_7909},
{C_text("f_7913:modules_2escm"),(void*)f_7913},
{C_text("f_7931:modules_2escm"),(void*)f_7931},
{C_text("f_7937:modules_2escm"),(void*)f_7937},
{C_text("f_7940:modules_2escm"),(void*)f_7940},
{C_text("f_7943:modules_2escm"),(void*)f_7943},
{C_text("f_7954:modules_2escm"),(void*)f_7954},
{C_text("f_7958:modules_2escm"),(void*)f_7958},
{C_text("f_7962:modules_2escm"),(void*)f_7962},
{C_text("f_7966:modules_2escm"),(void*)f_7966},
{C_text("f_7972:modules_2escm"),(void*)f_7972},
{C_text("f_7982:modules_2escm"),(void*)f_7982},
{C_text("f_7997:modules_2escm"),(void*)f_7997},
{C_text("f_8001:modules_2escm"),(void*)f_8001},
{C_text("f_8003:modules_2escm"),(void*)f_8003},
{C_text("f_8013:modules_2escm"),(void*)f_8013},
{C_text("f_8028:modules_2escm"),(void*)f_8028},
{C_text("f_8040:modules_2escm"),(void*)f_8040},
{C_text("f_8050:modules_2escm"),(void*)f_8050},
{C_text("f_8065:modules_2escm"),(void*)f_8065},
{C_text("f_8071:modules_2escm"),(void*)f_8071},
{C_text("f_8106:modules_2escm"),(void*)f_8106},
{C_text("f_8115:modules_2escm"),(void*)f_8115},
{C_text("f_8126:modules_2escm"),(void*)f_8126},
{C_text("f_8130:modules_2escm"),(void*)f_8130},
{C_text("f_8133:modules_2escm"),(void*)f_8133},
{C_text("f_8141:modules_2escm"),(void*)f_8141},
{C_text("f_8162:modules_2escm"),(void*)f_8162},
{C_text("f_8166:modules_2escm"),(void*)f_8166},
{C_text("f_8174:modules_2escm"),(void*)f_8174},
{C_text("f_8178:modules_2escm"),(void*)f_8178},
{C_text("f_8194:modules_2escm"),(void*)f_8194},
{C_text("f_8198:modules_2escm"),(void*)f_8198},
{C_text("f_8212:modules_2escm"),(void*)f_8212},
{C_text("f_8245:modules_2escm"),(void*)f_8245},
{C_text("f_8247:modules_2escm"),(void*)f_8247},
{C_text("f_8260:modules_2escm"),(void*)f_8260},
{C_text("f_8269:modules_2escm"),(void*)f_8269},
{C_text("f_8282:modules_2escm"),(void*)f_8282},
{C_text("f_8318:modules_2escm"),(void*)f_8318},
{C_text("f_8325:modules_2escm"),(void*)f_8325},
{C_text("f_8329:modules_2escm"),(void*)f_8329},
{C_text("f_8332:modules_2escm"),(void*)f_8332},
{C_text("f_8337:modules_2escm"),(void*)f_8337},
{C_text("f_8341:modules_2escm"),(void*)f_8341},
{C_text("f_8344:modules_2escm"),(void*)f_8344},
{C_text("f_8347:modules_2escm"),(void*)f_8347},
{C_text("f_8350:modules_2escm"),(void*)f_8350},
{C_text("f_8354:modules_2escm"),(void*)f_8354},
{C_text("f_8358:modules_2escm"),(void*)f_8358},
{C_text("f_8362:modules_2escm"),(void*)f_8362},
{C_text("f_8366:modules_2escm"),(void*)f_8366},
{C_text("f_8369:modules_2escm"),(void*)f_8369},
{C_text("f_8372:modules_2escm"),(void*)f_8372},
{C_text("f_8375:modules_2escm"),(void*)f_8375},
{C_text("f_8378:modules_2escm"),(void*)f_8378},
{C_text("f_8393:modules_2escm"),(void*)f_8393},
{C_text("f_8399:modules_2escm"),(void*)f_8399},
{C_text("f_8403:modules_2escm"),(void*)f_8403},
{C_text("f_8406:modules_2escm"),(void*)f_8406},
{C_text("f_8409:modules_2escm"),(void*)f_8409},
{C_text("f_8412:modules_2escm"),(void*)f_8412},
{C_text("f_8415:modules_2escm"),(void*)f_8415},
{C_text("f_8418:modules_2escm"),(void*)f_8418},
{C_text("f_8421:modules_2escm"),(void*)f_8421},
{C_text("f_8424:modules_2escm"),(void*)f_8424},
{C_text("f_8430:modules_2escm"),(void*)f_8430},
{C_text("f_8434:modules_2escm"),(void*)f_8434},
{C_text("f_8442:modules_2escm"),(void*)f_8442},
{C_text("f_8448:modules_2escm"),(void*)f_8448},
{C_text("f_8453:modules_2escm"),(void*)f_8453},
{C_text("f_8457:modules_2escm"),(void*)f_8457},
{C_text("f_8462:modules_2escm"),(void*)f_8462},
{C_text("f_8469:modules_2escm"),(void*)f_8469},
{C_text("f_8473:modules_2escm"),(void*)f_8473},
{C_text("f_8476:modules_2escm"),(void*)f_8476},
{C_text("f_8480:modules_2escm"),(void*)f_8480},
{C_text("f_8483:modules_2escm"),(void*)f_8483},
{C_text("f_8491:modules_2escm"),(void*)f_8491},
{C_text("f_8495:modules_2escm"),(void*)f_8495},
{C_text("f_8498:modules_2escm"),(void*)f_8498},
{C_text("f_8501:modules_2escm"),(void*)f_8501},
{C_text("f_8504:modules_2escm"),(void*)f_8504},
{C_text("f_8506:modules_2escm"),(void*)f_8506},
{C_text("f_8514:modules_2escm"),(void*)f_8514},
{C_text("f_8518:modules_2escm"),(void*)f_8518},
{C_text("f_8520:modules_2escm"),(void*)f_8520},
{C_text("f_8533:modules_2escm"),(void*)f_8533},
{C_text("f_8540:modules_2escm"),(void*)f_8540},
{C_text("f_8567:modules_2escm"),(void*)f_8567},
{C_text("f_8570:modules_2escm"),(void*)f_8570},
{C_text("f_8574:modules_2escm"),(void*)f_8574},
{C_text("f_8577:modules_2escm"),(void*)f_8577},
{C_text("f_8618:modules_2escm"),(void*)f_8618},
{C_text("f_8632:modules_2escm"),(void*)f_8632},
{C_text("f_8650:modules_2escm"),(void*)f_8650},
{C_text("f_8653:modules_2escm"),(void*)f_8653},
{C_text("f_8658:modules_2escm"),(void*)f_8658},
{C_text("f_8668:modules_2escm"),(void*)f_8668},
{C_text("f_8672:modules_2escm"),(void*)f_8672},
{C_text("f_8677:modules_2escm"),(void*)f_8677},
{C_text("f_8685:modules_2escm"),(void*)f_8685},
{C_text("f_8695:modules_2escm"),(void*)f_8695},
{C_text("f_8708:modules_2escm"),(void*)f_8708},
{C_text("f_8718:modules_2escm"),(void*)f_8718},
{C_text("f_8736:modules_2escm"),(void*)f_8736},
{C_text("f_8758:modules_2escm"),(void*)f_8758},
{C_text("f_8800:modules_2escm"),(void*)f_8800},
{C_text("f_8803:modules_2escm"),(void*)f_8803},
{C_text("f_8808:modules_2escm"),(void*)f_8808},
{C_text("f_8818:modules_2escm"),(void*)f_8818},
{C_text("f_8822:modules_2escm"),(void*)f_8822},
{C_text("f_8827:modules_2escm"),(void*)f_8827},
{C_text("f_8839:modules_2escm"),(void*)f_8839},
{C_text("f_8847:modules_2escm"),(void*)f_8847},
{C_text("f_8857:modules_2escm"),(void*)f_8857},
{C_text("f_8870:modules_2escm"),(void*)f_8870},
{C_text("f_8880:modules_2escm"),(void*)f_8880},
{C_text("f_8898:modules_2escm"),(void*)f_8898},
{C_text("f_8910:modules_2escm"),(void*)f_8910},
{C_text("f_8939:modules_2escm"),(void*)f_8939},
{C_text("f_8951:modules_2escm"),(void*)f_8951},
{C_text("f_8983:modules_2escm"),(void*)f_8983},
{C_text("f_8986:modules_2escm"),(void*)f_8986},
{C_text("f_8991:modules_2escm"),(void*)f_8991},
{C_text("f_9001:modules_2escm"),(void*)f_9001},
{C_text("f_9005:modules_2escm"),(void*)f_9005},
{C_text("f_9010:modules_2escm"),(void*)f_9010},
{C_text("f_9022:modules_2escm"),(void*)f_9022},
{C_text("f_9030:modules_2escm"),(void*)f_9030},
{C_text("f_9043:modules_2escm"),(void*)f_9043},
{C_text("f_9049:modules_2escm"),(void*)f_9049},
{C_text("f_9062:modules_2escm"),(void*)f_9062},
{C_text("f_9072:modules_2escm"),(void*)f_9072},
{C_text("f_9085:modules_2escm"),(void*)f_9085},
{C_text("f_9124:modules_2escm"),(void*)f_9124},
{C_text("f_9140:modules_2escm"),(void*)f_9140},
{C_text("f_9177:modules_2escm"),(void*)f_9177},
{C_text("f_9193:modules_2escm"),(void*)f_9193},
{C_text("f_9233:modules_2escm"),(void*)f_9233},
{C_text("f_9236:modules_2escm"),(void*)f_9236},
{C_text("f_9241:modules_2escm"),(void*)f_9241},
{C_text("f_9251:modules_2escm"),(void*)f_9251},
{C_text("f_9255:modules_2escm"),(void*)f_9255},
{C_text("f_9257:modules_2escm"),(void*)f_9257},
{C_text("f_9265:modules_2escm"),(void*)f_9265},
{C_text("f_9271:modules_2escm"),(void*)f_9271},
{C_text("f_9275:modules_2escm"),(void*)f_9275},
{C_text("f_9279:modules_2escm"),(void*)f_9279},
{C_text("f_9300:modules_2escm"),(void*)f_9300},
{C_text("f_9310:modules_2escm"),(void*)f_9310},
{C_text("f_9312:modules_2escm"),(void*)f_9312},
{C_text("f_9337:modules_2escm"),(void*)f_9337},
{C_text("f_9346:modules_2escm"),(void*)f_9346},
{C_text("f_9371:modules_2escm"),(void*)f_9371},
{C_text("f_9389:modules_2escm"),(void*)f_9389},
{C_text("f_9395:modules_2escm"),(void*)f_9395},
{C_text("f_9399:modules_2escm"),(void*)f_9399},
{C_text("f_9400:modules_2escm"),(void*)f_9400},
{C_text("f_9406:modules_2escm"),(void*)f_9406},
{C_text("f_9412:modules_2escm"),(void*)f_9412},
{C_text("f_9434:modules_2escm"),(void*)f_9434},
{C_text("f_9436:modules_2escm"),(void*)f_9436},
{C_text("f_9446:modules_2escm"),(void*)f_9446},
{C_text("f_9459:modules_2escm"),(void*)f_9459},
{C_text("f_9463:modules_2escm"),(void*)f_9463},
{C_text("f_9466:modules_2escm"),(void*)f_9466},
{C_text("f_9476:modules_2escm"),(void*)f_9476},
{C_text("f_9514:modules_2escm"),(void*)f_9514},
{C_text("f_9520:modules_2escm"),(void*)f_9520},
{C_text("f_9521:modules_2escm"),(void*)f_9521},
{C_text("f_9557:modules_2escm"),(void*)f_9557},
{C_text("f_9563:modules_2escm"),(void*)f_9563},
{C_text("f_9566:modules_2escm"),(void*)f_9566},
{C_text("f_9569:modules_2escm"),(void*)f_9569},
{C_text("f_9576:modules_2escm"),(void*)f_9576},
{C_text("f_9580:modules_2escm"),(void*)f_9580},
{C_text("f_9584:modules_2escm"),(void*)f_9584},
{C_text("f_9588:modules_2escm"),(void*)f_9588},
{C_text("f_9591:modules_2escm"),(void*)f_9591},
{C_text("f_9597:modules_2escm"),(void*)f_9597},
{C_text("f_9600:modules_2escm"),(void*)f_9600},
{C_text("f_9607:modules_2escm"),(void*)f_9607},
{C_text("f_9617:modules_2escm"),(void*)f_9617},
{C_text("f_9624:modules_2escm"),(void*)f_9624},
{C_text("f_9635:modules_2escm"),(void*)f_9635},
{C_text("f_9642:modules_2escm"),(void*)f_9642},
{C_text("f_9644:modules_2escm"),(void*)f_9644},
{C_text("f_9678:modules_2escm"),(void*)f_9678},
{C_text("f_9714:modules_2escm"),(void*)f_9714},
{C_text("f_9725:modules_2escm"),(void*)f_9725},
{C_text("f_9739:modules_2escm"),(void*)f_9739},
{C_text("f_9746:modules_2escm"),(void*)f_9746},
{C_text("f_9748:modules_2escm"),(void*)f_9748},
{C_text("f_9782:modules_2escm"),(void*)f_9782},
{C_text("f_9822:modules_2escm"),(void*)f_9822},
{C_text("f_9832:modules_2escm"),(void*)f_9832},
{C_text("f_9845:modules_2escm"),(void*)f_9845},
{C_text("f_9855:modules_2escm"),(void*)f_9855},
{C_text("f_9876:modules_2escm"),(void*)f_9876},
{C_text("f_9891:modules_2escm"),(void*)f_9891},
{C_text("f_9901:modules_2escm"),(void*)f_9901},
{C_text("f_9909:modules_2escm"),(void*)f_9909},
{C_text("f_9919:modules_2escm"),(void*)f_9919},
{C_text("f_9922:modules_2escm"),(void*)f_9922},
{C_text("f_9926:modules_2escm"),(void*)f_9926},
{C_text("f_9930:modules_2escm"),(void*)f_9930},
{C_text("f_9937:modules_2escm"),(void*)f_9937},
{C_text("f_9956:modules_2escm"),(void*)f_9956},
{C_text("f_9971:modules_2escm"),(void*)f_9971},
{C_text("f_9997:modules_2escm"),(void*)f_9997},
{C_text("f_9999:modules_2escm"),(void*)f_9999},
{C_text("toplevel:modules_2escm"),(void*)C_modules_toplevel},
{NULL,NULL}};
#endif

static C_PTABLE_ENTRY *create_ptable(void){
#ifdef C_ENABLE_PTABLES
return ptable;
#else
return NULL;
#endif
}

/*
S|applied compiler syntax:
S|  scheme#for-each		15
S|  chicken.base#foldl		3
S|  scheme#map		33
S|  chicken.base#foldr		3
o|eliminated procedure checks: 442 
o|specializations:
o|  1 (scheme#cddr (pair * pair))
o|  1 (scheme#number->string *)
o|  2 (scheme#string-append string string)
o|  1 (scheme#= fixnum fixnum)
o|  2 (scheme#cdar (pair pair *))
o|  1 (scheme#caar (pair pair *))
o|  1 (null (not null))
o|  1 (scheme#eqv? * *)
o|  10 (##sys#check-list (or pair list) *)
o|  67 (scheme#cdr pair)
o|  35 (scheme#car pair)
(o e)|safe calls: 1119 
(o e)|dropped branches: 1 
(o e)|assignments to immediate values: 1 
o|safe globals: (posv posq make-list iota find-tail find length+ lset=/eq? lset<=/eq? list-tabulate lset-intersection/eq? lset-union/eq? lset-difference/eq? lset-adjoin/eq? list-index last unzip1 remove filter-map filter alist-cons delete-duplicates fifth fourth third second first delete concatenate cons* any every append-map split-at drop take span partition) 
o|removed side-effect free assignment to unused variable: partition 
o|removed side-effect free assignment to unused variable: span 
o|removed side-effect free assignment to unused variable: drop 
o|removed side-effect free assignment to unused variable: split-at 
o|removed side-effect free assignment to unused variable: append-map 
o|inlining procedure: k4409 
o|inlining procedure: k4409 
o|inlining procedure: k4440 
o|inlining procedure: k4440 
o|removed side-effect free assignment to unused variable: cons* 
o|removed side-effect free assignment to unused variable: concatenate 
o|inlining procedure: k4526 
o|inlining procedure: k4526 
o|removed side-effect free assignment to unused variable: first 
o|removed side-effect free assignment to unused variable: second 
o|removed side-effect free assignment to unused variable: third 
o|removed side-effect free assignment to unused variable: fourth 
o|removed side-effect free assignment to unused variable: fifth 
o|removed side-effect free assignment to unused variable: delete-duplicates 
o|removed side-effect free assignment to unused variable: alist-cons 
o|inlining procedure: k4657 
o|inlining procedure: k4657 
o|inlining procedure: k4649 
o|inlining procedure: k4649 
o|removed side-effect free assignment to unused variable: filter-map 
o|removed side-effect free assignment to unused variable: remove 
o|removed side-effect free assignment to unused variable: unzip1 
o|removed side-effect free assignment to unused variable: last 
o|removed side-effect free assignment to unused variable: list-index 
o|removed side-effect free assignment to unused variable: lset-adjoin/eq? 
o|removed side-effect free assignment to unused variable: lset-difference/eq? 
o|removed side-effect free assignment to unused variable: lset-union/eq? 
o|removed side-effect free assignment to unused variable: lset-intersection/eq? 
o|inlining procedure: k5048 
o|inlining procedure: k5048 
o|removed side-effect free assignment to unused variable: lset<=/eq? 
o|removed side-effect free assignment to unused variable: lset=/eq? 
o|removed side-effect free assignment to unused variable: length+ 
o|removed side-effect free assignment to unused variable: find 
o|removed side-effect free assignment to unused variable: find-tail 
o|removed side-effect free assignment to unused variable: iota 
o|removed side-effect free assignment to unused variable: make-list 
o|removed side-effect free assignment to unused variable: posq 
o|removed side-effect free assignment to unused variable: posv 
o|removed side-effect free assignment to unused variable: module? 
o|contracted procedure: "(modules.scm:123) %make-module" 
o|inlining procedure: k5676 
o|contracted procedure: "(modules.scm:132) g905914" 
o|inlining procedure: k5676 
o|inlining procedure: k5731 
o|inlining procedure: k5731 
o|inlining procedure: k5723 
o|inlining procedure: k5723 
o|inlining procedure: k5772 
o|inlining procedure: k5772 
o|inlining procedure: k5823 
o|inlining procedure: k5823 
o|inlining procedure: k5884 
o|inlining procedure: k5884 
o|inlining procedure: k5867 
o|inlining procedure: k5923 
o|inlining procedure: k5923 
o|inlining procedure: k5867 
o|inlining procedure: k5959 
o|contracted procedure: "(modules.scm:186) set-module-meta-expressions!" 
o|inlining procedure: k5959 
o|inlining procedure: k5985 
o|inlining procedure: k5985 
o|inlining procedure: k5997 
o|inlining procedure: k5997 
o|inlining procedure: k6086 
o|inlining procedure: k6086 
o|inlining procedure: k6167 
o|inlining procedure: k6167 
o|merged explicitly consed rest parameter: ses*1169 
o|inlining procedure: k6588 
o|inlining procedure: k6588 
o|inlining procedure: k6608 
o|inlining procedure: k6635 
o|inlining procedure: k6635 
o|inlining procedure: k6608 
o|inlining procedure: k6663 
o|inlining procedure: k6663 
o|inlining procedure: k6810 
o|inlining procedure: k6810 
o|inlining procedure: k6826 
o|inlining procedure: k6826 
o|inlining procedure: k6889 
o|inlining procedure: k6889 
o|inlining procedure: k6925 
o|inlining procedure: k6925 
o|inlining procedure: k7014 
o|contracted procedure: "(modules.scm:335) g12451254" 
o|inlining procedure: k6966 
o|inlining procedure: k6966 
o|inlining procedure: k7014 
o|contracted procedure: "(modules.scm:328) g12331234" 
o|inlining procedure: k7160 
o|contracted procedure: "(modules.scm:378) find-reexport1339" 
o|inlining procedure: k7135 
o|inlining procedure: k7135 
o|inlining procedure: k7160 
o|consed rest parameter at call site: "(modules.scm:395) merge-se" 1 
o|inlining procedure: k7239 
o|consed rest parameter at call site: "(modules.scm:395) merge-se" 1 
o|inlining procedure: k7239 
o|consed rest parameter at call site: "(modules.scm:395) merge-se" 1 
o|consed rest parameter at call site: "(modules.scm:399) merge-se" 1 
o|inlining procedure: k7265 
o|consed rest parameter at call site: "(modules.scm:399) merge-se" 1 
o|inlining procedure: k7265 
o|consed rest parameter at call site: "(modules.scm:399) merge-se" 1 
o|consed rest parameter at call site: "(modules.scm:403) merge-se" 1 
o|inlining procedure: k7304 
o|inlining procedure: k7304 
o|inlining procedure: k7327 
o|inlining procedure: k7327 
o|consed rest parameter at call site: "(modules.scm:388) merge-se" 1 
o|inlining procedure: k7378 
o|contracted procedure: "(modules.scm:382) g13801389" 
o|inlining procedure: k7378 
o|inlining procedure: k7412 
o|inlining procedure: k7412 
o|consed rest parameter at call site: "(modules.scm:424) merge-se" 1 
o|inlining procedure: k7502 
o|inlining procedure: k7502 
o|inlining procedure: k7526 
o|inlining procedure: k7526 
o|inlining procedure: k7594 
o|inlining procedure: k7594 
o|inlining procedure: k7606 
o|inlining procedure: k7621 
o|inlining procedure: k7621 
o|inlining procedure: k7606 
o|inlining procedure: k7711 
o|inlining procedure: k7711 
o|inlining procedure: k7748 
o|inlining procedure: k7748 
o|inlining procedure: k7791 
o|inlining procedure: k7791 
o|substituted constant variable: a7811 
o|contracted procedure: "(modules.scm:521) g16591660" 
o|inlining procedure: k7836 
o|inlining procedure: k7836 
o|consed rest parameter at call site: "(modules.scm:550) merge-se" 1 
o|consed rest parameter at call site: "(modules.scm:568) merge-se" 1 
o|consed rest parameter at call site: "(modules.scm:565) merge-se" 1 
o|contracted procedure: "(modules.scm:561) set-module-vexports!" 
o|inlining procedure: k7974 
o|inlining procedure: k7974 
o|consed rest parameter at call site: "(modules.scm:544) merge-se" 1 
o|inlining procedure: k8005 
o|contracted procedure: "(modules.scm:539) g17111720" 
o|inlining procedure: k7870 
o|inlining procedure: k7870 
o|inlining procedure: k8005 
o|contracted procedure: "(modules.scm:543) module-indirect-exports" 
o|removed side-effect free assignment to unused variable: indirect?1120 
o|inlining procedure: k6377 
o|inlining procedure: k6377 
o|inlining procedure: k6389 
o|inlining procedure: k6389 
o|inlining procedure: k6416 
o|inlining procedure: k6416 
o|inlining procedure: k6474 
o|inlining procedure: k6474 
o|inlining procedure: k6448 
o|inlining procedure: k6448 
o|inlining procedure: k6493 
o|inlining procedure: k6493 
o|inlining procedure: k8042 
o|inlining procedure: k8042 
o|inlining procedure: k8073 
o|inlining procedure: k8073 
o|inlining procedure: k8113 
o|inlining procedure: k8113 
o|inlining procedure: k8154 
o|inlining procedure: k8154 
o|inlining procedure: k8182 
o|inlining procedure: k8182 
o|consed rest parameter at call site: "(modules.scm:462) merge-se" 1 
o|inlining procedure: k8249 
o|inlining procedure: k8249 
o|inlining procedure: k8284 
o|contracted procedure: "(modules.scm:458) g15631572" 
o|inlining procedure: k8284 
o|substituted constant variable: saved177717781797 
o|substituted constant variable: saved177917801798 
o|inlining procedure: k8435 
o|inlining procedure: k8435 
o|substituted constant variable: a8474 
o|inlining procedure: k8484 
o|inlining procedure: k8484 
o|inlining procedure: k8522 
o|inlining procedure: k8522 
o|inlining procedure: k8541 
o|inlining procedure: k8541 
o|inlining procedure: k8578 
o|inlining procedure: k8578 
o|inlining procedure: k8620 
o|inlining procedure: k8620 
o|inlining procedure: k8645 
o|inlining procedure: k8679 
o|inlining procedure: k8710 
o|inlining procedure: k8710 
o|inlining procedure: k8679 
o|inlining procedure: k8755 
o|inlining procedure: k8755 
o|inlining procedure: k8645 
o|inlining procedure: k8829 
o|inlining procedure: k8841 
o|inlining procedure: k8872 
o|inlining procedure: k8872 
o|inlining procedure: k8841 
o|inlining procedure: k8829 
o|inlining procedure: k8978 
o|inlining procedure: k9012 
o|inlining procedure: k9024 
o|inlining procedure: k9064 
o|inlining procedure: k9064 
o|inlining procedure: k9087 
o|inlining procedure: k9087 
o|inlining procedure: k9024 
o|inlining procedure: k9012 
o|inlining procedure: k8978 
o|inlining procedure: k9314 
o|inlining procedure: k9314 
o|inlining procedure: k9348 
o|inlining procedure: k9348 
o|inlining procedure: k9414 
o|inlining procedure: k9414 
o|inlining procedure: k9438 
o|inlining procedure: k9438 
o|inlining procedure: k9481 
o|inlining procedure: k9491 
o|inlining procedure: k9491 
o|inlining procedure: k9481 
o|inlining procedure: k9526 
o|inlining procedure: k9526 
o|consed rest parameter at call site: "(modules.scm:774) merge-se" 1 
o|inlining procedure: k9646 
o|inlining procedure: k9646 
o|inlining procedure: k9680 
o|inlining procedure: k9680 
o|inlining procedure: k9750 
o|inlining procedure: k9750 
o|inlining procedure: k9784 
o|inlining procedure: k9784 
o|inlining procedure: k9824 
o|inlining procedure: k9824 
o|inlining procedure: k9847 
o|inlining procedure: k9847 
o|inlining procedure: k9867 
o|contracted procedure: "(modules.scm:730) set-module-meta-import-forms!" 
o|inlining procedure: k9867 
o|contracted procedure: "(modules.scm:733) set-module-import-forms!" 
o|contracted procedure: "(modules.scm:792) register-undefined" 
o|inlining procedure: k6194 
o|inlining procedure: k6208 
o|inlining procedure: k6208 
o|inlining procedure: k6251 
o|inlining procedure: k6251 
o|inlining procedure: k6194 
o|inlining procedure: k9927 
o|inlining procedure: k9927 
o|inlining procedure: k9951 
o|inlining procedure: k9951 
o|inlining procedure: k9979 
o|inlining procedure: k9979 
o|inlining procedure: k9968 
o|inlining procedure: k9968 
o|contracted procedure: "(modules.scm:796) g25002501" 
o|merged explicitly consed rest parameter: args2521 
o|inlining procedure: k10015 
o|inlining procedure: k10015 
o|consed rest parameter at call site: "(modules.scm:811) err2519" 1 
o|contracted procedure: "(modules.scm:810) g25262527" 
o|inlining procedure: k10021 
o|inlining procedure: k10021 
o|inlining procedure: k10036 
o|consed rest parameter at call site: "(modules.scm:815) err2519" 1 
o|inlining procedure: k10036 
o|inlining procedure: k10051 
o|inlining procedure: k10051 
o|consed rest parameter at call site: "(modules.scm:820) err2519" 1 
o|inlining procedure: k10069 
o|inlining procedure: k10069 
o|consed rest parameter at call site: "(modules.scm:825) err2519" 1 
o|inlining procedure: k10093 
o|inlining procedure: k10093 
o|inlining procedure: k10116 
o|inlining procedure: k10116 
o|consed rest parameter at call site: "(modules.scm:831) err2519" 1 
o|inlining procedure: k10160 
o|inlining procedure: k10160 
o|consed rest parameter at call site: "(modules.scm:836) err2519" 1 
o|contracted procedure: "(modules.scm:839) g25572558" 
o|merged explicitly consed rest parameter: args2572 
o|consed rest parameter at call site: "(modules.scm:850) err2571" 1 
o|inlining procedure: k10281 
o|inlining procedure: k10281 
o|inlining procedure: k10332 
o|inlining procedure: k10332 
o|inlining procedure: k10341 
o|inlining procedure: k10353 
o|inlining procedure: k10353 
o|removed unused parameter to known procedure: alias2633 "(modules.scm:864) match-functor-argument" 
o|inlining procedure: k10341 
o|removed unused parameter to known procedure: alias2633 "(modules.scm:878) match-functor-argument" 
o|consed rest parameter at call site: "(modules.scm:845) err2571" 1 
o|contracted procedure: "(modules.scm:842) g25672568" 
o|removed unused formal parameters: (alias2633) 
o|inlining procedure: k10461 
o|inlining procedure: k10472 
o|inlining procedure: k10472 
o|inlining procedure: k10461 
o|inlining procedure: k10556 
o|contracted procedure: "(modules.scm:905) g26702679" 
o|substituted constant variable: a10542 
o|inlining procedure: k10556 
o|inlining procedure: k10590 
o|inlining procedure: k10590 
o|inlining procedure: k10707 
o|inlining procedure: k10707 
o|contracted procedure: "(modules.scm:1127) g31513152" 
o|inlining procedure: k10759 
o|inlining procedure: k10759 
o|contracted procedure: "(modules.scm:1124) g31133114" 
o|inlining procedure: k10813 
o|inlining procedure: k10813 
o|contracted procedure: "(modules.scm:1120) g30753076" 
o|inlining procedure: k10867 
o|inlining procedure: k10867 
o|contracted procedure: "(modules.scm:1114) g30373038" 
o|inlining procedure: k10921 
o|inlining procedure: k10921 
o|contracted procedure: "(modules.scm:1108) g29993000" 
o|inlining procedure: k10975 
o|inlining procedure: k10975 
o|contracted procedure: "(modules.scm:1105) g29612962" 
o|inlining procedure: k11029 
o|inlining procedure: k11029 
o|contracted procedure: "(modules.scm:1102) g29232924" 
o|inlining procedure: k11083 
o|inlining procedure: k11083 
o|contracted procedure: "(modules.scm:1099) g28852886" 
o|inlining procedure: k11137 
o|inlining procedure: k11137 
o|contracted procedure: "(modules.scm:1086) g28472848" 
o|inlining procedure: k11191 
o|inlining procedure: k11191 
o|contracted procedure: "(modules.scm:1080) g28092810" 
o|inlining procedure: k11245 
o|inlining procedure: k11245 
o|contracted procedure: "(modules.scm:1077) g27712772" 
o|inlining procedure: k11299 
o|inlining procedure: k11299 
o|contracted procedure: "(modules.scm:1068) g27332734" 
o|inlining procedure: k11353 
o|inlining procedure: k11353 
o|contracted procedure: "(modules.scm:1065) g26952696" 
o|inlining procedure: k11407 
o|inlining procedure: k11407 
o|propagated global variable: r4rs-syntax2691 ##sys#scheme-macro-environment 
o|replaced variables: 1610 
o|removed binding forms: 522 
o|removed side-effect free assignment to unused variable: every 
o|removed side-effect free assignment to unused variable: any 
o|removed side-effect free assignment to unused variable: filter 
o|removed side-effect free assignment to unused variable: list-tabulate 
o|substituted constant variable: defined-list742 
o|substituted constant variable: exist-list743 
o|substituted constant variable: defined-syntax-list744 
o|substituted constant variable: undefined-list745 
o|substituted constant variable: import-forms746 
o|substituted constant variable: meta-import-forms747 
o|substituted constant variable: meta-expressions748 
o|substituted constant variable: saved-environments752 
o|substituted constant variable: r596011486 
o|substituted constant variable: r598611488 
o|removed call to pure procedure with unused result: "(modules.scm:206) chicken.base#void" 
o|removed call to pure procedure with unused result: "(modules.scm:221) chicken.base#void" 
o|substituted constant variable: r681111503 
o|substituted constant variable: r681111503 
o|substituted constant variable: r682711507 
o|removed call to pure procedure with unused result: "(modules.scm:348) chicken.base#void" 
o|substituted constant variable: prop1236 
o|substituted constant variable: r724011523 
o|substituted constant variable: r724011523 
o|substituted constant variable: r726611527 
o|substituted constant variable: r726611527 
o|contracted procedure: "(modules.scm:376) g13521361" 
o|substituted constant variable: r759511541 
o|substituted constant variable: prop1662 
o|removed call to pure procedure with unused result: "(modules.scm:551) chicken.base#void" 
o|removed call to pure procedure with unused result: "(modules.scm:554) chicken.base#void" 
o|substituted constant variable: r637811561 
o|substituted constant variable: r639011563 
o|converted assignments to bindings: (warn1121) 
o|substituted constant variable: r807411577 
o|removed call to pure procedure with unused result: "(modules.scm:489) chicken.base#void" 
o|substituted constant variable: r825011587 
o|substituted constant variable: r843611592 
o|converted assignments to bindings: (rename2168) 
o|converted assignments to bindings: (module-imports1871) 
o|substituted constant variable: r949211637 
o|substituted constant variable: r948211638 
o|substituted constant variable: r952711640 
o|removed call to pure procedure with unused result: "(modules.scm:775) chicken.base#void" 
o|removed call to pure procedure with unused result: "(modules.scm:738) chicken.base#void" 
o|removed call to pure procedure with unused result: "(modules.scm:737) chicken.base#void" 
o|removed call to pure procedure with unused result: "(modules.scm:736) chicken.base#void" 
o|substituted constant variable: r625211660 
o|substituted constant variable: r625211660 
o|removed call to pure procedure with unused result: "(modules.scm:790) chicken.base#void" 
o|removed call to pure procedure with unused result: "(modules.scm:800) chicken.base#void" 
o|converted assignments to bindings: (mrename2485) 
o|substituted constant variable: prop2529 
o|substituted constant variable: r1005211677 
o|substituted constant variable: prop2560 
o|substituted constant variable: r1033311689 
o|substituted constant variable: r1033311689 
o|substituted constant variable: r1035411694 
o|converted assignments to bindings: (merr2579) 
o|converted assignments to bindings: (err2571) 
o|substituted constant variable: prop2570 
o|simplifications: ((let . 6)) 
o|replaced variables: 121 
o|removed binding forms: 1413 
o|contracted procedure: k6027 
o|contracted procedure: k6107 
o|contracted procedure: k6910 
o|inlining procedure: k7566 
o|inlining procedure: k7738 
o|contracted procedure: k7914 
o|contracted procedure: k7932 
o|inlining procedure: k8026 
o|inlining procedure: k8026 
o|contracted procedure: k8148 
o|contracted procedure: k9467 
o|contracted procedure: k9470 
o|contracted procedure: k9473 
o|contracted procedure: k9932 
o|inlining procedure: k9935 
o|inlining procedure: k9935 
o|contracted procedure: k9976 
o|replaced variables: 9 
o|removed binding forms: 142 
o|contracted procedure: k7055 
o|inlining procedure: k7433 
o|substituted constant variable: r756712015 
o|inlining procedure: k7618 
o|contracted procedure: k7735 
o|contracted procedure: k9962 
o|contracted procedure: k10012 
o|contracted procedure: k10234 
o|simplifications: ((let . 1)) 
o|replaced variables: 2 
o|removed binding forms: 21 
o|substituted constant variable: r761912161 
o|substituted constant variable: r761912161 
o|replaced variables: 1 
o|removed binding forms: 4 
o|removed conditional forms: 1 
o|removed binding forms: 2 
o|simplifications: ((if . 22) (##core#call . 742) (let . 47)) 
o|  call simplifications:
o|    scheme#list?	2
o|    scheme#apply	3
o|    scheme#set-cdr!
o|    scheme#caddr
o|    ##sys#call-with-values	5
o|    scheme#cddr	3
o|    scheme#string?
o|    scheme#number?
o|    scheme#cdar	3
o|    scheme#length
o|    scheme#write-char	2
o|    scheme#list	7
o|    scheme#set-car!	3
o|    scheme#symbol?	16
o|    ##sys#cons	19
o|    ##sys#list	22
o|    scheme#not	14
o|    scheme#caar	11
o|    scheme#eq?	21
o|    scheme#assq	42
o|    scheme#cdr	34
o|    scheme#memq	6
o|    ##sys#check-list	38
o|    scheme#pair?	61
o|    scheme#cadr	14
o|    ##sys#setslot	30
o|    ##sys#slot	122
o|    ##sys#make-structure	2
o|    scheme#values	6
o|    ##sys#check-structure	26
o|    ##sys#block-ref	14
o|    scheme#null?	36
o|    scheme#car	52
o|    scheme#cons	123
o|contracted procedure: k4529 
o|contracted procedure: k4555 
o|contracted procedure: k5330 
o|contracted procedure: k5339 
o|contracted procedure: k5348 
o|contracted procedure: k5357 
o|contracted procedure: k5366 
o|contracted procedure: k5375 
o|contracted procedure: k5384 
o|contracted procedure: k5393 
o|contracted procedure: k5402 
o|contracted procedure: k5411 
o|contracted procedure: k5420 
o|contracted procedure: k5429 
o|contracted procedure: k5438 
o|contracted procedure: k5456 
o|contracted procedure: k5474 
o|contracted procedure: k5492 
o|contracted procedure: k5510 
o|contracted procedure: k5519 
o|contracted procedure: k5528 
o|contracted procedure: k5537 
o|contracted procedure: k5546 
o|contracted procedure: k5555 
o|contracted procedure: k5597 
o|contracted procedure: k5593 
o|contracted procedure: k5647 
o|contracted procedure: k5663 
o|contracted procedure: k5679 
o|contracted procedure: k5701 
o|contracted procedure: k5656 
o|contracted procedure: k5660 
o|contracted procedure: k5697 
o|contracted procedure: k5682 
o|contracted procedure: k5685 
o|contracted procedure: k5693 
o|contracted procedure: k5720 
o|contracted procedure: k5728 
o|contracted procedure: k5734 
o|contracted procedure: k5744 
o|contracted procedure: k5802 
o|contracted procedure: k5757 
o|contracted procedure: k5796 
o|contracted procedure: k5760 
o|contracted procedure: k5790 
o|contracted procedure: k5763 
o|contracted procedure: k5784 
o|contracted procedure: k5766 
o|contracted procedure: k5769 
o|contracted procedure: k5811 
o|contracted procedure: k5838 
o|contracted procedure: k5870 
o|contracted procedure: k5881 
o|contracted procedure: k5890 
o|contracted procedure: k5896 
o|contracted procedure: k5926 
o|contracted procedure: k5936 
o|contracted procedure: k5940 
o|contracted procedure: k5966 
o|contracted procedure: k5483 
o|contracted procedure: k5976 
o|contracted procedure: k5982 
o|contracted procedure: k6000 
o|contracted procedure: k6012 
o|contracted procedure: k6038 
o|contracted procedure: k6034 
o|contracted procedure: k6046 
o|contracted procedure: k6089 
o|contracted procedure: k6121 
o|contracted procedure: k6117 
o|contracted procedure: k6136 
o|contracted procedure: k6132 
o|contracted procedure: k6151 
o|contracted procedure: k6188 
o|contracted procedure: k6301 
o|contracted procedure: k6260 
o|contracted procedure: k6295 
o|contracted procedure: k6263 
o|contracted procedure: k6289 
o|contracted procedure: k6266 
o|contracted procedure: k6283 
o|contracted procedure: k6269 
o|contracted procedure: k6280 
o|contracted procedure: k6276 
o|inlining procedure: "(modules.scm:250) make-module" 
o|contracted procedure: k6580 
o|contracted procedure: k6591 
o|contracted procedure: k6717 
o|contracted procedure: k6597 
o|contracted procedure: k6600 
o|contracted procedure: k6611 
o|contracted procedure: k6620 
o|contracted procedure: k6623 
o|contracted procedure: k6638 
o|contracted procedure: k6648 
o|contracted procedure: k6652 
o|contracted procedure: k6666 
o|contracted procedure: k6673 
o|contracted procedure: k6698 
o|contracted procedure: k6704 
o|contracted procedure: k6708 
o|contracted procedure: k6750 
o|contracted procedure: k6746 
o|contracted procedure: k6742 
o|contracted procedure: k6786 
o|contracted procedure: k6790 
o|contracted procedure: k6961 
o|contracted procedure: k7005 
o|contracted procedure: k6794 
o|contracted procedure: k6798 
o|contracted procedure: k6878 
o|contracted procedure: k6883 
o|contracted procedure: k6886 
o|contracted procedure: k6892 
o|contracted procedure: k6899 
o|contracted procedure: k6916 
o|contracted procedure: k6802 
o|contracted procedure: k6782 
o|contracted procedure: k6778 
o|contracted procedure: k6813 
o|contracted procedure: k6829 
o|contracted procedure: k6874 
o|contracted procedure: k6835 
o|contracted procedure: k6843 
o|contracted procedure: k6860 
o|contracted procedure: k6850 
o|contracted procedure: k6928 
o|contracted procedure: k6931 
o|contracted procedure: k6934 
o|contracted procedure: k6942 
o|contracted procedure: k6950 
o|contracted procedure: k7017 
o|contracted procedure: k7020 
o|contracted procedure: k7023 
o|contracted procedure: k7031 
o|contracted procedure: k7039 
o|contracted procedure: k6999 
o|contracted procedure: k6969 
o|contracted procedure: k6976 
o|contracted procedure: k6987 
o|contracted procedure: k6991 
o|contracted procedure: k7058 
o|contracted procedure: k7089 
o|contracted procedure: k7075 
o|contracted procedure: k7082 
o|contracted procedure: k7117 
o|contracted procedure: k7095 
o|contracted procedure: k7110 
o|contracted procedure: k7106 
o|contracted procedure: k7102 
o|contracted procedure: k7443 
o|contracted procedure: k7126 
o|contracted procedure: k7155 
o|contracted procedure: k7184 
o|contracted procedure: k7190 
o|contracted procedure: k7210 
o|contracted procedure: k7228 
o|contracted procedure: k7236 
o|contracted procedure: k7242 
o|contracted procedure: k7254 
o|contracted procedure: k7262 
o|contracted procedure: k7268 
o|contracted procedure: k7282 
o|contracted procedure: k7278 
o|contracted procedure: k7286 
o|contracted procedure: k7307 
o|contracted procedure: k7317 
o|contracted procedure: k7321 
o|contracted procedure: k7330 
o|contracted procedure: k7340 
o|contracted procedure: k7344 
o|contracted procedure: k7372 
o|contracted procedure: k7347 
o|contracted procedure: k7368 
o|inlining procedure: "(modules.scm:385) make-module" 
o|contracted procedure: k7381 
o|contracted procedure: k7403 
o|contracted procedure: k7199 
o|contracted procedure: k7399 
o|contracted procedure: k7384 
o|contracted procedure: k7387 
o|contracted procedure: k7395 
o|contracted procedure: k7415 
o|contracted procedure: k7421 
o|contracted procedure: k7429 
o|contracted procedure: k7437 
o|contracted procedure: k7163 
o|contracted procedure: k7132 
o|contracted procedure: k7148 
o|contracted procedure: k7173 
o|inlining procedure: k7433 
o|contracted procedure: k7557 
o|contracted procedure: k7452 
o|contracted procedure: k7469 
o|contracted procedure: k7465 
o|contracted procedure: k7473 
o|contracted procedure: k7497 
o|contracted procedure: k7505 
o|contracted procedure: k7508 
o|contracted procedure: k7517 
o|inlining procedure: "(modules.scm:410) make-module" 
o|contracted procedure: k7529 
o|contracted procedure: k7532 
o|contracted procedure: k7535 
o|contracted procedure: k7543 
o|contracted procedure: k7551 
o|contracted procedure: k7572 
o|contracted procedure: k7566 
o|contracted procedure: k7597 
o|contracted procedure: k7647 
o|contracted procedure: k7600 
o|contracted procedure: k7609 
o|contracted procedure: k7636 
o|contracted procedure: k7612 
o|contracted procedure: k7618 
o|contracted procedure: k7650 
o|contracted procedure: k8315 
o|contracted procedure: k7659 
o|contracted procedure: k7674 
o|contracted procedure: k7693 
o|contracted procedure: k7707 
o|contracted procedure: k7714 
o|contracted procedure: k7813 
o|contracted procedure: k7751 
o|contracted procedure: k7782 
o|contracted procedure: k7794 
o|contracted procedure: k7804 
o|contracted procedure: k7808 
o|contracted procedure: k7816 
o|contracted procedure: k7839 
o|contracted procedure: k7849 
o|contracted procedure: k7853 
o|contracted procedure: k7865 
o|contracted procedure: k7900 
o|contracted procedure: k7923 
o|contracted procedure: k7926 
o|contracted procedure: k7948 
o|contracted procedure: k5501 
o|contracted procedure: k7977 
o|contracted procedure: k7987 
o|contracted procedure: k7991 
o|contracted procedure: k8008 
o|contracted procedure: k8014 
o|contracted procedure: k8022 
o|contracted procedure: k8030 
o|contracted procedure: k7894 
o|contracted procedure: k7873 
o|contracted procedure: k7876 
o|contracted procedure: k6380 
o|contracted procedure: k6392 
o|contracted procedure: k6564 
o|contracted procedure: k6398 
o|contracted procedure: k6410 
o|contracted procedure: k6419 
o|contracted procedure: k6426 
o|contracted procedure: k6556 
o|contracted procedure: k6432 
o|contracted procedure: k6445 
o|contracted procedure: k6467 
o|contracted procedure: k6471 
o|inlining procedure: k6457 
o|inlining procedure: k6457 
o|contracted procedure: k6485 
o|contracted procedure: k6531 
o|contracted procedure: k6496 
o|contracted procedure: k6513 
o|contracted procedure: k6503 
o|contracted procedure: k6527 
o|contracted procedure: k8045 
o|contracted procedure: k8055 
o|contracted procedure: k8059 
o|contracted procedure: k8226 
o|contracted procedure: k8067 
o|contracted procedure: k8076 
o|contracted procedure: k8079 
o|contracted procedure: k8220 
o|contracted procedure: k8082 
o|contracted procedure: k8088 
o|contracted procedure: k8110 
o|contracted procedure: k8100 
o|contracted procedure: k8122 
o|contracted procedure: k8185 
o|contracted procedure: k8206 
o|contracted procedure: k8217 
o|contracted procedure: k8229 
o|contracted procedure: k8252 
o|contracted procedure: k8278 
o|contracted procedure: k8287 
o|contracted procedure: k8309 
o|contracted procedure: k7683 
o|contracted procedure: k8305 
o|contracted procedure: k8290 
o|contracted procedure: k8293 
o|contracted procedure: k8301 
o|contracted procedure: k8525 
o|contracted procedure: k8544 
o|contracted procedure: k8553 
o|contracted procedure: k8581 
o|contracted procedure: k8623 
o|contracted procedure: k9391 
o|contracted procedure: k8636 
o|contracted procedure: k8642 
o|contracted procedure: k8664 
o|contracted procedure: k8682 
o|contracted procedure: k8690 
o|contracted procedure: k8704 
o|contracted procedure: k8700 
o|contracted procedure: k8713 
o|contracted procedure: k8723 
o|contracted procedure: k8727 
o|contracted procedure: k8788 
o|contracted procedure: k8730 
o|contracted procedure: k8742 
o|contracted procedure: k8746 
o|contracted procedure: k8752 
o|contracted procedure: k8764 
o|contracted procedure: k8768 
o|contracted procedure: k8780 
o|contracted procedure: k8792 
o|contracted procedure: k8814 
o|contracted procedure: k8832 
o|contracted procedure: k8844 
o|contracted procedure: k8852 
o|contracted procedure: k8866 
o|contracted procedure: k8862 
o|contracted procedure: k8875 
o|contracted procedure: k8885 
o|contracted procedure: k8889 
o|contracted procedure: k8930 
o|contracted procedure: k8892 
o|contracted procedure: k8904 
o|contracted procedure: k8912 
o|contracted procedure: k8924 
o|contracted procedure: k8971 
o|contracted procedure: k8933 
o|contracted procedure: k8945 
o|contracted procedure: k8953 
o|contracted procedure: k8965 
o|contracted procedure: k8975 
o|contracted procedure: k8997 
o|contracted procedure: k9015 
o|contracted procedure: k9027 
o|contracted procedure: k9035 
o|contracted procedure: k9038 
o|contracted procedure: k9044 
o|contracted procedure: k9058 
o|contracted procedure: k9054 
o|contracted procedure: k9067 
o|contracted procedure: k9077 
o|contracted procedure: k9081 
o|contracted procedure: k9090 
o|contracted procedure: k9112 
o|contracted procedure: k9108 
o|contracted procedure: k9093 
o|contracted procedure: k9096 
o|contracted procedure: k9104 
o|contracted procedure: k9168 
o|contracted procedure: k9118 
o|contracted procedure: k9130 
o|contracted procedure: k9146 
o|contracted procedure: k9150 
o|contracted procedure: k9142 
o|contracted procedure: k9134 
o|contracted procedure: k9162 
o|contracted procedure: k9221 
o|contracted procedure: k9171 
o|contracted procedure: k9183 
o|contracted procedure: k9199 
o|contracted procedure: k9203 
o|contracted procedure: k9195 
o|contracted procedure: k9187 
o|contracted procedure: k9215 
o|contracted procedure: k9225 
o|contracted procedure: k9247 
o|contracted procedure: k9281 
o|contracted procedure: k9288 
o|contracted procedure: k9292 
o|contracted procedure: k9295 
o|contracted procedure: k9302 
o|contracted procedure: k9305 
o|contracted procedure: k9317 
o|contracted procedure: k9320 
o|contracted procedure: k9323 
o|contracted procedure: k9331 
o|contracted procedure: k9339 
o|contracted procedure: k9351 
o|contracted procedure: k9354 
o|contracted procedure: k9357 
o|contracted procedure: k9365 
o|contracted procedure: k9373 
o|contracted procedure: k9380 
o|contracted procedure: k9417 
o|contracted procedure: k9426 
o|contracted procedure: k9429 
o|contracted procedure: k9441 
o|contracted procedure: k9451 
o|contracted procedure: k9455 
o|contracted procedure: k9478 
o|contracted procedure: k9484 
o|contracted procedure: k9508 
o|contracted procedure: k9504 
o|contracted procedure: k9494 
o|contracted procedure: k9515 
o|contracted procedure: k9551 
o|contracted procedure: k9523 
o|contracted procedure: k9547 
o|contracted procedure: k9541 
o|contracted procedure: k9529 
o|contracted procedure: k9558 
o|contracted procedure: k9612 
o|contracted procedure: k9630 
o|contracted procedure: k9637 
o|contracted procedure: k9649 
o|contracted procedure: k9671 
o|contracted procedure: k9667 
o|contracted procedure: k9652 
o|contracted procedure: k9655 
o|contracted procedure: k9663 
o|contracted procedure: k9683 
o|contracted procedure: k9705 
o|contracted procedure: k9701 
o|contracted procedure: k9686 
o|contracted procedure: k9689 
o|contracted procedure: k9697 
o|contracted procedure: k9815 
o|contracted procedure: k9730 
o|contracted procedure: k9734 
o|contracted procedure: k9741 
o|contracted procedure: k9753 
o|contracted procedure: k9775 
o|contracted procedure: k9771 
o|contracted procedure: k9756 
o|contracted procedure: k9759 
o|contracted procedure: k9767 
o|contracted procedure: k9787 
o|contracted procedure: k9809 
o|contracted procedure: k9805 
o|contracted procedure: k9790 
o|contracted procedure: k9793 
o|contracted procedure: k9801 
o|contracted procedure: k9827 
o|contracted procedure: k9837 
o|contracted procedure: k9841 
o|contracted procedure: k9850 
o|contracted procedure: k9860 
o|contracted procedure: k9864 
o|contracted procedure: k5465 
o|contracted procedure: k9882 
o|contracted procedure: k5447 
o|contracted procedure: k9897 
o|contracted procedure: k9911 
o|contracted procedure: k9915 
o|contracted procedure: k6200 
o|contracted procedure: k6222 
o|contracted procedure: k6218 
o|contracted procedure: k6233 
o|contracted procedure: k6229 
o|contracted procedure: k6243 
o|contracted procedure: k6251 
o|contracted procedure: k9965 
o|contracted procedure: k9973 
o|contracted procedure: k9982 
o|contracted procedure: k10024 
o|contracted procedure: k10030 
o|contracted procedure: k10210 
o|contracted procedure: k10039 
o|contracted procedure: k10054 
o|contracted procedure: k10206 
o|contracted procedure: k10060 
o|contracted procedure: k10066 
o|contracted procedure: k10072 
o|contracted procedure: k10202 
o|contracted procedure: k10087 
o|contracted procedure: k10198 
o|contracted procedure: k10096 
o|contracted procedure: k10113 
o|contracted procedure: k10136 
o|contracted procedure: k10142 
o|contracted procedure: k10149 
o|contracted procedure: k10163 
o|contracted procedure: k10174 
o|contracted procedure: k10192 
o|contracted procedure: k10180 
o|contracted procedure: k10226 
o|contracted procedure: k10222 
o|contracted procedure: k10246 
o|contracted procedure: k10249 
o|contracted procedure: k10261 
o|contracted procedure: k10269 
o|contracted procedure: k10272 
o|contracted procedure: k10265 
o|contracted procedure: k10284 
o|contracted procedure: k10306 
o|contracted procedure: k10302 
o|contracted procedure: k10287 
o|contracted procedure: k10290 
o|contracted procedure: k10298 
o|contracted procedure: k10335 
o|contracted procedure: k10328 
o|contracted procedure: k10324 
o|contracted procedure: k10320 
o|contracted procedure: k10344 
o|contracted procedure: k10356 
o|contracted procedure: k10359 
o|contracted procedure: k10399 
o|contracted procedure: k10365 
o|contracted procedure: k10369 
o|contracted procedure: k10382 
o|contracted procedure: k10405 
o|contracted procedure: k10411 
o|contracted procedure: k10414 
o|contracted procedure: k10418 
o|contracted procedure: k10421 
o|contracted procedure: k10434 
o|contracted procedure: k10446 
o|contracted procedure: k10464 
o|contracted procedure: k10496 
o|contracted procedure: k10469 
o|contracted procedure: k10475 
o|contracted procedure: k10482 
o|contracted procedure: k10502 
o|contracted procedure: k10511 
o|contracted procedure: k10534 
o|contracted procedure: k10547 
o|contracted procedure: k10559 
o|contracted procedure: k10562 
o|contracted procedure: k10565 
o|contracted procedure: k10573 
o|contracted procedure: k10581 
o|contracted procedure: k10593 
o|contracted procedure: k10603 
o|contracted procedure: k10607 
o|contracted procedure: k10727 
o|contracted procedure: k10701 
o|contracted procedure: k10710 
o|contracted procedure: k10720 
o|contracted procedure: k10742 
o|contracted procedure: k10750 
o|contracted procedure: k10762 
o|contracted procedure: k10765 
o|contracted procedure: k10768 
o|contracted procedure: k10776 
o|contracted procedure: k10784 
o|contracted procedure: k10796 
o|contracted procedure: k10804 
o|contracted procedure: k10816 
o|contracted procedure: k10819 
o|contracted procedure: k10822 
o|contracted procedure: k10830 
o|contracted procedure: k10838 
o|contracted procedure: k10850 
o|contracted procedure: k10858 
o|contracted procedure: k10870 
o|contracted procedure: k10873 
o|contracted procedure: k10876 
o|contracted procedure: k10884 
o|contracted procedure: k10892 
o|contracted procedure: k10904 
o|contracted procedure: k10912 
o|contracted procedure: k10924 
o|contracted procedure: k10927 
o|contracted procedure: k10930 
o|contracted procedure: k10938 
o|contracted procedure: k10946 
o|contracted procedure: k10958 
o|contracted procedure: k10966 
o|contracted procedure: k10978 
o|contracted procedure: k10981 
o|contracted procedure: k10984 
o|contracted procedure: k10992 
o|contracted procedure: k11000 
o|contracted procedure: k11012 
o|contracted procedure: k11020 
o|contracted procedure: k11032 
o|contracted procedure: k11035 
o|contracted procedure: k11038 
o|contracted procedure: k11046 
o|contracted procedure: k11054 
o|contracted procedure: k11066 
o|contracted procedure: k11074 
o|contracted procedure: k11086 
o|contracted procedure: k11089 
o|contracted procedure: k11092 
o|contracted procedure: k11100 
o|contracted procedure: k11108 
o|contracted procedure: k11120 
o|contracted procedure: k11128 
o|contracted procedure: k11140 
o|contracted procedure: k11143 
o|contracted procedure: k11146 
o|contracted procedure: k11154 
o|contracted procedure: k11162 
o|contracted procedure: k11174 
o|contracted procedure: k11182 
o|contracted procedure: k11194 
o|contracted procedure: k11197 
o|contracted procedure: k11200 
o|contracted procedure: k11208 
o|contracted procedure: k11216 
o|contracted procedure: k11228 
o|contracted procedure: k11236 
o|contracted procedure: k11248 
o|contracted procedure: k11251 
o|contracted procedure: k11254 
o|contracted procedure: k11262 
o|contracted procedure: k11270 
o|contracted procedure: k11282 
o|contracted procedure: k11290 
o|contracted procedure: k11302 
o|contracted procedure: k11305 
o|contracted procedure: k11308 
o|contracted procedure: k11316 
o|contracted procedure: k11324 
o|contracted procedure: k11336 
o|contracted procedure: k11344 
o|contracted procedure: k11356 
o|contracted procedure: k11359 
o|contracted procedure: k11362 
o|contracted procedure: k11370 
o|contracted procedure: k11378 
o|contracted procedure: k11390 
o|contracted procedure: k11398 
o|contracted procedure: k11410 
o|contracted procedure: k11413 
o|contracted procedure: k11416 
o|contracted procedure: k11424 
o|contracted procedure: k11432 
o|simplifications: ((if . 5) (let . 214)) 
o|removed binding forms: 621 
o|inlining procedure: "(modules.scm:120) module-sexports" 
o|inlining procedure: "(modules.scm:119) module-vexports" 
o|inlining procedure: "(modules.scm:118) module-export-list" 
o|removed side-effect free assignment to unused variable: make-module 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest952954 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest952954 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest952954 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest952954 0 
o|inlining procedure: "(modules.scm:160) module-saved-environments" 
o|inlining procedure: "(modules.scm:157) set-module-saved-environments!" 
o|inlining procedure: "(modules.scm:179) set-module-exist-list!" 
o|inlining procedure: "(modules.scm:178) set-module-sexports!" 
o|inlining procedure: "(modules.scm:178) module-sexports" 
o|inlining procedure: "(modules.scm:169) module-exist-list" 
o|inlining procedure: "(modules.scm:180) set-module-export-list!" 
o|inlining procedure: "(modules.scm:167) module-export-list" 
o|inlining procedure: "(modules.scm:186) module-meta-expressions" 
o|inlining procedure: "(modules.scm:207) set-module-defined-list!" 
o|inlining procedure: "(modules.scm:210) module-defined-list" 
o|inlining procedure: "(modules.scm:204) set-module-exist-list!" 
o|inlining procedure: "(modules.scm:204) module-exist-list" 
o|inlining procedure: "(modules.scm:200) module-name" 
o|inlining procedure: "(modules.scm:196) module-export-list" 
o|inlining procedure: "(modules.scm:227) set-module-defined-syntax-list!" 
o|inlining procedure: "(modules.scm:229) module-defined-syntax-list" 
o|inlining procedure: "(modules.scm:223) set-module-defined-list!" 
o|inlining procedure: "(modules.scm:226) module-defined-list" 
o|inlining procedure: "(modules.scm:217) module-name" 
o|inlining procedure: "(modules.scm:214) module-export-list" 
o|inlining procedure: "(modules.scm:233) set-module-defined-syntax-list!" 
o|inlining procedure: "(modules.scm:235) module-defined-syntax-list" 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest10981102 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest10981102 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest10981102 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest10981102 0 
o|substituted constant variable: iexports88212797 
o|inlining procedure: "(modules.scm:354) module-defined-syntax-list" 
o|inlining procedure: "(modules.scm:340) module-vexports" 
o|inlining procedure: "(modules.scm:339) module-iexports" 
o|inlining procedure: "(modules.scm:333) module-library" 
o|inlining procedure: "(modules.scm:332) module-name" 
o|inlining procedure: "(modules.scm:329) module-meta-expressions" 
o|inlining procedure: "(modules.scm:319) module-meta-import-forms" 
o|inlining procedure: "(modules.scm:318) module-sexports" 
o|inlining procedure: "(modules.scm:317) module-import-forms" 
o|inlining procedure: "(modules.scm:316) module-name" 
o|inlining procedure: "(modules.scm:315) module-defined-list" 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest13271333 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest13271333 0 
o|inlining procedure: "(modules.scm:401) set-module-saved-environments!" 
o|contracted procedure: k7350 
o|substituted constant variable: explist87912817 
o|contracted procedure: k7138 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest14551459 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest14551459 0 
o|inlining procedure: "(modules.scm:422) set-module-saved-environments!" 
o|inlining procedure: "(modules.scm:426) module-sexports" 
o|inlining procedure: "(modules.scm:425) module-vexports" 
o|substituted constant variable: explist87912829 
o|substituted constant variable: iexports88212832 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest15031506 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest15031506 0 
o|inlining procedure: "(modules.scm:437) module-exist-list" 
o|inlining procedure: "(modules.scm:436) module-export-list" 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest15401542 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest15401542 0 
o|inlining procedure: "(modules.scm:566) set-module-saved-environments!" 
o|inlining procedure: "(modules.scm:563) set-module-iexports!" 
o|inlining procedure: "(modules.scm:565) module-iexports" 
o|inlining procedure: "(modules.scm:562) set-module-sexports!" 
o|inlining procedure: "(modules.scm:257) module-defined-list" 
o|inlining procedure: "(modules.scm:256) module-name" 
o|inlining procedure: "(modules.scm:255) module-export-list" 
o|contracted procedure: k8116 
o|contracted procedure: k8145 
o|inlining procedure: "(modules.scm:462) module-sexports" 
o|inlining procedure: "(modules.scm:459) module-defined-syntax-list" 
o|inlining procedure: "(modules.scm:456) module-exist-list" 
o|inlining procedure: "(modules.scm:455) module-defined-list" 
o|inlining procedure: "(modules.scm:454) module-name" 
o|inlining procedure: "(modules.scm:453) module-export-list" 
o|inlining procedure: "(modules.scm:625) module-iexports" 
o|inlining procedure: "(modules.scm:624) module-sexports" 
o|inlining procedure: "(modules.scm:623) module-vexports" 
o|inlining procedure: "(modules.scm:622) module-name" 
o|inlining procedure: "(modules.scm:621) module-library" 
o|inlining procedure: "(modules.scm:620) module-name" 
o|inlining procedure: "(modules.scm:772) set-module-iexports!" 
o|inlining procedure: "(modules.scm:774) module-iexports" 
o|inlining procedure: "(modules.scm:759) set-module-exist-list!" 
o|inlining procedure: "(modules.scm:761) module-exist-list" 
o|inlining procedure: "(modules.scm:758) set-module-sexports!" 
o|inlining procedure: "(modules.scm:758) module-sexports" 
o|inlining procedure: "(modules.scm:765) set-module-export-list!" 
o|inlining procedure: "(modules.scm:768) module-export-list" 
o|inlining procedure: "(modules.scm:756) module-export-list" 
o|inlining procedure: "(modules.scm:732) module-meta-import-forms" 
o|inlining procedure: "(modules.scm:735) module-import-forms" 
o|inlining procedure: "(modules.scm:793) module-name" 
o|inlining procedure: "(modules.scm:793) module-name" 
o|inlining procedure: k6247 
o|inlining procedure: k6247 
o|inlining procedure: "(modules.scm:793) module-name" 
o|contracted procedure: k10119 
o|inlining procedure: "(modules.scm:894) module-sexports" 
o|inlining procedure: "(modules.scm:893) module-vexports" 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest31933195 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest31933195 0 
o|inlining procedure: "(modules.scm:1146) module-saved-environments" 
o|replaced variables: 17 
o|removed binding forms: 6 
o|removed side-effect free assignment to unused variable: module-library 
o|removed side-effect free assignment to unused variable: module-export-list 
o|removed side-effect free assignment to unused variable: set-module-export-list! 
o|removed side-effect free assignment to unused variable: module-defined-list 
o|removed side-effect free assignment to unused variable: set-module-defined-list! 
o|removed side-effect free assignment to unused variable: module-exist-list 
o|removed side-effect free assignment to unused variable: set-module-exist-list! 
o|removed side-effect free assignment to unused variable: module-defined-syntax-list 
o|removed side-effect free assignment to unused variable: set-module-defined-syntax-list! 
o|removed side-effect free assignment to unused variable: module-import-forms 
o|removed side-effect free assignment to unused variable: module-meta-import-forms 
o|removed side-effect free assignment to unused variable: module-meta-expressions 
o|removed side-effect free assignment to unused variable: module-vexports 
o|removed side-effect free assignment to unused variable: module-sexports 
o|removed side-effect free assignment to unused variable: set-module-sexports! 
o|removed side-effect free assignment to unused variable: module-iexports 
o|removed side-effect free assignment to unused variable: set-module-iexports! 
o|removed side-effect free assignment to unused variable: module-saved-environments 
o|removed side-effect free assignment to unused variable: set-module-saved-environments! 
(o x)|known list op on rest arg sublist: ##core#rest-null? r5761 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r5761 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r5761 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r5761 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r6264 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r6264 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r6264 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r6264 1 
o|inlining procedure: k6272 
o|replaced variables: 255 
o|removed binding forms: 23 
o|inlining procedure: k5577 
o|inlining procedure: k5917 
o|inlining procedure: k6073 
o|inlining procedure: k7069 
o|contracted procedure: k7216 
o|contracted procedure: k7458 
o|inlining procedure: k7493 
o|inlining procedure: k7968 
o|inlining procedure: k8236 
o|inlining procedure: k8611 
o|inlining procedure: k9609 
o|inlining procedure: k9716 
o|inlining procedure: k9878 
o|inlining procedure: k9893 
o|inlining procedure: k9942 
o|inlining procedure: k994212085 
o|inlining procedure: k994212089 
o|inlining procedure: k10489 
o|inlining procedure: k10724 
o|removed binding forms: 202 
o|contracted procedure: k5569 
o|contracted procedure: k5573 
o|contracted procedure: k5864 
o|contracted procedure: k5873 
o|contracted procedure: k5970 
o|contracted procedure: k6080 
o|contracted procedure: k6042 
o|contracted procedure: k6050 
o|contracted procedure: k6161 
o|contracted procedure: k6098 
o|contracted procedure: k6125 
o|contracted procedure: k6140 
o|contracted procedure: k6178 
o|contracted procedure: k6723 
o|contracted procedure: k6726 
o|contracted procedure: k6729 
o|contracted procedure: k6732 
o|contracted procedure: k6735 
o|contracted procedure: k7050 
o|contracted procedure: k7046 
o|contracted procedure: k7002 
o|contracted procedure: k6957 
o|contracted procedure: k6820 
o|contracted procedure: k7489 
o|contracted procedure: k7581 
o|contracted procedure: k7662 
o|contracted procedure: k7665 
o|contracted procedure: k7668 
o|contracted procedure: k7671 
o|contracted procedure: k7690 
o|contracted procedure: k6310 
o|contracted procedure: k6313 
o|contracted procedure: k6316 
o|contracted procedure: k8591 
o|contracted procedure: k8595 
o|contracted procedure: k8599 
o|contracted procedure: k8603 
o|contracted procedure: k8607 
o|contracted procedure: k9592 
o|contracted procedure: k9626 
o|contracted procedure: k9727 
o|contracted procedure: k10493 
o|simplifications: ((let . 10)) 
o|removed binding forms: 59 
o|direct leaf routine/allocation: g10141015 3 
o|direct leaf routine/allocation: loop1517 0 
o|direct leaf routine/allocation: g31633172 0 
o|direct leaf routine/allocation: g31253134 0 
o|direct leaf routine/allocation: g30873096 0 
o|direct leaf routine/allocation: g30493058 0 
o|direct leaf routine/allocation: g30113020 0 
o|direct leaf routine/allocation: g29732982 0 
o|direct leaf routine/allocation: g29352944 0 
o|direct leaf routine/allocation: g28972906 0 
o|direct leaf routine/allocation: g28592868 0 
o|direct leaf routine/allocation: g28212830 0 
o|direct leaf routine/allocation: g27832792 0 
o|direct leaf routine/allocation: g27452754 0 
o|direct leaf routine/allocation: g27072716 0 
o|converted assignments to bindings: (loop1517) 
o|contracted procedure: "(modules.scm:1052) k10780" 
o|contracted procedure: "(modules.scm:1052) k10834" 
o|contracted procedure: "(modules.scm:1052) k10888" 
o|contracted procedure: "(modules.scm:1052) k10942" 
o|contracted procedure: "(modules.scm:1052) k10996" 
o|contracted procedure: "(modules.scm:1052) k11050" 
o|contracted procedure: "(modules.scm:1052) k11104" 
o|contracted procedure: "(modules.scm:1052) k11158" 
o|contracted procedure: "(modules.scm:1052) k11212" 
o|contracted procedure: "(modules.scm:1052) k11266" 
o|contracted procedure: "(modules.scm:1052) k11320" 
o|contracted procedure: "(modules.scm:1052) k11374" 
o|contracted procedure: "(modules.scm:1052) k11428" 
o|simplifications: ((let . 1)) 
o|removed binding forms: 13 
o|direct leaf routine with hoistable closures/allocation: g9981005 (g10141015) 3 
o|contracted procedure: "(modules.scm:172) k5929" 
o|removed binding forms: 2 
x|eliminated type checks:
x|  C_i_check_list_2:	13
o|customizable procedures: (map-loop27012726 map-loop27392764 map-loop27772802 map-loop28152840 map-loop28532878 map-loop28912916 map-loop29292954 map-loop29672992 map-loop30053030 map-loop30433068 map-loop30813106 map-loop31193144 map-loop31573182 g26432650 for-each-loop26422657 map-loop26642682 k10478 loop2606 merr2579 match-functor-argument loop22613 map-loop25822599 err2571 loop22542 loop2530 iface2520 err2519 g25042505 mrename2485 g24932494 g10901091 k6211 g23042321 for-each-loop23032331 g23142336 for-each-loop23132344 map-loop24102427 map-loop24362453 map-loop23562373 map-loop23822399 k9564 g22432250 for-each-loop22422277 map-loop21752192 rename2168 map-loop22012218 tostr1861 g21352136 loop2063 g21302131 loop2072 map-loop20962113 g20842091 for-each-loop20832120 g20302031 loop1984 g20252026 loop1993 g20052012 for-each-loop20042015 g19511952 g19471948 loop1914 g19271934 for-each-loop19261937 warn1860 loop1875 module-imports1871 find-module/import-library map-loop15571575 loop1583 g16161617 id-string1608 fail1607 loop1591 g16251632 for-each-loop16241697 g11551156 g11481149 warn1121 loop21139 loop1133 k8011 map-loop17051730 g17411750 for-each-loop17401757 g16411648 for-each-loop16401652 g16721679 for-each-loop16711684 k7588 g14751484 map-loop14691490 k7418 map-loop13461364 map-loop13741392 g14071424 for-each-loop14061430 g14171435 for-each-loop14161441 merge-se k6758 k6766 k7035 map-loop12391263 g12781287 map-loop12721305 loop1312 k6806 lp1202 g11861193 for-each-loop11851196 loop1172 find-export module-rename delete check-for-redef for-each-loop9971017 g981982 k5820 g943944 loop934 map-loop899917 loop254) 
o|calls to known targets: 292 
o|identified direct recursive calls: f_5674 1 
o|unused rest argument: rest952954 f_5755 
o|identified direct recursive calls: f_5921 1 
o|unused rest argument: rest10981102 f_6258 
o|identified direct recursive calls: f_6586 1 
o|identified direct recursive calls: f_6824 1 
o|unused rest argument: rest13271333 f_7124 
o|unused rest argument: rest14551459 f_7450 
o|unused rest argument: rest15031506 f_7564 
o|identified direct recursive calls: f_7592 3 
o|unused rest argument: _1548 f_8318 
o|identified direct recursive calls: f_6387 1 
o|identified direct recursive calls: f_8071 1 
o|unused rest argument: rest15401542 f_7657 
o|identified direct recursive calls: f_8677 1 
o|identified direct recursive calls: f_8839 1 
o|identified direct recursive calls: f_8827 1 
o|identified direct recursive calls: f_9085 1 
o|identified direct recursive calls: f_9022 1 
o|identified direct recursive calls: f_9010 1 
o|identified direct recursive calls: f_9644 1 
o|identified direct recursive calls: f_9678 1 
o|identified direct recursive calls: f_9748 1 
o|identified direct recursive calls: f_9782 1 
o|identified direct recursive calls: f_10158 1 
o|identified direct recursive calls: f_10049 2 
o|identified direct recursive calls: f_10279 1 
o|unused rest argument: rest31933195 f_10699 
o|identified direct recursive calls: f_10757 1 
o|identified direct recursive calls: f_10811 1 
o|identified direct recursive calls: f_10865 1 
o|identified direct recursive calls: f_10919 1 
o|identified direct recursive calls: f_10973 1 
o|identified direct recursive calls: f_11027 1 
o|identified direct recursive calls: f_11081 1 
o|identified direct recursive calls: f_11135 1 
o|identified direct recursive calls: f_11189 1 
o|identified direct recursive calls: f_11243 1 
o|identified direct recursive calls: f_11297 1 
o|identified direct recursive calls: f_11351 1 
o|identified direct recursive calls: f_11405 1 
o|fast box initializations: 70 
o|fast global references: 35 
o|fast global assignments: 8 
o|dropping unused closure argument: f_10456 
o|dropping unused closure argument: f_4518 
o|dropping unused closure argument: f_5974 
o|dropping unused closure argument: f_6568 
o|dropping unused closure argument: f_7579 
o|dropping unused closure argument: f_8476 
o|dropping unused closure argument: f_8506 
o|dropping unused closure argument: f_9901 
*/
/* end of file */
